import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withOptions } from '@storybook/addon-options';

import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { reducer as form } from 'redux-form';
import { composeWithDevTools } from 'redux-devtools-extension';

import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
import { MuiThemeProvider } from '@material-ui/core/styles';

import theme from '../src/assets/theme';
import dialogReducer from '../src/redux/dialogs/reducer';
import '../src/index.scss';

const styles = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  border: '1px solid rgb(238, 238, 238)',
  minHeight: '20em',
  flexDirection: 'column'
};

const fakeStore = createStore(
  combineReducers({
    form,
    dialogs: dialogReducer
  }),
  composeWithDevTools(
    applyMiddleware(
      thunk
    ),
  ),
);

const CenterDecorator = (storyFn) => (
  <Provider store={fakeStore}>
    <MuiThemeProvider theme={theme}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <div style={styles}>
          { storyFn() }
        </div>
      </MuiPickersUtilsProvider>
    </MuiThemeProvider>
  </Provider>
);

/**
 * ❌ Doesn't work yet with Storybook v5
 * API is not expected to change
 * @see https://github.com/storybooks/storybook/issues/5665
 */
addDecorator(
  withOptions({
    /**
     * name to display in the top left corner
     * @type {String}
     */
    name: 'MyUnisoft React Components',
    showSearchBox: false,
    /**
     * show addon panel as a vertical panel on the right
     * @type {Boolean}
     */
    addonPanelInRight: true,
  })
);

/**
 * Applied to all stories.
 * Can be overriden in each story if needed
 */
addDecorator(CenterDecorator);
addDecorator(withKnobs);

function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

function loadStories() {
  requireAll(require.context("../src", true, /\.story\.jsx?$/));
}

configure(loadStories, module);
