#!/bin/sh

BRANCH=`git name-rev --name-only HEAD`
if [ $BRANCH != 'dev' ] && [ $BRANCH != 'prod' ] && [ $BRANCH != 'preprod' ]
  then
    if [ `cat $1 | grep -oEi '#time'` ]
      then
          if [ "echo $BRANCH | grep -oEi 'MYUN-.\d+'" ]
            then
              ISSUE=`echo $BRANCH | grep -oEi 'MYUN-.\d+'`
              echo "$ISSUE"' : '$(cat "$1") > "$1"
            else
              echo "Branch doesn't follow pattern MYUN-*"
              exit 1
          fi
    else
      echo "Time is mandatory if you want to commit something"
      exit 1
    fi
fi
