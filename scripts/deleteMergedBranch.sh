#!/bin/sh

for branch in $(git branch --remote --merged origin/dev | grep -v 'dev' | grep -v 'preprod' | grep -v 'prod' | cut -b 10-); do
  if [ $branch == 'preprod' ] || [ $branch == 'prod' ] || [ $branch == 'preprod' ]
    then
      echo "WARNING THIS MUST NOT BE DELETED"
    else
      git push origin --delete $branch
  fi
done