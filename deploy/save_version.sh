#!/bin/bash

VERSION="1.2"
# Note: services apiVersions set in src/common/config/server.js

# version build index
BUILD=$(git rev-list --count --first-parent HEAD)

git log -1 --pretty=format:'{"version": "'$VERSION'","commit": {"hash": "%h","comment":"%f","author":"%aN", "date":"%aI", "build":'$BUILD'}}' > ./src/version.json
