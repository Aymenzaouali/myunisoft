#!/bin/bash

echo $BRANCH
if [ "$BRANCH" == 'preprod' ]
  then
    echo "COPY CONFIG FILE FOR PREPROD"
    cp ./deploy/preprod/server.js ./src/common/config
elif [ "$BRANCH" == 'prod' ]
  then
    echo "COPY CONFIG FILE FOR PROD"
    cp ./deploy/prod/server.js ./src/common/config
else
    echo "COPY CONFIG FILE FOR DEV"
    cp ./deploy/dev/server.js ./src/common/config
fi
