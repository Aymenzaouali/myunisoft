#!/bin/bash

echo $BRANCH

if [ "$BRANCH" == 'preprod' ]
  then
    echo "DEPLOYING PREPROD"
    scp -r -P 522 -i /var/jenkins_home/.ssh/id_rsa build/* jenkins@palpatine.myunisoft.fr:/home/jenkins/preprod
    # scp -r -P 522 -i /var/jenkins_home/.ssh/id_rsa build/* vs171@54.36.201.220:/home/jenkins/preprod
    # scp -r -P 522 -i /var/jenkins_home/.ssh/id_rsa build/* vs172@54.36.201.222:/home/jenkins/preprod
elif [ "$BRANCH" == 'prod' ]
  then
    echo "DEPLOYING PROD"
    scp -r -P 522 -i /var/jenkins_home/.ssh/id_rsa build/* jenkins@app.myunisoft.fr:/home/jenkins/prod
    # scp -r -P 522 -i /var/jenkins_home/.ssh/id_rsa build/* vs71@54.36.201.219:/home/prod
    # scp -r -P 522 -i /var/jenkins_home/.ssh/id_rsa build/* vs72@54.36.201.221:/home/prod
else
    echo "DEPLOYING DEV"
    scp -r -P 522 -i /var/jenkins_home/.ssh/id_rsa build jenkins@palpatine.myunisoft.fr:/home/jenkins/
fi
