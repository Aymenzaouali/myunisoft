export const windev_conf = {
  protocol: 'https',
  baseUrl: 'ws-prod.myunisoft.fr',
  name: 'Windev',
  apiVersion: 1
};

export const service_auth_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1339',
  name: 'Auth',
  apiVersion: 1
};

export const service_discussion_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1347',
  name: 'Discussions',
  apiVersion: 1
};

export const service_notification_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1342'
};

export const mock_conf = {
  protocol: 'https',
  baseUrl: 'd0a75735-21d6-4f12-b59e-b8d416ce591c.mock.pstmn.io/'
};

export const service_ged_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1352',
  name: 'Ged',
  apiVersion: 1
};

export const redirect_url_budget = {
  protocol: 'https',
  baseUrl: 'app.myunisoft.fr'
};

export const download_dynamic = {
  protocol: 'https',
  baseUrl: 'www.myunisoft.fr',
  path: 'outils/liens-dynamiques/Liens-Dynamiques.xlsm'
};

export const mode = 'prod';
