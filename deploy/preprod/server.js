export const windev_conf = {
  protocol: 'https',
  baseUrl: 'ws-preprod.myunisoft.fr',
  name: 'Windev',
  apiVersion: 1
};

export const service_auth_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1338',
  name: 'Auth',
  apiVersion: 1
};

export const service_notification_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1341'
};

export const service_discussion_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1346',
  name: 'Discussions',
  apiVersion: 1
};

export const service_ged_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1351',
  name: 'Ged',
  apiVersion: 1
};

export const mock_conf = {
  protocol: 'https',
  baseUrl: 'd0a75735-21d6-4f12-b59e-b8d416ce591c.mock.pstmn.io/'
};

export const redirect_url_budget = {
  protocol: 'https',
  baseUrl: 'palpatine.myunisoft.fr',
  port: '5001'
};

export const download_dynamic = {
  protocol: 'https',
  baseUrl: 'www.myunisoft.fr',
  path: 'outils/liens-dynamiques/Liens-Dynamiques.xlsm'
};

export const mode = 'preprod';
