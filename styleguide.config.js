const path = require('path');
const merge = require('webpack-merge');
const webPack = require('./node_modules/react-scripts/config/webpack.config.js'); // TODO: Fix react styleguidist doesnt support arrow func in class

module.exports = {
  ignore: ['src/components/groups/Dialogs/index.js',
    'src/components/basics/Cells/index.js',
    'src/components/groups/Drawers/menus.js',
    'src/components/reduxForm/Inputs/index.js',
    'src/components/reduxForm/Selections/index.js',
    'src/components/basics/Cells/HistoryCell/index.js'],
  webpackConfig: merge(webPack, { // eslint-disable-line
    resolve: {
      alias: {
        'redux-form': path.join(
          __dirname,
          'src/helpers/styleguidist'
        )
      }
    }
  })
};
