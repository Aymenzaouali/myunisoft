export default {
  common: {
    submit: 'Valider',
    from: 'Du',
    to: 'au',
    add: 'Ajouter',
    cancel: 'Annuler',
    close: 'Fermer',
    create: 'Créer',
    send: 'Envoyer',
    save: 'Enregistrer',
    validate: 'Valider',
    history: 'Historique',
    deleteOk: 'Supprimer ce paramètre',
    dest: 'Destinataire',
    page: 'Page',
    errors: {
      empty: 'Ce champ est obligatoire',
      invalidDate: 'Date invalide (XX/XX/XXXX)',
      invalid: 'Valeur invalide',
      negative: "Il n'est pas possible de saisir un montant négatif",
      isNaN: 'Merci de saisir un montant'
    },
    status: {
      ok: 'OK'
    }
  },
  societiesRedirect: {
    selectSociety: 'Veuillez renseigner une société'
  },
  dashboard: {
    society: {
      rightClick: {
        modifyvat: 'Modifier la TVA',
        modifyis: 'Modifier l\'IS',
        modifycvae: 'Modifier la CVAE',
        dwnldPDF: 'Visualiser le PDF',
        summary: 'Récapitulatif JeDeclare'
      }
    },
    charts: {
      absoluteData: 'En valeur absolue',
      relativeData_turnover: 'En pourcentage du Chiffre d\'affaires',
      empty: 'Aucune donnée',

      global_situation: 'Situation globale',
      client: 'Client',
      supplier: 'Fournisseur',
      treasury: 'Trésorerie'
    },
    snackBarWarning: {
      tva: 'Saisir l\'échéance de la TVA'
    }
  },
  notifications: {
    warning: 'Attention',
    canLoseChanges: 'Voulez-vous vraiment supprimer vos modifications?',
    modificationsNotSaved: 'Des modifications n\'ont pas été enregistrées',
    yes: 'Oui',
    no: 'Non',
    continue: 'Continuer',
    return: 'Retour'
  },
  newPassword: {
    old: 'Ancien mot de passe',
    new: 'Nouveau mot de passe',
    confirme: 'Confirmation',
    close: 'Annuler',
    valide: 'Valider',
    title: 'Changement de mot de passe'
  },
  date: {
    sameDay: "Aujourd'hui",
    nextDay: 'Demain',
    nextWeek: 'La semaine prochaine',
    lastDay: 'Hier'
  },
  dialogAccountParam: {
    cancel: 'Fermer',
    validate: 'Valider',
    newParameter: {
      title: 'Nouveau Paramètres',
      numAccount: 'Compte MyUnisoft',
      accountBase: 'Compte de base',
      selectParameter: 'Selection du parameters',
      back: 'Retour'
    },
    selectParameter: {
      title: 'Paramètrages',
      accountingBase: 'Compte de base',
      accounting: 'MyUnisoft',
      new: '+ ajouter un compte',
      modify: 'Modifier'
    }
  },
  calendar: {
    clear: 'Vider',
    cancel: 'Annuler'
  },
  unreachedBills: {
    title: 'Factures non parvenues',
    comment: 'Commentaire',
    newEntry: 'Nouvelle saisie',
    validate: 'Valider'
  },
  commentList: {
    openAll: 'Tout ouvrir',
    closeAll: 'Tout fermer'
  },
  dadp: {
    dadp: 'DA',
    da: 'DA',
    regularity: 'Régularité',
    treasury: 'Trésorerie',
    purchaseSupplier: 'Achats + Fournisseurs',
    externalCharges: 'Charges externes',
    salesCustomer: 'Ventes + clients',
    stock: 'Stocks',
    immos: 'Immos',
    state: 'Etat',
    personal: 'Personnel',
    capitalProvision: 'Capitaux + provisions',
    otherAccount: 'Autres comptes',
    taxReturn: 'Déclarations fiscales',
    anexVarious: 'Annexe + divers',
    summaryNote: 'Note de synthèse',
    letter: 'Lettre d\'affirmation',
    bilan: 'Bilan imagé',
    brochure: 'Plaquette',
    workProgram: 'Programme de travail',
    analyticalReview: 'Revue analytique',
    internalRevision: 'Révision interne',
    summary: 'Synthèse',
    cycle_summary: 'Synthèse du cycle',
    section: 'Section',
    cancel: 'Annuler',
    create: 'Valider',
    from: 'Du',
    to: 'Au',
    date: 'Date',
    wording: 'Libellé',
    permanentData: 'Données permanentes',
    review: {
      from: 'Du',
      to: 'Au',
      type: 'Périodicité',

      types: {
        BIL: 'Bilan',
        SITU: 'Situation'
      }
    },
    status: {
      ok: 'Validé',
      ko: 'Non validé',
      running: 'En cours',
      to_do: 'A faire',
      to_validate: 'A valider'
    },
    groupValidationWarning: 'Toutes les diligences du groupe n\'ont pas le même statut'
  },
  brochure: {
    title: 'Plaquette',
    review_mode: 'Choix du mode de révision',
    brochure_mode: 'Choix du modèle',
    generate: 'Générer'
  },
  plaquetteTree: {
    title: 'Inclure dans la plaquette : ',
    cancel: 'Annuler',
    validate: 'Valider',
    includeAll: 'Tout inclure'
  },
  cca: {
    accountNumber: 'N° de compte CCA',
    title: 'Charges constatées d\'avance'
  },
  pca: {
    accountNumber: 'N° de compte PCA',
    title: 'Produits constatés d\'avance'
  },
  aae: {
    accountNumber: 'N° de compte AAE',
    title: 'Avoirs A Emettre'
  },
  fae: {
    accountNumber: 'N° de compte FAE',
    title: 'Factures A Emettre'
  },
  aar: {
    accountNumber: 'N° de compte AAR',
    title: 'Avoirs A Recevoir'
  },
  fixedAssets: {
    addALine: 'Ajouter une ligne',
    accounting: 'Comptabilité',
    immoWorksheet: "Fichier d'immobilisation",
    all: 'Tous',
    immo: 'Ammortissements',
    filledAccounts: 'Comptes peuplés',
    from: 'Du :',
    gap: 'Écart',
    sale: 'Vente',
    selected: '{{count}} sélectionnée',
    selectedPlural: '{{count}} sélectionnées',
    show: 'Afficher',
    submit: 'Générer les écritures',
    title: 'Immobilisations et amortissements',
    to: 'Au :',
    salesTitle: 'Sélectionnez une écriture de cession ',
    pjs: "PJ's",
    errors: {
      lessValue: 'Merci de saisir un montant inférieur au montant initial',
      earlyStartDate: 'Attention cet exercice est clôturé'
    },
    table: {
      linear: 'Linéaire',
      degressive: 'Dégressif',
      accountNumber: 'N° Cpte',
      attachment: 'PJ',
      balance: 'Solde',
      comment: 'Commentaire',
      commentShort: 'Com',
      commissioning: 'Mise en service',
      immoPeriodEnding: 'Amort fin période',
      immoType: 'Type amort',
      endowmentGap: 'Écart dotation',
      endowmentPeriod: 'Dotation période',
      endowmentPeriodGap: 'Écart dot période',
      endowmentPeriodShort: 'Dot période',
      endowmentYear: 'Dot année',
      entryLink: 'Lien écriture',
      exercise: 'Exercice',
      inAccounting: 'En compta',
      label: 'Intitulé',
      immoPeriodEndingShort: 'Amort fin pér',
      lastImmoGap: 'Écart amort fin pér',
      monthNumber: 'Nb. mois',
      plusValue: '+ value',
      previousImmo: 'Amort ant',
      previousImmoGap: 'Écart amort ant',
      purchase: 'Achat',
      purchaseDate: 'Date achat',
      purchaseGap: 'Écart achat',
      purchases: 'Achats',
      releaseDate: 'Date de sortie',
      sellingPrice: 'Prix vente',
      supplier: 'Fournisseur',
      value: 'Valeur',
      vnc: 'VNC',
      diary: 'Journal',
      piece: 'Piece 1',
      piece2: 'Pièce 2',
      number: 'N° Compte',
      debit: 'Débit',
      credit: 'Crédit'
    },
    modal: {
      headerTitle: 'Scinder une immobilisation',
      validText: 'Le montant doit être inférieur au montant initial',
      negativeValueText: 'Il n\'est pas possible de saisir un montant négatif',
      cancel: 'Annuler',
      submit: 'Valider',
      form: {
        amount: 'Montant',
        label: 'Libellé'
      }
    },
    dialogs: {
      generatedModify: {
        message: 'Une fois vos changements validés, vous devrez générer à nouveau l\'écriture comptable associée'
      },
      earlyStartDateModal: { title: 'Attention cet exercice est clôturé. Voulez vous vraiment créer cette immobilisation à cette date' }
    },
    saleModal: {
      buttonText: 'Vente',
      title: 'Sélectionnez une écriture de cession',
      table: {
        number: 'N° Compte',
        date: 'Date',
        provider: 'Fournisseur',
        deb: 'Débit',
        cred: 'Crédit'
      },
      submit: 'Valider',
      cancel: 'Anuler'
    }
  },
  deleteDialog: {
    mainMessage: 'Voulez vous supprimer les élements sélectionnés ?',
    secondMessage: 'Cette action est irréversible',
    ok: 'Supprimer cet état',
    warning: 'Attention !',
    cancel: 'Annuler',
    group: 'Supprimer un groupe'
  },
  fnp: {
    title: 'Factures non parvenues',
    accountNumber: 'N° de compte FNP',
    noAccountNumber: 'Pas de journal disponible',
    comment: 'Commentaire',
    newEntry: 'Nouvelle saisie',
    validate: 'Valider',
    selected: '{{count}} sélectionée',
    selected_plural: '{{count}} sélectionnées',
    vat_rate: 'Taux: {{count}}%',
    total: 'TOTAL',
    gap: 'Ecart',
    balance: 'Solde',
    tableButton: 'Générer l\'écriture comptable',
    dialogs: {
      title: 'Attention !',
      removeModalText1: 'Voulez vous supprimer les élements sélectionnés ?',
      removeModalText2: 'Cette action est irréversible',
      removeSelection: 'Supprimer la sélection',
      cancel: 'Annuler',
      ignore: 'Ignorer',
      disable: 'Désactiver',
      editModalText1: 'Un filtre est actif sur le tableau et la ligne que vous venez de saisir risque de ne pas apparaître.',
      editModalText2: 'Voulez-vous désactiver le filtre ?'
    }
  },
  cap_personal: {
    title: 'CAP Personnel',
    comment: 'Commentaire',
    newEntry: 'Nouvelle saisie',
    tableButton: 'Générer l\'écriture comptable',
    accountNumber: 'N° de compte CAP Personnel'
  },
  cap_int: {
    title: 'CAP Intérêts non courus',
    comment: 'Commentaire',
    newEntry: 'Nouvelle saisie',
    tableButton: 'Générer l\'écriture comptable',
    accountNumber: 'N° de compte CAP INT',
    total: 'TOTAL',
    gap: 'Ecart',
    balance: 'Solde'
  },
  cap_cs: {
    title: 'CAP Charges Sociales',
    comment: 'Commentaire',
    newEntry: 'Nouvelle saisie',
    tableButton: 'Générer l\'écriture comptable',
    accountNumber: 'N° de compte CAP CS',
    total: 'TOTAL',
    gap: 'Ecart',
    balance: 'Solde'
  },
  cap_div: {
    title: 'CAP Divers',
    comment: 'Commentaire',
    newEntry: 'Nouvelle saisie',
    tableButton: 'Générer l\'écriture comptable',
    accountNumber: 'N° de compte CAP Divers',
    total: 'TOTAL',
    gap: 'Ecart',
    balance: 'Solde'
  },
  cap_it: {
    title: 'CAP Impôts et Taxes',
    comment: 'Commentaire',
    newEntry: 'Nouvelle saisie',
    tableButton: 'Générer l\'écriture comptable',
    accountNumber: 'N° de compte CAP Impôts et Taxes',
    total: 'TOTAL',
    gap: 'Ecart',
    balance: 'Solde'
  },
  letters: {
    title1: 'Courriers',
    show: 'Afficher',
    title: 'Éditeur de courriers',
    newLetter: 'Nouveau Courrier',
    dialog: {
      newLetter: 'Nouveau Courrier',
      chooseHeader: 'Sélectionner une entête'
    },
    editor: {
      dest: 'NOM DU DESTINATAIRE',
      steDest: 'NOM STE DESTINATAIRE',
      addressDest: 'ADRESSE DESTINATAIRE',
      telMail: 'TEL OU MAIL',
      object: 'OBJET',
      contentLetter: 'CONTENUE DE LA LETTRE'
    },
    new: {
      type: 'Type de courriers',
      linkedTo: 'Rattaché à',
      paraph: 'Paragraphe'
    }
  },
  exercice: 'Exercice(s)',
  lettrage: 'Lettrage',
  selectAccount: 'Sélectionnez un compte',
  commentBox: {
    title: 'Commentaires',
    buttons: {
      submit: 'Envoyer',
      cancel: 'Annuler'
    },
    steps: {
      libre: 'Commentaire libre',
      normal: 'Commentaire normé'
    },
    applyToTitle: 'Appliquer à :',
    applyTo: {
      all: 'écriture',
      line: 'ligne'
    },
    attachToTitle: 'Attacher à :',
    attachToNotes: 'Note de synthèse',
    attachToInternalNotes: 'Révision Interne',
    attachToDiligence: 'Diligence',
    attachToAccount: 'Compte',
    attachToOther: 'Autres',
    attachToConfirmationLetter: 'Lettre d’affirmation',

    templateCommentLabel: 'Ajouter un paragraphe',
    template1: 'Template1',
    template2: 'Template2',
    template3: 'Template3',

    showHistory: 'Afficher l’historique',
    hideHistory: 'Masquer l’historique',
    historyCommentSuffix: 'à écrit'
  },
  buttons: {
    refresh: 'Rafraîchir'
  },
  tooltips: {
    download: 'Télécharger',
    tvasave: 'Sauvegarder le formulaire',
    cvaesave: 'Sauvegarder le formulaire',
    newTab: 'Nouvel onglet société',
    exchanges: 'Discussion',
    stopTime: 'Stopper le temps',
    shortcuts: 'Raccourcis',
    balance: 'Édition courante',
    newSeizure: 'Nouvelle saisie',
    listPerson: 'Liste personnes physiques, entreprises et utilisateurs',
    invert: 'intervertir',
    newWindow: 'Ouvrir dans une nouvelle fenêtre',
    splitScreen: 'Split Screen CTRL + 2',
    close: 'Fermer',
    chartOfAccounts: 'Plan comptable',
    refresh: 'Rafraîchir',
    infoAccount: 'Infos compte',
    importDoc: 'Importer un document',
    viewDoc: 'Visualiser les documents',
    deletter: 'Delettrer ALT + 1',
    letter: 'Lettrer ALT + 0',
    print: 'Imprimer CTRL + P',
    edit: 'Modifier',
    savedSearches: 'Recherches sauvegardées',
    Scinder: 'Scinder',
    join: 'Joindre',
    attach: 'Attacher',
    copy: 'Copier',
    detach: 'Détacher',
    archive: 'Archive',
    delete: 'Supprimer',
    sendingNewDoc: 'Envoi de nouveaux documents',
    add: 'Ajouter',
    accountParameters: 'Paramètres comptes tiers',
    support: 'Support client',
    statement: 'Règlement'
  },
  statement: {
    title: 'Gestion du paiement',
    validate: 'Valider',
    typeLabel: 'Type de règlement',
    amountLabel: 'Montant à payer',
    totalAmountLabel: 'Montant Total',
    fullPayment: 'Règlement complet',
    partPayment: 'Règlement partiel',
    errors: {
      noStatement: 'aucune écriture de vente disponible',
      different: 'Votre formulaire contient des erreurs et ne peut être validé, le total ne peut être différent de la somme du montant des IBAN',
      superior: 'Votre formulaire contient des erreurs et ne peut être validé, le total ne peut être supérieur à la somme du montant des IBAN'
    }
  },
  collector: {
    title: 'Collecteurs fournisseurs',
    subtitle: 'Factures antérieures récupérées via le collecteur',
    buttonAddProvider: 'Ajouter un fournisseur',
    buttonModifProvider: 'Modifier un fournisseur',
    buttonSelection: 'Saisir la sélection',
    success: 'Saisie réussie'
  },
  disconnect: 'Déconnexion',
  newPasseword: 'Nouveau mot de passe',
  vatForm: {
    empty: 'Ce champ est obligatoire',
    errorSizeCode: 'Ce champ doit contenir 1 ou 2 caractères'
  },
  Lettrage: {
    select: '{{count}} Sélectionné',
    select_plural: '{{count}} Sélectionnés',
    notLetter: 'Total sélection non lettrée : ',
    letter: 'Total sélection lettrée :',
    total: 'Total sélection :'
  },
  lettrageDialog: {
    title: 'Lettrer la sélection',
    label: 'Lettrage',
    delettrer: 'Voulez-vous délettrer :',
    delettrer2: 'Cette action est irréversible',
    cancelled: 'Annuler',
    validateD: 'Délettrer',
    validateL: 'Lettrer',
    errorLength: 'Le lettrage doit être composé de 3 lettres'
  },
  fec: {
    close: 'Fermer'
  },
  bankLink: {
    title: 'Rapprochement bancaire',
    selected: '{{count}} Sélectionné',
    selected_plural: '{{count}} Sélectionnés ',
    totalUnpointed: 'Total sélection à pointer : ',
    pointButton: 'Pointer',
    totalPointed: 'Total sélection à dépointer : ',
    unpointButton: 'Dépointer'
  },
  is: {
    downPayment: 'Acompte',
    closeOut: 'Liquidation',
    title: 'Declaration IS par EDI'
  },
  taxDeclaration: {
    title: 'Liasse fiscale'
  },
  tva: {
    title: 'Déclaration de TVA par EDI',
    tvaca3: 'TVA CA3',
    tva3519: 'TVA 3519',
    tva3310: 'TVA 3310',
    sum_up: 'Récap',
    detail_control: 'Détail contrôle',
    control: 'Contrôle',
    period: 'Période',
    send: 'Envoyer à l\'EDI',
    help: 'Notice',
    list: 'Liste TVA',
    new: 'Nouvelle TVA',
    modify: 'Modifier',
    delete: 'Supprimer',
    autofill: 'Préremplir',
    nothingness: 'Néant',
    switchNothingness: 'Si vous souhaitez remplir le formulaire, veuillez décocher case \u00ABNéant\u00bb',
    dialog: {
      title: 'Nouvelle TVA',
      modify: 'Modifier TVA',
      cancel: 'Annuler',
      save: 'Enregistrer'
    },
    form: {
      account_ded: 'N° Compte TVA deductible',
      account_coll: 'N° Compte TVA collectée',
      taux: 'Taux TVA',
      type: 'Type TVA',
      exigility: 'Exigibilité'
    }
  },
  portfolioListView: {
    title: 'Gestion des portefeuilles',
    headerBtns: {
      new: 'Nouveau portefeuille',
      modify: 'Modifier',
      delete: 'Supprimer'
    },
    society: 'société',
    societies: 'sociétés',
    user: 'utilisateur',
    users: 'utilisateurs',
    table: {
      entitled: 'Intitulé',
      companies: 'Sociétés',
      relatedUsers: 'Utilisateurs liés'
    },
    dialog: {
      titleNew: 'Nouveau portefeuille',
      titleModify: 'Modifier le portefeuille',
      portfolioName: 'Nom du portefeuille',
      portfolioNameError: 'Champ obligatoire!',
      companies: 'Sociétés',
      cancel: 'Annuler',
      ok: 'Valider'
    }
  },
  decloyer: {
    title: 'Déclaration des loyers professionnels',
    declarationYear: 'Année de déclaration',
    local: {
      title: 'Local',
      refLocal: 'Référence du local',
      invarLocal: 'Invariant du local'
    },
    owner: {
      title: 'Propriétaire',
      actual: 'Actuel',
      new: {
        title: 'Nouveau',
        legalForm: 'Forme juridique / Titre',
        name: 'Nom',
        firsName: 'Prénom',
        siren: 'SIREN'
      }
    },
    addressDetail: {
      title: 'Précisions sur l’adresse du local'
    },
    permanentUpdate: {
      title: 'Mise à jour permanente',
      occupationPermises: 'Mode de l\'occupation du local',
      rentAmount: 'Montant du loyer actuel',
      endLeaseDate: 'Date de fin d’occupation du local'
    },
    buttons: {
      validate: 'Valider'
    }
  },
  ged: {
    title: 'Gestion des documents',
    society: 'Société',
    selectDiscussion: 'Sélectionnez la discussion',
    filter: {
      from: 'Du :',
      to: 'Au :',
      period: 'Période',
      exercise: 'Exercice(s)',
      all: 'Toutes',
      search: 'Intitulé, nom, Type',
      amountBetween: 'Montant compris entre :',
      amountStep: 'et',
      amount: 'Montant',
      amountRange: '+ ou -',
      linkedTo: 'Lié à',
      addedBy: 'Ajouté par',
      status: 'Statut',
      format: 'Format',
      searchBtn: 'Rechercher',
      resetBtn: 'Réinitialiser',
      emptyError: 'Ce nom existe déjà !',
      tabs: {
        general: 'Général',
        inProgress: 'En cours',
        accounts: 'Comptes',
        scriptures: 'Écritures',
        da_dp: 'DA/DP',
        discussion: 'Discussions',
        documentDatabase: 'Base documentaire'
      }
    },
    tree: {
      accounts: 'Une société'
    },
    table: {
      view: 'Affichage',
      name: 'Nom',
      label: 'Intitulé',
      company_name: 'Intitulé',
      entitled: 'Intitulé',
      date: 'Date',
      added_by: 'Ajouté par',
      worksheet: 'Feuille de travail',
      status: 'Statut',
      linked_to: 'Lié à',
      amount_ttc: 'Montant TTC',
      amount_ht: 'Montant HT',
      amount_tva: 'Montant TVA',
      due_date: 'Date d\'échéance',
      date_echeance: 'Date d\'échéance',
      comments: 'Com.',
      letter: 'Lettré',
      letterExist: 'Oui',
      letterNotExist: 'Non',
      titleFT: 'Intitulé FT',
      accountNumber: 'N° de compte',
      title: 'Titre',
      bodyMessage: 'Corps du message',
      tags: 'Tags',
      body_message: 'Corps du message',
      statusInfo: 'En cours',
      menu: {
        copy: 'Copier',
        copyLink: 'Copier le lien du document',
        sendByEmail: 'Envoyer par mail',
        sendInDiscussion: 'Envoyer dans une discussion',
        deatach: 'Détacher'
      }
    }
  },
  settingsWorksheets: {
    title: 'Paramétrages comptes feuilles de travail',
    buttons: {
      newWorksheet: 'Nouvelle feuille de travail',
      edit: 'Modifier',
      remove: 'Supprimer'
    },
    table: {
      type: 'Type',
      accountNumber: 'N° de compte',
      accountNumberTVA: 'N° de compte TVA'
    },
    popUp: {
      createNew: 'Nouveau compte feuille de travail',
      modify: 'Modification compte feuille de travail',
      attention: 'Attention ! ',
      massageImpossible: 'Cette feuille de travail compte des écriture\n'
        + 'et ne peut-être supprimée ',
      massagePossible: 'Voulez-vous supprimer ce paramétrage ? \n'
        + 'Cette action est irréversible',
      ok: 'OK',
      cancel: 'Annuler',
      deleteBtn: 'Supprimer ce paramètre'
    }
  },
  errors: {
    ArgumentError: {
      diary_id: 'Oups, il semblerait que la valeur indiquée pour le journal pose problème',
      no_pj: 'Aucune pj n\'a été selectionnée',
      society_without_permission: 'Vous devez indiquer un niveau d\'accès aux sociétés séléctionées',
      invalid_rib_key: 'La clé rib ne correspond pas à l\'IBAN'
    },
    MismatchError: {
      debit_credit: 'Oups, il semblerait que le débit est different du crédit !',
      bad_email_password: "L'email ou le mot de passe semble avoir été mal saisi",
      bad_password: 'Le mot de passe semble avoir été mal saisi',
      date_already_used: 'Il semblerait que les dates se chevauchent !'
    },
    AlreadyExistError: {
      mail: 'Oups, il semblerait que le mail existe déjà !',
      siret: 'Oups, il semblerait que le SIRET existe déjà !',
      entry_already_exist: 'Oups, il existe déjà des écritures sur l\'exercice'
    },
    FileExtension: {
      extension: 'Oups, l\'extension du fichier n\'est pas autorisée'
    },
    MissingArgumentError: {
      fiscal_year_id: 'Oups, la valeur de identifiant de l\'exercice est non cohérente',
      society_id: 'Oups, la valeur de identifiant de la société est non cohérente'
    },
    NotFoundId: {
      fiscal_year_not_found: 'Oups, impossible de se positionner sur l\'exercice fiscal sélectionné',
      no_data_returning: 'Oups, erreur serveur'
    },
    ServerError: {
      avoid_array: 'Oups, erreur dans le séparateur',
      error_query: 'Oups, erreur serveur'
    },
    DateInvalid: {
      entry_date: 'Oups, erreur dans le format de la date d\'ecriture',
      piece_date: 'Oups, erreur dans le format de la date de la piece',
      piece_date_outside_fiscal_year: 'Oups, date de la piece en dehors de l\'exercice'
    },
    NotFoundError: {
      'invalid_exercice_dateN-1': 'Les dates de l\'exercice N-1 ne vont pas',
      'invalid_exercice_dateN+1': 'Les dates de l\'exercice N+1 ne vont pas',
      invalid_exercice_dateN: 'Les dates de l\'exercice N ne vont pas',
      'invalid_exercice_dateN-2': 'Les dates de l\'exercice N-2 ne vont pas',
      'invalid_exercice_dateN-3': 'Les dates de l\'exercice N-3 ne vont pas',
      'invalid_exercice_dateN-4': 'Les dates de l\'exercice N-4 ne vont pas'
    },
    default: 'Oups, il semblerait que quelque chose ne va pas!',
    invalid_email: 'Adresse mail invalide',
    loading_tabs: 'Impossible de charger les onglets',
    alreadyExistFunction: 'Cette fonction existe déjà !'
  },
  select: {
    placeholder: 'Aucun',
    load_error: 'Impossible de charger les options'
  },
  citations: {
    1: 'Que serait le monde sans MyUnisoft ?',
    2: 'La compta c\'est génial!',
    3: 'Trop de débit pas assez de crédit...',
    4: ' Comment appelle-t-on une balance provisoire qui ne tombe pas juste à 6h du soir ? ... Une nuit blanche',
    5: 'Pourquoi les comptables préfèrent-ils les grands espaces? ...  Parce qu’ils n’aiment pas les clôtures. ',
    6: ' Où un spécialiste de l’ISOC fait-il dormir son chien ? ...  Dans une niche fiscale. ',
    7: 'Pourquoi les comptables sont-ils toujours calmes, posés et méthodiques ? ... Ils font preuve d’un grand contrôle interne. ',
    8: 'Si la femme d’un comptable ne peut pas dormir, que demande-t-elle à son mari ? ...  de lui raconter sa journée.',
    9: 'Il y a trois sortes de comptables dans le monde : Ceux qui savent compter et ceux qui ne le savent pas.',
    10: 'Quel est le comble pour un expert-comptable stagiaire ?... De ne pas avoir de mémoire.',
    11: 'En cas de famine, il est interdit de manger les provisions'
  },
  calculator: {
    validate: 'Valider'
  },
  consulting: {
    OD_dialog: {
      title: 'OD de Lettrage',
      cancel: 'Annuler',
      validate: 'Valider',
      amount: 'Montant',
      total_amount: 'Total',
      account: 'Compte',
      date: 'Date',
      diary: 'Journal'
    },
    comment: {
      selectComment: 'Commenter le :',
      checking: 'Validation',
      accountCommentType: 'Commentaire du compte',
      daCommentType: "Commentaire d'un DA",
      noComment: 'Pas de commentaire sur ce compte'
    },
    title: 'Consultation compte ',
    table: {
      error: 'Une erreur est survenue lors du chargement du tableau',
      empty: 'Aucun resultat trouvé',
      diary_code: 'test'
    },
    filter: {
      from: 'Du',
      to: 'Au',
      lettrageOn: 'Uniquement lettrées',
      lettrageOff: 'Uniquement non lettrées',
      lettrage: 'Toutes les lignes'
    },
    changeAccount: 'Changer le numero de compte',
    diary_code: 'Journal',
    Comm: 'Comm',
    pj: 'PJ',
    axe1: 'Axe1',
    axe2: 'Axe2',
    axe3: 'Axe3',
    type: 'Type',
    deadline: 'Echéance',
    payment_type_id: 'Type de règlement',
    date: 'Date',
    piece: 'Piece 1',
    piece2: 'Pièce 2',
    label: 'Intitulé',
    debit: 'Débit',
    credit: 'Crédit',
    solde: 'Solde',
    lettrage: 'Lett',
    changeDiary: 'Changer le journal',
    updateEntry: "Modifier l'entrée",
    warning: {
      notBalanced: "Impossible de lettrer car le lettrage n'est pas équilibré",
      notSelected: "Impossible de lettrer car aucune ligne n'est sélectionnée",
      alreadyLettered: 'Impossible de lettrer car des lignes déjà lettrées ont été sélectionnées'
    },
    slave: {
      accounting: {
        empty: 'Cliquez sur un numero de compte pour en afficher la consultation',
        not_ecriture: 'La consultation en split n’est disponible que sur les écritures validées (cocher \'Ecritures\' dans le tableau du haut.)'
      },
      new_accounting: {
        empty: 'Entrez un numéro de compte dans une ligne pour en voir le detail'
      },
      current_editions: {
        empty: 'Sélectionnez un compte dans la balance pour en afficher les détails'
      }
    }
  },
  navBar: {
    search: 'Rechercher',
    dialogTitle: 'Choix de l\'entreprise'
  },
  login: {
    title: 'Pour commencer, connectez-vous',
    mail: 'Adresse email',
    password: 'Mot de passe',
    connect: 'Se connecter',
    forgottenPassword: 'Mot de passe oublié ?',
    cgu: {
      pre: 'J’ai lu et j’accepte les',
      link: 'conditions générales d’utilisation'
    },
    allowData: 'J’accepte que l’on utilise mes données pour une utilisation comptable',
    chooseGroup: 'Choix du cabinet',
    validate: 'Valider',
    errorMessages: {
      cguConfirm: 'Merci d’accepter les CGU et l’utilisation des données avant de vous connecter'
    }
  },
  passwordRecovery: {
    title: 'Mot de passe oublié ?',
    cancel: 'Annuler',
    mail: 'Adresse email',
    chooseGroup: 'Choix du cabinet',
    helper: 'Un lien vous permettant de de réinitialiser votre mot de passe va vous être envoyé à cette adresse',
    validate: 'Valider',
    emailCheckerResponse: {
      success: 'Un mail va vous parvenir avec un mot de passe temporaire que vous devrez modifier à la connexion',
      failed: 'Login non reconnu'
    }
  },
  passwordReset: {
    title: 'Réinitialiser votre mot de passe',
    newPassword: 'nouveau mot de passe',
    confirmedPassword: 'confirmer le mot de passe',
    validate: 'Valider',
    passwordReseted: {
      success: 'Password successfully reseted',
      failed: 'Password can not reseted'
    }
  },
  managementCard: {
    title: 'Gestion de cabinet',
    subtitle: 'Collaborateurs et temps',
    footer: {
      bills: 'factures',
      reminders: 'relances'
    },
    avatarFunction: {
      ExpertComptable: 'M20 12l-1.41-1.41L13 16.17V4h-2v12.17l-5.58-5.59L4 12l8 8 8-8z',
      Collaborateur: 'M20 12l-1.41-1.41L13 16.17V4h-2v12.17l-5.58-5.59L4 12l8 8 8-8z',
      Comptable: 'M20 12l-1.41-1.41L13 16.17V4h-2v12.17l-5.58-5.59L4 12l8 8 8-8z',
      Superviseur: 'M20 12l-1.41-1.41L13 16.17V4h-2v12.17l-5.58-5.59L4 12l8 8 8-8z'
    }
  },
  taskCard: {
    title: 'Tâches'
  },
  pagination: {
    printPerPage: 'Afficher par page',
    results: '<0>{{count}}</0><1>résultat sur</1>',
    results_plural: '<0>{{count}}</0><1>résultats sur</1>',
    pages: '<0>{{count}} page</0>',
    pages_plural: '<0>{{count}} pages</0>'
  },
  memoCard: {
    noMemo: 'Pas de mémos pour le moment !',
    title: 'Mémos du dossier',
    subtitle_firstPart: 'mémos sur',
    subtitle_secondPart: 'sont déjà traités :',
    dialogMemoCreation: {
      title: 'Créer un mémo'
    }
  },
  overview: {
    title: 'Aperçu'
  },
  accounting: {
    dialog: {
      information_message: 'Impossible de modifier une écriture d\'A nouveaux'
    },
    noExercice: "Impossible d'effectuer une saisie. Veuillez renseigner des exercices.",
    types: {
      writing: 'Ecriture',
      flux: 'Flux manuel (M)',
      ocr: 'OCR en attente (O)',
      banking: 'Intégration bancaire en attente (IB)',
      pay: 'OD de paie (P)',
      pending: "Ecritures d'extournes en attente"
    },
    filter: {
      from: 'Du',
      to: 'Au',
      diary: 'Journal',
      account_number: 'N° Compte',
      debit: 'Débit',
      credit: 'Crédit',
      plus_less: '+ ou -',
      exercices: 'Exercice(s)',
      piece: 'Pièce',
      label: 'Intitulé',
      writeBy: 'Saisi par',
      searchField: 'N° facture, mois, compte…',
      filter: 'Filtres',
      importNum: 'I/N',
      listAccount: 'LC',
      accountSheets: 'FC',
      societyPlaceholder: 'Selectionner une ou plusieurs société(s)',
      society: 'Société',
      reset: 'Réinitialiser',
      odLoad: 'Charger',
      odPay: 'Paramètres compte'
    },
    accounting: 'Saisie',
    handleDocument: 'Gestion documents',
    slave: {
      consulting: {
        empty: 'Cliquez sur une saisie dans consultation pour en afficher les détails'
      }
    }
  },
  flagForm: {
    validation: {
      dateStart: 'Veuillez renseigner une date',
      dateEnd: 'Veuillez renseigner une date',
      vat_account: 'Veuillez choisir un compte',
      ttc_account: 'Veuillez choisir un compte',
      dossier_revision: 'Veuillez choisir un dossier',
      duration: 'Veuillez entrer une durée positive',
      missing_duration: 'Veuillez entrer une durée'
    }
  },
  newAccounting: {
    flagType: {
      aae: 'AAE',
      aar: 'AAR',
      cap: 'CAP',
      cca: 'CCA',
      fae: 'FAE',
      fnp: 'FNP',
      pca: 'PCA'
    },
    informationDialog: {
      title: 'Autocomplétion impossible',
      message: 'Plusieurs lignes d\'écritures ont été saisies, il est donc impossible d\'activer l\'autocomplétion'
    },
    autoReverseDialog: {
      title: 'Une écriture d\'extourne a déjà été générée',
      message: 'Voulez-vous également qu\'elle soit modifiée ?',
      confirm: 'Valider',
      cancel: 'Anuller'
    },
    flagDialog: {
      title: 'Flaguer comme {{flagType}}',
      cancel: 'Annuler',
      flag: 'Flaguer',
      amortissementOptions: {
        linear: 'Linéaire',
        degressive: 'Dégressif'
      },
      field: {
        vat_account: 'Sélectionner un compte TVA',
        ttc_account: 'Sélectionner un compte TTC',
        dossier: 'Sélectionner un bilan ou une situation',
        date: 'Date de mise en service',
        amortissement: 'Type amortissement',
        duration: 'Durée (en mois)'
      }
    },
    snackBarWarning: {
      popupAccount: 'Pas de compte sélectionné',
      accountIncompatibility: 'Création impossible avec le journal sélectionné'
    },
    newAccounting: 'Nouvelle Saisie',
    pj: 'Pièces jointes: ',
    validate: 'Valider',
    save: 'Sauvegarder',
    cancel: 'Annuler',
    modify: 'Modification écriture {{entry}}',
    duplicate: "Copie d'écriture",
    new: 'Nouvelle écriture',
    e: 'Ecriture',
    o: 'OCR en attente',
    ib: 'Intégration bancaire',
    p: 'OD de paie',
    ext: 'Ecritures d\'extournes en attente',
    linkDiaryAccountDialog: {
      title: 'Autocomplétion impossible',
      message: 'Pour réaliser l’autocomplétion votre journal de banque doit être configuré avec un compte de banque. Voulez-vous configurer votre journal de banque ?',
      accept: 'Oui',
      deny: 'Non'
    },
    noCounterPartDialog: {
      title: 'Autocomplétion impossible',
      message: 'Pour réaliser l\'autocomplétion de l\'écriture, votre compte {{account}} doit être configuré avec un compte de contrepartie et un code TVA le cas échéant.'
      + 'Voulez vous configurer le compte {{account}} ?',
      accept: 'Oui',
      deny: 'Non'
    }
  },
  ChartAccount: {
    title: 'Plan Comptable',
    filterLabel: 'Filtrer par classe',
    button: {
      newAccount: 'Nouveau Compte',
      changeAccount: 'Modifier',
      deleteAccount: 'Supprimer'
    },
    array: {
      column: {
        checkbox: '',
        accountNumber: 'N° Compte',
        label: 'Intitulé',
        solde: 'Solde',
        sens: 'Sens',
        counterPart: 'Contrepartie',
        vat: 'TVA',
        intraco: 'Intracto',
        presta: 'Presta',
        das2: 'DAS2',
        analyticSection: 'Section analytique',
        analyticImputation: 'Imputation analytique'
      }
    }
  },
  accountingPlans: {
    title: 'Paramétrage plans comptables étalons',
    button: {
      newPlan: 'Nouveau',
      changePlan: 'Modifier',
      deletePlan: 'Supprimer'
    },
    dialogs: {
      plan: {
        title: 'Nouvelle étalon',
        modify: 'Modifier étalon',
        cancel: 'Annuler',
        save: 'Enregistrer',
        empty: 'Ce champ est obligatoire'
      },
      planDetail: {
        title: 'Nouvelle étalon détail',
        modify: 'Modifier étalon détail',
        cancel: 'Annuler',
        save: 'Enregistrer',
        empty: 'Ce champ est obligatoire'
      },
      delete: {
        message: 'Etes vous sûr de vouloir supprimer ?'
      }
    },
    forms: {
      plan: {
        societyList: 'A partir du plan comptable de la société',
        label: 'Intitulé',
        description: 'Description'
      },
      planDetail: {
        account_number: 'N° Compte',
        label: 'Intitulé'
      }
    },
    tables: {
      plans: {
        column: {
          label: 'Intitulé',
          description: 'Description'
        },
        messages: {
          empty: 'Aucun plan fourni'
        }
      },
      planDetails: {
        title: 'Détail du plan comptable',
        column: {
          account_number: 'N° Compte',
          label: 'Intitulé'
        },
        messages: {
          empty: 'Aucun plan fourni'
        }
      }
    },
    slave: {
      planDetails: {
        empty: 'Cliquez sur un numero de plan compte pour en afficher la détail du plan comptable'
      }
    }
  },
  tables: {
    addNewRow: 'Ajouter une ligne',
    letters: {
      label: 'Intitulé',
      createdAt: 'Crée le',
      type: 'Type de lettre',
      createdBy: 'Crée par',
      modifedBy: 'Modifé par',
      status: 'Statut',
      sendDate: 'Date d\'envoi'
    },
    analyticReview: {
      balanceSheet: 'Comptes bilans',
      accountsResults: 'Comptes résultats',
      account_number: 'N° Cpte',
      label: 'Intitulé',
      soldeN: 'Solde N',
      'soldeN-1': 'Solde N-1',
      evo: '%evo',
      va: '%VA',
      valid_collab: 'Valid collab',
      valid_rm: 'Valid RM',
      nb_docs: 'NB Docs',
      comments: 'Commentaires'
    },
    workProgram: {
      all: 'Tout afficher',
      worksheet: 'Feuille de travail',
      dilligence: 'Diligences',
      guides: 'Guides',
      pj: 'PJ',
      comment: 'Commentaire',
      na: 'NA',
      valid_collab: 'Valid collab',
      valid_rm: 'Valid RM',
      ref: 'Référence'
    },
    vat: {
      checkbox: '',
      code: 'Code',
      vat_ded_number: 'N° TVA ded',
      vat_coll_number: 'N° compte TVA coll',
      vat_taux: 'Taux TVA',
      vat_type: 'Type TVA',
      vat_due: 'Exigibilité de la TVA'
    },
    collector: {
      checkbox: '',
      ProviderName: 'Fournisseur',
      BillDate: 'Date',
      BillName: 'Nom',
      BillAmount: 'Montant'
    },
    bankLink: {
      dotted: 'Pointé',
      date: 'Date',
      id_diary: 'Jal',
      line: 'Ligne',
      counterpart_number: 'N° contrepartie',
      piece: 'Pièce 1',
      piece_2: 'Pièce 2',
      label: 'Intitulé',
      debit: 'Débit',
      credit: 'Crédit',
      com: 'Com.',
      pj: 'PJ'
    },
    userList: {
      civility: 'Civilité',
      name: 'Nom',
      maiden_name: 'Nom de jeune fille',
      firstname: 'Prénom',
      mail: 'Email',
      tel: 'Numéro de téléphone',
      libelle_type_profil: 'Type général',
      libelle_profil: 'Profil général'
    },
    fnp: {
      number: 'N° Cpte',
      label: 'Intitulé',
      total_excl_taxes: 'HT',
      vat_code: 'Code TVA',
      vat_total: 'TVA',
      total_incl_taxes: 'TTC',
      addPj: '',
      pj: 'PJ',
      compta: 'En compta',
      comment: 'Com.',
      imputation: 'Imputation',
      section: 'Section'
    },
    CCA: {
      number: 'N° Cpte',
      label: 'Intitulé',
      amount: 'Montant',
      period: 'Période',
      day_total: 'Nb j total',
      advanced_day_total: 'Nb j avance',
      CCA: 'CCA',
      pj: 'PJ',
      addPj: '',
      compta: 'En compta',
      comment: 'Com.',
      imputation: 'Imputation',
      section: 'Section'
    },
    cap_personal: {
      number: 'N° Cpte',
      label: 'Intitulé',
      total_excl_taxes: 'HT',
      vat_code: 'Code TVA',
      vat_total: 'TVA',
      total_incl_taxes: 'TTC',
      pj: 'PJ',
      compta: 'En compta',
      comment: 'Com.'
    },
    diary: {
      diary_code: 'Code Journal',
      diary_label: 'Intitulé',
      diary_type: 'Type Journal',
      account_number: 'N° Compte'
    },
    userSort: {
      firstName: 'firstname',
      lastName: 'name',
      maidenName: 'maiden_name'
    },
    error: 'Une erreur est survenue !',
    noData: 'Aucune donnée !',
    newAccounting: {
      total: 'TOTAUX',
      error: {
        empty_label: 'Le label doit être renseigné',
        empty_date: 'La date doit être renseignée',
        empty_account_number: 'Le compte doit être renseigné',
        empty_debit: 'Un des deux montants doit être renseigné',
        empty_credit: 'Un des deux montants doit être renseigné'
      },
      popover: {
        delete_row: 'Supprimer la ligne',
        modify: 'Modifier le compte'
      },
      flag: 'Flag',
      oib: 'O/IB',
      status: 'Statut',
      type: 'Type',
      diary: 'Journal',
      date_piece: 'Jour',
      account_number: 'N° Compte',
      deadline: 'Echéance',
      label: 'Intitulé',
      piece_1: 'Pièce 1',
      piece_2: 'Pièce 2',
      debit: 'Débit',
      credit: 'Crédit',
      com: 'Com.',
      pj: 'Pj.',
      axe1: 'Axe1',
      axe2: 'Axe2.',
      axe3: 'Axe3',
      contrepartie: 'Contrepartie',
      payment_type: 'Type Règlement',
      date_accounting: 'Date de la saisie',
      period: 'Periode',
      checkbox: 'C',
      rightClick: {
        reset: 'Réinitialiser',
        validate: 'Valider',
        modify: 'Modifier',
        duplicate: 'Dupliquer',
        delete: 'Supprimer'
      },
      deleteConfirmationTitle: 'Suppression',
      deleteConfirmation: 'Voulez-vous vraiment supprimer l\'écriture',
      deleteConfirmation_plural: 'Voulez-vous vraiment supprimer les écritures',
      flagDelete: 'Impossible de supprimer une écriture avec un flag'
    },
    accounting: {
      info: 'Info',
      total: 'TOTAUX',
      error: {
        empty_label: 'Le label doit être renseigné',
        empty_date: 'La date doit être renseignée',
        empty_account_number: 'Le compte doit être renseigné',
        empty_debit: 'Un des deux montants doit être renseigné',
        empty_credit: 'Un des deux montants doit être renseigné'
      },
      popover: {
        delete_row: 'Supprimer la ligne',
        modify: 'Modifier le compte'
      },
      flag: 'Flag',
      oib: 'O/IB',
      status: 'Statut',
      type: 'Type',
      diary: 'Journal',
      date_piece: 'Date',
      account_number: 'N° Compte',
      deadline: 'Echéance',
      label: 'Intitulé',
      piece_1: 'Pièce 1',
      piece_2: 'Pièce 2',
      debit: 'Débit',
      credit: 'Crédit',
      com: 'Com.',
      pj: 'Pj.',
      axe1: 'Axe1',
      axe2: 'Axe2.',
      axe3: 'Axe3',
      contrepartie: 'Contrepartie',
      payment_type: 'Type Règlement',
      date_accounting: 'Date de la saisie',
      period: 'Periode',
      checkbox: 'C',
      rightClick: {
        reset: 'Réinitialiser',
        validate: 'Valider',
        modify: 'Modifier',
        duplicate: 'Dupliquer',
        delete: 'Supprimer'
      },
      deleteConfirmationTitle: 'Suppression',
      deleteConfirmation: 'Voulez-vous vraiment supprimer l\'écriture',
      letteredEntry_errorMessage: 'Impossible de supprimer une écriture qui possède une ligne lettrée.',
      deleteConfirmationTitle_lettered: 'Confirmation de suppression',
      deleteConfirmation_lettered: 'Cette écriture contient une ligne d\'écriture. La suppression de cette écriture entraînera le délettrage de l’ensemble des lignes d\'écritures liées. Êtes-vous sûr de vouloir la supprimer ?',
      deleteConfirmation_plural: 'Voulez-vous vraiment supprimer les écritures',
      flagDelete: 'Impossible de supprimer une écriture avec un flag'
    },
    companyList: {
      society_id: 'Identifiant',
      name: 'Nom entreprise',
      city: 'Ville',
      siret: 'SIRET',
      status: 'Statut',
      ape: 'APE',
      step: 'Etapes',
      companyType: 'Type société'
    },
    physicalPersonList: {
      gender: 'Civilité',
      name: 'Nom',
      firstname: 'Prénom',
      company: 'Entreprises',
      phoneNumber: 'Téléphone',
      phoneNumberCount: '',
      mail: 'Mail',
      mailCount: '',
      role: 'Fonction'
    }
  },
  validations: {
    groupFunction: {
      libelle: 'La fonction doit être renseignée'
    },
    societyAssociateValidation: {
      PP: 'Champ obligatoire',
      NP: 'Champ obligatoire',
      US: 'Champ obligatoire'
    },
    newPassword: {
      match: "Le mot de passe saisi n'est pas identique dans les deux champs",
      required: 'Champs obligatoire'
    },
    newAccounting: {
      label: 'Le label doit être renseigné',
      date: 'La date est incorrecte',
      date_futur: 'La date est supérieure à la date courante',
      debit: 'Un des deux montants doit être renseigné',
      credit: 'Un des deux montants doit être renseigné',
      account_number: 'Le numéro de compte doit être renseigné',
      diary: 'Le journal doit être renseigné',
      deadline: 'La date d\'échéance doit être supérieure ou égale à la date pièce',
      piece: 'La pièce ne doit pas dépasser 15 caractères',
      piece2: 'La pièce 2 ne doit pas dépasser 10 caractères',
      right: {
        profil: 'Le profil est obligatoire',
        type: 'Le type est obligatoire'
      }
    },
    newUser: {
      gender: 'La civilité doit être renseignée',
      last_name: 'Le nom doit être renseigné',
      first_name: 'Le prénom doit être renseigné',
      social_security_number: 'Veuillez saisir un numéro au format x xx xx xx xxx xxx xx',
      date_birth: 'Veuillez saisie une date au format AAAA-MM-DD',
      mail: 'Un mail doit être renseigné',
      mail_format: 'Veuillez saisir une adresse email valide',
      phone_number: 'Un numéro de téléphone doit être renseigné',
      phone_number_format: 'Veuillez renseigner un numéro de téléphone valide',
      piece: 'La pièce ne doit pas dépasser 4 caractères',
      piece1: 'La pièce 2 ne doit pas dépasser 10 caractères',
      right: {
        profil: 'Le profil est obligatoire',
        type: 'Le type est obligatoire'
      }
    },
    importEntry: {
      fec_source: 'Veuillez sélectionner une source'
    },

    infoGValidation: {
      account_required: 'Le numéro de compte est obligatoire',
      account_begin: 'le numero de compte doit commencé par un chiffre compris entre  1 et 7',
      account_entitled: 'L\'intitulé du compte est obligatoire',
      account_length: 'Le compte doit comporter 10 caractères maximum'
    }

  },
  filesDropperDialog: {
    dropbox: 'Faîtes glisser vos documents ici',
    import: '<0>importer</0> depuis vos fichiers',
    scan: '<0>scanner</0> (en 300 DPI)'
  },
  filesDroperDialog: {
    dropbox: 'Glisser-déposer vos fichiers ici',
    addFileButton: 'Importer',
    scanButton: 'Scanner',
    info: 'depuis votre explorateur ou ',
    info1: '(en 300 DPI)',
    confirmButton: 'Valider',
    cancelButton: 'Annuler'
  },
  customTable: {
    column_title: {
      folder: 'Dossier',
      vat_due_date: 'Echéance TVA',
      vat: 'TVA',
      bank_flow: 'Flux bancaire',
      ocr_flow: 'Flux OCR',
      lettering: 'Lettrage',
      outstand: 'Outstand'
    },
    table_title: {
      society_tracking: 'Suivi société'
    }
  },
  companiesMonitoring: {
    title: 'Suivi de vos sociétés',
    displayType: {
      history: 'Historique',
      week: 'Semaine',
      month: 'Mois',
      year: 'Année'
    },
    fields: {
      company_name: 'Dossier',
      endAt: 'Echéance TVA',
      vat: 'TVA',
      bank_flow: 'Flux bancaire',
      ocr_flow: 'Flux OCR',
      manual_flow: 'Flux manuel',
      lettering: 'Lettrage',
      amount: 'En-cours',
      closure: 'Clôture',
      firstIS: 'Premier accompte IS',
      is: 'IS',
      balance_sheet: 'Bilan',
      ago: 'AGO',
      cvae: 'CVAE',
      cfe: 'CFE',
      statement: 'Bilan',
      first_is_deposit: 'Premier accompte IS',
      month01: 'Janv.',
      month02: 'Févr.',
      month03: 'Mars',
      month04: 'Avr.',
      month05: 'Mai',
      month06: 'Juin',
      month07: 'Juill.',
      month08: 'Août',
      month09: 'Sept.',
      month10: 'Oct.',
      month11: 'Nov.',
      month12: 'Déc.'
    },
    historyFilters: {
      statement: 'Bilan',
      is: 'IS',
      cvae: 'CVAE',
      vat: 'TVA',
      ago: 'AGO'
    },
    status: {
      ok: 'OK',
      cancelled: 'Annulé',
      'in-progress': 'En cours'
    }
  },
  bankLinkFilter: {
    day: 'Jour',
    month: 'Mois',
    year: 'Année',
    bank: 'Code journal',
    dateText: 'Rapprochement bancaire au',
    accountNumber: 'N° de compte',
    countSold: 'Solde comptable',
    unpointedSold: 'Solde non pointé',
    bankSold: 'Solde relevé bancaire',
    gap: 'Ecart',
    validationButton: 'Valider',
    saisieButton: 'Saisir',
    importDoc: 'Importer des documents'
  },
  progressionTable: {
    title: 'Avancement',
    currentExercise: 'Exercice en cours',
    customCell: {
      inProgress: 'En cours',
      delayed: 'En retard',
      error: 'Erreur',
      ok: 'OK',
      failure: 'Erreur'
    },
    VATSteps: {
      eFilling: 'e-filling',
      clientSending: 'Client sending',
      customerFeedback: 'Customer Feedback',
      positiveFeedback: 'Positive Feedback'
    }
  },
  textFieldDialog: {
    validate: 'Valider',
    cancelled: 'Annuler'
  },
  drawer: {
    da_dp: 'DA',
    cb: 'Crédit-Bail',
    loan: 'Prêt',
    fixed_assets: 'Immobilisations',
    unreachedBills: 'Factures non parvenues',
    balance_sheet: 'Bilan',
    taxDeclaration: 'Liasse',
    rca: 'RCA',
    rcaExport: 'Export RCA',
    fec: 'FEC',
    fecImport: 'Import FEC',
    declaration: 'Déclarations',
    vat_ca3: 'TVA réelle CA3',
    dashboard: "Vue d'ensemble",
    keeping: 'Tenue',
    reviewing: 'Révision',
    myExchanges: 'Échanges',
    ecosystem: 'Écosystème',
    parameters: 'Paramètres',
    entry: 'Saisie/Consultation',
    declationToConfirm: 'Déclarations à confirmer',
    cca: 'CCA',
    FNP: 'FNP',
    AAE: 'AAE',
    AAR: 'AAR',
    FAE: 'FAE',
    CAPPersonal: 'CAP personnel',
    CAPInt: 'CAP Intérêt',
    CAPCs: 'CAP Cs',
    CAPDivers: 'CAP Divers',
    CAPIt: 'CAP It',
    pca: 'PCA',
    declare: 'Déclarer',
    officeManagement: 'Gestion cabinet',
    lmSpecific: 'LM spécifique',
    rhVariable: 'Variable RH',
    timeManagement: 'Gestion de temps',
    consultation: 'Consultation',
    accountConsultation: 'Consultation',
    chartAccount: 'Plan de compte',
    chartAccountTva: 'Plan de compte',
    accountingPlans: 'Plans comptables étalons',
    accountSheet: 'Fiche compte',
    trades: 'Échanges',
    diaryExport: 'Extourne journal',
    newAccounting: 'Saisie',
    dadp: 'DA/DP',
    ib: 'Intégration bancaire',
    diaryEntry: 'Saisie journal',
    cashing: 'Encaissement',
    ponctual: 'Ponctuel',
    keyFigures: 'Chiffres clés des dossiers',
    production: 'Production',
    wallet: 'Portefeuille',
    teledeclaration: 'Télédéclaration',
    lastTasks: 'Dernières tâches effectuées',
    exchanges: 'Échanges',
    memo: 'Mémos',
    comments: 'Commentaires',
    discussionsList: 'Discussions',
    ocr: 'Envoi document',
    ocrTracking: 'Suivi OCR',
    collector: 'Collecteurs Fournisseurs',
    dynamicsLinks: 'Liens dynamiques',
    newLink: 'Nouveau rapprochement',
    bankLink: 'Rapprochement bancaire',
    folder: 'Dossier',
    company: 'Société',
    book: 'Journaux',
    account: 'Comptes',
    ocr_unibox: 'OCR',
    connector: 'Connecteur (Silae, RCA...)',
    silae: 'SILAE',
    analytics: 'Analytique',
    writingPattern: "Modèles d'écritures",
    security: 'Titres',
    supplier: 'Fournisseurs',
    userList: 'Entreprise / Personne physique',
    rightManagement: 'Droits',
    companySheet: 'Fiche société',
    mail: 'Mails',
    alert: 'Alertes',
    billing: 'Facturation',
    followUp: 'Suivi EC/relances',
    time: 'Temps',
    iban: 'RIB',
    lm: 'LM',
    code: 'Codes',
    corporateName: 'Raison sociale',
    address: 'Adresses',
    legalForm: 'Forme juridique',
    siret: 'SIRET',
    vatNumber: 'N° TVA intraco',
    ecdmcm: 'EC/DM/CM/Collab',
    companyDetail: 'Coordonnées',
    apenaf: 'APE/NAF',
    collectiveAgreement: 'Convention collective',
    otherInformation: 'Informations Complémentaires',
    matriculationDate: 'Date immatriculation',
    capitalSharing: 'Composition du capital',
    financialYear: 'Exercices',
    cga: 'CGA',
    taxService: 'Impôts / organismes',
    currentEditions: 'Éditions courantes',
    balance: 'Balance',
    ledger: 'Grand livre',
    sig: 'SIG',
    statement: 'Bilan / CR',
    diary: 'Journaux',
    shortcuts: 'Raccourcis',
    notAvailable: 'Bientôt disponible',
    decloyer: 'Déclaration de loyer',
    ged: 'GED',
    settingsWorksheets: 'Comptes feuilles de travail',
    letters: 'Courriers',
    newLetters: 'Nouveau courrier',
    discussions: 'Mes échanges',
    rib: 'Banques',
    collection: 'Encaissements',
    import: 'Imports',
    export: 'Exports',
    margin: 'Marge',
    flow: 'Flux',
    tvaca3: 'TVA CA3',
    currentEditionDiary: 'Journaux',
    chainEditions: 'Editions chainées',
    raise: 'Relances',
    deadline: 'Echéances',
    ca12Advance: 'CA12 Acompte',
    annualCa12: 'CA12 Annuelle',
    is: 'IS',
    downPayment: 'Acompte',
    closeOut: 'Liquidation',
    refundTaxCredit: 'Remboursement CI',
    cvae_cfe: 'CVAE/CFE',
    downPaymentCvae: 'Acompte CVAE',
    balanceCvae: 'Solde CVAE',
    cfeM: 'CFE M',
    cfeCreation: 'CFE Création',
    other: 'Autres',
    declarationTracking: 'Suivi',
    declarationTrackingSettings: 'Paramètrage',
    das2: 'DAS 2',
    da_balanceSheet: 'DA Bilan',
    da_statusPending: 'DA Situation en-cours',
    da_newSituation: 'DA Nouvelle situation',
    worksheet: 'Feuilles de travail',
    vehicle: 'Véhicules',
    overhead_income: 'Relevé des frais généraux',
    booklet: 'Plaquettes',
    closing: 'Clôture',
    exportFEC: 'Exporter FEC',
    closingFiscalYear: 'Clôturer un exercice',
    closingMonth: 'Clôturer un mois',
    support: 'Support',
    generalLM: 'Lettre de mission générale',
    specificLM: 'Lettres de missions spécifiques',
    statisticsCrm: 'Statistiques',
    chargePlan: 'Plan de charge',
    internalStatistics: 'Statistiques',
    crm: 'CRM',
    internal_management: 'Gestion interne',
    function: 'Groupes',
    vat: 'TVA',
    other2561: '2561',
    other2777: '2777',
    tascom: 'TASCOM',
    archiveDeleteSociety: 'Archiver / Supprimer société',
    budgetary: 'Budgétaire',
    portfolioListView: 'Portefeuilles',
    bankIntegrationSettings: 'Comptes IB',
    reportsAndForms: 'Etats et formulaires',
    settingsStandardMailView: 'Courriers normés',
    scanAssociate: 'Associer un scan',
    accountingFirmSettings: 'Cabinet'
  },
  OCR: {
    title: 'Envoi de nouveaux documents',
    selectSociety: 'Sélectionnez une société',
    buttonOCRMonitoring: 'Consulter le suivi OCR',
    filesDropper: {
      purchases: 'Achats',
      expenseReports: 'Notes de frais',
      sales: 'Ventes',
      holdings: 'Avoirs',
      dropDocumentsMono: 'Faîtes glisser ici vos monofactures',
      dropDocumentsMulti: 'Faîtes glisser ici vos multifactures'
    },
    regrouping: {
      title: 'Regroupement de documents'
    },
    filesInformations: {
      title: 'Documents',
      billType: 'Type de facture',
      docType: 'Type de document',
      societyName: 'Nom de la société',
      name: 'Nom du fichier',
      status: 'État',
      buttonWithoutOCR: 'Écriture SANS OCR',
      buttonOCR: 'Écriture AVEC OCR',
      waiting: 'En attente d’envoi',
      error: 'Erreur !',
      dat: 'Relevé bancaire',
      purchases: 'Achats',
      expenseReports: 'Note de frais',
      sales: 'Ventes',
      holdings: 'Avoirs',
      sendBack: 'Renvoyer',
      success: 'Document envoyé, voir le suivi OCR pour plus d\'infos',
      successWithoutOCR: 'Document envoyé et disponible dans saisie / Flux Manuel'
    },
    trackingTable: {
      society: 'Société',
      fileName: 'Nom fichier',
      type: 'Type',
      sendBy: 'Envoyé par',
      status: 'Statut',
      totalInclTaxes: 'Total TTC',
      totalExclTaxes: 'Total HT',
      vatTotal: 'TVA',
      ocrRunning: 'OCR en cours',
      done: 'Traité',
      error: 'Erreur lecture',
      notRecognized: 'Document non reconnu. Transféré dans les flux manuels',
      parent: 'Parent',
      creditNote: 'Avoir',
      purchase: 'Achat',
      expenseReport: 'Note de frais',
      sale: 'Vente',
      me: 'Moi',
      title: 'OCR - Suivi',
      sendNewDoc: 'Envoyer un nouveau document',
      filter: {
        from: 'Du',
        to: 'Au',
        documentSend: {
          title: 'Documents envoyés :',
          all: 'Tous',
          byMe: 'Envoyés par moi'
        },
        reload: 'Actualiser'
      }
    }
  },
  filesDropper: {
    links: '<0>importer</0> depuis votre ordinateur ou <0>scanner</0> (en 300 DPI)'
  },
  Popover: {
    Society: {
      title: 'Ouvrir une société',
      empty: 'Aucune société disponible'
    },
    Flag: {
      empty: 'Aucun flag'
    }
  },
  newUserForm: {
    header: {
      title: 'Nouvel utilisateur',
      alternative_title: "Modification de l'utilisateur",
      steps: {
        identity: 'Identité',
        right: 'Gestion des droits',
        confirmation: 'Confirmation'
      }
    },
    userCreation: {
      title: 'Identité',
      validate: 'Suivant',
      cancel: 'Annuler',
      input: {
        gender: {
          mrs: 'Mme',
          mr: 'M.'
        },
        lastName: 'Nom',
        maidenName: "Nom d'usage",
        firstName: 'Prénom',
        mail: 'Email',
        mailWarning: 'Le mail est déjà attribué à un utilisateur',
        fixedPhone: 'Téléphone fixe',
        cellPhone: 'Téléphone portable'
      }
    },
    rightManagement: {
      title: 'Accès aux entreprises',
      wallet: 'Portefeuille',
      society: 'Entreprise',
      andOr: 'et / ou',
      type: 'Type',
      profil: 'Profil',
      add: 'Ajouter',
      validate: "Créer l'utilisateur",
      alternative_validate: "Modifier l'utilisateur",
      cancel: 'Annuler',
      reset: 'Réinitialiser les accès',
      walletList: {
        wallet: 'Portefeuille',
        type: 'Type',
        profil: 'Profil'
      },
      societyList: {
        society: 'Entreprise',
        type: 'Type',
        profil: 'Profil'
      }
    },
    function: {
      title: 'Groupes',
      type: 'Type',
      profil: 'Profil',
      function: 'Groupes'
    },
    groups: {
      type: 'Type général',
      profil: 'Profil général',
      groups: 'Groupes',
      validate: 'Valider',
      cancel: 'Annuler'
    },
    confirmation: {
      title: "Un email a été envoyé au collaborateur avec ses codes d'accès",
      alternative_title: "Le profil de l'utilisateur a bien été modifié",
      validate: 'Nouvel utilisateur',
      cancel: 'Retour à la liste des utilisateurs'
    }
  },
  account: {
    label: 'Intitulé du compte',
    number: 'Numéro de compte',
    intraco: 'Intraco',
    provider: 'Fournisseur.',
    das2: 'DAS 2',
    contrepartie: 'Contrepartie',
    validate: 'Valider',
    cancel: 'Annuler',
    createAccount: 'Création de compte',
    autoCompleteLabel: 'Créer : {{account_number}}',
    editVAT: {
      title: 'Modification code TVA'
    },
    newAccount: {
      title: 'Fiche compte',
      number: 'N° de compte',
      label: '',
      presta: 'Prestataire',
      contrepart: 'Contrepartie',
      TVA: 'Code TVA',
      cancel: 'Annuler',
      save: 'Enregistrer',
      resp: 'Responsable',
      SIRET: 'SIRET',
      nTvaDed: 'N°TVA ded',
      nTvaColl: 'N°TVA coll',
      taux: 'Taux',
      typeTva: 'Type TVA',
      dueTva: 'Exigibilité de la TVA',
      address: {
        label: 'Adresse',
        N: 'N°',
        comp: 'Comp',
        streetType: 'Type de voie',
        streetName: 'Nom de la voie',
        addressCompl: 'Complément d\'adresse',
        CP: 'Code postal',
        city: 'Ville',
        tel: 'Téléphone',
        email: 'Email',
        comment: 'Commentaire libre'
      },
      infoSteps: {
        infoGeneral: 'Info. générales',
        infoComplementary: 'Info. complémentaires'
      },
      ribSteps: {
        RIB: 'RIB',
        IBAN: 'IBAN'
      },
      BIC: 'BIC',
      establishmentName: 'Nom de l\'établissement',
      banc: 'Code banque',
      counter: 'Code guichet',
      NAccountBanc: 'Numéro de compte',
      paperBanc: 'Journal',
      addRib: '+ Ajouter un RIB'
    }
  },
  confirmation: {
    cancel: 'Annuler',
    validate: 'Valider'
  },
  counterPart: {
    title: 'Contrepartie',
    message: 'Voulez-vous vraiment changer la contrepartie du compte {{label}}?',
    msgParamAuto: 'Souhaitez-vous modifier la contrepartie du compte {{accountNumber}} par {{counterPartAccount}} ?'
  },
  comment: 'Commentaire',
  ok: 'ok',
  close: 'Fermer',
  attachments: {
    toolBar: {
      total: '{{count}} séléctionné',
      total_plural: '{{count}} séléctionnés'
    },
    ungroupedGridTitle: 'Rassembler des documents',
    groupedTitle: 'Documents attachés',
    importDocuments: 'Importer des documents',
    group: 'Agrafer',
    ungroup: 'Degrafer',
    manualAccounting: 'Saisie manuelle',
    cancel: 'Annuler',
    error: 'Une erreur est survenue !',
    loading: 'En cours de traitement',
    empty: 'Il n\'y a pas de documents'
  },
  Comment: {
    validate: 'Valider'
  },
  SingleComment: {
    comment: 'Commentaire',
    comment_information: 'Ajouté par {{userName}} le {{date}}'
  },
  userList: {
    toggleButton: 'Utilisateurs',
    userCreation: 'Nouvel Utilisateur',
    searchFilter: 'Nom, Prénom'
  },
  physicalPersonCreation: {
    title: 'Nouvelle personne physique',
    editPersonTitle: 'Informations concernant {{personName}}',
    previous: 'Précédent',
    next: 'Suivant',
    validate: 'Valider',
    tabs: {
      general: 'Informations générales',
      family: 'Liens familiaux',
      company: 'Sociétés',
      document: 'Documents liés',
      user_access: 'Utilisateur',
      employee: 'Salarié'
    },
    generalInformation_form: {
      contacts: 'Contacts',
      civility: 'Civilité',
      firstname: 'Prénom',
      name: 'Nom',
      maiden_name: "Nom d'usage",
      marital_situation: 'Situation matrimoniale',
      marital_regime: 'Régime matrimonial',
      birthdate: 'Date de naissance',
      birthdatePlaceholder: 'AAAA-MM-JJ',
      birthCity: 'Ville de naissance',
      birthDepartment: 'Département',
      birthCountry: 'Pays de naissance',
      Country: 'Pays',
      socialSecurity_number: 'N° de sécurité sociale',
      control_key: 'Clé de contrôle',
      address: 'Adresse',
      address_number: 'N°',
      comp: 'Comp',
      roadType: 'Type de voie',
      roadName: 'Nom de la voie',
      address_complement: "Complément d'adresse",
      zipCode: 'Code postal',
      city: 'Ville',
      physicalPerson_type: 'Catégorie de personnes',
      company: 'Entreprise',
      comment: 'Informations complémentaires',
      contacts_array: {
        addContact: '+ Ajouter un nouveau moyen de contact',
        contactDetails: 'Coordonnées',
        contactDescription: 'Description / Nom',
        contactType: 'Type'
      }
    },
    society_form: {
      labelPlaceholder: 'Ajouter une entreprise',
      empty: 'Pas de sociétés !',
      societyPPArrayTitle: {
        society: 'Société',
        address: 'Adresse',
        function: 'Fonction *',
        functionSignatory: 'Fonction signataire',
        entryDate: 'Date Entrée *',
        releaseDate: 'Date Sortie',
        numberFullOwnership: 'Nb parts en pleine propriété *',
        numberUsufruct: 'Nb parts usufruit *',
        numberPropertyBare: 'Nb parts nue propriété *',
        percentage: '%',
        total: 'Parts totales'
      },
      popUp: {
        title: 'Attention !',
        deleteMessage: 'Voulez-vous supprimer cette/ces société(s) ? \n'
        + 'Cette action est irréversible',
        deleteOk: 'Supprimer la/les société(s)',
        cancel: 'Annuler'
      }
    },
    employee_form: {
      labelPlaceholder: 'Ajouter une entreprise',
      empty: 'Pas de sociétés !',
      employeePPArrayTitle: {
        society: 'Société',
        function: 'Fonction *',
        functionSignatory: 'Fonction signataire',
        entryDate: 'Date Entrée *',
        releaseDate: 'Date Sortie'
      },
      popUp: {
        title: 'Attention !',
        deleteMessage: 'Voulez-vous supprimer cette/ces société(s) ? \n'
        + 'Cette action est irréversible',
        deleteOk: 'Supprimer la/les société(s)',
        cancel: 'Annuler'
      }
    },
    familyBoundaries_form: {
      autocomplete: 'Ajouter une personne à la liste',
      toCreationForm: 'Créer une nouvelle personne physique',
      genderHeader: 'Civilité',
      nameHeader: 'Nom',
      firstnameHeader: 'Prénom',
      linkHeader: 'Lien de parenté',
      empty: 'Pas de contacts !'
    },
    document_form: {
      dropzoneLegend: 'Faites glisser vos documents ici',
      dropzoneTitle: 'Documents liés',
      dateHeader: 'Date',
      nameHeader: 'Intitulé',
      empty: 'Pas de documents !'
    },
    companyAccess_form: {
      rightManagement: 'Gestion des droits de l\'utilisateur',
      isUser: 'utilise MyUnisoft',
      field: {
        company: 'Sociétes',
        level: "Niveau d'accès"
      }
    }
  },
  companyCreation: {
    title: 'Nouvelle entreprise',
    cancel: 'Annuler',
    previous: 'Précédent',
    next: 'Suivant',
    societyId: 'Identifiant société',
    editCompanyTitle: 'Modification {{companyName}}',
    tabs: {
      general: 'Infos générales',
      exercices: 'Exercices',
      taxRecords: 'Dossier fiscal',
      associates: 'Associés',
      subsidiaries: 'Filiales',
      clientUser: 'Utilisateurs',
      establishements: 'Établissements',
      buildings: 'Immeubles',
      evaluationElements: 'Éléments d’évaluation',
      billing: 'Facturation'
    },
    accountingPlan: 'Plan Comptable',
    nameAddress: 'Nom et Adresse',
    legal: 'Juridique',
    contacts: 'Contacts',
    access: 'Accès et informations',

    name: 'Nom de la société*',
    address_number: 'N°',
    address_bis: 'Comp',
    roadType: 'Type de voie',
    streetName: 'Nom de la voie',
    complement: 'Complément d’adresse',
    postalCode: 'Code postal',
    city: 'Ville',
    country: 'Pays',
    declarationAddressIsDifferent: 'L’adresse du déclarant est différente de l’adresse de l’entreprise ',
    siret: 'SIREN/SIRET',
    ape: 'APE/NAF',
    activity: 'Activitée exercée',
    legal_form: 'Forme',
    registration_date: 'Date d\'immatriculation',
    closeDate: 'Cessation',
    registrationPlace: 'Registre',
    accountingExpert: 'Expert comptable',

    accessInformationTitle: 'Définir une protection par mot de passe sur le dossier',
    password: 'Mot de passe',
    confirmation: 'Confirmation',
    registerPassword: 'Enregistrer',
    validate: 'Valider',
    ownerCompany: 'Dossier appartenant à*',
    comment: 'Informations',

    activeExercises: 'Exercice en cours',
    pastExercises: 'Exercices antérieurs',
    exerciseStartDate: 'Début',
    exerciseCloseDate: 'Fin',
    duration: 'Durée',
    result: 'Résultat en €',
    ca: 'CA en €',
    importDocument: 'FEC',
    batchButton: 'Liasse',
    editButton: 'Édition définitive',
    addNote: 'Note de synthèse',
    associates: {
      date: 'Date',
      dateInfo: 'Dernière date ayant eu un impact sur la répartition du capital Ex : date de création, augmentation du capital….',
      popUp: {
        title: 'Attention !',
        deleteMessage: 'Voulez-vous supprimer ce(s) associé(s) ? \n'
        + 'Cette action est irréversible',
        deleteOk: 'Supprimer le(s) associé(s)',
        cancel: 'Annuler'
      },
      associatesFilter: {
        display: 'Afficher: ',
        all: 'Tous',
        active: 'Associés actifs',
        out: 'Associés sortis'
      },
      capital: 'Capital €',
      numberSharesTotal: 'Nombre part total',
      nominalValue: 'Valeur nominale des parts',
      physicalPersonRef: '{{count}} personne physique référencée :',
      physicalPersonRef_plural: '{{count}} personnes physiques référencées :',
      physicalPerson: 'Sélectionnez une personne physique',
      legalPersonRef: '{{count}} personne morale référencée :',
      legalPersonRef_plural: '{{count}} personnes morales référencées :',
      buttonPhysicalPerson: 'Créer une nouvelle personne physique',
      physicalPersonArrayTitle: {
        name: 'Nom',
        firstname: 'Prénom',
        function: 'Fonction *',
        functionSignatory: 'Fonction signataire',
        entryDate: 'Date Entrée *',
        releaseDate: 'Date Sortie',
        numberFullOwnership: 'Nb parts en pleine propriété *',
        numberUsufruct: 'Nb parts usufruit *',
        numberPropertyBare: 'Nb parts nue propriété *',
        percentage: '%'
      },
      legalPerson: 'Sélectionnez une entreprise',
      buttonLegalPerson: 'Créer une nouvelle entreprise',
      legalPersonArrayTitle: {
        society: 'Société',
        siret: 'Siret',
        address: 'Adresse',
        functionSignatory: 'Fonction signataire',
        entryDate: 'Date Entrée *',
        releaseDate: 'Date Sortie',
        numberFullOwnership: 'Nb parts en pleine propriété *',
        numberUsufruct: 'Nb parts usufruit *',
        numberPropertyBare: 'Nb parts nue propriété *',
        percentage: '%',
        capital: 'Capital'
      },
      errors: {
        missingCapitalDate: 'Date de capital manquante'
      }
    },
    dossierFiscal: {
      dossier: 'Dossier',
      tva: 'TVA',
      edi: 'Envoi EDI',
      dossierStatus: 'Statut du dossier',
      date: 'Date',
      bilan: 'Bilan',
      taxes: 'Impôt',
      taxationRegime: 'Régime d\'imposition',
      gestionCenter: 'Centre de gestion',
      adherentCode: 'Code adhérent',
      memberShipYear: 'Année adhésion',
      rules: 'Résultat déterminé d\'après les régles',
      activityCode: 'Code activité pm',
      comptabilityHeld: 'Comptabilité tenue',
      comptabilityType: 'Type comptabilité',
      regimeTva: 'Régime TVA',
      tvaIntraco: 'N°TVA intracommunautaire',
      vatDueDate: 'Jour échéance TVA',
      rof_tva: 'ROF TVA',
      rof_tfdc: 'ROF TDFC',
      rof_cfe: 'ROF CFE',
      singleEstablishmment: 'Mono - établissement au sens de la CVAE',
      monoInfo: 'Si vous êtes assujetti à la CVAE et un mono-établissement au sens de la CVAE, cochez la case. Vous serez alors dispensé du dépôt de la déclaration 1330 CVAE.',
      accountingEntries: "Clôture automatique lors de l'envoi EDI TVA"
    },
    clientUser: {
      clientUserArrayTitle: {
        users: 'Utilisateur',
        profiltype: "Type d'utilisateur",
        profil: 'Profil'
      },
      manager: 'Manager',
      caseManager: 'Responsable de dossier'
    },
    subsidiaries: {
      subsidiariesRef: '{{count}} filiale référencée :',
      subsidiariesRef_plural: '{{count}} filiales référencées :',
      popUp: {
        title: 'Attention !',
        deleteMessage: 'Voulez-vous supprimer cette/ces filiale(s) ? \n'
        + 'Cette action est irréversible',
        deleteOk: 'Supprimer la/les filiale(s)',
        cancel: 'Annuler'
      },
      subsidiariesFilter: {
        display: 'Afficher: ',
        all: 'Tous',
        active: 'Filiales actifs',
        out: 'Filiales sortis'
      },
      Addsubsidiarie: 'Ajouter une filiale',
      buttonSubsidiarie: 'Nouvelle Filiale',
      subsidiaryArrayTitle: {
        subsidiary: 'Filiale',
        siret: 'Siret',
        address: 'Adresse',
        functionSignatory: 'Fonction signataire',
        entryDate: 'Date Entrée *',
        releaseDate: 'Date Sortie',
        numberFullOwnership: 'Nb parts en pleine propriété *',
        numberUsufruct: 'Nb parts usufruit *',
        numberPropertyBare: 'Nb parts nue propriété *',
        percent: '%',
        capital: 'Capital'
      },
      errors: {
        capital: 'Vous devez d\'abord remplir le champ capital dans la fiche de la société que vous voulez ajouter.'
      }
    },
    establishments: {
      establishmentsRef: '{{count}} établissement référencé',
      establishmentsRef_plural: '{{count}} établissements référencés',
      addEstablishments: 'Ajouter un établissement',
      buttonEstablishments: 'Créer un nouvel établissements',
      establishmentsArrayTitle: {
        name: 'Nom',
        siret: 'Siret',
        address: 'Adresse',
        RofCae: 'ROF CAE',
        leader: 'Responsable',
        tel: 'Téléphone',
        email: 'Email'
      },
      subTitleEstablishments: 'Informations sur «nom de l\'établissement ou nouvelle établissement»',
      nameEstablishments: 'Non de l\'établissement',
      siret: 'SIRET',
      RofCae: 'ROF CAE',
      Address: {
        title: 'Adresse',
        number: 'N°',
        comp: 'Comp',
        type: 'Type de voie',
        nameWay: 'Nom de la voie',
        compAddress: 'Complément d\'adresse',
        cp: 'Code postal',
        city: 'Ville',
        country: 'Pays'
      },
      leaderInfo: {
        title: 'Informations concernant le responsable',
        civility: 'Civilité',
        firstName: 'Prénom',
        name: 'Nom',
        tel: 'N° de tel de contact',
        email: 'Email de contact'
      }
    },
    evaluationsElements: {
      nbEstablisments: 'Nb établissements',
      nbSalaried: 'Nb de salariés',
      capital: 'Capital',
      exploitationProduct: 'Produit d\'exploitation',
      annualDeclaration: 'Déclaration de revenu annuel (par an)',
      adequacyReport: 'Bilan prévisionnel business plan',
      subtitleInvoiceNumber: 'Nombre de factures mensuelles',
      customerSale: 'Vente client',
      providers: 'Fournisseurs',
      nbBankAccount: 'Nombre de comptes bancaires professionnels',
      purchaseOfBusiness: 'Achat de fond de commerce',
      subtitleCompta: 'Accès MyUnisoft Compta',
      subtitleGestion: 'Accès MyUnisoft Gestion',
      yes: 'Oui',
      no: 'Non'
    },
    buildings: {
      buildingsRef: '{{count}} établissement référencé:',
      buildingsRef_plural: '{{count}} établissements référencés:',
      buttonBuildings: 'Nouvel Immeuble',
      buildingArrayTitle: {
        name: 'Nom',
        address: 'Adresse',
        acquisitionDate: 'Date d\'acquisition',
        sectionCode: 'Code section'
      },
      subTitleBuilding: 'Informations sur « nom de l\'immeuble »',
      nameBuilding: 'Nom de l\'immeuble',
      localNumber: 'Nombre de locaux',
      number: 'N°',
      comp: 'Comp',
      type: 'Type de voie',
      nameWay: 'Nom de la voie',
      compAddress: 'Complément d\'adresse',
      cp: 'Code postal',
      city: 'Ville',
      country: 'Pays',
      landSystem: 'Dispositif foncier',
      schema: 'Régime / Déduction',
      acquisitionDate: 'Acquisition',
      completion: 'Achèvement',
      checkboxNude: 'Immeuble nu à usage autre qu\'habitation (détermination de la valeur ajoutée-E)',
      natureTitle: 'Nature de l\'immeuble',
      radioNature: {
        IR: 'Immeuble de rapport - IR',
        P: 'Parking - P',
        AP: 'Appartement - AP',
        M: 'Maison - M',
        A: 'Autres - A'
      },
      situationTitle: 'Situation',
      switchSituation: {
        U: 'Urbain - U',
        R: 'Rural - R'
      },
      featureTitle: 'Caractéristiques',
      switchFeature: {
        B: 'Bâti - B',
        NB: 'Non bâti - NB'
      }
    },
    errors: {
      name: 'Ce champ doit être complété',
      formatDateError: 'Format de date invalide',
      owner_company: 'Ce champ doit être complété',
      postal_code: 'Le code postal doit comporter 5 chiffres',
      siret: 'Le format du SIRET saisi est invalide',
      password: 'Le mot de passe ne correspond pas',
      duration: 'La durée doit être entre 1 et 24 mois',
      start_date: 'La date de début doit être antérieure à la date de début des prochains exercices',
      end_date: 'La date de fin doit être antérieure à la date de début des prochains exercices',
      shareValue: 'Ce champ ne doit comporter que des chiffres',
      address_number: 'Le numéro ne doit comporter que des chiffres',
      function: 'Ce champ doit être complété',
      function_signatory: 'Ce champ doit être complété',
      effective_date: 'Ce champ doit être complété',
      social_part: {
        NP: 'Ce champ doit être complété',
        PP: 'Ce champ doit être complété',
        US: 'Ce champ doit être complété'
      },
      society_id: 'id de la societe invalide',
      signatory_function_id: 'fonction signataire inconnu',
      function_id: 'fonction inconnu',
      capital_date: 'Veuillez remplir tous les champs au niveau du capital',
      social_part_pp: 'Le nombre de part à ajouté est supérieur au nombre de part disponible sur la société',
      no_social_part_society: 'Vous devez d\'abord remplir le champ capital dans la fiche de la société que vous voulez ajouter.'
    },
    dialogs: {
      sirenConfirm: {
        title: 'ce SIREN correspond à une société déjà existante',
        ok: 'Voir la société existante',
        cancel: 'Revenir à la saisie'
      },
      sirenInfo: {
        title: 'Le SIRET saisi ne correspond à aucun SIREN connu',
        ok: 'OK'
      }
    }
  },
  totals: 'Totaux',
  companyUserList: {
    title: 'Liste des personnes physiques, entreprises et utilisateurs',
    listChoice: 'Sur quelle liste souhaitez-vous travailler ?'
  },
  companyList: {
    default: 'Global',
    addNewCompany: 'Ajouter une entreprise',
    stepFilter: 'Étapes',
    companyTypeHeader: 'Sur quelle liste souhaitez-vous travailler ?',
    companyTypeFilter: 'Type de société',
    searchFilter: 'Nom entreprise, Ville, SIRET, APE',
    physicalPersonToggleButton: 'Personnes physiques',
    legalPersonToggleButton: 'Entreprises'
  },
  physicalPersonList: {
    addNewPhysicalPerson: 'Ajouter une personne physique',
    searchFilter: 'Nom, Prénom'
  },
  changeAccount: {
    title: 'Changement du numéro de compte',
    validate: 'Valider',
    cancelled: 'Annuler',
    switchAccount: 'Numéro de compte:'
  },
  existingPerson: {
    title: 'Il semblerait que la personne existe déjà',
    validate: 'Valider',
    cancel: 'Ignorer'
  },
  changeDiary: {
    title: 'Changement de journal',
    validate: 'Valider',
    cancelled: 'Annuler',
    switchAccount: 'Numéro de journal:'
  },
  currentEditions: {
    reset: 'réinitialiser',
    header: {
      title: 'Editions courantes',
      selectOptions: {
        currentEditions: 'Editions courantes',
        sig: 'SIG',
        balance: 'Balance',
        resultAccount: 'Compte de résultat',
        diary: 'Journal',
        statement: 'Bilan',
        current: 'Courante',
        general: 'Générale',
        customer: 'Client',
        supplier: 'Fournisseur',
        supplies: 'Provisions client',
        salaried: 'Salarié',
        analytic: 'Analytique',
        ledger: 'Grand livre'
      },
      filtersButton: 'Filtres',
      variationsButton: 'Variations'
    },
    variations: {
      display: 'Afficher les variations',
      placeholder: {
        eur: 'Montant',
        percent: '0'
      }
    },
    filters: {
      from: 'Du',
      to: 'Au',
      exercice: 'Exercice(s)',
      deadline: 'Date limite',
      accountNumber: 'N° de compte',
      checkboxes: {
        allLines: 'Toutes les lignes',
        accountNumberExample: 'Ex: 1-5, 8, 11-13',
        something: 'Afficher les écritures de situations'
      },
      situationWrites: 'Écritures de situation',
      dialog: {
        save: 'Sauvegarder ma recherche',
        saveName: 'Nom de la sauvegarde',
        saveNameExemple: 'Exemple de titre',
        get: 'Utiliser une recherche sauvegardée',
        selectSavesPlaceholder: 'Mes recherches enregistrées',
        cancel: 'Annuler',
        getValidate: 'Valider',
        saveValidate: 'Sauvegarder'
      }
    },
    table: {
      filters: {
        isRevisionsHidden: 'Masquer les révisions',
        isAllMonthVisible: 'Afficher tous les mois',
        displayType: {
          month: 'Mois',
          plurality: 'Cumul',
          aged: 'Agée'
        },
        allAccounts: 'Toutes les lignes',
        accountNumber: 'N° de compte',
        loadTable: 'Charger le tableau',
        partialAccountHolder: 'Ex: 1-3, 5, 6-7'
      },
      header: {
        accountNumber: 'N° Compte',
        label: 'Intitulé',
        comment: 'Com.',
        valid_rm: 'Valid RM',
        valid_collab: 'Valid collab',
        '30d': '< 30j',
        '45d': '< 45j',
        '60d': '< 60j',
        '90d': '< 90j',
        '120d': '< 120j',
        '+120d': 'Au dela',
        month01: 'Janv.',
        month02: 'Févr.',
        month03: 'Mars',
        month04: 'Avr.',
        month05: 'Mai',
        month06: 'Juin',
        month07: 'Juill.',
        month08: 'Août',
        month09: 'Sept.',
        month10: 'Oct.',
        month11: 'Nov.',
        month12: 'Déc.',
        balance: 'Solde',
        relativeVariation: 'Var N/N-1',
        absoluteVariation: 'Montant € variation'
      },
      tools: {
        generate: 'Générer OD',
        export: 'Exporter',
        print: 'Imprimer',
        dialog: {
          cancel: 'Annuler',
          printTitle: 'Imprimer le tableau',
          exportTitle: 'Exporter le tableau',
          form: {
            lines: {
              all: 'Toutes les lignes',
              selected: 'Lignes sélectionnées'
            },
            onlyUnresolvedAccount: 'Afficher comptes non soldés uniquement',
            watermark: 'Filigrane',
            exportExtension: 'Format d’exportation'
          }
        }
      }
    }
  },
  shortcuts: {
    allShortcuts: 'Tous les raccourcis',
    arrow_up: 'Flèche haut',
    arrow_down: 'Flèche bas',
    ACCOUNTING: {
      title: 'Saisie',
      NEW_TAB: 'Nouvel onglet',
      NEW_WINDOW: 'Nouvelle fenêtre',
      ENTER: 'Entrer',
      SHOW_FILTER: 'Afficher les filtres',
      LAUNCH_CONSULTING: 'Split sur compte dans une nouvelle fenêtre'
    },
    NEW_ACCOUNTING: {
      title: 'Nouvelle saisie',
      NEW_ACCOUNTING: 'Nouvelle ligne',
      TAB: 'Passer à la case suivante',
      EQUAL: 'Egal',
      PLUS: 'Plus',
      MINUS: 'Moins',
      MULTIPLE: 'Multiplier',
      DIVIDE: 'Diviser',
      ENTER: 'Entrer',
      MANUAL_ACCOUNTING: 'Créer une écriture manuelle',
      RESET_NEW_ACCOUNTING: 'Réinitialiser l\'écriture',
      INTERCHANGE_DEBIT_CREDIT: 'Echanger le débit et le crédit de la ligne',
      AUTOFILL_OTHER_LINES: 'Autocompléter les lignes suivantes',
      SOLDE_ENTRY: 'Solder l\'écriture',
      COPY_ENTRY: 'Copier l\'écriture',
      SUBMIT_ENTRY: 'Envoyer l\'écriture',
      DELETE_ENTRY: 'Supprimer la ligne',
      CANCEL_ENTRY: 'Annuler l\'écriture',
      SHOW_ACCOUNTING_PLAN: 'Afficher le plan comptable',
      SHOW_ACCOUNT: 'Afficher les comptes',
      FOCUS_PREVIOUS_CELL: 'Retour au champ précédent',
      FOCUS_NEXT_CELL: 'Aller au champ suivant',
      NEXT_ENTRY: 'Ligne suivante',
      PREVIOUS_ENTRY: 'Ligne précédente'
    },
    CONSULTING: {
      title: 'Consultation',
      LAUNCH_NEW_ACCOUNTING: 'Lancer une nouvelle saisie',
      VALIDATE: 'Valider',
      SPLIT_ENTRY: 'Split sur une écriture dans une nouvelle fenêtre',
      CANCEL: 'Annuler',
      LETTER: 'Lettrer',
      UNLETTER: 'Délettrer',
      SHOW_ACCOUNT_LEFT: 'Compte précédent',
      SHOW_ACCOUNT_RIGHT: 'Compte suivant',
      FOCUS_NEXT_ENTRY: 'Sélectionner la ligne précédente',
      FOCUS_PREVIOUS_ENTRY: 'Sélectionner la ligne suivante'
    },
    DASHBOARD_SOCIETY: {
      title: 'Dashboard Société',
      NEW_ACCOUNTING: 'Lancer une nouvelle saisie',
      NEW_AWAITING_DOC: 'Vers les OCR en attente',
      OCR_FLOW: 'Vers le suivi OCR',
      BANKING_FLOW: 'Vers l\'intégration bancaire en attente',
      CURRENT_EDITION: 'Vers la balance'
    },
    NAV_BAR: {
      title: 'Global',
      NEW_TAB: 'Nouvel onglet ',
      NEW_WINDOW: 'Nouvelle fenetre',
      OPEN_NEW_ACCOUNTING: 'Nouvelle Saisie',
      CURRENT_EDITION: 'Editions courantes',
      CRM: 'Liste personnes physiques, entreprises et utilisateurs',
      SEND_NEW_DOCUMENT: 'Envoi de nouveaus documents',
      GED: 'Gestion et documents',
      ACCOUNT_SETTING: 'ouvrir le paramétrage du compte',
      CHAT: 'Discussions',
      PLAN: 'Ouvrir le plan de comptes',
      CONSULT: 'Ouvrir la consultation du compte',
      ECRITURE: 'Ouvrir Ecriture',
      DISCUS: 'Ouvrir discussion',
      GEDS: 'Ouvrir la GED',
      CRMS: ' Ouvrir le CRM '
    },
    BALANCE: {
      title: 'Balance',
      LAUNCH_CONSULTING: 'Split sur compte',
      SPLIT_ENTRY: 'Split sur écriture',
      SPLIT_ACCOUNT: 'Split sur compte dans une nouvelle fenêtre'
    },
    GED: {
      title: 'GED',
      LAUNCH_CONSULTING: 'Split sur compte dans une nouvelle fenêtre',
      SPLIT_ENTRY: 'Split sur écriture'
    },
    DIALOGS: {
      title: 'Modales',
      VALIDATE: 'Valider le contenu de la modale',
      CLOSE: 'Fermer la modale'
    },
    DIALOG_CONFIRMATION: {
      title: 'Fenêtre de dialogue'
    },
    DIALOG_INFORMATION: {
      title: 'Modal d\'information'
    }
  },
  discussions: {
    general: 'Général',
    dossiers: 'Dossiers',
    messagesDirect: 'Messages directs',
    declaration: 'Déclaration TVA',
    yesterday: 'Hier',
    createdAtBy: 'Créée le {{value.date}} par {{value.user}}',
    yourMessage: 'Votre message',
    commonFiles: 'Fichiers partagés',
    showAllFiles: '+ Afficher tous les fichiers',
    participants: 'Participants',
    addParticipants: '+ Ajouter des participants',
    tagLabel: 'Pour ajouter un # : #mot',
    tags: 'Tags',
    new: {
      title: 'Nouvelle discussion',
      selectPeople: 'Sélectionnez les personnes avec qui vous souhaitez communiquer',
      setPrivateCheckBox: 'Définir comme privée.',
      setPrivateCheckBoxLabel: 'Seul vous et les participants peuvent voir les messages',
      cancelButton: 'Annuler',
      createButton: 'Nouvelle discussion',
      setPrivateDescription: 'Seul vous et les participants peuvent voir les messages',
      conversationTitle: 'Titre de la conversation',
      addParticipants: 'Ajouter des participants'
    },
    directMessage: {
      title: 'Nouveau message direct',
      selectPeopleEmpty: 'Rechercher par nom ou entreprise',
      selectPeople: 'Sélectionnez la ou les personnes avec qui vous souhaitez communiquer:',
      selectPersonsLabel: 'Ajouter des participants',
      cancelButton: 'Annuler',
      createButton: 'Nouveau message direct',
      selectPlaceholder: 'Rechercher par nom ou personne morale'
    },
    messageDateNames: {
      sameDay: "[Aujourd'hui]",
      nextDay: '[Demain]',
      nextWeek: '[La semaine prochaine]',
      lastDay: '[Hier]',
      lastWeek: '[La semaine dernière]',
      sameElse: 'LL', // 'Plus ancien',
      uploadOngoing: 'LL' // "En cours d'envoi"
    },
    search: {
      title: 'Rechercher',
      searchInDiscussions: 'rechercher dans les discussions',
      searchPlaceholder: 'Rechercher',
      tags: 'Tags',
      mentions: 'mentions',
      directs: 'Messages direct',
      people: 'Personnes',
      findLabel: 'Rechercher',
      findButton: 'Rechercher',
      cancelButton: 'Annuler'
    },
    subheaders: {
      general: 'Général',
      dossiers: 'Dossiers',
      messagesDirect: 'Messages directs'
    },
    filterMenu: {
      filterBy: 'Filtrer par:',
      activity: 'Activité',
      alphabetical: 'Ordre alphabétique',
      personalised: 'Personnaliser …'
    },
    fileShare: {
      shareWith: 'Partager un fichier depuis:',
      GED: 'GED',
      myComputer: 'Mon ordinateur',
      new: 'Nouveau:',
      survey: 'Sondage',
      question: 'Question Oui / Non',
      searchInput: 'Rechercher',
      alreadySent: 'Le document est déjà present dans cette discussion. Voulez vous créer une copie?'
    },
    ged: {
      cancel: 'Annuler',
      share: 'Partager',
      title: 'Sélectionner des fichiers à importer depuis la GED',
      searchInputLabel: 'Rechercher'
    },
    participantMenu: {
      addParticipant: 'Ajouter des participants',
      removeParticipant: 'Retirer des participants',
      setAsPrivate: 'Définir comme privée',
      archive: 'Archiver'
    },
    selectParticipants: {
      add: {
        title: 'Ajouter des participants',
        selectPeopleEmpty: 'Rechercher par nom ou entreprise',
        selectPeople: 'Sélectionnez la ou les personnes avec qui vous souhaitez communiquer:',
        selectPersonsLabel: 'Ajouter des participants',
        cancelButton: 'Annuler',
        createButton: 'Ajouter'
      },
      delete: {
        title: 'Retirer des participants',
        selectPeopleEmpty: 'Rechercher par nom ou entreprise',
        selectPeople: 'Sélectionnez la ou les personnes avec qui vous souhaitez communiquer:',
        selectPersonsLabel: 'Retirer des participants',
        cancelButton: 'Annuler',
        createButton: 'Retirer de la discussion'
      }
    },
    quickSearch: {
      rechercher: 'Rechercher',
      searchMessagePlaceholder: 'Entrez un mot',
      searchUserPlaceholder: 'Nom ou prénom'
    },
    newAttachment: {
      title: 'Partager un fichier',
      messageHint: 'Ajoutez un message',
      cancelButton: 'Annuler',
      createButton: 'Partager'
    }
  },
  rib: {
    title: 'Liste des Rib',
    titleModal: 'Saisie d\'un Rib',
    titleModalEdit: 'Editer un Rib',
    button: {
      newRib: 'Nouveau Rib',
      changeRib: 'Modifier',
      deleteRib: 'Supprimer'
    },
    ribArrayTitle: {
      bankCode: 'Code banque',
      guichetCode: 'Code guichet',
      numberAccount: 'N° de cpte bancaire',
      IBAN: 'IBAN',
      key: 'Clé',
      BIC: 'BIC',
      paper: 'Journal'
    },
    buttonCancel: 'Annuler',
    buttonSave: 'Enregistrer'
  },
  connector: {
    title: 'Paramètres connecteurs',
    newConnector: 'Nouveau connecteur',
    modifyConnector: 'Modifier',
    deleteConnector: 'Supprimer',
    connectorArrayTitle: {
      society: 'Société',
      software: 'Logiciel',
      code: 'Code',
      information: 'Information complémentaire'
    },
    dialog: {
      creationTitle: 'Nouveau connecteur',
      modificationTitle: 'Fiche connecteur',
      society: 'Société',
      software: 'Logiciel',
      code: 'Code',
      additionalInformation: 'Information complémentaire',
      empty: 'Ce champ est obligatoire',
      cancel: 'Annuler',
      confirm: 'Enregistrer'
    }
  },
  function: {
    default: 'Global',
    title: 'Paramétrages des groupes',
    newFunction: 'Nouveau groupe',
    modifyFunction: 'Modifier',
    deleteFunction: 'Supprimer',
    functionArrayTitle: {
      functionName: 'Libellé',
      usersNumber: 'Nb utilisateurs'
    },
    user: 'utilisateur',
    users: 'utilisateurs',
    dialog: {
      libelle: 'Nom du groupe',
      creationTitle: 'Nouveau groupe',
      modificationTitle: 'Fiche groupe'
    },
    deleteDialog: {
      deleteButton: 'Supprimer un groupe'
    }
  },
  split: {
    accounting: 'Saisies',
    consulting: 'Consultation',
    new_accounting: 'Nouvelle saisie',
    toolbar: {
      split: 'Split',
      swap: 'Echanger',
      close: 'Fermer',
      master: 'Maître',
      popup: 'Ouvrir dans une nouvelle fenêtre',
      slave: 'Esclave'
    }
  },
  diary: {
    title: 'Paramétrages journaux',
    button: {
      newDiary: 'Nouveau Journal',
      changeDiary: 'Modifier',
      deleteDiary: 'Supprimer'
    },
    dialog: {
      title: 'Fiche Journal',
      code: 'Code Journal',
      label: 'Intitulé',
      type: 'Type de journal',
      number: 'N° de compte',
      save: 'Enregistrer',
      cancel: 'Annuler',
      empty: 'Ce champ est obligatoire',
      empty_code: 'Le code journal est obligatoire',
      empty_label: 'L’intitulé est obligatoire',
      empty_type: 'Le type de journal est obligatoire',
      errorSizeCode: 'Ce champ doit contenir entre 2 et 4 caractères',
      unavailable: 'Ce code est déjà utilisé'
    }
  },
  bankIntegrationSettings: {
    title: 'Paramétrages des comptes pour l’intégration bancaire',
    btns: {
      new: 'Nouveau paramétrage',
      modify: 'Modifier',
      delete: 'Supprimer'
    },
    table: {
      name: 'Intitulé',
      accountNumber: 'N° Compte',
      flowDirection: 'Sens Débit',
      meaningOfCredit: 'Sens Crédit',
      interbankCode: 'Codes interbancaires',
      keyword: 'Mot clé',
      heading: 'Intitulé',
      sens: 'Sens',
      code: 'Codes interbancaires',
      intitule: 'Intitulé'
    },
    popUp: {
      titleNew: 'Affiliation de comptes pour les intitulés de relevés bancaires',
      info: '(Laisser vide pour reprendre intitulé fourni par la banque)',
      titleModify: 'Modification du comptes pour les intitulés de relevés bancaires',
      cancel: 'Annuler',
      ok: 'Valider',
      debit: 'Sens Debit',
      all: 'Tous',
      credit: 'Sens Credit',
      warning: 'Attention',
      deleteMessage: 'Voulez-vous supprimer ce paramétrage ? \n'
        + 'Cette action est irréversible',
      deleteOk: 'Supprimer ce paramètre',
      empty: 'Ce champs est obligatoire.',
      none: 'Aucun',
      calculateTVA: 'Calculer TVA',
      tvaAndComm: 'Calculer TVA + Commissions',
      percentage: 'Saisir un pourcentage de commissions',
      commission: 'Saisir un compte de commissions',
      tvaCode: 'TVA Code'
    }
  },
  closingYear: {
    title: 'Clôture de l\'exercice',
    from: 'Du',
    to: 'Au',
    generateLabel: 'Génération des à nouveaux :',
    yes: 'Oui',
    details: 'Détaillés',
    question: 'Voulez-vous clôturer l’exercice du <1>{{start_date}}</1> au <3>{{end_date}}</3> ?',
    warning: 'Cette opération est irréversible',
    submit: 'Clôturer l\'exercice',
    cancel: 'Annuler',
    loading: 'Clôture en cours ...',
    popUp: {
      success: 'Votre exercice est clôturé avec succès',
      congrats: 'Félicitation!',
      ok: 'Ok'
    }
  },
  reportsAndForms: {
    title: 'Gestion des états et formulaires',
    createNew: 'Nouvel État',
    modify: 'Modifier',
    delete: 'Supprimer',
    selected: '{{count}} sélectionée',
    table: {
      type: 'Type de l\'état',
      name: 'Nom',
      description: 'Description',
      fiscalYear: 'Millésime',
      enterprises: 'Entreprises'
    },
    orderTable: {
      orderNumber: 'N° ordre dans l\'état',
      codeEDI: 'Code EDI',
      wording: 'Libellé',
      dataType: 'Type de donnée',
      viewing: 'Affichage',
      dataSource: 'Source des données',
      references: 'Références'
    },
    deletePopUp: {
      mainMessage: 'Voulez-vous vraiment supprimer l’état ?\n'
        + 'Cette opération est irréversible.',
      ok: 'Supprimer cet état',
      warning: 'Attention !',
      cancel: 'Annuler'
    },
    dialog: {
      createNew: 'Gestion des états et formulaires',
      modify: 'Modify', // TODO replace with original title
      ok: 'Valider',
      cancel: 'Annuler',
      name: 'Nom (15 caract. max)',
      selectData: 'Sélectionnez les données',
      selectStateOrForm: 'Sélectionner un état ou un formulaire',
      fromExistingState: 'À partir d’un état existant',
      newState: 'Nouvel état'
    },
    alreadySelectedTable: {
      title: 'Déjà sélectionnés :',
      stateOrForm: 'État ou formulaire',
      labelAndNumber: 'Libellé et numéro du champ'
    },
    confirmDialog: {
      title: 'Voulez vous récupérer les lignes d\'un autre état ?',
      cancel: 'Non',
      ok: 'Oui'
    }
  },
  exports: {
    title: 'Nouvel Export',
    noExercice: "Il n'y a pas d'exercice à exporter",
    typeOptions: {
      fec: 'FEC',
      partial_fec: 'FEC partiel'
    },
    field: {
      type: "Type d'export"
    },
    fec: {
      society: 'Sociétés',
      exercise: 'Exercices'
    },
    toExport: 'Exporter'
  },
  dinamicLink: {
    title: 'Liens dynamiques',
    idTitle: 'Numéro du schéma',
    linkToDownload: 'Id scociété à récupérer dans l’URL de la ',
    companyWindow: 'fenêtre société :',
    insruction: 'Les instructions pour utiliser le fichier Excel se trouvent sur la premiere page',
    downloadExel: 'Télécharger le fichier Excel'
  },
  imports: {
    import_input: 'Importer un fichier',
    fileOrigin: 'Import depuis',
    import_success: 'Import réussi',
    rows_number: 'lignes ont été importées',
    title: 'Nouvel Import',
    hasEntries: 'Attention, des écritures existent sur cet exercice vous pouvez les écraser ou les ajouter aux écritures existantes',
    buttons: {
      import: 'Importer',
      importAndAdd: 'Importer et ajouter',
      importAndErase: 'Importer et écraser'
    },
    field: {
      type: 'Type d\'import',
      source: 'Source',
      member_code: 'Code membre',
      society_code: 'Code société',
      member_group: 'Membre'
    },
    sourceOptions: {
      comptaSa: 'Compta SA',
      cegid: 'Cegid / Quadra'
    },
    typeOptions: {
      whole: 'Compta complète',
      fec: 'FEC',
      bank: 'EBICS',
      other: 'Autre',
      assets: 'Immobilisations',
      excel: 'Excel (xls/csv)',
      quadra: 'QUADRA (ASCII)'
    },
    formats: {
      cegid: 'Cegid',
      coala: 'Coala',
      eic: 'EIC',
      agiris: 'Agiris',
      acd: 'ACD'
    },
    import_source: {
      autres: 'Autres',
      coala: 'Coala'
    },
    ebisFilters: {
      title: 'Faites glisser vos documents'
    },
    fecFilters: {
      field: {
        society: 'Société',
        exercices: 'Exercice'
      }
    },
    assetsFilters: {
      field: {
        society: 'Société',
        importDepuis: 'Import depuis'
      }
    }
  },
  sigTable: {
    header: {
      title: 'Intitulé',
      current_year: 'Solde N',
      previous_year: 'Solde N-1',
      variation_percentage: 'Var N/N-1',
      variation_amount: 'Montant € variation'
    }
  },
  tree: {
    error: 'Une erreur est survenue !'
  },
  settingStandardMail: {
    title: 'Paramétrage des courriers',
    createNew: 'Nouveau type de courrier',
    createNewParagraph: 'Nouveau paragraphe',
    modify: 'Modifier',
    delete: 'Supprimer',
    dialogs: {
      newTypeTitle: 'Nouveau type de courrier',
      modifyTypeTitle: 'Modifier type de courrier',
      newParagraphTitle: 'Nouveau paragraphe',
      modifyParagraphTitle: 'Modifier paragraphe',
      newTypeCheck: 'Reprendre l\'intitulé comme objet du courrier ?',
      paragraphLabel: 'Libellé du paragraphe',
      paragraphBody: 'Corps du paragraphe'
    },
    deletePopUp: {
      mainMessage: 'Voulez-vous vraiment supprimer l’état ?\n'
          + 'Cette opération est irréversible.',
      ok: 'Supprimer cet état',
      warning: 'Attention !',
      cancel: 'Annuler'
    },
    tables: {
      label: 'Intitulé',
      resume: 'Reprendre l\'intitulé comme objet du courrier ?',
      functionName: 'Libellé',
      bodyOfText: 'Corps de texte'
    }
  },
  successDialog: {
    header: 'Félicitation!',
    message: 'Votre état a été généré. Vous pouvez le consulter depuis la diligence "Rapprochement bancaire" du DADP.',
    buttonOk: 'Ok'
  },
  scanAssociate: {
    title: 'Associer un scan',
    form: {
      typeOfScan: 'Type de scan à associer',
      associationCode: 'Code d’association',
      confirm: 'Valider',
      cancel: 'Annuler',
      emptyError: 'Ce nom existe déjà !'
    }
  },
  foreignBrowserAlert: {
    title: 'Il semble que vous n’utilisiez pas le navigateur Chrome',
    message: 'MyUnisoft est actuellement optimisée pour Chrome. Afin d’utiliser l’application dans les meilleures conditions nous vous invitons à télécharger google Chrome.',
    button: 'Télécharger Chrome'
  },
  accountingFirmSettings: {
    title: 'Paramètres cabinets',
    new: 'Nouveau',
    modify: 'Modifier',
    delete: 'Supprimer',
    tabs: {
      generalInfo: 'Infos générales',
      letters: 'Courriers',
      silae: 'Silae',
      declare: 'jeDeclare'
    },
    edi: 'Comptes EDI',
    rib: 'Comptes RB',
    cabinetLogo: 'Logo du cabinet',
    footer: 'Pied de page',
    forms: {
      siret: 'SIRET',
      cabinetName: 'Nom du Cabinet',
      number: 'N°',
      comp: 'Comp',
      chanelType: 'Type de voie',
      nameOfRoad: 'Nom de la voie',
      addressSupplement: 'Complément d’adresse',
      postalCode: 'Code postal',
      city: 'Ville',
      email: 'Email',
      tel: 'Téléphone',
      fax: 'Fax',
      site: 'Site internet',
      logo: 'Logo du cabinet',
      dragHere: 'Faîtes glisser votre logo ici',
      footer: 'Pied de page',
      import: 'importer',
      fromFile: 'depuis vos fichier',
      or: 'ou',
      scanner: 'scanner',
      dpi: '(en 300 DPI)',
      loginSilae: 'Login SILAE',
      passwordSilae: 'Mot de passe SILAE',
      passwordWSSilae: 'Mot de passe WS SILAE',
      loginWsSilae: 'Login WS SILAE',
      addressWsSilae: 'Adresse WS SILAE'
    },
    table: {
      siret: 'Siret',
      nameOfFirm: 'Nom du cabinet',
      postalCode: 'Code postal',
      city: 'Ville',
      login: 'Login',
      active: 'Actif',
      password: 'Mot de passe'
    },
    emptyError: 'Ce champ doit être renseigné',
    equal: 'The property must be equal',
    tooLong: 'Le code est trop long',
    dialog: {
      newTitle: 'Fiche comptes',
      modifyTitle: 'Modifier comptes',
      warning: 'Attention',
      deleteMessage: 'Voulez-vous supprimer ce paramétrage ? \n'
        + 'Cette action est irréversible'
    },
    slave: {
      accountingFirmSettingsBody: {
        empty: {
          title: 'Ecriture',
          description: 'Cliquez sur un cabinet pour en afficher les détails'
        }
      }
    }
  },
  pdfValidation: {
    tvaca12Annual: {
      empty: 'Ce champ est obligatoire'
    },
    tva3310: {
      empty: 'Ce champ est obligatoire'
    },
    tvaca3: {
      empty: 'Ce champ est obligatoire',
      GH: 'Attention ligne 7C + ligne 17 + ligne 19 > ligne 16',
      JA: 'Attention ligne AA + ligne 26 > ligne 25',
      KA: 'Attention ligne AB + ligne 29 > ligne 28',
      HF: 'Attention ligne 24 > ligne 23'
    },
    postalCode: 'Le code postal doit comporter 5 chiffres',
    invalidEmail: 'Veuillez saisir une adresse email valide'
  },
  cvae: {
    title: 'Déclaration de CVAE par EDI',
    cvae1329DEF: '1329-DEF',
    sum_up: 'Récap',
    detail_control: 'Détail contrôle',
    control: 'Contrôle',
    period: 'Période',
    send: 'Envoyer à l\'EDI',
    help: 'Notice',
    modify: 'Modifier',
    delete: 'Supprimer'
  },
  ediDialogs: {
    ENVOIEDI1: {
      message: 'Un envoi EDI est en cours de traitement (envoi sans accusé): voulez-vous annuler et remplacer l\'envoi EDI précédent ?'
    },
    ENVOIEDI2: {
      message: 'Un envoi EDI a été déjà validé et payé : voulez-vous annuler et remplacer l\'envoi EDI précédent ?'
    },
    ENVOIEDI3: {
      message: 'Un envoi EDI a été déjà validé mais le paiement a été rejeté : voulez-vous annuler et remplacer l\'envoi EDI précédent ?'
    },
    ENVOIEDI4: {
      message: 'Un envoi EDI a été déjà envoyé mais il a été rejeté : voulez-vous annuler et remplacer l\'envoi EDI précédent ?'
    },
    buttons: {
      yes: 'OUI',
      no: 'NON'
    }
  },
  accessDasboard: {
    password: 'Mot de passe'
  },
  crm: {
    dialog: {
      title: {
        user: 'Voulez-vous vraiment supprimer le profil de cet utilisateur ?',
        physicalPerson: 'Voulez-vous vraiment supprimer le profil de cette personne ?',
        company: 'Voulez-vous vraiment supprimer le profil de cette entreprise ?'
      }
    }
  },
  rentDeclarations: {
    title: 'Déclaration de loyer'
  }
};
