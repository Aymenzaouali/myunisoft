import i18n from 'i18next';
import { reactI18nextModule } from 'react-i18next';
import en from './locales/en';
import fr from './locales/fr';

i18n
  .use(reactI18nextModule) // passes i18n down to react-i18next
  .init({
    resources: {
      en: {
        translation: {
          ...en
        }
      },
      fr: {
        translation: {
          ...fr
        }
      }
    },
    lng: 'en',
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false
    }
  });

export default i18n;
