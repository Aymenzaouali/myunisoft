import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  palette: {
    primary: {
      light: '#0bd1d1',
      main: '#0bd1d1',
      dark: '#0bd1d1',
      contrastText: '#fff'
    },
    secondary: {
      light: '#ff8b1f',
      main: '#ff8b1f',
      dark: '#ff8b1f',
      contrastText: '#fff'
    },
    error: {
      light: '#fe3a5e',
      main: '#fe3a5e',
      dark: '#fe3a5e',
      contrastText: '#fff'
    }
  },
  overrides: {
    MuiModal: {
      root: {
        // Has to be above everything else
        zIndex: 3000
      }
    },
    MuiButton: {
      root: {
        'text-transform': 'none',
        fontFamily: 'basier_circlemedium',
        'border-radius': '2px',
        'box-shadow': 'none !important'
      },
      sizeSmall: {
        minWidth: 32
      }
    },
    MuiInput: {
      root: {
        fontSize: 14
      },
      underline: {
        '&:before': {
          // "border-bottom": "1px solid #9b9b9b",
        }
      },
      input: {
        '&[type=number]::-webkit-inner-spin-button, &[type=number]::-webkit-outer-spin-button': {
          margin: 0,
          WebkitAppearance: 'none'
        }
      }
    },
    MuiSnackbar: {
      root: {
        zIndex: 1600
      }
    },
    MuiInputBase: {
      root: {
        '&$disabled': {
          '&:before': {
            borderBottomStyle: 'solid !important'
          }
        }
      }
    },
    MuiTableCell: {
      root: {
        padding: '2px 4px',
        whiteSpace: 'nowrap'
      },
      body: {
        fontFamily: 'basier_circleregular',
        fontSize: '12px',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontStretch: 'normal',
        lineHeight: 'normal',
        letterSpacing: 'normal',
        color: '#000000'
      },
      head: {
        fontFamily: 'basier_circlemedium',
        fontSize: '12px',
        fontWeight: 500,
        fontStyle: 'normal',
        fontStretch: 'normal',
        lineHeight: 'normal',
        letterSpacing: 'normal',
        color: '#000000'
      }
    },
    MuiCheckbox: {
      root: {
        paddingRight: '8px',
        padding: '0px',
        color: 'primary'
      }
    },
    MuiToggleButtonGroup: {
      root: {
        '&$selected': {
          boxShadow: 'none'
        }
      }
    },
    MuiTableRow: {
      root: {
        transition: 'backgroundColor .2s'
      },
      hover: {
        cursor: 'pointer',
        '&:hover': {
          backgroundColor: '#e3e3e3'
        }
      }
    },
    MuiToggleButton: {
      root: {
        height: 'auto',
        padding: '8px 28px',
        color: '#9b9b9b',
        textTransform: 'none',
        border: '1px solid #9b9b9b',
        borderLeft: 'none',
        '&:hover': {
          backgroundColor: '#fff'
        },
        '&:first-child': {
          borderLeft: '1px solid #9b9b9b'
        },
        '&:last-child': {
          borderLeft: 'none'
        },
        '&$selected': {
          backgroundColor: '#0bd1d1',
          color: '#fff',
          border: '1px solid #0bd1d1 !important',
          '&:hover': {
            backgroundColor: '#0bd1d1'
          }
        }
      },
      selected: {}
    },
    MuiInputLabel: {
      root: {
        whiteSpace: 'nowrap',
        fontSize: 14
      }
    },
    MuiFormControlLabel: {
      root: {
        marginLeft: 0
      }
    },
    MuiTooltip: {
      tooltip: {
        boxShadow: '1px 5px 7px 0 rgba(0, 0, 0, 0.08)',
        backgroundColor: '#fff',
        border: '1px solid #0bd1d1',
        color: 'black',
        borderRadius: 2,
        fontSize: 14,
        maxWidth: 469,
        padding: 20
      }
    },
    MuiCard: {
      root: {
        'box-shadow': 'none'
      }
    }
  },
  typography: {
    useNextVariants: true,
    body1: {
      fontSize: '14px',
      fontFamily: 'basier_circleregular'
    },
    body2: {
      fontSize: '12px',
      fontFamily: 'basier_circleregular'
    },
    h1: {
      fontSize: '22px',
      fontWeight: 500,
      fontFamily: 'basier_circlemedium'
    },
    h4: {
      fontSize: '14px',
      fontFamily: 'basier_circlemedium',
      fontWeight: 500
    },
    h5: {
      fontSize: '12px',
      fontFamily: 'basier_circlemedium',
      fontWeight: 500
    },
    h6: {
      fontSize: '16px',
      fontFamily: 'basier_circleregular',
      fontWeight: 600
    },
    subtitle1: {
      fontSize: '16px',
      fontFamily: 'basier_circleregular'
    },
    subtitle2: {
      fontSize: '12px',
      fontFamily: 'basier_circleregular'
    },
    title3: {
      fontSize: 17,
      fontFamily: 'basier_circleregular',
      fontWeight: 500
    },
    subtitle3: {
      fontSize: 14,
      fontFamily: 'basier_circleregular'
    },
    button: {
      fontSize: '14px'
    },
    display1: {
      fontSize: '14px',
      fontFamily: 'basier_circleregular'
    },
    fontFamily: [
      'basier_circleregular',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Symbol"'
    ].join(',')
  }
});
