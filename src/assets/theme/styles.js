
export const borderInputStyles = () => ({
  focusedInputBorder: {
    border: '1px solid #0bd1d1',
    'background-color': 'white'
  },
  underlineBorder: {
    '&:before': {
      'border-bottom': 'none'
    },
    '&&&&:hover:before': {
      'border-bottom': '2px solid #0bd1d1'
    }
  }
});

export const InputLabelNoWrap = {
  inputLabelRootOverride: {
    whiteSpace: 'nowrap',
    fontSize: 14
  }
};

export const smallButtonNoPadding = () => ({
  noPadding: {
    padding: '2px',
    minHeight: '32px',
    minWidth: '36px'
  }
});
