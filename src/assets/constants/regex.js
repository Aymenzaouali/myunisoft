export const MAIL_FORMAT = /^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;  // eslint-disable-line
export const PHONE_NUMBER_FORMAT = /^[+]*[(]?[0-9]{1,4}[)]?[-\s./0-9]*$/;

export const TAB_REGEX = /tab\/([0-9]+|:id)\/(.+)/;
