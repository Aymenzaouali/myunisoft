export const LETTERS_TABLE_NAME = 'letters';
export const PHYSICAL_PERSON_TABLE_NAME = 'associates_physical_person';
export const LEGAL_PERSON_TABLE_NAME = 'associates_legal_person';
export const SOCIETY_PP_TABLE_NAME = 'society_list_pp';
export const EMPLOYEE_PP_TABLE_NAME = 'employee_list_pp';
export const SUBSIDIARY_TABLE_NAME = 'subsidiary';
export const CLIENT_USER_TABLE_NAME = 'clientUserTable';
export const STATE_OR_FORM_DIALOG_TABLE_NAME = 'stateOrFormDialogTable';
export const REPORT_AND_FORMS_NEW_STATE = 'reportsAndFormsCreateNewState';
export const REPORT_AND_FORMS_TABLE = 'reportsAndFormsTable';
export const PARAGRAPHS_TYPES_TABLE = 'paragraphsTypesTable';
export const PARAGRAPH_TABLE = 'paragraphTable';
export const ACCOUNTING_FIRM_TOP_MAIN_TABLE = 'AccountingFirmTopMainTable';
export const ACCOUNTING_FIRM_DECLARE_TABLE_EDI = 'AccountingFirmDeclareTableEDI';
export const ACCOUNTING_FIRM_DECLARE_TABLE_RIB = 'AccountingFirmDeclareTableRIB';

export const getTableName = (societyId, tableName) => `${tableName}-${societyId}`;
