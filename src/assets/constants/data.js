import I18n from 'assets/I18n';

export const accountingTypes = [
  {
    value: 'e',
    label: I18n.t('accounting.types.writing')
  },
  {
    value: 'm',
    label: I18n.t('accounting.types.flux')
  },
  {
    value: 'o',
    label: I18n.t('accounting.types.ocr')
  },
  {
    value: 'ib',
    label: I18n.t('accounting.types.banking')
  },
  {
    value: 'p',
    label: I18n.t('accounting.types.pay')
  },
  {
    value: 'ext',
    label: I18n.t('accounting.types.pending')
  }
];

export const associatesFilter = [
  // {
  //   value: 't',
  //   label: I18n.t('companyCreation.associates.associatesFilter.all'),
  //   color: 'colorError'
  // },
  {
    value: 'a',
    label: I18n.t('companyCreation.associates.associatesFilter.active')
  }
  // {
  //   value: 's',
  //   label: I18n.t('companyCreation.associates.associatesFilter.out'),
  //   color: 'colorError'
  // }
];

export const subsidiariesFilter = [
  // {
  //   value: 't',
  //   label: I18n.t('companyCreation.subsidiaries.subsidiariesFilter.all'),
  //   color: 'colorError'
  // },
  {
    value: 'a',
    label: I18n.t('companyCreation.subsidiaries.subsidiariesFilter.active')
  }
  // {
  //   value: 's',
  //   label: I18n.t('companyCreation.subsidiaries.subsidiariesFilter.out'),
  //   color: 'colorError'
  // }
];

export const status = [
  {
    label: I18n.t('dadp.status.to_do'),
    value: 'to_do'
  },
  {
    label: I18n.t('dadp.status.to_validate'),
    value: 'to_validate'
  },
  {
    label: I18n.t('dadp.status.running'),
    value: 'running'
  },
  {
    label: I18n.t('dadp.status.ko'),
    value: 'ko'
  },
  {
    label: I18n.t('dadp.status.ok'),
    value: 'ok'
  }
];

export const docFormats = {
  DOC: {
    format: 'doc',
    color: '#4b8bf5'
  },
  DOC_X: {
    format: 'docx',
    color: '#4b8bf5'
  },
  PDF: {
    format: 'pdf',
    color: '#f06a7d'
  },
  EXCEL: {
    format: 'xlsx',
    color: '#3da240'
  },
  EXCEL_X: {
    format: 'xls',
    color: '#3da240'
  },
  POWER_POINT: {
    format: 'ppt',
    color: '#cb4a32'
  },
  POWER_POINT_X: {
    format: 'pptx',
    color: '#cb4a32'
  },
  PNG: {
    format: 'png',
    color: '#238e11'
  },
  JPEG: {
    format: 'jpeg',
    color: '#238e11'
  },
  JPG: {
    format: 'jpg',
    color: '#238e11'
  },
  GIF: {
    format: 'gif',
    color: '#fa85a8'
  },
  BMP: {
    format: 'bmp',
    color: '#fa85a8'
  },
  TIFF: {
    format: 'tiff',
    color: '#f36f21'
  },
  VIDEO: {
    format: 'mkv',
    color: '#5268b2'
  },
  ZIP: {
    format: 'zip',
    color: '#f7e055'
  },
  RAR: {
    format: 'rar',
    color: '#663111'
  },
  MP3: {
    format: 'mp3',
    color: '#155d9b'
  },
  TXT: {
    format: 'txt',
    color: '#232524'
  }
};
export const GED_TABLE_OPTIONS = {
  COPY: 'copy',
  COPY_LINK: 'copyLink',
  SEND_BY_EMAIL: 'sendByEmail',
  SEND_IN_DISCUSSION: 'sendInDiscussion',
  DEATACH: 'deatach'
};

export const GEDTableMenu = [
  { label: I18n.t('ged.table.menu.copy'), value: GED_TABLE_OPTIONS.COPY },
  { label: I18n.t('ged.table.menu.copyLink'), value: GED_TABLE_OPTIONS.COPY_LINK },
  { label: I18n.t('ged.table.menu.sendByEmail'), value: GED_TABLE_OPTIONS.SEND_BY_EMAIL },
  { label: I18n.t('ged.table.menu.sendInDiscussion'), value: GED_TABLE_OPTIONS.SEND_IN_DISCUSSION },
  { label: I18n.t('ged.table.menu.deatach'), value: GED_TABLE_OPTIONS.DEATACH }
];

export const GEDTabs = [
  { id: 0, label: I18n.t('ged.filter.tabs.general') },
  { id: 1, label: I18n.t('ged.filter.tabs.inProgress') },
  { id: 2, label: I18n.t('ged.filter.tabs.accounts') },
  { id: 3, label: I18n.t('ged.filter.tabs.scriptures') },
  { id: 4, label: I18n.t('ged.filter.tabs.da_dp') },
  { id: 5, label: I18n.t('ged.filter.tabs.discussion') },
  { id: 6, label: I18n.t('ged.filter.tabs.documentDatabase') }
];

export const buyingDiaryAccounts = [
  '2',
  '40', '4452', '4456', '455', '471',
  '6',
  '75'
];
export const sellingDiaryAccounts = [
  '41', '4457',
  '580',
  '7'
];
export const odDiaryAccounts = ['51', '53'];

export const USER_TYPES = {
  ACCOUNTANT: 'ACCOUNTANT',
  RM: 'RM',
  COLLAB: 'COLLAB'
};

export const CODE_SHEET_GROUPS = {
  ACOMPTE_IS: 'ACOMPTE-IS',
  SOLDE_IS: 'SOLDE-IS',
  ACOMPTE_CVAE: 'ACOMPTE-CVAE',
  SOLDE_CVAE: 'SOLDE-CVAE'
};

export const supervisorList = [
  { role: 'Superviseur', hasChecked: true, color: 'green' },
  { role: 'Collaborateur', hasChecked: true, color: 'purple' },
  { role: 'RM', hasChecked: true, color: 'yellow' }
];

export const timelyOptions = [
  {
    id: 0,
    value: 'balance',
    label: I18n.t('currentEditions.header.selectOptions.balance')
  },
  {
    id: 2,
    value: 'sig',
    label: 'SIG'
  },
  {
    id: 1,
    value: 'ledger',
    label: I18n.t('currentEditions.header.selectOptions.ledger')
  }
];

export const permanentOptions = [
  {
    id: 0,
    value: 'balance',
    label: I18n.t('currentEditions.header.selectOptions.balance')
  },
  {
    id: 2,
    value: 'sig',
    label: 'SIG'
  },
  {
    id: 1,
    value: 'ledger',
    label: I18n.t('currentEditions.header.selectOptions.ledger')
  }
];
