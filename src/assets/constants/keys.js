export const sigTableHeaderKeys = [
  { type: 'toggleButton', keyLabel: 'toggle' },
  { type: 'div', keyLabel: 'title' },
  { type: 'div', keyLabel: 'current_year' },
  { type: 'div', keyLabel: 'previous_year' },
  { type: 'div', keyLabel: 'variation_percentage' },
  { type: 'div', keyLabel: 'variation_amount' }
];

export const ocrFilesTableHeaderKeys = [
  { label: 'OCR.filesInformations.name', field: 'name', isI18nKey: false },
  { label: 'OCR.filesInformations.billType', field: 'type', isI18nKey: true },
  { label: 'OCR.filesInformations.status', field: 'status', isI18nKey: true }
];

export const ebicsFilesTableHeaderKeys = [
  { label: 'OCR.filesInformations.name', field: 'blob.name', isI18nKey: false },
  { label: 'OCR.filesInformations.docType', field: 'type', isI18nKey: true }
];

export const accountingHeaderKeys = [
  { type: 'checkbox', keyLabel: 'checkbox' },
  { type: 'sortCell', keyLabel: 'flag' },
  { type: 'sortCell', keyLabel: 'info' },
  { type: 'sortCell', keyLabel: 'diary' },
  { type: 'sortCell', keyLabel: 'date_piece' },
  { type: 'sortCell', keyLabel: 'account_number' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'piece_1' },
  { type: 'sortCell', keyLabel: 'piece_2' },
  { type: 'sortCell', keyLabel: 'debit', isAmount: true },
  { type: 'sortCell', keyLabel: 'credit', isAmount: true },
  { type: 'sortCell', keyLabel: 'com', paddingLeft: true },
  { type: 'sortCell', keyLabel: 'pj' },
  { type: 'sortCell', keyLabel: 'contrepartie' },
  { type: 'sortCell', keyLabel: 'deadline' },
  { type: 'sortCell', keyLabel: 'date_accounting' },
  { type: 'sortCell', keyLabel: 'payment_type' }
];

export const newAccountingHeaderKeys = [
  { type: 'sortCell', keyLabel: 'flag' },
  // { type: 'sortCell', keyLabel: 'diary' },
  { type: 'sortCell', keyLabel: 'date_piece' },
  { type: 'sortCell', keyLabel: 'account_number' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'piece_1' },
  { type: 'sortCell', keyLabel: 'piece_2' },
  { type: 'sortCell', keyLabel: 'debit', isAmount: true },
  { type: 'sortCell', keyLabel: 'credit', isAmount: true },
  {
    type: 'sortCell', keyLabel: 'com', center: true
  },
  { type: 'sortCell', keyLabel: 'pj', center: true },
  { type: 'sortCell', keyLabel: 'contrepartie' },
  { type: 'sortCell', keyLabel: 'deadline' },
  { type: 'sortCell', keyLabel: 'payment_type' }
  // { type: 'sortCell', keyLabel: 'date_accounting' }
];

export const accountingPlansHeaderKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'description' }
];

export const accountingPlanDetailHeaderKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'account_number' },
  { type: 'sortCell', keyLabel: 'label' }
];

export const trackingOcrHeaderKeys = [
  'society',
  'fileName',
  'type',
  'sendBy',
  'status',
  'totalExclTaxes',
  'vatTotal',
  'totalInclTaxes'
];

export const accountListHeaderKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'accountNumber' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'solde', isAmount: true },
  { type: 'sortCell', keyLabel: 'sens' },
  { type: 'sortCell', keyLabel: 'counterPart' },
  { type: 'sortCell', keyLabel: 'vat' },
  { type: 'sortCell', keyLabel: 'intraco' },
  { type: 'sortCell', keyLabel: 'presta' },
  { type: 'sortCell', keyLabel: 'das2' },
  { type: 'sortCell', keyLabel: 'analyticSection' },
  { type: 'sortCell', keyLabel: 'analyticImputation' }
];

export const companyListTranslationKeys = [
  { type: 'label', keyLabel: 'society_id' },
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'city' },
  { type: 'sortCell', keyLabel: 'siret' },
  { type: 'sortCell', keyLabel: 'status', colorError: true },
  {
    type: 'sortCell', keyLabel: 'step', colorError: true, isDisabled: true
  },
  { type: 'sortCell', keyLabel: 'ape' },
  { type: 'string', keyLabel: 'delete' }
];

export const physicalPersonListTranslationKeys = [
  { type: 'sortCell', label: 'gender' },
  { type: 'sortCell', label: 'name' },
  { type: 'sortCell', label: 'firstname' },
  { type: 'sortCell', label: 'company' },
  { type: 'sortCell', label: 'phoneNumber' },
  { type: 'string', label: 'phoneNumberCount' },
  { type: 'sortCell', label: 'mail' },
  { type: 'string', label: 'mailCount' },
  { type: 'sortCell', label: 'role' },
  { type: 'string', keyLabel: 'delete' }
];

export const physicalPersonRefKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'firstname' },
  { type: 'sortCell', keyLabel: 'function' },
  { type: 'sortCell', keyLabel: 'functionSignatory' },
  { type: 'sortCell', keyLabel: 'entryDate' },
  { type: 'sortCell', keyLabel: 'releaseDate' },
  { type: 'sortCell', keyLabel: 'numberFullOwnership', isAmount: true },
  { type: 'sortCell', keyLabel: 'numberUsufruct', isAmount: true },
  { type: 'sortCell', keyLabel: 'numberPropertyBare', isAmount: true },
  { type: 'sortCell', keyLabel: 'percentage', isAmount: true }
];

export const physicalPersonSocietyRefKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'society' },
  { type: 'sortCell', keyLabel: 'address' },
  { type: 'sortCell', keyLabel: 'function' },
  { type: 'sortCell', keyLabel: 'functionSignatory' },
  { type: 'sortCell', keyLabel: 'entryDate' },
  { type: 'sortCell', keyLabel: 'releaseDate' },
  { type: 'sortCell', keyLabel: 'numberFullOwnership', isAmount: true },
  { type: 'sortCell', keyLabel: 'numberUsufruct', isAmount: true },
  { type: 'sortCell', keyLabel: 'numberPropertyBare', isAmount: true },
  { type: 'sortCell', keyLabel: 'percentage', isAmount: true },
  { type: 'sortCell', keyLabel: 'total', isAmount: true }
];

export const physicalPersonEmployeeRefKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'society' },
  { type: 'sortCell', keyLabel: 'function' },
  { type: 'sortCell', keyLabel: 'functionSignatory' },
  { type: 'sortCell', keyLabel: 'entryDate' },
  { type: 'sortCell', keyLabel: 'releaseDate' }
];

export const legalPersonRefKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'society' },
  { type: 'sortCell', keyLabel: 'siret' },
  { type: 'sortCell', keyLabel: 'address' },
  { type: 'sortCell', keyLabel: 'functionSignatory' },
  { type: 'sortCell', keyLabel: 'entryDate' },
  { type: 'sortCell', keyLabel: 'releaseDate' },
  { type: 'sortCell', keyLabel: 'numberFullOwnership', isAmount: true },
  { type: 'sortCell', keyLabel: 'numberUsufruct', isAmount: true },
  { type: 'sortCell', keyLabel: 'numberPropertyBare', isAmount: true },
  { type: 'sortCell', keyLabel: 'percentage' },
  { type: 'sortCell', keyLabel: 'capital', isAmount: true }
];

export const establishmentRefKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'siret' },
  { type: 'sortCell', keyLabel: 'address' },
  { type: 'sortCell', keyLabel: 'RofCae' },
  { type: 'sortCell', keyLabel: 'leader' },
  { type: 'sortCell', keyLabel: 'tel' },
  { type: 'sortCell', keyLabel: 'email' }
];

export const buildingRefKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'address' },
  { type: 'sortCell', keyLabel: 'acquisitionDate' },
  { type: 'sortCell', keyLabel: 'sectionCode' }
];

export const subsidiaryRefKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'subsidiary' },
  { type: 'sortCell', keyLabel: 'siret' },
  { type: 'sortCell', keyLabel: 'address' },
  { type: 'sortCell', keyLabel: 'functionSignatory' },
  { type: 'sortCell', keyLabel: 'entryDate' },
  { type: 'sortCell', keyLabel: 'releaseDate' },
  { type: 'sortCell', keyLabel: 'numberFullOwnership', isAmount: true },
  { type: 'sortCell', keyLabel: 'numberUsufruct', isAmount: true },
  { type: 'sortCell', keyLabel: 'numberPropertyBare', isAmount: true },
  { type: 'sortCell', keyLabel: 'percent' },
  { type: 'sortCell', keyLabel: 'capital', isAmount: true }
];

export const clientUserRefKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'users' },
  { type: 'sortCell', keyLabel: 'profiltype' },
  { type: 'sortCell', keyLabel: 'profil' }
];

export const ribRefKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'bankCode' },
  { type: 'sortCell', keyLabel: 'guichetCode' },
  { type: 'sortCell', keyLabel: 'numberAccount' },
  { type: 'sortCell', keyLabel: 'IBAN' },
  { type: 'sortCell', keyLabel: 'BIC' },
  { type: 'sortCell', keyLabel: 'paper' }
];

export const portfolioListViewTableKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'entitled' },
  { type: 'sortCell', keyLabel: 'companies' },
  { type: 'sortCell', keyLabel: 'relatedUsers' }
];

export const connectorHeaderKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'society' },
  { type: 'sortCell', keyLabel: 'software' },
  { type: 'sortCell', keyLabel: 'code' },
  { type: 'sortCell', keyLabel: 'information' }
];

export const functionHeaderKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'functionName' },
  { type: 'sortCell', keyLabel: 'usersNumber' }
];

export const vatHeaderKeys = [
  'checkbox',
  'code',
  'vat_ded_number',
  'vat_coll_number',
  'vat_taux',
  'vat_type',
  'vat_due'
];

export const userListTranslationKeys = [
  { type: 'sortCell', keyLabel: 'civility' },
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'maiden_name' },
  { type: 'sortCell', keyLabel: 'firstname' },
  { type: 'sortCell', keyLabel: 'mail' },
  { type: 'sortCell', keyLabel: 'tel' },
  { type: 'sortCell', keyLabel: 'libelle_type_profil' },
  { type: 'sortCell', keyLabel: 'libelle_profil' },
  { type: 'string', keyLabel: 'delete' }
];

export const collectorKeys = [
  'checkbox',
  'ProviderName',
  'BillDate',
  'BillName',
  'BillAmount'
];

// Général
export const generalDocMenagamentKeys = [
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'entitled' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'addedBy' },
  { type: 'sortCell', keyLabel: 'linkedTo' },
  { type: 'empty', keyLabel: '' }
];

// En cours
export const inProgresDocMenagamentKeys = [
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'entitled' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'addedBy' },
  { type: 'sortCell', keyLabel: 'amountTTC' },
  { type: 'empty', keyLabel: '' }
];

// Comptes
export const accountsDocMenagamentKeys = [
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'entitled' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'linkedTo' },
  { type: 'sortCell', keyLabel: 'amountTTC' },
  { type: 'sortCell', keyLabel: 'amountHT' },
  { type: 'sortCell', keyLabel: 'amountTVA' },
  { type: 'sortCell', keyLabel: 'dueDate' },
  { type: 'sortCell', keyLabel: 'comments' },
  { type: 'sortCell', keyLabel: 'letter' },
  { type: 'empty', keyLabel: '' }
];

// Écritures
export const scripturesMenagamentKeys = [
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'entitled' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'addedBy' },
  { type: 'sortCell', keyLabel: 'linkedTo' },
  { type: 'sortCell', keyLabel: 'amountTTC' },
  { type: 'sortCell', keyLabel: 'amountHT' },
  { type: 'sortCell', keyLabel: 'amountTVA' },
  { type: 'sortCell', keyLabel: 'dueDate' },
  { type: 'sortCell', keyLabel: 'comments' },
  { type: 'sortCell', keyLabel: 'letter' },
  { type: 'empty', keyLabel: '' }
];

// DA/DP - list
export const lDADPDocMenagamentKeys = [
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'entitled' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'addedBy' },
  { type: 'sortCell', keyLabel: 'linkedTo' },
  { type: 'sortCell', keyLabel: 'titleFT' },
  { type: 'sortCell', keyLabel: 'accountNumber' },
  { type: 'sortCell', keyLabel: 'comments' },
  { type: 'sortCell', keyLabel: 'amountTTC' },
  { type: 'sortCell', keyLabel: 'amountTVA' },
  { type: 'sortCell', keyLabel: 'amountHT' },
  { type: 'empty', keyLabel: '' }
];

// DA/DP - miniature
export const mDADPDocMenagamentKeys = [
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'worksheet' },
  { type: 'sortCell', keyLabel: 'linkedTo' },
  { type: 'sortCell', keyLabel: 'amountTTC' },
  { type: 'empty', keyLabel: '' }
];

// Discussions
export const discussionsDocMenagamentKeys = [
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'entitled' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'addedBy' },
  { type: 'sortCell', keyLabel: 'title' },
  { type: 'sortCell', keyLabel: 'bodyMessage' },
  { type: 'sortCell', keyLabel: 'tags' },
  { type: 'empty', keyLabel: '' }
];

// Base documentaire
export const baseDocMenagamentKeys = [
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'entitled' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'addedBy' },
  { type: 'empty', keyLabel: '' }
];

export const bankLinkKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'counterpart_number' },
  { type: 'sortCell', keyLabel: 'piece' },
  { type: 'sortCell', keyLabel: 'piece_2' },
  { type: 'sortCell', keyLabel: 'dotted' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'debit', isAmount: true },
  { type: 'sortCell', keyLabel: 'credit', isAmount: true }
];

export const workingPageDefaultKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'number' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'total_excl_taxes', isAmount: true },
  { type: 'sortCell', keyLabel: 'vat_code', paddingLeft: true },
  { type: 'sortCell', keyLabel: 'vat_total', isAmount: true },
  { type: 'sortCell', keyLabel: 'total_incl_taxes', isAmount: true },
  {
    type: 'sortCell',
    keyLabel: 'pj',
    paddingLeft: true,
    isCentered: true
  },
  { type: '', keyLabel: 'addPj' },
  { type: 'sortCell', keyLabel: 'compta', isCentered: true },
  { type: 'sortCell', keyLabel: 'comment', isCentered: true }
];

export const CCAKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'number' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'amount', isAmount: true },
  {
    type: 'sortCell', keyLabel: 'period', paddingLeft: true, isCentered: true
  },
  { type: 'sortCell', keyLabel: 'day_total', isAmount: true },
  { type: 'sortCell', keyLabel: 'advanced_day_total', isAmount: true },
  { type: 'sortCell', keyLabel: 'CCA', isAmount: true },
  {
    type: 'sortCell', keyLabel: 'pj', paddingLeft: true, colSpan: 2
  },
  { type: 'sortCell', keyLabel: 'compta' },
  { type: 'sortCell', keyLabel: 'comment' }
];

export const CAP_DIVKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'number' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'total_excl_taxes', isAmount: true },
  { type: 'sortCell', keyLabel: 'vat_code', paddingLeft: true },
  { type: 'sortCell', keyLabel: 'vat_total', isAmount: true },
  { type: 'sortCell', keyLabel: 'total_incl_taxes', isAmount: true },
  {
    type: 'sortCell',
    keyLabel: 'pj',
    paddingLeft: true,
    isCentered: true
  },
  { type: 'sortCell', keyLabel: 'compta', isCentered: true },
  { type: 'sortCell', keyLabel: 'comment', isCentered: true }
];

export const CAP_ITKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'number' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'total_excl_taxes', isAmount: true },
  { type: 'sortCell', keyLabel: 'vat_code', paddingLeft: true },
  { type: 'sortCell', keyLabel: 'vat_total', isAmount: true },
  { type: 'sortCell', keyLabel: 'total_incl_taxes', isAmount: true },
  {
    type: 'sortCell',
    keyLabel: 'pj',
    paddingLeft: true,
    isCentered: true
  },
  { type: 'sortCell', keyLabel: 'compta', isCentered: true },
  { type: 'sortCell', keyLabel: 'comment', isCentered: true }
];

export const workProgramKeys = [
  'dilligence',
  'guides',
  'pj',
  'comment',
  'na',
  'valid_collab',
  'valid_rm',
  'ref'
];

export const anayticReviewKeys = [
  { keyLabel: 'account_number' },
  { keyLabel: 'label' },
  { keyLabel: 'soldeN', isAmount: true },
  { keyLabel: 'soldeN-1', isAmount: true },
  { keyLabel: 'evo', isAmount: true },
  { keyLabel: 'va', isAmount: true },
  { keyLabel: 'valid_collab' },
  { keyLabel: 'valid_rm' },
  { keyLabel: 'nb_docs' },
  { keyLabel: 'comments' }
];

export const diaryKeys = [
  {
    type: 'checkbox',
    keyLabel: ''
  },
  {
    type: 'string',
    keyLabel: 'diary_code'
  },
  {
    type: 'string',
    keyLabel: 'diary_label'
  },
  {
    type: 'string',
    keyLabel: 'diary_type'
  },
  {
    type: 'string',
    keyLabel: 'account_number'
  }
];

// Bank integration settings
export const bankIntegrationSettingsColumns = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'sortCell', keyLabel: 'accountNumber' },
  { type: 'sortCell', keyLabel: 'flowDirection' },
  { type: 'sortCell', keyLabel: 'meaningOfCredit' },
  { type: 'sortCell', keyLabel: 'interbankCode' },
  { type: 'sortCell', keyLabel: 'keyword' }
];

export const interbankCodesColumns = [
  { type: 'sortCell', keyLabel: 'intitule' }
];

export const settingsWorksheetsKeys = [
  { type: 'checkbox', keyLabel: 'checkbox' },
  { type: 'sortCell', keyLabel: 'type' },
  { type: 'sortCell', keyLabel: 'accountNumber' },
  { type: 'sortCell', keyLabel: 'accountNumberTVA' }
];

// Reports and forms
export const reportsAndForms = [
  { type: 'checkbox', keyLabel: 'checkbox' },
  { type: 'sortCell', keyLabel: 'type' },
  { type: 'sortCell', keyLabel: 'name' },
  { type: 'string', keyLabel: 'description' },
  { type: 'sortCell', keyLabel: 'fiscalYear' },
  { type: 'string', keyLabel: 'enterprises' }
];
export const reportsAndFormsOrder = [
  { type: 'checkbox', keyLabel: 'checkbox' },
  { type: 'sortCell', keyLabel: 'orderNumber' },
  { type: 'sortCell', keyLabel: 'codeEDI' },
  { type: 'string', keyLabel: 'wording' },
  { type: 'sortCell', keyLabel: 'dataType' },
  { type: 'sortCell', keyLabel: 'viewing' },
  { type: 'sortCell', keyLabel: 'dataSource' },
  { type: 'string', keyLabel: 'references' }
];
export const RAFAlreadySelectedKeys = [
  { type: 'sortCell', keyLabel: 'stateOrForm' },
  { type: 'sortCell', keyLabel: 'labelAndNumber' }
];

export const lettersKeys = [
  { type: 'checkbox', keyLabel: '' },
  { type: 'sortCell', keyLabel: 'tables.letters.label' },
  { type: 'sortCell', keyLabel: 'tables.letters.createdAt' },
  { type: 'sortCell', keyLabel: 'tables.letters.type' },
  { type: 'sortCell', keyLabel: 'tables.letters.createdBy' },
  { type: 'sortCell', keyLabel: 'tables.letters.modifedBy' },
  { type: 'sortCell', keyLabel: 'tables.letters.status' },
  { type: 'sortCell', keyLabel: 'tables.letters.sendDate' }
];

export const paragraphsTypesKeys = [
  { type: 'checkbox', keyLabel: 'checkbox' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'resume' }
];

export const paragraphKeys = [
  { type: 'checkbox', keyLabel: 'checkbox' },
  { type: 'sortCell', keyLabel: 'functionName' },
  { type: 'sortCell', keyLabel: 'bodyOfText' }
];

export const AccountingFirmTopMainTableKeys = [
  { type: 'checkbox', keyLabel: 'checkbox' },
  { type: 'sortCell', keyLabel: 'siret' },
  { type: 'sortCell', keyLabel: 'nameOfFirm' },
  { type: 'sortCell', keyLabel: 'postalCode' },
  { type: 'sortCell', keyLabel: 'city' }
];

export const AccountingFirmDeclareTableKeys = [
  { type: 'checkbox', keyLabel: 'checkbox' },
  { type: 'sortCell', keyLabel: 'login' },
  { type: 'sortCell', keyLabel: 'password' },
  { type: 'sortCell', keyLabel: 'active' }
];
