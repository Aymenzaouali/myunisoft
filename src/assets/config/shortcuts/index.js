import { ShortcutManager } from 'react-shortcuts';
import keymap from './keymap';

const shortcutManager = new ShortcutManager(keymap);

export default shortcutManager;
