export default {
  NAV_BAR: {
    // NEW_TAB: 'shift+a',
    // CLOSE_TAB: 'shift+c',
    OPEN_NEW_ACCOUNTING: {
      osx: 'option+1',
      windows: 'alt+1'
    },
    CURRENT_EDITION: {
      osx: 'option+2',
      windows: 'alt+2'
    },
    CRM: {
      osx: 'option+3',
      windows: 'alt+3'
    },
    SEND_NEW_DOCUMENT: {
      osx: 'option+4',
      windows: 'alt+4'
    },
    GED: {
      osx: 'option+5',
      windows: 'alt+5'
    },
    CHAT: {
      osx: 'option+6',
      windows: 'alt+6'
    },
    NEW_TAB: {
      osx: 'option+o',
      windows: 'alt+o'
    },
    NEW_WINDOW: {
      osx: 'option+f',
      windows: 'alt+f'
    },
    PLAN: {
      osx: 'option+p',
      windows: 'alt+p'
    },
    ACCOUNT_SETTING: {
      osx: 'option+b',
      windows: 'alt+b'
    },
    CONSULT: {
      osx: 'option+c',
      windows: 'alt+c'
    },
    GEDS: {
      osx: 'option+g',
      windows: 'alt+g'
    },
    DISCUS: {
      osx: 'option+d',
      windows: 'alt+d'
    },
    ECRITURE: {
      osx: 'option+e',
      windows: 'alt+e'
    },
    CRMS: {
      osx: 'option+s',
      windows: 'alt+s'
    }
  },
  ACCOUNTING: {
    NEW_TAB: {
      osx: 'command+e',
      windows: 'ctrl+e'
    },
    NEW_WINDOW: {
      osx: 'command+f',
      windows: 'ctrl+f'
    },
    LAUNCH_CONSULTING: {
      osx: 'command+option+c',
      windows: 'ctrl+alt+c'
    }
  },
  BALANCE: {
    SPLIT_ACCOUNT: {
      osx: 'command+option+c',
      windows: 'ctrl+alt+c'
    }
  },
  GED: {
    LAUNCH_CONSULTING: {
      osx: 'command+option+c',
      windows: 'ctrl+alt+c'
    },
    SPLIT_ENTRY: {
      osx: 'command+option+e',
      windows: 'ctrl+alt+e'
    }
  },
  DIALOGS: {
    VALIDATE: {
      osx: 'command+enter',
      windows: 'ctrl+enter'
    },
    CLOSE: {
      osx: 'command+z',
      windows: 'ctrl+z'
    }
  },
  NEW_ACCOUNTING: {
    COPY_ENTRY: {
      osx: 'command+2',
      windows: 'ctrl+2'
    },
    AUTOFILL_OTHER_LINES: {
      osx: 'command+3',
      windows: 'ctrl+3'
    },
    SOLDE_ENTRY: {
      osx: 'command+4',
      windows: 'ctrl+4'
    },
    INTERCHANGE_DEBIT_CREDIT: {
      osx: 'command+5',
      windows: 'ctrl+5'
    },
    SHOW_ACCOUNTING_PLAN: {
      osx: 'command+8',
      windows: 'ctrl+8'
    },
    SHOW_ACCOUNT: {
      osx: 'command+9',
      windows: 'ctrl+9'
    },
    FOCUS_PREVIOUS_CELL: {
      osx: 'command+left',
      windows: 'ctrl+left'
    },
    FOCUS_NEXT_CELL: {
      osx: 'command+right',
      windows: 'ctrl+right'
    },
    CANCEL_ENTRY: {
      osx: 'command+z',
      windows: 'ctrl+z'
    },
    DELETE_ENTRY: {
      osx: 'command+minus',
      windows: 'ctrl+minus'
    },
    NEW_ACCOUNTING: {
      osx: 'command+plus',
      windows: 'ctrl+plus'
    },
    SUBMIT_ENTRY: {
      osx: 'command+enter',
      windows: 'ctrl+enter'
    },
    NEXT_ENTRY: {
      osx: 'command+down',
      windows: 'ctrl+down'
    },
    PREVIOUS_ENTRY: {
      osx: 'command+up',
      windows: 'ctrl+up'
    },
    ENTER: 'enter',
    TAB: 'tab',
    // MANUAL_ACCOUNTING: 'ctrl+plus+plus',
    EQUAL: '=',
    PLUS: '+',
    MINUS: '-',
    MULTIPLE: '*',
    DIVIDE: '/'
  },
  CONSULTING: {
    LETTER: {
      osx: 'command+1',
      windows: 'ctrl+1'
    },
    UNLETTER: {
      osx: 'command+2',
      windows: 'ctrl+2'
    },
    SHOW_ACCOUNT_LEFT: {
      osx: 'command+left',
      windows: 'ctrl+left'
    },
    SHOW_ACCOUNT_RIGHT: {
      osx: 'command+right',
      windows: 'ctrl+right'
    },
    SPLIT_ENTRY: {
      osx: 'command+option+e',
      windows: 'ctrl+alt+e'
    },
    FOCUS_NEXT_ENTRY: {
      osx: 'command+down',
      windows: 'ctrl+down'
    },
    FOCUS_PREVIOUS_ENTRY: {
      osx: 'command+up',
      windows: 'ctrl+up'
    }
    // LAUNCH_NEW_ACCOUNTING: 'ctrl+alt+s'  Demande de suppression par Cyril
    // SPLIT_SCREEN: 'ctrl+2',
  },
  DASHBOARD_SOCIETY: {
    // NEW_AWAITING_DOC: 'ctrl+2',
    // OCR_FLOW: 'alt+8',
    // BANKING_FLOW: 'alt+9'
  },
  DIALOG_CONFIRMATION: {
    ENTER: 'enter'
  }
};
