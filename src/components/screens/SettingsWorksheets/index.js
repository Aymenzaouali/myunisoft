import React from 'react';
import { SettingsWorksheetsHeaderContainer } from 'containers/groups/Headers';
import { SettingsWorksheetsTable } from 'containers/groups/Tables';

import styles from './settingsWorksheets.module.scss';

const SettingsWorksheets = () => (
  <div className={styles.container}>
    <SettingsWorksheetsHeaderContainer />
    <SettingsWorksheetsTable />
  </div>
);


export default SettingsWorksheets;
