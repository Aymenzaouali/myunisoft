import React from 'react';
import { PortfolioListViewHeader } from 'containers/groups/Headers';
import { PortfolioListViewTable } from 'containers/groups/Tables';
import styles from './portfolioListView.module.scss';

const PortfolioListView = () => (
  <div className={styles.container}>
    <PortfolioListViewHeader />
    <PortfolioListViewTable />
  </div>
);


export default PortfolioListView;
