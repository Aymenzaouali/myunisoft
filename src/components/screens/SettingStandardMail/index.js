import React, { useRef } from 'react';
import { SettingStandardMailTable, SettingStandardMailParagraphTable } from 'containers/groups/Tables';
import { SettingStandardMailHeader, MailParagraphTableHeader } from 'containers/groups/Headers/';
import PropTypes from 'prop-types';
import styles from './reportsAndFormsScreen.module.scss';

const SettingStandardMail = ({ isParagraphVisible, selectedRowsCount }) => {
  const rafRef = useRef(null);
  return (
    <>
      <div className={styles.container}>
        <SettingStandardMailHeader />
        <SettingStandardMailTable inputRef={rafRef} />
      </div>
      {
        isParagraphVisible && selectedRowsCount > 0 && (
          <div className={styles.container}>
            <MailParagraphTableHeader />
            <SettingStandardMailParagraphTable />
          </div>
        )
      }
    </>
  );
};


SettingStandardMail.propTypes = {
  isParagraphVisible: PropTypes.string.isRequired,
  selectedRowsCount: PropTypes.number
};

SettingStandardMail.defaultProps = {
  selectedRowsCount: 0
};


export default SettingStandardMail;
