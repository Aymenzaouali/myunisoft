import ChartAccount from 'containers/groups/Tables/ChartAccounts';
import { ChartAccountsFilterContainer } from 'containers/groups/Filters';
import { Route } from 'react-router';
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Typography
} from '@material-ui/core';
import I18n from 'assets/I18n';
import VATController from 'containers/controllers/VAT';
import styles from './chartAccounts.module.scss';

class ChartAccounts extends PureComponent {
  async componentDidMount() {
    const {
      getChartAccount
    } = this.props;

    await getChartAccount();
  }

  render() {
    return (
      <Fragment>
        <div className={styles.container}>
          <Typography variant="h1" gutterBottom>
            {I18n.t('ChartAccount.title')}
          </Typography>
          <ChartAccountsFilterContainer />
          <div className={styles.table}>
            <ChartAccount />
          </div>
        </div>
        <Route path="/tab/:id/chartAccount/tva" component={VATController} />
      </Fragment>
    );
  }
}


ChartAccounts.propTypes = {
  getChartAccount: PropTypes.func.isRequired
};

export default ChartAccounts;
