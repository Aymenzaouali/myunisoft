import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import FunctionTable from 'containers/groups/Tables/Function';
import { FunctionDialog } from 'containers/groups/Dialogs';
import I18n from 'assets/I18n';
import _ from 'lodash';
import DeleteConfirmation from 'containers/groups/Dialogs/DeleteConfirmation';
import styles from './function.module.scss';

const FunctionScreen = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isCreation, setIsCreation] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);
  const { disabledExport, csvData, baseFileName } = props;

  const updateGroupFunction = () => {
    const { getFunction } = props;
    getFunction();
  };

  useEffect(() => updateGroupFunction(), []);

  const onCloseDeleteDialog = () => setIsDeleteOpen(false);

  const createFunction = () => {
    setIsOpen(true);
    setIsCreation(true);
  };

  const modifyFunction = () => {
    const { initializeFunctionForm, selectedFunctionDetail } = props;
    setIsOpen(true);
    setIsCreation(false);
    initializeFunctionForm({
      libelle: selectedFunctionDetail.libelle
    });
  };

  const removeFunction = async () => {
    const { deleteFunction, list_selectedFunction, getFunction } = props;
    await deleteFunction(list_selectedFunction);
    await getFunction();
  };

  const closeDialog = () => {
    setIsOpen(false);
    setIsCreation(false);
  };

  const { list_selectedFunction, selectedFunction } = props;

  return (
    <div className={styles.functionContainer}>
      <div className={styles.functionHeader}>
        <Typography variant="h1">{I18n.t('function.title')}</Typography>
        <div />
      </div>
      <div className={styles.buttonsRow}>
        <InlineButton
          marginDirection="right"
          buttons={[
            {
              _type: 'string',
              text: I18n.t('function.newFunction'),
              color: 'primary',
              size: 'medium',
              onClick: createFunction
            },
            {
              _type: 'string',
              text: I18n.t('function.modifyFunction'),
              color: 'primary',
              size: 'medium',
              onClick: modifyFunction,
              disabled: !selectedFunction
            },
            {
              _type: 'string',
              text: I18n.t('function.deleteFunction'),
              color: 'error',
              size: 'medium',
              onClick: () => setIsDeleteOpen(true),
              disabled: _.isEmpty(list_selectedFunction)
            },
            {
              _type: 'component',
              color: 'primary',
              disabled: disabledExport,
              component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
            }
          ]}
        />
      </div>
      <div
        style={{
          overflow: 'auto',
          maxHeight: window.innerHeight - 172 - 20 // wH - header height - bottom padding
        }}
      >
        <FunctionTable />
      </div>
      <FunctionDialog
        isOpen={isOpen}
        creationMode={isCreation}
        onClose={closeDialog}
      />
      <DeleteConfirmation
        deleteReportAndForm={removeFunction}
        isOpen={isDeleteOpen}
        onClose={onCloseDeleteDialog}
        deleteButtonText={I18n.t('deleteDialog.group')}
      />
    </div>
  );
};

FunctionScreen.propTypes = {
  selectedFunction: PropTypes.number.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired,
  selectedFunctionDetail: PropTypes.shape().isRequired,
  list_selectedFunction: PropTypes.arrayOf(PropTypes.object).isRequired,
  initializeFunctionForm: PropTypes.func.isRequired,
  getFunction: PropTypes.func.isRequired,
  deleteFunction: PropTypes.func.isRequired
};

export default FunctionScreen;
