import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import CompaniesMonitoring from 'containers/groups/CompaniesMonitoring';
import styles from './DashboardCollab.module.scss';

const DashboardCollab = (props) => {
  const {
    loadData,
    isTrackingSocietiesLoading
  } = props;

  useEffect(() => {
    loadData();
  }, []);

  return (
    <div className={styles.container}>
      <CompaniesMonitoring
        isTrackingSocietiesLoading={isTrackingSocietiesLoading}
      />
    </div>
  );
};
// Props
DashboardCollab.propTypes = {
  loadData: PropTypes.func.isRequired,
  isTrackingSocietiesLoading: PropTypes.bool.isRequired
};

DashboardCollab.defaultProps = {};

export default DashboardCollab;
