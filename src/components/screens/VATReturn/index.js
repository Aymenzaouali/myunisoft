import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { Tabs, Tab } from '@material-ui/core';

import I18n from 'assets/I18n';

import { useWidth } from 'helpers/hooks';
import { ScrollTopButton } from 'components/basics/Buttons';

import { VATHeader } from 'containers/groups/Headers';
import TIDENTIF from 'containers/groups/Forms/VAT/TIDENTIF';
import TVACA3 from 'containers/groups/Forms/VAT/TVACA3';
import TVA3310 from 'containers/groups/Forms/VAT/TVA3310';
import TVA3519 from 'containers/groups/Forms/VAT/TVA3519';
import DisableElement from 'components/basics/DisableElement';
// import { tvaValidation } from 'helpers/pdfFormsValidation';

import styles from './VATReturn.module.scss';

// Component
const VAT = (props) => {
  const {
    selectForm,
    sendVat,
    selectedFormId,
    isNothingness
    // societyId,
    // tvaFormValues,
    // setAllLinesErrors
  } = props;
  const [selectedTab, setSelectedTab] = useState('tvaca3');
  const [factor, setFactor] = useState(1);
  const scrollContainer = useRef(null);

  useEffect(() => {
    setSelectedTab(selectedFormId);
  }, [selectedFormId]);

  // Functions
  const handleChange = async (e, value) => {
    try {
      // TODO enable validation when it would be requested
      // tvaValidation(tvaFormValues, selectedFormId);
      await sendVat();
      await selectForm(value);
    } catch (errors) {
      // setAllLinesErrors(`${societyId}${selectedFormId}`, errors);
    }
  };

  const factorCompute = (width) => {
    if (width < 1200) {
      setFactor(width / 1200);
    } else {
      setFactor(1);
    }
  };

  // Callbacks
  const containerCb = useWidth(factorCompute);

  // Rendering
  const pdfStyle = {};
  if (factor !== 1) {
    pdfStyle.transform = `scale(${factor})`;
  }

  return (
    <div ref={scrollContainer} className={styles.container}>
      <div className={styles.vatHeaderWrap}>
        <VATHeader />
        <Tabs
          value={selectedTab}
          onChange={handleChange}

          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"

          classes={{
            scrollButtons: styles.scrollButtons,
            root: styles.tabs
          }}
        >
          <Tab label={I18n.t('tva.tvaca3')} value="tvaca3" disabled={isNothingness} />
          <Tab label={I18n.t('tva.tva3310')} value="tva3310" disabled={isNothingness} />
          <Tab label={I18n.t('tva.tidentif')} value="tidentif" disabled={isNothingness} />
          <Tab label={I18n.t('tva.tva3519')} value="tva3519" disabled={isNothingness} />
        </Tabs>
      </div>
      <div ref={containerCb} className={styles.pdfContainer}>
        <div className={styles.pdf} style={pdfStyle}>
          {
            isNothingness && <DisableElement title={I18n.t('tva.switchNothingness')} />
          }
          {selectedTab === 'tvaca3' && <TVACA3 />}
          {selectedTab === 'tva3310' && <TVA3310 />}
          {selectedTab === 'tidentif' && <TIDENTIF />}
          {selectedTab === 'tva3519' && <TVA3519 />}
        </div>
      </div>
      <ScrollTopButton
        containerRef={scrollContainer}
      />
    </div>
  );
};

VAT.propTypes = {
  selectForm: PropTypes.func.isRequired,
  sendVat: PropTypes.func.isRequired,
  selectedFormId: PropTypes.string.isRequired,
  isNothingness: PropTypes.bool.isRequired
  // setAllLinesErrors: PropTypes.func.isRequired,
  // societyId: PropTypes.string.isRequired,
  // tvaFormValues: PropTypes.shape([])
};

// VAT.defaultProps = {
//   tvaFormValues: []
// };

export default VAT;
