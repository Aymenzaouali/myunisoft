import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { SuccessfulDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import CollectorTable from 'containers/groups/Tables/Collector';
import Button from 'components/basics/Buttons/Button';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import { getBudgetInsightUrl, getBudgetInsightModif } from 'helpers/url';
import styles from './collector.module.scss';

const Collector = (props) => {
  const {
    societyId,
    selectedDocuments,
    selectedDocumentsCount,
    getAccounting,
    getBudgetInsightRedirection,
    disabledExport,
    csvData,
    baseFileName
  } = props;

  const [open, setOpen] = useState(false);

  const onHeaderUpButtonClick = ({ societyId, getBudgetInsightRedirection }) => async () => {
    const token = await getBudgetInsightRedirection(societyId);
    window.open(getBudgetInsightUrl(token, societyId));
  };

  const onModifClickButton = ({ societyId, getBudgetInsightRedirection }) => async () => {
    const token = await getBudgetInsightRedirection(societyId);
    window.open(getBudgetInsightModif(token, societyId));
  };

  const sendSelectedClick = ({ selectedDocuments }) => async () => {
    const { data = {} } = await getAccounting(Object.keys(selectedDocuments)
      .reduce((acc, key) => (selectedDocuments[key] ? [...acc, parseInt(key, 10)] : acc), []));
    const { status } = data;
    if (status === 'success') { setOpen(true); }
  };

  return (
    <div className={styles.container}>
      <div className={styles.headerTitle}>
        <Typography variant="h1">
          {I18n.t('collector.title')}
        </Typography>
      </div>
      <div className={styles.headerUpButton}>
        <InlineButton
          marginDirection="left"
          buttons={[
            {
              _type: 'string',
              text: I18n.t('collector.buttonAddProvider'),
              size: 'large',
              onClick: onHeaderUpButtonClick({ societyId, getBudgetInsightRedirection })
            },
            {
              _type: 'string',
              text: I18n.t('collector.buttonModifProvider'),
              size: 'large',
              onClick: onModifClickButton({ societyId, getBudgetInsightRedirection })
            },
            {
              _type: 'component',
              color: 'primary',
              size: 'large',
              disabled: disabledExport,
              component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
            }
          ]}
        />
      </div>
      <div className={styles.table}>
        <Typography variant="h6">
          {I18n.t('collector.subtitle')}
        </Typography>
        <CollectorTable />
        <div className={styles.bottomTableButton}>
          <Button
            size="large"
            variant="contained"
            color="primary"
            onClick={sendSelectedClick({ selectedDocuments })}
            disabled={!selectedDocumentsCount}
          >
            {I18n.t('collector.buttonSelection')}
          </Button>
        </div>
      </div>
      <SuccessfulDialog
        isOpen={open}
        onClose={() => setOpen(false)}
        message={I18n.t('collector.success')}
      />
    </div>
  );
};

Collector.propTypes = {
  getBudgetInsightRedirection: PropTypes.func.isRequired,
  societyId: PropTypes.string.isRequired,
  selectedDocuments: PropTypes.shape({}).isRequired,
  selectedDocumentsCount: PropTypes.number.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired,
  getAccounting: PropTypes.func.isRequired
};

export default Collector;
