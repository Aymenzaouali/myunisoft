import React, { useState } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Field, Form } from 'redux-form';
import { Typography } from '@material-ui/core';
import I18n from 'assets/I18n';
import { ReactComponent as Logo } from 'assets/images/logo.svg';
import { lowercaseFormat } from 'helpers/format';
// import { emailValidation } from 'helpers/validation';
import Button from 'components/basics/Buttons/Button';
import { ReduxTextField } from 'components/reduxForm/Inputs';

import styles from 'components/screens/PasswordForgot/PasswordForgot.module.scss';

// Component
const PasswordForgot = (props) => {
  const {
    push,
    handleSubmit,
    onSubmit
  } = props;

  const [isSent, setIsSent] = useState(false);

  const submit = async (data) => {
    const redirectUrl = `${window.location.origin}/reset`;
    onSubmit(data, redirectUrl).then(() => {
      setIsSent(true);
    });
  };

  // push to the main page
  const onCancel = () => push('/');

  // Rendering
  return (
    <div className={styles.container}>
      <div className={classNames(styles.half, styles.logo)}>
        <Logo fill="white" height="100px" />
      </div>
      <div className={classNames(styles.half, styles.form)}>
        <Typography variant="h1">{ I18n.t('passwordRecovery.title') }</Typography>
        {!isSent ? (
          <Form onSubmit={handleSubmit(submit)}>
            <div className={styles.inputs}>
              <Field
                name="mail"
                component={ReduxTextField}
                format={lowercaseFormat}
                id="mail"
                label={I18n.t('login.mail')}
                fullWidth
              />
              <Typography variant="subtitle1" className={styles.helper}>{I18n.t('passwordRecovery.helper')}</Typography>
              <div className={styles.buttons}>
                <Button
                  size="large"
                  variant="outlined"
                  onClick={onCancel}
                  color="default"
                  className={styles.cancel}
                  classes={{ sizeLarge: styles.submitBtn }}
                >
                  { I18n.t('passwordRecovery.cancel') }
                </Button>
                <Button type="submit" size="large" variant="contained" color="primary" classes={{ sizeLarge: styles.submitBtn }}>
                  { I18n.t('passwordRecovery.validate') }
                </Button>
              </div>
            </div>
          </Form>
        ) : (
          <div className={styles.inputs}>
            <Typography variant="subtitle1" className={styles.helper}>{I18n.t('passwordRecovery.emailCheckerResponse.success')}</Typography>
          </div>
        )}
      </div>
    </div>
  );
};

// Props
PasswordForgot.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
};

export default PasswordForgot;
