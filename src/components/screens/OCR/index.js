import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Typography, Grid, Paper } from '@material-ui/core';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import AutoComplete from 'components/basics/Inputs/AutoComplete';
import FilesDropper from 'components/groups/FilesDropper';
import { routesByKey } from 'helpers/routes';
import FilesInformations from 'components/groups/FilesInformations';
import Regrouping from 'containers/groups/Regrouping';
import './styles.scss';
import Loader from 'components/basics/Loader';
import { blobToDataURL } from 'blob-util';
import { getImageDimensionsFromUrl, convertPDFtoImages } from 'helpers/file';

/**
 * Component screen OCR, files manager
 */
class OCR extends Component {
  state = {
    selectedSociety: null
  }

  getDropzoneConfig = this.getDropzoneConfig.bind(this);

  componentDidMount() {
    const { getSociety } = this.props;
    getSociety();
  }

  componentWillUnmount() {
    const { onUnmount } = this.props;
    if (onUnmount) {
      onUnmount();
    }
  }

  /**
   * Dropzone config generator
   * @param {String} type - purchases, bill
   */
  getDropzoneConfig({ type, format }) {
    const { addDocuments, society, setLoading } = this.props;
    return ({
      disableClick: true,
      onDrop: async (droppedFiles) => {
        setLoading(true);

        const splittedFiles = await Promise.all(
          droppedFiles.map(async (file) => {
            if (file.type === 'application/pdf' && format === 2) {
              return convertPDFtoImages(file);
            }

            if (file.type.includes('image') && format === 2) {
              const dataUrlImage = await blobToDataURL(file);
              const img = await getImageDimensionsFromUrl(dataUrlImage);

              const { width, height } = img;

              file.width = width; // eslint-disable-line
              file.height = height; // eslint-disable-line
            }

            return file;
          })
        );
        const files = [].concat(...splittedFiles);

        addDocuments({
          files,
          type,
          format,
          society
        });

        setLoading(false);
      }
    });
  }

  /**
   * Handle 'change' event from company autocomplete
   * @param {Event} ev | get societyId from target value
   */
  handleSocietySelectOnChange = (society) => {
    const { setCollabSociety, societies } = this.props;
    this.setState({ selectedSociety: society });
    setCollabSociety(societies.find(s => s.value === society.value));
  }

  render() {
    const { selectedSociety } = this.state;
    const {
      societies, files,
      deleteDocument, sendDocuments,
      isCollab,
      push,
      idSelectedSociety,
      isLoading
    } = this.props;

    const multiFiles = files.filter(files => files.format === 2);

    return (
      <div className="ocr">
        <Grid container component={Paper} className="ocr__header__container">
          <Grid item xs={12} className="ocr__header">
            <Typography variant="h1" gutterBottom>
              {I18n.t('OCR.title')}
            </Typography>
            <Button variant="contained" color="primary" onClick={() => push(routesByKey.ocrTracking)}>
              {I18n.t('OCR.buttonOCRMonitoring')}
            </Button>
          </Grid>
          {isCollab && (
            <Grid item xs={3}>
              <AutoComplete
                placeholder={I18n.t('OCR.selectSociety')}
                textFieldProps={{
                  InputLabelProps: {
                    shrink: true
                  }
                }}
                value={selectedSociety}
                onChangeValues={this.handleSocietySelectOnChange}
                options={societies}
              />
            </Grid>
          )}
        </Grid>

        {(selectedSociety || idSelectedSociety !== -2) && (
          <Grid container className="ocr__filesdropper__container" component={Paper}>
            <Grid item xs={3} className="ocr__filesdropper__container__item">
              <FilesDropper
                title="OCR.filesDropper.purchases"
                panes={[
                  { dropzoneConfig: this.getDropzoneConfig({ type: 'purchases', format: 1 }), icon: 'upload', title: 'OCR.filesDropper.dropDocumentsMono' },
                  { dropzoneConfig: this.getDropzoneConfig({ type: 'purchases', format: 2 }), icon: 'upload', title: 'OCR.filesDropper.dropDocumentsMulti' }
                ]}
              />
            </Grid>
            <Grid item xs={3} className="ocr__filesdropper__container__item">
              <FilesDropper
                title="OCR.filesDropper.expenseReports"
                panes={[
                  { dropzoneConfig: this.getDropzoneConfig({ type: 'expenseReports', format: 1 }), icon: 'upload', title: 'OCR.filesDropper.dropDocumentsMono' },
                  { dropzoneConfig: this.getDropzoneConfig({ type: 'expenseReports', format: 2 }), icon: 'upload', title: 'OCR.filesDropper.dropDocumentsMulti' }
                ]}
              />
            </Grid>
            <Grid item xs={3} className="ocr__filesdropper__container__item">
              <FilesDropper
                title="OCR.filesDropper.sales"
                panes={[
                  { dropzoneConfig: this.getDropzoneConfig({ type: 'sales', format: 1 }), icon: 'upload', title: 'OCR.filesDropper.dropDocumentsMono' },
                  { dropzoneConfig: this.getDropzoneConfig({ type: 'sales', format: 2 }), icon: 'upload', title: 'OCR.filesDropper.dropDocumentsMulti' }
                ]}
              />
            </Grid>
            <Grid item xs={3} className="ocr__filesdropper__container__item">
              <FilesDropper
                title="OCR.filesDropper.holdings"
                panes={[
                  { dropzoneConfig: this.getDropzoneConfig({ type: 'holdings', format: 1 }), icon: 'upload', title: 'OCR.filesDropper.dropDocumentsMono' },
                  { dropzoneConfig: this.getDropzoneConfig({ type: 'holdings', format: 2 }), icon: 'upload', title: 'OCR.filesDropper.dropDocumentsMulti' }
                ]}
              />
            </Grid>
          </Grid>
        )}

        { isLoading ? (
          <Loader />
        ) : files.length > 0 && (
        <>
          {multiFiles.length > 0
              && (
                <Grid container className="ocr__files-informations" component={Paper}>
                  <Grid item xs={12}>
                    <Typography variant="h1" gutterBottom>
                      {I18n.t('OCR.regrouping.title')}
                    </Typography>
                    <Regrouping files={multiFiles} showPagination={false} />
                  </Grid>
                </Grid>
              )}
          <Grid container className="ocr__files-informations" component={Paper}>
            <Grid item xs={12}>
              <FilesInformations
                files={files.map(({ documents }) => {
                  const [doc] = documents;
                  const name = documents.length > 1
                    ? doc.name.replace(/\.[^.]+$/, '.pdf')
                    : doc.name;

                  return { ...doc, name };
                })}
                handleDeleteFile={deleteDocument}
                handleSendFiles={sendDocuments}
                isCollab={isCollab}
                ocrMode
              />
            </Grid>
          </Grid>
        </>
        ) }

      </div>
    );
  }
}

OCR.defaultProps = {
  match: {},
  society: {},
  idSelectedSociety: -2,
  onUnmount: undefined
};

OCR.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      document_id: PropTypes.string
    })
  }),
  addDocuments: PropTypes.func.isRequired,
  deleteDocument: PropTypes.func.isRequired,
  sendDocuments: PropTypes.func.isRequired,
  setCollabSociety: PropTypes.func.isRequired,
  societies: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  files: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  society: PropTypes.shape({}),
  isCollab: PropTypes.bool.isRequired,
  push: PropTypes.func.isRequired,
  getSociety: PropTypes.func.isRequired,
  idSelectedSociety: PropTypes.number,
  onUnmount: PropTypes.func,
  setLoading: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default OCR;
