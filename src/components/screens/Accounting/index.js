import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router';

import I18n from 'assets/I18n';
import Loader from 'components/basics/Loader';

import NewAccountingScreen from 'containers/controllers/NewAccounting';

import Consulting from 'containers/groups/Consulting';
import AccountingForSplit from 'components/groups/AccountingForSplit';
import Split from 'containers/groups/Split';

// Component
const AccountingScreen = (props) => {
  const {
    societyId,
    location,
    exercicesAreLoading
  } = props;

  const slaves = {
    consulting: {
      label: I18n.t('split.consulting'),
      url: `/tab/${societyId}/consulting`,
      component: () => <Consulting />,
      swapKeepArgs: true,
      args: {
        isNotEcriture: false
      }
    }
  };

  return (
    <>
      {exercicesAreLoading
        ? <Loader />
        : <Route path="/tab/:id/(slave/)?accounting/new" component={NewAccountingScreen} />
      }
      <Split master_id="accounting" slaves={slaves}>
        <AccountingForSplit location={location} />
      </Split>
    </>
  );
};

// Props
AccountingScreen.propTypes = {
  societyId: PropTypes.number.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  exercicesAreLoading: PropTypes.bool.isRequired
};

export default AccountingScreen;
