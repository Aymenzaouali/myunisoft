import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Field, Form } from 'redux-form';
import { Typography } from '@material-ui/core';
import I18n from 'assets/I18n';
import { ReactComponent as Logo } from 'assets/images/logo.svg';
import { routesByKey } from 'helpers/routes';
import { passwordMatch, required } from 'helpers/validation';
import Button from 'components/basics/Buttons/Button';
import { ReduxSecureTextField } from 'components/reduxForm/Inputs';

import { Notifications } from 'common/helpers/SnackBarNotifications';
import styles from './PasswordReset.module.scss';

// Component
const PasswordReset = (props) => {
  const [userEmail, setUserEmail] = useState('');
  const [token, setToken] = useState('');

  const {
    push,
    handleSubmit,
    error,
    onSubmit,
    isActiveSession,
    enqueueSnackbar,
    location
  } = props;

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    setUserEmail(params.get('email'));
    setToken(params.get('token'));
  });

  useEffect(() => {
    if (token == null || userEmail == null) {
      push('/');
    }
  }, [userEmail, token]);

  // Effects
  useEffect(() => {
    if (isActiveSession) {
      push(routesByKey.dashboardCollab);
    }
  }, []);

  // Functions
  const submit = async (data) => {
    const password = data.newPassword;
    onSubmit(userEmail, token, password).then(() => {
      enqueueSnackbar(Notifications(I18n.t('passwordReset.passwordReseted.success'),
        'error'));
      push('/');
    }).catch(() => {
      enqueueSnackbar(Notifications(I18n.t('passwordReset.passwordReseted.failed'),
        'error'));
    });
  };

  // Rendering
  return (
    <div className={styles.container}>
      <div className={classNames(styles.half, styles.logo)}>
        <Logo fill="white" height="100px" />
      </div>
      <div className={classNames(styles.half, styles.form)}>
        <Typography variant="h1">{ I18n.t('passwordReset.title') }</Typography>
        <Form
          onSubmit={handleSubmit(submit)}
        >
          <div className={styles.inputs}>
            <Field
              name="newPassword"
              component={ReduxSecureTextField}
              id="password"
              error={!!error}
              label={I18n.t('passwordReset.newPassword')}
              fullWidth
            />
            <Field
              name="confirmedPassword"
              component={ReduxSecureTextField}
              id="password"
              validate={[required, passwordMatch]}
              error={!!error}
              label={I18n.t('passwordReset.confirmedPassword')}
              fullWidth
            />
            <Button
              type="submit"
              // onClick={event=>onClick(event)}
              size="large"
              variant="contained"
              color="primary"
              classes={{ sizeLarge: styles.submitBtn }}
            >
              { I18n.t('passwordReset.validate') }
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
};

// Props
PasswordReset.propTypes = {
  error: PropTypes.string,
  isActiveSession: PropTypes.bool,
  push: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  enqueueSnackbar: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired
};

PasswordReset.defaultProps = {
  error: '',
  isActiveSession: false
};

export default PasswordReset;
