import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { Typography } from '@material-ui/core';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import { VATTableContainer } from 'containers/groups/Tables';
import DeleteConfirmation from 'containers/groups/Dialogs/DeleteConfirmation';
import { VATDialog } from 'containers/groups/Dialogs';
import styles from './VATScreen.module.scss';

class VATScreen extends PureComponent {
  state = {
    isDialogOpen: false,
    modify: false,
    deleteConfirmationDialog: false

  };

  openDialog = (modify, select) => {
    const {
      initializeForm,
      resetVatForm
    } = this.props;

    if (modify) {
      initializeForm(select);
    } else {
      resetVatForm();
    }

    this.setState({
      isDialogOpen: true,
      modify
    });
  }

  closeDialog = () => {
    this.setState({
      isDialogOpen: false,
      modify: false
    });
  }

  render() {
    const {
      selectList,
      vatSelectedLine,
      vat,
      disabledExport,
      csvData,
      baseFileName,
      onDeleteVAT
    } = this.props;

    const {
      isDialogOpen,
      modify,
      deleteConfirmationDialog
    } = this.state;

    const select = vat.filter(item => vatSelectedLine === item.vat_param_id).map(
      item => ({
        account_coll: item.account_coll,
        account_ded: item.account_ded,
        exigility: item.vat_exigility,
        type: item.vat_type,
        taux: item.vat,
        code: item.code,
        vat_param_id: vatSelectedLine
      })
    )[0];

    const disabledModify = vatSelectedLine === null;
    const disabledDell = selectList.length === 0;
    return (
      <div className={styles.container}>
        <div className={styles.header}>
          <Typography variant="h1" gutterBottom>
            {I18n.t('tva.list')}
          </Typography>
          <div>
            <InlineButton buttons={[
              {
                _type: 'string',
                text: I18n.t('tva.new'),
                onClick: () => this.openDialog(false, {})
              },
              {
                _type: 'string',
                text: I18n.t('tva.modify'),
                disabled: disabledModify,
                onClick: () => this.openDialog(true, select)
              },
              {
                _type: 'string',
                text: I18n.t('tva.delete'),
                color: 'error',
                disabled: disabledDell,
                onClick: () => {
                  this.setState({ deleteConfirmationDialog: true });
                }
              },
              {
                _type: 'component',
                color: 'primary',
                disabled: disabledExport,
                component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
              }
            ]}
            />
          </div>
        </div>
        <div>
          <VATTableContainer />
        </div>
        <DeleteConfirmation
          deleteReportAndForm={onDeleteVAT}
          isOpen={deleteConfirmationDialog}
          onClose={() => this.setState({ deleteConfirmationDialog: false })}
        />
        {
          isDialogOpen && (
            <VATDialog
              isOpen={isDialogOpen}
              onClose={this.closeDialog}
              values={select}
              modify={modify}
            />
          )
        }

      </div>
    );
  }
}

VATScreen.defaultProps = {
  vatSelectedLine: null
};

VATScreen.propTypes = {
  onDeleteVAT: PropTypes.func.isRequired,
  initializeForm: PropTypes.func.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired,
  resetVatForm: PropTypes.func.isRequired,
  selectList: PropTypes.arrayOf(
    PropTypes.number
  ).isRequired,
  vat: PropTypes.arrayOf(
    PropTypes.shape({})
  ).isRequired,
  vatSelectedLine: PropTypes.number
};

export default VATScreen;
