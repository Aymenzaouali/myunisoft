import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { Tabs, Tab } from '@material-ui/core';
import { ScrollTopButton } from 'components/basics/Buttons';

import get from 'lodash/get';

import { useWidth } from 'helpers/hooks';

import { TaxDeclarationHeader } from 'containers/groups/Headers';
import Tax2033A from 'containers/groups/Forms/TaxDeclaration/Tax2033A';
import Tax2033B from 'containers/groups/Forms/TaxDeclaration/Tax2033B';
import Tax2033C from 'containers/groups/Forms/TaxDeclaration/Tax2033C';
import Tax2033D from 'containers/groups/Forms/TaxDeclaration/Tax2033D';
import Tax2033E from 'containers/groups/Forms/TaxDeclaration/Tax2033E';
import Tax2033F from 'containers/groups/Forms/TaxDeclaration/Tax2033F';
import Tax2033G from 'containers/groups/Forms/TaxDeclaration/Tax2033G';
import Tax2035E from 'containers/groups/Forms/TaxDeclaration/Tax2035E';
import Tax2050 from 'containers/groups/Forms/TaxDeclaration/Tax2050';
import Tax2051 from 'containers/groups/Forms/TaxDeclaration/Tax2051';
import Tax2052 from 'containers/groups/Forms/TaxDeclaration/Tax2052';
import Tax2053 from 'containers/groups/Forms/TaxDeclaration/Tax2053';
import Tax2054 from 'containers/groups/Forms/TaxDeclaration/Tax2054';
import Tax2055 from 'containers/groups/Forms/TaxDeclaration/Tax2055';
import Tax2056 from 'containers/groups/Forms/TaxDeclaration/Tax2056';
import Tax2057 from 'containers/groups/Forms/TaxDeclaration/Tax2057';
import Tax2059B from 'containers/groups/Forms/TaxDeclaration/Tax2059B';
import Tax2069A from 'containers/groups/Forms/TaxDeclaration/Tax2069A';
import Tax2059C from 'containers/groups/Forms/TaxDeclaration/Tax2059C';
import Tax2059D from 'containers/groups/Forms/TaxDeclaration/Tax2059D';
import Tax2059E from 'containers/groups/Forms/TaxDeclaration/Tax2059E';
import Tax2059F from 'containers/groups/Forms/TaxDeclaration/Tax2059F';
import Tax2059G from 'containers/groups/Forms/TaxDeclaration/Tax2059G';
// TODO Will be available after 26/03/2020
// import Tax2035 from 'containers/groups/Forms/TaxDeclaration/Tax2035';
// import Tax2035SUITE from 'containers/groups/Forms/TaxDeclaration/Tax2035SUITE';
import Tax2035A from 'containers/groups/Forms/TaxDeclaration/Tax2035A';
import Tax2035B from 'containers/groups/Forms/TaxDeclaration/Tax2035B';
import Tax2035F from 'containers/groups/Forms/TaxDeclaration/Tax2035F';
import Tax2065BIS from 'containers/groups/Forms/TaxDeclaration/Tax2065BIS';
import Tax2035G from 'containers/groups/Forms/TaxDeclaration/Tax2035G';
import Tax2058A from 'containers/groups/Forms/TaxDeclaration/Tax2058A';
import Tax2058B from 'containers/groups/Forms/TaxDeclaration/Tax2058B';
import Tax2058C from 'containers/groups/Forms/TaxDeclaration/Tax2058C';
import Tax2059A from 'containers/groups/Forms/TaxDeclaration/Tax2059A';
import Tax2065 from 'containers/groups/Forms/TaxDeclaration/Tax2065';
import Tax2054BIS from 'containers/groups/Forms/TaxDeclaration/Tax2054BIS';

import styles from './TaxDeclaration.module.scss';

// List of exist implemented PDF
const existDocs = {
  '2033A': {
    component: <Tax2033A />,
    label: '2033-A'
  },
  '2033B': {
    component: <Tax2033B />,
    label: '2033-B'
  },
  '2033C': {
    component: <Tax2033C />,
    label: '2033-C'
  },
  '2033D': {
    component: <Tax2033D />,
    label: '2033-D'
  },
  '2033E': {
    component: <Tax2033E />,
    label: '2033-E'
  },
  '2033F': {
    component: <Tax2033F />,
    label: '2033-F'
  },
  '2033G': {
    component: <Tax2033G />,
    label: '2033-G'
  },
  '2035E': {
    component: <Tax2035E />,
    label: '2035-E'
  },
  2050: {
    component: <Tax2050 />,
    label: '2050'
  },
  2051: {
    component: <Tax2051 />,
    label: '2051'
  },
  2052: {
    component: <Tax2052 />,
    label: '2052'
  },
  2053: {
    component: <Tax2053 />,
    label: '2053'
  },
  2054: {
    component: <Tax2054 />,
    label: '2054'
  },
  2055: {
    component: <Tax2055 />,
    label: '2055'
  },
  2056: {
    component: <Tax2056 />,
    label: '2056'
  },
  2057: {
    component: <Tax2057 />,
    label: '2057'
  },
  '2059B': {
    component: <Tax2059B />,
    label: '2059-B'
  },
  '2069A': {
    component: <Tax2069A />,
    label: '2069-A'
  },
  '2059C': {
    component: <Tax2059C />,
    label: '2059-C'
  },
  '2059D': {
    component: <Tax2059D />,
    label: '2059-D'
  },
  '2059E': {
    component: <Tax2059E />,
    label: '2059-E'
  },
  '2059F': {
    component: <Tax2059F />,
    label: '2059-F'
  },
  '2059G': {
    component: <Tax2059G />,
    label: '2059-G'
  },
  // TODO Will be available after 26/03/2020
  // 2035: {
  //   component: <Tax2035 />,
  //   label: '2035'
  // },
  // '2035SUITE': {
  //   component: <Tax2035SUITE />,
  //   label: '2035-SUITE'
  // },
  '2035A': {
    component: <Tax2035A />,
    label: '2035-A'
  },
  '2035B': {
    component: <Tax2035B />,
    label: '2035-B'
  },
  '2035F': {
    component: <Tax2035F />,
    label: '2035-F'
  },
  '2065BIS': {
    component: <Tax2065BIS />,
    label: '2065BIS'
  },
  '2035G': {
    component: <Tax2035G />,
    label: '2035-G'
  },
  '2058A': {
    component: <Tax2058A />,
    label: '2058-A'
  },
  '2058B': {
    component: <Tax2058B />,
    label: '2058-B'
  },
  '2058C': {
    component: <Tax2058C />,
    label: '2058-C'
  },
  '2059A': {
    component: <Tax2059A />,
    label: '2059-A'
  },
  2065: {
    component: <Tax2065 />,
    label: '2065'
  },
  '2054BIS': {
    component: <Tax2054BIS />,
    label: '2054BIS'
  }
};

// Component
const TaxDeclarationScreen = ({
  getSheetOfDocs, sheet, selectForm
}) => {
  const docs = get(sheet, 'lst_sheet', []);
  const docsList = docs.map(doc => doc.code);

  // State
  const [selectedTab, setSelectedTab] = useState('');
  const [factor, setFactor] = useState(1);

  const scrollContainer = useRef(null);

  useEffect(() => {
    getSheetOfDocs();
  }, []);

  useEffect(() => {
    if (sheet) {
      const selectedTab = get(sheet, 'lst_sheet[0].code', null);
      selectForm(selectedTab);
      setSelectedTab(selectedTab);
    }
  }, [sheet]);

  // Functions
  const handleChange = (e, value) => {
    setSelectedTab(value);
    selectForm(value);
  };

  useEffect(() => handleChange(null, get(sheet, 'lst_sheet[0].code')), [sheet]);

  const factorCompute = (width) => {
    if (width < 1200) {
      setFactor(width / 1200);
    } else {
      setFactor(1);
    }
  };

  // Callbacks
  const containerCb = useWidth(factorCompute);

  // Rendering
  const pdfStyle = {};
  if (factor !== 1) {
    pdfStyle.transform = `scale(${factor})`;
  }

  const component = get(existDocs[selectedTab], 'component');

  return (
    <div ref={scrollContainer} className={styles.container}>
      <div className={styles.taxDeclarationHeaderWrap}>
        <TaxDeclarationHeader />
        <Tabs
          value={selectedTab}
          onChange={handleChange}

          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          classes={{
            root: styles.tabs
          }}
        >
          {
            docsList.map((doc, key) => {
              const component = get(existDocs, `[${doc}]`, null);
              if (component) {
                return <Tab className={styles.tab} key={`${key}-${component.label}`} label={component.label} value={doc} />;
              }
              return null;
            })
          }
        </Tabs>
      </div>
      <div ref={containerCb} className={styles.pdfContainer}>
        <div className={styles.pdf} style={pdfStyle}>
          {component}
        </div>
      </div>
      <ScrollTopButton
        containerRef={scrollContainer}
      />
    </div>
  );
};

TaxDeclarationScreen.defaultProps = {
  getSheetOfDocs: () => {},
  selectForm: () => {},
  sheet: []
};

TaxDeclarationScreen.propTypes = {
  getSheetOfDocs: PropTypes.func,
  selectForm: PropTypes.func,
  sheet: PropTypes.array
};

export default TaxDeclarationScreen;
