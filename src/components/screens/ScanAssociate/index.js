import React from 'react';

import ScanAssociate from 'containers/groups/ScanAssociate/';
import styles from './scanAssociate.module.scss';

const ScanAssociateScrean = () => (
  <div className={styles.container}>
    <ScanAssociate />
  </div>
);

export default ScanAssociateScrean;
