import React, { useEffect } from 'react';
import { Shortcuts } from 'react-shortcuts';
import PropTypes from 'prop-types';
import { Grid, Typography } from '@material-ui/core';

import I18n from 'assets/I18n';
import { routesByKey } from 'helpers/routes';

import {
  GlobalSituation,
  Supplier,
  Client,
  Treasury,
  PercentageGlobalSituation,
  PercentageTreasury,
  PercentageClient,
  PercentageSupplier
} from 'containers/groups/cards/ChartCard';
import Memo from 'containers/groups/cards/MemoCard';

import Progression from 'containers/groups/Progression';

import styles from './Dasboard.module.scss';

// Component
function Dashboard(props) {
  const {
    redirect, societyId, redirectFromCollab,
    getCharts,
    getRights
  } = props;

  // Functions
  const handleShortcuts = (action) => {
    switch (action) {
    case 'NEW_AWAITING_DOC':
      redirectFromCollab(undefined, 'o', societyId);
      break;
    case 'OCR_FLOW':
      redirect(routesByKey.ocrTracking);
      break;
    case 'BANKING_FLOW':
      redirectFromCollab(undefined, 'ib', societyId);
      break;
    default:
      break;
    }
    return null;
  };

  // Effects
  useEffect(() => {
    getCharts(societyId);
    getRights();
  }, []);

  // Rendering
  return (
    <Shortcuts
      name="DASHBOARD_SOCIETY"
      handler={handleShortcuts}
      alwaysFireHandler
      stopPropagation={false}
      preventDefault={false}
      targetNodeSelector="body"
    >
      <div className={styles.container}>
        <Grid className={styles.row} container>
          <Grid item xs>
            <Progression />
          </Grid>
        </Grid>

        <Grid className={styles.row} container>
          <Grid item xs={12} md={6} lg={4} xl={3}>
            <Memo />
          </Grid>
        </Grid>

        <Typography className={styles.title} variant="h1">{I18n.t('dashboard.charts.absoluteData')}</Typography>
        <Grid className={styles.row} container>
          <Grid item xs={12} md={6} lg={4} xl={3}>
            <GlobalSituation />
          </Grid>
          <Grid item xs={12} md={6} lg={4} xl={3}>
            <Supplier />
          </Grid>
          <Grid item xs={12} md={6} lg={4} xl={3}>
            <Treasury />
          </Grid>
          <Grid item xs={12} md={6} lg={4} xl={3}>
            <Client />
          </Grid>
        </Grid>

        <Typography className={styles.title} variant="h1">{I18n.t('dashboard.charts.relativeData_turnover')}</Typography>
        <Grid className={styles.row} container>
          <Grid item xs={12} md={6} lg={4} xl={3}>
            <PercentageGlobalSituation />
          </Grid>
          <Grid item xs={12} md={6} lg={4} xl={3}>
            <PercentageSupplier />
          </Grid>
          <Grid item xs={12} md={6} lg={4} xl={3}>
            <PercentageTreasury />
          </Grid>
          <Grid item xs={12} md={6} lg={4} xl={3}>
            <PercentageClient />
          </Grid>
        </Grid>


      </div>
    </Shortcuts>
  );
}

Dashboard.propTypes = {
  getCharts: PropTypes.func.isRequired,
  getRights: PropTypes.func.isRequired,

  redirect: PropTypes.func.isRequired,
  redirectFromCollab: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired
};

export default Dashboard;
