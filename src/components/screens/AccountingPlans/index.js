import React from 'react';

import PlansContainer from 'containers/groups/Plans';

const AccountingPlans = () => <PlansContainer />;

export default AccountingPlans;
