import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { Tab, Tabs } from '@material-ui/core';

import I18n from 'assets/I18n';

import Brochure from 'containers/groups/Brochure';
import { DADPHeader } from 'containers/groups/Headers';
import { SummaryCommentList, CycleCommentList } from 'containers/groups/Comments';
import { WorkProgramTableContainer, AnalyticReviewTableContainer } from 'containers/groups/Tables';

import styles from './dadpScreen.module.scss';

// Constants
const COMMENTS = 1;
const WORK_PROGRAM = 2;
const ANALYTICAL_REVIEW = 3;
const INTERNAL_REVIEW = 4;
const LETTER = 5;
const BILAN = 6;
const BROCHURE = 7;

// Component
const DADP = (props) => {
  const {
    hasCycle, summary,
    subCategory, setSubCategory
  } = props;

  const tabClasses = {
    label: styles.tabLabel,
    selected: styles.selected,
    disabled: styles.disabled,
    textColorPrimary: styles.textColor
  };

  const tabs = [
    <Tab key={COMMENTS} classes={tabClasses} label={I18n.t('dadp.summaryNote')} value={COMMENTS} />
  ];

  if (hasCycle) {
    tabs.push(
      <Tab key={WORK_PROGRAM} classes={tabClasses} label={I18n.t('dadp.workProgram')} value={WORK_PROGRAM} />,
      <Tab key={ANALYTICAL_REVIEW} classes={tabClasses} label={I18n.t('dadp.analyticalReview')} value={ANALYTICAL_REVIEW} />,
      <Tab key={INTERNAL_REVIEW} classes={tabClasses} label={I18n.t('dadp.internalRevision')} value={INTERNAL_REVIEW} disabled />
    );
  }

  if (summary) {
    tabs.push(
      <Tab key={LETTER} classes={tabClasses} label={I18n.t('dadp.letter')} value={LETTER} disabled />,
      <Tab key={BILAN} classes={tabClasses} label={I18n.t('dadp.bilan')} value={BILAN} disabled />,
      <Tab key={BROCHURE} classes={tabClasses} label={I18n.t('dadp.brochure')} value={BROCHURE} />
    );
  }

  return (
    <div className={styles.container}>
      <DADPHeader />
      { (hasCycle || summary) && (
        <Fragment>
          <Tabs
            value={subCategory}
            onChange={(tab, id) => { setSubCategory(id); }}

            classes={{ root: styles.tabs }}
            textColor="primary"
            indicatorColor="primary"
          >
            {tabs}
          </Tabs>
          <div className={styles.body}>
            {subCategory === COMMENTS && (
              summary ? <SummaryCommentList /> : <CycleCommentList />
            ) }
            {subCategory === WORK_PROGRAM && <WorkProgramTableContainer />}
            {subCategory === ANALYTICAL_REVIEW && <AnalyticReviewTableContainer />}
            {subCategory === BROCHURE && <Brochure />}
          </div>
        </Fragment>
      ) }
    </div>
  );
};

// Props
DADP.propTypes = {
  hasCycle: PropTypes.bool,
  summary: PropTypes.bool,

  subCategory: PropTypes.number,

  setSubCategory: PropTypes.func.isRequired
};

DADP.defaultProps = {
  hasCycle: false,
  summary: false,

  subCategory: COMMENTS
};

export default DADP;
