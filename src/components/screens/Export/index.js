import React, { useEffect } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import { Typography, withStyles } from '@material-ui/core';
import FecExportFilter from 'containers/groups/Filters/Exports/FEC';
import Loader from 'components/basics/Loader';
import Button from 'components/basics/Buttons/Button';
import { ReduxSelect } from 'components/reduxForm/Selections';
import I18n from 'assets/I18n';
import styles from './exportScreen.module.scss';

const ExportScreen = (props) => {
  const {
    classes,
    typeOptions,
    handleSubmit, onSubmitExportForm,
    getSociety, resetImportAndSetType,
    isLoading,
    exercices,
    societyId,
    initializeForm,
    exercicesLoading,
    getDiaries,
    selectedSociety
  } = props;

  useEffect(() => {
    getSociety();
    if (societyId !== -2) initializeForm();
  }, []);

  const onChangeType = (payload) => {
    resetImportAndSetType(payload);
    if (payload.target.value === 2 && selectedSociety) getDiaries();
  };

  return (
    <div className={styles.container}>
      <div className={styles.headerTitle}>
        <Typography variant="h1">
          {I18n.t('exports.title')}
        </Typography>
      </div>
      <form onSubmit={handleSubmit(onSubmitExportForm)}>
        <div>
          <Field
            label={I18n.t('exports.field.type')}
            name="type"
            className={styles.typeField}
            component={ReduxSelect}
            list={typeOptions}
            margin="none"
            onChange={onChangeType}
          />
        </div>
        {!exercicesLoading && (
          <div className={styles.filtersRow}>
            <FecExportFilter />
            <Button
              classes={{ root: classes.importButton }}
              variant="contained"
              color="primary"
              type="submit"
              disabled={exercices.length === 0}
            >
              {I18n.t('exports.toExport')}
            </Button>
          </div>
        )
        }
      </form>
      {isLoading && <Loader />}
    </div>
  );
};

const ownStyles = () => ({
  importButton: {
    marginLeft: '20px'
  }
});

ExportScreen.defaultProps = {
  exercices: []
};

ExportScreen.propTypes = {
  typeOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
  getSociety: PropTypes.func.isRequired,
  resetImportAndSetType: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onSubmitExportForm: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  exercices: PropTypes.array,
  societyId: PropTypes.number.isRequired,
  initializeForm: PropTypes.func.isRequired,
  exercicesLoading: PropTypes.bool.isRequired,
  getDiaries: PropTypes.func.isRequired,
  selectedSociety: PropTypes.number.isRequired
};

export default withStyles(ownStyles)(ExportScreen);
