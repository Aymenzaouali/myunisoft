import React, { useState } from 'react';
import { BundleHeader } from 'containers/groups/Headers';
import { Tabs, Tab } from '@material-ui/core';
import I18n from 'assets/I18n';
import CVAE1329AC from 'containers/groups/Forms/CVAE/Advance/CVAE1329AC';
import { useWidth } from 'helpers/hooks';

import styles from './CVAEAdvance.module.scss';

const existDocs = [
  {
    component: <CVAE1329AC />,
    key: '1329AC',
    label: '1329AC'
  }
];

// Component
const CVAEAdvance = () => {
  // State
  const [selectedTab, setSelectedTab] = useState(0);
  const [factor, setFactor] = useState(1);

  // Functions
  const handleChange = (e, value) => {
    setSelectedTab(value);
  };

  const factorCompute = (width) => {
    if (width < 1200) {
      setFactor(width / 1200);
    } else {
      setFactor(1);
    }
  };

  // Callbacks
  const containerCb = useWidth(factorCompute);

  // Rendering
  const pdfStyle = {};
  if (factor !== 1) {
    pdfStyle.transform = `scale(${factor})`;
  }

  return (
    <div>
      <BundleHeader code_sheet_group="ACOMPTE-CVAE" title={I18n.t('cvae.title')} isTaxForms={false} />
      <Tabs
        value={selectedTab}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        classes={{
          scrollButtons: styles.scrollButtons,
          root: styles.tabs
        }}
      >
        {
          existDocs.map((doc, key) => <Tab key={`${key}-${doc.key}`} label={doc.label} value={key} />)
        }
      </Tabs>
      <div ref={containerCb} className={styles.pdfContainer}>
        <div className={styles.pdf} style={pdfStyle}>
          {selectedTab === 0 && <CVAE1329AC />}
        </div>
      </div>
    </div>
  );
};

export default CVAEAdvance;
