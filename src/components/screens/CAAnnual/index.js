import React, { useEffect, useState } from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import { Tabs, Tab } from '@material-ui/core';
import { useWidth } from 'helpers/hooks';

import { CycleHeader } from 'containers/groups/Headers';
import TIDENTIF from 'containers/groups/Forms/CA/Annual/TIDENTIF';
import CA12AN3517 from 'containers/groups/Forms/CA/Annual/CA12AN3517';
import CA12AN3517DDR from 'containers/groups/Forms/CA/Annual/CA12AN3517DDR';

import styles from './CAAnnual.module.scss';

const existDocs = [
  {
    component: <CA12AN3517 />,
    key: '3517',
    label: '3517',
    form: 'ca12Annual_3517'
  },
  {
    component: <CA12AN3517DDR />,
    key: '3517DDR',
    label: '3517DDR',
    form: 'ca12Annual_3517DDR'
  },
  {
    component: <TIDENTIF />,
    key: 'T-IDENTIF',
    label: 'TIDENTIF',
    form: 'ca12Annual_T_IDENTIF'
  }
];

const CAAnnual = (props) => {
  const [selectedTab, setSelectedTab] = useState(0);
  const [factor, setFactor] = useState(1);
  const { sendCycleForms, selectForm, selectedFormId } = props;

  useEffect(() => {
    selectForm(selectedFormId);
  }, [selectedFormId]);

  // Functions
  const handleChange = async (e, value) => {
    await sendCycleForms();
    await selectForm(existDocs[value].key);
    await setSelectedTab(value);
  };

  const factorCompute = (width) => {
    if (width < 1200) {
      setFactor(width / 1200);
    } else {
      setFactor(1);
    }
  };

  // Callbacks
  const containerCb = useWidth(factorCompute);

  // Rendering
  const pdfStyle = {};
  if (factor !== 1) {
    pdfStyle.transform = `scale(${factor})`;
  }

  const component = get(existDocs[selectedTab], 'component');
  const form = get(existDocs[selectedTab], 'form');

  return (
    <div>
      <div className={styles.cycleHeaderWrap}>
        <CycleHeader
          cycle_code="CA12"
          code_sheet_group="CA12-ANNUAL"
          cycleForm={form}
          isNeant
        />
        <Tabs
          value={selectedTab}
          indicatorColor="primary"
          textColor="primary"
          onChange={handleChange}
          classes={{
            root: styles.tabs
          }}
        >
          {
            existDocs.map((doc, key) => <Tab key={`${key}-${doc.key}`} label={doc.label} value={key} />)
          }
        </Tabs>
      </div>
      <div ref={containerCb} className={styles.pdfContainer}>
        <div className={styles.pdf} style={pdfStyle}>
          {component}
        </div>
      </div>
    </div>
  );
};

CAAnnual.propTypes = {
  sendCycleForms: PropTypes.func.isRequired,
  selectedFormId: PropTypes.string.isRequired,
  selectForm: PropTypes.func.isRequired
};

export default CAAnnual;
