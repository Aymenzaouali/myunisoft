import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { NewLetterDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import { Field } from 'redux-form';
import ReduxRadio from 'components/reduxForm/Selections/ReduxRadio';
import { InlineButton } from 'components/basics/Buttons';
import LettersTable from 'containers/groups/Tables/Letters';
import styles from './letters.module.scss';

const types = [{
  value: 'all',
  label: 'Tous'
},
{
  value: 'brouillons',
  label: 'Brouillons'
},
{
  value: 'send',
  label: 'Envoyés'
}];

const Letters = (props) => {
  const {
    loadData,
    handleSubmit, // eslint-disable-line
    pagination
  } = props;
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    loadData({ type: 'all', limit: 20, offset: 0 });
  }, []);

  const onChange = (event) => {
    loadData({ type: event.target.value, limit: pagination.limit, offset: pagination.offset });
  };

  const onPressNewLetter = () => {
    setIsOpen(true);
  };

  const redirectToNewLetter = (idLetter = 0) => {
    const { match, history } = props; // eslint-disable-line
    const { id } = match.params;
    const path = `/tab/${id}/newLetter/${idLetter}`;
    history.push(path);
  };

  const onValidate = () => {
    const { createLetter } = props;
    handleSubmit(({ type }) => {
      createLetter({ type });
    })();
    setIsOpen(false);
    redirectToNewLetter();
  };

  return (
    <div className={styles.container}>
      <div className={styles.title}>
        <Typography variant="h1">{I18n.t('letters.title1')}</Typography>
      </div>
      <div className={styles.header}>
        <div className={styles.header}>
          <Typography variant="body1">{I18n.t('letters.show')}</Typography>
          <Field
            name="type"
            list={types}
            row
            color="primary"
            component={ReduxRadio}
            onChange={onChange}
          />
        </div>
        <div>
          <InlineButton
            buttons={[
              {
                _type: 'string',
                text: I18n.t('letters.newLetter'),
                onClick: onPressNewLetter
              },
              {
                _type: 'icon',
                iconName: 'icon-copy',
                iconSize: 25,
                titleInfoBulle: I18n.t('tooltips.copy')
              },
              {
                _type: 'icon',
                iconName: 'icon-print',
                iconSize: 25,
                titleInfoBulle: I18n.t('tooltips.print')
              },
              {
                _type: 'icon',
                iconName: 'icon-download',
                iconSize: 25,
                titleInfoBulle: I18n.t('tooltips.download')
              },
              {
                _type: 'icon',
                iconName: 'icon-pencil',
                iconSize: 25,
                titleInfoBulle: I18n.t('tooltips.edit')
              },
              {
                _type: 'icon',
                iconName: 'icon-trash',
                color: 'error',
                iconSize: 25,
                titleInfoBulle: I18n.t('tooltips.delete')
              }
            ]}
          />
        </div>
      </div>
      <div>
        <LettersTable />
      </div>
      <NewLetterDialog
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        onValidate={onValidate}
      />
    </div>
  );
};

Letters.propTypes = {
  loadData: PropTypes.func,
  createLetter: PropTypes.func.isRequired,
  pagination: PropTypes.object
};

Letters.defaultProps = {
  loadData: async () => {},
  pagination: {
    limit: 20,
    offset: 0
  }
};

export default Letters;
