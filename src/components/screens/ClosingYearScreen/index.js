import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { IconButton, Typography } from '@material-ui/core';

import I18n from 'assets/I18n';

import { DatePicker } from 'components/basics/Inputs';
import Loader from 'components/basics/Loader';
import ClosingYearConfirmation from 'components/groups/Dialogs/ClosingYearConfirmation';

import ANouveauxOptions from 'containers/groups/Forms/ANouveauxOptions';

import { SuccessfulDialog } from 'containers/groups/Dialogs';

import styles from './ClosingYearScreen.module.scss';

// Component
const ClosingYearScreen = (props) => {
  const {
    society_id, no_exercises, exercise,
    loading, error,
    closingYear, getExercises
  } = props;

  // State
  const [dialog, setDialog] = useState({ open: false, onConfirm: () => {} });

  // Handle
  const handleSubmit = (options) => {
    const handleConfirm = async () => {
      setDialog(prev => ({ ...prev, open: false }));

      if (await closingYear(exercise.id_exercice, options)) {
        getExercises(society_id);
      }
    };

    setDialog({ open: true, onConfirm: handleConfirm });
  };

  // Effects
  useEffect(() => {
    if (exercise == null && !no_exercises) {
      getExercises(society_id);
    }
  }, [society_id]);

  // Rendering
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <Typography variant="h1">{I18n.t('closingYear.title')}</Typography>

        <IconButton><span className="icon-popup" /></IconButton>
        <IconButton><span className="icon-close" /></IconButton>
      </div>
      <div className={styles.content}>
        <DatePicker
          label={I18n.t('closingYear.from')}
          value={exercise && exercise.start_date}

          classes={{ root: styles.date }}
          disabled
        />
        <DatePicker
          label={I18n.t('closingYear.to')}
          value={exercise && exercise.end_date}

          classes={{ root: styles.date }}
          disabled
        />
        <ANouveauxOptions className={styles.options} onSubmit={handleSubmit} disabled={loading} />
      </div>
      { (loading || error) && (
        <div className={styles.progress}>
          { loading && (
            <Fragment>
              <Loader size={32} />
              <Typography variant="h4">{I18n.t('closingYear.loading')}</Typography>
            </Fragment>
          ) }
          { error && (
            <Typography variant="h4" color="error">{I18n.t('errors.default')}</Typography>
          ) }
        </div>
      ) }
      { exercise && (
        <ClosingYearConfirmation
          {...dialog}
          start_date={moment(exercise.start_date).format('DD/MM/YYYY')}
          end_date={moment(exercise.end_date).format('DD/MM/YYYY')}

          onClose={() => setDialog(prev => ({ ...prev, open: false }))}
        />
      )}
      <SuccessfulDialog
        message={I18n.t('closingYear.popUp.success')}
      />
    </div>
  );
};

// Props
ClosingYearScreen.propTypes = {
  society_id: PropTypes.number.isRequired,
  no_exercises: PropTypes.bool.isRequired,
  exercise: PropTypes.shape({
    id_exercice: PropTypes.number.isRequired,
    start_date: PropTypes.string.isRequired,
    end_date: PropTypes.string.isRequired
  }),

  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,

  closingYear: PropTypes.func.isRequired,
  getExercises: PropTypes.func.isRequired
};

ClosingYearScreen.defaultProps = {
  exercise: null
};

export default ClosingYearScreen;
