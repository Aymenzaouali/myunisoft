import React, { useRef } from 'react';
import { ReportsAndFormsTable } from 'containers/groups/Tables';
import ReportsAndFormsAdditionalView from 'containers/groups/ReportsAndFormsAdditionalView';
import { ReportsAndFormsHeader } from 'containers/groups/Headers/';
import PropTypes from 'prop-types';
import styles from '../ScanAssociate/scanAssociate.module.scss';

const ReportsAndForms = ({ isAddAllFormsAndReportsAddView }) => {
  const rafRef = useRef(null);
  return (
    <>
      <div className={styles.container}>
        <ReportsAndFormsHeader />
        <ReportsAndFormsTable inputRef={rafRef} />
      </div>
      { isAddAllFormsAndReportsAddView && <ReportsAndFormsAdditionalView /> }
    </>
  );
};


ReportsAndForms.propTypes = {
  isAddAllFormsAndReportsAddView: PropTypes.string.isRequired
};


export default ReportsAndForms;
