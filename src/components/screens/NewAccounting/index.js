import React from 'react';
import PropTypes from 'prop-types';
import { Typography, withStyles } from '@material-ui/core';
import I18n from 'assets/I18n';

import Consulting from 'containers/groups/Consulting';
import NewAccounting from 'containers/groups/NewAccounting';
import Split from 'containers/groups/Split';
import _ from 'lodash';

// Component
const NewAccountingScreen = (props) => {
  const { societyId, exercices, classes } = props;

  const slaves = {
    consulting: {
      label: I18n.t('split.consulting'),
      url: `/tab/${societyId}/consulting`,
      component: () => <Consulting />,
      swappable: false
    }
  };

  return (
    <>
      {
        _.isEmpty(exercices)
          ? <Typography classes={{ root: classes.title }} variant="h1">{I18n.t('accounting.noExercice')}</Typography>
          : (
            <Split master_id="new-accounting" slaves={slaves}>
              <NewAccounting />
            </Split>
          )
      }
    </>
  );
};

const styles = {
  title: {
    margin: 20
  }
};

// Props
NewAccountingScreen.propTypes = {
  societyId: PropTypes.number.isRequired,
  exercices: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default withStyles(styles)(NewAccountingScreen);
