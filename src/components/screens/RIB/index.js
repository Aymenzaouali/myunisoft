import React, { useState } from 'react';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import RIBTable from 'containers/groups/Tables/RIBTable';
import EditRib from 'containers/groups/Dialogs/Rib';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import DeleteConfirmation from 'containers/groups/Dialogs/DeleteConfirmation';
import styles from './RIB.module.scss';

const RIB = (props) => {
  const {
    selectedRibs, ribDelete, resetSelectRib, csvData, baseFileName, disabledExport
  } = props;
  const [isOpen, setIsOpen] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);

  const onCloseDeleteDialog = () => setIsDeleteOpen(false);

  const initializeEdit = async () => {
    const {
      initializeRib,
      ribData
    } = props;
    await initializeRib({
      ...ribData
    });
  };

  return (
    <div className={styles.ribContainer}>
      <div className={styles.ribHeader}>
        <Typography variant="h1">{I18n.t('rib.title')}</Typography>
        <div>
          <InlineButton
            marginDirection="right"
            buttons={[
              {
                _type: 'string',
                text: I18n.t('rib.button.newRib'),
                size: 'medium',
                titleInfoBulle: I18n.t('tooltips.add'),
                onClick: () => {
                  setIsOpen(true);
                  setIsEdit(false);
                }
              },
              {
                _type: 'string',
                text: I18n.t('rib.button.changeRib'),
                size: 'medium',
                titleInfoBulle: I18n.t('tooltips.edit'),
                disabled: !selectedRibs || !selectedRibs.length || selectedRibs.length > 1,
                onClick: async () => {
                  await initializeEdit();
                  setIsOpen(true);
                  setIsEdit(true);
                  resetSelectRib();
                }
              },
              {
                _type: 'string',
                text: I18n.t('rib.button.deleteRib'),
                size: 'medium',
                color: 'error',
                titleInfoBulle: I18n.t('tooltips.delete'),
                disabled: !selectedRibs || !selectedRibs.length,
                onClick: () => setIsDeleteOpen(true)
              },
              {
                _type: 'component',
                color: 'primary',
                size: 'medium',
                disabled: disabledExport,
                component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
              }
              // {
              //   _type: 'icon',
              //   iconName: 'icon-popup',
              //   iconSize: 28,
              //   variant: 'none',
              //   iconColor: 'black',
              //   titleInfoBulle: I18n.t('tooltips.newWindow'),
              //   onClick: () => {},
              //   colorError: true
              // },
              // {
              //   _type: 'icon',
              //   iconName: 'icon-close',
              //   iconSize: 28,
              //   variant: 'none',
              //   iconColor: 'black',
              //   titleInfoBulle: I18n.t('tooltips.close'),
              //   onClick: () => {},
              //   colorError: true
              // }
            ]}
          />
        </div>
      </div>
      <RIBTable />
      <EditRib
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        isEdit={isEdit}
      />
      <DeleteConfirmation
        deleteReportAndForm={ribDelete}
        isOpen={isDeleteOpen}
        onClose={onCloseDeleteDialog}
      />
    </div>
  );
};

RIB.propTypes = {
  selectedRibs: PropTypes.arrayOf(PropTypes.string),
  ribDelete: PropTypes.func.isRequired,
  ribData: PropTypes.shape({}).isRequired,
  initializeRib: PropTypes.func.isRequired,
  resetSelectRib: PropTypes.func.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired,
  disabledExport: PropTypes.bool.isRequired
};

RIB.defaultProps = {
  selectedRibs: []
};
export default RIB;
