import React from 'react';
import PropTypes from 'prop-types';

import I18n from 'assets/I18n';

import Consulting from 'containers/groups/Consulting';
import CurrentEditions from 'containers/groups/CurrentEditions';
import Split from 'containers/groups/Split';

// Component
const CurrentEditionsScreen = ({ societyId, currentOption }) => {
  const slaves = {
    consulting: {
      label: I18n.t('split.consulting'),
      url: `/tab/${societyId}/consulting`,
      component: () => <Consulting />,
      swappable: false
    }
  };
  if (currentOption === 'balance') {
    return (
      <Split master_id="current_editions" slaves={slaves}>
        <CurrentEditions />
      </Split>
    );
  }
  return <CurrentEditions />;
};

// Props
CurrentEditionsScreen.propTypes = {
  societyId: PropTypes.number.isRequired,
  currentOption: PropTypes.string.isRequired
};

export default CurrentEditionsScreen;
