import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { DiaryHeader } from 'containers/groups/Headers';
import DiaryTable from 'containers/groups/Tables/Diary';
import { DiaryDialog } from 'containers/groups/Dialogs';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import DeleteConfirmation from 'containers/groups/Dialogs/DeleteConfirmation';
import styles from './diary.module.scss';

const Diary = (props) => {
  const {
    societyId,
    diarySelectedLine,
    selectedDiaryCount,
    resetDiarySelected,
    getDiariesType,
    deleteDiary,
    getDiaryDetail,
    getDiaries,
    disabledExport,
    csvData,
    baseFileName,
    resetForm
  } = props;
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isModify, setIsModify] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);
  const hasSelected = selectedDiaryCount > 0;

  const onCloseDeleteDialog = () => setIsDeleteOpen(false);

  const handleNewDiary = () => {
    resetForm('diaryForm');
    getDiariesType();
    resetDiarySelected();
    setIsDialogOpen(true);
    setIsModify(false);
  };

  const handleModifyDiary = async () => {
    await getDiaryDetail();
    await getDiariesType();
    setIsDialogOpen(true);
    setIsModify(true);
  };

  const handleDeleteDiary = async () => {
    await deleteDiary(societyId);
    resetDiarySelected();
    await getDiaries();
  };

  const closeDialog = () => {
    setIsDialogOpen(false);
    setIsModify(false);
  };

  return (
    <div className={styles.container}>
      <DiaryHeader />
      <div className={styles.headerRightButton}>
        <InlineButton
          marginDirection="right"
          buttons={[
            {
              _type: 'string',
              text: I18n.t('diary.button.newDiary'),
              onClick: handleNewDiary
            },
            {
              _type: 'string',
              text: I18n.t('diary.button.changeDiary'),
              onClick: handleModifyDiary,
              disabled: !diarySelectedLine
            },
            {
              _type: 'string',
              text: I18n.t('diary.button.deleteDiary'),
              color: 'error',
              onClick: () => setIsDeleteOpen(true),
              disabled: !hasSelected
            },
            {
              _type: 'component',
              color: 'primary',
              disabled: disabledExport,
              component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
            }]
          }
        />
      </div>
      <div className={styles.table}>
        <DiaryTable selectedSociety={societyId} />
      </div>
      {
        isDialogOpen && (
          <DiaryDialog
            isOpen={isDialogOpen}
            onClose={closeDialog}
            modify={isModify}
          />
        )
      }
      <DeleteConfirmation
        deleteReportAndForm={handleDeleteDiary}
        isOpen={isDeleteOpen}
        onClose={onCloseDeleteDialog}
      />
    </div>
  );
};

Diary.propTypes = {
  diarySelectedLine: PropTypes.shape({}).isRequired,
  selectedDiary: PropTypes.shape({}).isRequired,
  selectedDiaryCount: PropTypes.number.isRequired,
  resetDiarySelected: PropTypes.func.isRequired,
  getDiariesType: PropTypes.func.isRequired,
  deleteDiary: PropTypes.func.isRequired,
  getDiaryDetail: PropTypes.func.isRequired,
  getDiaries: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired
};

export default Diary;
