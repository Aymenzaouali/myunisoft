import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import _get from 'lodash/get';
import I18n from 'assets/I18n';
import { FixedAssetsHeader } from 'containers/groups/Headers';
import TableFactory from 'components/groups/Tables/Factory';
import style from 'components/screens/FixedAssets/style.module.scss';
import Radio from 'components/basics/Radio';
import DatePicker from 'components/basics/Inputs/DatePicker';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import { useWindowState } from 'helpers/hooks';
import Window from 'components/basics/Window';
import FixedAssetsModal from 'containers/groups/Dialogs/FixedAssets';
import EarlyStartDateModal from 'containers/groups/Dialogs/EarlyStartDateModal';
import ModifiGeneratedDialog from 'containers/groups/Dialogs/FixedAssets/ModifiGeneratedDialog';
import FilesDropperDialog from 'components/groups/Dialogs/FilesDropper';
import SaleDialog from 'containers/groups/Dialogs/SaleDialog';
import { Typography } from '@material-ui/core';
import { formatNumber, roundNumber } from 'helpers/number';
import {
  getAccountsTableFactoryProps,
  getFixedAssetsTableFactoryProps,
  getDotationsTableFactoryProps,
  getSalesTableFactoryProps
} from './utils';

const FixedAssetsScreen = (props) => {
  const {
    diligence_id,
    societyId,
    isLoading,
    saveImmos,
    deleteImmos,
    currentImmoModal,
    resetForm,
    removeChecker,
    redirected,
    isEditing,
    startEdit,
    selectedAccount,
    selectedImmos,
    getDilligenceIdByDossierRevision,
    cancelImmos,
    startDate,
    endDate,
    dossierRevisionId,
    initialImmoModal,
    getExercice,
    exercices,
    loadData,
    removeRedirected,
    getDiligences,
    setTableData,
    disabledExport,
    csvDataAdditional,
    baseFileName,
    setAdditionalTableData,
    getGlobalComments,
    onImmosGenerated,
    sendPjs,
    getSale,
    isValidedSales = true,
    saleModalSubmit,
    isSelectedImmoHasEntry,
    showEarlyStartDateModal,
    toggleEarlyStartDateModal
  } = props;

  const [modal, setModal] = useState(false);
  const [saleModal, toggleSaleModal] = useState(false);
  const [PJImmo, setPJImmo] = useState(null);
  const [isPJOpen, setIsPJOpen] = useState(false);

  const openPJDialog = (params) => {
    setIsPJOpen(true);
    setPJImmo(params);
  };

  const overview = useWindowState();
  const immoFilter = () => {
    const newarrayImmo = _get(selectedAccount, 'arrayImmo', []).filter(
      immo => (startDate <= immo.start_date || endDate >= immo.end_date)
    );
    return redirected ? ({ ...selectedAccount, arrayImmo: newarrayImmo }) : selectedAccount;
  };
  const [showAllAccounts, setShowAllAccounts] = useState(true);
  const immoInputRef = useRef(null);
  const focusInput = () => {
    if (immoInputRef.current) {
      immoInputRef.current.focus();
    }
  };
  const immoValues = _get(immoFilter(), 'arrayImmo', []).reduce(
    (acc, {
      purchase_value = 0,
      previousDotation = 0,
      currentAnnualDotation = 0,
      EndingAnnualDotation = 0
    }) => ({
      purchase_value: acc.purchase_value + parseInt(purchase_value, 10),
      previousDotation: acc.previousDotation + parseInt(previousDotation, 10),
      currentAnnualDotation: acc.currentAnnualDotation + parseInt(currentAnnualDotation, 10),
      EndingAnnualDotation: acc.EndingAnnualDotation + parseInt(EndingAnnualDotation, 10)
    }),
    {
      purchase_value: 0,
      previousDotation: 0,
      currentAnnualDotation: 0,
      EndingAnnualDotation: 0
    }
  );

  useEffect(() => {
    loadData();
    if (dossierRevisionId) getDilligenceIdByDossierRevision('AC-G-01', dossierRevisionId, 'fixed_assets');
    if (startDate && endDate) getDiligences('DA', 1, 7, startDate, endDate, 1);
    if (!exercices.length) getExercice();
    if (diligence_id) getGlobalComments(diligence_id);
    return () => removeRedirected();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dossierRevisionId, diligence_id]);

  const onSaveClick = async () => {
    await saveImmos();
    setTimeout(focusInput, 0);
  };
  const onStartEdit = () => {
    startEdit();
    setTimeout(focusInput, 0);
  };

  const setInitialImmoModal = () => {
    const values = {
      immos: [
        {
          ...currentImmoModal[0]
        },
        {
          ...currentImmoModal[0],
          purchase_value: undefined,
          label: ''
        }
      ]
    };
    initialImmoModal(values);
    setModal(true);
  };

  const initSale = async () => {
    toggleSaleModal(true);
    await getSale();
  };

  const selectedImmosCount = Object
    .keys(selectedImmos || {})
    .filter(key => selectedImmos[key])
    .length;

  const saleModalConfirm = () => {
    saleModalSubmit();
    toggleSaleModal(false);
  };

  const tableProps = getAccountsTableFactoryProps(props, showAllAccounts);
  const { isLoading: isTableLoading, param } = tableProps;

  useEffect(() => {
    setTableData(param);
  }, [isTableLoading, isLoading]);

  const additionalTableProps = getFixedAssetsTableFactoryProps({
    ...props,
    focusInput,
    inputRef: immoInputRef,
    selectedAccount: immoFilter(),
    openPJDialog
  });

  const { param: additionalParam } = additionalTableProps;

  useEffect(() => {
    setAdditionalTableData(additionalParam);
  }, [isLoading, selectedImmosCount]);

  return (
    <div className={style.fixedAssets}>
      <div className={style.container}>
        <FixedAssetsHeader {...props} openPJDialog={openPJDialog} />
        <div className={style.form}>
          <div className={style.dates}>
            <div className={style.dateGroup}>
              <div>
                {I18n.t('fixedAssets.from')}
              </div>
              <div>
                <DatePicker value={startDate} disabled />
              </div>
            </div>
            <div className={style.dateGroup}>
              <div>
                {I18n.t('fixedAssets.to')}
              </div>
              <div>
                <DatePicker value={endDate} disabled />
              </div>
            </div>
          </div>
          <div className={style.filters}>
            <div className={style.showLabel}>
              {I18n.t('fixedAssets.show')}
            </div>
            <div>
              <button
                type="button"
                className={style.radioGroup}
                onClick={() => setShowAllAccounts(false)}
              >
                <Radio checked={!showAllAccounts} />
                {I18n.t('fixedAssets.filledAccounts')}
              </button>
              <button
                type="button"
                className={style.radioGroup}
                onClick={() => setShowAllAccounts(true)}
              >
                <Radio checked={showAllAccounts} />
                {I18n.t('fixedAssets.all')}
              </button>
            </div>
          </div>
        </div>
        <div className={style.accounts}>
          <TableFactory {...tableProps} />
        </div>
      </div>

      {selectedAccount && (
        <div className={style.container}>
          <div className={style.selectionManager}>
            <div>
              {selectedImmosCount < 2
                ? I18n.t('fixedAssets.selected', { count: selectedImmosCount })
                : I18n.t('fixedAssets.selectedPlural', { count: selectedImmosCount })
              }
            </div>
            <div>
              <InlineButton buttons={
                [
                  isEditing && {
                    _type: 'string',
                    text: I18n.t('common.cancel'),
                    onClick: () => cancelImmos(societyId),
                    color: 'error'
                  },
                  {
                    _type: 'string',
                    onClick: initSale,
                    color: 'primary',
                    text: 'Vente',
                    disabled: currentImmoModal.length !== 1 && isSelectedImmoHasEntry
                  },
                  {
                    _type: 'string',
                    iconName: '',
                    onClick: currentImmoModal.length === 1 ? setInitialImmoModal : null,
                    iconColor: 'white',
                    color: 'primary',
                    text: 'Scinder',
                    iconSize: 28,
                    titleInfoBulle: I18n.t('tooltips.Scinder'),
                    disabled: currentImmoModal.length !== 1
                  },
                  {
                    _type: 'icon',
                    iconName: isEditing ? 'icon-save' : 'icon-pencil',
                    onClick: isEditing ? () => onImmosGenerated(onSaveClick) : onStartEdit,
                    iconColor: 'white',
                    color: 'primary',
                    iconSize: 28,
                    titleInfoBulle: I18n.t('tooltips.savedSearches'),
                    disabled: !selectedAccount || !selectedAccount.arrayImmo.length
                  },
                  {
                    _type: 'icon',
                    iconName: 'icon-trash',
                    onClick: deleteImmos,
                    iconColor: 'white',
                    color: 'error',
                    iconSize: 28,
                    titleInfoBulle: I18n.t('tooltips.delete'),
                    disabled: !removeChecker
                  },
                  {
                    _type: 'component',
                    color: 'primary',
                    disabled: disabledExport,
                    component: <CSVButton csvData={csvDataAdditional} baseFileName={baseFileName} />
                  }
                ]}
              />
            </div>
          </div>
          {!isLoading && additionalTableProps && (
            <div className={style.immos}>
              <TableFactory
                {...additionalTableProps}
              />
            </div>
          )}
        </div>
      )}

      {
        isValidedSales && (
          <div className={classnames(style.container)}>
            <Typography variant="h6">
              {I18n.t('fixedAssets.salesTitle')}
            </Typography>
            <TableFactory {...getSalesTableFactoryProps({ ...props })} />
            {/* <InlineButton */}
            {/*  buttons={[ */}
            {/*    { */}
            {/*      _type: 'string', */}
            {/*      // onClick: postSale, */}
            {/*      iconColor: 'white', */}
            {/*      color: 'primary', */}
            {/*      iconSize: 28, */}
            {/*      text: 'Scinder', */}
            {/*      titleInfoBulle: I18n.t('tooltips.delete') */}
            {/*    } */}
            {/*  ]} */}
            {/* /> */}
          </div>
        )
      }

      {!!selectedImmosCount && (
        <div className={classnames(style.dotations, style.container)}>
          <TableFactory
            {...getDotationsTableFactoryProps(props)}
          />
        </div>
      )}

      {selectedAccount && (
        <div className={classnames(style.gaps, style.container)}>
          <div className={style.gapLine}>
            <div className={style.gapLineTitle}>{I18n.t('fixedAssets.immoWorksheet')}</div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.purchase')}</div>
              <div className={style.gapValue}>{formatNumber(immoValues.purchase_value)}</div>
            </div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.previousImmo')}</div>
              <div className={style.gapValue}>{formatNumber(immoValues.previousDotation)}</div>
            </div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.endowmentPeriod')}</div>
              <div className={style.gapValue}>{formatNumber(immoValues.currentAnnualDotation)}</div>
            </div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.immoPeriodEndingShort')}</div>
              <div className={style.gapValue}>{formatNumber(immoValues.EndingAnnualDotation)}</div>
            </div>
          </div>
          <div className={style.gapLine}>
            <div className={style.gapLineTitle}>{I18n.t('fixedAssets.accounting')}</div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.purchase')}</div>
              <div className={style.gapValue}>{formatNumber(selectedAccount.m_achat_compta)}</div>
            </div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.previousImmo')}</div>
              <div className={style.gapValue}>
                {formatNumber(selectedAccount.m_amort_ant_compta)}
              </div>
            </div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.endowmentPeriod')}</div>
              <div className={style.gapValue}>
                {formatNumber(selectedAccount.m_dotation_periode_compta)}
              </div>
            </div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.immoPeriodEndingShort')}</div>
              <div className={style.gapValue}>
                {formatNumber(selectedAccount.m_amortissement_fin_periode_compta)}
              </div>
            </div>
          </div>
          <div className={style.gapLine}>
            <div className={style.gapLineTitle}>{I18n.t('fixedAssets.gap')}</div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.purchaseGap')}</div>
              <div className={style.gapValue}>
                {formatNumber(roundNumber(Math.abs(immoValues.purchase_value
                  - selectedAccount.m_achat_compta)))}
              </div>
            </div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.previousImmoGap')}</div>
              <div className={style.gapValue}>
                {formatNumber(roundNumber(Math.abs(immoValues.previousDotation
                  - selectedAccount.m_amort_ant_compta)))}
              </div>
            </div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.endowmentPeriodGap')}</div>
              <div className={style.gapValue}>
                {formatNumber(roundNumber(Math.abs(
                  immoValues.currentAnnualDotation - selectedAccount.m_dotation_periode_compta
                )))}
              </div>
            </div>
            <div className={style.gap}>
              <div className={style.gapLabel}>{I18n.t('fixedAssets.table.lastImmoGap')}</div>
              <div className={style.gapValue}>
                {formatNumber(roundNumber(Math.abs(
                  immoValues.EndingAnnualDotation
                  - selectedAccount.m_amortissement_fin_periode_compta
                )))}
              </div>
            </div>
          </div>
        </div>
      )}
      <FixedAssetsModal
        onClose={() => { resetForm(); setModal(false); }}
        open={modal}
        defaultImmos={currentImmoModal[0]}
      />
      <FilesDropperDialog
        open={isPJOpen}
        onClose={() => setIsPJOpen(false)}
        onAddFiles={file => sendPjs(file, PJImmo.id, PJImmo.location)}
      />
      <ModifiGeneratedDialog />
      <SaleDialog
        onConfirm={saleModalConfirm}
        onClose={() => { toggleSaleModal(false); }}
        open={saleModal}
      />
      <EarlyStartDateModal
        onConfirm={() => {}}
        onClose={() => { toggleEarlyStartDateModal(false, societyId); }}
        open={showEarlyStartDateModal}
      />
      <Window
        path="/overview"
        window_id="overview"
        features={`width=${window.innerWidth / 2},left=${window.screenX + (window.innerWidth / 2)}`}

        {...overview.props}
      />
    </div>
  );
};

FixedAssetsScreen.propTypes = {
  societyId: PropTypes.number.isRequired,
  diligence_id: PropTypes.number.isRequired,
  currentImmoModal: PropTypes.array.isRequired,
  initialImmoModal: PropTypes.func.isRequired,
  getAccounts: PropTypes.func,
  accounts: PropTypes.array,
  selectedAccount: PropTypes.object,
  selectAccount: PropTypes.func,
  unselectAccount: PropTypes.func,
  selectedImmos: PropTypes.object,
  selectImmo: PropTypes.func,
  unselectImmo: PropTypes.func,
  deleteImmos: PropTypes.func,
  selectedDotation: PropTypes.object,
  selectDotation: PropTypes.func,
  unselectDotation: PropTypes.func,
  isEditing: PropTypes.bool,
  getDiligences: PropTypes.func.isRequired,
  setAdditionalTableData: PropTypes.func.isRequired,
  startEdit: PropTypes.func,
  resetForm: PropTypes.func.isRequired,
  endEdit: PropTypes.func,
  startDate: PropTypes.func,
  endDate: PropTypes.func,
  disabledExport: PropTypes.bool,
  csvDataAdditional: PropTypes.array,
  baseFileName: PropTypes.string,
  getDilligenceIdByDossierRevision: PropTypes.func.isRequired,
  getGlobalComments: PropTypes.func.isRequired,
  cancelImmos: PropTypes.func,
  getExercice: PropTypes.func.isRequired,
  exercices: PropTypes.array.isRequired,
  loadData: PropTypes.func,
  removeChecker: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool,
  redirected: PropTypes.bool.isRequired,
  saveImmos: PropTypes.func,
  showEarlyStartDateModal: PropTypes.bool,
  dossierRevisionId: PropTypes.number.isRequired,
  setTableData: PropTypes.func.isRequired,
  sendPjs: PropTypes.func.isRequired,
  onImmosGenerated: PropTypes.func.isRequired,
  removeRedirected: PropTypes.func.isRequired,
  getSale: PropTypes.func.isRequired,
  isValidedSales: PropTypes.bool.isRequired,
  postSale: PropTypes.func.isRequired,
  saleModalSubmit: PropTypes.func.isRequired,
  toggleEarlyStartDateModal: PropTypes.func.isRequired,
  isSelectedImmoHasEntry: PropTypes.bool.isRequired
};

FixedAssetsScreen.defaultProps = {
  getAccounts: undefined,
  accounts: undefined,
  selectedAccount: undefined,
  selectAccount: undefined,
  unselectAccount: undefined,
  selectedImmos: undefined,
  selectImmo: undefined,
  unselectImmo: undefined,
  deleteImmos: undefined,
  selectedDotation: undefined,
  selectDotation: undefined,
  unselectDotation: undefined,
  isEditing: undefined,
  startEdit: undefined,
  endEdit: undefined,
  startDate: undefined,
  endDate: undefined,
  loadData: undefined,
  isLoading: undefined,
  cancelImmos: undefined,
  saveImmos: undefined,
  disabledExport: false,
  showEarlyStartDateModal: false,
  csvDataAdditional: [],
  baseFileName: ''
};

export default FixedAssetsScreen;
