import I18n from 'assets/I18n';
import _get from 'lodash/get';
import moment from 'moment';
import { store } from 'redux/store';
import classnames from 'classnames';
import cellStyle from 'components/basics/Cells/BodyCell/BodyCell.module.scss';
import TextFieldCell from 'components/basics/Cells/TextFieldCell';
import AutoCompleteCell from 'components/basics/Cells/AutoCompleteCell';
import {
  empty, invalidDate, optional, isNaN, negative
} from 'helpers/validation';
import { routesByKey } from 'helpers/routes';
import WindowHandler from 'helpers/window';
import IconCell from 'components/basics/Cells/IconCell';
import styles from 'components/screens/WorkingPage/workingPage.module.scss';
import StatusCell from 'components/basics/Cells/StatusCell';
import { AccountingBoxComment, FixedAssetsCommentBox } from 'containers/groups/Comments';
import fixedAssetsActions from 'redux/fixedAssets/actions';
import { formatNumber } from 'helpers/number';
import style from './style.module.scss';

const focus = (focusInput) => {
  if (focusInput) {
    setTimeout(focusInput, 0);
  }
};

const showToEarlyModal = (index) => {
  const state = store.getState();
  store.dispatch(
    fixedAssetsActions.showEarlyStartDateModal(true, state.navigation.id, index)
  );
};

const onSelectItem = (isChecked, rowItem, selectCallback, unselectCallBack, focusInput) => {
  if (isChecked) {
    selectCallback(rowItem);
  } else {
    unselectCallBack(rowItem);
  }

  focus(focusInput);
};

const newWindowRedirect = (data, societyId) => {
  const { id_account, start_date, purchase_value } = data;
  const queryParams = `redirected=true&id=${id_account}&start_date=${start_date}&purchase_value=${purchase_value}`;
  window.open(`${routesByKey.newAccounting.replace(':id', societyId)}?${queryParams}`);
};
const cursorPointer = dep => (dep ? cellStyle.cursorPointer : '');

export const getAccountsTableFactoryProps = ({
  accounts = [],
  isLoading,
  loadData,
  getComments,
  selectedAccount,
  selectAccount,
  unselectAccount,
  openCommentsDialog,
  dossierRevisionId
}, showAllAccounts) => {
  const getAndOpenComments = async (event, id_compte) => {
    event.stopPropagation();
    if (id_compte !== undefined) {
      openCommentsDialog({
        body: {
          Component: AccountingBoxComment,
          props: {
            dossier_revision_id: dossierRevisionId,
            account_id: id_compte,
            createMode: false,
            getComments: () => id_compte
              && getComments({ account_id: id_compte, dossier_revision_id: dossierRevisionId })
          }
        }
      });
    } else {
      openCommentsDialog({
        body: {
          Component: AccountingBoxComment,
          props: {
            dossier_revision_id: dossierRevisionId,
            account_id: id_compte,
            createMode: true
          }
        }
      });
    }
  };
  return {
    className: style.accountTable,
    isLoading,
    loadData,
    param: {
      header: {
        props: {},
        row: [{
          props: {
            className: classnames(style.headerRow, style.row)
          },
          value: [
            {
              _type: 'checkbox',
              keyCell: 'account-header-check',
              cellProp: {
                className: style.checkCell
              },
              props: {
                className: cellStyle.iconCell
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'account-header-nocompte',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.accountNumber')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'account-header-intitule',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.label')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'account-header-solde',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.balance')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'account-header-amort ant',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.previousImmo')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'account-header-dot periode',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.endowmentPeriodShort')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'account-header-amort fin per',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.immoPeriodEnding')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'account-header-vnc',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.vnc')
              }
            },
            {
              _type: 'string',
              keyCell: 'account-header-commentaire',
              cellProp: {
                isCentered: true
              },
              props: {
                label: I18n.t('fixedAssets.table.comment')
              }
            }
          ]
        }]
      },
      body: {
        props: {},
        row: accounts.reduce((acc, account) => {
          const isSelected = !!(selectedAccount && selectedAccount.id_compte === account.id_compte);
          return showAllAccounts || (account.arrayImmo && account.arrayImmo.length) ? [...acc, {
            keyRow: account.id_compte,
            props: {
              className: style.row,
              isSelected,
              onClick: () => onSelectItem(
                !isSelected,
                account,
                selectAccount,
                unselectAccount
              )
            },
            value: [
              {
                _type: 'checkbox',
                keyCell: `${account.id_compte}.check`,
                cellProp: {
                  className: style.checkCell
                },
                props: {
                  className: cellStyle.iconCell,
                  checked: isSelected
                }
              },
              {
                keyCell: `${account.id_compte}.no compte`,
                cellProp: {},
                children: account.noCompte
              },
              {
                keyCell: `${account.id_compte}.intitule`,
                cellProp: {},
                children: account.label
              },
              {
                keyCell: `${account.id_compte}.solde`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: formatNumber(_get(account, 'arrayImmo', []).reduce((acc, immo) => acc + (+immo.purchase_value || 0), 0))
              },
              {
                keyCell: `${account.id_compte}.amort ant`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: account.previousDotation
              },
              {
                keyCell: `${account.id_compte}.dot période`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: account.currentAnnualDotation
              },
              {
                keyCell: `${account.id_compte}.amort fin pér`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: account.EndingAnnualDotation
              },
              {
                keyCell: `${account.id_compte}.vnc`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: account.netBookValue
              },
              {
                _type: 'icon',
                keyCell: `${account.id_compte}.commentaire`,
                cellProp: {
                  className: cellStyle.iconCell
                },
                props: {
                  onClick: event => getAndOpenComments(event, account.id_compte),
                  name: 'icon-comments'
                }
              }
            ]
          }] : acc;
        }, [])
      },
      footer: {}
    }
  };
};

const amortTypes = [
  { value: 1, label: I18n.t('fixedAssets.table.linear') },
  { value: 2, label: I18n.t('fixedAssets.table.degressive') }
];

const parseDate = value => (value && moment(value, 'DD/MM/YYYY').isValid() ? moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD') : value);
const formatDate = value => (value && (moment(value, 'YYYY-MM-DD').isValid() || moment(value, 'DD/MM/YYYY').isValid()) ? moment(value).format('DD/MM/YYYY') : value);
export const getFixedAssetsTableFactoryProps = ({
  inputRef,
  getComments,
  startDate,
  openCommentsDialog,
  errors,
  selectedAccount,
  selectedImmos,
  lastSelectedImmo,
  selectImmo,
  createdImmos,
  editImmo,
  unselectImmo,
  openPJDialog,
  setErrors,
  addImmoLine,
  isEditing,
  focusInput
}) => {
  let firstError = false;
  const getProps = (
    immo,
    propName,
    validate = () => undefined,
    parse = v => v,
    format = v => v
  ) => ({
    placeholder: format(immo[propName]),
    defaultValue: format(immo[propName]),
    onBlur: (event) => {
      editImmo({
        ...immo,
        [propName]: parse(event.target.value)
      });

      setErrors(immo.id_immo, { [propName]: validate(event.target.value) });
    }
  });

  const getAndOpenComments = async (immo_id) => {
    if (immo_id !== undefined) {
      openCommentsDialog({
        body: {
          Component: FixedAssetsCommentBox,
          props: {
            immo_id,
            createMode: false,
            getComments: () => getComments({ immo_id })
          }
        }
      });
    } else {
      openCommentsDialog({
        body: {
          Component: FixedAssetsCommentBox,
          props: {
            immo_id,
            createMode: true
          }
        }
      });
    }
  };

  return ({
    className: style.immoTable,
    addBodyRowCb: {
      onClick: () => {
        addImmoLine();
        focus(focusInput);
      }
    },
    param: {
      header: {
        props: {},
        row: [{
          props: {
            className: classnames(style.headerRow, style.row)
          },
          value: [
            {
              _type: 'checkbox',
              keyCell: 'immo-header-check',
              cellProp: {
                className: style.checkCell
              },
              props: {
                className: cellStyle.iconCell
              }
            },
            {
              _type: 'string',
              keyCell: 'immo-header-lien écriture',
              cellProp: {
                isCentered: true
              },
              props: {
                label: I18n.t('fixedAssets.table.entryLink')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-fournisseur',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.supplier')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-intitulé',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.label')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-date achat',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.purchaseDate')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-mise en service',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.commissioning')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-nb mois',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.monthNumber')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-type amort',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.immoType')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-achats',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.purchases')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-amort ant',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.previousImmo')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-dot année',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.endowmentYear')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-amort fin période',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.immoPeriodEnding')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-vnc',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.vnc')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-date de sortie',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.releaseDate')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-prix vente',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.sellingPrice')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-+ value',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.plusValue')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'immo-header-com',
              cellProp: {
                isCentered: true
              },
              props: {
                children: I18n.t('fixedAssets.table.commentShort')
              }
            },
            {
              _type: 'string',
              keyCell: 'immo-header-pj',
              cellProp: {
                isCentered: true
              },
              props: {
                label: I18n.t('fixedAssets.table.attachment')
              }
            },
            {
              _type: 'string',
              keyCell: 'immo-header-en compte',
              cellProp: {
                isCentered: true
              },
              props: {
                label: I18n.t('fixedAssets.table.inAccounting')
              }
            }
          ]
        }]
      },
      body: {
        props: {},
        row: selectedAccount && selectedAccount.arrayImmo.map((immo, index) => {
          const societeID = _get(selectedAccount, 'id_societe', '');
          const isSelected = !!selectedImmos[immo.id_immo];
          const isEditingImmo = isSelected && isEditing && !immo.previousDotation;
          const isCreated = !!createdImmos[immo.id_immo];
          const error = _get(errors, immo.id_immo, {});
          const firstErrorPath = Object.keys(error).find(key => error[key]);
          const isFirstErrorLine = !firstError && firstErrorPath;
          if (firstErrorPath) firstError = true;
          const isLastSelectedImmo = lastSelectedImmo && typeof lastSelectedImmo === 'object'
            && (immo.id_immo.toString() === lastSelectedImmo.toString())
            && isEditingImmo;

          return {
            keyRow: `${selectedAccount.id_compte}-${immo.id_immo}-${index}`,
            props: {
              isSelected
            },
            value: [
              {
                _type: 'checkbox',
                keyCell: `${immo.id_immo}.check`,
                cellProp: {
                  className: classnames(style.checkCell, cellStyle.checkboxCell)
                },
                props: {
                  className: cellStyle.iconCell,
                  onChange:
                    (_, isChecked) => onSelectItem(
                      isChecked,
                      immo,
                      selectImmo,
                      unselectImmo,
                      focusInput
                    ),
                  checked: isSelected
                }
              },
              {
                _type: immo.id_line_entry_purchase ? 'icon' : 'string',
                keyCell: `${immo.id_immo}.lien écriture`,
                cellProp: {
                  onClick: () => immo.id_line_entry_purchase
                    && newWindowRedirect(immo, societeID),
                  className: classnames(cellStyle.iconCell, style.green,
                    cursorPointer(immo.id_line_entry_purchase))
                },
                props: {
                  name: 'icon-link'
                }
              },
              {
                error: _get(error, 'provider'),
                keyCell: `${immo.id_immo}.fournisseur`,
                cellProp: {},
                component: isCreated ? TextFieldCell : 'div',
                props: isCreated ? {
                  inputRef: isCreated && (isLastSelectedImmo || (isFirstErrorLine && firstErrorPath === 'provider')) && inputRef,
                  ...getProps(immo, 'provider', empty)
                } : {
                  children: immo.provider
                }
              },
              {
                error: _get(error, 'label'),
                keyCell: `${immo.id_immo}.intitulé`,
                cellProp: {},
                component: isCreated ? TextFieldCell : 'div',
                props: isCreated ? {
                  inputRef: isCreated && isFirstErrorLine && firstErrorPath === 'label' && inputRef,
                  ...getProps(immo, 'label', empty)
                } : { children: immo.label }
              },
              {
                error: _get(error, 'purchase_date'),
                keyCell: `${immo.id_immo}.date achat`,
                cellProp: {},
                component: isCreated ? TextFieldCell : 'div',
                props: isCreated
                  ? {
                    inputRef: isCreated && isFirstErrorLine && firstErrorPath === 'purchase_date' && inputRef,
                    ...getProps(immo, 'purchase_date', v => empty(v) || invalidDate(v), parseDate, formatDate)
                  }
                  : { children: moment(immo.purchase_date).format('DD/MM/YYYY') }
              },
              {
                error: _get(error, 'start_date'),
                keyCell: `${immo.id_immo}.mise en service`,
                cellProp: {},
                component: isEditingImmo ? TextFieldCell : 'div',
                props: isEditingImmo ? {
                  inputRef: ((!isCreated && isLastSelectedImmo) || (isFirstErrorLine && firstErrorPath === 'start_date')) && inputRef,
                  placeholder: formatDate(immo.start_date),
                  defaultValue: formatDate(immo.start_date),
                  onChange: (event) => {
                    setErrors(immo.id_immo, { start_date: false });
                    editImmo({
                      ...immo,
                      start_date: parseDate(event.target.value)
                    });
                  },
                  onBlur: (event) => {
                    setErrors(
                      immo.id_immo,
                      {
                        start_date: empty(event.target.value)
                          || invalidDate(event.target.value)
                          || (moment(event.target.value, 'DD/MM/YYYY').isBefore(moment(startDate)) && showToEarlyModal(index))
                      }
                    );
                  },
                  value: immo.start_date
                } : {
                  children: moment(immo.start_date).format('DD/MM/YYYY')
                }
              },
              {
                error: _get(error, 'month'),
                keyCell: `${immo.id_immo}.nb mois`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                component: isEditingImmo ? TextFieldCell : 'div',
                props: isEditingImmo ? {
                  inputRef: isFirstErrorLine && firstErrorPath === 'month' && inputRef,
                  ...getProps(immo, 'month', empty || negative, parseFloat),
                  type: 'number'
                } : {
                  children: immo.month
                }
              },
              {
                error: _get(error, 'id_type_amort'),
                keyCell: `${immo.id_immo}.type amort`,
                cellProp: {},
                component: isEditingImmo ? AutoCompleteCell : 'div',
                props: isEditingImmo ? {
                  onChange: ({ value }) => editImmo({
                    ...immo,
                    id_type_amort: value
                  }),
                  value: amortTypes.find(type => type.value === immo.id_type_amort),
                  options: amortTypes
                } : {
                  children: immo.id_type_amort === 1 ? I18n.t('fixedAssets.table.linear') : I18n.t('fixedAssets.table.degressive')
                }
              },
              {
                error: _get(error, 'purchase_value'),
                keyCell: `${immo.id_immo}.achats`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                component: isEditingImmo ? TextFieldCell : 'div',
                props: isEditingImmo ? {
                  inputRef: isFirstErrorLine && firstErrorPath === 'purchase_value' && inputRef,
                  ...getProps(
                    immo,
                    'purchase_value',
                    v => isNaN(v) || empty(v) || negative(v) || (parseFloat(v) > immo.purchase_value && I18n.t('fixedAssets.errors.lessValue')),
                    v => (v ? parseFloat(v) : '')
                  )
                } : {
                  children: formatNumber(immo.purchase_value)
                }
              },
              {
                keyCell: `${immo.id_immo}.amort ant`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: immo.previousDotation
              },
              {
                keyCell: `${immo.id_immo}.dot année`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: immo.currentAnnualDotation
              },
              {
                keyCell: `${immo.id_immo}.amort fin période`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: immo.EndingAnnualDotation
              },
              {
                keyCell: `${immo.id_immo}.vnc`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: immo.netBookValue
              },
              {
                error: _get(error, 'end_date'),
                keyCell: `${immo.id_immo}.date sortie`,
                cellProp: {},
                component: isEditingImmo ? TextFieldCell : 'div',
                props: isEditingImmo
                  ? getProps(immo, 'end_date', invalidDate, parseDate, formatDate)
                  : {
                    children: moment(immo.end_date).format('DD/MM/YYYY')
                  }
              },
              {
                error: _get(error, 'sale_value'),
                keyCell: `${immo.id_immo}.prix vente`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                component: isEditingImmo ? TextFieldCell : 'div',
                props: isEditingImmo ? {
                  ...getProps(immo, 'sale_value', optional || negative, parseFloat),
                  type: 'number'
                } : {
                  children: formatNumber(immo.sale_value)
                }
              },
              {
                keyCell: `${immo.id_immo}.+value`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: immo.capitalGain
              },
              {
                _type: 'icon',
                keyCell: `${immo.id_immo}.commentaire`,
                cellProp: {
                  className: classnames(cellStyle.iconCell, cellStyle.cursorPointer)
                },
                props: {
                  onClick: () => getAndOpenComments(immo.id_immo),
                  name: 'icon-comments'
                }
              },
              {
                component: StatusCell,
                keyCell: `immo.pj${immo.id_immo}`,
                props: {
                  paddingLeft: true,
                  data: [{
                    status: 'pjs',
                    value: _get(immo, 'pj_list.length', 0),
                    pjs: _get(immo, 'pj_list', []),
                    onClick: () => WindowHandler.forceOpen(immo.pj_list)
                  }]
                },
                cellProp: {
                  className: classnames(styles.iconCell, cellStyle.iconCell)
                }
              },
              {
                component: (!immo.isCreated && IconCell),
                keyCell: `fnp.AddPj${immo.id_immo}`,
                props: {
                  name: 'icon-plus',
                  onClick: openPJDialog.bind(null, {
                    location: 'immo',
                    id: immo.id_immo
                  })
                },
                cellProp: {
                  className: classnames(styles.iconCell)
                }
              },
              {
                _type: immo.hasEntry ? 'icon' : 'string',
                keyCell: `${immo.id_immo}.en compta`,
                cellProp: {
                  className: classnames(cellStyle.iconCell, style.green)
                },
                props: {
                  name: 'icon-check'
                }
              }
            ]
          };
        })
      },
      footer: {}
    }
  });
};

export const getDotationsTableFactoryProps = ({
  selectedAccount,
  selectedDotations,
  selectDotation,
  unselectDotation,
  lastSelectedImmo
}) => {
  const dotations = (selectedAccount.arrayImmo || []).find((
    { id_immo }
  ) => id_immo === lastSelectedImmo);
  return {
    className: style.dotationsTable,
    param: {
      header: {
        props: {},
        row: [{
          props: {
            className: classnames(style.headerRow, style.row)
          },
          value: [
            {
              _type: 'checkbox',
              keyCell: 'dotation-header-check',
              cellProp: {
                className: style.checkCell
              },
              props: {}
            },
            {
              _type: 'sortCell',
              keyCell: 'dotation-header-exercice',
              cellProp: {},
              props: {
                children: I18n.t('fixedAssets.table.exercise')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'dotation-header-amort ant',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.previousImmo')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'dotation-header-dot année',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.endowmentYear')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'dotation-header-amort fin période',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.immoPeriodEnding')
              }
            },
            {
              _type: 'sortCell',
              keyCell: 'dotation-header-vnc',
              cellProp: {
                isAmount: true
              },
              props: {
                children: I18n.t('fixedAssets.table.vnc')
              }
            }
          ]
        }]
      },
      body: {
        props: {},
        row: _get(dotations, 'arrayDotation', []).map((dotation) => {
          const isSelected = selectedDotations[dotation.id_amort];
          return {
            keyRow: dotation.id_amort,
            props: {
              isSelected
            },
            value: [
              {
                _type: 'checkbox',
                keyCell: `${dotation.id_amort}.check`,
                cellProp: {
                  className: style.checkCell
                },
                props: {
                  onChange:
                    (_, isChecked) => onSelectItem(
                      isChecked, dotation, selectDotation, unselectDotation
                    ),
                  checked: isSelected
                }
              },
              {
                keyCell: `${dotation.id_amort}.exercice`,
                cellProp: {},
                children: dotation.periode
              },
              {
                _type: 'string',
                keyCell: `${dotation.id_amort}.amort ant`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                props: {
                  label: dotation.previous_value
                }
              },
              {
                keyCell: `${dotation.id_amort}.dot année`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: dotation.periode_value
              },
              {
                keyCell: `${dotation.id_amort}.amort fin période`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: dotation.end_periode_value
              },
              {
                keyCell: `${dotation.id_amort}.vnc`,
                cellProp: {
                  className: cellStyle.numberCell
                },
                children: dotation.vnc
              }
            ]
          };
        })
      },
      footer: {}
    }
  };
};

export const getSalesModalTableFactoryProps = ({
  sales = [],
  isLoading,
  loadData,
  selectedSale,
  selectSale,
  unselectSale
}) => ({
  className: style.accountTable,
  isLoading,
  loadData,
  param: {
    header: {
      props: {},
      row: [{
        props: {
          className: classnames(style.headerRow, style.row)
        },
        value: [
          {
            _type: 'checkbox',
            keyCell: 'account-header-check',
            cellProp: {
              className: style.checkCell
            },
            props: {
              className: cellStyle.iconCell
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'account-header-nocompte',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.saleModal.table.number')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'account-header-date',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.saleModal.table.date')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'account-header-intitule',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.saleModal.table.provider')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'account-header-solde',
            cellProp: {
              isAmount: true
            },
            props: {
              children: I18n.t('fixedAssets.saleModal.table.deb')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'account-header-amort ant',
            cellProp: {
              isAmount: true
            },
            props: {
              children: I18n.t('fixedAssets.saleModal.table.cred')
            }
          }

        ]
      }]
    },
    body: {
      props: {},
      row: sales.reduce((acc, sale) => {
        const isSelected = !!(selectedSale && selectedSale.line_entry_id === sale.line_entry_id);
        return sales.length ? [...acc, {
          keyRow: sale.line_entry_id,
          props: {
            className: style.row
          },
          value: [
            {
              _type: 'checkbox',
              keyCell: `${sale.line_entry_id}.check`,
              cellProp: {
                className: style.checkCell
              },
              props: {
                onChange:
                  (_, isChecked) => onSelectItem(
                    isChecked, sale, selectSale, unselectSale
                  ),
                className: cellStyle.iconCell,
                checked: isSelected
              }
            },
            {
              keyCell: `${sale.line_entry_id}.no compte`,
              cellProp: {},
              children: sale.account.account_id
            },
            {
              keyCell: `${sale.line_entry_id}.dot période`,
              cellProp: {},
              children: sale.deadline
            },
            {
              keyCell: `${sale.line_entry_id}.provider`,
              cellProp: {},
              children: sale.label
            },
            {
              keyCell: `${sale.line_entry_id}.debit`,
              cellProp: {
                className: cellStyle.numberCell
              },
              children: sale.debit
            },
            {
              keyCell: `${sale.line_entry_id}.credit`,
              cellProp: {
                className: cellStyle.numberCell
              },
              children: sale.credit
            }
          ]
        }] : acc;
      }, [])
    },
    footer: {}
  }
});

export const getSalesTableFactoryProps = ({
  sales,
  isLoading,
  selectedSale,
  selectSale,
  unselectSale
}) => ({
  className: style.accountTable,
  isLoading,
  param: {
    header: {
      props: {},
      row: [{
        props: {
          className: classnames(style.headerRow, style.row)
        },
        value: [
          {
            keyCell: 'sales-header-check',
            cellProp: {
              className: style.checkCell
            },
            props: {
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'sales-header-diary',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.table.diary')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'sales-header-purchaseData',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.table.purchaseDate')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'sales-header-piece',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.table.piece')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'sales-header-piece2',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.table.piece2')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'sales-header-number',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.table.number')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'sales-header-label',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.table.label')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'sales-header-debit',
            cellProp: {
              isAmount: true
            },
            props: {
              children: I18n.t('fixedAssets.table.debit')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'sales-header-credit',
            cellProp: {
              isAmount: true
            },
            props: {
              children: I18n.t('fixedAssets.table.credit')
            }
          },
          {
            _type: 'sortCell',
            keyCell: 'sales-header-commentShort',
            cellProp: {},
            props: {
              children: I18n.t('fixedAssets.table.commentShort')
            }
          }
        ]
      }]
    },
    body: {
      props: {},
      row: sales.entry_list.reduce((acc, sale) => {
        const isSelected = !!(selectedSale && selectedSale.line_entry_id === sale.line_entry_id);
        return [...acc, {
          keyRow: sale.line_entry_id,
          props: {
            className: style.row
          },
          value: [
            {
              _type: 'checkbox',
              keyCell: `${sale.id_compte}.check`,
              cellProp: {
                className: style.checkCell
              },
              props: {
                onChange:
                  (_, isChecked) => onSelectItem(
                    isChecked, sale, selectSale, unselectSale
                  ),
                className: cellStyle.iconCell,
                checked: isSelected
              }
            },
            {
              keyCell: `${sale.id_compte}.diary`,
              cellProp: {},
              children: sales.diary.code
            },
            {
              keyCell: `${sale.id_compte}.purchaseData`,
              cellProp: {
                className: cellStyle.numberCell
              },
              children: sale.deadline
            },
            {
              keyCell: `${sale.id_compte}.piece`,
              cellProp: {
                className: cellStyle.numberCell
              },
              children: sale.piece
            },
            {
              keyCell: `${sale.id_compte}.piece2`,
              cellProp: {
                className: cellStyle.numberCell
              },
              children: sale.piece2
            },
            {
              keyCell: `${sale.id_compte}.number`,
              cellProp: {},
              children: sale.account.account_id
            },
            {
              keyCell: `${sale.id_compte}.label`,
              cellProp: {},
              children: sale.label
            },
            {
              keyCell: `${sale.id_compte}.debit`,
              cellProp: {},
              children: sale.debit
            },
            {
              keyCell: `${sale.id_compte}.credit`,
              cellProp: {},
              children: sale.credit
            },
            {
              _type: 'icon',
              keyCell: `${sale.id_immo}.commentaire`,
              cellProp: {},
              props: {
                onClick: () => {},
                name: 'icon-comments'
              }
            }
          ]
        }];
      }, [])
    },
    footer: {}
  }
});
