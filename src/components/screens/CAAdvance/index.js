import React, { useEffect, useState } from 'react';

import { Tabs, Tab } from '@material-ui/core';
import get from 'lodash/get';
import PropTypes from 'prop-types';

import { useWidth } from 'helpers/hooks';

import { CycleHeader } from 'containers/groups/Headers';
import CA12AC3514 from 'containers/groups/Forms/CA/Advance/CA12AC3514';
import TIDENTIF from 'containers/groups/Forms/CA/Advance/TIDENTIF';

import styles from './CAAdvance.module.scss';

const existDocs = [
  {
    component: <CA12AC3514 />,
    label: '3514',
    form: 'ca12Advance_T-IDENTIF'
  },
  {
    component: <TIDENTIF />,
    label: 'T-IDENTIF',
    form: 'ca12Advance_T-IDENTIF'
  }
];

// Component
const CAAdvance = (props) => {
  // State
  const [selectedTab, setSelectedTab] = useState(0);
  const [factor, setFactor] = useState(1);
  const { sendCycleForms, selectForm, selectedFormId } = props;

  useEffect(() => {
    selectForm(selectedFormId);
  }, [selectedFormId]);

  // Functions
  const handleChange = async (e, value) => {
    await sendCycleForms();
    await selectForm(existDocs[value].label);
    await setSelectedTab(value);
  };

  const factorCompute = (width) => {
    if (width < 1200) {
      setFactor(width / 1200);
    } else {
      setFactor(1);
    }
  };

  // Callbacks
  const containerCb = useWidth(factorCompute);

  // Rendering
  const pdfStyle = {};
  if (factor !== 1) {
    pdfStyle.transform = `scale(${factor})`;
  }

  const component = get(existDocs[selectedTab], 'component');
  const form = get(existDocs[selectedTab], 'form');

  return (
    <div>
      <div className={styles.cycleHeaderWrap}>
        <CycleHeader cycle_code="CA12_AC" code_sheet_group="CA12-ACOMPTE" cycleForm={form} />
        <Tabs
          value={selectedTab}
          indicatorColor="primary"
          textColor="primary"
          onChange={handleChange}
          classes={{
            root: styles.tabs
          }}
        >
          {
            existDocs.map((doc, key) => <Tab key={`${key}-${doc.label}`} label={doc.label} value={key} />)
          }
        </Tabs>
      </div>
      <div ref={containerCb} className={styles.pdfContainer}>
        <div className={styles.pdf} style={pdfStyle}>
          {component}
        </div>
      </div>
    </div>
  );
};

export default CAAdvance;

CAAdvance.propTypes = {
  sendCycleForms: PropTypes.func.isRequired,
  selectedFormId: PropTypes.string.isRequired,
  selectForm: PropTypes.func.isRequired
};
