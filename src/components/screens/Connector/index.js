import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import ConnectorTable from 'containers/groups/Tables/Connector';
import { ConnectorDialog } from 'containers/groups/Dialogs';
import I18n from 'assets/I18n';
import _ from 'lodash';
import DeleteConfirmation from 'containers/groups/Dialogs/DeleteConfirmation';
import styles from './connector.module.scss';

const Connector = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isCreation, setIsCreation] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);

  const onCloseDeleteDialog = () => setIsDeleteOpen(false);

  const createConnector = () => {
    setIsOpen(true);
    setIsCreation(true);
  };

  const modifyConnector = () => {
    const { initializeConnectorForm, list_selectedConnector } = props;
    setIsOpen(true);
    setIsCreation(false);
    initializeConnectorForm({
      id_connector: list_selectedConnector[list_selectedConnector.length - 1].id_connector,
      code: list_selectedConnector[list_selectedConnector.length - 1].code,
      complementary_information: list_selectedConnector[list_selectedConnector.length - 1].complementary_information, // eslint-disable-line
      id_society: list_selectedConnector[list_selectedConnector.length - 1].id_society,
      id_third_party_api: list_selectedConnector[list_selectedConnector.length - 1].id_third_party_api // eslint-disable-line
    });
  };

  const closeDialog = () => {
    setIsOpen(false);
    setIsCreation(false);
  };

  const updateConnector = async () => {
    const { getConnector } = props;
    await getConnector();
  };

  const deleteSelectedConnector = async () => {
    const {
      societyId, list_selectedConnector, deleteConnector, getConnector, resetSelectedList
    } = props;
    await deleteConnector(list_selectedConnector);
    await resetSelectedList(societyId);
    await getConnector();
  };

  useEffect(() => { updateConnector(); }, []);

  const {
    list_selectedConnector,
    disabledExport,
    csvData,
    baseFileName
  } = props;

  return (
    <div className={styles.ConnectorContainer}>
      <div className={styles.ConnectorHeader}>
        <Typography variant="h1">{I18n.t('connector.title')}</Typography>
        <div>
          <InlineButton
            marginDirection="right"
            buttons={[
              {
                _type: 'icon',
                iconName: 'icon-popup',
                iconSize: 28,
                variant: 'none',
                iconColor: 'black',
                titleInfoBulle: I18n.t('tooltips.newWindow'),
                onClick: () => {}
              },
              {
                _type: 'icon',
                iconName: 'icon-close',
                iconSize: 28,
                variant: 'none',
                iconColor: 'black',
                titleInfoBulle: I18n.t('tooltips.close'),
                onClick: () => {}
              }
            ]}
          />
        </div>
      </div>
      <div className={styles.buttonsRow}>
        <InlineButton
          marginDirection="right"
          buttons={[
            {
              _type: 'string',
              text: I18n.t('connector.newConnector'),
              color: 'primary',
              size: 'medium',
              onClick: createConnector
            },
            {
              _type: 'string',
              text: I18n.t('connector.modifyConnector'),
              color: 'primary',
              size: 'medium',
              onClick: modifyConnector,
              disabled: _.isEmpty(list_selectedConnector)
            },
            {
              _type: 'string',
              text: I18n.t('connector.deleteConnector'),
              color: 'error',
              size: 'medium',
              onClick: () => setIsDeleteOpen(true),
              disabled: _.isEmpty(list_selectedConnector)
            },
            {
              _type: 'component',
              color: 'primary',
              size: 'medium',
              disabled: disabledExport,
              component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
            }
          ]}
        />
      </div>
      <ConnectorTable />
      {
        isOpen && (
          <ConnectorDialog
            isOpen={isOpen}
            creationMode={isCreation}
            onClose={closeDialog}
          />
        )
      }
      <DeleteConfirmation
        deleteReportAndForm={deleteSelectedConnector}
        isOpen={isDeleteOpen}
        onClose={onCloseDeleteDialog}
      />
    </div>
  );
};

Connector.propTypes = {
  deleteConnector: PropTypes.func.isRequired,
  getConnector: PropTypes.func.isRequired,
  initializeConnectorForm: PropTypes.func.isRequired,
  list_selectedConnector: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  societyId: PropTypes.number.isRequired,
  resetSelectedList: PropTypes.func.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired
};

export default Connector;
