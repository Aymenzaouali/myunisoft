import React, { Fragment, useEffect } from 'react';
import BankLink from 'containers/groups/BankLink';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import Split from 'containers/groups/Split';
import NewAccountingForSplit from 'components/groups/NewAccountingForSplit';
import styles from './banklink.module.scss';

const BankLinkScreen = (props) => {
  const {
    societyId, getExercice, getBankLink
  } = props;

  const slaves = {
    newAccounting: {
      label: I18n.t('split.accounting'),
      url: `/tab/${societyId}/accounting/new`,
      component: () => <NewAccountingForSplit />,
      swapKeepArgs: true,
      popupable: false
    }
  };

  const master_args = {
    showSplitToolbar: false
  };

  const args = {
    getBankLink
  };

  useEffect(() => {
    getExercice();
  }, []);

  return (
    <Fragment>
      <Split
        master_id="bankLink"
        default_args={args}
        master_args={master_args}
        slaves={slaves}
      >
        <div className={styles.container}>
          <BankLink />
        </div>
      </Split>
    </Fragment>
  );
};

BankLinkScreen.propTypes = {
  societyId: PropTypes.number.isRequired,
  getExercice: PropTypes.func.isRequired,
  getBankLink: PropTypes.func.isRequired
};

export default BankLinkScreen;
