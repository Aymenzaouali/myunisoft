import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { PlansHeader } from 'containers/groups/Headers';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import Split from 'containers/groups/Split';
import PlansTable from 'containers/groups/Tables/Plans';
import PlanDetailsContainer from 'containers/groups/PlanDetails';
import { PlanDialog, DeleteConfirmation } from 'containers/groups/Dialogs';

import I18n from 'assets/I18n';
import styles from './plans.module.scss';

const PlansScreen = (props) => {
  const {
    plan,
    selectedPlans,
    planSelectedLine,
    deleteAccountingPlans,
    disabledExport,
    csvData,
    baseFileName
  } = props;

  const [isDialogOpen, toggleDialog] = useState(false);
  const [modify, toggleModify] = useState(false);
  const [isDeleteOpen, toggleDelete] = useState(false);

  const initPlan = async () => {
    const { initializePlanData } = props;

    await initializePlanData(plan);
  };

  const closeDialog = () => {
    toggleDialog(false);
    toggleModify(false);
  };

  const closeDeleteDialog = () => toggleDelete(false);

  const disabledModify = planSelectedLine === null;
  const disabledDell = selectedPlans.length === 0;

  const slaves = {
    accounting: {
      label: I18n.t('split.accounting'),
      url: '/tab/collab/accountingPlans',
      component: () => <PlanDetailsContainer />,
      swapKeepArgs: true
    }
  };

  return (
    <>
      <Split master_id="accounting_plans" slaves={slaves}>
        <div className={styles.container}>
          <PlansHeader />
          <div className={styles.headerRightButton}>
            <InlineButton
              marginDirection="right"
              buttons={[
                {
                  _type: 'string',
                  text: I18n.t('accountingPlans.button.newPlan'),
                  onClick: () => {
                    toggleDialog(true);
                    toggleModify(false);
                  }
                },
                {
                  _type: 'string',
                  text: I18n.t('accountingPlans.button.changePlan'),
                  disabled: disabledModify,
                  onClick: async () => {
                    await initPlan();
                    toggleDialog(true);
                    toggleModify(true);
                  }
                },
                {
                  _type: 'string',
                  text: I18n.t('accountingPlans.button.deletePlan'),
                  color: 'error',
                  disabled: disabledDell,
                  onClick: () => {
                    toggleDelete(true);
                  }
                },
                {
                  _type: 'component',
                  color: 'primary',
                  disabled: disabledExport,
                  component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
                }
              ]}
            />
          </div>
          <PlansTable />
        </div>
      </Split>
      {
        isDialogOpen
        && (
          <PlanDialog
            isOpen={isDialogOpen}
            onClose={closeDialog}
            modify={modify}
          />
        )
      }
      <DeleteConfirmation
        deleteReportAndForm={deleteAccountingPlans}
        isOpen={isDeleteOpen}
        message={I18n.t('accountingPlans.dialogs.delete.message')}
        onClose={closeDeleteDialog}
      />
    </>
  );
};

PlansScreen.propTypes = {
  deleteAccountingPlans: PropTypes.func.isRequired,
  initializePlanData: PropTypes.func.isRequired,
  selectedPlans: PropTypes.array,
  planSelectedLine: PropTypes.number,
  disabledExport: PropTypes.bool,
  csvData: PropTypes.array,
  baseFileName: PropTypes.string,
  plan: PropTypes.shape({
    label: PropTypes.string,
    description: PropTypes.string
  })
};

PlansScreen.defaultProps = {
  selectedPlans: [],
  planSelectedLine: null,
  disabledExport: false,
  csvData: [],
  baseFileName: 'unnamed',
  plan: {}
};

export default PlansScreen;
