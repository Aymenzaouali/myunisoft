import React, { Fragment, useEffect, useState } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { Typography } from '@material-ui/core';

import I18n from 'assets/I18n';
import { ReactComponent as Logo } from 'assets/images/logo.svg';

import { routesByKey } from 'helpers/routes';
import { lowercaseFormat } from 'helpers/format';
import { isChromeBrowser } from 'helpers/browser';

import DialogGroupContainer from 'containers/groups/Dialogs/Groups';
import CGUDialog from 'containers/groups/Dialogs/CGU';

import Version from 'components/groups/Version';
import Button from 'components/basics/Buttons/Button';
import { ReduxSecureTextField, ReduxTextField } from 'components/reduxForm/Inputs';
import ReduxCheckBox from 'components/reduxForm/Selections/ReduxCheckBox';

import styles from './Login.module.scss';

// Component
const Login = (props) => {
  const {
    onSubmit,
    push,
    handleSubmit,
    cgu,
    allowData,
    isActiveSession
  } = props;

  // State
  const [isGroupOpen, setGroupOpen] = useState(false);
  const [isCguOpen, setCguOpen] = useState(false);
  const [isPermissionChecked, setPermissionChecked] = useState('');

  // Effects
  useEffect(() => {
    if (isActiveSession && isChromeBrowser()) {
      push(routesByKey.dashboardCollab);
    }
  }, [isActiveSession, push]);

  // Functions
  const submit = async (payload) => {
    if (cgu && allowData) {
      const groups = await onSubmit(payload);
      if (groups.length > 1) {
        setGroupOpen(true);
      }
    }
    setPermissionChecked(true);
  };

  const close = () => {
    setGroupOpen(false);
  };

  const recoveryPasswordPush = () => push('/forgot_password');

  // Rendering
  return (
    <div className={styles.container}>
      <div className={classNames(styles.half, styles.logo)}>
        <Logo fill="white" height="100px" />
      </div>
      <div className={classNames(styles.half, styles.form)}>
        <Typography variant="h1">{I18n.t('login.title')}</Typography>
        <form onSubmit={handleSubmit(submit)}>
          <div className={styles.inputs}>
            <Field
              name="mail"
              component={ReduxTextField}
              format={lowercaseFormat}
              id="mail"
              label={I18n.t('login.mail')}
              fullWidth
            />
            <Field
              name="password"
              component={ReduxSecureTextField}
              id="password"
              label={I18n.t('login.password')}
              fullWidth
            />

            <button
              onClick={recoveryPasswordPush}
              className={classNames(styles.btnLink, styles.forgottenLink)}
              type="button"
            >
              {I18n.t('login.forgottenPassword')}
            </button>

            <Button
              type="submit"
              size="large"
              variant="contained"
              color="primary"
              classes={{ sizeLarge: styles.submitBtn }}
            >
              {I18n.t('login.connect')}
            </Button>
            {
              (isPermissionChecked && !(cgu && allowData))
              && (
                <Typography
                  color="error"
                >
                  {I18n.t('login.errorMessages.cguConfirm')}
                </Typography>
              )
            }
          </div>

          <div className={styles.checkbox}>
            <Field
              color="primary"
              name="cgu"
              checked={cgu}
              component={ReduxCheckBox}
              label={(
                <Fragment>
                  {I18n.t('login.cgu.pre')}
                  <button
                    className={styles.btnLink}
                    type="button"
                    onClick={() => setCguOpen(true)}
                  >
                    {I18n.t('login.cgu.link')}
                  </button>
                </Fragment>
              )}
            />
          </div>

          <div className={styles.checkbox}>
            <Field
              color="primary"
              name="allowData"
              component={ReduxCheckBox}
              label={I18n.t('login.allowData')}
            />
          </div>
        </form>
        <Version />
      </div>
      <DialogGroupContainer
        handleSubmit={handleSubmit}
        isOpen={isGroupOpen}
        onClose={close}
      />
      <CGUDialog
        isOpen={isCguOpen}
        onClose={() => setCguOpen(false)}
        fullWidth
        maxWidth="lg"
      />
    </div>
  );
};

// Props
Login.propTypes = {
  isActiveSession: PropTypes.bool,
  push: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  cgu: PropTypes.bool,
  allowData: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired
};

Login.defaultProps = {
  cgu: false,
  allowData: false,
  isActiveSession: false
};

export default Login;
