import React, { useState } from 'react';
import { RentDeclarations } from 'containers/groups/Headers';
import { Tabs, Tab } from '@material-ui/core';

import { useWidth } from 'helpers/hooks';

import Decloyer from 'containers/groups/Forms/RentDeclarations/Decloyer';

import styles from './RentDeclarations.module.scss';

const existDocs = ['DEC LOYER'];

const RentDeclarationsScreen = () => {
  // State
  const [selectedTab, setSelectedTab] = useState(0);
  const [factor, setFactor] = useState(1);

  // Functions
  const handleChange = (e, value) => {
    setSelectedTab(value);
  };

  const factorCompute = (width) => {
    if (width < 1200) {
      setFactor(width / 1200);
    } else {
      setFactor(1);
    }
  };

  // Callbacks
  const containerCb = useWidth(factorCompute);

  // Rendering
  const pdfStyle = {};
  if (factor !== 1) {
    pdfStyle.transform = `scale(${factor})`;
  }


  return (
    <div>
      <RentDeclarations />
      <Tabs
        value={selectedTab}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        classes={{
          scrollButtons: styles.scrollButtons,
          root: styles.tabs
        }}
      >
        {
          existDocs.map((doc, key) => <Tab key={`${key}-${doc}`} label={doc} value={key} />)
        }
      </Tabs>
      <div ref={containerCb} className={styles.pdfContainer}>
        <div className={styles.pdf} style={pdfStyle}>
          {selectedTab === 0 && <Decloyer />}
        </div>
      </div>
    </div>
  );
};

export default RentDeclarationsScreen;
