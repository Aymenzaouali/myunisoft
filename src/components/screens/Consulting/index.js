import React, { useRef } from 'react';
import PropTypes from 'prop-types';

import I18n from 'assets/I18n';

import AccountingForSplit from 'components/groups/AccountingForSplit';
import Consulting from 'containers/groups/Consulting';
import Split from 'containers/groups/Split';

// Component
const ConsultingScreen = (props) => {
  const { societyId, account } = props;
  const consultingRef = useRef(null);
  const slaves = {
    accounting: {
      label: I18n.t('split.accounting'),
      url: `/tab/${societyId}/accounting`,
      component: () => <AccountingForSplit />,
      swapKeepArgs: true
    }
  };

  const master_args = {
    isNotEcriture: false
  };

  const args = {
    account,
    showNewAccounting: false
  };

  return (
    <div ref={consultingRef}>
      <Split
        master_id="consulting"
        default_args={args}
        master_args={master_args}
        slaves={slaves}
      >
        <Consulting consultingRef={consultingRef} />
      </Split>
    </div>
  );
};

// Props
ConsultingScreen.propTypes = {
  societyId: PropTypes.number.isRequired,
  account: PropTypes.object.isRequired
};

export default ConsultingScreen;
