import React from 'react';
import Split from 'containers/groups/Split';
import AccountingFirmSettings from 'containers/groups/AccountingFirmSettings';
import AccountingFirmCreation from 'containers/groups/AccountingFirmCreation';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';

const AccountingFirmSettingsScreen = ({ societyId, lastSelectedRow }) => {
  const slaves = {
    consulting: {
      label: I18n.t('split.consulting'),
      url: `/tab/${societyId}/AccountingFirmSettingsScreenBody`,
      component: () => <AccountingFirmCreation />,
      swappable: false,
      swapKeepArgs: true
    }
  };
  return (
    <Split
      master_id="accountingFirmSettings"
      slaves={slaves}
      default_args={{ lastSelectedRow }}
    >
      <AccountingFirmSettings />
    </Split>
  );
};

AccountingFirmSettingsScreen.propTypes = {
  societyId: PropTypes.number.isRequired,
  lastSelectedRow: PropTypes.number.isRequired
};

export default AccountingFirmSettingsScreen;
