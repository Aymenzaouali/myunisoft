import React from 'react';
import { BankIntegrationSettingsHeader } from 'containers/groups/Headers';
import { BankIntegrationSettingsTable } from 'containers/groups/Tables';
import styles from './bankIntegrationSettings.module.scss';

const BankIntegrationSettings = () => (
  <div className={styles.container}>
    <BankIntegrationSettingsHeader />
    <BankIntegrationSettingsTable />
  </div>
);


export default BankIntegrationSettings;
