import React from 'react';
import PropTypes from 'prop-types';
import { DocumentManagementHeaderContainer as DMHeaderContainer } from 'containers/groups/Headers';
import { CopyDocumentToDiscussion } from 'containers/groups/Dialogs';
import { DMSidebar, DocTable } from 'containers/groups/DocManagement';
import I18n from 'assets/I18n';
import Split from 'containers/groups/Split';
import AccountingForSplit from 'components/groups/AccountingForSplit';
import Consulting from 'containers/groups/Consulting';
import styles from './documentManagement.module.scss';

const DocumentManagement = ({
  activeTabId, societyId, account, showSendInDiscussionModal, isShowSendInDiscussionModal, history
}) => {
  const slaves = {
    accounting: {
      label: I18n.t('split.accounting'),
      url: `/tab/${societyId}/accounting`,
      component: () => <AccountingForSplit />,
      swapKeepArgs: true
    },
    consulting: {
      label: I18n.t('split.consulting'),
      url: `/tab/${societyId}/consulting`,
      component: () => <Consulting />,
      swapKeepArgs: true,
      args: {
        isNotEcriture: false,
        isEntriesSelect: true
      }
    }
  };

  const master_args = {
    isNotEcriture: false
  };

  const args = {
    account,
    showNewAccounting: false
  };

  return (
    <Split
      master_id="document-management"
      default_args={args}
      master_args={master_args}
      slaves={slaves}
    >
      <DMHeaderContainer className={styles.container} />
      <div
        style={{
          marginTop: '10px',
          padding: '10px',
          display: 'flex'
        }}
        className={styles.container}
      >
        {activeTabId !== 0 && <DMSidebar />}
        <div className={styles.tableContainer}>
          <DocTable />
        </div>
      </div>
      <CopyDocumentToDiscussion
        isOpen={isShowSendInDiscussionModal}
        history={history}
        onClose={() => showSendInDiscussionModal(false)}
        title={I18n.t('ged.selectDiscussion')}
      />
    </Split>
  );
};

DocumentManagement.propTypes = {
  account: PropTypes.object.isRequired,
  history: PropTypes.array.isRequired,
  showSendInDiscussionModal: PropTypes.func.isRequired,
  isShowSendInDiscussionModal: PropTypes.bool.isRequired,
  activeTabId: PropTypes.number.isRequired,
  societyId: PropTypes.number.isRequired
};

export default DocumentManagement;
