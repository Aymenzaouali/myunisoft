import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Typography, withStyles } from '@material-ui/core';
import { InlineButton } from 'components/basics/Buttons';
import { Field } from 'redux-form';
import {
  ReduxSelect, ReduxRadio, ReduxFile
} from 'components/reduxForm/Selections';
import FilesDropper from 'components/groups/FilesDropper';
import FilesInformations from 'components/groups/FilesInformations';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import Loader from 'components/basics/Loader';
import I18n from 'assets/I18n';
import _ from 'lodash';
import styles from './importScreen.module.scss';

const ImportScreen = (props) => {
  const {
    classes,
    typeOptions, sourceOptions, memberOptions, writingTypes, import_source,
    formValues = {},
    handleSubmit, onSubmitImportForm,
    getMemberCompanies,
    getSociety,
    importSuccess,
    exercices,
    formats,
    setSociety,
    societies,
    files, addEbics, removeEbics,
    importedRows,
    onTypeChange,
    // addExcel, removeExcel, excelFiles,
    isLoading,
    hideImportTypeComponent,
    hideSelectSocietyComponent,
    hasContainerStyles,
    onExerciceChange,
    onSocietyChange,
    exerciceInfos: {
      hasEntries
    },
    changeImportMode,
    onExerciseIdChanged
  } = props;

  const {
    type,
    source,
    member_code,
    society_code,
    member_id,
    filename,
    exerciseId,
    importedFile,
    format,
    society_id,
    writingType
  } = formValues;

  useEffect(() => {
    getSociety();
    getMemberCompanies();
  }, []);

  useEffect(() => {
    if (formValues.exerciseId) {
      onExerciseIdChanged();
    }
  }, [formValues.exerciseId]);

  const getDropzoneForEbics = () => ({
    disableClick: true,
    onDrop: files => addEbics(files)
  });

  // const getDropzoneForExcel = () => ({
  //   disableClick: true,
  //   onDrop: files => addExcel(files)
  // });

  const isUseable = !(
    (type === 0 && source === '0' && member_code !== undefined && society_code !== undefined)
    || (type === 0 && source === '1' && member_id !== undefined && filename !== undefined)
    || (type === 1 && exerciseId !== undefined && importedFile !== undefined)
    || (type === 2 && !_.isEmpty(files))
    || (type === 4 && importedFile !== undefined && format)
    || (type === 5 && society_id !== -2 && importedFile !== undefined)
    || (type === 6 && importedFile !== undefined)
  );

  let buttons = [
    {
      _type: 'string',
      text: I18n.t('imports.buttons.import'),
      type: 'submit',
      disabled: isUseable,
      onClick: () => changeImportMode(0)
    }
  ];

  if (hasEntries) {
    buttons = [
      {
        _type: 'string',
        text: I18n.t('imports.buttons.importAndAdd'),
        type: 'submit',
        disabled: isUseable,
        onClick: () => changeImportMode(1)
      },
      {
        _type: 'string',
        text: I18n.t('imports.buttons.importAndErase'),
        type: 'submit',
        disabled: isUseable,
        onClick: () => changeImportMode(2)
      }
    ];
  }

  const renderComptaSaFilters = () => (
    <div className={styles.marginTop}>
      <Field
        component={ReduxTextField}
        name="member_code"
        type="text"
        label={I18n.t('imports.field.member_code')}
        className={styles.codeField}
        margin="none"
      />
      <Field
        component={ReduxTextField}
        name="society_code"
        type="text"
        label={I18n.t('imports.field.society_code')}
        className={styles.codeField}
        style={{ marginLeft: '20px' }}
        margin="none"
      />
    </div>
  );

  const renderCegidFilters = () => (
    <div className={`${styles.cegidFilter} ${styles.marginTop}`}>
      <Field
        label={I18n.t('imports.field.member_group')}
        name="member_id"
        className={styles.typeField}
        component={ReduxSelect}
        list={memberOptions}
        margin="none"
      />
      <Field
        name="filename"
        component={ReduxFile}
        inputId="importedTRA"
      />
    </div>
  );

  const renderEbicsFilters = () => (
    <div className={styles.ebicsFilters}>
      <FilesDropper
        panes={[
          {
            dropzoneConfig: getDropzoneForEbics(),
            icon: 'upload',
            title: I18n.t('imports.ebisFilters.title')
          }
        ]}
      />
      {!_.isEmpty(files)
        && (
          <div className={styles.filesContainer}>
            <FilesInformations
              files={files}
              handleDeleteFile={removeEbics}
            />
          </div>
        )
      }
    </div>
  );

  const renderFecFilters = () => (
    <div className={styles.subContainer}>
      <div className={styles.societyContainer}>
        {!hideSelectSocietyComponent && (
          <Field
            name="society_id"
            component={ReduxSelect}
            label={I18n.t('imports.fecFilters.field.society')}
            list={societies}
            margin="none"
            shrink
            onChange={event => onSocietyChange(event.target.value)}
            className={styles.societyInput}
          />
        )
        }
      </div>
      <Field
        name="importedFile"
        component={ReduxFile}
        inputId="importedFEC"
      />
      {!_.isEmpty(exercices)
        && (
          <div className={styles.exerciceContainer}>
            <Field
              name="exerciseId"
              component={ReduxSelect}
              label={I18n.t('imports.fecFilters.field.exercices')}
              list={exercices}
              margin="none"
              shrink
              className={styles.exerciceInput}
              input={{
                value: exerciseId,
                onChange: onExerciceChange
              }}
            />
          </div>
        )
      }
      <div className={styles.ReduxRadioContainer}>
        <Typography>{I18n.t('imports.fileOrigin')}</Typography>
        <Field
          name="fecImportFrom"
          color="primary"
          component={ReduxRadio}
          list={import_source}
          row
          className={classes.radio}
        />
      </div>
    </div>
  );

  const renderAssetsFilters = () => (
    <div className={styles.assetsContainer}>
      <div className={styles.societyContainer}>
        <Field
          name="society_id"
          component={ReduxSelect}
          label={I18n.t('imports.assetsFilters.field.society')}
          list={societies}
          margin="none"
          shrink
          onChange={event => setSociety(event.target.value)}
          className={styles.societyInput}
        />
      </div>
      <Field
        name="importedFile"
        component={ReduxFile}
        inputId="importedFEC"
      />
      <Field
        label={I18n.t('imports.assetsFilters.field.importDepuis')}
        name="format"
        className={styles.societyInput}
        component={ReduxSelect}
        list={formats}
        margin="none"
      />
    </div>
  );

  const renderExcelFilters = () => (
    <div className={styles.subContainer}>
      {writingType === 0
      && (
        <div className={styles.societyContainer}>
          <Field
            name="society_id"
            component={ReduxSelect}
            label={I18n.t('imports.fecFilters.field.society')}
            list={societies}
            margin="none"
            shrink
            onChange={event => onSocietyChange(event.target.value)}
            className={styles.societyInput}
          />
        </div>
      )
      }
      <Field
        name="importedFile"
        component={ReduxFile}
        inputId="importedFEC"
      />
      {/* TO BE USED WHEN REQUEST CAN HANDLE PIECES JOINTES */}
      {/* <FilesDropper
        panes={[
          {
            dropzoneConfig: getDropzoneForExcel(),
            icon: 'upload',
            title: 'Faites glisser vos pièces-jointes'
          }
        ]}
      />
      {!_.isEmpty(excelFiles)
        && (
          <div className={styles.filesContainer}>
            <FilesInformations
              files={excelFiles}
              handleDeleteFile={removeExcel}
            />
          </div>
        )
      } */}
    </div>
  );

  return (
    <div className={hasContainerStyles && styles.container}>
      <div className={styles.headerTitle}>
        <Typography variant="h1">
          {I18n.t('imports.title')}
        </Typography>
      </div>
      <form onSubmit={handleSubmit(onSubmitImportForm)}>
        <div>
          { hasEntries && (
            <p className={styles.warning}>{I18n.t('imports.hasEntries')}</p>
          )
          }
          {!hideImportTypeComponent
          && (
            <Field
              label={I18n.t('imports.field.type')}
              name="type"
              className={styles.typeField}
              component={ReduxSelect}
              list={typeOptions}
              margin="none"
              onChange={onTypeChange}
            />
          )
          }
          {
            type === 0 && (
              <Field
                color="primary"
                name="source"
                component={ReduxRadio}
                label={I18n.t('imports.field.source')}
                list={sourceOptions}
                row
              />
            )
          }
          {
            type === 5 && (
              <Field
                color="primary"
                name="writingType"
                component={ReduxSelect}
                label="Types d'écritures"
                list={writingTypes}
                row
                margin="none"
              />
            )
          }
        </div>
        <div className={styles.filtersRow}>
          { type === 0 && source === '0' && renderComptaSaFilters() }
          { type === 0 && source === '1' && renderCegidFilters() }
          { type === 1 && renderFecFilters() }
          { type === 2 && renderEbicsFilters() }
          { type === 4 && renderAssetsFilters() }
          { type === 5 && renderExcelFilters() }
          <div className={styles.importButtons}>
            <InlineButton
              buttons={buttons}
            />
          </div>
        </div>
      </form>
      {isLoading && <Loader />}
      {importSuccess && <Typography variant="h6" color="primary">{I18n.t('imports.import_success')}</Typography>}
      {importSuccess && importedRows && (
        <Typography variant="h6" color="primary">
          {`${importedRows} ${I18n.t('imports.rows_number')}`}
        </Typography>
      )}
    </div>
  );
};

const ownStyles = () => ({
  importButton: {
    marginLeft: '20px',
    marginTop: '24px'
  }
});

ImportScreen.defaultProps = {
  formValues: {},
  importSuccess: false,
  importedRows: undefined,
  hideImportTypeComponent: false,
  hideSelectSocietyComponent: false,
  hasContainerStyles: true,
  exerciceInfos: {}
};

ImportScreen.propTypes = {
  files: PropTypes.arrayOf(PropTypes.object).isRequired,
  addEbics: PropTypes.func.isRequired,
  removeEbics: PropTypes.func.isRequired,
  getSociety: PropTypes.func.isRequired,
  setSociety: PropTypes.func.isRequired,
  importSuccess: PropTypes.bool,
  societies: PropTypes.arrayOf(PropTypes.object).isRequired,
  typeOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
  writingTypes: PropTypes.arrayOf(PropTypes.object).isRequired,
  sourceOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
  memberOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
  formValues: PropTypes.object,
  handleSubmit: PropTypes.func.isRequired,
  onSubmitImportForm: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  getMemberCompanies: PropTypes.func.isRequired,
  exercices: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  formats: PropTypes.arrayOf({}).isRequired,
  import_source: PropTypes.arrayOf({}).isRequired,
  importedRows: PropTypes.number,
  onTypeChange: PropTypes.func.isRequired,
  hideImportTypeComponent: PropTypes.bool,
  hideSelectSocietyComponent: PropTypes.bool,
  hasContainerStyles: PropTypes.bool,
  onSocietyChange: PropTypes.func.isRequired,
  onExerciceChange: PropTypes.func.isRequired,
  exerciceInfos: PropTypes.shape({}),
  changeImportMode: PropTypes.func.isRequired,
  onExerciseIdChanged: PropTypes.func.isRequired
};

export default withStyles(ownStyles)(ImportScreen);
