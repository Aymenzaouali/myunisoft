import React, { useState } from 'react';

import { Tabs, Tab } from '@material-ui/core';

import { useWidth } from 'helpers/hooks';

import { TaxDeclarationHeader } from 'containers/groups/Headers';
import Tax2573 from 'containers/groups/Forms/CIRefund/Tax2573';

import I18n from 'assets/I18n';

import styles from './CIRefund.module.scss';

// List of exist implemented PDF
const existDocs = [
  {
    component: <Tax2573 />,
    label: '2573'
  }
];

// Component
const CIRefundScreen = () => {
  // State
  const [selectedTab, setSelectedTab] = useState(0);
  const [factor, setFactor] = useState(1);

  // Functions
  const handleChange = (e, value) => {
    setSelectedTab(value);
  };

  const factorCompute = (width) => {
    if (width < 1200) {
      setFactor(width / 1200);
    } else {
      setFactor(1);
    }
  };

  // Callbacks
  const containerCb = useWidth(factorCompute);

  // Rendering
  const pdfStyle = {};
  if (factor !== 1) {
    pdfStyle.transform = `scale(${factor})`;
  }

  return (
    <div className={styles.container}>
      <div className={styles.ciRefundHeaderWrap}>
        <TaxDeclarationHeader title={I18n.t('drawer.refundTaxCredit')} code_sheet_group="REMBOURSEMENT-IS" />
        <Tabs
          value={selectedTab}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          classes={{
            scrollButtons: styles.scrollButtons,
            root: styles.tabs
          }}
        >
          {
            existDocs.map((doc, key) => <Tab key={`${key}-${doc.label}`} label={doc.label} value={key} />)
          }
        </Tabs>
      </div>
      <div ref={containerCb} className={styles.pdfContainer}>
        <div className={styles.pdf} style={pdfStyle}>
          {selectedTab === 0 && <Tax2573 />}
        </div>
      </div>
    </div>
  );
};

export default CIRefundScreen;
