import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { Typography } from '@material-ui/core';
import LetterEditor from 'containers/groups/LetterEditor';
import { InlineButton } from 'components/basics/Buttons';
import {
  PhysicalPersonAutoComplete, ParagraphTypeAutoComplete, BilanAutoComplete, ParagraphAutoComplete
} from 'containers/reduxForm/Inputs';
import { Field } from 'redux-form';
import styles from './newLetter.module.scss';

const NewLetter = (props) => {
  const {
    match, // eslint-disable-line
    loadData,
    autofill, // eslint-disable-line
    // lettersForm,
    onSelectParagraph,
    onSave,
    handleSubmit // eslint-disable-line
  } = props;

  const [caret, setCaret] = useState({
    page: 0,
    carretPosition: 0
  });

  useEffect(() => {
    (async function getLetter() { // eslint-disable-line
      try {
        const { idLetter } = match.params;
        const letter = await loadData(idLetter);
        autofill('type', letter.type);
        autofill('object', letter.object);
      } catch (err) {
        console.log(err);
      }
    })();
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.title}>
        <Typography variant="h1">{I18n.t('letters.newLetter')}</Typography>
      </div>
      <div className={styles.editContainer}>
        <div className={styles.editor}>
          <LetterEditor setCaret={setCaret} />
        </div>
        <div className={styles.editTools}>
          <div className={styles.toolsButtons}>
            <InlineButton
              buttons={[
                {
                  _type: 'string',
                  text: I18n.t('common.history'),
                  variant: 'none'
                },
                {
                  _type: 'icon',
                  iconName: 'icon-download1',
                  iconSize: 18,
                  variant: 'none',
                  iconColor: 'black'
                },
                {
                  _type: 'icon',
                  iconName: 'icon-save',
                  iconSize: 18,
                  onClick: handleSubmit(onSave)
                },
                {
                  _type: 'icon',
                  iconName: 'icon-print',
                  variant: 'none',
                  iconSize: 30,
                  iconColor: 'black'
                },
                {
                  _type: 'string',
                  text: I18n.t('common.send')
                }
              ]}
            />
          </div>
          <Field
            name="sendTo"
            component={PhysicalPersonAutoComplete}
            placeholder=""
            textFieldProps={{
              label: I18n.t('common.dest'),
              InputLabelProps: {
                shrink: true
              }
            }}
          />
          <Field
            name="paragraph_type"
            component={ParagraphTypeAutoComplete}
            placeholder=""
            textFieldProps={{
              label: I18n.t('letters.new.type'),
              InputLabelProps: {
                shrink: true
              }
            }}
          />
          <Field
            name="bilan"
            component={BilanAutoComplete}
            placeholder=""
            textFieldProps={{
              label: I18n.t('letters.new.linkedTo'),
              InputLabelProps: {
                shrink: true
              }
            }}
          />
          <Field
            name="paragraph"
            component={ParagraphAutoComplete}
            placeholder=""
            textFieldProps={{
              label: I18n.t('letters.new.paraph'),
              InputLabelProps: {
                shrink: true
              }
            }}
            onChangeValues={(val) => {
              onSelectParagraph(val, caret);
            }}
          />
        </div>
      </div>
    </div>
  );
};

NewLetter.propTypes = {
  loadData: PropTypes.func.isRequired,
  onSelectParagraph: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired
};

export default NewLetter;
