import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';

import { PlanDetailsHeader } from 'containers/groups/Headers';
import { InlineButton } from 'components/basics/Buttons';
import PlanDetailsTable from 'containers/groups/Tables/PlanDetails';
import { PlanDetailDialog, DeleteConfirmation } from 'containers/groups/Dialogs';

import I18n from 'assets/I18n';
import styles from './planDetails.module.scss';

const PlanDetailsScreen = (props) => {
  const [isDialogOpen, toggleDialog] = useState(false);
  const [modify, toggleModify] = useState(false);
  const [isDeleteOpen, toggleDelete] = useState(false);

  const openDialog = (modify, planDetail) => {
    const {
      initializeForm,
      resetPlanDetailForm
    } = props;

    if (modify) {
      initializeForm(planDetail);
    } else {
      resetPlanDetailForm();
    }

    toggleDialog(true);
    toggleModify(modify);
  };

  const closeDialog = () => {
    toggleDialog(false);
    toggleModify(false);
  };

  const closeDeleteDialog = () => toggleDelete(false);

  const {
    selectedPlanDetails,
    selectedPlanDetailLine,
    selectedPlan,
    planDetail,
    deletePlanDetails
  } = props;

  const disabledModify = selectedPlanDetailLine === null;
  const disabledDell = selectedPlanDetails.length === 0;

  return (
    <div className={styles.container}>
      <PlanDetailsHeader />
      {selectedPlan ? (
        <Fragment>
          <div className={styles.headerRightButton}>
            <InlineButton
              marginDirection="right"
              buttons={[
                {
                  _type: 'string',
                  text: I18n.t('accountingPlans.button.newPlan'),
                  onClick: () => openDialog(false, {})
                },
                {
                  _type: 'string',
                  text: I18n.t('accountingPlans.button.changePlan'),
                  disabled: disabledModify,
                  onClick: () => openDialog(true, planDetail)
                },
                {
                  _type: 'string',
                  text: I18n.t('accountingPlans.button.deletePlan'),
                  color: 'error',
                  disabled: disabledDell,
                  onClick: () => toggleDelete(true)
                }
              ]}
            />
          </div>
          <PlanDetailsTable />
        </Fragment>
      ) : (
        <div className={styles.headerTitle}>
          <p>{I18n.t('accountingPlans.slave.planDetails.empty')}</p>
        </div>
      )}
      <PlanDetailDialog
        isOpen={isDialogOpen}
        onClose={closeDialog}
        modify={modify}
      />
      <DeleteConfirmation
        deleteReportAndForm={deletePlanDetails}
        isOpen={isDeleteOpen}
        message={I18n.t('accountingPlans.dialogs.delete.message')}
        onClose={closeDeleteDialog}
      />
    </div>
  );
};

PlanDetailsScreen.propTypes = {
  deletePlanDetails: PropTypes.func.isRequired,
  initializeForm: PropTypes.func.isRequired,
  resetPlanDetailForm: PropTypes.func.isRequired,
  selectedPlanDetails: PropTypes.array,
  selectedPlanDetailLine: PropTypes.number,
  planDetail: PropTypes.shape({
    id: PropTypes.number,
    account_number: PropTypes.string,
    label: PropTypes.string
  }),
  selectedPlan: PropTypes.number
};

PlanDetailsScreen.defaultProps = {
  selectedPlanDetails: [],
  selectedPlan: null,
  selectedPlanDetailLine: null,
  planDetail: {}
};

export default PlanDetailsScreen;
