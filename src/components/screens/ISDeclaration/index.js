import React, { useState } from 'react';
import { BundleHeader } from 'containers/groups/Headers';
import { Tabs, Tab } from '@material-ui/core';
import I18n from 'assets/I18n';
import IS2571 from 'containers/groups/Forms/IS/Declaration/IS2571';
import { useWidth } from 'helpers/hooks';

import styles from './ISDeclaration.module.scss';

const existDocs = [
  {
    component: <IS2571 />,
    label: '2571'
  }
];

const ISDeclaration = () => {
  const [selectedTab, setSelectedTab] = useState(0);
  const [factor, setFactor] = useState(1);

  const handleChange = (e, value) => {
    setSelectedTab(value);
  };

  const factorCompute = (width) => {
    if (width < 1200) {
      setFactor(width / 1200);
    } else {
      setFactor(1);
    }
  };

  const pdfStyle = {};
  if (factor !== 1) {
    pdfStyle.transform = `scale(${factor})`;
  }

  const containerCb = useWidth(factorCompute);

  return (
    <div>
      <BundleHeader code_sheet_group="ACOMPTE-IS" title={I18n.t('is.title')} />
      <Tabs
        value={selectedTab}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        classes={{
          scrollButtons: styles.scrollButtons,
          root: styles.tabs
        }}
      >
        {
          existDocs.map((doc, key) => <Tab key={`${key}-${doc.label}`} label={doc.label} value={key} />)
        }
      </Tabs>
      <div ref={containerCb} className={styles.pdfContainer}>
        <div className={styles.pdf} style={pdfStyle}>
          {selectedTab === 0 && <IS2571 />}
        </div>
      </div>
    </div>
  );
};

export default ISDeclaration;
