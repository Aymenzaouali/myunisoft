import React from 'react';
import WorkingPage from 'components/screens/WorkingPage';
import Table from 'containers/groups/Tables/WorkingPage/worksheetTable';

const WorksheetLikeFNPPage = props => (
  <WorkingPage
    {...props}
    component={Table}
  />
);

export default WorksheetLikeFNPPage;
