import React, { useRef, useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { WorkingPageHeader } from 'containers/groups/Headers';
import WorkingPageToolBar from 'components/groups/ToolBar/WorkingPage';
import { InlineButton } from 'components/basics/Buttons';
import { Route } from 'react-router';
import I18n from 'assets/I18n';
import Split from 'containers/groups/Split';
import FilesDropperDialog from 'components/groups/Dialogs/FilesDropper';
import NewAccounting from 'containers/groups/NewAccounting';
import NewAccountingScreen from 'containers/controllers/NewAccounting';
import { SuccessfulDialog } from 'containers/groups/Dialogs';
import styles from './workingPage.module.scss';

const onGenerateClick = ({
  tableName,
  postGenerate,
  society_id,
  selectedBills,
  billsList,
  editRow
}) => async () => {
  try {
    const worksheetsIds = selectedBills
      .map(selectedBillId => ({
        id_worksheet: billsList
          .find(({ id }) => id.toString() === selectedBillId).bill_id.toString()
      }));
    await postGenerate(society_id, worksheetsIds);

    selectedBills.forEach((selectedBillId) => {
      editRow(tableName, {
        ...billsList.find(({ id }) => id.toString() === selectedBillId),
        generated: true
      });
    });
  } catch (e) {
    console.log('eerr in catch postGenerate', e); //eslint-disable-line
  }
};

const onEditStart = (inputRef) => {
  // waiting for the buttonRef to be filled (because not in the DOM when clicking)
  setTimeout(() => {
    if (inputRef && inputRef.current && inputRef.current.focus) {
      inputRef.current.focus();
    }
  }, 0);
};

const WorkingPage = (props) => {
  const {
    buttonTitle,
    billsList,
    isEditing,
    component: Component,
    className,
    accountFieldLabel,
    societyId,
    addPJ
  } = props;

  const inputRef = useRef(null);
  const [isPJOpen, setIsPJOpen] = useState(false);
  const handleEditStart = () => onEditStart(inputRef);
  const [PJBillId, setPJBillId] = useState(null);
  const openPJDialog = (id) => {
    setIsPJOpen(true);
    setPJBillId(id);
  };
  const slaves = {
    consulting: {
      label: I18n.t('split.consulting'),
      url: `/tab/${societyId}/accounting/new`,
      component: () => <NewAccounting />,
      swapKeepArgs: true,
      args: {
        isNotEcriture: false
      }
    }
  };

  return (
    <Fragment>
      <Route path="/tab/:id/(slave/)?accounting/new" component={NewAccountingScreen} />
      <Split master_id="workingPage" slaves={slaves}>
        <div className={classnames(styles.container, className, { 'is-editing': isEditing })}>
          <WorkingPageHeader {...props} />
          <WorkingPageToolBar
            {...props}
            onEditStart={handleEditStart}
            accountFieldLabel={accountFieldLabel}
          />
          <div className={styles.table}>
            <Component
              {...props}
              inputRef={inputRef}
              onEditStart={handleEditStart}
              openPJDialog={openPJDialog}
            />
          </div>
          {buttonTitle && (
            <div className={styles.generateContainer}>
              <InlineButton
                marginDirection="right"
                buttons={[
                  {
                    _type: 'string',
                    text: buttonTitle,
                    onClick: onGenerateClick(props),
                    size: 'large',
                    disabled: !(billsList && billsList !== null && billsList.length > 0),
                    className: styles.generateButton
                  }]
                }
              />
            </div>
          )}
        </div>
      </Split>
      <FilesDropperDialog
        open={isPJOpen}
        onClose={() => setIsPJOpen(false)}
        field="rapprochementPJ"
        onAddFiles={file => addPJ(file, PJBillId)}
      />
      <SuccessfulDialog />
    </Fragment>
  );
};

WorkingPage.propTypes = {
  className: PropTypes.string,
  buttonTitle: PropTypes.string,
  billsList: PropTypes.array.isRequired,
  postGenerate: PropTypes.func.isRequired,
  selectedBills: PropTypes.shape({}).isRequired,
  isEditing: PropTypes.bool,
  society_id: PropTypes.number.isRequired,
  component: PropTypes.element.isRequired,
  accountFieldLabel: PropTypes.string.isRequired,
  societyId: PropTypes.number.isRequired,

  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  addPJ: PropTypes.func
};

WorkingPage.defaultProps = {
  buttonTitle: undefined,
  className: undefined,
  isEditing: false,
  addPJ: undefined
};

export default WorkingPage;
