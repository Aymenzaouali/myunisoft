import React from 'react';
import WorkingPage from 'components/screens/WorkingPage';
import CCATable from 'containers/groups/Tables/WorkingPage/cca';

const WorksheetLikeCCAPage = props => (
  <WorkingPage
    {...props}
    component={CCATable}
  />
);

export default WorksheetLikeCCAPage;
