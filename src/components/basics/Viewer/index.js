import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class Viewer extends PureComponent {
  render() {
    const { file, ...otherProps } = this.props;

    return (
      <div>
        {file && (
          <iframe
            id="viewer"
            title="viewer"
            type="application/pdf"
            src={URL.createObjectURL(file)}
            frameBorder="0"
            width="100%"
            height="1000"
            {...otherProps}
          />
        )}
      </div>
    );
  }
}

Viewer.propTypes = {
  file: PropTypes.any // eslint-disable-line
};

export default Viewer;
