import React from 'react';
import PropTypes from 'prop-types';
import Popover from '@material-ui/core/Popover';
import Button from 'components/basics/Buttons/Button';
import './styles.scss';

const ButtonsPopover = ({
  isOpen, top, left, onClose, onExited, buttons, value
}) => (
  <Popover
    onContextMenu={(e) => { e.preventDefault(); onClose(); }}
    open={isOpen}
    anchorPosition={{ top, left }}
    anchorReference="anchorPosition"
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right'
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'left'
    }}
    onClose={onClose}
    onExited={onExited}
  >
    <div className="buttonsPopover__container">
      { buttons && buttons.map((button, i) => <Button key={`${button.label}${i}`} color={button.color || 'inherit'} size="medium" onClick={() => { button.onClick(value); onClose(); }} disabled={button.disabled}>{button.label}</Button>) }
    </div>
  </Popover>
);

ButtonsPopover.defaultProps = {
  onExited: () => {}
};

ButtonsPopover.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  top: PropTypes.number.isRequired,
  left: PropTypes.number.isRequired,
  value: PropTypes.any, // eslint-disable-line
  onClose: PropTypes.func.isRequired,
  onExited: PropTypes.func,
  buttons: PropTypes.arrayOf(PropTypes.shape({
    onClick: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired
  })).isRequired
};

export default ButtonsPopover;
