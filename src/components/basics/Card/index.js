import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';

import styles from './Card.module.scss';

const SimpleCard = (props) => {
  const {
    card = {},
    editIcon,
    children,
    iconOnClick
  } = props;

  return (
    <div className={styles.card}>
      <div className={styles.cardHeader}>
        <div className={styles.leftItem}>{card.title.replace(/_/, ' ')}</div>
        {editIcon
              && (
                <IconButton aria-label="Delete" className={styles.icon} onClick={iconOnClick}>
                  <span className="icon-edit" />
                </IconButton>
              )
        }
        {card.amount
              && <div className={styles.rightItem}>{card.amount}</div>
        }
      </div>
      <div className={styles.separator} />
      {card.subtitle
            && <div className={styles.subtitle}>{card.subtitle}</div>
      }
      {children}
    </div>
  );
};

SimpleCard.propTypes = {
  iconOnClick: PropTypes.func,
  card: PropTypes.shape({
    title: PropTypes.string,
    amount: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.node
    ]),
    subtitle: PropTypes.string
  }).isRequired,
  editIcon: PropTypes.bool,
  classes: PropTypes.shape({}),
  children: PropTypes.element.isRequired
};

SimpleCard.defaultProps = {
  editIcon: false,
  classes: {},
  iconOnClick: () => {}
};

export default SimpleCard;
