import React from 'react';
import PropTypes from 'prop-types';
import MaterialRadio from '@material-ui/core/Radio';
import RadioUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioCheckedIcon from '@material-ui/icons/RadioButtonChecked';

// Component
const Radio = (props) => {
  const {
    size,
    classes,
    color,
    ...others
  } = props;
  return (
    <MaterialRadio
      icon={<RadioUncheckedIcon fontSize={size} />}
      checkedIcon={<RadioCheckedIcon color={color} fontSize={size} />}

      {...others}
    />
  );
};

// Props
Radio.propTypes = {
  size: PropTypes.oneOf(['inherit', 'default', 'small', 'large']),
  color: PropTypes.string
};

Radio.defaultProps = {
  size: 'small',
  color: 'primary'
};

export default Radio;
