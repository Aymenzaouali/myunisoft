/* eslint-disable */
import React, { useState } from 'react';
import _get from 'lodash/get';

const Notifier =({notifications,enqueueSnackbar,removeSnackbar})=>{

  const [isDisplayed,setDisplayed] = useState([]);

  notifications.map(notification => {
    if (typeof _get(notification, 'message') === "string") {
      setTimeout(() => {
        // If notification already displayed, abort
        if (isDisplayed.filter(key => key === notification.key).length > 0) {
          return;
        }
        // Display notification using Snackbar
        enqueueSnackbar(notification.message, {variant: notification.options.variant});
        // Add notification's key to the local state
        setDisplayed([...isDisplayed, notification.key]);
        // Dispatch action to remove the notification from the redux store
        removeSnackbar(notification.key);
      }, 300);
    }
  });
  return null;
};
export default Notifier;
