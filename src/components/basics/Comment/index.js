import React from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';

import {
  Avatar,
  Typography,
  TextField,
  withStyles
} from '@material-ui/core';

import Button from 'components/basics/Buttons/Button';
import { ReduxTextField } from 'components/reduxForm/Inputs';

import './styles.comment.scss';

class Comment extends React.PureComponent {
  render() {
    const {
      avatar,
      textIcon,
      comment,
      classes,
      edit,
      actionEdit,
      validateButton,
      hasField
    } = this.props;

    const renderInputField = () => (
      hasField
        ? (
          <Field
            InputProps={{ classes: { root: classes.textArea } }}
            name="comment"
            margin="none"
            component={ReduxTextField}
            defaultValue={comment}
            multiline
            fullWidth
          />
        )
        : (
          <TextField
            classes={{ root: classes.textArea }}
            InputLabelProps={{ classes: { root: classes.textAreaLabel } }}
            defaultValue={comment}
            multiline
            fullWidth
          />
        )
    );

    const editComment = (
      <div className="comment_content-edit">
        {renderInputField()}
        {validateButton && (
          <Button
            classes={{ root: classes.button }}
            variant="contained"
            color="primary"
            onClick={actionEdit}
          >
            {I18n.t('Comment.validate')}
          </Button>
        )}
      </div>
    );

    const commentary = edit
      ? (editComment)
      : (<Typography classes={{ root: classes.body2 }} variant="body2" dangerouslySetInnerHTML={{ __html: comment }} />);

    return (
      <div className="Comment">
        <div className="header">
          <Avatar classes={avatar.classes} alt={avatar.initial} src={avatar.avatarImgSrc}>
            {avatar.initial}
          </Avatar>
        </div>
        <div className="comment_content">
          {textIcon && (
            <Typography classes={{ root: classes.subtitle2 }} variant="subtitle2">
              {textIcon}
            </Typography>
          )}
          {commentary}
        </div>
      </div>
    );
  }
}

const styles = () => ({
  subtitle2: {
    marginLeft: '10px',
    color: '#9b9b9b'
  },
  body2: {
    marginLeft: '10px'
  },
  textArea: {
    marginLeft: '10px'
  },
  textAreaLabel: {
    fontSize: '12px'
  },
  button: {
    marginLeft: '20px'
  }
});

Comment.propTypes = {
  /** avatar props for avatar material ui */
  avatar: PropTypes.shape({
    initial: PropTypes.string.isRequired,
    avatarImgSrc: PropTypes.string
  }).isRequired,
  /** Text for icon */
  textIcon: PropTypes.string,
  /** comment text */
  comment: PropTypes.string,
  /** edition for comment */
  edit: PropTypes.bool,
  /** action when clicking in button edition */
  actionEdit: PropTypes.func,
  classes: PropTypes.shape({}),
  validateButton: PropTypes.bool,
  hasField: PropTypes.bool
};

Comment.defaultProps = {
  comment: '',
  edit: false,
  actionEdit: () => {},
  textIcon: '',
  classes: {},
  validateButton: true,
  hasField: false
};

export default withStyles(styles)(Comment);
