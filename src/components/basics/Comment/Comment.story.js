import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import withReadme from 'storybook-readme/with-readme';

import Readme from './README.md';
import Comment from './index';

const stories = storiesOf('Comment', module);
/**
 * v4 of package not working with Storybook v5.
 * Waiting fix.
 * @see https://github.com/tuchk4/storybook-readme/issues/111
 */
stories.addDecorator(withReadme(Readme));

stories.add('Default', () => {
  const comment = text('Comment text', 'This is a nice app');
  const edited = boolean('Editing?', false);
  const textIcon = text('Icon text (e.g. name, surname)', 'Igor Doe');
  const actionEdit = action('Validate button clicked');
  const avatarName = text('Avatar initials', 'IR');

  return (
    <Comment
      avatar={{
        initial: avatarName
      }}
      comment={comment}
      edit={edited}
      textIcon={textIcon}
      actionEdit={actionEdit}
    />
  );
}, {
  info: {
    text: 'A generic comment component used across the app'
  }
});
