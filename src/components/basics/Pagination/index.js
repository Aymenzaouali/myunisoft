import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  IconButton, withStyles
} from '@material-ui/core';
import {
  SelectSelections
} from 'components/basics/Selections';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import classNames from 'classnames';
import I18n from 'assets/I18n';
import { Trans } from 'react-i18next';
import styles from './styles.module.scss';

const Pagination = (props) => {
  const {
    classes,

    page,
    pages,
    onChangePage,

    rows,
    rowsPerPage,
    rowsPerPageOptions,
    onChangeRowsPerPage
  } = props;

  // Effects
  useEffect(() => {
    if (page !== 0) onChangePage(0);
  }, [rowsPerPage]);

  // Rendering
  return (
    <div className={classNames(styles.container, styles.row)}>
      {/* Rows per page */}
      <div className={styles.row}>
        <span className={classNames(styles.text, styles.margin)}>
          { I18n.t('pagination.printPerPage') }
        </span>
        <SelectSelections
          pagination
          value={rowsPerPage}
          options={rowsPerPageOptions}
          onChange={onChangeRowsPerPage}
        />
      </div>

      {/* Page navigation */}
      <div className={styles.row}>
        {/* to first */}
        <IconButton
          disabled={page === 0}
          classes={{ root: classes.rootRotate }}
          onClick={() => onChangePage(0)}
        >
          <FontIcon name="icon-nav1" />
        </IconButton>

        {/* to previous (arrow) */}
        <IconButton
          disabled={page === 0}
          classes={{ root: classes.rootRotate }}
          onClick={() => onChangePage(page - 1)}
        >
          <FontIcon name="icon-nav" />
        </IconButton>

        {/* to previous (number) */}
        { (page > 0) && (
          <Button
            classes={{ root: classNames(classes.root, classes.otherPage) }}
            onClick={() => onChangePage(page - 1)}
          >
            { page }
          </Button>
        ) }

        {/* current page (no move => no onClick) */}
        <Button classes={{ root: classNames(classes.root, classes.mainPage) }}>
          {page + 1 }
        </Button>

        {/* to next (number) */}
        { (page < pages - 1) && (
          <Button
            classes={{ root: classNames(classes.root, classes.otherPage) }}
            onClick={() => onChangePage(page + 1)}
          >
            { page + 2 }
          </Button>
        ) }

        {/* to next (arrow) */}
        <IconButton
          disabled={pages === 0 || pages === page + 1}
          classes={{ root: classes.root }}
          onClick={() => onChangePage(page + 1)}
        >
          <FontIcon name="icon-nav" />
        </IconButton>

        {/* to last */}
        <IconButton
          disabled={pages === 0 || pages === page + 1}
          classes={{ root: classes.root }}
          onClick={() => onChangePage(pages - 1)}
        >
          <FontIcon name="icon-nav1" />
        </IconButton>
      </div>

      {/* counts */}
      <div className={styles.row}>
        <span className={styles.text}>
          <Trans i18nKey="pagination.results" count={rows}>
            <span className={classNames(styles.margin, styles.bold)}>{{ count: rows }}</span>
            <span className={styles.margin}>résultats sur</span>
          </Trans>
          <Trans i18nKey="pagination.pages" count={rows}>
            <span className={styles.bold}>
              {{ count: pages }} pages {/* eslint-disable-line react/jsx-one-expression-per-line */}
            </span>
          </Trans>
        </span>
      </div>
    </div>
  );
};

Pagination.propTypes = {
  rowsPerPageOptions: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
      PropTypes.arrayOf
    ]),
    label: PropTypes.string
  })),
  page: PropTypes.number.isRequired,
  rows: PropTypes.number,
  pages: PropTypes.number,
  rowsPerPage: PropTypes.number,
  onChangeRowsPerPage: PropTypes.func,
  onChangePage: PropTypes.func,
  classes: PropTypes.shape({
    root: PropTypes.string,
    rootRotate: PropTypes.string,
    mainPage: PropTypes.string,
    otherPage: PropTypes.string
  }).isRequired
};

Pagination.defaultProps = {
  rowsPerPageOptions: [
    { value: 5, label: '5' },
    { value: 10, label: '10' },
    { value: 25, label: '25' },
    { value: 50, label: '50' },
    { value: 1000, label: '1000' }
  ],
  rows: 0,
  rowsPerPage: 0,
  pages: 0,
  onChangeRowsPerPage: () => {},
  onChangePage: () => {}
};

const themeStyles = () => ({
  rootRotate: {
    transform: 'rotate(180deg)',
    padding: '0px'
  },
  root: {
    padding: '0px',
    minHeight: 0
  },
  mainPage: {
    color: '#0bd1d1',
    borderRadius: 0,
    border: '1px solid #9b9b9b',
    minWidth: 33
  },
  otherPage: {
    color: '#9b9b9b'
  }
});

export default withStyles(themeStyles)(Pagination);
