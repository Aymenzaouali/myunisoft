import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Tooltip, withStyles } from '@material-ui/core/';
import styles from './styles.module.scss';

const FontIcon = (props) => {
  const {
    name,
    size,
    color,
    titleInfoBulle,
    classes,
    marginRight,
    className,
    iconChar,
    iconCharStyle,
    ...otherProps
  } = props;

  if (titleInfoBulle) {
    return (
      <Tooltip title={titleInfoBulle} placement="left-end" disableFocusListener disableTouchListener classes={{ tooltip: classes.infoBulle }}>
        <span
          className={classNames(name, className, styles.fontIcon)}
          style={{ fontSize: size, color }}
          {...otherProps}
        />
      </Tooltip>

    );
  }
  return (
    <span
      className={classNames(
        name,
        className,
        styles.fontIcon,
        iconChar && styles.dataIconChar,
        iconCharStyle
      )}
      style={{ fontSize: size, color, marginRight }}
      data-icon-char={iconChar}
      {...otherProps}
    />
  );
};
const themeStyles = () => ({
  infoBulle: {
    border: 'none',
    heigth: 15,
    width: 'auto',
    padding: '4px 10px',
    borderRadius: 2,
    fontWeight: 500,
    boxShadow: '1px 5px 7px 0 rgba(0, 0, 0, 0.08)',
    backgroundColor: '#ffffff',
    color: '#464545',
    fontFamily: 'basier_circlemedium'
  }
});

FontIcon.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.string,
  titleInfoBulle: PropTypes.string,
  marginRight: PropTypes.number,
  classes: PropTypes.shape({}),
  iconChar: PropTypes.string,
  iconCharStyle: PropTypes.string
};

FontIcon.defaultProps = {
  name: null,
  size: undefined,
  color: undefined,
  titleInfoBulle: '',
  classes: {},
  className: '',
  iconChar: null,
  iconCharStyle: null,
  marginRight: 0
};

export default withStyles(themeStyles)(FontIcon);
