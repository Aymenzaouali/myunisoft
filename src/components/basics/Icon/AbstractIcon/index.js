import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, IconButton } from '@material-ui/core';
import FontIcon from 'components/basics/Icon/Font';

import styles from './AbstractIcon.module.scss';

class AbstractIcon extends React.PureComponent {
  render() {
    const {
      iconName,
      iconColor,
      iconSize,
      children,
      classes,
      classesButton,
      ...rest
    } = this.props;

    return (
      <IconButton
        classes={classesButton || classes}
        className={styles.container}
        {...rest}
      >
        <FontIcon
          name={iconName}
          color={iconColor}
          size={iconSize}
        />
      </IconButton>
    );
  }
}

AbstractIcon.defaultProps = {
  iconColor: '#fff',
  iconSize: 19,
  children: null,
  classes: '',
  classesButton: ''
};

AbstractIcon.propTypes = {
  iconColor: PropTypes.string,
  iconSize: PropTypes.number,
  iconName: PropTypes.string.isRequired,
  children: PropTypes.node,
  classes: PropTypes.string,
  classesButton: PropTypes.string
};

const theme = () => ({
  root: {
    backgroundColor: '#00d1d2',
    padding: 0,
    borderRadius: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',

    '&:hover': {
      backgroundColor: '#07b4b4'
    }
  }
});

export default withStyles(theme)(AbstractIcon);
