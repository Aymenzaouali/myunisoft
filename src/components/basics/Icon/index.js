import ArrowIcon from './Arrow';
import SvgIcon from './Svg';
import FontIcon from './Font';
import AbstractIcon from './AbstractIcon';
import StatusIcon from './Status';

export {
  ArrowIcon,
  SvgIcon,
  FontIcon,
  AbstractIcon,
  StatusIcon
};
