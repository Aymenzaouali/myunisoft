import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Button } from '@material-ui/core/';
import styles from './styles.module.scss';

const ButtonIcon = (props) => {
  const {
    name,
    size,
    color,
    marginRight,
    className,
    iconChar,
    iconCharStyle,
    disabled,
    hidden,
    ...otherProps
  } = props;

  return (
    <div>
      {!hidden && (
        <Button disabled={disabled}>
          <span
            className={classNames(
              name,
              className,
              styles.fontIcon,
              iconChar && styles.dataIconChar,
              iconCharStyle
            )}
            style={{ fontSize: size, color, marginRight }}
            data-icon-char={iconChar}
            {...otherProps}
          />
        </Button>
      )}
    </div>
  );
};

ButtonIcon.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.string,
  marginRight: PropTypes.number,
  iconChar: PropTypes.string,
  iconCharStyle: PropTypes.string,
  disabled: PropTypes.bool,
  hidden: PropTypes.bool
};

ButtonIcon.defaultProps = {
  name: null,
  size: undefined,
  color: undefined,
  className: '',
  iconChar: null,
  iconCharStyle: null,
  marginRight: 0,
  disabled: false,
  hidden: false
};

export default ButtonIcon;
