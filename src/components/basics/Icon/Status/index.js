import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Loader from 'components/basics/Loader';

import styles from './statusIcon.module.scss';

// Component
const StatusIcon = (props) => {
  const { className, status, size } = props;

  return (
    <div className={classNames(styles.container, className)}>
      {status === 'ok' && <span className="icon-check" />}
      {status === 'running' && <span className="icon-progress" />}
      {status === 'ko' && <span className="icon-close" />}
      {status === 'loading' && <Loader size={size} />}
    </div>
  );
};

// Props
StatusIcon.propTypes = {
  className: PropTypes.string,
  status: PropTypes.string.isRequired,
  size: PropTypes.number
};

StatusIcon.defaultProps = {
  className: undefined,
  size: 10
};

export default StatusIcon;
