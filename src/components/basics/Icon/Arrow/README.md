Display icon arrow according to props

ArrowIcon default
```js
  <ArrowIcon/>
```

ArrowIcon with props desc
```js
  <ArrowIcon direction='desc'/>
```
