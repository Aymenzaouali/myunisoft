import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Arrow.module.scss';

const ArrowIcon = ({ direction, colorError }) => (
  <React.Fragment>
    { direction === 'asc' && <span className={classNames(styles.asc, { [styles.colorErrorAsc]: colorError })} /> }
    { direction === 'desc' && <span className={classNames(styles.desc, { [styles.colorErrorDesc]: colorError })} /> }
    { direction === '' && (
      <div className={styles.container}>
        <span className={classNames(styles.asc, { [styles.colorErrorAsc]: colorError })} />
        <span className={classNames(styles.desc, { [styles.colorErrorDesc]: colorError })} />
      </div>
    ) }
  </React.Fragment>
);

ArrowIcon.defaultProps = {
  /** direction arrow */
  direction: '',
  colorError: false
};

ArrowIcon.propTypes = {
  /** direction arrow */
  direction: PropTypes.oneOf(['asc', 'desc', '']),
  colorError: PropTypes.bool
};

export default ArrowIcon;
