import React from 'react';
import PropTypes from 'prop-types';
import SvgIcon from '@material-ui/core/SvgIcon';
import './styles.scss';

const Icon = (props) => {
  const {
    icon_color,
    icon_source
  } = props;

  return (
    <div>
      <SvgIcon nativeColor={icon_color}>
        <path d={icon_source} />
      </SvgIcon>
    </div>
  );
};

Icon.defaultProps = {
  icon_color: 'black'
};

Icon.propTypes = {
  icon_color: PropTypes.string,
  icon_source: PropTypes.string.isRequired
};

export default Icon;
