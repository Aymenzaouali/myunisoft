import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';

class CustomCell extends React.Component {
    renderCell = () => {
      const {
        data
      } = this.props;

      switch (data.type) {
      case 'string':
        I18n.t(`progressionTable.customCell.${data.status}`);
        return <div className={`custom_cell__string-${data.status}`}>{I18n.t(`progressionTable.customCell.${data.status}`)}</div>;
      case 'array':
        return (
          data.value.map(value => (
            <div className={`custom_cell__array-${value.status}`}>
              {`${I18n.t(`progressionTable.customCell.${value.status}`)} : ${value.value}`}
            </div>
          ))
        );
      default:
        return <div>{data.value}</div>;
      }
    }

    render() {
      const {
        ...otherProps
      } = this.props;

      return (
        <TableCell {...otherProps}>
          {this.renderCell()}
        </TableCell>
      );
    }
}

CustomCell.propTypes = {
  data: PropTypes.shape({}).isRequired
};


export default CustomCell;
