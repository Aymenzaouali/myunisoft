import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, text, select } from '@storybook/addon-knobs';
import HelperInfo from './index';

const stories = storiesOf('HelperInfo', module);

stories.add('One entry', () => {
  const isVisible = boolean('Is error visible?', true);
  const message = text('Error text', 'Im an error');
  const type = select('Message type', ['error', 'warning'], 'error');

  return (
    <HelperInfo
      isVisible={isVisible}
      infoMessages={[
        {
          message,
          type
        }
      ]}
    />
  );
});
stories.add('Several messages', () => {
  const isVisible = boolean('Is error visible?', true);
  const type = select('Number of messages', [1, 2], 2);

  const messages = [
    {
      message: 'I\'m a big error',
      type: 'error'
    },
    {
      message: 'Not so important, but still a warning',
      type: 'warning'
    }
  ];

  return (
    <HelperInfo
      isVisible={isVisible}
      infoMessages={messages.slice(0, type)}
    />
  );
});
