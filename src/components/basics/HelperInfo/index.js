import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

const infoColorTypeMap = {
  warning: '',
  error: 'error'
};

class HelperInfo extends React.PureComponent {
  renderMessage = ({ message, type }, i) => (
    <div key={i}>
      <Typography color={infoColorTypeMap[type]}>{message}</Typography>
    </div>
  )

  render() {
    const {
      infoMessages,
      isVisible,
      className
    } = this.props;

    if (!isVisible) {
      return null;
    }

    return (
      <div className={className}>
        {infoMessages.map(this.renderMessage)}
      </div>
    );
  }
}

HelperInfo.defaultProps = {
  className: ''
};

HelperInfo.propTypes = {
  className: PropTypes.string,
  isVisible: PropTypes.bool.isRequired,
  infoMessages: PropTypes.arrayOf(PropTypes.shape({
    message: PropTypes.string,
    type: PropTypes.oneOf(['warning', 'error'])
  })).isRequired
};

export default HelperInfo;
