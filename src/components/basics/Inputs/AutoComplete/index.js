/* eslint-disable react/prop-types, react/jsx-handler-names */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select /* , { components as comps } */ from 'react-select';
import CreatableSelect from 'react-select/lib/Creatable';
import AsyncSelect from 'react-select/lib/Async';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import { FontIcon } from 'components/basics/Icon';
import I18n from 'assets/I18n';
import { emphasize } from '@material-ui/core/styles/colorManipulator';


const styles = theme => ({
  root: {
    flexGrow: 1,
    minWidth: 160,
    position: 'relative'
  },
  input: {
    display: 'flex',
    padding: 0
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center'
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
    backgroundColor: 'rgba(11, 209, 209, 0.14)',
    color: '#0bd1d1',
    height: 24,
    borderRadius: 16,
    paddingRight: 4
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    )
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  },
  singleValue: {
    fontSize: 16
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 14
  },
  paper: {
    position: 'absolute',
    zIndex: 20,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
    width: '150%'
  },
  divider: {
    height: theme.spacing.unit * 2
  },
  focusedInputBorder: {
    border: '1px solid #0bd1d1',
    'background-color': 'white'
  },
  underlineBorder: {
    '&:before': {
      'border-bottom': 'none'
    },
    '&&&&:hover:before': {
      'border-bottom': '2px solid #0bd1d1'
    }
  },
  infoText: {
    position: 'absolute',
    top: '50%',
    left: '105%',
    whiteSpace: 'nowrap',
    fontSize: 12,
    color: '#9b9b9b'
  }
});

function NoOptionsMessage(props) {
  const {
    selectProps,
    innerProps,
    children
  } = props;
  return (
    <Typography
      color="textSecondary"
      className={selectProps.classes.noOptionsMessage}
      {...innerProps}
    >
      {children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  const {
    selectProps,
    innerProps,
    innerRef,
    children
  } = props;

  return (
    <TextField
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: selectProps.classes.input,
          inputRef: innerRef,
          children,
          ...innerProps
        },
        classes: selectProps.border ? {
          focused: selectProps.classes.focusedInputBorder,
          underline: selectProps.classes.underlineBorder
        }
          : {}
      }}
      {...selectProps.textFieldProps}
    />
  );
}

function Option(props) {
  const {
    isFocused,
    isSelected,
    innerProps,
    innerRef,
    children,
    selectProps
  } = props;

  // console.log('Option:', children, isFocused, isSelected);
  return (
    <MenuItem
      buttonRef={innerRef}
      selected={isFocused}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
        color: isSelected ? '#0bd1d1' : 'inherit',
        ...selectProps.optionStyle && selectProps.optionStyle(props)
      }}
      {...innerProps}
    >
      { selectProps.optionRender
        ? selectProps.optionRender(props)
        : <span>{ children }</span>
      }
      {/* <comps.Option {...props} /> */}
    </MenuItem>
  );
}

function Placeholder(props) {
  const {
    selectProps,
    innerProps,
    children
  } = props;
  return (
    <Typography
      color="textSecondary"
      className={selectProps.classes.placeholder}
      {...innerProps}
    >
      {children}
    </Typography>
  );
}

function SingleValue(props) {
  const {
    selectProps,
    innerProps,
    children
  } = props;
  return (
    <Typography className={selectProps.classes.singleValue} {...innerProps}>
      {children}
    </Typography>
  );
}

function ValueContainer(props) {
  const {
    selectProps,
    children
  } = props;
  return <div className={selectProps.classes.valueContainer}>{children}</div>;
}

function MultiValue(props) {
  const {
    selectProps,
    isFocused,
    children,
    removeProps
  } = props;
  return (
    <Chip
      tabIndex={-1}
      label={children}
      className={classNames(selectProps.classes.chip, {
        [selectProps.classes.chipFocused]: isFocused
      })}
      onDelete={removeProps.onClick}
      deleteIcon={<FontIcon size="12px" color="#0bd1d1" name="icon-close" titleInfoBulle={I18n.t('tooltips.close')} />}
    />
  );
}

function Menu(props) {
  const {
    selectProps,
    innerProps,
    children
  } = props;
  return (
    <Paper square className={classNames(selectProps.classes.paper)} {...innerProps}>
      {children}
    </Paper>
  );
}

const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer
};

class AutoComplete extends React.Component {
  state = {
    multi: null
  };

  componentDidMount() {
    const { loadData } = this.props;
    if (loadData) {
      loadData();
    }
  }

  handleChange = name => (value) => {
    const { onChangeValues = () => {} } = this.props;
    onChangeValues(value);
    this.setState({
      [name]: value
    });
  };

  render() {
    const {
      classes,
      className,
      theme,
      isCreatable,
      customRef,
      label,
      selectStyles,
      classZIndex,
      isAsync,
      maxHeight,
      onDoubleClick,
      ...otherProps
    } = this.props;
    const {
      multi
    } = this.state;

    const styles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit'
        },
        ...selectStyles.input
      }),
      menuList: base => ({
        ...base,
        ...selectStyles.menuList,
        // height: 240,
        maxHeight
      }),
      menuPortal: provided => ({ ...provided, zIndex: 2000 })
    };

    const Wrapper = (
      (isAsync && AsyncSelect)
        || (isCreatable && CreatableSelect)
        || Select
    ); // note: async & creatable simultaneously isn't implemented yet

    return (
      <div className={classNames(classes.root, className)} onDoubleClick={onDoubleClick}>
        <NoSsr>
          {label && <Typography color="textSecondary" variant="caption">{label}</Typography>}
          <Wrapper
            classes={classes}
            styles={styles}
            components={components}
            value={multi}
            onChange={this.handleChange('multi')}
            ref={customRef}
            {...otherProps}
            menuPlacement="auto"
          />
          {otherProps.value && otherProps.value.info && (
            <small className={classes.infoText}>{otherProps.value.info}</small>
          )}
        </NoSsr>
      </div>
    );
  }
}

AutoComplete.defaultProps = {
  isCreatable: false,
  label: undefined,
  className: null,
  selectStyles: {
    input: {},
    menuList: {}
  },
  classZIndex: false,
  isAsync: false,
  maxHeight: 250
};

AutoComplete.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  theme: PropTypes.shape({}).isRequired,
  isCreatable: PropTypes.bool,
  label: PropTypes.string,
  classZIndex: PropTypes.bool,
  selectStyles: PropTypes.shape({
    input: PropTypes.shape({}),
    menuList: PropTypes.shape({})
    // ADD other keys here (https://react-select.com/styles#style-object)
  }),
  isAsync: PropTypes.bool,
  maxHeight: PropTypes.number
};

export default withStyles(styles, { withTheme: true })(AutoComplete);
