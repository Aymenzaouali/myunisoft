This component is an autocomplete selector, you can either select one value or multiple values according of what you are typing.

Basic example

```js
  <AutoComplete
    options={[
      {
        value: 1,
        label: "Selma Zaim",
      },
      {
        value: 2,
        label: "Cyril Mandrilly",
      }
    ]}
    placeholder="Select users"
  />
```

If you need a multi select with chips you must use options **isMulti**

```js
  <AutoComplete
    options={[
      {
        value: 1,
        label: "Selma Zaim",
      },
      {
        value: 2,
        label: "Cyril Mandrilly",
      }
    ]}
    placeholder="Select users"
    isMulti
  />
```
