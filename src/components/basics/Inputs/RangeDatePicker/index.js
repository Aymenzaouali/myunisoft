import React, { useState, useRef } from 'react';
import { DatePicker } from 'material-ui-pickers';
import moment from 'moment';
import { ArrowLeft, ArrowRight } from '@material-ui/icons';
import FontIcon from 'components/basics/Icon/Font';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';

const RangeDatePicker = ({
  value,
  onChange,
  labelFunc,
  emptyLabel,
  autoOk,
  onClose,
  ...props
}) => {
  const [begin, setBegin] = useState(moment(value[0]) || new Date());
  const [end, setEnd] = useState(moment(value[1]) || new Date());
  const [workDate, setWorkDate] = useState(null);
  const [hover, setHover] = useState(null);
  const picker = useRef();

  const min = Math.min(begin, end || hover);
  const max = Math.max(begin, end || hover);

  function renderDay(day, selectedDate, dayInCurrentMonth, dayComponent) {
    const style = {
      margin: 0,
      width: '40px',
      borderRadius: '50%'
    };

    if (day >= min && day <= max) {
      style.backgroundColor = '#0bd1d1';
      style.color = 'white';
      style.borderRadius = 0;
    }

    if (moment(day).isSame(min)) {
      style.borderRadius = '50% 0 0 50%';
      if (moment(day).isSame(begin) && moment(day).isSame(end)) {
        style.borderRadius = '50%';
      }
    } else if (moment(day).isSame(max)) {
      style.borderRadius = '0 50% 50% 0';
    } else if (day >= min && day <= max) {
      style.borderRadius = '0';
    } else {
      style.borderRadius = '50%';
    }

    return React.cloneElement(dayComponent, {
      onClick: (e) => {
        e.stopPropagation();
        if (!begin) {
          setBegin(day);
        } else if (!end) {
          setEnd(day);
          if (autoOk) {
            onChange([begin, end].sort((a, b) => a - b));
            picker.current.close();
          }
          if (moment(day).isSame(begin) && moment(day).isSame(end)) {
            setWorkDate(day);
          }
        } else {
          setBegin(day);
          setEnd(undefined);
        }
        setWorkDate(day);
      },
      onMouseEnter: () => setHover(day),
      style
    });
  }

  const formatDate = date => moment(date).format('DD/MM/YYYY');

  return (
    <DatePicker
      {...props}
      format="DD/MM/YYYY"
      leftArrowIcon={<ArrowLeft />}
      rightArrowIcon={<ArrowRight />}
      keyboardIcon={<FontIcon name="icon-calendar" />}
      clearLabel={I18n.t('calendar.clear')}
      cancelLabel={I18n.t('calendar.cancel')}
      value={workDate}
      clearable
      renderDay={renderDay}
      onClose={() => {
        onChange([begin, end].sort((a, b) => a - b));
        setWorkDate(end);
        if (onClose) onClose([begin, end].sort((a, b) => a - b));
      }}
      onChange={() => {
        onChange([begin, end].sort((a, b) => a - b));
      }}
      onClear={() => {
        setBegin(undefined);
        setEnd(undefined);
        setHover(undefined);
        setWorkDate(undefined);
        onChange([]);
      }}
      ref={picker}
      labelFunc={(date, invalid) => (labelFunc
        ? labelFunc([begin, end].sort((a, b) => a - b), invalid)
        : [begin, end].sort((a, b) => a - b).map(formatDate).join(' au '))
      }
    />
  );
};

// Props
RangeDatePicker.propTypes = {
  // Value Date
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(moment)]),
  // Label
  label: PropTypes.string,
  emptyLabel: PropTypes.string,
  // Action
  onChange: PropTypes.func,
  autoOk: PropTypes.func,
  onClose: PropTypes.func,
  labelFunc: PropTypes.func
};

RangeDatePicker.defaultProps = {
  value: '',
  emptyLabel: '',
  label: '',
  onChange: () => null,
  labelFunc: undefined,
  autoOk: undefined,
  onClose: undefined
};

export default RangeDatePicker;
