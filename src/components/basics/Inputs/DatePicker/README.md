```js
  const MomentUtils = require('@date-io/moment');
  const MuiPickersUtilsProvider = require('material-ui-pickers/MuiPickersUtilsProvider');

  <MuiPickersUtilsProvider utils={MomentUtils}>
    <DatePicker />
  </MuiPickersUtilsProvider>
```

```js
  const MomentUtils = require('@date-io/moment');
  const MuiPickersUtilsProvider = require('material-ui-pickers/MuiPickersUtilsProvider');
  
  <MuiPickersUtilsProvider utils={MomentUtils}>
    <DatePicker 
      minDate='20170101'
      maxDate='20190101'
      value='20180901'
      label='Test'
    />
  </MuiPickersUtilsProvider>
```
