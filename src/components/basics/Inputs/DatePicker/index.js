import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import { DatePicker } from 'material-ui-pickers';
import { ArrowRight, ArrowLeft } from '@material-ui/icons';

import I18n from 'assets/I18n';
import FontIcon from 'components/basics/Icon/Font';
import moment from 'moment';

const styles = () => ({
  root: {
    padding: 0
  }
});

// Components
const DatePickerComponent = (props) => {
  const {
    value,
    classes,
    ...customProps
  } = props;

  return (
    <DatePicker
      keyboard
      value={!value ? null : value}
      format="DD/MM/YYYY"
      leftArrowIcon={<ArrowLeft />}
      rightArrowIcon={<ArrowRight />}
      keyboardIcon={<FontIcon name="icon-calendar" />}
      KeyboardButtonProps={{
        classes: { root: classes.root }
      }}
      clearLabel={I18n.t('calendar.clear')}
      cancelLabel={I18n.t('calendar.cancel')}
      clearable
      mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
      {...customProps}
    />
  );
};

// Props
DatePickerComponent.propTypes = {
  // Value Date
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(moment)]),
  // Label
  label: PropTypes.string,
  // Action
  onChange: PropTypes.func,
  classes: PropTypes.string.isRequired
};

DatePickerComponent.defaultProps = {
  value: '',
  label: '',
  onChange: () => null
};

export default withStyles(styles)(DatePickerComponent);
