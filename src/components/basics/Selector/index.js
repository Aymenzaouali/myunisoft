import React from 'react';
import {
  IconButton, withStyles
} from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import styles from './selector.module.scss';

// Component
const Selector = (props) => {
  const {
    classes,
    current,
    currentIndex,
    data,
    onClickNext,
    onClickPrevious
  } = props;

  return (
    <div className={styles.container}>
      <IconButton
        disabled={currentIndex === 0}
        classes={{ root: classes.rootRotate }}
        onClick={() => onClickPrevious()}
      >
        <FontIcon name="icon-nav" />
      </IconButton>
      <Button classes={{ root: classNames(classes.root, classes.mainPage) }}>
        {current}
      </Button>
      <IconButton
        disabled={currentIndex + 1 === data.length}
        classes={{ root: classes.root }}
        onClick={() => onClickNext()}
      >
        <FontIcon name="icon-nav" />
      </IconButton>
    </div>
  );
};

// Props
Selector.propTypes = {
  current: PropTypes.string,
  currentIndex: PropTypes.number,
  data: PropTypes.arrayOf(),
  onClickNext: PropTypes.func.isRequired,
  onClickPrevious: PropTypes.func.isRequired
};

Selector.defaultProps = {
  current: '',
  currentIndex: -1,
  data: []
};

const themeStyles = () => ({
  rootRotate: {
    transform: 'rotate(180deg)',
    padding: '5px'
  },
  root: {
    padding: '5px',
    minHeight: 0
  },
  mainPage: {
    color: '#0bd1d1',
    minWidth: 33
  }
});

export default withStyles(themeStyles)(Selector);
