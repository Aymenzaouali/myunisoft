import React from 'react';
import TabLoader from 'components/basics/Loader/Tab';
import styles from './tabsLoader.module.scss';

const TabsLoader = () => (
  <div className={styles.container}>
    <TabLoader />
    <TabLoader />
    <TabLoader />
    <TabLoader />
    <TabLoader />
  </div>
);

export default TabsLoader;
