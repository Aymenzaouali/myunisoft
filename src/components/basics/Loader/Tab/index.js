import React from 'react';
import InlineLoader from 'components/basics/Loader/Inline';
import styles from './tabLoader.module.scss';

const TabLoader = () => (
  <div className={styles.container}>
    <div className={styles.text}>
      <InlineLoader />
    </div>
  </div>
);

export default TabLoader;
