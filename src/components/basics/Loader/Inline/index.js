import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './inlineLoader.module.scss';

// Component
const InlineLoader = ({ className }) => (
  <div className={classnames(styles.container, className)}>
    <div className={styles.animatedBackground} />
  </div>
);

// Props
InlineLoader.propTypes = {
  className: PropTypes.string
};

InlineLoader.defaultProps = {
  className: null
};

export default InlineLoader;
