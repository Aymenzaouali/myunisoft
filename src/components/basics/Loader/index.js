import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { ClipLoader } from 'react-spinners';

import styles from './loader.module.scss';

const Loader = ({
  className,
  sizeUnit, size, color
}) => (
  <div className={classnames(styles.container, className)}>
    <ClipLoader
      sizeUnit={sizeUnit}
      size={size}
      color={color}
    />
  </div>
);

Loader.propTypes = {
  className: PropTypes.string,
  sizeUnit: PropTypes.string,
  size: PropTypes.number,
  color: PropTypes.string
};

Loader.defaultProps = {
  className: null,
  sizeUnit: 'px',
  size: 70,
  color: '#0bd1d1'
};

export default Loader;
