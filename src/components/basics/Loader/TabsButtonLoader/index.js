import React from 'react';
import PropTypes from 'prop-types';

import InlineLoader from 'components/basics/Loader/Inline';

import styles from './TabsButtonLoader.module.scss';

// Component
const TabsButtonLoader = ({ count }) => (
  <div className={styles.container}>
    { [...Array(count)].map((_, i) => (
      <div key={i} className={styles.tab}>
        <InlineLoader className={styles.loader} />
      </div>
    )) }
  </div>
);

// Props
TabsButtonLoader.propTypes = {
  count: PropTypes.number
};

TabsButtonLoader.defaultProps = {
  count: 5
};

export default TabsButtonLoader;
