import React from 'react';
import PropTypes from 'prop-types';
import MaterialCheckbox from '@material-ui/core/Checkbox';

import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckboxIcon from '@material-ui/icons/CheckBox';
import IndeterminateCheckBoxIcon from '@material-ui/icons/IndeterminateCheckBox';

// Component
const CheckBox = (props) => {
  const { size, checked, ...others } = props;

  return (
    <MaterialCheckbox
      icon={<CheckBoxOutlineBlankIcon fontSize={size} />}
      checkedIcon={<CheckboxIcon fontSize={size} />}
      indeterminateIcon={<IndeterminateCheckBoxIcon fontSize={size} />}
      color="primary"
      checked={!!checked}
      {...others}
    />
  );
};

// Props
CheckBox.propTypes = {
  size: PropTypes.oneOf(['inherit', 'default', 'small', 'large']),
  checked: PropTypes.bool.isRequired
};

CheckBox.defaultProps = {
  size: 'small'
};

export default CheckBox;
