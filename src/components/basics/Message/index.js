import React from 'react';
import PropTypes from 'prop-types';
import MessageHeader from 'components/basics/Message/Header';
import MessageBody from 'components/basics/Message/Body';
import classNames from 'classnames';
import MessageAttachments from './Attachments';
import { GeneralCommentType } from './types';
import styles from './message.module.scss';

class Message extends React.PureComponent {
  render() {
    const {
      message,
      canEdit,
      onEdit,
      canReact,
      onReact,
      historyState,
      toolbarHidden,
      confirmCancel,
      classes,
      reactions,
      attachments,
      onAttachmentDelete,
      ...others
    } = this.props;

    const {
      avatarImgSrc,
      initials,
      name,
      date
    } = message;

    return (
      <div className={classNames(styles.container, classes)} {...others}>
        <div className={styles.header}>
          <MessageHeader
            initials={initials}
            name={name}
            isEdited={historyState}
            avatarImgSrc={avatarImgSrc}
            date={date}
          />
        </div>
        <MessageBody
          message={message}
          canEdit={canEdit}
          onEdit={onEdit}
          isEdited={historyState}
          canReact={canReact}
          onReact={onReact}
          reactions={reactions}
          toolbarHidden={toolbarHidden}
          confirmCancel={confirmCancel}
        />

        {attachments.length > 0 && (
          <MessageAttachments
            attachments={attachments}
            onDelete={onAttachmentDelete}
          />
        )
        }

      </div>
    );
  }
}

Message.propTypes = {
  message: GeneralCommentType.isRequired,
  historyState: PropTypes.bool,
  canEdit: PropTypes.bool,
  onEdit: PropTypes.func,
  canReact: PropTypes.bool,
  onReact: PropTypes.func,
  reactions: PropTypes.number, // number for beginning
  toolbarHidden: PropTypes.bool,
  confirmCancel: PropTypes.bool,
  classes: PropTypes.string,
  attachments: PropTypes.arrayOf(
    PropTypes.shape({
      id_document: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      nom_original: PropTypes.string,
      size: PropTypes.number
    })
  ),
  onAttachmentDelete: PropTypes.func
};

Message.defaultProps = {
  historyState: false,
  canEdit: true,
  onEdit: () => {},
  canReact: false,
  onReact: () => {},
  reactions: 0,
  toolbarHidden: false,
  confirmCancel: true,
  classes: null,
  attachments: [],
  onAttachmentDelete: () => {}
};

export default Message;
