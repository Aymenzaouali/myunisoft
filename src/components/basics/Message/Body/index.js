/* eslint-disable react/no-danger */
import React, {
  Fragment,
  memo, useState, useMemo
} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { GeneralCommentType } from 'components/groups/Comments/Box/types';
import { Typography, IconButton } from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import TextEditor from 'containers/groups/TextEditor';
import styles from './bodyMessage.module.scss';

// Component
const BodyMessage = (props) => {
  const {
    isEdited, canEdit, onEdit, canReact, onReact, reactions, message, onClick,
    toolbarHidden, confirmCancel
  } = props;
  const commentBodyClassnames = useMemo(() => classNames(
    styles.commentContainer,
    {
      [styles.historyComment]: isEdited,
      [styles.mainComment]: !isEdited
    }
  ), [isEdited]);

  const [isEditing, setIsEditing] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const onStartEdit = () => {
    setIsEditing(true);
  };

  const onCancelEditing = () => {
    setIsEditing(false);
  };

  const handleReact = () => {
    if (canReact && onReact) {
      onReact({ message });
    }
  };

  const onEditSubmit = async (data) => {
    if (!data.body || data.body === message.body) {
      setIsEditing(false);
      return;
    }

    setIsLoading(true);
    await onEdit({
      ...message,
      body: data.body
    });
    setIsEditing(false);
    setIsLoading(false);
  };

  if (isEditing) {
    return (
      <TextEditor
        body={message.body}
        onSubmit={onEditSubmit}
        isLoading={isLoading}
        onCancelCustom={onCancelEditing}
        hideOptions
        toolbarHidden={toolbarHidden}
        confirmCancel={confirmCancel}
      />
    );
  }

  const content = (
    <Fragment>
      <div className={styles.message}>
        <Typography
          variant="body1"
          classes={{ root: styles.content }}
          dangerouslySetInnerHTML={{ __html: message.body }}
        />
      </div>
      { (!isEdited && canEdit) && (
        <IconButton
          className={styles.messageButton}
          onClick={onStartEdit}
        >
          <FontIcon name="icon-pencil" />
        </IconButton>
      )}
      { canReact && (
        <IconButton
          className={classNames(
            styles.messageButton,
            { [styles.semiActive]: (reactions > 0) },
          )}
          onClick={handleReact}
        >
          <FontIcon className={classNames(styles.reaction)} name="icon-smiley" />

          { reactions > 1 && reactions }
          {/* display counter if reactions is 2 or more */}
        </IconButton>
      )}
    </Fragment>
  );

  if (onClick) {
    return (
      <Button
        fullWidth
        component="div"
        classes={{ root: styles.button }}
        onClick={onClick}
      >
        { content }
      </Button>
    );
  }

  return (
    <div className={commentBodyClassnames}>{ content }</div>
  );
};

// Props
BodyMessage.defaultProps = {
  isEdited: false,
  canEdit: true,
  onEdit: () => {},
  canReact: false,
  onReact: () => {},
  reactions: 0,
  toolbarHidden: false,
  confirmCancel: false,
  onClick: undefined
};

BodyMessage.propTypes = {
  message: GeneralCommentType.isRequired,
  isEdited: PropTypes.bool,
  canEdit: PropTypes.bool,
  onEdit: PropTypes.func,
  canReact: PropTypes.bool,
  onReact: PropTypes.func,
  reactions: PropTypes.number,
  toolbarHidden: PropTypes.bool,
  confirmCancel: PropTypes.bool,
  onClick: PropTypes.func
};

export default memo(BodyMessage);
