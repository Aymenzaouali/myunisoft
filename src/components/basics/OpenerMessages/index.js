import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import OpenerContext, { openerDefaults } from 'context/OpenerContext';
import { isSlaveRoute } from 'helpers/routes';

// Component
const OpenerMessages = (props) => {
  const { children, tab_id, dispatch } = props;

  // State
  const [id, setId] = useState(null);
  const [{ msg, from, to }, setMsg] = useState({ msg: {}, from: '', to: '' });

  // Functions
  const send = (type, payload) => {
    const { opener } = window;

    if (opener != null && id != null) {
      const msg = {
        from: id,
        to: 'opener',

        type,
        payload
      };

      console.debug('send', msg);
      opener.postMessage(msg, window.location.origin);
    }
  };

  const sendState = (state) => {
    if (state) {
      send('state', state);
    }
  };

  const sendAction = (action) => {
    if (action) {
      send('action', {
        tab_id,
        ...action
      });
    }
  };

  const sendMessage = (message) => {
    if (message) {
      send('message', message);
    }
  };

  const focusOpener = () => {
    const { opener } = window;
    opener.focus(); // but it don't work on firefox or chrome ...
  };

  const onMessage = (e) => {
    const { data, origin } = e;

    // Refuse messages from another origin
    if (origin !== window.location.origin) return;

    // Parse message if is from opener
    if (data.from === 'opener') {
      console.debug('recv', data);

      switch (data.type) {
      case 'init':
        setId(data.payload.id || null);
        break;

      case 'action':
        dispatch({
          tab_id,
          ...data.payload
        });

        break;

      case 'message':
        setMsg({
          from: data.from,
          to: data.to,
          msg: data.payload
        });
        break;

      default:
      }
    }
  };

  const onFocusBlur = (e) => {
    sendState({ focused: e.type === 'focus' });
  };

  // Effects
  // - manage event listeners (must be remounted on each update)
  useEffect(() => {
    window.addEventListener('message', onMessage, false);
    window.addEventListener('focus', onFocusBlur, false);
    window.addEventListener('blur', onFocusBlur, false);

    return () => {
      window.removeEventListener('message', onMessage);
      window.removeEventListener('focus', onFocusBlur);
      window.removeEventListener('blur', onFocusBlur);
    };
  });

  // send the focus state to the opener
  useEffect(() => {
    sendState({ focused: document.hasFocus() });
  }, [id]);

  // Rendering
  return (
    <OpenerContext.Provider
      value={{
        ...openerDefaults,

        message: msg,
        from,
        to,

        isPopup: window.opener != null && isSlaveRoute(window.location.pathname),

        sendAction,
        sendMessage,
        focusOpener
      }}
    >
      { children }
    </OpenerContext.Provider>
  );
};

// Props
OpenerMessages.propTypes = {
  children: PropTypes.element.isRequired,

  tab_id: PropTypes.oneOfType([
    PropTypes.string, PropTypes.number
  ]),
  dispatch: PropTypes.func.isRequired
};

OpenerMessages.defaultProps = {
  tab_id: undefined
};

export default OpenerMessages;
