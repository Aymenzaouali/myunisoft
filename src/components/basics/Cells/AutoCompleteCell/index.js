import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';
import CreatableSelect from 'react-select/lib/Creatable';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import { TextFieldCell } from 'components/basics/Cells';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';
import { emphasize } from '@material-ui/core/styles/colorManipulator';

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  input: {
    display: 'flex',
    padding: 0
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'nowrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden'
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    )
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  },
  singleValue: {
    fontSize: 12
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 12
  },
  paper: {
    position: 'absolute',
    zIndex: 20,
    marginTop: theme.spacing.unit,
    left: 0
  },
  divider: {
    height: theme.spacing.unit * 2
  },
  underlineBorder: {
    '&:before': {
      'border-bottom': 'none'
    },
    '&:after': {
      'border-bottom': 'none'
    },
    '&&&&:hover:before': {
      'border-bottom': 'none'
    }
  },
  infoText: {
    position: 'absolute',
    top: '50%',
    left: '105%',
    whiteSpace: 'nowrap',
    fontSize: 12,
    color: '#9b9b9b'
  }
});

function NoOptionsMessage({ selectProps, children, innerProps }) {
  return (
    <Typography
      color="textSecondary"
      className={selectProps.classes.noOptionsMessage}
      {...innerProps}
    >
      {children}
    </Typography>
  );
}

NoOptionsMessage.propTypes = {
  selectProps: PropTypes.object.isRequired,
  innerProps: PropTypes.object,
  children: PropTypes.node
};

NoOptionsMessage.defaultProps = {
  innerProps: {},
  children: null
};

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

inputComponent.propTypes = {
  inputRef: PropTypes.func
};

inputComponent.defaultProps = {
  inputRef: undefined
};

function Control({
  selectProps,
  innerRef,
  children,
  innerProps
}) {
  return (
    <TextFieldCell
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: selectProps.classes.input,
          inputRef: innerRef,
          children,
          ...innerProps
        },
        classes: selectProps.border ? {
          underline: selectProps.classes.underlineBorder
        }
          : {}
      }}
      {...selectProps.textFieldProps}
    />
  );
}

Control.propTypes = {
  selectProps: PropTypes.object.isRequired,
  innerProps: PropTypes.object,
  children: PropTypes.node,
  innerRef: PropTypes.func
};

Control.defaultProps = {
  innerProps: {},
  children: null,
  innerRef: undefined
};

function Option(props) {
  const {
    innerRef,
    isFocused,
    isSelected,
    innerProps,
    children,
    selectProps
  } = props;
  return (
    <MenuItem
      buttonRef={innerRef}
      selected={isFocused}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
        fontSize: '12px',
        color: isSelected ? '#0bd1d1' : 'inherit',
        ...selectProps.optionStyle && selectProps.optionStyle(props)
      }}
      {...innerProps}
    >
      { selectProps.optionRender
        ? selectProps.optionRender(props)
        : <span>{ children }</span>
      }
      {/* {children} */}
    </MenuItem>
  );
}

Option.propTypes = {
  innerProps: PropTypes.object,
  children: PropTypes.node,
  innerRef: PropTypes.func,
  isSelected: PropTypes.bool,
  isFocused: PropTypes.bool,
  selectProps: PropTypes.shape({})
};

Option.defaultProps = {
  innerProps: {},
  children: null,
  innerRef: undefined,
  isSelected: false,
  isFocused: false,
  selectProps: {}
};

function Placeholder({ selectProps, innerProps, children }) {
  return (
    <Typography
      color="textSecondary"
      className={selectProps.classes.placeholder}
      {...innerProps}
    >
      {children}
    </Typography>
  );
}

Placeholder.propTypes = {
  selectProps: PropTypes.object.isRequired,
  innerProps: PropTypes.object,
  children: PropTypes.node
};

Placeholder.defaultProps = {
  innerProps: {},
  children: null
};

function SingleValue({ selectProps, innerProps, children }) {
  const {
    getValueLabel, value
  } = selectProps;
  return (
    <Typography className={selectProps.classes.singleValue} {...innerProps}>
      {getValueLabel ? getValueLabel(value) : children}
    </Typography>
  );
}

SingleValue.propTypes = {
  selectProps: PropTypes.object.isRequired,
  innerProps: PropTypes.object,
  children: PropTypes.node
};

SingleValue.defaultProps = {
  innerProps: {},
  children: null
};


function ValueContainer({ selectProps, children }) {
  return <div className={selectProps.classes.valueContainer}>{children}</div>;
}

ValueContainer.propTypes = {
  selectProps: PropTypes.object.isRequired,
  children: PropTypes.node
};

ValueContainer.defaultProps = {
  children: null
};


function MultiValue({
  children,
  selectProps,
  isFocused,
  removeProps
}) {
  return (
    <Chip
      tabIndex={-1}
      label={children}
      className={classNames(selectProps.classes.chip, {
        [selectProps.classes.chipFocused]: isFocused
      })}
      onDelete={removeProps.onClick}
      deleteIcon={<CancelIcon {...removeProps} />}
    />
  );
}

MultiValue.propTypes = {
  selectProps: PropTypes.object.isRequired,
  innerProps: PropTypes.object,
  removeProps: PropTypes.object.isRequired,
  children: PropTypes.node,
  isFocused: PropTypes.bool
};

MultiValue.defaultProps = {
  innerProps: {},
  children: null,
  isFocused: false
};


function Menu({ selectProps, innerProps, children }) {
  return (
    <Paper square className={selectProps.classes.paper} {...innerProps}>
      {children}
    </Paper>
  );
}

Menu.propTypes = {
  selectProps: PropTypes.object.isRequired,
  innerProps: PropTypes.object,
  children: PropTypes.node
};

Menu.defaultProps = {
  innerProps: {},
  children: null
};

const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer,
  ClearIndicator: null,
  IndicatorSeparator: null,
  DropdownIndicator: null
};

class IntegrationReactSelect extends React.Component {
  state = {
    single: null
  };

  componentDidMount() {
    const { loadData } = this.props;
    if (loadData) {
      loadData();
    }
  }

  handleChange = name => (value) => {
    const { onChange } = this.props;
    if (onChange) onChange(value);
    this.setState({
      [name]: value
    });
  };

  render() {
    const {
      classes,
      theme,
      onChange,
      code,
      isCreatable,
      menuPosition,
      containerProps,
      selectStyles,
      customRef,
      ...otherProps
    } = this.props;
    const { single } = this.state;

    const styles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit'
        },
        ...selectStyles.input
      }),
      menuList: base => ({
        ...base,
        ...selectStyles.menuList
        // height: 240
      })
    };

    const Wrapper = isCreatable ? CreatableSelect : Select;

    return (
      <div className={classes.root} {...containerProps}>
        <NoSsr>
          <Wrapper
            classes={classes}
            styles={{
              ...styles,
              menuPortal: provided => ({
                ...provided,
                zIndex: 10000
              })
            }}
            components={components}
            ref={customRef}
            value={single}
            onChange={this.handleChange('single')}
            isClearable
            {...otherProps}
            menuPosition="absolute"
            menuPlacement="auto"
            menuPortalTarget={document.body}
          />
          {otherProps.value && otherProps.value.info && (
            <small className={classes.infoText}>{otherProps.value.info}</small>
          )}
        </NoSsr>
      </div>
    );
  }
}

IntegrationReactSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  onChange: PropTypes.func,
  code: PropTypes.bool,
  selectStyles: PropTypes.shape({
    input: PropTypes.shape({}),
    menuList: PropTypes.shape({})
  }),
  loadData: PropTypes.func,
  customRef: PropTypes.func,
  containerProps: PropTypes.object,
  isCreatable: PropTypes.bool,
  menuPosition: PropTypes.string
};

IntegrationReactSelect.defaultProps = {
  onChange: undefined,
  code: false,
  selectStyles: {
    input: {},
    menuList: {}
  },
  loadData: undefined,
  customRef: () => {},
  containerProps: {},
  isCreatable: false,
  menuPosition: 'absolute'
};

export default withStyles(styles, { withTheme: true })(IntegrationReactSelect);
