import React from 'react';
import PropTypes from 'prop-types';

import StatusCell from '../StatusCell';

import styles from './HistoryCell.module.scss';

// Component
const HistoryCell = (props) => {
  const { data } = props;

  return (
    <div className={styles.historyCell}>
      { data.map(({
        status, value, onClick = () => {}, onRightClick, clickable = false
      }, i) => (
        <div key={i} className={styles.status} onContextMenu={onRightClick}>
          <StatusCell data={{ status, onClick, clickable }} />
          { value }
        </div>
      )) }
    </div>
  );
};

// Props
HistoryCell.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      status: PropTypes.oneOf(['ok', 'in-progress', 'error', 'time-out', 'to-late']).isRequired,
      value: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};

export default HistoryCell;
