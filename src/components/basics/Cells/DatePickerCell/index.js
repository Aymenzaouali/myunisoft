import React from 'react';
import { DatePicker } from 'components/basics/Inputs';
import styles from './DatePickerCell.module.scss';

const DatePickerCell = props => (
  <DatePicker className={styles.datePickerCell} {...props} />
);

export default DatePickerCell;
