import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { TextField } from '@material-ui/core';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const TextFieldCell = (props) => {
  const { className } = props;
  return (
    <TextField {...props} className={classnames(cellStyles.textField, className)} />
  );
};

TextFieldCell.propTypes = {
  className: PropTypes.string
};

TextFieldCell.defaultProps = {
  className: undefined
};

export default TextFieldCell;
