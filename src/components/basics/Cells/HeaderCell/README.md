`DESCRIPTION:`

This cell provides the different header. 
You can add new header type

`USAGE:`

Here are the available types:

- [``StringCell``](#!/StringCell)
- [``SortCell``](#!/CustomTableSortLabel)
- [``Checkbox``](#!/CheckboxCell)
- [``Select``](#!/SelectCell)
- [``sortCell``](#!/sortCell)

Exemple
```js
  <HeaderCell 
    props={{
      label: 'test',
      onClick: () => alert("Demo")
    }}
    keyCell="key"
    _type="string" />
```

if you use a very specific cell you can use no type and add a children

Exemple:
```js
  <HeaderCell keyCell="key">
    <div>Example</div>
  </HeaderCell>
```

