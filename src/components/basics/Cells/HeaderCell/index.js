import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { TableCell } from '@material-ui/core';
import classNames from 'classnames';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';
import StringCell from '../StringCell';
import SortCell from '../SortCell';
import CheckBoxCell from '../CheckBoxCell';
import SelectCell from '../SelectCell';

import styles from './HeaderCell.module.scss';

// Props
const HeaderCell = (props) => {
  const {
    _type,
    keyCell,
    children,
    cellProp: {
      isAmount,
      isCentered,
      paddingLeft,
      ...cellProp
    },
    component: Component,
    props: childProps
  } = props;

  return (
    <TableCell
      key={keyCell}
      {...cellProp}
      className={classNames(cellStyles.cell, _.get(cellProp, 'className'))}
      classes={{
        root: classNames({
          [styles.isCentered]: isCentered,
          [styles.isAmount]: isAmount,
          [styles.paddingLeft]: paddingLeft
        })
      }}
    >
      {Component
        ? (
          <Component {...childProps} />
        )
        : (
          <Fragment>
            {_type === 'button' && <SelectCell key={keyCell} {...childProps} />}
            {_type === 'checkbox' && <CheckBoxCell key={keyCell} {...childProps} />}
            {_type === 'select' && <SelectCell key={keyCell} {...childProps} />}
            {_type === 'string' && <StringCell key={keyCell} {...childProps} />}
            {_type === 'sortCell' && <SortCell key={keyCell} {...childProps} />}
            {_.isEmpty(_type) && children}
          </Fragment>
        )
      }
    </TableCell>
  );
};

// Props
HeaderCell.propTypes = {
  _type: PropTypes.oneOf([
    'button',
    'checkbox',
    'select',
    'string',
    'sortCell',
    ''
  ]), /** cell type */
  keyCell: PropTypes.any, /** unique cell key */
  children: PropTypes.node, /** cell children */
  component: PropTypes.element,

  cellProp: PropTypes.shape({
    isAmount: PropTypes.bool,
    paddingLeft: PropTypes.bool,
    isCentered: PropTypes.bool
  }), /** TableCell props (MaterialUi) */

  props: PropTypes.shape({}) /** cell props (bind to type) */
};

HeaderCell.defaultProps = {
  _type: '',
  keyCell: null,
  children: null,
  component: undefined,

  cellProp: {
    isAmount: false,
    paddingLeft: false,
    isCentered: false
  },

  props: {}
};

export default HeaderCell;
