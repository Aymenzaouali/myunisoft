import React from 'react';
import { TableCell, Typography } from '@material-ui/core';
import I18n from 'assets/I18n';

// Component
const NoDataCell = props => (
  <TableCell {...props}>
    <Typography variant="h6" align="center" color="textSecondary">
      { I18n.t('tables.noData') }
    </Typography>
  </TableCell>
);

export default NoDataCell;
