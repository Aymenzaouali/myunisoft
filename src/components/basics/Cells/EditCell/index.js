import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import StringCell from 'components/basics/Cells/StringCell';
import { ReduxTextField, ReduxInput } from 'components/reduxForm/Inputs';
import _ from 'lodash';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

// Component
const EditCell = (props) => {
  const {
    label,
    name,
    type,
    onCustomChange,
    parse,
    format,
    component,
    error,
    ...others
  } = props;
  // State
  const [isEditing, setIsEditing] = useState(false);
  const [value, setValue] = useState(label);
  const inputRef = useRef(null);

  // Events
  const onCellClick = () => {
    setIsEditing(true);
    // setTimout to wait for the inputRef to be filled (because not in the DOM when clicking)
    setTimeout(() => {
      if (_.get(inputRef, 'current.focus', false)) {
        inputRef.current.focus();
      }
    }, 0);
  };

  const onTextFieldBlur = (e) => {
    setIsEditing(false);
    if (onCustomChange) onCustomChange(e.target.value);
  };
  const onTextFieldChange = (value) => {
    setValue(_.get(value, 'target.value') || value);
  };

  const onBlurProps = (component !== ReduxInput) ? {
    inputProps: {
      onBlur: onTextFieldBlur
    }
  } : {
    onBlur: onTextFieldBlur
  };
  // Rendering
  if (isEditing) {
    return (
      <Field
        className={cellStyles.fieldCell}
        inputRef={inputRef}
        name={name}
        margin="none"
        type={type}
        parse={parse}
        format={format}
        component={component}
        onCustomBlur={onTextFieldBlur}
        value={value}
        placeholder={value}
        onCustomChange={onTextFieldChange}
        {...onBlurProps}
      />
    );
  }

  return (
    <StringCell
      {...others}
      label={error || value}
      onClick={onCellClick}
    />
  );
};

// Props
EditCell.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  onCustomChange: PropTypes.func,
  parse: PropTypes.func,
  format: PropTypes.func,
  error: PropTypes.string,
  component: PropTypes.shape({})
};

EditCell.defaultProps = {
  label: '',
  type: 'text',
  onCustomChange: null,
  component: ReduxTextField,
  parse: undefined,
  format: undefined,
  error: ''
};

export default EditCell;
