import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Icon, Typography } from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';

import styles from './StringCell.module.scss';

// Components
function StringCell(props) {
  const {
    label,
    detail,
    iconLeft,
    iconRight,
    className,
    cellClassName,
    stringClassName,
    labelClassName,
    detailClassName,
    iconLeftClassName,
    iconRightClassName,
    onClick,
    value,
    classes,
    modification,
    typoVariant,
    isAmount,
    paddingLeft,
    noWrap,
    colorError,
    buttonRef,
    labelWidth,
    ...otherProps
  } = props;

  const StringCell = (
    <div className={classNames(cellClassName, className)} {...otherProps}>
      {iconLeft && (
        <Icon className={iconLeftClassName}>
          <span className={iconLeft} />
        </Icon>
      )}

      <div
        className={
          classNames({
            [styles.isAmount]: isAmount,
            [styles.paddingLeft]: paddingLeft,
            stringClassName
          })
        }
      >
        <div className={labelClassName}>
          <Typography
            variant={typoVariant}
            color={colorError ? 'secondary' : 'default'}
            noWrap={noWrap}
            title={labelWidth && label}
            style={labelWidth && {
              width: labelWidth,
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis'
            }}
          >
            {label}
          </Typography>
        </div>
        <div className={detailClassName}>
          {detail}
        </div>
      </div>

      {iconRight && (
        <Icon className={iconRightClassName}>
          <span className={iconRight} />
        </Icon>
      )}
    </div>
  );

  if (onClick != null) {
    return (
      <Button
        buttonRef={buttonRef}
        type="button"
        classes={{
          root: classNames(
            {
              [styles.isAmount]: isAmount,
              [styles.modify]: modification
            }, styles.button
          )
        }}
        onClick={onClick}
      >
        {StringCell}
      </Button>
    );
  }

  return StringCell;
}

StringCell.propTypes = {
  /** label use */
  label: PropTypes.node,
  /** detail of cell */
  detail: PropTypes.string,

  /** icon left */
  iconLeft: PropTypes.string,

  /** icon right */
  iconRight: PropTypes.string,

  /** Overide a CSS */
  cellClassName: PropTypes.string,
  stringClassName: PropTypes.string,
  labelClassName: PropTypes.string,
  detailClassName: PropTypes.string,
  iconLeftClassName: PropTypes.string,
  iconRightClassName: PropTypes.string,

  classes: PropTypes.shape({}),
  modification: PropTypes.bool,

  /** Action onClick */
  onClick: PropTypes.func,

  /** Value use in Action */
  value: PropTypes.any, // eslint-disable-line react/forbid-prop-types

  typoVariant: PropTypes.string,
  isAmount: PropTypes.bool,
  paddingLeft: PropTypes.bool,
  noWrap: PropTypes.bool,
  colorError: PropTypes.bool
};

StringCell.defaultProps = {
  isAmount: false,
  paddingLeft: false,
  label: '',
  detail: '',
  iconLeft: undefined,
  iconRight: undefined,
  cellClassName: '',
  stringClassName: '',
  labelClassName: '',
  detailClassName: '',
  iconLeftClassName: '',
  iconRightClassName: '',
  classes: {},
  modification: false,
  onClick: null,
  value: null,
  noWrap: false,
  typoVariant: 'body2',
  colorError: false
};

export default StringCell;

// import _ from 'lodash';
// import React from 'react';
// import PropTypes from 'prop-types';
// import {
//   Icon, Button, Typography, withStyles
// } from '@material-ui/core';

// /**
//  * Cells string
// */
// class StringCell extends React.PureComponent {
//   render() {
//     const {
//       label,
//       detail,
//       icon,
//       cellClassName,
//       stringClassName,
//       onClick,
//       value,
//       classes,
//       modification
//     } = this.props;

//     const isLabel = label
//       ? <Typography class={label.className}>{label.value}</Typography>
//       : null;

//     const isDetail = detail
//       ? <Typography class={detail.className}>{detail.value}</Typography>
//       : null;

//     const isIconLeft = icon.left
//       ? <Icon className={`${icon.left.className} ${icon.left.name}`} />
//       : null;

//     const isIconRight = icon.right
//       ? <Icon className={`${icon.right.className} ${icon.right.name}`} />
//       : null;

//     let StringCell = (
//       <div className={cellClassName}>
//         {isIconLeft}
//         <div className={stringClassName}>
//           {isLabel}
//           {isDetail}
//         </div>
//         {isIconRight}
//       </div>
//     );

//     if (!_.isNull(onClick)) {
//       StringCell = (
//         <Button
//            type="button"
//            classes={{ root: modification && classes.modify }}
//            onClick={() => onClick(value)}
//          >
//           {StringCell}
//         </Button>
//       );
//     }
//     return StringCell;
//   }
// }

// StringCell.propTypes = {
//   /** label use */
//   label: PropTypes.shape({
//     value: PropTypes.string,
//     className: PropTypes.string
//   }).isRequired,
//   /** detail of cell */
//   detail: PropTypes.shape({
//     value: PropTypes.string,
//     className: PropTypes.string
//   }),
//   /** icon object contains all icons */
//   icon: PropTypes.shape({
//     left: PropTypes.shape({
//       value: PropTypes.string,
//       className: PropTypes.string
//     }),
//     right: PropTypes.shape({
//       value: PropTypes.string,
//       className: PropTypes.string
//     })
//   }),
//   /** Overide a CSS */
//   cellClassName: PropTypes.string,
//   stringClassName: PropTypes.string,
//   classes: PropTypes.shape({}),
//   modification: PropTypes.bool,
//   /** Action onClick */
//   onClick: PropTypes.func,
//   /** Value use in Action */
//   value: PropTypes.node
// };

// StringCell.defaultProps = {
//   detail: {},
//   icon: {},
//   cellClassName: '',
//   stringClassName: '',
//   classes: {},
//   modification: false,
//   onClick: null,
//   value: null
// };

// const styles = () => ({
//   modify: {
//     color: '#0bd1d1',
//     'border-style': 'none',
//     'background-color': 'white',
//     'text-decoration': 'underline'
//   }
// });

// export default withStyles(styles)(StringCell);
