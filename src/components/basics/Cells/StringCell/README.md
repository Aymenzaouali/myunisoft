This component is a Dynamic Cell with possible icon left, right or both and detail. 


Param:

  *  **label**: Main information of cell,
  *  **detail**: (optional) detail of cell like date or other,
  *  **iconLeft**: (optional) left icon name,
  *  **iconRight**: (optional) right icon name,
  *  **cellClassName**: global cell's classname,
  *  **stringClassName**: classname for label and detail parts,
  *  **labelClassName**: classname for label specifically,
  *  **detailClassName**: classname for detail specifically,
  *  **iconLeftClassName**: classname for icon left,
  *  **iconRightClassName**: classname form icon right,


``StringCell``

``Props:``
  *  **label**: Main information of cell,
  *  **detail**: (optional) detail of cell like date or other,
  *  **iconLeft**: (optional) left icon name,
  *  **iconRight**: (optional) right icon name,
  *  **cellClassName**: global cell's classname,
  *  **stringClassName**: classname for label and detail parts,
  *  **labelClassName**: classname for label specifically,
  *  **detailClassName**: classname for detail specifically,
  *  **iconLeftClassName**: classname for icon left,
  *  **iconRightClassName**: classname for icon right,
  *  **onClick**: action
  *  **value**: value use in action
  * **classes**: styles classes for button
  * **modification**: to apply style to button


``simple example:``

```js
<StringCell
  label="test"/>
```

``example with this classname:``

```css
.test {
  color: pink;
}
```

```js
<StringCell
  label="test"
  className='test'/>
```
``example with action:``

```js
<StringCell
  value="troll"
  onClick={(value) => console.log(value)}
  label="test"/>
```
