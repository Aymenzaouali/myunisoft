import React from 'react';
import PropTypes from 'prop-types';
import { FontIcon } from 'components/basics/Icon';
import { Typography, Badge } from '@material-ui/core';
import i18n from 'i18next';

import styles from './styles.module.scss';

// Component
const StatusCell = (props) => {
  const {
    data,
    clickable
  } = props;

  const render = (data, key) => {
    let render;

    switch (data.status) {
    case 'ok':
      render = <Typography key={key} color="primary" classes={{ root: clickable ? styles.okClickable : styles.ok }} onClick={data.onClick && data.onClick}>{ i18n.t('common.status.ok') }</Typography>;
      break;
    case 'to-late':
      render = <Typography key={key} color="error" classes={{ root: clickable ? styles.okClickable : styles.ok }} onClick={data.onClick && data.onClick}>{ i18n.t('common.status.ok') }</Typography>;
      break;
    case 'error':
      render = <FontIcon classes={{ root: styles[data.status] }} name="icon-alert" color="#fe3a5e" onClick={data.onClick} />;
      break;
    default:
      render = (
        <div key={key} className={clickable ? styles.badgeClickable : styles.badge}>
          <Badge
            badgeContent={data.value}
            classes={{ root: styles[data.status] }}
            onClick={data.onClick}
          >
            {''}
          </Badge>
        </div>
      );
      break;
    }

    return render;
  };

  return (
    Array.isArray(data) ? (
      <div className={styles.container}>
        { data.map(render) }
      </div>
    ) : (
      render(data)
    )
  );
};

// Props
const dataShape = PropTypes.shape({
  status: PropTypes.oneOf(['ok', 'in-progress', 'error', 'to-late', 'time-out', 'pjs', 'entry_pjs', 'count']).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onClick: PropTypes.func
});

StatusCell.propTypes = {
  data: PropTypes.oneOfType([
    PropTypes.arrayOf(dataShape),
    dataShape
  ]).isRequired,
  clickable: PropTypes.bool
};

StatusCell.defaultProps = {
  clickable: true
};

export default StatusCell;
