Display Cell Status.
Cell Status have 6 differentes states:
  "ok"
  "in-progress"
  "error"
  "pjs"
  "entry_pjs"
  "count"

StatusCell with statu "ok":
```
  <StatusCell status="ok" value='Example' />
```

StatusCell with statu "in-progress":
```
  <StatusCell status="in-progress" value='Example' />
```

StatusCell with statu "error":
```
  <StatusCell status="error" value='Example' />
```

StatusCell with statu "pjs":
```
  <StatusCell status="pjs" value='Example' />
```

StatusCell with statu "entry_pjs":
```
  <StatusCell status="entry_pjs" value='Example' />
```

StatusCell with statu "count":
```
  <StatusCell status="count" value='Example' />
```
