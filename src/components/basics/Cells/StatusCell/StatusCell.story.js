import React from 'react';
import { storiesOf } from '@storybook/react';
import { select, text } from '@storybook/addon-knobs';
import withReadme from 'storybook-readme/with-readme';

import Readme from './README.md';
import StatusCell from './index';

const stories = storiesOf('StatusCell', module);
/**
 * v4 of package not working with Storybook v5.
 * Waiting fix.
 * @see https://github.com/tuchk4/storybook-readme/issues/111
 */
stories.addDecorator(withReadme(Readme));

stories.add('StatusCell', () => {
  const options = [
    'ok',
    'error',
    'in-progress',
    'pjs',
    'entry_pjs',
    'count'
  ];
  const status = select('Status cell', options, 'ok');
  const value = text('Value cell', '1');

  return (
    <StatusCell
      data={{
        status,
        value
      }}
    />
  );
}, {
  info: {
    text: 'StringCell with status'
  }
});
