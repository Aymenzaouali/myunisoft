import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { TableCell } from '@material-ui/core';
import Loader from 'components/basics/Loader';
import styles from './loadingCell.module.scss';

// Component
const LoadingCell = (props) => {
  const { className } = props;

  return (
    <TableCell {...props} className={classnames(styles.loadingCell, className)}>
      <Loader />
    </TableCell>
  );
};

LoadingCell.propTypes = {
  className: PropTypes.string
};

LoadingCell.defaultProps = {
  className: undefined
};

export default LoadingCell;
