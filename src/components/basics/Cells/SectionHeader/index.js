import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Typography } from '@material-ui/core';
import './styles.scss';

const SectionHeader = (props) => {
  const {
    key,
    sectionTitle,
    sectionAmount,
    amountStyle
  } = props;

  return (
    <div className="cellContainer">
      <ListItem key={key} className="cellContainer__leftItem">
        <ListItemText primary={sectionTitle} />
      </ListItem>
      <ListItem key={key} className="cellContainer__rightItem">
        <ListItemText
          disableTypography
          primary={<Typography style={amountStyle}>{sectionAmount}</Typography>}
        />
      </ListItem>
    </div>
  );
};

SectionHeader.defaultProps = {
  amountStyle: {}
};

SectionHeader.propTypes = {
  key: PropTypes.string.isRequired,
  sectionTitle: PropTypes.string.isRequired,
  sectionAmount: PropTypes.string.isRequired,
  amountStyle: PropTypes.shape({})
};

export default SectionHeader;
