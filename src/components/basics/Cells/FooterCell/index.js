import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _ from 'lodash';

import { TableCell } from '@material-ui/core';

import Pagination from 'components/basics/Pagination';
import StringCell from '../StringCell';
import CheckBoxCell from '../CheckBoxCell';
import SelectCell from '../SelectCell';

import styles from './FooterCell.module.scss';

// Component
const FooterCell = (props) => {
  const {
    _type,
    children,
    cellProp: {
      isNumber,
      isTotal,
      ...cellProp
    },
    props: childProps,
    keyCell,
    component: Component
  } = props;

  return (
    <TableCell
      key={keyCell}
      {...cellProp}
      classes={{
        root: classNames({
          [styles.number]: isNumber,
          [styles.total]: isTotal
        })
      }}
    >
      {Component
        ? (
          <Component {...childProps} />
        )
        : (
          <Fragment>
            {_type === 'checkbox' && <CheckBoxCell key={keyCell} {...childProps} />}
            {_type === 'pagination' && <Pagination key={keyCell} {...childProps} />}
            {_type === 'select' && <SelectCell key={keyCell} {...childProps} />}
            {_type === 'string' && <StringCell key={keyCell} {...childProps} />}
            {_.isEmpty(_type) && children}
          </Fragment>
        )
      }
    </TableCell>
  );
};

// Props
FooterCell.propTypes = {
  _type: PropTypes.oneOf([
    'string',
    'checkbox',
    'select',
    'pagination',
    ''
  ]), /** cell type */
  keyCell: PropTypes.any, /** unique cell key */
  children: PropTypes.node, /** cell children */
  component: PropTypes.element,

  cellProp: PropTypes.shape({
    isNumber: PropTypes.bool,
    isTotal: PropTypes.bool
  }), /** TableCell props (MaterialUi) */

  props: PropTypes.shape({}) /** cell props (bind to type) */
};

FooterCell.defaultProps = {
  _type: '',
  keyCell: null,
  children: null,
  component: undefined,

  cellProp: {
    isNumber: false,
    isTotal: false
  },

  props: {}
};

export default FooterCell;
