import React from 'react';
import FontIcon from 'components/basics/Icon/Font';

const IconCell = props => (
  <FontIcon
    {...props}
  />
);

export default IconCell;
