``SelectCell``

``Props:``
  * value: Value use in action
  * onChange: Function to change the value
  * renderList: Value of the array to display in the list
  * className: class for override css
  * list: object array used
  * All other props used for Select, Input and InputBase


For more details on the props of Cell look:
  - [Select API](https://material-ui.com/api/select/#css-api)
  - [Input API](https://material-ui.com/api/input/)
  - [InputBase API](https://material-ui.com/api/input-base/)

``simple example:``

```js
<SelectCell
	value='value'
  list={[{
    value: 'toto'
    },
    {
    value: 'tata'
    },
    {
    value: 'titi'
    },
    {
    value: 'tutu'
    }
  ]}
  renderList={(option) => <span>{option.value}</span>}
/>
```
``example with this classname:``

```css
.testSelectCell {
  width: 150px;
}
```

```js
<SelectCell
	value='value'
  list={[{
    value: 'toto'
    },
    {
    value: 'tata'
    },
    {
    value: 'titi'
    },
    {
    value: 'tutu'
    }
  ]}
  renderList={(option) => <span>{option.value}</span>}
  className='testSelectCell '
/>
```
``example with action onChange:``

```js
<SelectCell
	onChange={(event) => console.log(event.target.value)}
	value="value"
  list={[{
    value: 'toto'
    },
    {
    value: 'tata'
    },
    {
    value: 'titi'
    },
    {
    value: 'tutu'
    }
  ]}
  renderList={(option) => <span>{option.value}</span>}
/>
```