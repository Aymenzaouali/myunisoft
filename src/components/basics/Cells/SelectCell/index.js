import React from 'react';
import PropTypes from 'prop-types';
import { Select, MenuItem } from '@material-ui/core';

import styles from './SelectCell.module.scss';

// Component
const SelectCell = (props) => {
  const {
    className, disabled,
    value, list,
    renderValue, renderList = o => renderValue(o.value),
    onChange
  } = props;

  // Rendering
  return (
    <Select
      autoWidth
      disableUnderline

      color="primary"
      disabled={disabled}
      className={className}

      value={value}
      renderValue={renderValue}
      onChange={onChange}
      onClick={event => event.stopPropagation()}

      classes={{
        select: styles.input,
        disabled: styles.disabled,
        icon: styles.icon
      }}
    >
      { list.map(opt => (
        <MenuItem
          key={opt.value}
          value={opt.value}
          className={styles.text}
        >
          { renderList(opt) }
        </MenuItem>
      )) }
    </Select>
  );
};

// Props
SelectCell.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,

  value: PropTypes.string,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  renderValue: PropTypes.func,
  renderList: PropTypes.func,

  onChange: PropTypes.func.isRequired
};

SelectCell.defaultProps = {
  className: styles.select,
  disabled: false,

  value: '',
  renderValue: v => v,
  renderList: undefined
};

export default SelectCell;
