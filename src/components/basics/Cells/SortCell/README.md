Cell for table sort

The other props will be injected into the MaterialUi TableSortLabel element

- [``TableSortLabel``](https://material-ui.com/api/table-sort-label/#tablesortlabel-api)

```js
<TableSort>
  <span>HelloWorld</span>
</TableSort>
```

```js
<TableSort direction='desc'>
  <span>HelloWorld</span>
</TableSort>
```
