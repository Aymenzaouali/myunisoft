import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { TableSortLabel } from '@material-ui/core';
import ArrowIcon from 'components/basics/Icon/Arrow';
import classNames from 'classnames';

import styles from './SortCell.module.scss';

const SortCell = (props) => {
  const {
    direction,
    isAmount,
    isCentered,
    paddingLeft,
    children,
    classes,
    colorError,
    isDisabled,
    onSort,
    ...otherProps
  } = props;

  const [sortOrder, changeSortOrder] = useState(direction || 'desc');

  return (
    <TableSortLabel
      classes={{
        root: classNames({
          [styles.isCentered]: isCentered,
          [styles.isAmount]: isAmount,
          [styles.paddingLeft]: paddingLeft,
          [styles.isDisabled]: isDisabled
        })
      }}
      onClick={() => {
        onSort(sortOrder);
        changeSortOrder(sortOrder === 'desc' ? 'asc' : 'desc');
      }}
      {...otherProps}
      IconComponent={() => <ArrowIcon direction={direction || sortOrder} colorError={colorError} />}
    >
      {children}
    </TableSortLabel>
  );
};

SortCell.propTypes = {
  /** content TableSortLabel * */
  children: PropTypes.node.isRequired,
  /** constent TableSortLabel * */
  direction: PropTypes.oneOf(['asc', 'desc', '']),
  colorError: PropTypes.bool,
  onSort: PropTypes.func,
  isAmount: PropTypes.bool,
  isCentered: PropTypes.bool,
  paddingLeft: PropTypes.bool,
  isDisabled: PropTypes.bool
};

SortCell.defaultProps = {
  direction: '',
  onSort: () => {},
  isAmount: false,
  isCentered: false,
  paddingLeft: false,
  colorError: false,
  isDisabled: false
};

export default SortCell;
