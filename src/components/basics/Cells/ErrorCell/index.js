import React from 'react';
import { TableCell } from '@material-ui/core';
import I18n from 'assets/I18n';
import Info from 'components/basics/Info';

const ErrorCell = props => (
  <TableCell {...props}>
    <Info message={I18n.t('tables.error')} />
  </TableCell>
);

export default ErrorCell;
