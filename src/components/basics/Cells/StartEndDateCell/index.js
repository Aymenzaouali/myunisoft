import React from 'react';
import StartEndDate from 'components/groups/StartEndDate';
import styles from 'components/groups/StartEndDate/StartEndDate.module.scss';

const StartEndDateCell = props => (
  <StartEndDate className={styles.isCell} {...props} />
);

export default StartEndDateCell;
