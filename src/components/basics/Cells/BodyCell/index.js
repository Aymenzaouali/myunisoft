import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Tooltip } from '@material-ui/core';
import _ from 'lodash';
import TableCell from '@material-ui/core/TableCell';
import InlineLoader from 'components/basics/Loader/Inline';
import moment from 'moment';
import { Field } from 'redux-form';

import StringCell from '../StringCell';
import CheckBoxCell from '../CheckBoxCell';
import SelectCell from '../SelectCell';
import StatusCell from '../StatusCell';
import HistoryCell from '../HistoryCell';
import IconCell from '../IconCell';
import IconButtonCell from '../IconButtonCell';
import EditCell from '../EditCell';

import styles from './BodyCell.module.scss';

// Component
const BodyCell = (props) => {
  const {
    _type,
    children,
    cellProp: {
      titleInfoBulle,
      noFocus,
      className: cellClassName,
      ...cellProp
    },
    props: childProps,
    keyCell,
    component: Component,
    error
  } = props;

  const childClassName = _.get(childProps, 'props.className');
  // Sub-components
  const CustomTooltip = ({ children }) => (
    <Tooltip
      title={error || titleInfoBulle}
      placement="left-end"
      classes={{ tooltip: styles.tooltip }}
    >
      <div>{ children }</div>
    </Tooltip>
  );
  const WrapperComponent = (error || titleInfoBulle) ? CustomTooltip : Fragment;

  // Rendering
  return (
    <TableCell
      {...cellProp}
      className={classnames(
        styles.cell,
        childProps.cellClassName,
        cellClassName,
        {
          [styles.focus]: !noFocus,
          [styles.error]: !!error
        }
      )}
      key={keyCell}
    >
      <WrapperComponent>
        {Component
          ? (
            <Component {...childProps} />
          )
          : (
            <Fragment>
              {_type === 'string' && <StringCell {...childProps} />}
              {_type === 'loadingCell' && <InlineLoader />}
              {_type === 'date'
                && (
                  <StringCell
                    {...childProps}
                    label={
                      childProps.label && moment(childProps.label).format('DD/MM/YYYY')
                    }
                  />
                )
              }
              {_type === 'autocomplete' && <Field {...childProps} props={{ ...childProps.props, className: classnames(styles.autocomplete, styles.fieldCell, childClassName) }} />}
              {_type === 'checkbox' && <CheckBoxCell {...childProps} />}
              {_type === 'select' && <SelectCell {...childProps} />}
              {_type === 'status' && <StatusCell {...childProps} />}
              {_type === 'history' && <HistoryCell {...childProps} />}
              {_type === 'icon' && <IconCell {...childProps} />}
              {_type === 'iconButton' && <IconButtonCell {...childProps} />}
              {_type === 'edit' && <EditCell {...childProps} className={styles.editcell} />}
              {_.isEmpty(_type) && children}
            </Fragment>
          )
        }
      </WrapperComponent>
    </TableCell>
  );
};

// Props
BodyCell.propTypes = {
  _type: PropTypes.oneOf([
    'string',
    'loadingCell',
    'date',
    'autocomplete',
    'checkbox',
    'select',
    'status',
    'history',
    'icon',
    'iconButton',
    'edit',
    ''
  ]), /** cell type */
  keyCell: PropTypes.string,
  children: PropTypes.node, /** cell children */
  component: PropTypes.element,
  error: PropTypes.node,

  cellProp: PropTypes.shape({
    titleInfoBulle: PropTypes.node, /** Tooltip's title */
    className: PropTypes.string,
    noFocus: PropTypes.bool
  }), /** TableCell props (MaterialUi) */

  props: PropTypes.shape({
    label: PropTypes.node, /** for date type */
    cellClassName: PropTypes.string
  }) /** cell props (bind to type) */
};

BodyCell.defaultProps = {
  children: null,
  _type: '',
  cellProp: {},
  props: {},
  keyCell: null,
  error: undefined,
  component: undefined
};

export default BodyCell;
