import React from 'react';
import ButtonIcon from 'components/basics/Icon/Button';

const IconButtonCell = props => (
  <ButtonIcon
    {...props}
  />
);
export default IconButtonCell;
