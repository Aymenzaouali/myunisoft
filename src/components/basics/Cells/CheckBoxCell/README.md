``CheckboxCell``

``Props:``
  * className: class for override css
  * value: value use in action
  * checked: if checkbox checked or not
  * redux: if redux or not
  * className: override  css
  * ...customProps: all props

For more details on the props of Cell look:
  - [Checkbox API](https://material-ui.com/api/checkbox/)

``simple example:``

```js
<CheckboxCell

  />
```
