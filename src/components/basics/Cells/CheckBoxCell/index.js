import React from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';

import CheckBox from 'components/basics/CheckBox';
import ReduxCheckBox from 'components/reduxForm/Selections/ReduxCheckBox';

import styles from './CheckBoxCell.module.scss';

// Component
const colors = {
  yellow: {
    checked: styles.yellow
  },
  colorError: {
    root: styles.colorError
  }
};

const CheckboxCell = (props) => {
  const {
    checked,
    redux,
    className,
    name,
    value,
    onChange,
    disabled,
    customColor,
    ...customProps
  } = props;
  return (
    <div className={styles.cell}>
      { redux && (
        <Field
          name={name}
          labelStyle={className}
          value={value}
          color="primary"
          component={ReduxCheckBox}
          disabled={disabled}
          {...customProps}
        />
      ) }
      { !redux && (
        <CheckBox
          color="primary"
          onChange={onChange}
          checked={checked}
          value={value}
          disabled={disabled}
          classes={colors[customColor]}
          {...customProps}
        />
      ) }
    </div>
  );
};

CheckboxCell.propTypes = {
  /** Action checked */
  checked: PropTypes.bool,
  /** Redux */
  redux: PropTypes.bool,
  /** Css */
  className: PropTypes.string,
  /** Name */
  name: PropTypes.string,
  /** Value */
  value: PropTypes.string,
  /** Disabled */
  disabled: PropTypes.bool,
  /** Custom color */
  customColor: PropTypes.oneOf(['yellow', 'colorError']),
  onChange: PropTypes.func
};

CheckboxCell.defaultProps = {
  checked: false,
  redux: false,
  className: '',
  name: '',
  value: '',
  disabled: false,
  customColor: null,
  onChange: () => {}
};

export default CheckboxCell;
