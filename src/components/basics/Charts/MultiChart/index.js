import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ResponsiveLine } from '@nivo/line';
import { Typography } from '@material-ui/core';

import I18n from 'assets/I18n';

import styles from './MultiChart.module.scss';

const MultiChart = (props) => {
  const {
    upperBound,
    lowerBound,
    currentPeriod,
    previousPeriod,
    data,
    minY,
    maxY
  } = props;

  // Vide !
  if (data.length === 0) {
    return (
      <div className={styles.emptyContainer}>
        <Typography noWrap variant="h4" component="p">{ I18n.t('dashboard.charts.empty') }</Typography>
      </div>
    );
  }


  return (
    <div className={styles.cardContainer}>
      <div className={classNames(styles.bound, styles.top)}>
        <div className={styles.text}>{upperBound}</div>
        <div className={styles.lane} />
      </div>
      <div className={styles.periodContainer}>
        {previousPeriod !== currentPeriod
        && <div className={styles.previous}>{previousPeriod}</div>
        }
        <div className={styles.current}>{currentPeriod}</div>
      </div>
      <div className={styles.chartContainer}>
        {data && data.length > 0 && (
          <ResponsiveLine
            yScale={{
              type: 'linear',
              stacked: false,
              min: minY,
              max: maxY
            }}
            curve="natural"
            data={data}
            colorBy={index => (index.label === 'EX' ? '#0bd1d1' : '#9b9b9b')}
            enableDots={false}
            lineWidth={2}
            stacked={false}
            enableGridX={false}
            enableGridY={false}
            axisLeft={null}
            axisBottom={{
              tickPadding: 18
            }}
            margin={{
              top: 25,
              left: 20,
              bottom: 35,
              right: 20
            }}
            theme={{
              axis: {
                ticks: {
                  line: {
                    stroke: 'white'
                  },
                  text: {
                    fill: '#9b9b9b',
                    fontSize: 12,
                    fontFamily: 'basier_circleregular'
                  }
                }
              }
            }}
          />
        )}
      </div>
      <div className={classNames(styles.bound, styles.bottom)}>
        <div className={styles.text}>{lowerBound}</div>
        <div className={styles.lane} />
      </div>
    </div>
  );
};

// Props
MultiChart.propTypes = {
  upperBound: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]),
  lowerBound: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]),
  currentPeriod: PropTypes.string,
  previousPeriod: PropTypes.string,
  minY: PropTypes.number,
  maxY: PropTypes.number,

  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      data: PropTypes.arrayOf(
        PropTypes.shape({
          x: PropTypes.string,
          y: PropTypes.number,
          z: PropTypes.string
        }).isRequired
      ).isRequired
    }).isRequired
  ).isRequired
};

MultiChart.defaultProps = {
  upperBound: undefined,
  lowerBound: undefined,
  currentPeriod: undefined,
  previousPeriod: undefined,
  minY: undefined,
  maxY: undefined
};

export default MultiChart;
