import React, { useEffect, useState } from 'react';
import { FontIcon } from 'components/basics/Icon';
import PropTypes from 'prop-types';
import ownStyles from './selectLikeFinder.module.scss';


const SelectLikeMacFinder = ({ tree, onChange }) => {
  const [column, setColumn] = useState([]);
  useEffect(() => {
    const initialTree = [];
    tree.forEach((leaf) => {
      initialTree.push({
        ...leaf,
        child: null
      });
    });
    setColumn([initialTree]);
  }, [tree]);


  const addFoldersToView = (leaf, columnIndex) => {
    const updatedTree = [...column.splice(0, columnIndex + 1)];
    /** change selected status for folder or leaf */
    updatedTree[updatedTree.length - 1] = updatedTree[updatedTree.length - 1]
      .map(item => (item.id === leaf.id
        ? { ...leaf, selected: true } : { ...item, selected: false }));
    if (leaf.children) updatedTree.push(leaf.children);
    setColumn(updatedTree);
    const selected = updatedTree
      .map(folder => folder.find(leaf => leaf.selected)).filter(folder => folder !== undefined);
    if (onChange) onChange(selected);
  };

  return (
    <div className={ownStyles.finder}>
      {
        tree && (
          <div className={ownStyles.finderWrap}>
            {
              column && column.map((elem, columnIndex) => (
                <ul className={ownStyles.folderList}>
                  {
                    elem.map((leaf, i) => (
                      <li key={i} className={leaf.selected && ownStyles.selected}>
                        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
                        <div
                          role="button"
                          onClick={e => addFoldersToView(leaf, columnIndex, e)}
                          tabIndex="0"
                        >
                          {
                            leaf.children && <FontIcon name="icon-folder" color="#0bd1d1" size={14} className={ownStyles.folder} />
                          }
                          <span>{leaf.title}</span>
                        </div>
                      </li>
                    ))
                  }
                </ul>
              ))
            }
          </div>
        )
      }
    </div>
  );
};

SelectLikeMacFinder.propTypes = {
  tree: PropTypes.shape({}).isRequired,
  onChange: PropTypes.func
};

SelectLikeMacFinder.defaultProps = {
  onChange: () => {}
};

export default SelectLikeMacFinder;
