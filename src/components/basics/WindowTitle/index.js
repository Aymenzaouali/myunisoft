import { useEffect } from 'react';
import PropTypes from 'prop-types';

// Component
const WindowTitle = ({ title }) => {
  // Effect
  useEffect(() => {
    document.title = title;
  }, [title]);

  return null;
};

// Props
WindowTitle.propTypes = {
  title: PropTypes.string.isRequired
};

export default WindowTitle;
