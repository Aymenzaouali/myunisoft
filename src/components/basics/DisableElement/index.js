import React from 'react';
import PropTypes from 'prop-types';
import styles from './DisableElement.module.scss';

const DisableElement = (props) => {
  const { title } = props;
  return (
    <div className={styles.disableContainer}>
      <span>
        { title }
      </span>
    </div>
  );
};

DisableElement.propTypes = {
  title: PropTypes.string.isRequired
};

export default DisableElement;
