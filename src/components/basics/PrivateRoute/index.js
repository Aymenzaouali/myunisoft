import React from 'react';
import { Route, Redirect } from 'react-router';
import PropTypes from 'prop-types';

const PrivateRoute = ({ component: Component, isAuth, ...rest }) => (
  <Route
    {...rest}
    render={props => (isAuth ? (
      <Component {...props} />
    ) : (
      <Redirect
        to={{
          pathname: '/',
          state: { from: props.location } // eslint-disable-line
        }}
      />
    ))
    }
  />
);

PrivateRoute.propTypes = {
  isAuth: PropTypes.bool.isRequired,
  component: PropTypes.node.isRequired  // eslint-disable-line
};

export default PrivateRoute;
