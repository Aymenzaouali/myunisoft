import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './statusString.module.scss';

const StringStatus = ({ status, label, disabled }) => (
  <span className={classNames(styles.text, styles[status], { [styles.disabled]: disabled })}>
    {label}
  </span>
);

StringStatus.defaultProps = {
  disabled: false
};

StringStatus.propTypes = {
  status: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  disabled: PropTypes.bool
};

export default StringStatus;
