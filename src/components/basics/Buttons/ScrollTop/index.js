import React from 'react';
import PropTypes from 'prop-types';
import { IconButton, withStyles } from '@material-ui/core';
import { FontIcon } from 'components/basics/Icon';

const ScrollTopButton = ({ classes, containerRef }) => {
  const scrollToTop = () => containerRef.current.scrollIntoView({
    behavior: 'smooth', block: 'start'
  });

  return (
    <IconButton
      onClick={() => scrollToTop()}
      classes={{ root: classes.toTopButton }}
    >
      <FontIcon
        name="icon-arrow"
        color="white"
        size={16}
      />
    </IconButton>
  );
};

const ownStyles = () => ({
  toTopButton: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0bd1d1',
    borderRadius: 0,
    transform: 'rotate(90deg)',
    height: 30,
    width: 30,
    padding: 6,
    position: 'fixed',
    right: 30,
    bottom: 10
  }
});

ScrollTopButton.propTypes = {
  containerRef: PropTypes.element.isRequired
};

export default withStyles(ownStyles)(ScrollTopButton);
