import React, { Fragment, useState } from 'react';
import { smallButtonNoPadding } from 'assets/theme/styles';
import { withStyles, Tooltip, Badge } from '@material-ui/core';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { FontIcon } from 'components/basics/Icon';
import Button from 'components/basics/Buttons/Button';
import Loader from 'components/basics/Loader';
import { DropdownBtn } from 'components/basics/Buttons';
import { ButtonProps } from '../types';

const WrapperIcon = (props) => {
  const {
        badgeContent, // eslint-disable-line
        badgeColor, // eslint-disable-line
        children // eslint-disable-line
  } = props;

  if (badgeContent) {
    return (
      <Badge badgeContent={badgeContent} color={badgeColor || 'primary'}>
        {children}
      </Badge>
    );
  }

  return (<>{children}</>);
};

const InlineButton = (props) => {
  const {
    buttons,
    marginDirection,
    classes
  } = props;

  const [isLoading, setIsLoading] = useState({});
  const [disableAllOnLoad, setDisableAllOnLoad] = useState(false);


  const marginClassesMap = {
    left: classes.marginLeft,
    right: classes.marginRight,
    none: ''
  };

  const marginClassname = marginClassesMap[marginDirection];

  const onClick = async (e, button) => {
    if (button.onClick) {
      try {
        setIsLoading({ [button.text]: true });
        setDisableAllOnLoad(true);
        await button.onClick(e);
      } finally {
        setIsLoading({ [button.text]: false });
        setDisableAllOnLoad(false);
      }
    }
  };


  return (
    buttons && buttons.map((button, index) => (button ? (
      <Fragment key={index}>
        {
          button._type === 'icon-link'
            && (
              <Tooltip key={index} title={button.titleInfoBulle} placement="left-end" disableHoverListener={!button.titleInfoBulle} disableFocusListener disableTouchListener classes={{ tooltip: classes.infoBulle }}>
                <Button
                  className={marginClassname}
                  classes={{
                    root: classNames(
                      classes.noPadding,
                      button.className
                    )
                  }}
                  size={button.size || 'small'}
                  tabIndex="-1"
                  variant={button.variant || 'contained'}
                  color={button.color || (button.colorError && 'secondary') || 'primary'}
                  onClick={e => onClick(e, button)}
                  disabled={button.disabled || disableAllOnLoad}
                >
                  <a href={button.href}>
                    <FontIcon
                      name={button.iconName}
                      size={button.iconSize || '40px'}
                      color={button.iconColor || 'white'}
                    />
                  </a>
                </Button>
              </Tooltip>
            )
        }
        {
          button._type === 'icon'
            && (
              <Tooltip key={index} title={button.titleInfoBulle} placement="left-end" disableHoverListener={!button.titleInfoBulle} disableFocusListener disableTouchListener classes={{ tooltip: classes.infoBulle }}>
                <Button
                  className={marginClassname}
                  classes={{
                    root: classNames(
                      classes.noPadding,
                      button.className
                    )
                  }}
                  size={button.size || 'small'}
                  tabIndex="-1"
                  variant={button.variant || 'contained'}
                  color={button.color || (button.colorError && 'secondary') || 'primary'}
                  onClick={e => onClick(e, button)}
                  disabled={button.disabled || disableAllOnLoad}
                >
                  <WrapperIcon {...button}>
                    <FontIcon
                      name={button.iconName}
                      size={button.iconSize || '40px'}
                      color={button.iconColor || 'white'}
                    />
                    {isLoading[button.text] && <Loader size={12} />}
                  </WrapperIcon>
                </Button>
              </Tooltip>
            )
        }
        {
          button._type === 'string'
            && (
              <Badge color={button.badgeColor} badgeContent={button.badgeContent}>
                <Button
                  key={`${button.text}-${index}`}
                  className={
                    classNames(
                      marginClassname,
                      button.className
                    )
                  }
                  size={button.size || 'small'}
                  tabIndex="-1"
                  variant={button.variant || 'contained'}
                  color={button.color || (button.colorError && 'secondary') || 'primary'}
                  onClick={e => onClick(e, button)}
                  disabled={button.disabled || disableAllOnLoad}
                  style={button.style}
                  id={button.id}
                  type={button.type}
                >
                  {button.text}
                  {isLoading[button.text] && <Loader size={12} />}
                </Button>
              </Badge>
            )
        }
        {
          button._type === 'dropdown'
            && (
              <Tooltip key={index} title={button.titleInfoBulle} placement="left-end" disableHoverListener={!button.titleInfoBulle} disableFocusListener disableTouchListener classes={{ tooltip: classes.infoBulle }}>
                <DropdownBtn
                  className={marginClassname}
                  size={button.size || 'small'}
                  tabIndex="-1"
                  variant={button.variant || 'contained'}
                  color={button.color || (button.colorError && 'secondary') || 'primary'}
                  onClick={e => onClick(e, button)}
                  disabled={button.disabled || disableAllOnLoad}
                  options={button.options || []}
                >
                  <FontIcon
                    name={button.iconName}
                    size={button.iconSize || '40px'}
                    color={button.iconColor || 'white'}
                  />
                </DropdownBtn>
              </Tooltip>
            )
        }
        {
          button._type === 'component'
            && (
              <Badge color={button.badgeColor} badgeContent={button.badgeContent}>
                <Button
                  key={`${button.text}-${index}`}
                  className={
                    classNames(
                      marginClassname,
                      button.className
                    )
                  }
                  size={button.size || 'small'}
                  tabIndex="-1"
                  variant={button.variant || 'contained'}
                  color={button.color || (button.colorError && 'secondary') || 'primary'}
                  onClick={e => onClick(e, button)}
                  disabled={button.disabled || disableAllOnLoad}
                  style={button.style}
                  id={button.id}
                >
                  {button.component}
                  {isLoading[button.component] && <Loader size={12} />}
                </Button>
              </Badge>
            )
        }
      </Fragment>
    ) : null))
  );
};

InlineButton.defaultProps = {
  marginDirection: 'left'
};

InlineButton.propTypes = {
  buttons: PropTypes.arrayOf(ButtonProps).isRequired,
  marginDirection: PropTypes.oneOf(['left', 'right', 'none']),
  classes: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({})
  ]))
};

const themeStyles = () => ({
  ...smallButtonNoPadding(),
  marginLeft: {
    marginLeft: '10px'
  },
  marginRight: {
    marginRight: '10px'
  },
  infoBulle: {
    border: 'none',
    height: 15,
    width: 'auto',
    padding: '4px 10px',
    borderRadius: 2,
    fontSize: 10,
    fontWeight: 500,
    boxShadow: '1px 5px 7px 0 rgba(0, 0, 0, 0.08)',
    backgroundColor: '#ffffff',
    color: '#464545',
    fontFamily: 'basier_circlemedium'
  }
});

export default withStyles(themeStyles)(InlineButton);
