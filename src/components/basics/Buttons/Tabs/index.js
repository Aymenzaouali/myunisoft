import React, {
  Fragment,
  useCallback, useEffect, useState, useRef
} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _ from 'lodash';

import { IconButton, Tooltip } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';

import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';

import styles from './tabsButton.module.scss';

// Components
const TabsButton = (props) => {
  const { tabs, onClick, onClose } = props;

  // State
  const [scrollLeft, setScrollLeft] = useState(false);
  const [scrollRight, setScrollRight] = useState(false);

  // Refs
  const buttonsRef = useRef(null);

  // Function
  const scrollCompute = (node) => {
    if (node == null) return;

    if (node.clientWidth < node.scrollWidth) {
      setScrollLeft(node.scrollLeft > 0);
      setScrollRight(node.scrollLeft < node.scrollWidth - node.clientWidth);
    } else {
      setScrollLeft(false);
      setScrollRight(false);
    }
  };

  const scroll = (direction) => {
    if (buttonsRef.current != null) {
      const multiplier = direction === 'left' ? -1 : 1;
      buttonsRef.current.scrollLeft += buttonsRef.current.clientWidth * multiplier;
    }
  };

  // Callbacks
  const buttonsCb = useCallback((node) => {
    buttonsRef.current = node;
    scrollCompute(node);
  }, [tabs]);

  const handleScroll = useCallback(
    _.debounce(() => scrollCompute(buttonsRef.current), 20)
  );

  // Effects
  useEffect(() => {
    scrollCompute(buttonsRef.current);
  });

  useEffect(() => (
    () => handleScroll.cancel()
  ), [handleScroll]);

  useEffect(() => {
    const handleResize = _.debounce(() => scrollCompute(buttonsRef.current), 20);
    window.addEventListener('resize', handleResize);

    return () => {
      handleResize.cancel();
      window.removeEventListener('resize', handleResize);
    };
  }, [scrollCompute]);

  return (
    <div className={styles.container}>
      <div className={classNames(styles.scrollBtn, { [styles.show]: scrollLeft })}>
        <IconButton onClick={() => scroll('left')}>
          <KeyboardArrowLeft />
        </IconButton>
      </div>
      <div ref={buttonsCb} className={styles.buttons} onScroll={handleScroll}>
        {tabs && tabs.length > 0 && tabs.map((tab, index) => {
          const {
            label, disabled = false, className,
            isClosable,
            isSelected,
            id,
            secured
          } = tab;

          const btn = (
            <Button
              onClick={e => onClick(e, tab)}
              disabled={disabled}
              className={
                classNames(styles.button, className, {
                  [styles.selected]: isSelected
                })
              }
            >
              {label}
              {secured && (
                <FontIcon
                  name="icon-lock"
                  size={20}
                />
              )}
              {isClosable && (
                <span
                  role="button"
                  tabIndex="-1"
                  className={classNames('icon-close', styles.icon)}
                  onKeyPress={() => {}}
                  onClick={e => onClose(e, tab)}
                />
              )}
            </Button>
          );

          /** not secured and not a dashboard tab */
          if (id !== -2 && tab.id !== '+') {
            return (
              <Tooltip
                key={index}
                title={`ID: ${id}`}
                placement="bottom"
                disableFocusListener
                disableTouchListener
                classes={{ tooltip: styles.infoBulle }}
              >
                { btn }
              </Tooltip>
            );
          }

          if (tab.info) {
            return (
              <Tooltip
                key={index}
                title={tab.info}
                placement="left-end"
                disableFocusListener
                disableTouchListener
                classes={{ tooltip: styles.infoBulle }}
              >
                { btn }
              </Tooltip>
            );
          }

          return (
            <Fragment key={index}>{ btn }</Fragment>
          );
        })
        }
      </div>
      <div className={classNames(styles.scrollBtn, { [styles.show]: scrollRight })}>
        <IconButton onClick={() => scroll('right')}>
          <KeyboardArrowRight />
        </IconButton>
      </div>
    </div>
  );
};

// Props
TabsButton.propTypes = {
  tabs: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default TabsButton;
