import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/basics/Buttons/Button';
import ArrowIcon from 'components/basics/Icon/Arrow';

class ExpandButton extends PureComponent {
  state = {
    direction: this.direction
  };

  get direction() {
    const { isOpen } = this.props;
    return isOpen ? 'asc' : 'desc';
  }

  handleExpand = () => {
    const { onClick } = this.props;
    this.setState(state => ({ direction: state.direction === 'asc' ? 'desc' : 'asc' }));
    onClick();
  }

  render() {
    const {
      label, disabled
    } = this.props;
    const {
      direction
    } = this.state;

    return (
      <Fragment>
        <Button onClick={this.handleExpand} disabled={disabled}>
          {label}
          <ArrowIcon direction={direction} />
        </Button>
      </Fragment>
    );
  }
}

ExpandButton.propTypes = {
  isOpen: PropTypes.bool,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  onClick: PropTypes.func
};

ExpandButton.defaultProps = {
  isOpen: false,
  disabled: false,
  label: '',
  onClick: () => {}
};

export default ExpandButton;
