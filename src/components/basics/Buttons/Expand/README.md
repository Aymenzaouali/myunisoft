This component is a button which is used to expand/collapse something.

```js
  <ExpandButton defaultValue='asc' label='Expand' />
```