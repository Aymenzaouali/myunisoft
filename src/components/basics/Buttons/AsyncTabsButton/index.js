import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import I18n from 'assets/I18n';

import { TabsButton } from 'components/basics/Buttons';
import TabsButtonLoader from 'components/basics/Loader/TabsButtonLoader';

import styles from './AsyncTabsButton.module.scss';

// Constants
const ERROR_TAB = {
  id: 'err',
  label: I18n.t('errors.loading_tabs'),
  disabled: true,
  isSelected: true,
  className: styles.error
};

// Component
const AsyncTabsButton = (props) => {
  const {
    isLoading, isError,
    load, deps, onAdd,
    tabs, onClick, onClose
  } = props;

  // Effects
  useEffect(() => {
    if (!isLoading) {
      load();
    }
  }, deps);

  // Events
  const handleClick = (event, tab) => {
    if (onAdd && tab.id === '+') {
      onAdd();
    } else {
      onClick(event, tab);
    }
  };

  // Rendering
  if (isLoading) {
    return (
      <TabsButtonLoader />
    );
  }

  if (onAdd != null) {
    tabs.push({
      id: '+',
      label: '+'
    });
  }

  return (
    <TabsButton
      tabs={isError ? [ERROR_TAB] : tabs}
      onClick={isError ? () => {} : handleClick}
      onClose={isError ? () => {} : onClose}
    />
  );
};

// Props
AsyncTabsButton.propTypes = {
  isLoading: PropTypes.bool,
  isError: PropTypes.bool,

  load: PropTypes.func.isRequired,
  deps: PropTypes.array,
  onAdd: PropTypes.func,

  // TabsButton props
  tabs: PropTypes.array.isRequired,

  onClick: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

AsyncTabsButton.defaultProps = {
  isLoading: false,
  isError: false,
  deps: [],
  onAdd: null
};

export default AsyncTabsButton;
