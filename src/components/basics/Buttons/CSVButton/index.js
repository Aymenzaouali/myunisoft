import React from 'react';
import I18n from 'assets/I18n';
import { CSVLink } from 'react-csv';
import PropTypes from 'prop-types';

const CSVButton = ({ csvData, baseFileName }) => ((
  <CSVLink
    data={csvData}
    filename={baseFileName}
  >
    {I18n.t('currentEditions.table.tools.export')}
  </CSVLink>
));

// Props
CSVButton.propTypes = {
  baseFileName: PropTypes.string,
  csvData: PropTypes.array
};

CSVButton.defaultProps = {
  baseFileName: '',
  csvData: []
};

export default CSVButton;
