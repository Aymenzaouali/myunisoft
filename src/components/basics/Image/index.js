import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Loader from 'components/basics/Loader';
import I18n from 'assets/I18n';
import styles from './styles.module.scss';

class BasicImage extends React.PureComponent {
  state = {
    hasError: true
  }

  componentDidMount() {
    const image = new Image();
    const { imageUrl } = this.props;
    image.onerror = () => {
      this.setState({
        hasError: false
      });
    };

    image.src = imageUrl;
  }

  render() {
    const {
      checked,
      imageUrl,
      onClick,
      token
    } = this.props;
    const { hasError } = this.state;

    const imageStyles = {
      backgroundImage: `url(${imageUrl})`,
      backgroundSize: 'cover'
    };

    const render = hasError
      ? (
        <div
          style={imageStyles}
          onClick={onClick}
          role="checkbox"
          tabIndex={-1}
          aria-checked="mixed"
          onKeyDown={onClick}
          className={classnames(styles.container, { [styles.isChecked]: checked })}
        />
      )
      : (
        <div
          style={imageStyles}
          onClick={onClick}
          role="checkbox"
          tabIndex={-1}
          aria-checked="mixed"
          onKeyDown={onClick}
          className={classnames(styles.error, { [styles.isChecked]: checked })}
        >
          {token ? <span className={classnames(styles.errorIcon, 'icon-close')} /> : <Loader size="34" />}
          <div className={styles.errorLabel}>
            {token ? I18n.t('attachments.error') : I18n.t('attachments.loading')}
          </div>
        </div>
      );

    return (
      render
    );
  }
}

BasicImage.propTypes = {
  checked: PropTypes.bool,
  onClick: PropTypes.func,
  token: PropTypes.string,
  imageUrl: PropTypes.string.isRequired
};

BasicImage.defaultProps = {
  checked: false,
  token: '',
  onClick: () => {}
};

export default BasicImage;
