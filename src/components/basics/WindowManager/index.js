import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import _ from 'lodash';

import WindowContext from 'context/WindowContext';

// Component
class WindowManager extends PureComponent {
  // Lifecycle
  componentWillMount() {
    window.addEventListener('message', this.onMessage, false);
    window.addEventListener('beforeunload', this.onBeforeUnload, false);
  }

  componentDidUpdate(prevProps) {
    // close windows
    _.forIn(prevProps.windows, (pws, id) => {
      const { windows: { [id]: ws } } = this.props;
      const { ref: child } = pws;

      if (ws === undefined) {
        child.close();
      }
    });
  }

  componentWillUnmount() {
    window.removeEventListener('message', this.onMessage);
  }

  // Events
  onMessage = (e) => {
    const { data, origin } = e;
    const { windows } = this.props;

    // Refuse messages from another origin
    if (origin !== window.location.origin) return;

    // Parse message if is from known child
    if (data.to === 'opener' && windows[data.from]) {
      switch (data.type) {
      case 'state': {
        const { updateWindow } = this.props;

        console.debug('recv', data);
        updateWindow(data.from, {
          focused: data.payload.focused
        });

        break;
      }

      case 'action': {
        const { dispatch } = this.props;

        console.debug('recv', data);
        dispatch(data.payload);

        break;
      }

      default:
      }
    }
  };

  onBeforeUnload = () => {
    const { windows } = this.props;

    // close all child windows
    _.forIn(windows, ws => ws.ref.close());
  };

  // Méthodes
  openedWindow(id, ref) {
    const { openedWindow, updateWindow } = this.props;

    // store ref
    openedWindow(id, ref);

    // events
    ref.addEventListener('load', () => {
      this.sendInit(id);
      updateWindow(id, { loading: false });
    }, false);

    ref.addEventListener('beforeunload', () => {
      this.closeWindow(id, true);
    }, false);
  }

  send(id, type, payload, force = false) {
    const {
      windows: {
        [id]: ws
      }
    } = this.props;

    if (ws) {
      const { ref: child, loading } = ws;

      // send is useless while the child is loading
      if (!loading || force) {
        const msg = {
          from: 'opener',
          to: id,

          type,
          payload
        };

        console.debug('send', msg);
        child.postMessage(msg, window.location.origin);
      }
    }
  }

  sendInit(id) {
    this.send(id, 'init', { id }, true);
  }

  sendAction(id, action) {
    if (action) {
      this.send(id, 'action', action);
    }
  }

  sendMessage(id, payload) {
    if (payload) {
      this.send(id, 'message', payload);
    }
  }

  closeWindow(id) {
    const {
      windows: { [id]: ws },
      closeWindow
    } = this.props;

    // close it
    if (ws) {
      closeWindow(id);
    }
  }

  // Rendering
  render() {
    const { children, windows } = this.props;

    return (
      <WindowContext.Provider
        value={{
          ...windows,

          openedWindow: (id, ref) => this.openedWindow(id, ref),
          sendAction: (id, action) => this.sendAction(id, action),
          sendMessage: (id, payload) => this.sendMessage(id, payload),
          closeWindow: id => this.closeWindow(id)
        }}
      >
        { children }
      </WindowContext.Provider>
    );
  }
}

// Props
WindowManager.propTypes = {
  children: PropTypes.element.isRequired,

  windows: PropTypes.shape({}).isRequired,

  dispatch: PropTypes.func.isRequired,
  openedWindow: PropTypes.func.isRequired,
  updateWindow: PropTypes.func.isRequired,
  closeWindow: PropTypes.func.isRequired
};

export default WindowManager;
