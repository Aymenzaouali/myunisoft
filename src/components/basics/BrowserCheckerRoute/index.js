import React from 'react';
import styles from 'components/screens/Login/Login.module.scss';
import classNames from 'classnames';
import { ReactComponent as Logo } from 'assets/images/logo.svg';
import ForeignBrowserAlert from 'components/groups/ForeignBrowserAlert';

const BrowserCheckerRoute = () => (
  <div className={styles.container}>
    <div className={classNames(styles.half, styles.logo)}>
      <Logo fill="white" height="100px" />
    </div>
    <ForeignBrowserAlert />
  </div>
);

export default BrowserCheckerRoute;
