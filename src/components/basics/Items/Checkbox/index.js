import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core';
import { Field } from 'redux-form';
import ReduxCheckBox from 'components/reduxForm/Selections/ReduxCheckBox';

class Basic extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { };
  }

  render() {
    const {
      moment,
      leftItem,
      name,
      label,
      labelStyle,
      checked,
      onChange,
      classes
    } = this.props;

    return (
      <div>
        <ListItem key={moment}>
          <ListItemText classes={{ root: classes.listItemContainer }} primary={leftItem} />
          {name
             && (
               <ListItemSecondaryAction>
                 <Field
                   color="primary"
                   name={name}
                   label={label}
                   labelStyle={labelStyle}
                   component={ReduxCheckBox}
                   checked={checked}
                   onChange={onChange}
                 />
               </ListItemSecondaryAction>
             )
          }
        </ListItem>
      </div>
    );
  }
}

Basic.defaultProps = {
  leftItem: '',
  name: '',
  checked: false,
  label: '',
  labelStyle: {},
  classes: {}
};

Basic.propTypes = {
  moment: PropTypes.string.isRequired,
  leftItem: PropTypes.string,
  name: PropTypes.string,
  label: PropTypes.string,
  labelStyle: PropTypes.shape({}),
  checked: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  classes: PropTypes.shape({})
};

const styles = () => ({
  listItemContainer: {
    flex: 1,
    'margin-right': '10px'
  }
});

export default withStyles(styles)(Basic);
