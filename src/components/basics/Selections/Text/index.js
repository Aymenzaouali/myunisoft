import React, { PureComponent } from 'react';
import { Typography, Checkbox } from '@material-ui/core';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

class TextSelection extends PureComponent {
  state = {
    isChecked: this.isChecked
  }

  get isChecked() {
    const { isChecked } = this.props;
    return isChecked;
  }

  onToggleCheckbox = () => {
    const { onToggleCheckbox } = this.props;
    const { isChecked } = this.state;

    this.setState({
      isChecked: !isChecked
    });
    onToggleCheckbox(isChecked);
  }

  render() {
    const {
      text,
      children,
      ...otherProps
    } = this.props;
    const { isChecked } = this.state;

    return (
      <div>
        <div className={styles.text}>
          <Typography variant="h6">
            {text}
          </Typography>
          <Checkbox
            color="primary"
            checked={isChecked}
            onChange={this.onToggleCheckbox}
            {...otherProps}
          />
        </div>
        <div className={styles.childrenContainer}>
          {children}
        </div>
      </div>
    );
  }
}

TextSelection.defaultProps = {
  onToggleCheckbox: () => {},
  text: '',
  isChecked: false
};

TextSelection.propTypes = {
  onToggleCheckbox: PropTypes.func,
  text: PropTypes.string,
  isChecked: PropTypes.bool,
  children: PropTypes.node.isRequired
};
export default TextSelection;
