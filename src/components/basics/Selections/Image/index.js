import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from '@material-ui/core';
import Image from 'components/basics/Image';
import styles from './imageSelection.module.scss';

class ImageSelections extends React.PureComponent {
  state = {
    isChecked: this.isChecked
  }

  get isChecked() {
    const { isChecked } = this.props;
    return isChecked;
  }

  onToggleCheckbox = () => {
    const { onToggleCheckbox } = this.props;
    const { isChecked } = this.state;

    this.setState({
      isChecked: !isChecked
    });
    onToggleCheckbox(isChecked);
  }

  render() {
    const {
      thumbnail,
      name,
      checkboxProps,
      imageProps,
      ...otherProps
    } = this.props;
    const { isChecked } = this.state;
    return (
      <div
        className={styles.container}
      >
        <Image
          checked={isChecked}
          imageUrl={thumbnail}
          onClick={this.onToggleCheckbox}
          {...imageProps}
          {...otherProps}
        />
        <span className={styles.attachmentTitle}>{name}</span>
        <Checkbox
          color="primary"
          checked={isChecked}
          onChange={this.onToggleCheckbox}
          {...checkboxProps}
          {...otherProps}
        />
      </div>
    );
  }
}

ImageSelections.defaultProps = {
  onToggleCheckbox: () => {},
  thumbnail: '',
  isChecked: false,
  name: '',
  checkboxProps: {},
  imageProps: {}
};

ImageSelections.propTypes = {
  onToggleCheckbox: PropTypes.func,
  data: PropTypes.shape({
    document_id: PropTypes.number,
    name: PropTypes.string,
    token: PropTypes.string,
    date: PropTypes.string,
    amount: PropTypes.number,
    status: PropTypes.string
  }).isRequired,
  thumbnail: PropTypes.string,
  isChecked: PropTypes.bool,
  name: PropTypes.string,
  checkboxProps: PropTypes.shape({}),
  imageProps: PropTypes.shape({})
};

export default ImageSelections;
