import React from 'react';
import PropTypes from 'prop-types';
import { Select, MenuItem, withStyles } from '@material-ui/core';

const SelectSelections = (props) => {
  const {
    options,
    value,
    onChange,
    name,
    pagination,
    classes
  } = props;

  return (
    <Select
      value={value}
      onChange={onChange}
      name={name}
      displayEmpty
      classes={{ select: pagination && classes.pagination }}
    >
      {options.map((option, index) => (
        <MenuItem key={index} value={option.value}>{option.label}</MenuItem>
      ))
      }
    </Select>
  );
};

SelectSelections.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
      PropTypes.arrayOf
    ]),
    label: PropTypes.string
  })).isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
    PropTypes.arrayOf
  ]),
  onChange: PropTypes.func,
  name: PropTypes.string,
  pagination: PropTypes.bool,
  classes: PropTypes.shape({
    pagination: PropTypes.string
  }).isRequired
};

SelectSelections.defaultProps = {
  value: '',
  onChange: () => {},
  name: 'Select...',
  pagination: false
};

const themedStyle = () => ({
  pagination: {
    padding: '0px 22px 0px 0px',
    fontSize: '12px'
  }
});

export default withStyles(themedStyle)(SelectSelections);
