import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/core/styles';
import './styles.scss';

const styles = {
  avatar: {
    width: 60,
    height: 60
  }
};

const SimpleAvatar = (props) => {
  const {
    user_name,
    source,
    classes,
    ...others
  } = props;

  return (
    <div>
      <Avatar
        alt={user_name}
        src={source}
        className={classes.avatar}
        {...others}
      />
    </div>
  );
};

SimpleAvatar.defaultProps = {
  classes: {}
};

SimpleAvatar.propTypes = {
  user_name: PropTypes.string.isRequired,
  source: PropTypes.string.isRequired,
  classes: PropTypes.shape({})
};

export default withStyles(styles)(SimpleAvatar);
