import React from 'react';
import PropTypes from 'prop-types';
import SimpleAvatar from 'components/basics/Avatar';
import Icon from 'components/basics/Icon/Svg';
import { generateColorWithName } from 'common/helpers/colors';
import './styles.scss';

const UserAvatar = (props) => {
  const {
    icon_source,
    user_name,
    avatar_source,
    first_name,
    last_name,
    spent_time
  } = props;

  return (
    <div className="avatarContainer">
      <div className="basicContainer basicContainer-upperPart">
        <div className="basicContainer__photo" style={{ borderColor: generateColorWithName(last_name.concat(first_name)) }}>
          <SimpleAvatar user_name={user_name} source={avatar_source} />
        </div>
        <div className="basicContainer__text" style={{ marginBottom: '10px' }}>
          <div className="basicContainer__text-small">{first_name}</div>
          <div className="basicContainer__text-medium">{last_name}</div>
        </div>
        <div className="basicContainer__text-medium" style={{ color: generateColorWithName(last_name.concat(first_name)) }}>{spent_time}</div>
      </div>
      <div className="iconContainer">
        <div className="iconContainer__icon">
          <Icon
            icon_color={generateColorWithName(last_name.concat(first_name))}
            icon_source={icon_source}
          />
        </div>
      </div>
    </div>
  );
};

UserAvatar.defaultProps = {
  avatar_source: '',
  first_name: '',
  last_name: '',
  spent_time: ''
};

UserAvatar.propTypes = {
  icon_source: PropTypes.string.isRequired,
  user_name: PropTypes.string.isRequired,
  avatar_source: PropTypes.string,
  first_name: PropTypes.string,
  last_name: PropTypes.string,
  spent_time: PropTypes.string
};

export default UserAvatar;
