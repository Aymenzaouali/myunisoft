import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography
} from '@material-ui/core';
import styles from './info.module.scss';

const Info = ({ message, color, variant }) => (
  <div className={styles.container}>
    <Typography variant={variant} color={color}>
      {message}
    </Typography>
  </div>
);

Info.propTypes = {
  message: PropTypes.string,
  color: PropTypes.string,
  variant: PropTypes.string
};

Info.defaultProps = {
  message: '',
  color: 'textSecondary',
  variant: 'h6'
};

export default Info;
