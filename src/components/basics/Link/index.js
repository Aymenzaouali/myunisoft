import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import styles from './Link.module.scss';

const Link = ({
  className,
  push,
  path,
  children,
  onClick,
  ...props
}) => {
  const handleClick = (e) => {
    if (onClick) onClick(e);

    push(path);
  };
  return (
    <div
      role="button"
      tabIndex="0"
      onKeyUp={() => {}}
      {...props}
      className={classnames(className, styles.link)}
      onClick={handleClick}
    >
      {children}
    </div>
  );
};

Link.propTypes = {
  className: PropTypes.string,
  push: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
  children: PropTypes.node,
  onClick: PropTypes.func
};

Link.defaultProps = {
  className: undefined,
  children: undefined,
  onClick: undefined
};

const mapDispatchToProps = dispatch => ({
  push: href => dispatch(push(href))
});

export default connect(undefined, mapDispatchToProps)(Link);
