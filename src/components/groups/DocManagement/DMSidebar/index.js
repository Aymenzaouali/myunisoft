import React, { useState } from 'react';
import PropTypes from 'prop-types';
import TreeListItem from 'components/groups/Tree/TreeListItem';
import { Subheader, SubheaderCaption } from 'components/groups/Discussions/Subheader';
import { TreeList } from '../../Tree/TreeList';
import styles from './dmSidebar.module.scss';

const DMSidebar = ({
  tree,
  isTreeLoad,
  activeTabId,
  getDocuments,
  setActiveDocFolder,
  activeFolder,
  isTreeError
}) => {
  const [activeTreeItemData, setActiveTreeItemData] = useState(activeFolder);

  const handleActivate = (treeItemData, event) => {
    if (event.target.className.toString().includes('icon')) return;
    setActiveTreeItemData(treeItemData);
    getDocuments(treeItemData);
    setActiveDocFolder(treeItemData);
  };

  return (
    <div className={styles.tree_container}>
      {
        activeTabId !== 5 && (
          <TreeList isLoading={isTreeLoad} isError={isTreeError}>
            <TreeListItem
              id="general-root"
              childrenOnly
              childrenData={tree}
              onActivate={handleActivate}
              activeId={activeTreeItemData.id}
            />
          </TreeList>
        )
      }
      {activeTabId === 5 && (
        <div>
          {
            tree.map(leaf => (
              <div>
                <Subheader>
                  <SubheaderCaption caption={leaf.title} />
                </Subheader>
                <TreeListItem
                  id="general-root"
                  childrenOnly
                  childrenData={leaf.children}
                  onActivate={handleActivate}
                  activeId={activeTreeItemData.id}
                />
              </div>
            ))
          }
        </div>
      )
      }
    </div>
  );
};

DMSidebar.propTypes = {
  tree: PropTypes.shape([]).isRequired,
  isTreeLoad: PropTypes.bool,
  activeFolder: PropTypes.object,
  activeTabId: PropTypes.number.isRequired,
  isTreeError: PropTypes.bool,
  getDocuments: PropTypes.func.isRequired,
  setActiveDocFolder: PropTypes.func.isRequired
};

DMSidebar.defaultProps = {
  isTreeLoad: undefined,
  activeFolder: undefined,
  isTreeError: undefined
};

export default DMSidebar;
