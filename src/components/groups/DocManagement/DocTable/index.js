import React, { useState } from 'react';
import { Typography, withStyles } from '@material-ui/core';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import DocManTable from 'containers/groups/Tables/DocManTable';
import { FontIcon } from 'components/basics/Icon';
import { CSVLink } from 'react-csv';
import PropTypes from 'prop-types';

const viewTypes = {
  GRID: 'grid',
  LIST: 'list'
};

const DocTable = ({
  classes, csvData, baseFileName, disableExport
}) => {
  const [isList, changeView] = useState(true);
  const handleTableView = (viewType) => {
    if (viewType === viewTypes.GRID) {
      changeView(false);
    } else if (viewType === viewTypes.LIST) {
      changeView(true);
    }
  };

  return (
    <div>
      <div className={classes.tableViewWrap}>
        <Typography>{I18n.t('ged.table.view')}</Typography>
        <Button
          variant={isList ? 'contained' : 'outlined'}
          color="primary"
          size="small"
          onClick={() => handleTableView(viewTypes.LIST)}
          className={isList ? classes.listViewBtn : classes.gridViewBtn}
        >
          <FontIcon
            size={25}
            color={isList ? 'white' : 'black'}
            className="icon-list"
          />
        </Button>

        <Button
          variant={!isList ? 'contained' : 'outlined'}
          color="primary"
          size="small"
          onClick={() => handleTableView(viewTypes.GRID)}
          className={!isList ? classes.list : classes.grid}
        >
          <FontIcon
            size={25}
            color={!isList ? 'white' : 'black'}
            className="icon-miniature"
          />
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="small"
          disabled={disableExport}
          className={classes.exportBtn}
        >
          <CSVLink
            data={csvData}
            filename={baseFileName}
          >
            {I18n.t('currentEditions.table.tools.export')}
          </CSVLink>
        </Button>
      </div>
      <DocManTable isList={isList} />
    </div>
  );
};

DocTable.propTypes = {
  baseFileName: PropTypes.string,
  disableExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired
};

DocTable.defaultProps = {
  baseFileName: 'unnamed'
};

const stylesTableWrap = {
  tableViewWrap: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingRight: 15
  },
  listViewBtn: {
    marginRight: '10px',
    marginLeft: '10px',
    padding: '2px 4px',
    border: '1px solid transparent'
  },
  exportBtn: {
    marginLeft: 10
  },
  gridViewBtn: {
    marginRight: '10px',
    marginLeft: '10px',
    padding: '2px 4px'
  },
  list: {
    padding: '2px 4px',
    border: '1px solid transparent'
  },
  grid: { padding: '2px 4px' }
};

export default withStyles(stylesTableWrap)(DocTable);
