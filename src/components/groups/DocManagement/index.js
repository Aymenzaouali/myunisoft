import DocTable from './DocTable';
import DMSidebar from './DMSidebar';

export {
  DocTable,
  DMSidebar
};
