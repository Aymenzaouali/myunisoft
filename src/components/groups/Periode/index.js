import React, { Fragment, useEffect } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import moment from 'moment';

import I18n from 'assets/I18n';

import { ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';

import { ExerciceSelect } from 'containers/reduxForm/Inputs';

import styles from './Periode.module.scss';

// Constants
export const DEFAULT_START_DATE_NAME = 'dateStart';
export const DEFAULT_END_DATE_NAME = 'dateEnd';
export const DEFAULT_EXERCISE_NAME = 'exercice';

// Utils
const minDate = (d1, d2) => {
  if (d1 == null) return d2;
  if (d2 == null) return d1;

  if (moment(d1).isBefore(d2)) return d1;
  return d2;
};
const maxDate = (d1, d2) => {
  if (d1 == null) return d2;
  if (d2 == null) return d1;

  if (moment(d1).isAfter(d2)) return d1;
  return d2;
};

const bounds = (exercises) => {
  if (!exercises.length) {
    return {
      start: exercises.start_date,
      end: exercises.end_date
    };
  }
  return {
    start: exercises.reduce((acc, e) => minDate(acc, e.start_date), null),
    end: exercises.reduce((acc, e) => maxDate(acc, e.end_date), null)
  };
};

// Component
const Periode = (props) => {
  const {
    displayExercise, startDate, endDate, exercises,
    startDateName, endDateName, exerciseName,
    row, datePickers, leftDatePicker, rightDatePicker,
    autofill,
    onChangeValues,
    menuPosition,
    isMulti
  } = props;


  // Effects
  useEffect(() => { // set dates when changing exercises (if any exercise is set)
    if (!displayExercise) return;
    const { start, end } = bounds(exercises);

    if (start && end) {
      autofill(startDateName, start);
      autofill(endDateName, end);
    }
  }, [displayExercise, exercises]);

  useEffect(() => { // empty exercises when changing dates (if different from exercises bounds)
    if (!displayExercise) return;
    if (exercises.length === 0) return;
    if (!startDate || !endDate) return;

    const { start, end } = bounds(exercises);
    if (moment(start).isSame(startDate) && moment(end).isSame(endDate)) return;

    autofill(exerciseName, []);
  }, [displayExercise, startDate, endDate]);

  // Rendering
  return (
    <Fragment>
      <div className={classNames({ [styles.container]: row }, datePickers)}>
        <Field
          component={ReduxMaterialDatePicker}
          name={startDateName}
          label={I18n.t('consulting.filter.from')}
          className={classNames(styles.picker, leftDatePicker)}

          onChangeCustom={(value) => { onChangeValues({ start: value, end: endDate }); }}
        />
        <Field
          component={ReduxMaterialDatePicker}
          name={endDateName}
          label={I18n.t('consulting.filter.to')}
          className={classNames(styles.picker, rightDatePicker)}

          onChangeCustom={(value) => { onChangeValues({ start: startDate, end: value }); }}
        />
      </div>
      { displayExercise && (
        <Field
          component={ExerciceSelect}
          name={exerciseName}
          autoFill={false}
          isClearable
          menuPosition={menuPosition}
          isMulti={isMulti}
          // empty => field cleared => the dates stay the same
          onChangeValues={e => onChangeValues(bounds(e || exercises))}
        />
      ) }
    </Fragment>
  );
};

// Props
Periode.propTypes = {
  displayExercise: PropTypes.bool,
  startDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(moment)]),
  endDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(moment)]),
  exercises: PropTypes.array,

  // Field names
  startDateName: PropTypes.string,
  endDateName: PropTypes.string,
  exerciseName: PropTypes.string,

  // Style
  row: PropTypes.bool,
  datePickers: PropTypes.string,
  leftDatePicker: PropTypes.string,
  rightDatePicker: PropTypes.string,
  isMulti: PropTypes.bool,

  // Actions
  autofill: PropTypes.func.isRequired,

  menuPosition: PropTypes.string,

  // Events
  onChangeValues: PropTypes.func
};

Periode.defaultProps = {
  displayExercise: false,
  startDate: null,
  endDate: null,
  exercises: [],
  isMulti: true,

  startDateName: DEFAULT_START_DATE_NAME,
  endDateName: DEFAULT_END_DATE_NAME,
  exerciseName: DEFAULT_EXERCISE_NAME,

  row: false,
  datePickers: undefined,
  leftDatePicker: undefined,
  rightDatePicker: undefined,

  onChangeValues: () => {},
  menuPosition: 'absolute'
};

export default Periode;
