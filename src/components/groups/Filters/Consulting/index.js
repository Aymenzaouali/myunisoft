import React, { Fragment, useEffect } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';

import { IconButton, withStyles } from '@material-ui/core';

import I18n from 'assets/I18n';
import { splitShape } from 'context/SplitContext';

import { AccountAutoComplete, Periode } from 'containers/reduxForm/Inputs';

import { AutoComplete } from 'components/reduxForm/Inputs';
import { CSVLink } from 'react-csv';
import Button from 'components/basics/Buttons/Button';

import { FontIcon } from 'components/basics/Icon';

import styles from './ConsultingFilter.module.scss';

// Component
const ConsultingFilter = (props) => {
  const {
    classes,
    societyId,
    getEntries,
    split,
    updateArgsSplit,
    formValues,
    baseFileName,
    csvData,
    exercicesData,
    notLetter,
    setODDialog
  } = props;

  const { dateStart, dateEnd } = formValues;

  // Load entries when default Data are ready
  useEffect(() => {
    const { dateStart, dateEnd } = formValues;
    const { account_id } = formValues.Account || {};

    if (account_id && dateStart && dateEnd) {
      getEntries(societyId);
    }
  }, [
    split.slave,
    split.args.account,
    formValues.dateStart,
    formValues.dateEnd,
    formValues.Account,
    formValues.selectLettrage
  ]);

  const dataSelect = [{
    label: I18n.t('consulting.filter.lettrage'),
    value: null
  }, {
    label: I18n.t('consulting.filter.lettrageOn'),
    value: true
  }, {
    label: I18n.t('consulting.filter.lettrageOff'),
    value: false
  }];

  return (
    <div className={styles.consultingFilter}>
      { (!split.slave || split.args.isEntriesSelect) && (
        <Fragment>
          <Field
            component={AccountAutoComplete}
            className={styles.account}
            name="Account"
            onChangeValues={(value) => {
              updateArgsSplit({ account: value });
            }}
            label={I18n.t('selectAccount')}
            autoFocus
          />

          <IconButton onClick={() => getEntries(societyId, 'previous')}>
            <FontIcon name="icon-nav-left" />
          </IconButton>

          <IconButton onClick={() => getEntries(societyId, 'next')}>
            <FontIcon name="icon-nav" />
          </IconButton>
        </Fragment>
      ) }

      <Field
        component={Periode}
        name="Date"
        separator
        row
        displayExercice
        formName={`${societyId}consultingFilter`}
        classes={{
          dateField: styles.date
        }}
        exercicesData={exercicesData}
        dateStart={dateStart}
        dateEnd={dateEnd}
      />
      <Field
        component={AutoComplete}
        options={dataSelect}
        name="selectLettrage"
        label={I18n.t('lettrage')}
        classes={{
          dateField: styles.date
        }}
      />
      <Button
        className={styles.export}
        variant="contained"
        color="primary"
      >
        <CSVLink
          data={csvData}
          filename={baseFileName}
        >
          {I18n.t('currentEditions.table.tools.export')}
        </CSVLink>
      </Button>
      <Button
        className={styles.export}
        classes={{ root: classes.button }}
        variant="contained"
        color="primary"
        disabled={notLetter === 0}
        onClick={() => setODDialog()}
      >
        {I18n.t('currentEditions.table.tools.generate')}
      </Button>
    </div>
  );
};

// Props
ConsultingFilter.propTypes = {
  societyId: PropTypes.number.isRequired,
  csvData: PropTypes.array.isRequired,
  form: PropTypes.shape({}).isRequired,
  getEntries: PropTypes.func.isRequired,
  baseFileName: PropTypes.string.isRequired,
  classes: PropTypes.shape({}).isRequired,
  split: splitShape.isRequired,
  updateArgsSplit: PropTypes.func.isRequired,
  formValues: PropTypes.shape({}).isRequired,
  exercicesData: PropTypes.array.isRequired,
  notLetter: PropTypes.number.isRequired,
  setODDialog: PropTypes.func.isRequired
};

const themeStyles = () => ({
  button: {
    marginLeft: '20px'
  }
});

export default withStyles(themeStyles)(ConsultingFilter);
