import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import { Field } from 'redux-form';
import { AccountClassAutoComplete } from 'containers/reduxForm/Inputs';
import NewAccount from 'containers/groups/Dialogs/Account/NewAccount';
import _ from 'lodash';
import I18n from 'assets/I18n';
import { DeleteConfirmation } from 'containers/groups/Dialogs';
import styles from './chartAccountFilter.module.scss';

class ChartAccountsFilter extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isModification: false,
      deleteConfirmationDialog: false
    };
  }

  handleOpen = (isModification) => {
    this.setState(() => ({
      isOpen: true,
      isModification
    }));
  };

  handleClose = () => {
    this.setState(() => ({
      isOpen: false
    }));
  };

  initializeModification = async () => {
    const {
      initializeAccount,
      accountData
    } = this.props;

    const {
      account_number,
      counterpart_account,
      intraco,
      label,
      presta,
      vat_param,
      account_id
    } = accountData[0];


    await initializeAccount({
      infoG: {
        account_number,
        counterpart_account,
        intraco_account: intraco,
        label,
        provider: presta,
        tva: vat_param,
        account_id
      }
    });
  };

  render() {
    const {
      getChartAccount,
      accountSelected,
      selectedAccount,
      getAccountDetail,
      resetAccount,
      resetAccountSelected,
      deleteAccountProps,
      disabledExport,
      csvData,
      baseFileName
    } = this.props;

    const {
      isOpen,
      isModification,
      deleteConfirmationDialog
    } = this.state;

    return (
      <Fragment>
        <div className={styles.container}>
          <div className={styles.accountClassFilter}>
            <Field
              name="classFilterChartAccount"
              component={AccountClassAutoComplete}
              textFieldProps={{
                label: I18n.t('ChartAccount.filterLabel'),
                InputLabelProps: {
                  shrink: true
                }
              }}
              onChangeValues={() => getChartAccount({ page: 0 })}
            />
          </div>
          <div className={styles.headerRightButton}>
            <InlineButton
              marginDirection="right"
              buttons={[
                {
                  _type: 'string',
                  text: I18n.t('ChartAccount.button.newAccount'),
                  onClick: () => {
                    resetAccountSelected();
                    this.handleOpen(false);
                    resetAccount();
                  }
                },
                {
                  _type: 'string',
                  text: I18n.t('ChartAccount.button.changeAccount'),
                  onClick: async () => {
                    await getAccountDetail();
                    this.initializeModification();
                    this.handleOpen(true);
                  },
                  disabled: accountSelected === null
                },
                {
                  _type: 'string',
                  text: I18n.t('ChartAccount.button.deleteAccount'),
                  color: 'error',
                  onClick: () => this.setState({ deleteConfirmationDialog: true }),
                  disabled: _.get(selectedAccount, 'length', 0) === 0
                },
                {
                  _type: 'component',
                  color: 'primary',
                  disabled: disabledExport,
                  component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
                }]
              }
            />
          </div>
        </div>
        <DeleteConfirmation
          deleteReportAndForm={deleteAccountProps}
          isOpen={deleteConfirmationDialog}
          onClose={() => this.setState({ deleteConfirmationDialog: false })}
        />
        <NewAccount
          isOpen={isOpen}
          isModification={isModification}
          onClose={this.handleClose}
          onValidate={getChartAccount}
        />
      </Fragment>
    );
  }
}

ChartAccountsFilter.defaultProps = {
  accountSelected: null
};


ChartAccountsFilter.propTypes = {
  deleteAccountProps: PropTypes.func.isRequired,
  getChartAccount: PropTypes.func.isRequired,
  getAccountDetail: PropTypes.func.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired,
  resetAccount: PropTypes.func.isRequired,
  resetAccountSelected: PropTypes.func.isRequired,
  initializeAccount: PropTypes.func.isRequired,
  accountData: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedAccount: PropTypes.arrayOf(PropTypes.number).isRequired,
  accountSelected: PropTypes.number
};

export default ChartAccountsFilter;
