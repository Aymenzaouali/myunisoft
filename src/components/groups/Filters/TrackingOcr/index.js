import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ReduxRadio } from 'components/reduxForm/Selections';
import { withStyles, InputAdornment } from '@material-ui/core';
import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import { SocietyAutoComplete } from 'containers/reduxForm/Inputs';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import { Field } from 'redux-form';
import { FontIcon } from 'components/basics/Icon';
import I18n from 'assets/I18n';

const materialStyles = () => ({
  search: {
    width: 200
  },
  radio: {
    position: 'relative',
    right: 13
  }
});
class TrackingOcrFilter extends PureComponent {
  resetFilter = () => {
    const { reset, getTrackingOcr } = this.props;
    reset();
    getTrackingOcr();
  }

  render() {
    const {
      classes,
      dashboardCollab,
      getTrackingOcr,
      sendByFilterItem,
      disabledExport,
      csvData,
      baseFileName
    } = this.props;

    return (
      <div className="trackingOcr__headerDown">
        <div className="trackingOcr__headerDown-search">
          <Field
            id="searchOcrTracking"
            name="search"
            component={ReduxTextField}
            margin="none"
            onCustomChange={value => (value.length >= 3 || value.length === 0 ? getTrackingOcr() : '')}
            label={I18n.t('navBar.search')}
            style={{ width: 200 }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <FontIcon name="icon-search" />
                </InputAdornment>
              )
            }}
          />
        </div>
        <div className="trackingOcr__headerDown-date">
          <div className="trackingOcr__headerDown-date-from">
            <Field
              placeholder={I18n.t('OCR.trackingTable.filter.from')}
              name="start_date"
              label={I18n.t('OCR.trackingTable.filter.from')}
              minDate="19600101"
              maxDate="21901231"
              component={ReduxMaterialDatePicker}
              onCustomBlur={getTrackingOcr}
              onChangeCustom={getTrackingOcr}
            />
          </div>
          <div className="trackingOcr__headerDown-date-to">
            <Field
              placeholder={I18n.t('OCR.trackingTable.filter.to')}
              name="end_date"
              label={I18n.t('OCR.trackingTable.filter.to')}
              minDate="19600101"
              maxDate="21901231"
              component={ReduxMaterialDatePicker}
              onCustomBlur={getTrackingOcr}
              onChangeCustom={getTrackingOcr}
            />
          </div>
        </div>
        {dashboardCollab
          ? (
            <div className="trackingOcr__headerDown-society">
              <Field
                color="primary"
                name="societyFilter"
                isMulti
                component={SocietyAutoComplete}
                placeholder={I18n.t('OCR.trackingTable.society')}
                onChangeValues={() => getTrackingOcr()}
              />
            </div>
          ) : ''
        }
        <div className="trackingOcr__headerDown-docSent">
          <span className="trackingOcr__headerDown-radioButtons-title">{I18n.t('OCR.trackingTable.filter.documentSend.title')}</span>
          <div className="trackingOcr__headerDown-radioButtons">
            <Field
              name="sendBy"
              color="primary"
              component={ReduxRadio}
              list={sendByFilterItem}
              onCustomChange={getTrackingOcr}
              row
              className={classes.radio}
            />
          </div>
        </div>
        <div className="trackingOcr__headerDown-ButtonContainer">
          <InlineButton
            marginDirection="right"
            buttons={[
              {
                _type: 'component',
                color: 'primary',
                disabled: disabledExport,
                component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
              },
              {
                _type: 'string',
                text: I18n.t('OCR.trackingTable.filter.reload'),
                color: 'primary',
                onClick: getTrackingOcr,
                size: 'medium'
              },
              {
                _type: 'string',
                text: I18n.t('accounting.filter.reset'),
                variant: 'outlined',
                onClick: this.resetFilter,
                size: 'medium'
              }]
            }
          />
        </div>
      </div>
    );
  }
}


TrackingOcrFilter.propTypes = {
  sendByFilterItem: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number,
    label: PropTypes.string
  })),
  classes: PropTypes.shape({}).isRequired,
  dashboardCollab: PropTypes.bool,
  disabledExport: PropTypes.bool,
  csvData: PropTypes.array,
  baseFileName: PropTypes.string,
  getTrackingOcr: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired
};

TrackingOcrFilter.defaultProps = {
  sendByFilterItem: [{
    value: 1,
    label: 'Tous'
  },
  {
    value: 2,
    label: 'Envoyés par moi'
  }],
  dashboardCollab: true,
  disabledExport: true,
  csvData: [],
  baseFileName: ''
};

export default withStyles(materialStyles)(TrackingOcrFilter);
