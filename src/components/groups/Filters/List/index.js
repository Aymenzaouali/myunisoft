import React from 'react';
import { PropTypes } from 'prop-types';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { Field } from 'redux-form';
import { FontIcon } from 'components/basics/Icon';
import { InputAdornment } from '@material-ui/core';

import styles from './CompanyList.module.scss';

const ListFilter = (props) => {
  const {
    filterTag,
    toFilter,
    filterLabel
  } = props;

  return (
    <div className={styles.container}>
      <Field
        id={filterTag}
        name={filterTag}
        component={ReduxTextField}
        margin="none"
        onCustomChange={q => toFilter({ q })}
        label={filterLabel}
        style={{ width: '100%' }}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <FontIcon name="icon-search" />
            </InputAdornment>
          )
        }}
      />
    </div>
  );
};

ListFilter.propTypes = {
  filterTag: PropTypes.string.isRequired,
  toFilter: PropTypes.func.isRequired,
  filterLabel: PropTypes.string.isRequired
};

export default ListFilter;
