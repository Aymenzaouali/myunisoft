import React, { useEffect } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import Typography from '@material-ui/core/Typography';

import DiaryTable from 'containers/groups/Tables/Diary';
import { SocietyAutoComplete, Periode } from 'containers/reduxForm/Inputs';

import I18n from 'assets/I18n';
import _ from 'lodash';
import styles from './fecExport.module.scss';

const FecExportFilter = (props) => {
  const {
    exercices = [],
    setSociety,
    getExercice,
    selectedSociety,
    selectedType,
    isLoading,
    societyId,
    isFromCollab,
    initializeDate,
    start_date,
    end_date,
    exercice_N,
    fecType,
    getDiaries
  } = props;

  useEffect(() => {
    if (fecType === 2) {
      initializeDate(societyId, start_date, moment().format('YYYY-MM-DD'));
    } else {
      initializeDate(societyId, start_date, end_date, exercice_N);
    }
  }, [start_date, end_date, fecType]);

  const onChangeSociety = async (id) => {
    await setSociety(id);
    getExercice(id, societyId);
    if (fecType === 2) getDiaries(id);
  };
  return (
    <div>
      <div className={styles.subContainer}>
        <div className={styles.societyContainer}>
          <Field
            color="primary"
            name="societyInfo"
            component={SocietyAutoComplete}
            menuPosition="fixed"
            placeholder={I18n.t('exports.fec.society')}
            onChangeValues={event => onChangeSociety(event.society_id)}
            isDisabled={isFromCollab}
          />

          {_.isEmpty(exercices) && selectedSociety && !isLoading && <Typography variant="body1" color="secondary">{I18n.t('exports.noExercice')}</Typography>}
        </div>
        {!_.isEmpty(exercices)
        && (
          <Field
            component={Periode}
            displayExercice
            isMulti={false}
            row
            separator
            name="exerciseId"
            formName={`exportForm_${societyId}`}
            exportField
            exerciceField={styles.exerciceField}
          />
        )}
      </div>
      {!_.isEmpty(exercices) && selectedType === 2
        && <DiaryTable forExport selectedSociety={selectedSociety} />
      }
    </div>
  );
};

FecExportFilter.propTypes = {
  exercices: PropTypes.arrayOf(PropTypes.object).isRequired,
  setSociety: PropTypes.func.isRequired,
  getExercice: PropTypes.func.isRequired,
  selectedSociety: PropTypes.object,
  isLoading: PropTypes.bool,
  societyId: PropTypes.number.isRequired,
  initializeDate: PropTypes.func.isRequired,
  start_date: PropTypes.string.isRequired,
  end_date: PropTypes.string.isRequired,
  exercice_N: PropTypes.object,
  selectedType: PropTypes.number,
  fecType: PropTypes.number.isRequired,
  getDiaries: PropTypes.func.isRequired,
  isFromCollab: PropTypes.bool.isRequired
};

FecExportFilter.defaultProps = {
  selectedSociety: undefined,
  isLoading: false,
  exercice_N: {},
  selectedType: 1
};

export default FecExportFilter;
