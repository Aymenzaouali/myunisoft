import React from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import { ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import { Field } from 'redux-form';
import styles from './odPay.module.scss';

const odPayFilter = (props) => {
  const { loadData } = props;

  return (
    <div className={styles.container}>
      <Field
        placeholder={I18n.t('accounting.filter.from')}
        name="od_start_date"
        component={ReduxMaterialDatePicker}
      />
      <Field
        placeholder={I18n.t('accounting.filter.to')}
        name="od_end_date"
        component={ReduxMaterialDatePicker}
      />
      <InlineButton
        buttons={
          [{
            _type: 'string',
            text: I18n.t('accounting.filter.odLoad'),
            onClick: loadData,
            color: 'primary'
          }]
        }
      />
    </div>
  );
};

odPayFilter.propTypes = {
  loadData: PropTypes.func.isRequired
};


export default odPayFilter;
