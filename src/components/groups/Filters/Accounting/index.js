import React, { useEffect, Fragment } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import { CSVLink } from 'react-csv';

import classNames from 'classnames';

import I18n from 'assets/I18n';

import OdPay from 'containers/groups/Filters/Accounting/odPay';
import Periode from 'containers/groups/Periode';
import { DiaryAutoComplete, AccountAutoComplete } from 'containers/reduxForm/Inputs';

import Button from 'components/basics/Buttons/Button';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import ReduxRadio from 'components/reduxForm/Selections/ReduxRadio';
import styles from './styles.module.scss';

// Component
const AccountingFilter = (props) => {
  // Props
  const {
    types,
    filters,
    form,
    societyId,
    purchaseValue,
    onTypeChange,
    refreshData,
    reset,
    getDefaultFilter,
    initialAccount,
    startDate,
    changeForm,
    csvData,
    baseFileName,
    disabledExport,
    clearDataForAccounting
  } = props;

  // Functions
  const resetFilter = () => {
    reset();
  };

  // Effects
  useEffect(() => {
    if (initialAccount && startDate) {
      changeForm(`${societyId}accountingFilter`, 'diary', '');
      changeForm(`${societyId}accountingFilter`, 'account', initialAccount);
      changeForm(`${societyId}accountingFilter`, 'dateStart', startDate);
      changeForm(`${societyId}accountingFilter`, 'dateEnd', startDate);
      changeForm(`${societyId}accountingFilter`, 'debit', purchaseValue);
    } else getDefaultFilter();

    return () => {
      clearDataForAccounting();
    };
  }, [startDate, initialAccount, societyId]);

  useEffect(() => {
    resetFilter();
  }, [filters.type]);

  // Rendering
  const isManual = (filters.type === 'm');
  const isOdPay = (filters.type === 'p');

  return (
    <Fragment>
      <div className={styles.row}>
        <Field
          name="type"
          list={types}
          row
          color="primary"
          component={ReduxRadio}
          onChange={onTypeChange}
        />
        <div className={styles.grow} />
        <InlineButton
          buttons={[
            {
              _type: 'string',
              color: 'primary',
              size: 'medium',
              disabled: !isManual,
              text: I18n.t('buttons.refresh'),
              onClick: () => refreshData()
            }
          ]}
        />
      </div>
      { isOdPay && (
        <Fragment>
          <OdPay />
          <InlineButton
            buttons={[
              {
                _type: 'component',
                color: 'primary',
                disabled: disabledExport,
                component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
              }
            ]}
          />
        </Fragment>
      ) }
      { (!isManual && !isOdPay) && (
        <div className={styles.row}>
          <div className={styles.grow}>
            <div className={classNames(styles.row, styles.fields)}>
              <Periode
                formName={form}
                displayExercise

                row
                leftDatePicker={styles.leftDate}
              />
              <Field
                name="diary"
                placeholder={I18n.t('accounting.filter.diary')}
                fullWidth
                margin="none"
                component={DiaryAutoComplete}
              />
              <Field
                name="account"
                label={I18n.t('accounting.filter.account_number')}
                fullWidth
                margin="none"
                component={AccountAutoComplete}
              />
            </div>
            <div className={classNames(styles.row, styles.fields)}>
              <Field
                name="debit"
                type="number"
                label={I18n.t('accounting.filter.debit')}
                fullWidth
                margin="none"
                component={ReduxTextField}
              />
              <Field
                name="credit"
                label={I18n.t('accounting.filter.credit')}
                type="number"
                fullWidth
                margin="none"
                component={ReduxTextField}
              />
              <Field
                name="plus_less"
                label={I18n.t('accounting.filter.plus_less')}
                type="number"
                fullWidth
                margin="none"
                component={ReduxTextField}
              />
              <Field
                name="piece"
                label={I18n.t('accounting.filter.piece')}
                fullWidth
                margin="none"
                component={ReduxTextField}
              />
              <Field
                name="label"
                label={I18n.t('accounting.filter.label')}
                fullWidth
                margin="none"
                component={ReduxTextField}
              />
              <Field
                name="saisi_par"
                label={I18n.t('accounting.filter.writeBy')}
                fullWidth
                margin="none"
                component={ReduxTextField}
              />
            </div>
          </div>
          <div className={styles.reset}>
            <Button variant="outlined" onClick={resetFilter}>
              {I18n.t('accounting.filter.reset')}
            </Button>
            <Button variant="outlined">
              <CSVLink
                data={csvData}
                filename={baseFileName}
              >
                {I18n.t('currentEditions.table.tools.export')}
              </CSVLink>
            </Button>
          </div>
        </div>
      ) }
    </Fragment>
  );
};

// Props
AccountingFilter.propTypes = {
  form: PropTypes.string.isRequired,
  filters: PropTypes.shape({}),
  csvData: PropTypes.arrayOf(PropTypes.string).isRequired,
  types: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
  })).isRequired,
  societyId: PropTypes.number.isRequired,
  initialAccount: PropTypes.object.isRequired,
  purchaseValue: PropTypes.number.isRequired,
  reset: PropTypes.func.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  startDate: PropTypes.string.isRequired,
  baseFileName: PropTypes.string.isRequired,
  changeForm: PropTypes.func.isRequired,
  refreshData: PropTypes.func.isRequired,
  onTypeChange: PropTypes.func.isRequired,
  getDefaultFilter: PropTypes.func.isRequired,
  clearDataForAccounting: PropTypes.func.isRequired
};

AccountingFilter.defaultProps = {
  filters: {}
};

export default AccountingFilter;
