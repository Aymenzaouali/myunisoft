import React, { PureComponent } from 'react';
import CompanyCreation from 'containers/groups/CompanyCreation';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import Loader from 'components/basics/Loader';

import styles from './CompanyItem.module.scss';

export class ComponentItem extends PureComponent {
  componentDidMount() {
    const { loadData } = this.props;
    loadData();
  }

  render() {
    const { selectedCompany } = this.props;
    const loading = !selectedCompany;
    return (
      <div
        className={styles.companyItemContainer}
      >
        {loading ? (
          <Loader />
        ) : (
          <CompanyCreation
            title={I18n.t('companyCreation.editCompanyTitle', {
              companyName: selectedCompany.name
            })}
          />
        )}
      </div>
    );
  }
}

ComponentItem.propTypes = {
  loadData: PropTypes.func.isRequired,
  selectedCompany: PropTypes.object.isRequired
};

export default ComponentItem;
