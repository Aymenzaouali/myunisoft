import React, { useContext } from 'react';
import NewAccounting from 'containers/groups/NewAccounting';
import SplitContext from 'context/SplitContext';

export default () => {
  const {
    master_id,
    args
  } = useContext(SplitContext);
  return (
    <>
      <NewAccounting showSplitToolbar master_id={master_id} args={args} />
    </>
  );
};
