import React from 'react';
import PropTypes from 'prop-types';
import { version } from 'helpers/versionUtil';
import classNames from 'classnames';
import * as serverConfig from 'common/config/server';
import styles from './Version.module.scss';
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */

const showVersionDetails = serverConfig.mode !== 'prod';

const Version = (props) => {
  const { className } = props;

  const [detailsVisible, setDetailsVisible] = React.useState(false);

  return (
    <div className={classNames(styles.version, className)}>
      <div
        onClick={
          showVersionDetails
            ? () => setDetailsVisible(!detailsVisible)
            : null
        }
      >
        <b>
          &copy;MyUnisoft&nbsp;
          { version.version }
        </b>
      </div>
      { detailsVisible
        && (
          <>
            <div className="groupTitle">Front-End</div>
            <hr />
            <div>
              Build&nbsp;
              {version.commit.build}
            </div>
            {version.api.map(
              (item, i) => (
                <div key={`${i}-fe-${item}`}>
                  { item.name }
                  API V.
                  { item.expectedVersion }
                </div>
              )
            )
            }
            <div>
              {serverConfig.mode}
            </div>
            <div className="groupTitle">Back-End</div>
            <hr />

            {version.api.map(
              (item, i) => (
                <div key={`${i}-be-${item}`}>
                  { item.name }
                  API V.&nbsp;
                  { item.realVersion
                    || <span className={styles.unknown}>?</span> }
                  &nbsp;
                  Build&nbsp;
                  { item.realBuild
                    || <span className={styles.unknown}>?</span>
                  }
                </div>
              )
            )
            }
          </>
        )
      }
    </div>
  );
};

Version.propTypes = {
  className: PropTypes.string
};

Version.defaultProps = {
  className: null
};

export default Version;
