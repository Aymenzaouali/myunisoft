import React, { PureComponent } from 'react';
import {
  Typography,
  withStyles
} from '@material-ui/core';
import {
  ReduxTextField,
  ReduxMaterialDatePicker
} from 'components/reduxForm/Inputs';
import {
  ReduxSelect
} from 'components/reduxForm/Selections';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { Field } from 'redux-form';

class Decloyer extends PureComponent {
  render() {
    const {
      classes
    } = this.props;

    const list = [
      {
        value: '',
        label: 'Local exploité sous un bail'
      }
    ];

    return (
      <div className={classes.container}>
        <Field
          color="primary"
          name="decloyer.year"
          margin="none"
          component={ReduxTextField}
          label={I18n.t('decloyer.declarationYear')}
        />
        <div className={classes.local}>
          <Typography
            variant="subtitle"
          >
            {I18n.t('decloyer.local.title')}
          </Typography>
          <div className={classes.localDetail}>
            <Field
              color="primary"
              style={{ marginRight: '20px', width: '250px' }}
              name="decloyer.refLocal"
              margin="none"
              component={ReduxTextField}
              label={I18n.t('decloyer.local.refLocal')}
            />
            <Field
              color="primary"
              name="decloyer.invariantLocal"
              margin="none"
              component={ReduxTextField}
              label={I18n.t('decloyer.local.invarLocal')}
            />
          </div>
        </div>
        <div className={classes.owner}>
          <Typography
            variant="subtitle"
          >
            {I18n.t('decloyer.owner.title')}
          </Typography>
          <div className={classes.ownerFields}>
            <div className={classes.actualOwner}>
              <Typography
                variant="body1"
              >
                {I18n.t('decloyer.owner.actual')}
              </Typography>
              <Typography
                variant="body2"
                style={{ marginTop: '20px' }}
              >
                821535705 Les petits toits 6540
              </Typography>
            </div>
            <div className={classes.newOwner}>
              <Typography
                variant="body1"
              >
                {I18n.t('decloyer.owner.new.title')}
              </Typography>
              <Field
                color="primary"
                style={{ width: '100%' }}
                name="decloyer.juridicForm"
                margin="none"
                component={ReduxTextField}
                label={I18n.t('decloyer.owner.new.legalForm')}
              />
              <Field
                color="primary"
                style={{ width: '100%' }}
                name="decloyer.name"
                margin="none"
                component={ReduxTextField}
                label={I18n.t('decloyer.owner.new.name')}
              />
              <Field
                color="primary"
                style={{ width: '100%' }}
                name="decloyer.firstName"
                margin="none"
                component={ReduxTextField}
                label={I18n.t('decloyer.owner.new.firsName')}
              />
              <Field
                color="primary"
                style={{ width: '100%' }}
                name="decloyer.siren"
                margin="none"
                component={ReduxTextField}
                label={I18n.t('decloyer.owner.new.siren')}
              />
            </div>
          </div>
        </div>
        <div className={classes.addressDetail}>
          <div>
            <Typography
              variant="body1"
            >
              {I18n.t('decloyer.addressDetail.title')}
            </Typography>
          </div>
          <div className={classes.detail}>
            <Typography
              variant="body2"
              classes={{ root: classes.addressLocal }}
            >
              0003 PL du Général de Gaulle
              BOUTIGNY-SUR_ESSONE 91820
            </Typography>
            <Field
              color="primary"
              className={classes.precisionLocal}
              name="decloyer.precisionLocal"
              margin="none"
              component={ReduxTextField}
              label={I18n.t('decloyer.addressDetail.title')}
            />
          </div>
        </div>
        <div className={classes.permanentUpdate}>
          <div>
            <Typography
              variant="body1"
            >
              {I18n.t('decloyer.permanentUpdate.title')}
            </Typography>
          </div>
          <div>
            <Field
              color="primary"
              className={classes.occupationSelect}
              name="decloyer.occupationPermises"
              margin="none"
              component={ReduxSelect}
              label={I18n.t('decloyer.permanentUpdate.occupationPermises')}
              list={list}
            />
            <Field
              color="primary"
              className={classes.occupationSelect}
              name="decloyer.rent"
              margin="none"
              component={ReduxTextField}
              label={I18n.t('decloyer.permanentUpdate.rentAmount')}
            />
            <Field
              name="decloyer.endLeaseDate"
              label={I18n.t('decloyer.permanentUpdate.endLeaseDate')}
              minDate="19600101"
              maxDate="21901231"
              component={ReduxMaterialDatePicker}
              className={classes.occupationSelect}
              margin="none"
            />
          </div>
        </div>
      </div>
    );
  }
}


Decloyer.propTypes = {
  classes: PropTypes.shape({}).isRequired
};

const styles = () => ({
  container: {
    padding: '20px',
    borderRadius: '0px 2px 2px 2px',
    boxShadow: '2px 2px 4px 0 rgba(195, 195, 195, 0.11)',
    backgroundColor: '#fff',
    marginTop: '16px'
  },
  local: {
    marginTop: '24px'
  },
  localDetail: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '10px'
  },
  owner: {
    marginTop: '30px'
  },
  ownerFields: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '10px'
  },
  actualOwner: {
    display: 'flex',
    flexDirection: 'column'
  },
  newOwner: {
    marginLeft: '90px',
    width: '250px'
  },
  addressDetail: {
    marginTop: '20px'
  },
  addressLocal: {
    width: '215px',
    paddingTop: '10px',
    marginRight: '20px'
  },
  precisionLocal: {
    width: '300px',
    marginLeft: '85px'
  },
  detail: {
    display: 'flex',
    marginTop: '20px',
    flexDirection: 'row'
  },
  permanentUpdate: {
    marginTop: '30px'
  },
  occupationSelect: {
    width: '250px'
  }

});

export default withStyles(styles)(Decloyer);
