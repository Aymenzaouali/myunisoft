import React from 'react';
import PropTypes from 'prop-types';
import {
  Popover, Typography
} from '@material-ui/core';
import I18n from 'assets/I18n';
import { NewPasswordDialog } from 'containers/groups/Dialogs';
import Button from 'components/basics/Buttons/Button';
import styles from './avatar.module.scss';

class PopoverSelector extends React.Component {
  state = {
    isOpenNewPassword: false
  }

  onClosePassword = () => {
    const { onClose } = this.props;
    this.setState({
      isOpenNewPassword: false
    });
    onClose();
  }

  render() {
    const {
      isOpen,
      onClose,
      anchorPosition,
      disconnect
    } = this.props;

    const { isOpenNewPassword } = this.state;

    return (
      <Popover
        id="popoverSociety"
        open={isOpen}
        anchorPosition={anchorPosition}
        anchorReference="anchorPosition"
        onClose={onClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
      >
        <div className={styles.wraperButton}>
          <Button onClick={disconnect}>
            <Typography variant="h6" style={{ marginLeft: '8px' }}>
              {I18n.t('disconnect')}
            </Typography>
          </Button>
          <Button onClick={() => this.setState({ isOpenNewPassword: true })}>
            <Typography variant="h6" style={{ marginLeft: '8px' }}>
              {I18n.t('newPasseword')}
            </Typography>
          </Button>
        </div>
        <NewPasswordDialog
          isOpen={isOpenNewPassword}
          onClose={this.onClosePassword}
        />
      </Popover>
    );
  }
}

PopoverSelector.defaultProps = {
  anchorPosition: null
};

PopoverSelector.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  anchorPosition: PropTypes.object, // eslint-disable-line
  disconnect: PropTypes.func.isRequired
};

export default PopoverSelector;
