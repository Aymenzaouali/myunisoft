import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import { Popover, List, ListItem } from '@material-ui/core';

import SplitContext from 'context/SplitContext';

// Component
const SplitSlavePopover = (props) => {
  const { isOpen, anchor, onClose } = props;
  const { slaves, onActivateSplit } = useContext(SplitContext);

  return (
    <Popover
      open={isOpen}
      anchorEl={anchor}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right'
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right'
      }}
      onClose={onClose}
    >
      <List>
        {slaves.map(slave => (
          <ListItem button key={slave.id} onClick={() => onActivateSplit(slave.id, slave.args)}>
            {slave.label}
          </ListItem>
        ))}
      </List>
    </Popover>
  );
};

// Props
SplitSlavePopover.propTypes = {
  isOpen: PropTypes.bool,
  anchor: PropTypes.shape({}),
  onClose: PropTypes.func.isRequired
};

SplitSlavePopover.defaultProps = {
  isOpen: false,
  anchor: null
};

export default SplitSlavePopover;
