import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Popover, List, ListItem
} from '@material-ui/core';
import { FlagDialog } from 'containers/groups/Dialogs';
import I18n from 'assets/I18n';
import _ from 'lodash';
import styles from './FlagPopover.module.scss';

const FlagPopover = (props) => {
  const {
    societyId,
    linePosition,
    isOpen,
    anchorPosition,
    onClose,
    flags = [],
    disableOnClick
  } = props;

  const [isDialogOpen, setDialogOpen] = useState(false);
  const [flagName, setFlagName] = useState(null);

  const onSelect = async (item) => {
    if (disableOnClick) return;
    const { onValidate, cancelFlag } = props;

    if (item.flag_id !== -1) {
      await onValidate([item]);
      await setFlagName(item.name);
      setDialogOpen(true);
    } else {
      await cancelFlag(societyId, linePosition);
      onClose();
    }
  };

  const onCloseDialog = () => {
    setDialogOpen(false);
  };

  const renderList = () => {
    if (!_.isEmpty(flags)) {
      return (
        <List>
          {flags.map((item, index) => (
            <ListItem
              key={index}
              button
              onClick={() => onSelect(item)}
            >
              <div className={styles.list_item}>{item.name}</div>
            </ListItem>
          ))}
        </List>
      );
    }
    return (
      <div className={styles.no_item}>{I18n.t('Popover.Flag.empty')}</div>
    );
  };

  return (
    <div>
      <Popover
        id="popoverFlag"
        open={isOpen}
        anchorPosition={anchorPosition}
        anchorReference="anchorPosition"
        onClose={onClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
      >
        <div className={styles.list}>{renderList()}</div>
      </Popover>
      <FlagDialog
        isOpen={isDialogOpen}
        onClose={onCloseDialog}
        flagType={flagName}
      />
    </div>
  );
};

FlagPopover.defaultProps = {
  anchorPosition: null,
  linePosition: null,
  flags: [],
  disableOnClick: false
};

FlagPopover.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  anchorPosition: PropTypes.object,
  onValidate: PropTypes.func.isRequired,
  cancelFlag: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired,
  disableOnClick: PropTypes.bool,
  linePosition: PropTypes.number,
  flags: PropTypes.arrayOf(PropTypes.object)
};

export default FlagPopover;
