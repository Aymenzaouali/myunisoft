import React, { useState, useEffect } from 'react';
import { AccessDashboard } from 'containers/groups/Dialogs';
import PropTypes from 'prop-types';
import {
  Popover, TextField, List, ListItem, Typography
} from '@material-ui/core';
import I18n from 'assets/I18n';
import styles from './society.module.scss';

const PopoverSelector = (props) => {
  const {
    isOpen,
    onClose,
    anchorPosition,
    search,
    title,
    researchField,
    societies,
    onValidate
  } = props;

  const [data, setData] = useState([]);
  const [society, setSociety] = useState({});
  const [isOpenAccess, setIsOpenAccess] = useState(false);

  useEffect(() => setData(societies), [societies]);

  const onSelect = async (item) => {
    if (item.secured) {
      setIsOpenAccess(true);
      setSociety(item);
    } else {
      await onValidate([item]);
    }
  };

  const onValidateAccess = () => {
    onValidate([society]);
    setIsOpenAccess(false);
  };

  const renderList = () => {
    if (data.length > 0) {
      return (
        <List>
          {data.map((item, index) => (
            <ListItem
              key={index}
              button
              onClick={() => onSelect(item)}
            >
              <div className={styles.list_item}>
                {!item.secured
                  ? (
                    <>
                      <span className={styles.societyId}>{`(${item.society_id}) `}</span>
                      <span className={styles.societyName}>{item.name}</span>
                    </>
                  ) : item.name }
              </div>
            </ListItem>
          ))}
        </List>
      );
    }
    return (
      <div className={styles.no_item}>{I18n.t('Popover.Society.empty')}</div>
    );
  };
  return (
    <>
      <Popover
        id="popoverSociety"
        open={isOpen}
        anchorPosition={anchorPosition}
        anchorReference="anchorPosition"
        onClose={onClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
      >
        <div className={styles.popoverSocieties}>
          {title
          && (
            <Typography variant="h6" style={{ marginLeft: '8px' }}>
              {title}
            </Typography>
          )
          }
          {
            researchField
            && (
              <div className={styles.search}>
                <TextField
                  className={styles.TextField}
                  autoFocus
                  name="searchSociety"
                  type="text"
                  placeholder={I18n.t('navBar.search')}
                  onChange={event => setData(search(event))}
                  autoComplete="off"
                />
                <span className={styles.icon_search} />
              </div>
            )

          }
          <div className={styles.list}>{renderList()}</div>
        </div>
      </Popover>
      <AccessDashboard
        isOpen={isOpenAccess}
        society={society}
        onValidate={onValidateAccess}
        onClose={() => setIsOpenAccess(false)}
      />
    </>
  );
};

PopoverSelector.defaultProps = {
  anchorPosition: null,
  societySelect: {}
};

PopoverSelector.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  anchorPosition: PropTypes.object,
  search: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  societies: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onValidate: PropTypes.func.isRequired,
  researchField: PropTypes.bool.isRequired,
  societySelect: PropTypes.shape({})
};

export default PopoverSelector;
