import React, { useContext, useState } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import I18n from 'assets/I18n';
import SplitContext from 'context/SplitContext';

import { InlineButton } from 'components/basics/Buttons';
import SplitSlavePopover from 'components/groups/Popover/SplitSlavePopover';

import styles from './SplitToolbar.module.scss';

// Component
const SplitToolbar = (props) => {
  // Props
  const { className, onSplitOpen, onSplitClose } = props;

  // State
  const [popover, setPopover] = useState({ isOpen: false, anchor: null });

  // Context
  const {
    active, master, slave,
    swapped, swappable, popuped, popupable,
    slaves,

    onActivateSplit,
    onPopupSplit,
    focusOther,
    onSwapSplit,
    onCloseSplit
  } = useContext(SplitContext);

  // Functions
  const onSplitClick = (event) => {
    if (slaves.length > 1) {
      setPopover({ isOpen: true, anchor: event.currentTarget });
    } else {
      onActivateSplit(slaves[0].id, slaves[0].args);
    }
    if (onSplitOpen) onSplitOpen();
  };

  const onClose = () => {
    onCloseSplit();
    if (onSplitClose) onSplitClose();
  };

  // Rendering
  const buttons = [];

  if (active && popuped) {
    buttons.push({
      _type: 'icon',
      className: (master && styles.master) || (slave && styles.slave),
      iconName: 'icon-split',
      iconSize: 32,
      iconColor: 'black',
      variant: 'text',
      titleInfoBulle: I18n.t(`split.toolbar.${(master && 'master') || (slave && 'slave')}`),
      onClick: focusOther
    });
  }

  if (!active && slaves.length > 0) {
    buttons.push({
      _type: 'icon',
      iconName: 'icon-split',
      iconSize: 32,
      iconColor: 'black',
      variant: 'text',
      titleInfoBulle: I18n.t('split.toolbar.split'),
      onClick: onSplitClick
    });
  }

  //                    v xor
  if (active && (slave !== swapped) && !popuped && popupable) {
    buttons.push({
      _type: 'icon',
      iconName: 'icon-popup',
      iconSize: '32px',
      variant: 'text',
      iconColor: 'black',
      titleInfoBulle: I18n.t('split.toolbar.popup'),
      onClick: onPopupSplit
    });
  }

  if (active && swappable) {
    buttons.push({
      _type: 'icon',
      iconName: 'icon-swap',
      iconSize: 32,
      variant: 'text',
      iconColor: 'black',
      titleInfoBulle: I18n.t('split.toolbar.swap'),
      onClick: onSwapSplit
    });
  }

  if (active) {
    buttons.push({
      _type: 'icon',
      iconName: 'icon-close',
      iconSize: 32,
      variant: 'text',
      iconColor: 'black',
      titleInfoBulle: I18n.t('split.toolbar.close'),
      onClick: onClose
    });
  }

  return (
    <div className={classNames(className, styles.toolbar)}>
      <InlineButton buttons={buttons} />
      { !active && (
        <SplitSlavePopover
          {...popover}
          onClose={() => setPopover({ isOpen: false, anchor: null })}
        />
      )}
    </div>
  );
};

// Props
SplitToolbar.propTypes = {
  className: PropTypes.string,
  onSplitOpen: PropTypes.func,
  onSplitClose: PropTypes.func
};

SplitToolbar.defaultProps = {
  onSplitOpen: undefined,
  onSplitClose: undefined,
  className: undefined
};

export default SplitToolbar;
