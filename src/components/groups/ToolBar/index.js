import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { InlineButton } from 'components/basics/Buttons';
import styles from './toolbarGroup.module.scss';

const ToolBar = (props) => {
  const {
    text,
    buttons
  } = props;

  return (
    <div className={styles.container}>
      <Typography variant="h6">{text}</Typography>
      <div className={styles.buttonContainer}>
        <InlineButton buttons={buttons} />
      </div>
    </div>
  );
};

ToolBar.propTypes = {
  buttons: PropTypes.arrayOf({
    _type: PropTypes.string.isRequired,
    iconName: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    color: PropTypes.string,
    iconSize: PropTypes.number,
    iconColor: PropTypes.string,
    disabled: PropTypes.bool
  }),
  text: PropTypes.string.isRequired
};

ToolBar.defaultProps = {
  buttons: []
};

export default ToolBar;
