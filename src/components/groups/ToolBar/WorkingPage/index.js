import React, {
  useState, Fragment, useEffect
} from 'react';
import PropTypes from 'prop-types';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import WorkingPageDialog from 'components/groups/Dialogs/WorkingPage';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { Field, Form } from 'redux-form';
import DatePicker from 'components/basics/Inputs/DatePicker';
import styles from './workingPage.module.scss';

const handleOnButtonClick = (type, setDialog) => () => setDialog({
  type,
  isOpen: true
});

const onClose = setDialog => () => setDialog({ type: '', isOpen: false });

const onChangeSelect = (getBills, society_id, accountNumber) => () => {
  getBills(society_id, accountNumber.value);
};

const onEditClick = (isEditing, setEditing, tableName, callback) => {
  setEditing(tableName, !isEditing);

  if (callback) {
    callback();
  }
};

const WorkingPageToolbar = (props) => {
  const {
    tableName,
    selectedBills,
    selectedBillsCount,
    deleteBills,
    setEditing,
    society_id,
    paramWorksheetId,
    isEditing,
    accountNumber,
    getBills,
    formValues,
    onEditStart,
    editOrCreateBill,
    accountFieldLabel,
    errors,
    cancel,
    startDate,
    endDate
  } = props;
  const [dialog, setDialog] = useState(null);
  const hasErrors = Object.values(errors).some(error => Object.values(error).some(m => !!m));

  useEffect(() => {
    if (paramWorksheetId !== undefined) getBills(society_id, paramWorksheetId);
  }, [paramWorksheetId]);

  return (
    <Fragment>
      <div className={styles.headerContainer}>
        {accountNumber
        && (
          <div className={styles.accountNumber}>
            <Form>
              <Field
                name="accountNumberField"
                label={accountFieldLabel}
                component={ReduxSelect}
                className={styles.accountNumberField}
                list={accountNumber}
                onCustomChange={onChangeSelect(getBills, society_id, accountNumber)}
              />
            </Form>
          </div>
        )}

        <div className={styles.dates}>
          <div className={styles.dateGroup}>
            <div>
              {I18n.t('fixedAssets.from')}
            </div>
            <div>
              <DatePicker value={startDate} disabled />
            </div>
          </div>
          <div className={styles.dateGroup}>
            <div>
              {I18n.t('fixedAssets.to')}
            </div>
            <div>
              <DatePicker value={endDate} disabled />
            </div>
          </div>
        </div>
      </div>
      <div className={styles.container}>
        <div className={styles.selectedRows}>
          {selectedBillsCount !== 0 && I18n.t('fnp.selected', { count: selectedBillsCount })}
        </div>
        <div className={styles.buttons}>
          <InlineButton buttons={
            [
              isEditing ? {
                _type: 'string',
                color: 'error',
                text: I18n.t('common.cancel'),
                onClick: () => cancel(tableName)
              } : null,
              {
                _type: 'icon',
                iconName: !isEditing ? 'icon-pencil' : 'icon-save',
                iconSize: 28,
                onClick: !isEditing
                  ? onEditClick.bind(null, isEditing, setEditing, tableName, onEditStart)
                  : editOrCreateBill.bind(null, society_id, formValues),
                disabled: isEditing && hasErrors
              },
              {
                _type: 'icon',
                iconName: 'icon-trash',
                iconSize: 28,
                color: 'error',
                titleInfoBulle: I18n.t('tooltips.delete'),
                disabled: !selectedBillsCount,
                onClick: handleOnButtonClick('remove', setDialog)
              }
            ]}
          />
          <WorkingPageDialog
            onClose={onClose(setDialog)}
            deleteBills={deleteBills}
            selectedBills={selectedBills}
            {...dialog}
            society_id={society_id}
            formValues={formValues}
          />
        </div>
      </div>
    </Fragment>
  );
};

WorkingPageToolbar.propTypes = {
  tableName: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  deleteBills: PropTypes.func.isRequired,
  setEditing: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
  isEditing: PropTypes.bool.isRequired,
  selectedBills: PropTypes.arrayOf(PropTypes.string),
  selectedBillsCount: PropTypes.number,
  society_id: PropTypes.string.isRequired,
  formValues: PropTypes.shape({}),
  getBills: PropTypes.func.isRequired,
  onEditStart: PropTypes.func,
  errors: PropTypes.object,
  editOrCreateBill: PropTypes.func.isRequired,
  accountNumber: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  paramWorksheetId: PropTypes.number.isRequired,
  accountFieldLabel: PropTypes.string.isRequired
};

WorkingPageToolbar.defaultProps = {
  selectedBills: [],
  errors: {},
  selectedBillsCount: 0,
  formValues: undefined,
  startDate: undefined,
  endDate: undefined,
  onEditStart: undefined
};

export default WorkingPageToolbar;
