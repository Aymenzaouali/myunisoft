import React from 'react';
import PropTypes from 'prop-types';
import { FormControlLabel } from '@material-ui/core';
import I18n from 'assets/I18n';
import { formatNumber } from 'helpers/number';
import { InlineButton } from 'components/basics/Buttons';
import CheckBox from 'components/basics/CheckBox';
import _get from 'lodash/get';
import styles from './banklink.module.scss';

const onPointButton = (dotOrUndot, societyId, line, formValues) => () => {
  dotOrUndot(
    societyId, line.id_line_entries, String(formValues.year).slice(-2), String(formValues.month),
    _get(formValues, 'bank.value'),
    `${formValues.year}${formValues.month}${formValues.day}`,
    `${formValues.bank_balance}`,
    `${formValues.month}`,
    `${formValues.year}`
  );
};

const onUnpointButton = (dotOrUndot, societyId, line, formValues) => () => {
  dotOrUndot(societyId, line.id_line_entries, null, null,
    _get(formValues, 'bank.value'),
    `${formValues.year}${formValues.month}${formValues.day}`,
    `${formValues.bank_balance}`,
    `${formValues.month}`,
    `${formValues.year}`);
};

const BankLinkToolBar = (props) => {
  const {
    selectedBills,
    societyId,
    dotOrUndot,
    formValues,
    setFormValues
  } = props;

  const undottedValue = selectedBills.reduce(
    (acc, bill) => (!bill.isDotted ? (bill.credit - bill.debit + acc) : acc), 0
  );
  const dottedValue = selectedBills.reduce(
    (acc, bill) => (bill.isDotted ? (bill.credit - bill.debit + acc) : acc), 0
  );

  const dotted = selectedBills.reduce(
    (acc, line) => (line.isDotted ? line : acc), []
  );

  const undotted = selectedBills.reduce(
    (acc, line) => (!line.isDotted ? line : acc), []
  );

  const disableDotButton = !selectedBills.some(b => b.isDotted === false)
  || selectedBills.length === 0;
  const disableUndotButton = !selectedBills.some(b => b.isDotted === true)
  || selectedBills.length === 0;

  return (
    <div className={styles.container}>
      <div className={styles.toolbar}>
        {selectedBills.length !== 0 && I18n.t('bankLink.selected', { count: selectedBills.length })}
      </div>
      <div className={styles.toolbar}>
        <FormControlLabel
          label={I18n.t('bankLink.totalUnpointed') + formatNumber(undottedValue)}
          control={<CheckBox color="primary" checked />}
        />
      </div>
      <InlineButton
        marginDirection="right"
        buttons={
          [
            {
              _type: 'string',
              variant: 'outlined',
              color: 'none',
              size: 'small',
              disabled: disableDotButton,
              text: I18n.t('bankLink.pointButton'),
              onClick: onPointButton(dotOrUndot, societyId, undotted, formValues, setFormValues)
            }
          ]
        }
      />

      <div className={styles.toolbar}>
        <FormControlLabel
          label={I18n.t('bankLink.totalPointed') + formatNumber(dottedValue)}
          control={<CheckBox className={styles.checkbox} color="secondary" checked />}
        />
      </div>
      <InlineButton
        marginDirection="right"
        buttons={
          [
            {
              _type: 'string',
              variant: 'outlined',
              color: 'none',
              size: 'small',
              disabled: disableUndotButton,
              text: I18n.t('bankLink.unpointButton'),
              onClick: onUnpointButton(dotOrUndot, societyId, dotted, formValues, setFormValues)
            }
          ]
        }
      />
    </div>
  );
};

BankLinkToolBar.propTypes = {
  selectedBills: PropTypes.arrayOf(PropTypes.string),
  formValues: PropTypes.shape({}),
  societyId: PropTypes.number.isRequired,
  dotOrUndot: PropTypes.func.isRequired,
  setFormValues: PropTypes.func.isRequired
};

BankLinkToolBar.defaultProps = {
  selectedBills: [],
  formValues: {}
};

export default BankLinkToolBar;
