import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import styles from './RAFCreateNewStateToolBar.module.scss';

const onEditClick = (isEditing, setEditing, tableName, callback) => {
  setEditing(tableName, !isEditing);

  if (callback) {
    callback();
  }
};

const RAFCreateNewStateToolBar = (props) => {
  const {
    tableName,
    selectedStatesCount,
    createdStatesCount,
    setEditing,
    society_id,
    isEditing,
    getStates,
    formValues,
    onEditStart,
    editOrCreateState,
    errors,
    cancel
  } = props;
  const hasErrors = Object.values(errors).some(error => Object.values(error).some(m => !!m));

  useEffect(() => {
    if (formValues) {
      getStates(society_id, formValues, {});
    }
  }, [formValues]);

  return (
    <Fragment>
      <div className={styles.container}>
        <div className={styles.selectedRows}>
          {selectedStatesCount !== 0 && I18n.t('reportsAndForms.selected', { count: selectedStatesCount })}
        </div>
        <div>
          <InlineButton buttons={
            [
              isEditing ? {
                _type: 'string',
                color: 'error',
                text: I18n.t('common.cancel'),
                onClick: () => cancel(tableName)
              } : null,
              {
                _type: 'icon',
                iconName: !isEditing ? 'icon-pencil' : 'icon-save',
                iconSize: 28,
                onClick: !isEditing
                  ? onEditClick.bind(null, isEditing, setEditing, tableName, onEditStart)
                  : editOrCreateState.bind(null, society_id, formValues),
                disabled: isEditing && hasErrors
              },
              {
                _type: 'icon',
                iconName: 'icon-trash',
                iconSize: 28,
                color: 'error',
                titleInfoBulle: I18n.t('tooltips.delete'),
                disabled: !selectedStatesCount || (selectedStatesCount === createdStatesCount)
              }
            ]}
          />
        </div>
      </div>
    </Fragment>
  );
};

RAFCreateNewStateToolBar.propTypes = {
  tableName: PropTypes.string.isRequired,
  setEditing: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
  isEditing: PropTypes.bool.isRequired,
  selectedStatesCount: PropTypes.number,
  createdStatesCount: PropTypes.number,
  society_id: PropTypes.string.isRequired,
  formValues: PropTypes.shape({}),
  getStates: PropTypes.func.isRequired,
  onEditStart: PropTypes.func,
  errors: PropTypes.object,
  editOrCreateState: PropTypes.func.isRequired
};

RAFCreateNewStateToolBar.defaultProps = {
  errors: {},
  selectedStatesCount: 0,
  createdStatesCount: 0,
  formValues: undefined,
  onEditStart: undefined
};

export default RAFCreateNewStateToolBar;
