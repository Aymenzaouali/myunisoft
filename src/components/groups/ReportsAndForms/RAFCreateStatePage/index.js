import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import RAFCreateNewStateToolBar from 'components/groups/ToolBar/RAFCreateNewStateToolBar';
import RAFCreateStateTable from 'containers/groups/Tables/RAFCreateStateTable';
import styles from './RAFCreateStatePage.module.scss';

const onEditStart = (inputRef) => {
  // waiting for the buttonRef to be filled (because not in the DOM when clicking)
  setTimeout(() => {
    if (inputRef && inputRef.current && inputRef.current.focus) {
      inputRef.current.focus();
    }
  }, 0);
};

const RAFCreateStatePage = (props) => {
  const {
    isEditing,
    className
  } = props;
  const inputRef = useRef(null);
  const handleEditStart = () => onEditStart(inputRef);

  return (
    <div className={classnames(styles.container, className, { 'is-editing': isEditing })}>
      <RAFCreateNewStateToolBar
        {...props}
        onEditStart={handleEditStart}
      />
      <div className={styles.table}>
        <RAFCreateStateTable {...props} inputRef={inputRef} onEditStart={handleEditStart} />
      </div>
    </div>
  );
};

RAFCreateStatePage.propTypes = {
  className: PropTypes.string,
  isEditing: PropTypes.bool
};

RAFCreateStatePage.defaultProps = {
  className: undefined,
  isEditing: false
};

export default RAFCreateStatePage;
