import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import Button from 'components/basics/Buttons/Button';
import Typography from '@material-ui/core/Typography';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import I18n from 'assets/I18n';
import './styles.scss';

const Confirmation = (props) => {
  const {
    primaryRedirection,
    secondaryRedirection,
    open,
    user_id
  } = props;

  return (
    <Dialog open={open}>
      <div className="confirmation">
        <Typography variant="title" align="center" className="confirmation__title">
          {
            user_id
              ? I18n.t('newUserForm.confirmation.alternative_title')
              : I18n.t('newUserForm.confirmation.title')
          }
        </Typography>
        <InboxIcon className="confirmation__image" />
        <div className="confirmation__buttons">
          <Button size="large" color="error" variant="contained" onClick={secondaryRedirection}>{I18n.t('newUserForm.confirmation.cancel')}</Button>
          <div className="confirmation__buttons-divider" />
          <Button size="large" color="primary" variant="contained" onClick={primaryRedirection}>{I18n.t('newUserForm.confirmation.validate')}</Button>
        </div>
      </div>
    </Dialog>
  );
};

Confirmation.propTypes = ({
  primaryRedirection: PropTypes.func.isRequired,
  secondaryRedirection: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  user_id: PropTypes.number.isRequired
});

export default Confirmation;
