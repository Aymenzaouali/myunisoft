import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  withStyles
} from '@material-ui/core';
import { Field } from 'redux-form';
import {
  UserProfilAutoComplete,
  UserTypeAutoComplete,
  UserFunctionAutoComplete
} from 'containers/reduxForm/Inputs';
import I18n from 'assets/I18n';


const NewUserFunction = (props) => {
  const {
    classes,
    resetGeneralProfil
  } = props;

  // State
  const [idType, setidType] = useState('');

  return (
    <div className={classes.fields}>
      <Field
        component={UserTypeAutoComplete}
        name="newUserType"
        className={classes.typeField}
        placeholder=""
        textFieldProps={{
          label: I18n.t('newUserForm.groups.type'),
          InputLabelProps: {
            shrink: true
          }
        }}
        onChangeValues={(value) => {
          setidType(value.id);
          resetGeneralProfil();
        }}
      />
      <Field
        component={UserProfilAutoComplete}
        name="newUserProfil"
        className={classes.profilField}
        placeholder=""
        textFieldProps={{
          label: I18n.t('newUserForm.groups.profil'),
          InputLabelProps: {
            shrink: true
          }
        }}
        id_type={idType}
      />
      <Field
        component={UserFunctionAutoComplete}
        name="newUserFunction"
        className={classes.functionField}
        placeholder=""
        textFieldProps={{
          label: I18n.t('newUserForm.groups.groups'),
          InputLabelProps: {
            shrink: true
          }
        }}
      />
    </div>
  );
};

const styles = () => ({
  fields: {
    display: 'flex',
    flex: 1,
    'flex-direction': 'row',
    padding: 20
  },
  typeField: {
    'margin-right': 5
  },
  profilField: {
    'margin-right': 5,
    width: 256
  },
  functionField: {
    'margin-right': 5,
    width: 350
  },
  buttons: {
    display: 'flex',
    'flex-direction': 'row',
    'justify-content': 'flex-end',
    'margin-top': 30
  }
});

NewUserFunction.propTypes = ({
  classes: PropTypes.shape({}).isRequired,
  resetGeneralProfil: PropTypes.func.isRequired
});

export default withStyles(styles)(NewUserFunction);
