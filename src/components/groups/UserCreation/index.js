import React, { useState } from 'react';
import PropTypes from 'prop-types';
import StepperHeader from 'containers/groups/Headers/NewUserStepper';
import NewUserForm from 'containers/groups/Forms/NewUser';
import RightManagement from 'containers/groups/UserCreation/RightManagement';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import _ from 'lodash';
import styles from './newUserDialogs.module.scss';

const newUserStepComponentMap = {
  0: (isOpenDialog, list, onCheck, mailExist, closeUserDialog) => (
    <NewUserForm
      isOpen={isOpenDialog}
      list={list}
      onCheck={onCheck}
      mailExist={mailExist}
      closeUserDialog={closeUserDialog}
    />
  ),
  1: () => (<RightManagement />)
};

const User = (props) => {
  const [list, setList] = useState([]);
  const [isOpenDialog, setIsOpenDialog] = useState(false);
  const [mailExist, setMailExist] = useState(null);
  const {
    activeTab,
    onSelectTab,
    handleSubmit,
    isModificationMode,
    selectedUser,
    onCancel,
    cancelForm,
    onCheckMail,
    onSubmitUser
  } = props;


  const onSubmit = async (values) => {
    try {
      if (!isModificationMode) {
        const listOfExistingPhysicalPerson = await onSubmitUser(values);
        if (!_.isEmpty(listOfExistingPhysicalPerson) && _.isEmpty(selectedUser)) {
          await setIsOpenDialog(true);
          setList(listOfExistingPhysicalPerson);
        } else {
          onSelectTab(1);
        }
      } else {
        onSelectTab(1);
      }
    } catch (error) {
      console.log('Error Submit: ', error); //eslint-disable-line
    }
  };

  const onCheck = async () => {
    try {
      const { user_id } = await onCheckMail();
      await setMailExist(user_id || null);
    } catch (error) {
      console.log('Error Check : ', error); //eslint-disable-line
    }
  };

  const closeUserDialog = () => {
    setList([]);
    setIsOpenDialog(false);
  };


  const buttonsNewUserForm = [
    {
      _type: 'string',
      color: 'default',
      size: 'large',
      variant: 'contained',
      onClick: onCancel,
      text: I18n.t('newUserForm.userCreation.cancel')
    },
    {
      _type: 'string',
      size: 'large',
      variant: 'contained',
      onClick: handleSubmit(onSubmit),
      disabled: mailExist && selectedUser,
      text: I18n.t('newUserForm.userCreation.validate')
    }
  ];

  const onChangeTab = () => {
    if (activeTab === 1) {
      onSelectTab(0);
    } else if (activeTab === 0) {
      handleSubmit(onSubmit)();
    }
  };

  return (
    <div className={styles.container}>
      <StepperHeader
        title={isModificationMode ? I18n.t('newUserForm.header.alternative_title') : I18n.t('newUserForm.header.title')}
        openTab={() => onChangeTab()}
        onClose={cancelForm}
      />
      { newUserStepComponentMap[activeTab](
        isOpenDialog,
        list,
        onCheck,
        mailExist,
        closeUserDialog
      )}

      <InlineButton
        marginDirection="left"
        buttons={activeTab === 0 && buttonsNewUserForm}
      />
    </div>
  );
};

User.defaultProps = ({
  selectedUser: {}
});

User.propTypes = ({
  cancelForm: PropTypes.func.isRequired,
  activeTab: PropTypes.number.isRequired,
  onSelectTab: PropTypes.func.isRequired,
  isModificationMode: PropTypes.number.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  selectedUser: PropTypes.shape({
    user_id: PropTypes.number,
    civility_id: PropTypes.number,
    civility: PropTypes.string,
    name: PropTypes.string,
    maiden_name: PropTypes.string,
    firstname: PropTypes.string,
    mail: PropTypes.shape({}),
    tel_fix: PropTypes.shape({
      id: PropTypes.number,
      coordonnee: PropTypes.string
    }),
    tel_portable: PropTypes.shape({
      id: PropTypes.number,
      coordonnee: PropTypes.string
    }),
    access_list: PropTypes.arrayOf(PropTypes.shape({}))
  }),
  onCancel: PropTypes.func.isRequired,
  onSubmitUser: PropTypes.func.isRequired,
  onCheckMail: PropTypes.func.isRequired

});

export default User;
