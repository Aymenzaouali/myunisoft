import React from 'react';
import {
  Form
} from 'redux-form';
import PropTypes from 'prop-types';
import Button from 'components/basics/Buttons/Button';
import SocietiesGranting from 'containers/groups/Lists/Societies';
import NewUserConfirmation from 'containers/groups/UserCreation/Confirmation';
import NewUserFunction from 'containers/groups/UserCreation/Function';
import {
  Typography,
  withStyles
} from '@material-ui/core';
import I18n from 'assets/I18n';

const RightManagement = (props) => {
  const {
    classes,
    handleSubmit,
    onSubmitUser,
    error,
    isModificationMode,
    resetGranted,
    onCancel,
    isOpen
  } = props;

  return (
    <div>
      {error && <Typography variant="h6" color="error">{error}</Typography>}
      <Form onSubmit={handleSubmit(onSubmitUser)}>
        <NewUserFunction />
        <SocietiesGranting />
        <div className={classes.buttons}>
          <Button size="large" variant="outlined" color="primary" className={classes.cancelButton} onClick={resetGranted}>
            {I18n.t('newUserForm.rightManagement.reset')}
          </Button>
          <Button size="large" variant="contained" color="grey" className={classes.cancelButton} onClick={onCancel}>
            {I18n.t('newUserForm.rightManagement.cancel')}
          </Button>
          <Button size="large" variant="contained" color="primary" type="submit">
            {isModificationMode ? I18n.t('newUserForm.rightManagement.alternative_validate') : I18n.t('newUserForm.rightManagement.validate')}
          </Button>
        </div>
      </Form>
      <NewUserConfirmation
        open={isOpen}
      />
    </div>
  );
};

const styles = () => ({
  buttons: {
    margin: '75px 0',
    display: 'flex',
    'flex-direction': 'row',
    'justify-content': 'flex-end',
    marginRight: '20%'
  },
  cancelButton: {
    'margin-right': 20
  }
});

RightManagement.propTypes = ({
  resetGranted: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onSubmitUser: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  isModificationMode: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired
});

export default withStyles(styles)(RightManagement);
