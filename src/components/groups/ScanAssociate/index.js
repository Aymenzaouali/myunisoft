import React from 'react';
import * as I18n from 'i18next';
import { Typography } from '@material-ui/core';
import { Field, Form } from 'redux-form';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import { InlineButton } from 'components/basics/Buttons';
import { AutoComplete } from 'components/reduxForm/Inputs';
import PropTypes from 'prop-types';
import styles from './scanAssociate.module.scss';

export const ScanAssociate = ({ title, sendPinCode, resetCode }) => (
  <div>
    <Typography variant="h1">{title}</Typography>

    <Form>
      <div className={styles.row}>
        <Field
          component={AutoComplete}
          label={I18n.t('scanAssociate.form.typeOfScan')}
          name="typeOfScan"
          options={[
            {
              value: 'dematbox',
              label: 'dematbox'
            }
          ]}
        />

        <Field
          color="primary"
          name="associationCode"
          component={ReduxTextField}
          label={I18n.t('scanAssociate.form.associationCode')}
          className={styles.search}
          errorStyle={styles.error}
          margin="none"
        />

        <div>
          <InlineButton buttons={[
            {
              _type: 'string',
              size: 'medium',
              text: I18n.t('scanAssociate.form.cancel'),
              onClick: () => resetCode()
            },
            {
              _type: 'string',
              size: 'medium',
              text: I18n.t('scanAssociate.form.confirm'),
              color: 'default',
              onClick: () => sendPinCode()
            }]}
          />
        </div>

      </div>
    </Form>
  </div>
);

ScanAssociate.propTypes = {
  title: PropTypes.string.isRequired,
  sendPinCode: PropTypes.func.isRequired,
  resetCode: PropTypes.func.isRequired
};
