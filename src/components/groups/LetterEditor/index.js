import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Typography } from '@material-ui/core';
import { Field } from 'redux-form';
import { ReduxGrowInput, AutoCompleteCell } from 'components/reduxForm/Inputs';
import PhysicalPersonAutoComplete from 'containers/reduxForm/Inputs/PhysicalPersonAutoComplete/physicalPersonAutoComplete';
import MoralPersonAutoComplete from 'containers/reduxForm/Inputs/MoralPersonAutoComplete/moralPersonAutoComplete';
import { splitIntoPages, moveCaretToChar } from 'helpers/layout';
import { getCaretPos } from 'helpers/dom';
import get from 'lodash/get';
import I18n from 'assets/I18n';
import isEmpty from 'lodash/isEmpty';
import styles from './letterEditor.module.scss';

class LetterEditor extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      coordinates: {
        user: [],
        society: []
      },
      pages: []
    };
  }

  setCoordinates = (coordinates) => {
    this.setState(state => ({ ...state, coordinates }));
  }

  setPages = (pages) => {
    this.setState(state => ({ ...state, pages }));
  }

  generateNewPage = (i) => {
    const { change } = this.props; // eslint-disable-line
    const { pages } = this.state;
    this.setPages([
      ...pages,
      <div className={styles.page} key={`letterEditor${i}`}>
        <div className={styles.editor}>
          <Field
            customRef={(el) => { this[`editor[${i}]`] = el; }}
            name={`body${i}`}
            component={ReduxGrowInput}
            onCustomChange={(e) => {
              if (isEmpty(e.target.value)) {
                this.setState(state => ({ ...state, pages: state.pages.splice(i, 1) }));
              } else {
                const page = splitIntoPages(e.target.value, 786);
                if (page.length > 1) {
                  change(`body${i}`, page[0]);
                  change(`body${i + 1}`, page[1]);
                  if (!pages[i + 1]) {
                    this.generateNewPage(1);
                  }
                  this.generateNewPage(i + 1);
                  setTimeout(() => {
                    this[`editor[${i + 1}]`].focus();
                    moveCaretToChar(this[`editor[${i + 1}]`], page[1].length);
                  }, 100);
                  return false;
                }
                return true;
              }
              return true;
            }}
            placeholder=""
            style={{
              maxWidth: '472px', fontSize: '12px', minHeight: '24px', outlineWidth: 0
            }}
          />
        </div>
        <div className={styles.footer}>
          <Typography variant="body1">{`${I18n.t('common.page')} ${i + 1}`}</Typography>
        </div>
      </div>
    ]);
  };

  render() {
    const {
      change // eslint-disable-line
    } = this.props;

    const {
      coordinates,
      pages
    } = this.state;

    return (
      <div
        className={styles.container}
      >
        <div className={styles.page}>
          <div className={styles.editor}>
            <div className={styles.letterHeader}>
              <div className={styles.letterHeaderLeft}>
                <div className={styles.logo} style={{ background: 'url(\'https://recci.fr/wp-content/themes/recci/images/logo.svg\') no-repeat center' }} />
                <div>
                  <div className={styles.smalltext}>3 RUE GAI SEJOUR</div>
                  <div className={styles.smalltext}>91700 SAINTE-GENEVIEVE-DES-BOIS</div>
                  <div className={styles.smalltext}>01 70 61 14 10</div>
                </div>
              </div>
              <div className={styles.letterHeaderRight}>
                <div className={classNames(styles.logo, styles.dashed)} />
                <div className={styles.destInfo}>
                  <Field
                    name="sendTo"
                    component={PhysicalPersonAutoComplete}
                    onChangeValues={(value) => {
                      const coordonnee = get(value, 'person.coordonnee', []).map(c => ({ value: c.value, label: c.value }));
                      this.setCoordinates({
                        ...coordinates,
                        user: coordonnee
                      });
                    }}
                    placeholder={I18n.t('letters.editor.dest')}
                    border
                  />
                  <Field
                    name="sendToSct"
                    component={MoralPersonAutoComplete}
                    onChangeValues={(value) => {
                      const address = get(value, 'society.address', '');
                      const coordonnee = get(value, 'society.coordonnee', []).map(c => ({ value: c.value, label: c.value }));
                      change('sendAddress', address);
                      this.setCoordinates({
                        ...coordinates,
                        society: coordonnee
                      });
                    }}
                    placeholder={I18n.t('letters.editor.steDest')}
                    border
                  />
                  <Field
                    name="sendAddress"
                    component={ReduxGrowInput}
                    placeholder={I18n.t('letters.editor.addressDest')}
                    style={{
                      maxWidth: '209px', fontSize: '12px', minHeight: '24px', outlineWidth: 0
                    }}
                  />
                  <Field
                    name="coodoonnee"
                    component={AutoCompleteCell}
                    options={[...coordinates.user, ...coordinates.society]}
                    placeholder={I18n.t('letters.editor.telMail')}
                    border
                    getValueLabel={v => v.label}
                    isCreatable
                  />
                </div>
                <div className={styles.date}>
                  <Field
                    name="date"
                    component="input"
                    type="date"
                  />
                </div>
              </div>
            </div>
            <div className={styles.letterObject}>
              <Field
                name="object"
                component={ReduxGrowInput}
                placeholder={I18n.t('letters.editor.object')}
                style={{
                  maxWidth: '495px', fontSize: '12px', outlineWidth: 0
                }}
              />
            </div>
            <div className={styles.letterEditor}>
              <Field
                name="body0"
                customRef={(el) => { this['editor[0]'] = el; }}
                onCustomChange={(e) => {
                  const page = splitIntoPages(e.target.value, 320);
                  if (page.length > 1) {
                    change('body0', page[0]);
                    change('body1', page[1]);
                    if (!pages[0]) {
                      this.generateNewPage(1);
                    }
                    setTimeout(() => {
                      this['editor[1]'].focus();
                      moveCaretToChar(this['editor[1]'], page[1].length);
                    }, 100);
                    return false;
                  }
                  return true;
                }}
                onCustomBlur={() => {
                  const { setCaret } = this.props;
                  const carretPosition = getCaretPos(this['editor[0]']);
                  setCaret({ page: 0, carretPosition });
                }}
                component={ReduxGrowInput}
                placeholder={I18n.t('letters.editor.contentLetter')}
                style={{
                  maxWidth: '472px', fontSize: '12px', minHeight: '24px', outlineWidth: 0
                }}
              />
            </div>
            <div className={styles.letterFooter}>
            Footer
            </div>
          </div>
          <div className={styles.footer}>
            <Typography variant="body1">{`${I18n.t('common.page')} 1`}</Typography>
          </div>
        </div>
        {pages}
      </div>
    );
  }
}

LetterEditor.propTypes = {
  setCaret: PropTypes.func.isRequired
};

export default LetterEditor;
