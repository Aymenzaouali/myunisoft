import CommentBox from './Box';
import CommentList from './List';

export {
  CommentBox,
  CommentList
};
