import React, {
  memo, useState, Fragment, useMemo, useEffect
} from 'react';
import {
  Typography, Collapse, Divider, IconButton,
  List, ListItem, ListItemText
} from '@material-ui/core';
import PropTypes from 'prop-types';

import I18n from 'assets/I18n';

import Button from 'components/basics/Buttons/Button';
import Loader from 'components/basics/Loader';
import FontIcon from 'components/basics/Icon/Font'; // TODO: Integrate good icons when font ready
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import BodyMessage from 'components/basics/Message/Body';
import Message from 'components/basics/Message';
import TextEditor from 'containers/groups/TextEditor';

import styles from './listComment.module.scss';

// Component
const CommentList = (props) => {
  const {
    summary, comments,
    isLoading, isUpdating, deps,
    loadData, onAdd, onEdit
  } = props;

  const children = comments.children || [];

  // State
  const [expanded, setExpanded] = useState({});
  const [history, setHistory] = useState([]);
  const [isAdding, setAdding] = useState(null);

  const isAllExpanded = useMemo(
    () => children.every(section => expanded[section.section_code]),
    [children, expanded]
  );

  // Effects
  useEffect(() => {
    loadData();
    setHistory([]);
  }, deps);

  // Events
  const handleExpand = (id) => {
    setExpanded({ ...expanded, [id]: !expanded[id] });
  };

  const expandAll = () => {
    if (isAllExpanded) {
      setExpanded({});
    } else {
      const expandAll = {};
      children.forEach((section) => { expandAll[section.section_code] = true; });
      setExpanded(expandAll);
    }
  };

  const handleAddSubmit = (section_code, cycle_id) => async (data) => {
    if (data.body) {
      await onAdd(data, section_code, cycle_id);
    }

    setAdding(null);
  };

  // Rendering
  return (
    <div className={styles.container}>
      <div className={styles.leftContainer}>
        <div className={styles.header}>
          <Typography variant="h6">{ comments.label }</Typography>
          { (summary && children.length > 0) && (
            <Button color="primary" onClick={expandAll}>
              {I18n.t(`${isAllExpanded ? 'commentList.closeAll' : 'commentList.openAll'}`)}
            </Button>
          ) }
          { (!summary) && (
            <IconButton
              disabled={isAdding != null}
              classes={{ root: styles.addButton }}

              onClick={() => setAdding(true)}
            >
              <FontIcon name="icon-plus" color="inherit" />
            </IconButton>
          ) }
        </div>
        { isLoading ? (
          <Loader className={styles.loader} />
        ) : (
          <List component="nav" classes={{ root: styles.comments }} disablePadding>
            { (isAdding === true) && (
              <ListItem>
                <TextEditor
                  className={styles.addEditor}
                  isLoading={isUpdating}
                  hideOptions

                  onSubmit={handleAddSubmit()}
                  onCancelCustom={() => setAdding(null)}
                />
              </ListItem>
            ) }
            { summary && children.map(section => (
              <Fragment key={section.section_code}>
                <ListItem button onClick={() => handleExpand(section.section_code)}>
                  <ListItemText primaryTypographyProps={{ variant: 'h4' }} primary={`${section.section_code} - ${section.label}`} />
                  { expanded[section.section_code] ? <ExpandLess /> : <ExpandMore /> }
                </ListItem>

                <Collapse in={expanded[section.section_code]} timeout="auto" unmountOnExit>
                  { section.data.map(cycle => (
                    <div key={cycle.cycle_id} className={styles.comment}>
                      <div className={styles.subHeader}>
                        <Typography variant="h4" color="primary">
                          {cycle.label}
                        </Typography>
                        <div className={styles.grow} />
                        <IconButton
                          size="small"
                          disabled={isAdding != null}
                          classes={{ root: styles.addButton }}

                          onClick={() => setAdding(cycle.cycle_id)}
                        >
                          <FontIcon name="icon-plus" color="inherit" />
                        </IconButton>
                      </div>
                      { (isAdding === cycle.cycle_id) && (
                        <TextEditor
                          className={styles.addEditor}
                          isLoading={isUpdating}
                          hideOptions

                          onSubmit={handleAddSubmit(section.section_code, cycle.cycle_id)}
                          onCancelCustom={() => setAdding(null)}
                        />
                      ) }

                      { cycle.data.map(item => (
                        <BodyMessage
                          key={item.id}
                          message={item}

                          onEdit={comment => onEdit(comment, section.section_code, cycle.cycle_id)}
                          onClick={() => { setHistory(item.history || []); }}
                        />
                      )) }
                    </div>
                  )) }
                </Collapse>
                <Divider />
              </Fragment>
            )) }

            { !summary && (comments.data || []).map(item => (
              <BodyMessage
                key={item.id}
                message={item}

                onEdit={onEdit}
                onClick={() => { setHistory(item.history || []); }}
              />
            )) }
          </List>
        ) }
      </div>
      <div className={styles.rightContainer}>
        {history && history.map((comment, i) => (
          <Message
            key={i}
            message={comment}
            historyState
          />
        ))}
      </div>
    </div>
  );
};

// Props
const comments = PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.any.isRequired,
    history: PropTypes.array
  }).isRequired
);

CommentList.propTypes = {
  comments: PropTypes.shape({
    label: PropTypes.string,

    children: PropTypes.arrayOf(
      PropTypes.shape({
        section_code: PropTypes.any.isRequired,
        label: PropTypes.string.isRequired,

        data: PropTypes.arrayOf(
          PropTypes.shape({
            cycle_id: PropTypes.any.isRequired,
            label: PropTypes.string.isRequired,

            data: comments
          }).isRequired
        ).isRequired
      }).isRequired
    ),

    data: comments
  }).isRequired,

  summary: PropTypes.bool,
  isLoading: PropTypes.bool,
  isUpdating: PropTypes.bool,
  deps: PropTypes.array,

  loadData: PropTypes.func,
  onAdd: PropTypes.func,
  onEdit: PropTypes.func
};

CommentList.defaultProps = {
  summary: false,
  isLoading: false,
  isUpdating: false,
  deps: [],

  loadData: () => {},
  onAdd: () => {},
  onEdit: () => {}
};

export default memo(CommentList);
