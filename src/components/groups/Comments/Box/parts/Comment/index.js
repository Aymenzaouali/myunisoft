import React from 'react';
import PropTypes from 'prop-types';
import Message from 'components/basics/Message';
import { TopCommentType } from '../../types';
import CommentHistory from './CommentHistory';

import styles from './Comment.module.scss';

class Comment extends React.PureComponent {
  render() {
    const {
      comment,
      onEdit
    } = this.props;

    const {
      history,
      ...mainCommentProps
    } = comment;

    return (
      <div className={styles.container}>
        <Message
          message={mainCommentProps}
          onEdit={onEdit}
        />
        <CommentHistory history={history} />
      </div>
    );
  }
}

Comment.propTypes = {
  comment: TopCommentType.isRequired,
  onEdit: PropTypes.func
};

Comment.defaultProps = {
  onEdit: undefined
};

export default Comment;
