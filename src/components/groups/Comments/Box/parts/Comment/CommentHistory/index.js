import React from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import {
  Typography,
  withStyles
} from '@material-ui/core';
import Message from 'components/basics/Message';
import { GeneralCommentType } from '../../../types';
import styles from './CommentHistory.module.scss';

class CommentHistory extends React.PureComponent {
  state = {
    historyOpen: false
  }

  onToggleShowHistory = () => {
    this.setState(state => ({
      historyOpen: !state.historyOpen
    }));
  }

  getActionButtonText = () => {
    const { historyOpen } = this.state;
    return historyOpen
      ? I18n.t('commentBox.hideHistory')
      : I18n.t('commentBox.showHistory');
  }

  render() {
    const { history, classes } = this.props;
    const { historyOpen } = this.state;

    if (!history.length) {
      return null;
    }

    return (
      <div className={styles.container}>
        <Typography
          onClick={this.onToggleShowHistory}
          classes={{ root: classes.showHistory }}
        >
          {this.getActionButtonText()}
        </Typography>
        {historyOpen && (
          <div className={styles.commentListContainer}>
            {history.map((comment, i) => (
              <div className={styles.commentWrapper} key={i}>
                <Message
                  message={comment}
                  historyState
                />
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}

const customTheme = () => ({
  showHistory: {
    cursor: 'pointer',
    color: '#0bd1d1',
    fontWeight: 'bold'
  }
});

CommentHistory.propTypes = {
  history: PropTypes.arrayOf(GeneralCommentType),
  classes: PropTypes.shape({}).isRequired
};

CommentHistory.defaultProps = {
  history: []
};

export default withStyles(customTheme)(CommentHistory);
