import React from 'react';
import PropTypes from 'prop-types';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import Loader from 'components/basics/Loader';
import Comment from '../Comment';
import { CommentsType } from '../../types';
import styles from './Comments.module.scss';

const Comments = React.memo((props) => {
  const {
    comments,
    onEdit,
    commentsLoading
  } = props;

  if (commentsLoading) {
    return <div style={{ padding: 20 }}><Loader /></div>;
  }

  if (!comments.length) {
    return null;
  }

  return comments.map((comment, i) => (
    <div className={styles.container}>
      <Comment
        comment={comment}
        onEdit={onEdit}
        key={i}
      />
    </div>
  ));
});

Comments.defaultProps = {
  comments: []
};

Comments.propTypes = {
  comments: CommentsType,
  onEdit: PropTypes.func.isRequired,
  commentsLoading: PropTypes.bool.isRequired
};

export default Comments;
