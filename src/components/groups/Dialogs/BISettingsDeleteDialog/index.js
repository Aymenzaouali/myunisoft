import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import I18n from 'assets/I18n';
import { FontIcon } from 'components/basics/Icon';

const BISettingsDeleteDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    massage,
    onClose,
    onValidate
  } = props;

  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.header }}>
        <FontIcon color="#fe3a5e" size={40} name="icon-alert" />
        <Typography
          variant="h3"
          classes={{ root: classes.title }}
        >
          {title}
        </Typography>
      </DialogContent>
      <DialogContent classes={{ root: classes.content }}>
        <div>
          {massage}
        </div>
      </DialogContent>
      <DialogActions classes={{ root: classes.footer }}>
        <Button
          variant="outlined"
          onClick={onClose}
        >
          {I18n.t('bankIntegrationSettings.popUp.ok')}
        </Button>
        <Button
          variant="contained"
          classes={{ root: classes.okBtn }}
          onClick={onValidate}
        >
          {I18n.t('bankIntegrationSettings.popUp.deleteOk')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const ownStyles = {
  title: {
    fontSize: '2rem', fontWeight: '500', marginTop: '8px'
  },
  header: {
    textAlign: 'center',
    display: 'contents'
  },
  content: {
    padding: '40px 130px 0',
    textAlign: 'center'
  },
  footer: {
    margin: '50px',
    justifyContent: 'center'
  },
  okBtn: {
    backgroundColor: '#fe3a5e',
    color: 'white',
    marginLeft: 10,
    '&:hover': {
      backgroundColor: '#ff8b1f'
    }
  }
};

BISettingsDeleteDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  massage: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired
};

export default withStyles(ownStyles)(BISettingsDeleteDialog);
