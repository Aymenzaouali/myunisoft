import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Dialog } from '@material-ui/core';
import { NewVATForm } from 'components/groups/Forms';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import styles from './VATDialog.module.scss';

const VATDialog = (props) => {
  const {
    isOpen,
    onClose,
    onValidate,
    modify
  } = props;

  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      onValidate={onValidate}
    >
      <div className={styles.container}>
        <div className={styles.title}>
          <Typography variant="h1">{modify ? I18n.t('tva.dialog.modify') : I18n.t('tva.dialog.title') }</Typography>
        </div>
        <NewVATForm />
        <div className={styles.buttonContainer}>
          <InlineButton
            buttons={[{
              _type: 'string',
              text: I18n.t('tva.dialog.cancel'),
              color: 'default',
              onClick: onClose,
              size: 'large'
            }, {
              _type: 'string',
              text: I18n.t('tva.dialog.save'),
              size: 'large',
              onClick: onValidate
            }]}
          />
        </div>
      </div>
    </Dialog>
  );
};

VATDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  modify: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onValidate: PropTypes.func.isRequired
};

export default VATDialog;
