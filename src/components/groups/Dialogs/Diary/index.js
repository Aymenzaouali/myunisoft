import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Dialog } from '@material-ui/core';
import DiaryForm from 'containers/groups/Forms/Diary';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import styles from './diary.module.scss';

const DiaryDialog = (props) => {
  const {
    isOpen,
    onCloseDialog,
    onValidate,
    creationMode,
    codeDisabled,
    labelDisabled,
    diaryTypeDisabled
  } = props;

  return (
    <Dialog
      creationMode={creationMode}
      open={isOpen}
      onClose={onCloseDialog}
    >
      <div className={styles.container}>
        <div className={styles.title}>
          <Typography variant="h1">{I18n.t('diary.dialog.title')}</Typography>
        </div>
        <DiaryForm
          codeDisabled={codeDisabled}
          labelDisabled={labelDisabled}
          diaryTypeDisabled={diaryTypeDisabled}
        />
        <div className={styles.buttonContainer}>
          <InlineButton
            buttons={[{
              _type: 'string',
              text: I18n.t('diary.dialog.cancel'),
              color: '#000',
              variant: 'outlined',
              onClick: onCloseDialog,
              size: 'medium'
            }, {
              _type: 'string',
              text: I18n.t('diary.dialog.save'),
              size: 'medium',
              onClick: onValidate
            }]}
          />
        </div>
      </div>
    </Dialog>
  );
};

DiaryDialog.defaultProps = {
  codeDisabled: false,
  labelDisabled: false,
  diaryTypeDisabled: false
};

DiaryDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  creationMode: PropTypes.bool.isRequired,
  onCloseDialog: PropTypes.func.isRequired,
  onValidate: PropTypes.func.isRequired,
  codeDisabled: PropTypes.bool,
  labelDisabled: PropTypes.bool,
  diaryTypeDisabled: PropTypes.bool
};

export default DiaryDialog;
