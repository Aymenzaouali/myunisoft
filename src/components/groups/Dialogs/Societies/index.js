import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import { AccessDashboard } from 'containers/groups/Dialogs';
import Button from 'components/basics/Buttons/Button';
import I18n from 'assets/I18n';
import ReduxAutoComplete from 'components/reduxForm/Inputs/AutoComplete';
import { Field } from 'redux-form';
import { Shortcuts } from 'react-shortcuts/lib';
import './styles.scss';

const SocietyDialog = (props) => {
  const {
    isOpen,
    societies,
    onValidateAndAddTab,
    onClose,
    handleSubmit,
    tabs,
    onValidate
  } = props;

  const [society, setSociety] = useState({});
  const [isOpenAccess, setIsOpenAccess] = useState(false);

  const validation = (values) => {
    const { society } = values;
    const existInTabs = tabs.find(t => parseInt(t.id, 10) === parseInt(society.society_id, 10));
    if (society.secured && !existInTabs) {
      setIsOpenAccess(true);
      setSociety(society);
    } else if (existInTabs) {
      onValidate({ society });
    } else {
      onValidateAndAddTab({ society });
    }
  };

  const validate = () => {
    handleSubmit(onValidateAndAddTab)();
    setIsOpenAccess(false);
  };

  const handleShortcuts = (action, e) => {
    const {
      onClose = () => {} // eslint-disable-line
    } = props;

    e.preventDefault();
    e.stopPropagation();
    switch (action) {
    case 'VALIDATE':
      handleSubmit(validation)();
      break;
    case 'CLOSE':
      onClose();
      break;
    default:
      break;
    }
  };

  const renderDialog = () => (
    <Dialog aria-labelledby="group_choice" open={isOpen} onClose={onClose}>
      <div className="dialogSocieties">
        <Field
          name="society"
          component={ReduxAutoComplete}
          placeholder={I18n.t('navBar.search')}
          textFieldProps={{
            label: I18n.t('navBar.dialogTitle'),
            InputLabelProps: {
              shrink: true
            }
          }}
          menuPosition="fixed"
          options={societies}
          maxHeight={465}
          isMulti={false}
          isClearable
          autoFocus
        />
        <Button
          className="form__button form__button-connect"
          size="large"
          variant="contained"
          color="primary"
          onClick={handleSubmit(validation)}
        >
          {I18n.t('login.validate')}
        </Button>
      </div>
    </Dialog>
  );

  return (
    <>
      {isOpen
        ? (
          <Shortcuts
            name="DIALOGS"
            tabIndex={-1}
            handler={handleShortcuts}
            alwaysFireHandler
            stopPropagation={false}
            preventDefault={false}
            targetNodeSelector="body"
          >
            {renderDialog()}
          </Shortcuts>
        )
        : (
          <>
            {renderDialog()}
          </>
        )}
      <AccessDashboard
        isOpen={isOpenAccess}
        society={society}
        onClose={() => setIsOpenAccess(false)}
        onValidate={validate}
      />
    </>
  );
};

SocietyDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onValidateAndAddTab: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  tabs: PropTypes.array.isRequired,
  societies: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onValidate: PropTypes.func.isRequired
};

export default SocietyDialog;
