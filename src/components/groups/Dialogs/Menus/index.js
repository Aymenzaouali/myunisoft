import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
  Dialog,
  IconButton,
  Slide,
  Typography,
  withStyles
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ListMenu from 'components/groups/Lists/Menus';
import I18n from 'i18next';
import { routesByKey, getRouteByKey } from 'helpers/routes';
import { getBudgetInsightModif } from 'helpers/url';
import DialogSocieties from 'containers/groups/Dialogs/Firms';
import _ from 'lodash';
import './styles.scss';

function Transition(props) {
  return <Slide direction="right" {...props} />;
}

class MenuDialog extends React.Component {
  renderListMenu = this.renderListMenu.bind(this);

  constructor(props) {
    super(props);
    this.state = {};
  }

  onModifClickButton = async () => {
    const { getBudgetInsightRedirection, societyId } = this.props;
    const token = await getBudgetInsightRedirection(societyId);
    window.open(getBudgetInsightModif(token));
  };

  renderListMenu({ key, items }, i) {
    const {
      push,
      onClose,
      societyId,
      toSocietyDialog,
      redirectFromCollab,
      data,
      rights,
      setSubCategory
    } = this.props;

    let lastMenu = false;
    const lastIndex = data.items.length - 1;
    const hasRights = key => (
      rights.some(right => (
        key === right.reference_front && right.irights === 1
      ))
    );

    if (data.items.length > 3 && i === 2) {
      lastMenu = false;
    } else if (data.items.length > 3 && key === data.items[lastIndex].key) {
      lastMenu = false;
    } else if (i === lastIndex) {
      lastMenu = false;
    } else {
      lastMenu = true;
    }

    return (
      <div key={`ListMenu${i}`} className="listContainer">
        <ListMenu
          menuTitle={key && I18n.t(`drawer.${key}`)}
          menuItems={items.reduce((acc, item) => {
            if (hasRights(item.key)) {
              acc.push(
                {
                  label: I18n.t(`drawer.${item.key}`),
                  disabled: !item.isEnable,
                  colorError: item.colorError,
                  key: item.key,
                  isExternal: item.isExternal
                }
              );
            }
            return acc;
          }, [])
          }
          onClickItem={(item) => {
            const route = getRouteByKey(item.key);
            const state = _.get(route, 'state', {});
            if (societyId !== -2) {
              if (item.isExternal) {
                this.onModifClickButton();
              } else {
                redirectFromCollab(route.path, state.type, societyId);
                if (item.key === 'booklet') setSubCategory(7);
              }
            } else if (societyId === -2 && !route.isNotCollab) {
              push(routesByKey[item.key]);
            } else {
              toSocietyDialog(route);
            }
            onClose();
          }}
        />
        {lastMenu && <div className="listContainer__divider" />}
      </div>
    );
  }

  renderMenu = () => {
    const {
      data
    } = this.props;
    const {
      items
    } = data;

    const nbLine = items.length / 2;
    let menu;
    let lineOne = [];
    let lineTwo = [];

    if (nbLine >= 2) {
      for (let i = 0; i < 3; i += 1) {
        lineOne = [...lineOne, items[i]];
      }

      for (let i = 3; i < items.length; i += 1) {
        lineTwo = [...lineTwo, items[i]];
      }

      menu = (
        <div>
          <div className="dialogContainer__menu">
            {lineOne.map(this.renderListMenu)}
          </div>
          <div className="listContainer__bottomDivider" />
          <div className="dialogContainer__menu">
            {lineTwo.map(this.renderListMenu)}
          </div>
        </div>
      );
    } else {
      menu = (
        <div className="dialogContainer__menu">
          {items.map(this.renderListMenu)}
        </div>
      );
    }

    return menu;
  }

  render() {
    const {
      isOpen,
      onClose,
      classes,
      data,
      isDrawerOpen,
      isDialogOpen,
      onCloseDialog
    } = this.props;

    return (
      <>
        <Dialog
          open={isOpen}
          onClose={onClose}
          fullScreen
          TransitionComponent={Transition}
          className={classNames('outer', {
            [classes.isDrawerOpen]: isDrawerOpen
          })}
          BackdropComponent={() => null}
        >
          <div className="dialogContainer">
            <div className="dialogContainer__header">
              <Typography variant="h1" classes={{ h1: classes.h1 }}>
                {data && I18n.t(`drawer.${data.key}`)}
              </Typography>
              <IconButton classes={{ root: classes.close }} onClick={onClose} aria-label="Close">
                <CloseIcon />
              </IconButton>
            </div>
            {data && this.renderMenu()}
          </div>
        </Dialog>
        {isDialogOpen && (
          <DialogSocieties
            isOpen={isDialogOpen}
            onClose={onCloseDialog}
          />
        )}
      </>
    );
  }
}

const styles = {
  isDrawerOpen: {
    marginLeft: '240px'
  },
  h1: {
    color: 'white',
    fontSize: 30,
    fontWeight: 500,
    fontStretch: 'normal',
    fontFamily: 'basier_circlemedium'
  },
  close: {
    color: '#fff'
  }
};

MenuDialog.defaultProps = {
  classes: {},
  data: null,
  redirectFromCollab: () => {},
  isDialogOpen: false,
  onCloseDialog: () => {}
};

MenuDialog.propTypes = {
  getBudgetInsightRedirection: PropTypes.func.isRequired,
  rights: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  isDrawerOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.shape({}),
  push: PropTypes.func.isRequired,
  data: PropTypes.shape({}),
  isDialogOpen: PropTypes.bool,
  onCloseDialog: PropTypes.func,
  societyId: PropTypes.number.isRequired,
  toSocietyDialog: PropTypes.func.isRequired,
  redirectFromCollab: PropTypes.func,
  setSubCategory: PropTypes.func.isRequired
};

export default withStyles(styles)(MenuDialog);
