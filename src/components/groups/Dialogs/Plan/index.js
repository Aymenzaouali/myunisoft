import React from 'react';
import PropTypes from 'prop-types';

import { Typography, Dialog } from '@material-ui/core';
import { PlanForm } from 'components/groups/Forms';
import { InlineButton } from 'components/basics/Buttons';

import I18n from 'assets/I18n';
import styles from './PlanDialog.module.scss';

const PlanDialog = (props) => {
  const {
    isOpen,
    onClose,
    onValidate,
    modify
  } = props;

  return (
    <Dialog open={isOpen} onClose={onClose}>
      <div className={styles.container}>
        <div className={styles.title}>
          <Typography variant="h1">{modify ? I18n.t('accountingPlans.dialogs.plan.modify') : I18n.t('accountingPlans.dialogs.plan.title') }</Typography>
        </div>
        <PlanForm modify={modify} />
        <div className={styles.buttonContainer}>
          <InlineButton
            buttons={[{
              _type: 'string',
              text: I18n.t('accountingPlans.dialogs.plan.cancel'),
              color: 'default',
              onClick: onClose,
              size: 'large'
            }, {
              _type: 'string',
              text: I18n.t('accountingPlans.dialogs.plan.save'),
              size: 'large',
              onClick: onValidate
            }]}
          />
        </div>
      </div>
    </Dialog>
  );
};

PlanDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  modify: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onValidate: PropTypes.func.isRequired
};


export default PlanDialog;
