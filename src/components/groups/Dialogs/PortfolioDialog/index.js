import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import { Field } from 'redux-form';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import ReduxAutoComplete from 'components/reduxForm/Inputs/AutoComplete';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import styles from './portfolioDialog.module.scss';

const PortfolioDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    onClose,
    onValidate,
    societies,
    getSociety,
    initializePortfolioData
  } = props;

  useEffect(() => {
    getSociety();
    initializePortfolioData();
  }, []);

  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h6" classes={{ root: classes.title }}>
          {title}
        </Typography>
      </DialogContent>
      <form>
        <DialogContent classes={{ root: classes.body }}>
          <div className={styles.row}>
            <Field
              color="primary"
              name="libelle"
              component={ReduxTextField}
              label={I18n.t('portfolioListView.dialog.portfolioName')}
              margin="none"
              style={{ width: '100%' }}
            />
          </div>
          <div className={styles.row}>
            <Field
              name="societies"
              component={ReduxAutoComplete}
              options={societies}
              isMulti
              menuPosition="fixed"
              placeholder=""
              textFieldProps={{
                label: I18n.t('newUserForm.rightManagement.society'),
                InputLabelProps: {
                  shrink: true
                }
              }}
              closeMenuOnSelect={false}
            />
          </div>
        </DialogContent>
        <DialogActions classes={{ root: classes.footer }}>
          <InlineButton
            buttons={[
              {
                _type: 'string',
                text: I18n.t('portfolioListView.dialog.cancel'),
                color: 'default',
                variant: 'outlined',
                size: 'medium',
                style: { marginLeft: 0 },
                onClick: onClose
              },
              {
                _type: 'string',
                variant: 'contained',
                onClick: onValidate,
                text: I18n.t('portfolioListView.dialog.ok'),
                size: 'medium'
              }]
            }
          />
        </DialogActions>
      </form>
    </Dialog>
  );
};

const ownStyles = {
  content: {
    display: 'contents'
  },
  footer: {
    margin: '0 50px 28px 50px',
    justifyContent: 'flex-start'
  },
  body: {
    paddingLeft: 50,
    paddingRight: 50,
    overflowY: 'inherit'
  },
  title: {
    marginLeft: 50,
    marginTop: '-40px',
    fontSize: '1.5rem'
  }
};

PortfolioDialog.defaultProps = {
  onClose: () => {}
};

PortfolioDialog.propTypes = {
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired,
  getSociety: PropTypes.func.isRequired,
  initializePortfolioData: PropTypes.func.isRequired,
  societies: PropTypes.arrayOf(PropTypes.shape({
    ape: PropTypes.string,
    city: PropTypes.string,
    companyType: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string,
    siret: PropTypes.string,
    society_id: PropTypes.number,
    status: PropTypes.string,
    step: PropTypes.string,
    value: PropTypes.number
  })).isRequired
};

export default withStyles(ownStyles)(PortfolioDialog);
