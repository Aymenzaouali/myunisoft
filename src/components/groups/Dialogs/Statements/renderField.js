import { InputLabel } from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';

const styles = {
  amountLabel: {
    marginBottom: '10px',
    display: 'inline-block'
  }
};

const renderField = (props) => {
  const {
    input,
    label,
    type,
    meta: { touched, error }
  } = props;
  return (
    <div>
      <InputLabel className={styles.amountLabel}>{label}</InputLabel>
      <div>
        <input {...input} type={type} placeholder={label} />
        {touched && error && <span>{error}</span>}
      </div>
    </div>
  );
};

renderField.propTypes = {
  input: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.shape({})
};

renderField.defaultProps = {
  meta: {}
};

export default renderField;
