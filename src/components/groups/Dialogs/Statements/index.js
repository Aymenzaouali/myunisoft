import React, { useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray } from 'redux-form';
import { formatNumber } from 'helpers/number';
import FontIcon from 'components/basics/Icon/Font';
import { AutoComplete, ReduxInputCell } from 'components/reduxForm/Inputs';

import {
  IconButton, Button,
  Dialog, DialogTitle, DialogContent, DialogActions,
  Typography, InputLabel, CircularProgress
} from '@material-ui/core';

import I18n from 'assets/I18n';
import classNames from 'classnames';
import renderField from './renderField';
import styles from './Statements.module.scss';

const renderPayment = (props) => {
  const { fields, ribList, countOfRibs } = props;
  return fields.map((item, key) => (
    <div className={styles.row} key={key}>
      <div className={styles.selectWrap}>
        <InputLabel>
          {`IBAN ${key + 1}`}
        </InputLabel>
        <Field
          name={`${item}.rib`}
          component={AutoComplete}
          closeMenuOnSelect
          options={ribList}
          className={styles.selectWrap}
        />
      </div>
      <div className={styles.amountWrap}>
        <InputLabel>
          {`Montant ${key + 1}`}
        </InputLabel>
        <Field
          color="primary"
          name={`${item}.amount`}
          component={renderField}
          type="number"
          className={styles.rightAlign}
        />
      </div>
      {
        (fields.length < 3 && fields.length < countOfRibs && key === fields.length - 1) && (
          <Button
            onClick={() => fields.push({ amount: 0, rib: { value: 1, label: '' } })}
            size="small"
            color="primary"
            variant="contained"
          >
            <FontIcon name="icon-plus" />
          </Button>
        )
      }
      {fields.length > 1 && (
        <Button onClick={() => fields.remove(key)} className={styles.deleteBtn}>
          <FontIcon name="icon-trash" color="red" />
        </Button>
      )}
    </div>
  ));
};

const Statements = (props) => {
  const {
    isOpen,
    onClose,
    onValidate,
    getBundleStatements,
    filteredRibsList,
    countOfRibs,
    isLoading,
    isError,
    totalAmount,
    amountToBePaid
  } = props;

  useEffect(() => {
    if (isOpen) {
      getBundleStatements();
    }
  }, [isOpen]);

  return (
    <Dialog classes={{ paper: styles.dialog }} open={isOpen} onClose={onClose}>
      <DialogTitle disableTypography>
        <Typography variant="h6" classes={{ root: styles.title }}>{I18n.t('statement.title')}</Typography>
        <IconButton classes={{ root: styles.close }} onClick={() => onClose()}>
          <FontIcon name="icon-close" />
        </IconButton>
      </DialogTitle>
      {
        !isLoading ? (
          <DialogContent>
            {!isError ? (
              <Fragment>
                <div className={styles.row}>
                  <div className={styles.col}>
                    <InputLabel>{I18n.t('statement.typeLabel')}</InputLabel>
                    <Field
                      name="type"
                      component={AutoComplete}
                      closeMenuOnSelect
                      className={styles.selectWrap}
                      options={[
                        { value: 'full', label: I18n.t('statement.fullPayment') },
                        { value: 'part', label: I18n.t('statement.partPayment') }
                      ]}
                    />
                  </div>
                </div>
                <FieldArray
                  name="payment"
                  component={renderPayment}
                  ribList={filteredRibsList}
                  countOfRibs={countOfRibs}
                />
                <div className={styles.row}>
                  <div className={classNames(styles.col, styles.payer)}>
                    <InputLabel>{I18n.t('statement.amountLabel')}</InputLabel>
                    <span>
                      {formatNumber(amountToBePaid, '0,0[.]00 $')}
                    </span>
                  </div>
                  <div className={classNames(styles.col, styles.total)}>
                    <InputLabel>{I18n.t('statement.totalAmountLabel')}</InputLabel>
                    <span>
                      {formatNumber(totalAmount, '0,0[.]00 $')}
                    </span>
                  </div>
                </div>

                <div className={classNames(styles.col)}>
                  <Field
                    color="primary"
                    name="amountToBePaid"
                    component={ReduxInputCell}
                    className={styles.hiddenInput}
                    hidden
                  />
                </div>
              </Fragment>
            ) : (
              <p>{I18n.t('statement.errors.noStatement')}</p>
            )}
          </DialogContent>
        ) : <div className={styles.loading}><CircularProgress /></div>
      }

      <DialogActions classes={{ root: styles.actions }}>
        <Button variant="outlined" onClick={onClose}>{I18n.t('common.cancel')}</Button>
        <Button
          disabled={isError || isLoading}
          variant="contained"
          color="primary"
          onClick={onValidate}
        >
          {I18n.t('statement.validate')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

// Props
Statements.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  onValidate: PropTypes.func.isRequired,
  getBundleStatements: PropTypes.func.isRequired,
  filteredRibsList: PropTypes.array.isRequired,
  countOfRibs: PropTypes.number.isRequired,
  amountToBePaid: PropTypes.string.isRequired,
  totalAmount: PropTypes.string.isRequired
};

Statements.defaultProps = ({
  onClose: () => {}
});

export default Statements;
