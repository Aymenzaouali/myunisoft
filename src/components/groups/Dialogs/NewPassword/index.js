import React from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { Field } from 'redux-form';
import { withStyles } from '@material-ui/core/styles';
import {
  Dialog, DialogActions, DialogContent, Typography
} from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import { ReduxTextField } from 'components/reduxForm/Inputs';

const NewPasswordDialog = (props) => {
  const {
    isOpen,
    onClose,
    classes,
    changePassword,
    handleSubmit,
    error
  } = props;

  const handleValidate = () => {
    handleSubmit(async (values) => {
      try {
        await changePassword(values);
        onClose();
      } catch (error) {
        // return error;
      }
      return true;
    })();
  };

  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h1">
          {I18n.t('newPassword.title')}
        </Typography>
        <Field
          name="old"
          type="password"
          component={ReduxTextField}
          label={I18n.t('newPassword.old')}
        />

        <Field
          name="newPassword"
          type="password"
          component={ReduxTextField}
          label={I18n.t('newPassword.new')}
        />

        <Field
          name="confirmation"
          type="password"
          component={ReduxTextField}
          label={I18n.t('newPassword.confirme')}
        />
        <Typography>
          { error }
        </Typography>
      </DialogContent>
      <DialogActions classes={{ root: classes.footer }}>
        <Button
          variant="contained"
          className={classes.buttonCancel}
          onClick={onClose}
        >
          {I18n.t('newPassword.close')}
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={handleValidate}
        >
          {I18n.t('newPassword.valide')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const styles = {
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonCancel: {
    marginRight: '20px'
  },
  footer: {
    justifyContent: 'center',
    paddingBottom: '40px'
  }
};

NewPasswordDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.shape({}).isRequired,
  changePassword: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired
};

NewPasswordDialog.defaultProps = {

};

export default withStyles(styles)(NewPasswordDialog);
