import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import _ from 'lodash';
import { Field } from 'redux-form';
import classNames from 'classnames';
import { ReduxSelect } from 'components/reduxForm/Selections';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import I18n from 'assets/I18n';
import { AccountAutoComplete } from 'containers/reduxForm/Inputs';
import { InlineButton } from 'components/basics/Buttons';
import styles from './worksheetsDialog.module.scss';

const WorksheetsDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    handleSubmit,
    onPostWorksheets,
    getWorksheetsTypes,
    worksheets_types,
    initialValues,
    reset,
    change,
    creationMode
  } = props;

  const [worksheetAccount, setWorksheetAccounts] = useState({});

  useEffect(() => {
    getWorksheetsTypes();
  }, []);

  useEffect(() => {
    if (!_.isEmpty(initialValues) && initialValues.id_worksheet_type) {
      const worksheet = initialValues.id_worksheet_type;
      const { account_beginning_ttc, account_beginning_tva } = worksheet;

      setWorksheetAccounts({
        account_beginning_ttc,
        account_beginning_tva
      });
    }
  }, [initialValues]);

  useEffect(() => {
    if (creationMode) {
      setWorksheetAccounts({});
    }
  }, [creationMode]);

  const closeDialog = () => {
    const { onClose } = props;
    onClose();
    reset();
    setWorksheetAccounts({});
  };

  const worksheetsTypes = worksheets_types.map(worksheet => ({
    value: worksheet,
    label: worksheet.code
  }));

  const submitForm = onPostWorksheets => handleSubmit(() => {
    const res = onPostWorksheets();
    setWorksheetAccounts({});
    return res;
  });


  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="md"
    >
      <DialogActions>
        <Button onClick={closeDialog}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h6" classes={{ root: classes.title }}>
          {title}
        </Typography>
      </DialogContent>
      <form>
        <DialogContent style={{ overflowY: 'inherit' }}>
          <div className={styles.row}>
            <Field
              component={ReduxSelect}
              name="id_worksheet_type"
              margin="none"
              label={I18n.t('settingsWorksheets.table.type')}
              className={styles.Field}
              list={worksheetsTypes}
              errorStyle={{ position: 'absolute' }}
              onChange={
                (event) => {
                  const worksheet = worksheets_types
                    .find(item => item.id === event.target.value.id);

                  change('id_account_ttc', undefined);
                  change('id_account_tva', undefined);
                  setWorksheetAccounts(worksheet);
                }
              }
            />
            <Field
              component={AccountAutoComplete}
              prefixed={worksheetAccount.account_beginning_ttc}
              placeholder=""
              errorStyle={{ position: 'absolute' }}
              name="id_account_ttc"
              margin="none"
              label={I18n.t('settingsWorksheets.table.accountNumber')}
              className={worksheetAccount.account_beginning_ttc || initialValues.id_account_ttc
                ? styles.Field : classNames(styles.Field, styles.FieldBlocked)}
              menuPosition="fixed"
              selectStyles={{
                input: { paddingLeft: 50 }
              }}
            />
            <Field
              component={AccountAutoComplete}
              prefixed={worksheetAccount.account_beginning_tva}
              placeholder=""
              errorStyle={{ position: 'absolute' }}
              name="id_account_tva"
              margin="none"
              label={I18n.t('settingsWorksheets.table.accountNumberTVA')}
              className={worksheetAccount.account_beginning_tva || initialValues.id_account_tva
                ? styles.Field : classNames(styles.Field, styles.FieldBlocked)}
              menuPosition="fixed"
              disabled={!worksheetAccount.account_beginning_tva}
              selectStyles={{
                input: { paddingLeft: 50 }
              }}
            />
          </div>
        </DialogContent>
        <DialogActions classes={{ root: classes.footer }}>
          <InlineButton
            buttons={[
              {
                _type: 'string',
                text: I18n.t('account.newAccount.cancel'),
                color: 'default',
                variant: 'outlined',
                size: 'medium',
                onClick: closeDialog
              },
              {
                _type: 'string',
                variant: 'contained',
                text: I18n.t('account.newAccount.save'),
                size: 'medium',
                onClick: submitForm(onPostWorksheets)
              }]
            }
          />
        </DialogActions>
      </form>
    </Dialog>
  );
};

const ownStyles = {
  content: {
    display: 'contents'
  },
  footer: {
    margin: 50,
    justifyContent: 'flex-start'
  },
  title: {
    marginLeft: 50,
    marginTop: '-40px',
    fontSize: '1.5rem'
  }
};

WorksheetsDialog.defaultProps = {
  onClose: () => {}
};

WorksheetsDialog.propTypes = {
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  creationMode: PropTypes.bool.isRequired,
  change: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onPostWorksheets: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  initialValues: PropTypes.shape({}).isRequired,
  worksheets_types: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  getWorksheetsTypes: PropTypes.func.isRequired
};

export default withStyles(ownStyles)(WorksheetsDialog);
