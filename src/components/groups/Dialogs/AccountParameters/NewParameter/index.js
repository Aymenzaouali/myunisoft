import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Typography
} from '@material-ui/core';
import I18n from 'assets/I18n';
import { AccountAutoComplete } from 'containers/reduxForm/Inputs';
import {
  ReduxTextField
} from 'components/reduxForm/Inputs';
import { InlineButton } from 'components/basics/Buttons';
import { Field } from 'redux-form';
import styles from '../accountParameters.module.scss';

const NewAccountParameter = (props) => {
  const {
    onClose,
    postAccountParam
  } = props;

  const buttons = [{
    _type: 'string',
    text: I18n.t('dialogAccountParam.newParameter.back'),
    onClick: () => onClose(),
    style: { color: '#000' },
    size: 'medium',
    variant: 'outlined'
  }, {
    _type: 'string',
    text: I18n.t('dialogAccountParam.validate'),
    onClick: async () => {
      await postAccountParam();
      onClose();
    },
    size: 'medium'
  }];

  return (
    <Fragment>
      <div className={styles.title}>
        <Typography variant="h1">{I18n.t('dialogAccountParam.newParameter.title')}</Typography>
      </div>
      <div className={styles.field}>
        <Field
          component={ReduxTextField}
          name="account_number_original"
          style={{ width: '360px' }}
          label={I18n.t('dialogAccountParam.newParameter.accountBase')}
        />
        <Field
          component={AccountAutoComplete}
          name="final_account"
          menuPosition="fixed"
          placeholder={I18n.t('dialogAccountParam.newParameter.numAccount')}
        />
      </div>
      <div className={styles.container}>
        <InlineButton buttons={buttons} />
      </div>
    </Fragment>
  );
};

NewAccountParameter.defaultProps = ({});

NewAccountParameter.propTypes = ({
  classes: PropTypes.shape({}).isRequired,
  postAccountParam: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
});

export default NewAccountParameter;
