import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Dialog
} from '@material-ui/core';
import ListAccountParameters from './ListParameter';
import NewAccountParameter from './NewParameter';

const AccountParameter = (props) => {
  const {
    onClose,
    getAccountParam,
    postAccountParam,
    list,
    loading,
    isOpen,
    societyId,
    initFieldAccount,
    smartCreationMode
  } = props;

  const [view, setView] = useState('list');

  useEffect(() => {
    initFieldAccount();
  }, []);

  const changeView = () => {
    setView(view === 'list' ? 'new' : 'list');
  };

  const renderList = () => (
    <ListAccountParameters
      newParam={changeView}
      onClose={onClose}
      getAccountParam={getAccountParam}
      list={list}
      loading={loading}
      societyId={societyId}
    />
  );

  const renderNew = () => (
    <NewAccountParameter
      onClose={smartCreationMode ? onClose : changeView}
      postAccountParam={postAccountParam}
    />
  );

  const render = () => {
    if (!smartCreationMode) {
      return view === 'list' ? renderList() : renderNew();
    }
    return renderNew();
  };

  return (
    <Dialog open={isOpen} onClose={onClose} fullWidth>
      {render()}
    </Dialog>
  );
};

AccountParameter.defaultProps = ({
  list: [],
  smartCreationMode: false
});

AccountParameter.propTypes = ({
  initFieldAccount: PropTypes.func.isRequired,
  postAccountParam: PropTypes.func.isRequired,
  getAccountParam: PropTypes.func.isRequired,
  societyId: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(
    PropTypes.shape({})
  ),
  loading: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.shape({}).isRequired,
  smartCreationMode: PropTypes.bool
});

export default AccountParameter;
