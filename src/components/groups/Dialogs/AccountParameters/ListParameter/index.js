import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import _get from 'lodash/get';

import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import { TableFactory } from 'components/groups/Tables';
import styles from '../accountParameters.module.scss';

const ListAccountParameters = (props) => {
  const {
    getAccountParam,
    societyId,
    onClose,
    list,
    newParam,
    loading
  } = props;

  useEffect(() => {
    getAccountParam(societyId);
  }, []);

  const buttons = [{
    _type: 'string',
    text: I18n.t('dialogAccountParam.cancel'),
    style: { color: '#000' },
    onClick: onClose,
    size: 'medium',
    variant: 'outlined'
  }, {
    _type: 'string',
    text: I18n.t('dialogAccountParam.validate'),
    size: 'medium',
    onClick: () => {
      onClose();
    }
  }];

  const button = [{
    _type: 'string',
    text: I18n.t('dialogAccountParam.selectParameter.new'),
    onClick: () => newParam(),
    size: 'large',
    variant: 'text'
  }];

  const body = list.map(element => ({
    keyRow: element.id_parameter,
    value: [{
      _type: 'string',
      keyCell: `numAccountBase${_get(element, 'account_number_original', '')}`,
      props: {
        label: element.account_number_original ? element.account_number_original.toString() : ''
      }
    },
    {
      _type: 'string',
      keyCell: `accountMyUnisoft${_get(element, 'account_third_parties_id', '')}`,
      props: {
        label: element.account_object.account_number_final ? element.account_object.account_number_final.toString() : ''
      }
    }]
  }));

  const param = {
    header: {
      rightClickActions: [],
      props: {},
      row: [{
        keyRow: 'row1',
        props: {},
        value: [
          {
            _type: 'string',
            keyCell: 'accountingBase',
            props: {
              label: I18n.t('dialogAccountParam.selectParameter.accountingBase')
            }
          },
          {
            _type: 'string',
            keyCell: 'accounting',
            props: {
              label: I18n.t('dialogAccountParam.selectParameter.accounting')
            }
          }
        ]
      }]
    },
    body: {
      rightClickActions: [{
        label: I18n.t('dialogAccountParam.selectParameter.modify'),
        onClick: newParam
      }],
      props: {},
      row: body
    }
  };

  return (
    <Fragment>
      <div className={styles.title}>
        <Typography variant="h1">{I18n.t('dialogAccountParam.selectParameter.title')}</Typography>
      </div>
      <div className={styles.table}>
        <TableFactory
          param={param}
          isLoading={loading}
        />
        <InlineButton buttons={button} />
      </div>
      <div className={styles.container}>
        <InlineButton buttons={buttons} />
      </div>
    </Fragment>
  );
};

ListAccountParameters.defaultProps = ({
  list: []
});

ListAccountParameters.propTypes = ({
  getAccountParam: PropTypes.func.isRequired,
  societyId: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(
    PropTypes.shape({})
  ),
  newParam: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.shape({}).isRequired
});

export default ListAccountParameters;
