import React from 'react';
import PropTypes from 'prop-types';
import { Trans } from 'react-i18next';

import {
  IconButton,
  Dialog, DialogTitle, DialogContent, DialogActions,
  Typography
} from '@material-ui/core';

import I18n from 'assets/I18n';

import Button from 'components/basics/Buttons/Button';
import FontIcon from 'components/basics/Icon/Font';

import styles from './ClosingYearConfirmation.module.scss';

// Component
const ClosingYearConfirmation = (props) => {
  const {
    open, onClose, onConfirm,
    start_date, end_date
  } = props;

  // Rendering
  return (
    <Dialog classes={{ paper: styles.dialog }} open={open} onClose={onClose}>
      <DialogTitle classes={{ root: styles.header }} disableTypography>
        <FontIcon name="icon-alert" size={46} />
        <IconButton classes={{ root: styles.closeBtn }} onClick={onClose}>
          <FontIcon name="icon-close" />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Typography
          classes={{ root: styles.question }}
          variant="h1"
        >
          <Trans i18nKey="closingYear.question">
            Voulez-vous clôturer l'exercice du {{start_date}} au {{end_date}} ? {/* eslint-disable-line */}
          </Trans>
        </Typography>
        <Typography variant="subtitle1" align="center">{I18n.t('closingYear.warning')}</Typography>
      </DialogContent>
      <DialogActions classes={{ root: styles.actions }}>
        <Button variant="contained" onClick={onClose}>{I18n.t('closingYear.cancel')}</Button>
        <Button variant="contained" color="error" onClick={onConfirm}>{I18n.t('closingYear.submit')}</Button>
      </DialogActions>
    </Dialog>
  );
};

// Props
ClosingYearConfirmation.propTypes = {
  open: PropTypes.bool.isRequired,
  start_date: PropTypes.string.isRequired,
  end_date: PropTypes.string.isRequired,

  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired
};

export default ClosingYearConfirmation;
