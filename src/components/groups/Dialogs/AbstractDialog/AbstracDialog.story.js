/* eslint-disable jsx-a11y/accessible-emoji */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import {
  text,
  number
} from '@storybook/addon-knobs';
import _ from 'lodash';
import Calculator from 'components/basics/Calculator';

import Dialog from 'components/groups/Dialogs/AbstractDialog';

const stories = storiesOf('Dialogs', module);

const generateRandomDialog = i => ({
  title: `Title ${i}`,
  body: `Congratulations! Your layer number is ${i}`,
  width: 500 - 50 * i,
  height: 400 - 50 * i,
  buttons: [
    {
      _type: 'string',
      text: `Cancel ${i}`,
      onClick: action(`Pressed cancel ${i}`),
      color: 'error',
      variant: 'outlined'
    },
    {
      _type: 'string',
      text: `Submit ${i}`,
      onClick: action(`Pressed submit ${i}`),
      color: 'default',
      variant: 'outlined'
    }
  ]
});

const generateButton = i => ({
  _type: 'string',
  iconName: _.shuffle(['', 'icon-close'])[0],
  text: `Button ${i}`,
  onClick: action(`Clicked button ${i}`),
  color: _.shuffle(['secondary', 'default', 'primary', 'textSecondary'])[0],
  disabled: i % 2,
  variant: _.shuffle(['', 'outlined'])[0],
  titleInfoBulle: `Indeed Button ${i}`
});

const generateButtons = (number) => {
  const buttons = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < number; i++) {
    buttons.push(generateButton(i));
  }
  return buttons;
};

stories.addDecorator(story => (
  <>
    <p>Control the dialog with Knobs 🤔 🚀</p>
    {story()}
  </>
));
stories.add('Generic', () => {
  const dialogs = [{
    title: text('Title', 'This is very important'),
    body: text('Body', 'Lorem ipsum dolor sit amet, elit iudico vis in, sed senserit explicari eu. Oblique ancillae honestatis quo te. Eam at doming nonumes, ea vero partem delicata per, no saepe detraxit mei. Modo probatus temporibus has ex, noster tritani eu eos. Ullum nostrum moderatius ea has, adhuc putent at eum, te dicit iriure ancillae mel. Diam enim deterruisset qui ex, suas prima voluptua pro at, in falli ornatus appareat eam. Doctus quaestio te cum, et pro animal scaevola.')
  }];

  return (
    <Dialog
      dialogs={dialogs}
      closeDialog={action('Close dialog')}
    />
  );
});


stories.add('With a component for body', () => {
  const dialogs = [{
    title: text('Title', 'Calculate this'),
    body: {
      Component: Calculator
    }
  }];

  return (
    <Dialog
      dialogs={dialogs}
      closeDialog={action('Close dialog')}
    />
  );
});

stories.add('With a component for title', () => {
  const MyTitle = () => (
    <h1>
      ❤️Viva la 🌯
      <br />
      <small style={{ fontSize: 16, color: '#e7e7e7' }}>Taco is better</small>
    </h1>
  );
  const dialogs = [{
    title: <MyTitle />,
    body: text('Body', 'I am a useful text that you think is worth reading but think again...')
  }];

  return (
    <Dialog
      dialogs={dialogs}
      closeDialog={action('Close dialog')}
    />
  );
});

stories.add('Custom width', () => {
  const dialogs = [{
    title: text('Title', 'This is very important'),
    body: text('Body', 'Lorem ipsum dolor sit amet, elit iudico vis in, sed senserit explicari eu. Oblique ancillae honestatis quo te. Eam at doming nonumes, ea vero partem delicata per, no saepe detraxit mei. Modo probatus temporibus has ex, noster tritani eu eos. Ullum nostrum moderatius ea has, adhuc putent at eum, te dicit iriure ancillae mel. Diam enim deterruisset qui ex, suas prima voluptua pro at, in falli ornatus appareat eam. Doctus quaestio te cum, et pro animal scaevola.'),
    width: number('Width', 600),
    height: number('Height', 600)
  }];

  return (
    <Dialog
      dialogs={dialogs}
      closeDialog={action('Close dialog')}
    />
  );
});

stories.add('Controllable', () => {
  const numberOfButtons = number('Number of buttons', 2);
  const dialogs = [{
    title: text('Title', 'This is very important'),
    body: text('Body', 'Sausages are on a discount now!!!!!!!!'),
    buttons: generateButtons(numberOfButtons)
  }];

  return (
    <Dialog
      dialogs={dialogs}
      closeDialog={action('Close dialog')}
    />
  );
});

stories.add('Multiple layers', () => {
  const dialogs = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < number('Number of dialogs', 1); i++) {
    dialogs.push(generateRandomDialog(i));
  }

  return (
    <Dialog
      dialogs={dialogs}
      closeDialog={action('Close dialog')}
    />
  );
});
