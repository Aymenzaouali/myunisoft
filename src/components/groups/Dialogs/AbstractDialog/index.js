/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import {
  InlineButton,
  ButtonProps
} from 'components/basics/Buttons';
import { FontIcon } from 'components/basics/Icon';
import {
  Typography,
  withStyles,
  DialogActions,
  DialogContent,
  DialogTitle,
  Dialog
} from '@material-ui/core';
import classnames from 'classnames';

import styles from './AbstractDialog.module.scss';

class AbstractDialog extends React.PureComponent {
  renderTitle = (title) => {
    const { classes } = this.props;

    if (this.isAComponent(title)) {
      return title;
    }

    return (
      <Typography variant="h1" classes={{ root: classes.textTitleRoot }}>
        {title}
      </Typography>
    );
  }

  renderBody = (body) => {
    if (typeof body !== 'string') {
      const {
        Component,
        props
      } = body;

      return <Component {...props} />;
    }

    return body;
  }

  paperContainerClass = (title, body) => {
    const { classes } = this.props;

    return classnames(
      this.isAComponent(body)
        ? classes.paperContainerWithBodyComponent
        : classes.paperContainer,
      { [classes.noPaddingTop]: !title && this.isAComponent(body) }
    );
  }

  isAComponent = value => typeof value !== 'string';

  handleClose = () => {
    const { closeDialog } = this.props;
    closeDialog();
  }

  handleButtonCb = cb => async () => {
    if (cb) {
      await cb();
    }
    this.handleClose();
  }

  renderDialog = (dialogProps, key) => {
    const { classes } = this.props;

    const {
      buttons,
      title,
      body,
      height,
      width
    } = dialogProps;

    return (
      <Dialog
        open
        onClose={this.handleClose}
        PaperProps={{
          style: { height, width }
        }}
        fullWidth
        classes={{
          paper: this.paperContainerClass(title, body)
        }}
        key={key}
      >
        <div className={styles.iconContainer} onClick={this.handleClose}>
          <FontIcon
            size={19}
            className="icon-close"
            titleInfoBulle={I18n.t('tooltips.close')}
          />
        </div>
        <div>
          <div>
            {title && (
              <DialogTitle disableTypography classes={{ root: classes.titleRoot }}>
                {this.renderTitle(title)}
              </DialogTitle>
            )}
            <DialogContent
              classes={{
                root: this.isAComponent(body)
                  ? classes.contentRootWithBodyComponent
                  : classes.contentRoot
              }}
            >
              {this.renderBody(body)}
            </DialogContent>
            {buttons && buttons.length ? (
              <DialogActions classes={{ root: classes.actionsRoot }}>
                <InlineButton
                  marginDirection="left"
                  buttons={buttons.map(button => ({
                    ...button,
                    onClick: this.handleButtonCb(button.onClick)
                  }))}
                />
              </DialogActions>
            ) : null}
          </div>
        </div>
      </Dialog>
    );
  }

  render() {
    const { dialogs } = this.props;
    return dialogs.map(this.renderDialog);
  }
}

AbstractDialog.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  dialogs: PropTypes.arrayOf(
    PropTypes.shape({
      isOpen: PropTypes.bool.isRequired,
      width: PropTypes.string,
      height: PropTypes.string,
      title: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
      ]),
      body: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
      ]).isRequired,
      buttons: PropTypes.arrayOf(ButtonProps)
    })
  ).isRequired,
  closeDialog: PropTypes.func.isRequired
};

const theme = {
  paperContainer: {
    padding: '34px 44px',
    textAlign: 'center'
  },
  noPaddingTop: {
    paddingTop: 0
  },
  titleRoot: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 0,
    marginBottom: 30,
    padding: 0
  },
  textTitleRoot: {
    fontSize: 24,
    fontWeight: 500,
    margin: 0
  },
  contentRoot: {
    maskImage: 'linear-gradient(to top, transparent 0%, white 2%, white 98%, transparent 100%)',
    padding: '0 24px 0 0'
  },
  contentRootWithBodyComponent: {
    padding: 0,
    display: 'flex',
    flexDirection: 'column'
  },
  actionsRoot: {
    margin: 0,
    marginTop: 30,
    justifyContent: 'center',
    display: 'flex',
    alignItems: 'center'
  }
};

export default withStyles(theme)(AbstractDialog);
