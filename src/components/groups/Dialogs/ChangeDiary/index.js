import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogActions,
  DialogContent
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import { DiaryAutoComplete } from 'containers/reduxForm/Inputs';

import style from './changeDiary.module.scss';

const ChangeDiary = (props) => {
  const {
    isOpen,
    onClose,
    classes,
    onValidate
  } = props;

  return (
    <Dialog
      fullWidth
      aria-labelledby="change_accompt"
      open={isOpen}
      onClose={onClose}
    >

      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" titleInfoBulle={I18n.t('tooltips.close')} />
        </Button>
      </DialogActions>

      <DialogContent classes={{ root: classes.content }}>
        <span className={style.title}>{I18n.t('changeDiary.title')}</span>
        <div className={style.containerSelection}>
          <Field
            component={DiaryAutoComplete}
            menuPosition="fixed"
            name="diary"
            label={I18n.t('changeDiary.switchAccount')}
            autoFocus
          />
        </div>
      </DialogContent>

      <DialogActions classes={{ root: classes.footer }}>
        <Button variant="contained" className={classes.buttonCancel} onClick={onClose}>
          {I18n.t('changeDiary.cancelled')}
        </Button>
        <Button variant="contained" className={classes.buttonValidate} onClick={onValidate}>
          {I18n.t('changeDiary.validate')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const styles = {
  content: {
    textAlign: 'center',
    Zindex: 1500,
    overflow: 'visible'
  },
  button: {
    background: '#0bd1d1',
    marginLeft: '20px'
  },
  footer: {
    margin: '50px',
    justifyContent: 'space-evenly'
  },
  buttonValidate: {
    background: '#0bd1d1',
    color: 'white'
  }
};

ChangeDiary.propTypes = {
  data: PropTypes.shape({}).isRequired,
  onValidate: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  classes: PropTypes.shape({}).isRequired
};

export default withStyles(styles)(ChangeDiary);
