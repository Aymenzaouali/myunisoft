import React, { useEffect, useState } from 'react';
import {
  Dialog, DialogActions, DialogContent, DialogTitle, withStyles
} from '@material-ui/core';
import SelectLikeMacFinder from 'components/basics/SelectLikeMacFinder';
import { InlineButton } from 'components/basics/Buttons';
import PropTypes from 'prop-types';
import I18n from 'i18next';
import ownStyles from './selectLikeFinder.module.scss';

const CopyDocumentToDiscussion = ({
  title, tree, isOpen, onClose, classes, getDiscussionFolders, onRoomChosen
}) => {
  const [selectedFolder, setSelectedFolder] = useState([]);
  useEffect(() => {
    getDiscussionFolders();
  }, []);


  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      fullWidth
      maxWidth="md"
      classes={{ paper: ownStyles.dialog }}
    >
      <DialogTitle classes={{ root: classes.title }}>{title}</DialogTitle>
      <DialogContent className={ownStyles.dialogContent}>
        <SelectLikeMacFinder tree={tree} onChange={selected => setSelectedFolder(selected)} />
      </DialogContent>
      <DialogActions className={ownStyles.dialogActions}>
        <InlineButton
          buttons={[
            {
              _type: 'string',
              text: I18n.t('portfolioListView.dialog.cancel'),
              color: 'default',
              variant: 'outlined',
              size: 'medium',
              onClick: onClose
            },
            {
              _type: 'string',
              variant: 'contained',
              text: I18n.t('portfolioListView.dialog.ok'),
              size: 'medium',
              disabled: !selectedFolder.length
                || (selectedFolder.length && !Object.prototype.hasOwnProperty.call(selectedFolder[selectedFolder.length - 1], 'room_id')),
              onClick: () => onRoomChosen(selectedFolder[selectedFolder.length - 1])
            }]
          }
        />
      </DialogActions>
    </Dialog>
  );
};

CopyDocumentToDiscussion.propTypes = {
  title: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  tree: PropTypes.shape({}).isRequired,
  onClose: PropTypes.func.isRequired,
  onRoomChosen: PropTypes.func.isRequired,
  getDiscussionFolders: PropTypes.func.isRequired
};

CopyDocumentToDiscussion.defaultProps = {
  title: ''
};

const styles = {
  title: {
    textAlign: 'center'
  },
  content: {
    overflow: 'hidden',
    paddingTop: '0'
  },
  footer: {
  }
};

export default withStyles(styles)(CopyDocumentToDiscussion);
