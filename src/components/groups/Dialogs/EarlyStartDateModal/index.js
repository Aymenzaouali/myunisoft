import React from 'react';
import {
  IconButton,
  Dialog, DialogTitle, DialogContent, DialogActions,
  Typography
} from '@material-ui/core';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';

import Button from 'components/basics/Buttons/Button';
import FontIcon from 'components/basics/Icon/Font';
import { withShortcut } from 'hoc/dialogShortcut';

import styles from './FixedAssetsModal.module.scss';

// Component
const EarlyStartDateModal = ({
  open, closeDialog, onClose
}) => (
  <Dialog classes={{ paper: styles.dialog }} open={open} onClose={onClose}>
    <DialogTitle classes={{ root: styles.header }} disableTypography>
      <IconButton classes={{ root: styles.closeBtn }} onClick={onClose}>
        <FontIcon name="icon-close" />
      </IconButton>
    </DialogTitle>
    <DialogContent>
      <Typography
        classes={{ root: styles.headerTitle }}
        variant="h5"
      >
        {I18n.t('fixedAssets.dialogs.earlyStartDateModal.title')}
      </Typography>
    </DialogContent>
    <DialogActions classes={{ root: styles.actions }}>
      <Button
        variant="outlined"
        onClick={closeDialog}
      >
        {I18n.t('fixedAssets.modal.cancel')}
      </Button>
      <Button
        variant="contained"
        color="primary"
        classess={{ root: styles.actions.submit }}
        onClick={onClose}
      >
        {I18n.t('fixedAssets.modal.submit')}
      </Button>
    </DialogActions>
  </Dialog>
);


EarlyStartDateModal.propTypes = {
  open: PropTypes.bool,
  closeDialog: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

EarlyStartDateModal.defaultProps = {
  open: false
};

export default withShortcut()(EarlyStartDateModal);
