import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import I18n from 'assets/I18n';
import { FontIcon } from 'components/basics/Icon';

const DeleteConfirmation = (props) => {
  const {
    deleteButtonText,
    isOpen,
    classes,
    title,
    message,
    secondMessage,
    onClose,
    onValidate
  } = props;

  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button classes={{ root: classes.closeButton }} onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.header }}>
        <FontIcon color="error" size={40} name="icon-alert" />
        <Typography
          variant="h3"
          classes={{ root: classes.title }}
        >
          {title}
        </Typography>
      </DialogContent>
      <DialogContent classes={{ root: classes.content }}>
        <div>
          {message}
        </div>
        <div>
          {secondMessage}
        </div>
      </DialogContent>
      <DialogActions classes={{ root: classes.footer }}>
        <Button
          variant="outlined"
          onClick={onClose}
        >
          {I18n.t('deleteDialog.cancel')}
        </Button>
        <Button
          variant="contained"
          classes={{ root: classes.okBtn }}
          onClick={onValidate}
        >
          {deleteButtonText}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const ownStyles = ({ palette }) => ({
  title: {
    fontSize: '1.5rem', fontWeight: '800', marginTop: '8px'
  },
  header: {
    display: 'contents',
    color: '#fe3a5e',
    textAlign: 'center'
  },
  content: {
    padding: '40px 0 0',
    textAlign: 'center'
  },
  footer: {
    margin: '30px',
    justifyContent: 'center'
  },
  okBtn: {
    backgroundColor: palette.error.main,
    color: 'white',
    marginLeft: 10,
    '&:hover': {
      backgroundColor: palette.secondary.main
    }
  },
  closeButton: {
    position: 'absolute',
    top: '10px'
  }
});

DeleteConfirmation.defaultProps = {
  onClose: () => {},
  deleteButtonText: I18n.t('deleteDialog.ok')
};

DeleteConfirmation.propTypes = {
  deleteButtonText: PropTypes.string,
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  secondMessage: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired
};

export default withStyles(ownStyles)(DeleteConfirmation);
