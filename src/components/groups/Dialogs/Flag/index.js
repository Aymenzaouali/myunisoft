import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import I18n from 'assets/I18n';
import { Field } from 'redux-form';
import {
  IconButton,
  Dialog, DialogActions, DialogContent, Typography, DialogTitle
} from '@material-ui/core';
import Periode from 'containers/groups/Periode';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import ReduxAutoComplete from 'components/reduxForm/Inputs/AutoComplete';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';

import styles from './flagDialog.module.scss';

const FlagDialog = (props) => {
  const {
    form,
    flagType,
    vatOptions,
    ttcOptions,
    isOpen,
    onClose,
    initDialogTtcField,
    initDialogTvaField,
    dossierOptions,
    onValidate,
    amortissementOptions
  } = props;

  const [isImmoRef, setImmoRef] = useState(null);

  useEffect(() => {
    if (flagType === 'IMMO' && isImmoRef) isImmoRef.focus();
  }, [isImmoRef]);

  return (
    <Dialog
      classes={{ paper: styles.dialog }}
      open={isOpen}
      onClose={onClose}
      fullWidth
      maxWidth="sm"
    >
      <DialogTitle classes={{ root: styles.title }} disableTypography>
        <Typography variant="h1">
          {I18n.t('newAccounting.flagDialog.title', { flagType })}
        </Typography>
        <IconButton classes={{ root: styles.close }} onClick={onClose}>
          <FontIcon className="icon-close" />
        </IconButton>
      </DialogTitle>
      <DialogContent classes={{ root: styles.content }}>
        <form id={form} onSubmit={onValidate}>
          { (flagType === 'CCA' || flagType === 'PCA') && (
            <Periode datePickers={styles.pickers} row leftDatePicker={styles.leftPicker} />
          ) }
          {(flagType === 'IMMO')
        && (
          <div className={styles.autoCompleteContainer}>
            <Field
              component={ReduxMaterialDatePicker}
              className={styles.fieldContainer}
              label={I18n.t('newAccounting.flagDialog.field.date')}
              name="date"
              margin="none"
            />
            <Field
              classes={{ root: styles.fieldContainer }}
              component={ReduxSelect}
              label={I18n.t('newAccounting.flagDialog.field.amortissement')}
              list={amortissementOptions}
              margin="none"
              name="amortissement"
            />
            <Field
              component={ReduxTextField}
              type="number"
              className={styles.fieldContainer}
              label={I18n.t('newAccounting.flagDialog.field.duration')}
              name="duration"
              margin="none"
              inputRef={inputElement => setImmoRef(inputElement)}
            />
          </div>
        )
          }
          <div className={styles.autoCompleteContainer}>
            { (vatOptions.length > 1) && (
              <Field
                component={ReduxSelect}
                classes={{ root: styles.fieldContainer }}
                list={vatOptions}
                label={I18n.t('newAccounting.flagDialog.field.vat_account')}
                margin="none"
                name="vat_account"
                onChange={event => initDialogTtcField(event.target.value)}
              />
            ) }
            { (ttcOptions.length > 1) && (
              <Field
                component={ReduxSelect}
                classes={{ root: styles.fieldContainer }}
                list={ttcOptions}
                label={I18n.t('newAccounting.flagDialog.field.ttc_account')}
                margin="none"
                name="ttc_account"
                onChange={event => initDialogTvaField(event.target.value)}
              />
            ) }
            { (dossierOptions.length > 1) && (flagType !== 'IMMO') && (
              <Field
                component={ReduxAutoComplete}
                className={styles.fieldContainer}
                options={dossierOptions}
                menuPosition="fixed"
                placeholder={I18n.t('newAccounting.flagDialog.field.dossier')}
                margin="none"
                name="dossier_revision"
              />
            ) }
          </div>
        </form>
      </DialogContent>
      <DialogActions classes={{ root: styles.footer }}>
        <Button
          form={form}
          type="button"
          variant="outlined"
          onClick={onClose}
        >
          {I18n.t('newAccounting.flagDialog.cancel')}
        </Button>
        <Button variant="contained" color="primary" form={form} type="submit">
          {I18n.t('newAccounting.flagDialog.flag')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

FlagDialog.defaultProps = {
  flagType: 'random flag',
  vatOptions: [],
  ttcOptions: [],
  dossierOptions: [],
  amortissementOptions: []
};

FlagDialog.propTypes = {
  form: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired,
  initDialogTvaField: PropTypes.func.isRequired,
  initDialogTtcField: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  flagType: PropTypes.string,
  dossierOptions: PropTypes.arrayOf(PropTypes.shape({})),
  vatOptions: PropTypes.arrayOf(PropTypes.shape({})),
  ttcOptions: PropTypes.arrayOf(PropTypes.shape({})),
  amortissementOptions: PropTypes.arrayOf(PropTypes.shape({}))
};

export default FlagDialog;
