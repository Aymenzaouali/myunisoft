import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogActions,
  DialogContent
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import { AccountAutoComplete } from 'containers/reduxForm/Inputs';

import style from './changeAccount.module.scss';

const ChangeAccount = (props) => {
  const {
    isOpen,
    onClose,
    classes,
    onValidate
  } = props;

  return (
    <Dialog
      fullWidth
      aria-labelledby="change_accompt"
      open={isOpen}
      onClose={onClose}
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" titleInfoBulle={I18n.t('tooltips.close')} />
        </Button>
      </DialogActions>

      <DialogContent classes={{ root: classes.content }}>
        <span className={style.title}>{I18n.t('changeAccount.title')}</span>
        <div className={style.containerSelection}>
          <Field
            component={AccountAutoComplete}
            name="account"
            label={I18n.t('changeAccount.switchAccount')}
            menuPosition="fixed"
            autoFocus
          />
        </div>
      </DialogContent>

      <DialogActions classes={{ root: classes.footer }}>
        <Button variant="contained" className={classes.buttonCancel} onClick={onClose}>
          {I18n.t('changeAccount.cancelled')}
        </Button>
        <Button variant="contained" className={classes.buttonValidate} onClick={onValidate}>
          {I18n.t('changeAccount.validate')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const styles = {
  content: {
    textAlign: 'center',
    Zindex: 1500,
    overflow: 'visible'
  },
  button: {
    background: '#0bd1d1',
    marginLeft: '20px'
  },
  footer: {
    margin: '50px',
    justifyContent: 'space-evenly'
  },
  buttonValidate: {
    background: '#0bd1d1',
    color: 'white'
  }
};


ChangeAccount.propTypes = {
  onValidate: PropTypes.string.isRequired,
  data: PropTypes.shape({}).isRequired,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  classes: PropTypes.shape({}).isRequired
};

export default withStyles(styles)(ChangeAccount);
