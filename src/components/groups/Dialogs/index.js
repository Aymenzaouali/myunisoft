import FilesDropperDialog from './FilesDropper';
import GroupsDialog from './Groups';
import MenuDialog from './Menus';
import SocietiesDialog from './Societies';
import TextFieldDialog from './TextField';
import AccountDialog from './Account';
import ConfirmationDialog from './Confirmation';
import CalculatorDialog from './Calculator';
import ChangeAccount from './ChangeAccount';
import ChangeDiary from './ChangeDiary';
import VATDialog from './VAT';
import ShortcurtsListDialog from './ShortcutsList';
import LettrageDialog from './Lettrage';
import FECDialog from './FEC';
import AbstractDialog from './AbstractDialog';
import WorkingPageDialog from './WorkingPage';
import NewPasswordDialog from './NewPassword';
import AccountParameters from './AccountParameters';
import DiaryDialog from './Diary';
import ConnectorDialog from './Connector';
import FunctionDialog from './Function';
import FlagDialog from './Flag';
import WorksheetsDialog from './WorksheetsDialog';
import WorksheetsDeleteDialog from './WorksheetsDeleteDialog';
import CguDialog from './CGU';
import BISettingsDialog from './BISettingsDialog';
import BISettingsDeleteDialog from './BISettingsDeleteDialog';
import PortfolioDialog from './PortfolioDialog';
import AccessDashboardSociety from './AccessDashboard';
import ReportsAndFormsDialog from './ReportsAndFormsDialog';
import RAFConfirmDialog from './RAFConfirmDialog';
import AssociateDeleteDialog from './AssociateDeleteDialog';
import SocietyPPDeleteDialog from './SocietyPPDeleteDialog';
import EmployeePPDeleteDialog from './EmployeePPDeleteDialog';
import SubsidiaryDeleteDialog from './SubsidiaryDeleteDialog';
import FormsAndReportsDeleteDialog from './FormsAndReportsDeleteDialog';
import NewLetterDialog from './NewLetter';
import DeleteConfirmation from './DeleteConfirmation';
import InformationDialog from './InformationDialog';
import SettingStandardMailDialog from './SettingStandardMailDialog';
import SettingStandardMailDeleteDialog from './SettingStandardMailDeleteDialog';
import MailParagraphDialog from './MailParagraphDialog';
import MailParagraphDeleteDialog from './MailParagraphDeleteDialog';
import SuccessfulDialog from './SuccessfulDialog';
import PlanDialog from './Plan';
import PlanDetailDialog from './PlanDetail';
import AccountingFirmDeclareTableDialog from './AccountingFirmDeclareTableDialog';
import SaleDialog from './SaleDialog';
import PlaquetteTree from './PlaquetteTree';
import CopyDocumentToDiscussion from './CopyDocumentToDiscussion';
import ODDialog from './OD';
import EarlyStartDateModal from './EarlyStartDateModal';
import LinkAlreadySent from './LinkAlreadySent';

export {
  FilesDropperDialog,
  GroupsDialog,
  MenuDialog,
  SocietiesDialog,
  TextFieldDialog,
  AccountDialog,
  ConfirmationDialog,
  CalculatorDialog,
  ChangeAccount,
  ChangeDiary,
  VATDialog,
  ShortcurtsListDialog,
  LettrageDialog,
  FECDialog,
  AbstractDialog,
  WorkingPageDialog,
  NewPasswordDialog,
  AccountParameters,
  ConnectorDialog,
  FlagDialog,
  DiaryDialog,
  FunctionDialog,
  PortfolioDialog,
  CguDialog,
  BISettingsDeleteDialog,
  BISettingsDialog,
  WorksheetsDialog,
  AccessDashboardSociety,
  ReportsAndFormsDialog,
  RAFConfirmDialog,
  WorksheetsDeleteDialog,
  AssociateDeleteDialog,
  SocietyPPDeleteDialog,
  EmployeePPDeleteDialog,
  SubsidiaryDeleteDialog,
  FormsAndReportsDeleteDialog,
  NewLetterDialog,
  DeleteConfirmation,
  SettingStandardMailDialog,
  MailParagraphDialog,
  SettingStandardMailDeleteDialog,
  MailParagraphDeleteDialog,
  InformationDialog,
  SuccessfulDialog,
  AccountingFirmDeclareTableDialog,
  PlanDialog,
  PlanDetailDialog,
  SaleDialog,
  PlaquetteTree,
  CopyDocumentToDiscussion,
  ODDialog,
  EarlyStartDateModal,
  LinkAlreadySent
};
