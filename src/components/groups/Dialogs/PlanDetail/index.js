import React from 'react';
import PropTypes from 'prop-types';

import { Typography, Dialog } from '@material-ui/core';
import { PlanDetailForm } from 'components/groups/Forms';
import { InlineButton } from 'components/basics/Buttons';

import I18n from 'assets/I18n';
import styles from './PlanDetailDialog.module.scss';

const PlanDetailDialog = (props) => {
  const {
    isOpen,
    onClose,
    onValidate,
    modify,
    error
  } = props;

  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      onValidate={onValidate}
    >
      <div className={styles.container}>
        <div className={styles.title}>
          <Typography variant="h1">{modify ? I18n.t('accountingPlans.dialogs.planDetail.modify') : I18n.t('accountingPlans.dialogs.planDetail.title') }</Typography>
        </div>
        {error && <Typography variant="h6" color="error">{error}</Typography>}
        <PlanDetailForm />
        <div className={styles.buttonContainer}>
          <InlineButton
            buttons={[{
              _type: 'string',
              text: I18n.t('accountingPlans.dialogs.planDetail.cancel'),
              color: 'default',
              onClick: onClose,
              size: 'large'
            }, {
              _type: 'string',
              text: I18n.t('accountingPlans.dialogs.planDetail.save'),
              size: 'large',
              onClick: onValidate
            }]}
          />
        </div>
      </div>
    </Dialog>
  );
};

PlanDetailDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  modify: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onValidate: PropTypes.func.isRequired,
  error: PropTypes.string
};

PlanDetailDialog.defaultProps = {
  error: ''
};

export default PlanDetailDialog;
