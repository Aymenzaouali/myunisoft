import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray } from 'redux-form';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import {
  IconButton,
  Dialog, DialogTitle, DialogContent, DialogActions,
  Typography
} from '@material-ui/core';
import AddBoxIcon from '@material-ui/icons/AddBox';
import I18n from 'assets/I18n';

import Button from 'components/basics/Buttons/Button';
import FontIcon from 'components/basics/Icon/Font';

import { roundNumber } from 'helpers/number';
import styles from './FixedAssetsModal.module.scss';

const ImmoLine = ({
  fields, onChange, defaultImmos, isValide
}) => {
  const onCalculateAmountMemo = useCallback(() => (onChange()), [onChange]);
  useEffect(() => {
    onCalculateAmountMemo();
  }, [fields.length]);

  const push = () => fields.push({
    ...defaultImmos,
    purchase_value: undefined,
    label: ''
  });
  const remove = ({ index }) => {
    fields.remove(index);
    onCalculateAmountMemo();
  };
  const isLastRow = index => (index === fields.length - 1);
  return (
    <div className={styles.body}>
      {
        fields.map((immo, index) => (
          <div key={index} className={styles.line}>
            <div className={styles.fields}>
              <Field
                label={`${I18n.t('fixedAssets.modal.form.amount')} ${index + 1}`}
                type="number"
                format={value => roundNumber(value)}
                disabled={index === 0}
                onBlur={event => onChange(event)}
                component={ReduxTextField}
                name={`${immo}.purchase_value`}
              />
              <Field
                label={I18n.t('fixedAssets.modal.form.label')}
                component={ReduxTextField}
                name={`${immo}.label`}
              />
            </div>
            {
              (index !== 0 && (!isLastRow(index) || isValide)) ? (
                <IconButton
                  onClick={() => ((isLastRow(index))
                    ? push()
                    : remove({ immo, index }))}
                >
                  {
                    (isLastRow(index))
                      ? (
                        <AddBoxIcon
                          color="primary"
                        />
                      )
                      : (
                        <FontIcon
                          name="icon-trash"
                          color="red"
                        />
                      )
                  }
                </IconButton>
              ) : null
            }
          </div>
        ))
      }
    </div>
  );
};

// Component
const FixedAssetsModal = (props) => {
  const {
    open, onClose, onConfirm, onCalculateAmount, isValide,
    defaultImmos, isNegative, isFocusForm
  } = props;


  // functions
  // Rendering
  return (
    <Dialog classes={{ paper: styles.dialog }} open={open} onClose={onClose}>
      <DialogTitle classes={{ root: styles.header }} disableTypography>
        <Typography
          classes={{ root: styles.headerTitle }}
          variant="h1"
        >
          {I18n.t('fixedAssets.modal.headerTitle')}
        </Typography>
        <IconButton classes={{ root: styles.closeBtn }} onClick={onClose}>
          <FontIcon name="icon-close" />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <FieldArray
          defaultImmos={defaultImmos}
          onChange={onCalculateAmount}
          name="immos"
          isValide={isValide}
          component={ImmoLine}
        />
      </DialogContent>
      {
        !isValide && (
          <Typography
            classes={{ root: styles.validator }}
            variant="h1"
            color="error"
          >
            {I18n.t('fixedAssets.modal.validText')}
          </Typography>
        )
      }
      {
        isNegative && (
          <Typography
            classes={{ root: styles.validator }}
            variant="h1"
            color="error"
          >
            {I18n.t('fixedAssets.modal.negativeValueText')}
          </Typography>
        )
      }
      <DialogActions classes={{ root: styles.actions }}>
        <Button variant="outlined" onClick={onClose}>{I18n.t('fixedAssets.modal.cancel')}</Button>
        <Button
          variant="contained"
          disabled={!isValide || isNegative || isFocusForm}
          color="primary"
          classess={{ root: styles.actions.submit }}
          onClick={() => onConfirm()}
        >
          {I18n.t('fixedAssets.modal.submit')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

// Props
FixedAssetsModal.propTypes = {
  isValide: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  defaultImmos: PropTypes.object.isRequired,
  onCalculateAmount: PropTypes.func.isRequired,
  isNegative: PropTypes.bool.isRequired,
  isFocusForm: PropTypes.bool.isRequired
};

ImmoLine.propTypes = {
  fields: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  defaultImmos: PropTypes.func.isRequired,
  isValide: PropTypes.bool.isRequired
};

export default FixedAssetsModal;
