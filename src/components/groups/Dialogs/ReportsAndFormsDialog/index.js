import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import { Field } from 'redux-form';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import { ReduxTextField, ReduxMaterialRangeDatePicker } from 'components/reduxForm/Inputs';
import { ReduxSelect } from 'components/reduxForm/Selections';
import ReduxAutoComplete from 'components/reduxForm/Inputs/AutoComplete';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import { StateOrFormDialogTable, RAFAlreadySelected } from 'containers/groups/Tables';
import { RAFConfirmDialog } from 'containers/groups/Dialogs';
import RAFCreateStateContainer from 'containers/groups/ReportAndForms/RAFCreateStateContainer';

import styles from './reportsAndFormsDialog.module.scss';

const ReportsAndFormsDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    handleSubmit,
    onPostReportAndForm,
    reset,
    societies,
    getSociety,
    stateSelection,
    getStateOrFormDialogTable,
    onAcceptConfirmDialog,
    stateOrForm,
    selectedStateOrForm,
    selectedRowsCount,
    change,
    createdStatesCount,
    society_id,
    resetCreatedStates
  } = props;

  const [isConfirmDialogOpen, setConfirmDialogOpen] = useState(false);
  const [isSelectedTableVisible, setSelectedTableVisablity] = useState(false);
  const [selectedStateOrFormId, setSelectedStateOrForm] = useState(false);
  const [isAvailableForms, setDisplayAvailableForms] = useState(false);
  const [isFromNewStateTable, setDisplayFromNewStateTable] = useState(false);

  const closeDialog = () => {
    const { onClose, resetSelectedStateOrFormData } = props;
    onClose();
    resetSelectedStateOrFormData();
    setSelectedTableVisablity(false);
    setSelectedStateOrForm(false);
    setDisplayAvailableForms(false);
    setDisplayFromNewStateTable(false);
    resetCreatedStates(society_id);
    reset();
  };

  const closeConfirmation = () => {
    closeDialog();
    setConfirmDialogOpen(false);
  };

  const handleDataSelection = (event) => {
    if (event.target.value === 0) {
      setDisplayAvailableForms(true);
      setDisplayFromNewStateTable(false);
    }
    if (event.target.value === 1) {
      setDisplayAvailableForms(false);
      setSelectedTableVisablity(false);
      setSelectedStateOrForm(null);
      setDisplayFromNewStateTable(true);
    }
  };

  const handleConfirmDialog = () => {
    change('selectStateOrForm', null);
    onAcceptConfirmDialog(selectedStateOrFormId);
    setSelectedStateOrForm(false);
    setSelectedTableVisablity(true);
    setConfirmDialogOpen(false);
  };

  useEffect(() => {
    getSociety();
  }, []);

  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="lg"
    >
      <DialogActions>
        <Button onClick={closeDialog}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h6" classes={{ root: classes.title }}>
          {title}
        </Typography>
      </DialogContent>
      <form>
        <DialogContent classes={{ root: classes.body }}>
          <div className={styles.row}>
            <Field
              color="primary"
              name="type"
              component={ReduxSelect}
              margin="none"
              list={[{ label: 'type', value: 'type' }, { label: 'type2', value: 'type2' }]}
              label={I18n.t('reportsAndForms.table.type')}
              className={classes.selectField}
            />
          </div>
          <div className={styles.rowOfFields}>
            <Field
              color="primary"
              name="name"
              component={ReduxTextField}
              label={I18n.t('reportsAndForms.dialog.name')}
              margin="none"
              className={classes.formField}
            />
            <Field
              color="primary"
              name="decription"
              component={ReduxTextField}
              label={I18n.t('reportsAndForms.table.description')}
              margin="none"
              className={classes.formField}
            />
            <Field
              name="fiscalYear"
              component={ReduxMaterialRangeDatePicker}
              label={I18n.t('reportsAndForms.table.fiscalYear')}
            />
          </div>
          <div className={styles.row}>
            <Field
              name="enterprises"
              component={ReduxAutoComplete}
              options={societies}
              isMulti
              menuPosition="fixed"
              placeholder=""
              textFieldProps={{
                label: I18n.t('reportsAndForms.table.enterprises'),
                InputLabelProps: {
                  shrink: true
                }
              }}
              closeMenuOnSelect={false}
            />
          </div>
          <div className={styles.row}>
            <Field
              name="selectData"
              component={ReduxSelect}
              label={I18n.t('reportsAndForms.dialog.selectData')}
              list={stateSelection}
              className={styles.selectData}
              onChange={handleDataSelection}
              margin="none"
              shrink
            />
          </div>
          {
            isAvailableForms && (
              <div className={styles.row}>
                <Field
                  name="selectStateOrForm"
                  component={ReduxSelect}
                  label={I18n.t('reportsAndForms.dialog.selectStateOrForm')}
                  list={stateOrForm}
                  className={styles.selectData}
                  onChange={(event) => {
                    setSelectedStateOrForm(event.target.value);
                    getStateOrFormDialogTable(event.target.value);
                  }}
                  margin="none"
                  shrink
                />
              </div>
            )
          }
          <div>
            {
              !!selectedStateOrFormId && (
                <StateOrFormDialogTable />
              )
            }
            {
              ((isSelectedTableVisible || !_.isEmpty(selectedStateOrForm)) && !isFromNewStateTable)
              && (
                <>
                  <span className={styles.alreadySelectedTitle}>{I18n.t('reportsAndForms.alreadySelectedTable.title')}</span>
                  <RAFAlreadySelected />
                </>
              )
            }
            {
              isFromNewStateTable && <RAFCreateStateContainer />
            }
          </div>
        </DialogContent>
        <DialogActions classes={{ root: classes.footer }}>
          <InlineButton
            buttons={[
              {
                _type: 'string',
                text: I18n.t('reportsAndForms.dialog.cancel'),
                color: 'default',
                variant: 'outlined',
                size: 'medium',
                style: { marginLeft: 0 },
                onClick: closeDialog
              },
              {
                _type: 'string',
                variant: 'contained',
                disabled: (_.isEmpty(selectedStateOrForm)
                  && !selectedRowsCount && !isFromNewStateTable)
                  || (!createdStatesCount && !selectedStateOrFormId),
                onClick: () => {
                  if (isAvailableForms) {
                    setConfirmDialogOpen(true);
                  } else {
                    handleSubmit(onPostReportAndForm);
                  }
                },
                text: I18n.t('reportsAndForms.dialog.ok'),
                size: 'medium'
              }]
            }
          />
        </DialogActions>
        {isConfirmDialogOpen && (
          <RAFConfirmDialog
            isOpen={isConfirmDialogOpen}
            onValidate={handleConfirmDialog}
            onClose={closeConfirmation}
            isCreation
          />
        )}
      </form>
    </Dialog>
  );
};

const ownStyles = {
  content: {
    display: 'contents'
  },
  footer: {
    margin: '0 50px 28px 50px',
    justifyContent: 'flex-start'
  },
  body: {
    paddingLeft: 50,
    paddingRight: 50,
    overflowY: 'inherit'
  },
  title: {
    marginLeft: 50,
    marginTop: '-40px',
    fontSize: '1.5rem'
  },
  formField: {
    width: '100%'
  },
  selectField: {
    width: 300
  }
};

ReportsAndFormsDialog.defaultProps = {
  onClose: () => {}
};

ReportsAndFormsDialog.propTypes = {
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onAcceptConfirmDialog: PropTypes.func.isRequired,
  onPostReportAndForm: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  resetCreatedStates: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired,
  getStateOrFormDialogTable: PropTypes.func.isRequired,
  getSociety: PropTypes.func.isRequired,
  selectedStateOrForm: PropTypes.shape({}).isRequired,
  resetSelectedStateOrFormData: PropTypes.func.isRequired,
  selectedRowsCount: PropTypes.number.isRequired,
  society_id: PropTypes.number.isRequired,
  createdStatesCount: PropTypes.number.isRequired,
  stateOrForm: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  stateSelection: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  societies: PropTypes.arrayOf(PropTypes.shape({
    ape: PropTypes.string,
    city: PropTypes.string,
    companyType: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string,
    siret: PropTypes.string,
    society_id: PropTypes.number,
    status: PropTypes.string,
    step: PropTypes.string,
    value: PropTypes.number
  })).isRequired
};

export default withStyles(ownStyles)(ReportsAndFormsDialog);
