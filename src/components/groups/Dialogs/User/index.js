import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import Button from 'components/basics/Buttons/Button';
import { ReduxRadioCard } from 'components/reduxForm/Selections';
import { FontIcon } from 'components/basics/Icon';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';
import styles from './dialogs.user.module.scss';

const UserDialog = (props) => {
  const {
    isOpen,
    onClose,
    UserDialogTitle,
    onValidate,
    list
  } = props;

  return (
    <Dialog open={isOpen} onClose={onClose}>
      <div className={styles.dialogUser}>
        <div className={styles.headContainer}>
          <FontIcon size={50} color="#bd10e0" name="icon-alert" />
          <div className={styles.dialogTitle}>{UserDialogTitle}</div>
        </div>
        <form>
          <div className={styles.formContainer}>
            <Field
              name="personne_physique_id"
              color="primary"
              component={ReduxRadioCard}
              list={list}
            />
          </div>
          <div className={styles.buttonsContainer}>
            <Button size="large" variant="contained" onClick={onClose}>{I18n.t('existingPerson.cancel')}</Button>
            <div className={styles.buttonsSeparator} />
            <Button size="large" variant="contained" color="primary" onClick={onValidate}>{I18n.t('existingPerson.validate')}</Button>
          </div>
        </form>
      </div>
    </Dialog>
  );
};

UserDialog.defaultProps = {
  list: []
};

UserDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  UserDialogTitle: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  list: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    name: PropTypes.string
  }))
};

export default UserDialog;
