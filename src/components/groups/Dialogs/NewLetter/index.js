import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography, Dialog, DialogContent, Button
} from '@material-ui/core';
import ReduxRadio from 'components/reduxForm/Selections/ReduxRadio';
import { InlineButton } from 'components/basics/Buttons';
import { Field } from 'redux-form';
import { FontIcon } from 'components/basics/Icon';
import I18n from 'assets/I18n';
import styles from './newLetterDialogs.module.scss';

const types = [{
  value: 'cabinet',
  label: 'Cabinet'
},
{
  value: 'client',
  label: 'Client'
}];

const NewLetterDialog = (props) => {
  const {
    isOpen,
    onValidate = () => {},
    onClose = () => {}
  } = props;


  const handleValidate = () => {
    onValidate();
  };

  const handleClose = () => {
    onClose();
  };

  return (
    <Dialog
      open={isOpen}
      onClose={handleClose}
      PaperProps={{
        style: { height: 242, width: 604 }
      }}
    >
      <DialogContent>
        <div className={styles.header}>
          <Typography variant="h1">{I18n.t('letters.dialog.newLetter')}</Typography>
          <Button onClick={handleClose}>
            <FontIcon
              size={24}
              className="icon-close"
            />
          </Button>
        </div>
        <div className={styles.content}>
          <div>
            <Typography variat="body1">{I18n.t('letters.dialog.chooseHeader')}</Typography>
          </div>
          <Field
            name="type"
            list={types}
            row
            color="primary"
            component={ReduxRadio}
          />
          <div className={styles.footer}>
            <InlineButton
              buttons={[
                {
                  _type: 'string',
                  text: I18n.t('common.cancel'),
                  color: 'default',
                  variant: 'outlined',
                  size: 'medium',
                  onClick: handleClose
                },
                {
                  _type: 'string',
                  text: I18n.t('common.create'),
                  size: 'medium',
                  onClick: handleValidate
                }
              ]}
            />
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
};

NewLetterDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onValidate: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default NewLetterDialog;
