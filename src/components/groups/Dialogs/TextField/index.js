import React from 'react';
import PropTypes from 'prop-types';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';
import { Dialog, withStyles } from '@material-ui/core/';
import Button from 'components/basics/Buttons/Button';
import './styles.scss';

const themeStyles = () => ({
  comment: {
    width: 400
  }
});

const TextFieldDialog = (props) => {
  const {
    classes,
    isOpen,
    onCloseModal,
    onValidate,
    name,
    titleDialog,
    label
  } = props;

  return (
    <Dialog open={isOpen} onClose={onCloseModal}>
      <div className="textFieldDialog">
        <div className="textFieldDialog__title">
          {titleDialog}
        </div>
        <div className="textFieldDialog__dialog">
          <Field
            color="primary"
            name={name}
            multiline
            component={ReduxTextField}
            label={label}
            type="text"
            autoFocus
            className={classes.comment}
          />
        </div>

        <div className="textFieldDialog__button">
          <div className="textFieldDialog__buttonCancel">
            <Button
              size="large"
              variant="contained"
              color="error"
              onClick={onCloseModal}
            >
              {I18n.t('textFieldDialog.cancelled')}
            </Button>
          </div>
          <div className="textFieldDialog__buttonValidate">
            <Button
              size="large"
              variant="contained"
              color="primary"
              onClick={onValidate}
            >
              {I18n.t('textFieldDialog.validate')}
            </Button>
          </div>

        </div>
      </div>
    </Dialog>
  );
};

TextFieldDialog.defaultProps = {
  name: '',
  label: '',
  classes: {}
};

TextFieldDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onCloseModal: PropTypes.func.isRequired,
  onValidate: PropTypes.func.isRequired,
  name: PropTypes.string,
  titleDialog: PropTypes.string.isRequired,
  label: PropTypes.string,
  classes: PropTypes.shape({})
};

export default withStyles(themeStyles)(TextFieldDialog);
