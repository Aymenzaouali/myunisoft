import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import {
  withStyles,
  Typography,
  IconButton,
  Dialog, DialogActions, DialogContent, DialogTitle
} from '@material-ui/core';

import I18n from 'assets/I18n';

import { formatStringNumber } from 'helpers/number';

import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';

import { DiaryAutoComplete, AccountAutoComplete } from 'containers/reduxForm/Inputs';

import styles from './odDialog.module.scss';

const ODDialog = (props) => {
  const {
    classes,
    form,
    isOpen,
    onClose,
    onValidate,
    initODForm,
    changeODForm,
    selectedAccount,
    notLetter,
    ODvalues: {
      amount_0, amount_1
    }
  } = props;

  useEffect(() => {
    initODForm();
  }, [selectedAccount, notLetter]);

  const calculateAmount = async (index, event) => {
    const { target: { value } } = event;
    const formattedValue = formatStringNumber(value);
    const formatCurrentField = index => changeODForm(`amount_${index}`, formattedValue);

    if (index === 0) {
      await changeODForm('amount_1', (+notLetter - formattedValue));
      formatCurrentField(index);
    } else if (index === 1 && amount_0) {
      await changeODForm('amount_2', (+notLetter - (formatStringNumber(amount_0) + formattedValue)));
      formatCurrentField(index);
    } else if (index === 2 && amount_1) {
      await changeODForm('amount_0', (+notLetter - (formatStringNumber(amount_1) + formattedValue)));
      formatCurrentField(index);
    }
  };

  const renderAccountRows = () => (
    [0, 1, 2].map(index => (
      <div key={index} className={styles.accountRow}>
        <Field
          color="primary"
          name={`account_id_${index}`}
          component={AccountAutoComplete}
          className={classes.accountField}
          label={I18n.t('consulting.OD_dialog.account')}
        />
        <Field
          component={ReduxTextField}
          className={styles.amountField}
          name={`amount_${index}`}
          label={I18n.t('consulting.OD_dialog.amount')}
          onBlur={event => calculateAmount(index, event)}
        />
      </div>
    )));

  return (
    <Dialog
      classes={{ paper: styles.dialog }}
      open={isOpen}
      onClose={onClose}
      fullWidth
      maxWidth="sm"
    >
      <DialogTitle classes={{ root: styles.title }} disableTypography>
        <Typography variant="h1">
          {I18n.t('consulting.OD_dialog.title')}
        </Typography>
        <IconButton classes={{ root: styles.close }} onClick={onClose}>
          <FontIcon className="icon-close" />
        </IconButton>
      </DialogTitle>
      <DialogContent classes={{ root: styles.form }}>
        <form id={form} onSubmit={onValidate}>
          <div className={styles.dateAndDaryRow}>
            <Field
              component={ReduxMaterialDatePicker}
              label={I18n.t('consulting.OD_dialog.date')}
              name="date"
            />
            <Field
              name="diary"
              label={I18n.t('consulting.OD_dialog.diary')}
              component={DiaryAutoComplete}
              className={styles.diaryField}
            />
          </div>
          {renderAccountRows()}
          <div className={styles.totalAmountRow}>
            <Field
              component={ReduxTextField}
              disabled
              name="amount_total"
              label={I18n.t('consulting.OD_dialog.total_amount')}
            />
          </div>
        </form>
      </DialogContent>
      <DialogActions classes={{ root: styles.footer }}>
        <Button
          form={form}
          type="button"
          variant="outlined"
          onClick={onClose}
        >
          {I18n.t('consulting.OD_dialog.cancel')}
        </Button>
        <Button variant="contained" color="secondary" form={form} type="submit">
          {I18n.t('consulting.OD_dialog.validate')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ODDialog.propTypes = {
  form: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  initODForm: PropTypes.func.isRequired,
  changeODForm: PropTypes.func.isRequired,
  selectedAccount: PropTypes.object.isRequired,
  notLetter: PropTypes.number.isRequired,
  ODvalues: PropTypes.object.isRequired
};

const ownStyles = () => ({
  accountField: {
    width: 250
  }
});

export default withStyles(ownStyles)(ODDialog);
