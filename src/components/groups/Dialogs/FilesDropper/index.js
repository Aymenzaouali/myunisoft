/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import {
  Button, IconButton, Link, Typography,
  Dialog, DialogContent, DialogActions,
  List, ListItem, ListItemText, ListItemSecondaryAction, Divider
} from '@material-ui/core';
import Dropzone from 'react-dropzone';
import { Trans } from 'react-i18next';

import I18n from 'assets/I18n';

import { FontIcon } from 'components/basics/Icon';

import styles from './FilesDropper.module.scss';

// Component
const FilesDropperDialog = (props) => {
  const {
    open, onClose,
    files: initials, onAddFiles
  } = props;

  // State
  const [files, setFiles] = useState(initials);

  // Functions
  const handleScan = () => {
    console.log('scan !'); // TODO: scan !
  };
  const handleRemove = (file) => {
    setFiles(old => old.filter(f => f !== file));
  };

  const handleClose = () => {
    setFiles([]);
    onClose();
  };
  const handleDrop = (files) => {
    setFiles(old => [...old, ...files]);
  };
  const handleAddFiles = () => {
    if (onAddFiles) onAddFiles(files);
    handleClose();
  };

  // Effects
  useEffect(() => {
    if (open) setFiles(initials);
  }, [open, initials]);

  // Rendering
  return (
    <Dialog maxWidth={false} open={open} onClose={handleClose}>
      <DialogContent classes={{ root: styles.dialog }}>
        <Dropzone className={styles.dropzone} disableClick onDrop={handleDrop}>
          { ({ open }) => (
            <Fragment>
              <Typography classes={{ root: styles.title }} variant="h1">{ I18n.t('filesDropperDialog.dropbox') }</Typography>
              <FontIcon className={styles.icon} name="icon-upload" />
              <Typography>
                <Trans i18nKey="filesDropperDialog.import">
                  <Link component="button" color="primary" variant="body2" onClick={open}>importer</Link>
                  depuis vos fichiers
                </Trans>
              </Typography>
              <Typography>
                <Trans i18nKey="filesDropperDialog.scan">
                  <Link color="secondary" variant="body2" onClick={handleScan}>scanner</Link>
                  (en 300 DPI)
                </Trans>
              </Typography>
            </Fragment>
          ) }
        </Dropzone>

        { files.length > 0 && (
          <Fragment>
            <Divider />
            <List classes={{ root: styles.files }} disablePadding>
              { files.map((file, i) => (
                <Fragment key={i}>
                  { i !== 0 && <Divider component="hr" /> }
                  <ListItem>
                    <ListItemText>{ file.name }</ListItemText>
                    <ListItemSecondaryAction>
                      <IconButton
                        classes={{ label: styles.removeBtn }}
                        onClick={() => handleRemove(file)}
                      >
                        <FontIcon color="inherit" name="icon-trash" />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                </Fragment>
              )) }
            </List>
            <Divider />
          </Fragment>
        ) }
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={handleClose}>{ I18n.t('common.cancel') }</Button>
        <Button
          variant="contained"
          color="primary"
          onClick={handleAddFiles}
        >
          { I18n.t('common.add') }
        </Button>
      </DialogActions>
    </Dialog>
  );
};

// Props
FilesDropperDialog.propTypes = {
  open: PropTypes.bool,
  files: PropTypes.arrayOf(PropTypes.instanceOf(File)),
  onClose: PropTypes.func,
  onAddFiles: PropTypes.func
};

FilesDropperDialog.defaultProps = {
  open: false,
  files: [],
  onClose: () => {},
  onAddFiles: () => {}
};

export default FilesDropperDialog;
