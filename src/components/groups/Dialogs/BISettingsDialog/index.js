import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import { Field, change } from 'redux-form';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import ReduxAutoComplete from 'components/reduxForm/Inputs/AutoComplete';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import { AccountAutoComplete } from 'containers/reduxForm/Inputs';
import ReduxCheckBox from 'components/reduxForm/Selections/ReduxCheckBox';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import classNames from 'classnames';
import styles from './BISettingsDialog.module.scss';

const BISettingsDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    initFieldAccount,
    interbank_codes,
    isAllCodesSelected,
    onClose,
    creationMode,
    onValidate,
    submitting,
    pristine,
    initCurrentAccount,
    dispatch,
    invalid,
    getInterbankCodes,
    params,
    currentParam,
    getVat,
    tvaCodes,
    accounts
  } = props;

  const accountNumberBegin = '627';

  const paramsBlock = {
    none: {
      components: []
    },
    calculateTVA: {
      components: [
        {
          name: 'calculateTVA',
          component: ReduxAutoComplete,
          options: tvaCodes && tvaCodes.map(code => ({ label: code.code, value: code }))
        }
      ]
    },
    tvaAndComm: {
      components: [
        {
          component: ReduxTextField,
          name: 'pourcentage',
          className: '',
          type: 'number',
          label: I18n.t('bankIntegrationSettings.popUp.percentage')
        },
        {
          component: ReduxAutoComplete,
          name: 'commision',
          options: accounts.filter(
            account => account.account_number.includes(accountNumberBegin)
          ).map(
            account => ({
              label: `${account.account_number} ${account.label}`,
              value: account
            })
          ),
          label: I18n.t('bankIntegrationSettings.popUp.commission')
        },
        {
          component: ReduxAutoComplete,
          name: 'tvaCode',
          options: tvaCodes && tvaCodes.map(code => ({ label: code.code, value: code })),
          label: I18n.t('bankIntegrationSettings.popUp.tvaCode')
        }
      ]
    }
  };

  const [isAllCodes, setAllCodes] = useState(!creationMode ? isAllCodesSelected : true);

  async function getList() {
    await getInterbankCodes();
  }

  useEffect(() => {
    initFieldAccount();
    getVat();
    if (creationMode) initCurrentAccount({ allCodes: true });
    getList();
  }, []);

  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h6" classes={{ root: classes.title }}>
          {title}
        </Typography>
      </DialogContent>
      <form>
        <DialogContent classes={{ root: classes.body }}>
          <div className={styles.row}>
            <Field
              color="primary"
              name="label_to_find"
              component={ReduxTextField}
              label={I18n.t('bankIntegrationSettings.table.keyword')}
              margin="none"
              style={{ width: '100%' }}
            />
          </div>
          <div className={styles.row}>
            <Field
              color="primary"
              name="account_id"
              component={AccountAutoComplete}
              label={I18n.t('bankIntegrationSettings.table.accountNumber')}
            />
          </div>
          <div className={styles.row}>
            <Field
              color="primary"
              name="forced_label"
              component={ReduxTextField}
              label={I18n.t('bankIntegrationSettings.table.name')}
              margin="none"
              style={{ width: '100%' }}
            />
            <Typography>{I18n.t('bankIntegrationSettings.popUp.info')}</Typography>
          </div>
          <div className={classNames(styles.row, styles.row_ib)}>
            <Field
              color="primary"
              name="code_interbancaire"
              component={ReduxAutoComplete}
              isMulti
              disabled={isAllCodes}
              menuPosition="fixed"
              label={I18n.t('bankIntegrationSettings.table.interbankCode')}
              options={interbank_codes}
              margin="none"
              className={isAllCodes ? classNames(styles.menuItems_disabled, styles.menuItems)
                : styles.menuItems}
            />
            <Field
              color="primary"
              name="allCodes"
              component={ReduxCheckBox}
              checked={isAllCodes}
              onChange={(val) => {
                dispatch(change('BISettingsForm', 'code_interbancaire', null));
                setAllCodes(val);
              }}
              label={I18n.t('bankIntegrationSettings.popUp.all')}
              margin="none"
            />
          </div>
          <div className={styles.row}>
            <Field
              color="primary"
              name="debit"
              component={ReduxCheckBox}
              label={I18n.t('bankIntegrationSettings.popUp.debit')}
              margin="none"
            />
            <Field
              color="primary"
              name="credit"
              component={ReduxCheckBox}
              label={I18n.t('bankIntegrationSettings.popUp.credit')}
              margin="none"
            />
          </div>
          <div className={styles.row}>
            <Field
              color="primary"
              name="param"
              component={ReduxAutoComplete}
              options={params}
              label={I18n.t('bankIntegrationSettings.popUp.debit')}
              margin="none"
            />
          </div>
          {
            currentParam && currentParam.param
              ? (
                <div className={classNames(styles.row, styles.paramsBlock)}>
                  {
                    paramsBlock[currentParam.param.value].components.map(
                      component => (<div className={styles.param}><Field {...component} /></div>)
                    )
                  }
                </div>
              )
              : null
          }
        </DialogContent>
        <DialogActions classes={{ root: classes.footer }}>
          <InlineButton
            buttons={[
              {
                _type: 'string',
                text: I18n.t('bankIntegrationSettings.popUp.cancel'),
                color: 'default',
                variant: 'outlined',
                size: 'medium',
                disabled: pristine || submitting,
                style: { marginLeft: 0 },
                onClick: onClose
              },
              {
                _type: 'string',
                variant: 'contained',
                onClick: onValidate,
                disabled: invalid || submitting,
                text: I18n.t('bankIntegrationSettings.popUp.ok'),
                size: 'medium'
              }]
            }
          />
        </DialogActions>
      </form>
    </Dialog>
  );
};

const ownStyles = {
  content: {
    display: 'contents'
  },
  footer: {
    margin: '0 50px 28px 50px',
    justifyContent: 'flex-start'
  },
  body: {
    paddingLeft: 50,
    paddingRight: 50,
    overflowY: 'inherit'
  },
  title: {
    marginLeft: 50,
    marginTop: '-40px',
    paddingRight: 70,
    fontSize: '1.5rem'
  },
  menu: {
    backgroundColor: 'red',
    color: 'yellow'
  },
  select: {
    width: 100
  }
};

BISettingsDialog.defaultProps = {
  onClose: () => {}
};

BISettingsDialog.propTypes = {
  initFieldAccount: PropTypes.func.isRequired,
  creationMode: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  isAllCodesSelected: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  interbank_codes: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired,
  initCurrentAccount: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  getInterbankCodes: PropTypes.func.isRequired,
  params: PropTypes.array.isRequired,
  currentParam: PropTypes.object.isRequired,
  tvaCodes: PropTypes.array.isRequired,
  getVat: PropTypes.func.isRequired,
  accounts: PropTypes.array.isRequired
};

export default withStyles(ownStyles)(BISettingsDialog);
