import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogActions,
  DialogContent
} from '@material-ui/core';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import styles from './workingPage.module.scss';

const onDeleteClick = (deleteBills, society_id, selectedBills, onClose) => async () => {
  await deleteBills(society_id, selectedBills);
  onClose();
};

const WorkingPageDialog = (props) => {
  const {
    isOpen,
    type,
    onClose,
    deleteBills,
    society_id,
    selectedBills,
    formValues
  } = props;

  return (
    <Dialog
      fullWidth
      aria-labelledby="remove_fnp"
      open={isOpen}
      onClose={onClose}
    >
      {type === 'remove' && (
        <div className={styles.container}>
          <DialogActions className={styles.close}>
            <Button onClick={onClose}>
              <FontIcon size={30} className="icon-close" titleInfoBulle={I18n.t('tooltips.close')} />
            </Button>
          </DialogActions>

          <DialogContent>
            <div className={styles.header}>
              <FontIcon name="icon-alert" size={44} color="#fe3a5e" />
              <div>{I18n.t('fnp.dialogs.title')}</div>
            </div>
            <div className={styles.textContainer}>
              <div>{I18n.t('fnp.dialogs.removeModalText1')}</div>
              <div>{I18n.t('fnp.dialogs.removeModalText2')}</div>
            </div>
          </DialogContent>

          <DialogActions>
            <Button variant="outlined" onClick={onClose}>
              {I18n.t('fnp.dialogs.cancel')}
            </Button>
            <Button variant="contained" color="error" onClick={onDeleteClick(deleteBills, society_id, selectedBills, onClose, formValues)}>
              {I18n.t('fnp.dialogs.removeSelection')}
            </Button>
          </DialogActions>
        </div>
      )}
      {type === 'edit' && (
        <div className={styles.container}>
          <DialogActions className={styles.close}>
            <Button onClick={onClose}>
              <FontIcon size={30} className="icon-close" titleInfoBulle={I18n.t('tooltips.close')} />
            </Button>
          </DialogActions>

          <DialogContent>
            <div className={styles.header}>
              <div>{I18n.t('fnp.dialogs.title')}</div>
            </div>
            <div className={styles.textContainer}>
              <div>{I18n.t('fnp.dialogs.editModalText1')}</div>
              <div>{I18n.t('fnp.dialogs.editModalText2')}</div>
            </div>
          </DialogContent>

          <DialogActions>
            <Button variant="outlined" onClick={onClose}>
              {I18n.t('fnp.dialogs.ignore')}
            </Button>
            <Button variant="contained" color="primary">
              {I18n.t('fnp.dialogs.disable')}
            </Button>
          </DialogActions>
        </div>
      )}
    </Dialog>
  );
};

WorkingPageDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  deleteBills: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  society_id: PropTypes.string.isRequired,
  selectedBills: PropTypes.string.isRequired,
  formValues: PropTypes.string.isRequired
};

export default WorkingPageDialog;
