import React from 'react';
import {
  Dialog,
  DialogContent
} from '@material-ui/core';
import PropTypes from 'prop-types';
import Calculator from 'components/basics/Calculator';
import { withShortcut } from 'hoc/dialogShortcut';

const CalculatorDialog = (props) => {
  const {
    onValidate = () => {},
    onClose = () => {},
    isOpen,
    ...calculatorProps
  } = props;

  return (
    <Dialog open={isOpen} onClose={onClose} fullWidth>
      <DialogContent>
        <Calculator
          onValidate={onValidate}
          {...calculatorProps}
        />
      </DialogContent>
    </Dialog>
  );
};


CalculatorDialog.defaultProps = {
  onValidate: () => {},
  onClose: () => {},
  isOpen: false
};

CalculatorDialog.propTypes = {
  onValidate: PropTypes.func,
  onClose: PropTypes.func,
  isOpen: PropTypes.bool
};

export default withShortcut()(CalculatorDialog);
