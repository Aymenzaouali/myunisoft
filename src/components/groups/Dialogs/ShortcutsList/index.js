import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog, DialogContent,
  Typography, List, ListItem, withStyles
} from '@material-ui/core';
import I18n from 'assets/I18n';
import { routesByKey } from 'helpers/routes';
import Button from 'components/basics/Buttons/Button';

const styles = {
  paper: {
    width: '700px',
    maxHeight: '90%'
  },
  root: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  dialogTitle: {
    display: 'flex',
    flexDirection: 'row',
    height: '80px',
    justifyContent: 'center'
  },
  title: {
    marginBottom: '40px',
    marginTop: '20px'
  },
  closeButton: {
    height: '40px',
    width: '40px'
  },
  combination: {
    width: '90px',
    fontFamily: 'basier_circlesemibold',
    fontSize: '16px',
    color: '#000',
    marginRight: '60px'
  },
  name: {
    width: '282px',
    fontFamily: 'basier_circleregular',
    fontSize: '14px',
    color: '#000',
    justifyContent: 'center'
  },
  button: {
    display: 'flex',
    flexDirection: 'row',
    height: '32px',
    justifyContent: 'center',
    marginBottom: '40px'
  }
};


const ShortcutsList = (props) => {
  const {
    isOpen,
    onClose,
    classes,
    shortcutsList,
    push,
    hiddenShortCuts,
    nameShortcut
  } = props;

  const {
    shortcuts,
    name
  } = shortcutsList;

  const renderNoData = (
    <div className={classes.dialogTitle}>
      <Typography classes={{ root: classes.title }} variant="h1">
        {I18n.t('tables.noData')}
      </Typography>
    </div>
  );

  const renderList = (
    <div>
      <div className={classes.dialogTitle}>
        <Typography classes={{ root: classes.title }} variant="h1">
          {`${I18n.t('drawer.shortcuts')} ${I18n.t(`shortcuts.${name}.title`)}`}
        </Typography>
      </div>
      <DialogContent classes={{ root: classes.root }}>
        <List>
          {shortcuts.map((item, index) => (
            !hiddenShortCuts.find(path => path === `${nameShortcut}.${item.name}`)
            && (
              <ListItem key={index}>
                <Typography classes={{ root: classes.combination }}>{item.combination}</Typography>
                <Typography classes={{ root: classes.listItem }}>{I18n.t(`shortcuts.${name}.${item.name}`)}</Typography>
              </ListItem>
            )
          ))}
        </List>
      </DialogContent>
    </div>
  );

  return (
    <Dialog classes={{ paper: classes.paper }} aria-labelledby="shortcuts-list" open={isOpen} onClose={onClose}>
      {name === 'empty' ? renderNoData : renderList}
      <div className={classes.button}>
        <Button
          color="primary"
          variant="contained"
          onClick={() => {
            push(routesByKey.shortcuts);
            onClose();
          }}
        >
          {I18n.t('shortcuts.allShortcuts')}
        </Button>
      </div>
    </Dialog>
  );
};

ShortcutsList.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  shortcutsList: PropTypes.shape({}).isRequired,
  push: PropTypes.func.isRequired,
  classes: PropTypes.shape({}),
  hiddenShortCuts: PropTypes.shape({}),
  nameShortcut: PropTypes.string
};

ShortcutsList.defaultProps = {
  classes: {},
  hiddenShortCuts: [],
  nameShortcut: ''
};

export default withStyles(styles)(ShortcutsList);
