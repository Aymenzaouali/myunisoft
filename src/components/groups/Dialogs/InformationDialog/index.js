import React from 'react';
import { compose } from 'redux';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Typography,
  withStyles
} from '@material-ui/core';
import { withShortcut } from 'hoc/dialogShortcut';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import PropTypes from 'prop-types';

const InformationDialog = (props) => {
  const {
    onClose,
    message,
    title,
    isOpen,
    buttonsLabel,
    error,
    classes
  } = props;

  return (
    <Dialog classes={{ paper: classes.container }} open={isOpen} onClose={onClose}>
      <DialogTitle classes={{ root: classes.title }}>
        <Typography variant="h1">{title}</Typography>
      </DialogTitle>
      <DialogContent classes={{ root: classes.message }}>
        <Typography variant="body1">{message}</Typography>
        <Typography variant="h6" color="error">{error}</Typography>
      </DialogContent>
      <DialogActions classes={{ root: classes.button }}>
        <Button variant="contained" color="error" onClick={onClose}>{buttonsLabel.cancel}</Button>
      </DialogActions>
    </Dialog>
  );
};

const styles = {
  container: {
    paddingTop: '20px',
    paddingBottom: '20px',
    paddingLeft: '25px',
    paddingRight: '25px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    maxheight: '280px',
    maxWidth: '600px'
  },
  title: {
    textAlign: 'center'
  },
  message: {
    textAlign: 'center'
  },
  button: {
    justifyContent: 'center'
  }
};

InformationDialog.propTypes = {
  onClose: PropTypes.func,
  message: PropTypes.string,
  title: PropTypes.string,
  isOpen: PropTypes.bool,
  error: PropTypes.string,
  classes: PropTypes.shape({}).isRequired,
  buttonsLabel: PropTypes.shape({})
};

InformationDialog.defaultProps = {
  error: '',
  buttonsLabel: {
    cancel: I18n.t('confirmation.cancel')
  },
  isOpen: false,
  onClose: () => {},
  title: '',
  message: ''
};

export default compose(
  withStyles(styles),
  withShortcut()
)(InformationDialog);
