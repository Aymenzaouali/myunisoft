import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import Button from 'components/basics/Buttons/Button';
import ReduxRadio from 'components/reduxForm/Selections/ReduxRadio';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';
import './styles.scss';

const GroupsDialog = (props) => {
  const {
    isOpen,
    onClose,
    onValidate,
    items,
    title
  } = props;

  return (
    <Dialog aria-labelledby="group_choice" open={isOpen} onClose={onClose}>
      <div className="dialog__group login_container__block login_container__block-group">
        <div className="group__title">
          {title}
        </div>
        <div className="group__main-inputs">
          <form onSubmit={onValidate}>
            <div className="group__list">
              <Field color="primary" name="group" component={ReduxRadio} list={items} labelStyle="checkbox__label" />
            </div>
            <Button className="form__button form__button-connect" type="submit" size="large" variant="contained" color="primary">
              {I18n.t('login.validate')}
            </Button>
          </form>
        </div>
      </div>
    </Dialog>
  );
};

GroupsDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onValidate: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string
  })).isRequired,
  title: PropTypes.string.isRequired
};

export default GroupsDialog;
