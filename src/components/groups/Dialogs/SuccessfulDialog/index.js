import React from 'react';
import PropTypes from 'prop-types';
import {
  withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import I18n from 'assets/I18n';
import { FontIcon } from 'components/basics/Icon';

const SuccessfulDialog = (props) => {
  const {
    classes,
    message,
    isOpen,
    onClose
  } = props;

  return (
    <Dialog
      classes={{
        root: classes.container
      }}
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <div>
          {message}
        </div>
      </DialogContent>
      <DialogActions classes={{ root: classes.footer }}>
        <Button
          classes={{
            root: classes.confirmButton
          }}
          variant="contained"
          color="primary"
          type="submit"
          onClick={onClose}
        >
          {I18n.t('successDialog.buttonOk')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const ownStyles = {
  container: {
    height: '60vh'
  },
  title: {
    fontSize: '1.5rem',
    fontWeight: 'bold',
    marginTop: '8px'
  },
  header: {
    textAlign: 'center',
    display: 'contents'
  },
  content: {
    padding: '20px 14% 35px',
    textAlign: 'center'
  },
  footer: {
    margin: '0 0 35px',
    justifyContent: 'center'
  },
  closeButton: {
    position: 'absolute',
    top: '14px',
    right: '14px'
  },
  confirmButton: {
    textTransform: 'uppercase'
  }
};

SuccessfulDialog.defaultProps = {
  message: I18n.t('successDialog.message')
};

SuccessfulDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  message: PropTypes.string
};

export default withStyles(ownStyles)(SuccessfulDialog);
