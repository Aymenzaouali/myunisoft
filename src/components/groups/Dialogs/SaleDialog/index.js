import React from 'react';
import PropTypes from 'prop-types';
import {
  IconButton,
  Dialog, DialogTitle, DialogContent, DialogActions,
  Typography
} from '@material-ui/core';

import Button from 'components/basics/Buttons/Button';
import FontIcon from 'components/basics/Icon/Font';

import I18n from 'assets/I18n';
import TableFactory from 'components/groups/Tables/Factory';
import { getSalesModalTableFactoryProps } from 'components/screens/FixedAssets/utils';
import styles from './SaleDialogModal.module.scss';

// Component
const SaleDialog = (props) => {
  const {
    open, onClose, onConfirm, isValide, sales
  } = props;

  // functions
  // Rendering
  return (
    <Dialog classes={{ paper: styles.dialog }} open={open} onClose={onClose}>
      <DialogTitle classes={{ root: styles.header }} disableTypography>
        <Typography
          classes={{ root: styles.headerTitle }}
          variant="h1"
        >
          {I18n.t('fixedAssets.modal.headerTitle')}
        </Typography>
        <IconButton classes={{ root: styles.closeBtn }} onClick={onClose}>
          <FontIcon name="icon-close" />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <TableFactory {...getSalesModalTableFactoryProps({ sales, ...props })} />
      </DialogContent>
      <DialogActions classes={{ root: styles.actions }}>
        <Button variant="outlined" onClick={onClose}>{I18n.t('fixedAssets.saleModal.cancel')}</Button>
        <Button
          variant="contained"
          disabled={!isValide}
          color="primary"
          classess={{ root: styles.actions.submit }}
          onClick={() => onConfirm()}
        >
          {I18n.t('fixedAssets.saleModal.submit')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

// Props
SaleDialog.propTypes = {
  isValide: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  sales: PropTypes.array.isRequired,
  onConfirm: PropTypes.func.isRequired
};

export default SaleDialog;
