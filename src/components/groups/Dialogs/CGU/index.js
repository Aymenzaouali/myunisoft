import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Typography,
  withStyles
} from '@material-ui/core';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import PropTypes from 'prop-types';

import ownStyles from './cguDialog.module.scss';

const ConfirmationDialog = (props) => {
  const {
    onClose = () => {},
    message,
    title,
    isOpen,
    buttonsLabel,
    error,
    classes,
    src,
    onValidate,
    maxWidth,
    fullWidth
  } = props;

  return (
    <Dialog
      classes={{ paper: classes.container }}
      maxWidth={maxWidth}
      fullWidth={fullWidth}
      open={isOpen}
      onClose={onClose}
    >
      <DialogContent classes={{ root: classes.innerContainer }}>
        <DialogTitle classes={{ root: classes.title }}>
          <Typography variant="h1">{title}</Typography>
        </DialogTitle>
        <DialogContent classes={{ root: classes.content }}>
          <Typography variant="body1">{message}</Typography>
          <Typography variant="h6" color="error">{error}</Typography>
          <iframe src={src} className={ownStyles.iframe} title="cgu" />
        </DialogContent>
        <DialogActions classes={{ root: classes.footer }}>
          {buttonsLabel.cancel !== null && <Button variant="contained" color="error" onClick={onClose}>{buttonsLabel.cancel}</Button>}
          <Button variant="contained" color="primary" onClick={onValidate}>{buttonsLabel.validate}</Button>
        </DialogActions>
      </DialogContent>
    </Dialog>
  );
};

const styles = {
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    overflow: 'hidden',
    paddingTop: '22%',
    paddingBottom: '22%'
  },
  content: {
    overflow: 'hidden',
    paddingTop: '0'
  },
  innerContainer: {
    position: 'absolute',
    padding: '0',
    top: '0',
    left: '0',
    width: '100%',
    height: '95%',
    border: '0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  footer: {
    marginTop: '10px',
    marginBottom: '10px'
  }
};

ConfirmationDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  message: PropTypes.string,
  title: PropTypes.string,
  onValidate: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  error: PropTypes.string,
  classes: PropTypes.shape({}).isRequired,
  buttonsLabel: PropTypes.shape({}),
  src: PropTypes.string,
  maxWidth: PropTypes.string,
  fullWidth: PropTypes.bool
};

ConfirmationDialog.defaultProps = {
  error: '',
  buttonsLabel: {
    validate: I18n.t('confirmation.validate'),
    cancel: I18n.t('confirmation.cancel')
  },
  isOpen: false,
  title: '',
  message: '',
  src: '',
  maxWidth: '',
  fullWidth: false
};

export default withStyles(styles)(ConfirmationDialog);
