import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import { Field } from 'redux-form';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import ReduxCheckBox from 'components/reduxForm/Selections/ReduxCheckBox';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import { MAIL_FORMAT } from 'assets/constants/regex';
import styles from './AccountingFirmDeclareTableDialog.module.scss';

const required = value => (value ? undefined : I18n.t('validations.newPassword.required'));
const emailValidation = value => ((value && !MAIL_FORMAT.test(value)) ? I18n.t('errors.invalid_email') : undefined);

const AccountingFirmDeclareTableDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    onClose,
    onValidate,
    creationMode,
    resetForm,
    fillDefaultsValues
  } = props;

  useEffect(() => {
    if (!creationMode && fillDefaultsValues) fillDefaultsValues();
    if (creationMode) resetForm();
  }, [creationMode]);

  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h6" classes={{ root: classes.title }}>
          {title}
        </Typography>
      </DialogContent>
      <form>
        <DialogContent classes={{ root: classes.body }}>
          <div className={styles.row}>
            <Field
              autoFocus
              color="primary"
              name="mail"
              component={ReduxTextField}
              validate={[required, emailValidation]}
              label={I18n.t('accountingFirmSettings.table.login')}
              margin="none"
              style={{ width: '100%' }}
            />
          </div>
          <div className={styles.row}>
            <Field
              color="primary"
              name="password"
              component={ReduxTextField}
              validate={creationMode ? [required] : []}
              label={I18n.t('accountingFirmSettings.table.password')}
              margin="none"
              style={{ width: '100%' }}
              {...!creationMode && { shrink: true, placeholder: '*******' }}
            />
          </div>
          <div className={styles.row}>
            <Field
              name="active"
              color="primary"
              label={I18n.t('accountingFirmSettings.table.active')}
              component={ReduxCheckBox}
            />
          </div>
        </DialogContent>
        <DialogActions classes={{ root: classes.footer }}>
          <InlineButton
            buttons={[
              {
                _type: 'string',
                text: I18n.t('common.cancel'),
                color: 'default',
                variant: 'outlined',
                size: 'medium',
                style: { marginLeft: 0 },
                onClick: onClose
              },
              {
                _type: 'string',
                variant: 'contained',
                text: I18n.t('common.validate'),
                size: 'medium',
                onClick: onValidate
              }]
            }
          />
        </DialogActions>
      </form>
    </Dialog>
  );
};

const ownStyles = {
  content: {
    display: 'contents'
  },
  footer: {
    margin: '0 50px 28px 50px',
    justifyContent: 'flex-start'
  },
  body: {
    paddingLeft: 50,
    paddingRight: 50,
    overflowY: 'inherit'
  },
  title: {
    marginLeft: 50,
    marginTop: '-40px',
    paddingRight: 70,
    fontSize: '1.5rem'
  },
  menu: {
    backgroundColor: 'red',
    color: 'yellow'
  }
};

AccountingFirmDeclareTableDialog.defaultProps = {
  onClose: () => {}
};

AccountingFirmDeclareTableDialog.propTypes = {
  onClose: PropTypes.func,
  resetForm: PropTypes.func.isRequired,
  fillDefaultsValues: PropTypes.func.isRequired,
  creationMode: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired
};

export default withStyles(ownStyles)(AccountingFirmDeclareTableDialog);
