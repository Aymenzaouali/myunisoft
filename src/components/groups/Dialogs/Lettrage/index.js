import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogActions,
  DialogContent,
  CircularProgress,
  Typography
} from '@material-ui/core';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { withStyles } from '@material-ui/core/styles';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import { InlineButton } from 'components/basics/Buttons';
import style from './Lettrage.module.scss';

const LettrageDialog = (props) => {
  const {
    getLetter,
    isOpen,
    onClose,
    classes,
    lettering,
    entry,
    letter,
    letterFetched,
    letterFetching,
    onValidate
  } = props;

  if (isOpen && lettering && !letterFetching && !letterFetched) getLetter();
  const leterSelected = entry.map(item => item.lettrage).join(', ');
  return (
    <Dialog
      onEnter={getLetter}
      fullWidth
      open={isOpen}
      onClose={onClose}
    >

      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>

      {lettering ? (
        <DialogContent classes={{ root: classes.content }}>
          <Typography variant="h1" className={style.title}>{I18n.t('lettrageDialog.title')}</Typography>
          {letterFetched
            && (
              <Field
                name="value"
                label={I18n.t('lettrageDialog.label')}
                className={classes.letter}
                component={ReduxTextField}
                autoFocus
                defaultValue={letter}
                format={value => value && value.toUpperCase()}
                inputProps={{
                  maxLength: 3
                }}
              />
            )}
          {letterFetching && <CircularProgress classes={{ root: classes.letter }} />}
        </DialogContent>
      ) : (
        <DialogContent classes={{ root: classes.content }}>
          <FontIcon className="icon-alert" color="#fe3a5e" size={30} />
          <Typography color="error">{`${I18n.t('lettrageDialog.delettrer')} ${leterSelected}`}</Typography>
          <Typography color="error">{I18n.t('lettrageDialog.delettrer2')}</Typography>
        </DialogContent>
      )}
      <DialogActions classes={{ root: classes.footer }}>
        <InlineButton
          marginDirection="right"
          buttons={[
            {
              _type: 'string',
              text: I18n.t('lettrageDialog.cancelled'),
              onClick: onClose,
              color: 'default',
              size: 'medium'
            },
            {
              _type: 'string',
              text: lettering ? I18n.t('lettrageDialog.validateL') : I18n.t('lettrageDialog.validateD'),
              onClick: onValidate,
              color: lettering ? 'primary' : 'error',
              size: 'medium'
            }]
          }
        />
      </DialogActions>
    </Dialog>
  );
};

const styles = {
  content: {
    textAlign: 'center',
    display: 'contents'
  },
  footer: {
    margin: '50px',
    justifyContent: 'center'
  },
  letter: {
    marginLeft: 210,
    marginRight: 'auto',
    marginTop: 30,
    width: 100
  }
};


LettrageDialog.propTypes = {
  data: PropTypes.shape({}).isRequired,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onValidate: PropTypes.func.isRequired,
  classes: PropTypes.shape({}).isRequired,
  getLetter: PropTypes.func.isRequired,
  lettering: PropTypes.bool.isRequired,
  entry: PropTypes.arrayOf(
    PropTypes.shape({}).isRequired
  ).isRequired,
  letter: PropTypes.string.isRequired,
  letterFetched: PropTypes.bool.isRequired,
  letterFetching: PropTypes.bool.isRequired
};

export default withStyles(styles)(LettrageDialog);
