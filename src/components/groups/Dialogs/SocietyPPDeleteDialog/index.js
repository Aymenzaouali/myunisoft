import React from 'react';
import PropTypes from 'prop-types';
import { InlineButton } from 'components/basics/Buttons';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import I18n from 'assets/I18n';
import { FontIcon } from 'components/basics/Icon';

const SocietyPPDeleteDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    message,
    onClose,
    onValidate
  } = props;

  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      classes={{ paper: classes.dialog }}
    >
      <DialogActions classes={{ root: classes.close }}>
        <Button onClick={onClose}>
          <FontIcon size={30} name="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.header }}>
        <FontIcon color="#fe3a5e" size={40} name="icon-alert" />
        <Typography
          variant="h3"
          classes={{ root: classes.title }}
        >
          {title}
        </Typography>
      </DialogContent>
      <DialogContent classes={{ root: classes.content }}>
        <div>
          {message}
        </div>
      </DialogContent>
      <DialogActions classes={{ root: classes.footer }}>
        <InlineButton buttons={
          [
            {
              _type: 'string',
              variant: 'outlined',
              color: 'default',
              size: 'medium',
              text: I18n.t('physicalPersonCreation.society_form.popUp.cancel'),
              onClick: onClose
            },
            {
              _type: 'string',
              variant: 'contained',
              color: 'error',
              size: 'medium',
              text: I18n.t('physicalPersonCreation.society_form.popUp.deleteOk'),
              onClick: onValidate
            }
          ]
        }
        />
      </DialogActions>
    </Dialog>
  );
};

const ownStyles = {
  dialog: {
    width: 600,
    height: 330
  },
  close: {
    margin: 0
  },
  title: {
    fontSize: '2rem', fontWeight: '500', marginTop: '8px'
  },
  header: {
    textAlign: 'center',
    display: 'contents'
  },
  content: {
    padding: '40px 130px 0',
    textAlign: 'center'
  },
  footer: {
    margin: '20px auto 50px',
    justifyContent: 'center'
  },
  okBtn: {
    backgroundColor: '#fe3a5e',
    color: 'white',
    marginLeft: 10
  }
};

SocietyPPDeleteDialog.defaultProps = {
  onClose: () => {}
};

SocietyPPDeleteDialog.propTypes = {
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired
};

export default withStyles(ownStyles)(SocietyPPDeleteDialog);
