import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography, DialogActions, Dialog, DialogContent, withStyles
} from '@material-ui/core';
import { Field } from 'redux-form';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import I18n from 'assets/I18n';
import styles from './functionDialog.module.scss';

const FunctionDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    onValidate,
    onClose,
    error
  } = props;
  return (
    <Dialog
      creationMode
      open={isOpen}
      classes={{ paper: classes.container }}
    >
      <div className={styles.innerContainer}>
        <DialogContent classes={{ root: classes.titleContainer }}>
          <Typography variant="h1">
            {title}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>
            <FontIcon size={30} className="icon-close" />
          </Button>
        </DialogActions>
      </div>
      <form onSubmit={onValidate}>
        <DialogContent classes={{ root: classes.content }}>
          <Field
            component={ReduxTextField}
            name="libelle"
            type="text"
            label={I18n.t('function.dialog.libelle')}
          />
          {error && <Typography variant="h6" color="error">{I18n.t('errors.alreadyExistFunction')}</Typography>}
        </DialogContent>
        <DialogActions classes={{ root: classes.footer }}>
          <Button variant="contained" onClick={onClose} classes={{ root: classes.buttonContainer }}>
            {I18n.t('connector.dialog.cancel')}
          </Button>
          <Button variant="contained" color="primary" type="submit">
            {I18n.t('connector.dialog.confirm')}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

const ownStyles = {
  container: {
    width: '450px',
    height: '270px',
    paddingBottom: '40px',
    paddingLeft: '40px'
  },
  titleContainer: {
    paddingLeft: '0px'
  },
  content: {
    padding: '0px',
    paddingBottom: '40px'
  },
  footer: {
    justifyContent: 'flex-start',
    margin: '0px'
  },
  buttonContainer: {
    marginTop: '0px',
    marginRight: '20px',
    marginBottom: '0px',
    marginLeft: '0px'
  }
};

FunctionDialog.defaultProps = {
  onClose: () => {},
  error: undefined
};

FunctionDialog.propTypes = {
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired,
  error: PropTypes.string,
  initialize: PropTypes.shape({}).isRequired // redux-form
};

export default withStyles(ownStyles)(FunctionDialog);
