import {
  Dialog, DialogActions, Typography, DialogContent
} from '@material-ui/core';
import I18n from 'i18next';
import React from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

const LinkAlreadySent = ({ isOpen, onClose, onConfirm }) => (
  <Dialog open={isOpen} onClose={onClose}>
    <DialogContent>
      <Typography variant="h6">{ I18n.t('discussions.fileShare.alreadySent') }</Typography>
      <DialogActions>
        <Button
          type="button"
          variant="outlined"
          onClick={onClose}
        >
          {I18n.t('common.cancel')}
        </Button>
        <Button variant="contained" color="primary" onClick={onConfirm}>
          {I18n.t('common.submit')}
        </Button>
      </DialogActions>
    </DialogContent>
  </Dialog>
);

LinkAlreadySent.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired
};

export default LinkAlreadySent;
