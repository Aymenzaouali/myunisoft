import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogContent,
  Typography,
  IconButton,
  withStyles
} from '@material-ui/core';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import { FontIcon } from 'components/basics/Icon';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { Field, formPropTypes } from 'redux-form';
import styles from './rib.module.scss';

const materialStyles = () => ({
  title: {
    fontSize: 24,
    width: 179.7,
    height: 29,
    padding: '20px 40px 0px 40px'
  },
  dialog: {
    paddingLeft: 20,
    paddingBottom: 0
  },
  ribDialogHeaderButton: {
    padding: 20
  },
  ribDialogBank: {
    width: 96
  },
  ribDialogGuichet: {
    width: 96
  },
  ribDalogAccountNumber: {
    width: 200
  },
  ribDialogIban: {
    minWidth: 300
  },
  ribDialogKey: {
    width: 20
  },
  ribDialogBic: {
    width: 200
  },
  ribDialogDiary: {
    width: 256
  },
  ribDialog: {
    paddingRight: 200,
    paddingLeft: 40
  }
});

const RibDialog = (props) => {
  const {
    isOpen,
    onClose,
    classes,
    paperList,
    isEdit,
    onValidate,
    loadData
  } = props;

  useEffect(() => {
    loadData();
  }, []);

  return (
    <Dialog aria-labelledby="rib" open={isOpen} onClose={onClose}>
      <div className={styles.ribDialogHeader}>
        <Typography classes={{ root: classes.title }} variant="h1">{isEdit ? I18n.t('rib.titleModalEdit') : I18n.t('rib.titleModal')}</Typography>
        <IconButton classes={{ root: classes.ribDialogHeaderButton }} onClick={onClose}>
          <FontIcon name="icon-close" size="24" color="black" titleInfoBulle={I18n.t('tooltips.close')} />
        </IconButton>
      </div>
      <DialogContent classes={{ root: classes.ribDialog }}>
        <div className={styles.ribRow}>
          <div>
            <Field
              color="primary"
              name="banque_code"
              component={ReduxTextField}
              label={I18n.t('rib.ribArrayTitle.bankCode')}
              className={classes.ribDialogBank}
            />
          </div>
          <div>
            <Field
              color="primary"
              name="guichet_code"
              component={ReduxTextField}
              label={I18n.t('rib.ribArrayTitle.guichetCode')}
              className={classes.ribDialogGuichet}
            />
          </div>
        </div>
        <div className={styles.ribRow}>
          <Field
            color="primary"
            name="banque_account_number"
            component={ReduxTextField}
            label={I18n.t('rib.ribArrayTitle.numberAccount')}
            className={classes.ribDalogAccountNumber}
          />
          <Field
            color="primary"
            name="rib_key"
            component={ReduxTextField}
            label={I18n.t('rib.ribArrayTitle.key')}
            className={classes.ribDialogKey}
            errorStyle={styles.error}
          />
        </div>

        <Field
          color="primary"
          name="iban"
          component={ReduxTextField}
          label={I18n.t('rib.ribArrayTitle.IBAN')}
          className={classes.ribDialogIban}
        />
        <Field
          color="primary"
          name="bic"
          component={ReduxTextField}
          label={I18n.t('rib.ribArrayTitle.BIC')}
          className={classes.ribDialogBic}
        />
        <Field
          color="primary"
          list={paperList}
          name="diary_id"
          component={ReduxSelect}
          label={I18n.t('rib.ribArrayTitle.paper')}
          className={classes.ribDialogDiary}
        />

      </DialogContent>
      <div className={styles.button}>
        <InlineButton
          marginDirection="left"
          buttons={[
            {
              _type: 'string',
              text: I18n.t('rib.buttonCancel'),
              variant: 'outlined',
              color: 'default',
              size: 'large',
              onClick: onClose
            },
            {
              _type: 'string',
              text: I18n.t('rib.buttonSave'),
              size: 'large',
              onClick: onValidate
            }]
          }
        />
      </div>
    </Dialog>
  );
};

RibDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.bool.isRequired,
  paperList: PropTypes.shape({}).isRequired,
  isEdit: PropTypes.bool,
  onValidate: PropTypes.func.isRequired,
  loadData: PropTypes.func.isRequired,

  ...formPropTypes
};

RibDialog.defaultProps = {
  isEdit: false
};

export default withStyles(materialStyles)(RibDialog);
