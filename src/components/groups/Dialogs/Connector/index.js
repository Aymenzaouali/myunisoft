import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import { Field } from 'redux-form';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import I18n from 'assets/I18n';
import styles from './connectorDialog.module.scss';

const ConnectorDialog = (props) => {
  const {
    loadData,
    isOpen,
    classes,
    firms,
    api,
    title,
    onValidate,
    creationMode,
    onClose
  } = props;

  useEffect(() => { loadData(); }, []);


  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h6" style={{ marginLeft: '24px' }}>
          {title}
        </Typography>
      </DialogContent>
      <form onSubmit={onValidate}>
        <DialogContent>
          <div className={styles.row}>
            <Field
              component={ReduxSelect}
              name="id_society"
              margin="none"
              label={I18n.t('connector.dialog.society')}
              className={styles.societyField}
              list={firms}
              disabled={!creationMode}
            />
            <div className={styles.softwareFieldContainer}>
              <Field
                component={ReduxSelect}
                name="id_third_party_api"
                margin="none"
                label={I18n.t('connector.dialog.software')}
                className={styles.softwareField}
                list={api}
                disabled={!creationMode}
              />
            </div>
          </div>
          <div>
            <Field
              component={ReduxTextField}
              name="code"
              type="text"
              label={I18n.t('connector.dialog.code')}
              className={styles.codeField}
            />
            <Field
              multiline
              component={ReduxTextField}
              name="complementary_information"
              type="text"
              label={I18n.t('connector.dialog.additionalInformation')}
              className={styles.informationField}
            />
          </div>
        </DialogContent>
        <DialogActions classes={{ root: classes.footer }}>
          <Button variant="contained" onClick={onClose}>
            {I18n.t('connector.dialog.cancel')}
          </Button>
          <Button variant="contained" color="primary" type="submit">
            {I18n.t('connector.dialog.confirm')}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

const ownStyles = {
  content: {
    textAlign: 'center',
    display: 'contents'
  },
  footer: {
    margin: '50px',
    justifyContent: 'space-evenly'
  }
};

ConnectorDialog.defaultProps = {
  onClose: () => {},
  firms: []
};

ConnectorDialog.propTypes = {
  creationMode: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  loadData: PropTypes.func.isRequired,
  firms: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })),
  api: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  title: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired
};

export default withStyles(ownStyles)(ConnectorDialog);
