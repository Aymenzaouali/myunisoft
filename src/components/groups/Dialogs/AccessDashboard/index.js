import React from 'react';
import I18n from 'assets/I18n';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import { InlineButton } from 'components/basics/Buttons';
import { FontIcon } from 'components/basics/Icon';
import Button from 'components/basics/Buttons/Button';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import {
  Dialog,
  withStyles,
  DialogActions,
  DialogContent,
  Typography
} from '@material-ui/core';

const AccessDashboardSociety = (props) => {
  const {
    isOpen,
    societySelected,
    onValidate,
    error,
    classes,
    onClose
  } = props;

  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={20} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h1" className={classes.title}>
          {societySelected}
        </Typography>
        <Field
          name="password"
          type="password"
          component={ReduxTextField}
          autoFocus
          label={I18n.t('accessDasboard.password')}
          className={classes.password}
        />
        <Typography>
          { error }
        </Typography>
      </DialogContent>
      <DialogActions classes={{ root: classes.footer }}>
        <InlineButton
          buttons={[
            {
              _type: 'string',
              text: I18n.t('common.cancel'),
              variant: 'contained',
              color: 'default',
              size: 'mediun',
              onClick: onClose
            },
            {
              _type: 'string',
              text: I18n.t('common.submit'),
              color: 'primary',
              variant: 'contained',
              size: 'medium',
              onClick: onValidate
            }
          ]}
        />
      </DialogActions>
    </Dialog>
  );
};
const ownStyles = {
  title: {
    textAlign: 'center'
  },
  content: {
    margin: 'auto',
    display: 'contents'
  },
  footer: {
    padding: 10,
    justifyContent: 'space-evenly'
  },
  password: {
    margin: '10px 20px',
    width: 200
  }
};

AccessDashboardSociety.propTypes = {
  isOpen: PropTypes.bool,
  classes: PropTypes.shape({}).isRequired,
  error: PropTypes.string,
  societySelected: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

AccessDashboardSociety.defaultProps = {
  isOpen: false,
  error: ''
};

export default withStyles(ownStyles)(AccessDashboardSociety);
