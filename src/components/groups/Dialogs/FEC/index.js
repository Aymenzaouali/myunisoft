import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import ImportScreen from 'containers/controllers/Imports';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import styles from './fecDialog.module.scss';

const FECDialog = (props) => {
  const {
    isOpen,
    onClose,
    onEnter,
    onExit,
    societyId,
    initialValues
  } = props;

  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      onEnter={onEnter}
      onExit={onExit}
      fullWidth
      maxWidth="md"
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <div className={styles.container}>
        <ImportScreen
          hideExercice
          societyId={societyId}
          initialValues={initialValues}
          hideImportTypeComponent
          hideSelectSocietyComponent
          hasContainerStyles={false}
          refreshExCompany
        />
        <div className={styles.button}>
          <InlineButton
            buttons={[
              {
                _type: 'string',
                style: { color: '#fff' },
                text: I18n.t('fec.close'),
                size: 'medium',
                onClick: onClose
              }]
            }
          />
        </div>
      </div>
    </Dialog>
  );
};

FECDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onEnter: PropTypes.func.isRequired,
  onExit: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired,
  initialValues: PropTypes.object, // eslint-disable-line
  dropzoneConfig: PropTypes.shape({})
};

FECDialog.defaultProps = {
  dropzoneConfig: {
    style: {
      width: '100%',
      height: '200px',
      borderWidth: '2px',
      borderColor: 'rgb(102, 102, 102)',
      borderStyle: 'dashed',
      borderRadius: '5px'
    }
  }
};

export default FECDialog;
