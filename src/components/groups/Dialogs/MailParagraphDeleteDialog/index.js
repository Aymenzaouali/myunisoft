import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import I18n from 'assets/I18n';
import { FontIcon } from 'components/basics/Icon';

const MailParagraphDeleteDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    message,
    onClose,
    deleteParagraph
  } = props;

  const closeDialog = () => {
    onClose();
  };

  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={closeDialog}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.header }}>
        <FontIcon color="error" size={40} name="icon-alert" />
        <Typography
          variant="h3"
          classes={{ root: classes.title }}
        >
          {title}
        </Typography>
      </DialogContent>
      <DialogContent classes={{ root: classes.content }}>
        <div>
          {message}
        </div>
      </DialogContent>
      <DialogActions classes={{ root: classes.footer }}>
        <Button
          variant="outlined"
          onClick={closeDialog}
        >
          {I18n.t('settingStandardMail.deletePopUp.cancel')}
        </Button>
        <Button
          variant="contained"
          classes={{ root: classes.okBtn }}
          onClick={() => { deleteParagraph(); onClose(); }}
        >
          {I18n.t('settingStandardMail.deletePopUp.ok')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const ownStyles = ({ palette }) => ({
  title: {
    fontSize: '2rem', fontWeight: '500', marginTop: '8px'
  },
  header: {
    textAlign: 'center',
    display: 'contents'
  },
  content: {
    padding: '40px 130px 0',
    textAlign: 'center'
  },
  footer: {
    margin: '50px',
    justifyContent: 'center'
  },
  okBtn: {
    backgroundColor: palette.error.main,
    color: 'white',
    marginLeft: 10,
    '&:hover': {
      backgroundColor: palette.secondary.main
    }
  }
});

MailParagraphDeleteDialog.defaultProps = {
  onClose: () => {}
};

MailParagraphDeleteDialog.propTypes = {
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  deleteParagraph: PropTypes.func.isRequired
};

export default withStyles(ownStyles)(MailParagraphDeleteDialog);
