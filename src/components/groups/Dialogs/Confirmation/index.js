import React, { useMemo } from 'react';
import { compose } from 'redux';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Typography,
  withStyles
} from '@material-ui/core';
import I18n from 'assets/I18n';
import { withShortcut } from 'hoc/dialogShortcut';
import { InlineButton } from 'components/basics/Buttons';
import PropTypes from 'prop-types';

const ConfirmationDialog = (props) => {
  const {
    onValidate,
    onClose,
    message,
    title,
    isOpen,
    handleSubmit,
    buttonsLabel,
    error,
    classes,
    canValidate
  } = props;

  const buttons = useMemo(() => {
    const but = [];
    let onVal = onValidate;
    if (buttonsLabel.cancel) {
      but.push({
        _type: 'string',
        text: buttonsLabel.cancel,
        onClick: onClose,
        size: 'large',
        color: 'error'
      });
    }
    if (handleSubmit) {
      onVal = handleSubmit(onVal);
    }
    if (canValidate) {
      but.push({
        _type: 'string',
        text: buttonsLabel.validate,
        onClick: onVal,
        size: 'large'
      });
    }
    return but;
  }, [buttonsLabel, canValidate, handleSubmit]);

  return (
    <Dialog classes={{ paper: classes.container }} open={isOpen} onClose={onClose}>
      <DialogTitle>
        <Typography align="center" variant="h1">{title}</Typography>
      </DialogTitle>
      <DialogContent>
        <Typography variant="body1">{message}</Typography>
        <Typography variant="h6" color="error">{error}</Typography>
      </DialogContent>
      <DialogActions>
        <InlineButton
          marginDirection="right"
          buttons={buttons}
        />
      </DialogActions>
    </Dialog>
  );
};

const styles = {
  container: {
    paddingTop: '40px',
    paddingBottom: '40px',
    paddingLeft: '50px',
    paddingRight: '50px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }
};

ConfirmationDialog.propTypes = {
  onValidate: PropTypes.func,
  onClose: PropTypes.func,
  message: PropTypes.string,
  title: PropTypes.string,
  handleSubmit: PropTypes.func,
  isOpen: PropTypes.bool,
  error: PropTypes.string,
  classes: PropTypes.shape({}).isRequired,
  buttonsLabel: PropTypes.shape({}),
  canValidate: PropTypes.bool
};

ConfirmationDialog.defaultProps = {
  error: '',
  buttonsLabel: {
    validate: I18n.t('confirmation.validate'),
    cancel: I18n.t('confirmation.cancel')
  },
  isOpen: false,
  handleSubmit: false,
  onValidate: () => {},
  onClose: () => {},
  title: '',
  message: '',
  canValidate: true
};

const enhance = compose(
  withShortcut(),
  withStyles(styles)
);

export default enhance(ConfirmationDialog);
