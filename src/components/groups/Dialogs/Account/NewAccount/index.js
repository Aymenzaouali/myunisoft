import React from 'react';
import PropTypes from 'prop-types';

import {
  IconButton, Button,
  Dialog, DialogTitle, DialogContent, DialogActions,
  Tabs, Tab,
  Typography
} from '@material-ui/core';

import I18n from 'assets/I18n';

import AccountGeneralInfo from 'containers/groups/Forms/AccountGeneralInfo';

import FontIcon from 'components/basics/Icon/Font';
import AccountAdditionalInfo from 'components/groups/Forms/AccountAdditionalInfo';

import styles from './NewAccount.module.scss';

// Component
const NewAccount = (props) => {
  const {
    isOpen,
    switchTab,
    activeTab,
    onClose,
    onValidate,
    isContrepartie,
    accountNumberDisabled
  } = props;

  return (
    <Dialog classes={{ paper: styles.dialog }} open={isOpen} onClose={onClose}>
      <DialogTitle disableTypography>
        <Typography variant="h6" classes={{ root: styles.title }}>{ I18n.t('account.newAccount.title') }</Typography>
        <IconButton classes={{ root: styles.close }} onClick={() => onClose()}>
          <FontIcon name="icon-close" />
        </IconButton>
      </DialogTitle>

      <DialogContent>
        <Tabs value={activeTab} indicatorColor="primary" textColor="primary" onChange={(_, id) => switchTab(id)}>
          <Tab classes={{ label: styles.tab }} value={0} label={I18n.t('account.newAccount.infoSteps.infoGeneral')} />
          <Tab classes={{ label: styles.tab }} value={1} label={I18n.t('account.newAccount.infoSteps.infoComplementary')} disabled />
        </Tabs>
        <div className={styles.content}>
          { activeTab === 0 && (
            <AccountGeneralInfo
              isContrepartie={isContrepartie}
              accountNumberDisabled={accountNumberDisabled}
            />
          ) }
          { activeTab === 1 && <AccountAdditionalInfo /> }
        </div>
      </DialogContent>

      <DialogActions classes={{ root: styles.actions }}>
        <Button variant="outlined" onClick={onClose}>{ I18n.t('common.cancel') }</Button>
        <Button
          variant="contained"
          color="primary"
          onClick={onValidate}
        >
          { I18n.t('common.save') }
        </Button>
      </DialogActions>
    </Dialog>
  );
};

// Props
NewAccount.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  switchTab: PropTypes.func.isRequired,
  activeTab: PropTypes.number.isRequired,
  onValidate: PropTypes.func.isRequired,
  isContrepartie: PropTypes.bool,
  accountNumberDisabled: PropTypes.bool
};

NewAccount.defaultProps = ({
  onClose: () => {},
  isContrepartie: false,
  accountNumberDisabled: false
});

export default NewAccount;
