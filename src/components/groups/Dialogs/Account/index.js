import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  Typography
} from '@material-ui/core';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { AccountAutoComplete } from 'containers/reduxForm/Inputs';
import { ReduxCheckBox } from 'components/reduxForm/Selections';
import { Field } from 'redux-form';
import './styles.scss';

const AccountDialog = (props) => {
  const {
    isOpen,
    onClose,
    isContrepartie,
    error,
    handleSubmit,
    onSubmitAccount
  } = props;

  return (
    <Dialog aria-labelledby="account" open={isOpen} onClose={onClose}>
      <DialogTitle>
        {I18n.t('account.createAccount')}
      </DialogTitle>
      <DialogContent>
        <div className="accountDialog">
          <Field color="primary" name="account.label" component={ReduxTextField} label={I18n.t('account.label')} />
          <Field color="primary" name="account.account_number" component={ReduxTextField} label={I18n.t('account.number')} />
          <Field color="primary" name="account.intraco_account" component={ReduxCheckBox} label={I18n.t('account.intraco')} />
          <Field color="primary" name="account.provider" component={ReduxCheckBox} label={I18n.t('account.provider')} />
          <Field color="primary" name="account.das_2" component={ReduxCheckBox} label={I18n.t('account.das2')} />
          {!isContrepartie && <Field color="primary" name="account.counterpart_account_id" component={AccountAutoComplete} label={I18n.t('account.account_number')} />}
          <Typography variant="h6" color="error">{error}</Typography>
        </div>
      </DialogContent>
      <DialogActions>
        <Button color="error" variant="contained" onClick={onClose}>{I18n.t('account.cancel')}</Button>
        <Button color="primary" variant="contained" onClick={handleSubmit(onSubmitAccount)}>{I18n.t('account.validate')}</Button>
      </DialogActions>
    </Dialog>
  );
};

AccountDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.bool.isRequired,
  isContrepartie: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onSubmitAccount: PropTypes.func.isRequired,
  error: PropTypes.string
};

AccountDialog.defaultProps = {
  error: ''
};

export default AccountDialog;
