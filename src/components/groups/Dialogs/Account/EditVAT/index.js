import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import {
  IconButton, Button,
  Dialog, DialogTitle, DialogContent, DialogActions,
  Typography
} from '@material-ui/core';

import I18n from 'assets/I18n';

import FontIcon from 'components/basics/Icon/Font';

import { CodeTvaAutoComplete } from 'containers/reduxForm/Inputs';

import styles from './EditVAT.module.scss';

const EditVAT = ({
  isOpen, onClose,
  onValidate
}) => (
  <Dialog classes={{ paper: styles.dialog }} open={isOpen} onClose={onClose}>
    <DialogTitle disableTypography>
      <Typography variant="h6" classes={{ root: styles.title }}>{I18n.t('account.editVAT.title')}</Typography>
      <IconButton classes={{ root: styles.close }} onClick={onClose}>
        <FontIcon name="icon-close" />
      </IconButton>
    </DialogTitle>
    <DialogContent>
      <Field
        color="primary"
        name="vatCode"
        component={CodeTvaAutoComplete}
        menuPosition="fixed"
        placeholder={I18n.t('account.newAccount.TVA')}
        className={styles.VAT}
      />
    </DialogContent>
    <DialogActions classes={{ root: styles.actions }}>
      <Button variant="outlined" onClick={onClose}>{ I18n.t('common.cancel') }</Button>
      <Button
        variant="contained"
        color="primary"
        onClick={onValidate}
      >
        { I18n.t('common.save') }
      </Button>
    </DialogActions>
  </Dialog>
);
EditVAT.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  onValidate: PropTypes.func.isRequired
};

EditVAT.defaultProps = ({
  onClose: () => {}
});

export default EditVAT;
