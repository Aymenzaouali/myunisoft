import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';

const RAFConfirmDialog = (props) => {
  const {
    onClose,
    classes,
    onSubmit,
    isOpen
  } = props;

  return (
    <Dialog
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={onClose}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h6" classes={{ root: classes.title }}>
          {I18n.t('reportsAndForms.confirmDialog.title')}
        </Typography>
      </DialogContent>
      <DialogActions classes={{ root: classes.footer }}>
        <InlineButton
          buttons={[
            {
              _type: 'string',
              text: I18n.t('reportsAndForms.confirmDialog.cancel'),
              color: 'default',
              variant: 'outlined',
              size: 'medium',
              style: { marginLeft: 0 },
              onClick: onClose
            },
            {
              _type: 'string',
              variant: 'contained',
              onClick: onSubmit,
              text: I18n.t('reportsAndForms.confirmDialog.ok'),
              size: 'medium'
            }]
          }
        />
      </DialogActions>
    </Dialog>
  );
};

const ownStyles = {
  content: {
    display: 'contents'
  },
  footer: {
    margin: '0 50px 28px 50px',
    justifyContent: 'center'
  },
  body: {
    paddingLeft: 50,
    paddingRight: 50,
    overflowY: 'inherit'
  },
  title: {
    paddingLeft: 50,
    paddingRight: 50,
    marginTop: '-40px',
    fontSize: '1.5rem'
  }
};

RAFConfirmDialog.defaultProps = {
  onClose: () => {}
};

RAFConfirmDialog.propTypes = {
  onClose: PropTypes.func,
  onSubmit: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired
};

export default withStyles(ownStyles)(RAFConfirmDialog);
