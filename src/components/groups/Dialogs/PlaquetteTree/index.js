import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  IconButton,
  Typography,
  withStyles,
  FormControlLabel
} from '@material-ui/core';

import I18n from 'assets/I18n';

import CheckBox from 'components/basics/CheckBox';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import styles from './PlaquetteTree.module.scss';

const Box = ({
  label, ...others
}) => (
  <FormControlLabel
    control={(
      <CheckBox
        {...others}
      />
    )}
    label={label}
  />
);

const PlaquetteTree = ({
  getPlaquetteTreeView,
  plaquette_tree,
  onValidate,
  onClose,
  isOpen,
  reviewId,
  classes
}) => {
  const [selectedField, setSelectedField] = useState({ isAllSelected: false });
  useEffect(() => {
    getPlaquetteTreeView();
    setSelectedField({});
  }, [reviewId]);

  const onAllSelectedClick = (event) => {
    const newSelectedField = { isAllSelected: event.target.checked };
    plaquette_tree.forEach((pt) => {
      newSelectedField[pt.code] = event.target.checked;
      if (pt.children && (pt.children.length > 0)) {
        pt.children.forEach((ptc) => {
          newSelectedField[ptc.code] = event.target.checked;
        });
      }
    });
    setSelectedField(newSelectedField);
  };

  const checkIsAllSelected = (newSelectedField) => {
    let isAllSel = true;
    plaquette_tree.forEach((pt) => {
      if (!newSelectedField[pt.code]) {
        isAllSel = false;
      }
      if (pt.children && (pt.children.length > 0)) {
        pt.children.forEach((ptc) => {
          if (!newSelectedField[ptc.code]) {
            isAllSel = false;
          }
        });
      }
    });
    return isAllSel;
  };

  const onClick = (event, info) => {
    const childrens = {
      [info.code]: event.target.checked
    };
    info.children.forEach((c) => {
      childrens[c.code] = event.target.checked;
    });
    const newSelectedField = { ...selectedField, ...childrens };
    const newIsAllSelected = checkIsAllSelected(newSelectedField);
    setSelectedField({ ...newSelectedField, isAllSelected: newIsAllSelected });
  };

  const renderTree = array => (
    array.map(b => (
      <div className={styles.box}>
        <Box
          label={b.label}
          checked={selectedField[b.code]}
          value={b.code}
          onChange={checked => onClick(checked, b)}
        />
        {b.children && (b.children.length > 0) && (
          <div className={styles.boxContainer}>
            { renderTree(b.children) }
          </div>
        )}
      </div>
    ))
  );

  return (
    <Dialog classes={{ paper: classes.container }} open={isOpen} onClose={onClose}>
      <DialogTitle>
        <Typography variant="h1">{I18n.t('plaquetteTree.title')}</Typography>
        <IconButton
          onClick={onClose}
          classes={{ root: classes.closeButton }}
        >
          <FontIcon color="black" name="icon-close1" />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Box
          checked={selectedField.isAllSelected}
          onChange={onAllSelectedClick}
          label={I18n.t('plaquetteTree.includeAll')}
        />
        {renderTree(plaquette_tree)}
      </DialogContent>
      <DialogActions>
        <Button
          variant="outlined"
          color="error"
          onClick={onClose}
        >
          {I18n.t('plaquetteTree.cancel')}
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={() => onValidate(selectedField)}
        >
          {I18n.t('plaquetteTree.validate')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const themeStyles = () => ({
  container: {
    paddingTop: '30px',
    paddingBottom: '30px',
    paddingLeft: '50px',
    paddingRight: '50px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  closeButton: {
    position: 'absolute',
    top: '8px',
    right: '8px'
  }
});

Box.propTypes = {
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired
};

PlaquetteTree.propTypes = {
  onValidate: PropTypes.func,
  onClose: PropTypes.func,
  isOpen: PropTypes.bool,
  classes: PropTypes.shape({}).isRequired,
  getPlaquetteTreeView: PropTypes.func.isRequired,
  plaquette_tree: PropTypes.arrayOf(PropTypes.object).isRequired,
  reviewId: PropTypes.number.isRequired
};

PlaquetteTree.defaultProps = {
  isOpen: false,
  onValidate: () => {},
  onClose: () => {}
};

export default withStyles(themeStyles)(PlaquetteTree);
