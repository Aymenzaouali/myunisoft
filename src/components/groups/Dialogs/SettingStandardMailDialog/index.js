import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography, withStyles, DialogActions, Dialog, DialogContent
} from '@material-ui/core';
import { Field } from 'redux-form';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import ReduxCheckBox from 'components/reduxForm/Selections/ReduxCheckBox';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import styles from './SettingStandardMailDialog.module.scss';

const SettingStandardMailDialog = (props) => {
  const {
    isOpen,
    classes,
    title,
    handleSubmit,
    onPostStatement,
    reset
  } = props;

  const closeDialog = () => {
    const { onClose } = props;
    onClose();
    reset();
  };

  return (
    <Dialog
      creationMode
      open={isOpen}
      fullWidth
      maxWidth="sm"
    >
      <DialogActions>
        <Button onClick={closeDialog}>
          <FontIcon size={30} className="icon-close" />
        </Button>
      </DialogActions>
      <DialogContent classes={{ root: classes.content }}>
        <Typography variant="h6" classes={{ root: classes.title }}>
          {title}
        </Typography>
      </DialogContent>
      <form>
        <DialogContent classes={{ root: classes.body }}>
          <div className={styles.row}>
            <Field
              color="primary"
              name="name"
              component={ReduxTextField}
              label={I18n.t('bankIntegrationSettings.table.heading')}
              margin="none"
              style={{ width: '100%' }}
            />
          </div>
          <div className={styles.row}>
            <Field
              color="primary"
              name="label_as_object"
              component={ReduxCheckBox}
              label={I18n.t('settingStandardMail.dialogs.newTypeCheck')}
              margin="none"
            />
          </div>
        </DialogContent>
        <DialogActions classes={{ root: classes.footer }}>
          <InlineButton
            buttons={[
              {
                _type: 'string',
                text: I18n.t('bankIntegrationSettings.popUp.cancel'),
                color: 'default',
                variant: 'outlined',
                size: 'medium',
                style: { marginLeft: 0 },
                onClick: closeDialog
              },
              {
                _type: 'string',
                variant: 'contained',
                onClick: handleSubmit(onPostStatement),
                text: I18n.t('bankIntegrationSettings.popUp.ok'),
                size: 'medium'
              }]
            }
          />
        </DialogActions>
      </form>
    </Dialog>
  );
};

const ownStyles = {
  content: {
    display: 'contents'
  },
  footer: {
    margin: '0 50px 28px 50px',
    justifyContent: 'flex-start'
  },
  body: {
    paddingLeft: 50,
    paddingRight: 50,
    overflowY: 'inherit'
  },
  title: {
    marginLeft: 50,
    marginTop: '-40px',
    paddingRight: 70,
    fontSize: '1.5rem'
  },
  menu: {
    backgroundColor: 'red',
    color: 'yellow'
  },
  select: {
    width: 100
  }
};

SettingStandardMailDialog.defaultProps = {
  onClose: () => {}
};

SettingStandardMailDialog.propTypes = {
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onPostStatement: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired
};

export default withStyles(ownStyles)(SettingStandardMailDialog);
