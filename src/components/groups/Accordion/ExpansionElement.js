import React from 'react';
import PropTypes from 'prop-types';
import {
  withStyles,
  ExpansionPanel as MuiExpansionPanel,
  ExpansionPanelSummary as MuiExpansionPanelSummary,
  ExpansionPanelDetails as MuiExpansionPanelDetails,
  Typography
} from '@material-ui/core';

import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';

const ExpansionPanel = withStyles({
  root: {
    margin: '10px 0',
    boxShadow: 'none',
    '&:before': {
      display: 'none'
    }
  },
  content: {
    alignItems: 'center'
  }
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    minHeight: 49,
    padding: '0 24px 0 44px',
    '&$expanded': {
      minHeight: 56
    }
  },
  expandIcon: {
    left: 12,
    height: 0,
    width: 0
  },
  content: {
    '&$expanded': {
      margin: '12px 0'
    }
  },
  expanded: {}
})(props => <MuiExpansionPanelSummary {...props} />);

ExpansionPanelSummary.muiName = 'ExpansionPanelSummary';

const ExpansionPanelDetails = withStyles(() => ({
  root: {
    padding: '24px'
  }
}))(MuiExpansionPanelDetails);


const ExpansionPanelIconStyles = () => ({
  button: {
    margin: 0,
    padding: '1px 8px',
    minWidth: 0,
    minHeight: 0,
    height: '100%',
    width: '100%'
  }
});

const ExpansionPanelIcon = withStyles(ExpansionPanelIconStyles)(({ classes, expanded }) => (
  <Button
    className={classes.button}
    classes={{ root: classes.root }}
    size="small"
    tabIndex="-1"
    variant="contained"
    color="primary"
  >
    <FontIcon name={`icon-${expanded ? 'minus' : 'plus'}`} color="#fff" size={14} />
  </Button>
));

class ExpansionElementCustom extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: props.defaultExpanded
    };
  }

  handleChange = (instance, isOpen) => {
    const { onChange } = this.props;
    this.setState({ isOpen });
    onChange(instance, isOpen);
  }

  render() {
    const {
      expanded,
      label,
      defaultExpanded,
      disabled,
      colorError,
      // eslint-disable-next-line react/prop-types
      children
    } = this.props;
    const { isOpen } = this.state;

    return (
      <ExpansionPanel
        expanded={expanded}
        onChange={this.handleChange}
        defaultExpanded={defaultExpanded}
        disabled={disabled}
      >
        <ExpansionPanelSummary expandIcon={<ExpansionPanelIcon expanded={isOpen} />}>
          <Typography variant="h6" color={colorError ? 'secondary' : 'default'}>{label}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          {children}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

ExpansionElementCustom.defaultProps = {
  defaultExpanded: false,
  disabled: false,
  colorError: false
};


ExpansionElementCustom.propTypes = {
  label: PropTypes.string.isRequired,
  expanded: PropTypes.bool.isRequired,
  defaultExpanded: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  colorError: PropTypes.bool
};


export default ExpansionElementCustom;
