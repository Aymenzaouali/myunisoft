import React from 'react';
import PropTypes from 'prop-types';
import ExpansionElementCustom from './ExpansionElement';

import { PanelShape } from './types';


class Accordion extends React.PureComponent {
  state = {
    // eslint-disable-next-line react/destructuring-assignment
    expanded: this.props.panels[0].key
  };

  handleChange = panel => (_event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };

  render() {
    const { expanded } = this.state;
    const { panels, oneOpen } = this.props;
    return (
      <div>
        {panels.map((panelItem, i) => (
          <ExpansionElementCustom
            key={panelItem.key}
            label={panelItem.label}
            expanded={oneOpen ? expanded === panelItem.key : undefined}
            onChange={this.handleChange(panelItem.key)}
            defaultExpanded={i === 0}
            colorError={panelItem.colorError}
          >
            <panelItem.Component {...panelItem.handlers} />
          </ExpansionElementCustom>
        ))}
      </div>
    );
  }
}

Accordion.defaultProps = {
  oneOpen: false
};

Accordion.propTypes = {
  oneOpen: PropTypes.bool,
  panels: PropTypes.arrayOf(PanelShape).isRequired
};

export default Accordion;
