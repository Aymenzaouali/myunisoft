import PropTypes from 'prop-types';

export const PanelShape = PropTypes.shape({
  key: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  Component: PropTypes.shape.isRequired
});
