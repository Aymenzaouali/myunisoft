import React from 'react';
import PropTypes from 'prop-types';

import AccountingFirmGeneralInfo from 'components/groups/Forms/AccountingFirmGeneralInfo';
import AccountingFirmLetter from 'containers/groups/Forms/AccountingFirmLetter';
import AccountingFirmSilae from 'components/groups/Forms/AccountingFirmSilae';
import { AccountingFirmDeclareTableEDI, AccountingFirmDeclareTableRIB } from 'containers/groups/Tables';
import AccountingFirmTabs from 'containers/groups/AccountingFirmTabs';

import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';
import { InlineButton } from 'components/basics/Buttons';

import I18n from 'assets/I18n';
import styles from './accountingFirmTabsBody.module.scss';

const AccountingFirmCreation = (props) => {
  const {
    activeTabId,
    isCreateNew,
    selectedRowsCount,
    resetForms,
    handleSubmit,
    validateForms,
    pw_silae_secured,
    pw_ws_silae_secured,
    goToTab
  } = props;

  return (
    <div className={styles.container}>
      { selectedRowsCount || isCreateNew ? (
        <div className={styles.mainContent}>
          <AccountingFirmTabs
            goToTab={(id) => {
              handleSubmit(() => goToTab(id))();
            }}
          />
          {activeTabId === 0 && (<AccountingFirmGeneralInfo />)}
          {activeTabId === 1 && (<AccountingFirmLetter />)}
          {activeTabId === 2 && (
            <AccountingFirmSilae
              pw_silae_secured={pw_silae_secured}
              pw_ws_silae_secured={pw_ws_silae_secured}
              isCreateNew={isCreateNew}
            />
          )}
          {activeTabId === 3 && (
            <div>
              {!isCreateNew && !!selectedRowsCount && <AccountingFirmDeclareTableEDI />}
              <AccountingFirmDeclareTableRIB />
            </div>
          )}
          <div className={styles.buttonContainer}>
            <div>
              <InlineButton
                marginDirection="right"
                buttons={[
                  {
                    _type: 'string',
                    text: I18n.t('companyCreation.validate'),
                    onClick: handleSubmit(validateForms),
                    size: 'large'
                  }]
                }
              />
            </div>
          </div>
        </div>
      ) : (
        <div className={styles.headerTitle}>
          <p>{I18n.t('accountingFirmSettings.slave.accountingFirmSettingsBody.empty.description')}</p>
        </div>
      )}
      <SplitToolbar onSplitClose={() => {
        resetForms();
      }}
      />
    </div>
  );
};

AccountingFirmCreation.propTypes = {
  activeTabId: PropTypes.number.isRequired,
  selectedRowsCount: PropTypes.number.isRequired,
  isCreateNew: PropTypes.bool.isRequired,
  pw_silae_secured: PropTypes.bool.isRequired,
  pw_ws_silae_secured: PropTypes.bool.isRequired,
  resetForms: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  validateForms: PropTypes.func.isRequired,
  goToTab: PropTypes.func.isRequired,
  selectedRow: PropTypes.shape({})
};

AccountingFirmCreation.defaultProps = {
  selectedRow: {}
};

export default AccountingFirmCreation;
