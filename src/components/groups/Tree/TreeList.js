import React from 'react';
import { List, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import Loader from 'components/basics/Loader';
import I18n from 'assets/I18n';
import Info from 'components/basics/Info';
import styles from './treeList.module.scss';

const TreeListWithLoading = ({ isLoading, isError, ...otherProps }) => (
  <>
    { isLoading
      ? (
        <div className={styles.tree_list_loading_wrap}>
          <Loader />
        </div>
      ) : <List {...otherProps} />
    }
    { isError && <Info message={I18n.t('tree.error')} />}
  </>
);

TreeListWithLoading.propTypes = {
  isLoading: PropTypes.bool,
  isError: PropTypes.bool
};

TreeListWithLoading.defaultProps = {
  isLoading: undefined,
  isError: undefined
};

const rootStyles = {
  root: {
    marginLeft: 12,
    padding: 0,
    width: '100%'
  }
};

const TreeList = withStyles(rootStyles)(TreeListWithLoading);
export { TreeList };
