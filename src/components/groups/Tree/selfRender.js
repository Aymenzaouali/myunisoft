import { ListItem, ListItemText } from '@material-ui/core';
import classNames from 'classnames';
import IconButton from '@material-ui/core/IconButton';
import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';

export const renderSelf = (props, handleClick, data, handleExpand, expanded) => {
  const {
    title, id, locked, expandable, onAdd, classes, activeId, isFolder
  } = props;

  const isActive = activeId === id;

  const handleAdd = () => {
    if (onAdd) onAdd(this);
  };

  return (
    <ListItem
      classes={{
        root: classNames(
          classes.listItem, isActive && classes.activeItem, isFolder && classes.folder
        )
      }}
      onClick={e => handleClick(e, data())}
    >
      {
        (
          locked
          && (
            <IconButton aria-label="locked" classes={{ root: classes.iconButton }}>
              <FontIcon name="icon-delettrage" />
            </IconButton>
          )
        ) || (
          expandable
          && (
            <React.Fragment>
              {isFolder ? (
                <IconButton
                  aria-label="locked"
                  classes={{ root: classes.iconButton }}
                  onClick={handleExpand}
                >
                  <FontIcon name={expanded ? 'icon-folder-open' : 'icon-folder'} color="#0bd1d1" size={14} />
                </IconButton>
              ) : (
                <Button
                  color="primary"
                  variant="contained"
                  onClick={handleExpand}
                  classes={{
                    root: classes.expandButton,
                    label: classes.buttonLabel
                  }}
                >
                  { expanded ? '-' : '+' }
                </Button>
              )}
            </React.Fragment>
          )
        ) || (
          <div className={classes.noIconSpace}>&nbsp;</div>
        )
      }

      <ListItemText
        primary={title}
        classes={{
          root: isFolder ? classNames(classes.textRoot, classes.textRoot_pointer)
            : classes.textRoot,
          primary: classes.text
        }}
      />
      {
        onAdd && (
          <Button
            color="primary"
            variant="contained"
            onClick={handleAdd}
            classes={{
              root: classes.addButton,
              label: classes.buttonLabel
            }}
          >
            +
          </Button>
        )
      }
      {
        isFolder && (
          <FontIcon
            name="icon-arrow"
            onClick={handleExpand}
            style={
              expanded ? { transform: 'rotate(-90deg)', color: '#0bd1d1', transition: '0.2s all ease' }
                : { transform: 'rotate(90deg)', color: '#0bd1d1', transition: '0.2s all ease' }
            }
          />
        )
      }
    </ListItem>
  );
};

renderSelf.propTypes = {
  title: PropTypes.string,
  id: PropTypes.string.isRequired,
  expandable: PropTypes.bool.isRequired,
  locked: PropTypes.bool,
  onAdd: PropTypes.func.isRequired,
  isFolder: PropTypes.bool,
  activeId: PropTypes.string
};

renderSelf.defaultProps = {
  title: null,
  locked: false,
  isFolder: false,
  activeId: null
};
