import _ from 'lodash';
import { ListItem } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { Fragment } from 'react';
import { TreeList } from './TreeList';
import TreeListItemStyled from './TreeListItem';

export const renderChildren = (props, handleClick, data) => {
  const {
    childrenData, classes, activeId, onActivate, onAdd
  } = props;
  return (
    <ListItem classes={{ root: classes.listItem }}>
      <TreeList>
        { childrenData && _.map(childrenData, (item, i) => {
          const children = _.get(item, 'children', {});
          return (
            <Fragment>
              {
                item && (
                  <TreeListItemStyled
                    {...item}
                    expandable={children && children.length > 0}
                    canAdd={item.canAdd}
                    onAdd={onAdd}
                    key={i}
                    childrenData={children}
                    activeId={activeId}
                    onClick={e => handleClick(e, data())}
                    onActivate={onActivate}
                  />
                )
              }
            </Fragment>
          );
        })
        }
      </TreeList>
    </ListItem>
  );
};

renderChildren.propTypes = {
  childrenData: PropTypes.arrayOf(PropTypes.object),
  onAdd: PropTypes.func.isRequired,
  activeId: PropTypes.string,
  onActivate: PropTypes.func
};

renderChildren.defaultProps = {
  childrenData: null,
  activeId: null,
  onActivate: null
};
