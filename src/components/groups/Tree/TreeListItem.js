import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import { renderSelf } from './selfRender';
import { renderChildren } from './childrenRender';

const styles = {
  expandButton: {
    borderRadius: '2px',
    backgroundColor: '#0bd1d1',
    minWidth: '14px',
    minHeight: '14px',
    maxWidth: '14px',
    maxHeight: '14px',
    boxShadow: 'none',
    padding: '0px'
  },
  buttonLabel: {
    height: '14px'
  },
  addButton: {
    borderRadius: '2px',
    color: '#07b4b4',
    border: 'solid 2px #07b4b4',
    backgroundColor: 'rgba(0,0,0,0)',
    minWidth: '18px',
    minHeight: '18px',
    maxWidth: '18px',
    maxHeight: '18px',
    boxSizing: 'border-box',
    boxShadow: 'none',
    padding: 0,
    margin: 0
  },
  iconButton: {
    boxSizing: 'border-box',
    boxShadow: 'none',
    padding: 0,
    margin: 0,
    color: 'red'
  },
  listItem: {
    padding: '6px 6px 6px 10px',
    '&::first-child': {
      color: 'red'
    }
  },
  folder: {
    '& ~ li': {
      paddingLeft: '0'
    }
  },
  noIconSpace: {
    width: 20,
    height: 20,
    color: 'black'
  },
  textRoot: {
    padding: '0 0 0 10px'
  },
  textRoot_pointer: {
    cursor: 'pointer'
  },
  text: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    minWidth: 122
  },
  activeItem: {
    background: '#9ae8e8'
  },
  arrowButton: {
    transform: 'rotate(90deg)',
    color: '#0bd1d1'
  }
};

const TreeListItem = (props) => {
  const { isFolder } = props;
  const [expanded, setExpanded] = useState(!!isFolder);

  const handleExpand = () => {
    setExpanded(!expanded);
  };
  const handleClick = (event, data) => {
    // Ignore anything except native events.
    // stopPropagation & preventDefault doesn't work but
    // used as native event detector. Non-native events ignored
    // to avoid children list item click to parent propagation
    if (!(event.stopPropagation)) return;

    const { onActivate } = props;

    if (onActivate) {
      onActivate(data, event);
    }
  };

  const data = () => {
    const r = { ...props };

    if (r.messages) {
      r.messageUserCount = new Set(r.messages.map(item => item.name)).size;
    }
    return r;
  };

  const {
    childrenData, childrenOnly
  } = props;

  return (
    <React.Fragment>
      { !childrenOnly && renderSelf(props, handleClick, data, handleExpand, expanded) }

      { (expanded
        || childrenOnly)
        && childrenData
        && renderChildren(props, handleClick, data) }
    </React.Fragment>
  );
};

TreeListItem.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  childrenData: PropTypes.arrayOf(PropTypes.object),
  childrenOnly: PropTypes.bool,
  isFolder: PropTypes.bool,
  onActivate: PropTypes.func
};

TreeListItem.defaultProps = {
  childrenData: null,
  childrenOnly: false,
  isFolder: false,
  onActivate: null
};

const TreeListItemStyled = withStyles(styles)(TreeListItem);

export default TreeListItemStyled;
