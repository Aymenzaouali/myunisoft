import React from 'react';
import PropTypes from 'prop-types';

import {
  Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions
} from '@material-ui/core';

import I18n from 'assets/I18n';

import Button from 'components/basics/Buttons/Button';

// Component
const ValidationDialog = (props) => {
  const { open, onConfirm, onCancel } = props;

  // Rendering
  return (
    <Dialog open={open} onClose={onCancel}>
      <DialogTitle>{ I18n.t('notifications.warning') }</DialogTitle>
      <DialogContent>
        <DialogContentText>{ I18n.t('dadp.groupValidationWarning') }</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel} color="error">{ I18n.t('notifications.return') }</Button>
        <Button onClick={onConfirm}>{ I18n.t('notifications.continue') }</Button>
      </DialogActions>
    </Dialog>
  );
};

// Props
ValidationDialog.propTypes = {
  open: PropTypes.bool.isRequired,

  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

export default ValidationDialog;
