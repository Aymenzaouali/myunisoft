import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';

import { Tab, Tabs } from '@material-ui/core';

import { StatusIcon } from 'components/basics/Icon';
import Loader from 'components/basics/Loader';

import styles from './CycleTabs.module.scss';

// Component
const DADPCycleTabs = (props) => {
  const {
    category, reviewId, startDate, endDate, sectionId,
    cycles, selectedId, loading,

    getCycles, selectCycle
  } = props;

  // Effect
  useEffect(() => {
    if (reviewId != null && startDate && endDate && sectionId != null) {
      getCycles(category, reviewId, startDate, endDate, sectionId);
    }
  }, [category, reviewId, startDate, endDate, sectionId]);

  // Rendering
  let selected = selectedId;
  if (selected == null && cycles.length > 0) {
    selected = cycles[0].cycle_da_dp_id;
  }

  if (cycles.length === 0 && !loading) {
    return null;
  }

  return (
    <div className={styles.cycles}>
      <Tabs
        value={selected}
        onChange={(event, id) => selectCycle(id)}

        textColor="primary"
        indicatorColor="primary"
        variant="scrollable"

        classes={{
          scrollButtons: styles.scrollButtons
        }}
      >
        { loading && (
          <Tab
            value={null}
            disabled

            classes={{
              root: styles.tab,
              label: styles.tabLabel,
              labelContainer: styles.labelContainer,
              textColorPrimary: styles.textColor
            }}

            label={<Loader size={20} />}
          />
        ) }
        { cycles.map(cycle => (
          <Tab
            key={cycle.cycle_da_dp_id}
            value={cycle.cycle_da_dp_id}

            classes={{
              root: styles.tab,
              label: styles.tabLabel,
              labelContainer: styles.labelContainer,
              textColorPrimary: styles.textColor
            }}

            label={(
              <Fragment>
                <span className={styles.label}>{ cycle.label }</span>
                { cycle.valid_collab && (
                  <StatusIcon className={styles.icon} status={cycle.valid_collab} />
                ) }
                { cycle.valid_rm && (
                  <StatusIcon className={styles.icon} status={cycle.valid_rm} />
                ) }
              </Fragment>
            )}
          />
        )) }
      </Tabs>
    </div>
  );
};

// Props
DADPCycleTabs.propTypes = {
  category: PropTypes.string.isRequired,
  reviewId: PropTypes.number,
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  sectionId: PropTypes.number,

  cycles: PropTypes.array.isRequired,
  selectedId: PropTypes.number,
  loading: PropTypes.bool.isRequired,

  getCycles: PropTypes.func.isRequired,
  selectCycle: PropTypes.func.isRequired
};

DADPCycleTabs.defaultProps = {
  reviewId: null,
  startDate: null,
  endDate: null,
  sectionId: null,
  selectedId: null
};

export default DADPCycleTabs;
