import React, { useState } from 'react';
import {
  Typography
} from '@material-ui/core';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import { BISettingsDialog, BISettingsDeleteDialog } from 'containers/groups/Dialogs';
import I18n from 'assets/I18n';
import styles from './bankIntegrationSettingsHeader.module.scss';

const BankIntegrationSettingsHeader = ({
  selectedStatements,
  resetForm,
  initForModification,
  disabledExport,
  csvData,
  baseFileName
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);
  const [isCreation, setIsCreation] = useState(false);

  const closeDialog = () => {
    setIsOpen(false);
    setIsCreation(false);
  };

  const createBI = () => {
    resetForm();
    setIsOpen(true);
    setIsCreation(true);
  };

  const modifyBI = async () => {
    await initForModification();
    setIsOpen(true);
    setIsCreation(false);
  };

  const deleteBI = () => {
    setIsDeleteOpen(true);
  };

  const closeDeleteBI = () => {
    setIsDeleteOpen(false);
  };

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">
          {I18n.t('bankIntegrationSettings.title')}
        </Typography>
        <div>
          {/* <InlineButton buttons={
            [
              {
                _type: 'icon',
                variant: 'none',
                iconColor: 'black',
                iconName: 'icon-popup',
                iconSize: 24,
                titleInfoBulle: I18n.t('tooltips.newWindow')
              },
              {
                _type: 'icon',
                variant: 'none',
                iconColor: 'black',
                iconName: 'icon-close',
                iconSize: 24,
                titleInfoBulle: I18n.t('tooltips.close')
              }
            ]}
          /> */}
        </div>
      </div>
      <div className={styles.headerButtonContainer}>
        <InlineButton buttons={[
          {
            _type: 'string',
            size: 'medium',
            text: I18n.t('bankIntegrationSettings.btns.new'),
            onClick: createBI
          },
          {
            _type: 'string',
            size: 'medium',
            disabled: _.isEmpty(selectedStatements),
            text: I18n.t('bankIntegrationSettings.btns.modify'),
            onClick: modifyBI
          },
          {
            _type: 'string',
            size: 'medium',
            disabled: _.isEmpty(selectedStatements),
            text: I18n.t('bankIntegrationSettings.btns.delete'),
            color: 'error',
            onClick: deleteBI
          },
          {
            _type: 'component',
            color: 'primary',
            disabled: disabledExport,
            component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
          }
        ]}
        />
      </div>
      {
        isOpen && (
          <BISettingsDialog
            isOpen={isOpen}
            creationMode={isCreation}
            onClose={closeDialog}
            isCreation
          />
        )
      }
      <BISettingsDeleteDialog
        isOpen={isDeleteOpen}
        onClose={closeDeleteBI}
      />
    </div>
  );
};

BankIntegrationSettingsHeader.propTypes = {
  selectedStatements: PropTypes.shape({}).isRequired,
  resetForm: PropTypes.func.isRequired,
  initForModification: PropTypes.func.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired
};

export default BankIntegrationSettingsHeader;
