import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography
} from '@material-ui/core';
import I18n from 'assets/I18n';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import WindowHandler from 'helpers/window';
import styles from 'components/groups/Headers/WorkingPage/workingPage.module.scss';


const FixedAssetsHeader = ({
  className, openPJDialog, diligence_id, headerPjList, commentCount, getAndOpenComments,
  csvData, baseFileName, disabledExport
}) => (
  <div className={className}>
    <div className={styles.headerUp}>
      <div className={styles.headerTitle}>
        <Typography variant="h1">
          {I18n.t('fixedAssets.title')}
        </Typography>
      </div>
      <div className={styles.filterButtons}>
        <InlineButton buttons={
          [
            {
              _type: 'string',
              variant: 'outlined',
              color: 'none',
              badgeColor: 'primary',
              badgeContent: commentCount,
              text: I18n.t('fixedAssets.table.comment'),
              colorError: true,
              onClick: () => getAndOpenComments(diligence_id)
            },
            {
              _type: 'string',
              color: 'primary',
              text: I18n.t('fixedAssets.pjs'),
              onClick: () => WindowHandler.forceOpen(headerPjList)
            },
            {
              _type: 'icon',
              iconName: 'icon-upload',
              iconSize: 24,
              onClick: () => openPJDialog({
                id: diligence_id,
                location: 'diligence'
              })
            },
            {
              _type: 'string',
              color: 'primary',
              text: I18n.t('fixedAssets.submit')
            },
            {
              _type: 'icon',
              iconName: 'icon-save',
              variant: 'outlined',
              iconColor: 'black',
              color: 'inherit',
              iconSize: 24,
              titleInfoBulle: I18n.t('tooltips.savedSearches')
            },
            {
              _type: 'string',
              color: 'primary',
              text: I18n.t('common.validate')
            },
            {
              _type: 'component',
              color: 'primary',
              disabled: disabledExport,
              component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
            },
            {
              _type: 'icon',
              variant: 'text',
              iconColor: 'black',
              iconName: 'icon-popup',
              iconSize: 24,
              titleInfoBulle: I18n.t('tooltips.newWindow')
            },
            {
              _type: 'icon',
              variant: 'text',
              iconColor: 'black',
              iconName: 'icon-close',
              iconSize: 24,
              titleInfoBulle: I18n.t('tooltips.close')
            }
          ]}
        />
      </div>
    </div>
  </div>
);

FixedAssetsHeader.propTypes = {
  openPJDialog: PropTypes.func,
  headerPjList: PropTypes.array,
  commentCount: PropTypes.number.isRequired,
  getAndOpenComments: PropTypes.func.isRequired,
  diligence_id: PropTypes.number.isRequired,
  csvData: PropTypes.array.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  className: PropTypes.string,
  baseFileName: PropTypes.string.isRequired
};

FixedAssetsHeader.defaultProps = {
  className: undefined,
  openPJDialog: () => {},
  headerPjList: []
};

export default FixedAssetsHeader;
