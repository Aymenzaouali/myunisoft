import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { InlineButton } from 'components/basics/Buttons';
import { Field } from 'redux-form';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { EDIConfirmationDialog } from 'containers/groups/Dialogs';
import I18n from 'assets/I18n';
import ReduxCheckBox from 'components/reduxForm/Selections/ReduxCheckBox';
import { get } from 'lodash';
import styles from './VATHeaders.module.scss';


const VAT = (props) => {
  const {
    isCurrentMonthOrMore, previousPeriod, nextPeriod, period,
    getVatCycle, onSubmit, printOrDownloadCycle, changeIsNothingness,
    selectedFormId, onClickAutofill, isNothingness, dialog, onCloseDialog, onConfirm
  } = props;
  useEffect(() => {
    getVatCycle(period);
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">{I18n.t('tva.title')}</Typography>
        <div>
          <InlineButton buttons={
            [
              {
                _type: 'string', variant: 'contained', color: 'secondary', text: I18n.t('tva.control'), colorError: true
              },
              {
                _type: 'icon', iconName: 'icon-upload', iconSize: 28, titleInfoBulle: I18n.t('tooltips.download'), onClick: () => printOrDownloadCycle(period, 'download')
              },
              {
                _type: 'icon', iconName: 'icon-print', iconSize: 28, titleInfoBulle: I18n.t('tooltips.print'), onClick: () => printOrDownloadCycle(period, 'print')
              },
              {
                _type: 'string', color: 'secondary', text: I18n.t('tva.help'), colorError: true
              }
              // {
              //   _type: 'icon',
              //   variant: 'none',
              //   iconColor: 'black',
              //   iconName: 'icon-popup',
              //   iconSize: 24,
              //   titleInfoBulle: I18n.t('tooltips.newWindow'),
              //   colorError: true
              // },
              // {
              //   _type: 'icon',
              //   variant: 'none',
              //   iconColor: 'black',
              //   iconName: 'icon-close',
              //   iconSize: 24,
              //   titleInfoBulle: I18n.t('tooltips.close'),
              //   colorError: true
              // }
            ]}
          />
        </div>
      </div>
      <div className={styles.row}>
        <Field
          name="period"
          label="Période"
          component={ReduxTextField}
          type="month"
          margin="none"
          onCustomBlur={(e) => { getVatCycle(e.target.value); }}
        />
        <div>
          <InlineButton buttons={
            [
              {
                _type: 'icon',
                variant: 'none',
                iconName: 'icon-nav-left',
                iconSize: 19,
                iconColor: 'black',
                onClick: previousPeriod
              },
              {
                _type: 'icon',
                variant: 'none',
                iconName: 'icon-nav',
                iconSize: 19,
                iconColor: 'black',
                disabled: isCurrentMonthOrMore,
                onClick: nextPeriod
              }]}
          />
          <Field
            name="isNothingness"
            component={ReduxCheckBox}
            label={I18n.t('tva.nothingness')}
            color="primary"
            type="boolean"
            checked={isNothingness}
            onClick={(() => changeIsNothingness(!isNothingness))}
          />
          <InlineButton buttons={[
            {
              _type: 'icon',
              iconName: 'icon-save',
              iconSize: 19,
              titleInfoBulle: I18n.t('tooltips.tvasave'),
              onClick: () => onSubmit('sendVat')
            },
            { _type: 'string', text: I18n.t('tva.send'), onClick: () => onSubmit('sendVatEdi') },
            selectedFormId === 'tvaca3' && {
              _type: 'string', variant: 'outlined', color: 'none', text: I18n.t('tva.autofill'), onClick: onClickAutofill
            }
          ]}
          />
        </div>
      </div>
      <EDIConfirmationDialog
        isOpen={get(dialog, 'isOpen', false)}
        onClose={onCloseDialog}
        onConfirm={onConfirm}
        message={get(dialog, 'message', '')}
        buttonsLabel={get(dialog, 'buttonsLabel', {})}
      />
    </div>
  );
};

VAT.propTypes = {
  isCurrentMonthOrMore: PropTypes.bool.isRequired,
  previousPeriod: PropTypes.func.isRequired,
  nextPeriod: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  period: PropTypes.string.isRequired,
  getVatCycle: PropTypes.func.isRequired,
  printOrDownloadCycle: PropTypes.func.isRequired,
  changeIsNothingness: PropTypes.func.isRequired,
  isNothingness: PropTypes.func.isRequired,
  dialog: PropTypes.shape({}).isRequired,
  onClickAutofill: PropTypes.func.isRequired,
  selectedFormId: PropTypes.string.isRequired,
  onCloseDialog: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired
};

export default VAT;
