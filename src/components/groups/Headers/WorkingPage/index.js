import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography
} from '@material-ui/core';
import I18n from 'assets/I18n';
import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import styles from './workingPage.module.scss';

const WorkingPageHeader = (
  {
    pageTitle,
    headerBtn1,
    getAndOpenComments,
    csvData,
    baseFileName,
    disabledExport,
    commentCount
  }
) => ((
  <div>
    <div className={styles.headerUp}>
      <div className={styles.headerTitle}>
        <Typography variant="h1">
          {pageTitle}
        </Typography>
      </div>
      <div className={styles.filterButtons}>
        <InlineButton buttons={
          [
            {
              _type: headerBtn1 && 'string',
              variant: 'outlined',
              color: 'none',
              badgeColor: 'primary',
              badgeContent: commentCount,
              text: headerBtn1,
              colorError: true,
              onClick: getAndOpenComments
            },
            {
              _type: 'string',
              color: 'primary',
              text: I18n.t('fnp.validate'),
              colorError: true
            },
            {
              _type: 'icon',
              iconName: 'icon-save',
              iconSize: 28,
              titleInfoBulle: I18n.t('tooltips.savedSearches'),
              colorError: true
            },
            {
              _type: 'component',
              color: 'primary',
              disabled: disabledExport,
              component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
            }
          ]}
        />
        <SplitToolbar />
      </div>
    </div>
  </div>
)
);

WorkingPageHeader.propTypes = {
  pageTitle: PropTypes.string,
  baseFileName: PropTypes.string.isRequired,
  headerBtn1: PropTypes.string,
  getAndOpenComments: PropTypes.func,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array,
  commentCount: PropTypes.number
};

WorkingPageHeader.defaultProps = {
  pageTitle: 'Unnamed page',
  headerBtn1: undefined,
  csvData: [],
  getAndOpenComments: undefined,
  commentCount: null
};

export default WorkingPageHeader;
