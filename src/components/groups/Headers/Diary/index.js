import React from 'react';
// import PropTypes from 'prop-types';
import {
  Typography
} from '@material-ui/core';
import I18n from 'assets/I18n';
// import { InlineButton } from 'components/basics/Buttons';
import styles from './diary.module.scss';

const DiaryHeader = () => (
  <div className={styles.headerUp}>
    <div className={styles.headerTitle}>
      <Typography variant="h1">
        {I18n.t('diary.title')}
      </Typography>
    </div>
    <div className={styles.filterButtons}>
      {/* <InlineButton buttons={
        [
          {
            _type: 'icon',
            variant: 'none',
            iconColor: 'black',
            iconName: 'icon-popup',
            iconSize: 24,
            titleInfoBulle: I18n.t('tooltips.newWindow'),
            colorError: true
          },
          {
            _type: 'icon',
            variant: 'none',
            iconColor: 'black',
            iconName: 'icon-close',
            iconSize: 24,
            titleInfoBulle: I18n.t('tooltips.close'),
            colorError: true
          }
        ]}
      /> */}
    </div>
  </div>
);

// DiaryHeader.propTypes = {};

export default DiaryHeader;
