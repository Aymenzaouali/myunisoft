import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'redux-form';
import {
  StepLabel,
  Stepper,
  Typography,
  withStyles
} from '@material-ui/core';
import { ReduxRadio, ReduxSelect } from 'components/reduxForm/Selections';
import { InlineButton } from 'components/basics/Buttons';
import { Periode } from 'containers/reduxForm/Inputs';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import { GEDTabs } from 'assets/constants/data';
import I18n from 'assets/I18n';
import classNames from 'classnames';
import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';
import { Shortcuts } from 'react-shortcuts';
import SplitContext from 'context/SplitContext';
import styles from './documentManagementHeader.module.scss';

const DocumentManagementHeader = ({
  classes,
  setActiveTabId,
  activeTabId,
  handleSubmit,
  resetDocumentsSearchValues,
  onSubmit,
  location,
  initialValues,
  openPopupSplit,
  initializeDate,
  getDocuments,
  updateTree,
  start_date,
  end_date,
  exercice_N,
  format_document
}) => {
  const { slaves, onActivateSplit, onCloseSplit } = useContext(SplitContext);
  const [search_type, setSearch_type] = useState(initialValues.search_type || 'exercise');

  const handleSelectTab = async (id) => {
    if (id === 6) return;
    setActiveTabId(id);
    await initializeDate(start_date, end_date, exercice_N, format_document);
    getDocuments();
    updateTree(id);
  };

  const handleTabChange = async () => {
    await initializeDate(start_date, end_date, exercice_N, format_document);
  };

  useEffect(() => {
    handleTabChange().finally((getDocuments()));
  }, [start_date, end_date]);

  const handleShortcuts = (action) => {
    switch (action) {
    case 'NEW_TAB':
      window.open(location.pathname, `tab${new Date().getTime()}`);
      break;
    case 'NEW_WINDOW':
      window.open(location.pathname, `window${new Date().getTime()}`, `width=${window.innerWidth},height=${window.innerHeight}`);
      break;
    case 'LAUNCH_CONSULTING':
      if (slaves.length) {
        const { id, args } = slaves.find(item => item.id === 'consulting');
        onActivateSplit(id, args);
        openPopupSplit('document-management');
        onCloseSplit();
      }
      break;
    case 'SPLIT_ENTRY':
      if (slaves.length) {
        const { id, args } = slaves.find(item => item.id === 'accounting');
        onActivateSplit(id, args);
        openPopupSplit('document-management');
      }
      break;
    default:
      break;
    }
    return null;
  };

  return (
    <Shortcuts
      name="GED"
      handler={handleShortcuts}
      alwaysFireHandler
      stopPropagation={false}
      preventDefault={false}
      targetNodeSelector="body"
    >
      <div className={styles.container}>
        <div className={styles.headerContainer}>
          <Typography variant="h1">
            {I18n.t('ged.title')}
          </Typography>
          <div className={styles.headerButtons}>
            <SplitToolbar />
          </div>
        </div>
        <Form>
          <div className={styles.row}>
            <Field
              name="search_type"
              color="primary"
              component={ReduxRadio}
              list={[{ label: I18n.t('ged.filter.period'), value: 'period' },
                { label: I18n.t('ged.filter.exercise'), value: 'exercise' },
                { label: I18n.t('ged.filter.all'), value: 'toutes' }
              ]}
              row
              className={styles.timeFrame}
              onChange={e => setSearch_type(e.target.value)}
            />
            {
              search_type !== 'toutes' && (
                <Field
                  component={Periode}
                  displayExercice={search_type === 'exercise'}
                  disableDateSelection={search_type === 'exercise'}
                  separator
                  row
                  name="Date"
                  formName="documentManagementHeaderSearch"
                  leftDatePicker={styles.exerciceDate}
                  rightDatePicker={styles.exerciceDate}
                  exerciceField={styles.exerciceField}
                />
              )
            }

            <Field
              color="primary"
              name="doc_name"
              component={ReduxTextField}
              label={I18n.t('ged.filter.search')}
              className={styles.search}
              errorStyle={styles.error}
              margin="none"
              disabled={activeTabId === 5}
            />

            <Field
              color="primary"
              name="format_document"
              component={ReduxSelect}
              label={I18n.t('ged.filter.format')}
              className={styles.format}
              list={[
                { label: 'none', value: '' },
                { label: 'PDF', value: 'pdf' },
                { label: 'JPG', value: 'jpg' },
                { label: 'JPEG', value: 'jpeg' },
                { label: 'PNG', value: 'png' },
                { label: 'XLS', value: 'xls' },
                { label: 'XLSX', value: 'xlsx' },
                { label: 'DOC', value: 'doc' },
                { label: 'DOCX', value: 'docx' }
              ]}
              margin="none"
              errorStyle={styles.error}
            />

          </div>

          <div className={styles.row}>
            <div className={styles.ged_filter__bottom}>
              <div className={styles.amount}>
                <div className={styles.amount__inputs}>
                  <Field
                    color="primary"
                    name="debit"
                    component={ReduxTextField}
                    type="number"
                    label={I18n.t('ged.filter.amountBetween')}
                    className={styles.search}
                    errorStyle={styles.error}
                    margin="none"
                    disabled={activeTabId === 0 || activeTabId === 5}
                  />

                  <Typography
                    variant="body1"
                    className={classes.amountStep}
                  >
                    {I18n.t('ged.filter.amountStep')}
                  </Typography>

                  <Field
                    color="primary"
                    type="number"
                    name="debit_amount"
                    component={ReduxTextField}
                    className={styles.amountRange}
                    label={I18n.t('ged.filter.amountRange')}
                    margin="none"
                    errorStyle={styles.error}
                    disabled={activeTabId === 0 || activeTabId === 5}
                  />
                </div>
              </div>
              <Field
                color="primary"
                name="user_name"
                component={ReduxTextField}
                label={I18n.t('ged.filter.addedBy')}
                className={styles.amountRange}
                margin="none"
                errorStyle={styles.error}
              />
            </div>

            <div className={styles.ged_filter__bottom_buttons}>
              <div>
                <InlineButton buttons={[
                  {
                    _type: 'string',
                    size: 'medium',
                    text: I18n.t('ged.filter.searchBtn'),
                    onClick: handleSubmit(onSubmit)
                  },
                  {
                    _type: 'string',
                    size: 'medium',
                    text: I18n.t('ged.filter.resetBtn'),
                    color: 'default',
                    onClick: () => {
                      resetDocumentsSearchValues();
                      setSearch_type('exercise');
                      initializeDate(start_date, end_date, exercice_N);
                      handleSubmit(onSubmit)();
                    }
                  }]}
                />
              </div>
            </div>

          </div>

          <div className={styles.row}>
            <Stepper connector={null} className={classes.stepperTabsContainer}>
              {GEDTabs.map(e => (
                <div key={`Step${e.id}`} className={styles.stepperTabs}>
                  <StepLabel
                    onClick={() => handleSelectTab(e.id)}
                    classes={{
                      label: classNames(
                        { [classes.disabled]: e.disabled },
                        activeTabId === e.id ? classes.selectedLabel : classes.notSelectedLabel,
                      )
                    }}
                  >
                    {e.label}
                  </StepLabel>
                  <div className={
                    activeTabId === e.id
                      ? styles.stepperTabsSelected
                      : styles.stepperTabsNotSelected
                  }
                  />
                </div>
              ))}
            </Stepper>
          </div>
        </Form>
      </div>
    </Shortcuts>
  );
};
DocumentManagementHeader.propTypes = {
  openPopupSplit: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  formValues: PropTypes.shape({}),
  activeTabId: PropTypes.number.isRequired,
  initialValues: PropTypes.string.isRequired,
  setActiveTabId: PropTypes.func.isRequired,
  getDocuments: PropTypes.func.isRequired,
  updateTree: PropTypes.func.isRequired,
  initializeDate: PropTypes.func.isRequired,
  start_date: PropTypes.string.isRequired,
  end_date: PropTypes.string.isRequired,
  exercice_N: PropTypes.array,
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  resetDocumentsSearchValues: PropTypes.func.isRequired,
  form: PropTypes.shape({}).isRequired,
  format_document: PropTypes.string.isRequired
};

DocumentManagementHeader.defaultProps = {
  formValues: undefined,
  exercice_N: []
};

const stepperStyles = () => ({
  selectedLabel: {
    color: '#0bd1d1',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  notSelectedLabel: {
    color: 'black',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  stepperTabsContainer: {
    padding: '32px 0px 0px 0px',
    '& div:nth-last-child(-n+1)': {
      '& span': {
        color: '#ff8b1f'
      }
    }
  },
  disabled: {
    color: '#e7e7e7'
  },
  amountStep: {
    marginLeft: '10px',
    'margin-right': '10px',
    'margin-bottom': '-18px'
  }
});

export default withStyles(stepperStyles)(DocumentManagementHeader);
