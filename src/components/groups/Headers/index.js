import VATHeader from './VAT';
import ProgressionHeader from './Progression';
import DADPHeader from './DADP';
import DecloyerHeader from './Decloyer';
import BankLinkHeader from './BankLink';
import DocumentManagementHeader from './DocumentManagement';
import PortfolioListViewHeader from './PortfolioListViewHeader';
import SettingsWorksheetsHeader from './SettingsWorksheetsHeader';
import DiaryHeader from './Diary';
import PlansHeader from './Plans';
import PlanDetailsHeader from './PlanDetails';
import TaxDeclaration from './TaxDeclaration';
import BankIntegrationSettingsHeader from './BankIntegrationSettingsHeader';
import ReportsAndFormsHeader from './ReportsAndFormsHeader';
import SettingStandardMailHeader from './SettingStandardMailHeader';
import MailParagraphTableHeader from './MailParagraphTableHeader';
import AccountingFirmSettingsHeader from './AccountingFirmSettingsHeader';
import Bundle from './Bundle';
import Cycle from './Cycle';
import RentDeclarations from './RentDeclarations';

export {
  VATHeader,
  ProgressionHeader,
  DADPHeader,
  DecloyerHeader,
  BankLinkHeader,
  DiaryHeader,
  PlansHeader,
  PlanDetailsHeader,
  PortfolioListViewHeader,
  DocumentManagementHeader,
  TaxDeclaration,
  BankIntegrationSettingsHeader,
  SettingsWorksheetsHeader,
  ReportsAndFormsHeader,
  SettingStandardMailHeader,
  MailParagraphTableHeader,
  AccountingFirmSettingsHeader,
  Bundle,
  Cycle,
  RentDeclarations
};
