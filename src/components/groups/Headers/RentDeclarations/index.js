import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import CurrentEditionsFiltersDateFields from 'components/groups/CurrentEditions/CurrentEditionsFiltersDateFields';
import AutoComplete from 'components/basics/Inputs/AutoComplete';

import styles from './RentDeclarations.module.scss';

const RentDeclarations = (props) => {
  const {
    change,
    getRentDeclarationsForms,
    sendRentDeclarationsForms,
    sendEdiRentDeclarationsForms,
    printOrDownloadRentDeclarations,
    dateStart,
    dateEnd,
    exercises,
    exercise,
    getExercises
  } = props;

  useEffect(() => {
    getRentDeclarationsForms();
  }, [exercise]);

  useEffect(() => {
    getExercises();
  }, []);

  const handleAutocompleteOnChange = (e) => {
    change('exercise', e.value);
  };

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">{I18n.t('rentDeclarations.title')}</Typography>
        <div>
          <InlineButton buttons={
            [
              {
                _type: 'icon', iconName: 'icon-upload', iconSize: 28, titleInfoBulle: I18n.t('tooltips.download'), onClick: () => printOrDownloadRentDeclarations('download')
              },
              {
                _type: 'icon', iconName: 'icon-print', iconSize: 28, titleInfoBulle: I18n.t('tooltips.print'), onClick: () => printOrDownloadRentDeclarations('print')
              },
              {
                _type: 'string', color: 'secondary', text: I18n.t('tva.help'), colorError: true
              },
              {
                _type: 'icon', variant: 'none', iconColor: 'black', iconName: 'icon-popup', iconSize: 24, titleInfoBulle: I18n.t('tooltips.newWindow'), colorError: true
              },
              {
                _type: 'icon', variant: 'none', iconColor: 'black', iconName: 'icon-close', iconSize: 24, titleInfoBulle: I18n.t('tooltips.close'), colorError: true
              }
            ]}
          />
        </div>
      </div>
      <div className={styles.filter}>
        <CurrentEditionsFiltersDateFields
          disabled
          to={dateEnd}
          from={dateStart}
          onChange={() => {}}
        />
        <AutoComplete
          textFieldProps={{
            label: I18n.t('currentEditions.filters.exercice'),
            InputLabelProps: {
              shrink: true
            }
          }}
          className={styles.select}
          onChangeValues={handleAutocompleteOnChange}
          options={exercises}
          value={[exercise].map(e => exercises.find(v => v.value === e))}
        />
        <div className={styles.buttons}>
          <InlineButton buttons={[
            {
              _type: 'icon',
              iconName: 'icon-save',
              iconSize: 19,
              titleInfoBulle: I18n.t('tooltips.tvasave'),
              onClick: sendRentDeclarationsForms
            },
            { _type: 'string', text: I18n.t('tva.send'), onClick: sendEdiRentDeclarationsForms }
          ]}
          />
        </div>
      </div>
    </div>
  );
};

RentDeclarations.propTypes = {
  change: PropTypes.func.isRequired,
  getRentDeclarationsForms: PropTypes.func.isRequired,
  sendRentDeclarationsForms: PropTypes.func.isRequired,
  sendEdiRentDeclarationsForms: PropTypes.func.isRequired,
  printOrDownloadRentDeclarations: PropTypes.func.isRequired,
  dateStart: PropTypes.string.isRequired,
  dateEnd: PropTypes.string.isRequired,
  exercise: PropTypes.string.isRequired,
  exercises: PropTypes.array.isRequired,
  getExercises: PropTypes.func.isRequired
};

export default RentDeclarations;
