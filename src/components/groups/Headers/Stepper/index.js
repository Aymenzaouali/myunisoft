import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
  Typography,
  withStyles,
  StepLabel,
  Stepper,
  IconButton
} from '@material-ui/core';
import { FontIcon } from 'components/basics/Icon';
import { generateHeaderSteps } from 'helpers/headerSteppers';
import styles from './StepperHeader.module.scss';

const StepperHeader = (props) => {
  const {
    title,
    steps,
    activeTab,
    openTab,
    classes,
    padding,
    onClose,
    tabId
  } = props;
  return (
    <div className={padding && styles.stepperHeader}>
      <div className={styles.titleBar}>
        <Typography variant="h1">{title}</Typography>
        {(onClose && tabId === -2)
        && (
          <IconButton onClick={onClose}>
            <FontIcon name="icon-close" color="black" />
          </IconButton>
        )}
      </div>
      <Stepper connector={null} className={classes.stepperTabsContainer}>
        {generateHeaderSteps(steps).map(e => (
          <div key={`Step${e.id}`} className={styles.stepperTabs}>
            <StepLabel
              onClick={() => (e.onClick ? e.onClick(e.id) : (!e.disabled && openTab(e.id)))}
              classes={{
                label: classNames(
                  { [classes.disabled]: e.disabled },
                  activeTab === e.id
                    ? classes.selectedLabel
                    : classes.notSelectedLabel,
                  { [classes.colorError]: e.colorError }
                )
              }}
            >
              {e.label}
            </StepLabel>
            <div className={
              activeTab === e.id
                ? styles.stepperTabsSelected
                : styles.stepperTabsNotSelected
            }
            />
          </div>
        ))}
      </Stepper>
    </div>
  );
};

const materialStyles = ({ palette }) => ({
  selectedLabel: {
    color: '#0bd1d1',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  notSelectedLabel: {
    color: 'black',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  stepperTabsContainer: {
    padding: '32px 0px 0px 0px'
  },
  disabled: {
    color: '#e7e7e7'
  },
  colorError: {
    color: palette.secondary.main
  }
});

StepperHeader.defaultProps = ({
  classes: {},
  padding: true,
  onClose: undefined,
  tabId: -2
});

StepperHeader.propTypes = {
  title: PropTypes.string.isRequired,
  steps: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    label: PropTypes.string,
    disabled: PropTypes.bool,
    colorError: PropTypes.bool
  })).isRequired,
  activeTab: PropTypes.number.isRequired,
  openTab: PropTypes.func.isRequired,
  classes: PropTypes.shape({}),
  padding: PropTypes.bool,
  onClose: PropTypes.func,
  tabId: PropTypes.string
};

export default withStyles(materialStyles)(StepperHeader);
