import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import {
  Typography,
  withStyles
} from '@material-ui/core';
import { AccountingFirmTopMainTable } from 'containers/groups/Tables';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import SplitContext from 'context/SplitContext';
import styles from './accountingFirmSettingsHeader.module.scss';

const AccountingFirmSettingsHeader = ({
  selectedRowsCount,
  setCreateNewStatus
}) => {
  const {
    active,
    slaves,
    onActivateSplit
  } = useContext(SplitContext);
  const onButtonClick = async (createStatus) => {
    await setCreateNewStatus(createStatus);
    if (!active) onActivateSplit(slaves[0].id, slaves[0].args);
  };
  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">
          {I18n.t('accountingFirmSettings.title')}
        </Typography>
      </div>

      <div className={styles.tablerow}>
        <AccountingFirmTopMainTable />
        <div className={styles.tableButtons}>
          <InlineButton buttons={[
            {
              _type: 'string',
              size: 'medium',
              text: I18n.t('accountingFirmSettings.new'),
              onClick: () => {
                onButtonClick(true);
              }
            },
            {
              _type: 'string',
              size: 'medium',
              disabled: !selectedRowsCount,
              text: I18n.t('accountingFirmSettings.modify'),
              onClick: () => {
                onButtonClick(false);
              }
            }
          ]}
          />
        </div>
      </div>
    </div>
  );
};

AccountingFirmSettingsHeader.propTypes = {
  selectedRowsCount: PropTypes.number.isRequired,
  setCreateNewStatus: PropTypes.func.isRequired
};

const stepperStyles = () => ({
  selectedLabel: {
    color: '#0bd1d1',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  notSelectedLabel: {
    color: 'black',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  stepperTabsContainer: {
    padding: '32px 0px 0px 0px'
  },
  disabled: {
    color: '#e7e7e7'
  },
  amountStep: {
    marginLeft: '10px',
    'margin-right': '10px',
    'margin-bottom': '-18px'
  }
});

export default withStyles(stepperStyles)(AccountingFirmSettingsHeader);
