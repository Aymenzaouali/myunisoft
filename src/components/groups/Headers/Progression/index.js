import React from 'react';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import { ExerciseSelector } from 'containers/basics/Selector';
import PropTypes from 'prop-types';
import _ from 'lodash';

import styles from './progressionHeader.module.scss';

const ProgressionHeader = (props) => {
  const {
    currentExercise,
    selectedExercise,
    changeSelectedExercise,
    societyId,
    getProgressionSociety,
    getDashboard
  } = props;

  const handleClickCurrentExercise = async () => {
    await changeSelectedExercise(currentExercise, societyId);
    await getProgressionSociety();
    await getDashboard(societyId);
  };

  const renderButtons = () => (
    <div className={styles.exercisesSelector}>
      <div className={styles.selector}>
        <ExerciseSelector getDashboard={() => getDashboard(societyId)} />
      </div>
      <Button
        className={styles.refresh}
        variant="outlined"
        onClick={() => { handleClickCurrentExercise(); }}
        disabled={_.isEqual(currentExercise, selectedExercise)}
      >
        {I18n.t('progressionTable.currentExercise')}
      </Button>
    </div>
  );

  return (
    <div className={styles.header}>
      <div className={styles.title}>
        {I18n.t('progressionTable.title')}
      </div>
      {!_.isEmpty(currentExercise) && renderButtons()}
    </div>
  );
};

// Props
ProgressionHeader.propTypes = {
  currentExercise: PropTypes.shape(),
  selectedExercise: PropTypes.shape(),
  changeSelectedExercise: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired,
  getProgressionSociety: PropTypes.func.isRequired,
  getDashboard: PropTypes.func.isRequired
};

ProgressionHeader.defaultProps = {
  currentExercise: {},
  selectedExercise: {}
};

export default ProgressionHeader;
