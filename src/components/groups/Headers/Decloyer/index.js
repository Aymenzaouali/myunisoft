import React, { useState } from 'react';
import {
  Typography,
  StepLabel,
  Stepper,
  withStyles
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { InlineButton } from 'components/basics/Buttons';
import classNames from 'classnames';
import I18n from 'assets/I18n';
import styles from './Decloyer.module.scss';

const DecloyerHeader = (props) => {
  const {
    steps,
    classes
  } = props;

  const [activeTab, setActiveTab] = useState(0);

  const handleSelectTab = (id) => {
    setActiveTab(id);
  };

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="title">
          {I18n.t('decloyer.title')}
        </Typography>
        <div>
          <InlineButton
            marginDirection="right"
            buttons={[
              {
                _type: 'string',
                text: I18n.t('decloyer.buttons.validate'),
                onClick: () => console.log('Valider') //eslint-disable-line
              },
              {
                _type: 'icon',
                iconName: 'icon-pencil',
                iconSize: 28,
                titleInfoBulle: I18n.t('tooltips.print'),
                onClick: () => console.log('Modify') //eslint-disable-line
              },
              {
                _type: 'icon',
                iconName: 'icon-print',
                iconSize: 28,
                titleInfoBulle: I18n.t('tooltips.print'),
                onClick: () => console.log('Print') //eslint-disable-line
              }]
            }
          />
        </div>
      </div>
      <Stepper connector={null} className={classes.stepperTabsContainer}>
        {steps.map(e => (
          <div key={`Step${e.id}`} className={styles.stepperTabs}>
            <StepLabel
              onClick={() => handleSelectTab(e.id)}
              classes={{
                label: classNames(
                  { [classes.disabled]: e.disabled },
                  activeTab === e.id ? classes.selectedLabel : classes.notSelectedLabel,
                )
              }}
            >
              {e.label}
            </StepLabel>
            <div className={
              activeTab === e.id
                ? styles.stepperTabsSelected
                : styles.stepperTabsNotSelected
            }
            />
          </div>
        ))}
      </Stepper>
    </div>
  );
};

DecloyerHeader.propTypes = {
  steps: PropTypes.shape({}).isRequired,
  classes: PropTypes.shape({}).isRequired
};

const stepperStyles = () => ({
  selectedLabel: {
    color: '#0bd1d1',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  notSelectedLabel: {
    color: 'black',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  stepperTabsContainer: {
    padding: '32px 0px 0px 0px'
  },
  disabled: {
    color: '#e7e7e7'
  }
});

export default withStyles(stepperStyles)(DecloyerHeader);
