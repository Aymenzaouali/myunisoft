import React, { useState } from 'react';
import {
  Typography
} from '@material-ui/core';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';
import { ReportsAndFormsDialog, FormsAndReportsDeleteDialog } from 'containers/groups/Dialogs';
import styles from './reportsAndFormsHeader.module.scss';

const ReportsAndFormsHeader = ({ selectedRowsCount }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isCreation, setIsCreation] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);

  const closeDeleteRAF = () => {
    setIsDeleteOpen(false);
  };

  const closeDialog = () => {
    setIsOpen(false);
    setIsCreation(false);
  };

  const createReportAndForm = () => {
    setIsOpen(true);
    setIsCreation(true);
  };

  const modifyReportAndForm = () => {
    setIsOpen(true);
    setIsCreation(false);
  };

  return (
    <>
      <div className={styles.headerContainer}>
        <Typography variant="h1">
          {I18n.t('reportsAndForms.title')}
        </Typography>
        <div className={styles.btnControlContainer}>
          {/* <InlineButton buttons={
            [
              {
                _type: 'icon',
                variant: 'none',
                iconColor: 'black',
                iconName: 'icon-popup',
                iconSize: 24,
                titleInfoBulle: I18n.t('tooltips.newWindow')
              },
              {
                _type: 'icon',
                variant: 'none',
                iconColor: 'black',
                iconName: 'icon-close',
                iconSize: 24,
                titleInfoBulle: I18n.t('tooltips.close')
              }
            ]}
          /> */}
        </div>
      </div>
      <div className={styles.headerButtonContainer}>
        <InlineButton buttons={[
          {
            _type: 'string',
            size: 'medium',
            onClick: createReportAndForm,
            text: I18n.t('reportsAndForms.createNew')
          },
          {
            _type: 'string',
            size: 'medium',
            onClick: modifyReportAndForm,
            disabled: !selectedRowsCount,
            text: I18n.t('reportsAndForms.modify')
          },
          {
            _type: 'string',
            size: 'medium',
            disabled: !selectedRowsCount,
            text: I18n.t('reportsAndForms.delete'),
            color: 'error',
            onClick: () => setIsDeleteOpen(true)
          }
        ]}
        />
      </div>
      <ReportsAndFormsDialog
        isOpen={isOpen}
        creationMode={isCreation}
        onClose={closeDialog}
        isCreation
      />
      <FormsAndReportsDeleteDialog
        isOpen={isDeleteOpen}
        onClose={closeDeleteRAF}
      />
    </>
  );
};

ReportsAndFormsHeader.propTypes = {
  selectedRowsCount: PropTypes.shape({}).isRequired
};


export default ReportsAndFormsHeader;
