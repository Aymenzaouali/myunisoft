// TODO: Check definitive design
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { SectionAutoComplete } from 'containers/reduxForm/Inputs';
import { ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import { InlineButton } from 'components/basics/Buttons';
import {
  Typography, IconButton, Switch, withStyles
} from '@material-ui/core';
import styles from './DADPHeader.module.scss';

const stylesTheme = () => ({
  colorSwitchBase: {
    color: '#0bd1d1',
    '&$colorChecked': {
      color: '#a177ff',
      '& + $colorBar': {
        backgroundColor: '#a177ff'
      }
    }
  },
  colorBar: {
    color: '#0bd1d1',
    backgroundColor: '#0bd1d1'
  },
  colorChecked: {},
  iconButton: {
    padding: 0,
    'font-weight': 'bold'
  }
});

const DADPHeader = (props) => {
  const [isDa, setIsDa] = useState();
  const { classes } = props;

  return (
    <div className={styles.container}>
      <div className={styles.titleContainer}>
        <Typography variant="h1">DA / DP</Typography>
        <IconButton><span className="icon-close" /></IconButton>
      </div>
      <div className={styles.filterContainer}>
        <div className={styles.fieldContainer}>
          <div className={styles.switchContainer}>
            <Typography variant="body1">DA</Typography>
            <Switch
              checked={isDa}
              onChange={e => setIsDa(e.target.checked)}
              value="isDa"
              classes={{
                switchBase: classes.colorSwitchBase,
                checked: classes.colorChecked,
                bar: classes.colorBar,
                root: classes.iconButton
              }}
            />
            <Typography variant="body1">DP</Typography>
          </div>
          <Field
            name="section"
            component={SectionAutoComplete}
            placeholder="Section"
          />
          <Field
            name="start_date"
            component={ReduxMaterialDatePicker}
            placeholder="Du"
          />
          <Field
            name="end_date"
            component={ReduxMaterialDatePicker}
            placeholder="Au"
          />
          <IconButton classes={{ root: classes.iconButton }}><span className="icon-nav1" /></IconButton>
          <IconButton classes={{ root: classes.iconButton }}><span className="icon-nav-left" /></IconButton>
          <IconButton classes={{ root: classes.iconButton }}><span className="icon-nav" /></IconButton>
          <IconButton classes={{ root: classes.iconButton }}><span className="icon-nav1" /></IconButton>
        </div>
        <div>
          <InlineButton buttons={
            [
              {
                _type: 'string', text: 'Valider', size: 'Medium'
              },
              {
                _type: 'icon', iconName: 'icon-print', iconSize: 32
              }
            ]}
          />
        </div>
      </div>
    </div>
  );
};

DADPHeader.defaultProps = {
};

DADPHeader.propTypes = {
  classes: PropTypes.object.isRequired // eslint-disable-line
};

export default withStyles(stylesTheme)(DADPHeader);
