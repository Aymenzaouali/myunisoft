import React from 'react';
import {
  // IconButton,
  Typography
} from '@material-ui/core';

import I18n from 'assets/I18n';

import ReviewTabs from 'containers/groups/DADP/ReviewTabs';
import CycleTabs from 'containers/groups/DADP/CycleTabs';
import DADPReviewInfo from 'containers/groups/Forms/DADPReviewInfo';
import DADPSection from 'containers/groups/Forms/DADPSection';

import { InlineButton } from 'components/basics/Buttons';

import styles from './DADPHeader.module.scss';

// Component
const DADPHeader = () => (
  <div className={styles.container}>
    <div className={styles.title}>
      <Typography variant="h1">{I18n.t('dadp.dadp')}</Typography>
      <div className={styles.grow} />

      {/* <IconButton><span className="icon-popup" /></IconButton>
        <IconButton><span className="icon-close" /></IconButton> */}
    </div>
    <div className={styles.dadp}>
      <ReviewTabs />
      <div className={styles.filters}>
        <div>
          <DADPReviewInfo />
          <DADPSection />

          <div className={styles.grow} />

          <InlineButton
            buttons={[
              {
                _type: 'string', text: 'Clôturer le dossier', size: 'medium', disabled: true
              },
              {
                _type: 'icon', iconName: 'icon-archive', iconSize: 32
              },
              {
                _type: 'icon', iconName: 'icon-print', iconSize: 32
              }
            ]}
          />
        </div>
      </div>
      <CycleTabs />
    </div>
  </div>
);

export default DADPHeader;
