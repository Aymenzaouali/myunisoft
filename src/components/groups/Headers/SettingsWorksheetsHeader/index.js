import React, { useState } from 'react';
import _ from 'lodash';
import {
  Typography
} from '@material-ui/core';
import { Worksheets as WorksheetsDialog, WorksheetsDeleteDialog } from 'containers/groups/Dialogs';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import styles from './settingsWorksheetsHeader.module.scss';

const SettingsWorksheetsHeader = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isDeleteAttention, setIsDeleteAttention] = useState(false);
  const [isCreation, setIsCreation] = useState(false);
  const {
    selectedWorksheet, disabledExport, csvData, baseFileName
  } = props;

  const createWorksheet = () => {
    setIsOpen(true);
    setIsCreation(true);
  };

  const modifyWorksheet = () => {
    setIsOpen(true);
    setIsCreation(false);
  };

  const closeDialog = () => {
    setIsOpen(false);
    setIsCreation(false);
  };

  const openDeleteAttention = () => {
    setIsDeleteAttention(true);
  };

  const closeDeleteAttention = () => {
    setIsDeleteAttention(false);
  };

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">
          {I18n.t('settingsWorksheets.title')}
        </Typography>
      </div>

      <div className={styles.headerButtonsContainer}>
        <InlineButton buttons={[
          {
            _type: 'string',
            size: 'medium',
            text: I18n.t('settingsWorksheets.buttons.newWorksheet'),
            type: 'submit',
            onClick: createWorksheet
          },
          {
            _type: 'string',
            size: 'medium',
            text: I18n.t('settingsWorksheets.buttons.edit'),
            onClick: modifyWorksheet,
            disabled: _.isEmpty(selectedWorksheet)
          },
          {
            _type: 'string',
            size: 'medium',
            text: I18n.t('settingsWorksheets.buttons.remove'),
            color: 'error',
            disabled: _.isEmpty(selectedWorksheet),
            onClick: openDeleteAttention
          },
          {
            _type: 'component',
            color: 'primary',
            disabled: disabledExport,
            component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
          }]}
        />
      </div>
      <WorksheetsDialog
        isOpen={isOpen}
        creationMode={isCreation}
        onClose={closeDialog}
        isCreation
      />
      <WorksheetsDeleteDialog
        isOpen={isDeleteAttention}
        onClose={closeDeleteAttention}
        isPossible={false}
      />
    </div>
  );
};

SettingsWorksheetsHeader.propTypes = {
  selectedWorksheet: PropTypes.array.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired
};

export default SettingsWorksheetsHeader;
