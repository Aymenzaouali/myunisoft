import React, { useState } from 'react';
import {
  Typography
} from '@material-ui/core';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import { PortfolioDialog } from 'containers/groups/Dialogs';
import DeleteConfirmation from 'containers/groups/Dialogs/DeleteConfirmation';
import styles from './portfolioListViewHeader.module.scss';

const PortfolioListViewHeader = ({
  deleteWallets,
  selectedWallets,
  walletSelectedLine,
  disabledExport,
  csvData,
  baseFileName
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isCreation, setIsCreation] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);

  const closeDialog = () => {
    setIsOpen(false);
    setIsCreation(false);
  };

  const createPortfolio = () => {
    setIsOpen(true);
    setIsCreation(true);
  };

  const modifyPortfolio = () => {
    setIsOpen(true);
    setIsCreation(false);
  };

  const onCloseDeleteDialog = () => setIsDeleteOpen(false);

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">
          {I18n.t('portfolioListView.title')}
        </Typography>
        <div>
          {/* <InlineButton buttons={
            [
              {
                _type: 'icon',
                variant: 'none',
                iconColor: 'black',
                iconName: 'icon-popup',
                iconSize: 24,
                titleInfoBulle: I18n.t('tooltips.newWindow')
              },
              {
                _type: 'icon',
                variant: 'none',
                iconColor: 'black',
                iconName: 'icon-close',
                iconSize: 24,
                titleInfoBulle: I18n.t('tooltips.close')
              }
            ]}
          /> */}
        </div>
      </div>
      <div className={styles.headerButtonContainer}>
        <InlineButton buttons={[
          {
            _type: 'string',
            size: 'medium',
            text: I18n.t('portfolioListView.headerBtns.new'),
            onClick: createPortfolio
          },
          {
            _type: 'string',
            size: 'medium',
            disabled: !walletSelectedLine,
            text: I18n.t('portfolioListView.headerBtns.modify'),
            onClick: modifyPortfolio
          },
          {
            _type: 'string',
            size: 'medium',
            disabled: _.isEmpty(selectedWallets),
            text: I18n.t('portfolioListView.headerBtns.delete'),
            color: 'error',
            onClick: () => setIsDeleteOpen(true)
          },
          {
            _type: 'component',
            color: 'primary',
            disabled: disabledExport,
            component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
          }
        ]}
        />
      </div>
      <PortfolioDialog
        isOpen={isOpen}
        creationMode={isCreation}
        onClose={closeDialog}
        isCreation
      />

      <DeleteConfirmation
        deleteReportAndForm={deleteWallets}
        isOpen={isDeleteOpen}
        onClose={onCloseDeleteDialog}
      />
    </div>
  );
};


PortfolioListViewHeader.propTypes = {
  deleteWallets: PropTypes.func.isRequired,
  selectedWallets: PropTypes.shape({}).isRequired,
  walletSelectedLine: PropTypes.number.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired
};

export default PortfolioListViewHeader;
