import React, { useState } from 'react';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import { MailParagraphDialog, MailParagraphDeleteDialog } from 'containers/groups/Dialogs';
import PropTypes from 'prop-types';
import styles from './mailParagraphTableHeader.module.scss';

const MailParagraphTableHeader = ({
  selectedRowsCount,
  setParagraphsVisible,
  disabledExport,
  csvData,
  disableDelete,
  baseFileName
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isCreation, setIsCreation] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);

  const closeDeleteMailSettings = () => {
    setIsDeleteOpen(false);
  };

  const closeDialog = () => {
    setIsOpen(false);
    setIsCreation(false);
  };

  const createMailSetting = () => {
    setIsOpen(true);
    setIsCreation(true);
  };

  const modifyMailSetting = () => {
    setIsOpen(true);
    setIsCreation(false);
  };

  return (
    <>
      <div className={styles.headerContainer}>
        <div className={styles.btnControlContainer}>
          <InlineButton buttons={
            [
              {
                _type: 'icon',
                variant: 'none',
                iconColor: 'black',
                iconName: 'icon-close',
                iconSize: 24,
                titleInfoBulle: I18n.t('tooltips.close'),
                onClick: () => setParagraphsVisible(false)
              }
            ]}
          />
        </div>
      </div>
      <div className={styles.headerButtonContainer}>
        <InlineButton buttons={[
          {
            _type: 'string',
            size: 'medium',
            onClick: createMailSetting,
            text: I18n.t('settingStandardMail.createNewParagraph')
          },
          {
            _type: 'string',
            size: 'medium',
            onClick: modifyMailSetting,
            disabled: selectedRowsCount !== 1,
            text: I18n.t('settingStandardMail.modify')
          },
          {
            _type: 'string',
            size: 'medium',
            text: I18n.t('settingStandardMail.delete'),
            color: 'error',
            disabled: disableDelete,
            onClick: () => setIsDeleteOpen(true)
          },
          {
            _type: 'component',
            color: 'primary',
            disabled: disabledExport,
            component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
          }
        ]}
        />
      </div>
      <MailParagraphDialog
        isOpen={isOpen}
        creationMode={isCreation}
        onClose={closeDialog}
        isCreation
      />
      <MailParagraphDeleteDialog
        isOpen={isDeleteOpen}
        onClose={closeDeleteMailSettings}
      />
    </>
  );
};

MailParagraphTableHeader.propTypes = {
  selectedRowsCount: PropTypes.shape({}),
  disabledExport: PropTypes.bool.isRequired,
  disableDelete: PropTypes.bool,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired,
  setParagraphsVisible: PropTypes.func.isRequired
};

MailParagraphTableHeader.defaultProps = {
  selectedRowsCount: 0,
  disableDelete: false
};

export default MailParagraphTableHeader;
