import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Field, Form, Fields } from 'redux-form';
import {
  Typography,
  withStyles
} from '@material-ui/core';
import {
  ReduxTextField,
  ReduxDatePicker,
  AutoComplete
} from 'components/reduxForm/Inputs';
import { ReduxRadio } from 'components/reduxForm/Selections';
import FilesDropperDialog from 'components/groups/Dialogs/FilesDropper';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';
import { splitShape } from 'context/SplitContext';
import { roundNumber, formatNumber } from 'helpers/number';
import I18n from 'assets/I18n';
import WindowHandler from 'helpers/window';
import Loader from 'components/basics/Loader';
import _ from 'lodash';
import moment from 'moment';
import styles from './banklink.module.scss';

const BankLinkHeader = ({
  societyId,
  dottedMode,
  getEntries,
  printBankLink,
  bankCode,
  formValues,
  dottedLines,
  undottedLines,
  postValidate,
  undotted_balance,
  fillValues,
  bankLinkData,
  addPJBankLink,
  pjList,
  getDiaries,
  getDate,
  isDateLoading,
  autofill,
  id_diligence,
  closed_diligence,
  csvData,
  baseFileName,
  classes,
  resetAllBills,
  _updateBankBalance,
  split,
  updateDiaryOnSplit
}) => {
  const [disableValidation, setDisableValidation] = useState(false);
  const [isPJOpen, setIsPJOpen] = useState(false);

  const handleEntries = async (date = undefined) => {
    const data = await getEntries(date);
    const { response } = data;

    if (data.line_writing || (response && response.status.toString().substring(0, 1) !== '4')) setDisableValidation(false);

    autofill('accounting_balance', formatNumber(_.get(data, 'accounting_balance')));
    autofill('undotted_balance', formatNumber(_.get(data, 'undotted_balance')));
    autofill('gap', formatNumber(_.get(data, 'gap')));
    autofill('bank_balance', formatNumber(_.get(data, 'bank_balance')));
    await resetAllBills();
  };
  const handleData = async () => {
    const data = await getDate();
    const date = _.get(data, 'date') || moment();
    const [year, month, day] = moment(date).format('YYYY-MM-DD').split('-');
    autofill('year', year);
    autofill('month', month);
    autofill('day', day);
    const diaries = await getDiaries();
    if (diaries.length > 0) {
      const firstBankDiary = diaries[0];
      autofill('bank', { label: firstBankDiary.code, value: firstBankDiary });
      autofill('accountNumber', _.get(firstBankDiary, 'account.number'));
    }
    await handleEntries(date);
  };

  useEffect(() => {
    handleData();
  }, []);

  const computeGap = () => {
    const th_balance = (parseFloat(roundNumber(formValues.accounting_balance))
      - undotted_balance) || 0;
    const bank_balance = parseFloat(roundNumber(formValues.bank_balance)).toFixed(2);
    const formated_bank_balance = bank_balance !== 'NaN' ? bank_balance : 0;
    const gap = th_balance - formated_bank_balance;
    autofill('gap', formatNumber(gap));
  };

  const updateBankBalance = key => (val) => {
    const reg = new RegExp('^[0-9]*[,][0-9]+$');
    const value = formatNumber(formValues[key]);
    const currentValue = val.target.value;
    if (!formValues[key] || !reg.test(value.replace(/\s/g, ''))) {
      autofill('bank_balance', formatNumber(0));
    } else {
      autofill('bank_balance', formatNumber(currentValue.replace(',', '.')));
      _updateBankBalance();
    }
    computeGap();
  };

  const onClickPrint = (societyId, date, diary, bank_balance, dotted) => () => {
    printBankLink(societyId, date, diary, bank_balance, dotted);
  };

  const updateAccount = async (diary) => {
    autofill('accountNumber', _.get(diary, 'value.account.number'));
    handleEntries();
  };

  const validateBankLink = (
    diary,
    balance_date,
    balance
  ) => () => {
    postValidate(
      balance_date,
      balance,
      diary
    );
  };

  const numberPj = pjList.length;

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">
          {I18n.t('bankLink.title')}
        </Typography>
      </div>
      <div className={styles.filterButtons}>
        <SplitToolbar onSplitOpen={fillValues} />
      </div>
      <Form>
        <div className={styles.row}>
          <div className={styles.textDate}>
            {I18n.t('bankLinkFilter.dateText')}
          </div>
          <div className={styles.row}>
            <Fields
              component={ReduxDatePicker}
              names={['day', 'month', 'year']}
              onCustomChange={() => {
                setDisableValidation(true);
              }}
              onDayBlur={() => {
                handleEntries();
              }}
              onMonthBlur={() => {
                handleEntries();
              }}
              onYearBlur={() => {
                handleEntries();
              }}
              className={styles.datePicker}
            />
            {isDateLoading && <div className={styles.loader}><Loader size={14} /></div>}
          </div>
          <Field
            name="bank"
            label={I18n.t('bankLinkFilter.bank')}
            component={AutoComplete}
            classes={{ root: classes.autocomplete }}
            options={bankCode}
            onChangeValues={(val) => {
              updateAccount(val);
              if (split.master) {
                updateDiaryOnSplit(val.value);
              }
            }}
          />
          <Field
            component={ReduxTextField}
            name="accountNumber"
            className={styles.accountNumber}
            label={I18n.t('bankLinkFilter.accountNumber')}
            disabled
          />

          <Field
            component={ReduxTextField}
            name="bank_balance"
            className={styles.countSold}
            InputProps={{
              classes: { input: classes.isAmount }
            }}
            label={I18n.t('bankLinkFilter.bankSold')}
            onCustomBlur={updateBankBalance('bank_balance')}
          />

          <Field
            component={ReduxTextField}
            name="accounting_balance"
            className={styles.countSold}
            InputProps={{
              classes: { input: classes.isAmount }
            }}
            label={I18n.t('bankLinkFilter.countSold')}
            disabled
          />

          <Field
            component={ReduxTextField}
            name="undotted_balance"
            className={styles.countSold}
            InputProps={{
              classes: { input: classes.isAmount }
            }}
            label={I18n.t('bankLinkFilter.unpointedSold')}
            disabled
          />
          <InlineButton buttons={
            [
              {
                _type: 'icon',
                iconName: 'icon-attachment',
                iconSize: 32,
                iconColor: 'black',
                variant: 'text',
                titleInfoBulle: I18n.t('tooltips.viewDoc'),
                onClick: () => WindowHandler.forceOpen(pjList),
                disabled: _.isEmpty(pjList),
                badgeContent: numberPj,
                color: 'inherit'
              },
              {
                _type: 'icon',
                iconName: 'icon-attachment',
                iconSize: 32,
                titleInfoBulle: I18n.t('tooltips.importDoc'),
                onClick: () => setIsPJOpen(true),
                disabled: bankLinkData && !bankLinkData.line_writing
              }
            ]
          }
          />
          <Field
            component={ReduxTextField}
            name="gap"
            className={styles.gap}
            InputProps={{
              classes: { input: classes.isAmount }
            }}
            label={I18n.t('bankLinkFilter.gap')}
            disabled
          />

          <InlineButton buttons={
            [
              {
                _type: 'string',
                variant: 'contained',
                color: 'primary',
                size: 'Medium',
                text: I18n.t('bankLinkFilter.validationButton'),
                onClick: validateBankLink(
                  _.get(formValues, 'bank.value.diary_id'),
                  `${formValues.year}${formValues.month}${formValues.day}`,
                  formValues.bank_balance,
                  `${formValues.month}`,
                  `${formValues.year}`,
                  dottedLines,
                  undottedLines
                ),
                disabled: closed_diligence
                  || disableValidation
                  || (Math.abs(parseFloat(roundNumber(formValues.gap))) > 0.01)
                  || (bankLinkData && !bankLinkData.line_writing)
              }
            ]
          }
          />
        </div>
        <div className={styles.row}>
          <Field
            name="dotted"
            color="primary"
            component={ReduxRadio}
            list={dottedMode}
            row
            onCustomChange={() => {
              handleEntries();
            }}
          />
          <div>
            <InlineButton buttons={
              [
                {
                  _type: 'icon',
                  iconName: 'icon-print',
                  iconSize: 32,
                  titleInfoBulle: I18n.t('tooltips.print'),
                  disabled: bankLinkData && !bankLinkData.line_writing,
                  onClick: onClickPrint(
                    societyId,
                    `${formValues.year}${formValues.month}${formValues.day}`,
                    _.get(formValues, 'bank.value.diary_id'),
                    roundNumber(formValues.bank_balance),
                    formValues.checkBox
                  )
                },
                {
                  _type: 'component',
                  size: 'medium',
                  color: 'primary',
                  component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
                }
              ]
            }
            />
          </div>
          <FilesDropperDialog
            open={isPJOpen}
            onClose={() => setIsPJOpen(false)}
            field="rapprochementPJ"
            onAddFiles={file => addPJBankLink(file, `${formValues.year}${formValues.month}${formValues.day}`, id_diligence)}
          />
        </div>
      </Form>
    </div>
  );
};
BankLinkHeader.propTypes = {
  fillValues: PropTypes.func,
  csvData: PropTypes.array.isRequired,
  pjList: PropTypes.array.isRequired,
  bankLinkData: PropTypes.array.isRequired,
  societyId: PropTypes.string.isRequired,
  baseFileName: PropTypes.string.isRequired,
  id_diligence: PropTypes.string.isRequired,
  closed_diligence: PropTypes.bool.isRequired,
  selectedBills: PropTypes.shape({}).isRequired,
  getEntries: PropTypes.func.isRequired,
  getDiaries: PropTypes.func.isRequired,
  autofill: PropTypes.func.isRequired,
  getDate: PropTypes.func.isRequired,
  addPJBankLink: PropTypes.func.isRequired,
  printBankLink: PropTypes.func.isRequired,
  resetAllBills: PropTypes.func.isRequired,
  _updateBankBalance: PropTypes.func.isRequired,
  formValues: PropTypes.shape({}),
  postValidate: PropTypes.func.isRequired,
  dottedLines: PropTypes.shape({}).isRequired,
  undottedLines: PropTypes.shape({}).isRequired,
  undotted_balance: PropTypes.number.isRequired,
  form: PropTypes.shape({}).isRequired,
  bankCode: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  dottedMode: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number,
    label: PropTypes.string
  })).isRequired,
  isDateLoading: PropTypes.bool.isRequired,
  split: splitShape.isRequired,
  updateDiaryOnSplit: PropTypes.func.isRequired
};

BankLinkHeader.defaultProps = {
  formValues: undefined,
  fillValues: undefined
};

const themeStyles = () => ({
  isAmount: {
    textAlign: 'right',
    paddingRight: 10
  },
  tooltips: {
    border: 'none',
    height: 15,
    width: 'auto',
    padding: '4px 10px',
    borderRadius: 2,
    fontSize: 10,
    fontWeight: 500,
    boxShadow: '1px 5px 7px 0 rgba(0, 0, 0, 0.08)',
    backgroundColor: '#ffffff',
    color: '#464545',
    fontFamily: 'basier_circlemedium'
  },
  autocomplete: {
    minWidth: 80,
    marginBottom: 8
  }
});

export default withStyles(themeStyles)(BankLinkHeader);
