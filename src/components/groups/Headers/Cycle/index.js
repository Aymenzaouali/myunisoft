import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import CurrentEditionsFiltersDateFields from 'components/groups/CurrentEditions/CurrentEditionsFiltersDateFields';
import AutoComplete from 'components/basics/Inputs/AutoComplete';
import ReduxCheckBox from 'components/reduxForm/Selections/ReduxCheckBox';
import { Field } from 'redux-form';

import styles from './CycleHeader.module.scss';

const CycleHeader = (props) => {
  const {
    printOrDownloadCycleForms,
    dateStart, dateEnd,
    exercises, exercise,
    deadline, deadlines,
    change,
    getExercises,
    getDeadline,
    getCycleForms,
    sendCycleForms,
    sendCycleFormsEdi,
    validateForm,
    isNeant,
    isNothingness,
    changeIsNothingness
  } = props;

  useEffect(() => {
    getExercises();
  }, []);

  useEffect(() => {
    getDeadline();
  }, [exercise]);

  useEffect(() => {
    getCycleForms();
  }, [deadline]);

  const handleExerciseOnChange = (e) => {
    change('exercise', e.value);
  };

  const handleDeadlineOnChange = (e) => {
    change('deadline', e.value);
  };

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">{I18n.t('tva.title')}</Typography>
        <div>
          <InlineButton buttons={
            [
              {
                _type: 'string', variant: 'contained', color: 'secondary', text: I18n.t('tva.control'), colorError: true
              },
              {
                _type: 'icon',
                iconName: 'icon-upload',
                iconSize: 28,
                titleInfoBulle: I18n.t('tooltips.download'),
                onClick: () => printOrDownloadCycleForms('download')
              },
              {
                _type: 'icon',
                iconName: 'icon-print',
                iconSize: 28,
                titleInfoBulle: I18n.t('tooltips.print'),
                onClick: () => printOrDownloadCycleForms('print')
              },
              {
                _type: 'string', color: 'secondary', text: I18n.t('tva.help'), colorError: true
              }
            ]}
          />
        </div>
      </div>
      <div className={styles.filter}>
        <CurrentEditionsFiltersDateFields
          disabled
          to={dateEnd}
          from={dateStart}
          onChange={() => {}}
        />
        <AutoComplete
          textFieldProps={{
            label: I18n.t('currentEditions.filters.exercice'),
            InputLabelProps: {
              shrink: true
            }
          }}
          className={styles.select}
          onChangeValues={handleExerciseOnChange}
          options={exercises}
          value={[exercise].map(e => exercises.find(v => v.value === e))}
        />
        <AutoComplete
          textFieldProps={{
            label: I18n.t('currentEditions.filters.deadline'),
            InputLabelProps: {
              shrink: true
            }
          }}
          className={styles.select}
          onChangeValues={handleDeadlineOnChange}
          options={deadlines}
          value={[deadline].map(e => deadlines.find(v => v.value === e))}
        />
        {isNeant && (
          <Field
            name="isNothingness"
            component={ReduxCheckBox}
            label={I18n.t('tva.nothingness')}
            color="primary"
            type="boolean"
            checked={isNothingness}
            onClick={(() => changeIsNothingness(!isNothingness))}
          />
        )}
        <div className={styles.buttons}>
          <InlineButton buttons={
            [
              {
                _type: 'icon',
                iconName: 'icon-save',
                iconSize: 19,
                titleInfoBulle: I18n.t('tooltips.tvasave'),
                onClick: () => {
                  validateForm();
                  sendCycleForms();
                }
              },
              { _type: 'string', text: I18n.t('tva.send'), onClick: () => sendCycleFormsEdi() }
            ]}
          />
        </div>
      </div>
    </div>
  );
};

CycleHeader.propTypes = {
  getDeadline: PropTypes.func.isRequired,
  getCycleForms: PropTypes.func.isRequired,
  validateForm: PropTypes.func.isRequired,
  sendCycleForms: PropTypes.func.isRequired,
  sendCycleFormsEdi: PropTypes.func.isRequired,
  printOrDownloadCycleForms: PropTypes.func.isRequired,
  deadlines: PropTypes.array.isRequired,
  deadline: PropTypes.string.isRequired,
  exercises: PropTypes.array.isRequired,
  exercise: PropTypes.string.isRequired,
  dateStart: PropTypes.string.isRequired,
  dateEnd: PropTypes.string.isRequired,
  change: PropTypes.func.isRequired,
  getExercises: PropTypes.func.isRequired,
  changeIsNothingness: PropTypes.func,
  isNeant: PropTypes.bool,
  isNothingness: PropTypes.number
};

CycleHeader.defaultProps = {
  isNeant: false,
  isNothingness: 0,
  changeIsNothingness: () => {}
};

export default CycleHeader;
