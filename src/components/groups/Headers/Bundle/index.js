import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { InlineButton } from 'components/basics/Buttons';
import CurrentEditionsFiltersDateFields from 'components/groups/CurrentEditions/CurrentEditionsFiltersDateFields';
import AutoComplete from 'components/basics/Inputs/AutoComplete';
import Statements from 'containers/groups/Dialogs/Statements';

import I18n from 'assets/I18n';
import styles from './BundleHeaders.module.scss';

const Bundle = (props) => {
  const {
    title,
    dateStart, dateEnd,
    exercises, exercise,
    deadline, deadlines,
    getDeadline,
    getBundleForms,
    sendBundleForms,
    sendBundleFormsEdi,
    printOrDownloadBundle,
    getExercises, change,
    getBundleStatements,
    sendBundleStatements,
    getTaxForms,
    refreshAutoIS,
    code_sheet_group
  } = props;

  useEffect(() => {
    if (code_sheet_group) {
      getExercises();
    }
  }, []);

  useEffect(() => {
    if (exercise) {
      getDeadline();
    }
  }, [exercise]);

  useEffect(() => {
    if (deadline) {
      getBundleForms();
      getTaxForms();
    }
  }, [deadline]);

  const handleExerciseOnChange = (e) => {
    change('exercise', e.value);
  };

  const [statementShow, setStatementShow] = useState(false);

  const handleDeadlineOnChange = (e) => {
    change('deadline', e.value);
  };

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">{title}</Typography>
        <div>
          <InlineButton buttons={
            [
              {
                _type: 'icon', iconName: 'icon-upload', iconSize: 28, titleInfoBulle: I18n.t('tooltips.download'), onClick: () => printOrDownloadBundle('download')
              },
              {
                _type: 'icon', iconName: 'icon-print', iconSize: 28, titleInfoBulle: I18n.t('tooltips.print'), onClick: () => printOrDownloadBundle('print')
              },
              {
                _type: 'string', color: 'secondary', text: I18n.t('tva.help'), colorError: true
              },
              {
                _type: 'icon', variant: 'none', iconColor: 'black', iconName: 'icon-popup', iconSize: 24, titleInfoBulle: I18n.t('tooltips.newWindow'), colorError: true
              },
              {
                _type: 'icon', variant: 'none', iconColor: 'black', iconName: 'icon-close', iconSize: 24, titleInfoBulle: I18n.t('tooltips.close'), colorError: true
              },
              {
                _type: 'string', color: 'primary', text: I18n.t('tooltips.statement'), colorError: true, onClick: () => setStatementShow(true)
              }
            ]}
          />
        </div>
      </div>
      <div className={styles.filter}>
        <CurrentEditionsFiltersDateFields
          disabled
          to={dateEnd}
          from={dateStart}
          onChange={() => {}}
        />
        <AutoComplete
          textFieldProps={{
            label: I18n.t('currentEditions.filters.exercice'),
            InputLabelProps: {
              shrink: true
            }
          }}
          className={styles.select}
          onChangeValues={handleExerciseOnChange}
          options={exercises}
          value={[exercise].map(e => exercises.find(v => v.value === e))}
        />
        <AutoComplete
          textFieldProps={{
            label: I18n.t('currentEditions.filters.deadline'),
            InputLabelProps: {
              shrink: true
            }
          }}
          className={styles.select}
          onChangeValues={handleDeadlineOnChange}
          options={deadlines}
          value={[deadline].map(e => deadlines.find(v => v.value === e))}
        />
        <div className={styles.buttons}>
          <InlineButton buttons={
            [
              {
                _type: 'icon',
                iconName: 'icon-refresh',
                iconSize: 19,
                titleInfoBulle: I18n.t('tooltips.refresh'),
                onClick: refreshAutoIS
              },
              {
                _type: 'icon',
                iconName: 'icon-save',
                iconSize: 19,
                titleInfoBulle: I18n.t('tooltips.tvasave'),
                onClick: sendBundleForms
              },
              { _type: 'string', text: I18n.t('tva.send'), onClick: sendBundleFormsEdi }
            ]}
          />
        </div>
      </div>
      <Statements
        type={code_sheet_group}
        getBundleStatements={getBundleStatements}
        sendBundleStatements={sendBundleStatements}
        isOpen={statementShow}
        onClose={() => setStatementShow(false)}
        total={12}
      />
    </div>
  );
};

Bundle.propTypes = {
  getExercises: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired,
  getDeadline: PropTypes.func.isRequired,
  getTaxForms: PropTypes.func.isRequired,
  getBundleForms: PropTypes.func.isRequired,
  refreshAutoIS: PropTypes.func.isRequired,
  sendBundleForms: PropTypes.func.isRequired,
  sendBundleFormsEdi: PropTypes.func.isRequired,
  printOrDownloadBundle: PropTypes.func.isRequired,
  getBundleStatements: PropTypes.func.isRequired,
  sendBundleStatements: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  code_sheet_group: PropTypes.string.isRequired,
  dateStart: PropTypes.string.isRequired,
  dateEnd: PropTypes.string.isRequired,
  deadline: PropTypes.string.isRequired,
  exercise: PropTypes.string.isRequired,
  exercises: PropTypes.array.isRequired,
  deadlines: PropTypes.array.isRequired
};

export default Bundle;
