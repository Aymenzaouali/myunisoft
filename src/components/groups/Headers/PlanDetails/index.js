import React, { Fragment } from 'react';
import { Typography } from '@material-ui/core';

import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';

import I18n from 'assets/I18n';
import styles from './planDetails.module.scss';

const PlanDetailsHeader = () => (
  <Fragment>
    <div className={styles.headerUp}>
      <div className={styles.headerTitle}>
        <Typography variant="h1">
          {I18n.t('accountingPlans.tables.planDetails.title')}
        </Typography>
      </div>
      <SplitToolbar />
    </div>
  </Fragment>
);

PlanDetailsHeader.propTypes = {};

PlanDetailsHeader.defaultProps = {};

export default PlanDetailsHeader;
