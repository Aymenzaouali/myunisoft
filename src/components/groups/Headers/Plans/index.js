import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import I18n from 'assets/I18n';
import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';
import styles from './plans.module.scss';

const PlansHeader = ({ fillValues }) => (
  <div className={styles.headerUp}>
    <div className={styles.headerTitle}>
      <Typography variant="h1">
        {I18n.t('accountingPlans.title')}
      </Typography>
    </div>
    <div className={styles.filterButtons}>
      <SplitToolbar onSplitOpen={fillValues} />
    </div>
  </div>
);

PlansHeader.propTypes = {
  fillValues: PropTypes.func
};

PlansHeader.defaultProps = {
  fillValues: undefined
};

export default PlansHeader;
