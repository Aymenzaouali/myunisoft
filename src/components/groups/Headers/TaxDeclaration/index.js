import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import CurrentEditionsFiltersDateFields from 'components/groups/CurrentEditions/CurrentEditionsFiltersDateFields';
import AutoComplete from 'components/basics/Inputs/AutoComplete';

import styles from './TaxDeclarationHeaders.module.scss';

const Tax = (props) => {
  const {
    dateStart, dateEnd, exercises, exercise,
    getTaxForm, sendTaxForm, printOrDownloadTax, sendTaxFormEdi,
    getControls,
    refreshAuto,
    getExercises, change,
    title = I18n.t('taxDeclaration.title')
  } = props;

  useEffect(() => {
    getTaxForm();
  }, [exercise]);

  useEffect(() => {
    getExercises();
  }, []);

  const handleAutocompleteOnChange = (e) => {
    change('exercise', e.value);
  };

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <Typography variant="h1">{title}</Typography>
        <div>
          <InlineButton buttons={
            [
              {
                _type: 'string', variant: 'outlined', color: 'none', text: I18n.t('cvae.control'), onClick: () => getControls('download'), disabled: !exercise
              },
              {
                _type: 'icon', iconName: 'icon-upload', iconSize: 28, titleInfoBulle: I18n.t('tooltips.download'), onClick: () => printOrDownloadTax('download')
              },
              {
                _type: 'icon', iconName: 'icon-print', iconSize: 28, titleInfoBulle: I18n.t('tooltips.print'), onClick: () => printOrDownloadTax('print')
              },
              {
                _type: 'string', color: 'secondary', text: I18n.t('tva.help'), colorError: true
              },
              {
                _type: 'icon', variant: 'none', iconColor: 'black', iconName: 'icon-popup', iconSize: 24, titleInfoBulle: I18n.t('tooltips.newWindow'), colorError: true
              },
              {
                _type: 'icon', variant: 'none', iconColor: 'black', iconName: 'icon-close', iconSize: 24, titleInfoBulle: I18n.t('tooltips.close'), colorError: true
              }
            ]}
          />
        </div>
      </div>
      <div className={styles.filter}>
        <CurrentEditionsFiltersDateFields
          disabled
          to={dateEnd}
          from={dateStart}
          onChange={() => {}}
        />
        <AutoComplete
          textFieldProps={{
            label: I18n.t('currentEditions.filters.exercice'),
            InputLabelProps: {
              shrink: true
            }
          }}
          className={styles.select}
          onChangeValues={handleAutocompleteOnChange}
          options={exercises}
          value={[exercise].map(e => exercises.find(v => v.value === e))}
        />
        <div className={styles.buttons}>
          <InlineButton buttons={[
            {
              _type: 'icon',
              iconName: 'icon-refresh',
              iconSize: 19,
              titleInfoBulle: I18n.t('tooltips.refresh'),
              onClick: refreshAuto
            },
            {
              _type: 'icon',
              iconName: 'icon-save',
              iconSize: 19,
              titleInfoBulle: I18n.t('tooltips.tvasave'),
              onClick: sendTaxForm
            },
            { _type: 'string', text: I18n.t('tva.send'), onClick: sendTaxFormEdi }
          ]}
          />
        </div>
      </div>
    </div>
  );
};

Tax.propTypes = {
  getExercises: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired,
  getTaxForm: PropTypes.func.isRequired,
  sendTaxForm: PropTypes.func.isRequired,
  refreshAuto: PropTypes.func.isRequired,
  getControls: PropTypes.func.isRequired,
  printOrDownloadTax: PropTypes.func.isRequired,
  sendTaxFormEdi: PropTypes.func.isRequired,
  dateStart: PropTypes.string.isRequired,
  exercise: PropTypes.string.isRequired,
  dateEnd: PropTypes.string.isRequired,
  exercises: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired
};

export default Tax;
