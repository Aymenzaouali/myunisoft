import React, { useState } from 'react';
import {
  Typography
} from '@material-ui/core';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';
import { SettingStandardMailDialog, SettingStandardMailDeleteDialog } from 'containers/groups/Dialogs';
import styles from './settingStandardMailHeader.module.scss';

const SettingStandardMailHeader = ({
  selectedRowsCount,
  isDeletable,
  disabledExport,
  csvData,
  baseFileName
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isCreation, setIsCreation] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);

  const closeDeleteMailSettings = () => {
    setIsDeleteOpen(false);
  };

  const closeDialog = () => {
    setIsOpen(false);
    setIsCreation(false);
  };

  const createMailSetting = () => {
    setIsOpen(true);
    setIsCreation(true);
  };

  const modifyMailSetting = () => {
    setIsOpen(true);
    setIsCreation(false);
  };

  return (
    <>
      <div className={styles.headerContainer}>
        <Typography variant="h1">
          {I18n.t('settingStandardMail.title')}
        </Typography>
        <div className={styles.btnControlContainer}>
          <InlineButton buttons={
            [
              {
                _type: 'icon', variant: 'text', iconColor: 'black', iconName: 'icon-popup', iconSize: 24, titleInfoBulle: I18n.t('tooltips.newWindow')
              },
              {
                _type: 'icon', variant: 'text', iconColor: 'black', iconName: 'icon-close', iconSize: 24, titleInfoBulle: I18n.t('tooltips.close')
              }
            ]}
          />
        </div>
      </div>
      <div className={styles.headerButtonContainer}>
        <InlineButton buttons={[
          {
            _type: 'string',
            size: 'medium',
            onClick: createMailSetting,
            text: I18n.t('settingStandardMail.createNew')
          },
          {
            _type: 'string',
            size: 'medium',
            onClick: modifyMailSetting,
            disabled: !selectedRowsCount || selectedRowsCount > 1,
            text: I18n.t('settingStandardMail.modify')
          },
          {
            _type: 'string',
            size: 'medium',
            disabled: isDeletable,
            text: I18n.t('settingStandardMail.delete'),
            color: 'error',
            onClick: () => setIsDeleteOpen(true)
          },
          {
            _type: 'component',
            color: 'primary',
            disabled: disabledExport,
            component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
          }
        ]}
        />
      </div>
      <SettingStandardMailDialog
        isOpen={isOpen}
        creationMode={isCreation}
        onClose={closeDialog}
        isCreation
      />
      <SettingStandardMailDeleteDialog
        isOpen={isDeleteOpen}
        onClose={closeDeleteMailSettings}
      />
    </>
  );
};

SettingStandardMailHeader.propTypes = {
  selectedRowsCount: PropTypes.number.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired,
  isDeletable: PropTypes.number.isRequired
};


export default SettingStandardMailHeader;
