import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// import I18n from 'assets/I18n';
import { Route, Switch } from 'react-router';
import { withSnackbar } from 'notistack';
import { Avatar, Button as MaterialButton, Tooltip } from '@material-ui/core';
import deepPurple from '@material-ui/core/colors/deepPurple';
import { withStyles } from '@material-ui/core/styles';
import { reduxForm } from 'redux-form';
import DrawerContainer from 'containers/groups/Drawers';
import SocietyPopoverContainer from 'containers/groups/Popover/Societies';
import PopoverAvatar from 'containers/groups/Popover/Avatar';
import { Shortcuts } from 'react-shortcuts';
import Button from 'components/basics/Buttons/Button';
import { FontIcon } from 'components/basics/Icon';
// import CloseIcon from '@material-ui/icons/Close';
import _ from 'lodash';
import routes, { getRouteByKey, buildSlaveRoute } from 'helpers/routes';
import { ShortcurtsListDialog, AccessDashboard } from 'containers/groups/Dialogs';
import { TabsButton } from 'components/basics/Buttons';
import SocketService from 'common/helpers/socket';
import './styles.scss';
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

// TODO : A ajouter quand finalisé avec Candice
// import { Field } from 'redux-form';
// import { ReduxTextField } from 'components/reduxForm/Inputs';
// import I18n from 'assets/I18n';

const STICKY_ROUTES = [
  'declarationTva',
  'advanceCvae',
  'declarationCvae',
  'declarationTvaCaAnnual',
  'declarationTvaCaAdvance',
  'declarationIs',
  'liquidationIs',
  'taxDeclaration',
  'rentDeclarations',
  'CIRefund'
];

const NavBar = (props) => {
  const {
    tabs,
    classes,
    push,
    routeKey,
    societyId,
    navBarButtons,
    user,
    getRights,
    getSociety,
    location,
    match,
    addTabs,
    initUnreadMessagesStat
  } = props;

  // States
  const [isSocietyModalOpen, setIsSocietyModalOpen] = useState(false);
  const [isShortcutsModalOpen, setIsShortcutsModalOpen] = useState(false);
  const [anchorPosition, setAnchorPosition] = useState(null);
  const [anchorAvatar, setAnchorAvatar] = useState(null);
  const [isAvatarOpen, setIsAvatarOpen] = useState(false);
  const [soc, setSoc] = useState({});
  const [isOpenAccess, setIsOpenAccess] = useState(false);

  const checkIfExistsInOpenTabs = () => {
    const current_society_id = _.get(match, 'params.id');
    return tabs.find(
      t => parseInt(t.id, 10) === parseInt(current_society_id, 10)
    );
  };

  const checkIfSocietyExistsInTabs = async () => {
    const current_society_id = _.get(match, 'params.id');
    const existsInOpenTabs = checkIfExistsInOpenTabs();
    if (!existsInOpenTabs) {
      const response = await getSociety();
      const societies = _.get(response, 'data.society_array', []);
      const society = societies.find(s => s.society_id === parseInt(current_society_id, 10));
      if (society) {
        if (society.secured) {
          setSoc(society);
          setIsOpenAccess(true);
        } else {
          addTabs([{
            id: society.society_id,
            label: society.name,
            path: location.pathname,
            isClosable: true,
            secured: society.secured
          }]);
        }
      } else {
        // TODO: Is it suppose to display any error / redirect if society is not found?
      }
    }
  };

  const onValidateAccess = () => {
    const existsInOpenTabs = checkIfExistsInOpenTabs();
    if (!existsInOpenTabs) {
      addTabs([{
        id: soc.society_id,
        label: soc.name,
        path: location.pathname,
        isClosable: true,
        secured: soc.secured
      }]);
    }
    setIsOpenAccess(false);
  };

  const onCloseAccess = () => {
    setIsOpenAccess(false);
    push('/tab/collab/dashBoard');
  };

  useEffect(() => {
    getRights();
    initUnreadMessagesStat();
    SocketService.init();
    checkIfSocietyExistsInTabs();
  }, []);

  // Functions
  const onClickButton = (event, tab) => {
    const { clientY, clientX } = event;
    if (tab.id === '+') {
      getSociety();
      setIsSocietyModalOpen(true);
      setAnchorPosition({
        top: clientY,
        left: clientX
      });
    } else {
      getRights();
      push(tab.path);
    }
  };

  const onClickCross = (e, tab) => {
    const { closeTab } = props;
    e.stopPropagation();
    closeTab(tab.id);
  };

  const _handleShortcuts = (action, event) => {
    const {
      match,
      closeTab,
      withCutsRedirect
    } = props;
    switch (action) {
    case 'CRMS':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('userList'));
      break;
    case 'OPEN_NEW_ACCOUNTING':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('newAccounting'));
      break;
    case 'CURRENT_EDITION':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('balance'));
      break;
    case 'CRM':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('userList'));
      break;
    case 'SEND_NEW_DOCUMENT':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('ocr'));
      break;
    case 'GED':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('ged'));
      break;
    case 'CHAT':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('discussionsList'));
      break;
    case 'NEW_TAB':
      event.stopPropagation();
      event.preventDefault();
      window.open(location.pathname, `tab${new Date().getTime()}`);
      break;
    case 'CLOSE_TAB':
      if (match.params.id !== 'collab' && match.params.id !== '+') closeTab(match.params.id);
      break;
    case 'GEDS':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('ged'));
      break;
    case 'PLAN':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('chartAccountTva'));
      break;
    case 'CONSULT':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('accountConsultation'));
      break;
    case 'ECRITURE':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('newAccounting'));
      break;
    case 'DISCUS':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('discussionsList'));
      break;
    case 'NEW_WINDOW':
      event.stopPropagation();
      event.preventDefault();
      window.open(location.pathname, `window${new Date().getTime()}`, `width=${window.innerWidth},height=${window.innerHeight}`);
      break;
    case 'ACCOUNT_SETTING':
      event.stopPropagation();
      event.preventDefault();
      withCutsRedirect(getRouteByKey('bankIntegrationSettings'));
      break;
    default:
      break;
    }
  };

  const openAvatar = (event) => {
    setAnchorAvatar({
      top: event.clientY,
      left: event.clientX
    });
    setIsAvatarOpen(true);
  };

  const closeAvatar = () => {
    setAnchorAvatar(null);
    setIsAvatarOpen(false);
  };

  const handleClose = () => {
    setIsSocietyModalOpen(false);
    setAnchorPosition(null);
  };

  const handleClickShortcuts = () => {
    setIsShortcutsModalOpen(!isShortcutsModalOpen);
  };

  const handleLabelButtonClick = (routeKey, fromCollab) => {
    const { push } = props;
    if (!routeKey) return;

    const route = getRouteByKey(routeKey);
    let { path } = route;

    if (fromCollab) {
      path = path.replace(':id', 'collab');
    }

    push(path);
  };

  const renderNavBarButton = (e) => {
    const {
      classes,
      push,
      societyId,
      toSocietyDialog,
      redirectFromCollab
    } = props;
    const route = getRouteByKey(e.key);

    const buttonWithLabelsStyle = { padding: 0 };

    return (
      !e.collab && (
      <>
        <Tooltip
          key={e.key}
          title={e.info}
          disabled={e.disabled}
          placement="left-end"
          disableHoverListener={!e.info}
          disableFocusListener
          disableTouchListener
          classes={{ tooltip: classes.infoBulle }}
        >
          <MaterialButton
            className={classNames(classes.navBarButton, { [classes.whiteButton]: e.isActive })}
            style={e.withLabels && buttonWithLabelsStyle}
            aria-label={e.ariaLabel}
            onClick={() => {
              if (e.key === 'shortcuts') {
                handleClickShortcuts();
              } else if (route.key === 'linkToSupport') {
                window.open(route.path);
              } else if (societyId !== -2) {
                if (route.key === 'newAccounting') {
                  redirectFromCollab(route.key, 'e', societyId);
                } else {
                  push(route.path);
                }
              } else if (societyId === -2 && !route.isNotCollab) {
                push(route.path);
              } else {
                toSocietyDialog(route);
              }
            }}
          >
            <FontIcon name={e.iconName} size={e.fontSize} />
          </MaterialButton>
        </Tooltip>
        { (e.withLabels)
          && (
            <span className="navBarButtonSuffixes">
              <span
                className={
                  classNames(
                    'button', 'top',
                    !e.topLabel.value ? 'hidden' : null
                  )}
                onClick={() => handleLabelButtonClick(
                  e.topLabel.routeKey,
                  e.topLabel.fromCollab
                )}
              >
                {e.topLabel.value}
              </span>
              <span
                className={classNames('button', 'bottom',
                  !e.bottomLabel.value ? 'hidden' : null)}
                onClick={() => handleLabelButtonClick(
                  e.bottomLabel.routeKey,
                  e.bottomLabel.fromCollab
                )}
              >
                {e.bottomLabel.value}
              </span>
            </span>
          )
        }
      </>
      )
    );
  };

  const initials = `${_.get(user, 'prenom[0]', '').toLocaleUpperCase()}${_.get(user, 'nom[0]', '').toLocaleUpperCase()}`;

  const checkIsCurrentRouteSticky = () => {
    const currentRoute = props.location.pathname.split('/');
    const route = currentRoute[currentRoute.length - 1];
    return STICKY_ROUTES.includes(route);
  };

  // Render
  return (
    <Switch>
      <Route path="/tab/:id/slave">
        <div className="pageContent">
          <Switch>
            {
              // eslint-disable-next-line max-len
              // It must remount on push, key was static, while it must not because each view is related to a society so the key must not be the same.
              routes.map(route => (
                <Route
                  {...route}
                  path={buildSlaveRoute(route.path)}
                  key={`${societyId}_${route.key}`}
                />
              ))
            }
          </Switch>
        </div>
      </Route>
      <Route>
        <Shortcuts
          className="page"
          name="NAV_BAR"
          handler={_handleShortcuts}
          alwaysFireHandler
          stopPropagation={false}
          preventDefault={false}
          targetNodeSelector="body"
        >
          <DrawerContainer
            push={push}
            routeKey={routeKey}
          />
          <div className="pageContent">
            <div className={checkIsCurrentRouteSticky() ? 'navBar navBar__sticky' : 'navBar'}>
              <TabsButton
                tabs={tabs}
                onClick={onClickButton}
                onClose={onClickCross}
              />
              <div className="navBar__toolBar">
                {navBarButtons && navBarButtons.map(renderNavBarButton)}
                {/* TODO : A ajouter quand finalisé avec Candice */}
                {/* <Field
                              className="navBar__toolBar__textField"
                              name="search" component={ReduxTextField}
                              type="text" placeholder={I18n.t('navBar.search')}
                            />
                        <span className="search__icon icon-search" /> */}
                <Button onClick={e => openAvatar(e)}>
                  <Avatar className={classes.purpleAvatar}>{initials}</Avatar>
                </Button>
              </div>
            </div>
            <Switch>
              {
                routes.map(route => <Route {...route} key={`${societyId}_${route.key}`} />) // It must remount on push, key was static, while it must not because each view is related to a society so the key must not be the same.
              }
            </Switch>
          </div>
          <SocietyPopoverContainer
            isOpen={isSocietyModalOpen}
            onClose={handleClose}
            anchorPosition={anchorPosition}
            anchorReference="anchorPosition"
            researchField
          />
          <PopoverAvatar
            isOpen={isAvatarOpen}
            onClose={closeAvatar}
            anchorPosition={anchorAvatar}
          />
          <ShortcurtsListDialog
            isOpen={isShortcutsModalOpen}
            onClose={handleClickShortcuts}
          />
          <AccessDashboard
            isOpen={isOpenAccess}
            society={soc}
            onValidate={onValidateAccess}
            onClose={onCloseAccess}
          />
        </Shortcuts>
      </Route>
    </Switch>
  );
};

const styles = {
  purpleAvatar: {
    color: '#fff',
    width: 30,
    height: 30,
    backgroundColor: deepPurple[500]
  },
  dialogOpen: {
    filter: 'blur(5px)'
  },
  dialogClose: {
    filter: null
  },
  infoBulle: {
    border: 'none',
    heigth: 15,
    width: 'auto',
    padding: '4px 10px',
    borderRadius: 2,
    fonytSize: 10,
    fontWeight: 500,
    boxShadow: '1px 5px 7px 0 rgba(0, 0, 0, 0.08)',
    backgroundColor: '#ffffff',
    color: '#464545',
    fontFamily: 'basier_circlemedium'
  },
  navBarButton: {
    width: '40px',
    minWidth: '40px',
    height: '100%'
  },
  whiteButton: {
    backgroundColor: 'white'
  }
};

NavBar.defaultProps = {
  classes: {},
  navBarButtons: [{}],
  user: {}
};

NavBar.propTypes = {
  getSociety: PropTypes.func.isRequired,
  getRights: PropTypes.func.isRequired,
  redirectFromCollab: PropTypes.func.isRequired,
  withCutsRedirect: PropTypes.func.isRequired,
  addTabs: PropTypes.func.isRequired,
  toSocietyDialog: PropTypes.func.isRequired,
  user: PropTypes.shape({}),
  location: PropTypes.object.isRequired,
  navBarButtons: PropTypes.arrayOf(PropTypes.shape({})),
  societyId: PropTypes.number.isRequired,
  routeKey: PropTypes.string.isRequired,
  push: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired, // eslint-disable-line
  closeTab: PropTypes.func.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    isClosable: PropTypes.bool,
    isSelected: PropTypes.bool,
    label: PropTypes.string,
    path: PropTypes.string
  }),
  PropTypes.shape({
    id: PropTypes.string,
    isClosable: PropTypes.bool,
    label: PropTypes.string,
    path: PropTypes.string
  })).isRequired,
  classes: PropTypes.shape({}),
  match: PropTypes.any.isRequired, // eslint-disable-line
  enqueueSnackbar: PropTypes.func.isRequired, // eslint-disable-line
  closeSnackbar: PropTypes.func.isRequired, // eslint-disable-line
  initUnreadMessagesStat: PropTypes.func.isRequired
};

const WrappedWithFormNavBar = reduxForm({
  form: 'navBarForm',
  destroyOnUnmount: false,
  enableReinitialize: true
})(NavBar);

export default withSnackbar(withStyles(styles)(WrappedWithFormNavBar));
