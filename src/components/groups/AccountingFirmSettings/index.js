import React, { Fragment } from 'react';
import { AccountingFirmSettingsHeader } from 'containers/groups/Headers';
import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';
import PropTypes from 'prop-types';
import styles from './accountingFirmSettingsScreen.module.scss';

const AccountingFirmSettings = ({ setCreateNewStatus }) => (
  <Fragment>
    <div className={styles.container}>
      <div className={styles.header_container}>
        <AccountingFirmSettingsHeader />
      </div>
      <SplitToolbar
        onSplitOpen={() => setCreateNewStatus(false)}
        onSplitClose={() => setCreateNewStatus(true)}
      />
    </div>
  </Fragment>
);

AccountingFirmSettings.propTypes = {
  setCreateNewStatus: PropTypes.func.isRequired
};

export default AccountingFirmSettings;
