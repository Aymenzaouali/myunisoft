import React from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import ownStyles from './stateOrFormDialogTable.module.scss';

const StateOrFormDialogTable = ({ param, ...otherProps }) => (
  <div className={ownStyles.container}>
    <div className={ownStyles.tableContainer}>
      <TableFactory
        param={param}
        {...otherProps}
      />
    </div>
  </div>
);


StateOrFormDialogTable.propTypes = {
  param: PropTypes.shape({}).isRequired
};

export default StateOrFormDialogTable;
