import { GEDTableMenu } from 'assets/constants/data';
import { InlineButton } from 'components/basics/Buttons';
import React from 'react';
import styles from './docManagementTable.module.scss';

const renderButtons = (token, handleMenu, item) => (
  <div className={styles.tableRowBtns}>
    <InlineButton buttons={
      [
        {
          _type: 'icon-link',
          variant: 'none',
          iconName: 'icon-download',
          iconSize: 19,
          iconColor: 'black',
          href: `https://cloud.myunisoft.fr/index.php/s/${token}/download`
        },
        {
          _type: 'dropdown',
          variant: 'none',
          options: GEDTableMenu,
          onClick: e => handleMenu(e, item),
          iconName: 'icon-options',
          iconSize: 19,
          iconColor: 'black'
        }
      ]}
    />
  </div>
);

export default renderButtons;
