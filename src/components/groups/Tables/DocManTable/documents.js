import { docFormats } from 'assets/constants/data';

export const getDocIcon = (docName) => {
  const doc = docName ? docName.split('.') : 'Unknown';
  const docFormat = doc[doc.length - 1].toLocaleLowerCase() || '';
  const {
    PDF, DOC, DOC_X, EXCEL, EXCEL_X, POWER_POINT, POWER_POINT_X,
    JPEG, JPG, GIF, TIFF, PNG, BMP, VIDEO, ZIP, RAR, MP3, TXT
  } = docFormats;
  if (docFormat === PDF.format) {
    return { icon: 'icon-file-pdf-o', color: PDF.color };
  }
  if (docFormat === DOC.format || docFormat === DOC_X.format) {
    return { icon: 'icon-file-word-o', color: DOC.color };
  }
  if (docFormat === EXCEL.format || docFormat === EXCEL_X.format) {
    return { icon: 'icon-file-excel-o', color: EXCEL.color };
  }
  if (docFormat === POWER_POINT.format || docFormat === POWER_POINT_X.format) {
    return { icon: 'icon-file-powerpoint-o', color: POWER_POINT.color };
  }
  if (docFormat === JPEG.format
      || docFormat === JPG.format
      || docFormat === PNG.format
      || docFormat === GIF.format
      || docFormat === TIFF.format
      || docFormat === BMP.format) {
    return { icon: 'icon-file-image-o', color: JPG.color };
  }
  if (docFormat === VIDEO.format) {
    return { icon: 'icon-file-movie-o', color: VIDEO.color };
  }
  if (docFormat === ZIP.format || docFormat === RAR.format) {
    return { icon: 'icon-file-archive-o', color: ZIP.color };
  }
  if (docFormat === MP3.format) {
    return { icon: 'icon-file-audio-o', color: MP3.color };
  }
  if (docFormat === TXT.format) {
    return { icon: 'icon-file-text2', color: TXT.color };
  }
  return { icon: 'icon-docs', color: '#464545' };
};
