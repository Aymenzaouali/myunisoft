import React from 'react';
import { withStyles } from '@material-ui/core';
import { emphasize } from '@material-ui/core/styles/colorManipulator';
import Chip from '@material-ui/core/Chip';
import PropTypes from 'prop-types';

const Tags = ({ tags, classes }) => (
  <div>
    {
      tags && tags.map(tag => (
        <Chip
          key={tag.tag_id}
          label={tag.label}
          className={classes.chip}
        />
      ))
    }
  </div>
);

Tags.propTypes = {
  tags: PropTypes.array.isRequired,
  classes: PropTypes.shape({}).isRequired
};

const styles = theme => ({
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
    backgroundColor: 'rgba(11, 209, 209, 0.14)',
    color: '#0bd1d1',
    height: 24,
    borderRadius: 16,
    paddingRight: 4
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    )
  }
});

export default withStyles(styles, { withTheme: true })(Tags);
