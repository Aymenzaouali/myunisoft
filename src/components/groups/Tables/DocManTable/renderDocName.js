import classNames from 'classnames';
import React from 'react';
import { FontIcon } from 'components/basics/Icon';
import { getDocIcon } from './documents';
import styles from './docManagementTable.module.scss';

const renderDocName = (name, img, isList) => (
  <div style={{ display: 'flex', alignItems: 'center' }}>
    { isList
    && <FontIcon name={getDocIcon(name).icon} color={getDocIcon(name).color} size={16} /> }
    <div
      className={isList ? styles.docImage_Wrap
        : classNames(styles.docImage_Wrap, styles.docImage_Wrap_active)}
    >
      <div className={styles.docImage__background}>
        <div className={styles.docImage__background__icon}>
          <FontIcon name="icon-oeil" color="#ffffff" />
        </div>
      </div>
      <img className={styles.docImage} src={img} alt={name} />
    </div>
    {
      name ? (
        <span style={{ marginLeft: '15px' }}>{name}</span>
      ) : <span style={{ marginLeft: '15px' }}>Unknown</span>
    }
  </div>
);

export default renderDocName;
