import React, { PureComponent, Fragment } from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import { ConfirmationDialog } from 'components/groups/Dialogs';
import { GED_TABLE_OPTIONS } from 'assets/constants/data';
import I18n from 'assets/I18n';
import _ from 'lodash';
import { FontIcon } from 'components/basics/Icon';
import PropTypes from 'prop-types';
import { IconButton, Select } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import WindowHelper from 'helpers/window'; //eslint-disable-line
import classNames from 'classnames';
import Tags from './tags';
import renderButtons from './renderButtons';
import renderDocName from './renderDocName';
import styles from './docManagementTable.module.scss';

const CELL_TYPES = {
  NAME: 'name',
  DATE: 'date',
  ADDED_BY: 'added_by',
  DATA_ECHEANCE: 'date_echeance',
  DUE_DATE: 'due_date',
  COMMENTS: 'comments',
  TAGS: 'tags',
  LINKED_TO: 'linked_to'
};

let formattedParams = {};

class DocManTable extends PureComponent {
  state = {
    commentDialog: {
      isOpen: false,
      label: null
    },
    direction: {},
    opened: {},
    scrolledTable: false
  };

  docTable = React.createRef();

  componentDidMount() {
    this.handleScroll();
    const {
      setTableData
    } = this.props;
    setTableData(formattedParams);
  }

  // eslint-disable-next-line no-dupe-class-members
  componentDidUpdate(prevProps) {
    this.handleScroll();
    const {
      society_id, activeTabId, documents, setTableData
    } = this.props;
    const prevDocuments = prevProps.documents;
    const prevIsLoading = _.get(prevDocuments[society_id], `[${activeTabId}]isLoading`, false);
    const isLoading = _.get(documents[society_id], `[${activeTabId}]isLoading`, false);
    if (isLoading !== prevIsLoading) {
      setTableData(formattedParams);
    }
  }

  handleScroll = () => {
    const scroll = this.docTable.current.scrollLeft;
    const blockWidth = this.docTable.current.scrollWidth - this.docTable.current.offsetWidth;
    if (scroll !== blockWidth) {
      this.setState({ scrolledTable: true });
    } else {
      this.setState({ scrolledTable: false });
    }
  };

  openWindow = (e, item) => {
    const {
      id,
      name,
      date,
      token
    } = item;

    e.preventDefault();
    e.stopPropagation();

    const files = [{
      date,
      document_id: id,
      name,
      thumbnail: `https://cloud.myunisoft.fr/index.php/apps/files_sharing/ajax/publicpreview.php?x=90&y=120&a=true&t=${token}&scalingup=0`,
      token
    }];

    if (token) {
      WindowHelper.forceOpen(files);
    }
  };

  handleMenu = (option, item) => {
    const { showSendInDiscussionModal, setDocumentToCopy } = this.props;
    if (option === GED_TABLE_OPTIONS.SEND_IN_DISCUSSION) {
      setDocumentToCopy({ document: item });
      showSendInDiscussionModal(true);
    }
  };

  renderLinkedTo = (item) => {
    const { linked_to, token, id } = item;
    const { opened } = this.state;
    const linked_open = _.get(opened, `${[id]}`, false);
    return (
      <div className={styles.LinkedTo}>
        <div className={styles.LinkedToSelect}>
          {I18n.t('ged.table.linked_to')}
          <span>
            {' '}
            {linked_to ? linked_to.length : 0}
          </span>
          <Select
            disableUnderline
            autoWidth
            disabled={!linked_to.length}
            color="primary"
            margin="normal"
            open={linked_open}
            style={{ width: '23px', marginLeft: '10px' }}
            onOpen={async () => {
              this.setState({
                opened: {
                  ...opened,
                  [id]: true
                }
              });
            }}
            onClose={() => {
              this.setState({
                opened: {
                  ...opened,
                  [id]: false
                }
              });
            }}
          >
            {/* {_.isEmpty(linked_docs[id]) && <CircularProgress />} */}
            {_.map(linked_to, item => (
              <MenuItem
                key={`select-${item.id}`}
                value={item.id}
                label={item.label}
                className={styles.linkedItem}
                style={{ padding: '2px 7px', fontSize: 11 }}
              >
                {item.label}
              </MenuItem>
            ))}
          </Select>
        </div>
        {renderButtons(token, this.handleMenu, item)}
      </div>
    );
  };

  renderComment = (
    <IconButton onClick={() => this.openComment('some comment')}>
      <FontIcon name="icon-comments" color="#ff8b1f" />
    </IconButton>
  );

  openComment = (comment) => {
    /**
     * here will be comment func
     */
    console.log(comment);
  };

  getLabel = (elem, label, item) => {
    const {
      token, tags
    } = item;
    const { isList } = this.props;
    if (elem === CELL_TYPES.NAME) {
      return renderDocName(label, `https://cloud.myunisoft.fr/index.php/apps/files_sharing/ajax/publicpreview.php?x=90&y=120&a=true&t=${token}&scalingup=0`, isList, item);
    }
    if (elem === CELL_TYPES.COMMENTS) {
      return this.renderComment;
    }
    if (elem === CELL_TYPES.LINKED_TO) {
      return this.renderLinkedTo(item);
    }
    if (elem === CELL_TYPES.TAGS) {
      return <Tags tags={tags} />;
    }
    return label;
  };

  getLabelToExport = (elem, item) => {
    if (elem === CELL_TYPES.NAME) {
      return item[elem];
    }
    if (elem === CELL_TYPES.LINKED_TO) {
      return `${I18n.t('ged.table.linked_to')} ${item.linked_to}`;
    }
    if (elem === CELL_TYPES.COMMENTS) {
      return 'comment';
    }
    return item[elem];
  };

  render() {
    const {
      param,
      isList,
      documents,
      documentMode,
      activeTabId,
      society_id,
      getDocuments,
      itemsPerPage,
      setItemsPerPage,
      activeFolder,
      currentPage,
      totalDocs,
      headersDocs,
      setTableData,
      ...otherProps
    } = this.props;
    const {
      commentDialog,
      scrolledTable,
      direction
    } = this.state;

    const header = _.get(headersDocs[society_id], `[${activeTabId}]documents_header`, []);
    const body = _.get(documents[society_id], `[${activeTabId}]documents_list`, []);
    const isLoading = _.get(documents[society_id], `[${activeTabId}]isLoading`, false);
    const isError = _.get(documents[society_id], `[${activeTabId}]isError`, false);

    const buttonsDialog = {
      validate: I18n.t('close'),
      cancel: 'Close'
    };

    const generateKey = (cellName = '', id = '') => `documents_table${Math.floor(Math.random() * 100000)}${cellName}${id}`;

    const headers = [{
      keyRow: generateKey(),
      value: _.map(header, (key, i) => {
        const dir = _.get(direction, `${[activeTabId]}.${[key.keyLabel]}`, 'desc');
        const label = _.get(key, 'keyLabel', 'empty');
        return {
          _type: key.type,
          keyCell: `collectorHeaderCell.${label}${i}`,
          props: {
            label: label !== 'empty' && I18n.t(`ged.table.${label}`),
            children: label !== 'empty' && I18n.t(`ged.table.${label}`),
            direction: dir,
            onSort: (order) => {
              getDocuments(activeFolder, currentPage, null, label, dir);
              this.setState({
                direction: {
                  [activeTabId]: {
                    [label]: order === 'desc' ? 'asc' : 'desc'
                  }
                }
              });
            }
          },
          cellProp: {
            className: (label === 'comments') && styles.orangedHeader
          }
        };
      })
    }];

    let formattedBody;

    if (activeTabId === 0) {
      formattedBody = _.map(body, document => ({
        id: document.doc_id,
        name: document.name,
        token: document.token,
        urltoken_preview: document.urltoken_preview,
        entitled: document.label,
        date: document.date,
        added_by: document.added_by,
        linked_to: document.linked_to || 0
      }));
    } else if (activeTabId === 1) {
      formattedBody = _.map(body, document => ({
        id: document.doc_id,
        name: document.name,
        token: document.token,
        urltoken_preview: document.urltoken_preview,
        entitled: document.label,
        date: document.date,
        added_by: document.added_by,
        status: I18n.t('ged.table.statusInfo'),
        amount_ttc: document.amount_ttc
      }));
    } else if (activeTabId === 2) {
      formattedBody = _.map(body, document => ({
        id: document.doc_id,
        name: document.name,
        token: document.token,
        urltoken_preview: document.urltoken_preview,
        entitled: document.label,
        date: document.date,
        amount_ttc: document.amount_ttc,
        amount_ht: document.amount_ht,
        amount_tva: document.tva,
        due_date: document.date_echeance,
        comments: document.comment,
        letter: document.code ? I18n.t('ged.table.letterExist') : I18n.t('ged.table.letterNotExist'),
        linked_to: document.linked_to || 0
      }));
    } else if (activeTabId === 3) {
      formattedBody = _.map(body, document => ({
        id: document.doc_id,
        name: document.name,
        token: document.token,
        urltoken_preview: document.urltoken_preview,
        entitled: document.label,
        date: document.date,
        added_by: document.added_by,
        amount_ttc: document.amount_ttc,
        amount_ht: document.amount_ht,
        amount_tva: document.amount_tva,
        due_date: document.due_date,
        comments: document.comments,
        letter: document.letter ? I18n.t('ged.table.letterExist') : I18n.t('ged.table.letterNotExist'),
        linked_to: document.linked_to || 0
      }));
    } else if (activeTabId === 4) {
      formattedBody = body.map(document => ({
        id: document.doc_id,
        name: document.name,
        token: document.token,
        urltoken_preview: document.urltoken_preview,
        entitled: document.label,
        date: document.date,
        added_by: document.added_by,
        amount_ttc: document.amount_ttc,
        amount_ht: document.amount_ht,
        amount_tva: document.amount_tva,
        date_echeance: document.date_echeance,
        comments: document.comments,
        letter: document.letter ? I18n.t('ged.table.letterExist') : I18n.t('ged.table.letterNotExist'),
        linked_to: document.linked_to || 0
      }));
    } else if (activeTabId === 5) {
      formattedBody = body.map(document => ({
        id: document.id,
        token: document.token,
        urltoken_preview: document.urltoken_preview,
        name: document.name,
        company_name: document.entitled,
        date: document.date,
        added_by: document.added_by,
        title: document.title,
        body_message: document.body_message,
        tags: document.tags
      }));
    } else {
      formattedBody = body;
    }
    const tableFormattedBody = [];

    formattedBody.forEach((item, i) => {
      tableFormattedBody.push({
        keyRow: generateKey(i, i),
        value: []
      });

      Object.keys(item).map((elem) => {
        if (elem !== 'id' && elem !== 'token' && elem !== 'urltoken_preview') {
          tableFormattedBody[i].value.push({
            _type: elem === CELL_TYPES.DATE
              || elem === CELL_TYPES.DUE_DATE
              || elem === CELL_TYPES.DATA_ECHEANCE
              ? 'date' : 'string',
            keyCell: generateKey(item[elem], item.id),
            props: {
              label: this.getLabel(elem, item[elem], item),
              labelToExport: this.getLabelToExport(elem, item),
              onClick: (e) => {
                if (elem !== 'linked_to' && elem !== 'comments') this.openWindow(e, item);
              }
            }
          });
        }
        return elem;
      });
      if (!item.linked_to) {
        tableFormattedBody[i].value.push(
          {
            _type: 'string',
            keyCell: generateKey(item.id, i),
            props: {
              label: renderButtons(item.token || '', this.handleMenu)
            },
            cellProp: {
              style: { width: '115px' }
            }
          }
        );
      }
    });

    formattedParams = {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: tableFormattedBody
      }
    };

    return (
      <Fragment>
        <div className={styles.tableWrap} ref={this.docTable} onScroll={this.handleScroll}>
          <TableFactory
            param={formattedParams}
            className={
              scrolledTable ? classNames(styles.table, styles.table_active) : styles.table
            }
            isLoading={isLoading}
            isError={isError}
            {...otherProps}
            cellHeight={45}
            maxVisibleItems={10}
          />
        </div>
        <ConfirmationDialog
          isOpen={commentDialog.isOpen}
          onValidate={() => this.setState({ commentDialog: { isOpen: false } })}
          onClose={() => this.setState({ commentDialog: { isOpen: false } })}
          message={commentDialog.label}
          title={I18n.t('comment')}
          buttonsLabel={buttonsDialog}
        />
      </Fragment>
    );
  }
}

DocManTable.propTypes = {
  param: PropTypes.shape({}).isRequired,
  linked_docs: PropTypes.shape({}).isRequired,
  isList: PropTypes.bool.isRequired,
  getDocuments: PropTypes.func.isRequired,
  setDocumentToCopy: PropTypes.func.isRequired,
  showSendInDiscussionModal: PropTypes.func.isRequired,
  setTableData: PropTypes.func.isRequired,
  setItemsPerPage: PropTypes.func.isRequired,
  activeTabId: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  totalDocs: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  society_id: PropTypes.number.isRequired,
  activeFolder: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  headersDocs: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  documents: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  documentMode: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default DocManTable;
