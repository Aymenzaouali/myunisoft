import React, { useState } from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import { AccountingFirmDeclareTableDialog } from 'containers/groups/Dialogs';
import ownStyles from './accountingFirmDeclareTable.module.scss';

const AccountingFirmDeclareTable = ({
  param, title, disableButtons, onAdd, onModify, fillDefaultsValues, ...otherProps
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isCreation, setIsCreation] = useState(false);

  const create = () => {
    setIsOpen(true);
    setIsCreation(true);
  };

  const modify = async () => {
    setIsOpen(true);
    setIsCreation(false);
  };

  const closeDialog = () => {
    setIsOpen(false);
    setIsCreation(false);
  };

  return (
    <div>
      <div>{title}</div>
      <div className={ownStyles.table}>
        <div className={ownStyles.tableContainer}>
          <TableFactory
            param={param}
            {...otherProps}
          />
        </div>
        <div className={ownStyles.tableButtons}>
          <InlineButton buttons={[
            {
              _type: 'string',
              size: 'medium',
              text: I18n.t('accountingFirmSettings.new'),
              onClick: create
            },
            {
              _type: 'string',
              size: 'medium',
              disabled: disableButtons,
              text: I18n.t('accountingFirmSettings.modify'),
              onClick: modify
            }
          ]}
          />
        </div>
      </div>
      {
        isOpen && (
          <AccountingFirmDeclareTableDialog
            isOpen={isOpen}
            creationMode={isCreation}
            onClose={closeDialog}
            onAdd={onAdd}
            onModify={onModify}
            fillDefaultsValues={fillDefaultsValues}
            isCreation
          />
        )
      }
    </div>
  );
};

AccountingFirmDeclareTable.defaultProps = {
  onAdd: () => {},
  onModify: () => {}
};

AccountingFirmDeclareTable.propTypes = {
  param: PropTypes.shape({}).isRequired,
  disableButtons: PropTypes.bool.isRequired,
  onAdd: PropTypes.func,
  onModify: PropTypes.func,
  fillDefaultsValues: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

export default AccountingFirmDeclareTable;
