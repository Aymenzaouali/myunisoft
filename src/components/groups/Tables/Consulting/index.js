import React, {
  Fragment, useEffect, useState, useRef
} from 'react';
import PropTypes from 'prop-types';

import I18n from 'assets/I18n';
import { splitShape } from 'context/SplitContext';
import { useWindowState } from 'helpers/hooks';

import { ChangeAccount, ChangeDiary } from 'containers/groups/Dialogs';

import Window from 'components/basics/Window';
import TableFactory from 'components/groups/Tables/Factory';

import _ from 'lodash';

// Component
const TableConsulting = (props) => {
  const {
    arrayData,
    tableError,
    tableLoading,
    societyId,
    updateEntry,
    modifyEntry,
    refresh,
    split,
    consultingRef,
    setTableData,
    selectedItems
  } = props;

  // State
  const overview = useWindowState();
  const [changeAccount, setChangeAccount] = useState({ isopen: false, data: [] });
  const [changeDiary, setChangeDiary] = useState({ isopen: false, data: [] });

  const getAllItems = (value) => {
    let allItems = selectedItems || [];
    const isValueInSelected = selectedItems.find(e => e.entry_line_id === _.get(value, 'data.entry_line_id'));
    if (!isValueInSelected) {
      allItems = [...allItems, value.data];
    }
    return allItems;
  };

  // Rendering
  if (arrayData.body && !split.slave) {
    const rightClickActions = [
      {
        label: I18n.t('consulting.changeAccount'),
        onClick: (value) => {
          const allItems = getAllItems(value);
          let filteredItems = allItems.filter(e => !e.closed);
          if (filteredItems.length > 0) {
            filteredItems = filteredItems.map(
              ({ entry_line_id }) => ({ line_entry_id: entry_line_id })
            );
          }
          setChangeAccount({
            isOpen: true,
            data: filteredItems,
            societyId
          });
        }
      },
      {
        label: I18n.t('consulting.changeDiary'),
        onClick: (value) => {
          const allItems = getAllItems(value);
          const filteredItems = allItems.filter(e => !e.closed);
          setChangeDiary({ isOpen: true, data: filteredItems, societyId });
        }
      },
      {
        label: I18n.t('consulting.updateEntry'),
        onClick: (data) => {
          updateEntry(societyId, data.data.entry_line_id);
          modifyEntry();
          refresh();
          setTimeout(() => {
            if (_.get(consultingRef, 'current', false)) consultingRef.current.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' });
          }, 0);
        }
      }
    ];

    arrayData.body.rightClickActions = rightClickActions;
    // ADD DYNAMIQUE LOGIQUE TO HANDLE DISABLE BUTTONS
    arrayData.body.handleButtons = (value) => {
      const changeAccount = rightClickActions[0];
      const changeDiary = rightClickActions[1];
      const updateEntry = rightClickActions[2];
      const allItems = getAllItems(value);
      const isAllItemsClosed = allItems.every(e => e.closed);
      if (isAllItemsClosed) {
        rightClickActions[0] = { ...changeAccount, disabled: true };
        rightClickActions[1] = { ...changeDiary, disabled: true };
      } else {
        rightClickActions[0] = { ...changeAccount, disabled: false };
        rightClickActions[1] = { ...changeDiary, disabled: false };
      }
      if (_.get(value, 'data.closed')) {
        rightClickActions[2] = { ...updateEntry, disabled: true };
      } else {
        rightClickActions[2] = { ...updateEntry, disabled: false };
      }
      return rightClickActions;
    };
  }

  if (arrayData.body) {
    arrayData.body.row.forEach((row) => {
      row.value.forEach((cell) => {
        if (cell._type === 'status') {
          // eslint-disable-next-line no-param-reassign
          cell.props.data.onClick = (event) => {
            event.stopPropagation();
            overview.open({ documents: cell.props.data.pjs });
          };
        }
      });
    });
  }

  const tableDataRows = _.get(arrayData, 'body.row', []);
  const maxVisibleItems = 10;
  const cellHeight = 32;
  const headerHeight = 56;
  const footerHeight = 29;
  const tableContainerRef = useRef();

  arrayData.body.row = arrayData.body.row.map((row, index) => ({
    ...row,
    props: {
      ...row.props,
      // eslint-disable-next-line consistent-return
      onFocus: () => {
        const el = tableContainerRef.current.scrollContainer.current;
        const { scrollTop } = el;
        const posY = index * cellHeight;

        if (posY < scrollTop) {
          return el.scrollTo({
            top: posY
          });
        }

        if (posY > (
          scrollTop
            + maxVisibleItems * cellHeight
            - headerHeight
            - footerHeight
            - cellHeight)
        ) {
          el.scrollTo({
            top: scrollTop + cellHeight
          });
        }
      }
    }
  }));


  useEffect(() => {
    setTableData(arrayData);
  }, [tableLoading, tableDataRows.length]);

  return (
    <Fragment>
      <TableFactory
        isError={tableError}
        isLoading={tableLoading}
        param={arrayData}
        cellHeight={cellHeight}
        maxVisibleItems={maxVisibleItems}
        ref={tableContainerRef}
      />
      <ChangeAccount
        isOpen={changeAccount.isOpen}
        onClose={() => setChangeAccount({ isOpen: false })}
        data={changeAccount.data}
        societyId={societyId}
      />
      <ChangeDiary
        isOpen={changeDiary.isOpen}
        onClose={() => setChangeDiary({ isOpen: false })}
        data={changeDiary.data}
        societyId={societyId}
      />
      <Window
        path="/overview"
        window_id="overview"
        features={`width=${window.innerWidth / 2},left=${window.screenX + (window.innerWidth / 2)}`}

        {...overview.props}
      />
    </Fragment>
  );
};

// Props
TableConsulting.propTypes = {
  arrayData: PropTypes.shape({}),
  tableError: PropTypes.bool.isRequired,
  tableLoading: PropTypes.bool.isRequired,
  societyId: PropTypes.number.isRequired,
  updateEntry: PropTypes.func.isRequired,
  setTableData: PropTypes.func.isRequired,
  modifyEntry: PropTypes.func.isRequired,
  refresh: PropTypes.func.isRequired,
  split: splitShape.isRequired,
  consultingRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }).isRequired,
  selectedItems: PropTypes.array.isRequired
};

TableConsulting.defaultProps = {
  arrayData: {}
};

export default TableConsulting;
