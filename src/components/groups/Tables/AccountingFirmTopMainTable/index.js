import React from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import ownStyles from './accountingFirmTopMainTable.module.scss';

const AccountingFirmTopMainTable = ({ param, ...otherProps }) => (
  <div className={ownStyles.tableContainer}>
    <TableFactory
      param={param}
      cellHeight={45}
      maxVisibleItems={10}
      {...otherProps}
    />
  </div>
);

AccountingFirmTopMainTable.propTypes = {
  param: PropTypes.shape({}).isRequired
};

export default AccountingFirmTopMainTable;
