import React from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import ownStyles from './reportsAndFormsTable.module.scss';

const ReportsAndFormsTable = ({ param, ...otherProps }) => (
  <div className={ownStyles.tableContainer}>
    <TableFactory
      param={param}
      {...otherProps}
    />
  </div>
);

ReportsAndFormsTable.propTypes = {
  param: PropTypes.shape({}).isRequired
};

export default ReportsAndFormsTable;
