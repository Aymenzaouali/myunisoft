import React from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import { splitShape } from 'context/SplitContext';

import ownStyles from './reportsAndFormsTableAdditional.module.scss';

const ReportsAndFormsTableAdditional = ({ param, ...otherProps }) => (
  <div className={ownStyles.container}>
    <div className={ownStyles.tableContainer}>
      <TableFactory
        param={param}
        {...otherProps}
      />
    </div>
  </div>
);


ReportsAndFormsTableAdditional.propTypes = {
  param: PropTypes.shape({}).isRequired,
  split: splitShape.isRequired
};

export default ReportsAndFormsTableAdditional;
