import React, { useEffect } from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import ownStyles from './settingStandardMailParagraphTable.module.scss';

const SettingStandardMailParagraphTable = ({
  param, isLoading, setTable, selectedParagraphsTypesRowsCount,
  initialParagraphsTypesData, ...otherProps
}) => {
  useEffect(() => {
    setTable();
  }, [isLoading, selectedParagraphsTypesRowsCount, initialParagraphsTypesData]);
  return (
    <div className={ownStyles.tableContainer}>
      <TableFactory
        param={param}
        {...otherProps}
      />
    </div>
  );
};

SettingStandardMailParagraphTable.propTypes = {
  param: PropTypes.shape({}).isRequired,
  initialParagraphsTypesData: PropTypes.shape({}),
  setTable: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  selectedParagraphsTypesRowsCount: PropTypes.number
};

SettingStandardMailParagraphTable.defaultProps = {
  isLoading: false,
  selectedParagraphsTypesRowsCount: 0,
  initialParagraphsTypesData: []
};

export default SettingStandardMailParagraphTable;
