import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import FactoryTable from 'components/groups/Tables/Factory';

const PhysicalPerson = ({
  param, setTableData, employee_list_pp, ...otherProps
}) => {
  useEffect(() => {
    setTableData();
  }, [employee_list_pp]);
  return (
    <FactoryTable
      param={param}
      {...otherProps}
    />
  );
};

PhysicalPerson.propTypes = {
  param: PropTypes.shape({}).isRequired,
  setTableData: PropTypes.func.isRequired,
  employee_list_pp: PropTypes.array.isRequired
};

export default PhysicalPerson;
