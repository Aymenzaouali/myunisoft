import React, { useEffect } from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import ownStyles from './settingStandardMailTable.module.scss';

const SettingStandardMailTable = ({
  param, isLoading, setTable, ...otherProps
}) => {
  useEffect(() => {
    setTable();
  }, [isLoading, param.body]);
  return (
    <div className={ownStyles.tableContainer}>
      <TableFactory
        param={param}
        {...otherProps}
      />
    </div>
  );
};

SettingStandardMailTable.propTypes = {
  param: PropTypes.shape({}).isRequired,
  setTable: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default SettingStandardMailTable;
