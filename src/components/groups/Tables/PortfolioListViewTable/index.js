import React, { useEffect } from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import ownStyles from './portfolioListTable.module.scss';

const PortfolioListViewTable = ({
  param, setTableData, isLoading, ...otherProps
}) => {
  useEffect(() => {
    setTableData(param);
  }, [isLoading]);
  return (
    <div className={ownStyles.tableContainer}>
      <TableFactory
        param={param}
        {...otherProps}
      />
    </div>
  );
};

PortfolioListViewTable.propTypes = {
  param: PropTypes.shape({}).isRequired,
  setTableData: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default PortfolioListViewTable;
