import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { TableFactory } from 'components/groups/Tables';
import I18n from 'assets/I18n';
import styles from './analyticReview.module.scss';

const AnalyticReview = (props) => {
  const {
    balanceSheetTable, // eslint-disable-line
    accountsResultsTable, // eslint-disable-line
    reviewId, cycleId,

    getAnalyticReview
  } = props;

  useEffect(() => {
    if (reviewId != null && cycleId != null) {
      getAnalyticReview(reviewId, cycleId);
    }
  }, [reviewId, cycleId]);

  return (
    <div className={styles.container}>
      <Typography variant="h6">{I18n.t('tables.analyticReview.balanceSheet')}</Typography>
      <TableFactory {...balanceSheetTable} />
      <Typography variant="h6">{I18n.t('tables.analyticReview.accountsResults')}</Typography>
      <TableFactory {...accountsResultsTable} />
    </div>
  );
};

// Props
AnalyticReview.propTypes = {
  reviewId: PropTypes.number,
  cycleId: PropTypes.number,

  getAnalyticReview: PropTypes.func.isRequired
};

AnalyticReview.defaultProps = {
  reviewId: null,
  cycleId: null
};

export default AnalyticReview;
