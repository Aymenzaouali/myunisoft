import React from 'react';
import PropTypes from 'prop-types';

import { Badge, IconButton } from '@material-ui/core';

import FontIcon from 'components/basics/Icon/Font';

import styles from './PJCell.module.scss';

// Component
const PJCell = (props) => {
  const {
    diligence_id, documents, classes,
    onAskOverview, onAdd
  } = props;

  // Functions
  const handleAskOverview = (event) => {
    event.stopPropagation();
    if (onAskOverview) onAskOverview(documents);
  };

  const handleAdd = (event) => {
    event.stopPropagation();
    if (onAdd) onAdd(diligence_id);
  };

  // Rendering
  return (
    <div className={styles.container}>
      <IconButton
        classes={{ root: classes.buttons }}
        onClick={handleAskOverview}
      >
        <Badge
          color="primary"
          classes={{ badge: classes.badge }}
          badgeContent={documents.length}
        >
          <FontIcon name="icon-attachment" />
        </Badge>
      </IconButton>
      <IconButton
        color="primary"
        classes={{ root: classes.buttons }}
        onClick={handleAdd}
      >
        <FontIcon name="icon-plus" color="inherit" />
      </IconButton>
    </div>
  );
};

// Props
PJCell.propTypes = {
  diligence_id: PropTypes.number.isRequired,
  documents: PropTypes.array.isRequired,
  classes: PropTypes.shape({
    buttons: PropTypes.string,
    badge: PropTypes.string
  }),

  onAskOverview: PropTypes.func,
  onAdd: PropTypes.func
};

PJCell.defaultProps = {
  classes: {},

  onAskOverview: undefined,
  onAdd: undefined
};

export default PJCell;
