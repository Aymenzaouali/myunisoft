import React from 'react';
import PropTypes from 'prop-types';
import { InlineButton } from 'components/basics/Buttons';
import FormControl from '@material-ui/core/FormControl';
import { FontIcon } from 'components/basics/Icon';
import { Dialog, DialogContent } from '@material-ui/core';
import { Field } from 'redux-form';
import { ReduxSelect } from 'components/reduxForm/Selections';

import I18n from 'assets/I18n';

import { timelyOptions } from 'assets/constants/data';
import Button from 'components/basics/Buttons/Button';

import styles from './timelyDialogs.module.scss';

// Component
const TimelyDialog = (props) => {
  const { open, onConfirm, onCancel } = props;

  // Rendering
  return (
    <Dialog
      open={open}
      onClose={onCancel}
      PaperProps={{
        style: {
          padding: '0px 0px 35px 35px;'
        }
      }}
      fullWidth
      maxWidth="sm"
    >
      <div className={styles.header}>
        <Button onClick={onCancel}>
          <FontIcon size={24} className="icon-close" />
        </Button>
      </div>
      <DialogContent>
        <FormControl style={{ width: '75%' }}>
          <Field
            component={ReduxSelect}
            label={I18n.t('dadp.wording')}
            list={timelyOptions}
            name="reviewId"
            margin="none"
            classes={{ root: styles.fieldContainer }}
          />
        </FormControl>
      </DialogContent>
      <div className={styles.footer}>
        <InlineButton
          buttons={[
            {
              _type: 'string',
              text: I18n.t('dadp.cancel'),
              color: 'default',
              variant: 'outlined',
              size: 'medium',
              onClick: onCancel
            },
            {
              _type: 'string',
              text: I18n.t('dadp.create'),
              size: 'medium',
              onClick: onConfirm
            }
          ]}
        />
      </div>
    </Dialog>
  );
};

// Props
TimelyDialog.propTypes = {
  open: PropTypes.bool.isRequired,

  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

export default TimelyDialog;
