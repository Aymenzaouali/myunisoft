import React from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import { InlineButton } from 'components/basics/Buttons';
import FormControl from '@material-ui/core/FormControl';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { FontIcon } from 'components/basics/Icon';
import { Field } from 'redux-form';
import { ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import { Dialog, DialogContent } from '@material-ui/core';

import I18n from 'assets/I18n';

import { permanentOptions } from 'assets/constants/data';
import Button from 'components/basics/Buttons/Button';

import styles from './permanentDialogs.module.scss';

// Component
const PermanentDialog = (props) => {
  const {
    open, onConfirm, onCancel
  } = props;

  // Rendering
  return (
    <Dialog
      open={open}
      onClose={onCancel}
      PaperProps={{
        style: {
          padding: '0px 0px 35px 35px'
        }
      }}
      fullWidth
      maxWidth="sm"
    >
      <div className={styles.header}>
        <div>
          <h2>{I18n.t('dadp.permanentData')}</h2>
        </div>
        <Button onClick={onCancel}>
          <FontIcon size={24} className="icon-close" />
        </Button>
      </div>
      <DialogContent>
        <InputLabel id="demo-simple-select-label">
          {I18n.t('dadp.date')}
        </InputLabel>
        <div style={{ display: 'flex' }}>
          <Field
            component={ReduxMaterialDatePicker}
            className={styles.fieldContainer}
            label={I18n.t('dadp.from')}
            name="startDate"
          />
          <Field
            component={ReduxMaterialDatePicker}
            className={styles.fieldContainer}
            label={I18n.t('dadp.to')}
            name="endDate"
            style={{ margin: '0 20px' }}
          />
        </div>

        <FormControl style={{ width: '75%', marginTop: 20 }}>
          <Field
            component={ReduxSelect}
            label={I18n.t('dadp.wording')}
            list={permanentOptions}
            name="reviewId"
            margin="none"
            classes={{ root: styles.fieldContainer }}
          />
        </FormControl>
      </DialogContent>
      <div className={styles.footer}>
        <InlineButton
          buttons={[
            {
              _type: 'string',
              text: I18n.t('dadp.cancel'),
              color: 'default',
              variant: 'outlined',
              size: 'medium',
              onClick: onCancel
            },
            {
              _type: 'string',
              text: I18n.t('dadp.create'),
              size: 'medium',
              onClick: onConfirm
            }
          ]}
        />
      </div>
    </Dialog>
  );
};

// Props
PermanentDialog.propTypes = {
  open: PropTypes.bool.isRequired,

  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

export default PermanentDialog;
