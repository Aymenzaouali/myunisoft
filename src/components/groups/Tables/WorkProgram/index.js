import React, { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';

import { useWindowState } from 'helpers/hooks';

import Window from 'components/basics/Window';
import { ReduxRadio } from 'components/reduxForm/Selections';
import ValidationDialog from 'components/groups/DADP/ValidationDialog';
import FilesDropperDialog from 'components/groups/Dialogs/FilesDropper';
import { TableFactory } from 'components/groups/Tables';
import TimelyDialog from './Timely';
import PermanentDialog from './Permanent';

// Component
const WorkProgram = (props) => {
  const {
    getDiligences,
    category,
    startDate,
    endDate,
    section,
    cycleId,
    openPermanent,
    openTimely,
    workSheetOnly,
    table,
    toggleTimelyDialog,
    togglePermanentDialog,
    sendDiligencePJ
  } = props;

  // State
  const [dialogState, setDialogState] = useState({
    open: false,
    onConfirm: null
  });
  const [dropper, setDropper] = useState(null);
  const overview = useWindowState();

  // Effects
  useEffect(() => {
    if (startDate && endDate && section && cycleId != null) {
      getDiligences(
        category,
        section.id_section,
        cycleId,
        startDate,
        endDate,
        openPermanent,
        openTimely,
        workSheetOnly
      );
    }
  }, [category, section, cycleId, startDate, endDate, openTimely, workSheetOnly]);

  // Rendering
  const finalTable = table;
  finalTable.param.body.row = table.param.body.row.map(row => ({
    ...row,

    value: row.value.map((cell) => {
      if (
        cell.keyCell === 'valid_rm'
        && cell._type === 'select'
        && cell.isParent
      ) {
        return {
          ...cell,
          props: {
            ...cell.props,

            onChange: (event) => {
              if (cell.allSame) {
                cell.bulkUpdate(event.target.value);
              } else {
                setDialogState({
                  open: true,
                  onConfirm: () => cell.bulkUpdate(event.target.value)
                });
              }
            }
          }
        };
      }

      if (cell.keyCell === 'pjs') {
        return {
          ...cell,
          props: {
            ...cell.props,
            onAdd: setDropper,
            onAskOverview: documents => overview.open({ documents }, true)
          }
        };
      }

      return cell;
    })
  }));

  return (
    <Fragment>
      <Field
        name="workSheetOnly"
        component={ReduxRadio}
        list={[
          { label: I18n.t('tables.workProgram.all'), value: '0' },
          { label: I18n.t('tables.workProgram.worksheet'), value: '1' }
        ]}
        row
      />
      <TableFactory {...finalTable} />

      <ValidationDialog
        open={dialogState.open}
        onCancel={() => {
          setDialogState({ open: false, onConfirm: null });
        }}
        onConfirm={() => {
          if (dialogState.onConfirm != null) {
            dialogState.onConfirm();
          }

          setDialogState({ open: false, onConfirm: null });
        }}
      />

      <TimelyDialog
        open={openTimely}
        onCancel={() => {
          toggleTimelyDialog(null);
        }}
        onConfirm={() => {
          toggleTimelyDialog(null);
        }}
      />

      <PermanentDialog
        open={openPermanent}
        startDate={startDate}
        endDate={endDate}
        onCancel={() => {
          togglePermanentDialog(null);
        }}
        onConfirm={() => {
          togglePermanentDialog(null);
        }}
      />

      <FilesDropperDialog
        open={dropper != null}
        onAddFiles={files => sendDiligencePJ(dropper, files)}
        onClose={() => setDropper(null)}
      />

      <Window
        path="/overview"
        window_id="overview"
        features={`width=${window.innerWidth / 2},left=${window.screenX
          + window.innerWidth / 2}`}
        {...overview.props}
      />
    </Fragment>
  );
};

// Props
WorkProgram.propTypes = {
  category: PropTypes.string.isRequired,
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  section: PropTypes.object,
  cycleId: PropTypes.number,
  workSheetOnly: PropTypes.oneOf(['0', '1']),
  table: PropTypes.object.isRequired,
  openPermanent: PropTypes.bool.isRequired,
  openTimely: PropTypes.bool.isRequired,
  toggleTimelyDialog: PropTypes.func.isRequired,
  togglePermanentDialog: PropTypes.func.isRequired,
  getDiligences: PropTypes.func.isRequired,
  sendDiligencePJ: PropTypes.func.isRequired
};

WorkProgram.defaultProps = {
  workSheetOnly: '0',

  startDate: null,
  endDate: null,
  section: null,
  cycleId: null
};

export default WorkProgram;
