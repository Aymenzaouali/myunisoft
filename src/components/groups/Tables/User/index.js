import React from 'react';
import PropTypes from 'prop-types';
import TableFactory from 'components/groups/Tables/Factory';
import './styles.scss';

class TableUser extends React.Component {
  render() {
    const {
      param,
      isLoading,
      isError
    } = this.props;

    return (
      <div>
        <TableFactory
          isLoading={isLoading}
          isError={isError}
          param={param}
          cellHeight={45}
          maxVisibleItems={10}
        />
      </div>
    );
  }
}

TableUser.propTypes = {
  isError: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  param: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default TableUser;
