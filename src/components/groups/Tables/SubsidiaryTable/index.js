import React, { useEffect } from 'react';
import FactoryTable from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';

const SubsidiaryTable = ({
  param, setTableData, subsidiary_list, ...otherProps
}) => {
  useEffect(() => {
    setTableData();
  }, [subsidiary_list]);
  return (
    <FactoryTable
      param={param}
      {...otherProps}
    />
  );
};

SubsidiaryTable.propTypes = {
  param: PropTypes.shape({}).isRequired,
  setTableData: PropTypes.func.isRequired,
  subsidiary_list: PropTypes.array.isRequired
};

export default SubsidiaryTable;
