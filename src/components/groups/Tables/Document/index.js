import React, { PureComponent } from 'react';
import {
  TableCell,
  TableHead,
  Table
} from '@material-ui/core';
import { FieldArray } from 'redux-form';
import DocumentArray from 'containers/groups/Tables/Document/DocumentArray';
import I18n from 'assets/I18n';
import ownStyles from './Document.module.scss';

class DocumentTable extends PureComponent {
  render() {
    return (
      <div className={ownStyles.tableContainer}>
        <Table>
          <TableHead>
            <TableCell key="Date">{I18n.t('physicalPersonCreation.document_form.dateHeader')}</TableCell>
            <TableCell key="Name">{I18n.t('physicalPersonCreation.document_form.nameHeader')}</TableCell>
            <TableCell />
          </TableHead>
          <FieldArray
            name="coord"
            component={DocumentArray}
          />
        </Table>
      </div>
    );
  }
}

export default DocumentTable;
