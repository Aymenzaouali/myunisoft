import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import { Field } from 'redux-form';
import {
  TableBody,
  TableCell,
  TableRow,
  IconButton,
  Typography
} from '@material-ui/core';
import { FontIcon } from 'components/basics/Icon';
import I18n from 'assets/I18n';
import _ from 'lodash';
import styles from './DocumentArray.module.scss';

class DocumentArray extends React.PureComponent {
  render() {
    const {
      societyId,
      documents,
      removeDocument,
      selectedPersPhysique
    } = this.props;

    const pers_physique_id = _.get(selectedPersPhysique, 'pers_physique_id');

    return (
      <Fragment>
        {documents.length === 0
          && (
            <TableRow>
              <TableCell />
              <Typography color="primary" variant="title">{I18n.t('physicalPersonCreation.document_form.empty')}</Typography>
            </TableRow>
          )
        }
        <TableBody>
          {
            documents && documents.map((field, i) => [
              <TableRow>
                <TableCell>
                  <Field
                    name={`documentInfo[${i}].date_time`}
                    component={ReduxMaterialDatePicker}
                    margin="none"
                    className={styles.dateField}
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`documentInfo[${i}].name`}
                    component={ReduxTextField}
                    margin="none"
                    className={styles.titleField}
                  />
                </TableCell>
                <TableCell>
                  <IconButton onClick={() => removeDocument(societyId, i, field, pers_physique_id)}>
                    <FontIcon name="icon-trash" color="red" />
                  </IconButton>
                </TableCell>
              </TableRow>
            ])
          }
        </TableBody>
      </Fragment>
    );
  }
}

DocumentArray.defaultProps = {
  documents: [],
  selectedPersPhysique: {}
};

DocumentArray.propTypes = {
  societyId: PropTypes.number.isRequired,
  removeDocument: PropTypes.func.isRequired,
  documents: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    token: PropTypes.string,
    thumbnail: PropTypes.string,
    date_time: PropTypes.string
  })),
  selectedPersPhysique: PropTypes.shape({
    pers_physique_id: PropTypes.number,
    name: PropTypes.string,
    firstname: PropTypes.string,
    maiden_name: PropTypes.string,
    date_birth: PropTypes.string,
    city_birth: PropTypes.string,
    department_birth: PropTypes.string,
    country_birth: PropTypes.string,
    social_security_number: PropTypes.string,
    comment: PropTypes.string,
    organism: PropTypes.string,
    Actif_: PropTypes.bool,
    country_address: PropTypes.string,
    address_number: PropTypes.string,
    address_bis: PropTypes.string,
    address: PropTypes.string,
    address_complement: PropTypes.string,
    postal_code: PropTypes.string,
    city: PropTypes.string,
    civility: PropTypes.string,
    marital_situation: PropTypes.string,
    matrimonial_regime: PropTypes.string,
    physical_person_type: {
      id: PropTypes.number,
      label: PropTypes.string,
      value: PropTypes.string
    },
    road_type: {
      id: PropTypes.number,
      label: PropTypes.string,
      value: PropTypes.string
    },
    coordonnee: PropTypes.arrayOf(PropTypes.shape({}))
  })
};

export default DocumentArray;
