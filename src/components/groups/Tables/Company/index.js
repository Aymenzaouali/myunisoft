import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import TableFactory from 'components/groups/Tables/Factory';
import { AccessDashboard } from 'containers/groups/Dialogs';

// Component
const TableCompany = (props) => {
  const {
    param,
    currentCompanies,
    checkChangesAndGetCompany,
    setCompanyTableData,
    deleteSociety,
    isUserAdmin,
    setDeletionDialog,
    ...otherProps
  } = props;

  const [society, setSociety] = useState({});
  const [isOpenAccess, setIsOpenAccess] = useState(false);

  const onValidateAccess = () => {
    setIsOpenAccess(false);
    checkChangesAndGetCompany(society && society.society_id);
  };

  const onCloseAccess = () => {
    setIsOpenAccess(false);
  };

  const body = currentCompanies.map((company, i) => {
    const {
      society_id,
      name,
      siret,
      city,
      ape,
      status,
      step,
      secured
    } = company;

    return {
      keyRow: `company${i}`,
      props: {
        hover: true,
        society_id,
        onClick: () => {
          if (secured) {
            setIsOpenAccess(true);
            setSociety(company);
          } else {
            checkChangesAndGetCompany(society_id);
          }
        }
      },
      value: [
        {
          _type: 'string',
          keyCell: `companyId${i}`,
          props: {
            label: society_id
          }
        },
        {
          _type: 'string',
          keyCell: `companyName${i}`,
          props: {
            label: name
          }
        },
        {
          _type: 'string',
          keyCell: `companyCity${i}`,
          props: {
            label: city
          }
        },
        {
          _type: 'string',
          keyCell: `companySiret${i}`,
          props: {
            label: siret
          }
        },
        {
          _type: 'string',
          keyCell: `companyStatus${i}`,
          props: {
            label: status
          }
        },
        {
          _type: 'string',
          keyCell: `companyStep${i}`,
          props: {
            label: step
          }
        },
        {
          _type: 'string',
          keyCell: `companyApe${i}`,
          props: {
            label: ape
          }
        },
        {
          _type: 'iconButton',
          keyCell: `deleteCompany${i}`,
          props: {
            name: 'icon-trash',
            hidden: !isUserAdmin,
            onClick: (e) => {
              e.stopPropagation();
              setDeletionDialog('company', company.society_id);
            }
          }
        }
      ]
    };
  });


  const formattedParam = {
    ...param,
    body: {
      props: {},
      row: body
    }
  };

  return (
    <Fragment>
      <TableFactory
        param={formattedParam}
        onRefresh={() => setCompanyTableData(formattedParam)}
        {...otherProps}
      />

      <AccessDashboard
        isOpen={isOpenAccess}
        society={society}
        onValidate={onValidateAccess}
        onClose={onCloseAccess}
      />
    </Fragment>
  );
};

// Props
TableCompany.propTypes = {
  param: PropTypes.object.isRequired,
  currentCompanies: PropTypes.array.isRequired,
  checkChangesAndGetCompany: PropTypes.func.isRequired,
  setCompanyTableData: PropTypes.func.isRequired,
  deleteSociety: PropTypes.func.isRequired,
  isUserAdmin: PropTypes.bool.isRequired,
  setDeletionDialog: PropTypes.func.isRequired
};

export default TableCompany;
