This component is a function for generate a Table Dynamique. For this it use param props.
Param is compose of 3 part 'header / body / footer'

Param:

  * props: The props send to MaterialUi element
  * header: array of `object row` to generate rows in header
  * body: array of `object row` to generate rows in body
  * footer: array of `object row` to generate rows in footer

Row:
  * key: The key of the row
  * props: The props send to MaterialUi element
  * value: array of `object cell` to generate cells

Cell:
  * _type: cell type used
  * keyCell: cell key
  * props: set of props link to cell type
  * cellProp: props send to MaterialUi element

For more details on the types of Cell look:
  - [HeaderCell](#!/HeaderCell)
  - [BodyCell](#!/BodyCell)
  - [FooterCell](#!/FooterCell)

```js
<FactoryTable 
  param={{
    header: {
      props: {},
      row: [{
          keyRow:'row1',
          props: {},
          value:[{
            _type: 'string',
            keyCell:"header1 cell1",
            props: {
              label: 'header1 cell1'
            }
          }, {
            _type: 'string',
            keyCell:"header1 cell2",
            props: {
              label: 'header1 cell2'
            }
          }, {
            _type: 'string',
            keyCell:"header1 cell3",
            props: {
              label: 'header1 cell3'
            }
          }]
        },
        {
          keyRow:'row2',
          props: {},
          value:[{
            _type: 'string',
            keyCell:"header2 cell1",
            props: {
              label: 'header2 cell1'
            }
          }, {
            _type: 'string',
            keyCell:"header2 cell2",
            props: {
              label: 'header2 cell2'
            }
          }, {
            _type: 'string',
            keyCell:"header2 cell3",
            props: {
              label: 'header2 cell3'
            }
          }]
        },
      ]
    },
    body:{ 
      props: {},
      row: [{
        keyRow:'row3',
        props: {},
        value:[{
          _type: 'string',
          keyCell:"body-1 cell-1",
          props: {
            label: 'body-1 cell-1'
          }
        }, {
          _type: 'string',
          keyCell:"body-1 cell-2",
          props: {
            label: 'body-1 cell-2'
          }
        }, {
          _type: 'string',
          keyCell:"body-1 cell-3",
          props: {
            label: 'body-1 cell-3'
          }
        }]
      },
      {
        keyRow:'row4',
        props: {},
        value:[{
          _type: 'string',
          keyCell:"body-2 cell-1",
          props: {
            label: 'body-2 cell-1'
          }
        }, {
          _type: 'string',
          keyCell:"body-2 cell-2",
          props: {
            label: 'body-2 cell-2'
          }
        }, {
          _type: 'string',
          keyCell:"body-2 cell-3",
          props: {
            label: 'body-2 cell-3'
          }
        }]
      }]
    },
    footer: {
      props: {},
      row: [{
        keyRow:'row5',
        props: {},
        value:[{
          _type: 'string',
          keyCell:"footer-1 cell-1",
          props: {
            label: 'footer-1 cell-1'
          }
        }, {
          _type: 'none',
          keyCell:"none1",
        }, {
          _type: 'string',
          keyCell:"footer-1 cell-3",
          props: {
            label: 'footer-1 cell-3'
          }
        }]
      },
      {
        keyRow:'row6',
        props: {},
        value:[{
          _type: 'string',
          keyCell:"footer-2 cell-1",
          props: {
            label: 'footer-2 cell-1'
          }
        }, {
          _type: 'string',
          keyCell:"footer-2 cell-2",
          props: {
            label: 'footer-2 cell-2'
          }
        }, {
          _type: 'string',
          keyCell:"footer-2 cell-3",
          props: {
            label: 'footer-2 cell-3'
          }
        }]
      }]
    }
  }}
/>
```
