import React, { Fragment, PureComponent } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { FieldArray } from 'redux-form';
import _ from 'lodash';
import {
  TableHead,
  TableBody,
  TableFooter,
  TableRow,
  Table
} from '@material-ui/core';
import { ScrollTopButton } from 'components/basics/Buttons';
import Popover from 'components/basics/Popovers/Buttons';
import { ConfirmationDialog } from 'components/groups/Dialogs';
import {
  HeaderCell,
  BodyCell,
  FooterCell,
  LoadingCell,
  ErrorCell,
  NoDataCell
} from 'components/basics/Cells';
import I18n from 'assets/I18n';
import { AddTableRowToolbar } from 'containers/groups/ToolBars';

import styles from './TablesFactory.module.scss';

class FactoryTable extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      popover: {
        isOpen: false,
        buttons: [],
        left: 0,
        top: 0
      },
      from: 0,
      to: props.maxVisibleItems
    };
    this.closePopover = this.closePopover.bind(this);

    this.scrollContainer = React.createRef();
  }

  componentDidMount() {
    const {
      loadData,
      maxVisibleItems
    } = this.props;

    loadData();

    if (maxVisibleItems) this.scrollContainer.current.addEventListener('scroll', this.scrollTable);
  }

  componentDidUpdate(prevProps) {
    const { isLoading, maxVisibleItems, onRefresh } = this.props;

    if (isLoading !== prevProps.isLoading && maxVisibleItems) {
      this.setState({ // eslint-disable-line
        from: 0,
        to: maxVisibleItems
      });
      this.scrollContainer.current.scrollTo(0, 0);
    }
    if (isLoading !== prevProps.isLoading) {
      onRefresh();
    }
  }

  componentWillUnmount() {
    const { maxVisibleItems } = this.props;

    if (maxVisibleItems) this.scrollContainer.current.removeEventListener('scroll', this.scrollTable);
  }

  onRightClick(e, type, value) {
    e.preventDefault();
    if (_.get(this.props, `param.${type}.rightClickActions`, false)) {
      const existHandleButtons = _.get(this.props, `param.${type}.handleButtons`) !== undefined;
      let buttons = [];
      if (existHandleButtons) {
        buttons = _.get(this.props, `param.${type}.handleButtons`)(value);
      } else {
        buttons = _.get(this.props, `param.${type}.rightClickActions`, []);
      }
      const popover = {
        isOpen: true,
        left: e.clientX,
        top: e.clientY,
        buttons,
        value
      };
      this.setState({
        popover
      });
    }
  }

  scrollTable = (e) => {
    const { param: { body = { row: [] } }, cellHeight, maxVisibleItems } = this.props;
    const { scrollTop } = e.target;
    const from = Math.round(scrollTop / cellHeight);
    const to = from + maxVisibleItems + 1 >= body.row.length
      ? body.row.length
      : from + maxVisibleItems + 1;

    // clearTimeout(this.timeout);

    // this.timeout = setTimeout(()=>{
    this.setState({ from: from - 1 <= 0 ? 0 : from - 1, to });
    // }, 100);
  }

    addTableToolbar = () => {
      const { addBodyRowToolbar } = this.props;

      if (!addBodyRowToolbar) {
        return null;
      }

      /**
         * TODO: Props below should be
         * handled in a concrete Table container
         */
      return (
        <AddTableRowToolbar
          isSaveEnabled={false}
          isEditEnabled={false}
          isDeleteEnabled={false}
          count={0}
        />
      );
    };

    addTableRow = () => {
      const { addBodyRowCb } = this.props;

      if (!addBodyRowCb) {
        return null;
      }

      const {
        onClick,
        colorError
      } = addBodyRowCb;

      return (
        <TableRow
          key="add_body_row"
          className={classnames(styles.addRow, { [styles.addRowError]: colorError })}
          hover
          onClick={onClick}
        >
          <BodyCell
            cellProp={{
              colSpan: 9999
            }}
            _type="string"
            props={{
              cellClassName: styles.addRowCell,
              label: I18n.t('tables.addNewRow'),
              iconLeft: 'icon-plus',
              style: {
                display: 'flex',
                alignItems: 'center'
              },
              iconLeftClassName: styles.addIcon
            }}
          />
        </TableRow>
      );
    };

    closePopover() {
      const { popover } = this.state;
      this.setState({
        popover: {
          ...popover,
          isOpen: false
        }
      });
    }

    renderTableBody = (values) => {
      const { fieldArrayProps } = this.props;

      if (fieldArrayProps) {
        const {
          formName,
          component
        } = fieldArrayProps;

        return (
          <FieldArray
            name={formName}
            component={component}
            {...values}
          />
        );
      }

      return values.map(value => (
        <BodyCell
          key={value.keyCell}
          {...value}
        />
      ));
    };

    render() {
      const {
        param,
        isLoading,
        isRowLoading,
        isError,
        confirmation,
        onExited,
        // removing warnings
        loadData,
        arrayBody,
        addBodyRowCb,
        addBodyRowToolbar,
        fieldArrayProps,
        cellHeight,
        maxVisibleItems,
        shortcutToTop,
        ...props
      } = this.props;
      const { popover, from, to } = this.state;

      const {
        header,
        body = { row: [] },
        footer
      } = param;

      const noData = !_.isUndefined(body)
            && (_.isUndefined(body.row) || body.row.length === 0)
            && isLoading === false
            && isError === false;

      const headerLength = _.get(param, 'header.row[0].value.length', 0);
      const bodyRows = maxVisibleItems ? body.row.slice(from, to) : body.row;

      return (
        <Fragment>
          {this.addTableToolbar()}
          <div
            className={classnames({ [styles['scroll-container']]: maxVisibleItems })}
            ref={this.scrollContainer}
            style={{ 'max-height': `${maxVisibleItems * cellHeight}px` }}
          >
            <Table
              {..._.omit(props, ['arrayBody', 'loadData', 'addBodyRowCb', 'addBodyRowToolbar', 'fieldArrayProps'])}
              className={
                classnames(styles.tableFactory,
                  props.className,
                  { [styles.stickyTable]: maxVisibleItems })
              }
            >
              {!_.isEmpty(header) // If header exist
                    && (
                      <TableHead {...header.props}>
                        {header.row.map(arrayHeader => (
                          <TableRow
                            key={arrayHeader.keyRow}
                            onContextMenu={e => this.onRightClick(e, 'header', arrayHeader)}
                            {...arrayHeader.props}
                          >
                            {arrayHeader.value.map(headerValue => (
                              <HeaderCell
                                key={headerValue.keyCell}
                                {...props}
                                {...headerValue}
                              />
                            ))}
                          </TableRow>
                        ))}
                      </TableHead>
                    )}
              {!_.isEmpty(body) // If body exist
                    && (
                      <TableBody {...body.props}>
                        {
                          isRowLoading && (
                            <TableRow>
                              {
                                _.get(param, 'header.row[0].value', []).map(() => (
                                  <BodyCell _type="loadingCell" />
                                ))
                              }
                            </TableRow>
                          )
                        }
                        {(isLoading || isError || noData)
                            && (
                              <TableRow>
                                {isLoading && <LoadingCell colSpan={headerLength} />}
                                {isError && <ErrorCell colSpan={headerLength} />}
                                {noData && <NoDataCell colSpan={headerLength} />}
                              </TableRow>
                            )
                        }
                        {!isLoading && this.addTableRow()}
                        { maxVisibleItems && body.row.length > 0 && <tr style={{ height: `${from * cellHeight}px` }} /> }
                        {bodyRows.map((arrayBody, index) => (
                          <TableRow
                            key={arrayBody.keyRow}
                            {..._.get(arrayBody, 'props')}
                            className={classnames(
                              _.get(arrayBody, 'props.className'),
                              {
                                [styles.selected]: _.get(arrayBody, 'props.isSelected'),
                                [styles.oddRow]: (from - index) % 2
                              }
                            )}
                            onContextMenu={e => this.onRightClick(e, 'body', arrayBody)}
                          >
                            {this.renderTableBody(arrayBody.value)}
                          </TableRow>
                        ))}
                        { maxVisibleItems && body.row.length > 0 && <tr style={{ height: `${(body.row.length - to) * cellHeight}px` }} /> }
                      </TableBody>
                    )}
              {!_.isEmpty(footer) // If footer exist
                    && (
                      <TableFooter {...footer.props}>
                        {footer.row.map(arrayFooter => (
                          <TableRow
                            key={arrayFooter.keyRow}
                            onContextMenu={e => this.onRightClick(e, 'footer', arrayFooter)}
                            {...arrayFooter.props}
                          >
                            {arrayFooter.value.map(footerValue => (
                              <FooterCell
                                key={footerValue.keyCell}
                                {...footerValue}
                              />
                            ))}
                          </TableRow>
                        ))}
                      </TableFooter>
                    )}
            </Table>
            {shortcutToTop
              && (
                <ScrollTopButton
                  containerRef={this.scrollContainer}
                />
              )
            }
          </div>
          <Popover
            onClose={this.closePopover}
            onExited={onExited}
            {...popover}
          />
          {!_.isEmpty(confirmation) && (
            <ConfirmationDialog
              {...confirmation}
            />
          )}
        </Fragment>
      );
    }
}

FactoryTable.defaultProps = {
  maxVisibleItems: null,
  confirmation: {},
  param: {},
  loadData: () => {
  },
  onRefresh: () => {
  },
  onExited: () => {
  },
  arrayBody: [{}],
  addBodyRowCb: undefined,
  addBodyRowToolbar: undefined,
  isLoading: false,
  isError: false,
  isRowLoading: false,
  fieldArrayProps: undefined,
  cellHeight: undefined,
  shortcutToTop: false
};

FactoryTable.propTypes = {
  confirmation: PropTypes.shape({}),
  isError: PropTypes.bool,
  isLoading: PropTypes.bool,
  isRowLoading: PropTypes.bool,
  arrayBody: PropTypes.arrayOf(PropTypes.shape({})),
  onExited: PropTypes.func,
  loadData: PropTypes.func,
  onRefresh: PropTypes.func,
  addBodyRowCb: PropTypes.shape({}),
  addBodyRowToolbar: PropTypes.func,
  fieldArrayProps: PropTypes.shape({
    formName: PropTypes.string.isRequired,
    component: PropTypes.element.isRequired
  }),
  /** param object of parametrage use for the generation of the Table */
  param: PropTypes.shape({
    header: PropTypes.shape({
      props: PropTypes.shape({}),
      row: PropTypes.arrayOf(
        PropTypes.shape({
          keyRow: PropTypes.string.isRequired,
          props: PropTypes.shape({}),
          value: PropTypes.arrayOf(
            PropTypes.shape({
              _type: PropTypes.string.isRequired,
              keyCell: PropTypes.string.isRequired,
              props: PropTypes.shape
            })
          ).isRequired
        })
      )
    }),
    body: PropTypes.shape({
      props: PropTypes.shape({}),
      row: PropTypes.arrayOf(
        PropTypes.shape({
          keyRow: PropTypes.string.isRequired,
          props: PropTypes.shape({}),
          value: PropTypes.arrayOf(
            PropTypes.shape({
              _type: PropTypes.string.isRequired,
              keyCell: PropTypes.string.isRequired,
              props: PropTypes.shape
            })
          ).isRequired
        })
      )
    }),
    footer: PropTypes.shape({
      props: PropTypes.shape({}),
      row: PropTypes.arrayOf(
        PropTypes.shape({
          keyRow: PropTypes.string.isRequired,
          props: PropTypes.shape({}),
          value: PropTypes.arrayOf(
            PropTypes.shape({
              _type: PropTypes.string.isRequired,
              keyCell: PropTypes.string.isRequired,
              props: PropTypes.shape
            })
          ).isRequired
        })
      )
    })
  }),
  maxVisibleItems: PropTypes.number,
  cellHeight: PropTypes.number,
  shortcutToTop: PropTypes.bool
};

export default FactoryTable;
