import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import TableFactory from 'components/groups/Tables/Factory';
import { AccessDashboard } from 'containers/groups/Dialogs';
import {
  HISTORYABBREVIATIONSBYKEY
} from 'helpers/dashboard';
import moment from 'moment';

// Component
const TrackingSocietyTable = (props) => {
  const {
    param,
    formatedData,
    handleFolderClick,
    handleBubbleClick,
    onRightClick,
    selectedPeriod,
    currentMode,
    getPeriod,
    ...otherProps
  } = props;

  const [society, setSociety] = useState({});
  const [isOpenAccess, setIsOpenAccess] = useState(false);
  const [data, setData] = useState({});

  const onValidateAccess = () => {
    setIsOpenAccess(false);
    if (data.type === 'folder') {
      handleFolderClick(data.data, society);
    } else {
      handleBubbleClick(data.data.key, data.data.company_id, data.data.cell, society);
    }
  };

  const onCloseAccess = () => {
    setIsOpenAccess(false);
  };

  const handleFolder = (society = {}, dataToSet, callBack) => {
    if (society.secured) {
      setData({ type: 'folder', data: dataToSet });
      setSociety({ ...society, society_id: society.company_id, name: society.company_name });
      setIsOpenAccess(true);
    } else {
      callBack();
    }
  };

  const handleBubble = (society = {}, dataToSet, callBack) => {
    if (society.secured) {
      setData({ type: 'bubble', data: dataToSet });
      setSociety({ ...society, society_id: society.company_id, name: society.company_name });
      setIsOpenAccess(true);
    } else {
      callBack();
    }
  };


  const renderCell = (cell, index, soc) => {
    let formatCell;

    const {
      period
    } = selectedPeriod;

    if ((cell[1] !== null) && (cell[1] !== undefined)) {
      const {
        type,
        data,
        company_id,
        company_name
      } = cell[1];

      const cellType = type || 'string';

      let MMAA;
      let isDeclaration;

      formatCell = {
        _type: cellType !== 'folderName' ? cellType : 'string',
        keyCell: `trackingSocietyBody.${cell[0]}${index}`,
        cellProp: {
          style: {
            zIndex: 1
          }
        }
      };

      switch (cellType) {
      case 'string':
        formatCell = {
          ...formatCell,
          props: {
            label: cell[1]
          },
          cellProp: {
            onClick: () => {
              handleFolder(cell[1], cell[1], handleFolderClick.bind(this, cell[1], soc));
            }
          }
        };
        break;
      case 'folderName':
        formatCell = {
          ...formatCell,
          props: {
            label: company_name
          },
          cellProp: {
            onClick: () => {
              handleFolder(
                cell[1],
                cell[1].company_id,
                handleFolderClick.bind(this, company_id, soc)
              );
            }
          }
        };
        break;
      case 'history':
        const periodHistory = getPeriod(cell[0]);
        MMAA = moment(periodHistory).format('MMYY');

        formatCell = {
          ...formatCell,
          props: {
            data: data.map((v) => {
              const type = v.key.substring(0, 2) === 'is' ? 'is' : v.key;
              isDeclaration = type === 'vat' || type === 'cvae' || type === 'is';

              return {
                value: HISTORYABBREVIATIONSBYKEY[v.key],
                status: v.status,
                onClick: () => {
                  handleBubble(soc, {
                    key: v.key,
                    company_id,
                    cell: cell[0]
                  }, handleBubbleClick.bind(this, v.key, company_id, cell[0], soc));
                },
                onRightClick: isDeclaration && (
                  e => onRightClick(e, MMAA, v.id_diligence, company_id, type, true)
                ),
                clickable: true
              };
            })
          }
        };
        break;
      case 'status':
        const {
          id_diligence
        } = data;

        MMAA = period.substring(5, 7) + period.substring(2, 4);

        const type = cell[0] === 'first_is_deposit' ? 'is' : cell[0];
        isDeclaration = type === 'vat' || type === 'cvae' || type === 'is';

        formatCell = {
          ...formatCell,
          props: {
            data
          },
          cellProp: {
            ...formatCell.cellProp,
            onClick: () => {
              handleBubble(soc, {
                key: cell[0],
                company_id
              }, handleBubbleClick.bind(this, cell[0], company_id, undefined, soc));
            },
            onContextMenu: (isDeclaration && currentMode !== 1) && (
              e => onRightClick(e, MMAA, id_diligence, company_id, type)
            )
          }
        };
        break;
      default:
        formatCell = {
          ...formatCell,
          props: {
            label: ''
          }
        };
        break;
      }
    } else {
      formatCell = {
        ...formatCell,
        props: {
          label: ''
        }
      };
    }

    return formatCell;
  };

  const body = formatedData.map((row, index) => ({
    keyRow: `trackingSocietyBodyRow-${row.company.company_name}${index}`,
    props: {
      hover: true
    },
    value: Object.entries(row).map((cell, index) => renderCell(cell, index, row.company))
  }));


  const formattedParam = {
    ...param,
    body: {
      props: {},
      row: body
    }
  };


  return (
    <Fragment>
      <TableFactory
        param={formattedParam}
        {...otherProps}
      />

      <AccessDashboard
        isOpen={isOpenAccess}
        society={society}
        onValidate={onValidateAccess}
        onClose={onCloseAccess}
      />
    </Fragment>
  );
};

// Props
TrackingSocietyTable.propTypes = {
  param: PropTypes.object.isRequired,
  formatedData: PropTypes.array.isRequired,
  handleFolderClick: PropTypes.func.isRequired,
  handleBubbleClick: PropTypes.func.isRequired,
  onRightClick: PropTypes.func.isRequired,
  getPeriod: PropTypes.func.isRequired,
  selectedPeriod: PropTypes.shape({}).isRequired,
  currentMode: PropTypes.string.isRequired
};

TrackingSocietyTable.defaultProps = {
};

export default TrackingSocietyTable;
