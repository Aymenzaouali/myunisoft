import React, { Fragment, useState, useEffect } from 'react';
import {
  Table, TableBody,
  TableHead, TableRow, withStyles
} from '@material-ui/core';
import {
  HeaderCell,
  BodyCell,
  LoadingCell,
  ErrorCell,
  NoDataCell
} from 'components/basics/Cells';
import Popover from 'components/basics/Popovers/Buttons';
import _ from 'lodash';
import { getRouteByKey } from 'helpers/routes';
import {
  rightClickActions,
  getMonthBounds,
  getRouteKeyForDashboard
} from 'helpers/dashboard';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './progressionTable.module.scss';

const ProgressionTable = (props) => {
  const {
    isLoading,
    isError,
    loadData,
    body,
    headers,
    redirectFromCollab,
    society_id,
    changeAccountingFilter,
    clearDataForAccounting,
    clearFilterForVat,
    clearFilterForIs,
    clearFilterForCvae,
    getLink,
    _initPeriod,
    getInfoForInitPeriod
  } = props;

  // State
  const [tableBody, settableBody] = useState([]);
  const [popover, setpopover] = useState({
    isOpen: false,
    buttons: [],
    left: 0,
    top: 0
  });

  // Effects
  useEffect(() => {
    clearDataForAccounting();
    clearFilterForVat();
    clearFilterForIs();
    clearFilterForCvae();
    loadData();
  }, []);

  // Rendering
  const checkLevel = (idChild) => {
    let level = 0;

    const parent = body.find(item => item.id === idChild);
    if (!parent.parent_id) {
      level = 1;
    } else {
      level = 2;
    }

    return level;
  };

  const onRightClick = (e, month, id_diligence, type) => {
    const {
      initFilter,
      initHeader,
      headerName
    } = getInfoForInitPeriod(type);

    e.preventDefault();
    if (rightClickActions) {
      setpopover({
        isOpen: true,
        left: e.clientX,
        top: e.clientY,
        buttons: rightClickActions(
          month.MMAA,
          id_diligence,
          society_id,
          redirectFromCollab,
          getLink,
          type,
          false,
          initFilter,
          initHeader,
          headerName
        )
      });
    }
  };

  const closePopover = () => {
    setpopover({
      ...popover,
      isOpen: false
    });
  };

  const monthList = headers.slice(2);

  const genOnClickAction = async (type, id_item, month, type_flow, diary_code) => {
    const date = getMonthBounds(month.MMAA);
    const dateStart = date.start;
    const dateEnd = date.end;

    let onClickAction;
    let route;

    switch (type) {
    case 'flow':
      await changeAccountingFilter(dateStart, dateEnd, diary_code);
      route = getRouteByKey(type_flow);
      await redirectFromCollab(route.path, type_flow, society_id);
      break;
    case 'accounting':
      await changeAccountingFilter(dateStart, dateEnd, diary_code);
      route = getRouteByKey('e');
      await redirectFromCollab(route.path, 'e', society_id);
      break;
    case 'internal_transfer':
    case 'waiting_account':
      route = getRouteByKey('accountConsultation');
      await redirectFromCollab(`${route.path}?accountId=${id_item}&dateStart=${dateStart}&dateEnd=${dateEnd}`, '', society_id);
      break;
    case 'TVA':
      await _initPeriod('vat', moment(dateStart).format('YYYY-MM'));
      route = getRouteByKey(getRouteKeyForDashboard('vat'));
      await redirectFromCollab(route.path, '', society_id);
      break;
    case 'IS':
      await _initPeriod('is', moment(dateStart).format('YYYY-MM'));
      route = getRouteByKey(getRouteKeyForDashboard('is'));
      await redirectFromCollab(route.path, '', society_id);
      break;
    case 'CVAE':
      await _initPeriod('cvae', moment(dateStart).format('YYYY-MM'));
      route = getRouteByKey(getRouteKeyForDashboard('cvae'));
      await redirectFromCollab(route.path, '', society_id);
      break;
    default:
      break;
    }

    return onClickAction;
  };

  const wrapRollRows = (id) => {
    const table = tableBody.map((item) => {
      const row = { ...item };

      if (row.id === id) {
        row.isVisible = true;
        row.isSelected = !row.isSelected;
      } else if (row.parent_id === id) {
        row.isVisible = !row.isVisible;
      } else if (row.grdParent_id === id && row.isVisible === true) {
        row.isVisible = false;
      }
      return row;
    });

    settableBody(table);
  };

  const renderHeaderCells = (header, index) => {
    const cellType = header.type === 'sortable' ? 'sortCell' : 'string';
    const props = header.type === 'sortable'
      ? {
        children: header.label,
        direction: 'desc'
      }
      : {
        label: header.label,
        typoVariant: 'h5'
      };

    return (
      <HeaderCell
        key={`ProgressionHeader${header.label}${index}`}
        _type={cellType}
        props={props}
      />
    );
  };

  const setBody = (body) => {
    const tableBody = [];
    let grdParent_id;

    body.map((data) => {
      if (!data.parent_id) {
        grdParent_id = data.id;
        tableBody.push(
          {
            id: data.id,
            parent_id: null,
            grdParent_id: null,
            isVisible: true,
            isSelected: false,
            level: 0,
            label: data.label,
            total: data.total,
            id_item: data.id_item,
            code: data.code,
            type_flow: data.type,
            haveChildren: body.indexOf(body.find(item => item.parent_id === data.id)) !== -1,
            data: data.data
          }
        );
      } else {
        tableBody.splice(tableBody.findIndex(item => item.id === data.parent_id) + 1, 0,
          {
            id: data.id,
            parent_id: data.parent_id,
            grdParent_id: checkLevel(data.parent_id) === 2 ? grdParent_id : null,
            isVisible: checkLevel(data.parent_id) === 1,
            isSelected: body.indexOf(body.find(item => item.parent_id === data.id)) !== -1,
            level: checkLevel(data.parent_id),
            haveChildren: body.indexOf(body.find(item => item.parent_id === data.id)) !== -1,
            label: data.label,
            total: data.total,
            id_item: data.id_item,
            code: data.code,
            type_flow: data.type,
            data: data.data
          });
      }

      return ('');
    });

    settableBody(tableBody);
  };

  useEffect(() => {
    setBody(body);
  }, [body]);

  const noData = _.isEmpty(body);

  const renderRow = (row, index) => {
    const {
      id,
      label,
      total,
      data,
      parent_id,
      isSelected,
      haveChildren,
      level,
      id_item,
      type_flow,
      code
    } = row;

    const typeDeclaration = label === 'TVA' ? 'vat' : label.toLowerCase();

    let labelClassName;
    let type;

    if (label.startsWith('58')) {
      type = 'internal_transfer';
    } else if (label.startsWith('47')) {
      type = 'waiting_account';
    } else if (type_flow === 'ib' || type_flow === 'm' || type_flow === 'o' || type_flow === 'ext') {
      type = 'flow';
    } else if (code) {
      type = 'accounting';
    } else {
      type = label;
    }

    if (parent_id === null) {
      labelClassName = styles.rowParent;
    } else if (level === 1) {
      labelClassName = styles.rowChildLevel1;
    } else {
      labelClassName = styles.rowChildLevel2;
    }

    const cells = [];
    const taskCell = {
      _type: 'string',
      keyCell: `ProgressionTableCellTask${id}`,
      cellClassName: styles.bodyCellTask,
      props: {
        label,
        labelClassName,
        iconRight: (haveChildren) && (isSelected ? styles.arrowDown : styles.arrowUp),
        iconRightClassName: { root: { paddingTop: '10px' } },
        cellClassName: styles.taskCell
      },
      cellProp: {
        onClick: () => {
          wrapRollRows(id);
        }
      }
    };

    cells.push(taskCell);

    const totalCell = {
      _type: 'string',
      keyCell: `ProgressionTableCellTotal${id}`,
      props: {
        label: total
      }
    };

    cells.push(totalCell);

    data.forEach((month, i) => {
      const {
        status,
        id_diligence
      } = month.table_status[0];

      const isDeclaration = type === 'TVA' || type === 'CVAE' || type === 'IS';

      let cellProps;

      if (parent_id && !haveChildren) {
        cellProps = {
          _type: 'status',
          keyCell: `ProgressionTableCellMonth${i}`,
          props: {
            data: month.table_status,
            clickable: true
          },
          cellProp: {
            onClick: () => genOnClickAction(type, id_item, monthList[i], type_flow, code),
            onContextMenu: isDeclaration && (
              e => onRightClick(e, monthList[i], id_diligence, typeDeclaration)
            )
          }
        };
      } else {
        cellProps = {
          _type: 'status',
          keyCell: `ProgressionTableCellMonth${i}`,
          props: {
            cellClassName: styles.bodyCellMonthParent,
            data: month.table_status,
            clickable: false
          }
        };
      }

      const cell = status !== ''
        ? cellProps
        : {
          _type: 'string',
          keyCell: `ProgressionTableCellMonth${i}`,
          props: {
            label: status
          }
        };

      cells.push(cell);
    });

    return (
      <TableRow key={`ProgressionBodyRow${index}`}>
        {cells.map(cell => (
          <BodyCell
            key={`${cell.keyCell}-${index}`}
            cellClassName={cell.cellClassName}
            _type={cell._type}
            props={cell.props}
            cellProp={cell.cellProp}
          />
        ))}
      </TableRow>
    );
  };

  const renderBody = () => {
    const bodyData = [];
    const headerLength = headers.length;

    if (isLoading) {
      bodyData.push(
        <TableRow>
          <LoadingCell colSpan={headerLength} />
        </TableRow>
      );
      return bodyData;
    } if (isError) {
      bodyData.push(
        <TableRow>
          <ErrorCell colSpan={headerLength} />
        </TableRow>
      );
      return bodyData;
    } if (noData) {
      bodyData.push(
        <TableRow>
          <NoDataCell colSpan={headerLength} />
        </TableRow>
      );
      return bodyData;
    }
    tableBody.forEach((item, index) => item.isVisible && bodyData.push(renderRow(item, index)));
    return bodyData;
  };

  return (
    <Fragment>
      <Table className={styles.table}>
        <TableHead className={styles.header}>
          <TableRow className={styles.headerCellTask}>
            {headers.map((header, index) => renderHeaderCells(header, index))}
          </TableRow>
        </TableHead>
        <TableBody>
          {renderBody(body)}
        </TableBody>
      </Table>
      <Popover
        onClose={closePopover}
        // onExited={onExited}
        {...popover}
      />
    </Fragment>
  );
};


ProgressionTable.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  loadData: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  body: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  headers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  redirectFromCollab: PropTypes.func.isRequired,
  society_id: PropTypes.string.isRequired,
  changeAccountingFilter: PropTypes.func.isRequired,
  clearDataForAccounting: PropTypes.func.isRequired,
  clearFilterForVat: PropTypes.func.isRequired,
  clearFilterForIs: PropTypes.func.isRequired,
  clearFilterForCvae: PropTypes.func.isRequired,
  _initPeriod: PropTypes.func.isRequired,
  getInfoForInitPeriod: PropTypes.func.isRequired,
  getLink: PropTypes.func.isRequired
};

export default withStyles(styles)(ProgressionTable);
