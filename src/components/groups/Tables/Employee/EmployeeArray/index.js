import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  TableBody,
  TableCell,
  TableRow,
  IconButton,
  Typography
} from '@material-ui/core';
import { FontIcon } from 'components/basics/Icon';
import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';
import ownStyles from './EmployeeArray.module.scss';

class EmployeeArray extends React.PureComponent {
  render() {
    const {
      societyId,
      firm,
      employmentFunctionTypes,
      signatoryFunctionTypes,
      removeSociety
    } = this.props;

    return (
      <Fragment>
        {firm.length === 0
          && (
            <TableRow>
              <TableCell />
              <Typography color="primary" variant="title">{I18n.t('physicalPersonCreation.society_form.empty')}</Typography>
            </TableRow>
          )
        }
        <TableBody>
          {
            firm.length !== 0 && firm.map((field, i) => [
              <TableRow>
                <TableCell>
                  <Field
                    name={`society[${i}].name`}
                    component={ReduxTextField}
                    margin="none"
                    className={ownStyles.societyNameField}
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`society[${i}].function_id`}
                    component={ReduxSelect}
                    margin="none"
                    list={employmentFunctionTypes}
                    className={ownStyles.functionLabelField}
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`society[${i}].signatory_function_id`}
                    component={ReduxSelect}
                    margin="none"
                    list={signatoryFunctionTypes}
                    className={ownStyles.functionLabelField}
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`society[${i}].start_date`}
                    component={ReduxMaterialDatePicker}
                    margin="none"
                    className={ownStyles.dateField}
                    disabled
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`society[${i}].end_date`}
                    component={ReduxMaterialDatePicker}
                    margin="none"
                    className={ownStyles.dateField}
                    disabled
                  />
                </TableCell>
                <TableCell>
                  <IconButton
                    onClick={() => removeSociety(societyId, i, field)}
                  >
                    <FontIcon name="icon-trash" color="red" />
                  </IconButton>
                </TableCell>
              </TableRow>
            ])
          }
        </TableBody>
      </Fragment>
    );
  }
}

EmployeeArray.propTypes = {
  societyId: PropTypes.number.isRequired,
  firm: PropTypes.arrayOf(PropTypes.object).isRequired,
  signatoryFunctionTypes: PropTypes.arrayOf(PropTypes.object).isRequired,
  employmentFunctionTypes: PropTypes.arrayOf(PropTypes.object).isRequired,
  removeSociety: PropTypes.func.isRequired
};

export default EmployeeArray;
