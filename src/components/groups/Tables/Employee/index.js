import React, { PureComponent } from 'react';
import {
  TableCell,
  TableHead,
  Table
} from '@material-ui/core';
import { FieldArray } from 'redux-form';
import EmployeeArray from 'containers/groups/Tables/Employee/EmployeeArray';
import I18n from 'assets/I18n';
import ownStyles from './Employee.module.scss';

class EmployeeTable extends PureComponent {
  render() {
    return (
      <div className={ownStyles.tableContainer}>
        <Table>
          <TableHead>
            <TableCell key="SocietyName">{I18n.t('physicalPersonCreation.society_form.nameHeader')}</TableCell>
            <TableCell key="FunctionLabel">{I18n.t('physicalPersonCreation.society_form.functionHeader')}</TableCell>
            <TableCell key="SignatoryFunction">{I18n.t('physicalPersonCreation.society_form.signatoryHeader')}</TableCell>
            <TableCell key="StartDate">{I18n.t('physicalPersonCreation.society_form.startDateHeader')}</TableCell>
            <TableCell key="EndDate">{I18n.t('physicalPersonCreation.society_form.endDateHeader')}</TableCell>
          </TableHead>
          <FieldArray
            name="coord"
            component={EmployeeArray}
          />
        </Table>
      </div>
    );
  }
}

export default EmployeeTable;
