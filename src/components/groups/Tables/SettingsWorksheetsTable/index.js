import React, { Fragment, useEffect } from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import styles from './settingsWorksheetsTable.module.scss';


const SettingsWorksheetsTable = ({
  param, setTableData, isLoading, ...otherProps
}) => {
  const docTable = React.createRef();
  useEffect(() => {
    setTableData(param);
  }, [isLoading]);
  return (
    <Fragment>
      <div className={styles.tableWrap} ref={docTable}>
        <div className={styles.table}>
          <TableFactory
            param={param}
            {...otherProps}
          />
        </div>
      </div>
    </Fragment>
  );
};

SettingsWorksheetsTable.propTypes = {
  param: PropTypes.shape({}).isRequired,
  setTableData: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default SettingsWorksheetsTable;
