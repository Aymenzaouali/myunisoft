import React, { useEffect } from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';

const Connector = ({
  param, setTableData, isLoading, ...otherProps
}) => {
  useEffect(() => {
    setTableData(param);
  }, [isLoading]);
  return (
    <TableFactory
      param={param}
      {...otherProps}
    />
  );
};

Connector.propTypes = {
  param: PropTypes.shape({}).isRequired,
  setTableData: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default Connector;
