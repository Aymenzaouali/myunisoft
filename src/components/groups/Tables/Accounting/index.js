import React, {
  Fragment, useEffect, useState, useRef
} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import I18n from 'assets/I18n';

import { usePrevious, useDebouncedEffect, useWindowState } from 'helpers/hooks';

import { splitShape } from 'context/SplitContext';

import Window from 'components/basics/Window';
import { ConfirmationDialog, InformationDialog } from 'components/groups/Dialogs';
import TableFactory from 'components/groups/Tables/Factory';

import { AccountingCommentBox } from 'containers/groups/Comments';
import WorksheetPopover from 'containers/groups/Popover/Worksheet';

import styles from './accountingTable.module.scss';

// Component
const AccountingTable = (props) => {
  // Props
  const {
    param,
    split,
    getEntries,
    openCommentsDialog,
    getEntryComments,
    isLoadingFilter,
    confirmation,
    currentEntryId,
    duplicate,
    isRedirected,
    shouldReload,
    isError,
    isLoading,
    setTableData,
    isRowLoading,
    isInformationDialogOpen,
    setInformationDialog
  } = props;

  // State
  const overview = useWindowState();
  const [commentDialog, setCommentDialog] = useState({ isOpen: false, label: null });
  const previousConsultingSplitEntryId = useRef();
  const [flagPopover, setFlagPopover] = useState(
    { isOpen: false, flags: null, anchorPosition: { top: 0, left: 0 } }
  );
  const [infoPopover, setInfoPopover] = useState(
    { isOpen: false, info: null, anchorPosition: { top: 0, left: 0 } }
  );

  // Functions
  const openComment = (entryId) => { // eslint-disable-line no-unused-vars
    openCommentsDialog({
      body: {
        Component: AccountingCommentBox,
        props: {
          entryId,
          newEntry: false,
          getComments: () => getEntryComments(entryId)
        }
      }
    });
  };

  const openFlag = (event, flags) => {
    setFlagPopover({
      flags,
      isOpen: true,
      anchorPosition: {
        top: event.clientY,
        left: event.clientX
      }
    });
  };

  const openInfo = (event, info) => {
    setInfoPopover({
      info,
      isOpen: true,
      anchorPosition: {
        top: event.clientY,
        left: event.clientX
      }
    });
  };

  const closeFlag = () => {
    setFlagPopover(prev => ({ ...prev, isOpen: false }));
  };

  const closeInfo = () => {
    setInfoPopover(prev => ({ ...prev, isOpen: false }));
  };
  // Effects

  const wasLoadingFilter = usePrevious(isLoadingFilter, false);

  useDebouncedEffect(() => {
    if (shouldReload && !isLoadingFilter) getEntries();
  }, wasLoadingFilter ? 0 : 500, [shouldReload, isLoadingFilter]);

  useEffect(() => {
    if (split.args.entry_id && split.args.entry_id !== previousConsultingSplitEntryId.current) {
      previousConsultingSplitEntryId.current = split.args.entry_id;
      getEntries();
    }
    if (isRedirected && currentEntryId) duplicate(currentEntryId, 'e', 'modify');
  }, [split.slave, split.master_id, split.args.entry_id, currentEntryId]);


  useEffect(() => {
    if (!isLoadingFilter) {
      getEntries();
    }
  }, [isLoadingFilter]);

  // Rendering
  const buttonsDialog = {
    validate: I18n.t('close'),
    cancel: null
  };

  const formattedParams = param;
  const formattedRightClickActions = formattedParams.body.rightClickActions?.map((action) => {
    if (action.label !== I18n.t('tables.accounting.rightClick.duplicate')) {
      return {
        ...action,
        disabled: _.get(formattedParams, `body.row[${0}].props.blocked`)
      };
    } return { ...action };
  });

  const rows = _.get(param, 'body.row', false);
  if (rows) {
    const selectedAccount = _.get(split, 'args.account.account_number');

    const bodyData = rows.map(row => ({
      ...row,
      value: row.value.map((cell) => {
        if (cell.isComment) {
          return {
            ...cell,
            props: {
              ...cell.props
            }
          };
        }
        if (cell.account && split.master) {
          return {
            ...cell,
            cellProp: {
              ...cell.cellProp,
              className: (cell.account === selectedAccount) ? styles.splitSelected : styles.split
            }
          };
        }
        if (cell.isFlag) {
          return {
            ...cell,
            props: {
              ...cell.props,
              onClick: async (event) => {
                const { flagInfo } = cell;
                const flags = flagInfo && flagInfo.map(e => ({ name: e.worksheet_type_code }));
                openFlag(event, flags);
              }
            }
          };
        }
        if (cell.isInfo) {
          return {
            ...cell,
            props: {
              ...cell.props,
              onClick: async (event) => {
                const { info } = cell;
                const infos = info;
                openInfo(event, infos);
              }
            }
          };
        }
        if (cell._type === 'status') {
          return {
            ...cell,
            props: {
              ...cell.props,
              data: _.get(cell, 'props.data', []).map(pjBadge => ({
                ...pjBadge,
                onClick: () => overview.open({ documents: pjBadge.pjs }, true)
              }))
            }
          };
        }
        return cell;
      })
    }));

    formattedParams.body = {
      ...formattedParams.body,
      row: bodyData,
      rightClickActions: formattedRightClickActions
    };
  }
  useEffect(() => {
    setTableData(param);
  }, [isLoading, shouldReload, isRowLoading]);

  return (
    <Fragment>
      <TableFactory
        confirmation={confirmation}
        param={formattedParams}
        isError={isError}
        isLoading={isLoading}
        isRowLoading={isRowLoading}
        cellHeight={45}
        maxVisibleItems={10}
      />
      <ConfirmationDialog
        isOpen={commentDialog.isOpen}
        onValidate={() => setCommentDialog(prev => ({ ...prev, isOpen: false }))}
        message={commentDialog.label}
        title={I18n.t('comment')}
        buttonsLabel={buttonsDialog}
      />
      <WorksheetPopover
        isOpen={flagPopover.isOpen}
        flags={flagPopover.flags}
        anchorReference="anchorPosition"
        anchorPosition={flagPopover.anchorPosition}
        disableOnClick
        onClose={closeFlag}
      />
      <WorksheetPopover
        isOpen={infoPopover.isOpen}
        infos={infoPopover.infos}
        anchorReference="anchorPosition"
        anchorPosition={infoPopover.anchorPosition}
        disableOnClick
        onClose={closeInfo}
      />
      <Window
        path="/overview"
        window_id="overview"
        features={`width=${window.innerWidth / 2},left=${window.screenX + (window.innerWidth / 2)}`}

        {...overview.props}
      />
      <InformationDialog
        isOpen={isInformationDialogOpen}
        message={I18n.t('accounting.dialog.information_message')}
        onClose={() => setInformationDialog(false)}
      />
    </Fragment>
  );
};

// Props
AccountingTable.propTypes = {
  param: PropTypes.object.isRequired,
  split: splitShape.isRequired,
  confirmation: PropTypes.object.isRequired,
  isLoadingFilter: PropTypes.bool.isRequired,
  isError: PropTypes.bool,
  isLoading: PropTypes.bool,
  isRowLoading: PropTypes.bool,
  currentEntryId: PropTypes.number.isRequired,
  duplicate: PropTypes.func.isRequired,
  isRedirected: PropTypes.func.isRequired,
  setTableData: PropTypes.func.isRequired,
  getEntries: PropTypes.func.isRequired,
  openCommentsDialog: PropTypes.func.isRequired,
  getEntryComments: PropTypes.func.isRequired,
  shouldReload: PropTypes.bool.isRequired,
  isInformationDialogOpen: PropTypes.bool.isRequired,
  setInformationDialog: PropTypes.func.isRequired
};

AccountingTable.defaultProps = {
  isError: false,
  isLoading: false,
  isRowLoading: false
};

export default AccountingTable;
