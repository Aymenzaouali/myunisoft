import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import FactoryTable from 'components/groups/Tables/Factory';

const LegalPerson = ({
  param, setTableData, associate_list, ...otherProps
}) => {
  useEffect(() => {
    setTableData();
  }, [associate_list]);
  return (
    <FactoryTable
      param={param}
      {...otherProps}
    />
  );
};

LegalPerson.propTypes = {
  param: PropTypes.shape({}).isRequired,
  setTableData: PropTypes.func.isRequired,
  associate_list: PropTypes.array.isRequired
};

export default LegalPerson;
