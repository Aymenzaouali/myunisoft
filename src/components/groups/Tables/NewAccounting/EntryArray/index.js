import React, { Fragment } from 'react';
import _ from 'lodash';
import {
  TableCell,
  TableRow,
  Typography,
  IconButton,
  withStyles
} from '@material-ui/core';
import I18n from 'assets/I18n';
import classnames from 'classnames';
import { Shortcuts } from 'react-shortcuts';
import { Field, Fields } from 'redux-form';
import { splitShape } from 'context/SplitContext';
import {
  defaultAccountType,
  total,
  totalWithoutSelectedLine
} from 'helpers/entry';
import { getSelectedMonth, getSelectedYear } from 'helpers/date';
import PropTypes from 'prop-types';
import {
  roundNumber,
  formatNumber
} from 'helpers/number';
import { diaryAndAccountCompatibility, checkDateAndDiary } from 'helpers/accounting';
import {
  buyingDiaryAccounts,
  sellingDiaryAccounts,
  odDiaryAccounts
} from 'assets/constants/data';
import { openConsultingPopup } from 'helpers/consulting';
import { ReduxDayPickerCell, ReduxInputCell } from 'components/reduxForm/Inputs';
import {
  AccountAutoCompleteCell,
  PaymentTypeAutoCompleteCell
} from 'containers/reduxForm/Inputs';
import { getFilteredOptions } from 'containers/reduxForm/Inputs/AccountAutoCompleteCell';
import CheckBox from 'components/basics/CheckBox';
import Popover from 'components/basics/Popovers/Buttons';
import moment from 'moment';
import {
  CalculatorDialog,
  ConfirmationDialog,
  InformationDialog
} from 'components/groups/Dialogs';
import {
  NewAccountDialog,
  CounterPartDialog,
  PjDialog,
  BISettingsDialog,
  AccountParameters,
  DiaryDialog,
  FlagDialog
} from 'containers/groups/Dialogs';

import FlagPopoverContainer from 'containers/groups/Popover/Flag';

import { FontIcon } from 'components/basics/Icon';
import './styles.scss';
import stylesEntry from './EntryArray.module.scss';


class EntryArray extends React.Component {
  /**
   * TODO: State has to be removed in order
   * to improve performance
   *
   * Dialogs created with openDialog()
   * Popovers created outside of the component
   */
  constructor(props) {
    super(props);

    this.state = {
      activeIndexRow: 0,
      previousCell: null,
      nextCell: null,
      isFlagModalOpen: false,
      anchorPosition: null,
      immoDialog: false,
      dialogs: {
        immo: {
          isOpen: false
        },
        account: {
          isOpen: false,
          name: ''
        },
        pj: {
          isOpen: false,
          name: ''
        },
        information: {
          isOpen: false,
          title: '',
          message: ''
        },
        biSettings: {
          isOpen: false,
          creationMode: true,
          currentAccount: {}
        },
        accountParameters: {
          isOpen: false,
          currentAccount: {}
        }
      },
      popover: {
        isOpen: false,
        top: 0,
        left: 0,
        currentIndex: 0
      },
      confirmationIsOpen: false,
      permissionIsOpen: false,
      diaryDialogOpened: false,
      modifyDiaryDialogIsOpen: false,
      isPostingData: false // TODO REMOVE THIS ON NEXT REFACTORING
    };

    this.shortcutList = {
      SOLDE_ENTRY: 'SOLDE_ENTRY',
      COPY_ENTRY: 'COPY_ENTRY',
      DELETE_ENTRY: 'DELETE_ENTRY',
      PREVIOUS_ENTRY: 'PREVIOUS_ENTRY',
      NEXT_ENTRY: 'NEXT_ENTRY',
      NEW_ACCOUNTING: 'NEW_ACCOUNTING',
      AUTOFILL_OTHER_LINES: 'AUTOFILL_OTHER_LINES',
      SHOW_ACCOUNT: 'SHOW_ACCOUNT',
      FOCUS_PREVIOUS_CELL: 'FOCUS_PREVIOUS_CELL',
      FOCUS_NEXT_CELL: 'FOCUS_NEXT_CELL'
    };

    this.hiddenElementRef = React.createRef();

    this.selectedRowIndex = 0;

    this.onRightClick = this.onRightClick.bind(this);
    this.labelShortCut = this.labelShortCut.bind(this);
  }

  componentDidMount() {
    const { societyId } = this.props;
    this[`${societyId}${0}date`].focus();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!_.isEqual(nextState, this.state)) {
      return true;
    }

    // eslint-disable-next-line react/destructuring-assignment
    if (!_.isEqual(nextProps.openConfirmFromHeader, this.props.openConfirmFromHeader)) {
      return true;
    }

    // eslint-disable-next-line react/destructuring-assignment
    if (!_.isEqual(nextProps.openPermissionFromHeader, this.props.openPermissionFromHeader)) {
      return true;
    }

    // eslint-disable-next-line react/destructuring-assignment
    if (!_.isEqual(nextProps.formVal, this.props.formVal)) {
      return true;
    }

    // eslint-disable-next-line react/destructuring-assignment
    if (!_.isEqual(nextProps.comment, this.props.comment)) {
      return true;
    }

    // split state change
    const { split } = this.props;
    const { split: nsplit } = nextProps;

    if (split.active !== nsplit.active) {
      return true;
    }

    if (split.master !== nsplit.master || split.slave !== nsplit.slave) {
      return true;
    }

    if (split.slave && !_.isEqual(split.args, nsplit.args)) {
      return true;
    }

    // eslint-disable-next-line react/destructuring-assignment
    const currentFields = this.props.fields.getAll(); //eslint-disable-line
    const nextFields = nextProps.fields.getAll(); //eslint-disable-line

    const areSameSize = Object.keys(currentFields).length === Object.keys(nextFields).length;

    if (!areSameSize) {
      return true;
    }

    let shouldRender = false;
    // eslint-disable-next-line no-restricted-syntax
    for (const i in currentFields) {
      // eslint-disable-next-line no-prototype-builtins
      if (currentFields.hasOwnProperty(i)) {
        if (currentFields[i].line_entry_id !== nextFields[i].line_entry_id) {
          shouldRender = true;
          break;
        }
      }
    }

    return shouldRender;
  }

  componentDidUpdate(prevProps) {
    const { formVal: { entry_id: prevEntryId, entry_list: prevEntryList } } = prevProps;
    const {
      split, societyId, formVal: {
        entry_id, entry_list,
        diary: {
          diary_type_code
        } = {}
      },
      entriesType
    } = this.props;
    const prevCounterPartAccountId = prevEntryList[0]?.account?.counterpart_account?.account_id;
    const counterPartAccountId = entry_list[0]?.account?.counterpart_account?.account_id;

    if (((entry_id !== prevEntryId) || (_.get(entry_list, 'length') !== _.get(prevEntryList, 'length'))) && this[`${societyId}${0}date`]) this[`${societyId}${0}date`].focus();

    if (
      entry_id === prevEntryId
      && entry_id
      && counterPartAccountId
          && entry_list.length > 1
          && counterPartAccountId !== prevCounterPartAccountId
          && (diary_type_code === 'ACH' || diary_type_code === 'VTE')
          && (entriesType === 'E' || entriesType === 'O' || entriesType === 'M')
    ) {
      this.removeLinesAndApplyCounterPart();
    }

    // if became master
    if (split.master && !prevProps.split.master) {
      const currentRow = this.getCurrentRow(this.selectedRowIndex);

      split.updateArgsSplit({
        account: currentRow.account
      });
    }
  }

  onChangeValues(type, name, val) {
    const { changeFormValue } = this.props;
    const otherType = type === 'debit' ? 'credit' : 'debit';
    const otherTypeName = name.replace(new RegExp(type, 'gi'), otherType);
    const value = roundNumber(val.replace(/[^\d,.]/g, ''));
    if (value !== 0 && value !== '') {
      changeFormValue(otherTypeName, value > 0 ? '' : formatNumber(-value));
    }
    changeFormValue(name, value > 0 ? formatNumber(value) : '');
  }

  onRightClick(e, index) {
    e.preventDefault();
    this.setState({
      popover: {
        isOpen: true,
        left: e.clientX,
        top: e.clientY,
        currentIndex: index
      }
    });
  }

  onDayBlur(val, i) {
    const { changeFormValue, societyId, formVal } = this.props;
    const currentDate = moment();
    const inputDateString = this.entryDate;
    const inputDateWithoutDay = moment(`${inputDateString.substring(0, 6)}01`).format('YYYY-MM'); // Keep only month & year with 01 to get valid date in case of day isn't set
    const maxDayOnMonth = moment(inputDateWithoutDay).isValid() ? moment(inputDateWithoutDay).endOf('month').format('DD') : moment().endOf('month').format('DD');
    const { target } = val;
    const { value: day } = target;

    const toNumber = number => parseInt(number, 10);
    const formattedDay = day.length === 1 ? day.padStart(2, '0') : day;
    if (day) {
      if (toNumber(formattedDay) > toNumber(maxDayOnMonth)) {
        changeFormValue('day', maxDayOnMonth);
        this[`${societyId}${i}account`].focus();
      } else if ((toNumber(formattedDay) > toNumber(currentDate.format('DD'))) && (
        checkDateAndDiary(_.get(formVal, 'diary.diary_type_code'))
      ) && ((toNumber(_.get(formVal, 'year.label')) >= toNumber(currentDate.format('YYYY'))))) {
        changeFormValue('month', getSelectedMonth(`${currentDate.subtract(1, 'M')}`));
        if (currentDate.format('MM') === '12') changeFormValue('year', getSelectedYear(currentDate));
        changeFormValue('day', formattedDay);
      } else {
        changeFormValue('day', formattedDay);
      }
    } else {
      changeFormValue('day', currentDate.format('DD'));
    }
  }

  async onAccountChangeValue(i, account = {}, formVal) {
    const {
      changeFormValue,
      selectAccount,
      getAccountDetail
    } = this.props;

    const isIb = formVal.type === 'ib';
    const counter_part = _.get(account, 'counterpart_account', undefined);
    const label = _.get(account, 'label', '');
    selectAccount(_.get(account, 'account_id', undefined));
    if (i > 0 && !isIb) changeFormValue(`entry_list[${i}].label`, formVal.entry_list[0].label);
    else if (label && !isIb) changeFormValue(`entry_list[${i}].label`, label);
    else changeFormValue(`entry_list[${i}].label`, formVal.entry_list[0].label);
    if (counter_part) {
      changeFormValue(`entry_list[${i}].counterpart_account`, { ...counter_part, value: `${counter_part.account_id}` });
    } else {
      changeFormValue(`entry_list[${i}].counterpart_account`, '');
    }

    await getAccountDetail();
  }

  onCreateAccountHandler(selectedDiary, i, opts, value) {
    const diariesToControl = ['ACH', 'VTE', 'OD'];
    if (diariesToControl.some(e => e === selectedDiary)) {
      if (diaryAndAccountCompatibility(selectedDiary, value)) {
        this.handleDialog('account', this.generateAccountDialog({ account_number: value }, i, opts));
      } else {
        this.handleInternalError(I18n.t('newAccounting.snackBarWarning.accountIncompatibility'));
      }
    } else {
      this.handleDialog('account', this.generateAccountDialog({ account_number: value }, i, opts));
    }
  }

  getCurrentRow(i) {
    const { formVal } = this.props;
    const currentRow = formVal.entry_list[i];
    return currentRow;
  }

  get isConfirmationDialogOpen() {
    const { openConfirmFromHeader } = this.props;
    const { confirmationIsOpen } = this.state;

    return confirmationIsOpen || openConfirmFromHeader;
  }

  get isPermissionDialogOpen() {
    const { openPermissionFromHeader } = this.props;
    const { permissionIsOpen } = this.state;

    return permissionIsOpen || openPermissionFromHeader;
  }

  get isInformationDialogOpen() {
    const { openInformationFromHeader } = this.props;
    const { informationIsOpen } = this.state;

    return informationIsOpen || openInformationFromHeader;
  }


  get entryDate() {
    const { formVal } = this.props;
    const {
      day,
      month,
      year
    } = formVal;

    return `${_.get(year, 'value')}${_.get(month, 'value')}${day}`;
  }


  handleClose = () => {
    this.setState({
      isFlagModalOpen: false,
      anchorPosition: null
    });
  };

  blurAllFields = () => {
    if (this.hiddenElementRef.current) {
      this.hiddenElementRef.current.focus();
    }
  };

  handleInternalError = (errorMessage) => {
    const { enqueueSnackbarWarning } = this.props;
    return (
      enqueueSnackbarWarning(errorMessage)
    );
  };

  _handleShortcuts = async (action, e, fields) => {
    const {
      formVal,
      changeFormValue,
      pushEntry,
      societyId,
      handleCounterPart
    } = this.props;
    const { previousCell, nextCell, activeIndexRow } = this.state;
    const selected = this.selectedRowIndex;

    if (this.shortcutList[action]) {
      e.preventDefault();
      e.stopPropagation();

      this.blurAllFields();
    }

    switch (action) {
    case this.shortcutList.SOLDE_ENTRY:
      const initialEntry = _.get(formVal.entry_list, `${selected}.credit`)
        ? { initialColumn: 'credit', initialValue: _.get(formVal.entry_list, `${selected}.credit`) }
        : { initialColumn: 'debit', initialValue: _.get(formVal.entry_list, `${selected}.debit`) };

      if (_.get(formVal.entry_list, `${selected}.credit`) !== undefined || _.get(formVal.entry_list, `${selected}.debit`) !== undefined) {
        changeFormValue(`entry_list[${selected}][credit]`, '');
        changeFormValue(`entry_list[${selected}][debit]`, '');
      }

      const total_credit = totalWithoutSelectedLine(formVal.entry_list, selected, 'credit', false);
      const total_debit = totalWithoutSelectedLine(formVal.entry_list, selected, 'debit', false);
      const diff = roundNumber(total_debit - total_credit);

      let column;
      let value;

      if (diff > 0) {
        column = 'credit';
        value = formatNumber(parseFloat(Math.abs(diff), 10));
      } else if (diff < 0) {
        column = 'debit';
        value = formatNumber(parseFloat(Math.abs(diff), 10));
      } else {
        const {
          initialColumn,
          initialValue
        } = initialEntry;

        column = initialColumn;
        value = formatNumber(parseFloat(initialValue, 10));
      }

      changeFormValue(`entry_list[${selected}][${column}]`, value);
      this[`${societyId}${this.selectedRowIndex}${column}`].focus();
      break;
    case this.shortcutList.COPY_ENTRY:
      const currentEntry = _.get(formVal, `entry_list[${selected}]`, false);
      if (currentEntry) {
        pushEntry('entry_list', currentEntry);
      }
      break;
    case this.shortcutList.DELETE_ENTRY:
      this.deleteRow(fields, selected);
      break;
    case this.shortcutList.PREVIOUS_ENTRY:
      if (selected > 0) {
        this.selectedRowIndex -= 1;
        this[`${societyId}${this.selectedRowIndex}account`].focus();
      }
      break;
    case this.shortcutList.NEXT_ENTRY:
      if (selected === fields.length - 1) {
        pushEntry('entry_list', {
          date_entry: moment().format('YYYY-MM-DD'),
          account: ''
        });
      }

      this.selectedRowIndex += 1;
      this[`${societyId}${this.selectedRowIndex}account`].focus();
      break;
    case this.shortcutList.NEW_ACCOUNTING:
      const newEntry = formVal.entry_list;
      const lastRow = newEntry[newEntry.length - 1];
      pushEntry('entry_list', {
        date_entry: moment().format('YYYY-MM-DD'),
        piece: lastRow.piece,
        piece2: lastRow.piece2,
        deadline: lastRow.deadline
      });
      this[`${societyId}${newEntry.length}account`].focus();
      break;
    case this.shortcutList.AUTOFILL_OTHER_LINES:
      const { isPostingData } = this.state;

      if (isPostingData) {
        return;
      }

      this.setState({
        isPostingData: true
      });

      const validateButton = document.getElementById('newEntryValidateButton'); // TODO : use createref instead
      const counterPartApplied = await handleCounterPart(true);
      if (counterPartApplied) {
        validateButton.focus();
      }

      this.setState({
        isPostingData: false
      });

      break;
    case 'SHOW_ACCOUNT':
      const { accountData } = this.props;
      if (accountData[0]) {
        const {
          account_number,
          counterpart_account,
          intraco,
          label,
          presta,
          account_id,
          vat_param
        } = accountData[0];

        const infoG = {
          account_id,
          account_number,
          counterpart_account,
          intraco_account: intraco,
          label,
          provider: presta,
          tva: vat_param
        };

        this.handleDialog('account', this.generateAccountDialog({ infoG }, this.selectedRowIndex));
      } else {
        this.handleInternalError(I18n.t('newAccounting.snackBarWarning.popupAccount'));
      }
      break;
    case 'FOCUS_PREVIOUS_CELL':
      const { yearRef } = this.props;
      if (previousCell === 'year') {
        yearRef.focus();
      } else {
        this[`${societyId}${activeIndexRow}${previousCell}`].focus();
      }
      break;
    case 'FOCUS_NEXT_CELL':
      this[`${societyId}${activeIndexRow}${nextCell}`].focus();
      break;

    default:
      break;
    }
  };

  deleteRow = (fields, i) => {
    const { pushEntry } = this.props;
    const line = fields.get(i) || {};
    if (line.line_entry_id) {
      pushEntry('delete_line_entry', line.line_entry_id);
    }
    fields.remove(i);
    if (fields.getAll().length === 1) {
      this.addEntry();
    }
  };

  deleteRows = () => {
    const { fields } = this.props;
    fields.forEach((field, fieldIndex) => fieldIndex !== 0 && this.deleteRow(fields, fieldIndex));
  };

  onRowFocus = (i) => {
    if (this.selectedRowIndex !== i) {
      this.selectedRowIndex = i;

      // show current account
      const { split } = this.props;

      if (split.master) {
        const currentRow = this.getCurrentRow(i);

        split.updateArgsSplit({
          account: currentRow.account
        });
      }
    }
  };

  // On keydown for last array cell
  lastCellKeyDown = (e, i) => { //eslint-disable-line
    const {
      pushEntry,
      formVal,
      societyId,
      handleCounterPart,
      initializeImmoForm
    } = this.props;
    const { dialogs } = this.state;
    const { entry_list = [] } = formVal;
    const validateButton = document.getElementById('newEntryValidateButton'); // TODO : use createref instead

    if (e.keyCode === 9 || e.keyCode === 13) {
      const { isPostingData } = this.state;
      const { account: { account_number }, flags } = entry_list[i];

      const createLines = async () => {
        if (isPostingData) {
          return;
        }

        this.setState({
          isPostingData: true
        });

        // Try to apply counterPart
        const counterPartApplied = await handleCounterPart();

        if (counterPartApplied) {
          this.setState({
            isPostingData: false
          });
          validateButton.focus();
          return;
        }

        this.setState({
          isPostingData: false
        });

        // Calculate sum for debit and credit total
        const debitArray = entry_list.map(e => (e.debit && parseFloat(e.debit.replace(/,/g, '.').replace(/\s/g, ''))) || 0);
        const creditArray = entry_list.map(e => (e.credit && parseFloat(e.credit.replace(/,/g, '.').replace(/\s/g, ''))) || 0);
        const debitSum = debitArray.reduce((a, b) => a + b, 0);
        const creditSum = creditArray.reduce((a, b) => a + b, 0);

        if (debitSum === creditSum) {
          validateButton.focus();
        } else {
          // Get the current line entry
          const currentRow = formVal.entry_list[i];

          // if the current line is the last line entry
          if (i === formVal.entry_list.length - 1) {
            // Add a new entry with :
            pushEntry('entry_list', {
              date_entry: moment().format('YYYY-MM-DD'), // current date
              piece: currentRow.piece, // Same line piece 1
              piece2: currentRow.piece2, // Same line piece 2
              deadline: currentRow.deadline // Same line deadline
            });

            // Add focus to the NEW line entry
            setTimeout(() => {
              this[`${societyId}${i + 1}account`].focus();
            }, 0);
          } else {
            // Add focus to the NEXT line entry
            setTimeout(() => {
              this[`${societyId}${i + 1}account`].focus();
            }, 0);
          }
        }
      };

      // Evolution: When, at the end of a line,
      // the selected account starts with 2 but is neither 28xxx or 29xxx,
      // and there's no immo flag yet; then we open the flag dialog 
      if (!_.get(flags, 'immo') && account_number.startsWith('2') && !account_number.startsWith('28') && !account_number.startsWith('29')) {
        initializeImmoForm({ amortissement: 1, date: moment() });
        return this.setState({
          dialogs: {
            ...dialogs,
            immo: {
              isOpen: true,
              onClose: createLines
            }
          }
        });
      }

      createLines();
    }
  };

  handleValidatePermission = () => {
    const { enableLinkDiaryAccountPermission } = this.props;
    this.setState({
      permissionIsOpen: false,
      modifyDiaryDialogIsOpen: true
    });
    enableLinkDiaryAccountPermission(false);
  }

  async removeLinesAndApplyCounterPart() {
    const { handleCounterPart } = this.props;

    await this.deleteRows();
    const validateButton = document.getElementById('newEntryValidateButton'); // TODO : use createref instead
    const counterPartApplied = handleCounterPart();
    if (counterPartApplied) {
      validateButton.focus();
    }

    this.setState({
      isPostingData: false
    });
  }

  addEntry() {
    const { pushEntry } = this.props;
    setTimeout(() => {
      pushEntry('entry_list', {
        date_entry: moment().format('YYYY-MM-DD'),
        account: ''
      });
    }, 0);
  }

  generateAccountDialog(infoG, i, {
    accountNumberDisabled = false,
    onValidateCallback,
    isContrepartie = false
  } = {}) {
    const {
      changeFormValue,
      societyId,
      split,
      formVal
    } = this.props;

    const currentAccount = this.getCurrentRow(i).account;
    const onValidate = (data) => {
      const formattedData = {
        ...currentAccount,
        ...data
      };

      changeFormValue(`entry_list[${i}].account`, formattedData);

      this.onAccountChangeValue(i, formattedData, formVal);

      if (split.master) {
        split.updateArgsSplit({ account: formattedData });
      }

      if (onValidateCallback) onValidateCallback();
    };
    const initialValues = { infoG };

    const onClose = () => {
      this[`${societyId}${i}label`].focus();
      this.handleDialog('account', { isOpen: false });
    };

    return {
      isOpen: true,
      onValidate,
      initialValues,
      onClose,
      isContrepartie,
      currentAccount,
      accountNumberDisabled
    };
  }

  debitCreditShortcuts(action, event, i, type) {
    const { changeFormValue, societyId, formVal } = this.props;
    const selectedDiary = _.get(formVal, 'diary.diary_type_code');
    const calculatorDialog = {
      isOpen: true,
      onClose: () => this.handleDialog('calculator', { isOpen: false }),
      onValidate: (value) => {
        changeFormValue(`entry_list[${i}][${type}]`, value);
        this.handleDialog('calculator', { isOpen: false });
      },
      value: event.target.value
    };
    switch (action) {
    case 'EQUAL':
      this.handleDialog('calculator', { ...calculatorDialog });
      break;
    case 'PLUS':
      this.handleDialog('calculator', { ...calculatorDialog, operator: '+' });
      break;
    case 'MINUS':
      this.handleDialog('calculator', { ...calculatorDialog, operator: '-' });
      break;
    case 'MULTIPLE':
      this.handleDialog('calculator', { ...calculatorDialog, operator: '*' });
      break;
    case 'DIVIDE':
      this.handleDialog('calculator', { ...calculatorDialog, operator: '/' });
      break;
    case 'ENTER':
      if (type === 'debit') {
        this[`${societyId}${i}credit`].focus();
      } else {
        event.stopPropagation();
        event.preventDefault();
        if (selectedDiary === 'BQ' || selectedDiary === 'CAISSE') {
          if (formVal.entry_list[i].counterpart_account) {
            this[`${societyId}${i}paymentType`].focus();
          } else {
            this[`${societyId}${i}deadline`].focus();
          }
        } else if (formVal.entry_list[i].counterpart_account) {
          this[`${societyId}${i}deadline`].focus();
        } else {
          this[`${societyId}${i}counterPart`].focus();
        }
      }
      break;
    default:
      break;
    }
  }

  labelShortCut(action, event, i) {
    const { formVal, changeFormValue, societyId } = this.props;
    const currentRow = formVal.entry_list[i];
    const currentLabelValue = _.get(this[`${societyId}${i}label`], 'value');

    switch (action) {
    case 'PLUS':
      event.preventDefault();
      if (currentLabelValue !== _.get(currentRow, 'account.label', '')) {
        changeFormValue(`entry_list[${i}].label`, currentLabelValue ? `${currentLabelValue} ${_.get(currentRow, 'account.label', '')}` : _.get(currentRow, 'account.label', ''));
      }
      break;
    case 'ENTER':
      this[`${societyId}${i}piece`].focus();
      break;
    default:
      break;
    }
  }

  pieceShortCut(action, event, i) {
    const { formVal, societyId } = this.props;
    const currentRow = formVal.entry_list[i];
    const diary_id = _.get(formVal, 'diary.diary_type_id', 0);

    if (action === 'TAB' || action === 'ENTER') {
      event.preventDefault();
      event.stopPropagation();
      this[`${societyId}${i}${defaultAccountType(diary_id, currentRow, formVal.entry_list)}`].focus();
    }
  }

  handleCloseConfirm() {
    const {
      enableCreateCounterPartConfirmation
    } = this.props;

    enableCreateCounterPartConfirmation(false);
    this.setState({ confirmationIsOpen: false });
  }

  handleClosePermission() {
    const {
      enableLinkDiaryAccountPermission
    } = this.props;

    enableLinkDiaryAccountPermission(false);
    this.setState({ permissionIsOpen: false });
  }

  handleCloseInformation() {
    const {
      enableCounterPartInformation
    } = this.props;

    enableCounterPartInformation(false);
    this.setState({ informationIsOpen: false });
  }

  async handleRightClickToModifyAccount(index, opts = {
    accountNumberDisabled: true
  }, newData = {}) {
    const {
      enableCreateCounterPartConfirmation,
      selectAccount,
      getAccountDetail,
      resetAccountSelected
    } = this.props;

    const currentAccount = this.getCurrentRow(index).account;
    if (!currentAccount) return;
    selectAccount(currentAccount.account_id);
    await getAccountDetail();

    const {
      accountData
    } = this.props;

    const {
      account_id,
      account_number,
      counterpart_account,
      intraco,
      label,
      presta,
      vat_param
    } = {
      ...accountData[0],
      ...newData
    };

    const infoG = {
      account_id,
      account_number,
      counterpart_account,
      intraco_account: intraco,
      label,
      provider: presta,
      tva: vat_param
    };

    this.handleDialog('account', this.generateAccountDialog(infoG, index, opts));
    resetAccountSelected();
    enableCreateCounterPartConfirmation(false);
  }

  async handleValidateConfirm() {
    const {
      enableCreateCounterPartConfirmation,
      accountData,
      counterPartLines
    } = this.props;

    const {
      account_id,
      account_number,
      counterpart_account,
      intraco,
      label,
      presta,
      vat_param
    } = accountData[0];

    const infoG = {
      account_id,
      account_number,
      counterpart_account,
      intraco_account: intraco,
      label,
      provider: presta,
      tva: vat_param
    };

    this.handleDialog('account', this.generateAccountDialog(infoG, 0, {
      onValidateCallback: counterPartLines
    }));
    enableCreateCounterPartConfirmation(false);
  }

  handleDialog(type, value) {
    const { dialogs } = this.state;

    this.setState({
      dialogs: { ...dialogs, [type]: value },
      confirmationIsOpen: false
    });
  }

  handleAccountModification(entriesType, currentAccount, previousAccount, keyLabel = undefined) {
    const {
      dialogs
    } = this.state;

    if (entriesType === 'O' || entriesType === 'P') {
      this.setState({
        dialogs: {
          ...dialogs,
          accountParameters: {
            ...dialogs.accountParameters,
            isOpen: true,
            smartCreationMode: true,
            currentAccount,
            previousAccount
          }
        }
      });
    } else {
      this.setState({
        dialogs: {
          ...dialogs,
          biSettings: {
            ...dialogs.biSettings,
            isOpen: true,
            creationMode: true,
            currentAccount,
            previousAccount,
            keyLabel
          }
        }
      });
    }
  }

  handleAcccountBlur(i) {
    const {
      entriesType,
      currentEntry,
      formVal,
      changeFormValue
    } = this.props;

    const row = formVal.entry_list[i];
    const newAccount = row.account;

    const entryList = _.get(currentEntry, 'entry_list', []);
    const previousEntry = entryList[i];
    const previousEntryLabel = _.get(previousEntry, 'label');
    const previousAccount = _.get(previousEntry, 'account');
    const previousAccountNumber = _.get(previousAccount, 'account_number', '');

    if (newAccount === previousAccount) {
      return;
    }

    if (previousAccountNumber === '401' || previousAccountNumber === '411') {
      return;
    }

    switch (entriesType) {
    case 'O':
      if (previousAccountNumber.substring(0, 2) === '40' || previousAccountNumber.substring(0, 2) === '41') {
        this.handleAccountModification(entriesType, newAccount, previousAccountNumber);
      } else if (previousAccountNumber.substring(0, 1) === '7' || previousAccountNumber.substring(0, 1) === '6') {
        const entry = entryList.find((entry) => {
          if (entry.account.account_number.substring(0, 2) === '40' || entry.account.account_number.substring(0, 2) === '41') {
            return entry;
          }
          return {};
        });

        const index = entryList.findIndex(entry => (entry.account.account_number.substring(0, 2) === '40' || entry.account.account_number.substring(0, 2) === '41'));

        const {
          account
        } = entry;

        this.handleDialog('counterPart', {
          isOpen: true,
          fromParamAuto: true,
          onClose: () => this.handleDialog('counterPart', { isOpen: false }),
          onCloseValidate: () => {
            this.handleDialog('counterPart', { isOpen: false });
            changeFormValue(`entry_list[${index}].counterpart_account`, newAccount);
          },
          account,
          counterpart_account: newAccount,
          newAccount: {
            account_id: account.account_id,
            counterpart_account_id: _.get(newAccount, 'account_id', null)
          }
        });
      }
      break;
    case 'IB':
      if (previousAccountNumber.substring(0, 3) !== '512') {
        this.handleAccountModification(entriesType, newAccount, _.get(previousEntry, 'account'), previousEntryLabel);
      }
      break;
    case 'P':
      this.handleAccountModification(entriesType, newAccount, previousAccountNumber);
      break;
    default:
      break;
    }
  }

  renderFields = () => {
    const {
      fields,
      societyId,
      formVal, comment,
      classes,
      getAndOpenComments,
      split,
      onLabelBlur,
      setLinePosition,
      diaryValue,
      entryValue,
      dayRef,
      splitArgs,
      master_id
    } = this.props;

    const selectedDiary = _.get(formVal, 'diary.diary_type_code');

    const checkFlag = (index) => {
      const hasFlag = [];
      const isFlagged = !_.isEmpty(_.get(formVal, `entry_list[${index}].flags.worksheet`, []));
      const isImmo = !_.isEmpty(_.get(formVal, `entry_list[${index}].flags.immo`, {}));
      hasFlag.push(isFlagged, isImmo);
      return hasFlag;
    };

    const entryHasFlag = index => !checkFlag(index).includes(true);

    const { dialogs } = this.state;

    return fields.map((field, i) => {
      const isLastRow = i === fields.length - 1;
      const allowEdit = _.isEmpty(diaryValue) || _.isEmpty(entryValue[i].account);
      const accountId = _.get(formVal, `${field}.account`, {}).account_id;
      const counterpart_account = _.get(formVal, `${field}.account.counterpart_account`, {}) || {};
      const disableAmount = _.get(splitArgs, 'blocked', false);
      let disableAccount = false;

      if (master_id === 'bankLink') {
        const diaryAccountId = _.get(splitArgs, 'diary.account.id');
        disableAccount = accountId === diaryAccountId;
      }

      return [
        <TableRow
          onClick={() => this.onRowFocus(i)}
          className="entryArray__row"
          key={`table_row_accounting_${field}${i}`}
          onContextMenu={e => this.onRightClick(e, i)}
          hover
        >
          {[
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'iconButton',
              name: entryHasFlag(i) ? 'icon-plus' : 'icon-Flag',
              style: entryHasFlag(i) ? {
                fontSize: '10px',
                backgroundColor: '#d8d8d8',
                borderRadius: '50%'
              } : {
                fontSize: '30px',
                color: '#50e3c2'
              },
              onClick: (event) => {
                const {
                  detail, clientX, clientY
                } = event;
                if (detail === 1) {
                  setLinePosition(i);
                  this.setState({
                    isFlagModalOpen: true,
                    anchorPosition: {
                      top: clientY,
                      left: clientX
                    }
                  });
                }
              }
            },
            // {
            //   onCustomFocus: () => { this.onRowFocus(i); },
            //   _type: i === 0 ? 'autocomplete' : 'empty',
            //   menuPosition: 'fixed',
            //   placeholder: '',
            //   onCustomBlur: val => this.onDiaryBlur(val, i),
            //   name: 'diary',
            //   textFieldProps: {
            //     className: stylesEntry.autoCompleteCell
            //   },
            //   border: true,
            //   component: DiaryAutoCompleteCell,
            //   isDisabled: new RegExp(/.*bankLink.*/).test(window.location.pathname),
            //   autoFocus: true,
            //   customRef: (inputElement) => { this[`${societyId}${i}diary`] = inputElement; },
            //   onKeyDown: (event) => {
            //     if (
            // event.keyCode === 13
            // || (event.keyCode === 9 && this[`${societyId}${i}diary`].state.menuIsOpen)
            // ) {
            //       this[`${societyId}${i}date`].focus();
            //       setTimeout(() => this[`${societyId}${i}date`].select(), 0);
            //     }
            //   },
            //   containerProps: {
            //     onDoubleClick: async () => {
            //       const { code } = diaryValue;
            //       if (code) {
            //         await getDiaryDetail(code);
            //         this.setState({ diaryDialogOpened: true });
            //       }
            //     }

            //   }
            // },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: i === 0 ? 'datepicker' : 'empty',
              onDayBlur: val => this.onDayBlur(val, i),
              disabled: allowEdit,
              names: ['day'],
              type: 'date',
              textFieldProps: {
                className: stylesEntry.autoCompleteCell
              },
              component: ReduxDayPickerCell,
              border: true,
              customRef: (inputElement) => {
                this[`${societyId}${i}date`] = inputElement;
                dayRef(inputElement);
              },
              switchToNextInput: () => this[`${societyId}${i}account`].focus(),
              onKeyDown: (event) => {
                if (event.keyCode === 13) this[`${societyId}${i}account`].focus();
              },
              handler: () => this.setState({
                activeIndexRow: i,
                previousCell: 'year',
                nextCell: 'account'
              })
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'shortcut',
              placeholder: '',
              border: true,
              name: `${field}.account`,
              textFieldProps: {
                className: stylesEntry.autoCompleteCell
              },
              isDisabled: disableAccount,
              menuPosition: 'fixed',
              isCreatable: true,
              onCreateOption: this.onCreateAccountHandler.bind(this, selectedDiary, i,
                {
                  accountNumberDisabled: true
                }),
              component: AccountAutoCompleteCell,
              getOptions: (accounts, filter) => {
                let diaryFiltered = [];
                const idAccount = e => diaryFiltered.findIndex(item => e === item);
                if (selectedDiary === 'ACH') {
                  diaryFiltered = accounts.filter(
                    a => buyingDiaryAccounts.some(b => _.startsWith(a.account_number, b))
                  );
                } else if (selectedDiary === 'VTE') {
                  diaryFiltered = accounts.filter(
                    a => sellingDiaryAccounts.some(b => _.startsWith(a.account_number, b))
                  );
                } else if (selectedDiary === 'OD') {
                  diaryFiltered = accounts.filter(
                    a => !odDiaryAccounts.some(
                      e => (idAccount(e) === -1 && a.account_number.includes(e))
                    )
                  );
                } else diaryFiltered = accounts;
                return getFilteredOptions(diaryFiltered, filter);
              },
              onChangeValues: (value) => {
                this.onAccountChangeValue(i, value, formVal);

                if (split.master && value !== '') {
                  split.updateArgsSplit({ account: value });
                }
              },
              onKeyDown: async (event) => {
                if (event.keyCode === 13 || (event.keyCode === 9 && this[`${societyId}${i}account`].state.menuIsOpen)) {
                  setTimeout(async () => {
                    await this.handleAcccountBlur(i);
                    this[`${societyId}${i}label`].focus();
                  }, 0);
                }

                // close menu drawer if use 'alt+keydown' or 'alt+keyup'
                if (event.altKey) {
                  const unfocus = _.get(this, `${societyId}${i}label.select`);
                  if (unfocus && typeof unfocus.blur === 'function') {
                    unfocus.blur();
                  }
                }
              },
              customRef: (inputElement) => { this[`${societyId}${i}account`] = inputElement; },
              containerProps: {
                onDoubleClick: () => {
                  if (accountId) {
                    openConsultingPopup(societyId, { accountId });
                  }
                }
              },
              handler: () => this.setState({
                activeIndexRow: i,
                previousCell: 'date',
                nextCell: 'label'
              })
            },
            {
              onCustomFocus: (event) => {
                if (event) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                this.onRowFocus(i);
              },
              _type: 'shortcut',
              style: { height: '36px', width: '160px' },
              name: `${field}.label`,
              type: 'text',
              component: ReduxInputCell,
              border: true,
              disabled: allowEdit,
              onCustomBlur: e => onLabelBlur(e, i),
              handler: (action, event) => {
                this.setState({
                  activeIndexRow: i,
                  previousCell: 'account',
                  nextCell: 'piece'
                });
                this.labelShortCut(action, event, i);
              },
              inputRef: (inputElement) => { this[`${societyId}${i}label`] = inputElement; }
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'shortcut',
              style: { height: '36px', width: '70px' },
              name: `${field}.piece`,
              type: 'text',
              component: ReduxInputCell,
              border: true,
              disabled: allowEdit,
              handler: (action) => {
                this.setState({
                  activeIndexRow: i,
                  previousCell: 'label',
                  nextCell: 'piece2'
                });
                if (action === 'ENTER') { this[`${societyId}${i}piece2`].focus(); }
              },
              inputRef: (inputElement) => { this[`${societyId}${i}piece`] = inputElement; }
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'shortcut',
              style: { height: '36px', width: '70px' },
              name: `${field}.piece2`,
              type: 'text',
              component: ReduxInputCell,
              border: true,
              disabled: allowEdit,
              handler: (action, event) => {
                this.setState({
                  activeIndexRow: i,
                  previousCell: 'piece',
                  nextCell: 'debit'
                });
                this.pieceShortCut(action, event, i);
              },
              inputRef: (inputElement) => { this[`${societyId}${i}piece2`] = inputElement; }
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'shortcut',
              style: { height: '36px', width: '100%' },
              inputProps: { style: { textAlign: 'right' } },
              name: `${field}.debit`,
              component: ReduxInputCell,
              border: true,
              disabled: allowEdit || disableAmount,
              inputRef: (inputElement) => { this[`${societyId}${i}debit`] = inputElement; },
              onCustomBlur: e => this.onChangeValues('debit', `${field}.debit`, e.target.value),
              handler: (action, event) => {
                this.setState({
                  activeIndexRow: i,
                  previousCell: 'piece2',
                  nextCell: 'credit'
                });
                this.debitCreditShortcuts(action, event, i, 'debit');
              }
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'shortcut',
              style: { height: '36px', width: '100%' },
              inputProps: { style: { textAlign: 'right' } },
              name: `${field}.credit`,
              component: ReduxInputCell,
              border: true,
              disabled: allowEdit || disableAmount,
              inputRef: (inputElement) => { this[`${societyId}${i}credit`] = inputElement; },
              onCustomBlur: e => this.onChangeValues('credit', `${field}.credit`, e.target.value),
              handler: (action, event) => {
                this.setState({ activeIndexRow: i, previousCell: 'debit' });
                this.debitCreditShortcuts(action, event, i, 'credit');
              }
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'iconButton',
              name: 'icon-comments',
              color: (formVal.comment || (comment.body && comment.body !== '<p></p>')) ? 'primary' : 'inherit',
              disabled: allowEdit,
              onClick: () => getAndOpenComments(_.get(formVal, 'entry_id')),
              buttonRef: (inputElement) => { this[`${societyId}${i}comment`] = inputElement; }
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'iconButton',
              name: 'icon-attachment',
              titleInfoBulle: I18n.t('tooltips.importDoc'),
              color: !_.isEmpty(formVal.entry_list[i].pj_list) ? 'primary' : 'inherit',
              disabled: allowEdit,
              onClick: () => {
                this.setState(
                  {
                    dialogs: {
                      ...dialogs,
                      pj: {
                        isOpen: true,
                        field,
                        i
                      }
                    }
                  }
                );
              },
              buttonRef: (inputElement) => { this[`${societyId}${i}pj`] = inputElement; }
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'shortcut',
              menuPosition: 'fixed',
              placeholder: '',
              border: true,
              isDisabled: allowEdit || (selectedDiary === 'BQ' || selectedDiary === 'CAISSE'),
              textFieldProps: {
                className: stylesEntry.autoCompleteCell
              },
              name: `${field}.counterpart_account`,
              isCreatable: true,
              input: {
                value: {
                  ...counterpart_account,
                  value: counterpart_account.account_number
                }
              },
              onCreateOption: this.onCreateAccountHandler.bind(this, selectedDiary, i,
                {
                  accountNumberDisabled: true
                }),
              component: AccountAutoCompleteCell,
              onChangeValues: (counterpart_account) => {
                this.handleRightClickToModifyAccount(i,
                  {
                    accountNumberDisabled: true,
                    isContrepartie: true
                  },
                  {
                    counterpart_account
                  });
              },
              onKeyDown: (event) => {
                if (event.keyCode === 13 || (event.keyCode === 9 && this[`${societyId}${i}counterPart`].state.menuIsOpen)) {
                  this[`${societyId}${i}deadline`].focus();
                }
              },
              customRef: (inputElement) => { this[`${societyId}${i}counterPart`] = inputElement; },
              handler: () => this.setState({ activeIndexRow: i, previousCell: 'credit' })
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'shortcut',
              name: `${field}.deadline`,
              type: 'date',
              component: ReduxInputCell,
              border: true,
              disabled: allowEdit || (selectedDiary === 'BQ' || selectedDiary === 'CAISSE'),
              handler: (action) => {
                this.setState({ activeIndexRow: i, previousCell: 'counterpart_account' });
                if (action === 'ENTER') {
                  this[`${societyId}${i}paymentType`].focus();
                }
              },
              inputRef: (inputElement) => { this[`${societyId}${i}deadline`] = inputElement; }
            },
            {
              onCustomFocus: () => { this.onRowFocus(i); },
              _type: 'autocomplete',
              menuPosition: 'fixed',
              border: true,
              isDisabled: allowEdit,
              textFieldProps: {
                className: stylesEntry.autoCompleteCell
              },
              name: `${field}.payment_type`,
              component: PaymentTypeAutoCompleteCell,
              onKeyDown: (event) => {
                setLinePosition(i);
                this.lastCellKeyDown(event, i);
              },
              placeholder: '',
              customRef: (inputElement) => { this[`${societyId}${i}paymentType`] = inputElement; }
            }
            // {
            //   onCustomFocus: () => { this.onRowFocus(i); },
            //   _type: 'input',
            //   name: `${field}.date_entry`,
            //   disabled: true,
            //   type: 'date',
            //   component: ReduxInputCell,
            //   border: false,
            //   inputRef: (inputElement) => { this[`${societyId}${i}dateEntry`] = inputElement; }
            // }
          ].map((cell, index) => (
            <TableCell
              key={index}
              className={classnames(stylesEntry.cell)}
            >
              {cell._type === 'checkbox' && (
                <CheckBox
                  disabled
                  color="primary"
                />
              )}
              {cell._type === 'string' && <div {...cell}>{cell.value}</div>}
              {cell._type === 'input' && <Field {...cell} />}
              {cell._type === 'autocomplete' && <Field {...cell} />}
              {cell._type === 'empty' && ''}
              {cell._type === 'datepicker' && (
                <Shortcuts
                  {...cell}
                  name="NEW_ACCOUNTING"
                  alwaysFireHandler
                  stopPropagation={false}
                  preventDefault={false}
                >
                  <Fields {...cell} />
                </Shortcuts>
              )
              }
              {cell._type === 'shortcut'
              && (
                <Shortcuts
                  {...cell}
                  name="NEW_ACCOUNTING"
                  alwaysFireHandler
                  stopPropagation={false}
                  preventDefault={false}
                >
                  <Field {...cell} />
                </Shortcuts>
              )
              }
              {cell._type === 'iconButton' && (
                <IconButton {..._.omit(cell, '_type', 'name', 'titleInfoBulle')}>
                  <FontIcon {..._.omit(cell, '_type', 'onClick', 'buttonRef')} />
                </IconButton>
              )}
            </TableCell>
          ))}
        </TableRow>,
        isLastRow && (
          <TableRow key="table_row_accounting_total">
            <TableCell classes={{ root: classes.totaux }} colSpan={6}>
              <Typography variant="h6" align="right">
                {I18n.t('tables.accounting.total')}
              </Typography>
            </TableCell>
            <TableCell
              classes={{ root: classes.tableCellRootAlignRight }}
              className="entryArray__tableCell-outline"
            >
              <div className="entryArray__tableCell_numberContainer">
                <Typography variant="h6">
                  {total(formVal.entry_list, 'debit')}
                </Typography>
              </div>
            </TableCell>
            <TableCell
              classes={{ root: classes.tableCellRootAlignRight }}
              className="entryArray__tableCell-outline"
            >
              <div className="entryArray__tableCell_numberContainer">
                <Typography variant="h6">
                  {total(formVal.entry_list, 'credit')}
                </Typography>
              </div>
            </TableCell>
            <TableCell classes={{ root: classes.tableCellRoot }} colSpan={9} />
          </TableRow>
        )
      ];
    });
  };

  // eslint-disable-next-line arrow-body-style
  renderShortcutCatcher = () => {
    /**
     * This element is only needed to quickly remove focus from
     * any input field when a shortcut is pressed.
     *
     * Redux-form fields register values after blur
     * unless otherwise specified.
     * So we need to force blur() in case of a caught shortcut
     * to be 100% sure that the value is registered
     */
    return (
      <input
        type="text"
        style={{ position: 'fixed', left: -999999, opacity: 0 }}
        ref={this.hiddenElementRef}
      />
    );
  };

  render() {
    const {
      fields,
      confirmation,
      permission,
      information,
      formVal: {
        diary: {
          diary_id, diary_type_id
        } = {}
      } = {},
      handleCounterPart
    } = this.props;

    const {
      popover,
      dialogs,
      anchorPosition,
      isFlagModalOpen,
      diaryDialogOpened,
      modifyDiaryDialogIsOpen
    } = this.state;

    const {
      immo: { onClose: onCloseImmoFlagDialog, isOpen }
    } = dialogs;

    return (
      <Fragment>
        {this.renderShortcutCatcher()}
        {this.renderFields()}
        <Popover
          isOpen={popover.isOpen}
          onClose={() => this.setState({ popover: { isOpen: false } })}
          top={popover.top}
          left={popover.left}
          buttons={[
            {
              label: I18n.t('tables.accounting.popover.delete_row'),
              onClick: () => { this.deleteRow(fields, popover.currentIndex); }
            },
            {
              disabled: diary_type_id === 4
              || diary_type_id === 3, // These are the codes for CAISSE and BANQUE diaries
              label: I18n.t('tables.accounting.popover.modify'),
              onClick: () => { this.handleRightClickToModifyAccount(popover.currentIndex); }
            }
          ]}
        />
        <PjDialog
          open={dialogs.pj.isOpen}
          i={dialogs.pj.i}
          field={dialogs.pj.field}
          onClose={() => this.setState({ dialogs: { ...dialogs, pj: { isOpen: false } } })}
        />
        <NewAccountDialog
          {...dialogs.account}
        />
        <Shortcuts
          name="NEW_ACCOUNTING"
          handler={(action, event) => this._handleShortcuts(action, event, fields)}
          alwaysFireHandler
          stopPropagation={false}
          preventDefault={false}
          targetNodeSelector="body"
        />
        <CounterPartDialog
          {...dialogs.counterPart}
        />
        <CalculatorDialog
          {...dialogs.calculator}
        />
        <ConfirmationDialog
          isOpen={this.isConfirmationDialogOpen}
          onClose={() => this.handleCloseConfirm()}
          onValidate={() => this.handleValidateConfirm()}
          {...confirmation}
        />
        <ConfirmationDialog
          isOpen={this.isPermissionDialogOpen}
          onClose={() => this.handleClosePermission()}
          onValidate={() => this.handleValidatePermission()}
          {...permission}
        />
        <InformationDialog
          isOpen={this.isInformationDialogOpen}
          onClose={() => this.handleCloseInformation()}
          {...information}
        />
        {modifyDiaryDialogIsOpen && (
          <DiaryDialog
            isOpen={modifyDiaryDialogIsOpen}
            onClose={() => {
              this.setState({
                modifyDiaryDialogIsOpen: false
              });

              this.setState({
                isPostingData: false
              });
            }}
            onValidate={async () => {
              const validateButton = document.getElementById('newEntryValidateButton'); // TODO : use createref instead
              const counterPartApplied = await handleCounterPart(true);
              if (counterPartApplied) {
                validateButton.focus();
              }

              this.setState({
                isPostingData: false
              });
            }}
            diaryId={diary_id}
            modify
            codeDisabled
            labelDisabled
            diaryTypeDisabled
          />
        )}
        <FlagPopoverContainer
          isOpen={isFlagModalOpen}
          onClose={this.handleClose}
          anchorPosition={anchorPosition}
          anchorReference="anchorPosition"
          flag
        />
        <BISettingsDialog
          onClose={() => this.setState({
            dialogs: {
              ...dialogs,
              biSettings: {
                isOpen: false
              }
            }
          })}
          {...dialogs.biSettings}
        />
        <AccountParameters
          onClose={() => this.setState({
            dialogs: {
              ...dialogs,
              accountParameters: {
                isOpen: false
              }
            }
          })}
          {...dialogs.accountParameters}
        />
        {
          diaryDialogOpened && (
            <DiaryDialog
              isOpen={diaryDialogOpened}
              onClose={() => {
                this.setState({
                  diaryDialogOpened: false
                });
              }}
              modify={false}
            />
          )
        }
        <FlagDialog
          isOpen={isOpen}
          onClose={() => {
            this.setState({
              dialogs: { ...dialogs, immo: { isOpen: false } }
            });
            onCloseImmoFlagDialog();
          }
          }
          flagType="IMMO"
        />
      </Fragment>
    );
  }
}

const styles = () => ({
  totaux: {
    borderBottom: 'none',
    paddingRight: 12
  },
  tableCellRoot: {
    borderBottom: 'none'
  },
  tableCellRootAlignRight: {
    textAlign: 'right',
    borderBottom: 'none',
    padding: 0
  },
  focusedInput: {
    border: '1px solid blue'
  }
});

EntryArray.defaultProps = {
  classes: {},
  editFlagType: '',
  comment: {},
  diaryValue: {},
  formVal: {},
  master_id: '',
  splitArgs: {}
};

EntryArray.propTypes = {
  editFlagType: PropTypes.string,
  enableEditFlag: PropTypes.func.isRequired,
  isFlagDialogOpen: PropTypes.bool.isRequired,
  initializeAccount: PropTypes.func.isRequired,
  fields: PropTypes.shape({}).isRequired,
  pushEntry: PropTypes.func.isRequired,
  getAndOpenComments: PropTypes.func.isRequired,
  counterPartLines: PropTypes.func.isRequired,
  resetAccount: PropTypes.func.isRequired,
  resetAccountSelected: PropTypes.func.isRequired,
  selectAccount: PropTypes.func.isRequired,
  getAccountDetail: PropTypes.func.isRequired,
  changeFormValue: PropTypes.func.isRequired,
  onLabelBlur: PropTypes.func.isRequired,
  enableCreateCounterPartConfirmation: PropTypes.func.isRequired,
  enableLinkDiaryAccountPermission: PropTypes.func.isRequired,
  enableCounterPartInformation: PropTypes.func.isRequired,
  formVal: PropTypes.shape({}),
  currentEntry: PropTypes.shape({}).isRequired,
  comment: PropTypes.object,
  openConfirmFromHeader: PropTypes.bool.isRequired,
  openPermissionFromHeader: PropTypes.bool.isRequired,
  openInformationFromHeader: PropTypes.bool.isRequired,
  societyId: PropTypes.number.isRequired,
  confirmation: PropTypes.shape({}).isRequired,
  permission: PropTypes.shape({}).isRequired,
  information: PropTypes.shape({}).isRequired,
  accountData: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  classes: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({})
  ])),
  split: splitShape.isRequired,
  setLinePosition: PropTypes.func.isRequired,
  enqueueSnackbarWarning: PropTypes.func.isRequired,
  closeSnackbar: PropTypes.func.isRequired,
  entriesType: PropTypes.string.isRequired,
  diaryValue: PropTypes.shape({}),
  entryValue: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  getDiaryDetail: PropTypes.func.isRequired,
  dayRef: PropTypes.func.isRequired,
  handleCounterPart: PropTypes.func.isRequired,
  yearRef: PropTypes.object.isRequired,
  initializeImmoForm: PropTypes.func.isRequired,
  master_id: PropTypes.string,
  splitArgs: PropTypes.shape({})
};

export default withStyles(styles)(EntryArray);
