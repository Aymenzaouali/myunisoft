import React, { PureComponent } from 'react';
import _ from 'lodash';
import {
  Table, TableHead, TableBody, TableRow, TableCell,
  Typography, Chip
} from '@material-ui/core';
import { Shortcuts } from 'react-shortcuts';
import { FieldArray, Field } from 'redux-form';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { DiaryAutoComplete } from 'containers/reduxForm/Inputs';
import EntryArray from 'containers/groups/Tables/NewAccounting/EntryArray';
import { DiaryDialog } from 'containers/groups/Dialogs';
import { routesByKey } from 'helpers/routes';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import moment from 'moment';
import Dropzone from 'react-dropzone';
import I18n from 'assets/I18n';
import classNames from 'classnames';

import { newAccountingHeaderKeys } from 'assets/constants/keys';

import FontIcon from 'components/basics/Icon/Font';
import Window from 'components/basics/Window';
import { documentGroupShape } from 'components/groups/Attachments/props';

import styles from './newAccountingTable.module.scss';

// Constants
const stylesTheme = () => ({
  isAmount: {
    textAlign: 'right',
    paddingRight: 10
  },
  paddingLeft: {
    paddingLeft: 10
  },
  center: {
    textAlign: 'center'
  }
});

const HEADERS = newAccountingHeaderKeys.map(data => ({
  ...data,
  sortable: true,
  label: I18n.t(`tables.newAccounting.${data.keyLabel}`)
}));

// Component
class NewAccountingTable extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      popup: { open: false, focusing: false },
      diaryDialogOpened: false
    };
  }

  componentWillUnmount() {
    if (this.pjWindow) {
      this.pjWindow.close();
    }
  }

  onDrop = (files) => {
    const { pushEntry } = this.props;
    files.forEach(file => pushEntry('pj_list', file));
  };

  generateRow = () => {
    const { formVal } = this.props;
    if (!formVal.entry_list) {
      const { changeFormValue } = this.props;
      changeFormValue('entry_list', [{ date_entry: moment().format('YYYY-MM-DD') }]);
      changeFormValue('pj_list', []);
    }
  };

  getEntryPjs = () => {
    const { formVal, typeEntry, manualDocs } = this.props;
    const pjs = _.get(formVal, '[pj_list]', []);

    let manualPjs = [];
    if (typeEntry === 'm' && !_.isEmpty(manualDocs)) {
      manualPjs = _.flatten(
        _.values(manualDocs).map(
          docs => docs.documents && docs.documents.map((d, i) => ({
            ...d,
            isAttached: i !== 0,
            isGroup: docs.document_attached_id !== 0,
            document_attached_id: docs.document_attached_id !== 0 ? docs.document_attached_id : null
          }))
        )
      );
    }

    return [...pjs, ...manualPjs];
  };

  openWindow = (document) => {
    const { popup } = this.state;
    const { formVal } = this.props;
    const pjs = this.getEntryPjs();

    let otherPjs = [];
    _.get(formVal, 'entry_list', [])
      .forEach((entry) => {
        otherPjs = [...otherPjs, ..._.get(entry, 'pj_list', [])];
      });

    const documents = [...pjs, ...otherPjs];

    this.setState({
      popup: {
        ...popup,
        open: true,
        focusing: true,
        message: { document, documents }
      }
    });
  };

  handleShortCut = (action, e) => {
    const {
      formVal,
      changeFilterForm,
      resetNewAccounting,
      changeFormValue,
      societyId
    } = this.props;
    switch (action) {
    case 'SHOW_ACCOUNTING_PLAN':
      window.open(routesByKey.chartAccountTva.replace(':id', societyId), `window${new Date().getTime()}`, `width=${window.innerWidth},height=${window.innerHeight}`);
      break;
    case 'MANUAL_ACCOUNTING':
      e.preventDefault();
      e.stopPropagation();
      changeFilterForm('type', 'm');
      break;
    case 'RESET_NEW_ACCOUNTING':
      e.preventDefault();
      e.stopPropagation();
      resetNewAccounting();
      break;
    case 'INTERCHANGE_DEBIT_CREDIT':
      e.preventDefault();
      e.stopPropagation();
      _.get(formVal, 'entry_list', []).forEach((entry, i) => {
        changeFormValue(`entry_list[${i}]`, { ...entry, debit: entry.credit, credit: entry.debit });
      });
      break;
    default:
      break;
    }
  };

  deletePj = (i, file) => () => {
    const {
      deleteEntry, filters, changeFormValue, formVal,
      removeManualDoc
    } = this.props;

    if (filters.type === 'e' || file instanceof File) {
      deleteEntry('pj_list', i);

      if (filters.type === 'e' && !(file instanceof File)) {
        changeFormValue('delete_list', [..._.get(formVal, 'delete_list', []), file]);
      }
    } else {
      removeManualDoc(file);
    }
  };

  render() {
    const {
      classes,
      formVal: { diary, type },
      societyId,
      formName,
      monthsList,
      yearsList,
      handleCounterPart,
      setDiaryDialog,
      getDiariesType,
      getDiaries,
      changeNewAccountingForm,
      splitArgs,
      master_id
    } = this.props;
    const { popup, diaryDialogOpened } = this.state;

    const pjs = this.getEntryPjs();
    for (let i = 0; i < pjs.length; i += 1) {
      if (!pjs[i]) pjs.splice(i, 1);
    }

    const title = type || 'new';

    const onValidateModification = (newDiaryInfo) => {
      const {
        diary_code,
        diary_label,
        diary_type
      } = newDiaryInfo;
      changeNewAccountingForm({
        id: _.get(diary, 'diary_id'),
        code: diary_code,
        label: diary_label,
        diary_type,
        diary_accountNumber: _.get(diary, 'diary_accountNumber')
      });
      getDiaries(societyId);
    };
    return (
      <Shortcuts
        name="NEW_ACCOUNTING"
        handler={this.handleShortCut}
        alwaysFireHandler
        stopPropagation={false}
        preventDefault={false}
        targetNodeSelector="body"
      >
        <Typography variant="h6">
          {I18n.t(`newAccounting.${title}`)}
        </Typography>
        <Dropzone
          style={{
            width: '100%',
            height: '100%'
          }}
          disableClick
          onDrop={this.onDrop}
        >
          <div className={styles.topFieldsContainer}>
            <Field
              component={DiaryAutoComplete}
              name="diary"
              className={styles.diaryField}
              menuPosition="fixed"
              placeholder={I18n.t('accounting.filter.diary')}
              fullWidth
              onDoubleClick={() => {
                if (master_id !== 'bankLink') {
                  getDiariesType();
                  this.setState({ diaryDialogOpened: true });
                  setDiaryDialog(diary);
                }
              }}
              isDisabled={master_id === 'bankLink'}
              onKeyDown={(event) => {
                if (event.keyCode === 13) this.month.focus();
              }}
            />
            <Field
              component={AutoComplete}
              className={styles.monthField}
              menuPosition="fixed"
              placeholder="Mois"
              name="month"
              options={monthsList}
              onKeyDown={(event) => {
                if (event.keyCode === 13) this.year.focus();
              }}
              customRef={(inputElement) => { this.month = inputElement; }}
            />
            <Field
              component={AutoComplete}
              className={styles.yearField}
              menuPosition="fixed"
              placeholder="Année"
              name="year"
              options={yearsList}
              onKeyDown={(event) => {
                if (event.keyCode === 13) this.dayRef.focus();
              }}
              customRef={(inputElement) => { this.year = inputElement; }}
            />

          </div>
          <Table id={formName}>
            <TableHead>
              <TableRow classes={{ root: styles.headers }}>
                {HEADERS.map(header => (
                  <TableCell
                    key={header.keyLabel}
                    classes={{
                      root: classNames({
                        [classes.isAmount]: header.isAmount,
                        [classes.paddingLeft]: header.paddingLeft,
                        [classes.center]: header.center
                      })
                    }}
                  >
                    {header.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              <FieldArray
                name="entry_list"
                component={EntryArray}
                formName={formName}
                societyId={societyId}
                dayRef={(el) => { this.dayRef = el; }}
                yearRef={this.year}
                handleCounterPart={handleCounterPart}
                splitArgs={splitArgs}
                master_id={master_id}
              />
            </TableBody>
          </Table>
        </Dropzone>
        { pjs.length > 0 && (
          <div className={styles.pjContainer}>
            <Typography variant="h1">{I18n.t('newAccounting.pj')}</Typography>
            <div className={styles.chipsContainer}>
              { pjs.map((p, i) => (p && p.isAttached ? null : (
                <Chip
                  key={p.token || p.name}
                  icon={(p.isGroup) && <FontIcon name="icon-attach" />}
                  label={p.name}

                  onClick={() => this.openWindow(p)}
                  onDelete={this.deletePj(i, p)}
                />
              ))) }
            </div>
          </div>
        ) }
        <Window
          path="/overview"
          window_id="overview"
          message={popup.message}
          features={`width=${window.innerWidth / 2},left=${window.screenX + (window.innerWidth / 2)}`}

          open={popup.open}
          focusing={popup.focusing}
          onOpen={() => this.setState({ popup: { ...popup, open: true } })}
          onFocusChanged={() => this.setState({ popup: { ...popup, focusing: false } })}
          onClose={() => this.setState({ popup: { ...popup, open: false } })}
        />
        <DiaryDialog
          onValidate={newDiaryInfo => onValidateModification(newDiaryInfo)}
          isOpen={diaryDialogOpened}
          onClose={() => this.setState({ diaryDialogOpened: false })}
          diaryId={_.get(diary, 'diary_id')}
          modify
        />
      </Shortcuts>
    );
  }
}

// Props
NewAccountingTable.propTypes = {
  classes: PropTypes.shape({
    table: PropTypes.shape({
      minWidth: PropTypes.number
    }),
    tableWrapper: PropTypes.shape({
      width: PropTypes.string,
      marginTop: PropTypes.number,
      overflowX: PropTypes.string
    })
  }),
  formVal: PropTypes.shape({}),
  changeFormValue: PropTypes.func.isRequired,
  pushEntry: PropTypes.func.isRequired,
  changeFilterForm: PropTypes.func,
  resetNewAccounting: PropTypes.func,
  deleteEntry: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired,
  filters: PropTypes.object,
  manualDocs: PropTypes.objectOf(documentGroupShape),
  formName: PropTypes.string.isRequired,
  removeManualDoc: PropTypes.func.isRequired,
  monthsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  yearsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  typeEntry: PropTypes.string,
  handleCounterPart: PropTypes.func.isRequired,
  setDiaryDialog: PropTypes.func.isRequired,
  getDiariesType: PropTypes.func.isRequired,
  getDiaries: PropTypes.func.isRequired,
  changeNewAccountingForm: PropTypes.func.isRequired,
  master_id: PropTypes.string,
  splitArgs: PropTypes.shape({})
};

NewAccountingTable.defaultProps = {
  classes: {
    table: {},
    tableWrapper: {}
  },
  formVal: {},
  manualDocs: {},
  filters: undefined,
  changeFilterForm: () => {},
  resetNewAccounting: () => {},
  typeEntry: 'e',
  master_id: '',
  splitArgs: {}
};

export default withStyles(stylesTheme)(NewAccountingTable);
