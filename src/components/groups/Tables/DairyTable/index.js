import React, { useEffect } from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';

const DairyTable = ({
  param,
  isLoading,
  setTableData,
  ...otherProps
}) => {
  useEffect(() => {
    setTableData(param);
  }, [isLoading]);
  return (
    <TableFactory
      param={param}
      {...otherProps}
    />
  );
};

DairyTable.propTypes = {
  param: PropTypes.shape({}).isRequired,
  setTableData: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default DairyTable;
