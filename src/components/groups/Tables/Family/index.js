import React, { PureComponent } from 'react';
import {
  TableCell,
  TableHead,
  Table
} from '@material-ui/core';
import { FieldArray } from 'redux-form';
import FamilyArray from 'containers/groups/Tables/Family/FamilyArray';
import I18n from 'assets/I18n';
import ownStyles from './Family.module.scss';

class FamilyBoundariesTable extends PureComponent {
  render() {
    return (
      <div className={ownStyles.tableContainer}>
        <Table>
          <TableHead>
            <TableCell key="Civility">{I18n.t('physicalPersonCreation.familyBoundaries_form.genderHeader')}</TableCell>
            <TableCell key="Name">{I18n.t('physicalPersonCreation.familyBoundaries_form.nameHeader')}</TableCell>
            <TableCell key="Firstname">{I18n.t('physicalPersonCreation.familyBoundaries_form.firstnameHeader')}</TableCell>
            <TableCell key="Link">{I18n.t('physicalPersonCreation.familyBoundaries_form.linkHeader')}</TableCell>
          </TableHead>
          <FieldArray
            name="coord"
            component={FamilyArray}
          />
        </Table>
      </div>
    );
  }
}

export default FamilyBoundariesTable;
