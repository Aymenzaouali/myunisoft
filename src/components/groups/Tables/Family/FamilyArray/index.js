import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  TableBody,
  TableCell,
  TableRow,
  IconButton,
  withStyles,
  Typography
} from '@material-ui/core';
import { FontIcon } from 'components/basics/Icon';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';
import styles from './FamilyArray.module.scss';

class FamilyArray extends React.PureComponent {
  render() {
    const {
      societyId,
      familyLinkTypes,
      classes,
      removeRelative,
      changeTypeRelative,
      relatives
    } = this.props;

    const { family = [] } = relatives;

    return (
      <Fragment>
        {family.length === 0
        && (
          <TableRow>
            <TableCell />
            <Typography color="primary" variant="title">{I18n.t('physicalPersonCreation.familyBoundaries_form.empty')}</Typography>
          </TableRow>
        )
        }
        <TableBody>
          {
            family && family.map((field, i) => [
              <TableRow>
                <TableCell>
                  <Field
                    name={`family[${i}].civility`}
                    component={ReduxTextField}
                    margin="none"
                    className={styles.civilityField}
                    disabled
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`family[${i}].name`}
                    component={ReduxTextField}
                    margin="none"
                    disabled
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`family[${i}].firstname`}
                    component={ReduxTextField}
                    margin="none"
                    disabled
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`family[${i}].link_type_id`}
                    component={ReduxSelect}
                    margin="none"
                    list={familyLinkTypes}
                    className={classes.boundFieldContainer}
                    onChange={event => changeTypeRelative(societyId, i, event.target.value)}
                  />
                </TableCell>
                <TableCell>
                  <IconButton onClick={() => removeRelative(societyId, i, field.family_link_id)}>
                    <FontIcon name="icon-trash" color="red" />
                  </IconButton>
                </TableCell>
              </TableRow>
            ])
          }
        </TableBody>
      </Fragment>
    );
  }
}

const themeStyle = () => ({
  boundFieldContainer: {
    marginBottom: 18,
    width: 155
  }
});

FamilyArray.defaultProps = {
  classes: {},
  relatives: [{}]
};

FamilyArray.propTypes = {
  societyId: PropTypes.number.isRequired,
  relatives: PropTypes.arrayOf(
    PropTypes.shape({
      civility: PropTypes.string,
      firstname: PropTypes.string,
      label: PropTypes.string,
      name: PropTypes.string,
      pers_physique_id_A: PropTypes.number,
      value: PropTypes.number
    }),
    PropTypes.arrayOf(PropTypes.shape({
      civility: PropTypes.string,
      firstname: PropTypes.string,
      label: PropTypes.string,
      name: PropTypes.string,
      pers_physique_id_A: PropTypes.number,
      value: PropTypes.number
    }))
  ),
  classes: PropTypes.shape({}),
  changeTypeRelative: PropTypes.func.isRequired,
  removeRelative: PropTypes.func.isRequired,
  familyLinkTypes: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number,
    label: PropTypes.string,
    name: PropTypes.string
  })).isRequired
};

export default withStyles(themeStyle)(FamilyArray);
