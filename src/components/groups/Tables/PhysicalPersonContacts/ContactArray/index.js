import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  TableBody,
  TableCell,
  TableRow,
  IconButton,
  withStyles
} from '@material-ui/core';
import { FontIcon } from 'components/basics/Icon';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { Field } from 'redux-form';
import _ from 'lodash';
import I18n from 'assets/I18n';
import ownStyles from './ContactArray.module.scss';

const contactModel = { value: '', label: '' };

const ContactArray = (props) => {
  const {
    selectedPerson_coords, classes, coord_types, getCoordType
  } = props;

  const [persons, setPersons] = useState(selectedPerson_coords);

  useEffect(() => {
    getCoordType();
    setPersons(selectedPerson_coords);
  }, [selectedPerson_coords]);

  const actionRow = () => {
    const { societyId, addContact } = props;
    addContact(societyId);
    persons.push(contactModel);
  };

  const deleteContact = (index, coordId) => {
    const { societyId, removeContact } = props;
    removeContact(societyId, index, coordId);
    _.pullAt(persons, [index]);
  };

  return (
    <Fragment>
      <TableBody>
        <TableRow classes={{ root: classes.addContactRow }} onClick={() => actionRow()}>
          <TableCell colSpan={4} classes={{ root: classes.addContactCell }}>
            {I18n.t('physicalPersonCreation.generalInformation_form.contacts_array.addContact')}
          </TableCell>
        </TableRow>
        {
          persons.map((field, i) => [
            <TableRow>
              <TableCell>
                <Field
                  name={`coordonnee[${i}].value`}
                  component={ReduxTextField}
                  className={ownStyles.contactCoord}
                />
              </TableCell>
              <TableCell>
                <Field
                  name={`coordonnee[${i}].label`}
                  component={ReduxTextField}
                  className={ownStyles.contactCoord}
                />
              </TableCell>
              <TableCell>
                <Field
                  name={`coordonnee[${i}].type_id`}
                  component={ReduxSelect}
                  className={ownStyles.contactType}
                  list={coord_types}
                  margin="none"
                />
              </TableCell>
              <TableCell>
                <IconButton onClick={() => deleteContact(i, field.id)}>
                  <FontIcon name="icon-trash" color="red" />
                </IconButton>
              </TableCell>
            </TableRow>
          ])
        }
      </TableBody>
    </Fragment>

  );
};

ContactArray.defaultProps = {
  selectedPerson_coords: [],
  classes: {},
  coord_types: []
};

ContactArray.propTypes = {
  societyId: PropTypes.number.isRequired,
  getCoordType: PropTypes.func.isRequired,
  classes: PropTypes.shape({}),
  removeContact: PropTypes.func.isRequired,
  addContact: PropTypes.func.isRequired,
  selectedPerson_coords: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    value: PropTypes.string,
    label: PropTypes.string,
    type: PropTypes.shape({
      id: PropTypes.number,
      value: PropTypes.string,
      label: PropTypes.string
    })
  })),
  coord_types: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number,
    label: PropTypes.string
  }))
};

const styles = () => ({
  addContactRow: {
    cursor: 'pointer',
    width: 'auto',
    height: 32,
    backgroundColor: '#DDFAFA'
  },
  addContactCell: {
    fontSize: 14,
    fontWeight: 200,
    fontFamily: 'basier_circlemedium'
  }
});

export default withStyles(styles)(ContactArray);
