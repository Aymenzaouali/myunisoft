import React, { PureComponent } from 'react';
import {
  TableCell,
  TableHead,
  Table
} from '@material-ui/core';
import ContactArray from 'containers/groups/Tables/PhysicalPersonContacts/ContactArray';
import { FieldArray } from 'redux-form';
import I18n from 'assets/I18n';
import ownStyles from './PhysicalPersonContacts.module.scss';

class PhysicalPersonContactsTable extends PureComponent {
  render() {
    return (
      <div className={ownStyles.tableContainer}>
        <Table>
          <TableHead>
            <TableCell key="Coordonnées">{I18n.t('physicalPersonCreation.generalInformation_form.contacts_array.contactDetails')}</TableCell>
            <TableCell key="Description / Nom">{I18n.t('physicalPersonCreation.generalInformation_form.contacts_array.contactDescription')}</TableCell>
            <TableCell key="Type">{I18n.t('physicalPersonCreation.generalInformation_form.contacts_array.contactType')}</TableCell>
          </TableHead>
          <FieldArray
            name="coordonnee"
            component={ContactArray}
          />
        </Table>
      </div>
    );
  }
}

export default PhysicalPersonContactsTable;
