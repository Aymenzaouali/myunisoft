import React from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import ownStyles from './RAFAlreadySelected.module.scss';

const RAFAlreadySelected = ({ param, ...otherProps }) => (
  <div className={ownStyles.container}>
    <div className={ownStyles.tableContainer}>
      <TableFactory
        param={param}
        {...otherProps}
      />
    </div>
  </div>
);


RAFAlreadySelected.propTypes = {
  param: PropTypes.shape({}).isRequired
};

export default RAFAlreadySelected;
