import React, { PureComponent } from 'react';
import {
  TableCell,
  TableHead,
  Table
} from '@material-ui/core';
import { FieldArray } from 'redux-form';
import SocietyArray from 'containers/groups/Tables/Society/SocietyArray';
import I18n from 'assets/I18n';
import ownStyles from './Society.module.scss';

class SocietyTable extends PureComponent {
  render() {
    return (
      <div className={ownStyles.tableContainer}>
        <Table>
          <TableHead>
            <TableCell key="SocietyName">{I18n.t('physicalPersonCreation.society_form.nameHeader')}</TableCell>
            <TableCell key="FunctionLabel">{I18n.t('physicalPersonCreation.society_form.functionHeader')}</TableCell>
            <TableCell key="SignatoryFunction">{I18n.t('physicalPersonCreation.society_form.signatoryHeader')}</TableCell>
            <TableCell key="StartDate">{I18n.t('physicalPersonCreation.society_form.startDateHeader')}</TableCell>
            <TableCell key="EndDate">{I18n.t('physicalPersonCreation.society_form.endDateHeader')}</TableCell>
            <TableCell key="PP">{I18n.t('physicalPersonCreation.society_form.ppHeader')}</TableCell>
            <TableCell key="US">{I18n.t('physicalPersonCreation.society_form.usHeader')}</TableCell>
            <TableCell key="NP">{I18n.t('physicalPersonCreation.society_form.npHeader')}</TableCell>
            <TableCell key="Percent">{I18n.t('physicalPersonCreation.society_form.percentHeader')}</TableCell>
          </TableHead>
          <FieldArray
            name="coord"
            component={SocietyArray}
          />
        </Table>
      </div>
    );
  }
}

export default SocietyTable;
