import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  TableBody,
  TableCell,
  TableRow,
  IconButton,
  Typography
} from '@material-ui/core';
import { FontIcon } from 'components/basics/Icon';
import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';
import ownStyles from './SocietyArray.module.scss';

class SocietyArray extends React.PureComponent {
  render() {
    const {
      society = [],
      removeSociety,
      employmentFunctionTypes,
      signatoryFunctionTypes,
      societyId
    } = this.props;
    return (
      <Fragment>
        {society.length === 0
          && (
            <TableRow>
              <Typography
                color="primary"
                variant="title"
              >
                {I18n.t('physicalPersonCreation.society_form.empty')}
              </Typography>
            </TableRow>
          )
        }
        <TableBody>
          {
            society.length !== 0 && society.map((field, i) => [
              <TableRow>
                <TableCell>
                  <Field
                    name={`firm[${i}].name`}
                    component={ReduxTextField}
                    margin="none"
                    className={ownStyles.societyNameField}
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`firm[${i}].function_id`}
                    component={ReduxSelect}
                    list={employmentFunctionTypes}
                    margin="none"
                    className={ownStyles.functionLabelField}
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`firm[${i}].signatory_function_id`}
                    component={ReduxSelect}
                    list={signatoryFunctionTypes}
                    margin="none"
                    className={ownStyles.functionLabelField}
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`firm[${i}].start_date`}
                    component={ReduxMaterialDatePicker}
                    margin="none"
                    className={ownStyles.dateField}
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`firm[${i}].end_date`}
                    component={ReduxMaterialDatePicker}
                    margin="none"
                    className={ownStyles.dateField}
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`firm[${i}].PP`}
                    component={ReduxTextField}
                    margin="none"
                    className={ownStyles.shareField}
                    type="number"
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`firm[${i}].US`}
                    component={ReduxTextField}
                    margin="none"
                    className={ownStyles.shareField}
                    type="number"
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`firm[${i}].NP`}
                    component={ReduxTextField}
                    margin="none"
                    className={ownStyles.shareField}
                    type="number"
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`firm[${i}].percent`}
                    component={ReduxTextField}
                    margin="none"
                    className={ownStyles.shareField}
                    type="number"
                  />
                </TableCell>
                {
                  (
                    society[i].NP === undefined
                    || society[i].PP === undefined
                    || society[i].US === undefined
                  )
                  && (
                    <TableCell>
                      <IconButton
                        onClick={() => removeSociety(
                          societyId,
                          i,
                          field.link_society_physical_person_id
                        )}
                      >
                        <FontIcon name="icon-trash" color="red" />
                      </IconButton>
                    </TableCell>
                  )
                }
              </TableRow>
            ])
          }
        </TableBody>
      </Fragment>
    );
  }
}

SocietyArray.defaultProps = {
  employmentFunctionTypes: [],
  signatoryFunctionTypes: []
};

SocietyArray.propTypes = {
  societyId: PropTypes.number.isRequired,
  removeSociety: PropTypes.func.isRequired,
  society: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  employmentFunctionTypes: PropTypes.arrayOf(PropTypes.object),
  signatoryFunctionTypes: PropTypes.arrayOf(PropTypes.object)
};

export default SocietyArray;
