import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  TableBody,
  TableCell,
  TableRow,
  IconButton,
  Typography
} from '@material-ui/core';
import { FontIcon } from 'components/basics/Icon';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { Field } from 'redux-form';
import I18n from 'assets/I18n';

class UtilisateurArray extends React.PureComponent {
  render() {
    const {
      societyId,
      firms,
      removeCompany,
      user_id
    } = this.props;

    return (
      <Fragment>
        {firms.length === 0
        && (
          <TableRow>
            <TableCell />
            <Typography color="primary" variant="title">{I18n.t('physicalPersonCreation.familyBoundaries_form.empty')}</Typography>
          </TableRow>
        )
        }
        <TableBody>
          {
            firms && firms.map((field, i) => [
              <TableRow>
                <TableCell>
                  <Field
                    name={`user.access_list[${i}].label`}
                    component={ReduxTextField}
                    margin="none"
                  />
                </TableCell>
                <TableCell>
                  <Field
                    name={`user.access_list[${i}].profil_name`}
                    component={ReduxTextField}
                    margin="none"
                  />
                </TableCell>
                <TableCell>
                  <IconButton onClick={() => removeCompany(societyId, i, field, user_id)}>
                    <FontIcon name="icon-trash" color="red" />
                  </IconButton>
                </TableCell>
              </TableRow>
            ])
          }
        </TableBody>
      </Fragment>
    );
  }
}

UtilisateurArray.defaultProps = {
  user_id: null
};

UtilisateurArray.propTypes = {
  societyId: PropTypes.number.isRequired,
  user_id: PropTypes.number,
  firms: PropTypes.arrayOf().isRequired,
  removeCompany: PropTypes.func.isRequired
};

export default UtilisateurArray;
