import React from 'react';
import { withStyles } from '@material-ui/core';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';
import { Field, FieldArray } from 'redux-form';
import ReduxAutoComplete from 'components/reduxForm/Inputs/AutoComplete';
import SocietiesList from 'containers/groups/Tables/Utilisateur/UtilisateurArray';
import styles from './Utilisateur.module.scss';

class UserTable extends React.PureComponent {
  state = {
    selectedSociety: {
      society: {},
      level: {}
    }
  };

  render() {
    const { selectedSociety } = this.state;
    const {
      societyId,
      societies,
      accessLevel,
      addAllowedCompany,
      classes
    } = this.props;

    return (
      <div className={styles.container}>
        <Field
          name="societies"
          component={ReduxAutoComplete}
          options={societies}
          placeholder={I18n.t('physicalPersonCreation.companyAccess_form.field.company')}
          className={classes.field}
          onChange={(event) => {
            this.setState({ selectedSociety: event });
          }}
        />
        <Field
          name="level"
          component={ReduxAutoComplete}
          options={accessLevel}
          placeholder={I18n.t('physicalPersonCreation.companyAccess_form.field.level')}
          className={classes.field}
          onChange={(event) => {
            addAllowedCompany(societyId, selectedSociety, event);
          }}
        />
        <FieldArray
          name="access_list"
          component={SocietiesList}
        />
      </div>
    );
  }
}

const ownStyles = () => ({
  field: {
    'margin-right': 20
  }
});

UserTable.propTypes = {
  societyId: PropTypes.number.isRequired,
  addAllowedCompany: PropTypes.func.isRequired,
  accessLevel: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  societies: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default withStyles(ownStyles)(UserTable);
