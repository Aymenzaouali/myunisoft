import React, { useEffect } from 'react';
import TableFactory from 'components/groups/Tables/Factory';
import PropTypes from 'prop-types';
import ownStyles from './bankIntegrationSettingsTable.module.scss';

const BankIntegrationSettingsTable = ({
  param, setTableData, isLoading, ...otherProps
}) => {
  useEffect(() => {
    setTableData(param);
  }, [isLoading]);
  return (
    <div className={ownStyles.tableContainer}>
      <TableFactory
        param={param}
        {...otherProps}
      />
    </div>
  );
};

BankIntegrationSettingsTable.propTypes = {
  param: PropTypes.shape({}).isRequired,
  setTableData: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default BankIntegrationSettingsTable;
