import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import I18n from 'assets/I18n';
import _ from 'lodash';
import { formatNumber } from 'helpers/number';
import { FontIcon } from 'components/basics/Icon';
import Button from 'components/basics/Buttons/Button';
import { CheckBoxCell } from 'components/basics/Cells';

import styles from './Lettrage.module.scss';

function Lettrage(props) {
  const {
    letter,
    notLetter,
    onClickLetter,
    onClickNotLetter,
    total,
    classes,
    nbSelected,
    nbLetter
  } = props;

  return (
    <div className={styles.lettrage}>
      {!_.isNull(nbSelected) && (
        <Typography
          classes={{ root: classes.rootBold }}
        >
          {nbSelected !== 0 && I18n.t('Lettrage.select', { count: nbSelected })}
        </Typography>
      ) }

      {!_.isNull(notLetter) && (
        <Fragment>
          <CheckBoxCell customColor="yellow" checked />
          <Typography classes={{ root: classes.root }}>
            {`${I18n.t('Lettrage.notLetter')} ${formatNumber(notLetter)}`}
          </Typography>
          {onClickLetter && (
            <Button
              classes={{ root: classes.button }}
              variant="contained"
              color="primary"
              onClick={onClickLetter}
              disabled={total !== 0 || nbSelected < 2 || nbLetter !== 0}
            >
              <FontIcon size={34} className="icon-lettrer" />
            </Button>
          )}
        </Fragment>
      )}

      {!_.isNull(letter) && (
        <Fragment>
          <CheckBoxCell color="primary" checked />
          <Typography classes={{ root: classes.root }}>
            {`${I18n.t('Lettrage.letter')} ${formatNumber(letter)}`}
          </Typography>
          {onClickNotLetter && (
            <Button
              classes={{ root: classes.button }}
              variant="contained"
              color="primary"
              disabled={nbLetter === 0}
              onClick={onClickNotLetter}
            >
              <FontIcon size={34} className="icon-delettrage" />
            </Button>
          )}
        </Fragment>
      )}

      {!_.isNull(total) && (
        <Fragment>
          <Typography classes={{ root: classes.root }}>
            {`${I18n.t('Lettrage.total')} ${formatNumber(total)}`}
          </Typography>
        </Fragment>
      )}
    </div>
  );
}

Lettrage.defaultProps = {
  nbSelected: null,
  letter: null,
  notLetter: null,
  onClickLetter: null,
  onClickNotLetter: null,
  total: null
};

Lettrage.propTypes = {
  nbSelected: PropTypes.string,
  letter: PropTypes.string,
  notLetter: PropTypes.string,
  onClickLetter: PropTypes.func,
  onClickNotLetter: PropTypes.func,
  total: PropTypes.string,
  classes: PropTypes.shape({}).isRequired,
  nbLetter: PropTypes.string.isRequired
};

const stylesMaterial = {
  root: {
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: '10px'
  },
  rootBold: {
    marginTop: 'auto',
    marginBottom: 'auto',
    fontWeight: 'bold'
  },
  button: {
    minWidth: 34,
    minHeight: 30,
    fontSize: 28,
    margin: 10,
    padding: 0
  }
};

export default withStyles(stylesMaterial)(Lettrage);
