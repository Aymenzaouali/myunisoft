import React from 'react';
import classNames from 'classnames';
import { Typography, Button } from '@material-ui/core';
import WarningIcon from '@material-ui/icons/Warning';
import I18n from 'assets/I18n';
import styles from './ForeignBrowserAlert.module.scss';

const ForeignBrowserAlert = () => (
  <div className={classNames(styles.half, styles.form)}>
    <div className={styles.content}>
      <div className={styles.AlertIcon}>
        <WarningIcon
          fontSize="large"
          color="error"
        />
      </div>
      <Typography variant="h1">{I18n.t('foreignBrowserAlert.title')}</Typography>
      <Typography className={styles.message}>
        {I18n.t('foreignBrowserAlert.message')}
      </Typography>
      <Button
        href="https://www.google.com/intl/fr/chrome/"
        color="primary"
        variant="contained"
      >
        {I18n.t('foreignBrowserAlert.button')}
      </Button>
    </div>
  </div>
);

export default ForeignBrowserAlert;
