import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Typography,
  TextField,
  Button
} from '@material-ui/core';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import RefreshIcon from '@material-ui/icons/Refresh';
import HttpsIcon from '@material-ui/icons/Https';
import { InlineButton } from 'components/basics/Buttons';
import Loader from 'components/basics/Loader';
import { download_dynamic } from 'common/config/server';

import I18n from 'assets/I18n';

import './styles.scss';

const formUrl = val => `app.myunisoft/tab/${val}/dashBoard`;

const DynamicLinks = (props) => {
  const { getMember, member_id, loading } = props;
  useEffect(() => {
    getMember();
  }, []);
  const downloadUrl = `${download_dynamic.protocol}://${download_dynamic.baseUrl}/${download_dynamic.path}`;
  return (
    <div className="container">
      {!loading ? (
        <Fragment>
          <div className="headerTitle">
            <Typography variant="h1">{I18n.t('dinamicLink.title')}</Typography>
            <div>
              <InlineButton
                buttons={[
                  {
                    _type: 'icon',
                    variant: 'none',
                    iconColor: 'black',
                    iconName: 'icon-popup',
                    iconSize: 24,
                    titleInfoBulle: I18n.t('tooltips.newWindow'),
                    colorError: true
                  },
                  {
                    _type: 'icon',
                    variant: 'none',
                    iconColor: 'black',
                    iconName: 'icon-close',
                    iconSize: 24,
                    titleInfoBulle: I18n.t('tooltips.close'),
                    colorError: true
                  }
                ]}
              />
            </div>
          </div>
          <div className="content">
            <div className="dinamicId">

              <TextField
                id="standard-basic"
                label={I18n.t('dinamicLink.idTitle')}
                value={member_id}
                size="medium"
                style={{ flex: 1, fontSize: 16 }}
                disabled
              />
              <span>
                <FileCopyIcon color="primary" />
              </span>
            </div>
            <div>
              <div className="downloadLink">
                <p>{I18n.t('dinamicLink.linkToDownload')}</p>
                <span>{I18n.t('dinamicLink.companyWindow')}</span>
              </div>
              <div className="browserLink">
                <ArrowBackIcon />
                <ArrowForwardIcon color="disabled" />
                <RefreshIcon />
                <div className="urlView">
                  <HttpsIcon />
                  <span>{formUrl(member_id)}</span>
                </div>
              </div>
            </div>
            <div className="downloadButton">
              <div>{I18n.t('dinamicLink.insruction')}</div>
              <Button variant="contained" color="primary" href={downloadUrl}>{I18n.t('dinamicLink.downloadExel')}</Button>
            </div>
          </div>
        </Fragment>
      )
        : (
          <div className="loader">
            <Loader size={100} />
          </div>
        )
      }
    </div>
  );
};

DynamicLinks.propTypes = {
  getMember: PropTypes.func.isRequired,
  member_id: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired
};


export default DynamicLinks;
