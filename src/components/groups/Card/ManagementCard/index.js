import React from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import Card from 'components/basics/Card';
import Footer from 'components/groups/Footer';
import UserAvatar from 'components/basics/Avatar/UserAvatar';
import NumberFormat from 'react-number-format';
import './styles.scss';

const ManagementCard = ({ usersInformation, dataType = {} }) => {
  const UserContainer = [];
  usersInformation.forEach((dash) => {
    const card = (
      <div key={`userAvatar${dash.user_id}`}>
        <UserAvatar
          icon_source={I18n.t(`managementCard.avatarFunction.${dash.function}`)}
          {...dash}
        />
      </div>
    );
    UserContainer.push(card);
  });

  return (
    <div>
      <Card card={{ title: I18n.t('managementCard.title'), subtitle: I18n.t('managementCard.subtitle') }}>
        <div className="separator" />
        <div className="avatarContainer">{UserContainer}</div>
        <div className="separator__large" />
        <Footer
          title={dataType.type}
          leftElement={<NumberFormat value={dataType.amount} displayType="text" suffix="€" thousandSeparator=" " />}
          middleElement={`${dataType.bills} ${I18n.t('managementCard.footer.bills')}`}
          rightElement={`${dataType.reminders} ${I18n.t('managementCard.footer.reminders')}`}
        />
      </Card>
    </div>
  );
};

ManagementCard.propTypes = {
  usersInformation: PropTypes.shape({
    user_id: PropTypes.number.isRequired,
    function: PropTypes.string.isRequired
  }).isRequired,
  dataType: PropTypes.shape({
    type: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    bills: PropTypes.string.isRequired,
    reminders: PropTypes.string.isRequired
  }).isRequired
};

export default ManagementCard;
