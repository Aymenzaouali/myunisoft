import React from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import Card from 'components/basics/Card';
import TaskList from 'components/groups/Lists/TaskList';
import './styles.scss';

const TaskCard = ({ headers }) => (
  <div>
    <Card card={{ title: I18n.t('taskCard.title') }}>
      <TaskList
        headers={headers}
        amountStyle={{ color: 'rgb(11, 209, 209)', fontSize: 16 }}
      />
    </Card>
  </div>
);

TaskCard.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.shape({
    type: PropTypes.string.isRequired,
    total: PropTypes.string.isRequired,
    task: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      number: PropTypes.number.isRequired
    }))
  })).isRequired
};

export default TaskCard;
