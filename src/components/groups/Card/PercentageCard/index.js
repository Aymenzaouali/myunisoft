import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import _ from 'lodash';

import { formatPeriod, percentageFormat } from 'helpers/charts';

import Loader from 'components/basics/Loader';
import MultiChart from 'components/basics/Charts/MultiChart';
import Card from 'components/basics/Card';

import styles from './PercentageCard.module.scss';

// Fonctions
const computeData = (data = {}, over = {}) => {
  const { data: numerator = [], ...metadata } = data;
  const { data: denominator = [] } = over;

  return {
    ...metadata,
    total: ((data.total / over.total) * 100) || 0,
    data: percentageFormat(denominator, numerator)
  };
};

// Component
const PercentageCard = (props) => {
  const {
    title = '',
    isLoading,
    data, over
  } = props;

  // Computing
  const no_data = (data.length === 0) || (over.length === 0);

  const currentData = !no_data ? computeData(data[0], over[0]) : [];
  const previousData = !no_data ? computeData(data[1], over[1]) : [];
  const ys = _.concat(currentData.data || [], previousData.data || []).map(e => e.y);

  const roundMax = Math.round(_.max(ys) / 10) * 10;
  const roundMin = Math.round(_.min(ys) / 10) * 10;

  const max = (roundMax <= 100) ? 100 : roundMax + 10;
  const min = (roundMin >= 0) ? 0 : roundMin - 10;

  // add missing ids (if no data ...)
  if (!currentData.id) currentData.id = 'current';
  if (!previousData.id) previousData.id = 'previous';

  return (
    <Card
      card={{
        title,
        amount: <NumberFormat value={parseInt(no_data ? undefined : currentData.total, 10)} displayType="text" suffix="%" thousandSeparator=" " />
      }}
    >
      {isLoading ? (
        <div className={styles.loader}>
          <Loader />
        </div>
      ) : (
        <MultiChart
          data={no_data ? [] : [currentData, previousData]}
          maxY={max}
          minY={min}

          upperBound={<NumberFormat value={max} displayType="text" suffix="%" thousandSeparator=" " />}
          lowerBound={<NumberFormat value={min} displayType="text" suffix="%" thousandSeparator=" " />}

          currentPeriod={formatPeriod(currentData.start_date, currentData.end_date)}
          previousPeriod={formatPeriod(previousData.start_date, previousData.end_date)}
        />
      )}
    </Card>
  );
};

// Props
const dataShape = PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    start_date: PropTypes.string.isRequired,
    end_date: PropTypes.string.isRequired,
    total: PropTypes.number.isRequired,

    data: PropTypes.arrayOf(
      PropTypes.shape({
        x: PropTypes.string,
        y: PropTypes.number,
        z: PropTypes.string
      })
    ).isRequired
  }).isRequired
);

PercentageCard.propTypes = {
  title: PropTypes.string.isRequired,
  data: dataShape.isRequired,
  over: dataShape.isRequired,

  isLoading: PropTypes.bool
};

PercentageCard.defaultProps = {
  isLoading: false
};

export default PercentageCard;
