import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { Typography } from '@material-ui/core';
import Card from 'components/basics/Card';
import MemoList from 'containers/groups/Lists/MemoList';
import MemoDialog from 'containers/groups/Dialogs/MemoCreation';
import _ from 'lodash';
import './styles.scss';

const MemoCard = (props) => {
  const {
    checked, items = [], getMemo, idSociety
  } = props;

  const [open, setOpen] = useState(false);

  useEffect(() => {
    getMemo(idSociety);
  }, []);

  const handleClick = () => setOpen(!open);

  return (
    <div>
      <Card
        iconOnClick={handleClick}
        editIcon
        card={{ title: I18n.t('memoCard.title') }}
      >
        {_.isEmpty(items) ? (
          <Typography
            color="primary"
            variant="h1"
          >
            {I18n.t('memoCard.noMemo')}
          </Typography>
        ) : (
          <div>
            <MemoList memoData={items} checked={checked} />
          </div>
        )}
      </Card>
      <MemoDialog
        isOpen={open}
        onClose={handleClick}
        titleDialog={I18n.t('memoCard.dialogMemoCreation.title')}
        name="memoPost"
      />
    </div>
  );
};

MemoCard.defaultProps = {
  checked: false
};

MemoCard.propTypes = {
  checked: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.shape({
    memo_id: PropTypes.number,
    label: PropTypes.string,
    done: PropTypes.bool
  })).isRequired,
  getMemo: PropTypes.func.isRequired,
  idSociety: PropTypes.number.isRequired
};

export default MemoCard;
