import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import _ from 'lodash';

import { formatPeriod, roundMinMax } from 'helpers/charts';

import Card from 'components/basics/Card';
import MultiChart from 'components/basics/Charts/MultiChart';
import Loader from 'components/basics/Loader';

import styles from './ChartCard.module.scss';

// Component
const ChartCard = (props) => {
  const {
    title,
    isLoading,
    data
  } = props;

  // Rendering
  const currentData = data[0] || {};
  const previousData = data[1] || {};
  const ys = _.concat(currentData.data || [], previousData.data || []).map(e => e.y);

  const [roundMin, roundMax] = roundMinMax(_.min(ys), _.max(ys));

  return (

    <Card
      card={{
        title,
        amount: <NumberFormat value={parseInt(currentData.total, 10)} displayType="text" suffix="€" thousandSeparator=" " />
      }}
    >
      {isLoading ? (
        <div className={styles.loader}>
          <Loader />
        </div>
      ) : (
        <MultiChart
          data={data}
          maxY={roundMax}
          minY={roundMin}

          upperBound={<NumberFormat value={roundMax} displayType="text" suffix="€" thousandSeparator=" " />}
          lowerBound={<NumberFormat value={roundMin} displayType="text" suffix="€" thousandSeparator=" " />}

          currentPeriod={formatPeriod(currentData.start_date, currentData.end_date)}
          previousPeriod={formatPeriod(previousData.start_date, previousData.end_date)}
        />
      )}
    </Card>
  );
};

// Props
ChartCard.propTypes = {
  title: PropTypes.string.isRequired,
  isLoading: PropTypes.bool,

  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      start_date: PropTypes.string.isRequired,
      end_date: PropTypes.string.isRequired,
      total: PropTypes.number.isRequired,

      data: PropTypes.arrayOf(
        PropTypes.shape({
          x: PropTypes.string,
          y: PropTypes.number,
          z: PropTypes.string
        })
      ).isRequired
    }).isRequired
  ).isRequired
};

ChartCard.defaultProps = {
  isLoading: false
};

export default ChartCard;
