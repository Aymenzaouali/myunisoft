import React, { Fragment, useState } from 'react';
import Popover from 'components/basics/Popovers/Buttons';
import PropTypes from 'prop-types';
import TrackingSocietyTable from 'containers/groups/Tables/TrackingSociety';
import {
  rightClickActions
} from 'helpers/dashboard';
import styles from './styles.scss';

const TrackingSociety = (props) => {
  const {
    redirectFromCollab,
    getInfoForInitPeriod,
    getLink
  } = props;

  // State
  const [popover, setpopover] = useState({
    isOpen: false,
    buttons: [],
    left: 0,
    top: 0
  });

  const onRightClick = (e, month, id_diligence, company_id, type, substractMonth = false) => {
    const {
      initFilter,
      initHeader,
      headerName
    } = getInfoForInitPeriod(type, company_id);

    e.preventDefault();
    if (rightClickActions) {
      setpopover({
        isOpen: true,
        left: e.clientX,
        top: e.clientY,
        buttons: rightClickActions(
          month,
          id_diligence,
          company_id,
          redirectFromCollab,
          getLink,
          type,
          substractMonth,
          initFilter,
          initHeader,
          headerName
        )
      });
    }
  };

  const closePopover = () => {
    setpopover({
      ...popover,
      isOpen: false
    });
  };


  return (
    <Fragment>
      <TrackingSocietyTable
        classes={styles.trackingSocietyTableBorder}
        onRightClick={onRightClick}
        {...props}
      />
      <Popover
        onClose={closePopover}
        {...popover}
      />
    </Fragment>
  );
};

TrackingSociety.propTypes = {
  redirectFromCollab: PropTypes.func.isRequired,
  society_id: PropTypes.string.isRequired,
  getInfoForInitPeriod: PropTypes.func.isRequired,
  getLink: PropTypes.func.isRequired
};

export default TrackingSociety;
