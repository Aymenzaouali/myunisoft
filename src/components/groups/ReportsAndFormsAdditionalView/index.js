import React from 'react';
import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import { ReportsAndFormsTableAdditional } from 'containers/groups/Tables';
import PropTypes from 'prop-types';
import styles from './reportsAndFormsScreen.module.scss';

const ReportsAndFormsAdditionalView = ({ closeAdditionalReportsAndForms }) => (
  <div className={styles.container}>
    <div className={styles.btnControlContainer}>
      <InlineButton buttons={
        [
          {
            _type: 'icon',
            variant: 'none',
            iconColor: 'black',
            iconName: 'icon-close',
            iconSize: 24,
            titleInfoBulle: I18n.t('tooltips.close'),
            onClick: closeAdditionalReportsAndForms
          }
        ]}
      />
    </div>
    <ReportsAndFormsTableAdditional />
  </div>
);

ReportsAndFormsAdditionalView.propTypes = {
  closeAdditionalReportsAndForms: PropTypes.string.isRequired
};

export default ReportsAndFormsAdditionalView;
