import React from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { routesByKey } from 'helpers/routes';
import { ConfirmationDialog } from 'components/groups/Dialogs';
import CompanyListTable from 'containers/groups/Tables/CompanyList';
import CompanyListTableFilter from 'containers/groups/Filters/CompanyList';
import CompanyCreation from 'containers/groups/CompanyCreation';
import CompanyItem from 'containers/groups/CompanyItem';

import List from 'containers/groups/List';


class CompanyList extends React.PureComponent {
  renderEditableContent = () => {
    const { selectedCompany } = this.props;
    if (!selectedCompany) {
      return null;
    }
    return (
      <CompanyCreation
        title={I18n.t('companyCreation.editCompanyTitle', { companyName: selectedCompany.name })}
      />
    );
  };

  companyListRender = () => {
    const {
      onAddNewCompany,
      deletionInformation: { mode, id },
      deleteAndClose,
      setDeletionDialog
    } = this.props;
    return (
      <div>
        <List
          actionButtonProps={{
            onClick: () => onAddNewCompany(routesByKey.companyCreation),
            label: I18n.t('companyList.addNewCompany')
          }}
          filters={<CompanyListTableFilter />}
          listTable={<CompanyListTable />}
          editableContent={this.renderEditableContent()}
        />
        <ConfirmationDialog
          isOpen={mode === 'company'}
          title={I18n.t('crm.dialog.title.company')}
          onValidate={() => deleteAndClose(id)}
          onClose={() => setDeletionDialog()}
        />
      </div>
    );
  }

  companyItemRender = () => (
    <List disableExport editableContent={<CompanyItem />} />
  )

  render() {
    const { tabId } = this.props;
    let returnedElement = null;
    if (tabId === -2) {
      returnedElement = this.companyListRender();
    } else {
      returnedElement = this.companyItemRender();
    }
    return returnedElement;
  }
}

CompanyList.propTypes = {
  selectedCompany: PropTypes.shape.isRequired,
  onAddNewCompany: PropTypes.func.isRequired,
  tabId: PropTypes.number.isRequired,
  deletionInformation: PropTypes.object.isRequired,
  deleteAndClose: PropTypes.func.isRequired,
  setDeletionDialog: PropTypes.func.isRequired
};

export default CompanyList;
