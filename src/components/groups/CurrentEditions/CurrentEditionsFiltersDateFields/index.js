import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import I18n from 'i18next';
import moment from 'moment';
import DatePicker from 'components/basics/Inputs/DatePicker';

import styles from './style.module.scss';

const currentDate = moment().format('YYYYMMDD');

function CurrentEditionsFiltersDateFields({
  to,
  from,
  onChange,
  disabled = false
}) {
  return (
    <Fragment>
      <DatePicker
        label={`${I18n.t('currentEditions.filters.from')} :`}
        value={from}
        onChange={v => onChange('from', v.format('YYYY-MM-DD'))}
        className={styles.datepicker}
        disabled={disabled}
      />
      <DatePicker
        label={`${I18n.t('currentEditions.filters.to')} :`}
        value={to}
        onChange={v => onChange('to', v.format('YYYY-MM-DD'))}
        className={styles.datepicker}
        disabled={disabled}
      />
    </Fragment>
  );
}

CurrentEditionsFiltersDateFields.defaultProps = {
  to: currentDate,
  from: moment(moment(currentDate).subtract(1, 'month')).format('YYYYMMDD'),
  disabled: false
};

CurrentEditionsFiltersDateFields.propTypes = {
  to: PropTypes.string,
  from: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool
};

export default CurrentEditionsFiltersDateFields;
