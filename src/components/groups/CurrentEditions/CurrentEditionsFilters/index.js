import React, { Component, Fragment } from 'react';
import { propTypes } from 'redux-form';
import I18n from 'i18next';
import AutoComplete from 'components/basics/Inputs/AutoComplete';

import CurrentEditionsVariations from 'containers/groups/CurrentEditions/CurrentEditionsVariations';
import CurrentEditionsAccountNumberFieldsGroup from '../CurrentEditionsAccountNumberFieldsGroup';
import CurrentEditionsFiltersDateFields from '../CurrentEditionsFiltersDateFields';

import styles from './style.module.scss';

class CurrentEditionsFilters extends Component {
  constructor(props) {
    super(props);

    this.handleOnDateFieldChange = this.handleOnDateFieldChange.bind(this);
    this.handleAutocompleteOnChange = this.handleAutocompleteOnChange.bind(this);
    this.handleCheckboxOnChange = this.handleCheckboxOnChange.bind(this);
    this.handleOnAccountNumberChange = this.handleOnAccountNumberChange.bind(this);
  }

  componentDidMount() {
    const { getExercises } = this.props;
    getExercises();
  }

  handleOnDateFieldChange(field, value) {
    const { change } = this.props;

    change(field, value);
    change('exercices', []);
  }

  handleAutocompleteOnChange(value) {
    const { change, exercises } = this.props;

    change('exercices', value.map(v => v.value));

    if (value.length) {
      const currentExercices = exercises
        .filter(exercice => value.map(v => v.label).includes(exercice.label));
      change('from', currentExercices[0].start_date);
      change('to', currentExercices.pop().end_date);
    }
  }

  handleCheckboxOnChange(e, value) {
    const { change } = this.props;

    change('situationWrites', value);
  }

  handleOnAccountNumberChange(type, value) {
    const { change } = this.props;

    change('accountNumberType', type);
    change('accountNumberValue', value);
  }

  render() {
    const { formValues, exercises, reset } = this.props;

    return (
      <Fragment>
        <div className={styles.currentEditionsFilters}>
          <CurrentEditionsFiltersDateFields
            to={formValues.to}
            from={formValues.from}
            onChange={this.handleOnDateFieldChange}
          />
          <AutoComplete
            textFieldProps={{
              label: I18n.t('currentEditions.filters.exercice'),
              InputLabelProps: {
                shrink: true
              }
            }}
            onChangeValues={this.handleAutocompleteOnChange}
            options={exercises}
            className={styles.currentEditionsFiltersAutocomplete}
            value={(formValues.exercices || []).map(e => exercises.find(v => v.value === e))}
            isMulti
            isClearable
          />
          <CurrentEditionsAccountNumberFieldsGroup
            type={formValues.accountNumberType}
            value={formValues.accountNumberValue}
            onChange={this.handleOnAccountNumberChange}
          />
          <button type="button" className={styles.currentEditionsFiltersResetButton} onClick={reset}>
            {I18n.t('currentEditions.reset')}
          </button>
        </div>
        <CurrentEditionsVariations />
      </Fragment>
    );
  }
}

CurrentEditionsFilters.propTypes = {
  ...propTypes
};

export default CurrentEditionsFilters;
