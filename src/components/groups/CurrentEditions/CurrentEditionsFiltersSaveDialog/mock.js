export default [
  {
    id: '112',
    name: 'Save#1',
    accountNumberType: 'all',
    accountNumberValue: '',
    from: '2006-08-22',
    to: '2017-05-19'
  },
  {
    id: '114',
    name: 'Save#2',
    accountNumberType: 'custom',
    accountNumberValue: '1-13',
    from: '2006-08-22',
    to: '2017-05-19'
  },
  {
    id: '117',
    name: 'Save#3',
    accountNumberType: 'all',
    accountNumberValue: '',
    from: '2006-08-22',
    to: '2017-05-19'
  }
];
