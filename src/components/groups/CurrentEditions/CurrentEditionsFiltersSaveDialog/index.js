import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, propTypes as reduxFormPropTypes } from 'redux-form';
import {
  Dialog, DialogContent, FormGroup, FormControlLabel,
  Radio, RadioGroup, FormControl, InputLabel,
  Select, DialogActions, withStyles
} from '@material-ui/core';
import I18n from 'i18next';

import Button from 'components/basics/Buttons/Button';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';

import saves from './mock';

const style = {
  dialogPaper: {
    minWidth: '600px'
  },
  dialogContentRoot: {
    padding: '0 24px 24px 115px'
  },
  dialogActionsRoot: {
    justifyContent: 'center'
  },
  saveFormGroup: {
    paddingLeft: '35px'
  }
};

class CurrentEditionsFiltersSaveDialog extends Component {
  handleOnActionButtonClick = type => () => {
    const { onClose, reset, formValues: { mode, saveId } } = this.props;

    reset();
    onClose(type === 'validate' && mode === 'get' && saves.find(s => s.id === saveId));
  };

  render() {
    const {
      formValues,
      change,
      classes
    } = this.props;

    return (
      <Dialog open classes={{ paper: classes.dialogPaper }}>
        <DialogContent classes={{ root: classes.dialogContentRoot }}>
          <RadioGroup name="mode" value={formValues.mode} onChange={(e, v) => change('mode', v)}>
            <FormControlLabel
              control={<Radio color="primary" />}
              label={I18n.t('currentEditions.filters.dialog.save')}
              value="save"
            />
            {formValues.mode === 'save' && (
              <FormGroup classes={{ root: classes.saveFormGroup }}>
                <Field
                  name="accountNumberValue"
                  component={ReduxTextField}
                  label={I18n.t('currentEditions.filters.accountNumber')}
                  placeholder={I18n.t('currentEditions.filters.checkboxes.accountNumberExample')}
                  margin="dense"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
                <Field
                  name="saveName"
                  component={ReduxTextField}
                  label={I18n.t('currentEditions.filters.dialog.saveName')}
                  margin="dense"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </FormGroup>
            )}
            <FormControlLabel
              control={<Radio color="primary" />}
              label={I18n.t('currentEditions.filters.dialog.get')}
              value="get"
            />
            {formValues.mode === 'get' && (
              <FormControl>
                <InputLabel htmlFor="save-id">{I18n.t('currentEditions.filters.dialog.selectSavesPlaceholder')}</InputLabel>
                <Select
                  native
                  value={formValues.saveId}
                  onChange={e => change('saveId', e.target.value)}
                  inputProps={{
                    name: 'saveId',
                    id: 'save-id'
                  }}
                >
                  <option value="" />
                  {saves.map(({ id, name }) => (<option key={id} value={id}>{name}</option>))}
                </Select>
              </FormControl>
            )}
          </RadioGroup>
        </DialogContent>
        <DialogActions classes={{ root: classes.dialogActionsRoot }}>
          <Button size="large" variant="contained" onClick={this.handleOnActionButtonClick('cancel')}>
            {I18n.t('currentEditions.filters.dialog.cancel')}
          </Button>
          <Button size="large" variant="contained" color="primary" onClick={this.handleOnActionButtonClick('validate')}>
            {I18n.t(`currentEditions.filters.dialog.${formValues.mode}Validate`)}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

CurrentEditionsFiltersSaveDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  formValues: PropTypes.shape({
    mode: PropTypes.string,
    saveName: PropTypes.string,
    accountNumberValue: PropTypes.string,
    saveId: PropTypes.string
  }).isRequired,
  ...reduxFormPropTypes
};

export default withStyles(style)(CurrentEditionsFiltersSaveDialog);
