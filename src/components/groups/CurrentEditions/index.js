import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Collapse } from '@material-ui/core';

import CurrentEditionsFilters from 'containers/groups/CurrentEditions/CurrentEditionsFilters';
import CurrentEditionsHeader from 'containers/groups/CurrentEditions/CurrentEditionsHeader';
import CurrentEditionsTable from 'containers/groups/CurrentEditions/CurrentEditionsTable';
import CurrentEditionsAccountFilter from 'containers/groups/CurrentEditions/CurrentEditionsAccountFilter';

import SigTable from 'containers/groups/CurrentEditions/SigTable';

import Loader from 'components/basics/Loader';
import SplitContext from 'context/SplitContext';
import { Shortcuts } from 'react-shortcuts';
import styles from './style.module.scss';

/**
 * Current editions component
 */
class CurrentEditions extends React.PureComponent {
  static contextType = SplitContext;

  state = {
    isFiltersVisible: false
  };

  componentDidMount() {
    const { getExercice, societyId } = this.props;
    getExercice(societyId);
  }

  handleShortcuts = (action) => {
    const { openPopupSplit } = this.props;
    const { onActivateSplit, slaves, onCloseSplit } = this.context;
    switch (action) {
    case 'SPLIT_ACCOUNT':
      if (slaves.length) onActivateSplit(slaves[0].id, slaves[0].args);
      openPopupSplit('current_editions');
      onCloseSplit();
      break;
    default:
      break;
    }
  };

  /**
   * Handle filters button from CurrentEditionHeader, and toggle CurrentEditionFilters
   */
  handleFiltersButtonOnClick = () => {
    this.setState(({ isFiltersVisible }) => ({ isFiltersVisible: !isFiltersVisible }));
  };

  render() {
    const { isFiltersVisible } = this.state;
    const { currentOption, isLoading } = this.props;

    return (
      <Shortcuts
        name="BALANCE"
        handler={this.handleShortcuts}
        alwaysFireHandler
        stopPropagation={false}
        preventDefault={false}
        targetNodeSelector="body"
      >
        <div className={styles.currentEditions}>
          <CurrentEditionsHeader
            onFiltersButtonClick={this.handleFiltersButtonOnClick}
            isFiltersVisible={isFiltersVisible}
          />
          { (isLoading && currentOption !== 'balance') && <Loader /> }
          <Collapse in={isFiltersVisible}>
            <CurrentEditionsFilters />
          </Collapse>
          {
            currentOption === 'balance'
            && (
              <Fragment>
                <CurrentEditionsAccountFilter />
                <CurrentEditionsTable />
              </Fragment>
            )
          }
          {
            currentOption === 'sig'
            && (
              <SigTable />
            )
          }
        </div>
      </Shortcuts>
    );
  }
}

CurrentEditions.propTypes = {
  openPopupSplit: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  currentOption: PropTypes.string.isRequired,
  societyId: PropTypes.number.isRequired,
  getExercice: PropTypes.func.isRequired
};

export default CurrentEditions;
