import React from 'react';
import PropTypes from 'prop-types';
import { Field, propTypes as reduxFormPropTypes } from 'redux-form';
import I18n from 'i18next';
import {
  Dialog, DialogTitle, DialogContent,
  DialogActions, Slide, FormGroup,
  Typography
} from '@material-ui/core';

import Button from 'components/basics/Buttons/Button';
import { ReduxRadio } from 'components/reduxForm/Selections';
import ReduxCheckbox from 'components/reduxForm/Selections/ReduxCheckBox';

import WatermarkSelect from 'containers/reduxForm/Selects/WatermarkSelect';
import styles from './CurrentEditionsTableToolsDialog.module.scss';

// Utils
function Transition(props) {
  return <Slide direction="up" {...props} />;
}

// Component
const CurrentEditionsTableToolsDialog = (props) => {
  const {
    isOpen,
    type,
    error,
    onClose,
    reset,
    handleSubmit, customSubmit
  } = props;

  // Functions
  const handleClose = () => {
    reset();
    onClose();
  };

  const onSubmit = async () => {
    await handleSubmit(() => customSubmit(type))();
    await reset();
    await onClose();
  };

  // Rendering
  return (
    <Dialog
      className={styles.container}
      classes={{ paper: styles.paper }}
      TransitionComponent={Transition}
      open={isOpen}
      onClose={onClose}
      keepMounted
    >
      <DialogTitle className={styles.title}>
        {I18n.t(`currentEditions.table.tools.dialog.${type}Title`)}
      </DialogTitle>
      <DialogContent classes={{ root: styles.root }}>
        <FormGroup>
          <div className={styles.group}>
            <Field
              name="onlyUnresolvedAccount"
              component={ReduxCheckbox}
              label={I18n.t('currentEditions.table.tools.dialog.form.onlyUnresolvedAccount')}
              className={styles.checkbox}
              color="primary"
            />
          </div>
          {type === 'export' && (
            <div className={styles.group}>
              <Typography variant="h6">
                {I18n.t('currentEditions.table.tools.dialog.form.exportExtension')}
              </Typography>
              <Field
                name="exportExtension"
                component={ReduxRadio}
                list={['pdf'].map(value => ({ value, label: `.${value}` }))}
                color="primary"
                RadioProps={{
                  className: styles.radio
                }}
                row
              />
            </div>
          )}
          <Field
            name="watermark"
            component={WatermarkSelect}
            label={I18n.t('currentEditions.table.tools.dialog.form.watermark')}
            placeholder
          />
        </FormGroup>
      </DialogContent>
      {error && (
        <div className={styles.errorContainer}>
          <Typography variant="h6" color="error">{error}</Typography>
        </div>
      )}
      <DialogActions className={styles.actions}>
        <Button size="large" variant="contained" onClick={handleClose}>
          { I18n.t('currentEditions.table.tools.dialog.cancel') }
        </Button>
        <Button size="large" variant="contained" color="primary" onClick={onSubmit}>
          { I18n.t(`currentEditions.table.tools.${type}`) }
        </Button>
      </DialogActions>
    </Dialog>
  );
};

// Props
CurrentEditionsTableToolsDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  customSubmit: PropTypes.func.isRequired,
  ...reduxFormPropTypes
};

export default CurrentEditionsTableToolsDialog;
