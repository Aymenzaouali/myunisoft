import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, propTypes as reduxFormPropTypes } from 'redux-form';
import {
  FormGroup,
  InputAdornment,
  withStyles
} from '@material-ui/core';
import I18n from 'i18next';
// import { formatNumber } from 'helpers/number';
import ReduxCheckbox from 'components/reduxForm/Selections/ReduxCheckBox';
import {
  ReduxTextField
} from 'components/reduxForm/Inputs';
import { ReduxRadio } from 'components/reduxForm/Selections';

const styles = {
  checkboxRoot: {
    marginLeft: 0
  },
  inputRoot: {
    maxWidth: '48px'
  }
};

class CurrentEditionsVariations extends Component {
  get variationsList() {
    return [
      {
        value: 'euroAmount',
        label: (
          <Field
            margin="none"
            component={ReduxTextField}
            onCustomChange={this.onEuroValueChange}
            name="euroAmount"
            // format={value => (value ? formatNumber(value) : '')}
            label={I18n.t('currentEditions.variations.placeholder.eur')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">€</InputAdornment>
              )
            }}
          />
        )
      },
      {
        value: 'percentageAmount',
        label: (
          <Field
            margin="none"
            type="number"
            component={ReduxTextField}
            onCustomChange={this.onPercentageValueChange}
            normalize={this.normalizePercentage}
            name="percentageAmount"
            placeholder={I18n.t('currentEditions.variations.placeholder.percent')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">%</InputAdornment>
              )
            }}
          />
        )
      }
    ];
  }

  normalizePercentage = (value) => {
    if (!value) {
      return '';
    }

    return Math.min(100, +value);
  };

  onEuroValueChange = (value) => {
    console.log('zzz euro', value);  //eslint-disable-line
  }

  onPercentageValueChange = (value) => {
    console.log('zzz percent', value); //eslint-disable-line
  }

  render() {
    const {
      classes,
      formValues
    } = this.props;

    const {
      // percentageAmount,
      // variations,
      // euroAmount,
      displayVariations
    } = formValues;

    return (
      <FormGroup row>
        <Field
          name="displayVariations"
          component={ReduxCheckbox}
          label={I18n.t('currentEditions.variations.display')}
          labelPlacement="end"
          FormControlLabelProps={{
            classes: { root: classes.checkboxRoot }
          }}
          color="primary"
        />
        {displayVariations && (
          <Field
            color="primary"
            name="variations"
            component={ReduxRadio}
            list={this.variationsList}
            className="new-user__form-radios"
            row
          />
        )}
      </FormGroup>
    );
  }
}

CurrentEditionsVariations.propTypes = {
  formValues: PropTypes.shape({
    displayVariations: PropTypes.bool.isRequired,
    variations: PropTypes.oneOf(['percentageAmount', 'euroAmount', '']),
    euroAmount: PropTypes.string,
    percentageAmount: PropTypes.string
  }).isRequired,
  ...reduxFormPropTypes
};

export default withStyles(styles)(CurrentEditionsVariations);
