import React, { Fragment, useContext, useEffect } from 'react';
import I18n from 'i18next';
import { Field, propTypes as reduxFormPropTypes } from 'redux-form';
import { Typography, withStyles } from '@material-ui/core';

import SplitContext from 'context/SplitContext';

import { Periode } from 'containers/reduxForm/Inputs';

import Button from 'components/basics/Buttons/Button';
import { InlineButton } from 'components/basics/Buttons';
import { FontIcon } from 'components/basics/Icon';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { ReduxSelect, ReduxRadio, ReduxSwitch } from 'components/reduxForm/Selections';
import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';
import _ from 'lodash';

import styles from './CurrentEditionsHeader.module.scss';

const ownStyles = {
  iconContainer: {
    marginLeft: 22
  }
};

const CurrentEditionsHeader = (props) => {
  const {
    // Not for V0
    // onFiltersButtonClick,
    // isFiltersVisible,
    // change,
    selectOption,
    saveOrPrintLedger,
    currentOption,
    classes,
    getBalance,
    initializeHeader,
    dateStart,
    dateEnd,
    exercice_N,
    selectionType
  } = props;

  const split = useContext(SplitContext);

  const type = [
    {
      value: 'AllAccounts',
      label: I18n.t('currentEditions.table.filters.allAccounts')
    },
    {
      value: 'PartialAccounts',
      label: (
        <Field
          disabled={selectionType === 'AllAccounts'}
          name="filter"
          component={ReduxTextField}
          type="text"
          label={I18n.t('currentEditions.table.filters.partialAccountHolder')}
          margin="none"
        />
      )
    }
  ];

  const currentOptions = [
    {
      id: 0,
      value: 'balance',
      label: I18n.t('currentEditions.header.selectOptions.balance')
    },
    {
      id: 2,
      value: 'sig',
      label: 'SIG'
    },
    {
      id: 1,
      value: 'ledger',
      label: I18n.t('currentEditions.header.selectOptions.ledger')
    }
  ];

  useEffect(() => {
    initializeHeader(dateStart, dateEnd, exercice_N);
  },
  [dateStart, dateEnd]);

  const typeOptions = [
    'general',
    'customer',
    /*
    'supplies',
    'salaried',
    'analytic',
    */
    'supplier'
  ].map(item => ({
    label: I18n.t(`currentEditions.header.selectOptions.${item}`),
    value: item
  }));

  return (
    <div className={styles.container}>
      <div className={styles.selectFieldContainer}>
        <div className={styles.selectorContainer}>
          <Typography variant="h1">{I18n.t('currentEditions.header.title')}</Typography>
          <Field
            fullWidth
            name="current"
            component={ReduxSelect}
            list={currentOptions}
            margin="none"
            onChange={(e) => {
              if (_.get(e, 'target.value') === 'ledger' && split.active) {
                split.closeSlave();
              }
              selectOption(e.target.value);
            }}
            className={styles.field}
          />
        </div>
        {['balance', 'diary'].includes(currentOption) && (
          <Fragment>
            <Field
              fullWidth
              name="type"
              component={ReduxSelect}
              list={typeOptions}
              margin="none"
              onChange={value => getBalance(value)}
              className={styles.field}
            />
            <SplitToolbar className={styles.splitToolbar} />
          </Fragment>
        )}
      </div>
      {['ledger'].includes(currentOption) && (
        <div className={styles.dateFieldContainer}>
          <div className={styles.newContainer}>
            <div className={styles.periodContainer}>
              <Field
                displayExercice
                isMulti={false}
                component={Periode}
                separator
                row
                name="Date"
                formName="currentEditionsHeaderForm"
                exerciceField={styles.exercice}
              />
              <Field
                color="primary"
                name="selectionType"
                component={ReduxRadio}
                label={I18n.t('currentEditions.table.filters.accountNumber')}
                list={type}
                row
              />
              <Button
                color="primary"
                variant="contained"
                className={classes.iconContainer}
                onClick={() => saveOrPrintLedger('save')}
              >
                <FontIcon color="white" size={25} className="icon-download" />
              </Button>
            </div>
            <div className={styles.newRow}>
              <Field
                component={ReduxSwitch}
                label="P.J intégrées"
                name="attachment"
              />
              <Field
                component={ReduxSwitch}
                label="Commentaires sur les comptes"
                name="comment_account"
              />
              <Field
                component={ReduxSwitch}
                label="Commentaires sur les lignes"
                name="comment_entry"
              />
              <Field
                component={ReduxSwitch}
                label="Ecritures non lettrées uniquement"
                name="not_lettered_only"
              />
              <Field
                component={ReduxSwitch}
                label="Comptes non soldées uniquement"
                name="not_solde_only"
              />
            </div>
          </div>
          <div className={styles.inlineButtonsContainer}>
            <InlineButton
              buttons={[
                {
                  _type: 'string',
                  text: 'Exports',
                  size: 'large',
                  disabled: false,
                  colorError: true
                },
                {
                  _type: 'icon',
                  iconName: 'icon-print',
                  onClick: () => saveOrPrintLedger('print'),
                  disabled: false
                }
              ]}
            />
          </div>
        </div>
      )}

      {/* Not For V0 */}
      {/* <Button
        size="large"
        variant={isFiltersVisible ? 'contained' : 'outlined'}
        color={isFiltersVisible ? 'primary' : 'default'}
        onClick={onFiltersButtonClick}
        className={styles.button}
      >
        {I18n.t('currentEditions.header.filtersButton')}
      </Button> */}
    </div>
  );
};

CurrentEditionsHeader.propTypes = {
  // onFiltersButtonClick: PropTypes.func.isRequired,
  // isFiltersVisible: PropTypes.bool.isRequired,

  ...reduxFormPropTypes
};

export default withStyles(ownStyles)(CurrentEditionsHeader);
