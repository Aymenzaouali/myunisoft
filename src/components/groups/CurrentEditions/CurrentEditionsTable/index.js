import { compose } from 'redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import I18n from 'i18next';
import { withStyles } from '@material-ui/core';
import moment from 'moment';
import _ from 'lodash';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';
import { formatNumberIfNotNull, formatNumberIfNotNullAndNoDecimals, formatPercentage } from 'helpers/number';
import { splitWrap, splitShape } from 'context/SplitContext';

import CurrentEditionsTableFilters from 'containers/groups/CurrentEditions/CurrentEditionsTableFilters';

import { CommentBox } from 'components/groups/Comments';
import Button from 'components/basics/Buttons/Button';
import CurrentEditionsTableTools from 'components/groups/CurrentEditions/CurrentEditionsTableTools';
import TableFactory from 'components/groups/Tables/Factory';

import { openConsultingPopup } from 'helpers/consulting';
import styles from './style.module.scss';

// Constantes
// const conditionalColumns = {
//   revision: [
//     { field: 'comment', _type: 'string' }
//     // NOT FOR V.1
//     // { field: 'valid_rm', _type: 'checkbox' },
//     // { field: 'valid_collab', _type: 'checkbox' }
//   ],
//   balance: exercices => exercices.map(e => ({
//     field: `solde_${e}`,
//     _type: 'string'
//   }))
// };

// Style
const MuiButtonBaseStyle = ({
  root: {
    padding: 0,
    minWidth: 14,
    minHeight: 14,
    boxShadow: 'none'
  },
  label: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

// Component
class CurrentEditionsTable extends Component {
  constructor(props) {
    super(props);

    // Init state
    this.state = {
      openedParentIds: new Set(),
      isAllChildsVisible: false
    };
  }

  // Méthodes :
  // - lifecycle

  componentDidMount() {
    const {
      getBalanceData, start_date, end_date, selectedExercice: { exercice }, getReviews
    } = this.props;
    if ((start_date !== undefined) && (end_date !== undefined) && _.get(exercice, 'id_exercice')) {
      getBalanceData(undefined, start_date, end_date, _.get(exercice, 'id_exercice'));
      getReviews();
    }
  }

  componentDidUpdate(prevProps) {
    const {
      getBalanceData, start_date, end_date, selectedExercice, getReviews
    } = this.props;
    const $tr = document.getElementsByTagName('tr');
    if ((start_date && end_date && _.get(selectedExercice, 'exercice.id_exercice'))
      && (prevProps.start_date !== start_date
        || prevProps.end_date !== end_date
        || prevProps.selectedExercice.exercice !== selectedExercice.exercice)) {
      getBalanceData(undefined, start_date, end_date, _.get(selectedExercice, 'exercice.id_exercice'));
      getReviews();
    }

    if ($tr.length) {
      $tr[$tr.length - 2].childNodes
        .forEach(e => e.classList.add(styles.noBorderCell));
    }
  }

  // if comment exists, then we can call this function to open the comment box and get the comments
  getAndOpenComments = async (data) => {
    const {
      openCommentsDialog,
      getComments,
      currentRevision,
      postNewComment,
      editComment
    } = this.props;
    openCommentsDialog({
      body: {
        Component: CommentBox,
        props: {
          data,
          isNotEditor: data.hasComment,
          createMode: false,
          onSubmit: payload => postNewComment(payload, data),
          onEdit: payload => editComment(payload, data),
          getComments: () => getComments(data.id_account, _.get(currentRevision, 'id_dossier_revision'))
        }
      }
    });
  };

  // - utils
  findChildById = (data, id) => data.reduce((p, c) => {
    if (!p && c.account_number === id) return c;
    if (!p && c.account_List) return this.findChildById(c.account_List, id);
    return p;
  }, null);

  applyToChilds = (data, fn) => data.forEach((item) => {
    fn(item);
    if (item.account_List) this.applyToChilds(item.account_List, fn);
  });

  getLevelIndentSizeInPx = level => 1 + level * 20;

  // - events
  handleOnToggleChildsButtonClick = id => this.setState(({ openedParentIds }) => {
    const { balanceData } = this.props;

    if (openedParentIds.has(id)) {
      this.applyToChilds(
        [this.findChildById(balanceData.data, id)],
        item => openedParentIds.delete(item.account_number)
      );
    } else openedParentIds.add(id);

    return ({ openedParentIds, isAllChildsVisible: false });
  });

  handleOnToggleAllChildsButtonClick = () => {
    const { balanceData } = this.props;
    this.setState(({ openedParentIds, isAllChildsVisible }) => {
      if (isAllChildsVisible) openedParentIds.clear();
      else this.applyToChilds(balanceData.data, item => openedParentIds.add(item.account_number));

      return ({
        openedParentIds,
        isAllChildsVisible: !isAllChildsVisible
      });
    });
  };

  // - table
  generateTableFactoryHeaderCellsParam = columns => columns.map(({ field }) => ({
    _type: 'string',
    keyCell: `current-editions-table-header-cell-${field}`,
    cellProp: {
      className: cellStyles.stickyHeader
    },
    props: {
      label: I18n.t(`currentEditions.table.header.${field}`),
      noWrap: true
      // colorError: field === 'valid_rm' || field === 'valid_collab' // NOT FOR V.1
    }
  }));

  generateTableFactoryHeaderRowsParam = () => {
    const {
      tableFiltersValues,
      filtersValues,
      classes,
      balanceData
    } = this.props;
    const { isAllChildsVisible } = this.state;

    return [{
      keyRow: 'current-editions-table-header-row',
      value: [
        // Bouton affichage tous les enfants
        {
          keyCell: 'current-editions-table-header-cell-toggle',
          _type: 'string',
          props: {
            label: (
              <Button
                classes={classes}
                size="small"
                color="primary"
                variant="contained"
                onClick={this.handleOnToggleAllChildsButtonClick}
              >
                { isAllChildsVisible ? '-' : '+' }
              </Button>
            )
          },
          cellProp: {
            className: cellStyles.stickyHeader
          }
        },

        // Numéro de compte
        {
          keyCell: 'current-editions-table-header-cell-account-number',
          _type: 'string',
          props: {
            label: I18n.t('currentEditions.table.header.accountNumber'),
            noWrap: true
          },
          cellProp: {
            className: cellStyles.stickyHeader
          }
        },

        // Label
        {
          keyCell: 'current-editions-table-header-cell-label',
          _type: 'string',
          props: {
            label: I18n.t('currentEditions.table.header.label'),
            noWrap: true
          },
          cellProp: {
            className: cellStyles.stickyHeader
          }
        },

        // Révisions
        // ...(!tableFiltersValues.isRevisionsHidden
        //   ? this.generateTableFactoryHeaderCellsParam(conditionalColumns.revision)
        //   : []
        // ),

        ...(!tableFiltersValues.isRevisionsHidden ? [
          {
            keyCell: 'current-editions-table-header-cell-comment',
            _type: 'string',
            props: {
              label: I18n.t('currentEditions.table.header.comment'),
              noWrap: true
            },
            cellProp: {
              className: cellStyles.stickyHeader
            }
          },
          {
            keyCell: 'current-editions-table-header-cell-valid_rm',
            _type: 'string',
            props: {
              label: I18n.t('currentEditions.table.header.valid_rm'),
              noWrap: true
            },
            cellProp: {
              className: cellStyles.stickyHeader
            }
          },
          {
            keyCell: 'current-editions-table-header-cell-valid_collab',
            _type: 'string',
            props: {
              label: I18n.t('currentEditions.table.header.valid_collab'),
              noWrap: true
            },
            cellProp: {
              className: cellStyles.stickyHeader
            }
          }
        ] : []),

        // Soldes exercices
        ...filtersValues.exercices.map(exercice => ({
          keyCell: `current-editions-table-header-cell-balance-${exercice}`,
          _type: 'string',
          cellProp: { className: classNames(styles.alignRight, cellStyles.stickyHeader) },
          props: {
            label: `${I18n.t('currentEditions.table.header.balance')} ${exercice}`,
            noWrap: true
          }
        })),

        {
          keyCell: 'current-editions-table-header-cell-relative-variation',
          _type: 'string',
          props: {
            label: I18n.t('currentEditions.table.header.relativeVariation'),
            noWrap: true
          },
          cellProp: { className: classNames(styles.alignRight, cellStyles.stickyHeader) }
        },

        {
          keyCell: 'current-editions-table-header-cell-absolute-variation',
          _type: 'string',
          props: {
            label: I18n.t('currentEditions.table.header.absoluteVariation'),
            noWrap: true
          },
          cellProp: { className: classNames(styles.alignRight, cellStyles.stickyHeader) }
        },

        // Soldes par mois
        ...(tableFiltersValues.isAllMonthVisible && tableFiltersValues.displayType.length
          ? balanceData.month_list.map(month => ({
            keyCell: `current-editions-table-header-cell-balance-${month}`,
            _type: 'string',
            cellProp: { className: classNames(styles.alignRight, cellStyles.stickyHeader) },
            props: {
              label: moment(month.replace('/', '-')).format('MMM YYYY'),
              noWrap: true
            }
          })) : [])
      ]
    }];
  };

  generateTableFactoryBodyCellsParam = (columns, data) => columns.map(
    ({ field, _type }) => {
      const keyCell = `current-editions-table-body-cell-${field}-${data.account_number}`;
      if (_type === 'checkbox') {
        return {
          _type,
          keyCell,
          props: {
            checked: data[field],
            customColor: 'colorError'
          }
        };
      }

      return {
        keyCell,
        _type: 'string',
        props: { label: data[field] }
      };
    }
  );

  handleOpenConsultingPopup = (e, params) => {
    e.stopPropagation();
    const { societyId } = this.props;
    openConsultingPopup(societyId, params);
  }

  generateTableFactoryBodyRowParam = (data, level, index) => {
    const {
      tableFiltersValues,
      filtersValues,
      classes,
      split,
      balanceData,
      selectedExercice
    } = this.props;

    const {
      isRevisionsHidden,
      isAllMonthVisible,
      displayType
    } = tableFiltersValues;
    const { openedParentIds } = this.state;

    const selectedAccount = _.get(split, 'args.account.account_id');
    return ({
      keyRow: `current-editions-table-body-row-${data.account_number}`,
      props: {
        hover: true,
        onDoubleClick: () => this.handleOnToggleChildsButtonClick(data.account_number),
        onClick: () => {
          if (split.master && data.id_account !== 0) {
            split.updateArgsSplit({
              account: {
                account_id: data.id_account,
                account_number: data.account_number,
                label: data.label
              }
            });
          }
        },
        className: (selectedAccount === data.id_account) ? styles.splitSelected : undefined,
        selected: index % 2 !== 0
      },
      value: [
        // Bouton affichage enfants
        {
          keyCell: `current-editions-table-body-cell-toggle-${data.account_number}`,
          _type: 'string',
          cellProp: {
            style: { width: '20px' }
          },
          props: {
            label: data.account_List.length ? (
              <Button
                size="small"
                color="primary"
                variant="contained"
                onClick={() => this.handleOnToggleChildsButtonClick(data.account_number)}
                classes={classes}
                style={{
                  position: 'relative',
                  left: this.getLevelIndentSizeInPx(level)
                }}
              >
                {openedParentIds.has(data.account_number) ? '-' : '+'}
              </Button>
            ) : ''
          }
        },

        // Numéro de compte
        {
          keyCell: `current-editions-table-body-cell-account-number-${data.account_number}`,
          _type: 'string',
          cellProp: {
            className: !data.account_List.length && cellStyles.hasLink,
            style: {
              paddingLeft: this.getLevelIndentSizeInPx(level)
            },
            onClick: !data.account_List.length
              ? e => this.handleOpenConsultingPopup(e, {
                accountId: data.id_account,
                exerciceLabel: selectedExercice.exercice.label
              })
              : null
          },
          props: { label: data.account_number }
        },

        // Label
        {
          keyCell: `current-editions-table-body-cell-name-${data.account_number}`,
          _type: 'string',
          props: { label: data.label }
        },

        // Révisions
        // ...(!isRevisionsHidden
        //   ? this.generateTableFactoryBodyCellsParam(conditionalColumns.revision, data)
        //   : []
        // ),

        ...(!isRevisionsHidden ? [
          {
            keyCell: `current-editions-table-body-cell-name-${data.comment}`,
            _type: data.account_List.length === 0 ? 'icon' : 'string',
            props: data.account_List.length === 0 ? {
              name: data.hasComment ? 'icon-comments' : 'icon-pencil',
              onClick: (e) => {
                e.stopPropagation();
                this.getAndOpenComments(data);
              }
            } : {
              label: null
            }
          },
          {
            keyCell: `current-editions-table-body-cell-name-valid_rm-${index}`,
            _type: 'checkbox',
            props: {
              color: 'primary',
              checked: data.valid_rm !== 0,
              disabled: true
            }
          },
          {
            keyCell: `current-editions-table-body-cell-name-valid_collab-${index}`,
            _type: 'checkbox',
            props: {
              color: 'primary',
              checked: data.valid_collab !== 0,
              disabled: true
            }
          }
        ] : []),

        // Soldes exercices
        ...filtersValues.exercices.map((exercice, i) => {
          const amount = formatNumberIfNotNull(data.exercice_values[i]);

          return {
            keyCell: `current-editions-table-body-cell-${exercice}-${data.account_number}`,
            _type: 'string',
            cellProp: {
              className: classNames(
                styles.alignRight,
                styles.valueCell,
                !data.account_List.length && cellStyles.hasLink
              ),
              onClick: amount && !data.account_List.length
                ? e => this.handleOpenConsultingPopup(e, {
                  accountId: data.id_account,
                  /*
                    TODO can't use exerciceId, using a label is not a good practice
                    and should be used everywhere using a constant
                    Need backEnd evaluation
                    */
                  exerciceLabel: i === 0 ? 'N' : 'N-1'
                })
                : null
            },
            props: {
              label: amount,
              noWrap: true
            }
          };
        }),

        {
          keyCell: `current-editions-table-body-cell-relative-variation-${index}`,
          _type: 'string',
          cellProp: { className: classNames(styles.alignRight) },
          props: { label: formatPercentage(data.varPercent) }
        },

        {
          keyCell: `current-editions-table-body-cell-absolute-variation-${index}`,
          _type: 'string',
          cellProp: { className: classNames(styles.alignRight) },
          props: { label: formatNumberIfNotNullAndNoDecimals(data.varValue) }
        },

        // Soldes par mois ou cumul
        ...(isAllMonthVisible && displayType.length
          ? data.month_values.map((v, i) => {
            const amount = formatNumberIfNotNullAndNoDecimals(v);
            return {
              keyCell: `current-editions-table-body-cell-month${i}-${data.account_number}`,
              _type: 'string',
              cellProp: {
                className: classNames(
                  styles.alignRight,
                  styles.valueCell,
                  amount && !data.account_List.length && cellStyles.hasLink
                ),
                onClick: (!data.account_List.length && amount !== '')
                  ? e => this.handleOpenConsultingPopup(e, {
                    accountId: data.id_account,
                    /*
                        TODO this kind of transformation should be modified everywhere
                         with an helper str to moment format as YYYYMMDD
                       */
                    dateStart: moment(balanceData.month_list[displayType ? 0 : i].replace('/', '-')).format('YYYYMMDD'),
                    dateEnd: moment(balanceData.month_list[i].replace('/', '-')).endOf('month').format('YYYYMMDD')
                  }) : null
              },
              props: {
                label: amount,
                noWrap: true
              }
            };
          })
          : [])
      ]
    });
  };

  generateTableFactoryBodyRowsParam = (data, level) => {
    const { openedParentIds } = this.state;
    const rows = [];
    const filteredData = data.filter((acc) => {
      const soldeN = acc.exercice_values[0];
      const soldeN1 = acc.exercice_values[1];
      return (
        ((soldeN !== 0) && (soldeN !== null))
      || ((soldeN1 !== 0) && (soldeN1 !== null))
      );
    });

    filteredData.forEach((curr, index) => {
      rows.push(this.generateTableFactoryBodyRowParam(curr, level, index));

      if (curr.account_List && openedParentIds.has(curr.account_number)) {
        rows.push(
          ...this.generateTableFactoryBodyRowsParam(curr.account_List, level + 1)
        );
      }
    });

    return rows;
  };

  generateTableFactoryResultRow = ({ exercice_values, month_values }) => {
    const { filtersValues, tableFiltersValues } = this.props;
    const { isAllMonthVisible, displayType } = tableFiltersValues;

    return ({
      keyRow: 'current-editions-table-result-row',
      value: [
        ...Array.from({ length: tableFiltersValues.isRevisionsHidden ? 2 : 5 }, (_, i) => ({
          keyCell: `current-editions-table-result-cell-empty${i}`,
          _type: 'string',
          cellProp: {
            className: styles.noBorderCell
          },
          props: { label: null }
        })),
        ({
          keyCell: 'current-editions-table-result-cell-label',
          _type: 'string',
          cellProp: {
            className: styles.resultLabelCell
          },
          props: { label: 'résultat' }
        }),
        ...filtersValues.exercices.map((exercice, i) => ({
          keyCell: `current-editions-table-result-cell-balance-${exercice}`,
          _type: 'string',
          cellProp: {
            className: styles.resultCell
          },
          props: {
            label: formatNumberIfNotNull(exercice_values[i]),
            noWrap: true
          }
        })),
        ...(isAllMonthVisible && displayType.length
          ? month_values.map((v, i) => ({
            keyCell: `current-editions-table-result-cell-month${i}`,
            cellProp: {
              className: styles.resultCell
            },
            _type: 'string',
            props: {
              label: formatNumberIfNotNullAndNoDecimals(v),
              noWrap: true
            }
          })) : [])
      ]
    });
  };

  render() {
    const {
      balanceData,
      isLoading,
      isError,
      selectedExercice
    } = this.props;

    if (!Object.keys(balanceData).length) {
      return null;
    }

    return (
      <div className={styles.container}>
        <div className={styles.controls}>
          <CurrentEditionsTableFilters />
          <CurrentEditionsTableTools selectedExercice={selectedExercice} />
        </div>
        <div
          className={styles.scrollable}
          style={{
            maxHeight: window.innerHeight - 294
          }}
        >
          <TableFactory
            isLoading={isLoading}
            isError={isError}
            param={{
              header: { row: this.generateTableFactoryHeaderRowsParam() },
              body: {
                row: (isLoading || isError ? [] : [
                  ...this.generateTableFactoryBodyRowsParam(balanceData.data, 0),
                  this.generateTableFactoryResultRow(balanceData.results)
                ])
              }
            }}
          />
        </div>
      </div>
    );
  }
}

// Props
CurrentEditionsTable.propTypes = {
  tableFiltersValues: PropTypes.shape({
    isRevisionsHidden: PropTypes.bool,
    isAllMonthVisible: PropTypes.bool,
    displayType: PropTypes.string
  }).isRequired,

  filtersValues: PropTypes.shape({
    exercices: PropTypes.arrayOf(PropTypes.string)
  }).isRequired,

  classes: PropTypes.shape({
    root: PropTypes.string.isRequired
  }).isRequired,

  getBalanceData: PropTypes.func.isRequired,
  balanceData: PropTypes.shape({}).isRequired,

  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,

  split: splitShape.isRequired,

  start_date: PropTypes.string.isRequired,
  end_date: PropTypes.string.isRequired,

  societyId: PropTypes.number.isRequired,
  selectedExercice: PropTypes.object,
  openCommentsDialog: PropTypes.func.isRequired,
  getComments: PropTypes.func.isRequired,
  currentRevision: PropTypes.object,
  getReviews: PropTypes.func.isRequired,

  postNewComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired
};

CurrentEditionsTable.defaultProps = {
  selectedExercice: undefined,
  currentRevision: {}
};

// Enhance
const enhance = compose(
  splitWrap,
  withStyles(MuiButtonBaseStyle)
);

export default enhance(CurrentEditionsTable);
