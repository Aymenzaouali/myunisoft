import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  FormControl, FormLabel, RadioGroup, FormControlLabel,
  TextField, Radio
} from '@material-ui/core';
import I18n from 'i18next';

import styles from './style.module.scss';

class CurrentEditionsAccountNumberFieldsGroup extends Component {
  constructor(props) {
    super(props);

    this.handleCustomRadioTextFieldOnChange = this.handleCustomRadioTextFieldOnChange.bind(this);
    this.handleAccountNumberInputRef = this.handleAccountNumberInputRef.bind(this);
    this.handleRadioGroupOnChange = this.handleRadioGroupOnChange.bind(this);
  }

  handleRadioGroupOnChange(e, v) {
    const { onChange, value } = this.props;

    onChange(v, value);

    if (v === 'custom' && !value.length) this.customAccountNumberInputRef.focus();
  }

  handleCustomRadioTextFieldOnChange(ev) {
    const { onChange, type } = this.props;

    onChange(type, ev.target.value);
  }

  handleAccountNumberInputRef(ref) {
    this.customAccountNumberInputRef = ref;
  }

  render() {
    const { type, value } = this.props;

    return (
      <FormControl component="fieldset" className={styles.currentEditionsFiltersRadios}>
        <FormLabel component="legend">{I18n.t('currentEditions.filters.accountNumber')}</FormLabel>
        <RadioGroup value={type} onChange={this.handleRadioGroupOnChange} row>
          <FormControlLabel
            value="all"
            control={<Radio color="primary" />}
            label={I18n.t('currentEditions.filters.checkboxes.allLines')}
            labelPlacement="end"
          />
          <FormControlLabel
            value="custom"
            control={<Radio color="primary" />}
            label={(
              <TextField
                inputRef={this.handleAccountNumberInputRef}
                type="text"
                placeholder={I18n.t('currentEditions.filters.checkboxes.accountNumberExample')}
                onChange={this.handleCustomRadioTextFieldOnChange}
                value={value}
              />
            )}
            labelPlacement="end"
          />
        </RadioGroup>
      </FormControl>
    );
  }
}

CurrentEditionsAccountNumberFieldsGroup.defaultProps = {
  type: '',
  value: ''
};

CurrentEditionsAccountNumberFieldsGroup.propTypes = {
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
  value: PropTypes.string
};

export default CurrentEditionsAccountNumberFieldsGroup;
