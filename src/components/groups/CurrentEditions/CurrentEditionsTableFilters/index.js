import React from 'react';
import PropTypes from 'prop-types';
import { Field, propTypes as reduxFormPropTypes } from 'redux-form';
import { FormGroup } from '@material-ui/core';
import I18n from 'i18next';

import { ReduxRadio, ReduxCheckBox } from 'components/reduxForm/Selections';

const CurrentEditionsTableFilters = (props) => {
  const {
    modeTypes,
    formValues,
    headerValues,
    getBalanceData,
    getSigData,
    currentOption
  } = props;

  const {
    dateStart,
    dateEnd,
    filter
  } = headerValues;

  const { isAllMonthVisible } = formValues;

  const loadData = () => {
    if (currentOption === 'balance') {
      getBalanceData(filter, dateStart, dateEnd, 0);
    }
  };

  const onChangeMode = (event) => {
    if (currentOption === 'balance') {
      getBalanceData(filter, dateStart, dateEnd, event.target.value);
    } else if (currentOption === 'sig') {
      getSigData(dateStart, dateEnd, event.target.value);
    }
  };

  return (
    <FormGroup row>
      {currentOption === 'balance' && (
        <Field
          name="isRevisionsHidden"
          component={ReduxCheckBox}
          label={I18n.t('currentEditions.table.filters.isRevisionsHidden')}
          color="primary"
        />
      )
      }
      <Field
        name="isAllMonthVisible"
        component={ReduxCheckBox}
        label={I18n.t('currentEditions.table.filters.isAllMonthVisible')}
        color="primary"
        onChange={loadData}
      />
      {isAllMonthVisible === true
        && (
          <Field
            name="displayType"
            component={ReduxRadio}
            list={modeTypes}
            row
            color="primary"
            onChange={event => onChangeMode(event)}
          />
        )
      }
    </FormGroup>
  );
};

CurrentEditionsTableFilters.propTypes = {
  formValues: PropTypes.shape({
    isRevisionsHidden: PropTypes.bool.isRequired,
    isAllMonthVisible: PropTypes.bool.isRequired,
    displayType: PropTypes.string
  }).isRequired,
  currentOption: PropTypes.string.isRequired,
  ...reduxFormPropTypes
};

export default CurrentEditionsTableFilters;
