import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core';
import I18n from 'i18next';
import PropTypes from 'prop-types';

import CurrentEditionsTableToolsDialog from 'containers/groups/CurrentEditions/CurrentEditionsTableToolsDialog';

import Button from 'components/basics/Buttons/Button';

const defaultState = {
  dialog: {
    type: 'print',
    isOpen: false
  }
};

const styles = {
  root: {
    marginRight: '10px'
  }
};

class CurrentEditionsTableTools extends Component {
  constructor(props) {
    super(props);

    this.state = defaultState;
  }

  handleOnButtonClick = type => () => this.setState({ dialog: { type, isOpen: true } });

  handleOnModalClose = () => this.setState(defaultState);

  render() {
    const { classes, selectedExercice } = this.props;
    const { dialog } = this.state;
    return (
      <Fragment>
        <Button classes={classes} size="large" variant="contained" color="primary" disabled={!selectedExercice} onClick={this.handleOnButtonClick('export')}>
          {I18n.t('currentEditions.table.tools.export')}
        </Button>
        <Button size="large" variant="contained" color="primary" disabled={!selectedExercice} onClick={this.handleOnButtonClick('print')}>
          {I18n.t('currentEditions.table.tools.print')}
        </Button>
        <CurrentEditionsTableToolsDialog onClose={this.handleOnModalClose} {...dialog} />
      </Fragment>
    );
  }
}

CurrentEditionsTableTools.propTypes = {
  classes: PropTypes.object,
  selectedExercice: PropTypes.object
};

CurrentEditionsTableTools.defaultProps = {
  classes: {},
  selectedExercice: undefined
};

export default withStyles(styles)(CurrentEditionsTableTools);
