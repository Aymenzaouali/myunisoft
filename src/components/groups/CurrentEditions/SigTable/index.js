import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import moment from 'moment';
import I18n from 'assets/I18n';

import { withStyles } from '@material-ui/core';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';
import { formatNumberIfNotNull, formatNumberIfNotNullAndNoDecimals } from 'helpers/number';

import Button from 'components/basics/Buttons/Button';

import TableFactory from 'components/groups/Tables/Factory';
import CurrentEditionsTableFilters from 'containers/groups/CurrentEditions/CurrentEditionsTableFilters';
import CurrentEditionsAccountFilter from 'containers/groups/CurrentEditions/CurrentEditionsAccountFilter';
import CurrentEditionsTableTools from 'components/groups/CurrentEditions/CurrentEditionsTableTools';

import styles from './style.module.scss';

// *** Split feature still to be developed ***

const ownStyle = ({
  root: {
    padding: 0,
    minWidth: 14,
    minHeight: 14,
    boxShadow: 'none'
  },
  label: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

class SigTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openedParentIds: new Set(),
      isAllChildsVisible: false
    };
  }

  async componentDidMount() {
    const { getSigData } = this.props;
    await getSigData();
  }

  componentDidUpdate() {
    const $tr = document.getElementsByTagName('tr');

    if ($tr.length) {
      $tr[$tr.length - 2].childNodes
        .forEach(e => e.classList.add(styles.noBorderCell));
    }
  }

  findChildById = (data, id) => data.reduce((p, c) => {
    if (!p && c.label === id) return c;
    if (!p && c.account_List) return this.findChildById(c.account_List, id);
    return p;
  }, null);

  applyToChilds = (data, fn) => data.forEach((item) => {
    fn(item);
    if (item.account_List) this.applyToChilds(item.account_List, fn);
  });

  getLevelIndentSizeInPx = level => 1 + level * 20;

  handleOnToggleChildsButtonClick = id => this.setState(({ openedParentIds }) => {
    const { balanceData } = this.props;

    if (openedParentIds.has(id)) {
      this.applyToChilds(
        [this.findChildById(balanceData.data, id)],
        item => openedParentIds.delete(item.label)
      );
    } else openedParentIds.add(id);

    return ({ openedParentIds, isAllChildsVisible: false });
  });

  handleOnToggleAllChildsButtonClick = () => {
    const { balanceData } = this.props;

    this.setState(({ openedParentIds, isAllChildsVisible }) => {
      if (isAllChildsVisible) openedParentIds.clear();
      else this.applyToChilds(balanceData.data, item => openedParentIds.add(item.label));

      return ({
        openedParentIds,
        isAllChildsVisible: !isAllChildsVisible
      });
    });
  };

  generateHeader = () => {
    const {
      classes,
      societyId,
      isAllMonthVisible,
      balanceData
    } = this.props;

    const { isAllChildsVisible } = this.state;

    return [{
      keyRow: `${societyId}SigTableHeaderRow`,
      value: [
        {
          component: 'div',
          keyCell: `${societyId}SigTableHeaderCell`,
          props: {
            children: (
              <Button
                classes={classes}
                size="small"
                color="primary"
                variant="contained"
                onClick={this.handleOnToggleAllChildsButtonClick}
              >
                { isAllChildsVisible ? '-' : '+' }
              </Button>
            )
          },
          cellProp: {
            className: cellStyles.stickyHeader
          }
        },
        {
          component: 'div',
          keyCell: `${societyId}SigTableHeaderCell.title`,
          props: {
            children: I18n.t('sigTable.header.title'),
            noWrap: true
          },
          cellProp: {
            className: cellStyles.stickyHeader
          }
        },
        {
          component: 'div',
          keyCell: `${societyId}SigTableHeaderCell.current_year`,
          cellProp: {
            style: { textAlign: 'right' },
            className: cellStyles.stickyHeader
          },
          props: {
            children: I18n.t('sigTable.header.current_year'),
            noWrap: true
          }

        },
        {
          component: 'div',
          keyCell: `${societyId}SigTableHeaderCell.previous_year`,
          cellProp: {
            style: { textAlign: 'right' },
            className: cellStyles.stickyHeader
          },
          props: {
            children: I18n.t('sigTable.header.previous_year'),
            noWrap: true
          }

        },
        {
          component: 'div',
          keyCell: `${societyId}SigTableHeaderCell.variation_percentage`,
          cellProp: {
            style: { textAlign: 'right' },
            className: cellStyles.stickyHeader
          },
          props: {
            children: I18n.t('sigTable.header.variation_percentage'),
            noWrap: true
          }
        },
        {
          component: 'div',
          keyCell: `${societyId}SigTableHeaderCell.variation_amount`,
          cellProp: {
            style: { textAlign: 'right' },
            className: cellStyles.stickyHeader
          },
          props: {
            children: I18n.t('sigTable.header.variation_amount'),
            noWrap: true
          }
        },

        ...(isAllMonthVisible ? _.get(balanceData, 'month_list', []).map(month => ({
          component: 'div',
          keyCell: `${societyId}SigTableHeaderCell.${month}`,
          cellProp: {
            style: { textAlign: 'right' },
            className: cellStyles.stickyHeader
          },
          props: {
            children: moment(month.replace('/', '-')).format('MMM YYYY'),
            noWrap: true
          }
        })) : [])
      ]
    }];
  };

  SigTableRow = (data, level) => {
    const {
      classes,
      societyId,
      isAllMonthVisible,
      exercices
    } = this.props;

    const { openedParentIds } = this.state;

    const lettersRegex = /[a-zA-Z]/;

    return ({
      keyRow: `${societyId}SigTableRowHeader`,
      props: {
        hover: true,
        onDoubleClick: () => this.handleOnToggleChildsButtonClick(data.label),
        onClick: () => {},
        className: data.label.includes('=') ? styles.mainRows : null
      },
      value: [
        {
          component: 'div',
          keyCell: `${societyId}SigTableRowCell.toggleButton`,
          cellProp: {
            style: { width: '20px' }
          },
          props: {
            children: data.account_List.length ? (
              <Button
                size="small"
                color="primary"
                variant="contained"
                onClick={() => this.handleOnToggleChildsButtonClick(data.label)}
                classes={classes}
                style={{
                  position: 'relative',
                  left: this.getLevelIndentSizeInPx(level)
                }}
              >
                {openedParentIds.has(data.label) ? '-' : '+'}
              </Button>

            ) : ''
          }
        },
        {
          component: 'div',
          keyCell: `${societyId}SigTableRowCell.${data.label}`,
          cellProp: {
            style: {
              paddingLeft: this.getLevelIndentSizeInPx(level),
              fontWeight: data.label.includes('=') ? 'bold' : null
            }
          },
          props: {
            children: lettersRegex.test(data.account_number) ? data.label : `${data.account_number} ${data.label}`
          }
        },

        // Soldes par exercice
        ...exercices.map((exercice, i) => ({
          component: 'div',
          keyCell: `${societyId}SigTableRowCell.${exercice}`,
          cellProp: {
            style: { textAlign: 'right' }
          },
          props: {
            children: formatNumberIfNotNull(data.exercice_values[i])
          }
        })),

        // Var N/N-1
        {
          component: 'div',
          keyCell: `${societyId}SigTableRowCell.variation_percentage`,
          cellProp: {
            style: { textAlign: 'right' }
          },
          props: {
            children: data.exercice_variation_rate[0] === null ? null : `${formatNumberIfNotNull(data.exercice_variation_rate[0])} %`
          }
        },

        // Montant € variation
        {
          component: 'div',
          keyCell: `${societyId}SigTableRowCell.variation_amount`,
          cellProp: {
            style: { textAlign: 'right' }
          },
          props: {
            children: formatNumberIfNotNull(data.exercice_variation_amount)
          }
        },

        // Soldes par mois
        ...isAllMonthVisible ? data.month_values.map((v, i) => ({
          component: 'div',
          keyCell: `${societyId}SigTableRowCell.${i}`,
          cellProp: {
            style: { textAlign: 'right' }
          },
          props: {
            children: formatNumberIfNotNullAndNoDecimals(v)
          }
        })) : []
      ]
    });
  };

  generateBody = (data, level) => {
    const { openedParentIds } = this.state;
    const rows = [];

    data.forEach((curr, index) => {
      rows.push(this.SigTableRow(curr, level, index));

      if (curr.account_List && openedParentIds.has(curr.label)) {
        rows.push(
          ...this.generateBody(curr.account_List, level + 1)
        );
      }
    });

    return rows;
  };

  render() {
    const {
      balanceData,
      isLoading,
      isError,
      selectedExercice
    } = this.props;

    if (!Object.keys(balanceData).length) {
      return null;
    }

    return (
      <div>
        <CurrentEditionsAccountFilter />
        <div className={styles.controls}>
          <CurrentEditionsTableFilters />
          <CurrentEditionsTableTools selectedExercice={selectedExercice} />
        </div>
        <div
          style={{
            overflow: 'auto',
            maxHeight: window.innerHeight - 277
          }}
        >
          <TableFactory
            isLoading={isLoading}
            isError={isError}
            param={{
              header: { row: this.generateHeader() },
              body: {
                row: (isLoading || isError
                  ? []
                  : this.generateBody(balanceData.data, 0))
              }
            }}
          />
        </div>
      </div>
    );
  }
}

SigTable.propTypes = {
  getSigData: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  balanceData: PropTypes.object.isRequired,
  isAllMonthVisible: PropTypes.bool.isRequired,
  exercices: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedExercice: PropTypes.object
};

SigTable.defaultProps = {
  selectedExercice: undefined
};

export default withStyles(ownStyle)(SigTable);
