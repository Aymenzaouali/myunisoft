import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'redux-form';

import { Button, withStyles } from '@material-ui/core';

import { ReduxTextField } from 'components/reduxForm/Inputs';
import { ReduxRadio } from 'components/reduxForm/Selections';

import { Periode } from 'containers/reduxForm/Inputs';

import I18n from 'i18next';
import styles from './CurrentEditionsAccountFilter.module.scss';

const CurrentEditionsAccountFilter = (props) => {
  const {
    accountFilterFormValues,
    getTable,
    classes,
    societyId,
    initializeDate,
    start_date,
    end_date,
    exercice_N,
    currentOption,
    getSigTable
  } = props;
  const { selectionType, exercice } = accountFilterFormValues;

  const type = [
    {
      value: 'AllAccounts',
      label: I18n.t('currentEditions.table.filters.allAccounts')
    },
    {
      value: 'PartialAccounts',
      label: (
        <Field
          disabled={selectionType === 'AllAccounts'}
          name="filter"
          component={ReduxTextField}
          type="text"
          label={I18n.t('currentEditions.table.filters.partialAccountHolder')}
          margin="none"
          className={classes.accountField}
        />
      )
    }
  ];

  useEffect(() => {
    initializeDate(societyId, start_date, end_date, exercice_N);
  }, [start_date, end_date]);

  const loadTable = () => {
    if (currentOption === 'balance') {
      getTable();
    } else if (currentOption === 'sig') {
      getSigTable();
    }
  };

  return (
    <div>
      <Form className={styles.form}>
        <Field
          component={Periode}
          displayExercice
          isMulti={false}
          separator
          row
          name="Date"
          formName={`${societyId}currentEditionsAccountFilter`}
          exerciceField={styles.exercice}
        />
        {currentOption === 'balance'
        && (
          <Field
            color="primary"
            name="selectionType"
            component={ReduxRadio}
            label={I18n.t('currentEditions.table.filters.accountNumber')}
            list={type}
            row
          />
        )
        }
        <Button
          color="primary"
          size="medium"
          variant="contained"
          disabled={!exercice || exercice.length > 1}
          onClick={() => loadTable()}
        >
          {I18n.t('currentEditions.table.filters.loadTable')}
        </Button>
      </Form>
    </div>
  );
};

CurrentEditionsAccountFilter.defaultProps = {
  accountFilterFormValues: {},
  exercice_N: []
};

CurrentEditionsAccountFilter.propTypes = {
  accountFilterFormValues: PropTypes.object,
  getTable: PropTypes.func.isRequired,
  getSigTable: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired,
  initializeDate: PropTypes.func.isRequired,
  start_date: PropTypes.string.isRequired,
  end_date: PropTypes.string.isRequired,
  exercice_N: PropTypes.array,
  currentOption: PropTypes.string.isRequired
};

const ownStyles = () => ({
  accountField: {
    marginBottom: '18px'
  }
});

export default withStyles(ownStyles)(CurrentEditionsAccountFilter);
