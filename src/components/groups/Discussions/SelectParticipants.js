import React, { useState, useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Dialog, Typography, withStyles } from '@material-ui/core';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import { Field } from 'redux-form';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { FontIcon } from 'components/basics/Icon';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

// import { DiscussionParticipantsAutoComplete } from 'containers/reduxForm/Inputs';
import { AutoComplete } from 'components/reduxForm/Inputs';

import styles from './SelectParticipants.module.scss';

const style = () => ({
  cardAccount: {
    width: 700,
    maxHeight: '100%',
    overflow: 'hidden',
    borderRadius: 2,
    boxShadow: '1px 5px 7px 0px rgba(0,0,0, 0.08)',
    backgroundColor: '#fff',
    paddingLeft: 100,
    paddingRight: 70
  }
});


const SelectParticipants = memo((props) => {
  const {
    classes,
    isOpen,
    onClose,
    labels,
    options,
    loggedUserId
  } = props;

  const [participants, setParticipants] = useState([]);

  useEffect(() => {
    if (!isOpen) {
      setParticipants([]);
    }
  }, [isOpen]);

  const translations = `discussions.selectParticipants.${labels}`;
  const selectedParticipantIds = new Set(participants.map(item => item.id));

  const renderSelectParticipants = () => (
    <AutoComplete
      id="selectParticipants"
      className={styles.textInput}
      skippingParticipants={participants}
      placeholder={I18n.t(`${translations}.selectPeopleEmpty`)}
      onChangeValues={value => setParticipants(participants.concat(value))}
      label={I18n.t(`${translations}.selectPersonsLabel`)}
      options={options.filter(item => !selectedParticipantIds.has(item.id)
        && String(loggedUserId) !== String(item.id))}
      maxMenuHeight={150}
      menuPosition="fixed"
    />
  );

  return (
    <Dialog classes={{ paper: classes.cardAccount }} aria-labelledby="account" open={isOpen} onClose={() => onClose([])}>

      <div className={styles.closeButton}>
        <IconButton aria-label="Close" onClick={() => onClose([])}>
          <CloseIcon />
        </IconButton>
      </div>

      <div className={styles.header}>
        <Typography variant="h1">
          {I18n.t(`${translations}.title`)}
        </Typography>
      </div>

      <form className={styles.form}>
        <Field
          color="primary"
          className={styles.textInput}
          component={renderSelectParticipants}
          name="discussion.participants"
        />

        <div className={styles.participantList}>
          { participants.map((item, i) => (
            <div className={styles.participantItem} key={`${i}-${item.id}`}>
              <Chip
                label={item.label}
                avatar={<Avatar src={item.avatar} />}
                onDelete={value => setParticipants(participants.filter( // eslint-disable-line
                  value => item.id !== value.id
                ))}
                deleteIcon={(
                  <span className={styles.deleteIcon}>
                    <FontIcon
                      name="icon-close"
                      color="#0bd1d1"
                    />
                  </span>
                )}
                className={styles.participant}
                key={`${i}-${item.id}`}
              />
            </div>
          ))}

        </div>

        <div className={styles.buttonPanel}>
          <InlineButton
            marginDirection="right"
            buttons={[
              {
                _type: 'string',
                text: I18n.t(`${translations}.cancelButton`),
                style: { color: 'black', backgroundColor: 'white' },
                size: 'medium',
                onClick: () => onClose([]),
                key: 'cancel'
              },
              {
                _type: 'string',
                text: I18n.t(`${translations}.createButton`),
                size: 'medium',
                onClick: () => onClose(participants),
                key: 'create',
                disabled: (participants.length === 0)
              }]
            }
          />
        </div>
      </form>

    </Dialog>
  );
});


SelectParticipants.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.shape({}),
  labels: PropTypes.string.isRequired,
  loggedUserId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
      ]).isRequired,
      label: PropTypes.string.isRequired,
      avatar: PropTypes.string
    })
  )
};

SelectParticipants.defaultProps = ({
  classes: {},
  options: []
});

export default withStyles(style)(SelectParticipants);
