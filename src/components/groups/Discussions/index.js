import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import I18n from 'assets/I18n';
import { List } from '@material-ui/core';
import { Trans } from 'react-i18next';
import moment from 'moment';
import { getMoment } from 'helpers/date';
import { fileExtMime } from 'helpers/mime';
import { smile, allEmojis, getEmojiHtml } from 'helpers/emoji';
import classNames from 'classnames';
import Menu from '@material-ui/core/Menu';
import Message from 'components/basics/Message';
import Search from 'containers/groups/Discussions/search';
import { ERROR_CODE } from 'common/redux/discussions';
import NewDirectMessage from 'containers/groups/Discussions/NewDirectMessage';
import { LinkAlreadySent } from 'components/groups/Dialogs';
import SelectParticipants from 'components/groups/Discussions/SelectParticipants'; // containers
import FileImportDialog from 'containers/groups/Discussions/FileImportDialog';
import { StatusIcon } from 'components/basics/Icon';
import {
  getFlatFolders, cloneFolders, cloneAndSortFolders,
  FOLDER_TYPE_GENERAL, FOLDER_TYPE_DOSSIERS, FOLDER_TYPE_DIRECT
} from 'common/redux/discussions/common';
import { setsEqual } from 'helpers/sets';
import NewAttachmentDialog from './NewAttachmentDialog';
import {
  ROOM_TYPE_PUBLIC, ROOM_TYPE_PRIVATE, ROOM_TYPE_DIRECT, formatInitials,
  TREEITEM, formatUsername, htmlToText,
  getTreeItemId, getDOMId, treeItemDOMPrefix, formatDirectMessageRoomName
} from './common';
import commonStyles from './common.module.scss';
import QuickSearch from './QuickSearch';
import NewDiscussion from './NewDiscussion';
import { SubheaderMenuItem, MenuItem } from './Menu';
import { TreeListItem } from './TreeListItem';
import { TreeList } from './TreeList';
import { Subheader, SubheaderIcon, SubheaderCaption } from './Subheader';
import { TagList, Tag } from './TagList';
import { FileList, File } from './FileList';
import { ParticipantList, Participant } from './Participants';
import { NewMessage } from './NewMessage';
import styles from './index.module.scss';

/* eslint-disable react/sort-comp */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable class-methods-use-this */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */

// Artificial "new room" id (for urls like /rooms/0/getAvailableUsers)
const NEW_ROOM_ID = 0;

const foundHigilightTime = 3000;
const roomMessagesLoadPortion = 0.2;
const roomMessagesLoadInitialPortion = 0.2;
const roomMessagesMinDisplay = 10;
const scrollReactionThreshold = 1000;
const scrollToMessageDelay = 5000;

const oldDate = new Date(0);

const uploadAccept = Object.entries(fileExtMime).map(
  ([ext, mime]) => `.${ext},${mime}`
).join(',');


function getRoomIdTyped(roomId, roomTypeId, societyId) {
  return `${roomId}-${roomTypeId}-${societyId}`;
}

const FOLDERS_ORDERS = {
  activity: 'activity',
  alphabetical: 'alphabetical'
};

function compareFolderOrRoomByAlphabet(a, b) {
  return (a.title || a.room_title || '').localeCompare(b.title || b.room_title);
}

function compareFolderOrRoomByActivity(a, b) {
  return b.updated_at.localeCompare(a.updated_at);
}

// artifisional functions because of weird backend id-based format
function isFolderId(item, id) {
  return item.folder_id === id && item.room_id === undefined;
}


const FOLDERS_ORDERS_COMPARE_FUNC = {
  activity: compareFolderOrRoomByActivity,
  alphabetical: compareFolderOrRoomByAlphabet
  // personalised: 'personalised'
};


function highlightInHtml(html, textToHighlight) {
  // note: cross-tag highlight isn't implemented (too complex) (like "abc" in "a<>b</b>c")
  if (!textToHighlight) {
    return html;
  }

  const t = _.escape(_.escapeRegExp(textToHighlight));

  const tagsRe = new RegExp('(<.+?>|&\\w+;)');

  const htmlParts = html.split(tagsRe).filter(Boolean);

  const r = htmlParts.map(
    item => (
      tagsRe.test(item)
        ? item
        : item.replace(new RegExp(`(${t})`, 'ig'), `<span class="${styles.highlight}">$1</span>`)
    )
  ).join('');

  return r;
}

function imaginizeEmojis(html) {
  return html.replace(
    new RegExp(`(${allEmojis.join('|')})`, 'g'),
    (match, g1) => getEmojiHtml(g1)
  );
}

class Discussions extends PureComponent {
  state = {
    activeTreeItemData: null,
    isCreatingDiscussion: false,
    isCreatingDirectMessage: false,
    creatingDiscussionFolder: null,
    creatingDiscussionAvailableParticipants: null,
    creatingDirectMessageAvailableParticipants: null,
    searchingCategory: null,
    searchOptions: [],
    searchHandler: null,
    foundFolder: null,
    messageIdToScroll: null,
    foundDocumentId: null,
    foundUserId: null,
    foundTag: null,
    filterMenuVisible: false,
    foldersOrder: FOLDERS_ORDERS.activity,
    participantMenuVisible: false,
    isAddingParticipants: false,
    isDeletingParticipants: false,
    isGEDImportDialogActive: false,
    isNewAttachmentDialogActive: false,
    newMessageText: null,
    initialMessageHtml: null,
    isNewMessageEmpty: true,
    foldersVisual: [],
    newMessageUploadedFiles: null,
    newMessageGEDDocuments: [],
    expandedFolderIds: new Set([]),
    quickFoundMessageText: null,
    quickFoundUserText: null,
    quickFoundDocumentText: null, // TODO: after documents will be accessible
    roomMessagesLoadedPart: roomMessagesLoadInitialPortion,
    messageScrollChanged: null,
    activeRoomAvailableUsersToAdd: [],
    activeRoomAvailableUsersToDelete: []
  }

  componentDidMount() {
    const { getFolders, selectedDocumentToCopy } = this.props;
    getFolders();

    if (selectedDocumentToCopy && selectedDocumentToCopy.location) {
      this.activateTreeItem(selectedDocumentToCopy.location);
      this.setState({ initialMessageHtml: selectedDocumentToCopy?.document?.urltoken_preview || I18n.t('common.errors.invalid') });
    }
  }

  static getAdditionalTreeItemAttributes = (treeItem, state, props) => {
    const { loggedUser } = props;
    const { expandedFolderIds } = state;
    const r = {
      unreadMessagesOwnRooms: 0,
      unreadMessagesForeignRooms: 0
    };

    if (expandedFolderIds.has(treeItem.folder_id)) {
      r.expanded = true;
    }

    // set type (room/folder)
    if (treeItem.folder_id) {
      if (treeItem.room_id) {
        r.type = TREEITEM.ROOM;
      } else {
        r.type = TREEITEM.FOLDER;
      }
    }

    // generate folder updated_at by recent room updated_at
    if (!treeItem.updated_at) {
      r.updated_at = treeItem.rooms.length
        ? treeItem.rooms.reduce(
          (max, room) => (room.updated_at > max ? room.updated_at : max),
          treeItem.rooms[0].updated_at
        )
        : oldDate.toISOString(); // sic: keep all dates as strings
    }

    if (r.type === TREEITEM.FOLDER) { // set above
      treeItem.folders.concat(treeItem.rooms).reduce(
        (obj, item) => {
          obj.unreadMessagesOwnRooms += item.unreadMessagesOwnRooms;
          obj.unreadMessagesForeignRooms += item.unreadMessagesForeignRooms;
          return obj;
        },
        r
      );
    }

    if (r.type === TREEITEM.ROOM) { // set above
      r.isParticipant = treeItem.members.some(
        element => Number(element.user_id) === loggedUser.user_id
      );

      if (r.isParticipant) {
        r.unreadMessagesOwnRooms = treeItem.unread_messages;
      } else {
        r.unreadMessagesForeignRooms = treeItem.unread_messages;
      }
    }

    // format title
    if (treeItem.room_type_id === ROOM_TYPE_DIRECT) {
      // all room users except logged

      // patch user_id type until fixed https://myunisoft.atlassian.net/browse/MYUN-4330
      // eslint-disable-next-line
      treeItem.members.forEach(item => {item.user_id = Number(item.user_id)});

      r.title = formatDirectMessageRoomName(
        treeItem.members.filter(
          item => item.user_id !== loggedUser.user_id
        )
      );
    }

    return r;
  }

  static getDerivedStateFromProps(props, state) {
    const {
      isCreatingDiscussion, isCreatingDirectMessage, creatingDiscussionFolder,
      activeTreeItemData
    } = state;
    const { isPersonalTab, activeSocietyId } = props;

    const result = {};

    // set foldersVisual state based on props
    if (!_.isEmpty(props.folders)) {
      result.foldersVisual = cloneAndSortFolders(
        props.folders,
        FOLDERS_ORDERS_COMPARE_FUNC[state.foldersOrder],
        treeItem => Discussions.getAdditionalTreeItemAttributes(treeItem, state, props),
        // skip general folder for company tabs
        item => !(
          item.root
            && item.type_folder_id === FOLDER_TYPE_GENERAL
            && !isPersonalTab
        )
      );
    }

    // set first found subfolder active
    if (_.isEmpty(activeTreeItemData) && result.foldersVisual) {
      const firstFoundSubfolder = _.get(result.foldersVisual, '[0].folders[0]');

      if (firstFoundSubfolder) {
        result.activeTreeItemData = _.cloneDeep(firstFoundSubfolder);
      }
    }

    if (isCreatingDiscussion
        && creatingDiscussionFolder
        && props.roomAvailableUsers) {
      if (isCreatingDirectMessage) {
        result.creatingDirectMessageAvailableParticipants = (
          Discussions.getNewRoomAvailableParticipants(
            ROOM_TYPE_DIRECT, activeSocietyId, props
          )
        );
      } else {
        result.creatingDiscussionAvailableParticipants = {
          [ROOM_TYPE_PRIVATE]: Discussions.getNewRoomAvailableParticipants(
            ROOM_TYPE_PRIVATE, activeSocietyId, props
          ),
          [ROOM_TYPE_PUBLIC]: Discussions.getNewRoomAvailableParticipants(
            ROOM_TYPE_PUBLIC, activeSocietyId, props
          )
        };
      }
    }

    return result;
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      getRoom, getRoomMessages, lastCreatedMessage, lastCreatedRoom,
      roomAvailableUsers, rooms, setActiveRoomId, activeSocietyId
    } = this.props;

    const {
      foundFolder, messageIdToScroll, foundUserId, foundTagId, activeTreeItemData
    } = this.state;

    const room_id = _.get(activeTreeItemData, 'room_id');
    const room_id_prev = _.get(prevState.activeTreeItemData, 'room_id');
    // const { room_id: room_id_prev } = prevState.activeTreeItemData;

    if (room_id && room_id !== room_id_prev) {
      getRoom(room_id);
      getRoomMessages(room_id);

      this.setRoomMessagesScrollPosition(0);
      this.setState({
        roomMessagesLoadedPart: roomMessagesLoadInitialPortion
      });
    }

    if (room_id !== room_id_prev) {
      setActiveRoomId(room_id);
    }

    if (this.activeRoomMessages(prevProps, prevState).length
      !== this.activeRoomMessages(this.props, this.state).length) {
      this.setState({ isNewMessageEmpty: true });   // eslint-disable-line
    }

    if (foundFolder && foundFolder !== prevState.foundFolder) {
      document.getElementById(
        getDOMId(treeItemDOMPrefix, getTreeItemId(foundFolder))
      ).scrollIntoView();
    }

    // Note: not compare with prev state. Scroll to message until clear
    // this state (to support message scroll to quick searched with lazy-loading)

    if (messageIdToScroll) {
      this.scrollToMessageDOM(messageIdToScroll);
    }

    const { foundDocumentId } = this.state;
    if (foundDocumentId && foundDocumentId !== prevState.foundDocumentId) {
      const element = document.getElementById(getDOMId('document', foundDocumentId));
      if (!_.isEmpty(element)) {
        element.scrollIntoView();
      }
      // document.getElementById(getDOMId('document', foundDocumentId)).scrollIntoView();
    }

    if (foundUserId && foundUserId !== prevState.foundUserId) {
      this.scrollToUserDOM(foundUserId);
    }

    if (foundTagId && foundTagId !== prevState.foundTagId) {
      const element = document.getElementById(getDOMId('tag', foundTagId));
      if (!_.isEmpty(element)) {
        element.scrollIntoView();
      }
      // document.getElementById(getDOMId('tag', foundTagId)).scrollIntoView();
    }

    if (lastCreatedMessage !== prevProps.lastCreatedMessage) {
      this.scrollToMessage(lastCreatedMessage.message_id);
    }

    if (lastCreatedRoom !== prevProps.lastCreatedRoom && lastCreatedRoom) {
      this.activateTreeItem(lastCreatedRoom);
    }

    // refresh activeTreeItemData by actual room data
    if (rooms !== prevProps.rooms && activeTreeItemData) {
      const roomActive = rooms.find(
        item => item.room_id === activeTreeItemData.room_id
      );

      if (roomActive) {
        const activeTreeItemDataNew = _.cloneDeep(activeTreeItemData);
        // join additional fields + fresh room data
        Object.assign(activeTreeItemDataNew, roomActive);

        this.setState({
          activeTreeItemData: activeTreeItemDataNew
        });
      }
    }

    if (roomAvailableUsers !== prevProps.roomAvailableUsers) {
      const activeRoom = this.activeRoom();
      const activeRoomParentFolder = this.getActiveRoomParentFolder();

      const activeRoomAvailableUsersToAdd = ((
        activeRoom && activeRoomParentFolder
          && roomAvailableUsers[
            getRoomIdTyped(
              activeRoom.room_id,
              activeRoom.room_type_id,
              activeSocietyId
            )
          ]
      ) || []).map(item => ({
        id: item.id_pers_physique || item.user_id,
        label: formatUsername(item),
        avatar: item.avatar
      }));

      const activeRoomAvailableUsersToDelete = (
        activeRoom && activeRoom.members.map(item => ({
          id: item.id_pers_physique || item.user_id,
          label: formatUsername(item),
          avatar: item.avatar
        }))) || [];

      this.setState({
        activeRoomAvailableUsersToAdd,
        activeRoomAvailableUsersToDelete
      });
    }
  }

  getActiveRoomParentFolder() {
    const { folders } = this.props;
    const activeRoom = this.activeRoom();

    return activeRoom && folders && this.getFlatFoldersHere().find(
      item => isFolderId(item, activeRoom.folder_id)
    );
  }

  // TODO: memoize
  getFlatFoldersHere() {
    const { folders } = this.props;
    const flatFolders = getFlatFolders(folders);
    return flatFolders;
  }

  static getNewRoomAvailableParticipants(roomTypeId, societyId, props) {
    const { roomAvailableUsers } = props;

    return (roomAvailableUsers[getRoomIdTyped(NEW_ROOM_ID, roomTypeId, societyId)] || []).map(
      item => ({
        id: item.id_pers_physique || item.user_id,
        label: formatUsername(item),
        prenom: item.prenom || '',
        nom: item.nom || ''
      })
    );
  }

  getRootFolders() {
    const { foldersVisual } = this.state;

    return foldersVisual.reduce(
      (o, v) => {
        o[v.type_folder_id] = v; // eslint-disable-line
        return o;
      },
      {}
    );
  }

  handleCreateDiscussionStart = (destination) => { // eslint-disable
    const { getRoomAvailableUsers, activeSocietyId } = this.props;

    getRoomAvailableUsers(NEW_ROOM_ID, ROOM_TYPE_PUBLIC, activeSocietyId);
    getRoomAvailableUsers(NEW_ROOM_ID, ROOM_TYPE_PRIVATE, activeSocietyId);

    this.setState({
      isCreatingDiscussion: true,
      isCreatingDirectMessage: false,
      creatingDiscussionFolder: destination
    });
  }

  cloneTreeItem = treeItem => (
    Discussions.getAdditionalTreeItemAttributes(treeItem, this.state, this.props)
  )

  handleExpand = (data) => {
    const { foldersVisual, expandedFolderIds } = this.state;

    const expandedFolderIdsNew = new Set(expandedFolderIds);

    const clone = cloneFolders(foldersVisual, this.cloneTreeItem);
    const flatFolders = getFlatFolders(clone);

    const folder = flatFolders.find(
      item => isFolderId(item, data.folder_id)
    );

    folder.expanded = !folder.expanded;

    if (folder.expanded) {
      expandedFolderIdsNew.add(folder.folder_id);
    } else {
      expandedFolderIdsNew.delete(folder.folder_id);
    }

    this.setState({
      foldersVisual: clone,
      expandedFolderIds: expandedFolderIdsNew
    });
  }

  handleAddParticipantsFinished = (selected) => {
    const { roomAddUsers } = this.props;

    this.setState({
      isAddingParticipants: false,
      participantMenuVisible: false
    });

    if (!_.isEmpty(selected)) {
      roomAddUsers({
        room_id: this.activeRoom().room_id,
        users: selected.map(item => item.id)
      });
    }
  }

  handleDeleteParticipantsFinished = (selected) => {
    const { roomDeleteUsers } = this.props;

    this.setState({
      isDeletingParticipants: false,
      participantMenuVisible: false
    });

    if (!_.isEmpty(selected)) {
      roomDeleteUsers({
        room_id: this.activeRoom().room_id,
        ids: selected.map(item => item.id)
      });
    }
  }

  handleRoomTogglePrivate = () => {
    const { patchRoom } = this.props;
    const room = this.activeRoom();

    const newType = (
      room.room_type_id === ROOM_TYPE_PUBLIC
        ? ROOM_TYPE_PRIVATE
        : ROOM_TYPE_PUBLIC
    );

    patchRoom(this.activeRoom().room_id, { room_type_id: newType });

    this.setState({ participantMenuVisible: false });
  }

  handleRoomToggleArchived = () => {
    const { patchRoom } = this.props;
    const room = this.activeRoom();
    const newArchived = !room.is_archived;
    patchRoom(this.activeRoom().room_id, { is_archived: newArchived });
    this.setState({ participantMenuVisible: false });
  }

  handleDocumentUpload = (event) => {
    const documentInput = event.target;
    const files = documentInput.files;
    if (_.isEmpty(files)) return;

    this.setState({
      newMessageUploadedFiles: Array.from(files),
      isNewAttachmentDialogActive: true
    });

    // clear files to make onChange be called when selecting
    // same files second time
    documentInput.value = null;
  }

  handleGEDDialogClose = (data) => {
    if (data) {
      this.setState({ newMessageGEDDocuments: data });
    }

    this.setState({ isGEDImportDialogActive: false });
  }

  handleNewAttachmentDialogClose = (data) => {
    if (data) {
      this.createMessageInternal(data.text || '');
    }

    this.setState({
      isNewAttachmentDialogActive: false,
      newMessageUploadedFiles: null
    });
  }

  handleDeleteNewAttachment = (file) => {
    const { newMessageUploadedFiles } = this.state;

    this.setState({
      newMessageUploadedFiles: newMessageUploadedFiles.filter(
        item => item.name !== file.name
      )
    });
  }

  searchFolders = () => {
    this.setState({
      searchingCategory: 'folders',
      searchOptions: this.getFolderSearchOptions(),
      searchHandler: this.activateTreeItem
    });
  }

  handleAttachmentDelete = (roomId, message, attachment) => {
    const { messagePatch } = this.props;

    const documents = message.documents.filter(
      item => item.id_document !== attachment.id_document
    );
    messagePatch(roomId, message.message_id, { documents });
  }

  searchMessagesClicked = () => {
    this.setState({
      searchingCategory: 'messages',
      searchOptions: this.getMessageSearchOptions(),
      searchHandler: this.handleSearchMessages
    });
  }

  searchDocuments = () => {
    this.setState({
      searchingCategory: 'documents',
      searchOptions: this.getDocumentSearchOptions(),
      searchHandler: this.handleDocumentSearch
    });
  }

  searchUsers = () => {
    this.setState({
      searchingCategory: 'users',
      searchOptions: this.getUserSearchOptions(),
      searchHandler: this.handleUserSearch
    });
  }

  searchTags = () => {
    this.setState({
      searchingCategory: 'tags',
      searchOptions: this.getTagSearchOptions(),
      searchHandler: this.handleTagSearch
    });
  }

  activateTreeItem = (what) => {
    const { foldersVisual, expandedFolderIds } = this.state;

    const expandedFolderIdsNew = new Set(expandedFolderIds);

    const folders = cloneFolders(foldersVisual, this.cloneTreeItem);
    const flatFolders = getFlatFolders(folders);

    const searched = flatFolders.find(item => (
      (item.folder_id === what.folder_id)
        && (!what.room_id || item.room_id === what.room_id)
    ));

    const parents = flatFolders.filter(item => searched.parents.includes(item.id));

    // expand all parents
    parents.forEach(
      (item) => {
        item.expanded = true; // eslint-disable-line no-param-reassign
        expandedFolderIdsNew.add(item.folder_id);
      }
    );

    this.setState({
      searchingCategory: null,
      foldersVisual: folders,
      expandedFolderIds: expandedFolderIdsNew,
      activeTreeItemData: _.cloneDeep(searched),
      foundFolder: searched
    });

    setTimeout(() => this.setState({ foundFolderId: null }), foundHigilightTime);
  }

  handleSearchMessages = (searched) => {
    this.setState({
      searchingCategory: null
    });

    // magic: wait a little before actual scrolling (otherwise if call
    // scrollIntoView while search dialog displayed then messages div NOT
    // call onScroll event handler (that is necessary to load-on-scroll
    // all messages)
    setTimeout(() => this.scrollToMessage(searched.message_id), 500);
  }

  scrollToMessage = (message_id) => {
    this.setState({
      messageIdToScroll: message_id
    });

    setTimeout(() => {
      this.setState({ messageIdToScroll: null });
    }, scrollToMessageDelay);
  }

  handleDocumentSearch = (searched) => {
    this.setState({
      searchingCategory: null,
      foundDocumentId: searched.id
    });

    setTimeout(() => this.setState({ foundDocumentId: null }), foundHigilightTime);
  }

  handleUserSearch = (searched) => {
    this.setState({
      searchingCategory: null,
      foundUserId: searched.id
    });

    setTimeout(() => this.setState({ foundUserId: null }), foundHigilightTime);
  }


  handleTagSearch = (searched) => {
    this.setState({
      searchingCategory: null,
      foundTag: searched.id
    });

    setTimeout(() => this.setState({ foundTag: null }), foundHigilightTime);
  }

  filterAlphabetical = () => {
    const { foldersVisual } = this.state;

    this.setState({
      foldersOrder: FOLDERS_ORDERS.alphabetical,
      foldersVisual: cloneAndSortFolders(
        foldersVisual,
        FOLDERS_ORDERS_COMPARE_FUNC[FOLDERS_ORDERS.alphabetical]
      ),
      filterMenuVisible: false
    });
  };

  filterActivity = () => {
    const { foldersVisual } = this.state;

    this.setState({
      foldersOrder: FOLDERS_ORDERS.activity,
      foldersVisual: cloneAndSortFolders(
        foldersVisual,
        FOLDERS_ORDERS_COMPARE_FUNC[FOLDERS_ORDERS.activity]
      ),
      filterMenuVisible: false
    });
  };

  handleActivate = (treeItemData) => {
    this.setState({
      activeTreeItemData: _.cloneDeep(treeItemData)
    });
  }

  handleCreateDirectMessageStart = () => {
    const { getRoomAvailableUsers, activeSocietyId } = this.props;

    const messagesDirectFolder = this.getRootFolders()[FOLDER_TYPE_DIRECT];
    getRoomAvailableUsers(NEW_ROOM_ID, ROOM_TYPE_DIRECT, activeSocietyId);

    this.setState({
      isCreatingDiscussion: true,
      isCreatingDirectMessage: true,
      creatingDiscussionFolder: messagesDirectFolder
    });
  }

  handleAddParticipants = () => {
    this.updateActiveRoomAvailableUsers();

    this.setState({ isAddingParticipants: true });
  }

  handleDeleteParticipants = () => {
    this.updateActiveRoomAvailableUsers();

    this.setState({ isDeletingParticipants: true });
  }

  handleNewMessageMenuClick = (event, id) => {
    switch (id) {
    case 'GED':
      this.setState({ isGEDImportDialogActive: true });
      break;
    case 'myComputer':
      document.getElementById('documentInput').click();
      break;
    default:
      break;
    }
  }

  handleNewMessageImageMenuClick = () => {
    const { getRoomAvailableImages } = this.props;
    const activeRoomId = this.activeRoom().room_id;

    getRoomAvailableImages(activeRoomId);
  }

  handleNewMessageChanged = (text) => {
    const { isNewMessageEmpty } = this.state;

    // need verify if can do inside
    const isEmpty = (text === '');

    if (text) {
      this.setState({ newMessageText: text });
    }

    if (isEmpty !== isNewMessageEmpty) {
      this.setState({ isNewMessageEmpty: isEmpty });
    }
  }

  createMessageInternal = (html) => {
    const { createMessage, loggedUser } = this.props;
    const { newMessageUploadedFiles, newMessageGEDDocuments } = this.state;

    const roomId = this.activeRoom().room_id;
    // eslint-disable-next-line no-useless-escape
    const LINK_DETECTION_REGEX = /(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel|local|internal))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)(\s+|$)/gi;

    // handle links and replace it to clickable <a>link </a>
    // eslint-disable-next-line no-template-curly-in-string
    const withLinks = html.replace(LINK_DETECTION_REGEX, `<a href='$1' target='_blank' class="${styles.link}">$1</a> `);

    const formData = new FormData();
    formData.append('room_id', this.activeRoom().room_id);
    formData.append('user_id', loggedUser.user_id);
    formData.append('message', withLinks);

    if (newMessageGEDDocuments) {
      formData.append('documents_ids', newMessageGEDDocuments.map(item => item.id));
    }

    if (newMessageUploadedFiles) {
      newMessageUploadedFiles.forEach(
        file => formData.append('document', file, file.name)
      );
    }

    createMessage(roomId, formData);

    this.setState({
      newMessageText: null,
      newMessageUploadedFiles: null
    });
  }

  handleMessageEdited = (message, data) => {
    const roomId = this.activeRoom().room_id;
    const { messagePatch } = this.props;

    messagePatch(roomId, message.message_id, { body: data.body });
  }

  handleMessageReacted = (roomId, message/* , data */) => {
    const { reactMessage, unreactMessage } = this.props;

    // currently implement simplified reaction (smile or none)
    if (message.reactions.length === 0) {
      reactMessage(roomId, message.message_id, smile);
    } else {
      const reactionId = message.reactions[0].l_reaction_message_id;
      unreactMessage(roomId, message.message_id, reactionId);
    }
  }

  handleCreateRoom = async (value) => {
    const { createRoom, loggedUser } = this.props;
    const { creatingDiscussionFolder } = this.state;

    if (value) {
      if (value.typeId === ROOM_TYPE_DIRECT) {
        const users = [loggedUser.user_id, ...value.users];
        const directFolder = this.getRootFolders()[FOLDER_TYPE_DIRECT];

        const users_ids = new Set(users);
        const existingRoom = directFolder.rooms.find(
          item => setsEqual(
            new Set(item.members.map(m => m.user_id)),
            users_ids
          )
        );

        if (existingRoom) {
          this.activateTreeItem(existingRoom);
        } else {
          await createRoom({
            type_id: value.typeId,
            title: value.title,
            folder_id: creatingDiscussionFolder.folder_id,
            users
          });
        }
      } else {
        await createRoom({
          type_id: value.typeId,
          title: value.title,
          folder_id: creatingDiscussionFolder.folder_id,
          users: value.users
        });
      }
    }

    this.setState({
      isCreatingDiscussion: false,
      isCreatingDirectMessage: false,
      creatingDiscussionFolder: null
    });
  }

  getFolderSearchOptions = () => {
    const { foldersVisual } = this.state;

    const r = getFlatFolders(
      foldersVisual || []
    ).filter(
      item => !item.root
    ).map(item => ({
      ...item,
      group: 'Folders',
      id: item.room_id || item.folder_id,
      title: item.title || item.room_title || ''
    }));

    return r;
  }

  getMessageSearchOptions = () => {
    const messages = this.activeRoomMessages();

    return messages.map((item) => {
      const bodyText = htmlToText(item.body);

      return {
        group: 'Messages',
        id: item.message_id,
        message_id: item.message_id,
        title: `${formatUsername(item.user)}: ${bodyText}`
      };
    });
  }

  getDocumentSearchOptions = () => {
    const docs = this.activeRoom().documents;

    return docs.map(item => ({
      group: 'Docs',
      id: item.id_document,
      title: item.libelle
    }));
  }

  getUserSearchOptions = () => {
    const { members } = this.activeRoom();

    return members.map(item => ({
      group: 'Users',
      id: item.id_pers_physique || item.user_id,
      title: formatUsername(item)
    }));
  }

  getTagSearchOptions = () => {
    this.activeRoom().tags.map(item => ({
      group: 'Tags',
      id: item,
      title: item
    }));
  }

  scrollToMessageDOM(messageId) {
    // eslint-disable-line class-methods-use-this

    const element = document.getElementById(getDOMId('message', messageId));

    if (!_.isEmpty(element)) {
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }

  scrollToUserDOM(userId) { // eslint-disable-line class-methods-use-this
    const element = document.getElementById(getDOMId('user', userId));
    if (!_.isEmpty(element)) {
      element.scrollIntoView();
    }
  }

  activeRoom(props, state) {
    // const _props = props || this.props;
    const _state = state || this.state;

    const { activeTreeItemData } = _state;

    const r = activeTreeItemData && activeTreeItemData.room_id && activeTreeItemData;

    return r;
  }

  activeRoomMessages(props, state) {
    const _props = props || this.props;
    const _state = state || this.state;

    const { roomMessages } = _props;
    const activeRoom = this.activeRoom(_props, _state);
    return [...((activeRoom && roomMessages[activeRoom.room_id]) || [])].reverse();
  }

  isLoggedUserActiveRoomMember() {
    const { loggedUser } = this.props;
    const activeRoom = this.activeRoom();
    const activeRoomMemberIds = activeRoom && activeRoom.members.map(
      item => (item.id_pers_physique || item.user_id)
    );
    return activeRoomMemberIds && activeRoomMemberIds.includes(String(loggedUser.user_id));
  }

  isGettingRoom() {
    // eslint-disable-next-line
    return this.state.activeTreeItemData && this.state.activeTreeItemData.room_id && !this.activeRoom();
  }

  renderMessages() {
    const activeRoom = this.activeRoom();
    const { loggedUser } = this.props;
    const { roomMessagesLoadedPart } = this.state;
    const { messageIdToScroll, quickFoundMessageText } = this.state;
    let prevMoment = null;
    const result = [];
    const messages = this.activeRoomMessages(); // TODO
    const isLoggedUserActiveRoomMember = this.isLoggedUserActiveRoomMember();

    if (!messages) {
      result.push(
        <StatusIcon className={styles.statusIcon} status="loading" size={40} />
      );
    } else {
      let displayedMessages = 0;

      messages.forEach((item, i) => {
        const messagePart = i / messages.length;

        const id = getDOMId('message', item.message_id);
        const mom = getMoment(new Date(item.created_at));

        if (
          (displayedMessages < roomMessagesMinDisplay)
            || (messagePart < roomMessagesLoadedPart)
        ) {
          displayedMessages += 1;

          if (prevMoment && mom !== prevMoment) {
            result.push(
              <div className={styles.messagesYesterdaySeparator} key={`sep${mom}${i}`}>
                <hr />
                <div className={classNames(commonStyles.smalltext, commonStyles.gray)}>
                  { mom }
                </div>
                <hr />
              </div>
            );
          }
          prevMoment = mom;

          // simply count without distinguishing
          const reactions = (item.reactions || []).length;

          // Because component message handle this format of date, need all to be UTC
          const date = moment(item.created_at).format('YYYY-MM-DD HH:mm');

          result.push(
            <Message
              message={{
                name: formatUsername(item.user),
                body: imaginizeEmojis(highlightInHtml(item.body, quickFoundMessageText)) || '',
                avatarImgSrc: item.avatar,
                initials: formatInitials(item.user),
                date
              }}
              key={`${mom}-${i}-${item.message_id}`}
              canEdit={isLoggedUserActiveRoomMember && loggedUser.user_id === item.user_id}
              onEdit={data => this.handleMessageEdited(item, data)}
              canReact={loggedUser.user_id !== item.user_id}
              onReact={p => this.handleMessageReacted(activeRoom.room_id, item, p)}
              classes={
                classNames(
                  styles.message,
                  { [styles.foundItem]: item.message_id === messageIdToScroll }
                )
              }
              toolbarHidden
              confirmCancel={false}
              reactions={reactions}
              attachments={item.documents}
              onAttachmentDelete={
                attachment => this.handleAttachmentDelete(activeRoom.room_id, item, attachment)
              }
              id={id}
            />
          );
        } else {
          result.push(
            <div
              id={id}
              key={`${mom}-${i}-${item.message_id}`}
              className={styles.hiddenMessage}
            >
              { htmlToText(item.body) }
            </div>
          );
        }
      });
    }

    return result;
  }

  roomMessagesContainer() {
    return document.getElementById('roomMessagesContainer');
  }

  setRoomMessagesScrollPosition(position) {
    const container = this.roomMessagesContainer();

    if (!container) return;

    container.scrollTop = position / (container.scrollHeight - container.clientHeight);
  }

  getRoomMessagesScrollPosition() {
    const container = this.roomMessagesContainer();

    return container.scrollTop / (container.scrollHeight - container.clientHeight);
  }

  handleMessagesScroll = (/* event */) => {
    const { roomMessagesLoadedPart, messageScrollChanged } = this.state;
    const now = new Date();

    const sp = this.getRoomMessagesScrollPosition();

    if (sp > roomMessagesLoadedPart) {
      this.setState({
        roomMessagesLoadedPart: sp + roomMessagesLoadPortion
      });
    }

    if (
      !messageScrollChanged
        || (now - messageScrollChanged > scrollReactionThreshold)
    ) {
      this.setState({ messageScrollChanged: now });
    }
  }

  updateActiveRoomAvailableUsers = () => {
    const { getRoomAvailableUsers, activeSocietyId } = this.props;
    const activeRoom = this.activeRoom();
    const activeRoomParentFolder = this.getActiveRoomParentFolder();

    if (activeRoom && activeRoomParentFolder) {
      getRoomAvailableUsers(
        activeRoom.room_id,
        activeRoom.room_type_id,
        activeSocietyId,
        true
      );
    }
  }

  isLinkAlreadyInRoom = () => {
    const messages = this.activeRoomMessages();
    const { selectedDocumentToCopy } = this.props;
    return !!messages.find(message => message.body
      ?.includes(selectedDocumentToCopy?.document?.urltoken_preview));
  };

  render() {
    const {
      filterMenuVisible,
      participantMenuVisible,
      activeTreeItemData,
      isCreatingDiscussion,
      creatingDiscussionAvailableParticipants,
      creatingDirectMessageAvailableParticipants,
      isCreatingDirectMessage,
      isAddingParticipants,
      isDeletingParticipants,
      isGEDImportDialogActive,
      isNewAttachmentDialogActive,
      searchingCategory,
      isNewMessageEmpty,
      searchOptions,
      searchHandler,
      foundTag,
      foundUserId,
      foundDocumentId,
      quickFoundUserText,
      expandedFolderIds,
      foldersOrder,
      activeRoomAvailableUsersToAdd,
      activeRoomAvailableUsersToDelete,
      newMessageUploadedFiles,
      newMessageText,
      initialMessageHtml
    } = this.state;

    const {
      errors, loggedUser, roomAvailableImages,
      selectedDocumentToCopy,
      setDocumentToCopy
    } = this.props;

    const rootFolders = this.getRootFolders();
    const generalFolder = rootFolders[FOLDER_TYPE_GENERAL];
    const dossiersFolder = rootFolders[FOLDER_TYPE_DOSSIERS];
    const messagesDirectFolder = rootFolders[FOLDER_TYPE_DIRECT];

    // TODO: move to state or memoized function
    const activeRoom = this.activeRoom();

    const users = (activeRoom && activeRoom.members /* roomUsers[activeRoom.room_id] */) || [];

    const tags = ((activeRoom && activeRoom.tags) || []).filter(Boolean);

    const activeRoomTitle = [
      activeRoom && activeRoom.title,
      activeRoom && activeRoom.is_archived && '(archived)'
    ].filter(Boolean).join(' ');

    // Implemented: only room creator can add/remove room users, set private, archived flag
    const canManageRoom = (activeRoom && loggedUser.user_id === activeRoom.user_id);

    const discussionErrors = errors.filter(item => item.code === ERROR_CODE);
    const activeId = activeTreeItemData && getTreeItemId(activeTreeItemData);

    const leftPanelSubheaderIcons = (
      <>
        <SubheaderIcon name="icon-filter" onClick={() => this.setState({ filterMenuVisible: true })} id="filterMenuStubPositioner" className={styles.smallIcon} />
        <SubheaderIcon name="icon-search" iconCharStyle={styles.smallerIcon} onClick={this.searchFolders} />

        { loggedUser.isadmin && (
          <SubheaderIcon
            iconChar="+"
            className={styles.addButton}
            iconCharStyle={styles.addButtonChar}
            onClick={() => this.handleCreateDiscussionStart(generalFolder)}
          />
        )
        }
      </>
    );

    return (
      <>
        { (discussionErrors.length > 0) && (
          <div className={styles.errors}>
            <span>Errors:</span>
            <ul>
              { discussionErrors.map((item, i) => <li key={`${i}-${item.message}`}>{item.message}</li>) }
            </ul>
          </div>
        )}

        <div className={styles.main}>
          <Menu
            id="simple-menu"
            anchorEl={() => document.getElementById('filterMenuStubPositioner')}
            open={filterMenuVisible}
            onClose={() => this.setState({ filterMenuVisible: false })}
            disableAutoFocusItem
          >
            <SubheaderMenuItem>
              { I18n.t('discussions.filterMenu.filterBy') }
            </SubheaderMenuItem>
            <MenuItem
              onClick={this.filterActivity}
              selected={foldersOrder === FOLDERS_ORDERS.activity}
            >
              { I18n.t('discussions.filterMenu.activity') }
            </MenuItem>
            <MenuItem
              onClick={this.filterAlphabetical}
              selected={foldersOrder === FOLDERS_ORDERS.alphabetical}
            >
              { I18n.t('discussions.filterMenu.alphabetical') }
            </MenuItem>
            <MenuItem>
              { I18n.t('discussions.filterMenu.personalised') }
            </MenuItem>
          </Menu>

          <NewDiscussion
            isOpen={isCreatingDiscussion && !isCreatingDirectMessage}
            availableParticipants={creatingDiscussionAvailableParticipants}
            onClose={this.handleCreateRoom}
          />

          <NewDirectMessage
            isOpen={isCreatingDiscussion && isCreatingDirectMessage}
            availableParticipants={creatingDirectMessageAvailableParticipants}
            onClose={this.handleCreateRoom}
          />

          <Search
            isOpen={searchingCategory != null}
            onSearch={searchHandler}
            onClose={() => this.setState({ searchingCategory: null })}
            options={searchOptions}
          />

          <SelectParticipants
            isOpen={isAddingParticipants}
            onClose={this.handleAddParticipantsFinished}
            labels="add"
            loggedUserId={loggedUser.user_id}
            options={activeRoomAvailableUsersToAdd}
          />

          <SelectParticipants
            isOpen={isDeletingParticipants}
            onClose={this.handleDeleteParticipantsFinished}
            labels="delete"
            loggedUserId={loggedUser.user_id}
            options={activeRoomAvailableUsersToDelete}
          />

          <FileImportDialog
            isOpen={isGEDImportDialogActive}
            onClose={this.handleGEDDialogClose}
            roomId={activeRoom && activeRoom.room_id}
          />

          <NewAttachmentDialog
            isOpen={isNewAttachmentDialogActive}
            onClose={this.handleNewAttachmentDialogClose}
            onDelete={this.handleDeleteNewAttachment}
            attachments={newMessageUploadedFiles}
            initialText={newMessageText}
          />

          <List className={styles.leftPanel}>
            { !_.isEmpty(rootFolders)
              ? (
                <>
                  {generalFolder && (
                    <>
                      <Subheader className={styles.leftSubheader}>
                        <SubheaderCaption caption={I18n.t('discussions.general')} />
                        { leftPanelSubheaderIcons }
                      </Subheader>
                      <TreeList className={commonStyles.treeList}>
                        {
                          generalFolder ? (
                            <TreeListItem
                              {...generalFolder}
                              id={getTreeItemId(generalFolder)}
                              folders={generalFolder.folders}
                              title={generalFolder.title}
                              rooms={generalFolder.rooms}
                              expanded
                              expandedFolderIds={expandedFolderIds}
                              order={foldersOrder}
                              childrenOnly
                              onActivate={this.handleActivate}
                              activeData={activeTreeItemData}
                              activeId={activeId}
                              onAdd={this.handleCreateDiscussionStart}
                              onExpand={this.handleExpand}
                              type={TREEITEM.FOLDER}
                            />
                          )
                            : (
                              <StatusIcon className={styles.statusIcon} status="loading" size={40} />
                            )
                        }
                      </TreeList>
                    </>
                  )
                  }

                  { dossiersFolder && (
                    <>
                      <Subheader>
                        <SubheaderCaption caption={I18n.t('discussions.dossiers')} />
                        { !generalFolder && leftPanelSubheaderIcons }
                      </Subheader>

                      <TreeList className={commonStyles.treeList}>
                        <TreeListItem
                          id={getTreeItemId(dossiersFolder)}
                          folders={dossiersFolder.folders}
                          rooms={dossiersFolder.rooms}
                          title={dossiersFolder.title}
                          childrenOnly
                          expanded
                          expandedFolderIds={expandedFolderIds}
                          order={foldersOrder}
                          onActivate={this.handleActivate}
                          activeData={activeTreeItemData}
                          activeId={activeId}
                          onAdd={this.handleCreateDiscussionStart}
                          onExpand={this.handleExpand}
                          type={TREEITEM.FOLDER}
                        />
                      </TreeList>
                    </>
                  )
                  }

                  { messagesDirectFolder && (
                    <>
                      <Subheader>
                        <SubheaderCaption caption={I18n.t('discussions.messagesDirect')} />
                        <SubheaderIcon
                          iconChar="+"
                          className={styles.addButton}
                          iconCharStyle={styles.addButtonChar}
                          onClick={this.handleCreateDirectMessageStart}
                        />
                      </Subheader>

                      <TreeList className={commonStyles.treeList}>
                        <TreeListItem
                          id={getTreeItemId(messagesDirectFolder)}
                          folders={messagesDirectFolder.folders}
                          rooms={messagesDirectFolder.rooms}
                          title={messagesDirectFolder.title}
                          childrenOnly
                          expanded
                          expandedFolderIds={expandedFolderIds}
                          order={foldersOrder}
                          onActivate={this.handleActivate}
                          canAdd={false}
                          activeData={activeTreeItemData}
                          activeId={activeId}
                          type={TREEITEM.FOLDER}
                        />
                      </TreeList>
                    </>
                  )
                  }
                </>
              )
              : (
                <StatusIcon className={styles.statusIcon} status="loading" size={40} />
              )
            }
          </List>

          <div id="messages" className={styles.centralPanel}>
            { activeRoom
              && (
                <>
                  <Subheader className={styles.centralSubheader}>
                    <SubheaderCaption
                      caption={activeRoomTitle}
                      className={styles.biggerCaption}
                    />
                    <SubheaderIcon name="icon-search" color="#9b9b9b" onClick={this.searchMessagesClicked} />
                    <SubheaderIcon
                      iconChar="⋯"
                      iconCharStyle={styles.ellipsisCentralIconChar}
                      color="#d0d0d0"
                      className={styles.ellipsisCentralIcon}
                      onClick={() => this.setState({ participantMenuVisible: true })}
                      id="participantMenuPositioner"
                    />
                  </Subheader>

                  <Menu
                    open={participantMenuVisible}
                    anchorEl={() => document.getElementById('participantMenuPositioner')}
                    onClose={() => this.setState({ participantMenuVisible: false })}
                    disableAutoFocusItem
                  >
                    <MenuItem
                      divider
                      onClick={this.handleAddParticipants}
                      disabled={!canManageRoom}
                    >
                      { I18n.t('discussions.participantMenu.addParticipant') }
                    </MenuItem>
                    <MenuItem
                      divider
                      onClick={this.handleDeleteParticipants}
                      disabled={!canManageRoom}
                    >
                      { I18n.t('discussions.participantMenu.removeParticipant') }
                    </MenuItem>
                    <MenuItem
                      divider
                      onClick={this.handleRoomTogglePrivate}
                      disabled={!canManageRoom}
                    >
                      { I18n.t('discussions.participantMenu.setAsPrivate') }
                    </MenuItem>
                    <MenuItem onClick={this.handleRoomToggleArchived} disabled={!canManageRoom}>
                      { I18n.t('discussions.participantMenu.archive') }
                    </MenuItem>
                  </Menu>

                  <div
                    className={classNames(
                      commonStyles.smallText,
                      commonStyles.gray,
                      styles.smallLabel,
                      styles.roomInfo
                    )}
                  >
                    { activeRoom.member_count }
                    <span className={classNames('icon-human', styles.centralHumanIcon)} />
                    {' | '}
                    <Trans i18nKey="discussions.createdAtBy">
                      {{
                        value: {
                          date: moment(activeRoom.created_at).format('YYYY/MM/DD'),
                          user: formatUsername(activeRoom)
                        }
                      }}
                    </Trans>
                  </div>

                  <QuickSearch
                    options={this.activeRoomMessages()}
                    placeholder={I18n.t('discussions.quickSearch.searchMessagePlaceholder')}
                    buttonType="button"
                    isFound={
                      (text, item) => htmlToText(item.body).toLocaleLowerCase().includes(
                        text.toLocaleLowerCase()
                      )
                    }
                    onSearch={(text, found) => {
                      this.setState({ quickFoundMessageText: text });

                      if (found && text !== '') {
                        this.scrollToMessage(found.message_id);
                      }
                    }}
                  />

                  <div className={styles.messages} onScroll={this.handleMessagesScroll} id="roomMessagesContainer">
                    { this.renderMessages() }
                  </div>

                  <div className={styles.freeSpace}>&nbsp;</div>

                  <div className={styles.newMessage}>
                    <NewMessage
                      onMenuClick={this.handleNewMessageMenuClick}
                      onImageMenuClick={this.handleNewMessageImageMenuClick}
                      onPost={html => this.createMessageInternal(html)}
                      onChange={this.handleNewMessageChanged}
                      isEmpty={isNewMessageEmpty}
                      canEdit
                      placeholder={I18n.t('discussions.yourMessage')}
                      availableImages={roomAvailableImages[activeRoom.room_id]}
                      initialHTML={initialMessageHtml}
                    />
                    {/* canEdit=thisisLoggedUserActiveRoomMember() */}

                    <input
                      type="file"
                      style={{ display: 'none' }}
                      id="documentInput"
                      accept={uploadAccept}
                      multiple
                      onChange={this.handleDocumentUpload}
                    />

                  </div>
                </>
              )
            }

            { this.isGettingRoom() && (
              <StatusIcon className={styles.statusIcon} status="loading" size={40} />
            )
            }

          </div>

          <div className={styles.rightPanel}>
            { activeRoom && !_.isEmpty(activeRoom.documents)
              && (
                <div className={styles.attachments}>

                  <Subheader>
                    <SubheaderIcon name="icon-attachment" />
                    <SubheaderCaption caption={I18n.t('discussions.commonFiles')} />
                    <SubheaderIcon name="icon-search" color="#9b9b9b" onClick={this.searchDocuments} />
                  </Subheader>

                  <div className={styles.fileList}>
                    <FileList>
                      { activeRoom.documents.map((item, i) => (
                        <File
                          title={item.libelle}
                          user={formatUsername(item)}
                          date={new Date(item.created_at)}
                          key={`${i}-${item.id_document}`}
                          className={classNames({
                            [styles.foundItem]: item.id_document === foundDocumentId
                          })}
                          url={item.url}
                          id={getDOMId('document', item.id_document)}
                        />
                      ))}
                    </FileList>
                  </div>

                  <hr className={styles.hr} />
                </div>
              )
            }

            { users.length > 0
              && (
                <div className={styles.participants}>
                  <Subheader>
                    <SubheaderIcon name="icon-human" iconCharStyle={styles.biggerIcon} />
                    <SubheaderCaption caption={I18n.t('discussions.participants')} />
                    <SubheaderIcon name="icon-search" color="#9b9b9b" onClick={this.searchUsers} />
                  </Subheader>

                  <QuickSearch
                    options={users}
                    placeholder={I18n.t('discussions.quickSearch.searchUserPlaceholder')}
                    buttonType="icon"
                    isFound={
                      (text, item) => (
                        formatUsername(item).toLocaleLowerCase().includes(text.toLocaleLowerCase())
                      )
                    }
                    onSearch={(text, found) => {
                      this.setState({ quickFoundUserText: text });

                      if (found && text !== '') {
                        this.scrollToUserDOM(found.id_pers_physique || found.user_id);
                      }
                    }}
                    className={styles.userQuickSearch}
                  />

                  <ParticipantList className={styles.participantList}>
                    { users.map((item, i) => (
                      <Participant
                        icon={item.avatar}
                        alt={formatInitials(item)}
                        name={formatUsername(item)}
                        nameHTML={highlightInHtml(formatUsername(item), quickFoundUserText)}
                        key={`${i}-${item.id_pers_physique || item.user_id}`}
                        className={classNames(
                          {
                            [styles.foundItem]:
                            (item.id_pers_physique || item.user_id) === foundUserId
                          }
                        )}
                        id={getDOMId('user', item.id_pers_physique)}
                      />
                    ))
                    }
                  </ParticipantList>

                  <button
                    type="button"
                    disabled={!canManageRoom}
                    onClick={this.handleAddParticipants}
                    className={styles.addParticipantsButton}
                  >
                    +
                    { I18n.t('discussions.participantMenu.addParticipant') }
                  </button>

                  <hr className={styles.hr} />
                </div>
              )
            }

            { tags.length > 0
              && (
                <div className={styles.tags}>
                  <Subheader>
                    <SubheaderIcon iconChar="#" />
                    <SubheaderCaption caption={I18n.t('discussions.tags')} />
                    <SubheaderIcon name="icon-search" color="#9b9b9b" onClick={this.searchTags} />
                  </Subheader>

                  <div
                    className={classNames(
                      commonStyles.smallText,
                      commonStyles.gray,
                      styles.smallLabel
                    )}
                  >
                    {/* { I18n.t('discussions.tagLabel') } */}
                  </div>

                  <TagList>
                    { tags.map((item, i) => (
                      <Tag
                        key={`${i}-${item}`}
                        name={item}
                        className={classNames({ [styles.foundItem]: item === foundTag })}
                        id={getDOMId('tag', item)}
                      />
                    ))
                    }
                  </TagList>
                </div>
              )
            }

          </div>
        </div>
        <LinkAlreadySent
          isOpen={selectedDocumentToCopy?.location && this.isLinkAlreadyInRoom()}
          onClose={() => {
            setDocumentToCopy({});
            this.setState({ initialMessageHtml: '' });
          }}
          onConfirm={() => {
            this.setState(
              { initialMessageHtml: selectedDocumentToCopy?.document?.urltoken_preview || I18n.t('common.errors.invalid') },
              () => setDocumentToCopy({})
            );
          }}
        />
      </>
    );
  }
}

Discussions.propTypes = {
  folders: PropTypes.array.isRequired, // eslint-disable-line react/no-unused-prop-types
  rooms: PropTypes.array.isRequired, // eslint-disable-line react/no-unused-prop-types
  roomMessages: PropTypes.shape({}).isRequired,
  selectedDocumentToCopy: PropTypes.shape({}).isRequired,
  lastCreatedMessage: PropTypes.shape({}),
  getFolders: PropTypes.func.isRequired,
  getRoom: PropTypes.func.isRequired,
  getRoomMessages: PropTypes.func.isRequired,
  createMessage: PropTypes.func.isRequired,
  // deleteMessage: PropTypes.func.isRequired,
  reactMessage: PropTypes.func.isRequired,
  unreactMessage: PropTypes.func.isRequired,
  roomAvailableUsers: PropTypes.shape({}).isRequired,
  roomAvailableImages: PropTypes.shape({}).isRequired,
  roomAddUsers: PropTypes.func.isRequired,
  roomDeleteUsers: PropTypes.func.isRequired,
  getRoomAvailableUsers: PropTypes.func.isRequired,
  getRoomAvailableImages: PropTypes.func.isRequired,
  createRoom: PropTypes.func.isRequired,
  patchRoom: PropTypes.func.isRequired,
  messagePatch: PropTypes.func.isRequired,
  setDocumentToCopy: PropTypes.func.isRequired,
  errors: PropTypes.array.isRequired,
  loggedUser: PropTypes.shape({}).isRequired,
  lastCreatedRoom: PropTypes.shape({}),
  isPersonalTab: PropTypes.bool.isRequired,
  // activeRoomId: PropTypes.shape({}).isRequired,
  setActiveRoomId: PropTypes.func.isRequired,
  activeSocietyId: PropTypes.string.isRequired
};


Discussions.defaultProps = {
  lastCreatedMessage: null,
  lastCreatedRoom: null
};


export default Discussions;
