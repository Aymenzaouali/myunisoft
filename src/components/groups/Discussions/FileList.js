import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import { FontIcon } from 'components/basics/Icon';
import commonStyles from './common.module.scss';
import styles from './FileList.module.scss';

const FileList = memo(({ children }) => (
  <ul className={styles.fileList}>
    {children}
  </ul>
));
FileList.propTypes = {
  children: PropTypes.node
};

FileList.defaultProps = {
  children: null
};


const File = memo(({ // eslint-disable-line
  title, user, date, className, url, ...others
}) => (
  <li className={className} {...others}>
    <span className={styles.icon}>
      <FontIcon name="icon-copy" size="30px" color="#9b9b9b" /* onClick={handleSearch} */ />
    </span>
    <span className={styles.fileNameDate}>
      <span className={styles.filename}>
        <a
          href={url}
          target="_blank"
          rel="noopener noreferrer"
        >
          { title }
        </a>
      </span>
      <div className={classNames(commonStyles.smallText, commonStyles.gray)}>
        { user }
        {' '}
        { date && moment(date).fromNow() }
      </div>
    </span>
  </li>
));
File.propTypes = {
  title: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  date: PropTypes.instanceOf(Date),
  className: PropTypes.string,
  url: PropTypes.string
};

File.defaultProps = {
  date: null,
  className: null,
  url: null
};

export {
  FileList, File
};
