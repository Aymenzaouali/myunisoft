import React, { useState, memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import classNames from 'classnames';
import { SubheaderIcon } from './Subheader';
import styles from './QuickSearch.module.scss';

const QuickSearch = memo((props) => {
  const {
    options,
    placeholder,
    buttonType,
    isFound,
    onSearch,
    className
  } = props;

  const [input, setInput] = useState('');
  const [foundIndex, setFoundIndex] = useState(-1);
  const [foundItems, setFoundItems] = useState([]);

  useEffect(() => {
    const newFoundItems = options.filter(
      item => (input !== '') && isFound(input, item)
    );
    setFoundItems(newFoundItems);
    setFoundIndex(newFoundItems.length > 0 ? 0 : -1);
  }, [input]);

  useEffect(() => {
    onSearch(input, foundItems[foundIndex]);
  }, [foundItems, foundIndex]);

  return (
    <div className={classNames(styles.main, className)}>
      <button
        type="button"
        disabled={!(foundIndex > 0)}
        onClick={() => setFoundIndex(
          (foundItems.length + foundIndex - 1) % foundItems.length
        )}
      >
        {'<'}
      </button>
      <button
        type="button"
        disabled={!(foundIndex !== -1 && foundIndex < foundItems.length - 1)}
        onClick={() => setFoundIndex(
          (foundItems.length + foundIndex + 1) % foundItems.length
        )}
      >
        {'>'}
      </button>
      <input
        type="text"
        value={input}
        placeholder={placeholder}
        maxLength="60"
        onChange={event => setInput(event.target.value)}
      />

      <button
        type="button"
        className={classNames(styles.findButton, input && styles.active)}
      >
        { buttonType === 'button'
          && I18n.t('discussions.quickSearch.rechercher')
        }

        { buttonType === 'icon' && (
          <SubheaderIcon
            name="icon-search"
            color={null}
            className={classNames(styles.iconButton, input && styles.active)}
          />
        )
        }
      </button>
    </div>
  );
});

QuickSearch.propTypes = {
  options: PropTypes.array.isRequired,
  placeholder: PropTypes.string,
  buttonType: PropTypes.oneOf([null, 'button', 'icon']),
  isFound: PropTypes.func.isRequired,
  onSearch: PropTypes.func,
  className: PropTypes.string
};

QuickSearch.defaultProps = ({
  onSearch: () => {},
  placeholder: '',
  buttonType: null,
  className: null
});

export default QuickSearch;
