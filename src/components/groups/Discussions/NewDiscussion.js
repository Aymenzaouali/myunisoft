import React, { useState, memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Dialog, Typography, withStyles, TextField
} from '@material-ui/core';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { FontIcon } from 'components/basics/Icon';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import AutoComplete from 'components/basics/Inputs/AutoComplete';
import CheckBox from 'components/basics/CheckBox';
import { ROOM_TYPE_PRIVATE, ROOM_TYPE_PUBLIC } from './common';
import styles from './NewDiscussion.module.scss';

const style = () => ({
  cardAccount: {
    width: 700,
    maxHeight: '100%',
    overflow: 'hidden',
    borderRadius: 2,
    boxShadow: '1px 5px 7px 0px rgba(0,0,0, 0.08)',
    backgroundColor: '#fff',
    paddingLeft: 100,
    paddingRight: 70
  }
});

const NewDiscussion = memo((props) => {
  const {
    classes,
    isOpen,
    onClose,
    availableParticipants // { [room_type_id]: {id, label}, ... }
  } = props;

  const [isPrivate, setIsPrivate] = useState(false);
  const [title, setTitle] = useState('');
  const [participants, setParticipants] = useState([]);
  const [remainedAvailableParticipants, setRemainedAvailableParticipants] = useState([]);

  useEffect(() => {
    if (!isOpen) {
      setIsPrivate(false);
      setTitle('');
      setParticipants([]);
      setRemainedAvailableParticipants([]);
    }
  }, [isOpen]);

  const roomTypeId = isPrivate ? ROOM_TYPE_PRIVATE : ROOM_TYPE_PUBLIC;

  useEffect(() => {
    if (isOpen && availableParticipants) {
      setRemainedAvailableParticipants(
        [...availableParticipants[roomTypeId]].filter(
          x => !participants.find(item => item.id === x.id)
        ).map(item => ({
          id: item.id,
          value: item.id,
          label: item.label
        }))
      );
    }
  }, [isOpen, availableParticipants, participants]);

  const getFormData = () => ({
    typeId: roomTypeId,
    title,
    users: participants.map(item => item.id)
  });

  return (
    <Dialog classes={{ paper: classes.cardAccount }} aria-labelledby="account" open={isOpen} onClose={() => onClose(null)}>

      <div className={styles.closeButton}>
        <IconButton aria-label="Close" onClick={() => onClose(null)}>
          <CloseIcon />
        </IconButton>
      </div>

      <div className={styles.header}>
        <Typography variant="h1">
          {I18n.t('discussions.new.title')}
        </Typography>
      </div>

      <div className={styles.selectPeople}>{ I18n.t('discussions.new.selectPeople') }</div>

      <form
        id="newDiscussionForm"
        className={styles.form}
      >
        <label htmlFor="newDiscussionIsPrivate"> {/* eslint-disable-line */}
          <CheckBox
            id="newDiscussionIsPrivate"
            checked={isPrivate}
            onChange={event => setIsPrivate(event.target.checked)}
          />
          <span className={classes.checkboxLabel}>{I18n.t('discussions.new.setPrivateCheckBox')}</span>
        </label>

        <div className={styles.checkboxLabel}>
          {I18n.t('discussions.new.setPrivateDescription')}
        </div>

        <TextField
          className={styles.textInput}
          label={I18n.t('discussions.new.conversationTitle')}
          id="newDiscussionTitle"
          onChange={event => setTitle(event.target.value)}
          value={title}
        />

        <AutoComplete
          className={styles.textInput}
          options={remainedAvailableParticipants}
          placeHolder={I18n.t('discussions.new.addParticipants')}
          onChangeValues={value => setParticipants(participants.concat(value))}
          value={{}}
          label={I18n.t('discussions.new.addParticipants')}
          maxMenuHeight={150}
          menuPosition="fixed"
        />

        <div className={styles.participantList}>
          { participants.map((item, i) => (
            <div className={styles.participantItem} key={`${i}-${item.id}`}>
              <Chip
                label={item.label}
                avatar={<Avatar src="/android-icon-36x36.png" />}
                onDelete={value => setParticipants(participants.filter( // eslint-disable-line
                  value => item.id !== value.id
                ))}
                deleteIcon={(
                  <span className={styles.deleteIcon}>
                    <FontIcon
                      name="icon-close"
                      color="#0bd1d1"
                    />
                  </span>
                )}
                className={styles.participant}
                key={`${i}-${item.id}`}
              />
            </div>
          ))}

        </div>

        <div className={styles.buttonPanel}>
          <InlineButton
            marginDirection="right"
            buttons={[
              {
                _type: 'string',
                text: I18n.t('discussions.new.cancelButton'),
                style: { color: 'black', backgroundColor: 'white' },
                size: 'medium',
                onClick: () => onClose(null),
                key: 'cancel'
              },
              {
                _type: 'string',
                text: I18n.t('discussions.new.createButton'),
                disabled: (title === '' /* || participants.length === 0 */),
                size: 'medium',
                onClick: () => onClose(getFormData()),
                key: 'create'
              }]
            }
          />
        </div>
      </form>

    </Dialog>
  );
});


NewDiscussion.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.shape({}),
  availableParticipants: PropTypes.PropTypes.shape({})
};

NewDiscussion.defaultProps = ({
  classes: {},
  availableParticipants: {}
});

export default withStyles(style)(NewDiscussion);
