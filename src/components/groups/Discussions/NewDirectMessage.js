import React, { useState, memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Dialog, Typography, withStyles } from '@material-ui/core';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { FontIcon } from 'components/basics/Icon';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import AutoComplete from 'components/basics/Inputs/AutoComplete';
import { ROOM_TYPE_DIRECT, formatDirectMessageRoomName } from './common';
import styles from './NewDirectMessage.module.scss';

const style = () => ({
  cardAccount: {
    width: 700,
    maxHeight: '100%',
    overflow: 'hidden',
    borderRadius: 2,
    boxShadow: '1px 5px 7px 0px rgba(0,0,0, 0.08)',
    backgroundColor: '#fff',
    paddingLeft: 100,
    paddingRight: 70
  }
});


const NewDirectMessage = memo((props) => {
  const {
    classes,
    isOpen,
    onClose,
    availableParticipants,
    loggedUser
  } = props;

  const [participants, setParticipants] = useState([]);
  const [remainedAvailableParticipants, setRemainedAvailableParticipants] = useState([]);

  useEffect(() => {
    if (!isOpen) {
      setParticipants([]);
      setRemainedAvailableParticipants([]);
    }
  }, [isOpen]);

  useEffect(() => {
    if (isOpen && availableParticipants) {
      setRemainedAvailableParticipants(
        [...availableParticipants].filter(
          x => !participants.find(item => item.id === x.id)
        ).map(item => ({
          ...item,
          id: item.id,
          value: item.id,
          label: item.label
        }))
      );
    }
  }, [isOpen, availableParticipants, participants]);

  useEffect(() => {
    if (!isOpen) {
      setParticipants([]);
    }
  }, [isOpen]);

  const getFormData = () => ({
    typeId: ROOM_TYPE_DIRECT,
    title: formatDirectMessageRoomName([...participants, loggedUser]),
    users: participants.map(item => item.id)
  });

  return (
    <Dialog classes={{ paper: classes.cardAccount }} aria-labelledby="account" open={isOpen} onClose={() => onClose(null)}>

      <div className={styles.closeButton}>
        <IconButton aria-label="Close" onClick={() => onClose(null)}>
          <CloseIcon />
        </IconButton>
      </div>

      <div className={styles.header}>
        <Typography variant="h1">
          {I18n.t('discussions.directMessage.title')}
        </Typography>
      </div>

      <div className={styles.selectPeople}>{ I18n.t('discussions.directMessage.selectPeople') }</div>

      <form className={styles.form}>
        <AutoComplete
          className={styles.textInput}
          options={remainedAvailableParticipants}
          placeHolder={I18n.t('discussions.directMessage.selectPlaceholder')}
          onChangeValues={value => setParticipants(participants.concat(value))}
          value={{}}
          label={I18n.t('discussions.new.addParticipants')}
          maxMenuHeight={150}
          menuPosition="fixed"
        />

        <div className={styles.participantList}>
          { participants.map((item, i) => (
            <div className={styles.participantItem} key={`${i}-${item.id}`}>
              <Chip
                label={item.label}
                avatar={<Avatar src="/android-icon-36x36.png" />}
                onDelete={value => setParticipants(participants.filter( // eslint-disable-line
                  value => item.id !== value.id
                ))}
                deleteIcon={(
                  <span className={styles.deleteIcon}>
                    <FontIcon
                      name="icon-close"
                      color="#0bd1d1"
                    />
                  </span>
                )}
                className={styles.participant}
                key={`${i}-${item.id}`}
              />
            </div>
          ))}
        </div>

        <div className={styles.buttonPanel}>
          <InlineButton
            marginDirection="right"
            buttons={[
              {
                _type: 'string',
                text: I18n.t('discussions.directMessage.cancelButton'),
                style: { color: 'black', backgroundColor: 'white' },
                size: 'medium',
                onClick: () => onClose(null),
                key: 'cancel'
              },
              {
                _type: 'string',
                text: I18n.t('discussions.directMessage.createButton'),
                size: 'medium',
                onClick: () => onClose(getFormData()),
                key: 'create',
                disabled: (participants.length === 0)
              }]
            }
          />
        </div>
      </form>
    </Dialog>
  );
});


NewDirectMessage.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.shape({}),
  availableParticipants: PropTypes.array,
  loggedUser: PropTypes.shape({}).isRequired
};

NewDirectMessage.defaultProps = ({
  classes: {},
  availableParticipants: []
});

export default withStyles(style)(NewDirectMessage);
