import React, { useState, memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Dialog, Typography, TextField, withStyles, Checkbox
} from '@material-ui/core';

import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import { FontIcon } from 'components/basics/Icon';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import InputAdornment from '@material-ui/core/InputAdornment';
import styles from './FileImportDialog.module.scss';

const style = () => ({
  cardAccount: {
    width: 758,
    maxHeight: '100%',
    overflow: 'hidden',
    borderRadius: 2,
    boxShadow: '1px 5px 7px 0px rgba(0,0,0, 0.08)',
    backgroundColor: '#fff',
    paddingLeft: 100,
    paddingRight: 70
  }
});

const FileImportDialog = memo((props) => {
  const {
    roomId,
    classes,
    isOpen,
    onClose,
    roomGEDItems,
    getRoomGEDItems
  } = props;

  const [files, setFiles] = useState([]);

  useEffect(() => {
    if (isOpen && roomId) getRoomGEDItems(roomId);
  }, [roomId, isOpen]);

  useEffect(() => {
    if (roomGEDItems[roomId]) {
      setFiles(roomGEDItems[roomId].map(item => (
        {
          ...item,
          checked: false
        }
      )));
    } else {
      setFiles([]);
    }
  }, [roomGEDItems]);

  useEffect(() => {
    setFiles(
      files.map(item => (
        {
          ...item,
          checked: false
        }
      ))
    );
  }, [isOpen]);


  const [search, setSearch] = useState('');

  const anyFileChecked = files.some(item => item.checked);

  const hangleSearchChange = (event) => {
    setSearch(event.target.value);
  };

  const handleFileChecked = (event) => {
    const { id, checked } = event.target;
    setFiles(files.map(item => (id === item.id ? { ...item, checked } : item)));
  };

  return (
    <Dialog classes={{ paper: classes.cardAccount }} aria-labelledby="account" open={isOpen} onClose={() => onClose()}>

      <div className={styles.closeButton}>
        <IconButton aria-label="Close" onClick={() => onClose()}>
          <CloseIcon />
        </IconButton>
      </div>

      <div className={styles.header}>
        <Typography variant="h1">
          {I18n.t('discussions.ged.title')}
        </Typography>
      </div>

      <TextField
        InputLabelProps={{ style: { color: '#000000', zIndex: 100 } }}
        id="search-input"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end" className={styles.searchInputButton}>
              <FontIcon name="icon-search" color="#9b9b9b" /* onClick={handleSearch} */ />
            </InputAdornment>
          ),
          className: styles.searchInput,
          disableUnderline: true
        }}
        onChange={hangleSearchChange}
      />

      <div className={styles.gedItems}>
        { files.filter(
          item => item.filename.toLowerCase().includes(search.toLowerCase())
        ).map(
          item => (
            <div key={`gedItem-${item.id}`} className={styles.gedItem}>
              <label htmlFor={item.id}>   {/* eslint-disable-line */}
                <img src={item.preview} alt={item.filename} />
                <div>{ item.filename }</div>
                <Checkbox
                  color="primary"
                  checked={item.checked}
                  inputProps={{ id: item.id }}
                  onChange={handleFileChecked}
                />
              </label>
            </div>
          )
        )}
      </div>

      <div className={styles.buttonPanel}>
        <InlineButton
          marginDirection="right"
          buttons={[
            {
              _type: 'string',
              text: I18n.t('discussions.ged.cancel'),
              style: { color: 'black', backgroundColor: 'white' },
              size: 'medium',
              onClick: () => onClose(),
              key: 'cancel'
            },
            {
              _type: 'string',
              text: I18n.t('discussions.ged.share'),
              size: 'medium',
              onClick: () => onClose(files.filter(item => item.checked)),
              key: 'create',
              disabled: !anyFileChecked
            }]
          }
        />
      </div>

    </Dialog>
  );
});


FileImportDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.shape({}),
  roomId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  roomGEDItems: PropTypes.shape([]).isRequired,
  getRoomGEDItems: PropTypes.func.isRequired
};

FileImportDialog.defaultProps = ({
  classes: {},
  roomId: null
});

export default withStyles(style)(FileImportDialog);
