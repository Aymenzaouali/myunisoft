import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { setsEqual } from 'helpers/sets';
import {
  ROOM_TYPE_PRIVATE, ROOM_TYPE_DIRECT, TREEITEM, getTreeItemId, getDOMId, treeItemDOMPrefix
} from './common';
import styles from './TreeListItem.module.scss';
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions  */

class TreeListItem extends React.Component {
  state = {}

  shouldComponentUpdate(nextProps/* , nextState */) {
    const { props } = this;
    const activeChanged = props.activeId !== nextProps.activeId;
    const selfActiveChanged = ([props.activeId, nextProps.activeId].filter(
      item => item === props.id
    ).length === 1);

    const selfExpandedChanged = props.expanded !== nextProps.expanded;

    const expandedChanged = !setsEqual(props.expandedFolderIds, nextProps.expandedFolderIds);

    const r = (
      selfActiveChanged
        || selfExpandedChanged
        || (
          (activeChanged || expandedChanged)
            && nextProps.expanded
            && (nextProps.folders.length || nextProps.rooms.length)
        )
        || (props.order !== nextProps.order)
        || (props.folders !== nextProps.folders)
    );

    return r;
  }


  handleAdd = () => {
    const { onAdd } = this.props;

    if (onAdd) onAdd(this.props);
  }

  handleClick = (event) => {
    // Ignore anything except native events.
    // stopPropagation & preventDefault doesn't work but
    // used as native event detector. Non-native events ignored
    // to avoid children list item click to parent propagation
    if (!(event.stopPropagation)) return;

    const { onActivate } = this.props;

    if (onActivate) {
      onActivate(this.data());
    }
  }

  handleExpand = () => {
    const { onExpand } = this.props;
    if (onExpand) onExpand(this.data());
  }

  data = () => {
    const r = { ...this.props };

    if (r.messages) {
      r.messageUserCount = new Set(r.messages.map(item => item.name)).size;
    }
    return r;
  }

  renderSelf() {
    const {
      title,
      id,
      locked,
      expandable,
      expanded,
      can_add,
      onAdd,
      activeId,
      type,
      unreadMessagesOwnRooms,
      unreadMessagesForeignRooms
    } = this.props;

    const isActive = activeId === id;

    const isTitleBold = unreadMessagesOwnRooms || unreadMessagesForeignRooms;

    const titleToDisplay = (
      <>
        <div className={styles.titleText}>
          { title }
        </div>
        <div className={styles.titleCounter}>
          { unreadMessagesOwnRooms ? `(${unreadMessagesOwnRooms})` : '' }
        </div>
      </>
    );

    return (
      <li
        className={classNames(styles.listItem, isActive && styles.activeItem)}
        onClick={this.handleClick}
        id={getDOMId(treeItemDOMPrefix, id)}
      >
        {
          (locked
            && (
              <span
                className={
                  classNames(
                    styles.lockIcon,
                    'icon-lock'
                  )
                }
                role="img"
                aria-label="locked"
              />
            )
          ) || (
            expandable
              && (
                <button
                  onClick={this.handleExpand}
                  className={styles.expandButton}
                  type="button"
                >
                  { expanded ? '-' : '+' }
                </button>
              )
          ) || (
            <div className={styles.noIconSpace} />
          )
        }

        <div className={
          classNames(
            styles.text,
            { [styles.isBold]: isTitleBold }
          )
        }
        >
          { titleToDisplay }
        </div>

        {
          (type === TREEITEM.FOLDER) && can_add && onAdd && (
            <button
              onClick={this.handleAdd}
              className={styles.addButton}
              type="button"
            >
              +
            </button>
          )
        }


      </li>
    );
  }

  renderChildren() {
    const {
      folders, rooms, activeId, onActivate, onAdd, onExpand,
      expandedFolderIds, order
    } = this.props;

    return (
      <React.Fragment>
        <li className={classNames(styles.listItem)}>
          <ul style={{ width: '100%', padding: 0 }}>
            {
              folders.map((item, i) => {
                const { folders, rooms } = item;
                const id = getTreeItemId(item);

                return (
                  <TreeListItem
                    {...item}
                    id={id}
                    expandable={
                      (folders && folders.length > 0)
                        || (rooms && rooms.length > 0)
                    }
                    order={order}
                    onAdd={onAdd}
                    key={`${i}-${id}`}
                    activeId={activeId}
                    expanded={item.expanded}
                    expandedFolderIds={expandedFolderIds}
                    onActivate={onActivate}
                    onExpand={onExpand}
                    type={TREEITEM.FOLDER}
                  />
                );
              })
            }
            {
              rooms.map((item, i) => {
                const id = getTreeItemId(item);
                return (
                  <TreeListItem
                    {...item}
                    id={id}
                    title={item.title}
                    rooms={item.rooms}
                    expandable={false}
                    locked={[ROOM_TYPE_PRIVATE, ROOM_TYPE_DIRECT].includes(item.room_type_id)}
                    order={order}
                    onAdd={onAdd}
                    key={`${i}-${id}`}
                    activeId={activeId}
                    onActivate={onActivate}
                    type={TREEITEM.ROOM}
                  />
                );
              })
            }
          </ul>
        </li>
      </React.Fragment>
    );
  }

  render() {
    const {
      folders, rooms, childrenOnly, expanded
    } = this.props;

    const needRenderChildren = Boolean(
      (expanded || childrenOnly) && (folders.length || rooms.length)
    );

    return (
      <React.Fragment>
        { !childrenOnly && this.renderSelf() }
        { needRenderChildren && this.renderChildren() }
      </React.Fragment>
    );
  }
}


TreeListItem.propTypes = {
  title: PropTypes.string,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  createdBy: PropTypes.string,
  createdAt: PropTypes.string,
  expandable: PropTypes.bool,
  expanded: PropTypes.bool,
  locked: PropTypes.bool,
  can_add: PropTypes.bool,
  onAdd: PropTypes.func,
  onExpand: PropTypes.func,
  folders: PropTypes.arrayOf(PropTypes.object),
  rooms: PropTypes.arrayOf(PropTypes.object),
  childrenOnly: PropTypes.bool,
  onActivate: PropTypes.func,
  activeId: PropTypes.string,
  messages: PropTypes.arrayOf(PropTypes.object),
  type: PropTypes.oneOf(['folder', 'room']).isRequired,
  expandedFolderIds: PropTypes.instanceOf(Set),
  order: PropTypes.any.isRequired,
  unreadMessagesOwnRooms: PropTypes.number.isRequired,
  unreadMessagesForeignRooms: PropTypes.number.isRequired
};

TreeListItem.defaultProps = {
  expandable: false,
  expanded: false,
  title: null,
  locked: false,
  can_add: true, // NOTE: TRUE by default
  onAdd: null,
  onExpand: null,
  folders: [],
  rooms: [],
  childrenOnly: false,
  onActivate: null,
  activeId: null,
  messages: null,
  createdBy: null,
  createdAt: null,
  expandedFolderIds: new Set()
};

export { TreeListItem };
