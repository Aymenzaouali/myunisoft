import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Chip from '@material-ui/core/Chip';
import classNames from 'classnames';
import styles from './TagList.module.scss';

const TagList = memo(({ children }) => (
  <span className={styles.tagList}>
    { children }
  </span>
));

TagList.propTypes = {
  children: PropTypes.node.isRequired
};


const Tag = memo(({ name, className, ...others }) => ( // eslint-disable-line
  <span {...others}>
    <Chip label={name} className={classNames(className, styles.tag)} />
  </span>
));

Tag.propTypes = {
  name: PropTypes.string.isRequired,
  className: PropTypes.string
};

Tag.defaultProps = {
  className: null
};

export {
  TagList, Tag
};
