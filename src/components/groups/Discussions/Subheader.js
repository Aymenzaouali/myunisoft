import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FontIcon } from 'components/basics/Icon';
import styles from './Subheader.module.scss';

const Subheader = memo(({ children, className }) => (
  <div className={classNames(styles.subheader, className)}>
    {children}
  </div>
));

Subheader.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string
};

Subheader.defaultProps = {
  children: null,
  className: null
};


const SubheaderIcon = memo(({ icon, name, onClick, id, color, iconChar, iconCharStyle, className }) => (   // eslint-disable-line
  <React.Fragment>
    <span className={classNames(styles.subheaderIcon, className)} id={id}>
      <FontIcon
        name={name}
        color={color}
        onClick={onClick}
        iconChar={iconChar}
        iconCharStyle={iconCharStyle}
      />
    </span>
  </React.Fragment>
));

SubheaderIcon.propTypes = {
  iconChar: PropTypes.string,
  iconCharStyle: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func,
  id: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string
};

SubheaderIcon.defaultProps = {
  onClick: null,
  id: null,
  name: null,
  iconChar: null,
  iconCharStyle: null,
  color: '#0bd1d1',
  className: null
};

const SubheaderCaption = memo(({ caption, className }) => ( // eslint-disable-line
  <span className={classNames(className, styles.subheaderCaption)}>
    { caption }
  </span>
));

SubheaderCaption.propTypes = {
  caption: PropTypes.string.isRequired,
  className: PropTypes.string
};
SubheaderCaption.defaultProps = {
  className: ''
};

export {
  Subheader, SubheaderIcon, SubheaderCaption
};
