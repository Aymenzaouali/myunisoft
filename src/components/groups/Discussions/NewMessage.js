import React, { useState, useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import I18n from 'assets/I18n';
import Menu from '@material-ui/core/Menu';
import {
  Emoji, getEmojiImage, getEmojiHtml, allEmojis
} from 'helpers/emoji';
import { pasteHtmlAtCaret, getCaretPos, setSelectionRange } from 'helpers/dom';
import { FontIcon } from 'components/basics/Icon';
import { withStyles } from '@material-ui/core/styles';
import { SubheaderMenuItem, MenuItem } from './Menu';
import styles from './NewMessage.module.scss';

// message editor smileys & gifs support mouse only
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* jsx-a11y/no-noninteractive-element-interactions: 0 */
/* eslint-disable react/destructuring-assignment */


const materialStyle = (/* theme */) => ({
  root: {}
});


const NewMessage = memo(({
  isEmpty,
  onChange,
  onMenuClick,
  onImageMenuClick,
  onPost,
  placeholder,
  canEdit,
  availableImages,
  initialHTML
}) => {
  const [emojiGroupIndex, setEmogiGroupIndex] = useState(1);
  const [imageFilter, setImageFilter] = useState('');

  const [messageAttachMenuVisible, setMessageAttachMenuVisible] = useState(false);

  const [imageMenuVisible, setImageMenuVisible] = useState(false);

  // NOTE: everywhere DOM is manipulated directly and simple variables used instead of state
  // to save new message editable text caret (which is unfocused in case of React magic)

  let _anyPopupShowing;
  let _lastEditableCaretPos;

  const editableElement = () => document.getElementById('editable');
  const smilePopup = () => document.getElementById('smilePopup');
  const imagePopup = () => document.getElementById('imagePopup');
  const imageFilterInput = () => document.getElementById('imageFilterInput');
  const popupCloser = () => document.querySelector(`.${styles.popupCloser}`);

  const setHTMLToEditor = (html) => {
    editableElement().innerHTML = html;
  };

  useEffect(() => {
    if (isEmpty) {
      editableElement().innerHTML = '';
    }
  }, [isEmpty]);

  useEffect(() => {
    if (imageMenuVisible && onImageMenuClick) onImageMenuClick();
  }, [imageMenuVisible]);

  useEffect(() => {
    setHTMLToEditor(initialHTML);
  }, [initialHTML]);

  const addToText = (str) => {
    pasteHtmlAtCaret(str);
    if (onChange) onChange(editableElement().innerText);
  };

  const handleInput = (/* event */) => {
    if (onChange) onChange(editableElement().innerText);
  };

  const handleKeyPress = (event) => {
    // if (onChange) onChange(event.target.value);
    const html = event.target.innerHTML;

    if (event.key === 'Enter' && !event.ctrlKey && !event.altKey && !event.shiftKey
        && html && onPost) {
      onPost(html);
    }
  };

  const addEmoji = (emoji) => {
    editableElement().focus();
    addToText(getEmojiHtml(emoji));
  };

  const showDOMElement = (element) => {
    element.classList.remove(styles.hidden);
  };

  const hideDOMElement = (element) => {
    element.classList.add(styles.hidden);
  };

  const isDOMElementVisible = element => !element.classList.contains(styles.hidden);

  const showSmilePopup = () => {
    _anyPopupShowing = true;
    showDOMElement(smilePopup());
    showDOMElement(popupCloser());
  };

  const showImagePopup = () => {
    _anyPopupShowing = true;
    showDOMElement(imagePopup());
    showDOMElement(popupCloser());
    setImageMenuVisible(true);
  };

  const imagePopupMouseDown = () => {
    showImagePopup();
  };

  const handleMouseDown = () => {
    showSmilePopup();
  };


  const handleImagePopupFilter = (event) => {
    const text = event.target.value;
    setImageFilter(text);
  };

  const clearImagePopupFilter = () => {
    imageFilterInput().value = '';
    setImageFilter('');

    imageFilterInput().focus();
  };

  const hidePopups = () => {
    hideDOMElement(smilePopup());
    hideDOMElement(imagePopup());
    hideDOMElement(popupCloser());

    clearImagePopupFilter();
    imagePopup().scrollTop = 0;


    if (_lastEditableCaretPos) {
      editableElement().focus();
    }

    _anyPopupShowing = false;
    setImageMenuVisible(false);
  };

  const popupCloserClicked = () => {
    hidePopups();
  };

  const handleFocusOut = () => {
    if (isDOMElementVisible(smilePopup())) {
      editableElement().focus();
      return;
    }

    if (_anyPopupShowing && !_lastEditableCaretPos) {
      _lastEditableCaretPos = getCaretPos(editableElement());
    }
  };

  const handleFocusIn = () => {
    if (_lastEditableCaretPos) {
      setSelectionRange(
        editableElement(), _lastEditableCaretPos, _lastEditableCaretPos
      );
      _lastEditableCaretPos = null;
    }
  };

  const handlePaste = (event) => {
    // from https://stackoverflow.com/a/19269040/1948511
    const clipboardData = event.clipboardData;   // eslint-disable-line

    const type = (
      (clipboardData.types.includes('text/html') && 'text/html')
      || (clipboardData.types.includes('text/plain') && 'text/plain')
      || false
    );

    if (!type) return;

    event.stopPropagation();
    event.preventDefault();

    const data = clipboardData.getData(type); // event.originalEvent

    let dataModified = data;

    allEmojis.forEach((icon) => {
      dataModified = dataModified.replace(
        new RegExp(icon, 'g'),
        ic => getEmojiHtml(ic)
      );
    });

    addToText(dataModified);
  };

  const imageClicked = (image) => {
    editableElement().focus();

    const imageId = new Date().valueOf();
    // NOTES:
    // 1. prefixing and trailing <p></p> are fixes to allow editing in react-draft-wysiwyg
    // 2. training nmsp is necessary to avoid pasting text before post-image pp
    addToText(`<p></p><img data-type="image" src="${image.url}" class="messageImage" alt="${image.nom_original}" id="${imageId}" /><p></p>&nbsp;`);

    // hack: scroll to editable cursor after added image
    setTimeout(() => {
      const el = document.getElementById(imageId);
      if (el) el.scrollIntoView();
    }, 500);

    hidePopups();
  };

  const handleMenuClick = (event) => {
    if (onMenuClick) {
      onMenuClick(event, event.target.dataset.menuId);
    }

    setMessageAttachMenuVisible(false);
  };

  return (
    <div className={classNames(styles.newMessage, canEdit || styles.cursorNotAllowed)}>
      <div
        className={classNames(
          styles.messageButton,
          styles.attachButton,
          canEdit || styles.cursorNotAllowed
        )}
        onClick={() => setMessageAttachMenuVisible(true)}
      >
        <span className={styles.attachmentPlus}>+</span>
        <FontIcon name="icon-attachment" size="24px" color="#FFFFFF" className={styles.attachmentClip} />
      </div>

      <Menu
        id="message-attach-menu"
        anchorEl={() => document.getElementById('messageAttachMenuStubPositioner')}
        open={canEdit && messageAttachMenuVisible}
        onClose={() => setMessageAttachMenuVisible(false)}
        disableAutoFocusItem
      >
        <SubheaderMenuItem data-menu-id="shareWith">
          { I18n.t('discussions.fileShare.shareWith') }
        </SubheaderMenuItem>
        <MenuItem
          onClick={handleMenuClick}
          data-menu-id="GED"
        >
          { I18n.t('discussions.fileShare.GED') }
        </MenuItem>
        <MenuItem onClick={handleMenuClick} data-menu-id="myComputer">
          { I18n.t('discussions.fileShare.myComputer') }
        </MenuItem>
        {/*
           (add upper: <MenuItem divider)
           <SubheaderMenuItem onClick={handleMenuClick} data-menu-id="new">
           { I18n.t('discussions.fileShare.new') }
           </SubheaderMenuItem>
           <MenuItem onClick={handleMenuClick}>
           { I18n.t('discussions.fileShare.survey') }
           </MenuItem>
           <MenuItem>
           { I18n.t('discussions.fileShare.question') }
           </MenuItem>
         */}
      </Menu>

      <span id="messageAttachMenuStubPositioner" />

      <div
        contentEditable={canEdit}
        id="editable"
        className={styles.editable}
        suppressContentEditableWarning
        onBlur={handleFocusOut}
        onFocus={handleFocusIn}
        onPaste={handlePaste}
        placeholder={placeholder}
        onInput={handleInput}
        onKeyPress={handleKeyPress}
      />

      <div id="smilePopup" className={classNames(styles.smilePopup, styles.hidden)}>
        <div className={styles.smiles}>
          {Emoji[emojiGroupIndex].icons.map(
            (item, i) => (
              <div role="img" aria-label={item} onClick={() => addEmoji(item)} key={`${i}-${item}`}>
                <img src={getEmojiImage(item)} alt={item} />
              </div>
            )
          )}
        </div>
        <div className={styles.smileGroups}>
          { Emoji.map((item, i) => (
            <div
              key={`${i}-${item.group}`}
              className={classNames({ [styles.active]: i === emojiGroupIndex })}
            >
              <img
                src={getEmojiImage(item.group)}
                alt={item.group}
                onClick={() => setEmogiGroupIndex(i)}
              />
            </div>
          )) }
        </div>
      </div>

      <div id="imagePopup" className={classNames(styles.imagePopup, styles.hidden)}>
        <div>
          <input type="text" placeholder={I18n.t('discussions.fileShare.searchInput')} onInput={handleImagePopupFilter} className={styles.searchInput} id="imageFilterInput" />
          <FontIcon
            name={imageFilter && imageFilter.length > 0 ? 'icon-close' : 'icon-search'}
            className={styles.imageSearchIcon}
            onClick={clearImagePopupFilter}
          />
        </div>

        <div className={styles.imagesMenuList}>
          { availableImages.filter(
            item => item.nom_original.toLocaleLowerCase().includes(imageFilter.toLocaleLowerCase())
          ).map((item, i) => (
            <img
              title={item.nom_original}
              alt={item.nom_original}
              src={item.thumbnail || item.url}
              key={`${i}-${item.id_document}`}
              onClick={() => imageClicked(item)}
            />
          ))}
        </div>
      </div>

      <div
        className={classNames(styles.popupCloser, styles.hidden)}
        onClick={popupCloserClicked}
      />

      <div
        className={classNames(styles.messageButton, canEdit || styles.cursorNotAllowed)}
        onMouseDown={canEdit && imagePopupMouseDown}
      >
        <span>GIF</span>
      </div>

      <div className={classNames(styles.messageButton, canEdit || styles.cursorNotAllowed)}>
        <span onMouseDown={canEdit && handleMouseDown} role="img" aria-label="😉" className="icon-smiley" />
      </div>
    </div>
  );
});

NewMessage.propTypes = {
  isEmpty: PropTypes.bool,
  onChange: PropTypes.func,
  onMenuClick: PropTypes.func,
  onImageMenuClick: PropTypes.func,
  onPost: PropTypes.func,
  placeholder: PropTypes.string,
  canEdit: PropTypes.bool,
  initialHTML: PropTypes.instanceOf(Element),
  availableImages: PropTypes.array
};

NewMessage.defaultProps = {
  isEmpty: false,
  onChange: null,
  onMenuClick: null,
  onImageMenuClick: null,
  onPost: null,
  placeholder: null,
  initialHTML: null,
  canEdit: true,
  availableImages: []
};

const NewMessageStyled = withStyles(materialStyle)(NewMessage);

export {
  NewMessageStyled as NewMessage
};
