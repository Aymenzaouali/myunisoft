import React, { useState, memo } from 'react';
import PropTypes from 'prop-types';
import {
  Dialog, Typography, withStyles, TextField
} from '@material-ui/core';
import I18n from 'assets/I18n';
import { InlineButton } from 'components/basics/Buttons';
import Chip from '@material-ui/core/Chip';
import { FontIcon } from 'components/basics/Icon';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import styles from './NewAttachmentDialog.module.scss';

const style = () => ({
  cardAccount: {
    width: 700,
    maxHeight: '100%',
    overflow: 'hidden',
    borderRadius: 2,
    boxShadow: '1px 5px 7px 0px rgba(0,0,0, 0.08)',
    backgroundColor: '#fff',
    paddingLeft: 100,
    paddingRight: 70
  }
});

const NewAttachmentDialog = memo((props) => {
  const {
    classes,
    isOpen,
    onClose,
    onDelete,
    attachments,
    initialText
  } = props;

  if (!isOpen) return null;

  const [text, setText] = useState(initialText);

  const getFormData = () => ({
    text
  });

  return (
    <Dialog classes={{ paper: classes.cardAccount }} aria-labelledby="account" open={isOpen} onClose={() => onClose(null)}>

      <div className={styles.closeButton}>
        <IconButton aria-label="Close" onClick={() => onClose(null)}>
          <CloseIcon />
        </IconButton>
      </div>

      <div className={styles.header}>
        <Typography variant="h1">
          {I18n.t('discussions.newAttachment.title')}
        </Typography>
      </div>

      <form
        id="newAttachmentForm"
        className={styles.form}
      >

        <TextField
          className={styles.textInput}
          id="newDiscussionText"
          label={I18n.t('discussions.newAttachment.messageHint')}
          onChange={event => setText(event.target.value)}
          value={text || ''}
          multiline
          variant="outlined"
        />

        <div className={styles.attachmentList}>
          { attachments.map((item, i) => (
            <div className={styles.attachmentItem} key={`${i}-${item.name}-1`}>
              <Chip
                label={item.name}
                onDelete={() => onDelete(item)}
                deleteIcon={(
                  <span className={styles.deleteIcon}>
                    <FontIcon
                      name="icon-close"
                      color="#0bd1d1"
                    />
                  </span>
                )}
                className={styles.attachment}
                key={`${i}-${item.name}-2`}
              />
            </div>
          ))}
        </div>

        <div className={styles.buttonPanel}>
          <InlineButton
            buttons={[
              {
                _type: 'string',
                text: I18n.t('discussions.newAttachment.cancelButton'),
                size: 'medium',
                style: {
                  color: 'black',
                  backgroundColor: 'initial',
                  border: 'solid 1px'
                },
                onClick: () => onClose(null),
                key: 'cancel'
              },
              {
                _type: 'string',
                text: I18n.t('discussions.newAttachment.createButton'),
                disabled: false,
                size: 'medium',
                onClick: () => onClose(getFormData()),
                key: 'create'
              }]
            }
          />
        </div>
      </form>

    </Dialog>
  );
});


NewAttachmentDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  attachments: PropTypes.array,
  classes: PropTypes.shape({}),
  initialText: PropTypes.string
};

NewAttachmentDialog.defaultProps = ({
  classes: {},
  attachments: [],
  initialText: null
});

export default withStyles(style)(NewAttachmentDialog);
