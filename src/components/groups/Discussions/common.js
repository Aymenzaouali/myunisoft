// room types
export const ROOM_TYPE_PUBLIC = 1;
export const ROOM_TYPE_PRIVATE = 2;
export const ROOM_TYPE_DIRECT = 3;
// no matter ROOM_TYPE_PUBLIC or ROOM_TYPE_PRIVATE
export const ROOM_TYPE_NON_DIRECT = ROOM_TYPE_PUBLIC;

export const formatInitials = user => (
  `${(user.prenom || '').slice(0, 1)}${(user.nom || '').slice(0, 1)}`
);

export const formatUsername = user => [user.prenom, user.nom].filter(Boolean).join(' ');

export const TREEITEM = {
  ROOM: 'room',
  FOLDER: 'folder'
};

// return array of #tags found in html
export function extractTags(html) {
  return (html.match(new RegExp('#[\\w-_]+', 'g')) || []).map(item => item.trim().slice(1));
}

export function htmlToText(html) {
  let r = html.replace(/(<([^>]+)>)/g, ''); // strip all <tags>
  r = r.replace(new RegExp('&\\w+;', 'g'), ' '); // strip &nbsp;
  r = r.replace(new RegExp('&\\w+;', 'g'), ''); // strip others
  return r;
}

// artifisional functions because of weird backend id-based format
export function getTreeItemId(treeItem) {
  return (
    (treeItem.room_id && `room_${treeItem.room_id}`)
      || (treeItem.folder_id && `folder_${treeItem.folder_id}`)
  );
}

// return "unique" DOM element id by params
export const getDOMId = (key, id) => `${key}-${id}`;

export const treeItemDOMPrefix = 'treeItem';

export const deduplicate = array => [...new Set(array)];

export function formatDirectMessageRoomName(participants) {
  return deduplicate(participants.map(
    item => formatUsername(item)
  )).sort(
    (a, b) => a.localeCompare(b)
  ).join(', ');
}
