const generalTree = [
  {
    title: 'Vie du Cabinet',
    id: 'Vie du cabinet',
    createdBy: 'Robert Niro',
    createdAt: '2018-04-23T18:25:43.511Z',
    messages: [
      {
        name: 'Jack Daniels',
        initials: 'J.D.',
        avatar: '/android-icon-36x36.png',
        date: '2019-04-02T18:25:43.511Z',
        body: 'sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation'
      },
      {
        name: 'Norman Smith',
        initials: 'N.S.',
        avatar: '/android-icon-36x36.png',
        date: '2019-04-01T18:25:43.511Z',
        body: 'amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation'
      },
      {
        name: 'Bob Ralph',
        initials: 'B.R.',
        avatar: '/android-icon-36x36.png',
        date: '2019-02-30T18:25:43.511Z',
        body: 'Ok'
      }
    ],
    children: [
      {
        title: 'Compatabilite',
        id: 'Compatabilite',
        canAdd: true,
        children: [
          {
            title: 'Declaration TVA',
            id: 'Declaration TVA',
            locked: true
          },
          {
            title: 'Lorem ipsum',
            id: 'Lorem ipsum',
            canAdd: true
          },
          {
            title: 'Report bilan erreTOO LONG STRING',
            id: 'Report bilan erreTOO LONG STRING'
          },
          {
            title: 'Other',
            id: 'Other',
            children: [
              {
                title: 'ccc1',
                id: 'ccc1'
              },
              {
                title: 'ccc2',
                id: 'ccc2'
              }
            ]
          }
        ]
      },
      {
        title: 'Fiscalite0',
        id: 'Fiscalite0',
        children: [
          {
            title: 'fff1',
            id: 'fff1 1'
          },
          {
            title: 'fff2',
            id: 'fff2 1'
          }
        ]
      },
      {
        title: 'Jusridique',
        id: 'Jusridique',
        children: [
          {
            title: 'fff1',
            id: 'fff1 2'
          },
          {
            title: 'fff2',
            id: 'fff2 2'
          }
        ]
      },
      {
        title: 'Autres',
        id: 'Autres',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 3'
          },
          {
            title: 'fff2',
            id: 'fff2 3'
          }
        ]
      },
      {
        title: 'SomeElse  ',
        id: 'SomeElse  ',
        children: [
          {
            title: 'fff1',
            id: 'fff1 3'
          },
          {
            title: 'fff2',
            id: 'fff2 4'
          }
        ]
      }
    ]
  },
  {
    title: 'Fiscalite is very very long string',
    id: 'Fiscalite is very very long string',
    children: [
      {
        title: 'Compatabilite',
        id: 'Compatabilite',
        canAdd: true,
        children: [
          {
            title: 'Declaration TVA',
            id: 'Declaration TVA',
            locked: true
          },
          {
            title: 'Lorem ipsum3',
            id: 'Lorem ipsum3'
          },
          {
            title: 'Report bilan erreTOO LONG STRING',
            id: 'Report bilan erreTOO LONG STRING'
          }

        ]
      },
      {
        title: 'Fiscalite',
        id: 'Fiscalite',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 4'
          },
          {
            title: 'fff2',
            id: 'fff2 5'
          }
        ]
      },
      {
        title: 'Jusridique',
        id: 'Jusridique',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 5'
          },
          {
            title: 'fff2',
            id: 'fff2 6'
          }
        ]
      },
      {
        title: 'Autres',
        id: 'Autres',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 6'
          },
          {
            title: 'fff2',
            id: 'fff2 7'
          }
        ]
      }
    ]
  },
  {
    title: 'Social123',
    id: 'Social123',
    createdBy: 'Jack Robert',
    createdAt: '2019-03-29T14:25:43.511Z',
    messages: [
      {
        name: 'Selma Zaim',
        initials: 'S.Z',
        avatar: '/android-icon-36x36.png',
        date: '2018-04-23T18:25:43.511Z',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation'
      },
      {
        name: 'Emma Bovary',
        initials: 'E.B',
        avatar: '/android-icon-36x36.png',
        date: '2019-03-25T18:25:43.511Z',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      },
      {
        name: 'Bernard Barnard',
        initials: 'B.B',
        avatar: '/android-icon-36x36.png',
        date: '2019-03-27:25:43.511Z',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation'
      },
      {
        name: 'Hermione Granger',
        initials: 'H.G.',
        avatar: '/android-icon-36x36.png',
        date: '2019-03-28:25:43.511Z',
        body: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.'
      }
    ],
    children: [
      {
        title: 'Compatabilite',
        id: 'Compatabilite',
        canAdd: true,
        children: [
          {
            title: 'Declaration TVA',
            id: 'Declaration TVA',
            locked: true
          },
          {
            title: 'Lorem ipsum4',
            id: 'Lorem ipsum4'
          },
          {
            title: 'Report bilan erre',
            id: 'Report bilan erre'
          }

        ]
      },
      {
        title: 'Fiscalite',
        id: 'Fiscalite',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 7'
          },
          {
            title: 'fff2',
            id: 'fff2 8'
          }
        ]
      },
      {
        title: 'Jusridique',
        id: 'Jusridique',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 8'
          },
          {
            title: 'fff2',
            id: 'fff2 9'
          }
        ]
      },
      {
        title: 'Autres',
        id: 'Autres',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 9'
          },
          {
            title: 'fff2',
            id: 'fff2 10'
          }
        ]
      }
    ]
  },
  {
    title: 'Clients',
    id: 'Clients',
    children: [
      {
        title: 'Compatabilite',
        id: 'Compatabilite',
        canAdd: true,
        children: [
          {
            title: 'Declaration TVA',
            id: 'Declaration TVA',
            locked: true
          },
          {
            title: 'Lorem ipsum5',
            id: 'Lorem ipsum5'
          },
          {
            title: 'Report bilan erre',
            id: 'Report bilan erre'
          }

        ]
      },
      {
        title: 'Fiscalite',
        id: 'Fiscalite',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 10'
          },
          {
            title: 'fff2',
            id: 'fff2 11'
          }
        ]
      },
      {
        title: 'Jusridique',
        id: 'Jusridique',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 11'
          },
          {
            title: 'fff2',
            id: 'fff2 12'
          }
        ]
      },
      {
        title: 'Autres',
        id: 'Autres',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 12'
          },
          {
            title: 'fff2',
            id: 'fff2 13'
          }
        ]
      }
    ]
  }
];

const dossiersTree = [
  {
    title: 'Recci',
    id: 'Recci',
    messages: [
      {
        name: 'Jack Daniels',
        initials: 'J.D.',
        avatar: '/android-icon-36x36.png',
        date: '2019-04-02T18:25:43.511Z',
        body: 'sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation'
      },
      {
        name: 'Norman Smith',
        initials: 'N.S.',
        avatar: '/android-icon-36x36.png',
        date: '2019-04-01T18:25:43.511Z',
        body: 'amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation'
      },
      {
        name: 'Bob Ralph',
        initials: 'B.R.',
        avatar: '/android-icon-36x36.png',
        date: '2019-02-30T18:25:43.511Z',
        body: 'Ok'
      }
    ],
    children: [
      {
        title: 'Compatabilite',
        id: 'Compatabilite',
        canAdd: true,
        children: [
          {
            title: 'Declaration TVA',
            id: 'Declaration TVA',
            locked: true,
            createdBy: 'Tom Abrams',
            createdAt: '2018-04-23T18:25:43.511Z',
            messages: [
              {
                name: 'Jack Robin',
                initials: 'J.R.',
                avatar: '/android-icon-36x36.png',
                date: '2018-01-24T18:25:43.511Z',
                body: `Bonjour M Jhones,<br />
le montant de la TVA de ce mois s'élève a 12500€, sur quelle banque souhaitez vous etre prélevé?`
              },
              {
                name: 'Smith Jhones',
                initials: 'S.J.',
                avatar: '/android-icon-36x36.png',
                date: '2019-01-25T19:25:42.511Z',
                body: `Bonjour,<br />
Sur la BNP,<br />
Merci, Bonne journée`
              }
            ]
          },
          {
            title: 'Lorem ipsum',
            id: 'Lorem ipsum'
          },
          {
            title: 'Report bilan erreTOO LONG STRING',
            id: 'Report bilan erreTOO LONG STRING'
          }
        ]
      },
      {
        title: 'Fiscalite',
        id: 'Fiscalite',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 13'
          },
          {
            title: 'fff2 with children',
            id: 'fff2 14',
            children: [
              {
                title: 'fff1 dfasasdf',
                id: 'fff1 14'
              },
              {
                title: 'fff2 dffdf',
                id: 'fff2 15',
                children: [
                  {
                    title: 'fff1 dasffdsfsd',
                    id: 'fff1 15'
                  },
                  {
                    title: 'fff2 asdfasdfdfask',
                    id: 'fff2 16'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        title: 'Jusridique',
        id: 'Jusridique',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 16'
          },
          {
            title: 'fff2',
            id: 'fff2 17'
          }
        ]
      },
      {
        title: 'Autres',
        id: 'Autres',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1 17'
          },
          {
            title: 'fff2',
            id: 'fff2 18'
          }
        ]
      }
    ]
  },
  {
    title: 'Axa Assurances',
    id: 'Axa Assurances',
    createdBy: 'Abrams Michael',
    createdAt: '2018-04-23T18:25:43.511Z',
    messages: [
      {
        name: 'Jack Johnes',
        initials: 'S.Z',
        avatar: '/android-icon-36x36.png',
        date: '2018-02-23T18:25:43.511Z',
        body: 'amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation'
      },
      {
        name: 'John Smith',
        initials: 'E.B',
        avatar: '/android-icon-36x36.png',
        date: '2019-01-25T18:25:43.511Z',
        body: 'amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      }
    ]
  },
  {
    title: 'Agence A&K',
    id: 'Agence A&K',
    children: [
      {
        title: 'fff1',
        id: 'fff1 18'
      },
      {
        title: 'fff2',
        id: 'fff2 19'
      }
    ]
  }
];

const messagesDirectTree = [
  {
    title: 'Vincent, Paula',
    id: 'Vincent, Paula',
    locked: true
  },
  {
    title: 'Vincent, Selma, Greg',
    id: 'Vincent, Selma, Greg',
    locked: true
  }
];

const files = [
  {
    title: 'pj1.pdf',
    user: 'Selma',
    date: '2019-03-25T18:25:43.511Z',
    name: 'www.example.com/pj1.pdf'
  },
  {
    title: 'facture-22-09-18.jpg',
    user: 'Victor',
    date: '2019-03-25T18:25:43.511Z',
    name: 'www.example.com/facture.pdf'
  }
];

const participants = [
  {
    id: 'selma-zaim',
    name: 'Selma Zaim',
    icon: '/android-icon-36x36.png'
  },
  {
    id: 'victor-kouoi',
    name: 'Victor Kouoi',
    icon: '/android-icon-36x36.png'
  },
  {
    id: 'emma-bovary',
    name: 'Emma Bovary',
    icon: '/android-icon-36x36.png'
  },
  {
    id: 'cyril-mandrilly',
    name: 'Cyril Mandrilly',
    icon: '/android-icon-36x36.png'
  },
  {
    id: 'bill-gates',
    name: 'Bill Gates',
    icon: '/android-icon-36x36.png'
  },
  {
    id: 'bill-clinton',
    name: 'Bill Clinton',
    icon: '/android-icon-36x36.png'
  },
  {
    id: 'bill-layall',
    name: 'Bill Layall',
    icon: '/android-icon-36x36.png'
  },
  {
    id: 'bill-smith',
    name: 'Bill Smith',
    icon: '/android-icon-36x36.png'
  }
];

const tags = [
  'TVA', 'recci', 'compatibilite'
];

const searchOptions = [
  {
    group: 'Tags',
    title: 'TVA',
    subtitle: null,
    id: 100,
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Tags',
    title: 'recci',
    id: 101,
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Tags',
    title: 'comptabilité',
    id: 102,
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Mentions',
    title: 'Diner de Noel',
    subtitle: '2 mentions',
    id: 200,
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Mentions',
    title: 'Petit déjeuner du nouvel an',
    subtitle: '1 mention',
    id: 201,
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Personnes',
    title: 'Anniversaire Hermione',
    subtitle: 'Harry, Ron, Ginny',
    id: 202,
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Personnes',
    id: 'selma-zaim',
    title: 'Selma Zaim',
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Personnes',
    id: 'cyril-mandrilly',
    title: 'Cyril Mandrilly',
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Personnes',
    id: 'bill-gates',
    title: 'Bill Gates2',
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Messages direct',
    title: 'Hermione, Harry',
    subtitle: '8 mentions',
    id: 203,
    icon: '/android-icon-36x36.png'
  },
  {
    group: 'Messages direct',
    title: 'Ron, Ginny',
    subtitle: '3 mentions',
    id: 203,
    icon: '/android-icon-36x36.png'
  }
];

const dummyImages = [
  {
    id: 1,
    title: 'google',
    src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Googleplex_HQ_%28cropped%29.jpg/375px-Googleplex_HQ_%28cropped%29.jpg'

  },
  {
    id: 2,
    title: 'amazon',
    src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Seattle_Spheres_on_May_10%2C_2018.jpg/375px-Seattle_Spheres_on_May_10%2C_2018.jpg'
  },
  {
    id: 3,
    title: 'nature',
    src: 'http://wikilovesearth.org.ua/wp-content/uploads/2014/05/3813924161_cc5328f818_o_small.jpg'
  }
];

export {
  generalTree, dossiersTree, messagesDirectTree, files, participants, tags,
  searchOptions, dummyImages
};
