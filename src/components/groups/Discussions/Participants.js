import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Avatar } from '@material-ui/core';
import { generateColorWithName } from 'common/helpers/colors';
import styles from './Participants.module.scss';

/* eslint-disable react/no-multi-comp */

const ParticipantList = memo(({ children, className, ...props }) => (
  <ul className={classNames(className, styles.participantList)} {...props}>
    {children}
  </ul>
));

ParticipantList.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.shape({}),
    PropTypes.arrayOf(PropTypes.shape({}))
  ]),
  className: PropTypes.string
};

ParticipantList.defaultProps = {
  children: null,
  className: null
};

const Participant = memo(({
  icon, alt, name, nameHTML, className, ...others
}) => (
  <li className={className} {...others}>
    <Avatar /* UserAvatar and SimpleAvatar has too fixed styles */
      alt={alt}
      src={icon}
      className={styles.avatar}
      style={!icon ? { backgroundColor: generateColorWithName(name) } : null}
    >
      {alt}
    </Avatar>

    <div
      className={styles.name}
      dangerouslySetInnerHTML={{ __html: nameHTML || name }} // eslint-disable-line react/no-danger
    />
  </li>
));

Participant.propTypes = {
  icon: PropTypes.string,
  alt: PropTypes.string,
  name: PropTypes.string.isRequired,
  nameHTML: PropTypes.string,
  className: PropTypes.string
};

Participant.defaultProps = {
  className: null,
  icon: null,
  alt: '',
  nameHTML: null
};

export {
  ParticipantList, Participant
};
