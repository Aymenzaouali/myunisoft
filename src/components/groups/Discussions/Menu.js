import React, { memo } from 'react';
import PropTypes from 'prop-types';
import MaterialMenuItem from '@material-ui/core/MenuItem';
/* eslint-disable react/destructuring-assignment */

const subheaderMenuItemStyle = {
  opacity: 1,
  fontFamily: 'basier_circlemedium',
  fontSize: 11,
  fontWeight: 500,
  paddingTop: 5,
  paddingBottom: 0
};

const SubheaderMenuItem = memo(({ children, ...props }) => (
  <MaterialMenuItem style={subheaderMenuItemStyle} {...props} disabled>
    { children }
  </MaterialMenuItem>
));

SubheaderMenuItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.shape({}),
    PropTypes.string
  ])
};

SubheaderMenuItem.defaultProps = {
  children: {}
};

// https://stackoverflow.com/a/43839719/1948511
const MenuItem = props => (
  <MaterialMenuItem
    onMouseEnter={(e) => { e.target.style.background = '#ddfafa'; }}
    onMouseLeave={(e) => { e.target.style.background = 'unset'; }}
    {...props}
  >
    { props.children }
  </MaterialMenuItem>
);

MenuItem.propTypes = {
  children: PropTypes.shape({}).isRequired
};


export {
  SubheaderMenuItem,
  MenuItem
};
