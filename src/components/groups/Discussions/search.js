import React, { memo } from 'react';
import PropTypes from 'prop-types';
import {
  Dialog, withStyles, IconButton, DialogContent
} from '@material-ui/core';
import I18n from 'assets/I18n';
import { Field } from 'redux-form';
import CloseIcon from '@material-ui/icons/Close';
import AutoComplete from 'components/basics/Inputs/AutoComplete';
import styles from './search.module.scss';

const style = () => ({
  paper: {
    boxSizing: 'border-box',
    width: 700,
    maxWidth: 700,
    height: 600,
    margin: 0,
    overflow: 'hidden',
    borderRadius: 2,
    boxShadow: '1px 5px 7px 0px rgba(0,0,0, 0.08)',
    backgroundColor: '#fff',
    paddingLeft: 125,
    paddingRight: 125
  }
});

const Search = memo((props) => {
  const {
    onSearch,
    isOpen,
    onClose,
    options
  } = props;

  const handleClose = () => {
    onClose();
  };

  const getGroupedOptionsByFilter = (filterText = '') => {
    const filteredOptions = options.filter(
      item => item.title.toLocaleLowerCase().includes(filterText.toLocaleLowerCase())
    ).slice(0, 30);

    const groupedOptions = (filteredOptions || []).reduce(
      (grouped, option) => {
        const groupName = option.group;

        if (!grouped[groupName]) {
          grouped[groupName] = {      // eslint-disable-line
            label: groupName,
            options: []
          };
        }

        if (option.title.toLocaleLowerCase().includes(filterText.toLocaleLowerCase())) {
          grouped[groupName].options.push({
            ...option,
            label: option.title,
            value: option.id
          });
        }
        return grouped;
      },
      {}
    );

    return groupedOptions;
  };

  const filterOptions = (inputValue) => {
    const r = getGroupedOptionsByFilter(inputValue);
    return Object.values(r);
  };

  const loadAsyncOptions = (inputValue, callback) => {
    callback(filterOptions(inputValue));
  };

  const handleChangeValue = (value) => {
    if (onSearch) onSearch(value);
  };

  const renderOptions = () => (
    <AutoComplete
      className={styles.textInput}
      autoFocus
      menuPosition="fixed"

      /* options={Object.values(groupedOptions)} */
      isAsync
      cacheOptions
      loadOptions={loadAsyncOptions}
      defaultOptions

      optionRender={
        option => (
          <span className={styles.dropdownOption}>
            {option.data.icon && <img src={option.data.icon} alt="" /> }
            <div className={styles.personInfo}>
              <div className={styles.name}>{option.data.title}</div>
              <div>{option.data.subtitle}</div>
            </div>
          </span>
        )
      }
      optionStyle={option => ({
        padding: 10,
        ...option.isFocused && { backgroundColor: '#ddfafa' },
        minHeight: 28

      })}
      selectStyles={{
        menuList: { height: 240 }
      }}
      placeholder={I18n.t('discussions.search.searchPlaceholder')}
      onChangeValues={handleChangeValue}
    />
  );

  return (
    <Dialog classes={{ paper: styles.dialog }} aria-labelledby="search" open={isOpen} onClose={handleClose}>
      <div className={styles.closeButton}>
        <IconButton aria-label="Close" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </div>

      <div className={styles.header}>
        {I18n.t('discussions.search.title')}
      </div>
      <DialogContent>
        <form className={styles.form}>
          <Field
            color="primary"
            className={styles.textInput}
            name="rechercher.participants"
            component={renderOptions}
            label={I18n.t('discussions.search.searchInDiscussions')}
          />
        </form>
      </DialogContent>
    </Dialog>
  );
});


Search.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onSearch: PropTypes.func,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.shape({}),
  options: PropTypes.arrayOf(
    PropTypes.shape({
      group: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      title: PropTypes.string,
      subTitle: PropTypes.string,
      icon: PropTypes.string
    })
  ).isRequired
};

Search.defaultProps = ({
  classes: {},
  onSearch: null
});

export default withStyles(style)(Search);
