import React, { Component } from 'react';
import { Typography, LinearProgress } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import I18n from 'i18next';

import { ocrFilesTableHeaderKeys, ebicsFilesTableHeaderKeys } from 'assets/constants/keys';

import Button from 'components/basics/Buttons/Button';
import FactoryTable from 'components/groups/Tables/Factory';

import './styles.scss';

/**
 * Component for displaying informations about OCR files upload
 */
class FilesInformations extends Component {
  handleCheckboxHeaderOnChange = this.handleCheckboxHeaderOnChange.bind(this);

  state = {
    selectedFileIds: []
  }

  /**
   * Handle send button click event, call handleSendFiles (redux actions)
   * with selectedFileIds param, and re-init selectedFileIds array
   */
  handleSendButtonOnClick = async (noOCR = false) => {
    const { handleSendFiles } = this.props;
    const { selectedFileIds } = this.state;

    await handleSendFiles(selectedFileIds, noOCR);
    this.setState({ selectedFileIds: [] });
  }

  /**
   * Handle change event on table row(file) checkbox,
   * push/remove file document_id from selectedFileIds state array
   * @param {Event} fileId
   */
  handleFileCheckboxOnChange(fileId) {
    return (ev) => {
      const { checked } = ev.target;

      this.setState(({ selectedFileIds }) => ({
        selectedFileIds: checked
          ? [...selectedFileIds, fileId]
          : selectedFileIds.filter(document_id => document_id !== fileId)
      }));
    };
  }

  /**
   * Handle change event on table header checkbox,
   * push/remove all file document_id from selectedFileIds state array
   * @param {Event} ev
   */
  handleCheckboxHeaderOnChange(ev) {
    const { files } = this.props;
    this.setState({
      selectedFileIds: ev.target.checked
        ? files.filter(f => f.status !== 'success').map(f => f.document_id)
        : []
    });
  }

  render() {
    const { selectedFileIds } = this.state;
    const {
      files,
      handleDeleteFile,
      handleSendFiles,
      isCollab,
      ocrMode
    } = this.props;

    const tableHeaderCells = ocrMode ? ocrFilesTableHeaderKeys : ebicsFilesTableHeaderKeys;

    const reversedFiles = [...files].reverse();
    const availableFiles = files.filter(file => file.status !== 'success');

    return (
      <div className="files-informations">
        <Typography variant="h1" gutterBottom>
          {I18n.t('OCR.filesInformations.title')}
        </Typography>
        <FactoryTable
          className="files-informations__table"
          param={{
            header: {
              row: [{
                keyRow: 'file-informations-table-header-row',
                value: [
                  {
                    keyCell: 'files-informations-table-header-cell-select-all-checkbox',
                    _type: 'checkbox',
                    cellProp: { padding: 'checkbox' },
                    props: {
                      color: 'primary',
                      checked: selectedFileIds.length === availableFiles.length,
                      onChange: this.handleCheckboxHeaderOnChange
                    }
                  },
                  (isCollab ? {
                    _type: 'string',
                    keyCell: 'files-informations-table-header-cell-society-name',
                    props: { label: I18n.t('OCR.filesInformations.societyName') }
                  } : null),
                  ...tableHeaderCells.map(({ label }) => ({
                    _type: 'string',
                    keyCell: `files-informations-table-header-cell-${label}`,
                    props: { label: I18n.t(label) }
                  })),
                  { _type: 'string', keyCell: 'files-informations-table-header-cell-send-back-link', cellProps: { padding: 'none' } },
                  { _type: 'string', keyCell: 'files-informations-table-header-cell-delete-file', cellProps: { padding: 'none' } }
                ].filter(v => v)
              }]
            },
            body: {
              row: reversedFiles.map(file => ({
                keyRow: `files-informations-table-body-row-${file.document_id}`,
                props: {
                  className: `files-informations__table__body-row${file.status === 'error' ? '_error' : ''}`
                },
                value: [
                  {
                    keyCell: `files-informations-table-body-cell-checkbox-${file.document_id}`,
                    _type: (file.status === 'success') ? null : 'checkbox', // TODO: a voir si faut vraiment faire comme demande MYUN-3678
                    cellProp: { padding: 'checkbox' },
                    props: {
                      color: 'primary',
                      checked: selectedFileIds.includes(file.document_id),
                      onChange: this.handleFileCheckboxOnChange(file.document_id)
                    }
                  },
                  (isCollab ? {
                    _type: 'string',
                    keyCell: 'files-informations-table-body-cell-society-name',
                    props: { label: file.society.name || file.society.label }
                  } : null),
                  ...tableHeaderCells.map(c => ({
                    _type: 'string',
                    keyCell: `files-informations-table-body-cell-${c.label}-${file.document_id}`,
                    cellProp: {
                      className: `files-informations__table__body-row__cell-${c.field}`
                    },
                    props: {
                      label: (!c.isI18nKey && get(file, c.field))
                        || ((typeof get(file, c.field) !== 'number')
                              && (file.status === 'success' && file.withoutOCR && c.field === 'status'
                                ? I18n.t('OCR.filesInformations.successWithoutOCR')
                                : I18n.t(`OCR.filesInformations.${get(file, c.field)}`)))
                        || <LinearProgress variant="determinate" value={get(file, c.field)} />
                    }
                  })),
                  {
                    keyCell: `files-informations-table-body-cell-send-back-link-${file.document_id}`,
                    _type: null,
                    children: file.status === 'error' && (
                      <span
                        role="presentation"
                        className="files-informations__table__body-row__send-back-link"
                        onClick={handleSendFiles.bind(null, [file.document_id])}
                      >
                        {I18n.t('OCR.filesInformations.sendBack')}
                      </span>
                    )
                  },
                  {
                    keyCell: `files-informations-table-body-cell-delete-file-${file.document_id}`,
                    _type: null,
                    cellProps: { padding: 'none' },
                    children:
                      file.status !== 'success'
                        && (
                          <DeleteIcon
                            color="error"
                            className="files-informations__table__body-row__delete-icon"
                            onClick={() => handleDeleteFile(file.document_id || file.id)}
                          />
                        )
                  }
                ].filter(v => v)
              }))
            }
          }}
        />
        {ocrMode
        && (
          <div className="files-information__buttons">
            <Button variant="contained" color="primary" className="files-information__buttons__item" onClick={() => this.handleSendButtonOnClick(true)}>
              {I18n.t('OCR.filesInformations.buttonWithoutOCR')}
            </Button>
            <Button variant="contained" color="primary" className="files-information__buttons__item" onClick={() => this.handleSendButtonOnClick()}>
              {I18n.t('OCR.filesInformations.buttonOCR')}
            </Button>
          </div>
        )
        }
      </div>
    );
  }
}

const filePropTypes = PropTypes.shape({
  blob: PropTypes.instanceOf(File).isRequired,
  status: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  type: PropTypes.string.isRequired,
  format: PropTypes.string.isRequired,
  document_id: PropTypes.number.isRequired
});

FilesInformations.defaultProps = {
  ocrMode: false
};

FilesInformations.propTypes = {
  isCollab: PropTypes.bool.isRequired,
  files: PropTypes.arrayOf(filePropTypes).isRequired,
  handleDeleteFile: PropTypes.func.isRequired,
  handleSendFiles: PropTypes.func.isRequired,
  ocrMode: PropTypes.bool
};

export default FilesInformations;
