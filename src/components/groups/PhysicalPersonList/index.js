import React from 'react';
import PropTypes from 'prop-types';
import PhysicalPersonListTable from 'containers/groups/Tables/PhysicalPersonList';
import PhysicalPersonListTableFilter from 'containers/groups/Filters/PhysicalPersonList';
import PhysicalPersonCreation from 'containers/groups/PhysicalPersonCreation';
import { ConfirmationDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import List from 'containers/groups/List';

class physicalPersonList extends React.PureComponent {
  componentWillUnmount() {
    const { resetSelectedPersPhysique } = this.props;
    resetSelectedPersPhysique();
  }

  renderEditableContent = () => {
    const { selectedPersPhysique } = this.props;

    return selectedPersPhysique && (
      <PhysicalPersonCreation
        title={I18n.t('physicalPersonCreation.editPersonTitle', { personName: `${selectedPersPhysique.firstname} ${selectedPersPhysique.name}` })}
      />
    );
  };

  render() {
    const {
      onAddNewPhysicalPerson,
      deletionInformation: { mode, id },
      deleteAndClose,
      setDeletionDialog
    } = this.props;
    return (
      <div>
        <List
          actionButtonProps={{
            onClick: () => onAddNewPhysicalPerson(),
            label: I18n.t('physicalPersonList.addNewPhysicalPerson')
          }}
          filters={<PhysicalPersonListTableFilter />}
          listTable={<PhysicalPersonListTable />}
          editableContent={this.renderEditableContent()}
        />
        <ConfirmationDialog
          isOpen={mode === 'physicalPerson'}
          title={I18n.t('crm.dialog.title.physicalPerson')}
          onValidate={() => deleteAndClose(id)}
          onClose={() => setDeletionDialog()}
        />
      </div>
    );
  }
}

physicalPersonList.defaultProps = {
  selectedPersPhysique: {}
};

physicalPersonList.propTypes = {
  resetSelectedPersPhysique: PropTypes.func.isRequired,
  onAddNewPhysicalPerson: PropTypes.func.isRequired,
  selectedPersPhysique: PropTypes.shape({
    pers_physique_id: PropTypes.number,
    name: PropTypes.string,
    firstname: PropTypes.string,
    maiden_name: PropTypes.string,
    date_birth: PropTypes.string,
    city_birth: PropTypes.string,
    department_birth: PropTypes.string,
    country_birth: PropTypes.string,
    social_security_number: PropTypes.string,
    comment: PropTypes.string,
    organism: PropTypes.string,
    Actif_: PropTypes.bool,
    country_address: PropTypes.string,
    address_number: PropTypes.string,
    address_bis: PropTypes.string,
    address: PropTypes.string,
    address_complement: PropTypes.string,
    postal_code: PropTypes.string,
    city: PropTypes.string,
    civility: PropTypes.string,
    marital_situation: PropTypes.string,
    matrimonial_regime: PropTypes.string,
    physical_person_type: {
      id: PropTypes.number,
      label: PropTypes.string,
      value: PropTypes.string
    },
    road_type: {
      id: PropTypes.number,
      label: PropTypes.string,
      value: PropTypes.string
    },
    coordonnee: PropTypes.arrayOf(PropTypes.shape({}))
  }),
  deletionInformation: PropTypes.object.isRequired,
  deleteAndClose: PropTypes.func.isRequired,
  setDeletionDialog: PropTypes.func.isRequired
};

export default physicalPersonList;
