import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography, List, ListItem, Card,
  withStyles
} from '@material-ui/core';

import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';

const styles = {
  root: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: '12px'
  },
  title: {
    marginTop: '12px',
    marginLeft: '20px'
  },
  overflow: {
    overflow: 'scroll'
  },
  list: {
    marginLeft: '30px'
  },
  category: {
    display: 'flex',
    flexDirection: 'row',
    flex: 1
  },
  categoryName: {
    marginBottom: '20px',
    marginTop: '20px',
    fontFamily: 'basier_circlemedium',
    cursor: 'pointer'
  },
  button: {
    minWidth: '20px',
    minHeight: '0px',
    maxHeight: '20px',
    boxShadow: 'none',
    marginTop: '20px',
    marginRight: '10px',
    padding: '1px'
  },
  combination: {
    width: '60px',
    fontFamily: 'basier_circlesemibold',
    color: '#000',
    marginRight: '40px'
  },
  name: {
    fontFamily: 'basier_circleregular',
    color: '#000',
    justifyContent: 'center'
  }
};

class ShortcutsList extends React.Component {
  state = {
    categoryOpened: []
  };

  handleToggleShortcut = (categoryName) => {
    const {
      categoryOpened
    } = this.state;

    const updateIsOpen = categoryOpened.find(category => category === categoryName)
      ? categoryOpened.filter(item => item !== categoryName)
      : [...categoryOpened, categoryName];

    this.setState({
      categoryOpened: updateIsOpen
    });
  };

  render() {
    const {
      shortcutsTab,
      hiddenShortCuts,
      classes
    } = this.props;

    const {
      categoryOpened
    } = this.state;
    return (
      <Card classes={{ paper: classes.container, root: classes.overflow }}>
        <Typography classes={{ root: classes.title }} variant="h1">
          {I18n.t('drawer.shortcuts')}
        </Typography>
        <List classes={{ root: classes.list }}>
          {shortcutsTab.map(item => item.shortcuts.length > 0 && !hiddenShortCuts.find(path => path === `${item.name}`) && (
            <div>
              <div className={classes.category}>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={() => this.handleToggleShortcut(item.name)}
                  classes={{ root: classes.button }}
                  style={{
                    position: 'relative'
                  }}
                >
                  {categoryOpened.find(category => category === item.name) ? '-' : '+'}
                </Button>
                <Typography
                  classes={{ root: classes.categoryName }}
                  onClick={() => this.handleToggleShortcut(item.name)}
                >
                  {I18n.t(`shortcuts.${item.name}.title`)}
                </Typography>
              </div>
              {item.shortcuts.map(shortcut => (
                categoryOpened.find(category => category === item.name)
                && !hiddenShortCuts.find(path => path === `${item.name}.${shortcut.name}`)
                && (
                  <ListItem>
                    <Typography
                      classes={{ root: classes.combination }}
                    >
                      {shortcut.combination}
                    </Typography>
                    <Typography classes={{ root: classes.listItem }}>{I18n.t(`shortcuts.${item.name}.${shortcut.name}`)}</Typography>
                  </ListItem>
                )
              ))}
            </div>
          ))}
        </List>
      </Card>
    );
  }
}

ShortcutsList.propTypes = {
  shortcutsTab: PropTypes.arrayOf({}).isRequired,
  hiddenShortCuts: PropTypes.arrayOf(PropTypes.string),
  classes: PropTypes.shape({}).isRequired
};

ShortcutsList.defaultProps = {
  hiddenShortCuts: []
};

export default withStyles(styles)(ShortcutsList);
