import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Shortcuts } from 'react-shortcuts/lib';
import { Typography, IconButton } from '@material-ui/core';
import { verifyDebitCredit } from 'helpers/validation';
import { getSelectedMonth, getSelectedYear } from 'helpers/date';

import I18n from 'assets/I18n';

import NewAccountingTable from 'containers/groups/Tables/NewAccounting';
import AutoReverseDialog from 'containers/groups/Dialogs/AutoReverseDialog';

import { InlineButton } from 'components/basics/Buttons';
import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';
import _ from 'lodash';

import styles from './newAccounting.module.scss';

class NewAccounting extends Component {
  constructor(props) {
    super(props);

    // TODO REMOVE THIS ON NEXT REFACTORING
    this.state = {
      isPostingData: false
    };
  }

  componentDidMount() {
    const {
      form, initialValues, setCurrentForm, getWorksheetType
    } = this.props;

    getWorksheetType();

    // TODO: Why this condition?
    // if (!dirty) {
    setCurrentForm(form, initialValues);
    // }
  }

  componentDidUpdate(prevProps) {
    const {
      form, lastEntry, initializeEntryForm,
      diaries,
      formValues: { entry_id, diary }
    } = this.props;

    const { formValues: { entry_id: prevEntryId } } = prevProps;

    /* TODO this should be refacting in order to get
      the value from default filter i think when create and not edit mode */
    if (((entry_id !== prevEntryId) || (!entry_id && !diary)) && !_.isEmpty(lastEntry)) {
      const lastEntryDate = _.get(lastEntry, 'date_piece', '');
      const lastEntryDiaryId = _.get(lastEntry, 'diary.id');
      const lastEntryDiary = diaries.find(({ diary_id }) => diary_id === lastEntryDiaryId) || {};

      initializeEntryForm(form, {
        diary: {
          ...lastEntryDiary,
          value: lastEntryDiaryId,
          label: lastEntryDiary.name
        },
        month: getSelectedMonth(lastEntryDate),
        year: getSelectedYear(lastEntryDate)
      });
    }
  }

  render() {
    const {
      handleSubmit,
      onSubmitEntry,
      reset,
      onValidateFlux,
      typeEntry,
      customClose,
      title,
      iconClose,
      disableRefresh,
      disableAutocompleteCounterpart,
      form,
      closeDialog,
      onSubmitAutoReverseDialog,
      formValues,
      redirectChartAccount,
      enableCreateCounterPartConfirmation,
      enableLinkDiaryAccountPermission,
      enableCounterPartInformation,
      societyId,
      getDiaryDetail,
      isModification,
      showSplitToolbar,
      master_id,
      args,
      initFieldForBankLinkSplit
    } = this.props;

    const autoCompleteIsDisabled = disableAutocompleteCounterpart();

    const checkPermissionToAutofill = async (diaryCode, selectedAccount) => {
      const data = await getDiaryDetail(parseInt(diaryCode, 10));
      // undefined (because no account defined) , object (because fallback)
      const isSameAccount = _.get(data, '[0].account.id') === _.get(selectedAccount, 'id', {});

      return isSameAccount;
    };

    const handleCounterPart = async (shouldShowConfirmation = false) => {
      const {
        formValues: {
          diary: {
            diary_type_code,
            code: diaryCode,
            account: diaryAccount
          },
          entry_list
        },
        counterPartLines,
        selectAccount,
        getAccountDetail
      } = this.props;

      const { counterpart_account, account_id, account } = entry_list[0];

      // Do not apply if no account is defined
      if (!account) {
        return false;
      }

      // Do not apply if more than one row
      if (entry_list.length > 1) {
        if (shouldShowConfirmation) {
          // Open popin that show that it cannot apply counterpart
          enableCounterPartInformation(true);
        }

        return false;
      }

      // Logic if BANQUE or CAISSE diary
      if ((diary_type_code === 'BQ' || diary_type_code === 'CAISSE')) {
        // eslint-disable-next-line max-len
        // Get diary detail from back-end and check if passed journal CODE  and account is the same from the passed account (also autofill diary form in API response...)
        const isAble = await checkPermissionToAutofill(diaryCode, diaryAccount);
        // if is allow to autofill
        if (isAble && (master_id !== 'bankLink')) {
          // Apply counter part
          await counterPartLines();

          return true;
        }
        if (shouldShowConfirmation) {
          // Show permission popin (linkDiaryAccountDialog)
          enableLinkDiaryAccountPermission(true);
        }

        return false;
      }

      // logic if diary type is ACHAT or diary type is VENTE
      if ((diary_type_code === 'ACH' || diary_type_code === 'VTE')) {
        if (counterpart_account) {
          // Apply counterparlines
          await counterPartLines();
          return true;
        }

        if (shouldShowConfirmation) {
          enableCreateCounterPartConfirmation(true);
        }

        return false;
      }

      // Logic if no contrepartie defined
      if (!counterpart_account && shouldShowConfirmation) {
        // select account in diary ...
        selectAccount(account_id);
        // get account detail ...
        await getAccountDetail();
        // Show noCounterPartDialog popin
        enableCreateCounterPartConfirmation(true);
      }

      // Do not applied by default
      return false;
    };

    const save = async () => {
      if (master_id && master_id === 'bankLink') {
        const callBack = _.get(args, 'getBankLink', () => {});
        handleSubmit(value => onSubmitEntry(value, callBack))();
        initFieldForBankLinkSplit();
      } else {
        handleSubmit(value => onSubmitEntry(value, customClose))();
      }
    };

    const confirm = async () => {
      handleSubmit(value => onSubmitAutoReverseDialog(value, customClose))();
    };

    const valid = async () => {
      if (master_id && master_id === 'bankLink') {
        const callBack = _.get(args, 'getBankLink', () => {});
        handleSubmit(value => onValidateFlux(value, callBack))();
        initFieldForBankLinkSplit();
      } else {
        handleSubmit(value => onValidateFlux(value))();
      }
    };

    const close = () => {
      reset();
      customClose();
      if (master_id && master_id === 'bankLink') {
        initFieldForBankLinkSplit();
      }
    };

    const onAutoreverseDialogClose = () => {
      close();
      closeDialog();
    };

    const handleShortCut = (action) => {
      switch (action) {
      case 'SUBMIT_ENTRY':
        save();
        break;
      case 'CANCEL_ENTRY':
        close();
        break;
      default:
        break;
      }
    };

    const isSaveButtonEnable = (typeEntry === 'o' || typeEntry === 'ib' || typeEntry === 'p');

    let buttons = [
      {
        _type: 'string',
        text: I18n.t('newAccounting.cancel'),
        size: 'medium',
        color: 'default',
        onClick: close
      },
      {
        _type: 'string',
        text: isSaveButtonEnable ? I18n.t('newAccounting.save') : I18n.t('newAccounting.validate'),
        size: 'medium',
        disabled: isSaveButtonEnable ? (!isModification || !verifyDebitCredit(_.get(formValues, 'entry_list', []))) : !verifyDebitCredit(_.get(formValues, 'entry_list', [])),
        onClick: typeEntry === 'ext' ? valid : save,
        id: 'newEntryValidateButton'
      }
    ];

    if (isSaveButtonEnable) {
      buttons = [...buttons, {
        _type: 'string',
        text: I18n.t('newAccounting.validate'),
        size: 'medium',
        disabled: !verifyDebitCredit(_.get(formValues, 'entry_list', [])),
        onClick: valid
      }];
    }
    return (
      <div className={styles.container} id={`${societyId}newAccounting`}>
        <div className={styles.header}>
          <Typography className={styles.title} variant="h1">{title}</Typography>
          { !disableRefresh && (
            <IconButton
              color="primary"
              className={styles.noPadding}
              onClick={async () => {
                const { isPostingData } = this.state;

                if (isPostingData) {
                  return;
                }

                this.setState({
                  isPostingData: true
                });

                const validateButton = document.getElementById('newEntryValidateButton'); // TODO : use createref instead
                const counterPartApplied = await handleCounterPart(true);
                if (counterPartApplied) {
                  validateButton.focus();
                }

                this.setState({
                  isPostingData: false
                });
              }}
              disabled={autoCompleteIsDisabled}
            >
              <span className="icon-autosaisie" />
            </IconButton>
          ) }
          <InlineButton buttons={[
            {
              _type: 'icon',
              iconName: 'icon-account-list',
              iconSize: 32,
              onClick: redirectChartAccount,
              titleInfoBulle: I18n.t('tooltips.chartOfAccounts')
            }
          ]}
          />
          { iconClose && (
            <IconButton color="primary" className={styles.noPadding} onClick={close}>
              <span className="icon-close" />
            </IconButton>
          ) }
          {showSplitToolbar && <SplitToolbar />}
        </div>
        <div className={styles.overflowContained}>
          <Shortcuts
            name="NEW_ACCOUNTING"
            handler={handleShortCut}
            alwaysFireHandler
            stopPropagation={false}
            preventDefault={false}
            targetNodeSelector=".pageContent"
          >
            <NewAccountingTable
              formName={form}
              typeEntry={typeEntry}
              handleCounterPart={handleCounterPart}
              splitArgs={args || {}}
              master_id={master_id || ''}
            />
          </Shortcuts>
        </div>
        <div className={styles.buttonBar}>
          <InlineButton buttons={buttons} />
        </div>
        <AutoReverseDialog
          onValidate={confirm}
          onClose={onAutoreverseDialogClose}
        />
      </div>
    );
  }
}

NewAccounting.propTypes = {
  getWorksheetType: PropTypes.func.isRequired,
  societyId: PropTypes.string.isRequired,
  form: PropTypes.string.isRequired,
  // dirty: PropTypes.bool.isRequired,
  formValues: PropTypes.shape({}),
  initialValues: PropTypes.shape({}).isRequired,
  setCurrentForm: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onSubmitEntry: PropTypes.func.isRequired,
  onValidateFlux: PropTypes.func.isRequired,
  redirectChartAccount: PropTypes.func.isRequired,
  enableCreateCounterPartConfirmation: PropTypes.func.isRequired,
  enableLinkDiaryAccountPermission: PropTypes.func.isRequired,
  enableCounterPartInformation: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  typeEntry: PropTypes.string,
  customClose: PropTypes.func,
  title: PropTypes.string,
  iconClose: PropTypes.bool,
  disableRefresh: PropTypes.bool,
  disableAutocompleteCounterpart: PropTypes.func.isRequired,
  closeDialog: PropTypes.func.isRequired,
  onSubmitAutoReverseDialog: PropTypes.func.isRequired,
  lastEntry: PropTypes.object,
  initializeEntryForm: PropTypes.func.isRequired,
  getDiaryDetail: PropTypes.func.isRequired,
  diaries: PropTypes.array,
  isModification: PropTypes.string.isRequired,
  counterPartLines: PropTypes.func.isRequired,
  selectAccount: PropTypes.func.isRequired,
  getAccountDetail: PropTypes.func.isRequired,
  showSplitToolbar: PropTypes.bool,
  master_id: PropTypes.string,
  initFieldForBankLinkSplit: PropTypes.func,
  args: PropTypes.shape({})
};

NewAccounting.defaultProps = {
  typeEntry: 'e',
  customClose: () => {},
  title: I18n.t('newAccounting.newAccounting'),
  iconClose: false,
  disableRefresh: false,
  formValues: {},
  lastEntry: {},
  diaries: [],
  showSplitToolbar: true,
  master_id: '',
  initFieldForBankLinkSplit: () => {},
  args: {}
};

export default NewAccounting;
