import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Typography } from '@material-ui/core';
import I18n from 'assets/I18n';
import StepperHeader from 'containers/groups/Headers/CompanyCreationStepper';
import Exercises from 'containers/groups/Forms/Exercises';
import GeneralInfo from 'components/groups/Forms/GeneralInfo';
import { InlineButton } from 'components/basics/Buttons';
import DossierFiscal from 'components/groups/Forms/DossierFiscal';
import { ConfirmationDialog } from 'components/groups/Dialogs';
import ClientUser from 'containers/groups/Forms/ClientUser';
// Pour la version suivante
// import Establishments from 'containers/groups/Forms/Establishments';
import Associates from 'containers/groups/Forms/Associates';
import Subsidiaries from 'containers/groups/Forms/Subsidiaries';
// Pour la version suivante
// import EvaluationsElements from 'components/groups/Forms/EvaluationsElements';
// import Buildings from 'containers/groups/Forms/Buildings';

import styles from './CompanyCreation.module.scss';

// TODO: Map by keys instead
const companyCreationStepComponentMap = {
  0: (society_id, selectedCompany, getDataBySiren) => (
    <GeneralInfo getDataBySiren={getDataBySiren} selectedCompany={!!selectedCompany} />
  ),
  1: () => <Exercises />,
  2: () => <DossierFiscal />,
  3: society_id => <Associates society_id={society_id} />,
  4: () => <Subsidiaries />,
  5: () => <ClientUser />,
  6: () => (
    <div className={styles.comingSoon}>
      Bientôt disponible
    </div>
  ), // Pour la version suivante <Establishments />,
  7: () => (
    <div
      className={styles.comingSoon}
    >
      Bientôt disponible
    </div>
  ), // Pour la version suivante <Buildings />
  8: () => (
    <div className={styles.comingSoon}>
      Bientôt disponible
    </div>
  ), // Pour la version suivante <EvaluationsElements />
  9: () => <div>SOME STUFF7</div>
};

class CompanyCreation extends PureComponent {
  state = {
    isDialogOpen: false
  };

  componentDidMount() {
    const { selectTab, getAccountingPlans } = this.props;
    // Open first tab by default
    selectTab(0);
    getAccountingPlans();
  }

  componentWillUnmount() {
    const { selectCompany } = this.props;
    selectCompany();
  }

  onBlur = async () => {
    const { siren } = this.props;
    const numberRegex = (/^\d+$/);
    const { getDataBySiren, updateForm, setExistingCompany } = this.props;

    const notANumber = !numberRegex.test(siren);
    const incorrectLength = !(siren.length === 9 || siren.length === 14);
    try {
      if (!incorrectLength && !notANumber) {
        const data = await getDataBySiren(siren);
        await updateForm(data);
      }
    } catch (error) {
      const code = _.get(error, 'response.data.code', false) || _.get(error, 'data.code', false) || _.get(error, 'code', '');
      switch (code) {
      case 'AlreadyExistError':
        const society_id = _.get(error, 'response.data.id_societe', null);
        setExistingCompany(society_id);
        this.setState({ isDialogOpen: true });
        break;
      default:
        break;
      }
    }
  };

  onValidate = async () => {
    const {
      resetFormAndSetSociety, form, existingCompany
    } = this.props;
    try {
      await resetFormAndSetSociety(form, existingCompany);
      this.onCloseDialog();
    } catch (error) {
      console.log('ERROR ON GETTING COMPANY', error); //eslint-disable-line
      this.onCloseDialog();
    }
  };

  onCloseDialog = () => this.setState({ isDialogOpen: false });

  render() {
    const {
      selectTab,
      activeTab,
      handleSubmit,
      validateForm,
      error,
      nextStep,
      cancelForm,
      title,
      goToStep,
      society_id,
      isError,
      selectedCompany
    } = this.props;

    const { isDialogOpen } = this.state;

    return (
      <div className={styles.globalContainer}>
        <StepperHeader
          title={(selectedCompany && selectedCompany.society_id) ? I18n.t('companyCreation.editCompanyTitle', {
            companyName: selectedCompany.name
          }) : title}
          openTab={selectTab}
          onClose={cancelForm}
          goToStep={(id) => {
            handleSubmit(values => goToStep(values, id))();
          }}
        />
        {error && <Typography className={styles.error} variant="h6" color="error">{error}</Typography>}
        <div className={styles.bodyContainer}>
          {companyCreationStepComponentMap[activeTab](society_id, selectedCompany, this.onBlur)}
        </div>
        <div className={styles.buttonContainer}>
          <div>
            <InlineButton
              marginDirection="right"
              buttons={[
                {
                  _type: 'string',
                  text: I18n.t('companyCreation.previous'),
                  onClick: () => selectTab(activeTab - 1),
                  size: 'large',
                  color: '#d0d0d0',
                  disabled: activeTab === 0
                },
                {
                  _type: 'string',
                  text: I18n.t('companyCreation.next'),
                  onClick: !isError && handleSubmit(nextStep),
                  size: 'large',
                  disabled: activeTab >= 5
                }]
              }
            />
          </div>
          <div>
            <InlineButton
              marginDirection="right"
              buttons={[
                {
                  _type: 'string',
                  text: I18n.t('companyCreation.validate'),
                  onClick: !isError && handleSubmit(validateForm),
                  size: 'large'
                }]
              }
            />
          </div>
        </div>
        <ConfirmationDialog
          isOpen={isDialogOpen}
          onValidate={this.onValidate}
          message={I18n.t('companyCreation.dialogs.sirenConfirm.title')}
          buttonsLabel={
            {
              validate: I18n.t('companyCreation.dialogs.sirenConfirm.ok'),
              cancel: I18n.t('companyCreation.dialogs.sirenConfirm.cancel')
            }
          }
          onClose={this.onCloseDialog}
        />
      </div>
    );
  }
}

CompanyCreation.defaultProps = {
  title: I18n.t('companyCreation.title'),
  siren: {},
  error: undefined,
  isError: false,
  selectedCompany: undefined
};

CompanyCreation.propTypes = {
  title: PropTypes.string,
  form: PropTypes.isRequired,
  cancelForm: PropTypes.func.isRequired,
  activeTab: PropTypes.number.isRequired,
  existingCompany: PropTypes.number.isRequired,
  selectTab: PropTypes.func.isRequired,
  resetFormAndSetSociety: PropTypes.func.isRequired,
  selectCompany: PropTypes.func.isRequired,
  getDataBySiren: PropTypes.func.isRequired,
  setExistingCompany: PropTypes.func.isRequired,
  siren: PropTypes.object,
  updateForm: PropTypes.func.isRequired,
  validateForm: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  goToStep: PropTypes.func.isRequired,
  nextStep: PropTypes.func.isRequired,
  error: PropTypes.string,
  selectedCompany: PropTypes.object,
  society_id: PropTypes.number.isRequired,
  isError: PropTypes.bool,
  getAccountingPlans: PropTypes.func.isRequired
};

export default CompanyCreation;
