import React from 'react';
import PropTypes from 'prop-types';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

import styles from './Toggle.module.scss';

const Toggle = (props) => {
  // Props
  const {
    data,
    currentChoice,
    classes
  } = props;

  const handleChoice = (event, choice) => {
    if (!choice || choice === '') {
      return;
    }

    data.forEach((button) => {
      if (button.value === choice) {
        button.onClick();
      }
    });
  };

  if (!data.length) {
    return null;
  }

  return (
    <div className={styles.container}>
      <ToggleButtonGroup value={currentChoice} exclusive onChange={handleChoice}>
        {
          data.map(button => (
            <ToggleButton
              key={button.value}
              value={button.value}
              classes={classes}
              className={button.value === 'dashboardCollabYear' && styles.secondary}
            >
              {button.label}
            </ToggleButton>
          ))
        }
      </ToggleButtonGroup>
    </div>
  );
};

const items = PropTypes.shape({
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
});

Toggle.propTypes = {
  currentChoice: PropTypes.string.isRequired,
  classes: PropTypes.object,
  data: PropTypes.arrayOf(items).isRequired
};

Toggle.defaultProps = {
  classes: undefined
};

export default Toggle;
