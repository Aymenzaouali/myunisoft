import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { ConfirmationDialog } from 'components/groups/Dialogs';
import UserListTable from 'containers/groups/Tables/UserList';
import UserListTableFilter from 'containers/groups/Filters/UserList';
import UserCreation from 'containers/groups/UserCreation';
import I18n from 'assets/I18n';
import { routesByKey } from 'helpers/routes';
import List from 'containers/groups/List';

const UserList = (props) => {
  const {
    selectedUser,
    onAddNewUser,
    resetSelectedUser,
    deletionInformation: { mode, id },
    deleteAndClose,
    setDeletionDialog
  } = props;
  useEffect(() => resetSelectedUser, []);

  return (
    <div>
      <List
        actionButtonProps={{
          onClick: () => onAddNewUser(routesByKey.userCreation),
          label: I18n.t('userList.userCreation')
        }}
        filters={<UserListTableFilter />}
        listTable={<UserListTable />}
        editableContent={selectedUser && <UserCreation />}
      />
      <ConfirmationDialog
        isOpen={mode === 'user'}
        title={I18n.t('crm.dialog.title.user')}
        onValidate={() => deleteAndClose(id)}
        onClose={() => setDeletionDialog()}
      />
    </div>
  );
};

UserList.propTypes = ({
  resetSelectedUser: PropTypes.func.isRequired,
  selectedUser: PropTypes.number,
  onAddNewUser: PropTypes.func.isRequired,
  deletionInformation: PropTypes.object.isRequired,
  deleteAndClose: PropTypes.func.isRequired,
  setDeletionDialog: PropTypes.func.isRequired
});

UserList.defaultProps = {
  selectedUser: null
};

export default UserList;
