import React from 'react';
import ProgressionTable from 'containers/groups/Tables/Progression';
import { ProgressionHeader } from 'containers/groups/Headers';

import styles from './progression.module.scss';

const Progression = () => (
  <div className={styles.container}>
    <ProgressionHeader />
    <ProgressionTable />
  </div>
);

export default Progression;
