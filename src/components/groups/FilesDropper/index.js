import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FontIcon } from 'components/basics/Icon';
import {
  Typography
} from '@material-ui/core';
import Dropzone from 'react-dropzone';
import I18n from 'assets/I18n';

import './styles.scss';

/**
 * File Dropzone helper components
 * @example <caption>Example usage with one pane.</caption>
 * <FilesDropper
 *  title="What you want"
 *  pane={{ icon: 'not-implemented-yet', title: 'Subtitle', dropzoneConfig: Object }}
 * />
 * @example <caption>Example usage with multiple panes.</caption>
 * <FilesDropper
 *  title="What you want"
 *  pane={[
 *    { icon: 'icon-name', title: 'Subtitle', dropzoneConfig: Object, iconSize?: 20 },
 *    { icon: 'icon-name', title: 'Subtitle', dropzoneConfig: Object, iconSize?: 47 }]}
 * />
 */
const FilesDropper = (props) => {
  const { title, panes } = props;
  const dropzones = Array.isArray(panes) ? panes : [panes];

  return (
    <div className="files-dropper">
      {
        title && (
          <Typography className={`files-dropper__title${dropzones.length > 1 ? '_with-border' : ''}`} variant="h5" align="center">
            {I18n.t(title)}
          </Typography>
        )
      }
      {dropzones.map((dropzone, i) => (
        <Dropzone {...dropzone.dropzoneConfig} key={`dropzone-${title}-${i}`} className="files-dropper__item">
          {({ open }) => (
            <Fragment>
              {
                dropzone.icon && (<FontIcon size={dropzone.iconSize || 50} name={dropzone.icon} className="files-dropper__item__icon" />)
              }
              <Typography className="files-dropper__item__title" variant="h6" align="center">
                {I18n.t(dropzone.title)}
              </Typography>
              <Typography className="files-dropper__item__links" variant="body1">
                  <a className="files-dropper__item__links__item" href="#" onClick={open}>{I18n.t('filesDroperDialog.addFileButton')}</a> {/* eslint-disable-line */}
                {I18n.t('filesDroperDialog.info')}
                  <a className="files-dropper__item__links__item_error" href="#">{I18n.t('filesDroperDialog.scanButton')}</a> {/* eslint-disable-line */}
                {I18n.t('filesDroperDialog.info1')}
              </Typography>
            </Fragment>
          )}
        </Dropzone>
      ))}
    </div>
  );
};

const panesObjectProptypes = PropTypes.shape({
  icon: PropTypes.string.isRequired,
  iconSize: PropTypes.number,
  title: PropTypes.string.isRequired,
  dropzoneConfig: PropTypes.shape({}).isRequired
});

FilesDropper.defaultProps = {
  panes: []
};

FilesDropper.propTypes = {
  title: PropTypes.string.isRequired,
  panes: PropTypes.oneOfType([
    PropTypes.arrayOf(panesObjectProptypes),
    panesObjectProptypes
  ])
};

export default FilesDropper;
