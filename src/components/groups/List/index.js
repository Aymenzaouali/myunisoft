import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { CSVButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';
import { routesByKey } from 'helpers/routes';
import Toggle from 'components/groups/Toggle';
import Button from 'components/basics/Buttons/Button';

import styles from './List.module.scss';

class List extends React.PureComponent {
  editableContentRef = React.createRef();

  componentDidUpdate(nextProps) {
    const { editableContent } = this.props;
    if (nextProps.editableContent !== editableContent && editableContent !== null) {
      /**
       * Ensures animation start before next repaint
       * when rendering the component
       * and moved out
       */
      window.requestAnimationFrame(() => {
        if (this.editableContentRef && this.editableContentRef.current) {
          this.editableContentRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
        }
      });
    }
  }

  render() {
    const {
      listTable,
      editableContent,
      filters,
      actionButtonProps,
      routeKey,
      push,
      csvData,
      baseFileName,
      disableExport
    } = this.props;

    const toggleData = [
      {
        label: I18n.t('companyList.physicalPersonToggleButton'),
        onClick: () => push(routesByKey.physicalPersonList),
        value: 'physicalPersonList'
      },
      {
        label: I18n.t('companyList.legalPersonToggleButton'),
        onClick: () => push(routesByKey.companyList),
        value: 'companyList'
      },
      {
        label: I18n.t('userList.toggleButton'),
        onClick: () => push(routesByKey.userList),
        value: 'userList'
      }
    ];

    return (
      <React.Fragment>
        <div className={styles.container}>
          <Typography variant="h1">{I18n.t('companyUserList.title')}</Typography>
          <div className={styles.listHeader}>
            <div>
              <Typography variant="h6">{I18n.t('companyUserList.listChoice')}</Typography>
              <Toggle data={toggleData} defaultChoice={routeKey} currentChoice={routeKey} />
            </div>
            <div>
              {!disableExport && (
                <Button
                  size="large"
                  tabIndex="-1"
                  variant="contained"
                  color="primary"
                >
                  <CSVButton csvData={csvData} baseFileName={baseFileName} />
                </Button>
              )}
              { actionButtonProps
                && (
                  <Button
                    size="large"
                    tabIndex="-1"
                    variant="contained"
                    color="primary"
                    onClick={actionButtonProps.onClick}
                    style={{ marginLeft: 15 }}
                  >
                    {actionButtonProps.label}
                  </Button>
                )}
            </div>
          </div>
          {filters}
          <div className={styles.listContainer}>
            {listTable}
          </div>
        </div>
        { editableContent && (
          <div ref={this.editableContentRef}>
            {editableContent}
          </div>
        ) }
      </React.Fragment>
    );
  }
}

List.defaultProps = {
  filters: null,
  editableContent: null,
  disableExport: false
};

List.propTypes = {
  push: PropTypes.func.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired,
  listTable: PropTypes.shape({}).isRequired,
  editableContent: PropTypes.node,
  filters: PropTypes.shape({}),
  actionButtonProps: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired
  }).isRequired,
  routeKey: PropTypes.string.isRequired,
  disableExport: PropTypes.bool
};

export default List;
