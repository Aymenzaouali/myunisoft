import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const Footer = (props) => {
  const {
    title,
    leftElement,
    middleElement,
    rightElement
  } = props;

  return (
    <div className="footerContainer">
      <div className="container__title">{title}</div>
      <div className="separator" />
      <div className="container__items">
        <div className="container__item container__item--cyanElement">{leftElement}</div>
        <div className="container__item container__item--Element">{middleElement}</div>
        <div className="container__item container__item--Element">{rightElement}</div>
      </div>
    </div>
  );
};

Footer.propTypes = {
  title: PropTypes.string.isRequired,
  leftElement: PropTypes.number.isRequired,
  middleElement: PropTypes.string.isRequired,
  rightElement: PropTypes.string.isRequired
};

export default Footer;
