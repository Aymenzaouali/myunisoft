import React from 'react';
import { BankLinkHeaderContainer } from 'containers/groups/Headers';
import BankLinkTable from 'containers/groups/Tables/BankLink';
import BankLinkToolBar from 'containers/groups/ToolBars/BankLink';
import styles from './banklink.module.scss';

const BankLink = props => (
  <div>
    <BankLinkHeaderContainer {...props} />
    <BankLinkToolBar />
    <div className={styles.table}>
      <BankLinkTable {...props} />
    </div>
  </div>
);

export default BankLink;
