import React from 'react';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

import { InlineButton } from 'components/basics/Buttons';
import Comment from 'components/basics/Comment';
import styles from './singleComment.module.scss';

class SingleComment extends React.PureComponent {
  render() {
    const {
      avatar,
      userName,
      date,
      comment,
      isEditable,
      onEdit
    } = this.props;

    const textIcon = I18n.t('SingleComment.comment_information', { userName, date });

    return (
      <div className={styles.container}>
        <div className={styles.headerContainer}>
          <Typography variant="h6">
            {I18n.t('SingleComment.comment')}
          </Typography>
          <div className={styles.iconContainer}>
            <InlineButton buttons={[
              {
                _type: 'string',
                text: 'Editer',
                onClick: onEdit,
                variant: 'outlined'
              }
            ]}
            />
          </div>
        </div>
        <Comment
          validateButton={false}
          avatar={avatar}
          textIcon={textIcon}
          comment={comment}
          edit={isEditable}
          hasField
        />
      </div>
    );
  }
}

SingleComment.propTypes = {
  avatar: PropTypes.shape({
    initial: PropTypes.string.isRequired
  }).isRequired,
  userName: PropTypes.string.isRequired,
  date: PropTypes.string,
  comment: PropTypes.string,
  classes: PropTypes.shape({}).isRequired,
  onEdit: PropTypes.func,
  isEditable: PropTypes.bool
};

SingleComment.defaultProps = {
  date: 'date indisponible',
  comment: 'pas de commentaire',
  isEditable: false,
  onEdit: () => {}
};

export default (SingleComment);
