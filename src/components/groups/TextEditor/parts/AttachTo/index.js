import React from 'react';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';
import {
  FormGroup,
  withStyles,
  FormLabel,
  FormControl,
  FormControlLabel,
  Checkbox
} from '@material-ui/core';

const attachToOptions = [
  'attachToNotes',
  'attachToInternalNotes',
  'attachToAccount',
  'attachToOther',
  'attachToDiligence',
  'attachToConfirmationLetter'
];

const FormControlLabelElement = (props) => {
  // eslint-disable-next-line react/prop-types
  const { name, checked, handleChange } = props;

  return (
    <FormControlLabel
      control={(
        <Checkbox
          checked={!!checked}
          onChange={handleChange(name)}
          value={name}
        />
      )}
      label={I18n.t(`commentBox.${name}`)}
    />
  );
};

const AttachTo = (({ onChange, classes, state }) => {
  const handleChange = name => (event) => {
    onChange(name, event.target.checked);
  };

  return (
    <FormControl component="fieldset">
      <FormLabel
        component="legend"
        className={classes.labelRoot}
        focused={false}
      >
        {I18n.t('commentBox.attachToTitle')}
      </FormLabel>
      <FormGroup row>
        {attachToOptions.map((name, i) => (
          <FormControlLabelElement
            checked={state[name]}
            name={name}
            handleChange={handleChange}
            key={i}
          />
        ))}
      </FormGroup>
    </FormControl>
  );
});

AttachTo.propTypes = {
  onChange: PropTypes.func.isRequired,
  classes: PropTypes.shape({}).isRequired,
  state: PropTypes.shape({
    attachToNotes: PropTypes.bool,
    attachToInternalNotes: PropTypes.bool,
    attachToAccount: PropTypes.bool,
    attachToOther: PropTypes.bool,
    attachToDiligence: PropTypes.bool,
    attachToConfirmationLetter: PropTypes.bool
  }).isRequired
};

const theme = () => ({
  labelRoot: {
    color: '#000',
    marginBottom: 10
  }
});

export default withStyles(theme)(AttachTo);
