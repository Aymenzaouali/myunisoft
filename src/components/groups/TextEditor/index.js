import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import ColorPicker from 'components/basics/ColorPicker';
import Loader from 'components/basics/Loader';
import {
  htmlToDraftHelper,
  draftToHtmlHelper
} from 'helpers/textEditor';
import {
  AttachTo,
  CommentBoxButtons
} from './parts';

import styles from './TextEditor.module.scss';

const toolbarOptions = {
  options: ['inline', 'list', 'colorPicker', 'link', 'emoji', 'history'],
  inline: {
    options: ['bold', 'italic', 'underline']
  },
  list: {
    options: ['unordered']
  },
  colorPicker: {
    component: ColorPicker
  }
};

const commentBoxInitialState = {
  body: '',
  attachTo: {}
};


const TextEditor = ({
  className,
  onSubmit,
  onCancelCustom,
  hideOptions,
  body,
  isLoading,
  openConfirmationDialog,
  closeDialog,
  createMode,
  toolbarHidden,
  confirmCancel
}) => {
  const [editorState, setEditorState] = useState(htmlToDraftHelper(body));
  const [commentBoxState, setCommentBoxState] = useState(commentBoxInitialState);
  const [editorTextareaRef, setEditorTextareaRef] = useState(null);

  const onEditorStateChange = (editorState) => {
    setCommentBoxState({
      ...commentBoxState,
      body: draftToHtmlHelper(editorState)
    });
    setEditorState(editorState);
  };

  useEffect(() => {
    onEditorStateChange(htmlToDraftHelper(body));
  }, [body]);

  useEffect(() => {
    if (editorTextareaRef) {
      editorTextareaRef.focus();
    }
  }, [editorTextareaRef]);

  const discardState = () => {
    if (onCancelCustom) {
      onCancelCustom();
    }

    setEditorState(EditorState.createEmpty());
    setCommentBoxState(commentBoxInitialState);
    closeDialog();
  };

  const onCancel = () => {
    if (confirmCancel && commentBoxState.body !== body) {
      openConfirmationDialog({
        title: I18n.t('notifications.warning'),
        body: I18n.t('notifications.canLoseChanges'),
        buttons: [
          {
            _type: 'string',
            text: I18n.t('notifications.yes'),
            onClick: discardState,
            color: 'error',
            variant: 'outlined'
          },
          {
            _type: 'string',
            text: I18n.t('notifications.no'),
            color: 'default',
            variant: 'outlined'
          }
        ]
      });
    } else {
      discardState();
    }
  };

  const handleOnSubmit = async () => {
    await onSubmit({
      ...commentBoxState,
      attachTo: Object.keys(commentBoxState.attachTo)
        .filter(key => commentBoxState.attachTo[key])
    });

    setEditorState(EditorState.createEmpty());
    setCommentBoxState(commentBoxInitialState);
    if (createMode) closeDialog();
  };

  const onAttachToStateChange = (key, value) => {
    setCommentBoxState({
      ...commentBoxState,
      attachTo: {
        ...commentBoxState.attachTo,
        [key]: value
      }
    });
  };

  return (
    <div className={classnames(styles.textEditorWrapper, className)}>
      <Editor
        editorState={editorState}
        wrapperClassName={styles.wrapperContainer}
        toolbarClassName={styles.toolbarContainer}
        editorClassName={styles.editorContainer}
        onEditorStateChange={onEditorStateChange}
        toolbar={toolbarOptions}
        toolbarHidden={toolbarHidden}
        editorRef={setEditorTextareaRef}
      />
      {!hideOptions && (
        <React.Fragment>
          <AttachTo
            onChange={onAttachToStateChange}
            state={commentBoxState.attachTo}
          />
        </React.Fragment>
      )}
      <CommentBoxButtons
        disabled={commentBoxState.body === body}
        onSubmit={handleOnSubmit}
        onCancel={onCancel}
      />
      {isLoading && (
        <div className={styles.editorOverlay}>
          <Loader />
        </div>
      )}
    </div>
  );
};

TextEditor.defaultProps = {
  className: null,
  onCancelCustom: undefined,
  hideOptions: false,
  body: '',
  createMode: false,
  toolbarHidden: false,
  confirmCancel: true
};

TextEditor.propTypes = {
  className: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onCancelCustom: PropTypes.func,
  hideOptions: PropTypes.bool,
  body: PropTypes.string,
  openConfirmationDialog: PropTypes.func.isRequired,
  closeDialog: PropTypes.func.isRequired,
  createMode: PropTypes.bool,
  toolbarHidden: PropTypes.bool,
  confirmCancel: PropTypes.bool
};

export default TextEditor;
