import React, { useEffect } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
// import { Field } from 'redux-form';
import { Typography, withStyles } from '@material-ui/core';

import I18n from 'assets/I18n';

import SingleComment from 'components/groups/SingleComment';
// import { ReduxSelect } from 'components/reduxForm/Selections';
import Loader from 'components/basics/Loader';
import { InlineButton } from 'components/basics/Buttons';

import moment from 'moment';
import styles from './consultingComment.module.scss';

const ConsultingComment = ({
  // classes,
  // commentTypeList, supervisorList,
  getAndOpenComments, getComments,
  consultingFormValues: { exercice },
  Account, currentRevision,
  comment_information: {
    body,
    date,
    name,
    initials
  },
  isCommentLoading
}) => {
  useEffect(() => {
    getComments(_.get(Account, 'account_id'), _.get(currentRevision, 'id_dossier_revision'));
  }, [Account, exercice, currentRevision]);

  const renderComment = body ? (
    <SingleComment
      avatar={{ initial: initials }}
      date={moment(date).format('DD/MM/YYYY')}
      userName={name}
      comment={body}
      onEdit={() => getAndOpenComments(true)}
    />
  ) : (
    <div className={styles.noCommentContainer}>
      <Typography variant="h6">{I18n.t('consulting.comment.noComment')}</Typography>
      <InlineButton
        marginDirection="right"
        buttons={[
          {
            _type: 'string',
            text: 'Ajouter un commentaire',
            onClick: () => getAndOpenComments(),
            variant: 'outlined'
          }]
        }
      />
    </div>
  );

  // const renderSupervisor = list => (
  //   list.map(i => (
  //     <div className={styles.supervisorContainer}>
  //       {i.hasChecked
  //         && (
  //           <div className={styles.checkingMonitor} style={{ backgroundColor: i.color }} />
  //         )
  //       }
  //       <Typography>{i.role}</Typography>
  //     </div>
  //   ))
  // );

  return (
    <div className={styles.commentContainer}>
      {isCommentLoading ? <Loader /> : (
        renderComment
      )}
      {/* <div className={styles.accountContainer}>
        <Typography variant="h6">{I18n.t('consulting.comment.selectComment')}</Typography>
        <Field
          component={ReduxSelect}
          list={commentTypeList}
          name="commentType"
          margin="none"
          classes={{ root: styles.commentTypeContainer }}
          onChange={() => console.log('get commentaire selon le type choisi')}
        />
      </div>
      <div className={styles.checkingContainer}>
        <Typography variant="h6" color="secondary" classes={{ root: classes.checkingTitle }}>
          {I18n.t('consulting.comment.checking')}
          {' '}
            ?/3
        </Typography>
        {renderSupervisor(supervisorList)}
      </div> */}
    </div>
  );
};

ConsultingComment.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  // supervisorList: PropTypes.arrayOf(PropTypes.object).isRequired,
  // commentTypeList: PropTypes.arrayOf(PropTypes.object).isRequired,
  getAndOpenComments: PropTypes.func.isRequired,
  getComments: PropTypes.func.isRequired,
  Account: PropTypes.object,
  currentRevision: PropTypes.object,
  comment_information: PropTypes.object,
  isCommentLoading: PropTypes.bool.isRequired,
  consultingFormValues: PropTypes.object
};

ConsultingComment.defaultProps = {
  comment_information: null,
  Account: null,
  currentRevision: null,
  consultingFormValues: {}
};

const ownStyles = () => ({
  checkingTitle: {
    marginBottom: 10
  }
});

export default withStyles(ownStyles)(ConsultingComment);
