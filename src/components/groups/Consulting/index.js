import React, { Component, Fragment } from 'react';
import { Shortcuts } from 'react-shortcuts';
import PropTypes from 'prop-types';

import { Typography } from '@material-ui/core';
import _get from 'lodash/get';

import I18n from 'assets/I18n';
import { routesByKey } from 'helpers/routes';

import TableConsulting from 'containers/groups/Tables/Consulting';
import { LettrageDialog } from 'containers/groups/Dialogs';
import { ConsultingFilterContainer } from 'containers/groups/Filters';
import NewAccountingScreen from 'containers/groups/NewAccounting';
import ConsultingComment from 'containers/groups/Consulting/Comment';

import HelperInfo from 'components/basics/HelperInfo';
import Lettrage from 'components/groups/Letterage';
import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';

import styles from './consulting.module.scss';

// Component
class Consulting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lettrage: false,
      lettring: false,
      modifyEntry: {
        show: false
      },
      warningIsVisible: false,
      warningMessage: null
    };
  }

  componentDidMount() {
    const { getReviews } = this.props;
    getReviews();
  }

  handleShortcuts = (action, e) => {
    const {
      societyId,
      getEntries,
      push,
      accountID,
      Lettrage,
      selection,
      nbSelect,
      total,
      nbLetter,
      split,
      openPopupSplit,
      index,
      setCurrentIndex
    } = this.props;
    const { onActivateSplit, slaves } = this.context;
    const { lettring } = this.state;

    const moveOnTable = (direction) => {
      const newIndex = direction === 'up' ? index + 1 : index - 1;
      const consultingEntryCheckbox = document.getElementById(`consultingEntryCheckbox${newIndex}`);
      if (consultingEntryCheckbox) {
        consultingEntryCheckbox.focus();
        setCurrentIndex(newIndex);
      }
    };

    // deactivate if is slave
    if (split.slave) {
      return null;
    }

    e.preventDefault();
    e.stopPropagation();

    switch (action) {
    case 'SPLIT_ENTRY':
      if (slaves.length) onActivateSplit(slaves[0].id, slaves[0].args);
      openPopupSplit('consulting');
      break;
    case 'LAUNCH_NEW_ACCOUNTING':
      window.open(routesByKey.newAccounting.replace(':id', societyId), `window${new Date().getTime()}`, `width=${window.innerWidth},height=${window.innerHeight}`);
      break;
    case 'VALIDATE':
      if (accountID != null) {
        if (lettring === true) {
          push(societyId, _get(Lettrage, 'values.value', ''), selection[accountID], accountID);
        } else {
          push(societyId, '', selection[accountID], accountID);
        }
        this.setState({ lettrage: false });
      }
      break;
    case 'CANCEL':
      this.setState({ lettrage: false });
      break;
    case 'LETTER':
      if (nbSelect !== 0 && total !== 0) {
        this.setState({
          warningIsVisible: true,
          warningMessage: I18n.t('consulting.warning.notBalanced')
        });
      } else if (nbSelect === 0) {
        this.setState({
          warningIsVisible: true,
          warningMessage: I18n.t('consulting.warning.notSelected')
        });
      } else if (nbLetter !== 0) {
        this.setState({
          warningIsVisible: true,
          warningMessage: I18n.t('consulting.warning.alreadyLettered')
        });
      } else {
        this.setState({
          lettrage: true,
          lettring: true,
          warningIsVisible: false
        });
      }
      break;
    case 'UNLETTER':
      this.setState({ lettrage: true, lettring: false });
      break;
    case 'SHOW_ACCOUNT_LEFT':
      getEntries(societyId, undefined, undefined, 'previous');
      break;
    case 'SHOW_ACCOUNT_RIGHT':
      getEntries(societyId, undefined, undefined, 'next');
      break;
    case 'FOCUS_NEXT_ENTRY':
      moveOnTable('up');
      break;
    case 'FOCUS_PREVIOUS_ENTRY':
      moveOnTable('down');
      break;
    default:
      break;
    }
    return null;
  };

  accountName() {
    const { filters, split } = this.props;

    if (!filters || !filters.Account) return '';
    if (split.slave && !split.args.account) return '';

    const account = filters.Account;
    return `- ${account.account_number} ${account.label}`;
  }

  renderMaster() {
    const {
      letter,
      notLetter,
      total,
      nbSelect,
      nbLetter,
      consultingRef,
      filters = {}
    } = this.props;

    const {
      lettrage, lettring, warningIsVisible, warningMessage
    } = this.state;
    const { Account } = filters;

    return (
      <Fragment>
        <ConsultingFilterContainer notLetter={notLetter} />

        {/* Part 2 Not implement
          <div>
            <SingleComment
              avatar={{
                initial
              }}
              date="10/10/2010"
              userName={userName}
              comment="Text"
            />
          </div>
        */}
        {Account && <ConsultingComment />}
        <Lettrage
          nbSelected={nbSelect}
          letter={letter}
          onClickLetter={() => this.setState({ lettrage: true, lettring: true })}
          onClickNotLetter={() => this.setState({ lettrage: true, lettring: false })}
          notLetter={notLetter}
          total={total}
          nbLetter={nbLetter}
        />
        <HelperInfo
          isVisible={warningIsVisible}
          infoMessages={[{ message: warningMessage, type: 'error' }]}
          className={styles.consulting_warning}
        />
        <LettrageDialog
          isOpen={lettrage}
          onClose={() => this.setState({ lettrage: false })}
          lettering={lettring}
        />

        <TableConsulting
          consultingRef={consultingRef}
          modifyEntry={() => this.setState({ modifyEntry: { show: true } })}
        />
      </Fragment>
    );
  }

  renderSlave() {
    const { split: { master_id } } = this.props;

    switch (master_id) {
    case 'accounting':
      return this.renderSlaveAccounting();

    case 'new-accounting':
      return this.renderSlaveNewAccounting();

    case 'current_editions':
      return this.renderSlaveCurrentEditions();

    default:
      return this.renderMaster();
    }
  }

  renderSlaveAccounting() {
    const { split: { args } } = this.props;

    // pas sur écriture
    if (args.isNotEcriture) {
      return (
        <p>{ I18n.t('consulting.slave.accounting.not_ecriture') }</p>
      );
    }

    // pas de selection
    if (args.account === undefined) {
      return (
        <p>{ I18n.t('consulting.slave.accounting.empty') }</p>
      );
    }

    return this.renderMaster();
  }

  renderSlaveNewAccounting() {
    const { split: { args } } = this.props;

    // pas de selection
    if (args.account === undefined) {
      return (
        <p>{ I18n.t('consulting.slave.new_accounting.empty') }</p>
      );
    }

    return this.renderMaster();
  }

  renderSlaveCurrentEditions() {
    const { split: { args } } = this.props;

    // pas de selection
    if (args.account === undefined) {
      return (
        <p>{ I18n.t('consulting.slave.current_editions.empty') }</p>
      );
    }

    return this.renderMaster();
  }

  render() {
    const {
      getEntries, societyId, split
    } = this.props;
    const { modifyEntry: { show } } = this.state;

    return (
      <Fragment>
        { show && ( // TODO: better to use the split (direct swapped split ?)
          <NewAccountingScreen
            title={I18n.t('consulting.updateEntry')}
            disableRefresh
            iconClose
            customClose={() => {
              this.setState({ modifyEntry: { show: false } });
              getEntries(societyId);
            }}
            filterType="e"
          />
        )}

        <div className={styles.consulting}>
          <Shortcuts
            name="CONSULTING"
            handler={this.handleShortcuts}
            alwaysFireHandler
            stopPropagation={false}
            preventDefault={false}
            targetNodeSelector="body"
          >
            <div className={styles.header}>
              <Typography className={styles.title} component="h1" variant="h1">
                {`${I18n.t('consulting.title')} ${this.accountName()}`}
              </Typography>

              <SplitToolbar />
            </div>

            { split.slave ? this.renderSlave() : this.renderMaster() }
          </Shortcuts>
        </div>
      </Fragment>
    );
  }
}

// Props
Consulting.propTypes = {
  // not implement
  // initial: PropTypes.string,
  // userName: PropTypes.string,
  // date: PropTypes.shape({
  //   start: PropTypes.string,
  //   end: PropTypes.string
  // }).isRequired,
  Lettrage: PropTypes.shape({
    values: PropTypes.shape({
      value: PropTypes.string
    }),
    initial: PropTypes.shape({
      value: PropTypes.string
    })
  }),
  openPopupSplit: PropTypes.func.isRequired,
  selection: PropTypes.arrayOf(PropTypes.shape({})),
  accountID: PropTypes.number,
  push: PropTypes.func.isRequired,
  arrayData: PropTypes.shape({}),
  filters: PropTypes.shape({}).isRequired,
  nbSelect: PropTypes.number.isRequired,
  letter: PropTypes.number.isRequired,
  societyId: PropTypes.number.isRequired,
  notLetter: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  nbLetter: PropTypes.number.isRequired,
  getEntries: PropTypes.func.isRequired,
  split: PropTypes.shape({}).isRequired,
  consultingRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }).isRequired,
  consulting: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  setCurrentIndex: PropTypes.func.isRequired,
  getReviews: PropTypes.func.isRequired
};

Consulting.defaultProps = {
  // not implement
  // initial: '',
  // userName: '',
  arrayData: {},
  selection: {},
  Lettrage: {},
  accountID: null
};

export default Consulting;
