import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { Typography, withStyles } from '@material-ui/core';

import I18n from 'assets/I18n';

import Button from 'components/basics/Buttons/Button';
import { ReduxSelect } from 'components/reduxForm/Selections';

import { PlaquetteTree } from 'containers/groups/Dialogs';

import styles from './Brochure.module.scss';

const Brochure = ({
  classes, reviews, handleSubmit, onGenerate, isOpen
}) => (
  <form className={styles.container}>
    <Typography variant="title">
      {I18n.t('brochure.title')}
    </Typography>
    <div className={styles.selectionContainer}>
      <Field
        component={ReduxSelect}
        label={I18n.t('brochure.review_mode')}
        list={reviews}
        name="reviewId"
        margin="none"
        classes={{ root: styles.fieldContainer }}
      />
      <Button
        color="primary"
        variant="contained"
        classes={{ root: classes.button }}
        onClick={handleSubmit(onGenerate)}
      >
        {I18n.t('brochure.generate')}
      </Button>
    </div>
    <PlaquetteTree isOpen={isOpen} />
  </form>
);

Brochure.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onGenerate: PropTypes.func.isRequired,
  reviews: PropTypes.arrayOf(PropTypes.object).isRequired,
  isOpen: PropTypes.bool.isRequired
};

const themeStyles = () => ({
  button: {
    marginLeft: '10px'
  }
});

export default withStyles(themeStyles)(Brochure);
