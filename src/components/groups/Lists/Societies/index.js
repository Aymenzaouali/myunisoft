import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Field, FieldArray
} from 'redux-form';
import { withStyles, Typography, Divider } from '@material-ui/core';
import Button from 'components/basics/Buttons/Button';
import ReduxAutoComplete from 'components/reduxForm/Inputs/AutoComplete';
import SocietiesList from 'containers/groups/Tables/Permissions';
import WalletAccessList from 'containers/groups/UserCreation/WalletAccessList';
import I18n from 'assets/I18n';
import './styles.scss';

const Societies = (props) => {
  const {
    loadSocieties,
    loadWallets,
    wallets,
    onGranted,
    societies,
    selectedWallet,
    selectedSocieties,
    classes,
    type_id,
    profil_id
  } = props;

  useEffect(() => {
    loadSocieties();
    loadWallets();
  }, []);

  return (type_id && profil_id
    ? (
      <div className={classes.container}>
        <Typography variant="h6" className={classes.title}>{I18n.t('newUserForm.rightManagement.title')}</Typography>
        <div className={classes.fields}>
          <Field
            name="selectedWallet"
            component={ReduxAutoComplete}
            options={wallets}
            isMulti
            placeholder=""
            textFieldProps={{
              label: I18n.t('newUserForm.rightManagement.wallet'),
              InputLabelProps: {
                shrink: true
              }
            }}
            className={classes.field}
          />
          <Typography variant="h6" className={classes.andOrLabel}>{I18n.t('newUserForm.rightManagement.andOr')}</Typography>
          <Field
            name="societies"
            component={ReduxAutoComplete}
            options={societies}
            isMulti
            placeholder=""
            textFieldProps={{
              label: I18n.t('newUserForm.rightManagement.society'),
              InputLabelProps: {
                shrink: true
              }
            }}
            className={classes.field}
          />
          <Button
            size="large"
            variant="contained"
            disabled={selectedSocieties === false && selectedWallet === false}
            className={classes.addButton}
            color="primary"
            onClick={() => onGranted()}
          >
            {I18n.t('newUserForm.rightManagement.add')}
          </Button>
        </div>
        <FieldArray
          name="society_access_list"
          component={SocietiesList}
          classes={classes}
        />
        <Divider classes={{ root: classes.divider }} />
        <FieldArray
          name="wallet_access_list"
          component={WalletAccessList}
          classes={classes}
        />
      </div>
    ) : null
  );
};

const styles = () => ({
  container: {
    padding: 20
  },
  title: {
    'margin-bottom': 26
  },
  fields: {
    display: 'flex',
    'flex-direction': 'row',
    flex: 1,
    margin: '26px 0px'
  },
  field: {
    'margin-right': 20,
    width: 250
  },
  headerLabel: {
    'font-size': 16,
    'font-family': 'basier_circlemedium'
  },
  bodyLabel: {
    'font-size': 14
  },
  andOrLabel: {
    marginRight: 20,
    paddingTop: 20
  },
  labelModification: {
    color: '#0bd1d1',
    'border-style': 'none',
    'background-color': 'white',
    'text-decoration': 'underline'
  },
  labelDeletion: {
    color: '#fe3a5e',
    'border-style': 'none',
    'background-color': 'white'
  },
  divider: {
    marginTop: 20,
    height: 2,
    color: '#979797'
  },
  addButton: {
    height: 42,
    marginTop: 10
  }
});

Societies.defaultProps = {
  profil_id: undefined,
  type_id: undefined
};

Societies.propTypes = ({
  loadSocieties: PropTypes.func.isRequired,
  loadWallets: PropTypes.func.isRequired,
  onGranted: PropTypes.func.isRequired,
  selectedWallet: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedSocieties: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  wallets: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.value,
    name: PropTypes.string
  })).isRequired,
  societies: PropTypes.arrayOf(PropTypes.shape({
    ape: PropTypes.string,
    city: PropTypes.string,
    companyType: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string,
    siret: PropTypes.string,
    society_id: PropTypes.number,
    status: PropTypes.string,
    step: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  profil_id: PropTypes.number,
  type_id: PropTypes.string
});

export default withStyles(styles)(Societies);
