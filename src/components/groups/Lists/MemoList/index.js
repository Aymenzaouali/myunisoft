import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import Basic from 'components/basics/Items/Checkbox';
import './styles.scss';

const MemoList = (props) => {
  const {
    memoData, putMemo, getMemo, idSociety
  } = props;

  const handleChange = async (id, label, isChecked) => {
    await putMemo(id, label, isChecked);
    getMemo(idSociety);
  };

  return (
    <div className="listRoot">
      <List className="memoListContainer">
        { memoData.map(item => (
          <Basic
            moment={`Basic${item.memo_id}`}
            name={item.label}
            leftItem={item.label}
            checked={item.done}
            onChange={(e, checked) => {
              handleChange(item.memo_id, item.label, checked);
            }}
          />
        ))}
      </List>
    </div>
  );
};

MemoList.defaultProps = ({
  memoData: []
});

MemoList.propTypes = ({
  idSociety: PropTypes.number.isRequired,
  putMemo: PropTypes.func.isRequired,
  getMemo: PropTypes.func.isRequired,
  memoData: PropTypes.arrayOf(PropTypes.shape({
    memo_id: PropTypes.number,
    label: PropTypes.string,
    done: PropTypes.bool
  }))
});

export default MemoList;
