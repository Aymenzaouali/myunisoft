import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import './styles.scss';

class ListMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHovered: false
    };
    this.toggleHover = this.toggleHover.bind(this);
  }

  toggleHover() {
    const { isHovered } = this.state;
    this.setState({ isHovered: !isHovered });
  }

  renderListItem = (item, index) => {
    const {
      onClickItem,
      classes
    } = this.props;

    const {
      label,
      disabled,
      colorError
    } = item;

    return (
      <ListItem key={`Basic${index}${label}`} onClick={() => !disabled && onClickItem(item)} className="textContainer" disabled={disabled}>
        <ListItemText
          primary={item.label}
          classes={{
            primary: classNames(classes.text, { [classes.colorError]: colorError })
          }}
        />
      </ListItem>
    );
  }


  render() {
    const { isHovered } = this.state;
    const {
      classes,
      menuItems = [],
      menuTitle,
      onFocus
    } = this.props;


    return (
      <div className="root">
        <List>
          <ListItem
            className="titleContainer"
            classes={{ root: isHovered && classes.hoveredTitleContainer }}
            onMouseOver={this.toggleHover}
            onFocus={onFocus}
            onMouseLeave={this.toggleHover}
          >
            <ListItemText primary={menuTitle} classes={{ primary: classes.title }} />
          </ListItem>
          {menuItems.map((item, index) => this.renderListItem(item, index))}
        </List>
      </div>
    );
  }
}

const styles = ({ palette }) => ({
  hoveredTitleContainer: {
    borderLeft: 'solid #00D8D3',
    borderLeftWidth: '3px'
  },
  title: {
    color: 'white',
    fontSize: 22,
    fontFamily: 'basier_circlemedium',
    fontWeight: 500,
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal'
  },
  text: {
    color: 'white',
    fontSize: 16,
    fontFamily: 'basier_circleregular',
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal'
  },
  cursor: {
    cursor: 'pointer'
  },
  infoBulle: {
    border: 'none',
    heigth: 15,
    width: 'auto',
    padding: '4px 10px',
    borderRadius: 2,
    fonytSize: 10,
    fontWeight: 500,
    boxShadow: '1px 5px 7px 0 rgba(0, 0, 0, 0.08)',
    backgroundColor: '#ffffff',
    color: '#464545',
    fontFamily: 'basier_circlemedium'
  },
  colorError: {
    color: palette.secondary.main
  }
});

ListMenu.defaultProps = {
  classes: {},
  onFocus: () => {}
};

ListMenu.propTypes = {
  classes: PropTypes.shape({}),
  menuItems: PropTypes.arrayOf.isRequired,
  menuTitle: PropTypes.string.isRequired,
  onClickItem: PropTypes.func.isRequired,
  onFocus: PropTypes.func
};

export default withStyles(styles)(ListMenu);


// item.disabled
// ? (
// <Tooltip
// title={I18n.t('drawer.notAvailable')}
// placement="left-end"
// disableFocusListener
// disableTouchListener
// classes={{ tooltip: classes.infoBulle }}>
// {this.renderListItem(item, index)}
// </Tooltip>
// )
// : this.renderListItem(item, index)
