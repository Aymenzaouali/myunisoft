import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import Basic from 'components/basics/Items/Checkbox';
import SectionHeader from 'components/basics/Cells/SectionHeader';
import _ from 'lodash';
import './styles.scss';

class TaskList extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  render() {
    const {
      headers = [],
      amountStyle
    } = this.props;

    return (
      <div className="listContainer">
        <List>
          {headers.map((e, index) => (
            <li key={index}>
              <ListSubheader disableSticky style={{ paddingLeft: 0 }}>
                <SectionHeader
                  sectionTitle={e.type}
                  sectionAmount={`(${e.total})`}
                  amountStyle={amountStyle}
                />
              </ListSubheader>
              {_.get(e, 'task', []).map((item, index) => (
                <Basic moment={`Basic${index}`} leftItem={item.title} rightItem={item.number} />
              ))}
            </li>
          ))}
        </List>
      </div>
    );
  }
}

TaskList.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.shape({
    type: PropTypes.string.isRequired,
    total: PropTypes.string.isRequired,
    task: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      number: PropTypes.number.isRequired
    }))
  })).isRequired,
  amountStyle: PropTypes.shape({})
};

TaskList.defaultProps = {
  amountStyle: {}
};

export default TaskList;
