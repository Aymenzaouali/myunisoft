import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  StepLabel,
  Stepper,
  withStyles
} from '@material-ui/core';
import classNames from 'classnames';
import { generateHeaderSteps } from 'helpers/headerSteppers';
import styles from './accountingFirmTabs.module.scss';

const AccountingFirmTabs = ({
  classes,
  setActiveTab,
  activeTabId,
  tabs
}) => (
  <Fragment>
    <div className={styles.container}>
      <div className={styles.row}>
        <Stepper connector={null} className={classes.stepperTabsContainer}>
          {generateHeaderSteps(tabs).map(e => (
            <div key={`Step${e.id}`} className={styles.stepperTabs}>
              <StepLabel
                onClick={() => (e.onClick ? e.onClick(e.id) : (!e.disabled && setActiveTab(e.id)))}
                classes={{
                  label: classNames(
                    { [classes.disabled]: e.disabled },
                    activeTabId === e.id
                      ? classes.selectedLabel
                      : classes.notSelectedLabel,
                    { [classes.colorError]: e.colorError }
                  )
                }}
              >
                {e.step}
                {e.label}
              </StepLabel>
              <div className={
                activeTabId === e.id
                  ? styles.stepperTabsSelected
                  : styles.stepperTabsNotSelected
              }
              />
            </div>
          ))}
        </Stepper>
      </div>
    </div>
  </Fragment>
);
AccountingFirmTabs.propTypes = {
  activeTabId: PropTypes.number.isRequired,
  setActiveTab: PropTypes.func.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

const stepperStyles = () => ({
  selectedLabel: {
    color: '#0bd1d1',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  notSelectedLabel: {
    color: 'black',
    'font-size': '14px',
    'font-family': 'basier_circlemedium'
  },
  stepperTabsContainer: {
    padding: '32px 0px 0px 0px'
  },
  disabled: {
    color: '#FF6600'
    // color: '#e7e7e7'
  },
  amountStep: {
    marginLeft: '10px',
    'margin-right': '10px',
    'margin-bottom': '-18px'
  }
});

export default withStyles(stepperStyles)(AccountingFirmTabs);
