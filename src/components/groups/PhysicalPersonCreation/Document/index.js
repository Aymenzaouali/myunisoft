import React from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import DocumentTable from 'components/groups/Tables/Document';
import FilesDropper from 'components/groups/FilesDropper';
import { FieldArray } from 'redux-form';

import styles from './DocumentTab.module.scss';

class DocumentInfo extends React.PureComponent {
  getDropzoneConfig() {
    const { addDocument, societyId } = this.props;

    return ({
      disableClick: true,
      onDrop: (files) => {
        files.map(e => addDocument(societyId, {
          file: e,
          name: e.name,
          date_time: new Date()
        }));
      }
    });
  }


  render() {
    return (
      <div className={styles.container}>
        <div className={styles.tableContainer}>
          <FieldArray
            name="document_array"
            component={DocumentTable}
          />
        </div>
        <div className={styles.dropperContainer}>
          <FilesDropper
            title={I18n.t('physicalPersonCreation.document_form.dropzoneTitle')}
            panes={{
              dropzoneConfig: this.getDropzoneConfig({ type: 'sales', format: 'document' }),
              icon: 'upload',
              title: I18n.t('physicalPersonCreation.document_form.dropzoneLegend')
            }}
          />
        </div>
      </div>
    );
  }
}

DocumentInfo.propTypes = {
  societyId: PropTypes.number.isRequired,
  addDocument: PropTypes.func.isRequired
};

export default DocumentInfo;
