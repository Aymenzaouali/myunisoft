import React, { useEffect } from 'react';
import classNames from 'classnames';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Field, Form } from 'redux-form';

import { socialSecurityFormat } from 'helpers/format';
import I18n from 'assets/I18n';

import { CitiesAutoComplete, PostalCodeTextField } from 'containers/reduxForm/Inputs';
import PhysicalPersonContactsTable from 'components/groups/Tables/PhysicalPersonContacts';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { ReduxRadio, ReduxSelect } from 'components/reduxForm/Selections';

import styles from './GeneralInfo.module.scss';

// Components
function GeneralInfo(props) {
  const {
    civility_type_list,
    maritalSituation_type_list,
    maritalRegime_type_list,
    physicalPerson_type_list,
    road_type_list,
    marital_situation_selected,
    person_type_selected,
    civility_selected,
    comp_list,
    getFormInformation
  } = props;

  // Effects
  useEffect(() => {
    getFormInformation();
  }, []);

  return (
    <Form className={styles.form}>
      <Typography variant="title">
        {I18n.t('physicalPersonCreation.generalInformation_form.civility')}
      </Typography>
      <div className={styles.row}>
        <Field
          row
          color="primary"
          name="civility_id"
          component={ReduxRadio}
          list={civility_type_list}
          margin="dense"
        />
      </div>
      <div className={styles.row}>
        <Field
          name="firstname"
          label={I18n.t('physicalPersonCreation.generalInformation_form.firstname')}
          component={ReduxTextField}
          margin="dense"
          className={styles.field}
        />
        <Field
          name="name"
          label={I18n.t('physicalPersonCreation.generalInformation_form.name')}
          component={ReduxTextField}
          margin="dense"
          className={styles.field}
        />
        {civility_selected === '2'
        && (
          <Field
            name="maiden_name"
            label={I18n.t('physicalPersonCreation.generalInformation_form.maiden_name')}
            component={ReduxTextField}
            margin="dense"
            className={styles.field}
          />
        )
        }
      </div>
      <div className={styles.row}>
        <Field
          name="marital_situation_id"
          id="marital_situation_id"
          label={I18n.t('physicalPersonCreation.generalInformation_form.marital_situation')}
          component={ReduxSelect}
          margin="dense"
          className={styles.field}
          list={maritalSituation_type_list}
        />
        {marital_situation_selected === 2 && (
          <Field
            name="matrimonial_regime_id"
            label={I18n.t('physicalPersonCreation.generalInformation_form.marital_regime')}
            component={ReduxSelect}
            className={classNames(styles.field, styles.xl)}
            list={maritalRegime_type_list}
          />
        )}
      </div>
      <div className={styles.row}>
        <div className={styles.birthDate}>
          <Typography variant="body2">
            {I18n.t('physicalPersonCreation.generalInformation_form.birthdate')}
          </Typography>
          <Field
            name="date_birth"
            id="exerciseStartDate"
            component={ReduxTextField}
            type="date"
            margin="dense"
            style={{ marginTop: '0px' }}
          />
        </div>
        <Field
          name="city_birth"
          label={I18n.t('physicalPersonCreation.generalInformation_form.birthCity')}
          component={ReduxTextField}
          margin="dense"
          className={classNames(styles.field, styles.lgContainer)}
        />
        <Field
          name="department_birth"
          label={I18n.t('physicalPersonCreation.generalInformation_form.birthDepartment')}
          component={ReduxTextField}
          margin="dense"
          className={classNames(styles.field, styles.md)}
        />
        <Field
          name="country_birth"
          label={I18n.t('physicalPersonCreation.generalInformation_form.birthCountry')}
          component={ReduxTextField}
          margin="dense"
          className={styles.field}
        />
      </div>
      <div className={styles.row}>
        <Field
          format={socialSecurityFormat}
          name="social_security_number"
          label={I18n.t('physicalPersonCreation.generalInformation_form.socialSecurity_number')}
          component={ReduxTextField}
          margin="dense"
          className={styles.field}
        />
      </div>
      <Typography variant="title">
        {I18n.t('physicalPersonCreation.generalInformation_form.address')}
      </Typography>
      <div className={styles.row}>
        <Field
          name="address_number"
          label={I18n.t('physicalPersonCreation.generalInformation_form.address_number')}
          component={ReduxTextField}
          margin="dense"
          className={classNames(styles.field, styles.xs, styles.extraMargin)}
        />
        <Field
          name="address_bis"
          label={I18n.t('physicalPersonCreation.generalInformation_form.comp')}
          component={ReduxSelect}
          margin="dense"
          className={classNames(styles.field, styles.md)}
          list={comp_list}
        />
        <Field
          name="road_type_id"
          label={I18n.t('physicalPersonCreation.generalInformation_form.roadType')}
          component={ReduxSelect}
          margin="dense"
          className={classNames(styles.field, styles.md)}
          list={road_type_list}
        />
        <Field
          name="street_name"
          label={I18n.t('physicalPersonCreation.generalInformation_form.roadName')}
          component={ReduxTextField}
          margin="dense"
          className={classNames(styles.field, styles.extraMargin)}
        />
      </div>
      <div className={styles.row}>
        <Field
          name="address_complement"
          label={I18n.t('physicalPersonCreation.generalInformation_form.address_complement')}
          component={ReduxTextField}
          margin="dense"
          className={classNames(styles.field, styles.xl)}
        />
      </div>
      <div className={styles.row}>
        <Field
          name="postal_code"
          label={I18n.t('physicalPersonCreation.generalInformation_form.zipCode')}
          component={PostalCodeTextField}
          margin="dense"
          className={classNames(styles.field, styles.md)}
        />
        <Field
          name="city"
          component={CitiesAutoComplete}
          className={classNames(styles.field, styles.xxl)}
          placeholder=""
          textFieldProps={{
            label: I18n.t('physicalPersonCreation.generalInformation_form.city'),
            InputLabelProps: {
              shrink: true
            }
          }}
        />
        <Field
          name="country_address"
          label={I18n.t('physicalPersonCreation.generalInformation_form.Country')}
          component={ReduxTextField}
          margin="dense"
          className={styles.field}
        />
      </div>
      <div className={styles.row}>
        <Field
          name="physical_person_type_id"
          label={I18n.t('physicalPersonCreation.generalInformation_form.physicalPerson_type')}
          component={ReduxSelect}
          margin="dense"
          className={styles.field}
          list={physicalPerson_type_list}
        />
        {person_type_selected === 4
        && (
          <Field
            name="organism"
            label={I18n.t('physicalPersonCreation.generalInformation_form.company')}
            component={ReduxTextField}
            margin="dense"
            className={styles.field}
          />
        )
        }
      </div>
      <Typography variant="title">
        {I18n.t('physicalPersonCreation.generalInformation_form.contacts')}
      </Typography>
      <div className={styles.row}>
        <PhysicalPersonContactsTable />
      </div>
      <div className={styles.row}>
        <Field
          name="comment"
          label={I18n.t('physicalPersonCreation.generalInformation_form.comment')}
          component={ReduxTextField}
          className={classNames(styles.field, styles.xl)}
          margin="dense"
          multiline
        />
      </div>
    </Form>
  );
}

// Props
GeneralInfo.propTypes = {
  comp_list: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  getFormInformation: PropTypes.func.isRequired,
  civility_type_list: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  maritalSituation_type_list: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  maritalRegime_type_list: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  physicalPerson_type_list: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  road_type_list: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })).isRequired,
  marital_situation_selected: PropTypes.number.isRequired,
  person_type_selected: PropTypes.number.isRequired,
  civility_selected: PropTypes.number.isRequired
};

export default GeneralInfo;
