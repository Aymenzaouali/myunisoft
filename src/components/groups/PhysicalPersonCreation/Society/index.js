import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray } from 'redux-form';
import PhysicalPersonSociety from 'containers/groups/Tables/PhysicalPersonSociety';
import { CompanyAutoComplete } from 'containers/reduxForm/Inputs';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import Info from 'components/basics/Info';
import {
  SocietyPPDeleteDialog
} from 'containers/groups/Dialogs';
import I18n from 'assets/I18n';
import styles from './SocietyTab.module.scss';


const SocietyInfo = (props) => {
  const {
    loadData,
    getSocietyPP,
    addSocietySelect,
    errorPart,
    isEditingSocietyPP,
    startEditSocietyPP,
    cancelEditSocietyPP,
    nbSelectSociety,
    updateSocietyPP,
    idPP,
    reinitAddedSociety,
    disabledSocietyInfoExport,
    csvSocietyInfoData,
    baseSocietyInfoFileName
  } = props;

  useEffect(() => {
    reinitAddedSociety();
    loadData();
  }, []);

  const [isDeleteOpenSociety, setIsDeleteOpenSociety] = useState(false);

  const societyPPRef = useRef(null);

  const cancel = async () => {
    await cancelEditSocietyPP();
    reinitAddedSociety();
    await getSocietyPP(idPP);
  };

  const deleteSocietyPP = () => {
    setIsDeleteOpenSociety(true);
  };

  const closeDeleteSocietyPP = () => {
    setIsDeleteOpenSociety(false);
  };

  let buttonSocietyPP = [
    {
      _type: 'icon',
      iconName: !isEditingSocietyPP ? 'icon-pencil' : 'icon-save',
      iconSize: 28,
      onClick: () => {
        if (!isEditingSocietyPP) {
          startEditSocietyPP();
          setTimeout(() => {
            if (societyPPRef && societyPPRef.current && societyPPRef.current.focus) {
              societyPPRef.current.focus();
            }
          }, 0);
        } else {
          updateSocietyPP(idPP);
          reinitAddedSociety();
        }
      }
    },
    {
      _type: 'icon',
      iconName: 'icon-trash',
      iconSize: 28,
      color: 'error',
      titleInfoBulle: I18n.t('tooltips.delete'),
      disabled: (nbSelectSociety === 0 || isEditingSocietyPP),
      onClick: () => deleteSocietyPP()
    },
    {
      _type: 'component',
      disabled: disabledSocietyInfoExport,
      color: 'primary',
      component: <CSVButton csvData={csvSocietyInfoData} baseFileName={baseSocietyInfoFileName} />
    }
  ];

  if (isEditingSocietyPP) {
    buttonSocietyPP = [{
      _type: 'string',
      text: 'Annuler',
      color: 'default',
      variant: 'outlined',
      onClick: () => cancel()
    },
    ...buttonSocietyPP
    ];
  }

  const onSocietyChange = (value) => {
    addSocietySelect(value.value);
  };

  return (
    <div>
      <div className={styles.selectionContainer}>
        <Field
          name="addedSociety"
          id="legalPerson"
          component={CompanyAutoComplete}
          label={I18n.t('physicalPersonCreation.society_form.labelPlaceholder')}
          className={styles.selectSocietyField}
          onChangeValues={onSocietyChange}
          margin="none"
        />
        <div>
          <InlineButton buttons={buttonSocietyPP} />
        </div>
      </div>
      <div className={styles.tableWrap}>
        {errorPart && <Info message={I18n.t('companyCreation.errors.social_part_pp')} />}
        <FieldArray
          component={PhysicalPersonSociety}
          inputRef={societyPPRef}
        />
      </div>
      <SocietyPPDeleteDialog
        isOpen={isDeleteOpenSociety}
        onClose={closeDeleteSocietyPP}
      />
    </div>
  );
};

SocietyInfo.propTypes = {
  loadData: PropTypes.func.isRequired,
  addSocietySelect: PropTypes.func.isRequired,
  getSocietyPP: PropTypes.func.isRequired,
  errorPart: PropTypes.bool.isRequired,
  isEditingSocietyPP: PropTypes.bool.isRequired,
  disabledSocietyInfoExport: PropTypes.bool.isRequired,
  csvSocietyInfoData: PropTypes.array.isRequired,
  baseSocietyInfoFileName: PropTypes.string.isRequired,
  startEditSocietyPP: PropTypes.func.isRequired,
  cancelEditSocietyPP: PropTypes.func.isRequired,
  nbSelectSociety: PropTypes.number.isRequired,
  updateSocietyPP: PropTypes.func.isRequired,
  idPP: PropTypes.string.isRequired,
  reinitAddedSociety: PropTypes.func.isRequired
};

export default SocietyInfo;
