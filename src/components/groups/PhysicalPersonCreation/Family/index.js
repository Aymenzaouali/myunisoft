import React from 'react';
import PropTypes from 'prop-types';
import FamilyBoundariesTable from 'components/groups/Tables/Family';
import { Field, FieldArray } from 'redux-form';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';

import styles from './FamilyTab.module.scss';

const FamilyInfo = (props) => {
  const {
    arrayofPhysicalPersons,
    addRelative,
    societyId,
    onCustomFocus,
    csvFamilyInfoData,
    baseFamilyInfoFileName
  } = props;

  return (
    <div>
      <div className={styles.selectionContainer}>
        <Field
          component={AutoComplete}
          options={arrayofPhysicalPersons}
          name="addedFamily"
          onChangeValues={event => addRelative(event, societyId)}
          label={I18n.t('physicalPersonCreation.familyBoundaries_form.autocomplete')}
          className={styles.selectPersonField}
          onFocus={() => { onCustomFocus(); }}
        />
        <InlineButton buttons={[
          {
            _type: 'component',
            color: 'primary',
            component: <CSVButton
              csvData={csvFamilyInfoData}
              baseFileName={baseFamilyInfoFileName}
            />
          }
        ]}
        />
      </div>
      <FieldArray
        name="family_array"
        component={FamilyBoundariesTable}
      />
    </div>
  );
};


FamilyInfo.propTypes = {
  societyId: PropTypes.number.isRequired,
  onCustomFocus: PropTypes.func.isRequired,
  addRelative: PropTypes.func.isRequired,
  csvFamilyInfoData: PropTypes.array.isRequired,
  baseFamilyInfoFileName: PropTypes.string.isRequired,
  arrayofPhysicalPersons: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number,
    label: PropTypes.string
  })).isRequired
};

export default FamilyInfo;
