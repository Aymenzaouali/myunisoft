import React from 'react';
import Button from 'components/basics/Buttons/Button';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';

const UserInfo = (props) => {
  const {
    redirectToRightManagement
  } = props;

  return (
    <>
      <Button
        variant="contained"
        color="primary"
        onClick={() => redirectToRightManagement()}
      >
        {I18n.t('physicalPersonCreation.companyAccess_form.rightManagement')}
      </Button>
    </>
  );
};

UserInfo.propTypes = {
  redirectToRightManagement: PropTypes.func.isRequired
};

export default UserInfo;


// *** OLD VERSION: Maybe we need to put it again after ***/
// import { Checkbox } from '@material-ui/core';
// import UserTable from 'containers/groups/Tables/Utilisateur';

// import styles from './UserInfo.module.scss';

// class UserInfo extends React.PureComponent {
//   state = {
//     isUser: false
//   };

//   render() {
//     const { isUser } = this.state;

//     return (
//       <div>
//         <div className={styles.checkContainer}>
//           <Checkbox
//             color="primary"
//             checked={isUser}
//             value={isUser}
//             onChange={() => this.setState({ isUser: !isUser })}
//           />
//           <div>{I18n.t('physicalPersonCreation.companyAccess_form.isUser')}</div>
//         </div>
//         {
//           isUser
//           && <UserTable />
//         }
//       </div>
//     );
//   }
// }
