import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import StepperHeader from 'containers/groups/Headers/PhysicalPersonCreationStepper';

import GeneralInfo from 'containers/groups/PhysicalPersonCreation/GeneralInfo';
import SocietyInfo from 'containers/groups/PhysicalPersonCreation/Society';
import FamilyInfo from 'containers/groups/PhysicalPersonCreation/Family';
import DocumentInfo from 'containers/groups/PhysicalPersonCreation/Document';
import UserInfo from 'containers/groups/PhysicalPersonCreation/User';
import EmployeeInfo from 'containers/groups/PhysicalPersonCreation/EmployeeInfo';

import { InlineButton } from 'components/basics/Buttons';
import I18n from 'assets/I18n';

import styles from './PhysicalPersonCreation.module.scss';

const PhysicalPersonCreation = (props) => {
  const {
    title,
    selectTab,
    cancelForm,
    activeTab,
    handleSubmit,
    goToNextTab,
    validateForm,
    getLinkType,
    getSociety,
    isUser
  } = props;

  const physicalPersonCreationStepComponentMap = {
    0: () => (<GeneralInfo />),
    1: () => (<FamilyInfo />),
    2: () => (<SocietyInfo />),
    3: () => (<EmployeeInfo />),
    4: () => (<DocumentInfo />),
    5: () => (isUser && <UserInfo />)
  };

  useEffect(() => {
    getSociety();
    getLinkType();
    selectTab(0);
  }, []);

  return (
    <div className={styles.globalContainer}>
      <StepperHeader
        title={title}
        openTab={selectTab}
        onClose={cancelForm}
        isUser={isUser}
      />
      <div className={styles.bodyContainer}>
        {physicalPersonCreationStepComponentMap[activeTab]()}
      </div>

      <div className={styles.buttonContainer}>
        <div>
          <InlineButton
            marginDirection="right"
            buttons={[
              {
                _type: 'string',
                text: I18n.t('physicalPersonCreation.previous'),
                onClick: () => selectTab(activeTab - 1),
                size: 'large',
                disabled: activeTab === 0
              },
              {
                _type: 'string',
                text: I18n.t('physicalPersonCreation.next'),
                onClick: handleSubmit(goToNextTab),
                size: 'large',
                disabled: (isUser && activeTab === 5) || (!isUser && activeTab === 4)
              }]
            }
          />
        </div>
        {activeTab !== 5 && (
          <div>
            <InlineButton
              marginDirection="right"
              buttons={[
                {
                  _type: 'string',
                  text: I18n.t('physicalPersonCreation.validate'),
                  onClick: handleSubmit(validateForm),
                  size: 'large'
                }]
              }
            />
          </div>
        )}
      </div>
    </div>
  );
};

PhysicalPersonCreation.defaultProps = {
  title: I18n.t('physicalPersonCreation.title'),
  selectedPhysicalPerson: {}
};

PhysicalPersonCreation.propTypes = {
  validateForm: PropTypes.func.isRequired,
  getSociety: PropTypes.func.isRequired,
  selectedPhysicalPerson: PropTypes.shape({}),
  getLinkType: PropTypes.func.isRequired,
  title: PropTypes.string,
  selectTab: PropTypes.func.isRequired,
  cancelForm: PropTypes.func.isRequired,
  activeTab: PropTypes.number.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  goToNextTab: PropTypes.func.isRequired,
  isUser: PropTypes.bool.isRequired
};

export default PhysicalPersonCreation;
