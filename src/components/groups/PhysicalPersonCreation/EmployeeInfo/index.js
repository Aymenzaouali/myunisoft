import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray } from 'redux-form';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import PhysicalPersonEmployee from 'containers/groups/Tables/PhysicalPersonEmployee';
import { CompanyAutoComplete } from 'containers/reduxForm/Inputs';
import {
  EmployeePPDeleteDialog
} from 'containers/groups/Dialogs';
import I18n from 'assets/I18n';
import styles from './EmployeeTab.module.scss';


const EmployeeInfo = (props) => {
  const {
    loadData,
    getEmployee,
    addEmployeeSelect,
    isEditingEmployeePP,
    startEditEmployeePP,
    cancelEditEmployeePP,
    nbSelectEmployee,
    updateEmployeePP,
    idPP,
    reinitAddedSociety,
    disabledEmployeeInfoExport,
    csvEmployeeInfoData,
    baseEmployeeInfoFileName
  } = props;

  useEffect(() => {
    reinitAddedSociety();
    loadData();
  }, []);

  const [isDeleteOpenEmployee, setIsDeleteOpenEmployee] = useState(false);

  const employeePPRef = useRef(null);

  const cancel = async () => {
    await cancelEditEmployeePP();
    reinitAddedSociety();
    await getEmployee(idPP);
  };

  const deleteEmployeePP = () => {
    setIsDeleteOpenEmployee(true);
  };

  const closeDeleteEmployeePP = () => {
    setIsDeleteOpenEmployee(false);
  };

  let buttonEmployeePP = [
    {
      _type: 'icon',
      iconName: !isEditingEmployeePP ? 'icon-pencil' : 'icon-save',
      iconSize: 28,
      onClick: () => {
        if (!isEditingEmployeePP) {
          startEditEmployeePP();
          setTimeout(() => {
            if (employeePPRef && employeePPRef.current && employeePPRef.current.focus) {
              employeePPRef.current.focus();
            }
          }, 0);
        } else {
          updateEmployeePP(idPP);
          reinitAddedSociety();
        }
      }
    },
    {
      _type: 'icon',
      iconName: 'icon-trash',
      iconSize: 28,
      color: 'error',
      titleInfoBulle: I18n.t('tooltips.delete'),
      disabled: (nbSelectEmployee === 0 || isEditingEmployeePP),
      onClick: () => deleteEmployeePP()
    },
    {
      _type: 'component',
      disabled: disabledEmployeeInfoExport,
      color: 'primary',
      component: <CSVButton csvData={csvEmployeeInfoData} baseFileName={baseEmployeeInfoFileName} />
    }
  ];

  if (isEditingEmployeePP) {
    buttonEmployeePP = [{
      _type: 'string',
      text: 'Annuler',
      color: 'default',
      variant: 'outlined',
      onClick: () => cancel()
    },
    ...buttonEmployeePP
    ];
  }

  const onEmployeeChange = (value) => {
    addEmployeeSelect(value.value);
  };

  return (
    <div>
      <div className={styles.selectionContainer}>
        <Field
          name="addedSociety"
          id="legalPerson"
          component={CompanyAutoComplete}
          label={I18n.t('physicalPersonCreation.society_form.labelPlaceholder')}
          className={styles.selectSocietyField}
          onChangeValues={onEmployeeChange}
          margin="none"
        />
        <div>
          <InlineButton buttons={buttonEmployeePP} />
        </div>
      </div>
      <div>
        <FieldArray
          name="firm_array"
          component={PhysicalPersonEmployee}
        />
      </div>
      <EmployeePPDeleteDialog
        isOpen={isDeleteOpenEmployee}
        onClose={closeDeleteEmployeePP}
      />
    </div>
  );
};

EmployeeInfo.propTypes = {
  loadData: PropTypes.func.isRequired,
  getEmployee: PropTypes.func.isRequired,
  addEmployeeSelect: PropTypes.func.isRequired,
  isEditingEmployeePP: PropTypes.bool.isRequired,
  disabledEmployeeInfoExport: PropTypes.bool.isRequired,
  csvEmployeeInfoData: PropTypes.array.isRequired,
  baseEmployeeInfoFileName: PropTypes.string.isRequired,
  startEditEmployeePP: PropTypes.func.isRequired,
  cancelEditEmployeePP: PropTypes.func.isRequired,
  nbSelectEmployee: PropTypes.number.isRequired,
  updateEmployeePP: PropTypes.func.isRequired,
  idPP: PropTypes.string.isRequired,
  reinitAddedSociety: PropTypes.func.isRequired
};

export default EmployeeInfo;
