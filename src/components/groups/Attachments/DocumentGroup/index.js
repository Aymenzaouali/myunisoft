import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Badge, IconButton, Typography } from '@material-ui/core';

import FontIcon from 'components/basics/Icon/Font';

import { documentShape } from '../props';
import Image from '../Image';
import SelectCorner from '../SelectCorner';

import styles from './DocumentGroup.module.scss';

// Components
const DocumentGroup = (props) => {
  const {
    documents,
    selected, attached, onSelect,
    expanded, onExpand, onAskOverview
  } = props;

  const images = [];

  for (let i = 0; i < documents.length; i += 1) {
    const isFile = documents[i].thumbnail instanceof File;
    const src = isFile ? URL.createObjectURL(documents[i].thumbnail) : documents[i].thumbnail;

    images.push(
      <div
        key={documents[i].document_id}
        className={styles.thumbnail}
        style={{ right: (expanded || i < 2) ? 0 : (180 + 110 * (i - 2)) }}
      >
        <Image
          className={styles.image}
          src={src}
          hidden={(!expanded && i > 2)}
          onUnload={() => isFile && URL.revokeObjectURL(src)}
          onClick={() => onAskOverview(documents[i])}
        />
        <Typography className={styles.name} variant="caption" align="center" noWrap>
          { documents[i].name }
        </Typography>
      </div>
    );
  }

  return (
    <SelectCorner
      className={styles.document}
      selected={selected}
      attached={attached}

      onSelect={onSelect}
    >
      <Badge
        badgeContent={documents.length}
        invisible={expanded || documents.length < 3}

        color="primary"
        classes={{
          root: classNames(styles.thumbnails, { [styles.collapsed]: !expanded }),
          badge: styles.badge
        }}
        style={{ width: 110 * documents.length }}
      >
        { images }
      </Badge>
      <IconButton
        className={styles.expandBtn}
        onClick={() => onExpand(!expanded)}
      >
        { expanded ? (
          <FontIcon name="icon-minus" />
        ) : (
          <FontIcon name="icon-plus" />
        ) }
      </IconButton>
      <Typography className={styles.name} variant="caption" align="center" noWrap>
        { documents[0].name }
      </Typography>
    </SelectCorner>
  );
};

// Props
DocumentGroup.propTypes = {
  documents: PropTypes.arrayOf(documentShape.isRequired).isRequired,
  selected: PropTypes.bool,
  attached: PropTypes.bool,
  expanded: PropTypes.bool,

  onSelect: PropTypes.func,
  onExpand: PropTypes.func,
  onAskOverview: PropTypes.func
};

DocumentGroup.defaultProps = {
  selected: false,
  attached: false,
  expanded: false,

  onSelect: null,
  onExpand: null,
  onAskOverview: null
};

export default DocumentGroup;
