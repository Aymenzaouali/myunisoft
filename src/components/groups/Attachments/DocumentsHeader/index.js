import React, { useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { Typography } from '@material-ui/core';

import I18n from 'assets/I18n';

import { DeleteConfirmation } from 'containers/groups/Dialogs';
import { InlineButton } from 'components/basics/Buttons';

import { documentGroupShape } from '../props';

import styles from './DocumentsHeader.module.scss';

// Component
const DocumentsHeader = (props) => {
  const [deleteConfirmationDialog, setDeleteConfirmationDialog] = useState(false);

  const {
    selected,
    onJoin, onAttach, onDetach, onDelete
  } = props;

  // Rendering
  let count = 0;
  let groups = 0;
  const docs = [];

  _.values(selected).forEach((doc) => {
    if (doc) {
      count += 1;

      if (doc.document_attached_id) {
        groups += 1;
      }

      docs.push(doc);
    }
  });

  return (
    <div className={styles.header}>
      <Typography className={styles.count} variant="h6">{ I18n.t('attachments.toolBar.total', { count }) }</Typography>
      <InlineButton
        buttons={[
          onJoin ? {
            _type: 'icon',
            iconName: 'icon-attachment',
            iconSize: 25,
            disabled: count === 0,
            onClick: () => onJoin(docs),
            titleInfoBulle: I18n.t('tooltips.join')
          } : null,
          {
            _type: 'icon',
            iconName: 'icon-attach',
            iconSize: 25,
            disabled: (count <= 1) || (groups > 1) || (count === groups),
            onClick: () => onAttach(),
            titleInfoBulle: I18n.t('tooltips.attach')
          },
          {
            _type: 'icon',
            iconName: 'icon-close',
            iconSize: 25,
            disabled: (count === 0) || (groups !== count),
            onClick: () => onDetach(),
            titleInfoBulle: I18n.t('tooltips.detach')
          },
          onDelete ? {
            _type: 'icon',
            iconName: 'icon-trash',
            color: 'error',
            iconSize: 25,
            disabled: count === 0,
            onClick: () => setDeleteConfirmationDialog(true),
            titleInfoBulle: I18n.t('tooltips.delete')
          } : null
        ]}
      />
      <DeleteConfirmation
        deleteReportAndForm={onDelete}
        isOpen={deleteConfirmationDialog}
        onClose={() => setDeleteConfirmationDialog(false)}
        message={I18n.t('deleteDialog.mainMessage')}
      />
    </div>
  );
};

// Props
DocumentsHeader.propTypes = {
  selected: PropTypes.objectOf(documentGroupShape).isRequired,

  onJoin: PropTypes.func.isRequired,
  onAttach: PropTypes.func.isRequired,
  onDetach: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
};

export default DocumentsHeader;
