import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import CheckBox from 'components/basics/CheckBox';
import FontIcon from 'components/basics/Icon/Font';

import styles from './SelectCorner.module.scss';

// Component
const SelectCorner = (props) => {
  const {
    className, children,
    selected, attached, onSelect
  } = props;

  // Rendering
  return (
    <div className={classNames(styles.container, className)}>
      { children }
      <div className={styles.corner}>
        <CheckBox
          checked={selected}
          onChange={(_, value) => { if (onSelect) onSelect(value); }}

          color="primary"
          classes={{ root: styles.checkbox }}
        />
        { attached && (
          <FontIcon name="icon-attachment" />
        ) }
      </div>
    </div>
  );
};

// Props
SelectCorner.propTypes = {
  className: PropTypes.string,
  selected: PropTypes.bool,
  attached: PropTypes.bool,
  onSelect: PropTypes.func,

  children: PropTypes.node.isRequired
};

SelectCorner.defaultProps = {
  className: undefined,
  selected: false,
  attached: false,
  onSelect: null
};

export default SelectCorner;
