import React from 'react';
import PropTypes from 'prop-types';

import { Typography } from '@material-ui/core';

import { documentShape } from '../props';
import Image from '../Image';
import SelectCorner from '../SelectCorner';

import styles from './Document.module.scss';

// Component
const Document = (props) => {
  const {
    document, selected, attached,
    onSelect, onAskOverview
  } = props;

  const isFile = document.thumbnail instanceof File;
  const src = isFile ? URL.createObjectURL(document.thumbnail) : document.thumbnail;

  return (
    <SelectCorner
      className={styles.document}
      selected={selected}
      attached={attached}

      onSelect={onSelect}
    >
      <Image
        className={styles.thumbnail}
        src={src}
        onUnload={() => isFile && URL.revokeObjectURL(src)}
        onClick={() => onAskOverview()}
      />
      <Typography
        className={styles.name}
        variant="caption"
        align="center"
        noWrap
      >
        { document.name }
      </Typography>
    </SelectCorner>
  );
};

// Props
Document.propTypes = {
  document: documentShape.isRequired,
  selected: PropTypes.bool,
  attached: PropTypes.bool,
  onSelect: PropTypes.func,
  onAskOverview: PropTypes.func
};

Document.defaultProps = {
  selected: false,
  attached: false,
  onSelect: null,
  onAskOverview: null
};

export default Document;
