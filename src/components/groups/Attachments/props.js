import PropTypes from 'prop-types';

export const documentShape = PropTypes.shape({
  document_id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  thumbnail: PropTypes.string.isRequired
});

export const documentGroupShape = PropTypes.shape({
  document_id: PropTypes.number.isRequired,
  documents: PropTypes.arrayOf(documentShape).isRequired
});
