import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { Typography } from '@material-ui/core';

import I18n from 'assets/I18n';

import { useWindowState } from 'helpers/hooks';

import Loader from 'components/basics/Loader';
import Window from 'components/basics/Window';

import Document from './Document';
import DocumentGroup from './DocumentGroup';
import DocumentsHeader from './DocumentsHeader';

import { documentGroupShape } from './props';
import styles from './Attachments.module.scss';

// Component
const Attachments = (props) => {
  const {
    docs, loading, error, selected, attached, expanded,
    limit, page,
    getDocuments, selectDocument, expandDocument,
    onJoin, onAttach, onDetach, onDelete
  } = props;

  // State
  const popup = useWindowState();

  // Effects
  useEffect(() => {
    if (getDocuments) getDocuments(page, limit);
  }, [limit, page]);

  // Rendering
  const isSelected = doc => !!(selected[doc.document_id]);
  const isAttached = doc => !!(attached[doc.document_id]);
  const isExpanded = doc => !!(expanded[doc.document_id]);

  return (
    <div className={styles.container}>
      <DocumentsHeader
        selected={selected}

        onJoin={onJoin}
        onAttach={onAttach}
        onDetach={onDetach}
        onDelete={onDelete}
      />
      <div className={styles.documents}>
        { docs.map(doc => (doc.documents.length === 1
          ? (
            <Document
              key={doc.document_id}
              document={doc.documents[0]}

              selected={isSelected(doc)}
              attached={isAttached(doc)}
              onSelect={v => selectDocument(doc.document_id, v && doc)}
              onAskOverview={() => popup.open({ documents: doc.documents }, true)}
            />
          ) : (
            <DocumentGroup
              key={doc.document_id}
              documents={doc.documents}

              selected={isSelected(doc)}
              attached={isAttached(doc)}
              onSelect={v => selectDocument(doc.document_id, v && doc)}
              onAskOverview={document => popup.open({ document, documents: doc.documents }, true)}

              expanded={isExpanded(doc)}
              onExpand={v => expandDocument(doc.document_id, v)}
            />
          )
        )) }
        { loading && (
          <Loader className={styles.loader} />
        ) }
        { error && (
          <Typography color="error">{ I18n.t('errors.default') }</Typography>
        ) }
      </div>
      <Window
        path="/overview"
        window_id="overview"
        features={`width=${window.innerWidth / 2},left=${window.screenX + (window.innerWidth / 2)}`}

        {...popup.props}
      />
    </div>
  );
};

Attachments.defaultProps = {
  showPagination: true,
  getDocuments: null,
  onChangeRowsPerPage: null,
  onChangePage: null,
  onJoin: null,
  onDelete: null
};

// Props
Attachments.propTypes = {
  docs: PropTypes.arrayOf(documentGroupShape).isRequired,
  selected: PropTypes.objectOf(documentGroupShape).isRequired,
  attached: PropTypes.objectOf(documentGroupShape).isRequired,
  expanded: PropTypes.objectOf(PropTypes.bool).isRequired,

  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  limit: PropTypes.number.isRequired,
  rows: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  pages: PropTypes.number.isRequired,

  getDocuments: PropTypes.func,
  selectDocument: PropTypes.func.isRequired,
  expandDocument: PropTypes.func.isRequired,

  onChangeRowsPerPage: PropTypes.func,
  onChangePage: PropTypes.func,

  onJoin: PropTypes.func,
  onAttach: PropTypes.func.isRequired,
  onDetach: PropTypes.func.isRequired,
  onDelete: PropTypes.func,

  showPagination: PropTypes.bool
};

export default Attachments;
