import React, { useContext, useState, useEffect } from 'react';
import {
  Divider, Grid,
  List, ListItem, ListItemText,
  Link, Typography
} from '@material-ui/core';

import I18n from 'assets/I18n';
import { getCloudUrl } from 'helpers/file';

import OpenerContext from 'context/OpenerContext';

import styles from './overviewGroup.module.scss';

// Component
function Overview() {
  // Context
  const { message } = useContext(OpenerContext);

  // State
  const [files, setFiles] = useState([]);
  const [selectedFile, setSelectedFile] = useState(null);

  // Functions
  const onMessage = (e) => {
    if (e.data.type === 'files') {
      setFiles(e.data.files);
      setSelectedFile(e.data.files[0] || null);
    }
  };

  const fileIdentity = file => file.token || file.name;

  const renderFile = (file) => {
    const { thumbnail } = file;
    const isFile = file instanceof File || thumbnail instanceof File;
    // TODO refactor because of memory leak

    const src = isFile ? URL.createObjectURL(thumbnail || file) : thumbnail;
    const type = thumbnail ? thumbnail.type : file.type || src.type;

    // show picture
    if (/image/.test(type) && !file.token) {
      return (
        <img alt={file.name} src={src} className={styles['overview-img']} />
      );
    }

    // show pdf
    if (type === 'application/pdf' && !file.token) {
      return <iframe className={styles.iframe} title={file.name} src={src} />;
    }

    // show cloud page
    if (file.token) {
      return <iframe className={styles.iframe} title={file.name} src={getCloudUrl(file.token)} />;
    }

    // show download link
    return (
      <Link href={src} download>{ file.name }</Link>
    );
  };

  // Effect
  useEffect(() => {
    window.addEventListener('message', onMessage, false);

    return () => {
      window.removeEventListener('message', onMessage);
    };
  }, []);

  useEffect(() => {
    if (message.documents) {
      setFiles(message.documents);
      setSelectedFile(message.document || message.documents[0] || null);
    }
  }, [message.documents, message.document]);

  // Rendering
  return (
    <Grid container className={styles.root} wrap="nowrap">
      <Grid className={styles.panel} item xs="auto">
        <Typography className={styles.title} variant="h6" align="center">{ I18n.t('overview.title') }</Typography>
        <Divider />
        <List>
          { files.map(file => (
            <ListItem
              key={fileIdentity(file)}

              button
              selected={
                (selectedFile != null) && (fileIdentity(file) === fileIdentity(selectedFile))
              }

              onClick={() => { setSelectedFile(file); }}
            >
              <ListItemText primaryTypographyProps={{ noWrap: true }}>
                { file.name }
              </ListItemText>
            </ListItem>
          )) }
        </List>
      </Grid>
      <Grid item xs container alignItems="center" justify="center">
        { (selectedFile != null) && renderFile(selectedFile) }
      </Grid>
    </Grid>
  );
}

export default Overview;
