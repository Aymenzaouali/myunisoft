import React from 'react';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import OCRTable from 'containers/groups/Tables/OCR';
import { TrackingOcrFilterContainer } from 'containers/groups/Filters';
import Button from 'components/basics/Buttons/Button';
import I18n from 'assets/I18n';
import { routesByKey } from 'helpers/routes';
import './styles.scss';

class TrackingOCR extends React.Component {
  async componentDidMount() {
    const {
      getTrackingOcr
    } = this.props;

    await getTrackingOcr();
  }


  render() {
    const {
      push
    } = this.props;

    return (
      <div className="trackingOcr__container">
        <div className="trackingOcr__headerUp">
          <div className="trackingOcr__headerU-title">
            <Typography variant="h1">
              {I18n.t('OCR.trackingTable.title')}
            </Typography>
          </div>
          <div className="trackingOcr__headerUp-button">
            <Button size="medium" variant="contained" color="primary" onClick={() => push(routesByKey.ocr)}>
              {I18n.t('OCR.trackingTable.sendNewDoc')}
            </Button>
          </div>
        </div>
        <TrackingOcrFilterContainer />
        <div className="trackingOcr__table">
          <OCRTable />
        </div>
      </div>
    );
  }
}

TrackingOCR.propTypes = {
  getTrackingOcr: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired
};


export default TrackingOCR;
