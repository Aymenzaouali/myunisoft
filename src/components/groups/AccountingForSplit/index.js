import React, { useContext } from 'react';
import Accounting from 'containers/groups/Accounting';
import NewAccounting from 'containers/groups/NewAccounting';
import SplitContext from 'context/SplitContext';

export default () => {
  const { args: { showNewAccounting } } = useContext(SplitContext);
  return (
    <>
      {showNewAccounting && <NewAccounting showSplitToolbar={false} />}
      <Accounting />
    </>
  );
};
