import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import I18n from 'assets/I18n';
import { Field } from 'redux-form';
import classNames from 'classnames';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { OwnerCompanyAutoComplete } from 'containers/reduxForm/Inputs';
import Accordion from 'components/groups/Accordion';
import NameAddress from 'components/groups/Forms/NameAddress';
import Legal from 'components/groups/Forms/Legal';
import Accounting from 'containers/groups/Forms/Accounting';
import Access from 'components/groups/Forms/Access';

import styles from 'components/groups/CompanyCreation/CompanyCreation.module.scss';


class GeneralInfo extends PureComponent {
  render() {
    const { getDataBySiren, selectedCompany } = this.props;
    const panels = [
      {
        key: 'nameAddress',
        label: I18n.t('companyCreation.nameAddress'),
        Component: NameAddress
      },
      {
        key: 'accounting',
        label: 'Comptabilité',
        Component: Accounting,
        colorError: true
      },
      {
        key: 'legal',
        label: I18n.t('companyCreation.legal'),
        Component: Legal,
        handlers: { getDataBySiren }
      },
      // TODO: Implement when BE is ready
      // {
      //   key: 'contacts',
      //   label: I18n.t('companyCreation.contacts'),
      //   Component: () => <div>Hello 3</div>
      // },
      {
        key: 'access',
        label: I18n.t('companyCreation.access'),
        Component: Access
      }
    ];

    return (
      <Fragment>
        <div className={classNames(styles.row, styles.ownerCompanyRow)}>
          <Field
            name="owner_company"
            id="owner_company"
            component={OwnerCompanyAutoComplete}
            textFieldProps={{
              label: I18n.t('companyCreation.ownerCompany'),
              InputLabelProps: {
                shrink: true
              }
            }}
          />
        </div>
        <div className={classNames(styles.row, styles.ownerCompanyRow)}>
          <Field
            name="siret"
            id="siret"
            component={ReduxTextField}
            label={I18n.t('companyCreation.siret')}
            margin="none"
            inputProps={{ maxlength: 14 }}
            onBlur={getDataBySiren}
          />
          {selectedCompany
          && (
            <Field
              name="society_id"
              id="society_id"
              component={ReduxTextField}
              margin="none"
              type="text"
              label={I18n.t('companyCreation.societyId')}
              disabled
            />
          )}
        </div>
        <Accordion
          panels={panels}
        />
      </Fragment>
    );
  }
}

GeneralInfo.propTypes = {
  getDataBySiren: PropTypes.func.isRequired,
  selectedCompany: PropTypes.bool.isRequired
};

export default GeneralInfo;
