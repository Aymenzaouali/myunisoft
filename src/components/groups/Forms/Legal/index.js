import React, { PureComponent } from 'react';
import I18n from 'assets/I18n';
import {
  Field,
  Form
} from 'redux-form';
import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import {
  LegalFormsAutoComplete,
  // Not for V0
  // AccountantsAutoComplete,
  CompanyRegistrationPlaceAutoComplete,
  ApeAutoComplete
} from 'containers/reduxForm/Inputs';

import styles from 'components/groups/CompanyCreation/CompanyCreation.module.scss';

class Legal extends PureComponent {
  render() {
    return (
      <Form>
        <div className={styles.autocomplete}>
          <Field
            name="ape"
            id="ape"
            component={ApeAutoComplete}
            textFieldProps={{
              label: I18n.t('companyCreation.ape'),
              InputLabelProps: {
                shrink: true
              }
            }}
          />
        </div>
        <div className={styles.autocomplete}>
          <Field
            name="activity"
            id="activity"
            component={ReduxTextField}
            label={I18n.t('companyCreation.activity')}
            margin="none"
          />
        </div>
        <div className={styles.autocomplete}>
          <Field
            name="legal_form"
            id="legal_form"
            component={LegalFormsAutoComplete}
            textFieldProps={{
              label: I18n.t('companyCreation.legal_form'),
              InputLabelProps: {
                shrink: true
              }
            }}
          />
        </div>
        <div className={styles.row}>
          <Field
            name="registration_date"
            id="registration_date"
            component={ReduxMaterialDatePicker}
            label={I18n.t('companyCreation.registration_date')}
            margin="none"
          />
          {/* Not needed for V0 */}
          {/* <Field
            name="closeDate"
            id="closeDate"
            component={ReduxMaterialDatePicker}
            label={I18n.t('companyCreation.closeDate')}
            margin="none"
          /> */}
        </div>
        <div className={styles.autocomplete}>
          <Field
            name="register"
            id="register"
            component={CompanyRegistrationPlaceAutoComplete}
            label={I18n.t('companyCreation.registrationPlace')}
            margin="none"
          />
        </div>
        <div className={styles.row}>
          {/* Not needed for V0 */}
          {/* <Field
            name="accounting_expert"
            id="accounting_expert"
            component={AccountantsAutoComplete}
            textFieldProps={{
              label: I18n.t('companyCreation.accountingExpert'),
              InputLabelProps: {
                shrink: true
              }
            }}
          /> */}
        </div>
      </Form>
    );
  }
}

export default Legal;
