import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import {
  DiaryTypeAutoComplete,
  AccountAutoComplete
} from 'containers/reduxForm/Inputs';
import { withStyles } from '@material-ui/core';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import I18n from 'assets/I18n';
import { deleteSpace } from 'helpers/format';
import _ from 'lodash';

const styles = () => ({
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  block: {
    display: 'flex',
    flexDirection: 'row'
  },
  diaryCode: {
    width: '91px',
    height: '52px'
  },
  diaryLabel: {
    width: '296px',
    height: '52px',
    marginLeft: '20px'
  },
  diaryType: {
    height: '52px',
    marginTop: '20px'
  },
  diaryAccountNumber: {
    width: 'auto',
    height: '52px',
    marginLeft: '10px'
  }
});

const DiaryForm = (props) => {
  const {
    classes,
    bankType,
    cashType,
    checkDiaryCode,
    diaryDetail,
    resetDiaryAccountNumber,
    codeDisabled,
    labelDisabled,
    diaryTypeDisabled
  } = props;

  const {
    blocked
  } = diaryDetail;

  const [error, setError] = useState(undefined);

  const onCustomChange = async (value) => {
    try {
      const jnx = await checkDiaryCode(value);
      if (!_.isEmpty(jnx)) {
        setError(I18n.t('diary.dialog.unavailable'));
      } else {
        setError(undefined);
      }
    } catch (error) {
      console.log(error); // eslint-disable-line
    }
  };

  return (
    <div className={classes.container}>
      <form>
        <div className={classes.block}>
          <Field
            format={deleteSpace}
            name="diary_code"
            error={error}
            label={I18n.t('diary.dialog.code')}
            onCustomChange={onCustomChange}
            component={ReduxTextField}
            className={classes.diaryCode}
            disabled={blocked || codeDisabled}
            required
          />
          <Field
            name="diary_label"
            label={I18n.t('diary.dialog.label')}
            component={ReduxTextField}
            className={classes.diaryLabel}
            required
            disabled={labelDisabled}
          />
        </div>
        <div className={classes.block}>
          <div className={classes.diaryType}>
            <Field
              name="diary_type"
              component={DiaryTypeAutoComplete}
              onChangeValues={resetDiaryAccountNumber}
              menuPosition="fixed"
              placeholder=""
              textFieldProps={{
                label: I18n.t('diary.dialog.type'),
                InputLabelProps: {
                  shrink: true
                },
                required: true
              }}
              isDisabled={diaryTypeDisabled}
            />
          </div>
          {(bankType || cashType) && (
            <div className={classes.diaryAccountNumber}>
              <Field
                color="primary"
                name="diary_accountNumber"
                prefixed={bankType ? '512' : '53'}
                // TODO: La création de compte n'est pas implémenter pour cette fenêtre
                // elle n'a a mon avis pas été prévu d'embler
                // ceci passera donc en évolution
                // write description in English
                resetOnWrite
                menuPosition="fixed"
                placeholder=""
                selectStyles={{
                  input: { paddingLeft: 25 }
                }}
                component={AccountAutoComplete}
                textFieldProps={{
                  label: I18n.t('diary.dialog.number'),
                  InputLabelProps: {
                    shrink: true
                  },
                  required: true
                }}
              />
            </div>
          )}
        </div>
      </form>
    </div>
  );
};

DiaryForm.propTypes = {
  bankType: PropTypes.bool.isRequired,
  cashType: PropTypes.bool.isRequired,
  checkDiaryCode: PropTypes.func.isRequired,
  resetDiaryAccountNumber: PropTypes.func.isRequired,
  diaryDetail: PropTypes.shape({}).isRequired,
  codeDisabled: PropTypes.bool.isRequired,
  labelDisabled: PropTypes.bool.isRequired,
  diaryTypeDisabled: PropTypes.bool.isRequired
};

export default withStyles(styles)(DiaryForm);
