import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

import I18n from 'assets/I18n';

import { SUMMARY_SECTION } from 'helpers/dadp';

import { SectionAutoComplete } from 'containers/reduxForm/Inputs';

// Component
const DADPSection = ({ resetSubCategory }) => (
  <Field
    name="section"
    component={SectionAutoComplete}
    label={I18n.t('dadp.section')}
    onChange={(value) => {
      if (value === SUMMARY_SECTION) {
        resetSubCategory();
      }
    }}
  />
);

// Props
DADPSection.propTypes = {
  resetSubCategory: PropTypes.func.isRequired
};

export default DADPSection;
