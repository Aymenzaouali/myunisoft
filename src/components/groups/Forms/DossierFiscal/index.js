import React from 'react';
import I18n from 'assets/I18n';
import Accordion from 'components/groups/Accordion';
import Dossier from 'containers/groups/Forms/Dossier';
import TVA from 'components/groups/Forms/TVA';

const panels = [
  {
    key: 'nameAddress',
    label: I18n.t('companyCreation.dossierFiscal.dossier'),
    Component: Dossier
  },
  {
    key: 'legal',
    label: I18n.t('companyCreation.dossierFiscal.tva'),
    Component: TVA
  }
];

class DossierFiscal extends React.PureComponent {
  render() {
    return (
      <Accordion
        panels={panels}
      />
    );
  }
}

export default DossierFiscal;
