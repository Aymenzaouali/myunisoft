import React from 'react';
import PropTypes from 'prop-types';
import FilesDropper from 'components/groups/FilesDropper';
import { Typography, withStyles } from '@material-ui/core';
import * as I18n from 'i18next';

const AccountingFirmLetter = (props) => {
  const { classes, addDocuments, society } = props;
  const getDropzoneConfig = ({ type, format }) => ({
    disableClick: true,
    onDrop: files => addDocuments({
      files,
      type,
      format,
      society
    })
  });

  return (
    <div className={classes.droperWrap}>
      <div className={classes.row}>
        <Typography
          variant="h6"
          classes={{ root: classes.title }}
        >
          {I18n.t('accountingFirmSettings.cabinetLogo')}
        </Typography>
        <FilesDropper
          panes={{
            dropzoneConfig: getDropzoneConfig({ type: 'sales', format: 'document' }),
            icon: 'icon-upload',
            iconSize: 60,
            title: 'accountingFirmSettings.forms.dragHere'
          }}
        />
      </div>
      <div className={classes.row}>
        <Typography
          variant="h6"
          classes={{ root: classes.title }}
        >
          {I18n.t('accountingFirmSettings.footer')}
        </Typography>
        <FilesDropper
          panes={{
            dropzoneConfig: getDropzoneConfig({ type: 'sales', format: 'document' }),
            icon: 'icon-upload',
            iconSize: 60,
            title: 'accountingFirmSettings.forms.dragHere'
          }}
        />
      </div>

    </div>
  );
};

const ownStyles = {
  droperWrap: {
    maxWidth: 400
  },
  title: {
    fontSize: 14,
    fontWeight: 'normal',
    marginBottom: 20
  },
  row: {
    marginBottom: 20
  }
};

AccountingFirmLetter.defaultProps = {
  society: {}
};

AccountingFirmLetter.propTypes = {
  addDocuments: PropTypes.func.isRequired,
  society: PropTypes.shape({})
};

export default withStyles(ownStyles)(AccountingFirmLetter);
