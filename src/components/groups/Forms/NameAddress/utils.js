/**
 * Used in Select component
 * thus needs value and label keys present
 */
export const addressBuildingList = [
  {
    value: '',
    label: 'None'
  },
  {
    value: 'B',
    label: 'BIS'
  },
  {
    value: 'T',
    label: 'TER'
  },
  {
    value: 'Q',
    label: 'QUATER'
  },
  {
    value: 'C',
    label: 'QUINQUIES'
  }
];
