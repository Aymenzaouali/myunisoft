import React from 'react';
import I18n from 'assets/I18n';
import {
  Field,
  Form
} from 'redux-form';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { ReduxSelect } from 'components/reduxForm/Selections';
import {
  RoadTypesAutoComplete,
  CitiesAutoComplete,
  PostalCodeTextField
} from 'containers/reduxForm/Inputs';
import styles from 'components/groups/CompanyCreation/CompanyCreation.module.scss';
import ownStyles from './NameAddress.module.scss';

import { addressBuildingList } from './utils';

class NameAddress extends React.PureComponent {
  render() {
    return (
      <Form>
        <div className={styles.row}>
          <Field
            className={ownStyles.companyNameField}
            name="name"
            id="name"
            component={ReduxTextField}
            label={I18n.t('companyCreation.name')}
            margin="none"
          />
        </div>
        <div className={styles.row}>
          <Field
            className={ownStyles.addressNumberField}
            name="address_number"
            id="address_number"
            component={ReduxTextField}
            label={I18n.t('companyCreation.address_number')}
            margin="none"
            type="number"
          />
          <Field
            className={ownStyles.addressBuildingField}
            name="address_bis"
            id="address_bis"
            label={I18n.t('companyCreation.address_bis')}
            margin="none"
            component={ReduxSelect}
            list={addressBuildingList}
          />
          <Field
            name="road_type"
            id="road_type"
            component={RoadTypesAutoComplete}
            textFieldProps={{
              label: I18n.t('companyCreation.roadType'),
              InputLabelProps: {
                shrink: true
              }
            }}
          />
          <Field
            className={ownStyles.streetName}
            name="street_name"
            id="street_name"
            component={ReduxTextField}
            label={I18n.t('companyCreation.streetName')}
            margin="none"
          />
        </div>
        <div className={styles.row}>
          <Field
            name="complement"
            id="complement"
            component={ReduxTextField}
            label={I18n.t('companyCreation.complement')}
            margin="none"
          />
        </div>
        <div className={styles.row}>
          <Field
            name="postal_code"
            id="postal_code"
            component={PostalCodeTextField}
            label={I18n.t('companyCreation.postalCode')}
            margin="none"
            inputProps={{ maxlength: 5 }}
            type="number"
          />
          <Field
            name="city"
            id="city"
            component={CitiesAutoComplete}
            textFieldProps={{
              label: I18n.t('companyCreation.city'),
              InputLabelProps: {
                shrink: true
              }
            }}
          />
        </div>
        <div className={styles.row}>
          <Field
            name="country"
            id="country"
            component={ReduxTextField}
            label={I18n.t('companyCreation.country')}
            margin="none"
            format={value => value && value.toUpperCase()}
          />
        </div>
        <div className={styles.row}>
          {/* Not for V0 */}
          {/* <Field
            name="declarationAddressIsDifferent"
            id="declarationAddressIsDifferent"
            component={ReduxCheckBox}
            label={I18n.t('companyCreation.declarationAddressIsDifferent')}
            margin="none"
          /> */}
        </div>
      </Form>
    );
  }
}

export default NameAddress;
