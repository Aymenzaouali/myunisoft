import React from 'react';
import PropTypes from 'prop-types';
import { FontIcon } from 'components/basics/Icon';
import { withStyles } from '@material-ui/core';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { Field } from 'redux-form';
import styles from './RIB.module.scss';

const materialStyles = () => ({
  IBAN: {
    width: 256,
    marginRight: 20
  },
  BIC: {
    width: 89,
    marginRight: 20
  },
  Banc: {
    width: 87,
    marginRight: 20
  },
  counter: {
    width: 94,
    marginRight: 20
  },
  NAccountBanc: {
    width: 156,
    marginRight: 20
  },
  paper: {
    width: 256
  },
  deleteButton: {
    display: 'flex',
    alignSelf: 'flex-end',
    marginBottom: 10,
    '&:hover': {
      backgroundColor: '#fff'
    }
  },
  addButton: {
    display: 'flex',
    justifyContent: 'flex-start',
    decoration: 'none',
    textDecoration: 'underline',
    '&:hover': {
      backgroundColor: '#fff'
    }
  }
});
const RIB = (props) => {
  const {
    deleteRib,
    classes
  } = props;

  return (
    <div className={styles.accountDialog}>
      <div className={styles.blockInline}>
        <Field color="primary" className={classes.Banc} name="infoC.banc" component={ReduxTextField} label={I18n.t('account.newAccount.banc')} />
        <Field color="primary" className={classes.counter} name="infoC.counter" component={ReduxTextField} label={I18n.t('account.newAccount.counter')} />
        <Field color="primary" className={classes.NAccountBanc} name="infoC.NAccountBanc" component={ReduxTextField} label={I18n.t('account.newAccount.NAccountBanc')} />
        <Field color="primary" className={classes.IBAN} name="infoC.IBAN" component={ReduxTextField} label={I18n.t('account.newAccount.ribSteps.IBAN')} />
        <Button className={classes.deleteButton} aria-label="Delete">
          <FontIcon color="#fe3a5e" size="22px" name="icon-trash" onClick={deleteRib} titleInfoBulle={I18n.t('tooltips.delete')} />
        </Button>
      </div>
      <div className={styles.blockInline}>
        <Field color="primary" className={classes.BIC} name="infoC.BIC" component={ReduxTextField} label={I18n.t('account.newAccount.BIC')} />
      </div>
      <Button className={classes.addButton} variant="text">{I18n.t('account.newAccount.addRib')}</Button>
    </div>
  );
};

RIB.propTypes = {
  classes: PropTypes.shape({}),
  deleteRib: PropTypes.func.isRequired
};

RIB.defaultProps = ({
  classes: {}
});

export default withStyles(materialStyles)(RIB);
