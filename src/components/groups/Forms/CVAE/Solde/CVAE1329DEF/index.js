/* eslint-disable */
import React from 'react';
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from "components/reduxForm/Inputs";
import { sum, useCompute, parse as p, sumOfValues, setValue } from "helpers/pdfforms";
import { get as _get } from 'lodash';

import './style.scss';

const computeB = ({AA}) => {
  const A1 = p(AA);
  if (A1 <  5e5) return 0;
  else if (A1 >=  5e5 && A1 <  3e6) return ((0.5 * (A1 - 5e5)) / 25e5);
  else if (A1 >=  3e6 && A1 <  10e6) return ((0.9 * (A1 - 3e6)) / 7e6 + 0.5);
  else if (A1 >=  10e6 && A1 <  50e6) return ((0.1 * (A1 - 10e6)) / 40e6 + 1.4);
  else if (A1 >  50e6) return 1.4;
};

const computeD = ({BC, AA, AB}) => {
  const D0 = BC, A1 = p(AA), A2 = (AB);
  if (D0) return 0;
  else if (A2) return p(A2);
  else if (A1 <= 76e5) return p(A1) * 80 / 100;
  else if (A1 > 76e5) return p(A1) * 85 / 100;
};

const computeE = ({AC, BA, BB, BC}) => {
  const C = p(AC), B = p(BA), D = p(BB), D0 = BC;
  if (B <= D || D0) return B * C;
  else if (C > D) return D * C;
};

const computeF = ({AA, CA}) => {
  const A1 = p(AA), E = p(CA);
  if (A1 < 2e6 && E <= 1e3) return 0;
  else if (A1 < 2e6 && E > 1e3) return E - 1e3;
  else if (A1 >= 2e6) return E;
};

const computeI1 = ({CB, DA, DB}) => {
  const F = p(CB), G = p(DA), H = p(DB);
  if ((F - G - H) < 250) return F - G - H;
  else if ((F - G - H) > 250) return 250;
};

const computeI3 = ({EA, EB}) => {
  const I1 = p(EA), I2 = p(EB);
  if ((I1 - I2) > 0) return I1 - I2;
  else if ((I1 - I2) < 0) return '';
};

const computeI4 = ({EA, EB}) => {
  const I1 = p(EA), I2 = p(EB);
  if ((I2 - I1) > 0) return I2 - I1;
  else if ((I2 - I1) < 0) return '';
};

const computeJ1 = ({GA, EA}) => {
  const J0 = GA, I1 = p(EA);
  if (J0) return '';
  else return I1 * 1.83 / 100;
};

const computeJ3 = ({GB, GC}) => {
  const J1 = p(GB), J2 = p(GC);
  if ((J1 - J2) > 0) return J1 - J2;
  else if ((J2 - J1) < 0) return '';
};

const computeJ4 = ({GB, GC}) => {
  const J1 = p(GB), J2 = p(GC);
  if ((J2 - J1) > 0) return J2 - J1;
  else if ((J2 - J1) < 0) return '';
};

const computeL1 = ({EA, GB}) => {
  const I1 = p(EA), J1 = p(GB);
  return (I1 + J1) / 100;
};

const computeL3 = ({HA, HB}) => {
  const L1 = p(HA), L2 = p(HB);
  if ((L1 - L2) > 0) return L1 - L2;
  else if ((L2 - L1) < 0) return '';
};

const computeL4 = ({HA, HB}) => {
  const L1 = p(HA), L2 = p(HB);
  if ((L2 - L1) > 0) return L2 - L1;
  else if ((L2 - L1) < 0) return '';
};

const CVAE1329DEF = (props) => {
  const { change, cvaeForm, tax2059EForm, tax2035EForm, tax2033EForm, currentYearExercise } = props;
  const GA2059E = _get(tax2059EForm, 'GA', null);
  const ES2035E = _get(tax2035EForm, 'ES', null);
  const GA2033E = _get(tax2033EForm, 'GA', null);
  
  setValue(cvaeForm, 'BA', sumOfValues([GA2059E, ES2035E, GA2033E]), change);
  useCompute('KA', ({JK, JE}) => p(JK) - p(JE), ['JK', 'JE'], cvaeForm, change); // Y = line 26 - line 25
  useCompute('KB', ({JE, JK}) => p(JE) - p(JK), ['JK', 'JE'], cvaeForm, change); // Z = line 25 - line 26
  useCompute('AC', computeB, ['AA'], cvaeForm, change); // B = calculation of A1
  useCompute('BB', computeD, ['BC', 'AA', 'AB'], cvaeForm, change); // D = calculation of D0, A1, A2
  useCompute('CA', computeE, ['AC', 'BA', 'BB', 'BC'], cvaeForm, change); // E = calculation of C, B, D, D0
  useCompute('CB', computeF, ['AA', 'CA'], cvaeForm, change); // F = calculation of A1, E
  useCompute('EA', computeI1, ['CB', 'DA', 'DB'], cvaeForm, change); // I1 = calculation of F, G, H
  useCompute('EC', computeI3, ['EA', 'EB'], cvaeForm, change); // I3 = calculation of I1, I2
  useCompute('ED', computeI4, ['EA', 'EB'], cvaeForm, change); // I4 = calculation of I1, I2
  useCompute('GB', computeJ1, ['GA', 'EA'], cvaeForm, change); // J1 = calculation of J0, I1
  useCompute('GD', computeJ3, ['GB', 'GC'], cvaeForm, change); // J3 = calculation of J1, J2
  useCompute('GE', computeJ4, ['GB', 'GC'], cvaeForm, change); // J4 = calculation of J1, J2
  useCompute('HA', computeL1, ['EA', 'GB'], cvaeForm, change); // L1 = calculation of I1, J1
  useCompute('HC', computeL3, ['HA', 'HB'], cvaeForm, change); // L3 = calculation of L1, L2
  useCompute('HD', computeL4, ['HA', 'HB'], cvaeForm, change); // J4 = calculation of L1, L2
  useCompute('JA', sum, ['EB', 'GC', 'HB'], cvaeForm, change); // M = sum of I2, J2, L2
  useCompute('JK', sum, ['EC', 'GD', 'HC'], cvaeForm, change); // N = sum of I3, J3, L3
  useCompute('JE', sum, ['ED', 'GE', 'HD'], cvaeForm, change); // O = sum of I4, J4, L4

  return (
    <div className="CVAE1329DEF">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 42, top: 964, width: 296, height: 19, 'font-size': 21, }}>
<span>
PAIEMENT, DATE, SIGNATURE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 490, top: 964, width: 307, height: 16, 'font-size': 21, }}>
<span>
RESERVE A L&#39;ADMINISTRATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 999, width: 196, height: 13, 'font-size': 17, }}>
<span>
Date : ………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 537, width: 92, height: 13, 'font-size': 17, }}>
<span>
N° FRP - Clé
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 413, top: 187, width: 378, height: 20, 'font-size': 25, }}>
<span>
DECLARATION DE LIQUIDATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 459, top: 212, width: 293, height: 20, 'font-size': 25, }}>
<span>
ET DE REGULARISATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 591, top: 405, width: 204, height: 10, 'font-size': 13, }}>
<span>
Nom et adresse de l&#39;établissement :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 404, width: 25, height: 12, 'font-size': 17, }}>
<span>
RIB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 590, top: 263, width: 321, height: 12, 'font-size': 13, }}>
<span>
Service compétent où doit être adressée la déclaration :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 356, width: 25, height: 14, 'font-size': 17, }}>
<span>
SIE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 380, width: 93, height: 13, 'font-size': 17, }}>
<span>
Code service
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 30, width: 352, height: 19, 'font-size': 25, }}>
<span>
COTISATION SUR LA VALEUR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 55, top: 58, width: 348, height: 19, 'font-size': 25, }}>
<span>
AJOUTEE DES ENTREPRISES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 170, top: 102, width: 91, height: 32, 'font-size': 43, }}>
<span>
201 8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 345, top: 151, width: 507, height: 18, 'font-size': 21, }}>
<span>
DIRECTION GENERALE DES FINANCES PUBLIQUES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 477, top: 998, width: 63, height: 13, 'font-size': 17, }}>
<span>
Somme :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 982, top: 998, width: 128, height: 13, 'font-size': 17, }}>
<span>
Cachet du service
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 37, top: 1078, width: 362, height: 15, 'font-size': 17, }}>
<span>
Adresse électronique : ……………………………….
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 998, width: 109, height: 13, 'font-size': 17, }}>
<span>
Date : …………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1008, width: 88, height: 2, 'font-size': 17, }}>
<span>
...... …………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 1048, width: 36, height: 2, 'font-size': 17, }}>
<span>
..... …
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 144, top: 1048, width: 60, height: 2, 'font-size': 17, }}>
<span>
... ………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1038, width: 104, height: 16, 'font-size': 17, }}>
<span>
Téléphone : …
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 584, width: 89, height: 16, 'font-size': 17, }}>
<span>
de paiement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 562, width: 75, height: 13, 'font-size': 17, }}>
<span>
Date limite
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 1452, width: 1083, height: 14, 'font-size': 15, }}>
<span>
Les dispositions des articles 39 et 40 de la loi n° 78-17 du 6 janvier 1978 relative à l&#39;informatique, aux fichiers et aux libertés, modifiées par la loi n° 2004-801 du 6 août 2004,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 241, top: 1470, width: 689, height: 14, 'font-size': 15, }}>
<span>
garantissent les droits des personnes physiques à l&#39;égard des traitements des données à caractère personnel.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1172, width: 428, height: 16, 'font-size': 21, }}>
<span>
CADRE RESERVE A LA CORRESPONDANCE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 655, width: 254, height: 16, 'font-size': 21, }}>
<span>
PAIEMENT OU EXCEDENT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 245, top: 708, width: 104, height: 16, 'font-size': 21, }}>
<span>
CVAE DUE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 281, top: 748, width: 29, height: 16, 'font-size': 21, }}>
<span>
OU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 787, width: 274, height: 16, 'font-size': 21, }}>
<span>
EXCEDENT DE VERSEMENT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 32, top: 431, width: 110, height: 11, 'font-size': 13, }}>
<span>
N° d&#39;identification de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 32, top: 449, width: 175, height: 13, 'font-size': 13, }}>
<span>
l&#39;établissement principal (SIRET)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 32, top: 477, width: 52, height: 12, 'font-size': 15, }}>
<span>
Adresse
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 29, top: 497, width: 169, height: 14, 'font-size': 15, }}>
<span>
de l&#39;établissement principal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 1008, width: 23, height: 2, 'font-size': 17, }}>
<span>
.. …
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 299, top: 998, width: 77, height: 16, 'font-size': 17, }}>
<span>
Signature :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 754, top: 1037, width: 109, height: 17, 'font-size': 17, }}>
<span>
N° d&#39;opération :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 873, width: 385, height: 13, 'font-size': 17, }}>
<span>
Remboursement d&#39;excédent de versement demandé :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1008, top: 101, width: 48, height: 27, 'font-size': 29, }}>
<span>
cerfa
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 28, width: 130, height: 17, 'font-size': 23, }}>
<span>
N° 1329-DEF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 68, width: 108, height: 14, 'font-size': 15, }}>
<span>
@internet-DGFiP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 9, top: 1319, width: 13, height: 170, 'font-size': 13, }}>
<span>
SDNC - Conception Web 201 9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 898, top: 712, width: 168, height: 18, 'font-size': 19, }}>
<span>
(Ligne 2 - ligne 26) 6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 897, top: 790, width: 169, height: 18, 'font-size': 19, }}>
<span>
(Ligne 26 - ligne 25)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 711, width: 22, height: 15, 'font-size': 21, }}>
<span>
27
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 899, width: 771, height: 16, 'font-size': 17, }}>
<span>
(Joindre un RIB s&#39;il s&#39;agit d&#39;une première demande de remboursement ou en cas de changement de compte)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 789, width: 22, height: 16, 'font-size': 21, }}>
<span>
28
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 967, top: 713, width: 10, height: 14, 'font-size': 19, }}>
<span>
55
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 557, top: 708, width: 17, height: 19, 'font-size': 25, }}>
<span>
Y
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 558, top: 789, width: 15, height: 19, 'font-size': 25, }}>
<span>
Z
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 989, top: 131, width: 90, height: 13, 'font-size': 17, }}>
<span>
N° 14357*09
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 9, top: 1319, width: 10, height: 7, 'font-size': 13, }}>
<span>
9
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <textarea disabled className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="SERVICECOMPETENT_STE"
                      data-field-id="19693864" data-annot-id="19301152" readOnly>
            </textarea>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="SIE_STE"
                   data-field-id="19692184" data-annot-id="19301344" readonly maxLength="9" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field "
                   name="CODESERVICE_STE" data-field-id="19692520" data-annot-id="19301536" readonly maxLength="8"
                   type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="RIB_STE"
                   data-field-id="19692856" data-annot-id="19301728" readonly maxLength="25" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field "
                   name="SIRET_STET" data-field-id="19691848" data-annot-id="19301920" readonly maxLength="14"
                   type="text"/>

            <textarea disabled className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="a3.4"
                      data-field-id="19589928" data-annot-id="19302112">
            </textarea>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field "
                   name="NUMFRP_STE" data-field-id="19693192" data-annot-id="19302304" readonly maxLength="11"
                   type="text"/>

            <Field component={ReduxPicker} disabled className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field "
                   name="LIMITE_PAIEMENT_STE" data-field-id="19693528" data-annot-id="19302496" readonly maxLength="17"
                   type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="KA"
                   data-field-id="19688344" data-annot-id="19302688" readonly value="0" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="KB"
                   data-field-id="19638760" data-annot-id="19302880" readonly value="0" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="ZZ"
                   data-field-id="19590776" data-annot-id="19303072" value="1" type="checkbox"
                   data-default-value="Off"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field "
                   name="DATE_PAIEMENT" data-field-id="19694200" data-annot-id="19303264" readonly type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field "
                   name="TEL_PAIEMENT" data-field-id="19694536" data-annot-id="19303456" readonly type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field "
                   name="ADRESS_MAIL_PAIEMENT" data-field-id="19694872" data-annot-id="19303648" readonly type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field "
                   name="NOM_STEA" data-field-id="19695208" data-annot-id="19303840" maxLength="8" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "
                   name="ADR_STED" data-field-id="19695544" data-annot-id="19304032" readonly type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="ADR_STEF" data-field-id="19695880" data-annot-id="19304224" readonly type="text"/>

            <textarea className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="ADR_STEG"
                      data-field-id="19696216" data-annot-id="19304688">
            </textarea>

            <Field component="input" className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="VILLE_STEI" data-field-id="19696552" data-annot-id="19304880" readonly type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field "
                   name="CP_STEH" data-field-id="19696888" data-annot-id="19305072" readonly type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field "
                   name="SIGNATURE_STE" data-field-id="19697224" data-annot-id="19305264" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field "
                   name="CORRESPONDANCE_STE" data-field-id="19697560" data-annot-id="19305456" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 231, top: 515, width: 106, height: 16, 'font-size': 21, }}>
<span>
&#60; 500 000 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 472, width: 271, height: 16, 'font-size': 21, }}>
<span>
Si le montant de votre CA est :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 577, top: 472, width: 497, height: 19, 'font-size': 21, }}>
<span>
Alors, le taux à porter cadre B sera calculé comme suit :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1277, width: 978, height: 18, 'font-size': 19, }}>
<span>
Si CA 7 600 000 €, le montant à porter dans le cadre D correspond à 80% du chiffre d&#39;affaires porté au cadre A1 ou A2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1322, width: 978, height: 18, 'font-size': 19, }}>
<span>
Si CA &#62; 7 600 000 €, le montant à porter dans le cadre D correspond à 85% du chiffre d&#39;&#39;affaires porté au cadre A1 ou A2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 182, top: 746, width: 226, height: 20, 'font-size': 21, }}>
<span>
Supérieur à 50 000 000 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 630, width: 295, height: 16, 'font-size': 21, }}>
<span>
3 000 000 € &#60; CA 10 000 000 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 298, top: 812, width: 15, height: 13, 'font-size': 17, }}>
<span>
et
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 706, top: 618, width: 247, height: 20, 'font-size': 21, }}>
<span>
[0,9 x (CA-3 000 000)] + 0,5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 785, top: 643, width: 89, height: 16, 'font-size': 21, }}>
<span>
7 000 000
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 812, top: 747, width: 37, height: 18, 'font-size': 21, }}>
<span>
1,50
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 487, top: 940, width: 264, height: 14, 'font-size': 19, }}>
<span>
VALEUR AJOUTEE PRODUITE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 592, top: 807, width: 12, height: 16, 'font-size': 21, }}>
<span>
B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 895, width: 320, height: 16, 'font-size': 21, }}>
<span>
DONNEES DE VALEUR AJOUTEE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 797, width: 20, height: 20, 'font-size': 25, }}>
<span>
%
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 809, width: 22, height: 16, 'font-size': 21, }}>
<span>
04
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 64, width: 349, height: 16, 'font-size': 21, }}>
<span>
DONNEES DE CHIFFRE D&#39;AFFAIRES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 109, width: 318, height: 15, 'font-size': 19, }}>
<span>
MONTANT DU CHIFFRE D&#39;AFFAIRES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 372, width: 665, height: 14, 'font-size': 19, }}>
<span>
CALCUL DU POURCENTAGE DE LA VALEUR AJOUTEE CORRESPONDANTE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 76, top: 409, width: 1029, height: 20, 'font-size': 21, }}>
<span>
Le pourcentage à calculer et à porter dans la case B varie selon le montant du chiffre d&#39;affaires, mentionné au cadre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 435, width: 340, height: 16, 'font-size': 21, }}>
<span>
A1 conformém en t au barè me suiv ant :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 156, width: 19, height: 16, 'font-size': 21, }}>
<span>
01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 583, top: 156, width: 22, height: 15, 'font-size': 21, }}>
<span>
A1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 156, width: 373, height: 19, 'font-size': 21, }}>
<span>
Montant du CA de la période de référence
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 118, top: 200, width: 404, height: 19, 'font-size': 21, }}>
<span>
Montant du CA réel si la période de référence
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 118, top: 225, width: 219, height: 16, 'font-size': 21, }}>
<span>
est différente de 12 mois
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 584, top: 206, width: 24, height: 15, 'font-size': 21, }}>
<span>
A2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 206, width: 21, height: 16, 'font-size': 21, }}>
<span>
02
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 824, top: 514, width: 10, height: 16, 'font-size': 21, }}>
<span>
0
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 690, width: 306, height: 16, 'font-size': 21, }}>
<span>
10 000 000 € &#60; CA 50 000 000 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 740, top: 558, width: 179, height: 20, 'font-size': 21, }}>
<span>
[0,5 x (CA-500 000)]
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 793, top: 583, width: 89, height: 16, 'font-size': 21, }}>
<span>
2 500 000
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 571, width: 267, height: 16, 'font-size': 21, }}>
<span>
500 000 € CA 3 000 000 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 701, top: 677, width: 258, height: 20, 'font-size': 21, }}>
<span>
[0,1 x (CA-10 000 000)] + 1,4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 779, top: 702, width: 100, height: 16, 'font-size': 21, }}>
<span>
40 000 000
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 997, width: 22, height: 16, 'font-size': 21, }}>
<span>
05
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 1222, width: 22, height: 16, 'font-size': 21, }}>
<span>
06
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 548, top: 1173, width: 13, height: 16, 'font-size': 21, }}>
<span>
D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 76, top: 1048, width: 1044, height: 20, 'font-size': 21, }}>
<span>
Le montant à porter dans ce cadre figure sur les imprimés 2059-E (ligne SA), 2033-E (ligne 117), 2035-E (ligne JU) et
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1072, width: 173, height: 20, 'font-size': 21, }}>
<span>
2072-E (ligne D12).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 451, top: 1115, width: 332, height: 15, 'font-size': 19, }}>
<span>
LIMITATION DE LA VALEUR AJOUTEE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 588, top: 997, width: 14, height: 16, 'font-size': 21, }}>
<span>
C
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 812, width: 487, height: 16, 'font-size': 17, }}>
<span>
Taux exprimé en pourcentage arrondi au centième le plus proche
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 584, top: 256, width: 24, height: 16, 'font-size': 21, }}>
<span>
A3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 118, top: 260, width: 347, height: 20, 'font-size': 21, }}>
<span>
Montant du CA de référence du groupe
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 1220, width: 13, height: 16, 'font-size': 21, }}>
<span>
D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 70, top: 1159, width: 874, height: 18, 'font-size': 19, }}>
<span>
Pour certaines entreprises notamment à caractère financier (Cf. notice), cochez la case ci-contre et ne
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 1179, width: 223, height: 18, 'font-size': 19, }}>
<span>
remplissez pas la case D.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1036, top: 1164, width: 25, height: 16, 'font-size': 21, }}>
<span>
D0
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 451, top: 1398, width: 272, height: 15, 'font-size': 19, }}>
<span>
MONTANT DE LA CVAE BRUTE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1455, width: 940, height: 20, 'font-size': 21, }}>
<span>
Le montant de la cotisation sur la valeur ajoutée à porter dans la case E est obtenue par le calcul suivant :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1555, width: 1009, height: 20, 'font-size': 21, }}>
<span>
Si vous avez coché la case D0 ci-dessus, le montant de la cotisation sur la valeur ajoutée est égal à C multiplié par B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 1597, width: 22, height: 16, 'font-size': 21, }}>
<span>
07
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 1599, width: 12, height: 16, 'font-size': 21, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1496, width: 814, height: 20, 'font-size': 21, }}>
<span>
 Si C &#60; D, alors le montant de la cotisation sur la valeur ajoutée est égal à C multiplié par B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 90, top: 1526, width: 814, height: 20, 'font-size': 21, }}>
<span>
 Si C &#62; D, alors le montant de la cotisation sur la valeur ajoutée est égal à D multiplié par B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 1281, width: 7, height: 10, 'font-size': 21, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 1326, width: 7, height: 9, 'font-size': 21, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 260, width: 22, height: 16, 'font-size': 21, }}>
<span>
03
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 118, top: 291, width: 671, height: 20, 'font-size': 21, }}>
<span>
Le montant porté à cette ligne doit etre supérieur ou égal à 7 630 000 euros
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 289, width: 8, height: 7, 'font-size': 15, }}>
<span>
^
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="AA"
                   data-field-id="19600648" data-annot-id="15577392" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="b11"
                   data-field-id="19591080" data-annot-id="15577584" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="b111"
                   data-field-id="19597432" data-annot-id="15577776" maxLength="13" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="AC"
                   data-field-id="19611608" data-annot-id="15577968" readonly value="0.00" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="%"
                   data-field-id="19594472" data-annot-id="15578160" readonly value="%" type="text"
                   data-default-value="%"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="BA"
                   data-field-id="19614872" data-annot-id="15578352" readonly maxLength="13" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="BC"
                   data-field-id="19618504" data-annot-id="15578544" value="On" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="BB"
                   data-field-id="19622024" data-annot-id="15578736" readonly value="0" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="CA"
                   data-field-id="19625144" data-annot-id="15578928" readonly value="0" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="AB"
                   data-field-id="19605032" data-annot-id="15579120" maxLength="13" type="text"
                   disabled={_get(currentYearExercise, 'duration', null) === 12 ? true : false}
            />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="AD"
                   data-field-id="19608360" data-annot-id="15579312" maxLength="13" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-2" data-page-num="2" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-2 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 112, top: 595, width: 255, height: 18, 'font-size': 19, }}>
<span>
Solde de CVAE à payer (I1 - I2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 111, top: 776, width: 296, height: 19, 'font-size': 19, }}>
<span>
Taxe additionnelle due (I1 x 1, 83 %)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 1461, width: 21, height: 16, 'font-size': 21, }}>
<span>
18
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 111, top: 820, width: 311, height: 18, 'font-size': 19, }}>
<span>
Acomptes de taxe additionnelle versés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 639, width: 291, height: 18, 'font-size': 19, }}>
<span>
Excédent de CVAE constaté (I2 - I1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 652, top: 296, width: 11, height: 15, 'font-size': 21, }}>
<span>
F
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 421, width: 209, height: 18, 'font-size': 19, }}>
<span>
Réduction supplémentaire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 296, width: 22, height: 16, 'font-size': 21, }}>
<span>
08
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 377, width: 22, height: 16, 'font-size': 21, }}>
<span>
09
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 377, width: 104, height: 15, 'font-size': 19, }}>
<span>
Exonérations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 512, width: 451, height: 18, 'font-size': 19, }}>
<span>
CVAE due (F - G - H) ou cotisation minimum (Cf. notice)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 54, width: 546, height: 16, 'font-size': 21, }}>
<span>
CALCUL DE LA COTISATION SUR LA VALEUR AJOUTEE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 370, top: 98, width: 430, height: 15, 'font-size': 19, }}>
<span>
CALCUL DE LA COTISATION AVANT REDUCTION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 551, width: 219, height: 18, 'font-size': 19, }}>
<span>
Acomptes de CVAE versés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 863, width: 354, height: 18, 'font-size': 19, }}>
<span>
Solde de taxe additionnelle à payer (J1 - J2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 332, width: 115, height: 14, 'font-size': 19, }}>
<span>
MINORATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 951, top: 252, width: 11, height: 16, 'font-size': 21, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 134, top: 149, width: 266, height: 20, 'font-size': 21, }}>
<span>
MONTANT DU CA (cadre A1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 148, width: 88, height: 16, 'font-size': 21, }}>
<span>
CADRE E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 189, top: 252, width: 156, height: 16, 'font-size': 21, }}>
<span>
CA &#62; 2 000 000 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 539, top: 252, width: 160, height: 16, 'font-size': 21, }}>
<span>
TOUT MONTANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 201, width: 156, height: 16, 'font-size': 21, }}>
<span>
CA &#60; 2 000 000 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 789, top: 148, width: 328, height: 16, 'font-size': 21, }}>
<span>
MONTANT A REPORTER CADRE F
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 940, top: 186, width: 27, height: 16, 'font-size': 21, }}>
<span>
0 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 901, top: 219, width: 105, height: 20, 'font-size': 21, }}>
<span>
(E - 1000 €)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 186, width: 97, height: 16, 'font-size': 21, }}>
<span>
E &#60; 1000 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 219, width: 97, height: 16, 'font-size': 21, }}>
<span>
E &#62;1000 €
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 296, width: 209, height: 15, 'font-size': 19, }}>
<span>
Cotisation avant réduction
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 906, width: 391, height: 19, 'font-size': 19, }}>
<span>
Excédent de taxe additionnelle constaté (J2 - J1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 420, width: 20, height: 16, 'font-size': 21, }}>
<span>
10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 637, width: 20, height: 16, 'font-size': 21, }}>
<span>
14
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 594, width: 20, height: 16, 'font-size': 21, }}>
<span>
13
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 551, width: 20, height: 16, 'font-size': 21, }}>
<span>
12
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 511, width: 17, height: 15, 'font-size': 21, }}>
<span>
11
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 651, top: 376, width: 12, height: 16, 'font-size': 21, }}>
<span>
G
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 650, top: 418, width: 12, height: 16, 'font-size': 21, }}>
<span>
H
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 649, top: 509, width: 13, height: 16, 'font-size': 21, }}>
<span>
I1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 649, top: 549, width: 15, height: 16, 'font-size': 21, }}>
<span>
I2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 649, top: 594, width: 15, height: 16, 'font-size': 21, }}>
<span>
I3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 649, top: 636, width: 15, height: 15, 'font-size': 21, }}>
<span>
I4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 462, width: 50, height: 14, 'font-size': 19, }}>
<span>
CVAE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 481, top: 684, width: 200, height: 15, 'font-size': 19, }}>
<span>
TAXE ADDITIONNELLE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 645, top: 775, width: 18, height: 16, 'font-size': 21, }}>
<span>
J1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 775, width: 21, height: 16, 'font-size': 21, }}>
<span>
15
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 646, top: 818, width: 20, height: 16, 'font-size': 21, }}>
<span>
J2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 645, top: 860, width: 21, height: 16, 'font-size': 21, }}>
<span>
J3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 644, top: 904, width: 21, height: 16, 'font-size': 21, }}>
<span>
J4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 819, width: 21, height: 15, 'font-size': 21, }}>
<span>
16
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 862, width: 20, height: 15, 'font-size': 21, }}>
<span>
17
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 905, width: 21, height: 16, 'font-size': 21, }}>
<span>
18
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 724, width: 21, height: 16, 'font-size': 21, }}>
<span>
J0
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1004, width: 23, height: 16, 'font-size': 21, }}>
<span>
K0
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 651, top: 1046, width: 13, height: 16, 'font-size': 21, }}>
<span>
K
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 1045, width: 20, height: 16, 'font-size': 21, }}>
<span>
19
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 494, top: 1105, width: 170, height: 14, 'font-size': 19, }}>
<span>
FRAIS DE GESTION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 507, top: 1385, width: 143, height: 15, 'font-size': 19, }}>
<span>
RECAPITULATIF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 648, top: 1161, width: 18, height: 16, 'font-size': 21, }}>
<span>
L1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 647, top: 1205, width: 21, height: 16, 'font-size': 21, }}>
<span>
L2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 647, top: 1250, width: 21, height: 16, 'font-size': 21, }}>
<span>
L3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 647, top: 1292, width: 21, height: 16, 'font-size': 21, }}>
<span>
L4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 649, top: 1440, width: 14, height: 15, 'font-size': 21, }}>
<span>
M
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 649, top: 1483, width: 13, height: 15, 'font-size': 21, }}>
<span>
N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 649, top: 1527, width: 15, height: 16, 'font-size': 21, }}>
<span>
O
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 1163, width: 296, height: 18, 'font-size': 19, }}>
<span>
Frais de gestion dus (1 % de I1 + J1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 111, top: 1207, width: 293, height: 18, 'font-size': 19, }}>
<span>
Acomptes de frais de gestion versés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 1252, width: 348, height: 18, 'font-size': 19, }}>
<span>
Solde des frais de gestion à payer (L1 - L2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 1294, width: 376, height: 18, 'font-size': 19, }}>
<span>
Excédent de frais de gestion constaté (L2 - L1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 1441, width: 330, height: 18, 'font-size': 19, }}>
<span>
Total des acomptes versés (I2 + J2 + L2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 1483, width: 258, height: 20, 'font-size': 19, }}>
<span>
TOTAL A PAYER (I3 + J3 + L3)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 1528, width: 329, height: 18, 'font-size': 19, }}>
<span>
TOTAL DES EXCEDENTS (I4 + J4 + L4)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1162, width: 22, height: 16, 'font-size': 21, }}>
<span>
20
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 1206, width: 19, height: 16, 'font-size': 21, }}>
<span>
21
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1251, width: 22, height: 16, 'font-size': 21, }}>
<span>
22
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1293, width: 22, height: 16, 'font-size': 21, }}>
<span>
23
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1440, width: 22, height: 15, 'font-size': 21, }}>
<span>
24
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1483, width: 22, height: 16, 'font-size': 21, }}>
<span>
25
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1527, width: 21, height: 16, 'font-size': 21, }}>
<span>
26
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 498, top: 958, width: 156, height: 15, 'font-size': 19, }}>
<span>
CADRE RESERVE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 725, width: 826, height: 18, 'font-size': 19, }}>
<span>
Si vous êtes exonéré du paiement de la taxe additionnelle (Cf. notice), cochez la case ci-contre :
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="CB"
                   data-field-id="19628232" data-annot-id="18264432" readonly value="0" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="DA"
                   data-field-id="19631288" data-annot-id="18264624" maxLength="10" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="DB"
                   data-field-id="19634872" data-annot-id="18265056" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="EA"
                   data-field-id="19638456" data-annot-id="18265248" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="EB"
                   data-field-id="19641640" data-annot-id="18387264" readonly maxLength="13" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="EC"
                   data-field-id="19645336" data-annot-id="18387456" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="ED"
                   data-field-id="19648456" data-annot-id="18387648" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GA"
                   data-field-id="19651656" data-annot-id="18265744" value="On" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="GB"
                   data-field-id="19655080" data-annot-id="18265936" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="GC"
                   data-field-id="19658280" data-annot-id="18266272" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GD"
                   data-field-id="19661864" data-annot-id="18266464" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GE"
                   data-field-id="19664984" data-annot-id="18266656" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="HA"
                   data-field-id="19668184" data-annot-id="18266848" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="HB"
                   data-field-id="19671304" data-annot-id="18267040" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="HC"
                   data-field-id="19674952" data-annot-id="18267232" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="HD"
                   data-field-id="19678072" data-annot-id="18267424" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="JA"
                   data-field-id="19681720" data-annot-id="18267616" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="JK"
                   data-field-id="19684840" data-annot-id="18268080" readonly value="0" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="JE"
                   data-field-id="19688040" data-annot-id="18268272" readonly value="0" maxLength="13" type="text"/>

          </div>
        </div>
      </div>
    </div>
  );
};

export default CVAE1329DEF;
