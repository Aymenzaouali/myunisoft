/* eslint-disable */
import React from 'react';
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import { useCompute, parse as p, setValue } from 'helpers/pdfforms';
import { get as _get } from 'lodash';

import './style.scss';

const computeB = ({ AA }) => {
  const A1 = p(AA);
  if (A1 < 5e5) return 0;
  else if (A1 >= 5e5 && A1 < 3e6) return ((0.5 * (A1 - 5e5)) / 25e5);
  else if (A1 >= 3e6 && A1 < 10e6) return ((0.9 * (A1 - 3e6)) / 7e6 + 0.5);
  else if (A1 >= 10e6 && A1 < 50e6) return ((0.1 * (A1 - 10e6)) / 40e6 + 1.4);
  else if (A1 > 50e6) return 1.5;
};

const computeD = ({ BC, AA, AB }) => {
  const D0 = BC, A1 = p(AA), A2 = p(AB);
  if (D0) return 0;
  else if (A1 <= 76e5) {
    if (A2) return A2 * 80 / 100;
    else return A1 * 80 / 100;
  } else if (A1 > 76e5) {
    if (A2) return A2 * 85 / 100;
    else return A1 * 85 / 100;
  }
};

const computeE = ({ AC, BA, BB, BC }) => {
  const C = p(BA), B = p(AC), D = p(BB), D0 = BC;
  if (C <= D || D0) return B * C;
  else if (C > D) return D * C;
};

const computeF = ({ AA, CA }) => {
  const A1 = p(AA), E = p(CA);
  if (A1 < 2e6 && E <= 1e3) return 0;
  else if (A1 < 2e6 && E > 1e3) return (E - 1e3) * 0.5;
  else if (A1 >= 2e6) return E * 0.5;
};

const computeI = ({ CB, DA, DB }) => {
  const F = p(CB), G = p(DA), H = p(DB);
  return F - G - H;
};

const computeL = ({ EA, EB, EC }) => {
  const I = p(EA), J = p(EB), K = p(EC);
  return I + J + K;
};

const computeM = ({ ED, GA }) => {
  const L = p(ED), M0 = GA;
  if (M0) {
    return '';
  } else {
    return L * 1.73 / 100;
  }
};

const computeO = ({ ED, GB }) => {
  const L = p(ED), M = p(GB);
  return (L + M) * 1 / 100;
};

const computeP = ({ ED, GB, HA }) => {
  const L = p(ED), M = p(GB), O = p(HA);
  const sum = L + M + O;
  return sum < 0 ? 0 : sum;
};

const CVAE1329AC = (props) => {
  const { change, cvae1329ACForm } = props;

  const ZA = _get(cvae1329ACForm, 'ZA', 0); // P
  setValue(cvae1329ACForm, 'KA', ZA <= 1500 ? 0 : ZA, change);
  useCompute('AC', computeB, ['AA'], cvae1329ACForm, change); // B = calculation of A1
  useCompute('BB', computeD, ['BC', 'AA', 'AB'], cvae1329ACForm, change); // D = calculation of D0, A1, A2
  useCompute('CA', computeE, ['AC', 'BA', 'BB', 'BC'], cvae1329ACForm, change); // E = calculation of B, C, D, D0
  useCompute('CB', computeF, ['AA', 'CA'], cvae1329ACForm, change); // F = calculation of A1, E
  useCompute('EA', computeI, ['CB', 'DA', 'DB'], cvae1329ACForm, change); // I = calculation of F, G, H
  useCompute('ED', computeL, ['EA', 'EB', 'EC'], cvae1329ACForm, change); // L = calculation of I, J, K
  useCompute('GB', computeM, ['ED', 'GA'], cvae1329ACForm, change); // M = calculation of L, M0
  useCompute('HA', computeO, ['ED', 'GB'], cvae1329ACForm, change); // O = calculation of L and M
  useCompute('ZA', computeP, ['ED', 'GB', 'HA'], cvae1329ACForm, change); // P = calculation of L, M, O

  return (
    <div className="CVAE1329AC">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 79, top: 714, width: 217, height: 15, 'font-size': 18, }}>
<span>
PERIODE DE L&#39;ACOMPTE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 958, width: 289, height: 19, 'font-size': 20, }}>
<span>
PAIEMENT, DATE, SIGNATURE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 480, top: 958, width: 300, height: 17, 'font-size': 20, }}>
<span>
RESERVE A L&#39;ADMINISTRATION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 32, top: 511, width: 165, height: 14, 'font-size': 14, }}>
<span>
de l&#39;établissement principal
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 664, width: 259, height: 17, 'font-size': 20, }}>
<span>
MONTANT DU VERSEMENT
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 447, width: 108, height: 10, 'font-size': 12, }}>
<span>
N° d&#39;identification de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 465, width: 171, height: 12, 'font-size': 12, }}>
<span>
l&#39;établissement principal (SIRET)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 32, top: 492, width: 51, height: 11, 'font-size': 14, }}>
<span>
Adresse
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 374, width: 25, height: 13, 'font-size': 16, }}>
<span>
SIE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 397, width: 93, height: 13, 'font-size': 16, }}>
<span>
Code service
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 293, top: 991, width: 76, height: 17, 'font-size': 16, }}>
<span>
Signature :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 758, top: 1030, width: 107, height: 16, 'font-size': 16, }}>
<span>
N° d&#39;opération :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 710, top: 992, width: 41, height: 12, 'font-size': 16, }}>
<span>
Date :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1488, width: 1055, height: 14, 'font-size': 14, }}>
<span>
Les dispositions des articles 39 et 40 de la loi n° 78-17 du 6 janvier 1978 relative à l&#39;informatique, aux fichiers et aux libertés, modifiées par la loi n° 2004-801 du 6 août 2004,
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 254, top: 1506, width: 672, height: 14, 'font-size': 14, }}>
<span>
garantissent les droits des personnes physiques à l&#39;égard des traitements des données à caractère personnel.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1175, width: 417, height: 16, 'font-size': 20, }}>
<span>
CADRE RESERVE A LA CORRESPONDANCE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 468, top: 991, width: 62, height: 13, 'font-size': 16, }}>
<span>
Somme :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 991, width: 126, height: 13, 'font-size': 16, }}>
<span>
Cachet du service
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 37, top: 1069, width: 356, height: 16, 'font-size': 16, }}>
<span>
Adresse électronique : ……………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 992, width: 108, height: 12, 'font-size': 16, }}>
<span>
Date : …………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 149, top: 1001, width: 87, height: 3, 'font-size': 16, }}>
<span>
...... …………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 142, top: 1040, width: 97, height: 3, 'font-size': 16, }}>
<span>
... ……… ..... …
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 37, top: 1030, width: 103, height: 16, 'font-size': 16, }}>
<span>
Téléphone : …
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1137, top: 1496, width: 3, height: 2, 'font-size': 14, }}>
<span>
.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 159, width: 461, height: 16, 'font-size': 19, }}>
<span>
DIRECTION GENERALE DES FINANCES PUBLIQUES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 36, width: 316, height: 17, 'font-size': 22, }}>
<span>
COTISATION SUR LA VALEUR
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 76, top: 56, width: 313, height: 17, 'font-size': 22, }}>
<span>
AJOUTEE DES ENTREPRISES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 183, top: 106, width: 90, height: 32, 'font-size': 42, }}>
<span>
201 9
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 470, top: 207, width: 282, height: 23, 'font-size': 28, }}>
<span>
RELEVE D&#39;ACOMPTE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 579, top: 283, width: 278, height: 12, 'font-size': 12, }}>
<span>
Service compétent où doit être adressé le relevé :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 420, width: 25, height: 13, 'font-size': 16, }}>
<span>
RIB
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 886, width: 750, height: 18, 'font-size': 18, }}>
<span>
(*) Vous êtes dispensé du paiement de l&#39;acompte si celui-ci est inférieur ou égal à 1500 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 575, width: 74, height: 13, 'font-size': 16, }}>
<span>
Date limite
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 783, width: 293, height: 20, 'font-size': 20, }}>
<span>
MONTANT TOTAL A PAYER (*)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 579, top: 422, width: 200, height: 10, 'font-size': 12, }}>
<span>
Nom et adresse de l&#39;établissement :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 596, width: 88, height: 16, 'font-size': 16, }}>
<span>
de paiement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 550, width: 91, height: 13, 'font-size': 16, }}>
<span>
N° FRP - Clé
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 831, top: 713, width: 249, height: 18, 'font-size': 18, }}>
<span>
Cochez la case correspondante
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 716, width: 40, height: 14, 'font-size': 18, }}>
<span>
JUIN
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 652, top: 716, width: 110, height: 14, 'font-size': 18, }}>
<span>
SEPTEMBRE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 830, top: 784, width: 240, height: 18, 'font-size': 18, }}>
<span>
Report de la ligne 18 , page 3
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 785, width: 20, height: 16, 'font-size': 20, }}>
<span>
19
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1045, top: 98, width: 50, height: 28, 'font-size': 29, }}>
<span>
cer fa
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1028, top: 126, width: 82, height: 12, 'font-size': 15, }}>
<span>
n° 14 044*10
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 993, top: 36, width: 156, height: 18, 'font-size': 23, }}>
<span>
N° 1329-AC-SD
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 6, top: 1329, width: 12, height: 179, 'font-size': 13, }}>
<span>
SDNC - oncept ion W 2019
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 6, top: 1362, width: 10, height: 14, 'font-size': 13, }}>
<span>
eb
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 6, top: 1447, width: 10, height: 9, 'font-size': 13, }}>
<span>
C
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 480, top: 1446, width: 12, height: 16, 'font-size': 21, }}>
<span>
Z
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 477, top: 784, width: 16, height: 16, 'font-size': 21, }}>
<span>
N
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field "
                   name="CODESERVICE_STE" data-field-id="16964936" data-annot-id="15351328" maxLength="8"
                   type="text" disabled/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field "
                   name="RIB_STE" data-field-id="16962152" data-annot-id="16606048" disabled maxLength="25"
                   type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field "
                   name="SIRET_STET" data-field-id="16960040" data-annot-id="16606272" disabled maxLength="14"
                   type="text"/>

            <textarea className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="a3.4"
                      data-field-id="16883288" data-annot-id="16606464" disabled>
</textarea>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field "
                   name="NUMFRPE_STE" data-field-id="16963160" data-annot-id="16606656" disabled maxLength="11"
                   type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field "
                   name="LIMITE_PAIEMENT_STE" data-field-id="16962824" data-annot-id="16606848" disabled
                   maxLength="17" type="text"/>

            <textarea className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field "
                      name="SERVICECOMPETENT_STE"
                      data-field-id="16962488" data-annot-id="16607040" disabled>
</textarea>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope"
                   className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="CAC1"
                   data-field-id="16899128" data-annot-id="16607232" value="0" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope"
                   className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="CAC2"
                   data-field-id="16883592" data-annot-id="16607424" value="0" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="KA"
                   data-field-id="16958168" data-annot-id="16607616" disabled value="0" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field "
                   name="DATE_PAIEMENT" data-field-id="16963496" data-annot-id="16607808" maxLength="10"
                   type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field "
                   name="TEl_PAIEMENT" data-field-id="16963832" data-annot-id="16608000" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field "
                   name="ADRESS_MAIL_PAIEMENT" data-field-id="16964168" data-annot-id="16608192"
                   type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field "
                   name="ADR_STED" data-field-id="16960216" data-annot-id="16608384" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field "
                   name="ADR_STEF" data-field-id="16960552" data-annot-id="16608576" disabled type="text"/>

            <textarea className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="ADR_STEG"
                      data-field-id="16960888" data-annot-id="16608768" disabled>
</textarea>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="VILLE_STEI" data-field-id="16961224" data-annot-id="16608960" type="text" disabled/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field "
                   name="CP_STEH" data-field-id="16924776" data-annot-id="16609424" type="text" disabled/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="SIE_STE" data-field-id="16964504" data-annot-id="16609616" type="text" disabled/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field "
                   name="NOM_STEA" data-field-id="16965240" data-annot-id="16609808" maxLength="8"
                   type="text" disabled/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field "
                   name="SIGNATURE_STE" data-field-id="16967016" data-annot-id="16610000" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field "
                   name="CORRESPONDANCE" data-field-id="16967352" data-annot-id="16610192" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 228, top: 540, width: 106, height: 16, 'font-size': 21, }}>
<span>
&#60; 500 000 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 997, top: 826, width: 20, height: 20, 'font-size': 21, }}>
<span>
(*)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 158, width: 19, height: 16, 'font-size': 21, }}>
<span>
01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 224, width: 22, height: 16, 'font-size': 21, }}>
<span>
02
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1295, width: 936, height: 18, 'font-size': 19, }}>
<span>
&#60; 600 000 €, le montant à porter dans le cadre D correspond à 80% du chiffre d&#39;affaires porté au cadre A1 ou A2 .
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 1318, width: 982, height: 18, 'font-size': 19, }}>
<span>
Si CA &#62; 7 600 000 €, le montant à porter dans le cadre D correspond à 85% du chiffre d&#39;affaires porté au cadre A1 ou A2.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 212, width: 404, height: 20, 'font-size': 21, }}>
<span>
Montant du CA réel si la période de référence
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 237, width: 219, height: 16, 'font-size': 21, }}>
<span>
est différente de 12 mois
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 1027, width: 22, height: 15, 'font-size': 21, }}>
<span>
05
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 835, width: 22, height: 16, 'font-size': 21, }}>
<span>
04
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 182, top: 770, width: 226, height: 20, 'font-size': 21, }}>
<span>
Supérieur à 50 000 000 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 713, width: 306, height: 17, 'font-size': 21, }}>
<span>
10 000 000 € &#60; CA 50 000 000 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 654, width: 296, height: 16, 'font-size': 21, }}>
<span>
3 000 000 € &#60; CA 10 000 000 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 834, width: 20, height: 19, 'font-size': 25, }}>
<span>
%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 589, top: 837, width: 12, height: 15, 'font-size': 21, }}>
<span>
B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 487, top: 969, width: 264, height: 14, 'font-size': 19, }}>
<span>
VALEUR AJOUTEE PRODUITE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 924, width: 320, height: 16, 'font-size': 21, }}>
<span>
DONNEES DE VALEUR AJOUTEE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 84, top: 826, width: 256, height: 17, 'font-size': 17, }}>
<span>
(*) Taux exprimé en pourcentage et
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 847, width: 249, height: 17, 'font-size': 17, }}>
<span>
arrondi au centième le plus proche
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 583, top: 224, width: 24, height: 16, 'font-size': 21, }}>
<span>
A2
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1250, width: 21, height: 15, 'font-size': 21, }}>
<span>
06
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 496, width: 271, height: 17, 'font-size': 21, }}>
<span>
Si le montant de votre CA est :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 589, top: 1026, width: 13, height: 16, 'font-size': 21, }}>
<span>
C
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 1078, width: 981, height: 20, 'font-size': 21, }}>
<span>
Le montant à porter dans ce cadre figure sur les imprimés 2059-E (ligne SA), 2033-E (ligne 117), 2035-E (ligne JU) et
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1103, width: 166, height: 20, 'font-size': 21, }}>
<span>
2072-E (ligne D12).
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 452, top: 1148, width: 332, height: 15, 'font-size': 19, }}>
<span>
LIMITATION DE LA VALEUR AJOUTEE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 709, top: 642, width: 241, height: 20, 'font-size': 21, }}>
<span>
[0,9 x (CA-3 000 000)] +0,5
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 785, top: 667, width: 89, height: 16, 'font-size': 21, }}>
<span>
7 000 000
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 590, top: 1250, width: 13, height: 15, 'font-size': 21, }}>
<span>
D
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 824, top: 539, width: 11, height: 16, 'font-size': 21, }}>
<span>
0
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 595, width: 280, height: 16, 'font-size': 21, }}>
<span>
500 000 € &#60; CA &#60; 3 000 000 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 51, width: 349, height: 16, 'font-size': 21, }}>
<span>
DONNEES DE CHIFFRE D&#39;AFFAIRES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 456, top: 101, width: 317, height: 15, 'font-size': 19, }}>
<span>
MONTANT DU CHIFFRE D&#39;AFFAIRES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 396, width: 665, height: 15, 'font-size': 19, }}>
<span>
CALCUL DU POURCENTAGE DE LA VALEUR AJOUTEE CORRESPONDANTE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 76, top: 434, width: 981, height: 20, 'font-size': 21, }}>
<span>
Le pourcentage à calculer et à porter dans le cadre B varie selon le montant du chiffre d&#39;affaires, mentionné au
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 459, width: 462, height: 19, 'font-size': 21, }}>
<span>
cadre A1 ou A3, conformément au barème suivant :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 583, top: 161, width: 21, height: 16, 'font-size': 21, }}>
<span>
A1
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 158, width: 373, height: 20, 'font-size': 21, }}>
<span>
Montant du CA de la période de référence
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 740, top: 582, width: 179, height: 20, 'font-size': 21, }}>
<span>
[0,5 x (CA-500 000)]
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 793, top: 608, width: 89, height: 16, 'font-size': 21, }}>
<span>
2 500 000
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 285, width: 22, height: 16, 'font-size': 21, }}>
<span>
03
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 583, top: 285, width: 25, height: 16, 'font-size': 21, }}>
<span>
A3
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 284, width: 347, height: 21, 'font-size': 21, }}>
<span>
Montant du CA de référence du groupe
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 703, top: 701, width: 253, height: 20, 'font-size': 21, }}>
<span>
[0,1 x (CA-10 000 000)] +1,4
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 779, top: 726, width: 100, height: 16, 'font-size': 21, }}>
<span>
40 000 000
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 812, top: 770, width: 37, height: 18, 'font-size': 21, }}>
<span>
1,50
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 577, top: 496, width: 497, height: 20, 'font-size': 21, }}>
<span>
Alors, le taux à porter cadre B sera calculé comme suit :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1194, width: 931, height: 19, 'font-size': 18, }}>
<span>
Pour certaines entreprise à caractère financier (Cf. notice), cochez la case ci-contre et ne remplissez pas la case D
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 1294, width: 75, height: 15, 'font-size': 19, }}>
<span>
Si CA 7
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 315, top: 655, width: 10, height: 11, 'font-size': 21, }}>
<span>
&#60;
  <br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 319, top: 715, width: 11, height: 11, 'font-size': 21, }}>
<span>
&#60;
  <br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1016, top: 1192, width: 24, height: 16, 'font-size': 21, }}>
<span>
D0
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 451, top: 1379, width: 272, height: 15, 'font-size': 19, }}>
<span>
MONTANT DE LA CVAE BRUTE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1434, width: 929, height: 20, 'font-size': 21, }}>
<span>
Le montant de la cotisation sur la valeur ajoutée à porter dans la case E est obtenu par le calcul suivant :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1479, width: 814, height: 21, 'font-size': 21, }}>
<span>
 Si C &#60; D, alors le montant de la cotisation sur la valeur ajoutée est égal à C multiplié par B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 1505, width: 814, height: 21, 'font-size': 21, }}>
<span>
 Si C &#62; D, alors le montant de la cotisation sur la valeur ajoutée est égal à D multiplié par B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 84, top: 1542, width: 1005, height: 19, 'font-size': 19, }}>
<span>
Si vous avez coché la case D0 ci-dessus, alors le montant de la cotisation sur la valeur ajoutée est égal à C multiplié par B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 588, top: 1588, width: 12, height: 15, 'font-size': 21, }}>
<span>
E
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 1589, width: 22, height: 16, 'font-size': 21, }}>
<span>
07
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 115, top: 334, width: 690, height: 24, 'font-size': 21, }}>
<span>
Le montant porté à cette ligne doit etre supérieur ou égal à 7 630 000 euros
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 334, width: 9, height: 13, 'font-size': 17, }}>
<span>

<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="AA"
                   data-field-id="16902408" data-annot-id="15573792" maxLength="13" type="text"/>
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field "
                   name="AC"
                   data-field-id="16911976" data-annot-id="18147424" disabled value="0.00" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field "
                   name="BA"
                   data-field-id="16914968" data-annot-id="18182160" maxLength="13" type="text" disabled/>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope"
                   className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="BC"
                   data-field-id="16921176" data-annot-id="18182352" value="On" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field "
                   name="BB" disabled data-field-id="16918488" data-annot-id="18182544" value="0" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field "
                   name="CA"
                   data-field-id="16924440" data-annot-id="18182736" disabled value="0" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field "
                   name="AB"
                   data-field-id="16905672" data-annot-id="18182928" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field "
                   name="AD"
                   data-field-id="16908824" data-annot-id="18183120" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-2" data-page-num="2" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-2 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 46, top: 538, width: 21, height: 15, 'font-size': 21, }}>
<span>
10
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 487, width: 22, height: 16, 'font-size': 21, }}>
<span>
09
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 487, width: 14, height: 16, 'font-size': 21, }}>
<span>
G
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 103, width: 557, height: 17, 'font-size': 21, }}>
<span>
CALCUL DE LA COTISATION SUR LA VALEUR AJOUTEE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 438, width: 22, height: 16, 'font-size': 21, }}>
<span>
08
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 549, top: 438, width: 11, height: 16, 'font-size': 21, }}>
<span>
F
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 487, top: 144, width: 246, height: 19, 'font-size': 19, }}>
<span>
CALCUL DE L&#39;ACOMPTE DÛ
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 537, width: 306, height: 17, 'font-size': 21, }}>
<span>
REDUCTION SUPPLEMENTAIRE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 548, top: 538, width: 12, height: 15, 'font-size': 21, }}>
<span>
H
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 585, width: 230, height: 24, 'font-size': 21, }}>
<span>
ACOMPTE DÛ (F - G - H)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 512, top: 852, width: 201, height: 15, 'font-size': 19, }}>
<span>
TAXE ADDITIONNELLE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 438, width: 296, height: 16, 'font-size': 21, }}>
<span>
ACOMPTE AVANT REDUCTION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 487, width: 158, height: 17, 'font-size': 21, }}>
<span>
EXONERATIONS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 591, width: 18, height: 15, 'font-size': 21, }}>
<span>
11
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 552, top: 591, width: 3, height: 15, 'font-size': 21, }}>
<span>
I
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 951, width: 389, height: 21, 'font-size': 21, }}>
<span>
15 TAXE ADDITIONNELLE (L x 1,73 %)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 210, width: 286, height: 21, 'font-size': 21, }}>
<span>
MONTANT DU CA (CADRE A1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 210, width: 88, height: 17, 'font-size': 21, }}>
<span>
CADRE E
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 289, width: 156, height: 17, 'font-size': 21, }}>
<span>
CA &#60; 2 000 000 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 573, top: 263, width: 96, height: 16, 'font-size': 21, }}>
<span>
E &#60; 1000 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 573, top: 316, width: 96, height: 16, 'font-size': 21, }}>
<span>
E &#62;1000 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 189, top: 368, width: 156, height: 17, 'font-size': 21, }}>
<span>
CA &#62; 2 000 000 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 539, top: 368, width: 160, height: 17, 'font-size': 21, }}>
<span>
TOUT MONTANT
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 915, top: 369, width: 83, height: 16, 'font-size': 21, }}>
<span>
E X 50 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 789, top: 210, width: 328, height: 17, 'font-size': 21, }}>
<span>
MONTANT A REPORTER CADRE F
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 940, top: 264, width: 27, height: 15, 'font-size': 21, }}>
<span>
0 €
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 866, top: 316, width: 175, height: 20, 'font-size': 21, }}>
<span>
(E - 1000 €) X 50 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 629, top: 953, width: 15, height: 16, 'font-size': 21, }}>
<span>
M
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 527, top: 1012, width: 155, height: 15, 'font-size': 19, }}>
<span>
CADRE RESERVE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 433, top: 656, width: 338, height: 15, 'font-size': 19, }}>
<span>
AJUSTEMENT DU PREMIER ACOMPTE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 48, top: 711, width: 211, height: 17, 'font-size': 21, }}>
<span>
12 AUGMENTATION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 711, width: 9, height: 16, 'font-size': 21, }}>
<span>
J
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 48, top: 757, width: 168, height: 17, 'font-size': 21, }}>
<span>
13 DIMINUTION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 757, width: 13, height: 15, 'font-size': 21, }}>
<span>
K
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 48, top: 803, width: 344, height: 22, 'font-size': 21, }}>
<span>
14 ACOMPTE A VERSER (I + J - K)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 630, top: 802, width: 10, height: 16, 'font-size': 21, }}>
<span>
L
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 906, width: 861, height: 19, 'font-size': 19, }}>
<span>
Si vous êtes exonéré du paiement de la taxe additionnelle (Cf. notice), cochez la case ci-contre : MM
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 48, top: 1112, width: 20, height: 16, 'font-size': 21, }}>
<span>
1 6
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 950, top: 1080, width: 1, height: 1, 'font-size': 21, }}>
<span>
MM
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 628, top: 1114, width: 13, height: 16, 'font-size': 21, }}>
<span>
N
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1017, top: 1065, width: 24, height: 16, 'font-size': 21, }}>
<span>
N0
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 541, top: 1173, width: 169, height: 15, 'font-size': 19, }}>
<span>
FRAIS DE GESTION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 1243, width: 385, height: 21, 'font-size': 21, }}>
<span>
1 7 FRAIS DE GESTION (L + M) x 1 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 630, top: 1246, width: 15, height: 16, 'font-size': 21, }}>
<span>
O
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 552, top: 1318, width: 143, height: 15, 'font-size': 19, }}>
<span>
RECAPITULATIF
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 48, top: 1385, width: 20, height: 16, 'font-size': 21, }}>
<span>
18
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 630, top: 1387, width: 12, height: 16, 'font-size': 21, }}>
<span>
P
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1018, top: 905, width: 27, height: 16, 'font-size': 21, }}>
<span>
M0
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1387, width: 280, height: 20, 'font-size': 21, }}>
<span>
TOTAL A PAYER Y (L + M + O)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 1244, width: 5, height: 19, 'font-size': 20, }}>
<span>
]
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 303, top: 1244, width: 5, height: 19, 'font-size': 20, }}>
<span>
[
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field "
                   name="CB"
                   data-field-id="16927192" data-annot-id="18306384" disabled value="0" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field "
                   name="DA"
                   data-field-id="16929992" data-annot-id="18306576" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field "
                   name="DB"
                   data-field-id="16933160" data-annot-id="16396464" maxLength="13" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field "
                   name="EA"
                   data-field-id="16936280" data-annot-id="16396656" readonly value="0" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="ED"
                   data-field-id="16939000" data-annot-id="17310352" readonly value="0" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope"
                   className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="GA"
                   data-field-id="16950744" data-annot-id="17310544" value="X" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field "
                   name="GB"
                   data-field-id="16948024" data-annot-id="17310736" readonly value="0" type="text"
                   disabled={_get(cvae1329ACForm, 'GA', false) ? true : false}/>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope"
                   className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="CAC5"
                   data-field-id="16898856" data-annot-id="17310928" readonly value="On" type="checkbox"
                   data-default-value="Off" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field "
                   name="HA"
                   data-field-id="16954008" data-annot-id="17311312" readonly value="0" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field "
                   name="ZA"
                   data-field-id="16958168" data-annot-id="17311504" readonly value="0" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="EC"
                   data-field-id="16941720" data-annot-id="17311696" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="EB"
                   data-field-id="16944872" data-annot-id="17311888" type="text"/>

          </div>
        </div>
      </div>
    </div>
  );
};

export default CVAE1329AC;
