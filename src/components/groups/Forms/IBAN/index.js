import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import I18n from 'assets/I18n';
import { FontIcon } from 'components/basics/Icon';
import Button from 'components/basics/Buttons/Button';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { Field } from 'redux-form';
import styles from './IBAN.module.scss';

const materialStyles = () => ({
  IBAN: {
    width: 256,
    marginRight: 20
  },
  BIC: {
    width: 89,
    marginRight: 20
  },
  establishmentName: {
    width: 250,
    marginRight: 20
  },
  deleteButton: {
    display: 'flex',
    alignSelf: 'flex-end',
    marginBottom: 10,
    '&:hover': {
      backgroundColor: '#fff'
    }
  },
  addButton: {
    display: 'flex',
    justifyContent: 'flex-start',
    decoration: 'none',
    textDecoration: 'underline',
    '&:hover': {
      backgroundColor: '#fff'
    }
  }
});

const IBAN = (props) => {
  const {
    classes,
    deleteRib
  } = props;

  return (
    <div className={styles.accountDialog}>
      <div className={styles.blockInline}>
        <Field color="primary" className={classes.IBAN} name="infoC.IBAN" component={ReduxTextField} label={I18n.t('account.newAccount.ribSteps.IBAN')} />
        <Field color="primary" className={classes.BIC} name="infoC.BIC" component={ReduxTextField} label={I18n.t('account.newAccount.BIC')} />
        <Field color="primary" className={classes.establishmentName} name="infoC.establishmentName" component={ReduxTextField} label={I18n.t('account.newAccount.establishmentName')} />
        <Button className={classes.deleteButton} aria-label="Delete">
          <FontIcon color="#fe3a5e" size="22px" name="icon-trash" onClick={deleteRib} titleInfoBulle={I18n.t('tooltips.delete')} />
        </Button>
      </div>
      <Button className={classes.addButton} variant="text">{I18n.t('account.newAccount.addRib')}</Button>
    </div>
  );
};

IBAN.propTypes = {
  classes: PropTypes.shape({}),
  deleteRib: PropTypes.func.isRequired
};

IBAN.defaultProps = ({
  classes: {}
});

export default withStyles(materialStyles)(IBAN);
