import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {parse as p, sum, useCompute, setIsNothingness} from "helpers/pdfforms";

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";


const amountMoreZero = (values, fields) => {
  const diff = p(values[fields[0]]) - p(values[fields[1]]);
  return diff > 0 ? diff : 0;
};

const amountLessZero = (values, fields) => {
  const diff = p(values[fields[0]]) - p(values[fields[1]]);
  return diff < 0 ? diff : 0;
};

const notNeededFields = {
  'NOM_STEA': true,
  'GM': true
};

const Tax2059A = (props) => {
  const { taxDeclarationForm, change } = props;

  setIsNothingness(taxDeclarationForm, notNeededFields, 'GM', change);

  useCompute('WR', sum, ['WA', 'WB', 'XE', 'XW', 'XY', 'VG', 'BL', 'VJ', 'VC', 'WN', 'WO', 'XR', 'WQ', 'AG', 'AJ'], taxDeclarationForm, change);
  useCompute('XH', sum, ['WS', 'WT', 'WU', 'WV', 'VE', 'VF', 'WW', 'XB', 'VL', 'WZ', 'XA', 'ZY', 'XD', 'XF', 'XS', 'XG', 'AH'], taxDeclarationForm, change);
  useCompute('XI', amountMoreZero, ['WR', 'XH'], taxDeclarationForm, change);
  useCompute('XJ', amountLessZero, ['WR', 'XH'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2059a">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 369, top: 113, width: 508, height: 21, 'font-size': 23, }}>
<span>
DÉTERMINATION DES PLUS ET MOINS-VALUES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 950, top: 109, width: 236, height: 16, 'font-size': 20, }}>
<span>
DGFiP N° 2059-A-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 70, top: 1592, width: 491, height: 14, 'font-size': 14, }}>
<span>
* Des explications concernant cette rubrique sont données dans la notice n° 2032.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1608, width: 679, height: 14, 'font-size': 14, }}>
<span>
(1) Ces plus-values sont imposables au taux de 19 % en application des articles 238 bis JA, 208 C et 210 E du CGI.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 201, width: 30, height: 17, 'font-size': 23, }}>
<span>
A -
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 204, width: 320, height: 15, 'font-size': 15, }}>
<span>
DÉTERMINATION DE LA VALEUR RÉSIDUELLE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 71, top: 235, width: 278, height: 13, 'font-size': 14, }}>
<span>
Nature et date d’acquisition des éléments cédés*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 76, top: 173, width: 185, height: 15, 'font-size': 16, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 235, width: 97, height: 13, 'font-size': 14, }}>
<span>
Valeur d’origine*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 535, top: 235, width: 134, height: 10, 'font-size': 14, }}>
<span>
Valeur nette réévaluée*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 693, top: 228, width: 150, height: 14, 'font-size': 14, }}>
<span>
Amortissements pratiqués
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 709, top: 239, width: 119, height: 13, 'font-size': 14, }}>
<span>
en franchise d’impôt
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 915, top: 229, width: 37, height: 10, 'font-size': 14, }}>
<span>
Autres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 887, top: 239, width: 93, height: 10, 'font-size': 14, }}>
<span>
amortissements*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1051, top: 235, width: 97, height: 10, 'font-size': 14, }}>
<span>
Valeur résiduelle
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 136, width: 176, height: 12, 'font-size': 12, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 81, top: 146, width: 146, height: 11, 'font-size': 12, }}>
<span>
du code général des impôts).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 286, top: 116, width: 23, height: 17, 'font-size': 23, }}>
<span>
12
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 208, top: 252, width: 4, height: 7, 'font-size': 10, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 436, top: 251, width: 5, height: 8, 'font-size': 11, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 251, width: 5, height: 8, 'font-size': 10, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 767, top: 251, width: 5, height: 8, 'font-size': 10, }}>
<span>
4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 933, top: 251, width: 4, height: 8, 'font-size': 10, }}>
<span>
5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1098, top: 251, width: 5, height: 8, 'font-size': 10, }}>
<span>
6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1102, top: 170, width: 39, height: 12, 'font-size': 16, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1170, top: 163, width: 5, height: 6, 'font-size': 16, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 390, width: 13, height: 158, 'font-size': 18, }}>
<span>
I - Immobilisations*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 274, width: 4, height: 10, 'font-size': 14, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 309, width: 5, height: 9, 'font-size': 14, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 343, width: 5, height: 11, 'font-size': 14, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 376, width: 5, height: 11, 'font-size': 14, }}>
<span>
4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 412, width: 4, height: 11, 'font-size': 14, }}>
<span>
5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 445, width: 5, height: 11, 'font-size': 14, }}>
<span>
6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 481, width: 5, height: 10, 'font-size': 14, }}>
<span>
7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 515, width: 5, height: 10, 'font-size': 14, }}>
<span>
8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 550, width: 5, height: 11, 'font-size': 14, }}>
<span>
9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 584, width: 9, height: 10, 'font-size': 14, }}>
<span>
10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 619, width: 8, height: 9, 'font-size': 14, }}>
<span>
11
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 653, width: 8, height: 10, 'font-size': 14, }}>
<span>
12
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 251, top: 683, width: 27, height: 17, 'font-size': 23, }}>
<span>
B -
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 283, top: 688, width: 221, height: 14, 'font-size': 15, }}>
<span>
PLUS-VALUES, MOINS-VALUES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 704, top: 687, width: 428, height: 15, 'font-size': 16, }}>
<span>
Qualification fiscale des plus et moins-values réalisées *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 193, top: 720, width: 73, height: 10, 'font-size': 14, }}>
<span>
Prix de vente
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 426, top: 719, width: 179, height: 13, 'font-size': 14, }}>
<span>
Montant global de la plus-value
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 732, width: 117, height: 11, 'font-size': 14, }}>
<span>
ou de la moins-value
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 659, top: 725, width: 71, height: 10, 'font-size': 14, }}>
<span>
Court terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 878, top: 717, width: 66, height: 12, 'font-size': 14, }}>
<span>
Long terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 791, top: 749, width: 26, height: 11, 'font-size': 14, }}>
<span>
19 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 865, top: 749, width: 95, height: 12, 'font-size': 14, }}>
<span>
15 % ou 12,80 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1011, top: 749, width: 20, height: 10, 'font-size': 14, }}>
<span>
0 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1098, top: 716, width: 58, height: 11, 'font-size': 14, }}>
<span>
Plus-value
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1098, top: 732, width: 58, height: 11, 'font-size': 14, }}>
<span>
taxables à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1104, top: 749, width: 48, height: 12, 'font-size': 14, }}>
<span>
19 % (1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1173, width: 482, height: 13, 'font-size': 14, }}>
<span>
Fraction résiduelle de la provision spéciale de réévaluation afférente aux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1189, width: 92, height: 10, 'font-size': 14, }}>
<span>
éléments cédés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 651, top: 1183, width: 8, height: 8, 'font-size': 16, }}>
<span>
+
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1211, width: 423, height: 14, 'font-size': 14, }}>
<span>
Amortissements irrégulièrement différés se rapportant aux éléments cédés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 651, top: 1214, width: 8, height: 7, 'font-size': 16, }}>
<span>
+
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1236, width: 483, height: 14, 'font-size': 14, }}>
<span>
Amortissements afférents aux éléments cédés mais exclus des charges déduc -
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1253, width: 195, height: 13, 'font-size': 14, }}>
<span>
tibles par une disposition légale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 651, top: 1246, width: 8, height: 8, 'font-size': 16, }}>
<span>
+
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1268, width: 483, height: 14, 'font-size': 14, }}>
<span>
Amortissements non pratiqués en comptabilité et correspondant à la déduction
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1284, width: 482, height: 14, 'font-size': 14, }}>
<span>
fiscale pour investissement, définie par les lois de 1966, 1968 et 1975, effecti -
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1300, width: 92, height: 11, 'font-size': 14, }}>
<span>
vement utilisée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 651, top: 1295, width: 8, height: 8, 'font-size': 16, }}>
<span>
+
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1312, width: 482, height: 13, 'font-size': 14, }}>
<span>
Résultats nets de concession ou de sous concession de licences d’exploitation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1328, width: 481, height: 13, 'font-size': 14, }}>
<span>
de brevets faisant partie de l’actif immobilisé et n’ayant pas été acquis à titre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1344, width: 210, height: 14, 'font-size': 14, }}>
<span>
onéreux depuis moins de deux ans
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1358, width: 482, height: 14, 'font-size': 14, }}>
<span>
Provisions pour dépréciation des titres relevant du régime des plus ou moins-va -
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1374, width: 368, height: 14, 'font-size': 14, }}>
<span>
lues à long terme devenues sans objet au cours de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1391, width: 482, height: 14, 'font-size': 14, }}>
<span>
Dotations de l’exercice aux comptes de provisions pour dépréciation des titres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1407, width: 356, height: 14, 'font-size': 14, }}>
<span>
relevant du régime des plus ou moins-values à long terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1432, width: 281, height: 12, 'font-size': 14, }}>
<span>
Divers (détail à donner sur une note annexe)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 134, top: 1461, width: 495, height: 15, 'font-size': 16, }}>
<span>
CADRE A : plus ou moins-value nette à court terme (total algébrique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 210, top: 1480, width: 215, height: 14, 'font-size': 16, }}>
<span>
des lignes 1 à 20 de la colonne)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 134, top: 1505, width: 495, height: 15, 'font-size': 16, }}>
<span>
CADRE B : plus ou moins-value nette à long terme (total algébrique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 211, top: 1523, width: 214, height: 15, 'font-size': 16, }}>
<span>
des lignes 1 à 20 de la colonne)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 682, top: 1536, width: 25, height: 16, 'font-size': 20, }}>
<span>
(A)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 899, top: 1529, width: 24, height: 16, 'font-size': 20, }}>
<span>
(B)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 858, top: 1547, width: 106, height: 12, 'font-size': 12, }}>
<span>
(Ventilation par taux)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1114, top: 1536, width: 26, height: 16, 'font-size': 20, }}>
<span>
(C)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 134, top: 1558, width: 318, height: 15, 'font-size': 16, }}>
<span>
CADRE C : autres plus-values taxable à 19 % 11
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 757, width: 5, height: 8, 'font-size': 10, }}>
<span>
7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 514, top: 758, width: 5, height: 7, 'font-size': 10, }}>
<span>
8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 693, top: 757, width: 5, height: 8, 'font-size': 10, }}>
<span>
9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 908, top: 732, width: 9, height: 7, 'font-size': 10, }}>
<span>
10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1124, top: 760, width: 9, height: 7, 'font-size': 10, }}>
<span>
11
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 432, top: 1477, width: 8, height: 13, 'font-size': 16, }}>
<span>
9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 1521, width: 12, height: 11, 'font-size': 16, }}>
<span>
10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 895, width: 13, height: 158, 'font-size': 18, }}>
<span>
I - Immobilisations*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 90, top: 1297, width: 13, height: 159, 'font-size': 18, }}>
<span>
II - Autres éléments
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 789, width: 4, height: 10, 'font-size': 14, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 822, width: 5, height: 10, 'font-size': 14, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 855, width: 5, height: 10, 'font-size': 14, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 886, width: 5, height: 11, 'font-size': 14, }}>
<span>
4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 919, width: 5, height: 11, 'font-size': 14, }}>
<span>
5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 951, width: 5, height: 11, 'font-size': 14, }}>
<span>
6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 985, width: 5, height: 10, 'font-size': 14, }}>
<span>
7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 1017, width: 5, height: 10, 'font-size': 14, }}>
<span>
8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 1050, width: 5, height: 11, 'font-size': 14, }}>
<span>
9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1082, width: 9, height: 10, 'font-size': 14, }}>
<span>
10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1115, width: 9, height: 9, 'font-size': 14, }}>
<span>
11
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1147, width: 9, height: 10, 'font-size': 14, }}>
<span>
12
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1180, width: 9, height: 11, 'font-size': 14, }}>
<span>
13
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1212, width: 9, height: 11, 'font-size': 14, }}>
<span>
14
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1245, width: 9, height: 11, 'font-size': 14, }}>
<span>
15
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1284, width: 9, height: 11, 'font-size': 14, }}>
<span>
16
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1329, width: 9, height: 10, 'font-size': 14, }}>
<span>
17
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1369, width: 9, height: 10, 'font-size': 14, }}>
<span>
18
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1400, width: 9, height: 11, 'font-size': 14, }}>
<span>
19
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1433, width: 9, height: 10, 'font-size': 14, }}>
<span>
20
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 1226, width: 12, height: 263, 'font-size': 12, }}>
<span>
N° 2059-A- SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="GM" data-field-id="39251656" data-annot-id="38561952" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="HA1" data-field-id="39251800" data-annot-id="37594992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="HA2" data-field-id="39252104" data-annot-id="37595328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="HA3" data-field-id="39252408" data-annot-id="38651968" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="HA4" data-field-id="39252744" data-annot-id="38652160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="HA5" data-field-id="39253080" data-annot-id="38656160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="HA6" data-field-id="39254728" data-annot-id="38656352" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="HA7" data-field-id="39256008" data-annot-id="38656544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="HA8" data-field-id="39257288" data-annot-id="38656736" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="HA9" data-field-id="39258632" data-annot-id="38657072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="HA10" data-field-id="39257720" data-annot-id="38657264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="HA11" data-field-id="39257992" data-annot-id="38657456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="HA12" data-field-id="39259160" data-annot-id="38657648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="HB1" data-field-id="39259832" data-annot-id="38657840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="HB2" data-field-id="39260168" data-annot-id="38658032" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="HB3" data-field-id="39260504" data-annot-id="38658224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="HB4" data-field-id="39260984" data-annot-id="38658416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="HB5" data-field-id="39261288" data-annot-id="38658880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="HB6" data-field-id="39261624" data-annot-id="38659072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="HB7" data-field-id="39261960" data-annot-id="38659264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="HB8" data-field-id="39262296" data-annot-id="38659456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="HB9" data-field-id="39262632" data-annot-id="38659648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="HB10" data-field-id="39262968" data-annot-id="38659840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="HB11" data-field-id="39263304" data-annot-id="38660032" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="HB12" data-field-id="39263640" data-annot-id="38660224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="HC2" data-field-id="39259496" data-annot-id="38660416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="HC1" data-field-id="39263976" data-annot-id="38660608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="HC3" data-field-id="39264312" data-annot-id="38660800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="HC4" data-field-id="39264648" data-annot-id="38660992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="HC5" data-field-id="39264984" data-annot-id="38661184" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="HC6" data-field-id="39265320" data-annot-id="38661376" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="HC7" data-field-id="39267592" data-annot-id="38661568" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="HC8" data-field-id="39265656" data-annot-id="38661760" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="HC9" data-field-id="39265992" data-annot-id="38658608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="HC10" data-field-id="39260840" data-annot-id="38662480" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="HC11" data-field-id="39266920" data-annot-id="38662672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="HC12" data-field-id="39267256" data-annot-id="38662864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="HD1" data-field-id="39267928" data-annot-id="38663056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="HD2" data-field-id="39268264" data-annot-id="38663248" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="HD3" data-field-id="39268600" data-annot-id="38663440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="HD4" data-field-id="39268936" data-annot-id="38663632" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="HD6" data-field-id="39269272" data-annot-id="38663824" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="HD7" data-field-id="39269944" data-annot-id="38664016" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="HD5" data-field-id="39269608" data-annot-id="38664208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="HD8" data-field-id="39270280" data-annot-id="38664400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="HD9" data-field-id="39270616" data-annot-id="38664592" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="HD10" data-field-id="39270952" data-annot-id="38664784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="HD11" data-field-id="39271288" data-annot-id="38664976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="HD12" data-field-id="39271624" data-annot-id="38665168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="HE1" data-field-id="39271960" data-annot-id="38665360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="HE2" data-field-id="39272296" data-annot-id="38665552" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="HE3" data-field-id="39251208" data-annot-id="38665744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="HE4" data-field-id="39250808" data-annot-id="38665936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="HE5" data-field-id="39272568" data-annot-id="38666128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="HE7" data-field-id="39272904" data-annot-id="38666320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="HE6" data-field-id="39273240" data-annot-id="38666512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="HE8" data-field-id="39273576" data-annot-id="38666704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="HE9" data-field-id="39273912" data-annot-id="38666896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="HE10" data-field-id="39274248" data-annot-id="38667088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="HE11" data-field-id="39274584" data-annot-id="38667280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="HE12" data-field-id="39274920" data-annot-id="38667472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="HF1" data-field-id="39275256" data-annot-id="38667664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="HF2" data-field-id="39275592" data-annot-id="38667856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="HF3" data-field-id="39275928" data-annot-id="38668048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="HF4" data-field-id="39276264" data-annot-id="38668240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="HF5" data-field-id="39266344" data-annot-id="38661952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="HF6" data-field-id="39277512" data-annot-id="38662144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="HF7" data-field-id="39277816" data-annot-id="38669472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="HF8" data-field-id="39278152" data-annot-id="38669664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="HF9" data-field-id="39278488" data-annot-id="38669856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="HF10" data-field-id="39278824" data-annot-id="38670048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="HF11" data-field-id="39279160" data-annot-id="38670240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="HF12" data-field-id="39279496" data-annot-id="38670432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="HG1" data-field-id="39279832" data-annot-id="38670624" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="HG2" data-field-id="39280168" data-annot-id="38670816" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="HG3" data-field-id="39280504" data-annot-id="38671008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="HG5" data-field-id="39280840" data-annot-id="38671200" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="HG4" data-field-id="39281176" data-annot-id="38671392" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="HG6" data-field-id="39281512" data-annot-id="38671584" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="HG7" data-field-id="39281848" data-annot-id="38671776" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="HG8" data-field-id="39282184" data-annot-id="38671968" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="HG9" data-field-id="39282520" data-annot-id="38672160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="HG10" data-field-id="39282856" data-annot-id="38672352" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="HG11" data-field-id="39283192" data-annot-id="38672544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="HG12" data-field-id="39283528" data-annot-id="38672736" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="HH1" data-field-id="39283864" data-annot-id="38672928" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="HH2" data-field-id="39284872" data-annot-id="38673120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="HH3" data-field-id="39284536" data-annot-id="38673312" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="HH4" data-field-id="39284200" data-annot-id="38673504" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="HH5" data-field-id="39285208" data-annot-id="38673696" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field " name="HH6" data-field-id="39285544" data-annot-id="38673888" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="HH7" data-field-id="39285880" data-annot-id="38674080" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="HH8" data-field-id="39286216" data-annot-id="38674272" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="HH9" data-field-id="39286552" data-annot-id="38674464" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="HH10" data-field-id="39286888" data-annot-id="38674656" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="HH11" data-field-id="39287224" data-annot-id="38674848" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="HH12" data-field-id="39287560" data-annot-id="38675040" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="HJ1" data-field-id="39287896" data-annot-id="38675232" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field " name="HJ2" data-field-id="39288232" data-annot-id="38675424" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="HJ3" data-field-id="39288568" data-annot-id="38675616" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field " name="HJ4" data-field-id="39288904" data-annot-id="38675808" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field " name="HJ5" data-field-id="39289240" data-annot-id="38676000" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field " name="HJ6" data-field-id="39289576" data-annot-id="38676192" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field " name="HJ7" data-field-id="39289912" data-annot-id="38676384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field " name="HJ8" data-field-id="39290248" data-annot-id="38676576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field " name="HJ9" data-field-id="39290584" data-annot-id="38676768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field " name="HJ10" data-field-id="39290920" data-annot-id="38676960" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field " name="HJ11" data-field-id="39291256" data-annot-id="38677152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field " name="HJ12" data-field-id="39291592" data-annot-id="38677344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field " name="HK1" data-field-id="39291928" data-annot-id="38677536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field " name="HK2" data-field-id="39292264" data-annot-id="38677728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field " name="HK3" data-field-id="39292600" data-annot-id="38677920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field " name="HK4" data-field-id="39292936" data-annot-id="38678112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field " name="HK5" data-field-id="39293272" data-annot-id="38678304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field " name="HK6" data-field-id="39293608" data-annot-id="38678496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field " name="HK7" data-field-id="39293944" data-annot-id="38678688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field " name="HK8" data-field-id="39294280" data-annot-id="38678880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field " name="HK9" data-field-id="39294616" data-annot-id="38679072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field " name="HK10" data-field-id="39294952" data-annot-id="38679264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field " name="HK11" data-field-id="39295288" data-annot-id="38679456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field " name="HK12" data-field-id="39295624" data-annot-id="38679648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field " name="HM1" data-field-id="39295960" data-annot-id="38679840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field " name="HM2" data-field-id="39296296" data-annot-id="38680032" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field " name="HM3" data-field-id="39296632" data-annot-id="38680224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_125 pdf-obj-fixed acroform-field " name="HM4" data-field-id="39296968" data-annot-id="38680416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_126 pdf-obj-fixed acroform-field " name="HM5" data-field-id="39297304" data-annot-id="38680608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_127 pdf-obj-fixed acroform-field " name="HM6" data-field-id="39297640" data-annot-id="38680800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_128 pdf-obj-fixed acroform-field " name="HM7" data-field-id="39297976" data-annot-id="38680992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field " name="HM8" data-field-id="39298312" data-annot-id="38681184" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_130 pdf-obj-fixed acroform-field " name="HM9" data-field-id="39276616" data-annot-id="38668432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_131 pdf-obj-fixed acroform-field " name="HM10" data-field-id="39276952" data-annot-id="38668624" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_132 pdf-obj-fixed acroform-field " name="HM11" data-field-id="39277288" data-annot-id="38668816" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_133 pdf-obj-fixed acroform-field " name="HM12" data-field-id="39300712" data-annot-id="38669008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_134 pdf-obj-fixed acroform-field " name="GN1" data-field-id="39301048" data-annot-id="38669200" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_135 pdf-obj-fixed acroform-field " name="GN2" data-field-id="39301384" data-annot-id="38683440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_136 pdf-obj-fixed acroform-field " name="GN3" data-field-id="39301720" data-annot-id="38683632" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_137 pdf-obj-fixed acroform-field " name="GN4" data-field-id="39302056" data-annot-id="38683824" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_138 pdf-obj-fixed acroform-field " name="GN5" data-field-id="39302392" data-annot-id="38684016" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_139 pdf-obj-fixed acroform-field " name="GN6" data-field-id="39302728" data-annot-id="38684208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_140 pdf-obj-fixed acroform-field " name="GN7" data-field-id="39303064" data-annot-id="38684400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_141 pdf-obj-fixed acroform-field " name="GN8" data-field-id="39303400" data-annot-id="38684592" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_142 pdf-obj-fixed acroform-field " name="GN9" data-field-id="39303736" data-annot-id="38684784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_143 pdf-obj-fixed acroform-field " name="GN10" data-field-id="39304072" data-annot-id="38684976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_144 pdf-obj-fixed acroform-field " name="GN11" data-field-id="39304408" data-annot-id="38685168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_145 pdf-obj-fixed acroform-field " name="GN12" data-field-id="39304744" data-annot-id="38685360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field " name="GP1" data-field-id="39305080" data-annot-id="38685552" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_147 pdf-obj-fixed acroform-field " name="GP2" data-field-id="39305416" data-annot-id="38685744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_148 pdf-obj-fixed acroform-field " name="GP3" data-field-id="39305752" data-annot-id="38685936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_149 pdf-obj-fixed acroform-field " name="GP4" data-field-id="39306088" data-annot-id="38686128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_150 pdf-obj-fixed acroform-field " name="GP5" data-field-id="39306424" data-annot-id="38686320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_151 pdf-obj-fixed acroform-field " name="GP6" data-field-id="39306760" data-annot-id="38686512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_152 pdf-obj-fixed acroform-field " name="GP7" data-field-id="39307096" data-annot-id="38686704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_153 pdf-obj-fixed acroform-field " name="GP8" data-field-id="39307432" data-annot-id="38686896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_154 pdf-obj-fixed acroform-field " name="GP9" data-field-id="39307768" data-annot-id="38687088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_155 pdf-obj-fixed acroform-field " name="GP10" data-field-id="39308104" data-annot-id="38687280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_156 pdf-obj-fixed acroform-field " name="GP11" data-field-id="39308440" data-annot-id="38687472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_157 pdf-obj-fixed acroform-field " name="GP12" data-field-id="39308776" data-annot-id="38687664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_158 pdf-obj-fixed acroform-field " name="HT1" data-field-id="39309112" data-annot-id="38687856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_159 pdf-obj-fixed acroform-field " name="HT2" data-field-id="39309448" data-annot-id="38688048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_160 pdf-obj-fixed acroform-field " name="HT3" data-field-id="39309784" data-annot-id="38688240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_161 pdf-obj-fixed acroform-field " name="HT4" data-field-id="39310120" data-annot-id="38688432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_162 pdf-obj-fixed acroform-field " name="HT5" data-field-id="39310456" data-annot-id="38688624" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_163 pdf-obj-fixed acroform-field " name="HT6" data-field-id="39310792" data-annot-id="38688816" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_164 pdf-obj-fixed acroform-field " name="HT7" data-field-id="39311128" data-annot-id="38689008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_165 pdf-obj-fixed acroform-field " name="HT8" data-field-id="39311464" data-annot-id="38689200" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_166 pdf-obj-fixed acroform-field " name="HT9" data-field-id="39311800" data-annot-id="38689392" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_167 pdf-obj-fixed acroform-field " name="HT10" data-field-id="39312136" data-annot-id="38689584" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_168 pdf-obj-fixed acroform-field " name="HT11" data-field-id="39312472" data-annot-id="38689776" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_169 pdf-obj-fixed acroform-field " name="HT12" data-field-id="39312808" data-annot-id="38689968" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_170 pdf-obj-fixed acroform-field " name="GA" data-field-id="39313144" data-annot-id="38690160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_171 pdf-obj-fixed acroform-field " name="GB" data-field-id="39313480" data-annot-id="38690352" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_172 pdf-obj-fixed acroform-field " name="GC" data-field-id="39313816" data-annot-id="38690544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_173 pdf-obj-fixed acroform-field " name="GD" data-field-id="39314152" data-annot-id="38690736" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_174 pdf-obj-fixed acroform-field " name="HU" data-field-id="39314488" data-annot-id="38690928" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_175 pdf-obj-fixed acroform-field " name="HV" data-field-id="39314824" data-annot-id="38691120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_176 pdf-obj-fixed acroform-field " name="HW" data-field-id="39315160" data-annot-id="38691312" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_177 pdf-obj-fixed acroform-field " name="HZ" data-field-id="39315496" data-annot-id="38691504" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_178 pdf-obj-fixed acroform-field " name="GQ" data-field-id="39315832" data-annot-id="38691696" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_179 pdf-obj-fixed acroform-field " name="HQ" data-field-id="39316168" data-annot-id="38691888" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_180 pdf-obj-fixed acroform-field " name="GS" data-field-id="39316504" data-annot-id="38692080" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_181 pdf-obj-fixed acroform-field " name="GT" data-field-id="39316840" data-annot-id="38692272" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_182 pdf-obj-fixed acroform-field " name="HR" data-field-id="39317176" data-annot-id="38692464" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_183 pdf-obj-fixed acroform-field " name="GU" data-field-id="39317512" data-annot-id="38692656" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_184 pdf-obj-fixed acroform-field " name="GV" data-field-id="39317848" data-annot-id="38692848" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_185 pdf-obj-fixed acroform-field " name="GH" data-field-id="39318184" data-annot-id="38693040" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_186 pdf-obj-fixed acroform-field " name="HS" data-field-id="39318520" data-annot-id="38693232" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_187 pdf-obj-fixed acroform-field " name="GW" data-field-id="39318856" data-annot-id="38693424" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_188 pdf-obj-fixed acroform-field " name="GX" data-field-id="39319192" data-annot-id="38693616" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_189 pdf-obj-fixed acroform-field " name="HX" data-field-id="39319528" data-annot-id="38693808" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_190 pdf-obj-fixed acroform-field " name="GK" data-field-id="39319864" data-annot-id="38694000" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_191 pdf-obj-fixed acroform-field " name="HN" data-field-id="39320200" data-annot-id="38694192" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_192 pdf-obj-fixed acroform-field " name="GY" data-field-id="39320536" data-annot-id="38694384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_193 pdf-obj-fixed acroform-field " name="GZ" data-field-id="39320872" data-annot-id="38694576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_194 pdf-obj-fixed acroform-field " name="HY" data-field-id="39321208" data-annot-id="38694768" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_195 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="9733256" data-annot-id="9106432" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2059A;
