import React from 'react';
import { Field } from 'redux-form';

import { useCompute, sum, parse as p, setIsNothingness } from 'helpers/pdfforms';

import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

// Utils
const spm = (values, fields) => ( // returns S + P - M  (fields = ['S', 'P', 'M'], order is important !)
  p(values[fields[0]]) + p(values[fields[1]]) - p(values[fields[2]])
);

const notNeededFields = {
  'NOM_STEA': true,
  'ZE': true
};


// Component
const Tax2056 = (props) => {
  const { formValues, change } = props;

  setIsNothingness(formValues, notNeededFields, 'ZE', change);

  // Compute
  useCompute('TC', spm, ['VA', 'TA', 'TB'], formValues, change);
  useCompute('TF', spm, ['VB', 'TD', 'TE'], formValues, change);
  useCompute('TI', spm, ['VC', 'TG', 'TH'], formValues, change);
  useCompute('TO', spm, ['VE', 'TM', 'TN'], formValues, change);
  useCompute('AD', spm, ['AA', 'AB', 'AC'], formValues, change);
  useCompute('IM', spm, ['IJ', 'IK', 'IL'], formValues, change);
  useCompute('TR', spm, ['VF', 'TP', 'TQ'], formValues, change);

  useCompute('VG', sum, ['VA', 'VB', 'VC', 'VE', 'AA', 'IJ', 'VF'], formValues, change);
  useCompute('TS', sum, ['TA', 'TD', 'TG', 'TM', 'AB', 'IK', 'TP'], formValues, change);
  useCompute('TT', sum, ['TB', 'TE', 'TH', 'TN', 'AC', 'IL', 'TQ'], formValues, change);
  useCompute('TU', sum, ['TC', 'TF', 'TI', 'TO', 'AD', 'IM', 'TR'], formValues, change);

  useCompute('XS', spm, ['VH', 'WE', 'WY'], formValues, change);
  useCompute('XT', spm, ['VJ', 'WF', 'WZ'], formValues, change);
  useCompute('XU', spm, ['VK', 'WG', 'XA'], formValues, change);
  useCompute('XV', spm, ['VL', 'WH', 'XB'], formValues, change);
  useCompute('XW', spm, ['VM', 'WJ', 'XC'], formValues, change);
  useCompute('XX', spm, ['VN', 'WK', 'XD'], formValues, change);
  useCompute('XY', spm, ['VP', 'WL', 'XE'], formValues, change);
  useCompute('XZ', spm, ['VQ', 'WM', 'XF'], formValues, change);
  useCompute('ZJ', spm, ['ZF', 'ZG', 'ZH'], formValues, change);
  useCompute('YB', spm, ['VS', 'WP', 'XH'], formValues, change);
  useCompute('YC', spm, ['VT', 'WQ', 'XJ'], formValues, change);

  useCompute('VU', sum, ['VH', 'VJ', 'VK', 'VL', 'VM', 'VN', 'VP', 'VQ', 'ZF', 'VS', 'VT'], formValues, change);
  useCompute('TV', sum, ['WE', 'WF', 'WG', 'WH', 'WJ', 'WK', 'WL', 'WM', 'ZG', 'WP', 'WQ'], formValues, change);
  useCompute('TW', sum, ['WY', 'WZ', 'XA', 'XB', 'XC', 'XD', 'XE', 'XF', 'ZH', 'XH', 'XJ'], formValues, change);
  useCompute('TX', sum, ['XS', 'XT', 'XU', 'XV', 'XW', 'XX', 'XY', 'XZ', 'ZJ', 'YB', 'YC'], formValues, change);

  useCompute('YD', spm, ['VV', 'WR', 'XK'], formValues, change);
  useCompute('YE', spm, ['VW', 'WS', 'XL'], formValues, change);
  useCompute('YF', spm, ['VX', 'WT', 'XM'], formValues, change);
  useCompute('ZD', spm, ['ZA', 'ZB', 'ZC'], formValues, change);
  useCompute('YG', spm, ['VY', 'WU', 'XN'], formValues, change);
  useCompute('YH', spm, ['VZ', 'WV', 'XP'], formValues, change);
  useCompute('YJ', spm, ['WA', 'WW', 'XQ'], formValues, change);
  useCompute('YK', spm, ['WB', 'WX', 'XR'], formValues, change);

  useCompute('WC', sum, ['VV', 'VW', 'VX', 'ZA', 'VY', 'VZ', 'WA', 'WB'], formValues, change);
  useCompute('TY', sum, ['WR', 'WS', 'WT', 'ZB', 'WU', 'WV', 'WW', 'WX'], formValues, change);
  useCompute('TZ', sum, ['XK', 'XL', 'XM', 'ZC', 'XN', 'XP', 'XQ', 'XR'], formValues, change);
  useCompute('UA', sum, ['YD', 'YE', 'YF', 'ZD', 'YG', 'YH', 'YJ', 'YK'], formValues, change);

  useCompute('WD', sum, ['VG', 'VU', 'WC'], formValues, change);
  useCompute('UB', sum, ['TS', 'TV', 'TY'], formValues, change);
  useCompute('UC', sum, ['TT', 'TW', 'TZ'], formValues, change);
  useCompute('UD', sum, ['TU', 'TX', 'UA'], formValues, change);

  // Rendering
  return (
    <div className="tax2056">
      <div />
      <div className="pdf-page" id="pdf-page-1">
        <div className="pdf-page-inner pdf-page-1">
          <div className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 529, top: 273, width: 18, height: 15, fontSize: 21 }}>
              <span>TA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 528, top: 309, width: 19, height: 15, fontSize: 21 }}>
              <span>TD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 528, top: 344, width: 19, height: 15, fontSize: 21 }}>
              <span>TG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 526, top: 379, width: 21, height: 15, fontSize: 21 }}>
              <span>TM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 528, top: 415, width: 17, height: 16, fontSize: 21 }}>
              <span>D4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 531, top: 453, width: 14, height: 14, fontSize: 21 }}>
              <span>IK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 489, width: 17, height: 14, fontSize: 21 }}>
              <span>TP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 530, top: 525, width: 15, height: 14, fontSize: 21 }}>
              <span>TS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 272, width: 17, height: 15, fontSize: 21 }}>
              <span>TB</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 308, width: 17, height: 15, fontSize: 21 }}>
              <span>TE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 745, top: 345, width: 19, height: 15, fontSize: 21 }}>
              <span>TH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 745, top: 381, width: 19, height: 14, fontSize: 21 }}>
              <span>TN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 417, width: 17, height: 15, fontSize: 21 }}>
              <span>D5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 749, top: 453, width: 12, height: 14, fontSize: 21 }}>
              <span>IL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 745, top: 489, width: 19, height: 18, fontSize: 21 }}>
              <span>TQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 525, width: 17, height: 14, fontSize: 21 }}>
              <span>TT</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 272, width: 18, height: 15, fontSize: 21 }}>
              <span>TC</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 963, top: 308, width: 16, height: 15, fontSize: 21 }}>
              <span>TF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 964, top: 345, width: 14, height: 15, fontSize: 21 }}>
              <span>TI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 381, width: 20, height: 14, fontSize: 21 }}>
              <span>TO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 415, width: 18, height: 16, fontSize: 21 }}>
              <span>D6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 963, top: 453, width: 16, height: 14, fontSize: 21 }}>
              <span>IM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 489, width: 18, height: 14, fontSize: 21 }}>
              <span>TR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 525, width: 18, height: 14, fontSize: 21 }}>
              <span>TU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 265, width: 207, height: 16, fontSize: 17 }}>
              <span>Provisions pour reconstitution des</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 279, width: 194, height: 16, fontSize: 17 }}>
              <span>gisements miniers et pétroliers *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 302, width: 188, height: 15, fontSize: 17 }}>
              <span>Provisions pour investissement</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 315, width: 112, height: 14, fontSize: 17 }}>
              <span>(art. 237 bis A-II) *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 341, width: 225, height: 16, fontSize: 17 }}>
              <span>Provisions pour hausse des prix (1) *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 377, width: 175, height: 16, fontSize: 17 }}>
              <span>Amortissements dérogatoires</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 406, width: 204, height: 16, fontSize: 17 }}>
              <span>Dont majorations exceptionnelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 420, width: 49, height: 14, fontSize: 17 }}>
              <span>de 30 %</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 381, width: 16, height: 15, fontSize: 21 }}>
              <span>3X</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 417, width: 17, height: 15, fontSize: 21 }}>
              <span>D3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 446, width: 210, height: 16, fontSize: 17 }}>
              <span>Provisions pour prêts d’installation</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 460, width: 175, height: 16, fontSize: 17 }}>
              <span>(art. 39 quinquies H du CGI)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 316, top: 453, width: 9, height: 18, fontSize: 21 }}>
              <span>IJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 489, width: 212, height: 16, fontSize: 17 }}>
              <span>Autres provisions réglementées (1)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 490, width: 17, height: 15, fontSize: 21 }}>
              <span>3Y</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 205, top: 525, width: 80, height: 13, fontSize: 17 }}>
              <span>TOTAL I</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 525, width: 16, height: 16, fontSize: 21 }}>
              <span>3Z</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 561, width: 134, height: 16, fontSize: 17 }}>
              <span>Provisions pour litiges</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 560, width: 16, height: 16, fontSize: 21 }}>
              <span>4A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 560, width: 16, height: 16, fontSize: 21 }}>
              <span>4B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 560, width: 16, height: 16, fontSize: 21 }}>
              <span>4C</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 560, width: 17, height: 16, fontSize: 21 }}>
              <span>4D</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 590, width: 210, height: 16, fontSize: 17 }}>
              <span>Provisions pour garanties données</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 604, width: 64, height: 13, fontSize: 17 }}>
              <span>aux clients</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 596, width: 15, height: 16, fontSize: 21 }}>
              <span>4E</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 530, top: 596, width: 15, height: 16, fontSize: 21 }}>
              <span>4F</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 745, top: 596, width: 18, height: 16, fontSize: 21 }}>
              <span>4G</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 596, width: 17, height: 16, fontSize: 21 }}>
              <span>4H</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 626, width: 214, height: 16, fontSize: 17 }}>
              <span>Provisions pour pertes sur marchés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 641, width: 45, height: 12, fontSize: 17 }}>
              <span>à terme</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 315, top: 632, width: 12, height: 19, fontSize: 21 }}>
              <span>4J</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 632, width: 18, height: 16, fontSize: 21 }}>
              <span>4K</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 747, top: 632, width: 15, height: 16, fontSize: 21 }}>
              <span>4L</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 632, width: 19, height: 16, fontSize: 21 }}>
              <span>4M</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 670, width: 228, height: 16, fontSize: 17 }}>
              <span>Provisions pour amendes et pénalités</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 669, width: 18, height: 15, fontSize: 21 }}>
              <span>4N</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 530, top: 669, width: 15, height: 15, fontSize: 21 }}>
              <span>4P</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 669, width: 17, height: 15, fontSize: 21 }}>
              <span>4R</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 964, top: 669, width: 13, height: 15, fontSize: 21 }}>
              <span>4S</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 706, width: 204, height: 16, fontSize: 17 }}>
              <span>Provisions pour pertes de change</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 705, width: 16, height: 15, fontSize: 21 }}>
              <span>4T</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 705, width: 17, height: 15, fontSize: 21 }}>
              <span>4U</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 705, width: 17, height: 15, fontSize: 21 }}>
              <span>4V</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 705, width: 22, height: 15, fontSize: 21 }}>
              <span>4W</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 735, width: 215, height: 16, fontSize: 17 }}>
              <span>Provisions pour pensions et obliga-</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 749, width: 90, height: 12, fontSize: 17 }}>
              <span>tions similaires</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 741, width: 17, height: 15, fontSize: 21 }}>
              <span>4X</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 741, width: 17, height: 15, fontSize: 21 }}>
              <span>4Y</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 741, width: 16, height: 15, fontSize: 21 }}>
              <span>4Z</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 963, top: 742, width: 16, height: 15, fontSize: 21 }}>
              <span>5A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 778, width: 162, height: 16, fontSize: 17 }}>
              <span>Provisions pour impôts (1)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 778, width: 16, height: 15, fontSize: 21 }}>
              <span>5B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 778, width: 17, height: 15, fontSize: 21 }}>
              <span>5C</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 778, width: 17, height: 15, fontSize: 21 }}>
              <span>5D</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 964, top: 778, width: 14, height: 15, fontSize: 21 }}>
              <span>5E</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 807, width: 219, height: 16, fontSize: 17 }}>
              <span>Provisions pour renouvellement des</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 821, width: 104, height: 13, fontSize: 17 }}>
              <span>immobilisations *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 314, top: 814, width: 14, height: 15, fontSize: 21 }}>
              <span>5F</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 814, width: 17, height: 15, fontSize: 21 }}>
              <span>5H</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 748, top: 814, width: 12, height: 18, fontSize: 21 }}>
              <span>5J</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 814, width: 18, height: 15, fontSize: 21 }}>
              <span>5K</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 844, width: 204, height: 15, fontSize: 17 }}>
              <span>Provisions pour gros entretien et</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 857, width: 107, height: 16, fontSize: 17 }}>
              <span>grandes révisions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 851, width: 18, height: 13, fontSize: 21 }}>
              <span>EO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 530, top: 851, width: 15, height: 13, fontSize: 21 }}>
              <span>EP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 745, top: 851, width: 18, height: 17, fontSize: 21 }}>
              <span>EQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 963, top: 851, width: 16, height: 13, fontSize: 21 }}>
              <span>ER</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 879, width: 213, height: 16, fontSize: 17 }}>
              <span>Provisions pour charges sociales et</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 893, width: 172, height: 16, fontSize: 17 }}>
              <span>fiscales sur congés à payer *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 887, width: 16, height: 15, fontSize: 21 }}>
              <span>5R</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 531, top: 887, width: 13, height: 15, fontSize: 21 }}>
              <span>5S</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 747, top: 886, width: 15, height: 16, fontSize: 21 }}>
              <span>5T</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 963, top: 887, width: 16, height: 15, fontSize: 21 }}>
              <span>5U</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 916, width: 202, height: 15, fontSize: 17 }}>
              <span>Autres provisions pour risques et</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 929, width: 67, height: 16, fontSize: 17 }}>
              <span>charges (1)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 923, width: 17, height: 15, fontSize: 21 }}>
              <span>5V</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 527, top: 923, width: 21, height: 15, fontSize: 21 }}>
              <span>5W</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 923, width: 17, height: 15, fontSize: 21 }}>
              <span>5X</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 963, top: 923, width: 17, height: 15, fontSize: 21 }}>
              <span>5Y</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 993, width: 16, height: 16, fontSize: 21 }}>
              <span>6A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 993, width: 16, height: 16, fontSize: 21 }}>
              <span>6B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 993, width: 16, height: 16, fontSize: 21 }}>
              <span>6C</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 993, width: 17, height: 16, fontSize: 21 }}>
              <span>6D</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 1029, width: 15, height: 16, fontSize: 21 }}>
              <span>6E</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 530, top: 1029, width: 15, height: 16, fontSize: 21 }}>
              <span>6F</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 745, top: 1029, width: 18, height: 16, fontSize: 21 }}>
              <span>6G</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 1029, width: 17, height: 16, fontSize: 21 }}>
              <span>6H</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 1066, width: 17, height: 16, fontSize: 21 }}>
              <span>0/2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 1066, width: 16, height: 17, fontSize: 21 }}>
              <span>0/3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 1066, width: 16, height: 16, fontSize: 21 }}>
              <span>0/4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 1066, width: 17, height: 16, fontSize: 21 }}>
              <span>0/5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 1103, width: 18, height: 16, fontSize: 21 }}>
              <span>9U</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 1103, width: 17, height: 16, fontSize: 21 }}>
              <span>9V</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 744, top: 1103, width: 21, height: 16, fontSize: 21 }}>
              <span>9W</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 1103, width: 17, height: 16, fontSize: 21 }}>
              <span>9X</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 1137, width: 17, height: 17, fontSize: 21 }}>
              <span>0/6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 1138, width: 17, height: 16, fontSize: 21 }}>
              <span>0/7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 1138, width: 17, height: 16, fontSize: 21 }}>
              <span>0/8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 1138, width: 17, height: 17, fontSize: 21 }}>
              <span>0/9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 1175, width: 136, height: 12, fontSize: 17 }}>
              <span>Sur stocks et en cours</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 1174, width: 18, height: 16, fontSize: 21 }}>
              <span>6N</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 530, top: 1174, width: 15, height: 16, fontSize: 21 }}>
              <span>6P</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 1174, width: 17, height: 16, fontSize: 21 }}>
              <span>6R</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 964, top: 1174, width: 13, height: 16, fontSize: 21 }}>
              <span>6S</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 1211, width: 120, height: 16, fontSize: 17 }}>
              <span>Sur comptes clients</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 1210, width: 16, height: 16, fontSize: 21 }}>
              <span>6T</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 1210, width: 17, height: 16, fontSize: 21 }}>
              <span>6U</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 1210, width: 17, height: 16, fontSize: 21 }}>
              <span>6V</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 1210, width: 22, height: 16, fontSize: 21 }}>
              <span>6W</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 1240, width: 143, height: 15, fontSize: 17 }}>
              <span>Autres provisions pour</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 1255, width: 104, height: 16, fontSize: 17 }}>
              <span>dépréciation (1)*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 1246, width: 17, height: 16, fontSize: 21 }}>
              <span>6X</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 1246, width: 17, height: 16, fontSize: 21 }}>
              <span>6Y</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 746, top: 1246, width: 16, height: 16, fontSize: 21 }}>
              <span>6Z</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 1248, width: 17, height: 15, fontSize: 21 }}>
              <span>7A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 1284, width: 16, height: 15, fontSize: 21 }}>
              <span>7B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 1320, width: 17, height: 15, fontSize: 21 }}>
              <span>7C</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 528, top: 1283, width: 19, height: 15, fontSize: 21 }}>
              <span>TY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 528, top: 1320, width: 18, height: 14, fontSize: 21 }}>
              <span>UB</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 529, top: 1356, width: 17, height: 14, fontSize: 21 }}>
              <span>UE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 527, top: 1392, width: 20, height: 14, fontSize: 21 }}>
              <span>UG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 530, top: 1428, width: 15, height: 17, fontSize: 21 }}>
              <span>UJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 205, top: 958, width: 80, height: 13, fontSize: 17 }}>
              <span>TOTAL II</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 205, top: 1283, width: 80, height: 13, fontSize: 17 }}>
              <span>TOTAL III</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 1316, width: 213, height: 18, fontSize: 17 }}>
              <span>TOTAL GÉNÉRAL (I + II + III)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 958, width: 16, height: 16, fontSize: 21 }}>
              <span>5Z</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 528, top: 958, width: 18, height: 15, fontSize: 21 }}>
              <span>TV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 743, top: 958, width: 22, height: 15, fontSize: 21 }}>
              <span>TW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 745, top: 1283, width: 17, height: 15, fontSize: 21 }}>
              <span>TZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 744, top: 1320, width: 19, height: 14, fontSize: 21 }}>
              <span>UC</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 745, top: 1356, width: 17, height: 14, fontSize: 21 }}>
              <span>UF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 744, top: 1392, width: 20, height: 14, fontSize: 21 }}>
              <span>UH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 744, top: 1428, width: 20, height: 14, fontSize: 21 }}>
              <span>UK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 1284, width: 19, height: 14, fontSize: 21 }}>
              <span>UA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 1320, width: 20, height: 14, fontSize: 21 }}>
              <span>UD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 958, width: 18, height: 15, fontSize: 21 }}>
              <span>TX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 994, width: 75, height: 16, fontSize: 17 }}>
              <span>- incorporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 1030, width: 65, height: 16, fontSize: 17 }}>
              <span>- corporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 1060, width: 58, height: 12, fontSize: 17 }}>
              <span>- titres mis</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 184, top: 1073, width: 84, height: 17, fontSize: 17 }}>
              <span>en équivalence</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 1103, width: 118, height: 16, fontSize: 17 }}>
              <span>- titres de participation</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 1132, width: 109, height: 12, fontSize: 17 }}>
              <span>- autres immobilisa-</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 184, top: 1146, width: 114, height: 14, fontSize: 17 }}>
              <span>tions financières (1)*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 323, top: 1357, width: 96, height: 16, fontSize: 17 }}>
              <span>- d’exploitation</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 323, top: 1393, width: 79, height: 13, fontSize: 17 }}>
              <span>- financières</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 323, top: 1429, width: 106, height: 17, fontSize: 17 }}>
              <span>- exceptionnelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 1386, width: 92, height: 12, fontSize: 17 }}>
              <span>Dont dotations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 1401, width: 63, height: 15, fontSize: 17 }}>
              <span>et reprises</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 333, width: 13, height: 571, fontSize: 15 }}>
              <span>Provisions réglementéesProvisions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1094, width: 14, height: 109, fontSize: 15 }}>
              <span>pour dépréciation</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 700, width: 14, height: 568, fontSize: 15 }}>
              <span>pour risques et chargesProvisions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 1059, width: 16, height: 8, fontSize: 17 }}>
              <span>sur</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 1079, width: 81, height: 12, fontSize: 17 }}>
              <span>immobilisations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 988, width: 11, height: 169, fontSize: 211 }}>
              <span>&lbrace;</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 304, top: 1350, width: 9, height: 97, fontSize: 121 }}>
              <span>&lbrace;</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 357, top: 1599, width: 493, height: 14, fontSize: 15 }}>
              <span>* Des explications concernant cette rubrique sont données dans la notice n° 2032</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1464, width: 784, height: 16, fontSize: 17 }}>
              <span>Titres mis en équivalence : montant de la dépréciation à la clôture de l’exercice calculé selon les règles prévues à l’article 39-1-5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 822, top: 1466, width: 5, height: 6, fontSize: 13 }}>
              <span>e</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 831, top: 1464, width: 55, height: 12, fontSize: 17 }}>
              <span>du C.G.I.</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 964, top: 1464, width: 13, height: 14, fontSize: 21 }}>
              <span>10</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 1518, width: 667, height: 16, fontSize: 17 }}>
              <span>(1) à détailler sur feuillet séparé selon l’année de constitution de la provision ou selon l’objet de la provision.</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 1541, width: 1121, height: 14, fontSize: 15 }}>
              <span>NOTA : Les charges à payer ne doivent pas être mentionnées sur ce tableau mais être ventilées sur l’état détaillé des charges à payer dont la production est prévue par l’article 38 II</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 1557, width: 139, height: 11, fontSize: 15 }}>
              <span>de l’annexe III au CGI.</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 361, top: 206, width: 107, height: 12, fontSize: 15 }}>
              <span>Montant au début</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 376, top: 221, width: 76, height: 10, fontSize: 15 }}>
              <span>de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 412, top: 246, width: 5, height: 9, fontSize: 13 }}>
              <span>1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 207, width: 121, height: 11, fontSize: 15 }}>
              <span>AUGMENTATIONS :</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 561, top: 221, width: 139, height: 10, fontSize: 15 }}>
              <span>Dotations de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 628, top: 246, width: 6, height: 9, fontSize: 13 }}>
              <span>2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 799, top: 207, width: 96, height: 11, fontSize: 15 }}>
              <span>DIMINUTIONS :</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 782, top: 221, width: 131, height: 13, fontSize: 15 }}>
              <span>Reprises de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 844, top: 246, width: 6, height: 10, fontSize: 13 }}>
              <span>3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1039, top: 207, width: 50, height: 10, fontSize: 15 }}>
              <span>Montant</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1004, top: 220, width: 120, height: 11, fontSize: 15 }}>
              <span>à la fin de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1061, top: 246, width: 6, height: 9, fontSize: 13 }}>
              <span>4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 220, width: 130, height: 14, fontSize: 15 }}>
              <span>Nature des provisions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 81, width: 388, height: 18, fontSize: 25 }}>
              <span>PROVISIONS INSCRITES AU BILAN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 110, width: 182, height: 12, fontSize: 13 }}>
              <span>Formulaire obligatoire (article 53 A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 50, top: 123, width: 150, height: 13, fontSize: 13 }}>
              <span>du Code général des impôts)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 913, top: 77, width: 220, height: 18, fontSize: 21 }}>
              <span>DGFiP N° 2056-SD 2019</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 158, width: 185, height: 16, fontSize: 17 }}>
              <span>Désignation de l’entreprise</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 347, top: 78, width: 15, height: 23, fontSize: 31 }}>
              <span>7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 309, width: 17, height: 16, fontSize: 21 }}>
              <span>3U</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 272, width: 16, height: 16, fontSize: 21 }}>
              <span>3T</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 313, top: 346, width: 17, height: 15, fontSize: 21 }}>
              <span>3V</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1091, top: 161, width: 41, height: 12, fontSize: 17 }}>
              <span>Néant</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1162, top: 153, width: 5, height: 6, fontSize: 17 }}>
              <span>*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 16, top: 1209, width: 11, height: 264, fontSize: 13 }}>
              <span>N° 2056- SD – (SDNC-DGFiP) - Novembre 2018</span>
            </div>
          </div>
          <div className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot pdf-obj-0-1 pdf-obj-fixed acroform-field" name="ZE" type="checkbox" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-2 pdf-obj-fixed acroform-field" name="VA" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-3 pdf-obj-fixed acroform-field" name="TA"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-4 pdf-obj-fixed acroform-field" name="TB"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-5 pdf-obj-fixed acroform-field" name="TC" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-6 pdf-obj-fixed acroform-field" name="VB" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-7 pdf-obj-fixed acroform-field" name="VF"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-8 pdf-obj-fixed acroform-field" name="IJ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-9 pdf-obj-fixed acroform-field" name="AA"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-10 pdf-obj-fixed acroform-field" name="VE"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-11 pdf-obj-fixed acroform-field" name="VC"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-12 pdf-obj-fixed acroform-field" name="YL"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-13 pdf-obj-fixed acroform-field" name="UJ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-14 pdf-obj-fixed acroform-field" name="UK"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-15 pdf-obj-fixed acroform-field" name="UG"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-16 pdf-obj-fixed acroform-field" name="UH"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-17 pdf-obj-fixed acroform-field" name="UE"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-18 pdf-obj-fixed acroform-field" name="UF"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-19 pdf-obj-fixed acroform-field" name="WD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-20 pdf-obj-fixed acroform-field" name="UB" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-21 pdf-obj-fixed acroform-field" name="UC" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-22 pdf-obj-fixed acroform-field" name="UD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-23 pdf-obj-fixed acroform-field" name="WC" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-24 pdf-obj-fixed acroform-field" name="TY" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-25 pdf-obj-fixed acroform-field" name="TZ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-26 pdf-obj-fixed acroform-field" name="UA" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-27 pdf-obj-fixed acroform-field" name="WB"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-28 pdf-obj-fixed acroform-field" name="WX"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-29 pdf-obj-fixed acroform-field" name="XR"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-30 pdf-obj-fixed acroform-field" name="YK" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-31 pdf-obj-fixed acroform-field" name="WA"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-32 pdf-obj-fixed acroform-field" name="WW"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-33 pdf-obj-fixed acroform-field" name="XQ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-34 pdf-obj-fixed acroform-field" name="YJ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-35 pdf-obj-fixed acroform-field" name="VZ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-36 pdf-obj-fixed acroform-field" name="WV"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-37 pdf-obj-fixed acroform-field" name="XP"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-38 pdf-obj-fixed acroform-field" name="YH" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-39 pdf-obj-fixed acroform-field" name="VY"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-40 pdf-obj-fixed acroform-field" name="ZA"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-41 pdf-obj-fixed acroform-field" name="VX"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-42 pdf-obj-fixed acroform-field" name="VW"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-43 pdf-obj-fixed acroform-field" name="VV"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-44 pdf-obj-fixed acroform-field" name="VU" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-45 pdf-obj-fixed acroform-field" name="VT"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-46 pdf-obj-fixed acroform-field" name="VS"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-47 pdf-obj-fixed acroform-field" name="ZF"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-48 pdf-obj-fixed acroform-field" name="VQ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-49 pdf-obj-fixed acroform-field" name="VP"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-50 pdf-obj-fixed acroform-field" name="VN"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-51 pdf-obj-fixed acroform-field" name="VM"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-52 pdf-obj-fixed acroform-field" name="VL"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-53 pdf-obj-fixed acroform-field" name="VK"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-54 pdf-obj-fixed acroform-field" name="VJ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-55 pdf-obj-fixed acroform-field" name="VH"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-56 pdf-obj-fixed acroform-field" name="VG" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-57 pdf-obj-fixed acroform-field" name="WU"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-58 pdf-obj-fixed acroform-field" name="ZB"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-59 pdf-obj-fixed acroform-field" name="WT"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-60 pdf-obj-fixed acroform-field" name="WS"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-61 pdf-obj-fixed acroform-field" name="WR"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-62 pdf-obj-fixed acroform-field" name="TV" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-63 pdf-obj-fixed acroform-field" name="WQ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-64 pdf-obj-fixed acroform-field" name="WP"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-65 pdf-obj-fixed acroform-field" name="ZG"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-66 pdf-obj-fixed acroform-field" name="WM"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-67 pdf-obj-fixed acroform-field" name="WL"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-68 pdf-obj-fixed acroform-field" name="WK"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-69 pdf-obj-fixed acroform-field" name="WJ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-70 pdf-obj-fixed acroform-field" name="WH"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-71 pdf-obj-fixed acroform-field" name="WG"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-72 pdf-obj-fixed acroform-field" name="WF"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-73 pdf-obj-fixed acroform-field" name="WE"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-74 pdf-obj-fixed acroform-field" name="TS" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-75 pdf-obj-fixed acroform-field" name="TP"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-76 pdf-obj-fixed acroform-field" name="IK"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-77 pdf-obj-fixed acroform-field" name="AB"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-78 pdf-obj-fixed acroform-field" name="TM"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-79 pdf-obj-fixed acroform-field" name="TG"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-80 pdf-obj-fixed acroform-field" name="TD"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-81 pdf-obj-fixed acroform-field" name="XN"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-82 pdf-obj-fixed acroform-field" name="ZC"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-83 pdf-obj-fixed acroform-field" name="XM"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-84 pdf-obj-fixed acroform-field" name="XL"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-85 pdf-obj-fixed acroform-field" name="XK"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-86 pdf-obj-fixed acroform-field" name="TW" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-87 pdf-obj-fixed acroform-field" name="XJ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-88 pdf-obj-fixed acroform-field" name="XH"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-89 pdf-obj-fixed acroform-field" name="ZH"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-90 pdf-obj-fixed acroform-field" name="XF"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-91 pdf-obj-fixed acroform-field" name="XE"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-92 pdf-obj-fixed acroform-field" name="XD"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-93 pdf-obj-fixed acroform-field" name="XC"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-94 pdf-obj-fixed acroform-field" name="XB"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-95 pdf-obj-fixed acroform-field" name="XA"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-96 pdf-obj-fixed acroform-field" name="WZ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-97 pdf-obj-fixed acroform-field" name="WY"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-98 pdf-obj-fixed acroform-field" name="TT" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-99 pdf-obj-fixed acroform-field" name="TQ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-100 pdf-obj-fixed acroform-field" name="IL"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-101 pdf-obj-fixed acroform-field" name="AC"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-102 pdf-obj-fixed acroform-field" name="TN"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-103 pdf-obj-fixed acroform-field" name="TH"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-104 pdf-obj-fixed acroform-field" name="TE"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-105 pdf-obj-fixed acroform-field" name="YG" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-106 pdf-obj-fixed acroform-field" name="ZD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-107 pdf-obj-fixed acroform-field" name="YF" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-108 pdf-obj-fixed acroform-field" name="YE" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-109 pdf-obj-fixed acroform-field" name="YD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-110 pdf-obj-fixed acroform-field" name="TX" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-111 pdf-obj-fixed acroform-field" name="YC" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-112 pdf-obj-fixed acroform-field" name="YB" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-113 pdf-obj-fixed acroform-field" name="ZJ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-114 pdf-obj-fixed acroform-field" name="XZ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-115 pdf-obj-fixed acroform-field" name="XY" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-116 pdf-obj-fixed acroform-field" name="XX" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-117 pdf-obj-fixed acroform-field" name="XW" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-118 pdf-obj-fixed acroform-field" name="XV" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-119 pdf-obj-fixed acroform-field" name="XU" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-120 pdf-obj-fixed acroform-field" name="XT" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-121 pdf-obj-fixed acroform-field" name="XS" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-122 pdf-obj-fixed acroform-field" name="TU" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-123 pdf-obj-fixed acroform-field" name="TR" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-124 pdf-obj-fixed acroform-field" name="IM" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-125 pdf-obj-fixed acroform-field" name="AD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-126 pdf-obj-fixed acroform-field" name="TO" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-127 pdf-obj-fixed acroform-field" name="TI" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-128 pdf-obj-fixed acroform-field" name="TF" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="17269448" data-annot-id="16570368" type="text" disabled />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2056;
