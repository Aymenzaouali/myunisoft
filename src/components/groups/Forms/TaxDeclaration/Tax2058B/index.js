import React from 'react';
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {parse as p, sum, useCompute, setValue, setIsNothingness} from 'helpers/pdfforms';
import { get as _get } from 'lodash';

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const notNeededFields = {
  'NOM_STEA': true,
  'ZX': true
};

const diff = (values, fields) => {
  return p(values[fields[0]]) - p(values[fields[1]]);
};

const Tax2058B = (props) => {
  const { taxDeclarationForm, form2058A, change } = props;
  const XO = _get(form2058A, 'XO', null);

  setIsNothingness(taxDeclarationForm, notNeededFields, 'ZX', change);

  useCompute('DR', diff, ['DQ', 'DP'], taxDeclarationForm, change);
  useCompute('YK', sum, ['DR', 'YJ'], taxDeclarationForm, change);
  useCompute('YN', sum, ['ZV', 'CA', 'CD', 'CG'], taxDeclarationForm, change);
  useCompute('YO', sum, ['ZW', 'DA', 'DD', 'DG'], taxDeclarationForm, change);

  setValue(taxDeclarationForm, 'YJ', XO, change);

  return (
    <div className="form-tax-declaration-2058b">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 255, top: 83, width: 36, height: 36, 'font-size': 47, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 327, top: 78, width: 554, height: 25, 'font-size': 25, }}>
<span>
DÉFICITS, INDEMNITÉS POUR CONGÉS À PAYER
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 397, top: 107, width: 414, height: 21, 'font-size': 25, }}>
<span>
ET PROVISIONS NON DÉDUCTIBLES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 949, top: 102, width: 57, height: 14, 'font-size': 21, }}>
<span>
DGFiP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1011, top: 99, width: 163, height: 18, 'font-size': 25, }}>
<span>
N° 2058-B-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 21, top: 1172, width: 13, height: 268, 'font-size': 13, }}>
<span>
N° 2058-B-SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 130, width: 173, height: 11, 'font-size': 12, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 142, width: 143, height: 11, 'font-size': 12, }}>
<span>
du Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 55, top: 233, width: 193, height: 16, 'font-size': 17, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 254, top: 245, width: 796, height: 2, 'font-size': 13, }}>
<span>
____________________________________________________________________________________________________________________________________
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1075, top: 237, width: 41, height: 12, 'font-size': 17, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1149, top: 235, width: 7, height: 8, 'font-size': 23, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 280, width: 237, height: 19, 'font-size': 21, }}>
<span>
I. SUIVI DES DÉFICITS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 322, width: 419, height: 16, 'font-size': 17, }}>
<span>
Déficits restant à reporter au titre de l’exercice précédent (1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 322, width: 19, height: 12, 'font-size': 17, }}>
<span>
K4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 356, width: 400, height: 16, 'font-size': 17, }}>
<span>
Déficits imputés (total lignes XB et XL du tableau 2058-A)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 357, width: 19, height: 12, 'font-size': 17, }}>
<span>
K5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 391, width: 266, height: 16, 'font-size': 17, }}>
<span>
Déficits reportables (différence K4-K5)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 391, width: 19, height: 13, 'font-size': 17, }}>
<span>
K6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 428, width: 321, height: 16, 'font-size': 17, }}>
<span>
Déficit de l’exercice (tableau 2058A, ligne XO)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 429, width: 17, height: 14, 'font-size': 17, }}>
<span>
YJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 463, width: 355, height: 17, 'font-size': 17, }}>
<span>
Total des déficits restant à reporter (somme K6+YJ)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 465, width: 22, height: 11, 'font-size': 17, }}>
<span>
YK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 494, width: 985, height: 23, 'font-size': 21, }}>
<span>
II. INDEMNITÉS POUR CONGÉS À PAYER, CHARGES SOCIALES ET FISCALES CORRESPONDANTES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 94, top: 529, width: 665, height: 16, 'font-size': 17, }}>
<span>
Montant déductible correspondant aux droits acquis par les salariés pour les entreprises placées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 543, width: 521, height: 16, 'font-size': 17, }}>
<span>
sous le régime de l’article 39-1. 1e bis Al. 1er du CGI, dotations de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 536, width: 18, height: 12, 'font-size': 17, }}>
<span>
ZT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 567, width: 902, height: 23, 'font-size': 21, }}>
<span>
III. PROVISIONS ET CHARGES À PAYER, NON DÉDUCTIBLES POUR L’ASSIETTE DE L’IMPÔT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 610, width: 181, height: 14, 'font-size': 15, }}>
<span>
(à détailler sur feuillet séparé)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1001, top: 609, width: 152, height: 16, 'font-size': 17, }}>
<span>
Reprises sur l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 609, width: 158, height: 12, 'font-size': 17, }}>
<span>
Dotations de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 637, width: 651, height: 16, 'font-size': 17, }}>
<span>
Indemnités pour congés à payer, charges sociales et fiscales correspondantes non déductibles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 651, width: 558, height: 16, 'font-size': 17, }}>
<span>
pour les entreprises placées sous le régime de l’article 39-1. 1e bis Al. 2 du CGI *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 645, width: 24, height: 12, 'font-size': 17, }}>
<span>
ZW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 766, top: 645, width: 20, height: 12, 'font-size': 17, }}>
<span>
ZV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 680, width: 248, height: 16, 'font-size': 17, }}>
<span>
Provisions pour risques et charges *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 858, width: 210, height: 16, 'font-size': 17, }}>
<span>
Provisions pour dépréciation *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 1036, width: 112, height: 16, 'font-size': 17, }}>
<span>
Charges à payer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 1182, width: 16, height: 13, 'font-size': 17, }}>
<span>
9L
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1216, width: 23, height: 12, 'font-size': 17, }}>
<span>
YO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 1217, width: 23, height: 11, 'font-size': 17, }}>
<span>
YN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 326, top: 1227, width: 311, height: 14, 'font-size': 17, }}>
<span>
TOTAUX (YN = ZV à 9S) et (YO = ZW à 9T)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 377, top: 1249, width: 206, height: 16, 'font-size': 17, }}>
<span>
à reporter au tableau 2058-A :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 853, top: 1265, width: 59, height: 16, 'font-size': 17, }}>
<span>
ligne WI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1059, top: 1265, width: 65, height: 16, 'font-size': 17, }}>
<span>
ligne WU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 220, top: 1324, width: 570, height: 26, 'font-size': 25, }}>
<span>
CONSÉQUENCES DE LA MÉTHODE PAR COMPOSANTS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 797, top: 1331, width: 207, height: 19, 'font-size': 21, }}>
<span>
(art. 237 septies du CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 94, top: 1390, width: 325, height: 16, 'font-size': 17, }}>
<span>
Montant de la réintégration ou de la déduction
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 600, top: 1363, width: 57, height: 12, 'font-size': 17, }}>
<span>
Montant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 553, top: 1377, width: 152, height: 13, 'font-size': 17, }}>
<span>
au début de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1035, top: 1363, width: 84, height: 12, 'font-size': 17, }}>
<span>
Montant net
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 826, top: 1363, width: 81, height: 15, 'font-size': 17, }}>
<span>
Imputations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1007, top: 1377, width: 139, height: 13, 'font-size': 17, }}>
<span>
à la fin de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 505, top: 1412, width: 15, height: 12, 'font-size': 17, }}>
<span>
L1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1455, width: 8, height: 8, 'font-size': 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 1454, width: 452, height: 14, 'font-size': 15, }}>
<span>
Des explications concernant cette rubrique sont données dans la notice n° 2032.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1474, width: 658, height: 15, 'font-size': 15, }}>
<span>
(1) Cette case correspond au montant porté sur la ligne YK du tableau 2058 B déposé au titre de l’exercice précédent.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 683, top: 821, width: 36, height: 13, 'font-size': 17, }}>
<span>
Total
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 767, top: 823, width: 19, height: 12, 'font-size': 17, }}>
<span>
8X
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 823, width: 20, height: 12, 'font-size': 17, }}>
<span>
8Y
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 767, top: 1003, width: 19, height: 13, 'font-size': 17, }}>
<span>
9D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 982, top: 1003, width: 17, height: 13, 'font-size': 17, }}>
<span>
9E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 687, top: 1002, width: 36, height: 13, 'font-size': 17, }}>
<span>
Total
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 766, top: 1179, width: 19, height: 13, 'font-size': 17, }}>
<span>
9K
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 695, top: 1181, width: 35, height: 12, 'font-size': 17, }}>
<span>
Total
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="ZX" data-field-id="37843384" data-annot-id="37213424" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="DQ" data-field-id="37843592" data-annot-id="37446704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="DS" data-field-id="37844968" data-annot-id="37446896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="DT" data-field-id="37845272" data-annot-id="37447088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="DU" data-field-id="37845608" data-annot-id="37447280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="YN" data-field-id="37845944" data-annot-id="37447472" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="YO" data-field-id="37846280" data-annot-id="37447952" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="CG" data-field-id="37846616" data-annot-id="37448144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="DG" data-field-id="37846952" data-annot-id="37448336" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="CN1" data-field-id="37847432" data-annot-id="37448528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="DN1" data-field-id="37847768" data-annot-id="37448720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="CN2" data-field-id="37848104" data-annot-id="37448912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="CN3" data-field-id="37848440" data-annot-id="37449104" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="DN2" data-field-id="37848776" data-annot-id="37449296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="DN3" data-field-id="37849112" data-annot-id="37449488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="BN1" data-field-id="37849448" data-annot-id="37449680" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="BN2" data-field-id="37849784" data-annot-id="37449872" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="BN3" data-field-id="37850264" data-annot-id="37450336" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="CD" data-field-id="37850600" data-annot-id="37450528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="DD" data-field-id="37850936" data-annot-id="37450720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="BM1" data-field-id="37833000" data-annot-id="37450912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="BM2" data-field-id="37842888" data-annot-id="37451104" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="BM3" data-field-id="37851176" data-annot-id="37451296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="CM3" data-field-id="37851448" data-annot-id="37451488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="CM2" data-field-id="37851752" data-annot-id="37451680" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="CM1" data-field-id="37852088" data-annot-id="37451872" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="DM3" data-field-id="37852424" data-annot-id="37452064" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="DM2" data-field-id="37852760" data-annot-id="37452256" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="DM1" data-field-id="37853096" data-annot-id="37452448" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="CA" data-field-id="37853432" data-annot-id="37452640" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="DA" data-field-id="37853768" data-annot-id="37452832" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="CL3" data-field-id="37854104" data-annot-id="37453024" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="CL2" data-field-id="37854440" data-annot-id="37453216" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="CL1" data-field-id="37850120" data-annot-id="37450064" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="DL3" data-field-id="37855368" data-annot-id="37453936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="DL2" data-field-id="37855704" data-annot-id="37454128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="DL1" data-field-id="37856040" data-annot-id="37454320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="BL3" data-field-id="37856376" data-annot-id="37454512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="BL2" data-field-id="37856712" data-annot-id="37454704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="BL1" data-field-id="37857048" data-annot-id="37454896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="ZV" data-field-id="37857384" data-annot-id="37455088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="ZW" data-field-id="37857720" data-annot-id="37455280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="ZT" data-field-id="37858056" data-annot-id="37455472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="YK" data-field-id="37858392" data-annot-id="37455664" type="text" disabled />

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="YJ" data-field-id="37858728" data-annot-id="37455856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="DR" data-field-id="37859064" data-annot-id="37456048" type="text" disabled />

            <Field component={PdfNumberInput} disabled className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="DP" data-field-id="37859400" data-annot-id="37456240" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="19245336" data-annot-id="18841024" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2058B;
