import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';

import './style.scss';

const Tax2035SUITE = (props) => {
  const { change } = props;
  return (
    <div className="form-tax-declaration-2035-SUITE">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 521, top: 70, width: 116, height: 13, 'font-size': 17, }}>
<span>
REVENUS 2019
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 103, width: 341, height: 15, 'font-size': 17, }}>
<span>
RÉGIME DE LA DÉCLARATION CONTRÔLÉE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 232, width: 202, height: 16, 'font-size': 17, }}>
<span>
Désignation des employeurs
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 862, top: 219, width: 200, height: 16, 'font-size': 17, }}>
<span>
Montant des salaires perçus
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 382, width: 471, height: 16, 'font-size': 17, }}>
<span>
Montant brut des salaires (extrait de la déclaration DADS de 2019)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 429, width: 374, height: 16, 'font-size': 17, }}>
<span>
I – IMMOBILISATIONS ET AMORTISSEMENTS (B)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 840, top: 454, width: 206, height: 13, 'font-size': 17, }}>
<span>
Montant des amortissements
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 829, top: 500, width: 71, height: 13, 'font-size': 17, }}>
<span>
antérieurs
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 987, top: 500, width: 73, height: 13, 'font-size': 17, }}>
<span>
de l&#39;année
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 540, width: 4, height: 9, 'font-size': 13, }}>
<span>
1
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 378, top: 540, width: 7, height: 9, 'font-size': 13, }}>
<span>
2
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 493, top: 540, width: 7, height: 10, 'font-size': 13, }}>
<span>
3
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 608, top: 540, width: 7, height: 9, 'font-size': 13, }}>
<span>
4
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 723, top: 540, width: 7, height: 10, 'font-size': 13, }}>
<span>
5
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 861, top: 540, width: 7, height: 10, 'font-size': 13, }}>
<span>
6
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1020, top: 540, width: 7, height: 9, 'font-size': 13, }}>
<span>
7
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1469, width: 141, height: 16, 'font-size': 17, }}>
<span>
Total du tableau (B)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 920, top: 1537, width: 12, height: 13, 'font-size': 17, }}>
<span>
A
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1563, width: 10, height: 12, 'font-size': 17, }}>
<span>
B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 1587, width: 499, height: 16, 'font-size': 17, }}>
<span>
Dotation nette de l&#39;année à reporter ligne CH de l&#39;annexe 2035 B (A-B)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 96, width: 126, height: 13, 'font-size': 17, }}>
<span>
N° 2035-SD Suite
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1001, top: 114, width: 45, height: 17, 'font-size': 17, }}>
<span>
(2020)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 139, width: 308, height: 15, 'font-size': 17, }}>
<span>
NOM ET PRÉNOMS ou DÉNOMINATION :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 167, width: 77, height: 13, 'font-size': 17, }}>
<span>
N° SIRET :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 190, width: 608, height: 19, 'font-size': 17, }}>
<span>
SERVICES ASSURÉS PAR VOUS de façon régulière et rémunérés par des salaires :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 268, width: 199, height: 19, 'font-size': 17, }}>
<span>
PERSONNEL SALARIÉ (A)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 61, top: 329, width: 181, height: 13, 'font-size': 17, }}>
<span>
Nombre total de salariés :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 326, top: 329, width: 126, height: 16, 'font-size': 17, }}>
<span>
dont handicapés :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 329, width: 109, height: 16, 'font-size': 17, }}>
<span>
dont apprentis :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 335, width: 115, height: 13, 'font-size': 17, }}>
<span>
Société civile de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 837, top: 356, width: 55, height: 13, 'font-size': 17, }}>
<span>
moyen :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 804, top: 372, width: 121, height: 15, 'font-size': 17, }}>
<span>
- quote-part vous
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 823, top: 389, width: 82, height: 13, 'font-size': 17, }}>
<span>
incombant :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 965, top: 320, width: 36, height: 13, 'font-size': 17, }}>
<span>
- des
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 956, top: 338, width: 55, height: 13, 'font-size': 17, }}>
<span>
salariés
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 965, top: 364, width: 36, height: 13, 'font-size': 17, }}>
<span>
- des
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 956, top: 382, width: 55, height: 13, 'font-size': 17, }}>
<span>
salaires
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 969, top: 401, width: 29, height: 12, 'font-size': 17, }}>
<span>
nets
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 94, top: 460, width: 78, height: 13, 'font-size': 17, }}>
<span>
Nature des
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 478, width: 137, height: 16, 'font-size': 17, }}>
<span>
immobilisations (ou
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 496, width: 65, height: 13, 'font-size': 17, }}>
<span>
éléments
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 514, width: 96, height: 16, 'font-size': 17, }}>
<span>
décomposés)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 251, top: 451, width: 33, height: 13, 'font-size': 17, }}>
<span>
Date
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 212, top: 469, width: 109, height: 16, 'font-size': 17, }}>
<span>
d&#39;acquisition ou
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 228, top: 487, width: 77, height: 13, 'font-size': 17, }}>
<span>
de mise en
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 241, top: 505, width: 51, height: 13, 'font-size': 17, }}>
<span>
service
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 214, top: 523, width: 105, height: 16, 'font-size': 17, }}>
<span>
(JJ/MM/AAAA)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 478, width: 101, height: 16, 'font-size': 17, }}>
<span>
Prix total payé
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 496, width: 101, height: 16, 'font-size': 17, }}>
<span>
TVA comprise
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 478, width: 96, height: 13, 'font-size': 17, }}>
<span>
Montant de la
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 452, top: 496, width: 89, height: 13, 'font-size': 17, }}>
<span>
TVA déduite
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 594, top: 469, width: 36, height: 13, 'font-size': 17, }}>
<span>
Base
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 566, top: 487, width: 91, height: 13, 'font-size': 17, }}>
<span>
amortissable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 505, width: 89, height: 13, 'font-size': 17, }}>
<span>
Col 2 – col 3
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 686, top: 472, width: 81, height: 11, 'font-size': 15, }}>
<span>
Mode et taux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 675, top: 488, width: 103, height: 11, 'font-size': 15, }}>
<span>
d&#39;amortissement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 724, top: 503, width: 5, height: 6, 'font-size': 15, }}>
<span>
*
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 66, top: 1411, width: 134, height: 9, 'font-size': 13, }}>
<span>
Fraction d&#39;amortissement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 1424, width: 138, height: 10, 'font-size': 13, }}>
<span>
revenant à l&#39;associé d&#39;une
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 61, top: 1438, width: 144, height: 12, 'font-size': 13, }}>
<span>
société civile de moyen (B)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 1493, width: 143, height: 16, 'font-size': 17, }}>
<span>
Report du total de la
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1511, width: 142, height: 16, 'font-size': 17, }}>
<span>
dernière annexe (B)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1535, width: 496, height: 16, 'font-size': 17, }}>
<span>
Total général ------------------------------------------------------------------------
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1561, width: 835, height: 16, 'font-size': 17, }}>
<span>
Véhicules inscrits au registre des immobilisations : utilisation du barème forfaitaire (B) (cf. cadre 7 de l&#39;annexe 2035B)
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field "
                   name="SIRET_STET" data-field-id="24227288" data-annot-id="22647040" maxLength="14" type="text"
                   disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field "
                   name="NOM_STEA" data-field-id="24227496" data-annot-id="23911280" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field "
                   name="AG1"
                   data-field-id="24258584" data-annot-id="23911504" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field "
                   name="AG3"
                   data-field-id="24258888" data-annot-id="23911696" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field "
                   name="AG4"
                   data-field-id="24259224" data-annot-id="23911888" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field "
                   name="AG2"
                   data-field-id="24259560" data-annot-id="23912080" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field "
                   name="MG"
                   data-field-id="24259896" data-annot-id="23912272" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field "
                   name="AP"
                   data-field-id="24260232" data-annot-id="23912896" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field "
                   name="AN"
                   data-field-id="24260568" data-annot-id="23913088" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field "
                   name="AR"
                   data-field-id="24261048" data-annot-id="23913424" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field "
                   name="AS"
                   data-field-id="24261384" data-annot-id="23913616" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field "
                   name="KA"
                   data-field-id="24261720" data-annot-id="23913808" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field "
                   name="KB"
                   data-field-id="24262056" data-annot-id="23914000" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field "
                   name="CD7"
                   data-field-id="24262392" data-annot-id="23914192" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field "
                   name="BA1"
                   data-field-id="24262728" data-annot-id="23914384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "
                   name="BK1"
                   data-field-id="24263064" data-annot-id="23914576" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="BU1"
                   data-field-id="24263400" data-annot-id="23914768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field "
                   name="CD1"
                   data-field-id="24263880" data-annot-id="23915232" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="CN1"
                   data-field-id="24264216" data-annot-id="23915424" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field "
                   name="KC1"
                   data-field-id="24264552" data-annot-id="23915616" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field "
                   name="CX1"
                   data-field-id="24264888" data-annot-id="23915808" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field "
                   name="DG1"
                   data-field-id="24331240" data-annot-id="23916000" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field "
                   name="DR1"
                   data-field-id="24265224" data-annot-id="23916192" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field "
                   name="BA2"
                   data-field-id="24265560" data-annot-id="23916384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field "
                   name="BK2"
                   data-field-id="24265896" data-annot-id="23916576" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field "
                   name="BU2"
                   data-field-id="24266232" data-annot-id="23916768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field "
                   name="CD2"
                   data-field-id="24266568" data-annot-id="23916960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field "
                   name="CN2"
                   data-field-id="24266904" data-annot-id="23917152" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field "
                   name="KC2"
                   data-field-id="24267240" data-annot-id="23917344" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field "
                   name="CX2"
                   data-field-id="24267576" data-annot-id="23917536" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field "
                   name="DG2"
                   data-field-id="24333240" data-annot-id="23917728" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field "
                   name="DR2"
                   data-field-id="24267912" data-annot-id="23917920" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field "
                   name="BA3"
                   data-field-id="24268248" data-annot-id="23918112" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field "
                   name="BK3"
                   data-field-id="24268584" data-annot-id="23914960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field "
                   name="BU3"
                   data-field-id="24268920" data-annot-id="23918832" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field "
                   name="CD3"
                   data-field-id="24263736" data-annot-id="23919024" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field "
                   name="CN3"
                   data-field-id="24269848" data-annot-id="23919216" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field "
                   name="KC3"
                   data-field-id="24270184" data-annot-id="23919408" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field "
                   name="CX3"
                   data-field-id="24270520" data-annot-id="23919600" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field "
                   name="DG3"
                   data-field-id="24335208" data-annot-id="23919792" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field "
                   name="DR3"
                   data-field-id="24270856" data-annot-id="23919984" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field "
                   name="BA4"
                   data-field-id="24271192" data-annot-id="23920176" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field "
                   name="BA5"
                   data-field-id="24271528" data-annot-id="23920368" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field "
                   name="BK4"
                   data-field-id="24271864" data-annot-id="23920560" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field "
                   name="BK5"
                   data-field-id="24272200" data-annot-id="23920752" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field "
                   name="BU4"
                   data-field-id="24272536" data-annot-id="23920944" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field "
                   name="BU5"
                   data-field-id="24272872" data-annot-id="23921136" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field "
                   name="CD4"
                   data-field-id="24273208" data-annot-id="23921328" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field "
                   name="CD5"
                   data-field-id="24273544" data-annot-id="23921520" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field "
                   name="CN4"
                   data-field-id="24273880" data-annot-id="23921712" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field "
                   name="CN5"
                   data-field-id="24274216" data-annot-id="23921904" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field "
                   name="KC4"
                   data-field-id="24274552" data-annot-id="23922096" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field "
                   name="KC5"
                   data-field-id="24274888" data-annot-id="23922288" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field "
                   name="CX4"
                   data-field-id="24275224" data-annot-id="23922480" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field "
                   name="CX5"
                   data-field-id="24275560" data-annot-id="23922672" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field "
                   name="DG4"
                   data-field-id="24336936" data-annot-id="23922864" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field "
                   name="DG5"
                   data-field-id="24338664" data-annot-id="23923056" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field "
                   name="DR4"
                   data-field-id="24275896" data-annot-id="23923248" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field "
                   name="DR5"
                   data-field-id="24276232" data-annot-id="23923440" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field "
                   name="BA6"
                   data-field-id="24276568" data-annot-id="23923632" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field "
                   name="BA7"
                   data-field-id="24276904" data-annot-id="23923824" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field "
                   name="BK6"
                   data-field-id="24277240" data-annot-id="23924016" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field "
                   name="BK7"
                   data-field-id="24277576" data-annot-id="23924208" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field "
                   name="BU6"
                   data-field-id="24277912" data-annot-id="23924400" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field "
                   name="BU7"
                   data-field-id="24278248" data-annot-id="23924592" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field "
                   name="CD6"
                   data-field-id="24278584" data-annot-id="23918304" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field "
                   name="CN6"
                   data-field-id="24278920" data-annot-id="23918496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field "
                   name="CN7"
                   data-field-id="24279256" data-annot-id="23925824" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field "
                   name="KC6"
                   data-field-id="24279592" data-annot-id="23926016" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field "
                   name="KC7"
                   data-field-id="24279928" data-annot-id="23926208" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field "
                   name="CX6"
                   data-field-id="24269128" data-annot-id="23926400" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field "
                   name="CX7"
                   data-field-id="24269464" data-annot-id="23926592" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field "
                   name="DG6"
                   data-field-id="24340392" data-annot-id="23926784" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field "
                   name="DG7"
                   data-field-id="24342120" data-annot-id="23926976" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field "
                   name="DR6"
                   data-field-id="24281240" data-annot-id="23927168" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field "
                   name="DR7"
                   data-field-id="24281512" data-annot-id="23927360" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field "
                   name="BA8"
                   data-field-id="24281848" data-annot-id="23927552" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field "
                   name="BA9"
                   data-field-id="24282184" data-annot-id="23927744" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field "
                   name="BK8"
                   data-field-id="24282520" data-annot-id="23927936" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field "
                   name="BK9"
                   data-field-id="24282856" data-annot-id="23928128" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field "
                   name="BU8"
                   data-field-id="24283192" data-annot-id="23928320" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field "
                   name="BU9"
                   data-field-id="24283528" data-annot-id="23928512" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field "
                   name="CD8"
                   data-field-id="24283864" data-annot-id="23928704" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field "
                   name="CD9"
                   data-field-id="24284200" data-annot-id="23928896" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field "
                   name="CN8"
                   data-field-id="24284536" data-annot-id="23929088" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field "
                   name="CN9"
                   data-field-id="24284872" data-annot-id="23929280" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field "
                   name="KC8"
                   data-field-id="24285208" data-annot-id="23929472" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field "
                   name="KC9"
                   data-field-id="24285544" data-annot-id="23929664" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field "
                   name="CX8"
                   data-field-id="24285880" data-annot-id="23929856" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field "
                   name="CX9"
                   data-field-id="24286216" data-annot-id="23930048" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field "
                   name="DG8"
                   data-field-id="24343848" data-annot-id="23930240" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field "
                   name="DG9"
                   data-field-id="24345576" data-annot-id="23930432" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field "
                   name="DR8"
                   data-field-id="24286552" data-annot-id="23930624" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field "
                   name="DR9"
                   data-field-id="24286888" data-annot-id="23930816" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field "
                   name="BA10"
                   data-field-id="24287224" data-annot-id="23931008" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field "
                   name="BA11"
                   data-field-id="24287560" data-annot-id="23931200" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field "
                   name="BK10"
                   data-field-id="24287896" data-annot-id="23931392" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field "
                   name="BK11"
                   data-field-id="24288232" data-annot-id="23931584" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field "
                   name="BU10"
                   data-field-id="24288568" data-annot-id="23931776" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field "
                   name="BU11" data-field-id="24288904" data-annot-id="23931968" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field "
                   name="CD10" data-field-id="24289240" data-annot-id="23932160" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field "
                   name="CD11" data-field-id="24289576" data-annot-id="23932352" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field "
                   name="CN10" data-field-id="24289912" data-annot-id="23932544" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field "
                   name="CN11" data-field-id="24290248" data-annot-id="23932736" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field "
                   name="KC10" data-field-id="24290584" data-annot-id="23932928" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field "
                   name="KC11" data-field-id="24290920" data-annot-id="23933120" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field "
                   name="CX10" data-field-id="24291256" data-annot-id="23933312" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field "
                   name="CX11" data-field-id="24291592" data-annot-id="23933504" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field "
                   name="DG10" data-field-id="24347304" data-annot-id="23933696" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field "
                   name="DG11" data-field-id="24349032" data-annot-id="23933888" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field "
                   name="DR10" data-field-id="24291928" data-annot-id="23934080" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field "
                   name="DR11" data-field-id="24292264" data-annot-id="23934272" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field "
                   name="BA12" data-field-id="24292600" data-annot-id="23934464" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field "
                   name="BA13" data-field-id="24292936" data-annot-id="23934656" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field "
                   name="BK12" data-field-id="24293272" data-annot-id="23934848" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field "
                   name="BK13" data-field-id="24293608" data-annot-id="23935040" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field "
                   name="BU12" data-field-id="24293944" data-annot-id="23935232" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field "
                   name="BU13" data-field-id="24294280" data-annot-id="23935424" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field "
                   name="CD12" data-field-id="24294616" data-annot-id="23935616" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field "
                   name="CD13" data-field-id="24294952" data-annot-id="23935808" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field "
                   name="CN12" data-field-id="24295288" data-annot-id="23936000" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field "
                   name="CN13" data-field-id="24295624" data-annot-id="23936192" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field "
                   name="KC12" data-field-id="24295960" data-annot-id="23936384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field "
                   name="KC13" data-field-id="24296296" data-annot-id="23936576" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_125 pdf-obj-fixed acroform-field "
                   name="CX12" data-field-id="24296632" data-annot-id="23936768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_126 pdf-obj-fixed acroform-field "
                   name="CX13" data-field-id="24296968" data-annot-id="23936960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_127 pdf-obj-fixed acroform-field "
                   name="DG12" data-field-id="24350760" data-annot-id="23937152" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_128 pdf-obj-fixed acroform-field "
                   name="DG13" data-field-id="24352488" data-annot-id="23937344" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field "
                   name="DR12" data-field-id="24297304" data-annot-id="23937536" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_130 pdf-obj-fixed acroform-field "
                   name="DR13" data-field-id="24297640" data-annot-id="23924784" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_131 pdf-obj-fixed acroform-field "
                   name="BA14" data-field-id="24297976" data-annot-id="23924976" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_132 pdf-obj-fixed acroform-field "
                   name="BA15" data-field-id="24298312" data-annot-id="23925168" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_133 pdf-obj-fixed acroform-field "
                   name="BK14" data-field-id="24298648" data-annot-id="23925360" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_134 pdf-obj-fixed acroform-field "
                   name="BK15" data-field-id="24298984" data-annot-id="23925552" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_135 pdf-obj-fixed acroform-field "
                   name="BU14" data-field-id="24299320" data-annot-id="23939792" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_136 pdf-obj-fixed acroform-field "
                   name="BU15" data-field-id="24299656" data-annot-id="23939984" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_137 pdf-obj-fixed acroform-field "
                   name="CD14" data-field-id="24299992" data-annot-id="23940176" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_138 pdf-obj-fixed acroform-field "
                   name="CD15" data-field-id="24300328" data-annot-id="23940368" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_139 pdf-obj-fixed acroform-field "
                   name="CN14" data-field-id="24300664" data-annot-id="23940560" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_140 pdf-obj-fixed acroform-field "
                   name="CN15" data-field-id="24301000" data-annot-id="23940752" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_141 pdf-obj-fixed acroform-field "
                   name="KC14" data-field-id="24301336" data-annot-id="23940944" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_142 pdf-obj-fixed acroform-field "
                   name="KC15" data-field-id="24301672" data-annot-id="23941136" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_143 pdf-obj-fixed acroform-field "
                   name="CX14" data-field-id="24280280" data-annot-id="23941328" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_144 pdf-obj-fixed acroform-field "
                   name="CX15" data-field-id="24280616" data-annot-id="23941520" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_145 pdf-obj-fixed acroform-field "
                   name="DG14" data-field-id="24354216" data-annot-id="23941712" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field "
                   name="DG15" data-field-id="24355944" data-annot-id="23941904" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_147 pdf-obj-fixed acroform-field "
                   name="DR14" data-field-id="24280952" data-annot-id="23942096" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_148 pdf-obj-fixed acroform-field "
                   name="DR15" data-field-id="24304072" data-annot-id="23942288" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_149 pdf-obj-fixed acroform-field "
                   name="BA16" data-field-id="24304408" data-annot-id="23942480" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_150 pdf-obj-fixed acroform-field "
                   name="BA17" data-field-id="24304744" data-annot-id="23942672" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_151 pdf-obj-fixed acroform-field "
                   name="BK16" data-field-id="24305080" data-annot-id="23942864" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_152 pdf-obj-fixed acroform-field "
                   name="BK17" data-field-id="24305416" data-annot-id="23943056" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_153 pdf-obj-fixed acroform-field "
                   name="BU16" data-field-id="24305752" data-annot-id="23943248" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_154 pdf-obj-fixed acroform-field "
                   name="BU17" data-field-id="24306088" data-annot-id="23943440" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_155 pdf-obj-fixed acroform-field "
                   name="CD16" data-field-id="24306424" data-annot-id="23943632" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_156 pdf-obj-fixed acroform-field "
                   name="CD17" data-field-id="24306760" data-annot-id="23943824" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_157 pdf-obj-fixed acroform-field "
                   name="CN16" data-field-id="24307096" data-annot-id="23944016" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_158 pdf-obj-fixed acroform-field "
                   name="CN17" data-field-id="24307432" data-annot-id="23944208" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_159 pdf-obj-fixed acroform-field "
                   name="KC16" data-field-id="24307768" data-annot-id="23944400" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_160 pdf-obj-fixed acroform-field "
                   name="KC17" data-field-id="24308104" data-annot-id="23944592" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_161 pdf-obj-fixed acroform-field "
                   name="CX16" data-field-id="24308440" data-annot-id="23944784" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_162 pdf-obj-fixed acroform-field "
                   name="CX17" data-field-id="24308776" data-annot-id="23944976" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_163 pdf-obj-fixed acroform-field "
                   name="DG16" data-field-id="24357672" data-annot-id="23945168" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_164 pdf-obj-fixed acroform-field "
                   name="DG17" data-field-id="24359400" data-annot-id="23945360" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_165 pdf-obj-fixed acroform-field "
                   name="DR16" data-field-id="24309112" data-annot-id="23945552" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_166 pdf-obj-fixed acroform-field "
                   name="DR17" data-field-id="24309448" data-annot-id="23945744" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_167 pdf-obj-fixed acroform-field "
                   name="BA18" data-field-id="24309784" data-annot-id="23945936" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_168 pdf-obj-fixed acroform-field "
                   name="BA19" data-field-id="24310120" data-annot-id="23946128" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_169 pdf-obj-fixed acroform-field "
                   name="BK18" data-field-id="24310456" data-annot-id="23946320" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_170 pdf-obj-fixed acroform-field "
                   name="BK19" data-field-id="24310792" data-annot-id="23946512" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_171 pdf-obj-fixed acroform-field "
                   name="BU18" data-field-id="24311128" data-annot-id="23946704" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_172 pdf-obj-fixed acroform-field "
                   name="BU19" data-field-id="24311464" data-annot-id="23946896" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_173 pdf-obj-fixed acroform-field "
                   name="CD18" data-field-id="24311800" data-annot-id="23947088" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_174 pdf-obj-fixed acroform-field "
                   name="CD19" data-field-id="24312136" data-annot-id="23947280" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_175 pdf-obj-fixed acroform-field "
                   name="CN18" data-field-id="24312472" data-annot-id="23947472" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_176 pdf-obj-fixed acroform-field "
                   name="CN19" data-field-id="24312808" data-annot-id="23947664" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_177 pdf-obj-fixed acroform-field "
                   name="KC18" data-field-id="24313144" data-annot-id="23947856" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_178 pdf-obj-fixed acroform-field "
                   name="KC19" data-field-id="24313480" data-annot-id="23948048" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_179 pdf-obj-fixed acroform-field "
                   name="CX18" data-field-id="24313816" data-annot-id="23948240" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_180 pdf-obj-fixed acroform-field "
                   name="CX19" data-field-id="24314152" data-annot-id="23948432" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_181 pdf-obj-fixed acroform-field "
                   name="DG18" data-field-id="24314488" data-annot-id="23948624" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_182 pdf-obj-fixed acroform-field "
                   name="DG19" data-field-id="24314824" data-annot-id="23948816" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_183 pdf-obj-fixed acroform-field "
                   name="DR18" data-field-id="24315160" data-annot-id="23949008" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_184 pdf-obj-fixed acroform-field "
                   name="DR19" data-field-id="24315496" data-annot-id="23949200" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_185 pdf-obj-fixed acroform-field "
                   name="BA20" data-field-id="24315832" data-annot-id="23949392" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_186 pdf-obj-fixed acroform-field "
                   name="BA21" data-field-id="24316168" data-annot-id="23949584" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_187 pdf-obj-fixed acroform-field "
                   name="BK20" data-field-id="24316504" data-annot-id="23949776" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_188 pdf-obj-fixed acroform-field "
                   name="BK21" data-field-id="24316840" data-annot-id="23949968" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_189 pdf-obj-fixed acroform-field "
                   name="BU20" data-field-id="24317176" data-annot-id="23950160" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_190 pdf-obj-fixed acroform-field "
                   name="BU21" data-field-id="24317512" data-annot-id="23950352" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_191 pdf-obj-fixed acroform-field "
                   name="CD20" data-field-id="24317848" data-annot-id="23950544" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_192 pdf-obj-fixed acroform-field "
                   name="CD21" data-field-id="24318184" data-annot-id="23950736" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_193 pdf-obj-fixed acroform-field "
                   name="CN20" data-field-id="24318520" data-annot-id="23950928" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_194 pdf-obj-fixed acroform-field "
                   name="CN21" data-field-id="24318856" data-annot-id="23951120" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_195 pdf-obj-fixed acroform-field "
                   name="KC20" data-field-id="24319192" data-annot-id="23951312" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_196 pdf-obj-fixed acroform-field "
                   name="KC21" data-field-id="24319528" data-annot-id="23951504" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_197 pdf-obj-fixed acroform-field "
                   name="CX20" data-field-id="24319864" data-annot-id="23951696" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_198 pdf-obj-fixed acroform-field "
                   name="CX21" data-field-id="24320200" data-annot-id="23951888" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_199 pdf-obj-fixed acroform-field "
                   name="DG20" data-field-id="24320536" data-annot-id="23952080" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_200 pdf-obj-fixed acroform-field "
                   name="DG21" data-field-id="24320872" data-annot-id="23952272" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_201 pdf-obj-fixed acroform-field "
                   name="DR20" data-field-id="24321208" data-annot-id="23952464" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_202 pdf-obj-fixed acroform-field "
                   name="DR21" data-field-id="24321544" data-annot-id="23952656" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_203 pdf-obj-fixed acroform-field "
                   name="BA22" data-field-id="24321880" data-annot-id="23952848" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_204 pdf-obj-fixed acroform-field "
                   name="BA23" data-field-id="24322216" data-annot-id="23953040" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_205 pdf-obj-fixed acroform-field "
                   name="BK22" data-field-id="24322552" data-annot-id="23953232" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_206 pdf-obj-fixed acroform-field "
                   name="BK23" data-field-id="24322888" data-annot-id="23953424" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_207 pdf-obj-fixed acroform-field "
                   name="BU22" data-field-id="24323224" data-annot-id="23953616" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_208 pdf-obj-fixed acroform-field "
                   name="BU23" data-field-id="24323560" data-annot-id="23953808" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_209 pdf-obj-fixed acroform-field "
                   name="CD22" data-field-id="24323896" data-annot-id="23954000" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_210 pdf-obj-fixed acroform-field "
                   name="CD23" data-field-id="24324232" data-annot-id="23954192" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_211 pdf-obj-fixed acroform-field "
                   name="CN22" data-field-id="24324568" data-annot-id="23954384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_212 pdf-obj-fixed acroform-field "
                   name="CN23" data-field-id="24324904" data-annot-id="23954576" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_213 pdf-obj-fixed acroform-field "
                   name="KC22" data-field-id="24325240" data-annot-id="23954768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_214 pdf-obj-fixed acroform-field "
                   name="KC23" data-field-id="24325576" data-annot-id="23954960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_215 pdf-obj-fixed acroform-field "
                   name="CX22" data-field-id="24325912" data-annot-id="23955152" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_216 pdf-obj-fixed acroform-field "
                   name="CX23" data-field-id="24326248" data-annot-id="23955344" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_217 pdf-obj-fixed acroform-field "
                   name="DG22" data-field-id="24326584" data-annot-id="23955536" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_218 pdf-obj-fixed acroform-field "
                   name="DG23" data-field-id="24326920" data-annot-id="23955728" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_219 pdf-obj-fixed acroform-field "
                   name="DR22" data-field-id="24327256" data-annot-id="23955920" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_220 pdf-obj-fixed acroform-field "
                   name="DR23" data-field-id="24327592" data-annot-id="23956112" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_221 pdf-obj-fixed acroform-field "
                   name="KF"
                   data-field-id="24327928" data-annot-id="23956304" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_222 pdf-obj-fixed acroform-field "
                   name="KD"
                   data-field-id="24328264" data-annot-id="23956496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_223 pdf-obj-fixed acroform-field "
                   name="EF"
                   data-field-id="24328600" data-annot-id="23956688" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_224 pdf-obj-fixed acroform-field "
                   name="EG"
                   data-field-id="24328936" data-annot-id="23956880" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_225 pdf-obj-fixed acroform-field "
                   name="EJ"
                   data-field-id="24329272" data-annot-id="23957072" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_226 pdf-obj-fixed acroform-field "
                   name="ZF"
                   data-field-id="24329608" data-annot-id="23957264" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_227 pdf-obj-fixed acroform-field "
                   name="EH"
                   data-field-id="24329944" data-annot-id="23957456" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_228 pdf-obj-fixed acroform-field "
                   name="KG"
                   data-field-id="24330280" data-annot-id="23957648" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_229 pdf-obj-fixed acroform-field "
                   name="KE"
                   data-field-id="24330616" data-annot-id="23957840" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 534, top: 70, width: 117, height: 13, 'font-size': 17, }}>
<span>
REVENUS 2019
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 140, width: 415, height: 19, 'font-size': 17, }}>
<span>
II- DÉTERMINATION DES PLUS ET MOINS-VALUES (C)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 936, top: 169, width: 144, height: 13, 'font-size': 17, }}>
<span>
Plus ou moins-value
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 418, width: 810, height: 16, 'font-size': 17, }}>
<span>
Plus ou moins-value à court terme (à reporter ligne CB ou CK de l&#39;annexe 2035-B)---------------------------------&#62;
  <br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 612, width: 305, height: 16, 'font-size': 17, }}>
<span>
Plus-values à court terme exonérées (C)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 544, top: 612, width: 568, height: 16, 'font-size': 17, }}>
<span>
Plus-values nette à long terme exonérées (C) (à reporter page 1 de la 2035)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 774, top: 817, width: 76, height: 16, 'font-size': 17, }}>
<span>
Répartition
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 671, top: 853, width: 116, height: 13, 'font-size': 17, }}>
<span>
du résultat fiscal
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 499, top: 910, width: 156, height: 16, 'font-size': 17, }}>
<span>
Quote-part du résultat
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 845, top: 910, width: 85, height: 13, 'font-size': 17, }}>
<span>
Montant net
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 1333, width: 360, height: 16, 'font-size': 17, }}>
<span>
Report des totaux de la dernière annexe Totaux →
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 983, top: 69, width: 126, height: 13, 'font-size': 17, }}>
<span>
N° 2035-SD Suite
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1023, top: 87, width: 46, height: 16, 'font-size': 17, }}>
<span>
(2020)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 172, width: 82, height: 12, 'font-size': 17, }}>
<span>
Nature des
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 190, width: 122, height: 13, 'font-size': 17, }}>
<span>
immobilisations
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 209, width: 54, height: 13, 'font-size': 17, }}>
<span>
cédées
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 241, top: 181, width: 32, height: 13, 'font-size': 17, }}>
<span>
Date
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 213, top: 199, width: 87, height: 16, 'font-size': 17, }}>
<span>
d&#39;acquisition
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 334, top: 181, width: 56, height: 13, 'font-size': 17, }}>
<span>
Date de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 334, top: 199, width: 54, height: 13, 'font-size': 17, }}>
<span>
cession
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 450, top: 175, width: 48, height: 12, 'font-size': 17, }}>
<span>
Valeur
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 444, top: 192, width: 60, height: 16, 'font-size': 17, }}>
<span>
d&#39;origine
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 471, top: 210, width: 4, height: 9, 'font-size': 13, }}>
<span>
1
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 535, top: 175, width: 115, height: 12, 'font-size': 17, }}>
<span>
Amortissements
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 589, top: 210, width: 7, height: 9, 'font-size': 13, }}>
<span>
2
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 691, top: 175, width: 47, height: 12, 'font-size': 17, }}>
<span>
Valeur
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 681, top: 192, width: 68, height: 13, 'font-size': 17, }}>
<span>
résiduelle
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 711, top: 210, width: 7, height: 9, 'font-size': 13, }}>
<span>
3
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 776, top: 175, width: 107, height: 12, 'font-size': 17, }}>
<span>
Prix de cession
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 826, top: 210, width: 7, height: 9, 'font-size': 13, }}>
<span>
4
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 893, top: 196, width: 102, height: 13, 'font-size': 17, }}>
<span>
à court terme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 941, top: 214, width: 7, height: 10, 'font-size': 13, }}>
<span>
5
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1017, top: 196, width: 96, height: 16, 'font-size': 17, }}>
<span>
à long terme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1062, top: 214, width: 6, height: 10, 'font-size': 13, }}>
<span>
6
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 497, width: 328, height: 16, 'font-size': 17, }}>
<span>
Vous optez pour l&#39;étalement de la plus-value à
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 515, width: 90, height: 12, 'font-size': 17, }}>
<span>
court-terme :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 532, width: 326, height: 17, 'font-size': 17, }}>
<span>
Montant pour lequel l&#39;imposition est différé (C)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 531, top: 506, width: 417, height: 16, 'font-size': 17, }}>
<span>
Plus-value à long terme imposable (à reporter page 1 de la
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 530, top: 524, width: 152, height: 16, 'font-size': 17, }}>
<span>
déclaration 2035-SD)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 649, width: 74, height: 13, 'font-size': 17, }}>
<span>
Article 151
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 61, top: 667, width: 105, height: 16, 'font-size': 17, }}>
<span>
septies du CGI
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 323, top: 640, width: 76, height: 13, 'font-size': 17, }}>
<span>
Article 238
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 658, width: 98, height: 16, 'font-size': 17, }}>
<span>
quindecies du
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 347, top: 676, width: 28, height: 13, 'font-size': 17, }}>
<span>
CGI
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 649, width: 74, height: 13, 'font-size': 17, }}>
<span>
Article 151
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 540, top: 667, width: 105, height: 16, 'font-size': 17, }}>
<span>
septies du CGI
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 887, top: 649, width: 76, height: 13, 'font-size': 17, }}>
<span>
Article 238
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 859, top: 667, width: 131, height: 16, 'font-size': 17, }}>
<span>
quindecies du CGI
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 717, width: 74, height: 13, 'font-size': 17, }}>
<span>
Article 151
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 735, width: 88, height: 16, 'font-size': 17, }}>
<span>
septies A du
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 753, width: 27, height: 12, 'font-size': 17, }}>
<span>
CGI
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 726, width: 74, height: 12, 'font-size': 17, }}>
<span>
Article 151
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 532, top: 743, width: 120, height: 16, 'font-size': 17, }}>
<span>
septies A du CGI
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 851, top: 726, width: 147, height: 15, 'font-size': 17, }}>
<span>
Article 151 septies B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 900, top: 743, width: 50, height: 13, 'font-size': 17, }}>
<span>
du CGI
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 788, width: 705, height: 19, 'font-size': 17, }}>
<span>
III – RÉPARTITION DES RÉSULTATS ENTRE LES ASSOCIÉS (tableau réservé aux sociétés) (D)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 873, width: 262, height: 16, 'font-size': 17, }}>
<span>
Nom, prénom, domicile des associés
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 461, top: 833, width: 13, height: 93, 'font-size': 17, }}>
<span>
Part dans les
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 479, top: 830, width: 13, height: 100, 'font-size': 17, }}>
<span>
résultats en %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 970, top: 877, width: 152, height: 16, 'font-size': 17, }}>
<span>
de la plus-value nette
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1001, top: 895, width: 89, height: 16, 'font-size': 17, }}>
<span>
à long terme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 704, top: 892, width: 60, height: 16, 'font-size': 17, }}>
<span>
Charges
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 676, top: 910, width: 115, height: 16, 'font-size': 17, }}>
<span>
professionnelles
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 690, top: 928, width: 87, height: 13, 'font-size': 17, }}>
<span>
individuelles
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1367, width: 1059, height: 12, 'font-size': 13, }}>
<span>
Les dispositions des articles 39 et 40 de la loi n° 78-17 du 6 janvier 1978 relative à l&#39;informatique, aux fichiers et aux libertés, modifiée par la loi n° 2004-801 du 6 août 2004, garantissent les droits des
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1381, width: 443, height: 12, 'font-size': 13, }}>
<span>
personnes physiques à l&#39;égard des traitements des données à caractère personnel.
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_231 pdf-obj-fixed acroform-field "
                   name="FA1"
                   data-field-id="24361128" data-annot-id="24108304" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_232 pdf-obj-fixed acroform-field "
                   name="KH1"
                   data-field-id="24364104" data-annot-id="23923024" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_233 pdf-obj-fixed acroform-field "
                   name="KH2"
                   data-field-id="24367144" data-annot-id="23923248" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_234 pdf-obj-fixed acroform-field "
                   name="FA2"
                   data-field-id="24370184" data-annot-id="23923440" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_235 pdf-obj-fixed acroform-field "
                   name="FA3"
                   data-field-id="24373224" data-annot-id="23923632" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_236 pdf-obj-fixed acroform-field "
                   name="FA4"
                   data-field-id="24376264" data-annot-id="24870448" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_237 pdf-obj-fixed acroform-field "
                   name="FA5"
                   data-field-id="24681256" data-annot-id="24870640" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_238 pdf-obj-fixed acroform-field "
                   name="KH5"
                   data-field-id="24692952" data-annot-id="24870832" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_239 pdf-obj-fixed acroform-field "
                   name="KH4"
                   data-field-id="24379304" data-annot-id="24871024" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_240 pdf-obj-fixed acroform-field "
                   name="KH3"
                   data-field-id="24382344" data-annot-id="24871216" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_241 pdf-obj-fixed acroform-field "
                   name="KN3"
                   data-field-id="24385384" data-annot-id="24871408" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_242 pdf-obj-fixed acroform-field "
                   name="KN1"
                   data-field-id="24388424" data-annot-id="24871600" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_243 pdf-obj-fixed acroform-field "
                   name="KJ1"
                   data-field-id="24391336" data-annot-id="24871792" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_244 pdf-obj-fixed acroform-field "
                   name="KK1"
                   data-field-id="24395672" data-annot-id="24871984" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_245 pdf-obj-fixed acroform-field "
                   name="KL1"
                   data-field-id="24408728" data-annot-id="24872176" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_246 pdf-obj-fixed acroform-field "
                   name="FW1"
                   data-field-id="24412056" data-annot-id="24872368" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_247 pdf-obj-fixed acroform-field "
                   name="HA1"
                   data-field-id="24415304" data-annot-id="24872560" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_248 pdf-obj-fixed acroform-field "
                   name="HL1"
                   data-field-id="24418632" data-annot-id="24879024" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_249 pdf-obj-fixed acroform-field "
                   name="KN2"
                   data-field-id="24421880" data-annot-id="24879216" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_250 pdf-obj-fixed acroform-field "
                   name="KJ2"
                   data-field-id="24424952" data-annot-id="24879408" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_251 pdf-obj-fixed acroform-field "
                   name="KK2"
                   data-field-id="24428200" data-annot-id="24879600" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_252 pdf-obj-fixed acroform-field "
                   name="KL2"
                   data-field-id="24431528" data-annot-id="24879792" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_253 pdf-obj-fixed acroform-field "
                   name="FW2"
                   data-field-id="24434776" data-annot-id="24879984" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_254 pdf-obj-fixed acroform-field "
                   name="HA2"
                   data-field-id="24438104" data-annot-id="24880176" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_255 pdf-obj-fixed acroform-field "
                   name="HL2"
                   data-field-id="24441352" data-annot-id="24880368" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_256 pdf-obj-fixed acroform-field "
                   name="KJ3"
                   data-field-id="24444680" data-annot-id="24880560" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_257 pdf-obj-fixed acroform-field "
                   name="KK3"
                   data-field-id="24447928" data-annot-id="24880752" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_258 pdf-obj-fixed acroform-field "
                   name="KL3"
                   data-field-id="24451256" data-annot-id="24880944" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_259 pdf-obj-fixed acroform-field "
                   name="FW3"
                   data-field-id="24454504" data-annot-id="24881136" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_260 pdf-obj-fixed acroform-field "
                   name="HA3" data-field-id="24457832" data-annot-id="24881328" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_261 pdf-obj-fixed acroform-field "
                   name="HL3" data-field-id="24301880" data-annot-id="24881520" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_262 pdf-obj-fixed acroform-field "
                   name="KN4" data-field-id="24466408" data-annot-id="24881712" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_263 pdf-obj-fixed acroform-field "
                   name="KJ4" data-field-id="24469480" data-annot-id="24881904" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_264 pdf-obj-fixed acroform-field "
                   name="KK4" data-field-id="24472760" data-annot-id="24872752" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_265 pdf-obj-fixed acroform-field "
                   name="KL4" data-field-id="24476040" data-annot-id="24882624" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_266 pdf-obj-fixed acroform-field "
                   name="FW4" data-field-id="24479320" data-annot-id="24882816" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_267 pdf-obj-fixed acroform-field "
                   name="HA4" data-field-id="24482600" data-annot-id="24883008" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_268 pdf-obj-fixed acroform-field "
                   name="HL4" data-field-id="24485880" data-annot-id="24883200" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_269 pdf-obj-fixed acroform-field "
                   name="KN5" data-field-id="24704744" data-annot-id="24883392" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_270 pdf-obj-fixed acroform-field "
                   name="KJ5" data-field-id="24717272" data-annot-id="24883584" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_271 pdf-obj-fixed acroform-field "
                   name="KK5" data-field-id="24730488" data-annot-id="24883776" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_272 pdf-obj-fixed acroform-field "
                   name="KL5" data-field-id="24743640" data-annot-id="24883968" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_273 pdf-obj-fixed acroform-field "
                   name="FW5" data-field-id="24753848" data-annot-id="24884160" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_274 pdf-obj-fixed acroform-field "
                   name="HA5" data-field-id="24764088" data-annot-id="24884352" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_275 pdf-obj-fixed acroform-field "
                   name="HL5" data-field-id="24774328" data-annot-id="24884544" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_276 pdf-obj-fixed acroform-field "
                   name="KM" data-field-id="24489160" data-annot-id="24884736" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_277 pdf-obj-fixed acroform-field "
                   name="JA" data-field-id="24492440" data-annot-id="24884928" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_278 pdf-obj-fixed acroform-field "
                   name="JB" data-field-id="24495720" data-annot-id="24885120" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_279 pdf-obj-fixed acroform-field "
                   name="JD" data-field-id="24499000" data-annot-id="24885312" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_280 pdf-obj-fixed acroform-field "
                   name="JL" data-field-id="24502280" data-annot-id="24885504" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_281 pdf-obj-fixed acroform-field "
                   name="JH" data-field-id="24505560" data-annot-id="24885696" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_282 pdf-obj-fixed acroform-field "
                   name="JF" data-field-id="24508840" data-annot-id="24885888" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_283 pdf-obj-fixed acroform-field "
                   name="JM" data-field-id="24512120" data-annot-id="24886080" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_284 pdf-obj-fixed acroform-field "
                   name="JJ" data-field-id="24515400" data-annot-id="24886272" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_285 pdf-obj-fixed acroform-field "
                   name="JK" data-field-id="24518680" data-annot-id="24886464" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_286 pdf-obj-fixed acroform-field "
                   name="LA1" data-field-id="24521960" data-annot-id="24886656" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_287 pdf-obj-fixed acroform-field "
                   name="LB1" data-field-id="24524984" data-annot-id="24886848" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_288 pdf-obj-fixed acroform-field "
                   name="LC1" data-field-id="24528136" data-annot-id="24887040" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_289 pdf-obj-fixed acroform-field "
                   name="LD1" data-field-id="24531416" data-annot-id="24887232" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_290 pdf-obj-fixed acroform-field "
                   name="LE1" data-field-id="24534664" data-annot-id="24887424" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_291 pdf-obj-fixed acroform-field "
                   name="LF1" data-field-id="24537992" data-annot-id="24887616" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_292 pdf-obj-fixed acroform-field "
                   name="LA2" data-field-id="24541240" data-annot-id="24887808" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_293 pdf-obj-fixed acroform-field "
                   name="LB2" data-field-id="24544312" data-annot-id="24888000" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_294 pdf-obj-fixed acroform-field "
                   name="LC2" data-field-id="24547432" data-annot-id="24888192" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_295 pdf-obj-fixed acroform-field "
                   name="LD2" data-field-id="24550760" data-annot-id="24888384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_296 pdf-obj-fixed acroform-field "
                   name="LE2" data-field-id="24554008" data-annot-id="24882096" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_297 pdf-obj-fixed acroform-field "
                   name="LF2" data-field-id="24558280" data-annot-id="24882288" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_298 pdf-obj-fixed acroform-field "
                   name="LA3" data-field-id="24571176" data-annot-id="24889616" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_299 pdf-obj-fixed acroform-field "
                   name="LB3" data-field-id="24574248" data-annot-id="24889808" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_300 pdf-obj-fixed acroform-field "
                   name="LC3" data-field-id="24577368" data-annot-id="24890000" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_301 pdf-obj-fixed acroform-field "
                   name="LD3" data-field-id="24580696" data-annot-id="24890192" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_302 pdf-obj-fixed acroform-field "
                   name="LE3" data-field-id="24583944" data-annot-id="24890384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_303 pdf-obj-fixed acroform-field "
                   name="LF3" data-field-id="24587272" data-annot-id="24890576" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_304 pdf-obj-fixed acroform-field "
                   name="LA4" data-field-id="24590520" data-annot-id="24890768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_305 pdf-obj-fixed acroform-field "
                   name="LB4" data-field-id="24593592" data-annot-id="24890960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_306 pdf-obj-fixed acroform-field "
                   name="LC4" data-field-id="24596712" data-annot-id="24891152" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_307 pdf-obj-fixed acroform-field "
                   name="LD4" data-field-id="24600040" data-annot-id="24891344" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_308 pdf-obj-fixed acroform-field "
                   name="LE4" data-field-id="24603320" data-annot-id="24891536" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_309 pdf-obj-fixed acroform-field "
                   name="LF4" data-field-id="24606600" data-annot-id="24891728" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_310 pdf-obj-fixed acroform-field "
                   name="LA5" data-field-id="24609880" data-annot-id="24891920" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_311 pdf-obj-fixed acroform-field "
                   name="LB5" data-field-id="24612904" data-annot-id="24892112" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_312 pdf-obj-fixed acroform-field "
                   name="LC5" data-field-id="24616056" data-annot-id="24892304" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_313 pdf-obj-fixed acroform-field "
                   name="LD5" data-field-id="24619336" data-annot-id="24892496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_314 pdf-obj-fixed acroform-field "
                   name="LE5" data-field-id="24622616" data-annot-id="24892688" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_315 pdf-obj-fixed acroform-field "
                   name="LF5" data-field-id="24625896" data-annot-id="24892880" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_316 pdf-obj-fixed acroform-field "
                   name="LA6" data-field-id="24629176" data-annot-id="24893072" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_317 pdf-obj-fixed acroform-field "
                   name="LB6" data-field-id="24632200" data-annot-id="24893264" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_318 pdf-obj-fixed acroform-field "
                   name="LC6" data-field-id="24635352" data-annot-id="24893456" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_319 pdf-obj-fixed acroform-field "
                   name="LD6" data-field-id="24638632" data-annot-id="24893648" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_320 pdf-obj-fixed acroform-field "
                   name="LE6" data-field-id="24641912" data-annot-id="24893840" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_321 pdf-obj-fixed acroform-field "
                   name="LF6" data-field-id="24645192" data-annot-id="24894032" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_322 pdf-obj-fixed acroform-field "
                   name="LA7" data-field-id="24648472" data-annot-id="24894224" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_323 pdf-obj-fixed acroform-field "
                   name="LB7" data-field-id="24651496" data-annot-id="24894416" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_324 pdf-obj-fixed acroform-field "
                   name="LC7" data-field-id="24654648" data-annot-id="24894608" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_325 pdf-obj-fixed acroform-field "
                   name="LD7" data-field-id="24657928" data-annot-id="24894800" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_326 pdf-obj-fixed acroform-field "
                   name="LE7" data-field-id="24661208" data-annot-id="24894992" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_327 pdf-obj-fixed acroform-field "
                   name="LF7" data-field-id="24664488" data-annot-id="24895184" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_328 pdf-obj-fixed acroform-field "
                   name="MB" data-field-id="24667736" data-annot-id="24895376" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_329 pdf-obj-fixed acroform-field "
                   name="MC" data-field-id="24670936" data-annot-id="24895568" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_330 pdf-obj-fixed acroform-field "
                   name="MF" data-field-id="24674184" data-annot-id="24895760" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_331 pdf-obj-fixed acroform-field "
                   name="FA5" data-field-id="24681256" data-annot-id="24895952" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_332 pdf-obj-fixed acroform-field "
                   name="KH5" data-field-id="24692952" data-annot-id="24896144" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_333 pdf-obj-fixed acroform-field "
                   name="KN5" data-field-id="24704744" data-annot-id="24896336" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_334 pdf-obj-fixed acroform-field "
                   name="KJ5" data-field-id="24717272" data-annot-id="24896528" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_335 pdf-obj-fixed acroform-field "
                   name="KK5" data-field-id="24730488" data-annot-id="24896720" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_336 pdf-obj-fixed acroform-field "
                   name="KL5" data-field-id="24743640" data-annot-id="24896912" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_337 pdf-obj-fixed acroform-field "
                   name="FW5" data-field-id="24753848" data-annot-id="24897104" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_338 pdf-obj-fixed acroform-field "
                   name="HA5" data-field-id="24764088" data-annot-id="24897296" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_339 pdf-obj-fixed acroform-field "
                   name="HL5" data-field-id="24774328" data-annot-id="24897488" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_340 pdf-obj-fixed acroform-field "
                   name="FA5" data-field-id="24681256" data-annot-id="24897680" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_341 pdf-obj-fixed acroform-field "
                   name="KH5" data-field-id="24692952" data-annot-id="24897872" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_342 pdf-obj-fixed acroform-field "
                   name="KN5" data-field-id="24704744" data-annot-id="24898064" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_343 pdf-obj-fixed acroform-field "
                   name="KJ5" data-field-id="24717272" data-annot-id="24898256" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_344 pdf-obj-fixed acroform-field "
                   name="KK5" data-field-id="24730488" data-annot-id="24898448" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_345 pdf-obj-fixed acroform-field "
                   name="KL5" data-field-id="24743640" data-annot-id="24898640" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_346 pdf-obj-fixed acroform-field "
                   name="FW5" data-field-id="24753848" data-annot-id="24898832" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_347 pdf-obj-fixed acroform-field "
                   name="HA5" data-field-id="24764088" data-annot-id="24899024" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_348 pdf-obj-fixed acroform-field "
                   name="HL5" data-field-id="24774328" data-annot-id="24899216" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_349 pdf-obj-fixed acroform-field "
                   name="FA5" data-field-id="24681256" data-annot-id="24899408" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_350 pdf-obj-fixed acroform-field "
                   name="KH5" data-field-id="24692952" data-annot-id="24899600" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_351 pdf-obj-fixed acroform-field "
                   name="KN5" data-field-id="24704744" data-annot-id="24899792" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_352 pdf-obj-fixed acroform-field "
                   name="KJ5" data-field-id="24717272" data-annot-id="24899984" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_353 pdf-obj-fixed acroform-field "
                   name="KK5" data-field-id="24730488" data-annot-id="24900176" type="text"/>

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2035SUITE;
