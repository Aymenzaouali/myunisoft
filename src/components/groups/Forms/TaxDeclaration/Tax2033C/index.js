import React from "react";
import { Field } from 'redux-form';
import { useCompute, sum, roundField, setIsNothingness, parse as p } from "helpers/pdfforms";

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const makeCalcImmmo = (form, fields) => (p(form[fields[0]]) + p(form[fields[1]]) - p(form[fields[2]])).toFixed(2);
const makeSumWithFixed = (form, fields) => {
  let result = sum(form, fields);
  return result.toFixed(2);
}

const Tax2033C = (props) => {
  const { taxDeclarationForm, change } = props;
  const notNeededProps = { 
    'NOM_STEA': true,
    'RQ': true
   };

   
  setIsNothingness(taxDeclarationForm, notNeededProps, 'RQ', change);

  useCompute('AK', makeSumWithFixed, ['AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AJ'], taxDeclarationForm, change);
  useCompute('BK', makeSumWithFixed, ['BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BJ'], taxDeclarationForm, change);
  useCompute('CK', makeSumWithFixed, ['CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CJ'], taxDeclarationForm, change);

  useCompute('DA', ({AA, BA, CA}) => p(AA) + p(BA) - p(CA), ['AA', 'BA', 'CA'], taxDeclarationForm, change);
  useCompute('DB', makeCalcImmmo, ['AB', 'BB', 'CB'], taxDeclarationForm, change);
  useCompute('DC', makeCalcImmmo, ['AC', 'BC', 'CC'], taxDeclarationForm, change);
  useCompute('DD', makeCalcImmmo, ['AD', 'BD', 'CD'], taxDeclarationForm, change);
  useCompute('DE', makeCalcImmmo, ['AE', 'BE', 'CE'], taxDeclarationForm, change);
  useCompute('DF', makeCalcImmmo, ['AF', 'BF', 'CF'], taxDeclarationForm, change);
  useCompute('DG', makeCalcImmmo, ['AG', 'BG', 'CG'], taxDeclarationForm, change);
  useCompute('DH', makeCalcImmmo, ['AH', 'BH', 'CH'], taxDeclarationForm, change);
  useCompute('DJ', makeCalcImmmo, ['AJ', 'BJ', 'CJ'], taxDeclarationForm, change);

  useCompute('DK', makeSumWithFixed, ['DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DJ'], taxDeclarationForm, change);
  useCompute('FH', makeSumWithFixed, ['FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG'], taxDeclarationForm, change);
  useCompute('GH', makeSumWithFixed, ['GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG'], taxDeclarationForm, change);
  useCompute('HH', makeSumWithFixed, ['HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG'], taxDeclarationForm, change);

  useCompute('JA', ({FA, GA, HA}) => p(FA) + p(GA) - p(HA), ['FA', 'GA', 'HA'], taxDeclarationForm, change);
  useCompute('JB', ({FB, GB, HB}) => p(FB) + p(GB) - p(HB), ['FB', 'GB', 'HB'], taxDeclarationForm, change);
  useCompute('JC', ({FC, GC, HC}) => p(FC) + p(GC) - p(HC), ['FC', 'GC', 'HC'], taxDeclarationForm, change);
  useCompute('JD', ({FD, GD, HD}) => p(FD) + p(GD) - p(HD), ['FD', 'GD', 'HD'], taxDeclarationForm, change);
  useCompute('JE', ({FE, GE, HE}) => p(FE) + p(GE) - p(HE), ['FE', 'GE', 'HE'], taxDeclarationForm, change);
  useCompute('JF', ({FF, GF, HF}) => p(FF) + p(GF) - p(HF), ['FF', 'GF', 'HF'], taxDeclarationForm, change);
  useCompute('JF', ({FF, GF, HF}) => p(FF) + p(GF) - p(HF), ['FF', 'GF', 'HF'], taxDeclarationForm, change);
  useCompute('JG', ({FG, GG, HG}) => p(FG) + p(GG) - p(HG), ['FG', 'GG', 'HG'], taxDeclarationForm, change);

  useCompute('JH', sum, ['JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG'], taxDeclarationForm, change);

  useCompute('LL', sum, ['LA1', 'LA2', 'LA3', 'LA4', 'LA5', 'LA6', 'LA7', 'LA8', 'LA9', 'LA10'], taxDeclarationForm, change);
  useCompute('ML', sum, ['MA1', 'MA2', 'MA3', 'MA4', 'MA5', 'MA6', 'MA7', 'MA8', 'MA9', 'MA10'], taxDeclarationForm, change);
  useCompute('NL', sum, ['NA1', 'NA2', 'NA3', 'NA4', 'NA5', 'NA6', 'NA7', 'NA8', 'NA9', 'NA10'], taxDeclarationForm, change);
  useCompute('PL', sum, ['PA1', 'PA2', 'PA3', 'PA4', 'PA5', 'PA6', 'PA7', 'PA8', 'PA9', 'PA10'], taxDeclarationForm, change);
  useCompute('QL', sum, ['QA1', 'QA2', 'QA3', 'QA4', 'QA5', 'QA6', 'QA7', 'QA8', 'QA9', 'QA10'], taxDeclarationForm, change);
  useCompute('UL', sum, ['UA1', 'UA2', 'UA3', 'UA4', 'UA5', 'UA6', 'UA7', 'UA8', 'UA9', 'UA10'], taxDeclarationForm, change);
  useCompute('SL', sum, ['SA1', 'SA2', 'SA3', 'SA4', 'SA5', 'SA6', 'SA7', 'SA8', 'SA9', 'SA10'], taxDeclarationForm, change);
  useCompute('TL', sum, ['TA1', 'TA2', 'TA3', 'TA4', 'TA5', 'TA6', 'TA7', 'TA8', 'TA9', 'TA10'], taxDeclarationForm, change);

  useCompute('QN', sum, ['QL', 'QM'], taxDeclarationForm, change);
  useCompute('UN', sum, ['UL', 'UM'], taxDeclarationForm, change);
  useCompute('SN', sum, ['SL', 'SM', 'SP'], taxDeclarationForm, change);
  useCompute('TN', sum, ['TL', 'TM'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2033c">
      <div id="pdf-document" data-type="pdf-document" data-num-pages="1" data-layout="fixed">

      </div>

      <form id="acroform"></form>

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.293333" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 233, top: 57, width: 31, height: 32, 'font-size': 40, }}>
              <span>
                
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 332, top: 69, width: 539, height: 15, 'font-size': 21, }}>
              <span>
                IMMOBILISATIONS - AMORTISSEMENTS - PLUS-VALUES - MOINS-VALUES
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 60, width: 50, height: 12, 'font-size': 18, }}>
              <span>
                DGFiP
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 958, top: 58, width: 143, height: 16, 'font-size': 21, }}>
              <span>
                N° 2033-C-SD 2019
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1254, width: 11, height: 214, 'font-size': 11, }}>
              <span>
                N° 2033-C-SD – (SDNC-DGFiP) - Janvier 201
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 108, top: 1476, width: 7, height: 8, 'font-size': 21, }}>
              <span>
                *
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 1476, width: 451, height: 12, 'font-size': 13, }}>
              <span>
                Des explications concernant cette rubrique sont données dans la notice 2033-NOT-SD
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1490, width: 994, height: 12, 'font-size': 13, }}>
              <span>
                (1) Ces plus-values sont imposables au taux de 19 % en application des articles 238 bis JA, 210 F et 208 C du CGI. Joindre un état établi selon le même modèle, indiquant les modalités de
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 1504, width: 132, height: 12, 'font-size': 13, }}>
              <span>
                calcul de ces plus-values.
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 126, width: 184, height: 10, 'font-size': 11, }}>
              <span>
                Formulaire obligatoire (article 302 septies
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 136, width: 157, height: 11, 'font-size': 11, }}>
              <span>
                A bis du Code général des impôts)
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 320, top: 124, width: 167, height: 14, 'font-size': 14, }}>
              <span>
                Désignation de l’entreprise :
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1029, top: 113, width: 36, height: 11, 'font-size': 14, }}>
              <span>
                Néant
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1086, top: 112, width: 7, height: 7, 'font-size': 20, }}>
              <span>
                *
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 168, width: 257, height: 13, 'font-size': 16, }}>
              <span>
                IIMMOBILISATIONSValeur
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 170, width: 49, height: 10, 'font-size': 13, }}>
              <span>
                brute des
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 346, top: 184, width: 98, height: 10, 'font-size': 13, }}>
              <span>
                immobilisations au
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 346, top: 198, width: 99, height: 10, 'font-size': 13, }}>
              <span>
                début de l’exercice
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 512, top: 185, width: 79, height: 12, 'font-size': 13, }}>
              <span>
                Augmentations
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 677, top: 185, width: 64, height: 9, 'font-size': 13, }}>
              <span>
                Diminutions
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 824, top: 170, width: 86, height: 10, 'font-size': 13, }}>
              <span>
                Valeur brute des
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 822, top: 184, width: 91, height: 10, 'font-size': 13, }}>
              <span>
                immobilisations à
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 819, top: 198, width: 96, height: 10, 'font-size': 13, }}>
              <span>
                la fin de l’exercice
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 969, top: 168, width: 101, height: 13, 'font-size': 13, }}>
              <span>
                Réévaluation légale
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1074, top: 168, width: 6, height: 7, 'font-size': 20, }}>
              <span>
                *
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 196, width: 118, height: 13, 'font-size': 14, }}>
              <span>
                ACTIF IMMOBILISÉ
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 953, top: 192, width: 143, height: 13, 'font-size': 13, }}>
              <span>
                Valeur d’origine des immo-
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 953, top: 205, width: 143, height: 9, 'font-size': 13, }}>
              <span>
                bilisations en fin d’exercice
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 236, width: 83, height: 10, 'font-size': 13, }}>
              <span>
                Immobilisations
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 119, top: 250, width: 68, height: 12, 'font-size': 13, }}>
              <span>
                incorporelles
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 205, top: 225, width: 7, height: 42, 'font-size': 52, }}>
              <span>

                <br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 217, top: 228, width: 122, height: 10, 'font-size': 13, }}>
              <span>
                Fonds commercial400
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 228, width: 19, height: 10, 'font-size': 13, }}>
              <span>
                402
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 228, width: 19, height: 10, 'font-size': 13, }}>
              <span>
                404
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 227, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                406
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 216, top: 258, width: 34, height: 9, 'font-size': 13, }}>
              <span>
                Autres
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 321, top: 257, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                410
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 257, width: 19, height: 10, 'font-size': 13, }}>
              <span>
                412
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 257, width: 19, height: 10, 'font-size': 13, }}>
              <span>
                414
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 257, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                416
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 297, width: 13, height: 138, 'font-size': 13, }}>
              <span>
                Immobilisations corporelles
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 287, width: 42, height: 10, 'font-size': 13, }}>
              <span>
                Terrains
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 286, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                426
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 287, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                424
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 287, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                422
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 320, top: 287, width: 19, height: 10, 'font-size': 13, }}>
              <span>
                420
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 317, width: 71, height: 9, 'font-size': 13, }}>
              <span>
                Constructions
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 320, top: 316, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                430
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 316, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                432
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 316, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                434
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 316, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                436
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 340, width: 122, height: 12, 'font-size': 13, }}>
              <span>
                Installations techniques
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 346, width: 185, height: 18, 'font-size': 13, }}>
              <span>
                matériel et outillage industriels440
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 346, width: 18, height: 9, 'font-size': 13, }}>
              <span>
                442
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 345, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                446
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 346, width: 19, height: 9, 'font-size': 13, }}>
              <span>
                444
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 369, width: 115, height: 13, 'font-size': 13, }}>
              <span>
                Installations générales
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 381, width: 103, height: 13, 'font-size': 13, }}>
              <span>
                agencements divers
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 321, top: 375, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                450
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 375, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                452
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 375, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                454
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 375, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                456
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 405, width: 109, height: 12, 'font-size': 13, }}>
              <span>
                Matériel de transport
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 320, top: 404, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                460
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 404, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                462
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 404, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                464
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 404, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                466
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 428, width: 119, height: 10, 'font-size': 13, }}>
              <span>
                Autres immobilisations
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 440, width: 58, height: 13, 'font-size': 13, }}>
              <span>
                corporelles
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 321, top: 434, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                470
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 434, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                472
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 434, width: 19, height: 10, 'font-size': 13, }}>
              <span>
                474
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 434, width: 19, height: 10, 'font-size': 13, }}>
              <span>
                476
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 464, width: 198, height: 10, 'font-size': 13, }}>
              <span>
                Immobilisations financières480
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 464, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                482
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 464, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                484
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 463, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                486
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 260, top: 493, width: 79, height: 11, 'font-size': 14, }}>
              <span>
                TOTAL490
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 493, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                496
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 493, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                492
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 493, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                494
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 523, width: 223, height: 13, 'font-size': 16, }}>
              <span>
                I IAMORTISSEMENTS
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 411, top: 532, width: 148, height: 10, 'font-size': 13, }}>
              <span>
                Montant des amortissements
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 429, top: 546, width: 116, height: 10, 'font-size': 13, }}>
              <span>
                au début de l’exercice
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 593, top: 532, width: 137, height: 13, 'font-size': 13, }}>
              <span>
                Augmentations : dotations
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 629, top: 546, width: 66, height: 10, 'font-size': 13, }}>
              <span>
                de l’exercice
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 761, top: 526, width: 155, height: 9, 'font-size': 13, }}>
              <span>
                Diminutions : amortissements
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 539, width: 148, height: 10, 'font-size': 13, }}>
              <span>
                afférents aux éléments sortis
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 553, width: 102, height: 13, 'font-size': 13, }}>
              <span>
                de l’actif et reprises
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 940, top: 532, width: 148, height: 10, 'font-size': 13, }}>
              <span>
                Montant des amortissements
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 963, top: 546, width: 106, height: 10, 'font-size': 13, }}>
              <span>
                à la fin de l’exercice
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 553, width: 225, height: 11, 'font-size': 14, }}>
              <span>
                IMMOBILISATIONS AMORTISSABLES
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 174, top: 583, width: 154, height: 12, 'font-size': 13, }}>
              <span>
                Immobilisations incorporelles
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 582, width: 18, height: 12, 'font-size': 13, }}>
              <span>
                506
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 753, top: 583, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                504
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 584, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                502
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 400, top: 584, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                500
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 622, width: 13, height: 138, 'font-size': 13, }}>
              <span>
                Immobilisations corporelles
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 613, width: 42, height: 9, 'font-size': 13, }}>
              <span>
                Terrains
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 613, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                510
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 613, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                512
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 753, top: 612, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                514
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 612, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                516
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 643, width: 71, height: 9, 'font-size': 13, }}>
              <span>
                Constructions
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 641, width: 18, height: 12, 'font-size': 13, }}>
              <span>
                526
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 753, top: 642, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                524
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 643, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                520
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 643, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                522
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 665, width: 122, height: 13, 'font-size': 13, }}>
              <span>
                Installations techniques
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 678, width: 161, height: 12, 'font-size': 13, }}>
              <span>
                matériel et outillage industriels
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 672, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                532
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 753, top: 671, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                534
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 671, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                536
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 672, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                530
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 695, width: 193, height: 12, 'font-size': 13, }}>
              <span>
                Installations générales, agencements,
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 707, width: 113, height: 12, 'font-size': 13, }}>
              <span>
                aménagements divers
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 701, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                540
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 701, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                542
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 753, top: 701, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                544
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 700, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                546
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 731, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                550
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 730, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                556
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 753, top: 730, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                554
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 731, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                552
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 730, width: 109, height: 13, 'font-size': 13, }}>
              <span>
                Matériel de transport
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 760, width: 180, height: 12, 'font-size': 13, }}>
              <span>
                Autres immobilisations corporelles
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 759, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                560
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 759, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                562
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 753, top: 759, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                564
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 759, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                566
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 339, top: 789, width: 80, height: 11, 'font-size': 14, }}>
              <span>
                TOTAL570
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 789, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                576
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 753, top: 789, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                574
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 790, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                572
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 821, width: 294, height: 15, 'font-size': 16, }}>
              <span>
                I I IPLUS-VALUES, MOINS-VALUES
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 817, width: 417, height: 12, 'font-size': 13, }}>
              <span>
                (19 %, 15 % et 0 % pour les entreprises à l’IS, 12,8 % pour les entreprises à l’IR)
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 828, width: 319, height: 13, 'font-size': 13, }}>
              <span>
                (si ce cadre est insuffisant, joindre un état du même modèle)
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 856, width: 142, height: 10, 'font-size': 13, }}>
              <span>
                Nature des immobilisations
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 162, top: 871, width: 69, height: 10, 'font-size': 13, }}>
              <span>
                cédées virées
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 886, width: 90, height: 12, 'font-size': 13, }}>
              <span>
                de poste à poste,
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 900, width: 113, height: 10, 'font-size': 13, }}>
              <span>
                mises hors service ou
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 915, width: 85, height: 12, 'font-size': 13, }}>
              <span>
                réintégrées dans
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 929, width: 100, height: 13, 'font-size': 13, }}>
              <span>
                le patrimoine privé
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 944, width: 117, height: 12, 'font-size': 13, }}>
              <span>
                y compris les produits
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 958, width: 142, height: 13, 'font-size': 13, }}>
              <span>
                de la propriété industrielle.
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 296, top: 853, width: 5, height: 9, 'font-size': 13, }}>
              <span>
                1
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 948, top: 853, width: 5, height: 10, 'font-size': 13, }}>
              <span>
                5
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 784, top: 852, width: 6, height: 10, 'font-size': 13, }}>
              <span>
                4
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 621, top: 853, width: 6, height: 10, 'font-size': 13, }}>
              <span>
                3
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 853, width: 6, height: 9, 'font-size': 13, }}>
              <span>
                2
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 295, top: 977, width: 7, height: 10, 'font-size': 13, }}>
              <span>
                6
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 978, width: 6, height: 10, 'font-size': 13, }}>
              <span>
                7
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 621, top: 978, width: 6, height: 9, 'font-size': 13, }}>
              <span>
                8
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 784, top: 978, width: 6, height: 10, 'font-size': 13, }}>
              <span>
                9
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 978, width: 12, height: 9, 'font-size': 13, }}>
              <span>
                10
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 1001, width: 10, height: 48, 'font-size': 13, }}>
              <span>
                Immobilisa-
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 1015, width: 9, height: 20, 'font-size': 13, }}>
              <span>
                tions
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 179, top: 1000, width: 70, height: 10, 'font-size': 13, }}>
              <span>
                Valeur d’actif
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 252, top: 998, width: 6, height: 7, 'font-size': 20, }}>
              <span>
                *
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 290, top: 1001, width: 82, height: 9, 'font-size': 13, }}>
              <span>
                Amortissements
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 377, top: 998, width: 6, height: 7, 'font-size': 20, }}>
              <span>
                *
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 410, top: 1000, width: 197, height: 10, 'font-size': 13, }}>
              <span>
                Valeur résiduellePrix de cession
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 611, top: 998, width: 6, height: 7, 'font-size': 20, }}>
              <span>
                *
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 811, top: 1000, width: 111, height: 10, 'font-size': 13, }}>
              <span>
                Plus ou moins-values
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 654, top: 1022, width: 62, height: 9, 'font-size': 13, }}>
              <span>
                Court terme
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 720, top: 1019, width: 6, height: 7, 'font-size': 20, }}>
              <span>
                *
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 896, top: 1021, width: 59, height: 11, 'font-size': 13, }}>
              <span>
                Long terme
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 1040, width: 11, height: 10, 'font-size': 13, }}>
              <span>
                
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 685, top: 1040, width: 11, height: 10, 'font-size': 13, }}>
              <span>
                
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 1040, width: 11, height: 10, 'font-size': 13, }}>
              <span>
                
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 1040, width: 11, height: 10, 'font-size': 13, }}>
              <span>
                
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 214, top: 1040, width: 10, height: 10, 'font-size': 13, }}>
              <span>
                
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 1040, width: 270, height: 12, 'font-size': 13, }}>
              <span>
                19 % 15 % ou 12,8 % 0 % 
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 1065, width: 5, height: 9, 'font-size': 13, }}>
              <span>
                1
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 1095, width: 6, height: 8, 'font-size': 13, }}>
              <span>
                2
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 1124, width: 6, height: 10, 'font-size': 13, }}>
              <span>
                3
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 1152, width: 6, height: 10, 'font-size': 13, }}>
              <span>
                4
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 1182, width: 6, height: 10, 'font-size': 13, }}>
              <span>
                5
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 1211, width: 6, height: 10, 'font-size': 13, }}>
              <span>
                6
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 1242, width: 6, height: 9, 'font-size': 13, }}>
              <span>
                7
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 1271, width: 6, height: 9, 'font-size': 13, }}>
              <span>
                8
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 1300, width: 6, height: 11, 'font-size': 13, }}>
              <span>
                9
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 1330, width: 11, height: 9, 'font-size': 13, }}>
              <span>
                10
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 1359, width: 68, height: 10, 'font-size': 13, }}>
              <span>
                TOTAL578
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 281, top: 1359, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                580
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 399, top: 1359, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                582
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 517, top: 1359, width: 19, height: 10, 'font-size': 13, }}>
              <span>
                584
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 1358, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                586
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 752, top: 1359, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                581
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 871, top: 1359, width: 19, height: 10, 'font-size': 13, }}>
              <span>
                587
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 988, top: 1359, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                589
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 209, top: 1388, width: 144, height: 11, 'font-size': 13, }}>
              <span>
                Plus-values taxables à 19 %
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 360, top: 1387, width: 58, height: 12, 'font-size': 9, }}>
              <span>
                (1)579
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 752, top: 1389, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                583
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 988, top: 1389, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                595
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 871, top: 1388, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                594
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 546, top: 1388, width: 107, height: 13, 'font-size': 13, }}>
              <span>
                Régularisations590
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 119, top: 1412, width: 499, height: 12, 'font-size': 13, }}>
              <span>
                Résultat net de la concession et de la sous-concession de licences d’exploitation de droits de la
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 119, top: 1424, width: 499, height: 12, 'font-size': 13, }}>
              <span>
                propriété industrielle bénéficiant du régime des plus-values à long terme (CGI art. 39 terdecies)
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 872, top: 1418, width: 17, height: 10, 'font-size': 13, }}>
              <span>
                591
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 1447, width: 38, height: 9, 'font-size': 13, }}>
              <span>
                TOTAL
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 635, top: 1446, width: 18, height: 12, 'font-size': 13, }}>
              <span>
                596
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 752, top: 1447, width: 18, height: 10, 'font-size': 13, }}>
              <span>
                585
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 871, top: 1447, width: 19, height: 11, 'font-size': 13, }}>
              <span>
                597
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 988, top: 1447, width: 18, height: 11, 'font-size': 13, }}>
              <span>
                599
<br />
              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 90, top: 1247, width: 8, height: 6, 'font-size': 11, }}>
              <span>
                9
<br />
              </span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" class="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="AA" data-field-id="36622040" data-annot-id="35964848" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="BA" data-field-id="36622344" data-annot-id="35965040" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="CA" data-field-id="36622680" data-annot-id="35965232" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="DA" data-field-id="36647480" data-annot-id="35965424" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="EA" data-field-id="36647656" data-annot-id="35965616" type="text" onChange={roundField(change)} />

            <Field component={PdfCheckbox} onCustomChange={change} class="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="RQ" data-field-id="36647992" data-annot-id="35965808" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component="input" class="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="AB" data-field-id="36648328" data-annot-id="35966000" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="BB" data-field-id="36648664" data-annot-id="35966192" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="CB" data-field-id="36649000" data-annot-id="35966384" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="DB" data-field-id="36649480" data-annot-id="35966576" type="text" disabled onChange={roundField(change)}/>

            <Field component="input" class="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="EB" data-field-id="36649816" data-annot-id="35966768" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="AC" data-field-id="36650152" data-annot-id="35966960" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="BC" data-field-id="36650488" data-annot-id="35967152" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="CC" data-field-id="36650824" data-annot-id="35967344" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="DC" data-field-id="36651160" data-annot-id="35967536" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="EC" data-field-id="36651496" data-annot-id="35967728" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="AK" data-field-id="36651832" data-annot-id="35967920" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="AJ" data-field-id="36652312" data-annot-id="35968384" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="AH" data-field-id="36652648" data-annot-id="35968576" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="AG" data-field-id="36652984" data-annot-id="35968768" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="AF" data-field-id="36653320" data-annot-id="35968960" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="AE" data-field-id="36653656" data-annot-id="35969152" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="AD" data-field-id="36653992" data-annot-id="35969344" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="BK" data-field-id="36654328" data-annot-id="35969536" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="BJ" data-field-id="36654664" data-annot-id="35969728" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="BH" data-field-id="36655000" data-annot-id="35969920" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="BG" data-field-id="36655336" data-annot-id="35970112" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="BF" data-field-id="36655672" data-annot-id="35970304" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="BE" data-field-id="36656008" data-annot-id="35970496" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="BD" data-field-id="36656344" data-annot-id="35970688" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="CK" data-field-id="36656680" data-annot-id="35970880" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="CJ" data-field-id="36657016" data-annot-id="35971072" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="CH" data-field-id="36657352" data-annot-id="35971264" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="CG" data-field-id="36652168" data-annot-id="35968112" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="CF" data-field-id="36658280" data-annot-id="35971984" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="CE" data-field-id="36658616" data-annot-id="35972176" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="CD" data-field-id="36658952" data-annot-id="35972368" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="DK" data-field-id="36659288" data-annot-id="35972560" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="DJ" data-field-id="36659624" data-annot-id="35972752" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="DH" data-field-id="36659960" data-annot-id="35972944" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="DG" data-field-id="36660296" data-annot-id="35973136" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="DF" data-field-id="36660632" data-annot-id="35973328" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="DE" data-field-id="36660968" data-annot-id="35973520" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="DD" data-field-id="36661304" data-annot-id="35973712" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="EK" data-field-id="36661640" data-annot-id="35973904" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="EJ" data-field-id="36661976" data-annot-id="35974096" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="EH" data-field-id="36662312" data-annot-id="35974288" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="EG" data-field-id="36662648" data-annot-id="35974480" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="EF" data-field-id="36662984" data-annot-id="35974672" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="EE" data-field-id="36663320" data-annot-id="35974864" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="ED" data-field-id="36663656" data-annot-id="35975056" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="FH" data-field-id="36663992" data-annot-id="35975248" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="FG" data-field-id="36664328" data-annot-id="35975440" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="FF" data-field-id="36664664" data-annot-id="35975632" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="FE" data-field-id="36665000" data-annot-id="35975824" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="FD" data-field-id="36665336" data-annot-id="35976016" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="FC" data-field-id="36665672" data-annot-id="35976208" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="FB" data-field-id="36666008" data-annot-id="35976400" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="FA" data-field-id="36666344" data-annot-id="35976592" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="GH" data-field-id="36666680" data-annot-id="35976784" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="GG" data-field-id="36667016" data-annot-id="35976976" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="GF" data-field-id="36667352" data-annot-id="35977168" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="GE" data-field-id="36667688" data-annot-id="35977360" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="GD" data-field-id="36668024" data-annot-id="35977552" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="GC" data-field-id="36668360" data-annot-id="35977744" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="GB" data-field-id="36621336" data-annot-id="35971456" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="GA" data-field-id="36657560" data-annot-id="35971648" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="HH" data-field-id="36657896" data-annot-id="35978976" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="HG" data-field-id="36669736" data-annot-id="35979168" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="HF" data-field-id="36670072" data-annot-id="35979360" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="HE" data-field-id="36670408" data-annot-id="35979552" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="HD" data-field-id="36670744" data-annot-id="35979744" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="HC" data-field-id="36671080" data-annot-id="35979936" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="HB" data-field-id="36671416" data-annot-id="35980128" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="HA" data-field-id="36671752" data-annot-id="35980320" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="JH" data-field-id="36672088" data-annot-id="35980512" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="JG" data-field-id="36672424" data-annot-id="35980704" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="JF" data-field-id="36672760" data-annot-id="35980896" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="JE" data-field-id="36673096" data-annot-id="35981088" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="JD" data-field-id="36673432" data-annot-id="35981280" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="JC" data-field-id="36673768" data-annot-id="35981472" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="JB" data-field-id="36674104" data-annot-id="35981664" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="JA" data-field-id="36674440" data-annot-id="35981856" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="KA1" data-field-id="36674776" data-annot-id="35982048" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="KA2" data-field-id="36675112" data-annot-id="35982240" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="KA3" data-field-id="36675448" data-annot-id="35982432" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="KA4" data-field-id="36675784" data-annot-id="35982624" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="KA5" data-field-id="36676120" data-annot-id="35982816" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="KA6" data-field-id="36676456" data-annot-id="35983008" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="KA7" data-field-id="36676792" data-annot-id="35983200" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field " name="KA8" data-field-id="36677128" data-annot-id="35983392" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="KA9" data-field-id="36677464" data-annot-id="35983584" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="KA10" data-field-id="36677800" data-annot-id="35983776" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="LA1" data-field-id="36681160" data-annot-id="35983968" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="LA10" data-field-id="36678136" data-annot-id="35984160" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="LA9" data-field-id="36678472" data-annot-id="35984352" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="LA8" data-field-id="36678808" data-annot-id="35984544" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="LA7" data-field-id="36679144" data-annot-id="35984736" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field " name="LA6" data-field-id="36679480" data-annot-id="35984928" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="LA5" data-field-id="36679816" data-annot-id="35985120" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field " name="LA4" data-field-id="36680152" data-annot-id="35985312" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field " name="LA3" data-field-id="36680488" data-annot-id="35985504" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field " name="LA2" data-field-id="36680824" data-annot-id="35985696" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field " name="MA10" data-field-id="36681496" data-annot-id="35985888" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field " name="MA9" data-field-id="36681832" data-annot-id="35986080" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field " name="MA8" data-field-id="36682168" data-annot-id="35986272" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field " name="MA7" data-field-id="36682504" data-annot-id="35986464" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field " name="MA6" data-field-id="36682840" data-annot-id="35986656" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field " name="MA5" data-field-id="36683176" data-annot-id="35986848" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field " name="MA4" data-field-id="36683512" data-annot-id="35987040" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field " name="MA3" data-field-id="36683848" data-annot-id="35987232" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field " name="MA2" data-field-id="36684184" data-annot-id="35987424" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field " name="MA1" data-field-id="36684520" data-annot-id="35987616" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field " name="NA10" data-field-id="36684856" data-annot-id="35987808" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field " name="NA9" data-field-id="36685192" data-annot-id="35988000" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field " name="NA8" data-field-id="36685528" data-annot-id="35988192" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field " name="NA7" data-field-id="36685864" data-annot-id="35988384" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field " name="NA6" data-field-id="36686200" data-annot-id="35988576" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field " name="NA5" data-field-id="36686536" data-annot-id="35988768" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field " name="NA4" data-field-id="36686872" data-annot-id="35988960" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field " name="NA3" data-field-id="36687208" data-annot-id="35989152" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field " name="NA2" data-field-id="36687544" data-annot-id="35989344" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field " name="NA1" data-field-id="36687880" data-annot-id="35989536" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field " name="PA10" data-field-id="36688216" data-annot-id="35989728" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_125 pdf-obj-fixed acroform-field " name="PA9" data-field-id="36688552" data-annot-id="35989920" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_126 pdf-obj-fixed acroform-field " name="PA8" data-field-id="36688888" data-annot-id="35990112" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_127 pdf-obj-fixed acroform-field " name="PA7" data-field-id="36689224" data-annot-id="35990304" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_128 pdf-obj-fixed acroform-field " name="PA6" data-field-id="36689560" data-annot-id="35990496" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field " name="PA5" data-field-id="36689896" data-annot-id="35990688" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_130 pdf-obj-fixed acroform-field " name="PA4" data-field-id="36668712" data-annot-id="35977936" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_131 pdf-obj-fixed acroform-field " name="PA3" data-field-id="36669048" data-annot-id="35978128" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_132 pdf-obj-fixed acroform-field " name="PA2" data-field-id="36669384" data-annot-id="35978320" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_133 pdf-obj-fixed acroform-field " name="PA1" data-field-id="36692296" data-annot-id="35978512" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_134 pdf-obj-fixed acroform-field " name="QA10" data-field-id="36692632" data-annot-id="35978704" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_135 pdf-obj-fixed acroform-field " name="QA9" data-field-id="36692968" data-annot-id="35992944" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_136 pdf-obj-fixed acroform-field " name="QA8" data-field-id="36693304" data-annot-id="35993136" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_137 pdf-obj-fixed acroform-field " name="QA7" data-field-id="36693640" data-annot-id="35993328" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_138 pdf-obj-fixed acroform-field " name="QA6" data-field-id="36693976" data-annot-id="35993520" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_139 pdf-obj-fixed acroform-field " name="QA5" data-field-id="36694312" data-annot-id="35993712" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_140 pdf-obj-fixed acroform-field " name="QA4" data-field-id="36694648" data-annot-id="35993904" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_141 pdf-obj-fixed acroform-field " name="QA3" data-field-id="36694984" data-annot-id="35994096" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_142 pdf-obj-fixed acroform-field " name="QA2" data-field-id="36695320" data-annot-id="35994288" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_143 pdf-obj-fixed acroform-field " name="QA1" data-field-id="36695656" data-annot-id="35994480" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_144 pdf-obj-fixed acroform-field " name="UA10" data-field-id="36695992" data-annot-id="35994672" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_145 pdf-obj-fixed acroform-field " name="UA9" data-field-id="36696328" data-annot-id="35994864" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field " name="UA8" data-field-id="36705736" data-annot-id="35995056" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_147 pdf-obj-fixed acroform-field " name="UA7" data-field-id="36696664" data-annot-id="35995248" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_148 pdf-obj-fixed acroform-field " name="UA6" data-field-id="36697000" data-annot-id="35995440" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_149 pdf-obj-fixed acroform-field " name="UA5" data-field-id="36697336" data-annot-id="35995632" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_150 pdf-obj-fixed acroform-field " name="UA4" data-field-id="36697672" data-annot-id="35995824" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_151 pdf-obj-fixed acroform-field " name="UA3" data-field-id="36698008" data-annot-id="35996016" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_152 pdf-obj-fixed acroform-field " name="UA2" data-field-id="36698344" data-annot-id="35996208" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_153 pdf-obj-fixed acroform-field " name="UA1" data-field-id="36698680" data-annot-id="35996400" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_154 pdf-obj-fixed acroform-field " name="SA10" data-field-id="36699016" data-annot-id="35996592" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_155 pdf-obj-fixed acroform-field " name="SA9" data-field-id="36699352" data-annot-id="35996784" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_156 pdf-obj-fixed acroform-field " name="SA8" data-field-id="36699688" data-annot-id="35996976" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_157 pdf-obj-fixed acroform-field " name="SA7" data-field-id="36700024" data-annot-id="35997168" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_158 pdf-obj-fixed acroform-field " name="SA6" data-field-id="36700360" data-annot-id="35997360" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_159 pdf-obj-fixed acroform-field " name="SA5" data-field-id="36700696" data-annot-id="35997552" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_160 pdf-obj-fixed acroform-field " name="SA4" data-field-id="36701032" data-annot-id="35997744" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_161 pdf-obj-fixed acroform-field " name="SA3" data-field-id="36701368" data-annot-id="35997936" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_162 pdf-obj-fixed acroform-field " name="SA2" data-field-id="36701704" data-annot-id="35998128" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_163 pdf-obj-fixed acroform-field " name="SA1" data-field-id="36702040" data-annot-id="35998320" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_164 pdf-obj-fixed acroform-field " name="TA10" data-field-id="36702376" data-annot-id="35998512" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_165 pdf-obj-fixed acroform-field " name="TA9" data-field-id="36702712" data-annot-id="35998704" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_166 pdf-obj-fixed acroform-field " name="TA8" data-field-id="36705400" data-annot-id="35998896" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_167 pdf-obj-fixed acroform-field " name="TA7" data-field-id="36703048" data-annot-id="35999088" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_168 pdf-obj-fixed acroform-field " name="TA6" data-field-id="36703384" data-annot-id="35999280" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_169 pdf-obj-fixed acroform-field " name="TA5" data-field-id="36703720" data-annot-id="35999472" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_170 pdf-obj-fixed acroform-field " name="TA4" data-field-id="36704056" data-annot-id="35999664" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_171 pdf-obj-fixed acroform-field " name="TA3" data-field-id="36704392" data-annot-id="35999856" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_172 pdf-obj-fixed acroform-field " name="TA1" data-field-id="36704728" data-annot-id="36000048" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_173 pdf-obj-fixed acroform-field " name="TA2" data-field-id="36705064" data-annot-id="36000240" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_174 pdf-obj-fixed acroform-field " name="LL" data-field-id="36706072" data-annot-id="36000432" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_175 pdf-obj-fixed acroform-field " name="ML" data-field-id="36706408" data-annot-id="36000624" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_176 pdf-obj-fixed acroform-field " name="NL" data-field-id="36706744" data-annot-id="36000816" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_177 pdf-obj-fixed acroform-field " name="PL" data-field-id="36707080" data-annot-id="36001008" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_178 pdf-obj-fixed acroform-field " name="QL" data-field-id="36707416" data-annot-id="36001200" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_179 pdf-obj-fixed acroform-field " name="UL" data-field-id="36707752" data-annot-id="36001392" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_180 pdf-obj-fixed acroform-field " name="SL" data-field-id="36708088" data-annot-id="36001584" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_181 pdf-obj-fixed acroform-field " name="TL" data-field-id="36708424" data-annot-id="36001776" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_182 pdf-obj-fixed acroform-field " name="QP" data-field-id="36708760" data-annot-id="36001968" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_183 pdf-obj-fixed acroform-field " name="QM" data-field-id="36709096" data-annot-id="36002160" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_184 pdf-obj-fixed acroform-field " name="UM" data-field-id="36709432" data-annot-id="36002352" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_185 pdf-obj-fixed acroform-field " name="SM" data-field-id="36709768" data-annot-id="36002544" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_186 pdf-obj-fixed acroform-field " name="TM" data-field-id="36710104" data-annot-id="36002736" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_187 pdf-obj-fixed acroform-field " name="SP" data-field-id="36710440" data-annot-id="36002928" type="text" onChange={roundField(change)} />

            <Field component="input" class="pde-form-field pdf-annot obj_188 pdf-obj-fixed acroform-field pde-form-field-text" name="QN" data-field-id="36710776" data-annot-id="36003120" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_189 pdf-obj-fixed acroform-field pde-form-field-text" name="UN" data-field-id="36711112" data-annot-id="36003312" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_190 pdf-obj-fixed acroform-field pde-form-field-text" name="SN" data-field-id="36711448" data-annot-id="36003504" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_191 pdf-obj-fixed acroform-field pde-form-field-text" name="TN" data-field-id="36711784" data-annot-id="36003696" type="text" disabled />

            <Field component="input" class="pde-form-field pdf-annot obj_192 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="42345336" data-annot-id="41636912" type="text" disabled />

          </div>
        </div>

      </div>
    </div>
  );
};

export default Tax2033C;
