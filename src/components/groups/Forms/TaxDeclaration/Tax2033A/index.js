/* eslint-disable */
import React from "react";
import { Field } from "redux-form";
import {sum, useCompute, setIsNothingness, parse as p} from 'helpers/pdfforms';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from "components/reduxForm/Inputs";

import "./style.scss";

const computeNet1 = ({ AA, BA}) => {
  const c010 = p(AA), c012 = p(BA);
  return c010 - c012;
};

const computeNet2 = ({ AB, BB}) => {
  const c014 = p(AB), c016 = p(BB);
  return c014 - c016;
};

const computeNet3 = ({ AC, BC}) => {
  const c028 = p(AC), c030 = p(BC);
  return c028 - c030;
};

const computeNet4 = ({ AD, BD}) => {
  const c040 = p(AD), c042 = p(BD);
  return c040 - c042;
};

const notNeededFields = {
  'NOM_STEA': true,
  'ADR_STED': true,
  'CP_STEH': true,
  'VILLE_STEI': true,
  'SIRET_STET': true,
  'NBR_M_EX': true,
  'NBR_M_EX-1': true,
  'ADR_STEF': true,
  'ADR_STEG': true,
  'JD': true,
  'FIN_EX': true
};


const Tax2033A = (props) => {
  const { taxDeclarationForm, change } = props;


  setIsNothingness(taxDeclarationForm, notNeededFields, 'JD', change);
  

  useCompute('CA', computeNet1, ['AA', 'BA'], taxDeclarationForm, change); // calculate 010 - 012
  useCompute('CB', computeNet2, ['AB', 'BB'], taxDeclarationForm, change); // calculate 014 - 015
  useCompute('CC', computeNet3, ['AC', 'BC'], taxDeclarationForm, change); // calculate 028 - 030
  useCompute('CD', computeNet4, ['AD', 'BD'], taxDeclarationForm, change); // calculate 040 - 042
  useCompute('AE', sum, ['AA', 'AB', 'AC', 'AD'], taxDeclarationForm, change);
  useCompute('BE', sum, ['BA', 'BB', 'BC', 'BD'], taxDeclarationForm, change);
  useCompute('CE', sum, ['CA', 'CB', 'CC', 'CD'], taxDeclarationForm, change);
  useCompute('CF', sum, ['AF', 'BF'], taxDeclarationForm, change);
  useCompute('CG', sum, ['AG', 'BG'], taxDeclarationForm, change);
  useCompute('CH', sum, ['AH', 'BH'], taxDeclarationForm, change);
  useCompute('CJ', sum, ['AJ', 'BJ'], taxDeclarationForm, change);
  useCompute('CK', sum, ['AK', 'BK'], taxDeclarationForm, change);
  useCompute('CL', sum, ['AL', 'BL'], taxDeclarationForm, change);
  useCompute('CM', sum, ['AM', 'BM'], taxDeclarationForm, change);
  useCompute('CP', sum, ['AP', 'BP'], taxDeclarationForm, change);
  useCompute('AQ', sum, ['AF', 'AG', 'AH', 'AJ', 'AK', 'AL', 'AM', 'AP'], taxDeclarationForm, change);
  useCompute('BQ', sum, ['BF', 'BG', 'BH', 'BJ', 'BK', 'BL', 'BM', 'BP'], taxDeclarationForm, change);
  useCompute('CQ', sum, ['AQ', 'BQ'], taxDeclarationForm, change);
  useCompute('AR', sum, ['AQ', 'AE'], taxDeclarationForm, change);
  useCompute('BR', sum, ['BQ', 'BE'], taxDeclarationForm, change);
  useCompute('CR', sum, ['AR', 'BR'], taxDeclarationForm, change);
  useCompute('FJ', sum, ['FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH'], taxDeclarationForm, change);
  useCompute('FR', sum, ['FL', 'FM', 'FN', 'FP', 'FQ'], taxDeclarationForm, change);
  useCompute('FS', sum, ['FJ', 'FK', 'FR'], taxDeclarationForm, change);
 
  return (
    <div className="form-tax-declaration-2033a">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.293333" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 178, top: 49, width: 189, height: 11, "font-size": 12, }}>
<span>
Formula  re obl  gato  re  art  cle 302 sept  es
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 194, top: 60, width: 156, height: 11, "font-size": 12, }}>
<span>
A b  s du Code général des  mpôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 32, width: 235, height: 45, "font-size": 15, }}>
<span>
N° 15948  01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 525, top: 40, width: 159, height: 19, "font-size": 23, }}>
<span>
BILAN SIMPLIFIÉ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 897, top: 44, width: 46, height: 14, "font-size": 19, }}>
<span>
DGF  P
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 949, top: 41, width: 172, height: 17, "font-size": 23, }}>
<span>
N° 2033-A-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 1416, width: 9, height: 76, "font-size": 12, }}>
<span>
N° 2033-A-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 1274, width: 10, height: 140, "font-size": 12, }}>
<span>
–  SDNC-DGF  P) - Janv  er 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 124, width: 164, height: 15, "font-size": 15, }}>
<span>
Dés  gnat  on de l’entrepr  se
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 154, width: 138, height: 15, "font-size": 15, }}>
<span>
Adresse de l’entrepr  se
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 222, width: 236, height: 12, "font-size": 15, }}>
<span>
Durée de l’exerc  ce en nombre de mo  s
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 328, top: 219, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 497, top: 222, width: 180, height: 15, "font-size": 15, }}>
<span>
Durée de l’exerc  ce précédent
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 680, top: 219, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 190, width: 34, height: 11, "font-size": 15, }}>
<span>
SIRET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1051, top: 113, width: 36, height: 12, "font-size": 15, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1113, top: 109, width: 8, height: 9, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1001, top: 267, width: 90, height: 10, "font-size": 13, }}>
<span>
Exerc  ce N clos le
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 309, top: 309, width: 64, height: 18, "font-size": 23, }}>
<span>
ACTIF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 679, top: 306, width: 25, height: 11, "font-size": 15, }}>
<span>
Brut
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 689, top: 321, width: 3, height: 8, "font-size": 12, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 791, top: 306, width: 167, height: 11, "font-size": 15, }}>
<span>
Amort  ssements-Prov  s  ons
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 872, top: 321, width: 5, height: 8, "font-size": 12, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1035, top: 306, width: 22, height: 11, "font-size": 15, }}>
<span>
Net
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1043, top: 321, width: 5, height: 9, "font-size": 12, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 87, top: 353, width: 13, height: 116, "font-size": 15, }}>
<span>
ACTIF IMMOBILISÉ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 350, width: 97, height: 12, "font-size": 15, }}>
<span>
Immob  l  sat  ons
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 367, width: 79, height: 15, "font-size": 15, }}>
<span>
ncorporelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 253, top: 339, width: 8, height: 51, "font-size": 63, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 266, top: 345, width: 111, height: 12, "font-size": 15, }}>
<span>
Fonds commerc  al
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 380, top: 341, width: 9, height: 9, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 344, width: 18, height: 11, "font-size": 15, }}>
<span>
010
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 344, width: 18, height: 11, "font-size": 15, }}>
<span>
012
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 265, top: 377, width: 40, height: 11, "font-size": 15, }}>
<span>
Autres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 308, top: 373, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 375, width: 18, height: 11, "font-size": 15, }}>
<span>
014
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 375, width: 18, height: 11, "font-size": 15, }}>
<span>
016
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 407, width: 169, height: 15, "font-size": 15, }}>
<span>
Immob  l  sat  ons corporelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 289, top: 404, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 406, width: 18, height: 11, "font-size": 15, }}>
<span>
030
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 406, width: 18, height: 11, "font-size": 15, }}>
<span>
028
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 438, width: 167, height: 12, "font-size": 15, }}>
<span>
Immob  l  sat  ons i nanc  ères
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 287, top: 435, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 300, top: 439, width: 15, height: 13, "font-size": 15, }}>
<span>
1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 437, width: 18, height: 11, "font-size": 15, }}>
<span>
040
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 437, width: 18, height: 11, "font-size": 15, }}>
<span>
042
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 523, top: 468, width: 98, height: 13, "font-size": 15, }}>
<span>
Total I  5)044
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 469, width: 18, height: 11, "font-size": 15, }}>
<span>
048
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 542, width: 11, height: 114, "font-size": 15, }}>
<span>
ACTIF CIRCULANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 497, width: 11, height: 49, "font-size": 15, }}>
<span>
STOCKS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 143, top: 499, width: 397, height: 15, "font-size": 15, }}>
<span>
Mat  ères prem  ères, approv  s  onnements, en cours de product  on
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 543, top: 498, width: 9, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 500, width: 18, height: 11, "font-size": 15, }}>
<span>
050
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 500, width: 18, height: 11, "font-size": 15, }}>
<span>
052
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 143, top: 532, width: 83, height: 12, "font-size": 15, }}>
<span>
Marchand  ses
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 230, top: 529, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 531, width: 18, height: 11, "font-size": 15, }}>
<span>
062
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 531, width: 18, height: 11, "font-size": 15, }}>
<span>
060
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 116, top: 562, width: 272, height: 14, "font-size": 15, }}>
<span>
Avances et acomptes versés sur commandes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 562, width: 18, height: 11, "font-size": 15, }}>
<span>
064
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 562, width: 18, height: 11, "font-size": 15, }}>
<span>
066
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 600, width: 55, height: 11, "font-size": 15, }}>
<span>
Créances
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 617, width: 15, height: 14, "font-size": 15, }}>
<span>
2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 205, top: 589, width: 24, height: 51, "font-size": 63, }}>
<span>
Cl
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 231, top: 595, width: 160, height: 14, "font-size": 15, }}>
<span>
ents et comptes rattachés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 394, top: 591, width: 9, height: 9, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 594, width: 18, height: 11, "font-size": 15, }}>
<span>
068
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 594, width: 18, height: 11, "font-size": 15, }}>
<span>
070
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 218, top: 627, width: 40, height: 11, "font-size": 15, }}>
<span>
Autres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 261, top: 623, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 274, top: 626, width: 15, height: 14, "font-size": 15, }}>
<span>
3)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 625, width: 17, height: 11, "font-size": 15, }}>
<span>
072
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 625, width: 18, height: 11, "font-size": 15, }}>
<span>
074
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 116, top: 655, width: 199, height: 15, "font-size": 15, }}>
<span>
Valeurs mob  l  ères de placement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 656, width: 18, height: 11, "font-size": 15, }}>
<span>
082
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 656, width: 18, height: 11, "font-size": 15, }}>
<span>
080
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 687, width: 83, height: 14, "font-size": 15, }}>
<span>
D  spon  b  l  tés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 687, width: 18, height: 11, "font-size": 15, }}>
<span>
086
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 687, width: 18, height: 11, "font-size": 15, }}>
<span>
084
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 720, width: 175, height: 14, "font-size": 15, }}>
<span>
Charges constatées d’avance
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 296, top: 716, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 718, width: 17, height: 11, "font-size": 15, }}>
<span>
092
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 718, width: 18, height: 11, "font-size": 15, }}>
<span>
094
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 536, top: 750, width: 85, height: 11, "font-size": 15, }}>
<span>
Total II096
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 750, width: 18, height: 11, "font-size": 15, }}>
<span>
098
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 781, width: 172, height: 15, "font-size": 15, }}>
<span>
Total général ( I + II )110
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 788, top: 781, width: 17, height: 11, "font-size": 15, }}>
<span>
112
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 395, top: 810, width: 77, height: 18, "font-size": 23, }}>
<span>
PASSIF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 993, top: 808, width: 54, height: 10, "font-size": 13, }}>
<span>
Exerc  ce N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1078, top: 808, width: 22, height: 10, "font-size": 13, }}>
<span>
NET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1043, top: 822, width: 4, height: 9, "font-size": 12, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 912, width: 12, height: 126, "font-size": 15, }}>
<span>
CAPITAUX PROPRES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 846, width: 164, height: 15, "font-size": 15, }}>
<span>
Cap  tal soc  al ou  nd  v  duel
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 842, width: 9, height: 9, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 845, width: 18, height: 11, "font-size": 15, }}>
<span>
120
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 873, width: 136, height: 14, "font-size": 15, }}>
<span>
Écarts de réévaluat  on
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 876, width: 18, height: 11, "font-size": 15, }}>
<span>
124
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 906, width: 88, height: 15, "font-size": 15, }}>
<span>
Réserve légale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 907, width: 18, height: 11, "font-size": 15, }}>
<span>
126
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 939, width: 142, height: 15, "font-size": 15, }}>
<span>
Réserves réglementées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 262, top: 936, width: 9, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 938, width: 18, height: 11, "font-size": 15, }}>
<span>
130
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 971, width: 513, height: 15, "font-size": 15, }}>
<span>
Autres réserves  dont réserve relat  ve à l’achat d’oeuvres or  g  nales d’art  stes v  vants
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 967, width: 8, height: 9, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 970, width: 16, height: 11, "font-size": 15, }}>
<span>
131
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 969, width: 36, height: 13, "font-size": 15, }}>
<span>
)132
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1001, width: 109, height: 14, "font-size": 15, }}>
<span>
Report à nouveau
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1001, width: 18, height: 11, "font-size": 15, }}>
<span>
134
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1031, width: 128, height: 12, "font-size": 15, }}>
<span>
Résultat de l’exerc  ce
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1032, width: 18, height: 11, "font-size": 15, }}>
<span>
136
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1062, width: 150, height: 15, "font-size": 15, }}>
<span>
Prov  s  ons réglementées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1063, width: 18, height: 11, "font-size": 15, }}>
<span>
140
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 889, top: 1094, width: 74, height: 12, "font-size": 15, }}>
<span>
Total I142
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 1125, width: 209, height: 15, "font-size": 15, }}>
<span>
Prov  s  ons pour r  sques et charges
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 883, top: 1126, width: 80, height: 11, "font-size": 15, }}>
<span>
Total II154
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 1176, width: 14, height: 67, "font-size": 15, }}>
<span>
DETTES (4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1157, width: 183, height: 14, "font-size": 15, }}>
<span>
Emprunts et dettes ass  m  lées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1157, width: 18, height: 11, "font-size": 15, }}>
<span>
156
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1188, width: 323, height: 14, "font-size": 15, }}>
<span>
Avances et acomptes reçus sur commandes en cours
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1188, width: 18, height: 11, "font-size": 15, }}>
<span>
164
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1221, width: 210, height: 14, "font-size": 15, }}>
<span>
Fourn  sseurs et comptes rattachés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 1217, width: 9, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1220, width: 18, height: 11, "font-size": 15, }}>
<span>
166
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 119, top: 1250, width: 426, height: 15, "font-size": 15, }}>
<span>
Autres dettes  dont comptes courants d’assoc  és de l’exerc  ce N : .........
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 1251, width: 18, height: 11, "font-size": 15, }}>
<span>
169
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 1250, width: 36, height: 13, "font-size": 15, }}>
<span>
)172
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1281, width: 171, height: 12, "font-size": 15, }}>
<span>
Produ  ts constatés d’avance
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1282, width: 18, height: 11, "font-size": 15, }}>
<span>
174
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1313, width: 86, height: 11, "font-size": 15, }}>
<span>
Total III176
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 759, top: 1344, width: 204, height: 15, "font-size": 15, }}>
<span>
Total général ( I + II + III )180
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1384, width: 12, height: 56, "font-size": 15, }}>
<span>
RENVOIS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1376, width: 15, height: 13, "font-size": 15, }}>
<span>
1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 1369, width: 254, height: 12, "font-size": 15, }}>
<span>
Dont  mmob  l  sat  ons i nanc  ères à mo  ns
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 144, top: 1382, width: 46, height: 11, "font-size": 15, }}>
<span>
d’un an
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 428, top: 1376, width: 17, height: 11, "font-size": 15, }}>
<span>
193
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 608, top: 1375, width: 193, height: 15, "font-size": 15, }}>
<span>
4)Dont dettes à plus d’un an
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1376, width: 18, height: 11, "font-size": 15, }}>
<span>
195
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1406, width: 209, height: 15, "font-size": 15, }}>
<span>
2)Dont créances à plus d’un an
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 428, top: 1407, width: 17, height: 11, "font-size": 15, }}>
<span>
197
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 608, top: 1422, width: 15, height: 13, "font-size": 15, }}>
<span>
5)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 1400, width: 278, height: 14, "font-size": 15, }}>
<span>
Coût de rev  ent des  mmob  l  sat  ons acqu  ses
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 1413, width: 192, height: 12, "font-size": 15, }}>
<span>
ou créées au cours de l’exerc  ce
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 835, top: 1413, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1407, width: 18, height: 11, "font-size": 15, }}>
<span>
182
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1437, width: 333, height: 15, "font-size": 15, }}>
<span>
3)Dont compte courant d’assoc  és déb  teurs199
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 1431, width: 263, height: 12, "font-size": 15, }}>
<span>
Pr  x de vente hors TVA des  mmob  l  sat  ons
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 1444, width: 176, height: 12, "font-size": 15, }}>
<span>
cédées au cours de l’exerc  ce
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 819, top: 1444, width: 8, height: 8, "font-size": 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1438, width: 18, height: 11, "font-size": 15, }}>
<span>
184
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 375, top: 1473, width: 10, height: 11, "font-size": 23, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 1472, width: 444, height: 13, "font-size": 13, }}>
<span>
Des expl  cat  ons concernant cette rubr  que i gurent dans la not  ce n° 2033-NOT-SD
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="JD" data-field-id="38922712" data-annot-id="38096944" value="Yes" type="checkbox" data-default-value="Off"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="BA" data-field-id="38923080" data-annot-id="38284960" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="CA" data-field-id="38923416" data-annot-id="38021280" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="AB" data-field-id="38923720" data-annot-id="38021472" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="BB" data-field-id="38932248" data-annot-id="38021664" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="CB" data-field-id="38932568" data-annot-id="38021856" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="AC" data-field-id="38932904" data-annot-id="38289456" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="BC" data-field-id="38933240" data-annot-id="38289648" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="CC" data-field-id="38933576" data-annot-id="38289840" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="AD" data-field-id="38934056" data-annot-id="38290176" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="BD" data-field-id="38934392" data-annot-id="38290368" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="CD" data-field-id="38934728" data-annot-id="38290560" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="AE" data-field-id="38935064" data-annot-id="38290752" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="BE" data-field-id="38935400" data-annot-id="38290944" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="CE" data-field-id="38935736" data-annot-id="38291136" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="AF" data-field-id="38936072" data-annot-id="38291328" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="BF" data-field-id="38936408" data-annot-id="38291520" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="CF" data-field-id="38936888" data-annot-id="38291984" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="AG" data-field-id="38937224" data-annot-id="38292176" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="BG" data-field-id="38937560" data-annot-id="38292368" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="CG" data-field-id="38937896" data-annot-id="38292560" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="AH" data-field-id="38938232" data-annot-id="38292752" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="BH" data-field-id="38938568" data-annot-id="38292944" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="CH" data-field-id="38938904" data-annot-id="38293136" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="AJ" data-field-id="38939240" data-annot-id="38293328" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="BJ" data-field-id="38939576" data-annot-id="38293520" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="CJ" data-field-id="38939912" data-annot-id="38293712" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="AK" data-field-id="38940248" data-annot-id="38293904" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="BK" data-field-id="38940584" data-annot-id="38294096" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="CK" data-field-id="38940920" data-annot-id="38294288" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="AL" data-field-id="38941256" data-annot-id="38294480" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="BL" data-field-id="38941592" data-annot-id="38294672" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="CL" data-field-id="38941928" data-annot-id="38294864" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="AM" data-field-id="38936744" data-annot-id="38291712" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="BM" data-field-id="38942856" data-annot-id="38295584" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="CM" data-field-id="38943192" data-annot-id="38295776" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="AP" data-field-id="38943528" data-annot-id="38295968" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="BP" data-field-id="38943864" data-annot-id="38296160" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="CP" data-field-id="38944200" data-annot-id="38296352" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="AQ" data-field-id="38944536" data-annot-id="38296544" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="BQ" data-field-id="38944872" data-annot-id="38296736" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="CQ" data-field-id="38945208" data-annot-id="38296928" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="AR" data-field-id="38945544" data-annot-id="38297120" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="BR" data-field-id="38945880" data-annot-id="38297312" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="CR" data-field-id="38946216" data-annot-id="38297504" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="FA" data-field-id="38946552" data-annot-id="38297696" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="FB" data-field-id="38946888" data-annot-id="38297888" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="FC" data-field-id="38947224" data-annot-id="38298080" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="FD" data-field-id="38947560" data-annot-id="38298272" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="FE" data-field-id="38948232" data-annot-id="38298464" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="EB" data-field-id="38947896" data-annot-id="38298656" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="FF" data-field-id="38948568" data-annot-id="38298848" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="FG" data-field-id="38948904" data-annot-id="38299040" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="FH" data-field-id="38949240" data-annot-id="38299232" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="FJ" data-field-id="38949576" data-annot-id="38299424" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="FK" data-field-id="38949912" data-annot-id="38299616" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="FL" data-field-id="38950248" data-annot-id="38299808" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="FM" data-field-id="38950584" data-annot-id="38300000" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="FN" data-field-id="38950920" data-annot-id="38300192" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="FP" data-field-id="38951256" data-annot-id="38300384" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="FQ" data-field-id="38951592" data-annot-id="38300576" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="FR" data-field-id="38951928" data-annot-id="38300768" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="FS" data-field-id="38952264" data-annot-id="38300960" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="EE" data-field-id="38952600" data-annot-id="38301152" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="JA" data-field-id="38952936" data-annot-id="38301344" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="JB" data-field-id="38922536" data-annot-id="38295056" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="JC" data-field-id="38942232" data-annot-id="38295248" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="HA" data-field-id="38954184" data-annot-id="38302576" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="HB" data-field-id="38954424" data-annot-id="38302768" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="HC" data-field-id="38954760" data-annot-id="38302960" type="text"  />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="38956440" data-annot-id="38303152" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="AA" data-field-id="38955096" data-annot-id="38303344" type="text"  />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STED" data-field-id="38957784" data-annot-id="38303536" type="text"  />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field pde-form-field-text" name="CP_STEH" data-field-id="38957112" data-annot-id="38303728" type="text"  />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field pde-form-field-text" name="VILLE_STEI" data-field-id="38957448" data-annot-id="38303920" type="text"  />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field pde-form-field-text" name="SIRET_STET" data-field-id="38956776" data-annot-id="38304112" type="text"  />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field pde-form-field-text" name="NBR_M_EX" data-field-id="38955432" data-annot-id="38304304" type="text"  />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field pde-form-field-text" name="NBR_M_EX-1" data-field-id="38955768" data-annot-id="38304496" type="text"  />

            <Field disabled component={ReduxPicker} handleChange={change} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="FIN_EX" data-field-id="38956104" data-annot-id="38304688" type="text"  />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEF" data-field-id="38959656" data-annot-id="38304880" type="text"  />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEG" data-field-id="38959352" data-annot-id="38305072" type="text"  />

          </div>
        </div>

      </div>
    </div>
  );
}

export default Tax2033A;
