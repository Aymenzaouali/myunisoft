import React from "react";
import { Field } from 'redux-form';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { get as _get } from "lodash";
import { setValue } from 'helpers/pdfforms';

import './style.scss';

const Tax2035 = (props) => {
  const { change, form, form2035B } = props;

  const CP = _get(form2035B, 'CP', null); // 46 from 2035B
  const CR = _get(form2035B, 'CR', null); // 47 from 2035B

  if (CP) {
    if (CP > 0) {
      setValue(form, 'FG', CP, change);
      setValue(form, 'FH', '', change);
    } else if (CP < 0) {
      setValue(form, 'FH', CP, change);
      setValue(form, 'FG', '', change);
    }
  } else if (CR) {
    if (CR > 0) {
      setValue(form, 'FG', CR, change);
      setValue(form, 'FH', '', change);
    } else if (CR < 0) {
      setValue(form, 'FH', CR, change);
      setValue(form, 'FG', '', change);
    }
  }

  return (
    <div className="form-tax-declaration-2035">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 66, top: 65, width: 355, height: 15, fontSize: 15, }}>
<span>
DIRECTION GÉNÉRALE DES FINANCES PUBLIQUES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 261, width: 163, height: 13, fontSize: 17, }}>
<span>
Adresse du déclarant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 361, width: 152, height: 12, fontSize: 17, }}>
<span>
Adresse du déclarant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 459, width: 69, height: 13, fontSize: 17, }}>
<span>
N° SIRET
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 663, top: 459, width: 93, height: 13, fontSize: 17, }}>
<span>
Adresse mail
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 660, top: 491, width: 114, height: 15, fontSize: 17, }}>
<span>
N° de téléphone
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 878, width: 172, height: 12, fontSize: 13, }}>
<span>
● d&#39;une société civile de moyens
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 751, top: 914, width: 22, height: 12, fontSize: 17, }}>
<span>
AU
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1114, width: 693, height: 17, fontSize: 17, }}>
<span>
- Charges : les charges ou dépenses ayant la nature de moins-value définie à l&#39;art. 39 duodécies :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 1195, width: 974, height: 16, fontSize: 17, }}>
<span>
1 bis- Résultat net de cession, de concession ou de sous-concession de brevets et actifs incorporels assimilés (art. 238 du CGI)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 61, top: 1272, width: 116, height: 13, fontSize: 17, }}>
<span>
2- Plus-value ©
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 853, top: 89, width: 270, height: 12, fontSize: 17, }}>
<span>
REVENUS NON COMMERCIAUX ET
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 885, top: 104, width: 205, height: 15, fontSize: 17, }}>
<span>
ASSIMILÉS RÉGIME DE LA
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 879, top: 122, width: 218, height: 15, fontSize: 17, }}>
<span>
DÉCLARATION CONTRÔLÉE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 116, top: 147, width: 90, height: 13, fontSize: 17, }}>
<span>
N° 11176*22
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 898, top: 156, width: 138, height: 13, fontSize: 17, }}>
<span>
N° 2035-SD – 2020
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 204, width: 117, height: 12, fontSize: 17, }}>
<span>
Nom et Prénom
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 410, width: 178, height: 15, fontSize: 15, }}>
<span>
(Quand elle est différente de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 426, width: 152, height: 12, fontSize: 15, }}>
<span>
l&#39;adresse du destinataire
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 523, width: 1109, height: 14, fontSize: 15, }}>
<span>
Attention : Toutes les entreprises soumises à un régime réel d&#39;imposition en matière de résultats ont l&#39;obligation de déposer par voie dématérialisée leur déclaration de résultats
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 538, width: 1109, height: 15, fontSize: 15, }}>
<span>
et ses annexes. Le non respect de cette obligation est sanctionné par l&#39;application de la majoration de 0,2 % prévue par l&#39;article 1738 du code général des impôts. Vous trouverez
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 554, width: 481, height: 15, fontSize: 15, }}>
<span>
toutes les informations utiles pour télédéclarer sur le site www.impots.gouv.fr.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 587, width: 421, height: 16, fontSize: 17, }}>
<span>
Indiquez ci-contre les éventuelles modifications intervenues
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 71, top: 606, width: 472, height: 16, fontSize: 17, }}>
<span>
(ancienne adresse en cas de changement au 1er janvier précédent,
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 287, top: 624, width: 39, height: 16, fontSize: 17, }}>
<span>
etc.) :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 665, width: 252, height: 13, fontSize: 17, }}>
<span>
Adresse des cabinets secondaires :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 705, width: 245, height: 13, fontSize: 17, }}>
<span>
Adresse du domicile du déclarant :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 745, width: 139, height: 13, fontSize: 17, }}>
<span>
Nature de l&#39;activité :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 660, top: 736, width: 215, height: 13, fontSize: 17, }}>
<span>
Date de début de l&#39;exercice de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 660, top: 754, width: 98, height: 16, fontSize: 17, }}>
<span>
la profession :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 782, width: 606, height: 19, fontSize: 17, }}>
<span>
SI VOUS ÊTES MEMBRE : Dénomination et adresse du groupement, de la société :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 819, width: 190, height: 12, fontSize: 13, }}>
<span>
● d&#39;une société ou d&#39;un groupement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 832, width: 189, height: 12, fontSize: 13, }}>
<span>
exerçant une activité libérale et non
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 846, width: 166, height: 12, fontSize: 13, }}>
<span>
soumis à l&#39;impôt sur les société
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 911, width: 531, height: 15, fontSize: 17, }}>
<span>
RENSEIGNEMENTS RELATIFS À L&#39;ANNÉE 2019 OU À LA PÉRIODE DU
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 937, top: 908, width: 231, height: 12, fontSize: 13, }}>
<span>
(si l&#39;activité a commencé ou cessé en cours
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1029, top: 922, width: 47, height: 12, fontSize: 13, }}>
<span>
d&#39;année)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 939, width: 925, height: 19, fontSize: 17, }}>
<span>
RÉCAPITULATION DES ÉLÉMENTS D&#39;IMPOSITION (Ces résultats sont à reporter sur la déclaration de revenus n° 2042C-PRO)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 960, width: 143, height: 11, fontSize: 15, }}>
<span>
Voir renvois à la notice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 989, width: 469, height: 16, fontSize: 17, }}>
<span>
1- Résultat fiscal (report des lignes 46 ou 47 de l&#39;annexe 2035-B)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 601, top: 989, width: 75, height: 13, fontSize: 17, }}>
<span>
Bénéfice :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 874, top: 989, width: 57, height: 13, fontSize: 17, }}>
<span>
Déficit :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 1035, width: 856, height: 16, fontSize: 17, }}>
<span>
Prélèvement à la source : Produits et charges exclus du calcul des acomptes d&#39;impôt sur le revenu à compter de 2020 :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1065, width: 842, height: 16, fontSize: 17, }}>
<span>
- Produits : quote-part de subvention d&#39;équipement et indemnités d&#39;assurance compensant la perte d&#39;un élément d&#39;actif
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 1083, width: 654, height: 17, fontSize: 17, }}>
<span>
immobilisé, produits ou recettes ayant la nature de plus-values définies à l&#39;art. 39 duodécies.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 1155, width: 484, height: 16, fontSize: 17, }}>
<span>
Revenus de capitaux mobiliers (y compris les crédits d&#39;impôt) (21)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 1235, width: 285, height: 16, fontSize: 17, }}>
<span>
Résultat net imposé au taux de 10 % :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 199, top: 1271, width: 89, height: 16, fontSize: 17, }}>
<span>
à long terme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 197, top: 1289, width: 94, height: 16, fontSize: 17, }}>
<span>
imposable au
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 190, top: 1307, width: 107, height: 15, fontSize: 17, }}>
<span>
taux de 12,8 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 439, top: 1271, width: 169, height: 16, fontSize: 17, }}>
<span>
à long terme exonérées
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 451, top: 1289, width: 145, height: 16, fontSize: 17, }}>
<span>
(art. 238 quindecies)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 1271, width: 125, height: 16, fontSize: 17, }}>
<span>
à long terme dont
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 768, top: 1289, width: 162, height: 16, fontSize: 17, }}>
<span>
l&#39;imposition est différée
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 795, top: 1307, width: 108, height: 16, fontSize: 17, }}>
<span>
de 2ans (art 39
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 1325, width: 105, height: 16, fontSize: 17, }}>
<span>
quindecies I-1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 199, top: 1371, width: 89, height: 16, fontSize: 17, }}>
<span>
à long terme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 190, top: 1389, width: 107, height: 16, fontSize: 17, }}>
<span>
exonérées (art.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 202, top: 1407, width: 85, height: 16, fontSize: 17, }}>
<span>
151 septies)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 439, top: 1371, width: 169, height: 16, fontSize: 17, }}>
<span>
à long terme exonérées
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 456, top: 1389, width: 135, height: 16, fontSize: 17, }}>
<span>
(art. 151 septies A)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 804, top: 1371, width: 89, height: 16, fontSize: 17, }}>
<span>
à long terme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 781, top: 1389, width: 133, height: 16, fontSize: 17, }}>
<span>
exonérées (art 151
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 813, top: 1407, width: 71, height: 16, fontSize: 17, }}>
<span>
septies B)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 109, width: 62, height: 22, fontSize: 24, }}>
<span>
cerfa
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field align-left"
                   name="NOM_STEA" data-field-id="27888696" data-annot-id="27367552" maxLength="8" type="text"
                   disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field align-left"
                   name="ADR_STED" data-field-id="27888904" data-annot-id="27367744" readonly type="text"
                   disabled/>

            <textarea className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field align-left"
                      name="ADR_STEG" data-field-id="27911608" data-annot-id="27367936" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field align-left"
                   name="CP_STEH" data-field-id="27911912" data-annot-id="27368128" readonly type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field align-left"
                   name="VILLE_STEI" data-field-id="27912248" data-annot-id="27368320" readonly type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field align-left"
                   name="ADR_STEF" data-field-id="27912584" data-annot-id="27368512" readonly type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field align-left"
                   name="SIRET_STET" data-field-id="27912920" data-annot-id="27368704" maxLength="14" type="text"
                   disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field align-left"
                   name="TEL_STE" data-field-id="27913256" data-annot-id="27368896" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field align-left"
                   name="MAIL_STE" data-field-id="27913592" data-annot-id="27369088" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field align-left" name="CA"
                   data-field-id="27914072" data-annot-id="27369424" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field align-left" name="BA"
                   data-field-id="27916088" data-annot-id="27369616" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field align-left" name="DA"
                   data-field-id="27915752" data-annot-id="27369808" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field align-left" name="EA"
                   data-field-id="27914408" data-annot-id="27370000" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field align-left" name="FA"
                   data-field-id="27916424" data-annot-id="27370192" type="text" disabled/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="ME"
                   data-field-id="27914744" data-annot-id="27370384" type="text" disabled/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "
                   name="FIN_EX" data-field-id="27915080" data-annot-id="27370576" type="text" disabled/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="DEB_EX" data-field-id="27915416" data-annot-id="27370768" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="FG"
                   data-field-id="27916904" data-annot-id="27371232" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="FH"
                   data-field-id="27917240" data-annot-id="27371424" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="AA"
                   data-field-id="27917576" data-annot-id="27371616" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="AB"
                   data-field-id="27917912" data-annot-id="27371808" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="FV"
                   data-field-id="27918248" data-annot-id="27372000" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="NF"
                   data-field-id="27926072" data-annot-id="27372192" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="ND"
                   data-field-id="27930440" data-annot-id="27372384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="NP"
                   data-field-id="27918584" data-annot-id="27372576" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="NN"
                   data-field-id="27918920" data-annot-id="27372768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="FJ"
                   data-field-id="27919256" data-annot-id="27372960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="NR"
                   data-field-id="27919592" data-annot-id="27373152" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 471, top: 67, width: 105, height: 13, fontSize: 17, }}>
<span>
sur le bénéfice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 338, width: 206, height: 16, fontSize: 17, }}>
<span>
4- BNC non professionnels
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 422, top: 338, width: 62, height: 13, fontSize: 17, }}>
<span>
Bénéfice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 709, top: 338, width: 45, height: 13, fontSize: 17, }}>
<span>
Déficit
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 953, top: 338, width: 74, height: 13, fontSize: 17, }}>
<span>
Plus-value
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 688, top: 494, width: 174, height: 13, fontSize: 17, }}>
<span>
Plus-value à court-terme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 602, width: 693, height: 17, fontSize: 17, }}>
<span>
- Charges : les charges ou dépenses ayant la nature de moins-value définie à l&#39;art. 39 duodécies :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 681, width: 145, height: 12, fontSize: 17, }}>
<span>
Viseur conventionné
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 350, top: 680, width: 98, height: 13, fontSize: 17, }}>
<span>
AA ou OMGA
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 729, width: 313, height: 16, fontSize: 17, }}>
<span>
- du professionnel de l&#39;expertise comptable :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 753, width: 90, height: 13, fontSize: 17, }}>
<span>
- du conseil :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 803, width: 279, height: 16, fontSize: 17, }}>
<span>
- N° d&#39;agrément de l&#39;AA ou de l&#39;OMGA :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 829, width: 249, height: 16, fontSize: 17, }}>
<span>
Signature et qualité du déclarant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 860, width: 11, height: 15, fontSize: 17, }}>
<span>
À
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 268, top: 863, width: 20, height: 15, fontSize: 17, }}>
<span>
, le
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 79, width: 315, height: 16, fontSize: 17, }}>
<span>
3- Exonérations et abattements © et (21 )
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 98, width: 364, height: 16, fontSize: 17, }}>
<span>
pratiqués (cocher la case ci-dessus correspondant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 116, width: 119, height: 16, fontSize: 17, }}>
<span>
à votre situation)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 68, width: 147, height: 15, fontSize: 17, }}>
<span>
sur les plus-values à
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 86, width: 152, height: 15, fontSize: 17, }}>
<span>
long terme imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 780, top: 103, width: 137, height: 15, fontSize: 17, }}>
<span>
aux taux de 12,8 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 166, width: 72, height: 16, fontSize: 17, }}>
<span>
Entreprise
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 184, width: 109, height: 15, fontSize: 17, }}>
<span>
nouvelle, art 44
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 91, top: 202, width: 54, height: 13, fontSize: 17, }}>
<span>
sexies :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 231, top: 166, width: 234, height: 13, fontSize: 17, }}>
<span>
Activité exercée en zone franche
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 240, top: 184, width: 218, height: 16, fontSize: 17, }}>
<span>
urbaine, territoire entrepreneur
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 244, top: 202, width: 207, height: 13, fontSize: 17, }}>
<span>
Art. 44 octies ou 44 octies A :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 532, top: 166, width: 130, height: 16, fontSize: 17, }}>
<span>
Autres dispositifs :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 166, width: 144, height: 16, fontSize: 17, }}>
<span>
Date de création (ou
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 751, top: 184, width: 154, height: 16, fontSize: 17, }}>
<span>
d&#39;entrée) dans un des
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 744, top: 202, width: 167, height: 16, fontSize: 17, }}>
<span>
régimes visés ci-avant :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 235, width: 72, height: 15, fontSize: 17, }}>
<span>
Entreprise
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 253, width: 109, height: 14, fontSize: 17, }}>
<span>
nouvelle, art.44
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 81, top: 270, width: 76, height: 16, fontSize: 17, }}>
<span>
quindecies
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 235, width: 237, height: 16, fontSize: 17, }}>
<span>
Activité éligible à l&#39;exonération en
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 240, top: 252, width: 211, height: 16, fontSize: 17, }}>
<span>
faveur des jeunes entreprises
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 248, top: 270, width: 200, height: 15, fontSize: 17, }}>
<span>
innovantes, art 44 sexies A :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 524, top: 234, width: 141, height: 15, fontSize: 17, }}>
<span>
Zone franche DOM,
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 528, top: 253, width: 139, height: 15, fontSize: 17, }}>
<span>
Art 44 quaterdecies
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 731, top: 234, width: 194, height: 17, fontSize: 17, }}>
<span>
Date de début d’activité (ou
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 731, top: 252, width: 194, height: 16, fontSize: 17, }}>
<span>
de création) dans le régime
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 778, top: 270, width: 98, height: 13, fontSize: 17, }}>
<span>
visé ci-avant :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 394, width: 238, height: 13, fontSize: 17, }}>
<span>
Exonérations sur le bénéfice non-
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 412, width: 94, height: 16, fontSize: 17, }}>
<span>
professionnel
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 642, top: 394, width: 267, height: 13, fontSize: 17, }}>
<span>
Dont exonération sur le bénéfice non-
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 656, top: 412, width: 237, height: 16, fontSize: 17, }}>
<span>
professionnel « jeunes artistes » :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 60, top: 485, width: 326, height: 16, fontSize: 17, }}>
<span>
Plus-value à long-terme imposable au taux de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 199, top: 503, width: 49, height: 15, fontSize: 17, }}>
<span>
12,8 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 551, width: 842, height: 16, fontSize: 17, }}>
<span>
- Produits : quote-part de subvention d&#39;équipement et indemnités d&#39;assurance compensant la perte d&#39;un élément d&#39;actif
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 569, width: 654, height: 16, fontSize: 17, }}>
<span>
immobilisé, produits ou recettes ayant la nature de plus-values définies à l&#39;art. 39 duodécies.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 649, width: 314, height: 16, fontSize: 17, }}>
<span>
Votre comptabilité est-elle informatisée ?
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 649, width: 209, height: 16, fontSize: 17, }}>
<span>
Si oui, nom du logiciel utilisé :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 707, width: 228, height: 14, fontSize: 15, }}>
<span>
Nom, adresse, téléphone, télécopie :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 777, width: 648, height: 16, fontSize: 17, }}>
<span>
- l&#39;association agréée ou de l&#39;organisme mixte de gestion agréé ou du viseur conventionné :
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="NG"
                   data-field-id="27920024" data-annot-id="26899136" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="NH"
                   data-field-id="27922920" data-annot-id="27288640" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="NF"
                   data-field-id="27926072" data-annot-id="27009040" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="ND"
                   data-field-id="27930440" data-annot-id="27009232" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change}
                   className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="NA"
                   data-field-id="27934552" data-annot-id="26942704" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change}
                   className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="PA"
                   data-field-id="27939032" data-annot-id="26942896" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change}
                   className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="NB"
                   data-field-id="27943752" data-annot-id="26943088" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change}
                   className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="NE"
                   data-field-id="27948968" data-annot-id="26943280" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change}
                   className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="NC"
                   data-field-id="27953384" data-annot-id="26948144" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change}
                   className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="NW"
                   data-field-id="27958072" data-annot-id="26948336" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="AP"
                   data-field-id="27962760" data-annot-id="26948528" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="AL"
                   data-field-id="27965992" data-annot-id="26948720" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="AM"
                   data-field-id="27969272" data-annot-id="26948912" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="AJ"
                   data-field-id="27972552" data-annot-id="26949104" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="AF"
                   data-field-id="27975832" data-annot-id="26949296" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="AN"
                   data-field-id="27979112" data-annot-id="26949488" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="AR"
                   data-field-id="27982392" data-annot-id="26949680" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="AG"
                   data-field-id="27985672" data-annot-id="26950144" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="AH"
                   data-field-id="27988952" data-annot-id="26950336" type="text" autoComplete="nope"/>

            <Field component={PdfCheckbox} onCustomChange={change}
                   className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="AD"
                   data-field-id="27992232" data-annot-id="26950528" value="X" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="AE"
                   data-field-id="27997240" data-annot-id="26950720" type="text" value="MyUnisoft" autoComplete="nope"/>

            <Field component={PdfCheckbox} onCustomChange={change}
                   className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="PE"
                   data-field-id="28000328" data-annot-id="26950912" value="X" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change}
                   className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="PD"
                   data-field-id="28005384" data-annot-id="26951104" value="X" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field align-left"
                   name="NUM_CTREGEST" data-field-id="28010376" data-annot-id="26951296" maxLength="6" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field align-left" name="HA"
                   data-field-id="28023768" data-annot-id="26951488" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field "
                   name="DATE_SIGNATURE" data-field-id="28014104" data-annot-id="26951680" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field align-left"
                   name="LIEU_SIGNATURE" data-field-id="28017240" data-annot-id="26951872" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field align-left" name="GA"
                   data-field-id="28027464" data-annot-id="26952064" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field align-left" name="GA"
                   data-field-id="28027464" data-annot-id="26952256" type="text" autoComplete="nope"/>

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2035;
