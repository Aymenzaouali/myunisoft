import React from 'react';
import { Field } from 'redux-form';

import { useCompute, sum, sub, setIsNothingness } from 'helpers/pdfforms';

import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const notNeededFields = {
  'NOM_STEA': true,
  'PG': true
};
// Component
const Tax2053 = props => {
  const { formValues, change } = props;

  setIsNothingness(formValues, notNeededFields, 'PG', change);
  // Compute
  useCompute('HD', sum, ['HA', 'HB', 'HC'], formValues, change);
  useCompute('HH', sum, ['HE', 'HF', 'HG'], formValues, change);
  useCompute('HI', sub, ['HD', 'HH'], formValues, change);
  useCompute('HL', sum, ['HD', 'FR', 'GH', 'GP'], formValues, change);
  useCompute('HM', sum, ['HH', 'HJ', 'HK', 'GF', 'GI', 'GU'], formValues, change);

  // Rendering
  return (
    <div className="tax2053">
      <div id="pdf-page-1" className="pdf-page">
        <div className="pdf-page-inner pdf-page-1">
          <div className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 373, top: 86, width: 564, height: 30, fontSize: 27 }}>
              <span>COMPTE DE RÉSULTAT DE L’EXERCICE (Suite)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 128, width: 157, height: 11, fontSize: 12 }}>
              <span>Formulaire obligatoire (article 53 A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 110, top: 139, width: 127, height: 12, fontSize: 12 }}>
              <span>du Code général des impôts)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 86, width: 218, height: 16, fontSize: 20 }}>
              <span>DGFiP N° 2053-SD 2019</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 181, width: 157, height: 15, fontSize: 16 }}>
              <span>Désignation de l’entreprise</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 1641, width: 429, height: 14, fontSize: 14 }}>
              <span>* Des explications concernant cette rubrique sont données dans la notice n° 2032.</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 160, top: 275, width: 288, height: 15, fontSize: 16 }}>
              <span>Produits exceptionnels sur opérations de gestion</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 272, width: 19, height: 14, fontSize: 20 }}>
              <span>HA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 160, top: 309, width: 294, height: 15, fontSize: 16 }}>
              <span>Produits exceptionnels sur opérations en capital *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 307, width: 18, height: 13, fontSize: 20 }}>
              <span>HB</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 160, top: 343, width: 278, height: 16, fontSize: 16 }}>
              <span>Reprises sur provisions et transferts de charges</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 341, width: 18, height: 14, fontSize: 20 }}>
              <span>HC</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 642, top: 377, width: 216, height: 16, fontSize: 16 }}>
              <span>Total des produits exceptionnels (7)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 928, top: 377, width: 27, height: 15, fontSize: 16 }}>
              <span>(VII)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 376, width: 20, height: 13, fontSize: 20 }}>
              <span>HD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 161, top: 412, width: 335, height: 16, fontSize: 16 }}>
              <span>Charges exceptionnelles sur opérations de gestion (6 bis )</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 410, width: 17, height: 14, fontSize: 20 }}>
              <span>HE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 161, top: 447, width: 300, height: 15, fontSize: 16 }}>
              <span>Charges exceptionnelles sur opérations en capital *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 445, width: 17, height: 13, fontSize: 20 }}>
              <span>HF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 160, top: 481, width: 396, height: 15, fontSize: 16 }}>
              <span>Dotations exceptionnelles aux amortissements et provisions (6 ter )</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 479, width: 20, height: 14, fontSize: 20 }}>
              <span>HG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 627, top: 515, width: 223, height: 16, fontSize: 16 }}>
              <span>Total des charges exceptionnelles (7)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 923, top: 515, width: 32, height: 15, fontSize: 16 }}>
              <span>(VIII)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 514, width: 20, height: 13, fontSize: 20 }}>
              <span>HH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 547, width: 301, height: 16, fontSize: 15 }}>
              <span>4 - RÉSULTAT EXCEPTIONNEL (VII - VIII)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 548, width: 14, height: 13, fontSize: 20 }}>
              <span>HI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 585, width: 310, height: 15, fontSize: 16 }}>
              <span>Participation des salariés aux résultats de l’entreprise</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 934, top: 584, width: 21, height: 14, fontSize: 16 }}>
              <span>(IX)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 583, width: 14, height: 17, fontSize: 20 }}>
              <span>HJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 619, width: 153, height: 15, fontSize: 16 }}>
              <span>Impôts sur les bénéfices *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 938, top: 618, width: 17, height: 15, fontSize: 16 }}>
              <span>(X)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 617, width: 19, height: 13, fontSize: 20 }}>
              <span>HK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 653, width: 263, height: 14, fontSize: 16 }}>
              <span>TOTAL DES PRODUITS (I + III + V + VII)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 652, width: 17, height: 13, fontSize: 20 }}>
              <span>HL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 687, width: 327, height: 15, fontSize: 16 }}>
              <span>TOTAL DES CHARGES (II + IV + VI + VIII + IX + X)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 686, width: 21, height: 13, fontSize: 20 }}>
              <span>HM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 719, width: 464, height: 18, fontSize: 15 }}>
              <span>5 - BÉNÉFICE OU PERTE (Total des produits - total des charges )</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 721, width: 19, height: 13, fontSize: 20 }}>
              <span>HN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 757, width: 358, height: 15, fontSize: 16 }}>
              <span>(1) Dont produits nets partiels sur opérations à long terme</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 755, width: 20, height: 13, fontSize: 20 }}>
              <span>HO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 1032, width: 463, height: 16, fontSize: 16 }}>
              <span>(6 bis ) Dont dons faits aux organismes d’intérêt général (art. 238 bis du C.G.I.)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1031, width: 19, height: 13, fontSize: 20 }}>
              <span>HX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1068, width: 522, height: 15, fontSize: 16 }}>
              <span>(6ter ) Dont amortissements des souscriptions dans des PME innovantes (art. 217 octies)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1066, width: 16, height: 14, fontSize: 20 }}>
              <span>RC</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 1103, width: 549, height: 15, fontSize: 16 }}>
              <span>Dont amortissements exceptionnel de 25% des constructions nouvelles (art. 39 quinquies D)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1101, width: 18, height: 13, fontSize: 20 }}>
              <span>RD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 1132, width: 190, height: 15, fontSize: 16 }}>
              <span>(9) Dont transferts de charges</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1130, width: 16, height: 14, fontSize: 20 }}>
              <span>A1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1168, width: 323, height: 15, fontSize: 16 }}>
              <span>(10) Dont cotisations personnelles de l’exploitant (13)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1166, width: 16, height: 13, fontSize: 20 }}>
              <span>A2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 1262, width: 165, height: 15, fontSize: 16 }}>
              <span>Dont primes et cotisations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 1277, width: 269, height: 15, fontSize: 16 }}>
              <span>complémentaires personnelles : facultatives</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 751, top: 1277, width: 72, height: 15, fontSize: 16 }}>
              <span>obligatoires</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1201, width: 438, height: 15, fontSize: 16 }}>
              <span>(11) Dont redevances pour concessions de brevets, de licences (produits)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1199, width: 17, height: 15, fontSize: 20 }}>
              <span>A3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1236, width: 433, height: 15, fontSize: 16 }}>
              <span>(12) Dont redevances pour concessions de brevets, de licences (charges)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1232, width: 17, height: 15, fontSize: 20 }}>
              <span>A4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 1270, width: 17, height: 15, fontSize: 16 }}>
              <span>(13)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 1305, width: 16, height: 15, fontSize: 16 }}>
              <span>(7)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 1466, width: 306, height: 16, fontSize: 16 }}>
              <span>(8) Détail des produits et charges sur exercices antérieurs :</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 214, top: 791, width: 205, height: 16, fontSize: 16 }}>
              <span>produits de locations immobilières</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 789, width: 19, height: 14, fontSize: 20 }}>
              <span>HY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 214, top: 826, width: 491, height: 15, fontSize: 16 }}>
              <span>produits d’exploitation afférents à des exercices antérieurs (à détailler au (8) ci- dessous)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 929, width: 558, height: 15, fontSize: 16 }}>
              <span>(4) Dont charges d’exploitation afférentes à des exercices antérieurs (à détailler au (8) ci- dessous)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1296, width: 48, height: 9, fontSize: 12 }}>
              <span>Exercice N</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 865, top: 1313, width: 101, height: 12, fontSize: 12 }}>
              <span>Charges exceptionnelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1039, top: 1313, width: 97, height: 12, fontSize: 12 }}>
              <span>Produits exceptionnels</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1041, top: 236, width: 62, height: 10, fontSize: 13 }}>
              <span>Exercice N</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 108, top: 300, width: 10, height: 62, fontSize: 14 }}>
              <span>PRODUITS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 281, width: 10, height: 99, fontSize: 14 }}>
              <span>EXCEPTIONNELS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 108, top: 440, width: 10, height: 57, fontSize: 14 }}>
              <span>CHARGES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 413, width: 10, height: 112, fontSize: 14 }}>
              <span>EXCEPTIONNELLES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 1144, width: 11, height: 62, fontSize: 16 }}>
              <span>RENVOIS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1458, width: 48, height: 9, fontSize: 12 }}>
              <span>Exercice N</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 874, top: 1475, width: 82, height: 12, fontSize: 12 }}>
              <span>Charges antérieures</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1047, top: 1475, width: 80, height: 9, fontSize: 12 }}>
              <span>Produits antérieurs</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 808, width: 56, height: 15, fontSize: 16 }}>
              <span>(2) Dont</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 778, width: 7, height: 54, fontSize: 68 }}>
              <span>&lbrace;</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 210, top: 860, width: 144, height: 12, fontSize: 16 }}>
              <span>- Crédit-bail mobilier *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 858, width: 17, height: 14, fontSize: 20 }}>
              <span>HP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 964, width: 304, height: 15, fontSize: 16 }}>
              <span>(5) Dont produits concernant les entreprises liées</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 983, top: 962, width: 10, height: 17, fontSize: 20 }}>
              <span>1J</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 999, width: 303, height: 17, fontSize: 16 }}>
              <span>(6) Dont intérêts concernant les entreprises liées</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 999, width: 15, height: 14, fontSize: 20 }}>
              <span>1K</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 210, top: 895, width: 150, height: 11, fontSize: 16 }}>
              <span>- Crédit-bail immobilier</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 893, width: 20, height: 17, fontSize: 20 }}>
              <span>HQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 877, width: 56, height: 15, fontSize: 16 }}>
              <span>(3) Dont</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 847, width: 7, height: 54, fontSize: 68 }}>
              <span>&lbrace;</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 823, width: 16, height: 14, fontSize: 20 }}>
              <span>1G</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 927, width: 16, height: 13, fontSize: 20 }}>
              <span>1H</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 646, top: 1266, width: 16, height: 16, fontSize: 20 }}>
              <span>A6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 852, top: 1268, width: 16, height: 16, fontSize: 20 }}>
              <span>A9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1068, top: 182, width: 37, height: 11, fontSize: 16 }}>
              <span>Néant</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1151, top: 182, width: 6, height: 7, fontSize: 16 }}>
              <span>*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 1296, width: 642, height: 18, fontSize: 16 }}>
              <span>Détail des produits et charges exceptionnels (Si le nombre de lignes est insuffisant, reproduire le cadre (7) et le joindre en</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 1314, width: 47, height: 15, fontSize: 16 }}>
              <span>annexe) :</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 311, top: 84, width: 35, height: 35, fontSize: 47 }}>
              <span>④</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 1376, width: 11, height: 252, fontSize: 12 }}>
              <span>N° 2053- SD – (SDNC-DGFiP) - Novembre 2018</span>
            </div>
          </div>
          <div className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot pdf-obj-0-1 pdf-obj-fixed acroform-field " name="PG" type="checkbox" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-2 pdf-obj-fixed acroform-field" name="HA"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-3 pdf-obj-fixed acroform-field" name="HB"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-4 pdf-obj-fixed acroform-field" name="HC"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-5 pdf-obj-fixed acroform-field" name="HD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-6 pdf-obj-fixed acroform-field" name="HE"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-7 pdf-obj-fixed acroform-field" name="HF"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-8 pdf-obj-fixed acroform-field" name="HG"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-9 pdf-obj-fixed acroform-field" name="HH" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-10 pdf-obj-fixed acroform-field" name="HI" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-11 pdf-obj-fixed acroform-field" name="HJ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-12 pdf-obj-fixed acroform-field" name="HK"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-13 pdf-obj-fixed acroform-field" name="HL" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-14 pdf-obj-fixed acroform-field" name="HM" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-15 pdf-obj-fixed acroform-field" name="HN"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-16 pdf-obj-fixed acroform-field" name="HO"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-17 pdf-obj-fixed acroform-field" name="HY"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-18 pdf-obj-fixed acroform-field" name="KA"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-19 pdf-obj-fixed acroform-field" name="HP"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-20 pdf-obj-fixed acroform-field" name="HQ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-21 pdf-obj-fixed acroform-field" name="KB"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-22 pdf-obj-fixed acroform-field" name="KC"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-23 pdf-obj-fixed acroform-field" name="KD"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-24 pdf-obj-fixed acroform-field" name="HX"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-25 pdf-obj-fixed acroform-field" name="AM"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-26 pdf-obj-fixed acroform-field" name="AN"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-27 pdf-obj-fixed acroform-field" name="LH"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-28 pdf-obj-fixed acroform-field" name="LJ"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-29 pdf-obj-fixed acroform-field" name="LK"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-30 pdf-obj-fixed acroform-field" name="LL"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-31 pdf-obj-fixed acroform-field" name="LM"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-32 pdf-obj-fixed acroform-field" name="LN"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-33 pdf-obj-fixed acroform-field" name="MA1"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-34 pdf-obj-fixed acroform-field" name="MA2"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-35 pdf-obj-fixed acroform-field" name="MA3"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-36 pdf-obj-fixed acroform-field" name="MA4"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-37 pdf-obj-fixed acroform-field" name="NA1"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-38 pdf-obj-fixed acroform-field" name="NA2"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-39 pdf-obj-fixed acroform-field" name="NA3"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-40 pdf-obj-fixed acroform-field" name="NA4"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-41 pdf-obj-fixed acroform-field" name="PA1"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-42 pdf-obj-fixed acroform-field" name="PA2"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-43 pdf-obj-fixed acroform-field" name="PA3"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-44 pdf-obj-fixed acroform-field" name="PA4"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-45 pdf-obj-fixed acroform-field" name="MF1"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-46 pdf-obj-fixed acroform-field" name="MF2"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-47 pdf-obj-fixed acroform-field" name="MF3"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-48 pdf-obj-fixed acroform-field" name="MF4"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-49 pdf-obj-fixed acroform-field" name="NF1"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-50 pdf-obj-fixed acroform-field" name="NF2"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-51 pdf-obj-fixed acroform-field" name="NF3"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-52 pdf-obj-fixed acroform-field" name="NF4"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-53 pdf-obj-fixed acroform-field" name="PF1"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-54 pdf-obj-fixed acroform-field" name="PF2"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-55 pdf-obj-fixed acroform-field" name="PF3"  />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-56 pdf-obj-fixed acroform-field" name="PF4"  />
            <Field component="input" className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" type="text" disabled />
          </div>
        </div>
      </div>
    </div>
  );
};
export default Tax2053;
