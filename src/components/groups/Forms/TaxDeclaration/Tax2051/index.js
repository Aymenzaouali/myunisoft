/* eslint-disable */
import React from "react";
import { Field } from "redux-form";
import {sum, useCompute, setIsNothingness} from 'helpers/pdfforms';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';

import "./style.scss";
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const notNeededFields = {
  'NOM_STEA': true,
  'GX': true
};

const Tax2051 = (props) => {
  const { change, taxDeclarationForm } = props;

  setIsNothingness(taxDeclarationForm, notNeededFields, 'GX', change);

  useCompute('DL', sum, ['DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK'], taxDeclarationForm, change);
  useCompute('DO', sum, ['DM', 'DN'], taxDeclarationForm, change);
  useCompute('DR', sum, ['DP', 'DQ'], taxDeclarationForm, change);
  useCompute('EC', sum, ['DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ', 'EA', 'EB'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2051">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 455, top: 123, width: 234, height: 20, 'font-size': 29, }}>
<span>
BILAN - PASSIF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 697, top: 126, width: 170, height: 23, 'font-size': 25, }}>
<span>
avant répartition
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 140, width: 165, height: 12, 'font-size': 13, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 70, top: 152, width: 132, height: 12, 'font-size': 13, }}>
<span>
du Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 926, top: 113, width: 223, height: 15, 'font-size': 21, }}>
<span>
DGFiP N° 2051-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 241, top: 186, width: 165, height: 16, 'font-size': 17, }}>
<span>
Désignation de l’entreprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 1633, width: 450, height: 15, 'font-size': 15, }}>
<span>
* Des explications concernant cette rubrique sont données dans la notice n° 2032.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 270, width: 486, height: 16, 'font-size': 17, }}>
<span>
Capital social ou individuel (1)* (Dont versé : .................................................. )
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 268, width: 21, height: 14, 'font-size': 21, }}>
<span>
DA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 307, width: 296, height: 15, 'font-size': 17, }}>
<span>
Primes d’émission, de fusion, d’apport, .... .......
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 304, width: 19, height: 15, 'font-size': 21, }}>
<span>
DB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 343, width: 325, height: 16, 'font-size': 17, }}>
<span>
Ecarts de réévaluation (2)* (dont écart d’équivalence
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 343, width: 5, height: 15, 'font-size': 17, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 340, width: 20, height: 15, 'font-size': 21, }}>
<span>
DC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 379, width: 110, height: 16, 'font-size': 17, }}>
<span>
Réserve légale (3)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 377, width: 21, height: 14, 'font-size': 21, }}>
<span>
DD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 415, width: 236, height: 12, 'font-size': 17, }}>
<span>
Réserves statutaires ou contractuelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 413, width: 20, height: 14, 'font-size': 21, }}>
<span>
DE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 451, width: 171, height: 16, 'font-size': 17, }}>
<span>
Réserves réglementées (3)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 442, width: 8, height: 28, 'font-size': 31, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 449, width: 19, height: 14, 'font-size': 21, }}>
<span>
DF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 253, top: 485, width: 437, height: 16, 'font-size': 17, }}>
<span>
Dont réserve relative à l’achat d’œuvres originales d’artistes vivants *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 244, top: 481, width: 8, height: 28, 'font-size': 31, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 487, width: 98, height: 12, 'font-size': 17, }}>
<span>
Autres réserves
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 478, width: 8, height: 28, 'font-size': 31, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 485, width: 22, height: 14, 'font-size': 21, }}>
<span>
DG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 524, width: 113, height: 15, 'font-size': 17, }}>
<span>
Report à nouveau
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 521, width: 22, height: 14, 'font-size': 21, }}>
<span>
DH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 556, width: 376, height: 18, 'font-size': 16, }}>
<span>
RÉSULTAT DE L’EXERCICE (bénéfice ou perte)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 557, width: 16, height: 14, 'font-size': 21, }}>
<span>
DI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 595, width: 188, height: 13, 'font-size': 17, }}>
<span>
Subventions d’investissement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 593, width: 15, height: 19, 'font-size': 21, }}>
<span>
DJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 632, width: 164, height: 15, 'font-size': 17, }}>
<span>
Provisions réglementées *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 629, width: 22, height: 14, 'font-size': 21, }}>
<span>
DK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 860, top: 666, width: 90, height: 15, 'font-size': 17, }}>
<span>
TOTAL (I)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 665, width: 20, height: 14, 'font-size': 21, }}>
<span>
DL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 958, top: 228, width: 61, height: 11, 'font-size': 15, }}>
<span>
Exercice N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 391, width: 11, height: 166, 'font-size': 17, }}>
<span>
CAPITAUX PROPRES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 707, width: 11, height: 77, 'font-size': 15, }}>
<span>
Autres fonds
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 722, width: 11, height: 45, 'font-size': 15, }}>
<span>
propres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 61, top: 822, width: 11, height: 62, 'font-size': 15, }}>
<span>
Provisions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 817, width: 14, height: 73, 'font-size': 15, }}>
<span>
pour risques
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 823, width: 14, height: 59, 'font-size': 15, }}>
<span>
et charges
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 1027, width: 15, height: 86, 'font-size': 17, }}>
<span>
DETTES (4)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 56, top: 1466, width: 12, height: 74, 'font-size': 17, }}>
<span>
RENVOIS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 373, top: 119, width: 31, height: 31, 'font-size': 42, }}>
<span>
②
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 704, width: 273, height: 16, 'font-size': 17, }}>
<span>
Produit des émissions de titres participatifs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 702, width: 22, height: 14, 'font-size': 21, }}>
<span>
DM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 740, width: 147, height: 12, 'font-size': 17, }}>
<span>
Avances conditionnées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 738, width: 22, height: 14, 'font-size': 21, }}>
<span>
DN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 859, top: 775, width: 91, height: 14, 'font-size': 17, }}>
<span>
TOTAL (II)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 774, width: 22, height: 14, 'font-size': 21, }}>
<span>
DO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 812, width: 149, height: 16, 'font-size': 17, }}>
<span>
Provisions pour risques
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 810, width: 19, height: 14, 'font-size': 21, }}>
<span>
DP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 848, width: 153, height: 16, 'font-size': 17, }}>
<span>
Provisions pour charges
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 846, width: 22, height: 18, 'font-size': 21, }}>
<span>
DQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 920, width: 221, height: 16, 'font-size': 17, }}>
<span>
Emprunts obligataires convertibles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 918, width: 18, height: 14, 'font-size': 21, }}>
<span>
DS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 956, width: 184, height: 16, 'font-size': 17, }}>
<span>
Autres emprunts obligataires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 954, width: 20, height: 14, 'font-size': 21, }}>
<span>
DT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 993, width: 369, height: 15, 'font-size': 17, }}>
<span>
Emprunts et dettes auprès des établissements de crédit (5)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 990, width: 21, height: 15, 'font-size': 21, }}>
<span>
DU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 1029, width: 423, height: 15, 'font-size': 17, }}>
<span>
Emprunts et dettes financières divers (Dont emprunts participatifs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 1029, width: 5, height: 15, 'font-size': 17, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 1026, width: 22, height: 15, 'font-size': 21, }}>
<span>
DV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 1065, width: 334, height: 16, 'font-size': 17, }}>
<span>
Avances et acomptes reçus sur commandes en cours
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 1063, width: 24, height: 14, 'font-size': 21, }}>
<span>
DW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 1101, width: 258, height: 16, 'font-size': 17, }}>
<span>
Dettes fournisseurs et comptes rattachés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 1099, width: 22, height: 14, 'font-size': 21, }}>
<span>
DX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 1137, width: 159, height: 12, 'font-size': 17, }}>
<span>
Dettes fiscales et sociales
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1135, width: 20, height: 14, 'font-size': 21, }}>
<span>
DY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 1173, width: 305, height: 16, 'font-size': 17, }}>
<span>
Dettes sur immobilisations et comptes rattachés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1170, width: 20, height: 15, 'font-size': 21, }}>
<span>
DZ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 1209, width: 84, height: 12, 'font-size': 17, }}>
<span>
Autres dettes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1206, width: 19, height: 15, 'font-size': 21, }}>
<span>
EA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 1245, width: 197, height: 16, 'font-size': 17, }}>
<span>
Produits constatés d’avance (4)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1243, width: 17, height: 14, 'font-size': 21, }}>
<span>
EB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1387, width: 298, height: 18, 'font-size': 17, }}>
<span>
(1) Écart de réévaluation incorporé au capital
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1462, width: 16, height: 16, 'font-size': 17, }}>
<span>
(2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 200, top: 1426, width: 248, height: 16, 'font-size': 17, }}>
<span>
Réserve spéciale de réévaluation (1959)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 1462, width: 35, height: 12, 'font-size': 17, }}>
<span>
Dont
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 200, top: 1459, width: 168, height: 15, 'font-size': 17, }}>
<span>
Écart de réévaluation libre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 200, top: 1498, width: 195, height: 16, 'font-size': 17, }}>
<span>
Réserve de réévaluation (1976)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1534, width: 363, height: 16, 'font-size': 17, }}>
<span>
(3) Dont réserve spéciale des plus-values à long terme *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1532, width: 18, height: 14, 'font-size': 21, }}>
<span>
EF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1570, width: 375, height: 16, 'font-size': 17, }}>
<span>
(4) Dettes et produits constatés d’avance à moins d’un an
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1568, width: 20, height: 14, 'font-size': 21, }}>
<span>
EG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1606, width: 506, height: 16, 'font-size': 17, }}>
<span>
(5) Dont concours bancaires courants, et soldes créditeurs de banques et CCP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1604, width: 20, height: 14, 'font-size': 21, }}>
<span>
EH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 857, top: 883, width: 93, height: 15, 'font-size': 17, }}>
<span>
TOTAL (III)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 882, width: 20, height: 14, 'font-size': 21, }}>
<span>
DR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 857, top: 1280, width: 93, height: 15, 'font-size': 17, }}>
<span>
TOTAL (IV)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1279, width: 18, height: 14, 'font-size': 21, }}>
<span>
EC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 1317, width: 185, height: 16, 'font-size': 17, }}>
<span>
Ecarts de conversion passif *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 1316, width: 23, height: 15, 'font-size': 17, }}>
<span>
(V)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1315, width: 20, height: 14, 'font-size': 21, }}>
<span>
ED
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 743, top: 1349, width: 207, height: 18, 'font-size': 17, }}>
<span>
TOTAL GÉNÉRAL (I à V)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1352, width: 18, height: 13, 'font-size': 21, }}>
<span>
EE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 1236, width: 50, height: 16, 'font-size': 17, }}>
<span>
Compte
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1248, width: 34, height: 16, 'font-size': 17, }}>
<span>
régul.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 174, top: 1398, width: 14, height: 112, 'font-size': 121, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 503, top: 340, width: 21, height: 14, 'font-size': 21, }}>
<span>
EK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 754, top: 484, width: 14, height: 19, 'font-size': 21, }}>
<span>
EJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 335, top: 449, width: 288, height: 16, 'font-size': 17, }}>
<span>
Dont réserve spéciale des provisions pour fluctuation des cours
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 326, top: 445, width: 8, height: 28, 'font-size': 31, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 673, top: 448, width: 15, height: 14, 'font-size': 21, }}>
<span>
B1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 608, top: 1026, width: 14, height: 14, 'font-size': 21, }}>
<span>
EI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1083, top: 187, width: 38, height: 11, 'font-size': 17, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1169, top: 187, width: 7, height: 7, 'font-size': 17, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1388, width: 14, height: 14, 'font-size': 21, }}>
<span>
1B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1423, width: 14, height: 14, 'font-size': 21, }}>
<span>
1C
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1459, width: 16, height: 14, 'font-size': 21, }}>
<span>
1D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1495, width: 15, height: 14, 'font-size': 21, }}>
<span>
1E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 19, top: 1365, width: 11, height: 270, 'font-size': 13, }}>
<span>
N° 2051- SD – (SDNC-DGFiP) - Noevembre 2018
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="GX" data-field-id="22375336" data-annot-id="21593920" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="FA" data-field-id="22375480" data-annot-id="21813120" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="DA" data-field-id="22375784" data-annot-id="21913056" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="DB" data-field-id="22376088" data-annot-id="21913248" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="EK" data-field-id="22376424" data-annot-id="21913440" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="DC" data-field-id="22376760" data-annot-id="21919424" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="DD" data-field-id="22377096" data-annot-id="21919616" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="EL" data-field-id="22377432" data-annot-id="21919808" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="EJ" data-field-id="22377768" data-annot-id="21920000" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="DE" data-field-id="22378248" data-annot-id="21920192" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="DF" data-field-id="22378584" data-annot-id="21920384" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="DG" data-field-id="22378920" data-annot-id="21920576" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="DH" data-field-id="22379256" data-annot-id="21920768" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="DI" data-field-id="22379592" data-annot-id="21920960" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="DJ" data-field-id="22379928" data-annot-id="21921152" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="DK" data-field-id="22380264" data-annot-id="21921344" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="DL" data-field-id="22380600" data-annot-id="21921536" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="DM" data-field-id="22381080" data-annot-id="21922000" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="DN" data-field-id="22381416" data-annot-id="21922192" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="DO" data-field-id="22381752" data-annot-id="21922384" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="DP" data-field-id="22382088" data-annot-id="21922576" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="DQ" data-field-id="22382424" data-annot-id="21922768" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="DR" data-field-id="22382760" data-annot-id="21922960" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="DS" data-field-id="22383096" data-annot-id="21923152" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="DT" data-field-id="22383432" data-annot-id="21923344" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="DU" data-field-id="22383768" data-annot-id="21923536" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="DV" data-field-id="22384104" data-annot-id="21923728" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="DW" data-field-id="22384440" data-annot-id="21923920" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="DX" data-field-id="22384776" data-annot-id="21924112" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="DY" data-field-id="22385112" data-annot-id="21924304" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="DZ" data-field-id="22385448" data-annot-id="21924496" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="EA" data-field-id="22385784" data-annot-id="21924688" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="EB" data-field-id="22386120" data-annot-id="21924880" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="EC" data-field-id="22380936" data-annot-id="21921728" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="ED" data-field-id="22387048" data-annot-id="21925600" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="EE" data-field-id="22387384" data-annot-id="21925792" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="EI" data-field-id="22387720" data-annot-id="21925984" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="FC" data-field-id="22388056" data-annot-id="21926176" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="FD" data-field-id="22388392" data-annot-id="21926368" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="FE" data-field-id="22388728" data-annot-id="21926560" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="FF" data-field-id="22389064" data-annot-id="21926752" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="EF" data-field-id="22389400" data-annot-id="21926944" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="EG" data-field-id="22389736" data-annot-id="21927136" type="number" />
               
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="EH" data-field-id="22390072" data-annot-id="21927328" type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="38390504" data-annot-id="37927328" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
}

export default Tax2051;
