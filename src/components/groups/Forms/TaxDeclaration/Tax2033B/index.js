import React from "react";
import { Field } from 'redux-form';
import { sum, useCompute, setIsNothingness, parse as p, setValue } from 'helpers/pdfforms';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import lodash from 'lodash';

import './style.scss';

const notNeededFields = {
  'NOM_STEA': true,
  'JB': true,
  'FIN_EX': true
};

const compute352 = ({ CC, AE, CE, CF, CG, CH, CJ, AF, AJ, AG, EK, EL }) => {
  const sum1 = p(CC) + p(AE) + p(CE) + p(CF) + p(CG) + p(CH) + p(CJ) + p(AF) + p(AJ); // 310 + SOMME(316:999)
  const sum2 = p(AG) + p(EK) + p(EL); // 997 + 342 + 350
  const diff = sum1 - sum2;
  return diff > 0 ? diff : 0;
};

const compute354 = ({ CC, AE, CE, CF, CG, CH, CJ, AF, AJ, AG, EK, EL }) => {
  const sum1 = p(CC) + p(AE) + p(CE) + p(CF) + p(CG) + p(CH) + p(CJ) + p(AF) + p(AJ); // 310 + SOMME(316:999)
  const sum2 = p(AG) + p(EK) + p(EL); // 997 + 342 + 350
  const diff = sum1 - sum2;
  return diff < 0 ? diff : 0;
};

const compute370 = ({CM, EM, CN, EP}) => {
  const result = p(CM) - p(EM) + p(CN) - p(EP);
  return (p(CM) && result > 0) ? result : 0;
};

const compute372 = ({CM, EM, CN, EP}) => {
  const result = p(EM) - p(CM) - p(CN) + p(EP);
  return (p(EM) && result > 0) ? result : 0;
};

const Tax2033B = (props) => {
  const {taxDeclarationForm, taxForm2033D, change} = props;
  const PG = lodash.get(taxForm2033D, 'PG', 0);
  setValue(taxDeclarationForm, 'JA', PG, change);
  /**
   * Lines list
   * AE - 251
   * CE - 316
   * CF - 318
   * CG - 322
   * CH - 324
   * CJ - 330
   * EK - 342
   * HL - 344
   * MH - 345
   * FL - 346
   * AK - 347
   * AL - 348
   * EL - 350
   * CM - 352
   * EM - 354
   * CN - 356
   * EP - 360
   * CR - 370
   * ER - 372
   * AH - 655
   * HW - 981
   * MC - 986
   * MD - 987
   * MF - 989
   * MG - 990
   * AQ - 991
   * AR - 992
   * AG - 997
   * AF - 998
   * AJ - 999
   */
  setIsNothingness(taxDeclarationForm, notNeededFields, 'JB', change);
  useCompute('BH', sum, ['BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG'], taxDeclarationForm, change);
  useCompute('BV', sum, ['BJ', 'BK', 'BL', 'BM', 'BN', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU'], taxDeclarationForm, change);
  useCompute('BW', ({BH, BV}) => p(BH) - (BV), ['BH', 'BV'], taxDeclarationForm, change);
  useCompute('CC', ({BH, BX, BY, BV, BZ, CA, CB}) => (p(BH) + p(BX) + p(BY)) - (p(BV) + p(BZ) + p(CA) + p(CB)),
    ['BH', 'BX', 'BY', 'BV', 'BZ', 'CA', 'CB'], taxDeclarationForm, change); // LINE310
  useCompute('CD', ({CC}) => p(CC) > 0 ? p(CC) : 0, ['CC'], taxDeclarationForm, change);
  useCompute('ED', ({CC}) => p(CC) < 0 ? p(CC) : 0, ['CC'], taxDeclarationForm, change);
  useCompute('CM', compute352, ['CC', 'CE', 'CF', 'CG', 'CH', 'CJ', 'AE', 'AF', 'AJ', 'AG', 'EK', 'EL'], taxDeclarationForm, change); // LINE 352
  useCompute('EM', compute354, ['CC', 'CE', 'CF', 'CG', 'CH', 'CJ', 'AE', 'AF', 'AJ', 'AG', 'EK', 'EL'], taxDeclarationForm, change);  // LINE 354
  useCompute('CR', compute370, ['CM', 'EM', 'CN', 'EP'], taxDeclarationForm, change); // LINE 370
  useCompute('ER', compute372, ['CM', 'EM', 'CN', 'EP'], taxDeclarationForm, change); // LINE 372

  return (
    <div className="form-tax-declaration-2033b">

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 134, top: 161, width: 183, height: 11, 'font-size': 11, }}>
<span>
Formulaire obligatoire (article 302 septies
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 172, width: 156, height: 10, 'font-size': 11, }}>
<span>
A bis du Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 324, top: 113, width: 31, height: 31, 'font-size': 40, }}>
<span>
Á
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 375, top: 119, width: 492, height: 22, 'font-size': 21, }}>
<span>
COMPTE DE RÉSULTAT SIMPLIFIÉ DE L’EXERCICE (en liste)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 914, top: 120, width: 49, height: 12, 'font-size': 18, }}>
<span>
DGFiP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 967, top: 117, width: 141, height: 17, 'font-size': 21, }}>
<span>
N° 2033-B-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 1347, width: 11, height: 219, 'font-size': 11, }}>
<span>
N° 2033-B-SD – (SDNC-DGFiP) - Janvier 201 9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 163, width: 160, height: 14, 'font-size': 14, }}>
<span>
Désignation de l’entreprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1040, top: 160, width: 35, height: 10, 'font-size': 14, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1097, top: 158, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 189, width: 277, height: 20, 'font-size': 21, }}>
<span>
A – RÉSULTAT COMPTABLE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 701, top: 185, width: 111, height: 14, 'font-size': 14, }}>
<span>
Formulaire déposé
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 715, top: 199, width: 84, height: 11, 'font-size': 14, }}>
<span>
au titre de l’IR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 829, top: 195, width: 17, height: 10, 'font-size': 14, }}>
<span>
018
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 177, width: 106, height: 11, 'font-size': 14, }}>
<span>
Exercice N clos le
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1029, top: 208, width: 4, height: 8, 'font-size': 11, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 234, width: 10, height: 179, 'font-size': 14, }}>
<span>
PRODUITS D’EXPLOITATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 226, width: 144, height: 11, 'font-size': 14, }}>
<span>
Ventes de marchandises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 315, top: 224, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 659, top: 238, width: 70, height: 14, 'font-size': 14, }}>
<span>
dont export
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 658, top: 251, width: 71, height: 11, 'font-size': 14, }}>
<span>
et livraisons
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 632, top: 266, width: 123, height: 10, 'font-size': 14, }}>
<span>
intracommunautaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 767, top: 221, width: 6, height: 70, 'font-size': 87, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 791, top: 226, width: 17, height: 11, 'font-size': 14, }}>
<span>
209
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 226, width: 16, height: 10, 'font-size': 14, }}>
<span>
210
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 265, width: 113, height: 11, 'font-size': 14, }}>
<span>
Production vendue
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 491, top: 245, width: 6, height: 47, 'font-size': 59, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 522, top: 252, width: 32, height: 10, 'font-size': 14, }}>
<span>
Biens
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 791, top: 252, width: 17, height: 11, 'font-size': 14, }}>
<span>
215
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 251, width: 16, height: 11, 'font-size': 14, }}>
<span>
214
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 522, top: 281, width: 47, height: 10, 'font-size': 14, }}>
<span>
Services
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 573, top: 277, width: 7, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 791, top: 278, width: 17, height: 11, 'font-size': 14, }}>
<span>
217
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 279, width: 17, height: 10, 'font-size': 14, }}>
<span>
218
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 307, width: 113, height: 11, 'font-size': 14, }}>
<span>
Production stockée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 304, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 436, top: 297, width: 7, height: 29, 'font-size': 30, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 298, width: 272, height: 14, 'font-size': 14, }}>
<span>
Variation du stock en produits intermédiaires,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 450, top: 312, width: 237, height: 14, 'font-size': 14, }}>
<span>
produits finis et en cours de production
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 738, top: 297, width: 7, height: 29, 'font-size': 30, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 306, width: 17, height: 10, 'font-size': 14, }}>
<span>
222
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 333, width: 140, height: 11, 'font-size': 14, }}>
<span>
Production immobilisée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 331, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 332, width: 17, height: 11, 'font-size': 14, }}>
<span>
224
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 359, width: 205, height: 14, 'font-size': 14, }}>
<span>
Subventions d’exploitations reçues
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 358, width: 17, height: 12, 'font-size': 14, }}>
<span>
226
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 385, width: 91, height: 14, 'font-size': 14, }}>
<span>
Autres produits
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 386, width: 17, height: 12, 'font-size': 14, }}>
<span>
230
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 668, top: 412, width: 309, height: 14, 'font-size': 14, }}>
<span>
Total des produits d’exploitation hors TVA (I) 232
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 523, width: 10, height: 173, 'font-size': 14, }}>
<span>
CHARGES D’EXPLOITATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 441, width: 143, height: 11, 'font-size': 14, }}>
<span>
Achats de marchandises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 314, top: 439, width: 6, height: 6, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 326, top: 441, width: 173, height: 14, 'font-size': 14, }}>
<span>
(y compris droits de douane)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 439, width: 17, height: 12, 'font-size': 14, }}>
<span>
234
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 468, width: 208, height: 12, 'font-size': 14, }}>
<span>
Variation de stocks (marchandises)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 379, top: 465, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 465, width: 17, height: 13, 'font-size': 14, }}>
<span>
236
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 495, width: 354, height: 14, 'font-size': 14, }}>
<span>
Achats de matières premières et autres approvisionnements
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 525, top: 492, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 538, top: 495, width: 173, height: 14, 'font-size': 14, }}>
<span>
(y compris droits de douane)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 494, width: 17, height: 11, 'font-size': 14, }}>
<span>
238
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 522, width: 374, height: 14, 'font-size': 14, }}>
<span>
Variation de stock (matières premières et approvisionnements)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 545, top: 519, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 520, width: 17, height: 11, 'font-size': 14, }}>
<span>
240
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 550, width: 141, height: 14, 'font-size': 14, }}>
<span>
Autres charges externes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 312, top: 547, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 325, top: 554, width: 3, height: 7, 'font-size': 14, }}>
<span>
:
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 527, top: 539, width: 7, height: 32, 'font-size': 33, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 539, top: 541, width: 98, height: 11, 'font-size': 14, }}>
<span>
dont crédit bail :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 539, top: 554, width: 68, height: 11, 'font-size': 14, }}>
<span>
– mobilier :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 613, top: 563, width: 125, height: 2, 'font-size': 11, }}>
<span>
...............................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 741, top: 554, width: 84, height: 11, 'font-size': 14, }}>
<span>
– immobilier :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 830, top: 563, width: 107, height: 2, 'font-size': 11, }}>
<span>
........................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 942, top: 539, width: 8, height: 32, 'font-size': 33, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 548, width: 17, height: 11, 'font-size': 14, }}>
<span>
242
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 577, width: 221, height: 14, 'font-size': 14, }}>
<span>
Impôts, taxes et versements assimilés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 579, top: 568, width: 7, height: 32, 'font-size': 33, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 591, top: 570, width: 151, height: 14, 'font-size': 14, }}>
<span>
dont taxe professionnelle
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 591, top: 585, width: 77, height: 10, 'font-size': 14, }}>
<span>
CFE et CVAE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 673, top: 581, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 786, top: 577, width: 17, height: 12, 'font-size': 14, }}>
<span>
243
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 942, top: 569, width: 8, height: 32, 'font-size': 33, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 577, width: 17, height: 11, 'font-size': 14, }}>
<span>
244
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 607, width: 171, height: 14, 'font-size': 14, }}>
<span>
Rémunérations du personnel
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 342, top: 605, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 606, width: 17, height: 11, 'font-size': 14, }}>
<span>
250
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 632, width: 192, height: 14, 'font-size': 14, }}>
<span>
Charges sociales (cf. renvoi 380)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 633, width: 17, height: 11, 'font-size': 14, }}>
<span>
252
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 662, width: 177, height: 10, 'font-size': 14, }}>
<span>
Dotations aux amortissements
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 349, top: 658, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 659, width: 17, height: 12, 'font-size': 14, }}>
<span>
254
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 686, width: 148, height: 14, 'font-size': 14, }}>
<span>
Dotations aux provisions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 685, width: 17, height: 13, 'font-size': 14, }}>
<span>
256
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 726, width: 88, height: 14, 'font-size': 14, }}>
<span>
Autres charges
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 295, top: 706, width: 6, height: 47, 'font-size': 59, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 315, top: 707, width: 256, height: 14, 'font-size': 14, }}>
<span>
dont provisions fiscales pour implantations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 315, top: 719, width: 152, height: 14, 'font-size': 14, }}>
<span>
commerciales à l’étranger
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 471, top: 720, width: 6, height: 6, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 786, top: 713, width: 17, height: 12, 'font-size': 14, }}>
<span>
259
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 937, top: 706, width: 7, height: 48, 'font-size': 59, }}>
<span>
}
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 712, width: 17, height: 12, 'font-size': 14, }}>
<span>
262
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 315, top: 733, width: 250, height: 14, 'font-size': 14, }}>
<span>
dont cotisations versées aux organisations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 315, top: 745, width: 176, height: 14, 'font-size': 14, }}>
<span>
syndicales et professionnelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 786, top: 739, width: 17, height: 12, 'font-size': 14, }}>
<span>
260
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 722, top: 767, width: 255, height: 14, 'font-size': 14, }}>
<span>
Total des charges d’exploitation ( I I ) 264
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 974, width: 223, height: 20, 'font-size': 21, }}>
<span>
B – RÉSULTAT FISCAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 400, top: 981, width: 423, height: 14, 'font-size': 14, }}>
<span>
Reporter le bénéfice comptable col. 1, le déficit comptable col. 2 312
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 981, width: 17, height: 12, 'font-size': 14, }}>
<span>
314
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 1070, width: 13, height: 83, 'font-size': 14, }}>
<span>
Réintégrations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1011, width: 333, height: 14, 'font-size': 14, }}>
<span>
Rémunérations et avantages personnels non déductibles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 505, top: 1009, width: 6, height: 6, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 806, top: 1009, width: 17, height: 13, 'font-size': 14, }}>
<span>
316
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 1036, width: 519, height: 12, 'font-size': 14, }}>
<span>
Amortissements excédentaires (art. 39-4 CGI) et autres amortissements non déductibles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 806, top: 1037, width: 17, height: 11, 'font-size': 14, }}>
<span>
318
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1064, width: 159, height: 11, 'font-size': 14, }}>
<span>
Provisions non déductibles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 332, top: 1064, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 806, top: 1064, width: 17, height: 11, 'font-size': 14, }}>
<span>
322
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1091, width: 189, height: 14, 'font-size': 14, }}>
<span>
Impôts et taxes non déductibles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 361, top: 1088, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 372, top: 1091, width: 225, height: 14, 'font-size': 14, }}>
<span>
(cf page 7 de la notice 2033-NOT-SD)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 806, top: 1090, width: 17, height: 12, 'font-size': 14, }}>
<span>
324
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1113, width: 186, height: 13, 'font-size': 14, }}>
<span>
Divers , dont intérêts excéden-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 208, top: 1113, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1118, width: 214, height: 20, 'font-size': 14, }}>
<span>
taires des cptes-cts d’associés 247
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 499, top: 1112, width: 98, height: 11, 'font-size': 14, }}>
<span>
écarts de valeurs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 498, top: 1123, width: 142, height: 14, 'font-size': 14, }}>
<span>
liquidatives sur OPCVM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 644, top: 1121, width: 6, height: 6, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 677, top: 1118, width: 17, height: 11, 'font-size': 14, }}>
<span>
248
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 806, top: 1119, width: 17, height: 11, 'font-size': 14, }}>
<span>
330
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1141, width: 298, height: 13, 'font-size': 14, }}>
<span>
Fraction des loyers à réintégrer dans le cadre d’un
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1154, width: 251, height: 14, 'font-size': 14, }}>
<span>
crédit-bail immobilier et de levée d’option
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 492, top: 1138, width: 8, height: 30, 'font-size': 32, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 507, top: 1140, width: 146, height: 14, 'font-size': 14, }}>
<span>
Part des loyers dispensée de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 507, top: 1154, width: 162, height: 14, 'font-size': 14, }}>
<span>
réintégration (art. 239 sexies D)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 677, top: 1147, width: 17, height: 12, 'font-size': 14, }}>
<span>
249
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 789, top: 1138, width: 8, height: 30, 'font-size': 32, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 806, top: 1148, width: 16, height: 11, 'font-size': 14, }}>
<span>
251
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1176, width: 656, height: 14, 'font-size': 14, }}>
<span>
Charges afférentes à l’activité relevant du régime optionnel de taxation au tonnage des entreprises de transport maritime 998
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1202, width: 656, height: 14, 'font-size': 14, }}>
<span>
Résultat fiscal afférent à l’activité relevant du régime optionnel de taxation au tonnage des entreprises de transport maritime 999
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 1460, width: 419, height: 13, 'font-size': 14, }}>
<span>
RÉSULTAT FISCAL AVANT IMPUTATION DES DÉFICITS ANTÉRIEURS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 701, top: 1455, width: 90, height: 11, 'font-size': 14, }}>
<span>
Bénéfices col. 1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 719, top: 1462, width: 104, height: 16, 'font-size': 14, }}>
<span>
Déficit col. 2 352
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 1461, width: 16, height: 12, 'font-size': 14, }}>
<span>
354
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 135, top: 1486, width: 11, height: 44, 'font-size': 14, }}>
<span>
Déficits
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1491, width: 221, height: 14, 'font-size': 14, }}>
<span>
Déficit de l’exercice reporté en arrière
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 393, top: 1488, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 806, top: 1488, width: 17, height: 13, 'font-size': 14, }}>
<span>
356
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1516, width: 173, height: 14, 'font-size': 14, }}>
<span>
Déficits antérieurs reportables
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 345, top: 1515, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 358, top: 1527, width: 227, height: 2, 'font-size': 11, }}>
<span>
.....................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 592, top: 1516, width: 166, height: 14, 'font-size': 14, }}>
<span>
dont imputés sur le résultat :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 1516, width: 16, height: 13, 'font-size': 14, }}>
<span>
360
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 1542, width: 331, height: 13, 'font-size': 14, }}>
<span>
RÉSULTAT FISCAL APRÈS IMPUTATION DES DÉFICITS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 701, top: 1538, width: 90, height: 11, 'font-size': 14, }}>
<span>
Bénéfices col. 1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 719, top: 1545, width: 104, height: 16, 'font-size': 14, }}>
<span>
Déficit col. 2 370
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 1544, width: 16, height: 12, 'font-size': 14, }}>
<span>
372
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 1570, width: 404, height: 11, 'font-size': 11, }}>
<span>
* Des explications concernant cette rubrique sont données dans la notice n° 2033-NOT-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 793, width: 277, height: 15, 'font-size': 16, }}>
<span>
1 – RÉSULTAT D’EXPLOITATION ( I – I I )
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 796, width: 17, height: 11, 'font-size': 14, }}>
<span>
270
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 823, width: 109, height: 11, 'font-size': 14, }}>
<span>
Produits financiers
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 403, top: 822, width: 25, height: 14, 'font-size': 16, }}>
<span>
(III)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 465, top: 824, width: 17, height: 10, 'font-size': 14, }}>
<span>
280
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 702, top: 823, width: 114, height: 14, 'font-size': 14, }}>
<span>
Charges financières
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 910, top: 823, width: 19, height: 12, 'font-size': 14, }}>
<span>
(V)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 823, width: 17, height: 12, 'font-size': 14, }}>
<span>
294
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 849, width: 134, height: 14, 'font-size': 14, }}>
<span>
Produits exceptionnels
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 403, top: 849, width: 25, height: 13, 'font-size': 16, }}>
<span>
(IV)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 850, width: 17, height: 12, 'font-size': 14, }}>
<span>
290
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 890, width: 172, height: 13, 'font-size': 14, }}>
<span>
Charges exceptionnelles (VI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 386, top: 871, width: 7, height: 48, 'font-size': 59, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 870, width: 260, height: 14, 'font-size': 14, }}>
<span>
dont amortissements des souscriptions dans
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 882, width: 216, height: 12, 'font-size': 14, }}>
<span>
des PME innovantes (art. 217 octies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 701, top: 876, width: 21, height: 12, 'font-size': 14, }}>
<span>
347
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 871, width: 6, height: 48, 'font-size': 59, }}>
<span>
}
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 877, width: 17, height: 11, 'font-size': 14, }}>
<span>
300
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 897, width: 281, height: 14, 'font-size': 14, }}>
<span>
dont amortissements exceptionnels de 25% des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 903, width: 314, height: 20, 'font-size': 14, }}>
<span>
constructions nouvelles (art. 39 quinquies D) 348
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 931, width: 137, height: 14, 'font-size': 14, }}>
<span>
Impôt sur les bénéfices
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 309, top: 929, width: 6, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 409, top: 931, width: 27, height: 13, 'font-size': 14, }}>
<span>
(VII)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 929, width: 17, height: 13, 'font-size': 14, }}>
<span>
306
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 955, width: 545, height: 17, 'font-size': 16, }}>
<span>
2 – BÉNÉFICES OU PERTES : Produits ( I + III + IV ) – Charges ( II + V + VI + VII )
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 958, width: 17, height: 12, 'font-size': 14, }}>
<span>
310
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 135, top: 1303, width: 11, height: 68, 'font-size': 14, }}>
<span>
Déductions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 1230, width: 627, height: 14, 'font-size': 14, }}>
<span>
Produits afférents à l’activité relevant du régime optionnel de taxation au tonnage des entreprises de transport maritime
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 1232, width: 17, height: 11, 'font-size': 14, }}>
<span>
997
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 1257, width: 215, height: 15, 'font-size': 14, }}>
<span>
Entreprises nouvelles (44. sexies) 986
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 503, top: 1257, width: 191, height: 13, 'font-size': 14, }}>
<span>
ZFU-TE (44. octies et octies A) 987
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 1257, width: 17, height: 13, 'font-size': 14, }}>
<span>
342
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1276, width: 116, height: 14, 'font-size': 14, }}>
<span>
Reprise d’entreprises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1287, width: 136, height: 15, 'font-size': 14, }}>
<span>
en difficulté (44. septies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 364, top: 1282, width: 17, height: 12, 'font-size': 14, }}>
<span>
981
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 1282, width: 96, height: 14, 'font-size': 14, }}>
<span>
JEI (44. sexies A)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 677, top: 1282, width: 17, height: 12, 'font-size': 14, }}>
<span>
989
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1307, width: 113, height: 12, 'font-size': 14, }}>
<span>
ZRD (44. terdecies )
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 365, top: 1307, width: 16, height: 10, 'font-size': 14, }}>
<span>
127
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 503, top: 1306, width: 115, height: 15, 'font-size': 14, }}>
<span>
ZRR (44. quindecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 1307, width: 16, height: 11, 'font-size': 14, }}>
<span>
138
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 1327, width: 183, height: 14, 'font-size': 14, }}>
<span>
Bassins d’emploi à redynamiser (art.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 1339, width: 70, height: 12, 'font-size': 14, }}>
<span>
44 duodecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 364, top: 1334, width: 17, height: 11, 'font-size': 14, }}>
<span>
991
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 503, top: 1327, width: 153, height: 14, 'font-size': 14, }}>
<span>
Pôles de compétitivité hors
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 503, top: 1339, width: 135, height: 12, 'font-size': 14, }}>
<span>
CICE (art 44. undecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 677, top: 1334, width: 17, height: 11, 'font-size': 14, }}>
<span>
990
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1361, width: 127, height: 14, 'font-size': 14, }}>
<span>
ZFA (44. quaterdecies )
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 365, top: 1360, width: 16, height: 12, 'font-size': 14, }}>
<span>
345
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 503, top: 1360, width: 191, height: 12, 'font-size': 14, }}>
<span>
Investissements outre-mer 344
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 165, top: 1384, width: 324, height: 15, 'font-size': 14, }}>
<span>
Bassins urbains à dynamiser – BUD (art. 44 sexdecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 677, top: 1385, width: 17, height: 12, 'font-size': 14, }}>
<span>
992
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 157, top: 1411, width: 10, height: 31, 'font-size': 14, }}>
<span>
Droit
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1409, width: 11, height: 35, 'font-size': 14, }}>
<span>
divers
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 196, top: 1409, width: 243, height: 14, 'font-size': 14, }}>
<span>
Créance due au report en arrière du déficit
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 677, top: 1408, width: 17, height: 13, 'font-size': 14, }}>
<span>
346
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 1409, width: 17, height: 12, 'font-size': 14, }}>
<span>
350
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 196, top: 1434, width: 234, height: 14, 'font-size': 14, }}>
<span>
Déduction exceptionnelle (Art. 39 decies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 1432, width: 16, height: 13, 'font-size': 14, }}>
<span>
655
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="JB" data-field-id="29640648" data-annot-id="28267008" value="Yes" type="checkbox" data-default-value="Off" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="CR" data-field-id="29640792" data-annot-id="28594608" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="ER" data-field-id="29641128" data-annot-id="28594800" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="EP" data-field-id="29641432" data-annot-id="28594992" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="CN" data-field-id="29819720" data-annot-id="28595184" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="CM" data-field-id="29820056" data-annot-id="28595376" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="EM" data-field-id="29820392" data-annot-id="28856608" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="EL" data-field-id="29820728" data-annot-id="28856800" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="AH" data-field-id="29821064" data-annot-id="28856992" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="FL" data-field-id="29821544" data-annot-id="28857328" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="AR" data-field-id="29821880" data-annot-id="28857520" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="HL" data-field-id="29822216" data-annot-id="28857712" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="MG" data-field-id="29822552" data-annot-id="28857904" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="FB" data-field-id="29822888" data-annot-id="28858096" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="MF" data-field-id="29823224" data-annot-id="28858288" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="MD" data-field-id="29823560" data-annot-id="28858480" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="MH" data-field-id="29823896" data-annot-id="28858672" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="AQ" data-field-id="29824376" data-annot-id="28859136" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="FA" data-field-id="29824712" data-annot-id="28859328" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="HW" data-field-id="29825048" data-annot-id="28859520" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="MC" data-field-id="29825384" data-annot-id="28859712" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="EK" data-field-id="29825720" data-annot-id="28859904" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="AG" data-field-id="29826056" data-annot-id="28860096" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="AJ" data-field-id="29826392" data-annot-id="28860288" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="AF" data-field-id="29826728" data-annot-id="28860480" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="AE" data-field-id="29827064" data-annot-id="28860672" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="CJ" data-field-id="29827400" data-annot-id="28860864" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="CH" data-field-id="29827736" data-annot-id="28861056" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="CG" data-field-id="29828072" data-annot-id="28861248" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="CF" data-field-id="29828408" data-annot-id="28861440" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="AD" data-field-id="29828744" data-annot-id="28861632" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="FJ" data-field-id="29829080" data-annot-id="28861824" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="FK" data-field-id="29829416" data-annot-id="28862016" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="CE" data-field-id="29824232" data-annot-id="28858864" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="CD" data-field-id="29830344" data-annot-id="28862736" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="ED" data-field-id="29830680" data-annot-id="28862928" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="CC" data-field-id="29831016" data-annot-id="28863120" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="CB" data-field-id="29831352" data-annot-id="28863312" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="CA" data-field-id="29831688" data-annot-id="28863504" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="BY" data-field-id="29832024" data-annot-id="28863696" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="BZ" data-field-id="29832360" data-annot-id="28863888" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="BW" data-field-id="29832696" data-annot-id="28864080" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="BV" data-field-id="29833032" data-annot-id="28864272" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="AL" data-field-id="29833368" data-annot-id="28864464" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="AK" data-field-id="29833704" data-annot-id="28864656" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="BX" data-field-id="29834040" data-annot-id="28864848" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="AV" data-field-id="29834376" data-annot-id="28865040" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="AU" data-field-id="29834712" data-annot-id="28865232" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="BU" data-field-id="29835048" data-annot-id="28865424" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="BT" data-field-id="29835384" data-annot-id="28865616" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="BS" data-field-id="29835720" data-annot-id="28865808" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="BR" data-field-id="29836056" data-annot-id="28866000" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="BQ" data-field-id="29836392" data-annot-id="28866192" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="BP" data-field-id="29836728" data-annot-id="28866384" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="AP" data-field-id="29686024" data-annot-id="28866576" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="BN" data-field-id="29686360" data-annot-id="28866768" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="BM" data-field-id="29686696" data-annot-id="28866960" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="BL" data-field-id="29687032" data-annot-id="28867152" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="BK" data-field-id="29687368" data-annot-id="28867344" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="BJ" data-field-id="29687704" data-annot-id="28867536" type="number" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="BH" data-field-id="29688040" data-annot-id="28867728" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="BG" data-field-id="29688376" data-annot-id="28867920" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="BF" data-field-id="29688712" data-annot-id="28868112" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="BE" data-field-id="29689048" data-annot-id="28868304" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="BD" data-field-id="29689384" data-annot-id="28868496" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="BC" data-field-id="29640296" data-annot-id="28862208" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="BB" data-field-id="29829624" data-annot-id="28862400" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="BA" data-field-id="29829960" data-annot-id="28869728" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="AC" data-field-id="29690632" data-annot-id="28869920" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="AB" data-field-id="29690968" data-annot-id="28870112" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="AA" data-field-id="29691304" data-annot-id="28870304" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="AN" data-field-id="29691640" data-annot-id="28870496" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="GN" data-field-id="29691976" data-annot-id="28870688" type="number" />

            <Field disabled component="input" className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="32057736" data-annot-id="31083520" type="text"  />

            <Field disabled component={ReduxPicker} handleChange={change} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="FIN_EX" data-field-id="32058072" data-annot-id="31083712" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="JA" data-field-id="39597176" data-annot-id="38622480" type="number" />

          </div>
        </div>

      </div>
    </div>
  );
};

export default Tax2033B;
