/* eslint-disable */
import React from "react";
import { Field } from "redux-form";
import { sum, useCompute, sub, setIsNothingness } from 'helpers/pdfforms';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';

import "./style.scss";

const notNeededFields = {
  'NOM_STEA': true,
  'ADR_STED': true,
  'CP_STEH': true,
  'VILLE_STEI': true,
  'SIRET_STET': true,
  'NBR_M_EX': true,
  'NBR_M_EX-1': true,
  'ADR_STEF': true,
  'ADR_STEG': true,
  'GR': true,
  'FIN_EX': true
}

const Tax2050 = (props) => {
  const { change, taxDeclarationForm } = props;


  setIsNothingness(taxDeclarationForm, notNeededFields, 'GR', change);


  useCompute('DH', sum, ['AB', 'AC'], taxDeclarationForm, change);
  useCompute('GX', sum, ['GV', 'GW'], taxDeclarationForm, change);
  useCompute('DK', sub, ['AF', 'AG'], taxDeclarationForm, change);
  useCompute('DL', sum, ['AH', 'AI'], taxDeclarationForm, change);
  useCompute('DM', sum, ['AJ', 'AK'], taxDeclarationForm, change);
  useCompute('DN', sum, ['AL', 'AM'], taxDeclarationForm, change);
  useCompute('DP', sum, ['AN', 'AO'], taxDeclarationForm, change);
  useCompute('DQ', sum, ['AR', 'AS'], taxDeclarationForm, change);
  useCompute('DS', sum, ['AT', 'AU'], taxDeclarationForm, change);
  useCompute('DT', sum, ['AV', 'AW'], taxDeclarationForm, change);
  useCompute('DU', sum, ['AX', 'AY'], taxDeclarationForm, change);
  useCompute('DV', sum, ['CS', 'CT'], taxDeclarationForm, change);
  useCompute('DW', sum, ['CU', 'CV'], taxDeclarationForm, change);
  useCompute('DX', sum, ['BB', 'BC'], taxDeclarationForm, change);
  useCompute('DY', sum, ['BD', 'BE'], taxDeclarationForm, change);
  useCompute('DZ', sum, ['BF', 'BG'], taxDeclarationForm, change);
  useCompute('EA', sum, ['BH', 'BI'], taxDeclarationForm, change);
  useCompute('BJ', sum, ['AB', 'GV', 'AF', 'AH', 'AJ', 'AL', 'AN', 'AP', 'AR', 'AT', 'AV', 'AX', 'CS', 'CU', 'BB', 'BD', 'BF', 'BH'], taxDeclarationForm, change);
  useCompute('BK', sum, ['AC', 'GW', 'AG', 'AI', 'AK', 'AM', 'AO', 'AQ', 'AS', 'AU', 'AW', 'AY', 'CT', 'CV', 'BC', 'BE', 'BG', 'BI'], taxDeclarationForm, change);
  useCompute('EB', sum, ['BJ', 'BK'], taxDeclarationForm, change);
  useCompute('EC', sum, ['BL', 'BM'], taxDeclarationForm, change);
  useCompute('ED', sum, ['BN', 'BO'], taxDeclarationForm, change);
  useCompute('EE', sum, ['BP', 'BQ'], taxDeclarationForm, change);
  useCompute('EF', sum, ['BR', 'BS'], taxDeclarationForm, change);
  useCompute('EG', sum, ['BT', 'BU'], taxDeclarationForm, change);
  useCompute('EH', sum, ['BV', 'BW'], taxDeclarationForm, change);
  useCompute('EJ', sum, ['BX', 'BY'], taxDeclarationForm, change);
  useCompute('EK', sum, ['BZ', 'CA'], taxDeclarationForm, change);
  useCompute('EL', sum, ['CB', 'CC'], taxDeclarationForm, change);
  useCompute('EM', sum, ['CD', 'CE'], taxDeclarationForm, change);
  useCompute('EN', sum, ['CF', 'CG'], taxDeclarationForm, change);
  useCompute('CH', () => 486, [], taxDeclarationForm, change);
  useCompute('EP', sum, ['CH', 'CI'], taxDeclarationForm, change);
  useCompute('CJ', sum, ['BL', 'BN', 'BP', 'BR', 'BT', 'BV', 'BX', 'BZ', 'CB', 'CD', 'CF', 'CH'], taxDeclarationForm, change);
  useCompute('CK', sum, ['BM', 'BO', 'BQ', 'BS', 'BU', 'BW', 'BY', 'CA', 'CC', 'CE', 'CG', 'CI'], taxDeclarationForm, change);
  useCompute('EQ', sum, ['CK', 'CJ'], taxDeclarationForm, change);
  useCompute('CO', sum, ['CN', 'CM', 'GS', 'CJ', 'BJ', 'AA'], taxDeclarationForm, change);
  useCompute('GH', sum, ['CK', 'BK'], taxDeclarationForm, change);
  useCompute('GJ', sum, ['CO', 'GH'], taxDeclarationForm, change);
  useCompute('CR', sum, ['GL', 'CP'], taxDeclarationForm, change);
  useCompute('GP', sum, ['GM', 'GN'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2050">

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 47, top: 168, width: 112, height: 12, 'font-size': 17, }}>
<span>
Numéro SIRET *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 517, top: 38, width: 188, height: 17, 'font-size': 25, }}>
<span>
BILAN - ACTIF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 37, width: 102, height: 13, 'font-size': 17, }}>
<span>
N° 15949 Q 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 58, width: 165, height: 12, 'font-size': 13, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 161, top: 70, width: 132, height: 12, 'font-size': 13, }}>
<span>
du code général des impôts).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 932, top: 31, width: 211, height: 15, 'font-size': 21, }}>
<span>
DGFiP N° 2050-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 98, width: 172, height: 16, 'font-size': 17, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 731, top: 98, width: 312, height: 16, 'font-size': 17, }}>
<span>
Durée de l’exercice exprimée en nombre de mois *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 130, width: 140, height: 15, 'font-size': 17, }}>
<span>
Adresse de l’entreprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 865, top: 130, width: 193, height: 15, 'font-size': 17, }}>
<span>
Durée de l’exercice précédent *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1659, width: 448, height: 14, 'font-size': 15, }}>
<span>
* Des explications concernant cette rubrique sont données dans la notice n° 2032
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 600, top: 1595, width: 19, height: 15, 'font-size': 21, }}>
<span>
CP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 766, top: 1560, width: 17, height: 14, 'font-size': 21, }}>
<span>
1A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 315, width: 141, height: 12, 'font-size': 17, }}>
<span>
Frais d’établissement *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 313, width: 20, height: 14, 'font-size': 21, }}>
<span>
AB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 313, width: 21, height: 14, 'font-size': 21, }}>
<span>
AC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 351, width: 160, height: 16, 'font-size': 17, }}>
<span>
Frais de développement *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 349, width: 21, height: 14, 'font-size': 21, }}>
<span>
CX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 349, width: 22, height: 18, 'font-size': 21, }}>
<span>
CQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 386, width: 246, height: 15, 'font-size': 17, }}>
<span>
Concessions, brevets et droits similaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 384, width: 20, height: 14, 'font-size': 21, }}>
<span>
AF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 384, width: 23, height: 14, 'font-size': 21, }}>
<span>
AG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 422, width: 136, height: 16, 'font-size': 17, }}>
<span>
Fonds commercial (1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 766, top: 419, width: 17, height: 15, 'font-size': 21, }}>
<span>
AI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 553, top: 419, width: 24, height: 15, 'font-size': 21, }}>
<span>
AH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 458, width: 229, height: 16, 'font-size': 17, }}>
<span>
Autres immobilisations incorporelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 557, top: 455, width: 16, height: 20, 'font-size': 21, }}>
<span>
AJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 455, width: 24, height: 15, 'font-size': 21, }}>
<span>
AK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 493, width: 341, height: 16, 'font-size': 17, }}>
<span>
Avances et acomptes sur immobilisations incorporelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 491, width: 21, height: 14, 'font-size': 21, }}>
<span>
AL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 761, top: 491, width: 25, height: 14, 'font-size': 21, }}>
<span>
AM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 529, width: 52, height: 12, 'font-size': 17, }}>
<span>
Terrains
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 526, width: 24, height: 15, 'font-size': 21, }}>
<span>
AN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 526, width: 23, height: 15, 'font-size': 21, }}>
<span>
AO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 565, width: 87, height: 11, 'font-size': 17, }}>
<span>
Constructions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 562, width: 20, height: 14, 'font-size': 21, }}>
<span>
AP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 562, width: 23, height: 19, 'font-size': 21, }}>
<span>
AQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 600, width: 342, height: 16, 'font-size': 17, }}>
<span>
Installations techniques, matériel et outillage industriels
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 598, width: 22, height: 14, 'font-size': 21, }}>
<span>
AR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 765, top: 598, width: 19, height: 14, 'font-size': 21, }}>
<span>
AS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 636, width: 216, height: 16, 'font-size': 17, }}>
<span>
Autres immobilisations corporelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 633, width: 21, height: 15, 'font-size': 21, }}>
<span>
AT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 633, width: 23, height: 15, 'font-size': 21, }}>
<span>
AU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 671, width: 158, height: 12, 'font-size': 17, }}>
<span>
Immobilisations en cours
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 669, width: 23, height: 15, 'font-size': 21, }}>
<span>
AV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 762, top: 669, width: 26, height: 15, 'font-size': 21, }}>
<span>
AW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 707, width: 131, height: 16, 'font-size': 17, }}>
<span>
Avances et acomptes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 704, width: 23, height: 15, 'font-size': 21, }}>
<span>
AX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 704, width: 22, height: 15, 'font-size': 21, }}>
<span>
AY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 743, width: 385, height: 16, 'font-size': 17, }}>
<span>
Participations évaluées selon la méthode de mise en équivalence
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 557, top: 740, width: 17, height: 15, 'font-size': 21, }}>
<span>
CS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 740, width: 20, height: 15, 'font-size': 21, }}>
<span>
CT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 778, width: 130, height: 16, 'font-size': 17, }}>
<span>
Autres participations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 776, width: 21, height: 14, 'font-size': 21, }}>
<span>
CU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 765, top: 776, width: 21, height: 14, 'font-size': 21, }}>
<span>
CV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 814, width: 245, height: 16, 'font-size': 17, }}>
<span>
Créances rattachées à des participations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 812, width: 20, height: 14, 'font-size': 21, }}>
<span>
BB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 765, top: 812, width: 20, height: 14, 'font-size': 21, }}>
<span>
BC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 850, width: 155, height: 12, 'font-size': 17, }}>
<span>
Autres titres immobilisés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 847, width: 22, height: 15, 'font-size': 21, }}>
<span>
BD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 848, width: 21, height: 14, 'font-size': 21, }}>
<span>
BE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 885, width: 32, height: 12, 'font-size': 17, }}>
<span>
Prêts
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 883, width: 19, height: 14, 'font-size': 21, }}>
<span>
BF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 883, width: 23, height: 14, 'font-size': 21, }}>
<span>
BG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 921, width: 226, height: 12, 'font-size': 17, }}>
<span>
Autres immobilisations financières *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 919, width: 22, height: 14, 'font-size': 21, }}>
<span>
BH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 767, top: 919, width: 16, height: 14, 'font-size': 21, }}>
<span>
BI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 992, width: 251, height: 16, 'font-size': 17, }}>
<span>
Matières premières, approvisionnements
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 990, width: 19, height: 14, 'font-size': 21, }}>
<span>
BL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 990, width: 24, height: 14, 'font-size': 21, }}>
<span>
BM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1028, width: 204, height: 16, 'font-size': 17, }}>
<span>
En cours de production de biens
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 1026, width: 22, height: 14, 'font-size': 21, }}>
<span>
BN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 1026, width: 23, height: 14, 'font-size': 21, }}>
<span>
BO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1063, width: 220, height: 16, 'font-size': 17, }}>
<span>
En cours de production de services
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 1061, width: 19, height: 14, 'font-size': 21, }}>
<span>
BP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 1061, width: 23, height: 18, 'font-size': 21, }}>
<span>
BQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1099, width: 190, height: 12, 'font-size': 17, }}>
<span>
Produits intermédiaires et finis
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 1097, width: 21, height: 14, 'font-size': 21, }}>
<span>
BR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 766, top: 1096, width: 17, height: 15, 'font-size': 21, }}>
<span>
BS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1135, width: 85, height: 12, 'font-size': 17, }}>
<span>
Marchandises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 557, top: 1132, width: 20, height: 15, 'font-size': 21, }}>
<span>
BT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 1132, width: 21, height: 15, 'font-size': 21, }}>
<span>
BU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1170, width: 247, height: 16, 'font-size': 17, }}>
<span>
Avances et acomptes versés sur commandes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 1168, width: 21, height: 14, 'font-size': 21, }}>
<span>
BV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 762, top: 1168, width: 25, height: 14, 'font-size': 21, }}>
<span>
BW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1206, width: 201, height: 16, 'font-size': 17, }}>
<span>
Clients et comptes rattachés (3)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 1204, width: 22, height: 14, 'font-size': 21, }}>
<span>
BX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 1204, width: 22, height: 14, 'font-size': 21, }}>
<span>
BY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1242, width: 119, height: 15, 'font-size': 17, }}>
<span>
Autres créances (3)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 1239, width: 20, height: 14, 'font-size': 21, }}>
<span>
BZ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 765, top: 1239, width: 20, height: 14, 'font-size': 21, }}>
<span>
CA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1277, width: 223, height: 16, 'font-size': 17, }}>
<span>
Capital souscrit et appelé, non versé
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 1275, width: 19, height: 14, 'font-size': 21, }}>
<span>
CB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 765, top: 1275, width: 20, height: 14, 'font-size': 21, }}>
<span>
CC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1304, width: 202, height: 16, 'font-size': 17, }}>
<span>
Valeurs mobilières de placement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1319, width: 437, height: 16, 'font-size': 17, }}>
<span>
(dont actions propres : ................................................................................... )
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 1317, width: 21, height: 14, 'font-size': 21, }}>
<span>
CD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 765, top: 1317, width: 20, height: 14, 'font-size': 21, }}>
<span>
CE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1348, width: 87, height: 16, 'font-size': 17, }}>
<span>
Disponibilités
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 556, top: 1346, width: 19, height: 14, 'font-size': 21, }}>
<span>
CF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 1346, width: 22, height: 14, 'font-size': 21, }}>
<span>
CG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1384, width: 203, height: 16, 'font-size': 17, }}>
<span>
Charges constatées d’avance (3)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 1382, width: 22, height: 14, 'font-size': 21, }}>
<span>
CH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 767, top: 1382, width: 15, height: 14, 'font-size': 21, }}>
<span>
CI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 454, top: 1420, width: 82, height: 15, 'font-size': 17, }}>
<span>
TOTAL (III)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 558, top: 1417, width: 14, height: 19, 'font-size': 21, }}>
<span>
CJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 1417, width: 22, height: 14, 'font-size': 21, }}>
<span>
CK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1455, width: 206, height: 16, 'font-size': 17, }}>
<span>
Frais d’émission d’emprunt à étaler
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 512, top: 1455, width: 24, height: 16, 'font-size': 17, }}>
<span>
(IV)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 553, top: 1453, width: 25, height: 14, 'font-size': 21, }}>
<span>
CW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1491, width: 209, height: 16, 'font-size': 17, }}>
<span>
Primes de remboursement des obligations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 517, top: 1491, width: 19, height: 16, 'font-size': 17, }}>
<span>
(V)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 1489, width: 23, height: 14, 'font-size': 21, }}>
<span>
CM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1524, width: 173, height: 14, 'font-size': 17, }}>
<span>
Écarts de conversion actif *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 512, top: 1527, width: 24, height: 15, 'font-size': 17, }}>
<span>
(VI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 1524, width: 22, height: 15, 'font-size': 21, }}>
<span>
CN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 280, width: 168, height: 15, 'font-size': 17, }}>
<span>
Capital souscrit non appelé
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 522, top: 280, width: 13, height: 15, 'font-size': 17, }}>
<span>
(I)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 277, width: 23, height: 14, 'font-size': 21, }}>
<span>
AA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 236, width: 25, height: 10, 'font-size': 15, }}>
<span>
Brut
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 651, top: 254, width: 5, height: 8, 'font-size': 13, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 793, top: 236, width: 138, height: 14, 'font-size': 15, }}>
<span>
Amortissements, provisions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 859, top: 254, width: 6, height: 8, 'font-size': 13, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1061, top: 236, width: 22, height: 11, 'font-size': 15, }}>
<span>
Net
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1069, top: 254, width: 6, height: 8, 'font-size': 13, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1021, top: 198, width: 102, height: 12, 'font-size': 15, }}>
<span>
Exercice N clos le,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 556, width: 13, height: 135, 'font-size': 15, }}>
<span>
ACTIF IMMOBILISÉ *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 338, width: 6, height: 141, 'font-size': 9, }}>
<span>
IMMOBILISATIONS INCORPORELLES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 768, width: 10, height: 138, 'font-size': 9, }}>
<span>
IMMOBILISATIONS FINANCIÈRES (2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 1114, width: 11, height: 123, 'font-size': 15, }}>
<span>
ACTIF CIRCULANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 1037, width: 10, height: 62, 'font-size': 15, }}>
<span>
STOCKS *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 1211, width: 13, height: 71, 'font-size': 15, }}>
<span>
CRÉANCES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 1311, width: 10, height: 50, 'font-size': 15, }}>
<span>
DIVERS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 1453, width: 14, height: 49, 'font-size': 15, }}>
<span>
Comptes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 61, top: 1433, width: 14, height: 89, 'font-size': 15, }}>
<span>
de régularisation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 556, width: 6, height: 132, 'font-size': 9, }}>
<span>
IMMOBILISATIONS CORPORELLES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 460, top: 956, width: 76, height: 15, 'font-size': 17, }}>
<span>
TOTAL (II)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 559, top: 954, width: 15, height: 19, 'font-size': 21, }}>
<span>
BJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 954, width: 23, height: 14, 'font-size': 21, }}>
<span>
BK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 345, top: 1559, width: 189, height: 18, 'font-size': 17, }}>
<span>
TOTAL GÉNÉRAL (I à VI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 1559, width: 22, height: 14, 'font-size': 21, }}>
<span>
CO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 1589, width: 107, height: 15, 'font-size': 17, }}>
<span>
(2) Part à moins d’un an des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 435, top: 1604, width: 134, height: 12, 'font-size': 17, }}>
<span>
immobilisations financières nettes :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 813, top: 1596, width: 121, height: 16, 'font-size': 17, }}>
<span>
(3) Part à plus d’un an :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 912, top: 1632, width: 63, height: 12, 'font-size': 17, }}>
<span>
Créances :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 50, top: 1624, width: 81, height: 12, 'font-size': 17, }}>
<span>
Clause de réserve
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 50, top: 1639, width: 71, height: 16, 'font-size': 17, }}>
<span>
de propriété : *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 157, top: 1632, width: 65, height: 12, 'font-size': 17, }}>
<span>
Immobilisations :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 562, top: 1632, width: 47, height: 12, 'font-size': 17, }}>
<span>
Stocks :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 50, top: 1596, width: 151, height: 16, 'font-size': 17, }}>
<span>
Renvois : (1) Dont droit au bail :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1062, top: 171, width: 39, height: 11, 'font-size': 17, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1129, top: 163, width: 7, height: 7, 'font-size': 17, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 966, top: 1595, width: 21, height: 15, 'font-size': 21, }}>
<span>
CR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 19, top: 1364, width: 11, height: 264, 'font-size': 13, }}>
<span>
N° 2050- SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 428, top: 38, width: 36, height: 36, 'font-size': 49, }}>
<span>
①
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field "      name="GR" data-field-id="40756936" data-annot-id="40046560" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="AA" data-field-id="40757256" data-annot-id="40080784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="CM" data-field-id="40757560" data-annot-id="40081008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="CN" data-field-id="40757864" data-annot-id="39806160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="AF" data-field-id="40758200" data-annot-id="39806352" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="AH" data-field-id="40758536" data-annot-id="39806544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="AJ" data-field-id="40758872" data-annot-id="39806736" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="AL" data-field-id="40759208" data-annot-id="39806928" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="AR" data-field-id="40759544" data-annot-id="40172048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="CS" data-field-id="40760024" data-annot-id="40172240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="CU" data-field-id="40760360" data-annot-id="40172432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="BB" data-field-id="40760696" data-annot-id="40172624" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="BD" data-field-id="40761032" data-annot-id="40172816" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="BF" data-field-id="40761368" data-annot-id="40173008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="BH" data-field-id="40761704" data-annot-id="40173200" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="BN" data-field-id="40762040" data-annot-id="40173392" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="BP" data-field-id="40762376" data-annot-id="40173584" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="BR" data-field-id="40762856" data-annot-id="40174048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="BT" data-field-id="40763192" data-annot-id="40174240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="BV" data-field-id="40763528" data-annot-id="40174432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="BX" data-field-id="40763864" data-annot-id="40174624" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="CD" data-field-id="40764200" data-annot-id="40174816" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="CF" data-field-id="40764536" data-annot-id="40175008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="AB" data-field-id="40764872" data-annot-id="40175200" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="GV" data-field-id="40765208" data-annot-id="40175392" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="AC" data-field-id="40765544" data-annot-id="40175584" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="GW" data-field-id="40765880" data-annot-id="40175776" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="DG" data-field-id="40766216" data-annot-id="40175968" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="DH" data-field-id="40766552" data-annot-id="40176160" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="GX" data-field-id="40766888" data-annot-id="40176352" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="AG" data-field-id="40767224" data-annot-id="40176544" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="DK" data-field-id="40767560" data-annot-id="40176736" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="AI" data-field-id="40767896" data-annot-id="40176928" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="DL" data-field-id="40762712" data-annot-id="40173776" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="AK" data-field-id="40768824" data-annot-id="40177648" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="DM" data-field-id="40769160" data-annot-id="40177840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="AM" data-field-id="40769496" data-annot-id="40178032" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="DN" data-field-id="40769832" data-annot-id="40178224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="AN" data-field-id="40770168" data-annot-id="40178416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="AO" data-field-id="40770504" data-annot-id="40178608" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="DP" data-field-id="40770840" data-annot-id="40178800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="AP" data-field-id="40771176" data-annot-id="40178992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="AQ" data-field-id="40771512" data-annot-id="40179184" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="DQ" data-field-id="40771848" data-annot-id="40179376" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="AS" data-field-id="40772184" data-annot-id="40179568" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="DR" data-field-id="40772520" data-annot-id="40179760" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="AT" data-field-id="40772856" data-annot-id="40179952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="AU" data-field-id="40773192" data-annot-id="40180144" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="DS" data-field-id="40773528" data-annot-id="40180336" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="AV" data-field-id="40773864" data-annot-id="40180528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="AW" data-field-id="40774200" data-annot-id="40180720" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="DT" data-field-id="40774536" data-annot-id="40180912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="AX" data-field-id="40774872" data-annot-id="40181104" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="AY" data-field-id="40775208" data-annot-id="40181296" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="DU" data-field-id="40775544" data-annot-id="40181488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="CT" data-field-id="40775880" data-annot-id="40181680" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="DV" data-field-id="40776216" data-annot-id="40181872" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="CV" data-field-id="40776552" data-annot-id="40182064" type="text" />

            <Field disavled component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="DW" data-field-id="40776888" data-annot-id="40182256" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="BC" data-field-id="40777224" data-annot-id="40182448" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="DX" data-field-id="40795080" data-annot-id="40182640" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="BE" data-field-id="40777560" data-annot-id="40182832" type="text" />

            <Field disavled component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="DY" data-field-id="40777896" data-annot-id="40183024" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="BG" data-field-id="40778232" data-annot-id="40183216" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="DZ" data-field-id="40778568" data-annot-id="40183408" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="BI" data-field-id="40778904" data-annot-id="40177120" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="EA" data-field-id="40756408" data-annot-id="40177312" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="BJ" data-field-id="40756024" data-annot-id="40184640" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="BK" data-field-id="40768104" data-annot-id="40184832" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="EB" data-field-id="40768440" data-annot-id="40185024" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="BL" data-field-id="40780280" data-annot-id="40185216" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="BM" data-field-id="40780616" data-annot-id="40185408" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="EC" data-field-id="40780952" data-annot-id="40185600" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="BO" data-field-id="40781288" data-annot-id="40185792" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="ED" data-field-id="40781624" data-annot-id="40185984" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="BQ" data-field-id="40781960" data-annot-id="40186176" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="EE" data-field-id="40782296" data-annot-id="40186368" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="BS" data-field-id="40782632" data-annot-id="40186560" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="EF" data-field-id="40782968" data-annot-id="40186752" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="BU" data-field-id="40783304" data-annot-id="40186944" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="EG" data-field-id="40783640" data-annot-id="40187136" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="BW" data-field-id="40783976" data-annot-id="40187328" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="EH" data-field-id="40784312" data-annot-id="40187520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="BY" data-field-id="40784648" data-annot-id="40187712" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="EJ" data-field-id="40784984" data-annot-id="40187904" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="BZ" data-field-id="40785320" data-annot-id="40188096" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="CA" data-field-id="40785656" data-annot-id="40188288" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="EK" data-field-id="40785992" data-annot-id="40188480" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="CB" data-field-id="40786328" data-annot-id="40188672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="CC" data-field-id="40786664" data-annot-id="40188864" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field " name="EL" data-field-id="40787000" data-annot-id="40189056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="CE" data-field-id="40787336" data-annot-id="40189248" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="EM" data-field-id="40787672" data-annot-id="40189440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="CG" data-field-id="40788008" data-annot-id="40189632" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="EN" data-field-id="40788360" data-annot-id="40189824" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="CH" data-field-id="40788696" data-annot-id="40190016" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="CI" data-field-id="40789032" data-annot-id="40190208" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="EP" data-field-id="40789368" data-annot-id="40190400" type="text" />

            <Field disavled component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field " name="CJ" data-field-id="40789704" data-annot-id="40190592" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="CK" data-field-id="40790040" data-annot-id="40190784" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field " name="EQ" data-field-id="40790376" data-annot-id="40190976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field " name="GS" data-field-id="40790712" data-annot-id="40191168" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field " name="CO" data-field-id="40791048" data-annot-id="40191360" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field " name="GH" data-field-id="40791384" data-annot-id="40191552" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field " name="GJ" data-field-id="40791720" data-annot-id="40191744" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field " name="GT" data-field-id="40792056" data-annot-id="40191936" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field " name="ES" data-field-id="40792392" data-annot-id="40192128" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field " name="ET" data-field-id="40792728" data-annot-id="40192320" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field " name="CR" data-field-id="40793064" data-annot-id="40192512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field " name="CP" data-field-id="40793400" data-annot-id="40192704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field " name="GL" data-field-id="40793736" data-annot-id="40192896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field " name="GM" data-field-id="40794072" data-annot-id="40193088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field " name="GN" data-field-id="40794408" data-annot-id="40193280" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field " name="GP" data-field-id="40794744" data-annot-id="40193472" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="41301672" data-annot-id="40696608" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STED" data-field-id="41302008" data-annot-id="40696800" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEF" data-field-id="41302344" data-annot-id="40696992" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEG" data-field-id="41302680" data-annot-id="40697184" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field pde-form-field-text" name="CP_STEH" data-field-id="41303016" data-annot-id="40697376" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field pde-form-field-text" name="VILLE_STEI" data-field-id="41303352" data-annot-id="40697568" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field pde-form-field-text" name="NBR_M_EX" data-field-id="41303688" data-annot-id="40697760" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field pde-form-field-text" name="NBR_M_EX-1" data-field-id="41304024" data-annot-id="40697952" type="text" disabled />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field pde-form-field-text" name="FIN_EX" data-field-id="41304360" data-annot-id="40698144" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field " name="SIRET_STET" data-field-id="41304696" data-annot-id="40698336" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2050;
