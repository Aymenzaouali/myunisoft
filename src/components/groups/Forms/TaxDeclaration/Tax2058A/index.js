import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {parse as p, sum, useCompute, setIsNothingness} from "helpers/pdfforms";
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';

import './style.scss';


const amountMoreZero = (values, fields) => {
  const diff = p(values[fields[0]]) - p(values[fields[1]]);
  return diff > 0 ? diff : 0;
};

const amountLessZero = (values, fields) => {
  const diff = p(values[fields[0]]) - p(values[fields[1]]);
  return diff < 0 ? diff : 0;
};

const notNeededFields = {
  'NOM_STEA': true,
  'FIN_EX': true,
  'ZM': true
};
const sameValue = (values, fields) => {
  return p(values[fields[0]]);
}

const Tax2058A = (props) => {
  const { taxDeclarationForm, change } = props;

  setIsNothingness(taxDeclarationForm, notNeededFields, 'ZM', change);

  useCompute('WR', sum, ['WA', 'WB', 'XE', 'XW', 'XY', 'VG', 'BL', 'VJ', 'VC', 'WN', 'WO', 'XR', 'WQ', 'AG', 'AJ'], taxDeclarationForm, change);
  useCompute('XH', sum, ['WS', 'WT', 'WU', 'WV', 'VE', 'VF', 'WW', 'XB', 'VL', 'WZ', 'XA', 'ZY', 'XD', 'XF', 'XS', 'XG', 'AH'], taxDeclarationForm, change);
  useCompute('XI', amountMoreZero, ['WR', 'XH'], taxDeclarationForm, change);
  useCompute('XJ', amountLessZero, ['WR', 'XH'], taxDeclarationForm, change);
  useCompute('XO', sameValue, ['XJ'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2058a">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 271, top: 35, width: 36, height: 36, 'font-size': 47, }}>
<span>
È
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 370, top: 39, width: 453, height: 21, 'font-size': 25, }}>
<span>
DÉTERMINATION DU RÉSULTAT FISCAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 54, width: 57, height: 14, 'font-size': 21, }}>
<span>
DGFiP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1022, top: 51, width: 164, height: 19, 'font-size': 25, }}>
<span>
N° 2058-A-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 16, top: 1343, width: 13, height: 269, 'font-size': 13, }}>
<span>
N° 2058-A-SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1025, top: 90, width: 136, height: 16, 'font-size': 17, }}>
<span>
Exercice N, clos le :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 58, width: 173, height: 12, 'font-size': 12, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 48, top: 70, width: 143, height: 12, 'font-size': 12, }}>
<span>
du Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 108, width: 193, height: 16, 'font-size': 17, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 99, width: 253, height: 17, 'font-size': 17, }}>
<span>
Formulaire déposé au titre de l’IR E T
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 886, top: 101, width: 40, height: 12, 'font-size': 17, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 952, top: 100, width: 6, height: 7, 'font-size': 23, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 127, width: 198, height: 20, 'font-size': 21, }}>
<span>
I. RÉINTÉGRATIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 589, top: 130, width: 301, height: 15, 'font-size': 17, }}>
<span>
BÉNÉFICE COMPTABLE DE L’EXERCICE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 135, width: 25, height: 11, 'font-size': 17, }}>
<span>
WA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 161, width: 16, height: 243, 'font-size': 17, }}>
<span>
Charges non admises en déduction
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 56, top: 226, width: 13, height: 112, 'font-size': 17, }}>
<span>
du résultat fiscal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 164, width: 513, height: 16, 'font-size': 17, }}>
<span>
Rémunération du travail de l’exploitant ou des associés (entreprises à l’IR)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 165, width: 24, height: 12, 'font-size': 17, }}>
<span>
WB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 189, width: 222, height: 16, 'font-size': 17, }}>
<span>
Avantages personnels non déductibles *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 203, width: 260, height: 16, 'font-size': 17, }}>
<span>
(sauf amortissements à porter ligne ci-dessous)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 403, top: 197, width: 27, height: 12, 'font-size': 17, }}>
<span>
WD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 546, top: 188, width: 269, height: 15, 'font-size': 17, }}>
<span>
Amortissements excédentaires (art. 39-4 du CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 203, width: 228, height: 12, 'font-size': 17, }}>
<span>
et autres amortissements non déductibles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 828, top: 197, width: 24, height: 12, 'font-size': 17, }}>
<span>
WE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 197, width: 20, height: 11, 'font-size': 17, }}>
<span>
XE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 220, width: 222, height: 16, 'font-size': 17, }}>
<span>
Autres charges et dépenses somptuaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 234, width: 108, height: 14, 'font-size': 17, }}>
<span>
(art. 39-4 du C.G.I.)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 405, top: 228, width: 24, height: 11, 'font-size': 17, }}>
<span>
WF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 546, top: 220, width: 193, height: 13, 'font-size': 17, }}>
<span>
Taxe sur les véhicules des sociétés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 234, width: 100, height: 16, 'font-size': 17, }}>
<span>
(entreprises à l’IS)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 826, top: 228, width: 27, height: 11, 'font-size': 17, }}>
<span>
WG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 252, width: 278, height: 16, 'font-size': 17, }}>
<span>
Fraction des loyers à réintégrer dans le cadre d’un
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 266, width: 234, height: 16, 'font-size': 17, }}>
<span>
crédit-bail immobilier et de levée d’option
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 407, top: 260, width: 20, height: 12, 'font-size': 17, }}>
<span>
RA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 546, top: 249, width: 7, height: 32, 'font-size': 33, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 252, width: 232, height: 16, 'font-size': 17, }}>
<span>
Part des loyers dispensée de réintégration
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 266, width: 101, height: 14, 'font-size': 17, }}>
<span>
(art. 239 sexies D)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 830, top: 260, width: 19, height: 12, 'font-size': 17, }}>
<span>
RB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 956, top: 249, width: 8, height: 32, 'font-size': 33, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 286, width: 188, height: 16, 'font-size': 17, }}>
<span>
Provisions et charges à payer non
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 300, width: 229, height: 15, 'font-size': 17, }}>
<span>
déductibles (cf. tableau 2058-B, cadre III)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 407, top: 293, width: 20, height: 12, 'font-size': 17, }}>
<span>
WI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 286, width: 250, height: 16, 'font-size': 17, }}>
<span>
Charges à payer liées à des états et territoires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 546, top: 299, width: 255, height: 17, 'font-size': 17, }}>
<span>
non coopératifs non déductibles (cf. 2067-bis)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 829, top: 293, width: 22, height: 12, 'font-size': 17, }}>
<span>
XX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 293, width: 25, height: 12, 'font-size': 17, }}>
<span>
XW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 323, width: 119, height: 16, 'font-size': 17, }}>
<span>
Amendes et pénalités
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 407, top: 325, width: 20, height: 14, 'font-size': 17, }}>
<span>
WJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 324, width: 189, height: 16, 'font-size': 17, }}>
<span>
Charges financières (art. 212 bis) *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 830, top: 324, width: 20, height: 12, 'font-size': 17, }}>
<span>
XZ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 355, width: 319, height: 16, 'font-size': 17, }}>
<span>
Réintégrations prévues à l’article 155 du CGI *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 356, width: 22, height: 11, 'font-size': 17, }}>
<span>
XY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 386, width: 381, height: 16, 'font-size': 17, }}>
<span>
Impôt sur les sociétés (cf. page 9 de la notice 2032-NOT-SD)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 387, width: 16, height: 12, 'font-size': 17, }}>
<span>
I7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 420, width: 75, height: 15, 'font-size': 17, }}>
<span>
Quote-part
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 415, width: 147, height: 12, 'font-size': 13, }}>
<span>
Bénéfices réalisés par une société
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 427, width: 110, height: 12, 'font-size': 13, }}>
<span>
de personnes ou un GIE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 279, top: 421, width: 23, height: 11, 'font-size': 17, }}>
<span>
WL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 415, width: 125, height: 10, 'font-size': 13, }}>
<span>
Résultats bénéficiaires visés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 427, width: 109, height: 11, 'font-size': 13, }}>
<span>
à l’article 209 B du CGI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 688, top: 421, width: 16, height: 12, 'font-size': 17, }}>
<span>
L7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 420, width: 18, height: 12, 'font-size': 17, }}>
<span>
K7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 444, width: 13, height: 119, 'font-size': 13, }}>
<span>
Régimes d’imposition
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 56, top: 467, width: 13, height: 75, 'font-size': 13, }}>
<span>
particuliers et
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 70, top: 447, width: 13, height: 114, 'font-size': 13, }}>
<span>
impositions différées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 444, width: 90, height: 12, 'font-size': 17, }}>
<span>
Moins-values
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 171, top: 462, width: 41, height: 10, 'font-size': 17, }}>
<span>
nettes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 476, width: 7, height: 12, 'font-size': 17, }}>
<span>
à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 492, width: 75, height: 16, 'font-size': 17, }}>
<span>
long terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 300, top: 441, width: 9, height: 60, 'font-size': 75, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 452, width: 661, height: 16, 'font-size': 17, }}>
<span>
– imposées aux taux de 15 % ou de 19 % (12,80 % pour les entreprises à l’impôt sur le revenu) I8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 483, width: 184, height: 16, 'font-size': 17, }}>
<span>
– imposées au taux de 0 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 483, width: 21, height: 12, 'font-size': 17, }}>
<span>
ZN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 175, top: 520, width: 306, height: 16, 'font-size': 17, }}>
<span>
Fraction imposable des plus-values réalisées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 217, top: 539, width: 222, height: 12, 'font-size': 17, }}>
<span>
au cours d’exercices antérieurs *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 510, width: 6, height: 45, 'font-size': 55, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 596, top: 514, width: 229, height: 13, 'font-size': 17, }}>
<span>
– Plus-values nettes à court terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 515, width: 25, height: 12, 'font-size': 17, }}>
<span>
WN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 596, top: 545, width: 309, height: 16, 'font-size': 17, }}>
<span>
– Plus-values soumises au régime des fusions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 546, width: 22, height: 12, 'font-size': 17, }}>
<span>
WO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 574, width: 426, height: 18, 'font-size': 17, }}>
<span>
Écarts de valeurs liquidatives sur OPCVM * (entreprises à l’IS)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 578, width: 21, height: 11, 'font-size': 17, }}>
<span>
XR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 617, width: 171, height: 16, 'font-size': 17, }}>
<span>
Réintégrations diverses à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 636, width: 243, height: 16, 'font-size': 17, }}>
<span>
détailler sur feuillet séparé DONT *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 354, top: 603, width: 128, height: 12, 'font-size': 17, }}>
<span>
Intérêts excédentaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 354, top: 617, width: 134, height: 14, 'font-size': 17, }}>
<span>
(art. 39-1-3e et 212 du C.G.I.)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 520, top: 610, width: 19, height: 12, 'font-size': 17, }}>
<span>
SU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 681, top: 603, width: 103, height: 16, 'font-size': 17, }}>
<span>
Zones d’entreprises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 606, width: 4, height: 5, 'font-size': 13, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 681, top: 617, width: 110, height: 14, 'font-size': 17, }}>
<span>
(activité exonérée)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 809, top: 610, width: 23, height: 12, 'font-size': 17, }}>
<span>
SW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 610, width: 22, height: 15, 'font-size': 17, }}>
<span>
WQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 354, top: 636, width: 156, height: 16, 'font-size': 17, }}>
<span>
Déficits étrangers antérieurement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 354, top: 650, width: 149, height: 16, 'font-size': 17, }}>
<span>
déduits par les PME (art. 209 C)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 520, top: 644, width: 19, height: 12, 'font-size': 17, }}>
<span>
SX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 681, top: 636, width: 116, height: 16, 'font-size': 17, }}>
<span>
Quote-part de 12 % des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 681, top: 650, width: 112, height: 16, 'font-size': 17, }}>
<span>
plus-values à taux zéro
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 809, top: 644, width: 21, height: 12, 'font-size': 17, }}>
<span>
M8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 676, width: 633, height: 16, 'font-size': 17, }}>
<span>
Réintégration des charges affectées aux activités éligibles au régime de taxation au tonnage
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 677, width: 17, height: 12, 'font-size': 17, }}>
<span>
Y1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 707, width: 596, height: 16, 'font-size': 17, }}>
<span>
Résultat fiscal afférent à l’activité relevant du régime optionnel de taxation au tonnage
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 708, width: 18, height: 13, 'font-size': 17, }}>
<span>
Y3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 898, top: 740, width: 98, height: 12, 'font-size': 17, }}>
<span>
TOTAL I WR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 766, width: 161, height: 19, 'font-size': 21, }}>
<span>
I I. DÉDUCTIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 680, top: 771, width: 314, height: 13, 'font-size': 17, }}>
<span>
PERTE COMPTABLE DE L’EXERCICE WS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 803, width: 533, height: 16, 'font-size': 17, }}>
<span>
Quote-part dans les pertes subies par une société de personne ou un G.I.E. *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 803, width: 23, height: 12, 'font-size': 17, }}>
<span>
WT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 834, width: 952, height: 16, 'font-size': 17, }}>
<span>
Provisions et charges à payer non déductibles, antérieurement taxées, et réintégrées dans les résultats comptables de l’exercice (cf. tableau 2058-B-SD, cadre III) WU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 922, width: 13, height: 119, 'font-size': 13, }}>
<span>
Régimes d’imposition
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 944, width: 13, height: 75, 'font-size': 13, }}>
<span>
particuliers et
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 71, top: 925, width: 13, height: 114, 'font-size': 13, }}>
<span>
impositions différées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 901, width: 77, height: 13, 'font-size': 17, }}>
<span>
Plus-values
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 928, width: 53, height: 12, 'font-size': 17, }}>
<span>
nettes à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 954, width: 74, height: 16, 'font-size': 17, }}>
<span>
long terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 291, top: 856, width: 9, height: 151, 'font-size': 189, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 865, width: 574, height: 16, 'font-size': 17, }}>
<span>
– imposées au taux de 15 % (12,80 % pour les entreprises soumises à l’impôt sur le revenu)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 866, width: 23, height: 12, 'font-size': 17, }}>
<span>
WV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 896, width: 191, height: 16, 'font-size': 17, }}>
<span>
– imposées au taux de 0 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 897, width: 23, height: 12, 'font-size': 17, }}>
<span>
WH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 928, width: 199, height: 16, 'font-size': 17, }}>
<span>
– imposées au taux de 19 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 929, width: 22, height: 11, 'font-size': 17, }}>
<span>
WP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 959, width: 444, height: 16, 'font-size': 17, }}>
<span>
– imputées sur les moins-values nettes à long terme antérieures
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 960, width: 23, height: 11, 'font-size': 17, }}>
<span>
WW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 990, width: 255, height: 16, 'font-size': 17, }}>
<span>
– imputées sur les déficits antérieurs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 991, width: 19, height: 12, 'font-size': 17, }}>
<span>
XB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1021, width: 314, height: 17, 'font-size': 17, }}>
<span>
Autres plus-values imposées au taux de 19 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1021, width: 13, height: 13, 'font-size': 17, }}>
<span>
I6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1052, width: 615, height: 16, 'font-size': 17, }}>
<span>
Fraction des plus-values nettes à court terme de l’exercice dont l’imposition est différée *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 1053, width: 22, height: 12, 'font-size': 17, }}>
<span>
WZ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1079, width: 292, height: 16, 'font-size': 17, }}>
<span>
Régime des sociétés mères et des filiales *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1093, width: 290, height: 16, 'font-size': 17, }}>
<span>
Produit net des actions et parts d’intérêts :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 452, top: 1075, width: 7, height: 33, 'font-size': 35, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 476, top: 1079, width: 306, height: 16, 'font-size': 17, }}>
<span>
Quote-part de frais et charges restant imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 476, top: 1093, width: 279, height: 16, 'font-size': 17, }}>
<span>
à déduire des produits nets de participation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 813, top: 1087, width: 19, height: 12, 'font-size': 17, }}>
<span>
2A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 1075, width: 7, height: 33, 'font-size': 35, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1086, width: 19, height: 12, 'font-size': 17, }}>
<span>
XA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 1464, width: 388, height: 16, 'font-size': 17, }}>
<span>
Résultat fiscal avant imputation des déficits reportables :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 1443, width: 7, height: 49, 'font-size': 61, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 1448, width: 142, height: 15, 'font-size': 17, }}>
<span>
bénéfice (I moins II)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 778, top: 1450, width: 16, height: 11, 'font-size': 17, }}>
<span>
XI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 1479, width: 126, height: 14, 'font-size': 17, }}>
<span>
déficit (II moins I)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1481, width: 16, height: 14, 'font-size': 17, }}>
<span>
XJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 1511, width: 404, height: 16, 'font-size': 17, }}>
<span>
Déficit de l’exercice reporté en arrière (entreprises à l’IS) *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 777, top: 1511, width: 20, height: 12, 'font-size': 17, }}>
<span>
Z L
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 1542, width: 527, height: 16, 'font-size': 17, }}>
<span>
Déficits antérieurs imputés sur les résultats de l’exercice (entreprises à l’IS) *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1543, width: 19, height: 11, 'font-size': 17, }}>
<span>
XL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 1570, width: 608, height: 19, 'font-size': 17, }}>
<span>
RÉSULTAT FISCAL BÉNÉFICE (ligne XN) ou DÉFICIT reportable en avant (ligne XO)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 775, top: 1573, width: 23, height: 12, 'font-size': 17, }}>
<span>
XN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 1573, width: 23, height: 12, 'font-size': 17, }}>
<span>
XO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 37, top: 1603, width: 8, height: 9, 'font-size': 25, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 1603, width: 540, height: 14, 'font-size': 15, }}>
<span>
Des explications concernant cette rubrique sont données dans la notice n° 2032-NOT-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 1149, width: 10, height: 110, 'font-size': 13, }}>
<span>
Mesures d’incitation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1120, width: 633, height: 13, 'font-size': 17, }}>
<span>
Déduction autorisée au titre des investissements réalisés dans les collectivités d’outre-mer *.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1120, width: 19, height: 12, 'font-size': 17, }}>
<span>
ZY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1150, width: 201, height: 17, 'font-size': 17, }}>
<span>
Majoration d’amortissement *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 1152, width: 20, height: 11, 'font-size': 17, }}>
<span>
XD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 1203, width: 10, height: 64, 'font-size': 13, }}>
<span>
Abattement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 1194, width: 10, height: 82, 'font-size': 13, }}>
<span>
sur le bénéfice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 118, top: 1185, width: 9, height: 92, 'font-size': 13, }}>
<span>
et exonérations *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1177, width: 124, height: 12, 'font-size': 13, }}>
<span>
Entreprises nouvelles - (Reprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1191, width: 135, height: 12, 'font-size': 13, }}>
<span>
d’entreprises en difficultés 44 septies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 1183, width: 15, height: 13, 'font-size': 17, }}>
<span>
K9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1177, width: 82, height: 12, 'font-size': 13, }}>
<span>
Entreprises nouvelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1191, width: 37, height: 11, 'font-size': 13, }}>
<span>
(44 sexies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 1183, width: 16, height: 12, 'font-size': 17, }}>
<span>
L2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 698, top: 1177, width: 102, height: 12, 'font-size': 13, }}>
<span>
Jeunes entreprises innovantes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 699, top: 1191, width: 60, height: 11, 'font-size': 13, }}>
<span>
(art. 44 sexies A)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 815, top: 1183, width: 15, height: 13, 'font-size': 17, }}>
<span>
L5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1184, width: 18, height: 11, 'font-size': 17, }}>
<span>
XF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1208, width: 125, height: 12, 'font-size': 13, }}>
<span>
Pôle de compétitivité hors CICE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 1222, width: 65, height: 11, 'font-size': 13, }}>
<span>
(Art. 44 undecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 286, top: 1213, width: 13, height: 13, 'font-size': 17, }}>
<span>
L6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1208, width: 131, height: 10, 'font-size': 13, }}>
<span>
Société investissements immobilier
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1222, width: 64, height: 11, 'font-size': 13, }}>
<span>
cotées (art. 208 C)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 1214, width: 18, height: 13, 'font-size': 17, }}>
<span>
K3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 699, top: 1208, width: 98, height: 10, 'font-size': 13, }}>
<span>
Zone de restructuration de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 699, top: 1222, width: 87, height: 11, 'font-size': 13, }}>
<span>
la défense (44 terdecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 813, top: 1215, width: 20, height: 11, 'font-size': 17, }}>
<span>
PA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1240, width: 30, height: 9, 'font-size': 13, }}>
<span>
ZFU-TE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1253, width: 91, height: 11, 'font-size': 13, }}>
<span>
(art. 44 octies et octies A)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 283, top: 1245, width: 20, height: 14, 'font-size': 17, }}>
<span>
ØV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1239, width: 119, height: 12, 'font-size': 13, }}>
<span>
Bassin d’emploi à redynamiser
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1253, width: 67, height: 11, 'font-size': 13, }}>
<span>
(art. 44 duodecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 1246, width: 16, height: 11, 'font-size': 17, }}>
<span>
1F
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 699, top: 1239, width: 90, height: 10, 'font-size': 13, }}>
<span>
Zone franche d’activité
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 699, top: 1253, width: 75, height: 13, 'font-size': 13, }}>
<span>
(art. 44 quaterdecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 812, top: 1246, width: 21, height: 11, 'font-size': 17, }}>
<span>
XC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1270, width: 103, height: 13, 'font-size': 13, }}>
<span>
Bassin urbain à dynamiser
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1285, width: 71, height: 11, 'font-size': 13, }}>
<span>
(art. 44 sexdecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 1278, width: 15, height: 11, 'font-size': 17, }}>
<span>
PP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1278, width: 187, height: 13, 'font-size': 13, }}>
<span>
Zone de revitalisation rurale (art. 44 quindecies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 813, top: 1277, width: 19, height: 12, 'font-size': 17, }}>
<span>
PC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1305, width: 426, height: 18, 'font-size': 17, }}>
<span>
Écarts de valeurs liquidatives sur OPCVM * (entreprises à l’IS)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1308, width: 18, height: 12, 'font-size': 17, }}>
<span>
XS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 1338, width: 213, height: 12, 'font-size': 17, }}>
<span>
Déductions diverses à détailler
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 1354, width: 122, height: 16, 'font-size': 17, }}>
<span>
sur feuillet séparé
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 296, top: 1332, width: 92, height: 12, 'font-size': 15, }}>
<span>
dont déduction
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 281, top: 1347, width: 122, height: 14, 'font-size': 15, }}>
<span>
exceptionnelle pour
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 294, top: 1361, width: 96, height: 11, 'font-size': 15, }}>
<span>
investissement *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 423, top: 1347, width: 19, height: 13, 'font-size': 17, }}>
<span>
X9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 628, top: 1339, width: 142, height: 15, 'font-size': 15, }}>
<span>
Créance dégagée par le
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 619, top: 1354, width: 160, height: 14, 'font-size': 15, }}>
<span>
report en arrière de déficit
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 814, top: 1346, width: 17, height: 12, 'font-size': 17, }}>
<span>
Z I
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 1335, width: 7, height: 33, 'font-size': 35, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 1347, width: 23, height: 11, 'font-size': 17, }}>
<span>
XG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 1384, width: 610, height: 16, 'font-size': 17, }}>
<span>
Déduction des produits affectés aux activités éligibles au régime de taxation au tonnage
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1385, width: 18, height: 11, 'font-size': 17, }}>
<span>
Y2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1411, width: 217, height: 20, 'font-size': 21, }}>
<span>
I I I. RÉSULTAT FISCAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 896, top: 1416, width: 98, height: 13, 'font-size': 17, }}>
<span>
TOTAL I I XH
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="ZM" data-field-id="33700392" data-annot-id="32422016" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="WA" data-field-id="33700872" data-annot-id="32422208" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="WB" data-field-id="33701208" data-annot-id="32422400" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="XN" data-field-id="33701512" data-annot-id="32422592" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="XO" data-field-id="33875608" data-annot-id="32422784" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="ZL" data-field-id="33875944" data-annot-id="32718544" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="XL" data-field-id="33876280" data-annot-id="32718736" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="XI" data-field-id="33876616" data-annot-id="32718928" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="XJ" data-field-id="33876952" data-annot-id="32719120" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="XH" data-field-id="33877432" data-annot-id="32719456" type="number" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="AH" data-field-id="33877768" data-annot-id="32719648" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="XG" data-field-id="33878104" data-annot-id="32719840" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="ZI" data-field-id="33878440" data-annot-id="32720032" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="AF" data-field-id="33878776" data-annot-id="32720224" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="PC" data-field-id="33879112" data-annot-id="32720416" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="AM" data-field-id="33879448" data-annot-id="32720608" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="BA" data-field-id="33879784" data-annot-id="32720800" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="BS" data-field-id="33880264" data-annot-id="32721264" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="BM" data-field-id="33880600" data-annot-id="32721456" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="VK" data-field-id="33880936" data-annot-id="32721648" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="BH" data-field-id="33881272" data-annot-id="32721840" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="BN" data-field-id="33881608" data-annot-id="32722032" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="BT" data-field-id="33881944" data-annot-id="32722224" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="PA" data-field-id="33882280" data-annot-id="32722416" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="BR" data-field-id="33882616" data-annot-id="32722608" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="XS" data-field-id="33882952" data-annot-id="32722800" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="XF" data-field-id="33883288" data-annot-id="32722992" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="XD" data-field-id="33883624" data-annot-id="32723184" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="ZY" data-field-id="33883960" data-annot-id="32723376" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="XA" data-field-id="33884296" data-annot-id="32723568" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="WZ" data-field-id="33884632" data-annot-id="32723760" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="VL" data-field-id="33884968" data-annot-id="32723952" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="AE" data-field-id="33885304" data-annot-id="32724144" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="XB" data-field-id="33880088" data-annot-id="32720992" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="WW" data-field-id="33886200" data-annot-id="32724864" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="VF" data-field-id="33886536" data-annot-id="32725056" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="VE" data-field-id="33886872" data-annot-id="32725248" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="WV" data-field-id="33887208" data-annot-id="32725440" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="WU" data-field-id="33887544" data-annot-id="32725632" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="WT" data-field-id="33887880" data-annot-id="32725824" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="WS" data-field-id="33888216" data-annot-id="32726016" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="WR" data-field-id="33888552" data-annot-id="32726208" type="number" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="AJ" data-field-id="33888888" data-annot-id="32726400" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="AG" data-field-id="33889224" data-annot-id="32726592" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="WQ" data-field-id="33889560" data-annot-id="32726784" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="ZZ" data-field-id="33889896" data-annot-id="32726976" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="SW" data-field-id="33890232" data-annot-id="32727168" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="XR" data-field-id="33890568" data-annot-id="32727360" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="WO" data-field-id="33890904" data-annot-id="32727552" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="WN" data-field-id="33891240" data-annot-id="32727744" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="VC" data-field-id="33891576" data-annot-id="32727936" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="VJ" data-field-id="33891912" data-annot-id="32728128" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="BL" data-field-id="33892248" data-annot-id="32728320" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="VG" data-field-id="33892584" data-annot-id="32728512" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="XY" data-field-id="33892920" data-annot-id="32728704" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="BK" data-field-id="33893256" data-annot-id="32728896" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="SX" data-field-id="33893592" data-annot-id="32729088" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="SU" data-field-id="33893928" data-annot-id="32729280" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="WL" data-field-id="33743192" data-annot-id="32729472" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="XW" data-field-id="33743528" data-annot-id="32729664" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="XE" data-field-id="33743864" data-annot-id="32729856" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="XZ" data-field-id="33744200" data-annot-id="32730048" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="XX" data-field-id="33744536" data-annot-id="32730240" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="AL" data-field-id="33744872" data-annot-id="32730432" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="WG" data-field-id="33745208" data-annot-id="32730624" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="WE" data-field-id="33700568" data-annot-id="32724336" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="WJ" data-field-id="33815592" data-annot-id="32724528" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="WI" data-field-id="33885512" data-annot-id="32731856" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="AK" data-field-id="33885816" data-annot-id="32732048" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="WF" data-field-id="33746456" data-annot-id="32732240" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="WD" data-field-id="33746792" data-annot-id="32732432" type="number" />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field" name="FIN_EX" data-field-id="12198328" data-annot-id="11028688" type="number" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="12198664" data-annot-id="11028880" type="text" disabled />

          </div>
        </div>

      </div>
    </div>
  );
};

export default Tax2058A;
