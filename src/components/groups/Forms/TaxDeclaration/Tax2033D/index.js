import React from "react";
import { Field } from 'redux-form';
import {useCompute, sum, setIsNothingness, parse as p, setValue} from "helpers/pdfforms";
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { get as _get } from 'lodash';

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const amountOfTheEnd = (values, fields) => (
  p(values[fields[0]]) + p(values[fields[1]]) - p(values[fields[2]])
);

const Tax2033D = (props) => {
  const { taxDeclarationForm, form2033B, change } = props;
  const ER = _get(form2033B, 'ER', null);

  const notNeededProps = { 
    'NOM_STEA': true,
    'PF': true 
  };
   
  setIsNothingness(taxDeclarationForm, notNeededProps, 'PF', change);

  useCompute('AH', sum, ['AA', 'AN', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG'], taxDeclarationForm, change);
  useCompute('BH', sum, ['BA', 'BN', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG'], taxDeclarationForm, change);
  useCompute('CH', sum, ['CA', 'CN', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG'], taxDeclarationForm, change);
  useCompute('DH', sum, ['DA', 'DN', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG'], taxDeclarationForm, change);

  useCompute('DA', amountOfTheEnd, ['AA', 'BA', 'CA'], taxDeclarationForm, change);
  useCompute('DN', amountOfTheEnd, ['AN', 'BN', 'CN'], taxDeclarationForm, change);
  useCompute('DB', amountOfTheEnd, ['AB', 'BB', 'CB'], taxDeclarationForm, change);
  useCompute('DC', amountOfTheEnd, ['AC', 'BC', 'CC'], taxDeclarationForm, change);
  useCompute('DD', amountOfTheEnd, ['AD', 'BD', 'CD'], taxDeclarationForm, change);
  useCompute('DE', amountOfTheEnd, ['AE', 'BE', 'CE'], taxDeclarationForm, change);
  useCompute('DF', amountOfTheEnd, ['AF', 'BF', 'CF'], taxDeclarationForm, change);
  useCompute('DG', amountOfTheEnd, ['AG', 'BG', 'CG'], taxDeclarationForm, change);

  useCompute('EH', sum, ['EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG'], taxDeclarationForm, change);
  useCompute('FH', sum, ['FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG'], taxDeclarationForm, change);

  useCompute('HH', sum, ['HA', 'HJ1', 'HJ2', 'HJ3', 'HJ4', 'HJ5', 'HJ6'], taxDeclarationForm, change);
  useCompute('MH', sum, ['PG', 'PH', 'PJ', 'MG'], taxDeclarationForm, change);

  setValue(taxDeclarationForm, 'MG', ER, change);

  return (
    <div className="form-tax-declaration-2033d">
      <div id="pdf-document" data-type="pdf-document" data-num-pages="1" data-layout="fixed">

      </div>

      <form id="acroform"></form>

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.293333" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 301, top: 83, width: 33, height: 33, 'font-size': 43, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 384, top: 74, width: 477, height: 20, 'font-size': 23, }}>
<span>
RELEVÉ DES PROVISIONS - AMORTISSEMENTS DÉROGATOIRES -
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 497, top: 100, width: 251, height: 21, 'font-size': 23, }}>
<span>
DÉFICITS REPORTABLES - DIVERS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 44, width: 52, height: 13, 'font-size': 19, }}>
<span>
DGFiP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 1209, width: 11, height: 231, 'font-size': 12, }}>
<span>
N° 2033-D-SD – (SDNC-D  GFi  P) - Janvi  er 20  1 9 
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 103, width: 195, height: 12, 'font-size': 12, }}>
<span>
Formulaire obligatoire (article 302 septies
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 87, top: 114, width: 166, height: 12, 'font-size': 12, }}>
<span>
A bis du Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 181, width: 178, height: 15, 'font-size': 15, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 316, top: 182, width: 771, height: 12, 'font-size': 12, }}>
<span>
_________________________________________________________________________________________________________________________________Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1112, top: 181, width: 7, height: 7, 'font-size': 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 223, width: 576, height: 20, 'font-size': 23, }}>
<span>
IRELEVÉ DES PROVISIONS - AMORTISSEMENTS DÉROGATOIRES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 286, width: 15, height: 15, 'font-size': 21, }}>
<span>
A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 290, width: 174, height: 11, 'font-size': 15, }}>
<span>
NATURE DES PROVISIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 446, top: 279, width: 114, height: 11, 'font-size': 15, }}>
<span>
Montant au début
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 464, top: 296, width: 79, height: 12, 'font-size': 15, }}>
<span>
de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 636, top: 279, width: 103, height: 14, 'font-size': 15, }}>
<span>
Augmentations :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 621, top: 296, width: 134, height: 12, 'font-size': 15, }}>
<span>
dotations de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 835, top: 279, width: 77, height: 11, 'font-size': 15, }}>
<span>
Diminutions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 296, width: 133, height: 15, 'font-size': 15, }}>
<span>
reprises de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1033, top: 280, width: 52, height: 10, 'font-size': 15, }}>
<span>
Montant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 995, top: 296, width: 128, height: 12, 'font-size': 15, }}>
<span>
à la fin de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 361, width: 65, height: 11, 'font-size': 15, }}>
<span>
Provisions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 378, width: 83, height: 14, 'font-size': 15, }}>
<span>
réglementées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 335, width: 230, height: 16, 'font-size': 15, }}>
<span>
Amortissements dérogatoires600
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 335, width: 19, height: 13, 'font-size': 15, }}>
<span>
602
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 335, width: 20, height: 13, 'font-size': 15, }}>
<span>
604
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 335, width: 20, height: 13, 'font-size': 15, }}>
<span>
606
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 364, width: 109, height: 14, 'font-size': 15, }}>
<span>
Dont majorations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 375, width: 154, height: 15, 'font-size': 15, }}>
<span>
exceptionnelles de 30 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 402, top: 368, width: 19, height: 13, 'font-size': 15, }}>
<span>
601
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 368, width: 19, height: 14, 'font-size': 15, }}>
<span>
603
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 368, width: 19, height: 14, 'font-size': 15, }}>
<span>
605
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 368, width: 20, height: 13, 'font-size': 15, }}>
<span>
607
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 401, width: 230, height: 16, 'font-size': 15, }}>
<span>
Autres provisions réglementées * 610
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 401, width: 19, height: 12, 'font-size': 15, }}>
<span>
612
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 401, width: 20, height: 12, 'font-size': 15, }}>
<span>
614
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 401, width: 20, height: 12, 'font-size': 15, }}>
<span>
616
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 435, width: 217, height: 15, 'font-size': 15, }}>
<span>
Provisions pour risques et charges
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 435, width: 19, height: 12, 'font-size': 15, }}>
<span>
622
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 402, top: 435, width: 20, height: 12, 'font-size': 15, }}>
<span>
620
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 435, width: 20, height: 12, 'font-size': 15, }}>
<span>
626
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 435, width: 20, height: 12, 'font-size': 15, }}>
<span>
624
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 501, width: 65, height: 11, 'font-size': 15, }}>
<span>
Provisions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 119, top: 521, width: 31, height: 11, 'font-size': 15, }}>
<span>
pour
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 535, width: 79, height: 15, 'font-size': 15, }}>
<span>
dépréciation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 469, width: 124, height: 12, 'font-size': 15, }}>
<span>
Sur immobilisations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 402, top: 469, width: 20, height: 13, 'font-size': 15, }}>
<span>
630
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 469, width: 19, height: 13, 'font-size': 15, }}>
<span>
632
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 469, width: 20, height: 13, 'font-size': 15, }}>
<span>
634
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 469, width: 20, height: 13, 'font-size': 15, }}>
<span>
636
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 502, width: 136, height: 12, 'font-size': 15, }}>
<span>
Sur stocks et en cours
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 402, top: 501, width: 20, height: 13, 'font-size': 15, }}>
<span>
640
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 501, width: 19, height: 13, 'font-size': 15, }}>
<span>
642
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 501, width: 20, height: 13, 'font-size': 15, }}>
<span>
644
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 501, width: 20, height: 13, 'font-size': 15, }}>
<span>
646
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 534, width: 230, height: 16, 'font-size': 15, }}>
<span>
Sur clients et comptes rattachés650
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 534, width: 19, height: 14, 'font-size': 15, }}>
<span>
652
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 534, width: 20, height: 14, 'font-size': 15, }}>
<span>
654
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 534, width: 20, height: 14, 'font-size': 15, }}>
<span>
656
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 192, top: 567, width: 230, height: 15, 'font-size': 15, }}>
<span>
Autres provisions pour dépréciation660
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 567, width: 19, height: 12, 'font-size': 15, }}>
<span>
662
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 567, width: 20, height: 12, 'font-size': 15, }}>
<span>
664
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 567, width: 20, height: 12, 'font-size': 15, }}>
<span>
666
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 336, top: 600, width: 86, height: 13, 'font-size': 15, }}>
<span>
TOTAL680
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 600, width: 19, height: 13, 'font-size': 15, }}>
<span>
682
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 600, width: 20, height: 13, 'font-size': 15, }}>
<span>
684
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 600, width: 20, height: 13, 'font-size': 15, }}>
<span>
686
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 967, width: 256, height: 21, 'font-size': 23, }}>
<span>
IIDÉFICITS REPORTABLES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 627, top: 967, width: 475, height: 21, 'font-size': 23, }}>
<span>
IIIDÉFICITS PROVENANT DE L’APPLICATION DU 209C
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1013, width: 413, height: 15, 'font-size': 15, }}>
<span>
Déficits restant à reporter au titre de l’exercice précédent (1)982
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 614, top: 1013, width: 327, height: 13, 'font-size': 15, }}>
<span>
Résultat déficitaire relevant de l’article 209C du CGI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1022, top: 1014, width: 20, height: 12, 'font-size': 15, }}>
<span>
995
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1045, width: 102, height: 15, 'font-size': 15, }}>
<span>
Déficits imputés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 480, top: 1046, width: 20, height: 12, 'font-size': 15, }}>
<span>
983
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 614, top: 1044, width: 428, height: 16, 'font-size': 15, }}>
<span>
Déficits étrangers des PME antérieurement déduits (article 209C du CGI)996
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1085, width: 122, height: 15, 'font-size': 15, }}>
<span>
Déficits reportables
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 480, top: 1085, width: 20, height: 13, 'font-size': 15, }}>
<span>
984
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1125, width: 131, height: 11, 'font-size': 15, }}>
<span>
Déficits de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 480, top: 1124, width: 20, height: 13, 'font-size': 15, }}>
<span>
860
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1165, width: 221, height: 15, 'font-size': 15, }}>
<span>
Total des déficits restant à reporter
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 480, top: 1166, width: 20, height: 11, 'font-size': 15, }}>
<span>
870
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 96, top: 1211, width: 115, height: 17, 'font-size': 23, }}>
<span>
VIDIVERS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1254, width: 316, height: 15, 'font-size': 15, }}>
<span>
Primes et cotisations complémentaires facultatives
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 932, top: 1255, width: 19, height: 12, 'font-size': 15, }}>
<span>
381
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1286, width: 336, height: 15, 'font-size': 15, }}>
<span>
Cotisations personnelles obligatoires de l’exploitant *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 932, top: 1287, width: 20, height: 12, 'font-size': 15, }}>
<span>
380
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1320, width: 190, height: 14, 'font-size': 15, }}>
<span>
N° du centre de gestion agréé
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 932, top: 1320, width: 20, height: 12, 'font-size': 15, }}>
<span>
388
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1353, width: 180, height: 11, 'font-size': 15, }}>
<span>
Montant de la TVA collectée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 932, top: 1353, width: 20, height: 13, 'font-size': 15, }}>
<span>
374
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1386, width: 467, height: 13, 'font-size': 15, }}>
<span>
Montant de la TVA déductible sur biens et services (sauf immobilisations)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 932, top: 1387, width: 20, height: 12, 'font-size': 15, }}>
<span>
378
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1419, width: 335, height: 15, 'font-size': 15, }}>
<span>
Montant des prélèvements personnels de l’exploitant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 932, top: 1420, width: 20, height: 12, 'font-size': 15, }}>
<span>
399
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 1452, width: 774, height: 15, 'font-size': 15, }}>
<span>
Aides perçues ayant donné droit à la réduction d’impôt prévue au 4 de l’article 238 bis du CGI pour l’entreprise donatrice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 932, top: 1453, width: 20, height: 12, 'font-size': 15, }}>
<span>
398
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 1486, width: 866, height: 15, 'font-size': 15, }}>
<span>
Montant de l’investissement qui a donné lieu à amortissement exceptionnel chez l’entreprise investisseur dans le cadre de l’article 217 octies du CGI397
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 634, width: 13, height: 15, 'font-size': 21, }}>
<span>
B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 635, width: 464, height: 14, 'font-size': 15, }}>
<span>
MOUVEMENTS AFFECTANT LA PROVISION POUR AMORTISSEMENTS DÉROGATOIRES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 288, top: 669, width: 59, height: 11, 'font-size': 15, }}>
<span>
Dotations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 477, top: 669, width: 51, height: 14, 'font-size': 15, }}>
<span>
Reprises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 701, width: 163, height: 14, 'font-size': 15, }}>
<span>
Immob. incorporelles700
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 701, width: 19, height: 12, 'font-size': 15, }}>
<span>
705
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 734, width: 50, height: 11, 'font-size': 15, }}>
<span>
Terrains
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 734, width: 20, height: 12, 'font-size': 15, }}>
<span>
710
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 734, width: 19, height: 12, 'font-size': 15, }}>
<span>
715
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 768, width: 83, height: 11, 'font-size': 15, }}>
<span>
Constructions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 768, width: 20, height: 11, 'font-size': 15, }}>
<span>
720
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 768, width: 19, height: 11, 'font-size': 15, }}>
<span>
725
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 794, width: 97, height: 15, 'font-size': 15, }}>
<span>
Inst. techniques
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 807, width: 99, height: 15, 'font-size': 15, }}>
<span>
mat. et outillage
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 801, width: 20, height: 12, 'font-size': 15, }}>
<span>
730
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 801, width: 19, height: 12, 'font-size': 15, }}>
<span>
735
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 827, width: 131, height: 15, 'font-size': 15, }}>
<span>
Inst. générales, agen-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 840, width: 118, height: 12, 'font-size': 15, }}>
<span>
cements amén. div.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 834, width: 20, height: 12, 'font-size': 15, }}>
<span>
740
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 834, width: 19, height: 12, 'font-size': 15, }}>
<span>
745
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 867, width: 163, height: 15, 'font-size': 15, }}>
<span>
Matériel de transport750
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 868, width: 19, height: 11, 'font-size': 15, }}>
<span>
755
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 893, width: 116, height: 11, 'font-size': 15, }}>
<span>
Autres immobilisa-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 906, width: 102, height: 14, 'font-size': 15, }}>
<span>
tions corporelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 898, width: 20, height: 13, 'font-size': 15, }}>
<span>
760
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 898, width: 19, height: 14, 'font-size': 15, }}>
<span>
765
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 165, top: 934, width: 84, height: 11, 'font-size': 15, }}>
<span>
TOTAL770
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 934, width: 19, height: 11, 'font-size': 15, }}>
<span>
775
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 645, top: 634, width: 13, height: 14, 'font-size': 21, }}>
<span>
C
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 665, top: 634, width: 438, height: 14, 'font-size': 15, }}>
<span>
VENTILATION DES DOTATIONS AUX PROVISIONS ET CHARGES À PAYER
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 665, top: 652, width: 316, height: 13, 'font-size': 15, }}>
<span>
NON DÉDUCTIBLES POUR L’ASSIETTE DE L’IMPÔT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 666, top: 671, width: 386, height: 16, 'font-size': 15, }}>
<span>
(Si le cadre C est insuffisant, joindre un état du même modèle)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 632, top: 702, width: 6, height: 11, 'font-size': 15, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 653, top: 695, width: 188, height: 15, 'font-size': 15, }}>
<span>
Indemnités pour congés à payer,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 654, top: 708, width: 248, height: 15, 'font-size': 15, }}>
<span>
charges sociales et fiscales correspondantes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 632, top: 736, width: 7, height: 10, 'font-size': 15, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 632, top: 768, width: 7, height: 12, 'font-size': 15, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 632, top: 799, width: 7, height: 12, 'font-size': 15, }}>
<span>
4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 632, top: 834, width: 7, height: 12, 'font-size': 15, }}>
<span>
5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 632, top: 866, width: 7, height: 13, 'font-size': 15, }}>
<span>
6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 632, top: 900, width: 7, height: 11, 'font-size': 15, }}>
<span>
7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 628, top: 933, width: 348, height: 15, 'font-size': 15, }}>
<span>
TOTAL à reporter ligne 322 du tableau n° 2033-B-SD780
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1513, width: 668, height: 14, 'font-size': 13, }}>
<span>
(1) Cette case correspond au montant porté sur la ligne 870 du tableau 2033D déposé au titre de l’exercice précédent.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 1527, width: 5, height: 6, 'font-size': 17, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1529, width: 474, height: 13, 'font-size': 13, }}>
<span>
Des explications concernant cette rubrique figurent dans la notice n° 2033-NOT-SD.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 43, width: 152, height: 17, 'font-size': 23, }}>
<span>
N° 2033-D-SD 2019
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="PF" data-field-id="27483448" data-annot-id="26633968" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="AA" data-field-id="27483896" data-annot-id="25435056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="BA" data-field-id="27484232" data-annot-id="26550928" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="CA" data-field-id="27484536" data-annot-id="26551120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="DA" data-field-id="27484872" data-annot-id="26877216" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="AN" data-field-id="27485208" data-annot-id="26877408" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="BN" data-field-id="27485544" data-annot-id="26877600" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="CN" data-field-id="27485880" data-annot-id="26877792" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="DN" data-field-id="27486216" data-annot-id="26877984" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="AB" data-field-id="27486696" data-annot-id="26878176" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="BB" data-field-id="27487032" data-annot-id="26878368" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="CB" data-field-id="27487368" data-annot-id="26878560" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="DB" data-field-id="27487704" data-annot-id="26878752" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="AC" data-field-id="27488040" data-annot-id="26878944" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="BC" data-field-id="27488376" data-annot-id="26879136" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="CC" data-field-id="27488712" data-annot-id="26879328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="DC" data-field-id="27489048" data-annot-id="26879520" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="AD" data-field-id="27489528" data-annot-id="26879984" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="BD" data-field-id="27489864" data-annot-id="26880176" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="CD" data-field-id="27490200" data-annot-id="26880368" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="DD" data-field-id="27490536" data-annot-id="26880560" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="AE" data-field-id="27490872" data-annot-id="26880752" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="BE" data-field-id="27491208" data-annot-id="26880944" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="CE" data-field-id="27491544" data-annot-id="26881136" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="DE" data-field-id="27491880" data-annot-id="26881328" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="AF" data-field-id="27492216" data-annot-id="26881520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="BF" data-field-id="27492552" data-annot-id="26881712" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="CF" data-field-id="27492888" data-annot-id="26881904" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="DF" data-field-id="27493224" data-annot-id="26882096" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="AG" data-field-id="27493560" data-annot-id="26882288" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="BG" data-field-id="27493896" data-annot-id="26882480" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="CG" data-field-id="27494232" data-annot-id="26882672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="DG" data-field-id="27494568" data-annot-id="26882864" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="AH" data-field-id="27489384" data-annot-id="26879712" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="BH" data-field-id="27495496" data-annot-id="26883584" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="CH" data-field-id="27495832" data-annot-id="26883776" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="DH" data-field-id="27496168" data-annot-id="26883968" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="EA" data-field-id="27482440" data-annot-id="26884160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="FA" data-field-id="27482824" data-annot-id="26884352" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="EB" data-field-id="27483160" data-annot-id="26884544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="FB" data-field-id="27496632" data-annot-id="26884736" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="EC" data-field-id="27496936" data-annot-id="26884928" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="FC" data-field-id="27497272" data-annot-id="26885120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="ED" data-field-id="27497608" data-annot-id="26885312" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="FD" data-field-id="27497944" data-annot-id="26885504" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="EE" data-field-id="27498280" data-annot-id="26885696" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="FE" data-field-id="27498616" data-annot-id="26885888" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="EF" data-field-id="27498952" data-annot-id="26886080" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="FF" data-field-id="27499288" data-annot-id="26886272" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="EG" data-field-id="27499624" data-annot-id="26886464" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="FG" data-field-id="27499960" data-annot-id="26886656" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="EH" data-field-id="27500296" data-annot-id="26886848" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="FH" data-field-id="27500632" data-annot-id="26887040" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="GJ1" data-field-id="27500968" data-annot-id="26887232" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="GJ2" data-field-id="27501304" data-annot-id="26887424" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="GJ3" data-field-id="27501640" data-annot-id="26887616" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="GJ4" data-field-id="27501976" data-annot-id="26887808" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="GJ5" data-field-id="27502312" data-annot-id="26888000" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="GJ6" data-field-id="27502648" data-annot-id="26888192" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="HA" data-field-id="27502984" data-annot-id="26888384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="HJ1" data-field-id="27503320" data-annot-id="26888576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="HJ2" data-field-id="27503656" data-annot-id="26888768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="HJ3" data-field-id="27503992" data-annot-id="26888960" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="HJ4" data-field-id="27504328" data-annot-id="26889152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="HJ5" data-field-id="27504664" data-annot-id="26889344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="HJ6" data-field-id="27494920" data-annot-id="26883056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="HH" data-field-id="27505912" data-annot-id="26883248" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="PG" data-field-id="27506216" data-annot-id="26890576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="PJ" data-field-id="27506552" data-annot-id="26890768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="PH" data-field-id="27506888" data-annot-id="26890960" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="MG" data-field-id="27507224" data-annot-id="26891152" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="PK" data-field-id="27507560" data-annot-id="26891344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="MH" data-field-id="27507896" data-annot-id="26891536" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="PL" data-field-id="27508232" data-annot-id="26891728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="AP" data-field-id="27508568" data-annot-id="26891920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="AQ" data-field-id="27508904" data-annot-id="26892112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="AS" data-field-id="27509240" data-annot-id="26892304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="AR" data-field-id="27509576" data-annot-id="26892496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="AT" data-field-id="27509912" data-annot-id="26892688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="AU" data-field-id="27510248" data-annot-id="26892880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="AJ" data-field-id="27510584" data-annot-id="26893072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="AK" data-field-id="27510920" data-annot-id="26893264" type="text" />

            <Field component="input" class="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="19926104" data-annot-id="19308096" type="text" disabled  />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2033D;
