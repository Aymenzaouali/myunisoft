import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';

import './style.scss';

const Tax2035F = (props) => {
  const { change } = props;
  return (
    <div className="form-tax-declaration-2035f">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 462, top: 56, width: 330, height: 16, 'font-size': 20, }}>
<span>
COMPOSITION DU CAPITAL SOCIAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 91, top: 217, width: 285, height: 18, 'font-size': 20, }}>
<span>
DÉNOMINATION DE L’ENTREPRISE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 90, top: 251, width: 131, height: 17, 'font-size': 20, }}>
<span>
ADRESSE (voie)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 91, top: 285, width: 119, height: 16, 'font-size': 20, }}>
<span>
CODE POSTAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 524, top: 286, width: 46, height: 15, 'font-size': 20, }}>
<span>
VILLE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 690, top: 189, width: 74, height: 14, 'font-size': 18, }}>
<span>
N° SIRET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 327, width: 521, height: 15, 'font-size': 15, }}>
<span>
NOMBRE TOTAL D’ASSOCIÉS OU ACTIONNAIRES PERSONNES MORALES DE L’ENTREPRISE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 356, width: 526, height: 14, 'font-size': 15, }}>
<span>
NOMBRE TOTAL D’ASSOCIÉS OU ACTIONNAIRES PERSONNES PHYSIQUES DE L’ENTREPRISE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 383, width: 352, height: 14, 'font-size': 16, }}>
<span>
I. CAPITAL DÉTENU PAR LES PERSONNES MORALES :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 412, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 443, width: 276, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 476, width: 67, height: 13, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 222, top: 476, width: 17, height: 13, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 509, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 412, width: 101, height: 13, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 471, top: 478, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 456, top: 509, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 991, width: 367, height: 14, 'font-size': 16, }}>
<span>
II. CAPITAL DÉTENU PAR LES PERSONNES PHYSIQUES :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1027, width: 52, height: 15, 'font-size': 16, }}>
<span>
Titre(2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 1028, width: 141, height: 15, 'font-size': 16, }}>
<span>
Nom patronymique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 183, top: 1063, width: 89, height: 12, 'font-size': 16, }}>
<span>
Nom marital
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1100, width: 77, height: 13, 'font-size': 16, }}>
<span>
Naissance:
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 221, top: 1100, width: 32, height: 13, 'font-size': 16, }}>
<span>
Date
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1132, width: 67, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 222, top: 1132, width: 17, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 1163, width: 85, height: 16, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 423, top: 1100, width: 115, height: 16, 'font-size': 16, }}>
<span>
N° Département
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 1132, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 456, top: 1163, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 720, top: 330, width: 366, height: 12, 'font-size': 15, }}>
<span>
NOMBRE TOTAL DE PARTS OU D‘ACTIONS CORRESPONDANTES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 720, top: 361, width: 366, height: 11, 'font-size': 15, }}>
<span>
NOMBRE TOTAL DE PARTS OU D‘ACTIONS CORRESPONDANTES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 626, top: 446, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 581, top: 1063, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 642, top: 1100, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 778, top: 1027, width: 73, height: 15, 'font-size': 16, }}>
<span>
Prénom(s)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 908, top: 446, width: 162, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 510, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 908, top: 1063, width: 162, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 1164, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 966, top: 1100, width: 32, height: 16, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 90, top: 1550, width: 1072, height: 14, 'font-size': 14, }}>
<span>
(1) Lorsque le nombre d’associés excède le nombre de lignes de l’imprimé, utiliser un ou plusieurs tableaux supplémentaires. Dans ce cas, il convient de numéroter chaque
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 1566, width: 898, height: 14, 'font-size': 14, }}>
<span>
tableau en haut et à gauche de la case prévue à cet effet et de porter le nombre total de tableaux souscrits en bas à droite de cette même case.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 90, top: 1581, width: 508, height: 14, 'font-size': 14, }}>
<span>
(2) Indiquer : M pour Monsieur, Mme pour Madame ou Mle pour Mademoiselle.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 98, width: 88, height: 11, 'font-size': 14, }}>
<span>
N° 15945 Ý 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 149, width: 114, height: 18, 'font-size': 20, }}>
<span>
N ° DE DÉPÔT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 186, top: 64, width: 120, height: 11, 'font-size': 12, }}>
<span>
Formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 185, top: 76, width: 141, height: 11, 'font-size': 12, }}>
<span>
(article 40 A de l’annexe III
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 185, top: 89, width: 155, height: 12, 'font-size': 12, }}>
<span>
au Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 395, top: 92, width: 467, height: 16, 'font-size': 16, }}>
<span>
(liste des personnes ou groupements de personnes de droit
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 349, top: 110, width: 558, height: 15, 'font-size': 16, }}>
<span>
ou de fait détenant directement au moins 10 % du capital de la société)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 452, top: 151, width: 162, height: 15, 'font-size': 20, }}>
<span>
E XERCICE CLOS LE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1027, top: 59, width: 132, height: 13, 'font-size': 16, }}>
<span>
N° 2035-F-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1044, top: 97, width: 21, height: 18, 'font-size': 20, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 831, top: 139, width: 246, height: 14, 'font-size': 14, }}>
<span>
Si ce formulaire est déposé sans informations,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 831, top: 155, width: 165, height: 10, 'font-size': 14, }}>
<span>
cocher la case néant ci-contre :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 998, top: 164, width: 102, height: 1, 'font-size': 8, }}>
<span>
. . . . . . . . . . . . . . . . . . . . . . . . . . .
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 558, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 589, width: 276, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 622, width: 67, height: 13, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 222, top: 622, width: 17, height: 13, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 655, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 558, width: 101, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 471, top: 624, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 456, top: 655, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 626, top: 592, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 908, top: 592, width: 162, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 656, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 705, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 736, width: 276, height: 14, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 769, width: 67, height: 13, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 222, top: 769, width: 17, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 802, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 705, width: 101, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 471, top: 771, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 456, top: 802, width: 75, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 626, top: 738, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 908, top: 739, width: 162, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 802, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 854, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 885, width: 276, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 918, width: 67, height: 13, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 222, top: 918, width: 17, height: 13, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 951, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 854, width: 101, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 471, top: 920, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 456, top: 951, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 626, top: 888, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 908, top: 888, width: 162, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 952, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1210, width: 52, height: 15, 'font-size': 16, }}>
<span>
Titre(2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 1211, width: 141, height: 15, 'font-size': 16, }}>
<span>
Nom patronymique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 183, top: 1246, width: 89, height: 12, 'font-size': 16, }}>
<span>
Nom marital
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1278, width: 77, height: 12, 'font-size': 16, }}>
<span>
Naissance:
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 221, top: 1278, width: 32, height: 12, 'font-size': 16, }}>
<span>
Date
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1309, width: 67, height: 13, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 222, top: 1309, width: 17, height: 13, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 1341, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 423, top: 1278, width: 115, height: 15, 'font-size': 16, }}>
<span>
N° Département
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 1309, width: 31, height: 13, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 456, top: 1341, width: 75, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 581, top: 1246, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 642, top: 1278, width: 75, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 778, top: 1210, width: 73, height: 15, 'font-size': 16, }}>
<span>
Prénom(s)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 908, top: 1246, width: 162, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 1342, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 966, top: 1278, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1388, width: 52, height: 14, 'font-size': 16, }}>
<span>
Titre(2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 1388, width: 141, height: 15, 'font-size': 16, }}>
<span>
Nom patronymique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 183, top: 1423, width: 89, height: 13, 'font-size': 16, }}>
<span>
Nom marital
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 221, top: 1455, width: 32, height: 13, 'font-size': 16, }}>
<span>
Date
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1455, width: 77, height: 13, 'font-size': 16, }}>
<span>
Naissance:
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1487, width: 67, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 222, top: 1487, width: 17, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 1518, width: 85, height: 16, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 423, top: 1455, width: 115, height: 15, 'font-size': 16, }}>
<span>
N° Département
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 1487, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 456, top: 1518, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 581, top: 1423, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 642, top: 1455, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 778, top: 1388, width: 73, height: 14, 'font-size': 16, }}>
<span>
Prénom(s)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 908, top: 1423, width: 162, height: 16, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 1519, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 966, top: 1455, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 1476, width: 9, height: 66, 'font-size': 11, }}>
<span>
N° 2035-F-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 1300, width: 11, height: 170, 'font-size': 12, }}>
<span>
– (SDNC-DGFiP) – Octobre 2017
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="GAK1" data-field-id="35517864" data-annot-id="34175984" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="GAD1" data-field-id="35518136" data-annot-id="33709136" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="GAE1" data-field-id="35518504" data-annot-id="33709328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="GAH1" data-field-id="35518808" data-annot-id="34506816" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="GAT1" data-field-id="35519144" data-annot-id="34507008" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="GAF1" data-field-id="35519480" data-annot-id="34507200" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="GAA1" data-field-id="35519816" data-annot-id="34507392" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="GR1" data-field-id="35520152" data-annot-id="34507584" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="GAI1" data-field-id="35520488" data-annot-id="34907872" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="GAG1" data-field-id="35520968" data-annot-id="34908208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="GD1" data-field-id="35521304" data-annot-id="34908400" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="GAJ1" data-field-id="35521640" data-annot-id="34908592" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="GAK2" data-field-id="35521976" data-annot-id="34908784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="GAD2" data-field-id="35522312" data-annot-id="34908976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="GAE2" data-field-id="35522648" data-annot-id="34909168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="GAH2" data-field-id="35522984" data-annot-id="34909360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="GAT2" data-field-id="35523320" data-annot-id="34909552" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="GAF2" data-field-id="35523800" data-annot-id="34910016" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="GAA2" data-field-id="35524136" data-annot-id="34910208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="GR2" data-field-id="35524472" data-annot-id="34910400" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="GAI2" data-field-id="35516616" data-annot-id="34910592" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="GAG2" data-field-id="35517640" data-annot-id="34910784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="GD2" data-field-id="35517048" data-annot-id="34910976" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="GAJ2" data-field-id="35524728" data-annot-id="34911168" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="GAK3" data-field-id="35525064" data-annot-id="34911360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="GAD3" data-field-id="35525400" data-annot-id="34911552" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="GAE3" data-field-id="35525736" data-annot-id="34911744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GAH3" data-field-id="35526072" data-annot-id="34911936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="GAT3" data-field-id="35526408" data-annot-id="34912128" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="GAF3" data-field-id="35526744" data-annot-id="34912320" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="GAA3" data-field-id="35527080" data-annot-id="34912512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="GR3" data-field-id="35527416" data-annot-id="34912704" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="GAI3" data-field-id="35527752" data-annot-id="34912896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="GAG3" data-field-id="35523656" data-annot-id="34909744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="GD3" data-field-id="35528680" data-annot-id="34913616" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="GAJ3" data-field-id="35529016" data-annot-id="34913808" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GAK4" data-field-id="35529352" data-annot-id="34914000" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="GAD4" data-field-id="35529688" data-annot-id="34914192" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="GAE4" data-field-id="35530024" data-annot-id="34914384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="GAH4" data-field-id="35530360" data-annot-id="34914576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="GAT4" data-field-id="35530696" data-annot-id="34914768" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="GAF4" data-field-id="35531032" data-annot-id="34914960" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GAA4" data-field-id="35531368" data-annot-id="34915152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="GR4" data-field-id="35531704" data-annot-id="34915344" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="GAI4" data-field-id="35532040" data-annot-id="34915536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GAG4" data-field-id="35532376" data-annot-id="34915728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GD4" data-field-id="35532712" data-annot-id="34915920" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="GAJ4" data-field-id="35533048" data-annot-id="34916112" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="BAK1" data-field-id="35533384" data-annot-id="34916304" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="BAA1" data-field-id="35533720" data-annot-id="34916496" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="BAC1" data-field-id="35534056" data-annot-id="34916688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="BE1" data-field-id="35534392" data-annot-id="34916880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="BAD1" data-field-id="35534728" data-annot-id="34917072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="BAE1" data-field-id="35535064" data-annot-id="34917264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="BAH1" data-field-id="35535400" data-annot-id="34917456" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="BAF1" data-field-id="35535736" data-annot-id="34917648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="BGH1" data-field-id="35536072" data-annot-id="34917840" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="BAI1" data-field-id="35536408" data-annot-id="34918032" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="BAG1" data-field-id="35536744" data-annot-id="34918224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="BGI1" data-field-id="35537080" data-annot-id="34918416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="BR1" data-field-id="35537416" data-annot-id="34918608" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="BAB1" data-field-id="35537752" data-annot-id="34918800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="BD1" data-field-id="35538088" data-annot-id="34918992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="BGJ1" data-field-id="35538424" data-annot-id="34919184" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="BAJ1" data-field-id="35538760" data-annot-id="34919376" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="BAK2" data-field-id="35528104" data-annot-id="34913088" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="BAA2" data-field-id="35540008" data-annot-id="34913280" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="BAB2" data-field-id="35540312" data-annot-id="34920608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="BD2" data-field-id="35540648" data-annot-id="34920800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="BGJ2" data-field-id="35540984" data-annot-id="34920992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="BR2" data-field-id="35541320" data-annot-id="34921184" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="BGI2" data-field-id="35541656" data-annot-id="34921376" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="BAC2" data-field-id="35541992" data-annot-id="34921568" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="BE2" data-field-id="35542328" data-annot-id="34921760" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="BAD2" data-field-id="35542664" data-annot-id="34921952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="BAE2" data-field-id="35543000" data-annot-id="34922144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="BAH2" data-field-id="35543336" data-annot-id="34922336" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="BAF2" data-field-id="35543672" data-annot-id="34922528" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="BAI2" data-field-id="35544008" data-annot-id="34922720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="BAG2" data-field-id="35544344" data-annot-id="34922912" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="BAJ2" data-field-id="35544680" data-annot-id="34923104" type="text" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="GS" data-field-id="35545016" data-annot-id="34923296" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="GT" data-field-id="35545352" data-annot-id="34923488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="GU" data-field-id="35545688" data-annot-id="34923680" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="GV" data-field-id="35546024" data-annot-id="34923872" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="GW" data-field-id="35546360" data-annot-id="34924064" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="BGH2" data-field-id="35546696" data-annot-id="34924256" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="BAK3" data-field-id="35547032" data-annot-id="34924448" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="BAA3" data-field-id="35547368" data-annot-id="34924640" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="BAB3" data-field-id="35547704" data-annot-id="34924832" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field " name="BD3" data-field-id="35548040" data-annot-id="34925024" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="BR3" data-field-id="35548376" data-annot-id="34925216" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="BAC3" data-field-id="35548712" data-annot-id="34925408" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="BGJ3" data-field-id="35549048" data-annot-id="34925600" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="BGI3" data-field-id="35549384" data-annot-id="34925792" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="BE3" data-field-id="35549720" data-annot-id="34925984" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="BGH3" data-field-id="35550056" data-annot-id="34926176" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="BAJ3" data-field-id="35550392" data-annot-id="34926368" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field " name="BAD3" data-field-id="35550728" data-annot-id="34926560" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="BAE3" data-field-id="35551064" data-annot-id="34926752" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field " name="BAH3" data-field-id="35551400" data-annot-id="34926944" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field " name="BAF3" data-field-id="35551736" data-annot-id="34927136" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field " name="BAI3" data-field-id="35552072" data-annot-id="34927328" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field " name="BAG3" data-field-id="35552408" data-annot-id="34927520" type="text" />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field " name="FIN_EX" data-field-id="14719960" data-annot-id="14092704" type="text"  />

            <Field component="input" className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field pde-form-field-text" name="SIRET_STET" data-field-id="14720296" data-annot-id="14092896" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="14720632" data-annot-id="14093088" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STED" data-field-id="14720968" data-annot-id="14093280" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEF" data-field-id="14721304" data-annot-id="14093472" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEG" data-field-id="14721640" data-annot-id="14093664" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field pde-form-field-text" name="CP_STEH" data-field-id="14721976" data-annot-id="14093856" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field pde-form-field-text" name="VILLE_STEI" data-field-id="14722312" data-annot-id="14094048" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2035F;
