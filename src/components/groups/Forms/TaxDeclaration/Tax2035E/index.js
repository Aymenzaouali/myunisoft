import React from 'react';
import { Field } from 'redux-form';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import { useCompute, sum, sub, setValue } from 'helpers/pdfforms';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { get as _get } from 'lodash';

import './style.scss';

// Component
const Tax2035E = (props) => {
  const { formValues, form235A, change } = props;
  const BM = _get(form235A, 'BM', null);

  // Compute
  useCompute('EI', sum, ['EF', 'EG', 'EH', 'EY'], formValues, change);
  useCompute('ER', sum, ['EA', 'EK', 'EL', 'ET', 'EN', 'EO', 'EP', 'EU', 'EW', 'EX'], formValues, change);
  useCompute('ES', sub, ['EI', 'ER'], formValues, change);

  setValue(formValues, 'EO', BM, change);

  // Rendering
  return (
    <div className="tax2035E">
      <div id="pdf-document"/>
      <form id="acroform"/>
      <div className="pdf-page" id="pdf-page-1">
        <div className="pdf-page-inner pdf-page-1">
          <div className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 220, top: 75, width: 122, height: 11, fontSize: 12 }}>
                <span>Formulaire obligatoire</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 220, top: 87, width: 140, height: 12, fontSize: 12 }}>
                <span>(article 40A de l’annexe III</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 220, top: 100, width: 157, height: 12, fontSize: 12 }}>
                <span>au Code général des impôts)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 134, top: 118, width: 78, height: 11, fontSize: 12 }}>
                <span>N° 15945 Ý 01</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 450, top: 71, width: 399, height: 20, fontSize: 22 }}>
                <span>ANNEXE À LA DÉCLARATION N° 2035</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 301, top: 132, width: 694, height: 17, fontSize: 18 }}>
                <span>DÉTERMINATION DE LA VALEUR AJOUTÉE PRODUITE AU COURS DE L’EXERCICE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1024, top: 75, width: 144, height: 15, fontSize: 19 }}>
                <span>N° 2035-E-SD 2019</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1007, top: 97, width: 125, height: 9, fontSize: 9 }}>
                <span>Si ce formulaire est déposé sans</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1007, top: 107, width: 122, height: 9, fontSize: 9 }}>
                <span>informations chiffrées, cocher la</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1007, top: 117, width: 84, height: 7, fontSize: 9 }}>
                <span>case Néant ci-contre :</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1007, top: 127, width: 130, height: 9, fontSize: 9 }}>
                <span>Ne porter qu’une somme par ligne</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1007, top: 137, width: 109, height: 9, fontSize: 9 }}>
                <span>(ne pas porter les centimes)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 729, top: 204, width: 66, height: 12, fontSize: 16 }}>
                <span>N° SIRET</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 248, width: 361, height: 17, fontSize: 18 }}>
                <span>Nom et prénom du déclarant ou dénomination:</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 144, top: 285, width: 192, height: 17, fontSize: 18 }}>
                <span>Adresse professionnelle:</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 318, width: 102, height: 17, fontSize: 18 }}>
                <span>Code postal :</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 495, top: 318, width: 33, height: 14, fontSize: 18 }}>
                <span>Ville</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 354, width: 301, height: 16, fontSize: 16 }}>
                <span>RENSEIGNEMENTS RELATIFS À L’ANNÉE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 357, width: 17, height: 13, fontSize: 16 }}>
                <span>20</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 573, top: 354, width: 166, height: 16, fontSize: 16 }}>
                <span>OU À LA PERIODE DU :</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 947, top: 357, width: 29, height: 13, fontSize: 16 }}>
                <span>AU :</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 386, width: 102, height: 12, fontSize: 16 }}>
                <span>A. RECETTES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 414, width: 573, height: 15, fontSize: 16 }}>
                <span>Montant net des honoraires ou recettes provenant de l’exercice d’une profession</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 432, width: 121, height: 13, fontSize: 16 }}>
                <span>non commerciale</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 797, top: 427, width: 18, height: 12, fontSize: 16 }}>
                <span>EF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 469, width: 471, height: 16, fontSize: 16 }}>
                <span>Gains divers (à l’exclusion des remboursements de crédit de TVA)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 795, top: 469, width: 21, height: 13, fontSize: 16 }}>
                <span>EG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 510, width: 516, height: 15, fontSize: 16 }}>
                <span>TVA déductible afférente aux dépenses mentionnées aux lignes EJ à EP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 669, top: 504, width: 19, height: 20, fontSize: 22 }}>
                <span>B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 507, width: 19, height: 12, fontSize: 16 }}>
                <span>EH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 537, width: 572, height: 15, fontSize: 16 }}>
                <span>Plus-values de cession d’éléments d’immobilisations corporelles et incorporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 552, width: 428, height: 16, fontSize: 16 }}>
                <span>lorsqu’elles se rapportent à une activité normale et courante</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 545, width: 19, height: 12, fontSize: 16 }}>
                <span>EN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 712, top: 583, width: 100, height: 12, fontSize: 16 }}>
                <span>TOTAL 1 EI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 618, width: 102, height: 15, fontSize: 16 }}>
                <span>B. DÉPENSES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 659, width: 49, height: 13, fontSize: 16 }}>
                <span>Achats</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 797, top: 659, width: 17, height: 13, fontSize: 16 }}>
                <span>EJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 700, width: 128, height: 12, fontSize: 16 }}>
                <span>Variation de stock</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 280, top: 694, width: 19, height: 20, fontSize: 22 }}>
                <span>C</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 698, width: 20, height: 12, fontSize: 16 }}>
                <span>EK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 738, width: 415, height: 16, fontSize: 16 }}>
                <span>Services extérieurs à l’exception des loyers et redevances</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 733, width: 19, height: 19, fontSize: 22 }}>
                <span>D</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 797, top: 736, width: 18, height: 12, fontSize: 16 }}>
                <span>EL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 765, width: 610, height: 15, fontSize: 16 }}>
                <span>Loyers et redevances, à l’exception de ceux afférents à des immobilisations corporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 780, width: 616, height: 16, fontSize: 16 }}>
                <span>mises à disposition dans le cadre d’une convention de location-gérance ou de crédit-bail</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 794, width: 403, height: 15, fontSize: 16 }}>
                <span>ou encore d’une convention de location de plus de 6 mois</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 792, width: 19, height: 20, fontSize: 22 }}>
                <span>D</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 795, top: 785, width: 21, height: 13, fontSize: 16 }}>
                <span>EM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 833, width: 263, height: 16, fontSize: 16 }}>
                <span>Frais de transport et de déplacement</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 416, top: 828, width: 20, height: 20, fontSize: 22 }}>
                <span>D</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 795, top: 831, width: 21, height: 12, fontSize: 16 }}>
                <span>EO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 869, width: 161, height: 15, fontSize: 16 }}>
                <span>Frais divers de gestion</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 869, width: 20, height: 12, fontSize: 16 }}>
                <span>EP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 910, width: 371, height: 15, fontSize: 16 }}>
                <span>TVA incluse dans les recettes mentionnées ligne EF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 524, top: 904, width: 19, height: 20, fontSize: 22 }}>
                <span>B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 795, top: 907, width: 21, height: 13, fontSize: 16 }}>
                <span>EQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 939, width: 557, height: 14, fontSize: 16 }}>
                <span>Taxes sur le chiffre d’affaires et assimilées, contributions indirectes, taxe intérieure</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 956, width: 322, height: 16, fontSize: 16 }}>
                <span>de consommation sur les produits énergétiques</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 947, width: 20, height: 13, fontSize: 16 }}>
                <span>ER</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 977, width: 613, height: 15, fontSize: 16 }}>
                <span>Dotations aux amortissements afférentes à des immobilisations corporelles mises à dis-</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 994, width: 612, height: 15, fontSize: 16 }}>
                <span>position dans le cadre d’une convention de location-gérance ou de crédit-bail ou encore</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 1011, width: 586, height: 15, fontSize: 16 }}>
                <span>d’une convention de location de plus de 6 mois en proportion de la seule période de</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1029, width: 318, height: 15, fontSize: 16 }}>
                <span>location-gérance, de crédit-bail ou de location</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 1003, width: 19, height: 12, fontSize: 16 }}>
                <span>EU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1049, width: 584, height: 15, fontSize: 16 }}>
                <span>Moins-values de cession d’éléments d’immobilisations corporelles et incorporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1067, width: 428, height: 16, fontSize: 16 }}>
                <span>lorsqu’elles se rapportent à une activité normale et courante</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 1058, width: 20, height: 12, fontSize: 16 }}>
                <span>EV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 712, top: 1096, width: 106, height: 13, fontSize: 16 }}>
                <span>TOTAL 2 EW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 1132, width: 157, height: 15, fontSize: 16 }}>
                <span>C. VALEUR AJOUTÉE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 1173, width: 190, height: 15, fontSize: 16 }}>
                <span>Calcul de la valeur ajoutée</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 635, top: 1173, width: 181, height: 12, fontSize: 16 }}>
                <span>TOTAL 1 – TOTAL 2 EX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 1205, width: 486, height: 16, fontSize: 16 }}>
                <span>D. COTISATION SUR LA VALEUR AJOUTÉE DES ENTREPRISES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1238, width: 584, height: 16, fontSize: 16 }}>
                <span>Valeur ajoutée assujettie à la CVAE (reporter sur la déclaration n° 1330-CVAE pour les</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 1255, width: 284, height: 13, fontSize: 16 }}>
                <span>multi-établissements et sur les relevés n</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 433, top: 1257, width: 9, height: 5, fontSize: 9 }}>
                <span>os</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 446, top: 1255, width: 164, height: 16, fontSize: 16 }}>
                <span>1329-DEF et 1329-AC)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 1247, width: 18, height: 12, fontSize: 16 }}>
                <span>JU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 436, top: 1288, width: 431, height: 13, fontSize: 16 }}>
                <span>Cadre réservé au mono-établissement au sens de la CVAE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 1322, width: 960, height: 15, fontSize: 16 }}>
                <span>Si vous êtes assujetti à la CVAE et êtes un mono établissement au sens de la CVAE (cf. notice de la déclaration n° 1330-CVAE), complèter</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 1340, width: 616, height: 15, fontSize: 16 }}>
                <span>le cadre ci-dessous. Vous serez alors dispensé du dépôt de la déclaration n° 1330-CVAE.</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 1477, width: 146, height: 12, fontSize: 16 }}>
                <span>Période de référence</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 445, top: 1477, width: 20, height: 12, fontSize: 16 }}>
                <span>KA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 549, top: 1475, width: 7, height: 17, fontSize: 23 }}>
                <span>/</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 648, top: 1475, width: 8, height: 17, fontSize: 23 }}>
                <span>/</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 809, top: 1477, width: 19, height: 12, fontSize: 16 }}>
                <span>LA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 913, top: 1475, width: 7, height: 17, fontSize: 23 }}>
                <span>/</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 1475, width: 7, height: 17, fontSize: 23 }}>
                <span>/</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 1509, width: 124, height: 13, fontSize: 16 }}>
                <span>Date de cessation</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 1509, width: 23, height: 12, fontSize: 16 }}>
                <span>MA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 913, top: 1508, width: 7, height: 17, fontSize: 23 }}>
                <span>/</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 1508, width: 7, height: 17, fontSize: 23 }}>
                <span>/</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 865, width: 12, height: 255, fontSize: 12 }}>
                <span>– (SDNC-DGFiP) – Novembre 2018N°</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 1054, width: 9, height: 52, fontSize: 11 }}>
                <span>2035-E-SD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1369, width: 325, height: 15, fontSize: 16 }}>
                <span>MONO ÉTABLISSEMENT au sens de la CVAE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 531, top: 1371, width: 21, height: 13, fontSize: 16 }}>
                <span>AH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1404, width: 256, height: 12, fontSize: 16 }}>
                <span>Chiffre d’affaires de référence CVAE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 814, top: 1404, width: 17, height: 12, fontSize: 16 }}>
                <span>AJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1436, width: 202, height: 13, fontSize: 16 }}>
                <span>Effectifs au sens de la CVAE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 812, top: 1437, width: 22, height: 12, fontSize: 16 }}>
                <span>AK</span>
            </div>
          </div>
          <div className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} class="pde-form-field pdf-annot pdf-obj-0-1 pdf-obj-fixed acroform-field" name="DT" type="checkbox" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-2 pdf-obj-fixed acroform-field" name="EF" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-3 pdf-obj-fixed acroform-field" name="EG" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-4 pdf-obj-fixed acroform-field" name="EH" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-5 pdf-obj-fixed acroform-field" name="EY" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-6 pdf-obj-fixed acroform-field" name="EI" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-7 pdf-obj-fixed acroform-field" name="EA" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-8 pdf-obj-fixed acroform-field" name="EK" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-9 pdf-obj-fixed acroform-field" name="EL" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-10 pdf-obj-fixed acroform-field" name="ET" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-11 pdf-obj-fixed acroform-field" name="EN" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-12 pdf-obj-fixed acroform-field" name="EO" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-13 pdf-obj-fixed acroform-field" name="EP" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-14 pdf-obj-fixed acroform-field" name="EU" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-15 pdf-obj-fixed acroform-field" name="EW" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-16 pdf-obj-fixed acroform-field" name="EX" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-18 pdf-obj-fixed acroform-field" name="ER" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-17 pdf-obj-fixed acroform-field" name="ES" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-19 pdf-obj-fixed acroform-field" name="JU" />
            <Field component={PdfCheckbox} onCustomChange={change} class="pde-form-field pdf-annot pdf-obj-0-20 pdf-obj-fixed acroform-field" name="KA" type="checkbox" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-21 pdf-obj-fixed acroform-field" name="KD" />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-22 pdf-obj-fixed acroform-field" name="AH" type="text" />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-23 pdf-obj-fixed acroform-field" name="KB" type="date" />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-25 pdf-obj-fixed acroform-field" name="KC" type="date" />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-24 pdf-obj-fixed acroform-field" name="KG" type="date" />

            <Field component="input" className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field pde-form-field-text pde-form-field-text" name="CP_STEH" data-field-id="40338088" data-annot-id="39729392" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field pde-form-field-text pde-form-field-text" name="VILLE_STEI" data-field-id="40338424" data-annot-id="39729584" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field pde-form-field-text pde-form-field-text" name="ADR_STED" data-field-id="40338760" data-annot-id="39729776" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field pde-form-field-text pde-form-field-text" name="ADR_STEF" data-field-id="40339096" data-annot-id="39729968" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field pde-form-field-text pde-form-field-text" name="ADR_STEG" data-field-id="40339432" data-annot-id="39730160" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field pde-form-field-text pde-form-field-text" name="NOM_STEA" data-field-id="40339768" data-annot-id="39730352" type="text" disabled />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="SIRET_STET" data-field-id="40340104" data-annot-id="39730544" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field pde-form-field-text pde-form-field-text" name="DEB_EX" data-field-id="40340440" data-annot-id="39730736" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field pde-form-field-text pde-form-field-text" name="FIN_EX" data-field-id="40335256" data-annot-id="39727584" type="text" disabled />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2035E;
