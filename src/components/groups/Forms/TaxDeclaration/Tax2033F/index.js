import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { setIsNothingness } from "helpers/pdfforms";
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';

import './style.scss';

const Tax2033F = (props) => {
  const { taxDeclarationForm, change } = props;
  const notNeededProps = { 
    'FIN_EX': true,
    'SIREN_STET': true,
    'NOM_STEA': true,
    'ADR_STED': true,
    'ADR_STEF': true,
    'ADR_STEG': true,
    'CP_STEH': true,
    'VILLE_STEI': true,
    'GS': true
  };
   
  setIsNothingness(taxDeclarationForm, notNeededProps, 'GS', change);
  return (
    <div className="form-tax-declaration-2033f">
      <div id="pdf-document" data-type="pdf-document" data-num-pages="1" data-layout="fixed">

      </div>

      <form id="acroform"></form>

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.293333" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 294, top: 63, width: 32, height: 32, 'font-size': 41, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 70, width: 338, height: 16, 'font-size': 22, }}>
<span>
COMPOSITION DU CAPITAL SOCIAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 896, top: 68, width: 51, height: 12, 'font-size': 18, }}>
<span>
DGFiP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 951, top: 65, width: 143, height: 17, 'font-size': 22, }}>
<span>
N° 2033-F-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1212, width: 11, height: 214, 'font-size': 11, }}>
<span>
N° 2033-F-SD – (SDNC-DGFiP) - Janvier 201
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 86, width: 102, height: 11, 'font-size': 11, }}>
<span>
Formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 97, width: 150, height: 9, 'font-size': 10, }}>
<span>
(article 38 de l’annexe III au C.G.I.)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 367, top: 117, width: 471, height: 15, 'font-size': 15, }}>
<span>
( liste des personnes ou groupements de personnes de droit ou de fait
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 396, top: 135, width: 412, height: 14, 'font-size': 15, }}>
<span>
détenant directement au moins 10 % du capital de la société )
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 134, top: 124, width: 85, height: 16, 'font-size': 16, }}>
<span>
N° de dépôt
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 969, top: 117, width: 20, height: 13, 'font-size': 15, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1009, top: 133, width: 36, height: 10, 'font-size': 15, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1088, top: 134, width: 7, height: 7, 'font-size': 22, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 203, width: 106, height: 13, 'font-size': 16, }}>
<span>
Exercice clos le
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 843, top: 203, width: 42, height: 11, 'font-size': 16, }}>
<span>
SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 238, width: 200, height: 16, 'font-size': 16, }}>
<span>
Dénomination de l’entreprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 273, width: 99, height: 14, 'font-size': 16, }}>
<span>
Adresse (voie)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 309, width: 81, height: 16, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 309, width: 31, height: 12, 'font-size': 16, }}>
<span>
Ville
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 349, width: 264, height: 13, 'font-size': 14, }}>
<span>
NOMBRE TOTAL D’ASSOCIÉS OU ACTIONNAIRES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 364, width: 228, height: 11, 'font-size': 14, }}>
<span>
PERSONNES MORALES DE L’ENTREPRISE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 380, top: 358, width: 16, height: 10, 'font-size': 14, }}>
<span>
901
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 601, top: 351, width: 228, height: 11, 'font-size': 14, }}>
<span>
NOMBRE TOTAL DE PARTS OU D’ACTIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 601, top: 364, width: 116, height: 11, 'font-size': 14, }}>
<span>
CORRESPONDANTES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 358, width: 17, height: 10, 'font-size': 14, }}>
<span>
902
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 384, width: 264, height: 13, 'font-size': 14, }}>
<span>
NOMBRE TOTAL D’ASSOCIÉS OU ACTIONNAIRES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 399, width: 237, height: 12, 'font-size': 14, }}>
<span>
PERSONNES PHYSIQUES DE L’ENTREPRISE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 380, top: 393, width: 17, height: 10, 'font-size': 14, }}>
<span>
903
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 601, top: 386, width: 228, height: 11, 'font-size': 14, }}>
<span>
NOMBRE TOTAL DE PARTS OU D’ACTIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 601, top: 399, width: 116, height: 11, 'font-size': 14, }}>
<span>
CORRESPONDANTES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 393, width: 17, height: 10, 'font-size': 14, }}>
<span>
904
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 441, width: 355, height: 13, 'font-size': 15, }}>
<span>
I - CAPITAL DÉTENU PAR LES PERSONNES MORALES :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 487, width: 110, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 360, top: 488, width: 98, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 523, width: 65, height: 12, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 524, width: 165, height: 12, 'font-size': 14, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 522, width: 105, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 837, top: 522, width: 139, height: 16, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 556, width: 105, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 557, width: 32, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 193, top: 589, width: 71, height: 13, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 419, top: 590, width: 71, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 899, top: 590, width: 30, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 634, width: 110, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 360, top: 635, width: 98, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 670, width: 65, height: 12, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 671, width: 165, height: 12, 'font-size': 14, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 669, width: 105, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 837, top: 669, width: 139, height: 16, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 703, width: 105, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 704, width: 32, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 193, top: 736, width: 71, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 419, top: 737, width: 71, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 899, top: 737, width: 30, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 780, width: 110, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 360, top: 781, width: 98, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 816, width: 65, height: 12, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 817, width: 165, height: 12, 'font-size': 14, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 815, width: 105, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 837, top: 815, width: 139, height: 16, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 849, width: 105, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 850, width: 32, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 193, top: 882, width: 71, height: 13, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 419, top: 883, width: 71, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 899, top: 883, width: 30, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 926, width: 110, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 360, top: 927, width: 98, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 962, width: 65, height: 12, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 176, top: 963, width: 165, height: 12, 'font-size': 14, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 961, width: 105, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 837, top: 961, width: 139, height: 16, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 995, width: 105, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 996, width: 32, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 193, top: 1028, width: 71, height: 13, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 419, top: 1029, width: 71, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 899, top: 1029, width: 30, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1056, width: 374, height: 16, 'font-size': 15, }}>
<span>
II - CAPITAL DÉTENU PAR LES PERSONNES PHYSIQUES :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 1102, width: 57, height: 14, 'font-size': 16, }}>
<span>
Titre (2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 284, top: 1103, width: 123, height: 15, 'font-size': 16, }}>
<span>
Nom patronymique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 759, top: 1102, width: 65, height: 14, 'font-size': 16, }}>
<span>
Prénom(s)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1136, width: 86, height: 12, 'font-size': 16, }}>
<span>
Nom marital
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 648, top: 1136, width: 105, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 843, top: 1136, width: 140, height: 16, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1170, width: 76, height: 12, 'font-size': 16, }}>
<span>
Naissance :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 221, top: 1170, width: 32, height: 12, 'font-size': 16, }}>
<span>
Date
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 391, top: 1170, width: 104, height: 15, 'font-size': 16, }}>
<span>
N° Département
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 581, top: 1170, width: 71, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 899, top: 1170, width: 31, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 1202, width: 105, height: 13, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1203, width: 32, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 193, top: 1236, width: 71, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 419, top: 1237, width: 71, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 899, top: 1237, width: 30, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 1279, width: 57, height: 14, 'font-size': 16, }}>
<span>
Titre (2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 284, top: 1280, width: 123, height: 15, 'font-size': 16, }}>
<span>
Nom patronymique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 759, top: 1279, width: 65, height: 14, 'font-size': 16, }}>
<span>
Prénom(s)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1312, width: 86, height: 13, 'font-size': 16, }}>
<span>
Nom marital
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 648, top: 1312, width: 105, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 843, top: 1312, width: 140, height: 17, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1347, width: 76, height: 12, 'font-size': 16, }}>
<span>
Naissance :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 221, top: 1347, width: 32, height: 12, 'font-size': 16, }}>
<span>
Date
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 391, top: 1347, width: 104, height: 15, 'font-size': 16, }}>
<span>
N° Département
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 581, top: 1347, width: 71, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 899, top: 1347, width: 31, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 1380, width: 105, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1381, width: 32, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 193, top: 1413, width: 71, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 419, top: 1414, width: 71, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 899, top: 1414, width: 30, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 96, top: 1447, width: 1001, height: 13, 'font-size': 13, }}>
<span>
(1) Lorsque le nombre d’associés excède le nombre de lignes de l’imprimé, utiliser un ou plusieurs tableaux supplémentaires. Dans ce cas, il convient de numéroter chaque tableau en
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1461, width: 711, height: 13, 'font-size': 13, }}>
<span>
haut et à gauche de la case prévue à cet effet et de porter le nombre total de tableaux souscrits en bas à droite de cette même case.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 96, top: 1478, width: 290, height: 13, 'font-size': 13, }}>
<span>
(2) Indiquer : M. pour Monsieur, MME pour Madame.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1497, width: 6, height: 6, 'font-size': 16, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1495, width: 451, height: 12, 'font-size': 13, }}>
<span>
Des explications concernant cette rubrique figurent dans la notice n° 2033-NOT-SD.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1204, width: 8, height: 5, 'font-size': 11, }}>
<span>
9
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="GS" data-field-id="31823976" data-annot-id="30968608" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="GT" data-field-id="31825240" data-annot-id="30968800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="GU" data-field-id="31825608" data-annot-id="30968992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="GV" data-field-id="31825912" data-annot-id="30969184" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="GW" data-field-id="31826248" data-annot-id="31258304" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="GAK1" data-field-id="31828408" data-annot-id="31258496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="GD1" data-field-id="31826584" data-annot-id="31258688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="GR1" data-field-id="31826920" data-annot-id="31258880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="BE1" data-field-id="31827256" data-annot-id="31259072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="BGH1" data-field-id="31841240" data-annot-id="31259264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="BD1" data-field-id="31827592" data-annot-id="31259456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="BR1" data-field-id="31828072" data-annot-id="31259648" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="GAA1" data-field-id="31828744" data-annot-id="31259840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="GAD1" data-field-id="31829080" data-annot-id="31260032" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="GAH1" data-field-id="31829416" data-annot-id="31260224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="GAG1" data-field-id="31853512" data-annot-id="31260416" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="GAI1" data-field-id="31830088" data-annot-id="31260608" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="GAJ1" data-field-id="31829752" data-annot-id="31261072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="GAT1" data-field-id="31830424" data-annot-id="31261264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="GAK2" data-field-id="31830904" data-annot-id="31261456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="GAT2" data-field-id="31831240" data-annot-id="31261648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="GAA2" data-field-id="31831576" data-annot-id="31261840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="GR2" data-field-id="31831912" data-annot-id="31262032" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="GD2" data-field-id="31832248" data-annot-id="31262224" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="GAJ2" data-field-id="31832584" data-annot-id="31262416" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="GAG2" data-field-id="31854728" data-annot-id="31262608" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="GAI2" data-field-id="31832920" data-annot-id="31262800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GAH2" data-field-id="31833256" data-annot-id="31262992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="GAD2" data-field-id="31833592" data-annot-id="31263184" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="GAK3" data-field-id="31833928" data-annot-id="31263376" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="GAD3" data-field-id="31834264" data-annot-id="31263568" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="GAH3" data-field-id="31834600" data-annot-id="31263760" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="GAT3" data-field-id="31834936" data-annot-id="31263952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="GAA3" data-field-id="31835272" data-annot-id="31260800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="GR3" data-field-id="31835608" data-annot-id="31264672" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="GAI3" data-field-id="31835944" data-annot-id="31264864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GAJ3" data-field-id="31830760" data-annot-id="31265056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="GD3" data-field-id="31836872" data-annot-id="31265248" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="GAK4" data-field-id="31837208" data-annot-id="31265440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="GAD4" data-field-id="31837544" data-annot-id="31265632" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="GAH4" data-field-id="31837880" data-annot-id="31265824" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="GAT4" data-field-id="31838216" data-annot-id="31266016" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GAI4" data-field-id="31838552" data-annot-id="31266208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="GR4" data-field-id="31838888" data-annot-id="31266400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="GAA4" data-field-id="31839224" data-annot-id="31266592" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GAJ4" data-field-id="31839560" data-annot-id="31266784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GD4" data-field-id="31839896" data-annot-id="31266976" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="BAK1" data-field-id="31840232" data-annot-id="31267168" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="BAA1" data-field-id="31840568" data-annot-id="31267360" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="BAB1" data-field-id="31840904" data-annot-id="31267552" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="BAC1" data-field-id="31858168" data-annot-id="31267744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="BGI1" data-field-id="31841576" data-annot-id="31267936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="BGJ1" data-field-id="31846040" data-annot-id="31268128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="BAD1" data-field-id="31841912" data-annot-id="31268320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="BAG1" data-field-id="31842248" data-annot-id="31268512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="BAH1" data-field-id="31842584" data-annot-id="31268704" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="BAI1" data-field-id="31842920" data-annot-id="31268896" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="BAJ1" data-field-id="31843256" data-annot-id="31269088" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="BAC1" data-field-id="31858168" data-annot-id="31269280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="BAK2" data-field-id="31843592" data-annot-id="31269472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="BAA2" data-field-id="31843928" data-annot-id="31269664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="BAC2" data-field-id="31844264" data-annot-id="31269856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="BE2" data-field-id="31844600" data-annot-id="31270048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="BAD2" data-field-id="31836280" data-annot-id="31270240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="BAH2" data-field-id="31849304" data-annot-id="31270432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="BGH2" data-field-id="31824488" data-annot-id="31264144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="BGI2" data-field-id="31846408" data-annot-id="31264336" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="BAG2" data-field-id="31847720" data-annot-id="31271664" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="BAI2" data-field-id="31848056" data-annot-id="31271856" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="BAJ2" data-field-id="31824120" data-annot-id="31272048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="BGJ1" data-field-id="31846040" data-annot-id="31272240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="BD2" data-field-id="31845608" data-annot-id="31272432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="BAB2" data-field-id="31844936" data-annot-id="31272624" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="BR2" data-field-id="31845272" data-annot-id="31272816" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="BAE1" data-field-id="31849576" data-annot-id="31273008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="BAF1" data-field-id="31849912" data-annot-id="31273200" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="BAE2" data-field-id="31850248" data-annot-id="31273392" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="BAF2" data-field-id="31850584" data-annot-id="31273584" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="GAE1" data-field-id="31850920" data-annot-id="31273776" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="GAF1" data-field-id="31851544" data-annot-id="31273968" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="GAE2" data-field-id="31851848" data-annot-id="31274160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="GAF2" data-field-id="31853208" data-annot-id="31274352" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="GAE3" data-field-id="31855064" data-annot-id="31274544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="GAF3" data-field-id="31855400" data-annot-id="31274736" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="GAG3" data-field-id="31855736" data-annot-id="31274928" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="GAE4" data-field-id="31856072" data-annot-id="31275120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="GAF4" data-field-id="31856408" data-annot-id="31275312" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="GAG4" data-field-id="31856744" data-annot-id="31275504" type="text" />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="FIN_EX" data-field-id="9214568" data-annot-id="8630064" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field pde-form-field-text" name="SIREN_STET" data-field-id="9217896" data-annot-id="8630256" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="9216888" data-annot-id="8630448" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field pde-form-field-text" name="VILLE_STEI" data-field-id="9217560" data-annot-id="8630640" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field pde-form-field-text" name="CP_STEH" data-field-id="9217224" data-annot-id="8630832" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STED" data-field-id="9215880" data-annot-id="8631024" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEF" data-field-id="9216216" data-annot-id="8631216" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEG" data-field-id="9216552" data-annot-id="8631408" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2033F;
