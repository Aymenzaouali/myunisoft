import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {sum, useCompute, parse as p} from "helpers/pdfforms";
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';

import './style.scss';

const amount = (values, fields) => (
  p(values[fields[0]]) - (p(values[fields[1]]) + p(values[fields[2]]))
);

const Tax2035A = (props) => {
  const { taxDeclarationForm, change } = props;

  useCompute('AD', amount, ['AA', 'AB', 'AC'], taxDeclarationForm, change);
  useCompute('AG', sum, ['AD', 'AE', 'AF'], taxDeclarationForm, change);
  useCompute('BH', sum, ['EB', 'EC', 'ED', 'EE', 'EF', 'EG'], taxDeclarationForm, change);
  useCompute('BJ', sum, ['GF', 'EJ'], taxDeclarationForm, change);
  useCompute('BM', sum, ['BL', 'EK', 'EL', 'EM', 'EN'], taxDeclarationForm, change);
  useCompute('BR', sum, ['BL', 'BA', 'BB', 'BC', 'BD', 'BE', 'BS', 'BV', 'BF', 'BG', 'BH', 'BJ', 'BK', 'BM', 'BN', 'BP'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2035a">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 539, top: 50, width: 199, height: 21, 'font-size': 27, }}>
<span>
REVENUS 2018
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 232, top: 55, width: 120, height: 12, 'font-size': 12, }}>
<span>
Formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 231, top: 68, width: 143, height: 11, 'font-size': 12, }}>
<span>
(article 40 A de l’annexe III
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 231, top: 81, width: 155, height: 12, 'font-size': 12, }}>
<span>
au Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 97, width: 95, height: 13, 'font-size': 16, }}>
<span>
N° 15945 Ý 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 957, top: 50, width: 196, height: 17, 'font-size': 22, }}>
<span>
N° 2035-A-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 94, width: 143, height: 9, 'font-size': 9, }}>
<span>
Si ce  ormulaire est déposé sans
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 106, width: 141, height: 8, 'font-size': 9, }}>
<span>
in  ormations chi  rées, cocher la
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 117, width: 88, height: 8, 'font-size': 9, }}>
<span>
case Néant ci-contre :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 131, width: 140, height: 9, 'font-size': 9, }}>
<span>
Ne porter qu’une somme par ligne
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 142, width: 112, height: 9, 'font-size': 9, }}>
<span>
(ne pas porter les centimes)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 599, top: 140, width: 19, height: 12, 'font-size': 16, }}>
<span>
AJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 552, top: 144, width: 37, height: 14, 'font-size': 18, }}>
<span>
pour
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 683, top: 141, width: 39, height: 14, 'font-size': 18, }}>
<span>
mois
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 161, top: 187, width: 298, height: 16, 'font-size': 16, }}>
<span>
NOM ET PRÉNOMS OU DÉNOMINATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 222, width: 152, height: 15, 'font-size': 16, }}>
<span>
Nature de l’activité (1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 912, top: 218, width: 139, height: 14, 'font-size': 14, }}>
<span>
Code activité pour les
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 915, top: 231, width: 128, height: 14, 'font-size': 14, }}>
<span>
praticiens médicaux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 259, width: 66, height: 12, 'font-size': 16, }}>
<span>
N° SIRET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 720, top: 259, width: 179, height: 14, 'font-size': 14, }}>
<span>
si exercice en société (2) AV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 943, top: 259, width: 149, height: 12, 'font-size': 14, }}>
<span>
Nombre d’associés AS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 293, width: 166, height: 14, 'font-size': 16, }}>
<span>
Résultat déterminé (2) :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 377, top: 293, width: 284, height: 15, 'font-size': 14, }}>
<span>
d’après les règles «recettes-dépenses» AK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 712, top: 293, width: 270, height: 15, 'font-size': 14, }}>
<span>
d’après les règles «créances-dettes» AL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 328, width: 167, height: 15, 'font-size': 16, }}>
<span>
Comptabilité tenue (2) :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 333, top: 329, width: 60, height: 11, 'font-size': 14, }}>
<span>
Hors taxe
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 328, width: 22, height: 12, 'font-size': 16, }}>
<span>
CV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 524, top: 329, width: 80, height: 11, 'font-size': 14, }}>
<span>
Taxe incluse
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 675, top: 328, width: 26, height: 12, 'font-size': 16, }}>
<span>
CW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 770, top: 329, width: 142, height: 14, 'font-size': 14, }}>
<span>
Non assujetti à la TVA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 328, width: 21, height: 12, 'font-size': 16, }}>
<span>
AT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 355, width: 241, height: 15, 'font-size': 16, }}>
<span>
Si vous êtes adhérent d’un organis
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 470, top: 363, width: 42, height: 12, 'font-size': 14, }}>
<span>
Année
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 675, top: 364, width: 51, height: 11, 'font-size': 14, }}>
<span>
Nombre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 878, top: 363, width: 50, height: 12, 'font-size': 14, }}>
<span>
Salaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 370, width: 155, height: 15, 'font-size': 16, }}>
<span>
association agréée (2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 469, top: 379, width: 70, height: 11, 'font-size': 14, }}>
<span>
d’adhésion
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 674, top: 379, width: 68, height: 11, 'font-size': 14, }}>
<span>
de salariés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 878, top: 380, width: 74, height: 13, 'font-size': 14, }}>
<span>
nets perçus
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 192, width: 6, height: 14, 'font-size': 20, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 373, width: 24, height: 12, 'font-size': 15, }}>
<span>
AM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 560, top: 373, width: 21, height: 12, 'font-size': 15, }}>
<span>
AN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 783, top: 373, width: 19, height: 12, 'font-size': 15, }}>
<span>
AP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 993, top: 373, width: 20, height: 12, 'font-size': 15, }}>
<span>
AR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 451, width: 9, height: 14, 'font-size': 20, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 520, width: 8, height: 10, 'font-size': 13, }}>
<span>
R
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 541, width: 7, height: 10, 'font-size': 13, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 561, width: 8, height: 10, 'font-size': 13, }}>
<span>
C
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 581, width: 8, height: 10, 'font-size': 13, }}>
<span>
T
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 602, width: 8, height: 10, 'font-size': 13, }}>
<span>
T
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 622, width: 7, height: 10, 'font-size': 13, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 642, width: 8, height: 11, 'font-size': 13, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 160, top: 476, width: 425, height: 14, 'font-size': 15, }}>
<span>
1 Recettes encaissées y compris les remboursements de  rais
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 589, top: 472, width: 17, height: 17, 'font-size': 19, }}>
<span>
B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 607, top: 485, width: 310, height: 2, 'font-size': 15, }}>
<span>
..............................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 929, top: 478, width: 19, height: 11, 'font-size': 14, }}>
<span>
AA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 504, width: 7, height: 11, 'font-size': 15, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 183, top: 517, width: 66, height: 12, 'font-size': 15, }}>
<span>
A déduire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 282, top: 505, width: 283, height: 15, 'font-size': 15, }}>
<span>
Débours payés pour le compte des clients
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 502, width: 17, height: 17, 'font-size': 19, }}>
<span>
C
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 514, width: 330, height: 3, 'font-size': 15, }}>
<span>
...................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 929, top: 504, width: 18, height: 11, 'font-size': 14, }}>
<span>
AB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 538, width: 7, height: 11, 'font-size': 15, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 282, top: 535, width: 357, height: 15, 'font-size': 15, }}>
<span>
Honoraires rétrocédés (dont suppléments rétrocédés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 748, top: 535, width: 3, height: 13, 'font-size': 15, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 531, width: 17, height: 17, 'font-size': 19, }}>
<span>
D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 544, width: 143, height: 3, 'font-size': 15, }}>
<span>
....................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 929, top: 538, width: 19, height: 11, 'font-size': 14, }}>
<span>
AC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 158, top: 575, width: 790, height: 12, 'font-size': 15, }}>
<span>
4 Montant net des recettes ............................................................................................................................................ AD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 609, width: 151, height: 14, 'font-size': 15, }}>
<span>
5 Produits fnanciers
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 314, top: 607, width: 17, height: 17, 'font-size': 19, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 332, top: 620, width: 585, height: 2, 'font-size': 15, }}>
<span>
...................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 929, top: 609, width: 18, height: 11, 'font-size': 14, }}>
<span>
AE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 642, width: 109, height: 13, 'font-size': 15, }}>
<span>
6 Gains divers
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 272, top: 639, width: 17, height: 17, 'font-size': 19, }}>
<span>
F
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 290, top: 652, width: 625, height: 3, 'font-size': 15, }}>
<span>
.............................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 642, width: 17, height: 11, 'font-size': 14, }}>
<span>
AF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 158, top: 677, width: 8, height: 11, 'font-size': 15, }}>
<span>
7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 286, top: 676, width: 134, height: 15, 'font-size': 15, }}>
<span>
TOTAL (lignes 4 à 6)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 422, top: 686, width: 449, height: 2, 'font-size': 15, }}>
<span>
.................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 873, top: 676, width: 75, height: 12, 'font-size': 15, }}>
<span>
........... AG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 703, width: 10, height: 15, 'font-size': 20, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 709, width: 70, height: 11, 'font-size': 15, }}>
<span>
8 Achats
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 233, top: 705, width: 17, height: 17, 'font-size': 19, }}>
<span>
G
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 251, top: 717, width: 665, height: 3, 'font-size': 15, }}>
<span>
.......................................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 708, width: 19, height: 11, 'font-size': 14, }}>
<span>
BA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 159, top: 743, width: 8, height: 11, 'font-size': 15, }}>
<span>
9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 184, top: 760, width: 124, height: 14, 'font-size': 15, }}>
<span>
Frais de personnel
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 355, top: 741, width: 245, height: 15, 'font-size': 15, }}>
<span>
Salaires nets et avantages en nature
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 604, top: 737, width: 17, height: 17, 'font-size': 19, }}>
<span>
H
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 622, top: 750, width: 294, height: 3, 'font-size': 15, }}>
<span>
..........................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 742, width: 17, height: 11, 'font-size': 14, }}>
<span>
BB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 778, width: 14, height: 12, 'font-size': 15, }}>
<span>
10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 355, top: 772, width: 388, height: 15, 'font-size': 15, }}>
<span>
Charges sociales sur salaires (parts patronale et ouvrière)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 744, top: 782, width: 166, height: 2, 'font-size': 15, }}>
<span>
..........................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 778, width: 17, height: 11, 'font-size': 14, }}>
<span>
BC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 157, top: 812, width: 10, height: 11, 'font-size': 15, }}>
<span>
11
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 184, top: 847, width: 103, height: 15, 'font-size': 15, }}>
<span>
Impôts et taxes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 291, top: 844, width: 17, height: 17, 'font-size': 19, }}>
<span>
I
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 354, top: 808, width: 174, height: 15, 'font-size': 15, }}>
<span>
Taxe sur la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 528, top: 817, width: 382, height: 3, 'font-size': 15, }}>
<span>
................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 812, width: 18, height: 11, 'font-size': 14, }}>
<span>
BD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 845, width: 14, height: 11, 'font-size': 15, }}>
<span>
12
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 604, top: 850, width: 305, height: 2, 'font-size': 15, }}>
<span>
.............................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 355, top: 841, width: 248, height: 14, 'font-size': 15, }}>
<span>
Contribution économique territoriale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 845, width: 18, height: 11, 'font-size': 14, }}>
<span>
JY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 879, width: 14, height: 11, 'font-size': 15, }}>
<span>
13
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 354, top: 873, width: 96, height: 15, 'font-size': 15, }}>
<span>
Autres impôts
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 451, top: 882, width: 453, height: 3, 'font-size': 15, }}>
<span>
..................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 906, top: 878, width: 42, height: 12, 'font-size': 15, }}>
<span>
. BS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 913, width: 14, height: 11, 'font-size': 15, }}>
<span>
14
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 323, top: 910, width: 17, height: 17, 'font-size': 19, }}>
<span>
I
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 355, top: 909, width: 293, height: 14, 'font-size': 15, }}>
<span>
Contribution sociale généralisée déductible
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 649, top: 918, width: 258, height: 2, 'font-size': 15, }}>
<span>
.................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 913, width: 17, height: 10, 'font-size': 14, }}>
<span>
BV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 948, width: 203, height: 14, 'font-size': 15, }}>
<span>
15 Loyer et charges locatives
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 360, top: 957, width: 553, height: 2, 'font-size': 15, }}>
<span>
...........................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 948, width: 16, height: 11, 'font-size': 14, }}>
<span>
BF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 987, width: 503, height: 13, 'font-size': 15, }}>
<span>
16 Location de matériel et de mobilier - dont redevances de collaboration
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 664, top: 984, width: 16, height: 17, 'font-size': 19, }}>
<span>
J
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 681, top: 997, width: 15, height: 3, 'font-size': 15, }}>
<span>
....
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 987, width: 18, height: 11, 'font-size': 14, }}>
<span>
BG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 738, width: 7, height: 50, 'font-size': 54, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 329, top: 808, width: 7, height: 81, 'font-size': 88, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 708, top: 982, width: 22, height: 11, 'font-size': 14, }}>
<span>
BW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 1016, width: 186, height: 14, 'font-size': 15, }}>
<span>
17 Entretien et réparations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 343, top: 1025, width: 182, height: 3, 'font-size': 15, }}>
<span>
..............................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 1050, width: 173, height: 12, 'font-size': 15, }}>
<span>
18 Personnel intérimaire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 1059, width: 194, height: 3, 'font-size': 15, }}>
<span>
.................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 156, top: 1086, width: 120, height: 16, 'font-size': 15, }}>
<span>
19 Petit outillage
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 280, top: 1084, width: 17, height: 17, 'font-size': 19, }}>
<span>
K
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 298, top: 1097, width: 226, height: 2, 'font-size': 15, }}>
<span>
.........................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 1086, width: 18, height: 11, 'font-size': 14, }}>
<span>
BH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1118, width: 234, height: 15, 'font-size': 15, }}>
<span>
20 Chau  age, eau, gaz, électricité
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 1127, width: 130, height: 3, 'font-size': 15, }}>
<span>
.................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1150, width: 349, height: 16, 'font-size': 15, }}>
<span>
21 Honoraires ne constituant pas des rétrocessions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 509, top: 1148, width: 17, height: 17, 'font-size': 19, }}>
<span>
L
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1187, width: 166, height: 11, 'font-size': 15, }}>
<span>
22 Primes d&#39;assurances
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 322, top: 1195, width: 198, height: 3, 'font-size': 15, }}>
<span>
...................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1222, width: 150, height: 13, 'font-size': 15, }}>
<span>
23 Frais de véhicules
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 309, top: 1219, width: 17, height: 18, 'font-size': 19, }}>
<span>
M
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 184, top: 1261, width: 257, height: 13, 'font-size': 15, }}>
<span>
(cochez la case si évaluation  or  aitaire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 485, top: 1261, width: 4, height: 13, 'font-size': 15, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 490, top: 1270, width: 34, height: 3, 'font-size': 15, }}>
<span>
.........
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 1255, width: 15, height: 11, 'font-size': 14, }}>
<span>
BJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1291, width: 288, height: 15, 'font-size': 15, }}>
<span>
24 Autres  rais de déplacements (voyages
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 444, top: 1291, width: 15, height: 14, 'font-size': 15, }}>
<span>
... )
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 460, top: 1301, width: 65, height: 2, 'font-size': 15, }}>
<span>
.................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1326, width: 210, height: 16, 'font-size': 15, }}>
<span>
25 Charges sociales personnelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 368, top: 1324, width: 16, height: 17, 'font-size': 19, }}>
<span>
N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 1328, width: 107, height: 14, 'font-size': 15, }}>
<span>
: dont obligatoires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 1326, width: 17, height: 11, 'font-size': 14, }}>
<span>
BK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1355, width: 369, height: 15, 'font-size': 15, }}>
<span>
26 Frais de réception, de représentation et de congrès
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1390, width: 15, height: 11, 'font-size': 15, }}>
<span>
27
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 184, top: 1381, width: 319, height: 14, 'font-size': 15, }}>
<span>
Fournitures de bureau,  rais de documentation,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 423, top: 1406, width: 102, height: 3, 'font-size': 15, }}>
<span>
..........................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 184, top: 1397, width: 238, height: 14, 'font-size': 15, }}>
<span>
de correspondance et de téléphone
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1428, width: 234, height: 12, 'font-size': 15, }}>
<span>
28 Frais d&#39;actes et de contentieux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 1437, width: 134, height: 2, 'font-size': 15, }}>
<span>
..................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 929, top: 1428, width: 20, height: 11, 'font-size': 14, }}>
<span>
BM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1464, width: 312, height: 14, 'font-size': 15, }}>
<span>
29 Cotisations syndicales et pro  essionnelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 467, top: 1473, width: 27, height: 2, 'font-size': 15, }}>
<span>
.......
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1499, width: 227, height: 15, 'font-size': 15, }}>
<span>
30 Autres  rais divers de gestion
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 383, top: 1508, width: 142, height: 3, 'font-size': 15, }}>
<span>
....................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1535, width: 131, height: 13, 'font-size': 15, }}>
<span>
31 Frais fnanciers
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 294, top: 1533, width: 17, height: 17, 'font-size': 19, }}>
<span>
O
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 316, top: 1535, width: 632, height: 13, 'font-size': 15, }}>
<span>
....................................................................................................................................................... BN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1569, width: 131, height: 13, 'font-size': 15, }}>
<span>
32 Pertes diverses
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 294, top: 1567, width: 17, height: 17, 'font-size': 19, }}>
<span>
P
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 316, top: 1569, width: 632, height: 13, 'font-size': 15, }}>
<span>
...................................................................................................................................................... BP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1608, width: 16, height: 11, 'font-size': 15, }}>
<span>
33
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 286, top: 1607, width: 143, height: 15, 'font-size': 15, }}>
<span>
TOTAL (lignes 8 à 32)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 430, top: 1617, width: 445, height: 2, 'font-size': 15, }}>
<span>
................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1608, width: 71, height: 11, 'font-size': 15, }}>
<span>
......... BR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1064, width: 42, height: 10, 'font-size': 12, }}>
<span>
TOTAL :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1079, width: 44, height: 10, 'font-size': 12, }}>
<span>
travaux,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1092, width: 54, height: 10, 'font-size': 12, }}>
<span>
 ourniture
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1106, width: 57, height: 9, 'font-size': 12, }}>
<span>
et services
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1119, width: 54, height: 10, 'font-size': 12, }}>
<span>
extérieurs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 737, top: 1013, width: 10, height: 189, 'font-size': 205, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 510, top: 1331, width: 17, height: 10, 'font-size': 14, }}>
<span>
BT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 510, top: 1461, width: 17, height: 11, 'font-size': 14, }}>
<span>
BY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 1331, width: 18, height: 11, 'font-size': 14, }}>
<span>
BU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 661, top: 1327, width: 101, height: 12, 'font-size': 15, }}>
<span>
dont  acultatives
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1233, width: 42, height: 10, 'font-size': 12, }}>
<span>
TOTAL :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1248, width: 63, height: 11, 'font-size': 12, }}>
<span>
transport et
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1261, width: 76, height: 12, 'font-size': 12, }}>
<span>
déplacements
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 740, top: 1208, width: 5, height: 106, 'font-size': 114, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 738, top: 1346, width: 8, height: 167, 'font-size': 181, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1412, width: 42, height: 10, 'font-size': 12, }}>
<span>
TOTAL :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1426, width: 60, height: 10, 'font-size': 12, }}>
<span>
 rais divers
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 756, top: 1440, width: 56, height: 12, 'font-size': 12, }}>
<span>
de gestion
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 923, width: 8, height: 10, 'font-size': 13, }}>
<span>
D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 941, width: 7, height: 13, 'font-size': 13, }}>
<span>
É
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 964, width: 8, height: 10, 'font-size': 13, }}>
<span>
P
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 984, width: 7, height: 11, 'font-size': 13, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 1005, width: 9, height: 10, 'font-size': 13, }}>
<span>
N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1025, width: 7, height: 10, 'font-size': 13, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1045, width: 7, height: 11, 'font-size': 13, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1066, width: 7, height: 10, 'font-size': 13, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1109, width: 8, height: 10, 'font-size': 13, }}>
<span>
P
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1130, width: 7, height: 10, 'font-size': 13, }}>
<span>
R
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 1150, width: 9, height: 10, 'font-size': 13, }}>
<span>
O
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1170, width: 6, height: 10, 'font-size': 13, }}>
<span>
F
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1191, width: 7, height: 10, 'font-size': 13, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1211, width: 7, height: 10, 'font-size': 13, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1231, width: 7, height: 11, 'font-size': 13, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 134, top: 1252, width: 3, height: 10, 'font-size': 13, }}>
<span>
I
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 1272, width: 9, height: 10, 'font-size': 13, }}>
<span>
O
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 1292, width: 9, height: 11, 'font-size': 13, }}>
<span>
N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 1313, width: 9, height: 10, 'font-size': 13, }}>
<span>
N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1333, width: 7, height: 10, 'font-size': 13, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1354, width: 6, height: 10, 'font-size': 13, }}>
<span>
L
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 1374, width: 6, height: 10, 'font-size': 13, }}>
<span>
L
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1394, width: 7, height: 10, 'font-size': 13, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1414, width: 7, height: 11, 'font-size': 13, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1272, width: 9, height: 67, 'font-size': 11, }}>
<span>
N° 2035-A-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1083, width: 12, height: 183, 'font-size': 12, }}>
<span>
– (SDNC-DGFiP) – Novembre 2018
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 514, top: 104, width: 249, height: 16, 'font-size': 16, }}>
<span>
COMPTE DE RÉSULTAT FISCAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 357, width: 247, height: 15, 'font-size': 16, }}>
<span>
Si vous êtes adhérent d&#39;un organisme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 372, width: 220, height: 15, 'font-size': 16, }}>
<span>
agréé (association ou organisme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 387, width: 223, height: 15, 'font-size': 16, }}>
<span>
mixte) ou client d&#39;un viseur fscal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 414, width: 214, height: 12, 'font-size': 16, }}>
<span>
Montant des immobilisations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 347, top: 416, width: 220, height: 12, 'font-size': 12, }}>
<span>
(report du total des bases amortissables
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 429, width: 382, height: 12, 'font-size': 12, }}>
<span>
hors TVA déductible de la col. 4 du tableau I de la déclaration n° 2035)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 638, top: 421, width: 20, height: 11, 'font-size': 15, }}>
<span>
DA
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="FH" data-field-id="31143848" data-annot-id="29958880" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="GB" data-field-id="31144056" data-annot-id="29959072" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="GC" data-field-id="31144392" data-annot-id="30328768" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="GD" data-field-id="31144696" data-annot-id="30328960" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="GE" data-field-id="31175736" data-annot-id="30329152" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="GK" data-field-id="31176072" data-annot-id="30329344" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="FA" data-field-id="31176408" data-annot-id="30329536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="GA" data-field-id="31042296" data-annot-id="30329728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="FG" data-field-id="31176744" data-annot-id="30333744" type="text" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="FJ" data-field-id="31177080" data-annot-id="30333936" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="GH" data-field-id="31177560" data-annot-id="30334128" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="FF" data-field-id="31177896" data-annot-id="30334320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="FD" data-field-id="31178232" data-annot-id="30334512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="FC" data-field-id="31178568" data-annot-id="30334704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="GJ" data-field-id="31178904" data-annot-id="30334896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="AA" data-field-id="31179240" data-annot-id="30335088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="AB" data-field-id="31179576" data-annot-id="30335280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="AC" data-field-id="31179912" data-annot-id="30335744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="AD" data-field-id="31180392" data-annot-id="30335936" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="AE" data-field-id="31180728" data-annot-id="30336128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="AF" data-field-id="31181064" data-annot-id="30336320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="AG" data-field-id="31181400" data-annot-id="30336512" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="BB" data-field-id="31181736" data-annot-id="30336704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="BA" data-field-id="31182072" data-annot-id="30336896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="BC" data-field-id="31182408" data-annot-id="30337088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="BD" data-field-id="31182744" data-annot-id="30337280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="BE" data-field-id="31183080" data-annot-id="30337472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="BS" data-field-id="31183416" data-annot-id="30337664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="BV" data-field-id="31183752" data-annot-id="30337856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="BF" data-field-id="31184088" data-annot-id="30338048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="BG" data-field-id="31184424" data-annot-id="30338240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="EA" data-field-id="31184760" data-annot-id="30338432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="GL" data-field-id="31185096" data-annot-id="30338624" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="BH" data-field-id="31185432" data-annot-id="30335472" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="BJ" data-field-id="31180216" data-annot-id="30339344" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="BK" data-field-id="31186328" data-annot-id="30339536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="BM" data-field-id="31186664" data-annot-id="30339728" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="BN" data-field-id="31187000" data-annot-id="30339920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="BP" data-field-id="31187336" data-annot-id="30340112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="BR" data-field-id="31187672" data-annot-id="30340304" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="EB" data-field-id="31188008" data-annot-id="30340496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="EC" data-field-id="31188344" data-annot-id="30340688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="ED" data-field-id="31188680" data-annot-id="30340880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="EE" data-field-id="31189016" data-annot-id="30341072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="EF" data-field-id="31189352" data-annot-id="30341264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="EG" data-field-id="31038600" data-annot-id="30341456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GF" data-field-id="31038936" data-annot-id="30341648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="EJ" data-field-id="31039272" data-annot-id="30341840" type="text" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="GG" data-field-id="31039608" data-annot-id="30342032" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="BT" data-field-id="31039944" data-annot-id="30342224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="BU" data-field-id="31040280" data-annot-id="30342416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="BL" data-field-id="31040616" data-annot-id="30342608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="EK" data-field-id="31040952" data-annot-id="30342800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="EL" data-field-id="31041288" data-annot-id="30342992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="EM" data-field-id="31041624" data-annot-id="30343184" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="EN" data-field-id="31041960" data-annot-id="30343376" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="14065576" data-annot-id="13211552" type="text" disabled />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="SIRET_STET" data-field-id="14065912" data-annot-id="13211744" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2035A;
