import React from 'react';
import { Field } from 'redux-form';
import './style.scss';

import { useCompute, copy, sum, parse as p } from 'helpers/pdfforms';

import { PdfNumberInput, PdfDateInputFormat } from 'components/reduxForm/Inputs';
import { PdfCheckbox } from 'components/reduxForm/Inputs';

// Utils
const min = Math.min;
const max = Math.max;

const cadreAtest = compute => (values, fields) => {
  if (p(values.CP) <= 100000000) return compute(values, fields);
  return '';
};
const cadreBtest = compute => (values, fields) => {
  if (p(values.CP) > 100000000) return compute(values, fields);
  return '';
};

const compute25 = ({ BM, BN, BS, BT }) => {
  if (BM === undefined && BN === undefined && BS === undefined && BT === undefined) {
    return 2000000;
  }

  return 10000000 + min(p(BM) + p(BN), 2000000);
};

const compute50a = ({ CX, CY, CZ }) => {
  if (p(CY) === 200000) return 0;
  if (p(CZ) < 200000) return p(CX);
  return 200000 - p(CY);
};

const compute61 = ({ DK, GK, DL }) => {
  const lDK = min(p(DK), p(DL));
  const lGK = min(p(GK), p(DL));

  return (lDK - lGK) * .3 + lGK * .5;
};

const compute68a = ({ DR, DS, DT }) => {
  if (p(DS) === 200000) return 0;
  if (p(DT) < 200000) return p(DR);
  return 200000 - p(DS);
};

// Component
const Tax2069A = (props) => {
  const { formValues, change } = props;

  // Compute
  useCompute('AY', ({ AT, AV, AW, AX }) => p(AT) * .75 + (p(AV) + p(AW)) * .5 + p(AX), ['AT', 'AV', 'AW', 'AX'], formValues, change);
  useCompute('AZ', sum, ['AT', 'AU', 'AV', 'AW', 'AX', 'AY'], formValues, change);
  useCompute('BJ', sum, ['AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF'], formValues, change);
  useCompute('BP', sum, ['BK', 'BL', 'BM', 'BN'], formValues, change);
  useCompute('BU', sum, ['BQ', 'BR', 'BS', 'BT'], formValues, change);
  useCompute('BV', ({ BJ, BP, BU }) => min(p(BU), (p(BJ) + p(BP)) * 3), ['BJ', 'BP', 'BU'], formValues, change);
  useCompute('BX', sum, ['BP', 'BV'], formValues, change);
  useCompute('BY', ({ BK, BL, BQ, BR, BV }) => min(min(p(BQ) + p(BR), p(BV)) + p(BK) + p(BL), 2000000), ['BK', 'BL', 'BQ', 'BR', 'BV'], formValues, change);
  useCompute('BZ', ({ BM, BN, BS, BT, BX, BY }) => min(p(BM) + p(BN) + p(BS) + p(BT), p(BX) - p(BY)), ['BM', 'BN', 'BS', 'BT', 'BX', 'BY'], formValues, change);
  useCompute('BH', compute25, ['BM', 'BN', 'BS', 'BT'], formValues, change);
  useCompute('BG', ({ BY, BZ, BH }) => min(p(BY) + p(BZ), p(BH)), ['BY', 'BZ', 'BH'], formValues, change);
  useCompute('CA', sum, ['BJ', 'BG'], formValues, change);
  useCompute('CF', ({ CA, CB, CC, CD, CE }) => p(CA) - p(CB) - p(CC) - p(CD) + p(CE), ['CA', 'CB', 'CC', 'CD', 'CE'], formValues, change);
  useCompute('CJ', sum, ['CG', 'CH'], formValues, change);
  useCompute('CN', ({ CJ, CK, CL, CM }) => p(CJ) - p(CK) - p(CL) + p(CM), ['CJ', 'CK', 'CL', 'CM'], formValues, change);
  useCompute('CP', sum, ['CF', 'CN'], formValues, change);
  useCompute('FW', sum, ['FU', 'FV'], formValues, change);

  useCompute('CQ', cadreAtest(copy), ['CP', 'CF'], formValues, change);
  useCompute('FX', cadreAtest(copy), ['CP', 'FU'], formValues, change);
  useCompute('CR', cadreAtest(({ CQ, FX }) => (p(CQ) - p(FX)) * .3 + p(FX) * .5), ['CP', 'CQ', 'FX'], formValues, change);
  useCompute('CS', cadreAtest(copy), ['CP', 'ET'], formValues, change);
  useCompute('CT', cadreAtest(sum), ['CP', 'CR', 'CS'], formValues, change);
  useCompute('CU', cadreAtest(copy), ['CP', 'CN'], formValues, change);
  useCompute('GA', cadreAtest(copy), ['CP', 'FV'], formValues, change);
  useCompute('CV', cadreAtest(({ CU, GA }) => (p(CU) - p(GA)) * .3 + p(GA) * .5), ['CP', 'CU', 'GA'], formValues, change);
  useCompute('CW', cadreAtest(copy), ['CP', 'EU'], formValues, change);
  useCompute('CX', cadreAtest(sum), ['CP', 'CV', 'CW'], formValues, change);
  useCompute('CZ', cadreAtest(sum), ['CP', 'CX', 'CY'], formValues, change);
  useCompute('DA', cadreAtest(compute50a), ['CP', 'CX', 'CY', 'CZ'], formValues, change);
  useCompute('DB', cadreAtest(sum), ['CP', 'CT', 'DA'], formValues, change);
  useCompute('GE', cadreAtest(sum), ['CP', 'FZ', 'GD'], formValues, change);

  useCompute('DC', cadreBtest(({ CF }) => min(p(CF), 100000000)), ['CP', 'CF'], formValues, change);
  useCompute('GF', cadreBtest(({ FU }) => min(p(FU), 100000000)), ['CP', 'FU'], formValues, change);
  useCompute('DD', cadreBtest(({ DC, GF }) => (p(DC) - p(GF)) * .3 + p(GF) * .5), ['CP', 'DC', 'GF'], formValues, change);
  useCompute('DE', cadreBtest(({ CF }) => max(p(CF) - 100000000, 0)), ['CP', 'CF'], formValues, change);
  useCompute('DF', cadreBtest(({ DE }) => p(DE) * .05), ['CP', 'DE'], formValues, change);
  useCompute('DG', cadreBtest(sum), ['CP', 'DD', 'DF'], formValues, change);
  useCompute('DH', cadreBtest(copy), ['CP', 'ET'], formValues, change);
  useCompute('DJ', cadreBtest(sum), ['CP', 'DG', 'DH'], formValues, change);
  useCompute('DK', cadreBtest(copy), ['CP', 'CN'], formValues, change);
  useCompute('GK', cadreBtest(copy), ['CP', 'FV'], formValues, change);
  useCompute('DL', cadreBtest(({ DC }) => p(DC) < 100000000 ? 100000000 - p(DC) : 0), ['CP', 'DC'], formValues, change);
  useCompute('DM', cadreBtest(compute61), ['CP', 'DK', 'GK', 'DL'], formValues, change);
  useCompute('DN', cadreBtest(({ DK, DL }) => p(DK) > p(DL) ? (p(DK) - p(DL)) * .5 : 0), ['CP', 'DK', 'DL'], formValues, change);
  useCompute('DP', cadreBtest(sum), ['CP', 'DM', 'DN'], formValues, change);
  useCompute('DQ', cadreBtest(copy), ['CP', 'EU'], formValues, change);
  useCompute('DR', cadreBtest(sum), ['CP', 'DP', 'DQ'], formValues, change);
  useCompute('DT', cadreBtest(sum), ['CP', 'DR', 'DS'], formValues, change);
  useCompute('DU', cadreBtest(compute68a), ['CP', 'DR', 'DS', 'DT'], formValues, change);
  useCompute('DV', cadreBtest(sum), ['CP', 'DK', 'DU'], formValues, change);
  useCompute('GP', cadreBtest(sum), ['CP', 'GK', 'GN'], formValues, change);

  useCompute('DY', ({ DW, DX }) => p(DW) * .75 + p(DX) * .5, ['DW', 'DX'], formValues, change);
  useCompute('EC', sum, ['DW', 'DX', 'DY', 'DZ', 'EA', 'EB'], formValues, change);
  useCompute('ED', ({ EC }) => min(p(EC), 400000), ['EC'], formValues, change);
  useCompute('GQ', ({ ED, EE, EF, EG, EH }) => p(ED) - p(EE) - p(EF) - p(EG) + p(EH), ['ED', 'EE', 'EF', 'EG', 'EH'], formValues, change);
  useCompute('EJ', ({ GQ, GR }) => (p(GQ) - p(GR)) * .2 + p(GR) * .4, ['GQ', 'GR'], formValues, change);
  useCompute('EK', copy, ['EV'], formValues, change);
  useCompute('EL', sum, ['EJ', 'EK'], formValues, change);
  useCompute('EM', ({ DB, DV, EL }) => p(DB || DV) + p(EL), ['DB', 'DV', 'EL'], formValues, change);
  useCompute('GU', ({ GE, GP, GT }) => p(GE || GP) + p(GT), ['DB', 'GP', 'GT'], formValues, change);

  useCompute('ET', sum, ['EQ1', 'EQ2', 'EQ3', 'EQ4'], formValues, change);
  useCompute('EU', sum, ['ER1', 'ER2', 'ER3', 'ER4'], formValues, change);
  useCompute('EV', sum, ['EQ1', 'EQ2', 'EQ3', 'EQ4'], formValues, change);

  useCompute('FB', sum, ['EY1', 'EY2', 'EY3', 'EY4'], formValues, change);
  useCompute('FC', sum, ['EZ1', 'EZ2', 'EZ3', 'EZ4'], formValues, change);
  useCompute('FD', sum, ['FA1', 'FA2', 'FA3', 'FA4'], formValues, change);

  // Rendering
  return (
    <div className="tax2069A">
      <div id="pdf-document" data-type="pdf-document" data-num-pages="6" data-layout="fixed" >

</div>

<form id="acroform">
</form>

<div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
  <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
    <div data-type="pdf-page-text" className="pdf-page-text">
      <div className="pdf-obj-fixed" style={{ left: 362, top: 160, width: 479, height: 19, 'font-size': 20, }}>
        <span>
          DIRECTION GENERALE DES FINANCES PUBLIQUES
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 326, top: 196, width: 550, height: 22, 'font-size': 23, }}>
        <span>
          CRÉDIT D’IMPÔT EN FAVEUR DE LA RECHERCHE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 328, top: 227, width: 22, height: 15, 'font-size': 20, }}>
        <span>
          1er
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 353, top: 223, width: 524, height: 19, 'font-size': 20, }}>
        <span>
          EXEMPLAIRE À ADRESSER AU SERVICE DES IMPÔTS
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 87, top: 259, width: 1031, height: 17, 'font-size': 19, }}>
        <span>
          Les déclarations 2069-A-SD, 2069-A-1-SD et 2069-A-2-SD peuvent être télé-déclarées en utilisant la procédure EDI-TDFC.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 131, top: 279, width: 943, height: 18, 'font-size': 19, }}>
        <span>
          Pour plus d’information sur la télédéclaration, veui llez consulter le portail fiscal www.impots.gouv.fr , rubrique
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 72, top: 300, width: 1060, height: 18, 'font-size': 19, }}>
        <span>
          « Professionnels ». Dans ces conditions, il n&#39;est pl us nécessaire d&#39;adresser une copie au Ministère cha rgé de la Recherche,
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 407, top: 321, width: 390, height: 14, 'font-size': 19, }}>
        <span>
          les données lui étant directement transmises.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 291, top: 341, width: 623, height: 19, 'font-size': 19, }}>
        <span>
          Elles seront obligatoirement télé-déclarées à compte r du 1er janvier 2020.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 74, top: 362, width: 1057, height: 18, 'font-size': 19, }}>
        <span>
          Le modèle de dossier justificatif du CIR est à la d isposition des entreprises sur le site du Ministère chargé de la Recherche.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 225, top: 383, width: 754, height: 18, 'font-size': 19, }}>
        <span>
          Ce dossier est à remplir chaque année par les entreprises pour justifier leur déclaration.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 267, top: 412, width: 158, height: 15, 'font-size': 20, }}>
        <span>
          Exercice ouvert le
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 432, top: 410, width: 4, height: 10, 'font-size': 12, }}>
        <span>
          1
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 637, top: 411, width: 60, height: 16, 'font-size': 20, }}>
        <span>
          Clos le
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 78, top: 524, width: 163, height: 16, 'font-size': 20, }}>
        <span>
          Cachet du Service
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 294, top: 460, width: 300, height: 20, 'font-size': 20, }}>
        <span>
          Nom et prénoms ou dénomination
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 332, top: 483, width: 224, height: 20, 'font-size': 20, }}>
        <span>
          et adresse de l&#39;entreprise
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 647, top: 460, width: 218, height: 20, 'font-size': 20, }}>
        <span>
          N° SIREN de l&#39;entreprise
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 922, top: 461, width: 108, height: 16, 'font-size': 20, }}>
        <span>
          Code NACE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1082, top: 469, width: 56, height: 3, 'font-size': 20, }}>
        <span>
          - - - - -
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 938, top: 500, width: 74, height: 16, 'font-size': 20, }}>
        <span>
          Activités
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 930, top: 523, width: 91, height: 20, 'font-size': 20, }}>
        <span>
          (cf. notice)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 637, top: 561, width: 379, height: 20, 'font-size': 20, }}>
        <span>
          (ancienne adresse en cas de changement)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 642, width: 10, height: 11, 'font-size': 17, }}>
        <span>

          <br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 85, top: 638, width: 745, height: 20, 'font-size': 20, }}>
        <span>
          Société bénéficiant du régime fiscal des groupes de sociétés (article 223 A du CGI)*
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 900, top: 688, width: 27, height: 16, 'font-size': 20, }}>
        <span>
          CX
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 676, width: 257, height: 16, 'font-size': 20, }}>
        <span>
          N° SIREN de la société mère
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 714, width: 698, height: 20, 'font-size': 20, }}>
        <span>
          Nombre de sociétés du groupe (y compris la société mère) pour lesquelles une
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 738, width: 393, height: 19, 'font-size': 20, }}>
        <span>
          déclaration 2069-A-SD est ou sera déposée,
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 776, width: 780, height: 20, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt du groupe (à compléter exclusivement dans le cadre du dépôt
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 799, width: 755, height: 20, 'font-size': 20, }}>
        <span>
          de la déclaration de la société mère, renseignement non demandé à une société fille)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 900, top: 788, width: 27, height: 15, 'font-size': 20, }}>
        <span>
          DX
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 848, width: 715, height: 20, 'font-size': 20, }}>
        <span>
          Entreprises ayant engagé pour la 1ère fois des dépe nses de recherche en 2018*
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 900, top: 848, width: 26, height: 16, 'font-size': 20, }}>
        <span>
          AZ
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 900, width: 10, height: 10, 'font-size': 17, }}>
        <span>

          <br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 85, top: 896, width: 191, height: 19, 'font-size': 20, }}>
        <span>
          Entreprises nouvelles
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 919, width: 145, height: 16, 'font-size': 20, }}>
        <span>
          créées en 2018*
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 325, top: 907, width: 24, height: 16, 'font-size': 20, }}>
        <span>
          BZ
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 466, top: 891, width: 405, height: 20, 'font-size': 20, }}>
        <span>
          Préciser la date de début d’activité (cf. notice)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 958, width: 10, height: 10, 'font-size': 17, }}>
        <span>

          <br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 85, top: 954, width: 118, height: 16, 'font-size': 20, }}>
        <span>
          PME au sens
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 977, width: 145, height: 16, 'font-size': 20, }}>
        <span>
          communautaire*
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 325, top: 966, width: 24, height: 15, 'font-size': 20, }}>
        <span>
          KZ
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 466, top: 948, width: 569, height: 20, 'font-size': 20, }}>
        <span>
          Préciser si entreprise autonome, partenaire et/ou l iée (cf. notice)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1027, width: 10, height: 11, 'font-size': 17, }}>
        <span>

          <br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 85, top: 1023, width: 177, height: 16, 'font-size': 20, }}>
        <span>
          Chiffre d’affaires HT
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 324, top: 1024, width: 25, height: 15, 'font-size': 20, }}>
        <span>
          DZ
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1092, width: 10, height: 10, 'font-size': 17, }}>
        <span>

          <br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 85, top: 1088, width: 173, height: 15, 'font-size': 20, }}>
        <span>
          Nombre de salariés
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 323, top: 1087, width: 26, height: 16, 'font-size': 20, }}>
        <span>
          CZ
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 466, top: 1068, width: 11, height: 11, 'font-size': 17, }}>
        <span>

          <br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 484, top: 1064, width: 98, height: 16, 'font-size': 20, }}>
        <span>
          Nombre de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 466, top: 1088, width: 121, height: 15, 'font-size': 20, }}>
        <span>
          chercheurs et
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 465, top: 1111, width: 100, height: 15, 'font-size': 20, }}>
        <span>
          techniciens
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 611, top: 1088, width: 24, height: 15, 'font-size': 20, }}>
        <span>
          EZ
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 740, top: 1080, width: 11, height: 10, 'font-size': 17, }}>
        <span>

          <br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 758, top: 1076, width: 278, height: 20, 'font-size': 20, }}>
        <span>
          Nombre de « jeunes docteurs »
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 740, top: 1099, width: 288, height: 20, 'font-size': 20, }}>
        <span>
          (si dépenses déclarées ligne I-5)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1064, top: 1088, width: 23, height: 15, 'font-size': 20, }}>
        <span>
          FZ
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1147, width: 10, height: 10, 'font-size': 17, }}>
        <span>

          <br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 85, top: 1143, width: 203, height: 20, 'font-size': 20, }}>
        <span>
          Sociétés de personnes
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1166, width: 232, height: 20, 'font-size': 20, }}>
        <span>
          n’ayant pas opté pour l’IS*
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 329, top: 1154, width: 16, height: 16, 'font-size': 20, }}>
        <span>
          IZ
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 466, top: 1147, width: 11, height: 10, 'font-size': 17, }}>
        <span>

          <br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 483, top: 1143, width: 539, height: 20, 'font-size': 20, }}>
        <span>
          Société bénéficiant du régime des JEI (article 44 sexies A du
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 466, top: 1166, width: 49, height: 20, 'font-size': 20, }}>
        <span>
          CGI)*
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1062, top: 1154, width: 27, height: 16, 'font-size': 20, }}>
        <span>
          GZ
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1198, width: 233, height: 18, 'font-size': 19, }}>
        <span>
          * Cocher la case correspondante
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1234, width: 39, height: 17, 'font-size': 23, }}>
        <span>
          I - D
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 109, top: 1234, width: 557, height: 18, 'font-size': 18, }}>
        <span>
          ÉPENSES DE RECHERCHE OUVRANT DROIT À CRÉDIT D’IMPÔT
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 971, top: 1235, width: 116, height: 16, 'font-size': 20, }}>
        <span>
          ANNÉE CIVILE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1093, top: 1235, width: 44, height: 16, 'font-size': 20, }}>
        <span>
          2018
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1260, width: 668, height: 16, 'font-size': 20, }}>
        <span>
          Dotations aux amortissements des immobilisations af fectées à la recherche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 979, top: 1260, width: 6, height: 16, 'font-size': 20, }}>
        <span>
          1
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1284, width: 578, height: 20, 'font-size': 20, }}>
        <span>
          Dotations aux amortissements pour les immobilisations sinistrées
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 978, top: 1284, width: 10, height: 16, 'font-size': 20, }}>
        <span>
          2
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1308, width: 684, height: 20, 'font-size': 20, }}>
        <span>
          Dépenses de personnel relatives aux chercheurs et techniciens de recherche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1331, width: 255, height: 20, 'font-size': 20, }}>
        <span>
          (sauf dépenses lignes 4 et 5)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 978, top: 1320, width: 10, height: 16, 'font-size': 20, }}>
        <span>
          3
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1355, width: 877, height: 20, 'font-size': 20, }}>
        <span>
          Rémunérations et justes prix au profit des salariés auteurs d’une invention résultant d’opérations
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1379, width: 116, height: 15, 'font-size': 20, }}>
        <span>
          de recherche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 977, top: 1367, width: 11, height: 16, 'font-size': 20, }}>
        <span>
          4
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1403, width: 818, height: 20, 'font-size': 20, }}>
        <span>
          Dépenses de personnel relatives aux jeunes docteurs (à indiquer pour le double de leur montant
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1425, width: 554, height: 18, 'font-size': 19, }}>
        <span>
          pour les vingt-quatre premiers mois suivant leur premier recrutement)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 978, top: 1413, width: 10, height: 16, 'font-size': 20, }}>
        <span>
          5
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1447, width: 550, height: 20, 'font-size': 20, }}>
        <span>
          Autres dépenses de fonctionnement (hors frais de collection) :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1470, width: 485, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 1 × 75 %) + [(ligne 3 + ligne 4) x 50 %)] + ligne 5
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 978, top: 1459, width: 10, height: 16, 'font-size': 20, }}>
        <span>
          6
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1532, width: 4, height: 9, 'font-size': 12, }}>
        <span>
          1
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 72, top: 1536, width: 1084, height: 16, 'font-size': 17, }}>
        <span>
          Le crédit d’impôt est calculé par référence aux dép enses exposées au cours de l&#39;année civile. En cas de clôture d’exercice en cours d’année, le montant
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1556, width: 908, height: 16, 'font-size': 17, }}>
        <span>
          du crédit d’impôt est calculé en prenant en compte les dépenses éligibles exposées au titre de l’année d’ouverture de l’exercice.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 145, top: 67, width: 58, height: 27, 'font-size': 29, }}>
        <span>
          cerfa
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 131, top: 113, width: 86, height: 13, 'font-size': 17, }}>
        <span>
          N° 11081*21
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 86, top: 131, width: 176, height: 14, 'font-size': 14, }}>
        <span>
          (article 244 quater B du CGI)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 997, top: 66, width: 121, height: 15, 'font-size': 20, }}>
        <span>
          N° 2069-A-SD
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 978, top: 100, width: 159, height: 19, 'font-size': 20, }}>
        <span>
          Dépenses engagées
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 992, top: 123, width: 132, height: 15, 'font-size': 20, }}>
        <span>
          au titre de 2018
<br />
        </span>
      </div>
    </div>

    <div data-type="pdf-page-annots" className="pdf-page-annots" >
      <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="FR" data-field-id="9956568" data-annot-id="9267680" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="FS" data-field-id="9956840" data-annot-id="8534416" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="AC" data-field-id="9957176" data-annot-id="8534608" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="AF" data-field-id="9957480" data-annot-id="9271328" type="number" />

      <Field component="input" className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="AJ" data-field-id="9957816" data-annot-id="9271520" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="AM" data-field-id="9958120" data-annot-id="9271712" type="number" />

      <Field component="input" className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="AN" data-field-id="9958456" data-annot-id="9271904" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="AP" data-field-id="9958792" data-annot-id="9272096" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="FT" data-field-id="9959128" data-annot-id="9272288" type="text" />


      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="AT" data-field-id="9959608" data-annot-id="9272480" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="AU" data-field-id="9959944" data-annot-id="9272672" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="AV" data-field-id="9960280" data-annot-id="9272864" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="AW" data-field-id="9960616" data-annot-id="9273056" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="AX" data-field-id="9960952" data-annot-id="9273248" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="AY" data-field-id="9961288" data-annot-id="9273440" type="number" disabled />


      <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="AH" data-field-id="9961624" data-annot-id="9273632" value="Yes" type="checkbox" data-default-value="Off" />

      <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="AD" data-field-id="9961960" data-annot-id="9273824" value="Yes" type="checkbox" data-default-value="Off" />

      <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="AB" data-field-id="9962440" data-annot-id="9274288" value="Yes" type="checkbox" data-default-value="Off" />

      <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="AG" data-field-id="9962776" data-annot-id="9274480" value="Yes" type="checkbox" data-default-value="Off" />

      <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="AK" data-field-id="9963112" data-annot-id="9274672" value="Yes" type="checkbox" data-default-value="Off" />

      <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="AL" data-field-id="9963448" data-annot-id="9274864" value="Yes" type="checkbox" data-default-value="Off" />

      <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="AQ" data-field-id="9963784" data-annot-id="9275056" value="Yes" type="checkbox" data-default-value="Off" />

      <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="AS" data-field-id="9964120" data-annot-id="9275248" value="Yes" type="checkbox" data-default-value="Off" />

      <Field component="input" className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="AAT" data-field-id="10433512" data-annot-id="9275440" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="AAA" data-field-id="10436424" data-annot-id="9275632" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="AAD" data-field-id="10436760" data-annot-id="9275824" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="AAE" data-field-id="10437096" data-annot-id="9276016" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="AAF" data-field-id="10437432" data-annot-id="9276208" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="AAG" data-field-id="10437768" data-annot-id="9276400" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="AAH" data-field-id="10438104" data-annot-id="9276592" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="AAI" data-field-id="10438440" data-annot-id="9276784" type="text" />

      <Field component={ PdfDateInputFormat } className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="DEB_EX" data-field-id="10438776" data-annot-id="9276976" type="text" data_format="DD MM YYYY" disabled/>

      <Field component={ PdfDateInputFormat } className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="FIN_EX" data-field-id="10439112" data-annot-id="9277168" type="text" data_format="DD MM YYYY" disabled/>

    </div>
  </div>

  {/* <script>
    pdfixUpdatePage(document.getElementById('pdf-page-0'));</script> */}

</div>

<div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.414167" className="pdf-page ">
  <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
    <div data-type="pdf-page-text" className="pdf-page-text">
      <div className="pdf-obj-fixed" style={{ left: 68, top: 37, width: 458, height: 20, 'font-size': 20, }}>
        <span>
          Montant total des dépenses de fonctionnement :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 60, width: 480, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 1 + ligne 2 + ligne 3 + ligne 4 + ligne 5 + ligne 6)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 979, top: 49, width: 10, height: 15, 'font-size': 20, }}>
        <span>
          7
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 84, width: 674, height: 20, 'font-size': 20, }}>
        <span>
          Prise et maintenance de brevets et de certificats d ’obtention végétale (COV)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 979, top: 84, width: 10, height: 16, 'font-size': 20, }}>
        <span>
          8
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 108, width: 680, height: 20, 'font-size': 20, }}>
        <span>
          Dépenses de défense de brevets et de certificats d’obtention végétale (COV)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 979, top: 108, width: 10, height: 16, 'font-size': 20, }}>
        <span>
          9
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 132, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Dotations aux amortissements de brevets acquis en vue de la recherche et du développement
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 155, width: 502, height: 20, 'font-size': 20, }}>
        <span>
          expérimental et de certificats d’obtention végétale (COV)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 144, width: 20, height: 16, 'font-size': 20, }}>
        <span>
          10
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 179, width: 761, height: 20, 'font-size': 20, }}>
        <span>
          Dépenses liées à la normalisation (à indiquer pour la moitié de leur montant cf. notice)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 180, width: 17, height: 15, 'font-size': 20, }}>
        <span>
          11
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 204, width: 879, height: 19, 'font-size': 20, }}>
        <span>
          Primes et cotisations ou part des primes et cotisat ions afférentes à des contrats d’assurance de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 226, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          protection juridique prévoyant la prise en charge des dépenses exposées dans le cadre de litiges
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 250, width: 879, height: 19, 'font-size': 20, }}>
        <span>
          portant sur un brevet ou un certificat d’obtention végétale dont l’entreprise est titulaire dans la limite
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 273, width: 106, height: 16, 'font-size': 20, }}>
        <span>
          de 60 000 €
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 238, width: 20, height: 16, 'font-size': 20, }}>
        <span>
          12
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 297, width: 533, height: 20, 'font-size': 20, }}>
        <span>
          Dépenses de veille technologique dans la limite de 60 000 €
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 297, width: 20, height: 16, 'font-size': 20, }}>
        <span>
          13
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 321, width: 634, height: 20, 'font-size': 20, }}>
        <span>
          Montant total des dépenses de recherche réalisées p ar l’entreprise
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 344, width: 638, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 7 + ligne 8 + ligne 9 + ligne 10 + ligne 11 + ligne 12 + ligne 13)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 332, width: 20, height: 16, 'font-size': 20, }}>
        <span>
          14
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 380, width: 266, height: 16, 'font-size': 20, }}>
        <span>
          DÉPENSES DE SOUS -TRAITANCE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 404, width: 618, height: 20, 'font-size': 20, }}>
        <span>
          (joindre la liste des organismes à partir du formulaire n° 2069-A-2-SD)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 392, width: 115, height: 16, 'font-size': 20, }}>
        <span>
          ANNÉE CIVILE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1094, top: 392, width: 44, height: 16, 'font-size': 20, }}>
        <span>
          2018
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 428, width: 186, height: 16, 'font-size': 20, }}>
        <span>
          ORGANISMES PUBLICS
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 452, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          Opérations confiées à des organismes de recherche publics, à des établissements d’enseignement
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 475, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          supérieur délivrant un diplôme conférant un grade d e master, à des fondations de coopération
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 498, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          scientifique agréées, à des établissements publics de coopération scientifique, à des fondations
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 521, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          reconnues d’utilité publique du secteur de la reche rche agréées, à certaines associations régies
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 544, width: 368, height: 20, 'font-size': 20, }}>
        <span>
          par la loi de 1901 et sociétés de capitaux
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 437, top: 543, width: 11, height: 20, 'font-size': 12, }}>
        <span>
          2,
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 456, top: 544, width: 492, height: 20, 'font-size': 20, }}>
        <span>
          aux instituts techniques agricoles ou agro-industriels et
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 567, width: 879, height: 19, 'font-size': 20, }}>
        <span>
          à leurs structures nationales de coordination, à des communautés d’universités et établissements,
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 590, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          à des stations ou fermes expérimentales dans le secteur de la recherche scientifique et technique
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 614, width: 747, height: 19, 'font-size': 20, }}>
        <span>
          agricole ayant pour membre une chambre d&#39;agricultur e départementale ou régionale
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 815, top: 612, width: 11, height: 20, 'font-size': 12, }}>
        <span>
          3,
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 834, top: 614, width: 113, height: 15, 'font-size': 20, }}>
        <span>
          avec un lien
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 636, width: 156, height: 20, 'font-size': 20, }}>
        <span>
          de dépendance :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 847, top: 660, width: 100, height: 15, 'font-size': 20, }}>
        <span>
          en France :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 969, top: 556, width: 30, height: 16, 'font-size': 20, }}>
        <span>
          15a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 831, top: 688, width: 169, height: 20, 'font-size': 20, }}>
        <span>
          à l’étranger : 15b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 930, top: 687, width: 7, height: 9, 'font-size': 12, }}>
        <span>
          4
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 716, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          Opérations confiées à des organismes de recherche publics, à des établissements d’enseignement
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 739, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          supérieur délivrant un diplôme conférant un grade d e master, à des fondations de coopération
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 762, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          scientifique agréées, à des établissements publics de coopération scientifique, à des fondations
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 786, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          reconnues d’utilité publique du secteur de la reche rche agréées, à certaines associations régies
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 809, width: 368, height: 19, 'font-size': 20, }}>
        <span>
          par la loi de 1901 et sociétés de capitaux
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 437, top: 807, width: 11, height: 20, 'font-size': 12, }}>
        <span>
          2,
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 456, top: 809, width: 492, height: 20, 'font-size': 20, }}>
        <span>
          aux instituts techniques agricoles ou agro-industriels et
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 832, width: 879, height: 18, 'font-size': 20, }}>
        <span>
          à leurs structures nationales de coordination, à de s communautés d’universités et établissements,
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 855, width: 880, height: 19, 'font-size': 20, }}>
        <span>
          à des stations ou fermes expérimentales dans le secteur de la recherche scientifique et technique
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 878, width: 747, height: 20, 'font-size': 20, }}>
        <span>
          agricole ayant pour membre une chambre d&#39;agricultur e départementale ou régionale
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 815, top: 877, width: 11, height: 19, 'font-size': 12, }}>
        <span>
          3,
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 833, top: 878, width: 115, height: 16, 'font-size': 20, }}>
        <span>
          sans lien de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 901, width: 399, height: 20, 'font-size': 20, }}>
        <span>
          dépendance (indiquer le double du montant)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 847, top: 924, width: 100, height: 16, 'font-size': 20, }}>
        <span>
          en France :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 969, top: 820, width: 30, height: 16, 'font-size': 20, }}>
        <span>
          16a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 831, top: 948, width: 169, height: 20, 'font-size': 20, }}>
        <span>
          à l’étranger : 16b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 930, top: 947, width: 7, height: 10, 'font-size': 12, }}>
        <span>
          4
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 972, width: 881, height: 20, 'font-size': 20, }}>
        <span>
          Total des opérations confiées aux organismes de recherche publics mentionnés aux lignes
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 995, width: 511, height: 20, 'font-size': 20, }}>
        <span>
          15a à 16b : (ligne 15a + ligne 15b + ligne 16a + ligne 16b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 984, width: 20, height: 15, 'font-size': 20, }}>
        <span>
          17
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1019, width: 176, height: 16, 'font-size': 20, }}>
        <span>
          ORGANISMES PRIVÉS
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1043, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          Opérations confiées à des organismes de recherche privés ou experts scientifiques ou techniques
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1066, width: 332, height: 20, 'font-size': 20, }}>
        <span>
          agréés avec un lien de dépendance
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 847, top: 1090, width: 100, height: 16, 'font-size': 20, }}>
        <span>
          en France :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 969, top: 1067, width: 30, height: 15, 'font-size': 20, }}>
        <span>
          18a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 829, top: 1114, width: 170, height: 20, 'font-size': 20, }}>
        <span>
          à l’étranger : 18b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 929, top: 1112, width: 7, height: 10, 'font-size': 12, }}>
        <span>
          4
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1138, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          Opérations confiées à des organismes de recherche privés ou experts scientifiques ou techniques
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1161, width: 303, height: 19, 'font-size': 20, }}>
        <span>
          agréés sans lien de dépendance
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 847, top: 1184, width: 100, height: 16, 'font-size': 20, }}>
        <span>
          en France :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 969, top: 1161, width: 30, height: 16, 'font-size': 20, }}>
        <span>
          19a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 831, top: 1210, width: 169, height: 20, 'font-size': 20, }}>
        <span>
          à l’étranger : 19b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 930, top: 1210, width: 7, height: 9, 'font-size': 12, }}>
        <span>
          4
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1237, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          Total des opérations confiées à des organismes de recherche privés ou experts scientifiques
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1260, width: 615, height: 20, 'font-size': 20, }}>
        <span>
          ou techniques agréés : (ligne 18a + ligne 18b + ligne 19a + ligne 19b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1249, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          20
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1285, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Plafonnement des opérations de sous-traitance confiées à des organismes de recherche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1308, width: 504, height: 20, 'font-size': 20, }}>
        <span>
          privés ou experts scientifiques ou techniques agréés
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1331, width: 751, height: 20, 'font-size': 20, }}>
        <span>
          Si ligne 20 inférieure à [(ligne 14 + ligne 17) x 3], reporter le montant indiqué ligne 20
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1354, width: 759, height: 20, 'font-size': 20, }}>
        <span>
          Si ligne 20 supérieure à [(ligne 14 + ligne 17) x 3 ], reporter le résultat du calcul précité
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1319, width: 18, height: 16, 'font-size': 20, }}>
        <span>
          21
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1379, width: 361, height: 15, 'font-size': 20, }}>
        <span>
          TOTAL DES DÉPENSES DE SOUS -TRAITANCE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1408, width: 561, height: 20, 'font-size': 20, }}>
        <span>
          Total des opérations de sous-traitance : (ligne 17 + ligne 21)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1409, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          22
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1472, width: 6, height: 9, 'font-size': 12, }}>
        <span>
          2
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 71, top: 1476, width: 1084, height: 17, 'font-size': 17, }}>
        <span>
          Associations ayant pour fondateur et membre un org anisme de recherche public ou un établissement d’enseignement supérieur délivrant un diplôme
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1496, width: 1095, height: 16, 'font-size': 17, }}>
        <span>
          conférant un grade de master ; sociétés de capitaux dont le capital ou les droits de vote sont détenus pour plus de 50 % par un organisme de recherche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1514, width: 1093, height: 17, 'font-size': 17, }}>
        <span>
          public ou un établissement d’enseignement supérieur délivrant un diplôme conférant un grade de master. Se reporter à la notice pour connaître
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1533, width: 265, height: 16, 'font-size': 17, }}>
        <span>
          l’ensemble des conditions d’éligibilité.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1551, width: 6, height: 9, 'font-size': 12, }}>
        <span>
          3
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 73, top: 1555, width: 1081, height: 16, 'font-size': 17, }}>
        <span>
          La prise en compte des opérations confiées à des st ations ou fermes expérimentales dans le secteur de la recherche scientifique et technique agricole
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1574, width: 1093, height: 17, 'font-size': 17, }}>
        <span>
          ayant pour membre une chambre d&#39;agriculture départementale ou régionale dans le calcul du crédit d&#39;impôt ne s&#39;applique qu&#39;aux sommes venant en
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1593, width: 828, height: 16, 'font-size': 17, }}>
        <span>
          déduction de l&#39;impôt dû (article 103 de la loi n° 2016-1918 du 29 décembre 2016 de finances rectificative pour 2016).
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1611, width: 6, height: 9, 'font-size': 12, }}>
        <span>
          4
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 74, top: 1612, width: 1080, height: 19, 'font-size': 17, }}>
        <span>
          Les prestataires publics ou privés peuvent être implantés en France, dans un État membre de l’Union Européenne ou de l’Espace économique
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1635, width: 356, height: 16, 'font-size': 17, }}>
        <span>
          européen (UE, Norvège, Islande et Liechtenstein).
<br />
        </span>
      </div>
    </div>

    <div data-type="pdf-page-annots" className="pdf-page-annots" >
      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="AZ" data-field-id="9964552" data-annot-id="7862848" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="BA" data-field-id="9967624" data-annot-id="8639600" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="BB" data-field-id="9970776" data-annot-id="8639824" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="BC" data-field-id="9973928" data-annot-id="8640016" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="BD" data-field-id="9977080" data-annot-id="8640208" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="BE" data-field-id="9980232" data-annot-id="8640400" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="BF" data-field-id="9983384" data-annot-id="8640592" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="BJ" data-field-id="9986536" data-annot-id="8643312" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="BK" data-field-id="9989688" data-annot-id="8643504" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="BL" data-field-id="9992840" data-annot-id="8643840" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="BM" data-field-id="9996520" data-annot-id="8644032" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="BN" data-field-id="9999352" data-annot-id="8644224" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="BP" data-field-id="10002504" data-annot-id="8644416" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="BQ" data-field-id="10005656" data-annot-id="8644608" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="BR" data-field-id="10008808" data-annot-id="8644800" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="BS" data-field-id="10423736" data-annot-id="8644992" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="BT" data-field-id="10426904" data-annot-id="8645184" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="BU" data-field-id="10011960" data-annot-id="8645648" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="BV" data-field-id="10015112" data-annot-id="8645840" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="BX" data-field-id="10018264" data-annot-id="8646032" type="number" disabled/>

    </div>
  </div>

  <script>
    pdfixUpdatePage(document.getElementById('pdf-page-1'));</script>

</div>

<div data-type="pdf-page" id="pdf-page-2" data-page-num="2" data-ratio="1.414167" className="pdf-page ">
  <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-2 ">
    <div data-type="pdf-page-text" className="pdf-page-text">
      <div className="pdf-obj-fixed" style={{ left: 68, top: 37, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Plafonnement des opérations de sous-traitance confi ées à des organismes avec un lien de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 60, width: 127, height: 20, 'font-size': 20, }}>
        <span>
          dépendance :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 83, width: 877, height: 20, 'font-size': 20, }}>
        <span>
          Si la somme des lignes 15a, 15b, 18a et 18b (dans la limite du montant figurant ligne 22) n’excède
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 106, width: 435, height: 20, 'font-size': 20, }}>
        <span>
          pas 2 000 000 €, reporter ce montant en ligne 23
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 129, width: 874, height: 20, 'font-size': 20, }}>
        <span>
          Si la somme des lignes 15a, 15b, 18a et 18b excède 2 000 000 €, reporter 2 000 000 € en ligne 23
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 152, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          Pour la somme des lignes 18a et 18b, son montant ne doit pas excéder la limite du montant
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 175, width: 141, height: 20, 'font-size': 20, }}>
        <span>
          figurant ligne 21
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 106, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          23
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 199, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          Montant plafonné des opérations de sous-traitance confiées à des organismes sans lien de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 223, width: 116, height: 19, 'font-size': 20, }}>
        <span>
          dépendance
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 252, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 16a + ligne 16b + ligne 19a + ligne 19b à pr endre en compte dans la limite du montant
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 275, width: 258, height: 20, 'font-size': 20, }}>
        <span>
          suivant : (ligne 22 - ligne 23))
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 237, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          24
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 299, width: 517, height: 20, 'font-size': 20, }}>
        <span>
          Plafonnement général des dépenses de sous-traitance
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 328, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          - Sont complétées les seules lignes 15a et/ou 15b et/ou 18a et/ou 18b (lignes 19a, b et 16a, b non
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 351, width: 390, height: 20, 'font-size': 20, }}>
        <span>
          complétées) : reporter 2 000 000 € ligne 25
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 374, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          - Sont complétées (les lignes 15a et/ou 15b et/ou 18a et/ou 18b) + (lignes 19a ou 19b)(lignes
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 397, width: 493, height: 20, 'font-size': 20, }}>
        <span>
          16a, b non complétées) : reporter 10 000 000 € ligne 25
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 420, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          - Sont complétées [(les lignes 15a et/ou 15b et/ou 18a et/ou 18b) + (lignes 19a et/ou 19b)] +
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 443, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          (lignes 16a et/ou 16b) : reporter [10 000 000 € + (ligne 16a + ligne 16b dans la limite de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 466, width: 195, height: 20, 'font-size': 20, }}>
        <span>
          2 000 000 €)] ligne 25
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 383, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          25
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 490, width: 641, height: 20, 'font-size': 20, }}>
        <span>
          Montant total des dépenses de sous-traitance après plafonnements
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 513, width: 825, height: 20, 'font-size': 20, }}>
        <span>
          Si la somme des lignes 23 et 24 n’excède pas la ligne 25 : reporter cette somme à la ligne 26
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 536, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          Si la somme des lignes 23 et 24 est supérieure à ligne 25 : reporter le montant indiqué ligne 25 à
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 560, width: 91, height: 20, 'font-size': 20, }}>
        <span>
          la ligne 26
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 525, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          26
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 598, width: 15, height: 16, 'font-size': 20, }}>
        <span>
          M
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 83, top: 598, width: 390, height: 16, 'font-size': 17, }}>
        <span>
          ONTANT TOTAL DES DÉPENSES DE RECHERCHE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 598, width: 115, height: 16, 'font-size': 20, }}>
        <span>
          ANNÉE CIVILE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1093, top: 598, width: 44, height: 16, 'font-size': 20, }}>
        <span>
          2018
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 626, width: 506, height: 20, 'font-size': 20, }}>
        <span>
          Montant des dépenses de recherche (ligne 14 + ligne 26)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 626, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          27
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 656, width: 517, height: 20, 'font-size': 20, }}>
        <span>
          Montant des subventions publiques remboursables ou non
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 585, top: 655, width: 7, height: 10, 'font-size': 12, }}>
        <span>
          5
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 656, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          28a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 685, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Pour les sous-traitants, le montant des sommes enca issées au titre des opérations de recherche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 708, width: 211, height: 20, 'font-size': 20, }}>
        <span>
          qui leur ont été confiées
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 697, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          28b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 732, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          Montant des dépenses exposées auprès de tiers au ti tre de prestations de conseil pour l’octroi du
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 755, width: 227, height: 20, 'font-size': 20, }}>
        <span>
          bénéfice du crédit d’impôt
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 294, top: 754, width: 7, height: 10, 'font-size': 12, }}>
        <span>
          6
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 744, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          29
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 784, width: 496, height: 20, 'font-size': 20, }}>
        <span>
          Montant des remboursements de subventions publiques
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 564, top: 783, width: 7, height: 10, 'font-size': 12, }}>
        <span>
          7
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 784, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          30
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 813, width: 428, height: 20, 'font-size': 20, }}>
        <span>
          Montant net total des dépenses de recherche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 836, width: 485, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 27 - ligne 28a - ligne 28b - ligne 29 + lign e 30)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 825, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          31a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 860, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant net des dépenses de recherche exposées dans des exploitations situées
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 883, width: 130, height: 16, 'font-size': 20, }}>
        <span>
          dans un DOM
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 872, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          31b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 920, width: 607, height: 18, 'font-size': 23, }}>
        <span>
          II - DÉPENSES DE COLLECTION OUVRANT DROIT À CRÉDIT D’IMPÔT
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 921, width: 115, height: 16, 'font-size': 20, }}>
        <span>
          ANNÉE CIVILE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1093, top: 921, width: 44, height: 16, 'font-size': 20, }}>
        <span>
          2018
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 946, width: 159, height: 16, 'font-size': 20, }}>
        <span>
          Frais de collection
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 946, width: 20, height: 16, 'font-size': 20, }}>
        <span>
          32
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 970, width: 597, height: 16, 'font-size': 20, }}>
        <span>
          Frais de défense des dessins et modèles dans la lim ite de 60 000 €
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 970, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          33
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 994, width: 473, height: 20, 'font-size': 20, }}>
        <span>
          Total des dépenses de collection (ligne 32 + ligne 33)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 995, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          34
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1027, width: 522, height: 19, 'font-size': 20, }}>
        <span>
          Montant des subventions publiques remboursables ou non
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 591, top: 1026, width: 6, height: 9, 'font-size': 12, }}>
        <span>
          5
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1026, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          35
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1058, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          Montant des dépenses exposées auprès de tiers au ti tre de prestations de conseil pour l’octroi du
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1081, width: 227, height: 20, 'font-size': 20, }}>
        <span>
          bénéfice du crédit d’impôt
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 294, top: 1081, width: 7, height: 9, 'font-size': 12, }}>
        <span>
          6
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1070, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          36
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1114, width: 496, height: 19, 'font-size': 20, }}>
        <span>
          Montant des remboursements de subventions publiques
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 564, top: 1113, width: 7, height: 9, 'font-size': 12, }}>
        <span>
          7
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1114, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          37
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1154, width: 803, height: 19, 'font-size': 20, }}>
        <span>
          Montant net total des dépenses de collection (ligne 34 - ligne 35 - ligne 36 + ligne 37)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1154, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          38a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1186, width: 880, height: 19, 'font-size': 20, }}>
        <span>
          Dont montant net des dépenses de collection exposée s dans des exploitations situées dans
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1209, width: 77, height: 16, 'font-size': 20, }}>
        <span>
          un DOM
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1197, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          38b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1244, width: 744, height: 19, 'font-size': 20, }}>
        <span>
          MONTANT NET TOTAL DES DÉPENSES DE RECHERCHE ET DE COLLECTION
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1270, width: 206, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 31a + ligne 38a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1259, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          39a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1296, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant net des dépenses de recherche et de collection exposées dans des
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1319, width: 548, height: 20, 'font-size': 20, }}>
        <span>
          exploitations situées dans un DOM (ligne 31b + lign e 38b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1308, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          39b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1358, width: 52, height: 18, 'font-size': 23, }}>
        <span>
          III - C
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 119, top: 1359, width: 805, height: 17, 'font-size': 18, }}>
        <span>
          ALCUL DU CRÉDIT D’IMPÔT AU TITRE DES DÉPENSES DE RECHERCHE ET DE COLLECTION
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 222, top: 1400, width: 41, height: 17, 'font-size': 23, }}>
        <span>
          A. L
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 264, top: 1400, width: 369, height: 18, 'font-size': 18, }}>
        <span>
          ORSQUE LES DÉPENSES PORTÉES LIGNE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 640, top: 1400, width: 36, height: 17, 'font-size': 23, }}>
        <span>
          39a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 684, top: 1400, width: 155, height: 17, 'font-size': 18, }}>
        <span>
          N’EXCÈDENT PAS
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 847, top: 1400, width: 141, height: 17, 'font-size': 23, }}>
        <span>
          100 000 000 €
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1447, width: 620, height: 16, 'font-size': 20, }}>
        <span>
          DÉTERMINATION DU CRÉDIT D’IMPÔT POUR LES DÉPENSES DE RECHERCHE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1491, width: 6, height: 9, 'font-size': 12, }}>
        <span>
          5
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 70, top: 1495, width: 1083, height: 16, 'font-size': 17, }}>
        <span>
          Les subventions publiques, remboursables ou non, doivent être déduites de la base de calcul du crédit d’impôt calculé au titre de l&#39;année ou des années
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1514, width: 1094, height: 17, 'font-size': 17, }}>
        <span>
          au cours de laquelle ou desquelles les dépenses éli gibles, que ces avances ou subventions ont vocation à couvrir, sont exposées, conformément au III de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1533, width: 449, height: 16, 'font-size': 17, }}>
        <span>
          l&#39;article 244 quater B du CGI. (BOI-BIC-RICI-10-10-30-20 § 10).
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1551, width: 6, height: 9, 'font-size': 12, }}>
        <span>
          6
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 76, top: 1555, width: 1077, height: 16, 'font-size': 17, }}>
        <span>
          Le montant des dépenses à déduire correspond soit au montant total des rémunérations allouées en contrepartie de ces prestations fixées
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1574, width: 1093, height: 16, 'font-size': 17, }}>
        <span>
          proportionnellement au montant du crédit d’impôt ob tenu par l’entreprise, soit le montant des dépenses exposées autres que les rémunérations
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1593, width: 1087, height: 16, 'font-size': 17, }}>
        <span>
          proportionnelles excédant 15 000 € hors taxes ou 5 % du montant des dépenses éligibles au crédit d’impôt minoré des subventions publiques (cf. notice).
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1611, width: 6, height: 9, 'font-size': 12, }}>
        <span>
          7
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 73, top: 1615, width: 1081, height: 16, 'font-size': 17, }}>
        <span>
          Le montant des remboursements de subventions publi ques doit être multiplié par le rapport existant entre le taux du crédit d’impôt de l’année où la
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1635, width: 917, height: 16, 'font-size': 17, }}>
        <span>
          subvention remboursable a été déduite et le taux du crédit d’impôt de l’année où elle est remboursée partiellement ou totalement.
<br />
        </span>
      </div>
    </div>

    <div data-type="pdf-page-annots" className="pdf-page-annots" >
      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="BY" data-field-id="10021416" data-annot-id="7915664" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="BZ" data-field-id="10024280" data-annot-id="8850752" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="BH" data-field-id="10027336" data-annot-id="8850944" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="BG" data-field-id="10030536" data-annot-id="8851408" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="CA" data-field-id="10033656" data-annot-id="8851600" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="CB" data-field-id="10036856" data-annot-id="8851792" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="CC" data-field-id="10039976" data-annot-id="8643264" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="CD" data-field-id="10043176" data-annot-id="8643456" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="CE" data-field-id="10046296" data-annot-id="8643648" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="CF" data-field-id="10049496" data-annot-id="8643984" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="FU" data-field-id="10052616" data-annot-id="8644176" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="CG" data-field-id="10055816" data-annot-id="8852224" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="CH" data-field-id="10058936" data-annot-id="8852416" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="CJ" data-field-id="10062136" data-annot-id="8852608" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="CK" data-field-id="10065256" data-annot-id="8852800" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="CL" data-field-id="10068456" data-annot-id="8852992" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="CM" data-field-id="10071576" data-annot-id="8853184" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="CN" data-field-id="10074776" data-annot-id="8949136" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="FV" data-field-id="10077896" data-annot-id="8949328" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="CP" data-field-id="10081096" data-annot-id="8949520" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="FW" data-field-id="10084216" data-annot-id="8949712" type="number" disabled/>

    </div>
  </div>

  <script>
    pdfixUpdatePage(document.getElementById('pdf-page-2'));</script>

</div>

<div data-type="pdf-page" id="pdf-page-3" data-page-num="3" data-ratio="1.414167" className="pdf-page ">
  <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-3 ">
    <div data-type="pdf-page-text" className="pdf-page-text">
      <div className="pdf-obj-fixed" style={{ left: 67, top: 37, width: 608, height: 20, 'font-size': 20, }}>
        <span>
          Montant net total des dépenses de recherche (report de la ligne 31a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 37, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          40a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 61, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant net total des dépenses de recherche ex posées dans des exploitations situées dans
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 84, width: 280, height: 20, 'font-size': 20, }}>
        <span>
          un DOM (report de la ligne 31b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 73, width: 32, height: 15, 'font-size': 20, }}>
        <span>
          40b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 107, width: 682, height: 21, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt [(ligne 40a - ligne 40b) × 30 % + ligne 40b x 50 %]8
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 108, width: 19, height: 16, 'font-size': 20, }}>
        <span>
          41
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 132, width: 819, height: 20, 'font-size': 20, }}>
        <span>
          Quote-part de crédit d’impôt résultant de la partic ipation de l’entreprise dans des sociétés de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 155, width: 676, height: 20, 'font-size': 20, }}>
        <span>
          personnes ou groupements assimilés (reporter le montant indiqué ligne 87a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 144, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          42
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 179, width: 698, height: 20, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt pour les dépenses de rech erche (ligne 41 + ligne 42)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 180, width: 32, height: 15, 'font-size': 20, }}>
        <span>
          43a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 204, width: 733, height: 19, 'font-size': 20, }}>
        <span>
          Dont montant du crédit d&#39;impôt pour dépenses de rec herche situées dans un DOM
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 204, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          43b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 240, width: 620, height: 16, 'font-size': 20, }}>
        <span>
          DÉTERMINATION DU CRÉDIT D’IMPÔT POUR LES DÉPENSES DE COLLECTION
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 264, width: 602, height: 20, 'font-size': 20, }}>
        <span>
          Montant net total des dépenses de collection (report de la ligne 38a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 265, width: 32, height: 15, 'font-size': 20, }}>
        <span>
          44a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 288, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant net total des dépenses de collection exposées dans des exploitations situées dans
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 311, width: 280, height: 20, 'font-size': 20, }}>
        <span>
          un DOM (report de la ligne 38b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 300, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          44b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 335, width: 763, height: 20, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt pour dépenses de collection exposées par l’entreprise avant
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 357, width: 573, height: 21, 'font-size': 20, }}>
        <span>
          plafonnement [(ligne 44a - ligne 44b)× 30 % + ligne 44b x 50 % ]8
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 347, width: 22, height: 16, 'font-size': 20, }}>
        <span>
          45
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 382, width: 819, height: 20, 'font-size': 20, }}>
        <span>
          Quote-part de crédit d’impôt résultant de la partic ipation de l’entreprise dans des sociétés de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 406, width: 676, height: 19, 'font-size': 20, }}>
        <span>
          personnes ou groupements assimilés (reporter le montant indiqué ligne 87b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 394, width: 22, height: 16, 'font-size': 20, }}>
        <span>
          46
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 430, width: 752, height: 19, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt pour dépenses de collection avant plafonnement des aides
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 453, width: 174, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 45 + ligne 46)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 441, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          47a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 477, width: 782, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant du crédit d&#39;impôt pour dépenses de collection situées dans un DOM avant
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 65, top: 500, width: 123, height: 20, 'font-size': 20, }}>
        <span>
          plafonnement
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 489, width: 32, height: 15, 'font-size': 20, }}>
        <span>
          47b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 524, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Montant des aides de minimis accordées à l&#39;entreprise dans les conditions du rè glement (UE)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 547, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          n° 1407/2013 de la Commission, du 18 décembre 2013, relatif à l&#39;application des articles 107 et
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 570, width: 707, height: 20, 'font-size': 20, }}>
        <span>
          108 du traité sur le fonctionnement de l&#39;Union européenne aux aides de minimis
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 547, width: 22, height: 16, 'font-size': 20, }}>
        <span>
          48
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 594, width: 334, height: 20, 'font-size': 20, }}>
        <span>
          Montant cumulé (ligne 47a + ligne 48)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 594, width: 22, height: 16, 'font-size': 20, }}>
        <span>
          49
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 618, width: 672, height: 20, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt pour dépenses de collection après plafonnement :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 641, width: 592, height: 20, 'font-size': 20, }}>
        <span>
          Si le montant ligne 48 est égal à 200 000 €, report er zéro ligne 50a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 664, width: 813, height: 20, 'font-size': 20, }}>
        <span>
          Si le montant ligne 49 est inférieur à 200 000 €, r eporter à la ligne 50a le montant déterminé
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 688, width: 81, height: 20, 'font-size': 20, }}>
        <span>
          ligne 47a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 711, width: 870, height: 19, 'font-size': 20, }}>
        <span>
          Si le montant ligne 49 est supérieur à 200 000 €, le montant à reporter ligne 50a est égal
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 734, width: 284, height: 20, 'font-size': 20, }}>
        <span>
          à (200 000 € - montant ligne 48)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 676, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          50a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 758, width: 782, height: 19, 'font-size': 20, }}>
        <span>
          Dont montant du crédit d&#39;impôt pour dépenses de collection situées dans un DOM après
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 65, top: 781, width: 123, height: 19, 'font-size': 20, }}>
        <span>
          plafonnement
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 770, width: 32, height: 15, 'font-size': 20, }}>
        <span>
          50b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 819, width: 784, height: 20, 'font-size': 20, }}>
        <span>
          Montant total du crédit d’impôt au titre des dépenses de recherche et de collection
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 843, width: 207, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 43a + ligne 50a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 831, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          51a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 868, width: 850, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant total du crédit d&#39;impôt pour dépenses de recherche et de collection situées
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 892, width: 344, height: 20, 'font-size': 20, }}>
        <span>
          dans un DOM (ligne 43b + ligne 50b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 880, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          51b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 207, top: 933, width: 40, height: 17, 'font-size': 23, }}>
        <span>
          B. L
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 247, top: 933, width: 370, height: 18, 'font-size': 18, }}>
        <span>
          ORSQUE LES DÉPENSES PORTÉES LIGNE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 623, top: 933, width: 36, height: 17, 'font-size': 23, }}>
        <span>
          39a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 667, top: 933, width: 200, height: 17, 'font-size': 18, }}>
        <span>
          SONT SUPÉRIEURES À
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 874, top: 933, width: 140, height: 17, 'font-size': 23, }}>
        <span>
          100 000 000 €
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 973, width: 619, height: 16, 'font-size': 20, }}>
        <span>
          DÉTERMINATION DU CRÉDIT D’IMPÔT POUR LES DÉPENSES DE RECHERCHE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 997, width: 846, height: 20, 'font-size': 20, }}>
        <span>
          Montant net total des dépenses de recherche limité à 100 000 000 € (montant indiqué ligne 31a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1020, width: 284, height: 20, 'font-size': 20, }}>
        <span>
          dans la limite de 100 000 000 €)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1009, width: 33, height: 16, 'font-size': 20, }}>
        <span>
          52a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1045, width: 877, height: 19, 'font-size': 20, }}>
        <span>
          Dont montant net total des dépenses de recherche ex posées dans des exploitations situées dans
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1067, width: 565, height: 20, 'font-size': 20, }}>
        <span>
          un DOM (report de la ligne 31b dans la limite de 10 0 000 000 €)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1056, width: 33, height: 16, 'font-size': 20, }}>
        <span>
          52b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1092, width: 531, height: 19, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt relatif aux dépenses de r echerche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1113, width: 450, height: 21, 'font-size': 20, }}>
        <span>
          [(ligne 52a - ligne 52b) x 30 % + ligne 52b x 50 % ]8
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1103, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          53
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1139, width: 637, height: 19, 'font-size': 20, }}>
        <span>
          Indiquer la part des dépenses de recherche supérieure à 100 000 000 €
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1162, width: 238, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 31a - 100 000 000 €)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1151, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          54
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1186, width: 806, height: 20, 'font-size': 20, }}>
        <span>
          Puis déterminer le montant du crédit d’impôt relati f à la fraction supérieure à 100 000 000 €
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1209, width: 140, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 54 × 5 %)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1198, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          55
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1233, width: 449, height: 20, 'font-size': 20, }}>
        <span>
          Montant total du crédit d’impôt (ligne 53 + ligne 55)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1233, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          56
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1257, width: 819, height: 20, 'font-size': 20, }}>
        <span>
          Quote-part de crédit d’impôt résultant de la partic ipation de l’entreprise dans des sociétés de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1280, width: 675, height: 20, 'font-size': 20, }}>
        <span>
          personnes ou groupements assimilés (reporter le montant indiqué ligne 87a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1269, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          57
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1304, width: 697, height: 20, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt pour les dépenses de rech erche (ligne 56 + ligne 57)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1305, width: 33, height: 15, 'font-size': 20, }}>
        <span>
          58a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1328, width: 729, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant du crédit d&#39;impôt pour dépenses de rec herche situées dans un DOM
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1329, width: 33, height: 15, 'font-size': 20, }}>
        <span>
          58b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1365, width: 620, height: 16, 'font-size': 20, }}>
        <span>
          DÉTERMINATION DU CRÉDIT D’IMPÔT POUR LES DÉPENSES DE COLLECTION
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1389, width: 709, height: 20, 'font-size': 20, }}>
        <span>
          Montant net total des dépenses de collection (report du montant porté ligne 38a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1389, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          59a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1413, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant net total des dépenses de collection e xposées dans des exploitations situées dans
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1436, width: 281, height: 20, 'font-size': 20, }}>
        <span>
          un DOM (report de la ligne 38b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1425, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          59b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1460, width: 408, height: 20, 'font-size': 20, }}>
        <span>
          Plafond disponible ( 100 000 000 € - ligne 52a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1461, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          60
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1484, width: 608, height: 20, 'font-size': 20, }}>
        <span>
          Crédit d’impôt pour dépenses de collection exposées par l’entreprise
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1507, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          [(Dépenses portées ligne 59a dans la limite de la ligne 60 - Dépenses portées ligne 59 b dans la
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1530, width: 845, height: 20, 'font-size': 20, }}>
        <span>
          limite de la ligne 60) x 30 % + (Dépenses portées ligne 59b dans la limite de la ligne 60) x 50 %
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 914, top: 1529, width: 7, height: 10, 'font-size': 12, }}>
        <span>
          8
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1508, width: 18, height: 15, 'font-size': 20, }}>
        <span>
          61
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1555, width: 638, height: 19, 'font-size': 20, }}>
        <span>
          Lorsque la part des dépenses de collection excède le plafond disponible
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 1578, width: 709, height: 19, 'font-size': 20, }}>
        <span>
          [(ligne 59a - ligne 60)&#62;0] le crédit d’impôt est égal à [(ligne 59a - ligne 60) x 5 %]
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 1566, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          62
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 62, top: 1630, width: 5, height: 9, 'font-size': 12, }}>
        <span>
          8
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 72, top: 1632, width: 22, height: 15, 'font-size': 20, }}>
        <span>
          Ce
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 99, top: 1633, width: 1008, height: 17, 'font-size': 17, }}>
        <span>
          taux est de 50 % pour les dépenses exposées à com pter du 1er janvier 2015 dans des exploitations situées dans un département d&#39;outre-mer
<br />
        </span>
      </div>
    </div>

    <div data-type="pdf-page-annots" className="pdf-page-annots" >
      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="CQ" data-field-id="10087416" data-annot-id="8134352" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="FX" data-field-id="10090536" data-annot-id="9230144" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="CR" data-field-id="10093736" data-annot-id="9230336" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="CS" data-field-id="9995992" data-annot-id="9224944" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="CT" data-field-id="10100552" data-annot-id="9225136" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="FZ" data-field-id="10103704" data-annot-id="9225328" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="CU" data-field-id="10106856" data-annot-id="9225520" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="GA" data-field-id="10110008" data-annot-id="9232496" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="CV" data-field-id="10113160" data-annot-id="9232688" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="CW" data-field-id="10116312" data-annot-id="9233024" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="CX" data-field-id="10119464" data-annot-id="9233216" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="GC" data-field-id="10430008" data-annot-id="9233408" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="CY" data-field-id="10122616" data-annot-id="9233600" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field " name="CZ" data-field-id="10433208" data-annot-id="9233792" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="DA" data-field-id="10125768" data-annot-id="9233984" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="GD" data-field-id="10128920" data-annot-id="9234176" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="DB" data-field-id="10132072" data-annot-id="9234368" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="GE" data-field-id="10135224" data-annot-id="9234832" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="DC" data-field-id="10138376" data-annot-id="9235024" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="GF" data-field-id="10141528" data-annot-id="9235216" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="DD" data-field-id="10144680" data-annot-id="9235408" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field " name="DE" data-field-id="10147832" data-annot-id="9235600" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="DF" data-field-id="10150984" data-annot-id="9235792" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field " name="DG" data-field-id="10154136" data-annot-id="9235984" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field " name="DH" data-field-id="10157288" data-annot-id="9236176" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field " name="DJ" data-field-id="10160408" data-annot-id="9236368" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field " name="GJ" data-field-id="10163608" data-annot-id="9236560" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field " name="DK" data-field-id="10166728" data-annot-id="9236752" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field " name="GK" data-field-id="10169928" data-annot-id="9236944" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field " name="DL" data-field-id="10173048" data-annot-id="9237136" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field " name="DM" data-field-id="10176248" data-annot-id="9237328" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field " name="DN" data-field-id="10179368" data-annot-id="9237520" type="number" disabled />

    </div>
  </div>

  <script>
    pdfixUpdatePage(document.getElementById('pdf-page-3'));</script>

</div>

<div data-type="pdf-page" id="pdf-page-4" data-page-num="4" data-ratio="1.414167" className="pdf-page ">
  <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-4 ">
    <div data-type="pdf-page-text" className="pdf-page-text">
      <div className="pdf-obj-fixed" style={{ left: 68, top: 37, width: 877, height: 20, 'font-size': 20, }}>
        <span>
          Crédit d’impôt pour dépenses de collection exposées par l’entreprise avant plafonnement (ligne 61
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 60, width: 92, height: 20, 'font-size': 20, }}>
        <span>
          + ligne 62)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 49, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          63
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 84, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Quote-part de crédit d’impôt résultant de la partic ipation de l’entreprise dans des sociétés de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 107, width: 675, height: 20, 'font-size': 20, }}>
        <span>
          personnes ou groupements assimilés (reporter le montant indiqué ligne 87b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 96, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          64
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 131, width: 678, height: 20, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt avant plafonnement des ai des (ligne 63 + ligne 64)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 131, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          65
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 155, width: 878, height: 20, 'font-size': 20, }}>
        <span>
          Montant des aides de minimis accordées à l&#39;entreprise dans les conditions du règlement (UE)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 178, width: 877, height: 20, 'font-size': 20, }}>
        <span>
          n° 1407/2013 de la Commission, du 18 décembre 2013, relatif à l’application des articles 107 et
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 201, width: 707, height: 20, 'font-size': 20, }}>
        <span>
          108 du traité sur le fonctionnement de l&#39;Union européenne aux aides de minimis
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 178, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          66
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 225, width: 323, height: 20, 'font-size': 20, }}>
        <span>
          Montant cumulé (ligne 65 + ligne 66)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 973, top: 226, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          67
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 250, width: 672, height: 19, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt pour dépenses de collecti on après plafonnement :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 273, width: 592, height: 20, 'font-size': 20, }}>
        <span>
          Si le montant ligne 66 est égal à 200 000 €, report er zéro ligne 68a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 296, width: 876, height: 20, 'font-size': 20, }}>
        <span>
          Si le montant ligne 67 est inférieur à 200 000 €, r eporter à la ligne 68a le montant déterminé
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 319, width: 70, height: 20, 'font-size': 20, }}>
        <span>
          ligne 65
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 342, width: 868, height: 20, 'font-size': 20, }}>
        <span>
          Si le montant ligne 67 est supérieur à 200 000 €, le montant à reporter ligne 68a est égal
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 365, width: 284, height: 20, 'font-size': 20, }}>
        <span>
          à (200 000 € - montant ligne 66)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 308, width: 32, height: 15, 'font-size': 20, }}>
        <span>
          68a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 389, width: 727, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant du crédit d&#39;impôt pour dépenses de col lection situées dans un DOM
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 389, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          68b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 434, width: 784, height: 19, 'font-size': 20, }}>
        <span>
          Montant total du crédit d’impôt au titre des dépenses de recherche et de collection
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 457, width: 205, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 58a + ligne 68a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 445, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          69a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 489, width: 855, height: 19, 'font-size': 20, }}>
        <span>
          Dont montant du crédit d&#39;impôt pour dépenses de recherche et de collection situées dans
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 69, top: 512, width: 290, height: 20, 'font-size': 20, }}>
        <span>
          un DOM (ligne 58b + ligne 68b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 501, width: 32, height: 15, 'font-size': 20, }}>
        <span>
          69b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 571, width: 64, height: 17, 'font-size': 23, }}>
        <span>
          IV - C
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 133, top: 571, width: 858, height: 17, 'font-size': 18, }}>
        <span>
          ALCUL DU CRÉDIT D&#39;IMPÔT AU TITRE DES DÉPENSES D’INNOVATION ENGAGÉES PAR LES
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1004, top: 571, width: 46, height: 17, 'font-size': 23, }}>
        <span>
          PME
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1062, top: 574, width: 84, height: 14, 'font-size': 18, }}>
        <span>
          AU SENS
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 599, width: 158, height: 15, 'font-size': 18, }}>
        <span>
          COMMUNAUTAIRE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 636, width: 302, height: 16, 'font-size': 20, }}>
        <span>
          DÉTERMINATION DU CRÉDIT D’IMPÔT
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 972, top: 636, width: 115, height: 16, 'font-size': 20, }}>
        <span>
          ANNÉE CIVILE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1093, top: 636, width: 44, height: 16, 'font-size': 20, }}>
        <span>
          2018
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 660, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Dotations aux amortissements des immobilisations af fectées aux opérations de conception de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 684, width: 491, height: 19, 'font-size': 20, }}>
        <span>
          prototypes ou installations pilotes de nouveaux produits
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 672, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          70
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 707, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Dépenses de personnel affecté à la réalisation d’op érations de conception de prototypes ou
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 731, width: 364, height: 19, 'font-size': 20, }}>
        <span>
          installations pilotes de nouveaux produits
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 719, width: 18, height: 16, 'font-size': 20, }}>
        <span>
          71
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 755, width: 667, height: 20, 'font-size': 20, }}>
        <span>
          Autres dépenses de fonctionnement [(ligne 70 × 75 %) + (ligne 71 × 50 %)]
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 756, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          72
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 780, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Dotations aux amortissements, frais de prise et de maintenance de brevets et de certificats
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 803, width: 517, height: 20, 'font-size': 20, }}>
        <span>
          d’obtention végétale, frais de dépôt de dessins et modèles
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 792, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          73
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 828, width: 720, height: 20, 'font-size': 20, }}>
        <span>
          Frais de défense des brevets, certificats d’obtention végétale, dessins et modèles
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 829, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          74
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 854, width: 722, height: 20, 'font-size': 20, }}>
        <span>
          Opérations confiées à des entreprises ou bureaux d’ études et d’ingénierie agréés
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 854, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          75
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 879, width: 581, height: 20, 'font-size': 20, }}>
        <span>
          Montant total des dépenses d’innovation réalisées par l’entreprise
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 902, width: 547, height: 20, 'font-size': 20, }}>
        <span>
          (ligne 70 + ligne 71 + ligne 72 + ligne 73 + ligne 74 + ligne 75)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 891, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          76
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 927, width: 804, height: 20, 'font-size': 20, }}>
        <span>
          Total des dépenses d’innovation après plafonnement (ligne 76 dans la limite de 400 000 €)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 927, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          77
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 953, width: 517, height: 19, 'font-size': 20, }}>
        <span>
          Montant des subventions publiques remboursables ou non
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 586, top: 952, width: 7, height: 9, 'font-size': 12, }}>
        <span>
          9
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 953, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          78
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 978, width: 879, height: 19, 'font-size': 20, }}>
        <span>
          Pour les sous-traitants, montant des sommes encaiss ées au titre des travaux d’innovation qui leur
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1000, width: 140, height: 16, 'font-size': 20, }}>
        <span>
          ont été confiées
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 989, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          79
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1025, width: 878, height: 19, 'font-size': 20, }}>
        <span>
          Montant des dépenses exposées auprès de tiers au ti tre de prestations de conseil pour l’octroi du
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1048, width: 227, height: 20, 'font-size': 20, }}>
        <span>
          bénéfice du crédit d’impôt
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 296, top: 1047, width: 12, height: 9, 'font-size': 12, }}>
        <span>
          10
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 1036, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          80
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1073, width: 496, height: 19, 'font-size': 20, }}>
        <span>
          Montant des remboursements de subventions publiques
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 566, top: 1072, width: 10, height: 9, 'font-size': 12, }}>
        <span>
          11
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 1073, width: 18, height: 15, 'font-size': 20, }}>
        <span>
          81
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1098, width: 794, height: 20, 'font-size': 20, }}>
        <span>
          Montant net des dépenses d&#39;innovation (ligne 77 - l igne 78 - ligne 79 - ligne 80 + ligne 81)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 968, top: 1099, width: 33, height: 15, 'font-size': 20, }}>
        <span>
          82a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1123, width: 879, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant net des dépenses d&#39;innovation exposées dans des exploitations situées dans un
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1146, width: 44, height: 16, 'font-size': 20, }}>
        <span>
          DOM
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 968, top: 1135, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          82b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1171, width: 717, height: 20, 'font-size': 20, }}>
        <span>
          Montant total du crédit d’impôt [(ligne 82a - ligne 82b)) x 20 % + ligne 82b x 40%]
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 787, top: 1170, width: 12, height: 9, 'font-size': 12, }}>
        <span>
          12
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 1171, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          83
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1196, width: 880, height: 20, 'font-size': 20, }}>
        <span>
          Quote-part de crédit d’impôt résultant de la partic ipation de l’entreprise dans des sociétés de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1219, width: 675, height: 20, 'font-size': 20, }}>
        <span>
          personnes ou groupements assimilés (reporter le montant indiqué ligne 87c)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 1208, width: 21, height: 15, 'font-size': 20, }}>
        <span>
          84
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1244, width: 716, height: 20, 'font-size': 20, }}>
        <span>
          Montant du crédit d’impôt au titre des dépenses d’innovation (ligne 83 + ligne 84)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 968, top: 1244, width: 33, height: 16, 'font-size': 20, }}>
        <span>
          85a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1269, width: 722, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant du crédit d&#39;impôt pour dépenses d&#39;innovation situées dans un DOM
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 968, top: 1270, width: 32, height: 15, 'font-size': 20, }}>
        <span>
          85b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1316, width: 791, height: 19, 'font-size': 20, }}>
        <span>
          Montant total du crédit d’impôt au titre des dépenses de recherche, de collection et
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1339, width: 400, height: 20, 'font-size': 20, }}>
        <span>
          d’innovation (ligne 51a ou 69a + ligne 85a)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1328, width: 32, height: 15, 'font-size': 20, }}>
        <span>
          86a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1371, width: 857, height: 20, 'font-size': 20, }}>
        <span>
          Dont montant du crédit d&#39;impôt pour dépenses de recherche, de collection et d&#39;innovation
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1394, width: 489, height: 20, 'font-size': 20, }}>
        <span>
          situées dans un DOM (ligne 51b ou 69b + ligne 85b)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 967, top: 1383, width: 33, height: 16, 'font-size': 20, }}>
        <span>
          86b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1486, width: 5, height: 7, 'font-size': 10, }}>
        <span>
          9
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 70, top: 1487, width: 1085, height: 16, 'font-size': 17, }}>
        <span>
          Les subventions publiques, remboursables ou non, doivent être déduites de la base de calcul du crédit d’impôt calculé au titre de l&#39;année ou des années
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1505, width: 1094, height: 17, 'font-size': 17, }}>
        <span>
          au cours de laquelle ou desquelles les dépenses éli gibles, que ces avances ou subventions ont vocation à couvrir, sont exposées, conformément au III de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1524, width: 449, height: 16, 'font-size': 17, }}>
        <span>
          l&#39;article 244 quater B du CGI. (BOI-BIC-RICI-10-10-30-20 § 10).
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1541, width: 9, height: 8, 'font-size': 10, }}>
        <span>
          10
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 80, top: 1542, width: 1074, height: 16, 'font-size': 17, }}>
        <span>
          Le montant des dépenses à déduire correspond soit au montant total des rémunérations allouées en cont repartie de ces prestations fixées
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1561, width: 1093, height: 16, 'font-size': 17, }}>
        <span>
          proportionnellement au montant du crédit d’impôt ob tenu par l’entreprise, soit le montant des dépenses exposées autres que les rémunérations
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1579, width: 1088, height: 16, 'font-size': 17, }}>
        <span>
          proportionnelles excédant 15 000 € hors taxes ou 5 % du montant des dépenses éligibles au crédit d’impôt minoré des subventions publiques (cf. notice).
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1597, width: 8, height: 7, 'font-size': 10, }}>
        <span>
          11
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 77, top: 1598, width: 1076, height: 16, 'font-size': 17, }}>
        <span>
          Le montant des remboursements de subventions publiques doit être multiplié par le rapport existant entre le taux du crédit d’impôt de l’année où la
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1616, width: 917, height: 16, 'font-size': 17, }}>
        <span>
          subvention remboursable a été déduite et le taux du crédit d’impôt de l’année où elle est remboursée partiellement ou totalement.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 62, top: 1633, width: 9, height: 8, 'font-size': 10, }}>
        <span>
          12
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 80, top: 1635, width: 1066, height: 16, 'font-size': 17, }}>
        <span>
          Ce taux est porté à 40 % pour les dépenses exposées à compter du 1er janvier 2015 dans des exploitat ions situées dans un département d’outre-mer
<br />
        </span>
      </div>
    </div>

    <div data-type="pdf-page-annots" className="pdf-page-annots" >
      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field " name="DP" data-field-id="10182568" data-annot-id="8215088" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field " name="DQ" data-field-id="10185688" data-annot-id="8832160" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field " name="DR" data-field-id="10188888" data-annot-id="8832384" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field " name="DS" data-field-id="10192008" data-annot-id="8832576" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field " name="DT" data-field-id="10195208" data-annot-id="8832768" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field " name="DU" data-field-id="10198328" data-annot-id="8832960" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field " name="GN" data-field-id="10201528" data-annot-id="8833152" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field " name="DV" data-field-id="10204648" data-annot-id="8833344" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field " name="GP" data-field-id="10207848" data-annot-id="8836656" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field " name="DW" data-field-id="10210968" data-annot-id="8836992" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field " name="DX" data-field-id="10214168" data-annot-id="8837184" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field " name="DY" data-field-id="10217288" data-annot-id="8837376" type="number"  disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field " name="DZ" data-field-id="10220488" data-annot-id="8837568" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field " name="EA" data-field-id="10223608" data-annot-id="8837760" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_125 pdf-obj-fixed acroform-field " name="EB" data-field-id="10226808" data-annot-id="8837952" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_126 pdf-obj-fixed acroform-field " name="EC" data-field-id="10229960" data-annot-id="8838144" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_127 pdf-obj-fixed acroform-field " name="ED" data-field-id="10233112" data-annot-id="8838336" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_128 pdf-obj-fixed acroform-field " name="EE" data-field-id="10236264" data-annot-id="8838800" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field " name="EF" data-field-id="10239416" data-annot-id="8838992" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_130 pdf-obj-fixed acroform-field " name="EG" data-field-id="10242568" data-annot-id="8839184" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_131 pdf-obj-fixed acroform-field " name="EH" data-field-id="10245720" data-annot-id="8839376" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_132 pdf-obj-fixed acroform-field " name="GQ" data-field-id="10248872" data-annot-id="8839568" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_133 pdf-obj-fixed acroform-field " name="GR" data-field-id="10252024" data-annot-id="8839760" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_134 pdf-obj-fixed acroform-field " name="EJ" data-field-id="10255176" data-annot-id="8839952" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_135 pdf-obj-fixed acroform-field " name="EK" data-field-id="10258328" data-annot-id="8840144" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_136 pdf-obj-fixed acroform-field " name="EL" data-field-id="10261480" data-annot-id="8840336" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_137 pdf-obj-fixed acroform-field " name="GT" data-field-id="10264632" data-annot-id="8840528" type="number"  />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_138 pdf-obj-fixed acroform-field " name="EM" data-field-id="10267784" data-annot-id="8840720" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_139 pdf-obj-fixed acroform-field " name="GU" data-field-id="10270936" data-annot-id="8840912" type="number" disabled />

    </div>
  </div>

  <script>
    pdfixUpdatePage(document.getElementById('pdf-page-4'));</script>

</div>

<div data-type="pdf-page" id="pdf-page-5" data-page-num="5" data-ratio="1.414167" className="pdf-page ">
  <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-5 ">
    <div data-type="pdf-page-text" className="pdf-page-text">
      <div className="pdf-obj-fixed" style={{ left: 60, top: 39, width: 1094, height: 19, 'font-size': 23, }}>
        <span>
          V - CADRE À SERVIR PAR LES ENTREPRISES DÉCLARANTES QUI DÉTIENNENT DES PARTICIPATIONS DANS DES SOCIÉTÉS
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 65, width: 419, height: 17, 'font-size': 18, }}>
        <span>
          DE PERSONNES OU GROUPEMENTS ASSIMILÉS
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 81, top: 112, width: 384, height: 20, 'font-size': 20, }}>
        <span>
          Nom et adresse des sociétés de personnes
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 100, top: 135, width: 345, height: 20, 'font-size': 20, }}>
        <span>
          ou groupements assimilés et n° SIREN
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 179, top: 158, width: 188, height: 20, 'font-size': 20, }}>
        <span>
          (pour les entreprises)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 505, top: 112, width: 99, height: 16, 'font-size': 20, }}>
        <span>
          % de droits
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 494, top: 135, width: 119, height: 16, 'font-size': 20, }}>
        <span>
          détenus dans
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 513, top: 158, width: 83, height: 16, 'font-size': 20, }}>
        <span>
          la société
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 747, top: 105, width: 248, height: 20, 'font-size': 20, }}>
        <span>
          Quote-part du crédit d’impôt
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 648, top: 129, width: 133, height: 20, 'font-size': 20, }}>
        <span>
          Pour dépenses
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 656, top: 153, width: 116, height: 15, 'font-size': 20, }}>
        <span>
          de recherche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 818, top: 129, width: 133, height: 20, 'font-size': 20, }}>
        <span>
          Pour dépenses
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 829, top: 153, width: 110, height: 15, 'font-size': 20, }}>
        <span>
          de collection
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 996, top: 129, width: 133, height: 20, 'font-size': 20, }}>
        <span>
          Pour dépenses
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1010, top: 152, width: 105, height: 16, 'font-size': 20, }}>
        <span>
          d’innovation
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 549, top: 285, width: 124, height: 16, 'font-size': 20, }}>
        <span>
          TOTAL 87a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 977, top: 285, width: 32, height: 16, 'font-size': 20, }}>
        <span>
          87c
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 811, top: 285, width: 33, height: 16, 'font-size': 20, }}>
        <span>
          87b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 334, width: 108, height: 17, 'font-size': 23, }}>
        <span>
          VI - CADRE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 178, top: 334, width: 976, height: 17, 'font-size': 18, }}>
        <span>
          À SERVIR POUR LA RÉPARTITION DU CRÉDIT D’IMPÔT ENTRE LES ASSOCIÉS MEMBRES DE SOCIÉTÉS DE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 359, width: 388, height: 18, 'font-size': 18, }}>
        <span>
          PERSONNES OU GROUPEMENTS ASSIMILÉS
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 84, top: 406, width: 379, height: 16, 'font-size': 20, }}>
        <span>
          Nom et adresse des associés membres de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 113, top: 430, width: 313, height: 19, 'font-size': 20, }}>
        <span>
          sociétés de personnes et n° SIREN
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 179, top: 452, width: 188, height: 20, 'font-size': 20, }}>
        <span>
          (pour les entreprises)
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 508, top: 406, width: 99, height: 17, 'font-size': 20, }}>
        <span>
          % de droits
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 487, top: 430, width: 141, height: 16, 'font-size': 20, }}>
        <span>
          détenus dans la
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 526, top: 453, width: 63, height: 15, 'font-size': 20, }}>
        <span>
          société
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 769, top: 400, width: 248, height: 19, 'font-size': 20, }}>
        <span>
          Quote-part du crédit d’impôt
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 648, top: 424, width: 133, height: 20, 'font-size': 20, }}>
        <span>
          Pour dépenses
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 656, top: 447, width: 116, height: 16, 'font-size': 20, }}>
        <span>
          de recherche
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 818, top: 424, width: 133, height: 20, 'font-size': 20, }}>
        <span>
          Pour dépenses
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 829, top: 447, width: 110, height: 16, 'font-size': 20, }}>
        <span>
          de collection
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 996, top: 424, width: 133, height: 20, 'font-size': 20, }}>
        <span>
          Pour dépenses
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 1010, top: 447, width: 105, height: 16, 'font-size': 20, }}>
        <span>
          d’innovation
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 555, top: 579, width: 113, height: 16, 'font-size': 20, }}>
        <span>
          TOTAL 88a
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 806, top: 580, width: 34, height: 15, 'font-size': 20, }}>
        <span>
          88b
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 977, top: 580, width: 33, height: 15, 'font-size': 20, }}>
        <span>
          88c
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 631, width: 61, height: 17, 'font-size': 23, }}>
        <span>
          VII - U
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 123, top: 631, width: 250, height: 17, 'font-size': 18, }}>
        <span>
          TILISATION DE LA CRÉANCE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 375, top: 628, width: 14, height: 11, 'font-size': 13, }}>
        <span>
          13
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 670, width: 1094, height: 20, 'font-size': 20, }}>
        <span>
          VII-1. Entreprises à l’impôt sur les sociétés : reporter le montant du crédit d’impôt déterminé ligne 86a sur le relevé de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 693, width: 1094, height: 20, 'font-size': 20, }}>
        <span>
          solde n° 2572-SD et les montants déterminés lignes 86a et 86b sur la déclaration des réductions et crédits d’impôt
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 716, width: 148, height: 16, 'font-size': 20, }}>
        <span>
          n° 2069-RCI-SD.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 751, width: 1094, height: 19, 'font-size': 20, }}>
        <span>
          VII-2. Entreprises à l’impôt sur le revenu : reporter le montant du crédit d’impôt déterminé ligne 86a sur la déclaration de
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 774, width: 1094, height: 20, 'font-size': 20, }}>
        <span>
          revenus n° 2042-C-PRO et les montants déterminés lignes 86a et 86b sur la déclaration des réductions e t crédits d&#39;impôt
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 797, width: 148, height: 16, 'font-size': 20, }}>
        <span>
          n° 2069-RCI-SD.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 832, width: 640, height: 19, 'font-size': 20, }}>
        <span>
          VII-3. Mobilisation de créance auprès d’un établissement d e crédit :
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 867, width: 505, height: 16, 'font-size': 20, }}>
        <span>
          Montant des créances dont la mobilisation est demandée
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 974, top: 867, width: 21, height: 16, 'font-size': 20, }}>
        <span>
          89
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 73, top: 913, width: 1069, height: 16, 'font-size': 17, }}>
        <span>
          Les demandes de remboursement immédiat ou à l’issue de la période d’imputation du crédit d’impôt non imputé sur l’impôt sur les sociétés
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 138, top: 932, width: 941, height: 16, 'font-size': 17, }}>
        <span>
          sont formulées sur l’imprimé n° 2573-SD par voie dématérialisée ou sur l’imprimé n° 2573-SD disponible sur le portail fiscal
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 529, top: 950, width: 156, height: 16, 'font-size': 17, }}>
        <span>
          www.impots.gouv.fr.
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1001, width: 67, height: 17, 'font-size': 23, }}>
        <span>
          VIII - S
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 135, top: 1004, width: 91, height: 14, 'font-size': 18, }}>
        <span>
          IGNATURE
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1052, width: 279, height: 16, 'font-size': 20, }}>
        <span>
          A  .
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 820, top: 1052, width: 287, height: 16, 'font-size': 20, }}>
        <span>
          Le  .
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 68, top: 1122, width: 561, height: 18, 'font-size': 20, }}>
        <span>
          Nom, Qualité  .
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 804, top: 1122, width: 85, height: 20, 'font-size': 20, }}>
        <span>
          Signature
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 66, top: 1168, width: 574, height: 16, 'font-size': 20, }}>
        <span>
          Adresse e-mail
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 67, top: 1191, width: 273, height: 19, 'font-size': 20, }}>
        <span>
          Téléphone
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 61, top: 1593, width: 10, height: 9, 'font-size': 12, }}>
        <span>
          13
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 77, top: 1597, width: 1077, height: 16, 'font-size': 17, }}>
        <span>
          S’agissant des sociétés relevant du régime de groupe prévu à l’article 223 A du CGI, la société mère joint les déclarations spéciales des sociétés du
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1616, width: 1095, height: 16, 'font-size': 17, }}>
        <span>
          groupe y compris sa propre déclaration au relevé de solde 2572 relatif au résultat d’ensemble. Le crédit d’impôt de chaque société du groupe est porté
<br />
        </span>
      </div>
      <div className="pdf-obj-fixed" style={{ left: 60, top: 1635, width: 211, height: 13, 'font-size': 17, }}>
        <span>
          sur la déclaration n°2058-CG.
<br />
        </span>
      </div>
    </div>

    <div data-type="pdf-page-annots" className="pdf-page-annots" >
      <Field component="input" className="pde-form-field pdf-annot obj_141 pdf-obj-fixed acroform-field " name="EN1" data-field-id="10274088" data-annot-id="8139824" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_142 pdf-obj-fixed acroform-field " name="EN2" data-field-id="10277240" data-annot-id="8331536" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_143 pdf-obj-fixed acroform-field " name="EN3" data-field-id="10280392" data-annot-id="8921024" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_144 pdf-obj-fixed acroform-field " name="EN4" data-field-id="10283544" data-annot-id="8921216" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_145 pdf-obj-fixed acroform-field " name="EP1" data-field-id="10286696" data-annot-id="8921408" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field " name="EP2" data-field-id="10289848" data-annot-id="8921600" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_147 pdf-obj-fixed acroform-field " name="EP3" data-field-id="10293000" data-annot-id="8921792" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_148 pdf-obj-fixed acroform-field " name="EP4" data-field-id="10296120" data-annot-id="8921984" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_149 pdf-obj-fixed acroform-field " name="EQ1" data-field-id="10096888" data-annot-id="8922176" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_150 pdf-obj-fixed acroform-field " name="EQ2" data-field-id="10303512" data-annot-id="8961776" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_151 pdf-obj-fixed acroform-field " name="EQ3" data-field-id="10306632" data-annot-id="8961968" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_152 pdf-obj-fixed acroform-field " name="EQ4" data-field-id="10309832" data-annot-id="8962160" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_153 pdf-obj-fixed acroform-field " name="ER1" data-field-id="10312952" data-annot-id="8962352" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_154 pdf-obj-fixed acroform-field " name="ER2" data-field-id="10316152" data-annot-id="8962544" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_155 pdf-obj-fixed acroform-field " name="ER3" data-field-id="10319272" data-annot-id="8962736" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_156 pdf-obj-fixed acroform-field " name="ER4" data-field-id="10322472" data-annot-id="8962928" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_157 pdf-obj-fixed acroform-field " name="ES1" data-field-id="10325592" data-annot-id="8963120" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_158 pdf-obj-fixed acroform-field " name="ES2" data-field-id="10328792" data-annot-id="8922368" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_159 pdf-obj-fixed acroform-field " name="ES3" data-field-id="10331912" data-annot-id="8963584" type="number" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_160 pdf-obj-fixed acroform-field " name="ES4" data-field-id="10335112" data-annot-id="8963776" type="number" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_161 pdf-obj-fixed acroform-field " name="ET" data-field-id="10338232" data-annot-id="8963968" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_162 pdf-obj-fixed acroform-field " name="EU" data-field-id="10341432" data-annot-id="8964160" type="number" disabled/>

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_163 pdf-obj-fixed acroform-field " name="EV" data-field-id="10344552" data-annot-id="8964352" type="number" disabled/>

      <Field component="input" className="pde-form-field pdf-annot obj_164 pdf-obj-fixed acroform-field " name="EW1" data-field-id="10347752" data-annot-id="8964544" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_165 pdf-obj-fixed acroform-field " name="EW2" data-field-id="10350872" data-annot-id="8964736" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_166 pdf-obj-fixed acroform-field " name="EW3" data-field-id="10354072" data-annot-id="8964928" type="text" />

      <Field component="input" className="pde-form-field pdf-annot obj_167 pdf-obj-fixed acroform-field " name="EW4" data-field-id="10357192" data-annot-id="8965120" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_168 pdf-obj-fixed acroform-field " name="EX1" data-field-id="10360392" data-annot-id="8965312" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_169 pdf-obj-fixed acroform-field " name="EX2" data-field-id="10363512" data-annot-id="8965504" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_170 pdf-obj-fixed acroform-field " name="EX3" data-field-id="10366744" data-annot-id="8965696" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_171 pdf-obj-fixed acroform-field " name="EX4" data-field-id="10369912" data-annot-id="8965888" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_172 pdf-obj-fixed acroform-field " name="EY1" data-field-id="10373080" data-annot-id="8966080" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_173 pdf-obj-fixed acroform-field " name="EY2" data-field-id="10376248" data-annot-id="8966272" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_174 pdf-obj-fixed acroform-field " name="EY3" data-field-id="10379416" data-annot-id="8963312" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_175 pdf-obj-fixed acroform-field " name="EY4" data-field-id="10382584" data-annot-id="8966992" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_176 pdf-obj-fixed acroform-field " name="EZ1" data-field-id="10385752" data-annot-id="8967184" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_177 pdf-obj-fixed acroform-field " name="EZ2" data-field-id="10388920" data-annot-id="8967376" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_178 pdf-obj-fixed acroform-field " name="EZ3" data-field-id="10392088" data-annot-id="8967568" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_179 pdf-obj-fixed acroform-field " name="EZ4" data-field-id="10395256" data-annot-id="8967760" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_180 pdf-obj-fixed acroform-field " name="FA1" data-field-id="10398424" data-annot-id="8967952" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_181 pdf-obj-fixed acroform-field " name="FA2" data-field-id="10401592" data-annot-id="8968144" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_182 pdf-obj-fixed acroform-field " name="FA3" data-field-id="10404760" data-annot-id="8968336" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_183 pdf-obj-fixed acroform-field " name="FA4" data-field-id="10407784" data-annot-id="8968528" type="text" />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_184 pdf-obj-fixed acroform-field " name="FB" data-field-id="10410888" data-annot-id="8968720" type="text" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_185 pdf-obj-fixed acroform-field " name="FC" data-field-id="10414232" data-annot-id="8968912" type="text" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_186 pdf-obj-fixed acroform-field " name="FD" data-field-id="10417400" data-annot-id="8969104" type="text" disabled />

      <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_187 pdf-obj-fixed acroform-field " name="FM" data-field-id="10420568" data-annot-id="8969296" type="text" />

    </div>
  </div>


</div>
  
    </div>
  );
};

export default Tax2069A;
