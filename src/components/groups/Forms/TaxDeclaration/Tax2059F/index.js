import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {sum, useCompute, setIsNothingness} from "helpers/pdfforms";
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';

import './style.scss';
const notNeededFields = {
       'NOM_STEA': true,
       'CP_STEH': true,
       'VILLE_STEI': true,
       'ADR_STEG': true,
       'ADR_STED': true,
       'ADR_STEF': true,
       'SIRET_STET': true,
       'FIN_EX': true,
       'GS': true
     };

const Tax2059F = (props) => {
  const { taxDeclarationForm, change } = props;

  useCompute('BD', sum, ['BA', 'BC'], taxDeclarationForm, change);
  setIsNothingness(taxDeclarationForm, notNeededFields, 'GS', change);

  return (
    <div className="form-tax-declaration-2059f">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 424, top: 118, width: 413, height: 17, 'font-size': 23, }}>
<span>
COMPOSITION DU CAPITAL SOCIAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 934, top: 110, width: 232, height: 15, 'font-size': 20, }}>
<span>
DGFiP N° 2059-F-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 1567, width: 937, height: 13, 'font-size': 14, }}>
<span>
(1) Lorsque le nombre d’associés excède le nombre de lignes de l’imprimé, utiliser un ou plusieurs tableaux supplémentaires. Dans ce cas, il convient de numéroter chaque tableau
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1583, width: 696, height: 13, 'font-size': 14, }}>
<span>
en haut et à gauche de la case prévue à cet effet et de porter le nombre total de tableaux souscrits en bas à droite de cette même case.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 1599, width: 438, height: 13, 'font-size': 14, }}>
<span>
(2) Indiquer : M pour Monsieur, MME pour Madame ou MLE pour Mademoiselle.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 1615, width: 430, height: 13, 'font-size': 14, }}>
<span>
* Des explications concernant cette rubrique sont données dans la notice n° 2032.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 385, top: 168, width: 499, height: 16, 'font-size': 16, }}>
<span>
(liste des personnes ou groupements de personnes de droit ou de fait
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 416, top: 187, width: 436, height: 15, 'font-size': 16, }}>
<span>
détenant directement au moins 10 % du capital de la société)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 135, width: 99, height: 12, 'font-size': 12, }}>
<span>
Formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 111, top: 147, width: 133, height: 11, 'font-size': 12, }}>
<span>
(art. 38 de l’ann. III au C.G.I.)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 308, top: 116, width: 24, height: 17, 'font-size': 23, }}>
<span>
17
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 564, width: 388, height: 15, 'font-size': 18, }}>
<span>
I - CAPITAL DÉTENU PAR LES PERSONNES MORALES :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1029, top: 174, width: 20, height: 14, 'font-size': 16, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 602, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 602, width: 87, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 634, width: 234, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 687, top: 634, width: 90, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 634, width: 134, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 667, width: 54, height: 11, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 667, width: 18, height: 11, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 667, width: 28, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 699, width: 71, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 700, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 700, width: 27, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 747, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 747, width: 87, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 779, width: 234, height: 16, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 780, width: 134, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 687, top: 780, width: 90, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 812, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 812, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 812, width: 28, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 845, width: 71, height: 11, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 845, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 845, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 894, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 894, width: 87, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 927, width: 234, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 687, top: 927, width: 90, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 927, width: 134, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 959, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 960, width: 28, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 959, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 992, width: 71, height: 11, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 992, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 992, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 1038, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 1038, width: 87, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1070, width: 234, height: 16, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 687, top: 1071, width: 90, height: 11, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 1071, width: 134, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1103, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 1103, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 1103, width: 28, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 1136, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1136, width: 71, height: 11, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1136, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1176, width: 409, height: 17, 'font-size': 18, }}>
<span>
II - CAPITAL DÉTENU PAR LES PERSONNES PHYSIQUES :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1219, width: 53, height: 14, 'font-size': 16, }}>
<span>
Titre (2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 1220, width: 117, height: 14, 'font-size': 16, }}>
<span>
Nom patronymique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 823, top: 1220, width: 61, height: 14, 'font-size': 16, }}>
<span>
Prénom(s)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 1252, width: 76, height: 11, 'font-size': 16, }}>
<span>
Nom marital
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 688, top: 1252, width: 89, height: 11, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 1252, width: 134, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1285, width: 66, height: 11, 'font-size': 16, }}>
<span>
Naissance :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1285, width: 29, height: 11, 'font-size': 16, }}>
<span>
Date
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 405, top: 1284, width: 100, height: 15, 'font-size': 16, }}>
<span>
N° Département
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 611, top: 1285, width: 63, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1285, width: 27, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1317, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 1317, width: 28, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1317, width: 54, height: 11, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1349, width: 71, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 1350, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1350, width: 27, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1393, width: 53, height: 14, 'font-size': 16, }}>
<span>
Titre (2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 1394, width: 117, height: 15, 'font-size': 16, }}>
<span>
Nom patronymique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 823, top: 1394, width: 61, height: 14, 'font-size': 16, }}>
<span>
Prénom(s)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 1426, width: 76, height: 12, 'font-size': 16, }}>
<span>
Nom marital
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 688, top: 1426, width: 89, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 1426, width: 134, height: 15, 'font-size': 16, }}>
<span>
Nb de parts ou actions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1459, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 611, top: 1459, width: 63, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 405, top: 1458, width: 100, height: 16, 'font-size': 16, }}>
<span>
N° Département
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1459, width: 66, height: 11, 'font-size': 16, }}>
<span>
Naissance :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1459, width: 29, height: 11, 'font-size': 16, }}>
<span>
Date
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1491, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1491, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 1491, width: 28, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1524, width: 71, height: 11, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 1524, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1524, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 296, width: 140, height: 13, 'font-size': 20, }}>
<span>
EXERCICE CLOS LE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 714, top: 295, width: 21, height: 15, 'font-size': 20, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 740, top: 298, width: 42, height: 11, 'font-size': 16, }}>
<span>
SIRET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 335, width: 256, height: 15, 'font-size': 20, }}>
<span>
DÉNOMINATION DE L’ENTREPRISE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 376, width: 105, height: 17, 'font-size': 20, }}>
<span>
ADRESSE (voie)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 419, width: 103, height: 11, 'font-size': 16, }}>
<span>
CODE POSTAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 563, top: 419, width: 43, height: 11, 'font-size': 16, }}>
<span>
VILLE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 465, width: 439, height: 15, 'font-size': 16, }}>
<span>
Nombre total d’associés ou actionnaires personnes morales de l’entreprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 617, top: 466, width: 15, height: 10, 'font-size': 16, }}>
<span>
P1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 514, width: 444, height: 15, 'font-size': 16, }}>
<span>
Nombre total d’associés ou actionnaires personnes physiques de l’entreprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 617, top: 514, width: 16, height: 11, 'font-size': 16, }}>
<span>
P2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 729, top: 465, width: 298, height: 15, 'font-size': 16, }}>
<span>
Nombre total de parts ou d’actions correspondantes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1084, top: 465, width: 16, height: 12, 'font-size': 16, }}>
<span>
P3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 729, top: 514, width: 298, height: 15, 'font-size': 16, }}>
<span>
Nombre total de parts ou d’actions correspondantes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1084, top: 514, width: 16, height: 11, 'font-size': 16, }}>
<span>
P4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 144, top: 176, width: 73, height: 16, 'font-size': 16, }}>
<span>
N° de dépôt
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1090, top: 187, width: 37, height: 11, 'font-size': 16, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1173, top: 187, width: 6, height: 7, 'font-size': 16, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 1294, width: 11, height: 259, 'font-size': 12, }}>
<span>
N° 2059-F-SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="GS"
                   data-field-id="20037560" data-annot-id="19391536" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="GAK1"
                   data-field-id="20037928" data-annot-id="18912640" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="GAD1"
                   data-field-id="20038264" data-annot-id="18911232" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="GAE1"
                   data-field-id="20038568" data-annot-id="19238096" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="GAH1"
                   data-field-id="20038904" data-annot-id="19238288" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="GAT1"
                   data-field-id="20039240" data-annot-id="19238480" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="GAF1"
                   data-field-id="20039576" data-annot-id="19238672" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="GAA1"
                   data-field-id="20039912" data-annot-id="19238864" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="GR1"
                   data-field-id="20040248" data-annot-id="19491680" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="GAI1"
                   data-field-id="20040728" data-annot-id="19491872" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="GAG1"
                   data-field-id="20041064" data-annot-id="19492064" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="GD1"
                   data-field-id="20041400" data-annot-id="19492256" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="GAJ1"
                   data-field-id="20041736" data-annot-id="19492448" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="GAK2"
                   data-field-id="20042072" data-annot-id="19492640" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="GAD2"
                   data-field-id="20042408" data-annot-id="19492832" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="GAE2"
                   data-field-id="20042744" data-annot-id="19493024" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="GAH2"
                   data-field-id="20043080" data-annot-id="19493216" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="GAT2"
                   data-field-id="20043560" data-annot-id="19493680" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="GAF2"
                   data-field-id="20043896" data-annot-id="19493872" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="GAA2"
                   data-field-id="20044232" data-annot-id="19494064" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="GR2"
                   data-field-id="20044568" data-annot-id="19494256" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="GAI2"
                   data-field-id="20044904" data-annot-id="19494448" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="GAG2"
                   data-field-id="20045240" data-annot-id="19494640" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="GD2"
                   data-field-id="20045576" data-annot-id="19494832" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="GAJ2"
                   data-field-id="20045912" data-annot-id="19495024" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="GAK3"
                   data-field-id="20046248" data-annot-id="19495216" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="GAD3"
                   data-field-id="20046584" data-annot-id="19495408" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GAE3"
                   data-field-id="20046920" data-annot-id="19495600" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="GAH3"
                   data-field-id="20047256" data-annot-id="19495792" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="GAT3"
                   data-field-id="20047592" data-annot-id="19495984" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="GAF3"
                   data-field-id="20047928" data-annot-id="19496176" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="GAA3"
                   data-field-id="20048264" data-annot-id="19496368" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="GR3"
                   data-field-id="20048600" data-annot-id="19496560" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="GAI3"
                   data-field-id="20043416" data-annot-id="19493408" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="GAG3"
                   data-field-id="20049528" data-annot-id="19497280" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="GD3"
                   data-field-id="20049864" data-annot-id="19497472" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GAJ3"
                   data-field-id="20050200" data-annot-id="19497664" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="GAK4"
                   data-field-id="20050536" data-annot-id="19497856" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="GAD4"
                   data-field-id="20050872" data-annot-id="19498048" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="GAE4"
                   data-field-id="20051208" data-annot-id="19498240" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="GAH4"
                   data-field-id="20051544" data-annot-id="19498432" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="GAT4"
                   data-field-id="20051880" data-annot-id="19498624" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GAF4"
                   data-field-id="20052216" data-annot-id="19498816" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="GAA4"
                   data-field-id="20052552" data-annot-id="19499008" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="GR4"
                   data-field-id="20052888" data-annot-id="19499200" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GAI4"
                   data-field-id="20053224" data-annot-id="19499392" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GAG4"
                   data-field-id="20053560" data-annot-id="19499584" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="GD4"
                   data-field-id="20053896" data-annot-id="19499776" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="GAJ4"
                   data-field-id="20054232" data-annot-id="19499968" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="BAK1"
                   data-field-id="20054568" data-annot-id="19500160" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="BAA1"
                   data-field-id="20054904" data-annot-id="19500352" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="BAC1"
                   data-field-id="20037160" data-annot-id="19500544" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="BE1"
                   data-field-id="20055240" data-annot-id="19500736" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="BAD1"
                   data-field-id="20055576" data-annot-id="19500928" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="BAE1"
                   data-field-id="20055912" data-annot-id="19501120" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="BAH1"
                   data-field-id="20056248" data-annot-id="19501312" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="BAF1"
                   data-field-id="20056584" data-annot-id="19501504" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="BGH1"
                   data-field-id="20056920" data-annot-id="19501696" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="BAI1"
                   data-field-id="20057256" data-annot-id="19501888" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="BAG1"
                   data-field-id="20057592" data-annot-id="19502080" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="BGI1"
                   data-field-id="20057928" data-annot-id="19502272" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="BR1"
                   data-field-id="20058264" data-annot-id="19502464" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="BAB1"
                   data-field-id="20058600" data-annot-id="19502656" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="BD1"
                   data-field-id="20058936" data-annot-id="19502848" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="BGJ1"
                   data-field-id="20059272" data-annot-id="19503040" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="BAJ1"
                   data-field-id="20059608" data-annot-id="19485888" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="BAK2"
                   data-field-id="20048904" data-annot-id="19496752" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="BAA2"
                   data-field-id="20060856" data-annot-id="19496944" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="BAB2"
                   data-field-id="20061096" data-annot-id="19504272" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="BD2"
                   data-field-id="20061432" data-annot-id="19504464" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="BGJ2"
                   data-field-id="20061768" data-annot-id="19504656" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="BR2"
                   data-field-id="20062104" data-annot-id="19504848" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="BGI2"
                   data-field-id="20062440" data-annot-id="19505040" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="BAC2"
                   data-field-id="20062776" data-annot-id="19505232" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="BE2"
                   data-field-id="20063112" data-annot-id="19505424" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="BAD2"
                   data-field-id="20063448" data-annot-id="19505616" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="BAE2"
                   data-field-id="20063784" data-annot-id="19505808" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="BAH2"
                   data-field-id="20064120" data-annot-id="19506000" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="BAF2"
                   data-field-id="20065752" data-annot-id="19506192" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="BAI2"
                   data-field-id="20064456" data-annot-id="19506384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="BAG2"
                   data-field-id="20064792" data-annot-id="19506576" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="BAJ2"
                   data-field-id="20065128" data-annot-id="19506768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="BGH2"
                   data-field-id="20066056" data-annot-id="19506960" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field "
                   name="NOM_STEA" data-field-id="20067320" data-annot-id="19507152" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field "
                   name="CP_STEH" data-field-id="20068760" data-annot-id="19507344" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field "
                   name="VILLE_STEI" data-field-id="20070280" data-annot-id="19507536" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field "
                   name="ADR_STED" data-field-id="20067656" data-annot-id="19507728" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field "
                   name="ADR_STEF" data-field-id="20067992" data-annot-id="19507920" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field "
                   name="ADR_STEG" data-field-id="20068328" data-annot-id="19508112" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field "
                   name="SIRET_STET" data-field-id="20070616" data-annot-id="19508688" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="FIN_EX"
                   data-field-id="20071864" data-annot-id="19508880" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="GT"
                   data-field-id="20072200" data-annot-id="19509072" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="GV"
                   data-field-id="20072632" data-annot-id="19509264" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="GU"
                   data-field-id="20072632" data-annot-id="19509456" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="GW"
                   data-field-id="20074376" data-annot-id="19509648" type="text"/>

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2059F;
