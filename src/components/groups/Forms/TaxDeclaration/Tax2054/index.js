import React from 'react';
import { Field } from 'redux-form';

import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { useCompute, sum, setIsNothingness, roundField } from 'helpers/pdfforms';

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";


const notNeededFields = {
  'NOM_STEA': true,
  'RN': true
};

const Tax2054 = (props) => {
  const { formValues, change } = props;

  setIsNothingness(formValues, notNeededFields, 'RN', change);

  // Compute
  useCompute('LN', sum, ['KG', 'KJ', 'KM', 'KP', 'KS', 'KV', 'KY', 'LB', 'LE', 'LH', 'LK'], formValues, change);
  useCompute('LO', sum, ['KH', 'KK', 'KN', 'KQ', 'KT', 'KW', 'KZ', 'LC', 'LF', 'LI', 'LL'], formValues, change);
  useCompute('LP', sum, ['KI', 'KL', 'KO', 'KR', 'KU', 'KX', 'LA', 'LD', 'LG', 'LJ', 'LM'], formValues, change);

  useCompute('LQ', sum, ['PA', 'PD', 'PG', 'PK'], formValues, change);
  useCompute('LR', sum, ['PB', 'PE', 'PH', 'PL'], formValues, change);
  useCompute('LS', sum, ['PC', 'PF', 'PJ', 'PM'], formValues, change);

  useCompute('PN', sum, ['RT', 'KD', 'LN', 'LQ'], formValues, change);
  useCompute('PP', sum, ['RU', 'KE', 'LO', 'LR'], formValues, change);
  useCompute('PQ', sum, ['RV', 'KF', 'LP', 'LS'], formValues, change);

  useCompute('QP', sum, ['QE', 'QF', 'QG', 'QH', 'QJ', 'QK', 'QL', 'QM', 'QN', 'MY', 'NC'], formValues, change);
  useCompute('NG', sum, ['LX', 'MA', 'MD', 'MG', 'MJ', 'MM', 'MP', 'MS', 'MV', 'MZ', 'ND'], formValues, change);
  useCompute('NH', sum, ['LY', 'MB', 'ME', 'MH', 'MK', 'MN', 'MQ', 'MT', 'MW', 'NA', 'NE'], formValues, change);
  useCompute('NI', sum, ['LZ', 'MC', 'MF', 'MI', 'ML', 'MO', 'MR', 'MU', 'MX', 'NB', 'NF'], formValues, change);

  useCompute('RG', sum, ['QQ', 'QU', 'QY', 'RC'], formValues, change);
  useCompute('NJ', sum, ['QR', 'QV', 'QZ', 'RD'], formValues, change);
  useCompute('NK', sum, ['QS', 'QW', 'RA', 'RE'], formValues, change);
  useCompute('RH', sum, ['QT', 'QX', 'RB', 'RF'], formValues, change);

  useCompute('RJ', sum, ['RW', 'QB', 'QP', 'RG'], formValues, change);
  useCompute('RK', sum, ['RX', 'LV', 'NG', 'NJ'], formValues, change);
  useCompute('RL', sum, ['RY', 'LW', 'NH', 'NK'], formValues, change);
  useCompute('RM', sum, ['RZ', 'QD', 'NI', 'RH'], formValues, change);

  // Rendering
  return (
    <div className="tax2054">
      <div id="pdf-document" />
      <form id="acroform" />
      <div className="pdf-page" id="pdf-page-1">
        <div className="pdf-page-inner pdf-page-1">
          <div className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 365, top: 395, width: 20, height: 7, fontSize: 10 }}>
              <span>Dont</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 352, top: 410, width: 46, height: 10, fontSize: 10 }}>
              <span>Composants</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 365, top: 429, width: 21, height: 8, fontSize: 10 }}>
              <span>Dont</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 352, top: 445, width: 47, height: 9, fontSize: 10 }}>
              <span>Composants</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 186, width: 13, height: 226, fontSize: 14 }}>
              <span>(Ne pas reporter le montant des centimes)*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 561, top: 65, width: 218, height: 17, fontSize: 23 }}>
              <span>IMMOBILISATIONS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 182, top: 76, width: 146, height: 12, fontSize: 12 }}>
              <span>Formulaire obligatoire (article 53</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 185, top: 87, width: 140, height: 12, fontSize: 12 }}>
              <span>A du Code général des impôts)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 940, top: 68, width: 199, height: 14, fontSize: 20 }}>
              <span>DGFiP N° 2054-SD 2019</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 141, width: 160, height: 15, fontSize: 16 }}>
              <span>Désignation de l’entreprise</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 425, top: 1655, width: 426, height: 14, fontSize: 14 }}>
              <span>* Des explications concernant cette rubrique sont données dans la notice n° 2032</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 225, width: 13, height: 604, fontSize: 14 }}>
              <span>INCORP.CORPORELLESFINANCIÈRES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 228, width: 223, height: 15, fontSize: 16 }}>
              <span>Frais d’établissement et de développement</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 477, top: 228, width: 64, height: 11, fontSize: 16 }}>
              <span>TOTAL I</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 262, width: 272, height: 16, fontSize: 16 }}>
              <span>Autres postes d’immobilisations incorporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 472, top: 262, width: 69, height: 12, fontSize: 16 }}>
              <span>TOTAL II</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 676, width: 75, height: 11, fontSize: 16 }}>
              <span>TOTAL III</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 467, top: 848, width: 74, height: 12, fontSize: 16 }}>
              <span>TOTAL IV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 297, width: 49, height: 11, fontSize: 16 }}>
              <span>Terrains</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 572, width: 192, height: 16, fontSize: 16 }}>
              <span>Emballages récupérables et divers *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 530, width: 112, height: 12, fontSize: 16 }}>
              <span>Matériel de bureau</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 546, width: 145, height: 15, fontSize: 16 }}>
              <span>et mobilier informatique</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 504, width: 136, height: 15, fontSize: 16 }}>
              <span>Matériel de transport *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 462, width: 165, height: 15, fontSize: 16 }}>
              <span>Installations générales, agencements,</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 477, width: 105, height: 15, fontSize: 16 }}>
              <span>aménagements divers *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 607, width: 220, height: 15, fontSize: 16 }}>
              <span>Immobilisations corporelles en cours</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 641, width: 126, height: 16, fontSize: 16 }}>
              <span>Avances et acomptes</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 711, width: 281, height: 15, fontSize: 16 }}>
              <span>Participations évaluées par mise en équivalence</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 745, width: 124, height: 15, fontSize: 16 }}>
              <span>Autres participations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 779, width: 148, height: 12, fontSize: 16 }}>
              <span>Autres titres immobilisés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 814, width: 251, height: 11, fontSize: 16 }}>
              <span>Prêts et autres immobilisations financières</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 204, top: 881, width: 280, height: 17, fontSize: 16 }}>
              <span>TOTAL GÉNÉRAL (I + II + III + IV)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 183, width: 72, height: 13, fontSize: 17 }}>
              <span>CADRE A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 621, top: 170, width: 87, height: 11, fontSize: 14 }}>
              <span>Valeur brute des</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 615, top: 184, width: 100, height: 10, fontSize: 14 }}>
              <span>immobilisations au</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 617, top: 197, width: 97, height: 10, fontSize: 14 }}>
              <span>début de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 663, top: 212, width: 4, height: 9, fontSize: 12 }}>
              <span>1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 935, top: 161, width: 80, height: 14, fontSize: 14 }}>
              <span>Augmentations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 792, top: 180, width: 160, height: 12, fontSize: 12 }}>
              <span>Consécutives à une réévaluation pratiquée</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 795, top: 191, width: 153, height: 8, fontSize: 12 }}>
              <span>au cours de l’exercice ou résultant d’une</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 834, top: 201, width: 76, height: 12, fontSize: 12 }}>
              <span>mise en équivalence</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 869, top: 212, width: 6, height: 8, fontSize: 12 }}>
              <span>2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1008, top: 185, width: 140, height: 12, fontSize: 12 }}>
              <span>Acquisitions, créations, apports</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1013, top: 196, width: 131, height: 11, fontSize: 12 }}>
              <span>et virements de poste à poste</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1076, top: 212, width: 5, height: 9, fontSize: 12 }}>
              <span>3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 298, top: 183, width: 155, height: 13, fontSize: 17 }}>
              <span>IMMOBILISATIONS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 227, width: 18, height: 14, fontSize: 20 }}>
              <span>CZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 566, top: 262, width: 20, height: 13, fontSize: 20 }}>
              <span>KD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 565, top: 295, width: 21, height: 14, fontSize: 20 }}>
              <span>KG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 330, width: 14, height: 18, fontSize: 20 }}>
              <span>KJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 364, width: 22, height: 14, fontSize: 20 }}>
              <span>KM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 566, top: 403, width: 18, height: 13, fontSize: 20 }}>
              <span>KP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 433, width: 16, height: 14, fontSize: 20 }}>
              <span>KS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 566, top: 472, width: 20, height: 13, fontSize: 20 }}>
              <span>KV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 538, width: 16, height: 13, fontSize: 20 }}>
              <span>LB</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 571, width: 17, height: 14, fontSize: 20 }}>
              <span>LE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 566, top: 606, width: 18, height: 13, fontSize: 20 }}>
              <span>LH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 295, width: 20, height: 14, fontSize: 20 }}>
              <span>KH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 330, width: 21, height: 13, fontSize: 20 }}>
              <span>KK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 364, width: 20, height: 14, fontSize: 20 }}>
              <span>KN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 399, width: 21, height: 17, fontSize: 20 }}>
              <span>KQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 433, width: 19, height: 14, fontSize: 20 }}>
              <span>KT</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 470, width: 23, height: 14, fontSize: 20 }}>
              <span>KW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 502, width: 19, height: 14, fontSize: 20 }}>
              <span>KZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 537, width: 17, height: 13, fontSize: 20 }}>
              <span>LC</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 774, top: 571, width: 16, height: 14, fontSize: 20 }}>
              <span>LF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 775, top: 606, width: 13, height: 13, fontSize: 20 }}>
              <span>LI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 295, width: 15, height: 14, fontSize: 20 }}>
              <span>KI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 330, width: 18, height: 13, fontSize: 20 }}>
              <span>KL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 364, width: 21, height: 14, fontSize: 20 }}>
              <span>KO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 399, width: 20, height: 13, fontSize: 20 }}>
              <span>KR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 433, width: 20, height: 14, fontSize: 20 }}>
              <span>KU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 468, width: 20, height: 13, fontSize: 20 }}>
              <span>KX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 537, width: 19, height: 13, fontSize: 20 }}>
              <span>LD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 571, width: 19, height: 14, fontSize: 20 }}>
              <span>LG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 122, top: 476, width: 9, height: 103, fontSize: 12 }}>
              <span>Autres immobilisations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 502, width: 12, height: 49, fontSize: 12 }}>
              <span>corporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 983, top: 607, width: 12, height: 18, fontSize: 20 }}>
              <span>LJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 641, width: 20, height: 13, fontSize: 20 }}>
              <span>LM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 982, top: 676, width: 15, height: 13, fontSize: 20 }}>
              <span>LP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 566, top: 504, width: 20, height: 14, fontSize: 20 }}>
              <span>KY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 774, top: 227, width: 17, height: 14, fontSize: 20 }}>
              <span>D8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 262, width: 20, height: 13, fontSize: 20 }}>
              <span>KE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 227, width: 17, height: 14, fontSize: 20 }}>
              <span>D9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 262, width: 18, height: 13, fontSize: 20 }}>
              <span>KF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 503, width: 17, height: 14, fontSize: 20 }}>
              <span>LA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 709, width: 17, height: 14, fontSize: 20 }}>
              <span>8G</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 744, width: 16, height: 14, fontSize: 20 }}>
              <span>8U</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 778, width: 14, height: 14, fontSize: 20 }}>
              <span>1P</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 813, width: 14, height: 14, fontSize: 20 }}>
              <span>1T</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 774, top: 709, width: 18, height: 14, fontSize: 20 }}>
              <span>8M</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 775, top: 744, width: 16, height: 14, fontSize: 20 }}>
              <span>8V</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 776, top: 778, width: 15, height: 14, fontSize: 20 }}>
              <span>1R</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 775, top: 813, width: 15, height: 14, fontSize: 20 }}>
              <span>1U</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 982, top: 709, width: 15, height: 14, fontSize: 20 }}>
              <span>8T</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 744, width: 19, height: 14, fontSize: 20 }}>
              <span>8W</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 983, top: 778, width: 13, height: 14, fontSize: 20 }}>
              <span>1S</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 971, width: 13, height: 604, fontSize: 14 }}>
              <span>INCORP.CORPORELLESFINANCIÈRES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 967, width: 108, height: 12, fontSize: 16 }}>
              <span>Frais d’établissement</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 982, width: 108, height: 15, fontSize: 16 }}>
              <span>et de développement</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 301, top: 982, width: 63, height: 12, fontSize: 16 }}>
              <span>TOTAL I</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1001, width: 236, height: 15, fontSize: 16 }}>
              <span>Autres postes d’immobilisations incorporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1016, width: 69, height: 12, fontSize: 16 }}>
              <span>TOTAL II</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 284, top: 1422, width: 75, height: 12, fontSize: 16 }}>
              <span>TOTAL III</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 1595, width: 74, height: 11, fontSize: 16 }}>
              <span>TOTAL IV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1044, width: 47, height: 11, fontSize: 16 }}>
              <span>Terrains</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 1311, width: 137, height: 16, fontSize: 16 }}>
              <span>Emballages récupérables et</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 1327, width: 35, height: 11, fontSize: 16 }}>
              <span>divers*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 1277, width: 157, height: 12, fontSize: 16 }}>
              <span>Matériel de bureau et infor-</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 1292, width: 99, height: 15, fontSize: 16 }}>
              <span>matique, mobilier</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 1250, width: 120, height: 15, fontSize: 16 }}>
              <span>Matériel de transport</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 1208, width: 125, height: 15, fontSize: 16 }}>
              <span>Inst. gales, agencts, amé-</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 223, top: 1223, width: 88, height: 16, fontSize: 16 }}>
              <span>nagements divers</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1174, width: 208, height: 15, fontSize: 16 }}>
              <span>Installations techniques, matériel et outil-</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1189, width: 75, height: 15, fontSize: 16 }}>
              <span>lage industriels</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1353, width: 198, height: 16, fontSize: 16 }}>
              <span>Immobilisations corporelles en cours</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1388, width: 126, height: 15, fontSize: 16 }}>
              <span>Avances et acomptes</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1449, width: 158, height: 15, fontSize: 16 }}>
              <span>Participations évaluées par</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1465, width: 119, height: 15, fontSize: 16 }}>
              <span>mise en équivalence</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1491, width: 124, height: 16, fontSize: 16 }}>
              <span>Autres participations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1526, width: 148, height: 12, fontSize: 16 }}>
              <span>Autres titres immobilisés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1560, width: 226, height: 12, fontSize: 16 }}>
              <span>Prêts et autres immobilisations financières</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 1139, width: 131, height: 15, fontSize: 16 }}>
              <span>Inst. gales, agencts et am. des</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 1155, width: 60, height: 11, fontSize: 16 }}>
              <span>constructions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 1112, width: 90, height: 12, fontSize: 16 }}>
              <span>Sur sol d’autrui</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 1078, width: 85, height: 15, fontSize: 16 }}>
              <span>Sur sol propre</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1627, width: 257, height: 18, fontSize: 16 }}>
              <span>TOTAL GÉNÉRAL (I + II + III + IV)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 929, width: 70, height: 14, fontSize: 17 }}>
              <span>CADRE B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 415, top: 932, width: 114, height: 14, fontSize: 14 }}>
              <span>par virement de poste</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 453, top: 944, width: 38, height: 13, fontSize: 14 }}>
              <span>à poste</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 470, top: 959, width: 4, height: 8, fontSize: 12 }}>
              <span>1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 599, top: 930, width: 132, height: 11, fontSize: 11 }}>
              <span>par cessions à des tiers ou mises</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 615, top: 940, width: 101, height: 8, fontSize: 11 }}>
              <span>hors service ou résultant</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 611, top: 950, width: 108, height: 10, fontSize: 11 }}>
              <span>d’une mise en équivalence</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 959, width: 6, height: 8, fontSize: 12 }}>
              <span>2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1013, top: 908, width: 131, height: 10, fontSize: 10 }}>
              <span>Réévaluation légale 2054_V1_14062019.pdf index1.html index2.html index.js page0.jpg script.js style.css ou évaluation</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1035, top: 918, width: 89, height: 10, fontSize: 10 }}>
              <span>par mise en équivalence</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 542, top: 912, width: 66, height: 10, fontSize: 14 }}>
              <span>Diminutions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 828, top: 915, width: 87, height: 10, fontSize: 14 }}>
              <span>Valeur brute des</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 825, top: 929, width: 93, height: 10, fontSize: 14 }}>
              <span>immobilisations à</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 825, top: 942, width: 94, height: 10, fontSize: 14 }}>
              <span>la fin de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 869, top: 959, width: 6, height: 8, fontSize: 12 }}>
              <span>3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1013, top: 936, width: 131, height: 11, fontSize: 12 }}>
              <span>Valeur d’origine des immobi-</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1022, top: 946, width: 113, height: 9, fontSize: 12 }}>
              <span>lisations en fin d’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1076, top: 960, width: 6, height: 8, fontSize: 12 }}>
              <span>4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 209, top: 929, width: 154, height: 14, fontSize: 17 }}>
              <span>IMMOBILISATIONS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 972, width: 19, height: 17, fontSize: 20 }}>
              <span>CO/</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 1008, width: 18, height: 13, fontSize: 20 }}>
              <span>LV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 1076, width: 22, height: 14, fontSize: 20 }}>
              <span>MA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 1111, width: 22, height: 13, fontSize: 20 }}>
              <span>MD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 1145, width: 23, height: 14, fontSize: 20 }}>
              <span>MG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 1180, width: 16, height: 18, fontSize: 20 }}>
              <span>MJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 1214, width: 23, height: 14, fontSize: 20 }}>
              <span>MM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 565, top: 1249, width: 20, height: 13, fontSize: 20 }}>
              <span>MP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 566, top: 1284, width: 18, height: 14, fontSize: 20 }}>
              <span>MS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 1318, width: 22, height: 13, fontSize: 20 }}>
              <span>MV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 565, top: 1354, width: 20, height: 13, fontSize: 20 }}>
              <span>MZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 1042, width: 18, height: 13, fontSize: 20 }}>
              <span>LY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 1077, width: 20, height: 13, fontSize: 20 }}>
              <span>MB</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 1146, width: 22, height: 13, fontSize: 20 }}>
              <span>MH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 1182, width: 23, height: 13, fontSize: 20 }}>
              <span>MK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 1216, width: 22, height: 14, fontSize: 20 }}>
              <span>MN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 770, top: 1250, width: 23, height: 18, fontSize: 20 }}>
              <span>MQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 1285, width: 21, height: 14, fontSize: 20 }}>
              <span>MT</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 770, top: 1320, width: 25, height: 14, fontSize: 20 }}>
              <span>MW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 1352, width: 20, height: 14, fontSize: 20 }}>
              <span>NA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 1042, width: 17, height: 14, fontSize: 20 }}>
              <span>LZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1077, width: 21, height: 13, fontSize: 20 }}>
              <span>MC</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1111, width: 20, height: 13, fontSize: 20 }}>
              <span>MF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 1146, width: 16, height: 13, fontSize: 20 }}>
              <span>MI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1180, width: 20, height: 13, fontSize: 20 }}>
              <span>ML</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1214, width: 23, height: 14, fontSize: 20 }}>
              <span>MO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1249, width: 22, height: 13, fontSize: 20 }}>
              <span>MR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1285, width: 22, height: 14, fontSize: 20 }}>
              <span>MU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1319, width: 22, height: 13, fontSize: 20 }}>
              <span>MX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1352, width: 19, height: 14, fontSize: 20 }}>
              <span>NB</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1387, width: 19, height: 13, fontSize: 20 }}>
              <span>NF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 1421, width: 16, height: 14, fontSize: 20 }}>
              <span>NI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 1111, width: 75, height: 12, fontSize: 16 }}>
              <span>Constructions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1236, width: 34, height: 11, fontSize: 16 }}>
              <span>Autres</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 1270, width: 81, height: 12, fontSize: 16 }}>
              <span>immobilisations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 1305, width: 55, height: 15, fontSize: 16 }}>
              <span>corporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 1112, width: 21, height: 13, fontSize: 20 }}>
              <span>ME</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 972, width: 22, height: 17, fontSize: 20 }}>
              <span>DO/</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 1009, width: 22, height: 13, fontSize: 20 }}>
              <span>LW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 975, width: 17, height: 14, fontSize: 20 }}>
              <span>D7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 982, top: 1009, width: 16, height: 13, fontSize: 20 }}>
              <span>1X</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 1454, width: 23, height: 17, fontSize: 20 }}>
              <span>O/U</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 1488, width: 24, height: 17, fontSize: 20 }}>
              <span>O/X</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 1525, width: 15, height: 13, fontSize: 20 }}>
              <span>2B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 1559, width: 16, height: 13, fontSize: 20 }}>
              <span>2E</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 1455, width: 18, height: 14, fontSize: 20 }}>
              <span>M7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 1488, width: 24, height: 17, fontSize: 20 }}>
              <span>O/Y</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 775, top: 1525, width: 15, height: 14, fontSize: 20 }}>
              <span>2C</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 775, top: 1560, width: 15, height: 13, fontSize: 20 }}>
              <span>2F</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1454, width: 27, height: 17, fontSize: 20 }}>
              <span>O/W</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1488, width: 23, height: 17, fontSize: 20 }}>
              <span>O/Z</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 1525, width: 17, height: 14, fontSize: 20 }}>
              <span>2D</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 1559, width: 17, height: 14, fontSize: 20 }}>
              <span>2G</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 1594, width: 17, height: 14, fontSize: 20 }}>
              <span>2H</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1626, width: 25, height: 17, fontSize: 20 }}>
              <span>O/M</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 847, width: 18, height: 18, fontSize: 20 }}>
              <span>LQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 880, width: 25, height: 17, fontSize: 20 }}>
              <span>O/G</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 774, top: 847, width: 18, height: 13, fontSize: 20 }}>
              <span>LR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 880, width: 24, height: 17, fontSize: 20 }}>
              <span>O/H</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 982, top: 847, width: 14, height: 14, fontSize: 20 }}>
              <span>LS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 880, width: 18, height: 20, fontSize: 20 }}>
              <span>O/J</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 982, top: 813, width: 16, height: 14, fontSize: 20 }}>
              <span>1V</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1100, top: 142, width: 38, height: 11, fontSize: 16 }}>
              <span>Néant</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1166, top: 135, width: 6, height: 7, fontSize: 16 }}>
              <span>*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 641, width: 19, height: 13, fontSize: 20 }}>
              <span>LK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 676, width: 18, height: 13, fontSize: 20 }}>
              <span>LN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 775, top: 642, width: 16, height: 13, fontSize: 20 }}>
              <span>LL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 676, width: 19, height: 13, fontSize: 20 }}>
              <span>LO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 565, top: 1388, width: 21, height: 13, fontSize: 20 }}>
              <span>ND</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 565, top: 1422, width: 21, height: 14, fontSize: 20 }}>
              <span>NG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 1391, width: 20, height: 13, fontSize: 20 }}>
              <span>NE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 1422, width: 21, height: 14, fontSize: 20 }}>
              <span>NH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 1597, width: 21, height: 14, fontSize: 20 }}>
              <span>NK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 1627, width: 22, height: 17, fontSize: 20 }}>
              <span>O/L</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 1598, width: 15, height: 18, fontSize: 20 }}>
              <span>NJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 564, top: 1625, width: 24, height: 18, fontSize: 20 }}>
              <span>O/K</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 427, width: 176, height: 15, fontSize: 16 }}>
              <span>Installations techniques, matériel</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 442, width: 116, height: 16, fontSize: 16 }}>
              <span>et outillage industriels</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 393, width: 168, height: 15, fontSize: 16 }}>
              <span>Installations générales, agencements*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 408, width: 161, height: 15, fontSize: 16 }}>
              <span>et aménagements des constructions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 366, width: 91, height: 11, fontSize: 16 }}>
              <span>Sur sol d’autrui</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 331, width: 85, height: 15, fontSize: 16 }}>
              <span>Sur sol propre</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 330, width: 12, height: 83, fontSize: 16 }}>
              <span>Constructions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 270, top: 334, width: 67, height: 9, fontSize: 10 }}>
              <span>Dont Composants</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 270, top: 368, width: 67, height: 10, fontSize: 10 }}>
              <span>Dont Composants</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 334, width: 14, height: 13, fontSize: 20 }}>
              <span>L9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 320, width: 6, height: 33, fontSize: 37 }}>
              <span>[</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 348, top: 423, width: 6, height: 32, fontSize: 37 }}>
              <span>[</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 551, top: 389, width: 6, height: 32, fontSize: 37 }}>
              <span>]</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 348, top: 389, width: 6, height: 32, fontSize: 37 }}>
              <span>[</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 552, top: 354, width: 6, height: 33, fontSize: 37 }}>
              <span>]</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 552, top: 320, width: 6, height: 32, fontSize: 37 }}>
              <span>]</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 552, top: 423, width: 6, height: 32, fontSize: 37 }}>
              <span>]</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 355, width: 6, height: 32, fontSize: 37 }}>
              <span>[</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 349, top: 365, width: 17, height: 14, fontSize: 20 }}>
              <span>M1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 404, top: 403, width: 18, height: 13, fontSize: 20 }}>
              <span>M2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 974, width: 15, height: 14, fontSize: 20 }}>
              <span>IN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 1008, width: 15, height: 13, fontSize: 20 }}>
              <span>IO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 1111, width: 13, height: 13, fontSize: 20 }}>
              <span>IR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 1145, width: 11, height: 14, fontSize: 20 }}>
              <span>IS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 1180, width: 13, height: 13, fontSize: 20 }}>
              <span>IT</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 1214, width: 14, height: 14, fontSize: 20 }}>
              <span>IU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 1249, width: 15, height: 14, fontSize: 20 }}>
              <span>IV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 1284, width: 17, height: 14, fontSize: 20 }}>
              <span>IW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 1318, width: 15, height: 13, fontSize: 20 }}>
              <span>IX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 385, top: 1355, width: 22, height: 13, fontSize: 20 }}>
              <span>MY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 1456, width: 14, height: 14, fontSize: 20 }}>
              <span>IZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 1488, width: 16, height: 17, fontSize: 20 }}>
              <span>IO/</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 391, top: 1525, width: 10, height: 13, fontSize: 20 }}>
              <span>I1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 391, top: 1559, width: 11, height: 13, fontSize: 20 }}>
              <span>I2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 387, top: 1388, width: 20, height: 13, fontSize: 20 }}>
              <span>NC</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 1422, width: 14, height: 13, fontSize: 20 }}>
              <span>IY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 1598, width: 11, height: 13, fontSize: 20 }}>
              <span>I3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 1628, width: 12, height: 13, fontSize: 20 }}>
              <span>I4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 404, top: 437, width: 18, height: 14, fontSize: 20 }}>
              <span>M3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 566, top: 1042, width: 18, height: 14, fontSize: 20 }}>
              <span>LX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 1042, width: 12, height: 14, fontSize: 20 }}>
              <span>IP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 1076, width: 16, height: 18, fontSize: 20 }}>
              <span>IQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 1394, width: 11, height: 234, fontSize: 12 }}>
              <span>N° 2054- SD – (SDNC-DGFiP) - Octobre 2017</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 465, top: 67, width: 34, height: 35, fontSize: 47 }}>
              <span>⑤</span>
            </div>
          </div>
          <div className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot pdf-obj-0-1 pdf-obj-fixed acroform-field" id="pdf-obj-0-1" name="RN" type="checkbox" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-2 pdf-obj-fixed acroform-field" id="pdf-obj-0-2" name="RT" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-3 pdf-obj-fixed acroform-field" id="pdf-obj-0-3" name="RR" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-4 pdf-obj-fixed acroform-field" id="pdf-obj-0-4" name="RS" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-5 pdf-obj-fixed acroform-field" id="pdf-obj-0-5" name="LA" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-6 pdf-obj-fixed acroform-field" id="pdf-obj-0-6" name="PE" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-7 pdf-obj-fixed acroform-field" id="pdf-obj-0-7" name="PF" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-8 pdf-obj-fixed acroform-field" id="pdf-obj-0-8" name="PH" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-9 pdf-obj-fixed acroform-field" id="pdf-obj-0-9" name="PJ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-10 pdf-obj-fixed acroform-field" id="pdf-obj-0-10" name="PL" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-11 pdf-obj-fixed acroform-field" id="pdf-obj-0-11" name="PM" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-12 pdf-obj-fixed acroform-field" id="pdf-obj-0-12" name="KY" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-13 pdf-obj-fixed acroform-field" id="pdf-obj-0-13" name="ME" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-14 pdf-obj-fixed acroform-field" id="pdf-obj-0-14" name="QW" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-15 pdf-obj-fixed acroform-field" id="pdf-obj-0-15" name="QX" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-16 pdf-obj-fixed acroform-field" id="pdf-obj-0-16" name="RA" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-17 pdf-obj-fixed acroform-field" id="pdf-obj-0-17" name="RB" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-18 pdf-obj-fixed acroform-field" id="pdf-obj-0-18" name="RE" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-19 pdf-obj-fixed acroform-field" id="pdf-obj-0-19" name="RF" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-20 pdf-obj-fixed acroform-field" id="pdf-obj-0-20" name="QG" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-21 pdf-obj-fixed acroform-field" id="pdf-obj-0-21" name="QD" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-22 pdf-obj-fixed acroform-field" id="pdf-obj-0-22" name="RZ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-23 pdf-obj-fixed acroform-field" id="pdf-obj-0-23" name="LR" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-24 pdf-obj-fixed acroform-field" id="pdf-obj-0-24" name="PP" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-25 pdf-obj-fixed acroform-field" id="pdf-obj-0-25" name="LS" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-26 pdf-obj-fixed acroform-field" id="pdf-obj-0-26" name="PQ" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-27 pdf-obj-fixed acroform-field" id="pdf-obj-0-27" name="NK" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-28 pdf-obj-fixed acroform-field" id="pdf-obj-0-28" name="RL" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-29 pdf-obj-fixed acroform-field" id="pdf-obj-0-29" name="RH" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-30 pdf-obj-fixed acroform-field" id="pdf-obj-0-30" name="RM" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-31 pdf-obj-fixed acroform-field" id="pdf-obj-0-31" name="RU" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-32 pdf-obj-fixed acroform-field" id="pdf-obj-0-32" name="RV" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-33 pdf-obj-fixed acroform-field" id="pdf-obj-0-33" name="KD" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-34 pdf-obj-fixed acroform-field" id="pdf-obj-0-34" name="KE" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-35 pdf-obj-fixed acroform-field" id="pdf-obj-0-35" name="KF" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-36 pdf-obj-fixed acroform-field" id="pdf-obj-0-36" name="KG" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-37 pdf-obj-fixed acroform-field" id="pdf-obj-0-37" name="KH" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-38 pdf-obj-fixed acroform-field" id="pdf-obj-0-38" name="KI" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-39 pdf-obj-fixed acroform-field" id="pdf-obj-0-39" name="KJ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-40 pdf-obj-fixed acroform-field" id="pdf-obj-0-40" name="RP" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-41 pdf-obj-fixed acroform-field" id="pdf-obj-0-41" name="RQ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-42 pdf-obj-fixed acroform-field" id="pdf-obj-0-42" name="KK" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-43 pdf-obj-fixed acroform-field" id="pdf-obj-0-43" name="KL" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-44 pdf-obj-fixed acroform-field" id="pdf-obj-0-44" name="KM" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-45 pdf-obj-fixed acroform-field" id="pdf-obj-0-45" name="KN" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-46 pdf-obj-fixed acroform-field" id="pdf-obj-0-46" name="KO" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-47 pdf-obj-fixed acroform-field" id="pdf-obj-0-47" name="KP" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-48 pdf-obj-fixed acroform-field" id="pdf-obj-0-48" name="KQ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-49 pdf-obj-fixed acroform-field" id="pdf-obj-0-49" name="KR" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-50 pdf-obj-fixed acroform-field" id="pdf-obj-0-50" name="KS" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-51 pdf-obj-fixed acroform-field" id="pdf-obj-0-51" name="KT" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-52 pdf-obj-fixed acroform-field" id="pdf-obj-0-52" name="KU" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-53 pdf-obj-fixed acroform-field" id="pdf-obj-0-53" name="KV" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-54 pdf-obj-fixed acroform-field" id="pdf-obj-0-54" name="KW" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-55 pdf-obj-fixed acroform-field" id="pdf-obj-0-55" name="KX" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-56 pdf-obj-fixed acroform-field" id="pdf-obj-0-56" name="KZ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-57 pdf-obj-fixed acroform-field" id="pdf-obj-0-57" name="LB" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-58 pdf-obj-fixed acroform-field" id="pdf-obj-0-58" name="LC" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-59 pdf-obj-fixed acroform-field" id="pdf-obj-0-59" name="LD" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-60 pdf-obj-fixed acroform-field" id="pdf-obj-0-60" name="LE" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-61 pdf-obj-fixed acroform-field" id="pdf-obj-0-61" name="LF" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-62 pdf-obj-fixed acroform-field" id="pdf-obj-0-62" name="LG" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-63 pdf-obj-fixed acroform-field" id="pdf-obj-0-63" name="LH" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-64 pdf-obj-fixed acroform-field" id="pdf-obj-0-64" name="LI" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-65 pdf-obj-fixed acroform-field" id="pdf-obj-0-65" name="LJ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-66 pdf-obj-fixed acroform-field" id="pdf-obj-0-66" name="LK" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-67 pdf-obj-fixed acroform-field" id="pdf-obj-0-67" name="LL" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-68 pdf-obj-fixed acroform-field" id="pdf-obj-0-68" name="LM" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-69 pdf-obj-fixed acroform-field" id="pdf-obj-0-69" name="LN" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-70 pdf-obj-fixed acroform-field" id="pdf-obj-0-70" name="LO" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-71 pdf-obj-fixed acroform-field" id="pdf-obj-0-71" name="LP" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-72 pdf-obj-fixed acroform-field" id="pdf-obj-0-72" name="PA" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-73 pdf-obj-fixed acroform-field" id="pdf-obj-0-73" name="PB" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-74 pdf-obj-fixed acroform-field" id="pdf-obj-0-74" name="PC" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-75 pdf-obj-fixed acroform-field" id="pdf-obj-0-75" name="PD" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-76 pdf-obj-fixed acroform-field" id="pdf-obj-0-76" name="PG" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-77 pdf-obj-fixed acroform-field" id="pdf-obj-0-77" name="PK" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-78 pdf-obj-fixed acroform-field" id="pdf-obj-0-78" name="LQ" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-79 pdf-obj-fixed acroform-field" id="pdf-obj-0-79" name="PN" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-80 pdf-obj-fixed acroform-field" id="pdf-obj-0-80" name="RW" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-81 pdf-obj-fixed acroform-field" id="pdf-obj-0-81" name="RX" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-82 pdf-obj-fixed acroform-field" id="pdf-obj-0-82" name="RY" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-83 pdf-obj-fixed acroform-field" id="pdf-obj-0-83" name="QB" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-84 pdf-obj-fixed acroform-field" id="pdf-obj-0-84" name="LV" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-85 pdf-obj-fixed acroform-field" id="pdf-obj-0-85" name="LW" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-86 pdf-obj-fixed acroform-field" id="pdf-obj-0-86" name="QE" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-87 pdf-obj-fixed acroform-field" id="pdf-obj-0-87" name="LX" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-88 pdf-obj-fixed acroform-field" id="pdf-obj-0-88" name="LY" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-89 pdf-obj-fixed acroform-field" id="pdf-obj-0-89" name="LZ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-90 pdf-obj-fixed acroform-field" id="pdf-obj-0-90" name="QF" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-91 pdf-obj-fixed acroform-field" id="pdf-obj-0-91" name="MA" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-92 pdf-obj-fixed acroform-field" id="pdf-obj-0-92" name="MB" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-93 pdf-obj-fixed acroform-field" id="pdf-obj-0-93" name="MC" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-94 pdf-obj-fixed acroform-field" id="pdf-obj-0-94" name="MD" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-95 pdf-obj-fixed acroform-field" id="pdf-obj-0-95" name="MF" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-96 pdf-obj-fixed acroform-field" id="pdf-obj-0-96" name="QH" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-97 pdf-obj-fixed acroform-field" id="pdf-obj-0-97" name="MG" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-98 pdf-obj-fixed acroform-field" id="pdf-obj-0-98" name="MH" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-99 pdf-obj-fixed acroform-field" id="pdf-obj-0-99" name="MI" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-100 pdf-obj-fixed acroform-field" id="pdf-obj-0-100" name="QJ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-101 pdf-obj-fixed acroform-field" id="pdf-obj-0-101" name="MJ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-102 pdf-obj-fixed acroform-field" id="pdf-obj-0-102" name="MK" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-103 pdf-obj-fixed acroform-field" id="pdf-obj-0-103" name="ML" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-104 pdf-obj-fixed acroform-field" id="pdf-obj-0-104" name="QK" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-105 pdf-obj-fixed acroform-field" id="pdf-obj-0-105" name="MM" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-106 pdf-obj-fixed acroform-field" id="pdf-obj-0-106" name="MN" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-107 pdf-obj-fixed acroform-field" id="pdf-obj-0-107" name="MO" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-108 pdf-obj-fixed acroform-field" id="pdf-obj-0-108" name="QL" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-109 pdf-obj-fixed acroform-field" id="pdf-obj-0-109" name="MP" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-110 pdf-obj-fixed acroform-field" id="pdf-obj-0-110" name="MQ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-111 pdf-obj-fixed acroform-field" id="pdf-obj-0-111" name="MR" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-112 pdf-obj-fixed acroform-field" id="pdf-obj-0-112" name="QM" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-113 pdf-obj-fixed acroform-field" id="pdf-obj-0-113" name="MS" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-114 pdf-obj-fixed acroform-field" id="pdf-obj-0-114" name="MT" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-115 pdf-obj-fixed acroform-field" id="pdf-obj-0-115" name="MU" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-116 pdf-obj-fixed acroform-field" id="pdf-obj-0-116" name="QN" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-117 pdf-obj-fixed acroform-field" id="pdf-obj-0-117" name="MV" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-118 pdf-obj-fixed acroform-field" id="pdf-obj-0-118" name="MW" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-119 pdf-obj-fixed acroform-field" id="pdf-obj-0-119" name="MX" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-120 pdf-obj-fixed acroform-field" id="pdf-obj-0-120" name="MY" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-121 pdf-obj-fixed acroform-field" id="pdf-obj-0-121" name="MZ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-122 pdf-obj-fixed acroform-field" id="pdf-obj-0-122" name="NA" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-123 pdf-obj-fixed acroform-field" id="pdf-obj-0-123" name="NB" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-124 pdf-obj-fixed acroform-field" id="pdf-obj-0-124" name="NC" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-125 pdf-obj-fixed acroform-field" id="pdf-obj-0-125" name="ND" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-126 pdf-obj-fixed acroform-field" id="pdf-obj-0-126" name="NE" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-127 pdf-obj-fixed acroform-field" id="pdf-obj-0-127" name="NF" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-128 pdf-obj-fixed acroform-field" id="pdf-obj-0-128" name="QP" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-129 pdf-obj-fixed acroform-field" id="pdf-obj-0-129" name="NG" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-130 pdf-obj-fixed acroform-field" id="pdf-obj-0-130" name="NH" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-131 pdf-obj-fixed acroform-field" id="pdf-obj-0-131" name="NI" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-132 pdf-obj-fixed acroform-field" id="pdf-obj-0-132" name="QQ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-133 pdf-obj-fixed acroform-field" id="pdf-obj-0-133" name="QR" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-134 pdf-obj-fixed acroform-field" id="pdf-obj-0-134" name="QS" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-135 pdf-obj-fixed acroform-field" id="pdf-obj-0-135" name="QT" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-136 pdf-obj-fixed acroform-field" id="pdf-obj-0-136" name="QU" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-137 pdf-obj-fixed acroform-field" id="pdf-obj-0-137" name="QV" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-138 pdf-obj-fixed acroform-field" id="pdf-obj-0-138" name="QY" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-139 pdf-obj-fixed acroform-field" id="pdf-obj-0-139" name="QZ" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-140 pdf-obj-fixed acroform-field" id="pdf-obj-0-140" name="RC" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-141 pdf-obj-fixed acroform-field" id="pdf-obj-0-141" name="RD" type="text" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-142 pdf-obj-fixed acroform-field" id="pdf-obj-0-142" name="RG" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-143 pdf-obj-fixed acroform-field" id="pdf-obj-0-143" name="NJ" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-144 pdf-obj-fixed acroform-field" id="pdf-obj-0-144" name="RJ" type="text" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-145 pdf-obj-fixed acroform-field" id="pdf-obj-0-145" name="RK" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="41955976" data-annot-id="41193936" type="text" disabled />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2054;
