import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {sum, useCompute, setIsNothingness} from "helpers/pdfforms";
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';

import './style.scss';

const notNeededFields = {
  'NOM_STEA': true,
  'CP_STEH': true,
  'VILLE_STEI': true,
  'ADR_STEG': true,
  'ADR_STED': true,
  'ADR_STEF': true,
  'SIRET_STET': true,
  'FIN_EX': true,
  'GS': true
};

const Tax2059G = (props) => {
  const { taxDeclarationForm, change  } = props;

  useCompute('BD', sum, ['BA', 'BC'], taxDeclarationForm, change);
  setIsNothingness(taxDeclarationForm, notNeededFields, 'GS', change);

  return (
    <div className="form-tax-declaration-2059g">

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 457, top: 118, width: 345, height: 17, 'font-size': 23, }}>
<span>
FILIALES ET PARTICIPATIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 1507, width: 1067, height: 13, 'font-size': 14, }}>
<span>
(1) Lorsque le nombre de filiales excède le nombre de lignes de l’imprimé, utiliser un ou plusieurs tableaux supplémentaires. Dans ce cas, il convient de numéroter chaque tableau en haut et à gauche de la
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1523, width: 564, height: 13, 'font-size': 14, }}>
<span>
case prévue à cet effet et de porter le nombre total de tableaux souscrits en bas à droite de cette même case.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 1540, width: 409, height: 14, 'font-size': 14, }}>
<span>
* Des explications concernant cette rubrique sont données dans la notice 2032.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 385, top: 168, width: 499, height: 16, 'font-size': 16, }}>
<span>
(liste des personnes ou groupements de personnes de droit ou de fait
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 187, width: 440, height: 15, 'font-size': 16, }}>
<span>
dont la société détient directement au moins 10 % du capital)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 135, width: 99, height: 12, 'font-size': 12, }}>
<span>
Formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 111, top: 147, width: 133, height: 11, 'font-size': 12, }}>
<span>
(art. 38 de l’ann. III au C.G.I.)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 116, width: 24, height: 18, 'font-size': 23, }}>
<span>
18
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 240, width: 140, height: 13, 'font-size': 20, }}>
<span>
EXERCICE CLOS LE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 714, top: 239, width: 21, height: 14, 'font-size': 20, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 740, top: 242, width: 42, height: 11, 'font-size': 16, }}>
<span>
SIRET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 273, width: 241, height: 15, 'font-size': 20, }}>
<span>
DÉNOMINATION DE L’ENTREPRISE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 308, width: 106, height: 18, 'font-size': 20, }}>
<span>
ADRESSE (voie)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 345, width: 103, height: 11, 'font-size': 16, }}>
<span>
CODE POSTAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 345, width: 43, height: 11, 'font-size': 16, }}>
<span>
VILLE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 431, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 432, width: 87, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 464, width: 234, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 464, width: 90, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 496, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 496, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 497, width: 28, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 529, width: 71, height: 11, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 529, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 529, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 567, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 567, width: 87, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 599, width: 234, height: 16, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 600, width: 90, height: 11, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 632, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 632, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 632, width: 28, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 665, width: 71, height: 11, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 665, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 665, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 703, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 703, width: 87, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 735, width: 234, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 735, width: 90, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 768, width: 54, height: 11, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 767, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 768, width: 28, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 800, width: 71, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 801, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 801, width: 27, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 838, width: 94, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 839, width: 87, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 870, width: 234, height: 16, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 871, width: 90, height: 11, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 904, width: 28, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 903, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 903, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 936, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 936, width: 71, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 936, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 974, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 974, width: 87, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1006, width: 234, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 1006, width: 90, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1039, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 1039, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 1039, width: 28, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1072, width: 71, height: 11, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 1072, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1072, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 1110, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 1110, width: 87, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1142, width: 234, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 1142, width: 90, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1175, width: 54, height: 11, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 1174, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 1175, width: 28, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1207, width: 71, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 1207, width: 62, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1208, width: 27, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 1246, width: 87, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 1245, width: 94, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1278, width: 234, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 1278, width: 90, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1310, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 1310, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 1311, width: 28, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1343, width: 71, height: 11, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 1343, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1343, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 1381, width: 94, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 1381, width: 87, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1413, width: 234, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 1414, width: 90, height: 11, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 1446, width: 54, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 443, top: 1446, width: 28, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 1446, width: 18, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1479, width: 71, height: 11, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 1479, width: 62, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 1479, width: 27, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1029, top: 174, width: 20, height: 14, 'font-size': 16, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 144, top: 176, width: 73, height: 16, 'font-size': 16, }}>
<span>
N° de dépôt
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1105, top: 186, width: 38, height: 11, 'font-size': 16, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1170, top: 178, width: 7, height: 7, 'font-size': 16, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 928, top: 102, width: 250, height: 16, 'font-size': 20, }}>
<span>
DGFiP N° 2059-G-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 383, width: 419, height: 13, 'font-size': 20, }}>
<span>
Nombre total de filiales détenues par l’entreprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 695, top: 383, width: 19, height: 13, 'font-size': 20, }}>
<span>
p5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 1236, width: 11, height: 262, 'font-size': 12, }}>
<span>
N° 2059-G-SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="GAK1" data-field-id="31689560" data-annot-id="30645520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="GAT1" data-field-id="31689736" data-annot-id="30644496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="GAD1" data-field-id="31690072" data-annot-id="31212464" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="GAE1" data-field-id="31690376" data-annot-id="31212656" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="GAH1" data-field-id="31690712" data-annot-id="30971552" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="GAF1" data-field-id="31691048" data-annot-id="30971744" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="GAI1" data-field-id="31691384" data-annot-id="30971936" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="GAA1" data-field-id="31691720" data-annot-id="30972128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="GAG1" data-field-id="31692056" data-annot-id="30972320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="GR1" data-field-id="31692536" data-annot-id="31218592" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="GAJ1" data-field-id="31692872" data-annot-id="31218784" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="GAK2" data-field-id="31693208" data-annot-id="31218976" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="GAA2" data-field-id="31693544" data-annot-id="31219168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="GAT2" data-field-id="31693880" data-annot-id="31219360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="GR2" data-field-id="31694216" data-annot-id="31219552" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="GAJ2" data-field-id="31694552" data-annot-id="31219744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="GAG2" data-field-id="31694888" data-annot-id="31219936" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="GAI2" data-field-id="31695368" data-annot-id="31220400" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="GAF2" data-field-id="31695704" data-annot-id="31220592" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="GAE2" data-field-id="31696040" data-annot-id="31220784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="GAD2" data-field-id="31696376" data-annot-id="31220976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="GAH2" data-field-id="31696712" data-annot-id="31221168" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="GAK3" data-field-id="31697048" data-annot-id="31221360" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="GAA3" data-field-id="31697384" data-annot-id="31221552" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="GAT3" data-field-id="31697720" data-annot-id="31221744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="GR3" data-field-id="31698056" data-annot-id="31221936" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="GAJ3" data-field-id="31698392" data-annot-id="31222128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GAG3" data-field-id="31698728" data-annot-id="31222320" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="GAI3" data-field-id="31699064" data-annot-id="31222512" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="GAF3" data-field-id="31699400" data-annot-id="31222704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="GAE3" data-field-id="31699736" data-annot-id="31222896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="GAD3" data-field-id="31700072" data-annot-id="31223088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="GAH3" data-field-id="31700408" data-annot-id="31223280" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="GAK4" data-field-id="31695224" data-annot-id="31220128" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="GAA4" data-field-id="31701336" data-annot-id="31224000" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="GAT4" data-field-id="31701672" data-annot-id="31224192" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GR4" data-field-id="31702008" data-annot-id="31224384" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="GAJ4" data-field-id="31702344" data-annot-id="31224576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="GAG4" data-field-id="31702680" data-annot-id="31224768" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="GAI4" data-field-id="31687960" data-annot-id="31224960" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="GAF4" data-field-id="31688872" data-annot-id="31225152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="GAE4" data-field-id="31689256" data-annot-id="31225344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GAD4" data-field-id="31703016" data-annot-id="31225536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="GAH4" data-field-id="31703304" data-annot-id="31225728" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="GAK5" data-field-id="31703640" data-annot-id="31225920" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GAA5" data-field-id="31703976" data-annot-id="31226112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GAT5" data-field-id="31704312" data-annot-id="31226304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="GR5" data-field-id="31704648" data-annot-id="31226496" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="GAJ5" data-field-id="31704984" data-annot-id="31226688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="GAG5" data-field-id="31705320" data-annot-id="31226880" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="GAI5" data-field-id="31705656" data-annot-id="31227072" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="GAF5" data-field-id="31705992" data-annot-id="31227264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="GAE5" data-field-id="31706328" data-annot-id="31227456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="GAD5" data-field-id="31706664" data-annot-id="31227648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="GAH5" data-field-id="31707000" data-annot-id="31227840" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="GAK6" data-field-id="31707336" data-annot-id="31228032" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="GAA6" data-field-id="31707672" data-annot-id="31228224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="GAT6" data-field-id="31708008" data-annot-id="31228416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="GR6" data-field-id="31708344" data-annot-id="31228608" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="GAJ6" data-field-id="31708680" data-annot-id="31228800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="GAG6" data-field-id="31709016" data-annot-id="31228992" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="GAI6" data-field-id="31709352" data-annot-id="31229184" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="GAF6" data-field-id="31709688" data-annot-id="31229376" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="GAE6" data-field-id="31710024" data-annot-id="31229568" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="GAD6" data-field-id="31710360" data-annot-id="31229760" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="GAH6" data-field-id="31700760" data-annot-id="31223472" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="GAK7" data-field-id="31711608" data-annot-id="31223664" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="GAA7" data-field-id="31711912" data-annot-id="31230992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="GAT7" data-field-id="31712248" data-annot-id="31231184" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="GR7" data-field-id="31712584" data-annot-id="31231376" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="GAJ7" data-field-id="31712920" data-annot-id="31231568" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="GAG7" data-field-id="31713256" data-annot-id="31231760" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="GAI7" data-field-id="31713592" data-annot-id="31231952" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="GAF7" data-field-id="31713928" data-annot-id="31232144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="GAE7" data-field-id="31714264" data-annot-id="31232336" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="GAD7" data-field-id="31714600" data-annot-id="31232528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="GAH7" data-field-id="31714936" data-annot-id="31232720" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="GAK8" data-field-id="31715272" data-annot-id="31232912" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="GAA8" data-field-id="31715608" data-annot-id="31233104" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="GAT8" data-field-id="31715944" data-annot-id="31233296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="GR8" data-field-id="31716280" data-annot-id="31233488" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="GAJ8" data-field-id="31716616" data-annot-id="31233680" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="GAG8" data-field-id="31716952" data-annot-id="31233872" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="GAI8" data-field-id="31717288" data-annot-id="31234064" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="GAF8" data-field-id="31717624" data-annot-id="31234256" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="GAE8" data-field-id="31717960" data-annot-id="31234448" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="GAD8" data-field-id="31718296" data-annot-id="31234640" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="GAH8" data-field-id="31718632" data-annot-id="31234832" type="text" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="GS" data-field-id="31718968" data-annot-id="31235024" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="GT" data-field-id="31719304" data-annot-id="31235216" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STED" data-field-id="35449592" data-annot-id="34962928" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEF" data-field-id="35449928" data-annot-id="34963120" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEG" data-field-id="35450264" data-annot-id="34963312" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field pde-form-field-text" name="CP_STEH" data-field-id="35450600" data-annot-id="34963504" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field pde-form-field-text" name="VILLE_STEI" data-field-id="35450936" data-annot-id="34963696" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="35451272" data-annot-id="34963888" type="text" disabled />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field pde-form-field-text" name="FIN_EX" data-field-id="35451608" data-annot-id="34964080" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field pde-form-field-text" name="SIRET_STET" data-field-id="35451944" data-annot-id="34964272" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2059G;
