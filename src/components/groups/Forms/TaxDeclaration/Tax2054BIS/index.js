import React from 'react';
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from "components/reduxForm/Inputs";
import {sum, useCompute, setIsNothingness, parse as p} from "helpers/pdfforms";

import './style.scss';

const notNeededFields = {
       'NOM_STEA': true,
       'HD': true,
       'FIN_EX': true
     };
   

const Tax2054BIS = (props) => {
  const { formValues, change } = props;
  setIsNothingness(formValues, notNeededFields, 'HD', change);

  // Compute
  useCompute('GA', ({BA, CA, FA}) => p(BA) - p(CA) - p(FA), ['BA', 'CA', 'FA'], formValues, change);
  useCompute('GB', ({BB, CB, FB}) => p(BB) - p(CB) - p(FB), ['BB', 'CB', 'FB'], formValues, change);
  useCompute('GC', ({BC, CC, FC}) => p(BC) - p(CC) - p(FC), ['BC', 'CC', 'FC'], formValues, change);
  useCompute('GD', ({BD, CD, FD}) => p(BD) - p(CD) - p(FD), ['BD', 'CD', 'FD'], formValues, change);
  useCompute('GE', ({BE, CE, FE}) => p(BE) - p(CE) - p(FE), ['BE', 'CE', 'FE'], formValues, change);
  useCompute('GF', ({BF, CF, FF}) => p(BF) - p(CF) - p(FF), ['BF', 'CF', 'FF'], formValues, change);
  useCompute('GG', ({BG, CG, FG}) => p(BG) - p(CG) - p(FG), ['BG', 'CG', 'FG'], formValues, change);
  useCompute('GH', ({BH, CH, FH}) => p(BH) - p(CH) - p(FH), ['BH', 'CH', 'FH'], formValues, change);
  useCompute('GJ', ({BJ, CJ, FJ}) => p(BJ) - p(CJ) - p(FJ), ['BJ', 'CJ', 'FJ'], formValues, change);
  useCompute('GK', ({BK, CK, FK}) => p(BK) - p(CK) - p(FK), ['BK', 'CK', 'FK'], formValues, change);
  useCompute('HC', ({HA, HB}) => p(HA) - p(HB), ['HA', 'HB'], formValues, change);
  useCompute('BK', sum, ['BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BJ'], formValues, change);
  useCompute('CK', sum, ['CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CJ'], formValues, change);
  useCompute('DK', sum, ['DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DJ'], formValues, change);
  useCompute('EK', sum, ['EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EJ'], formValues, change);
  useCompute('FK', sum, ['FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FJ'], formValues, change);

  // Rendering
  return (
    <div className="tax2054">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{left: 337, top: 155, width: 540, height: 23, 'font-size': 26,}}>
<span>
TABLEAU DES ÉCARTS DE RÉÉVALUATION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 353, top: 184, width: 510, height: 18, 'font-size': 26,}}>
<span>
SUR IMMOBILISATIONS AMORTISSABLES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 47, top: 188, width: 166, height: 12, 'font-size': 13,}}>
<span>
Formulaire obligatoire (article 53 A
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 200, width: 134, height: 12, 'font-size': 13,}}>
<span>
du Code général des impôts)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 923, top: 164, width: 239, height: 15, 'font-size': 21,}}>
<span>
DGFiP N° 2054 bis-SD 2019
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 51, top: 326, width: 174, height: 16, 'font-size': 17,}}>
<span>
Désignation de l’entreprise :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 344, top: 398, width: 223, height: 10, 'font-size': 15,}}>
<span>
Détermination du montant des écarts
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 396, top: 415, width: 120, height: 13, 'font-size': 15,}}>
<span>
(col. 1 - col. 2) (1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 341, top: 462, width: 86, height: 14, 'font-size': 15,}}>
<span>
Augmentation
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 335, top: 479, width: 98, height: 10, 'font-size': 15,}}>
<span>
du montant brut
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 324, top: 496, width: 120, height: 10, 'font-size': 15,}}>
<span>
des immobilisations
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 381, top: 524, width: 5, height: 9, 'font-size': 13,}}>
<span>
1
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 485, top: 462, width: 86, height: 14, 'font-size': 15,}}>
<span>
Augmentation
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 494, top: 479, width: 69, height: 10, 'font-size': 15,}}>
<span>
du montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 469, top: 496, width: 118, height: 10, 'font-size': 15,}}>
<span>
des amortissements
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 525, top: 524, width: 6, height: 9, 'font-size': 13,}}>
<span>
2
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 912, top: 462, width: 99, height: 11, 'font-size': 15,}}>
<span>
Montant cumulé
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 902, top: 479, width: 119, height: 10, 'font-size': 15,}}>
<span>
à la fin de l’exercice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 954, top: 496, width: 15, height: 13, 'font-size': 15,}}>
<span>
(4)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 959, top: 524, width: 5, height: 9, 'font-size': 13,}}>
<span>
5
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 1064, top: 426, width: 84, height: 11, 'font-size': 15,}}>
<span>
Montant de la
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 1047, top: 444, width: 118, height: 14, 'font-size': 15,}}>
<span>
provision spéciale à
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 1052, top: 462, width: 108, height: 11, 'font-size': 15,}}>
<span>
la fin de l’exercice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 1059, top: 480, width: 95, height: 14, 'font-size': 15,}}>
<span>
[(col. 1 - col. 2)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 1068, top: 498, width: 76, height: 14, 'font-size': 15,}}>
<span>
- col. 5 (5)]
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 1103, top: 528, width: 6, height: 9, 'font-size': 13,}}>
<span>
6
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 647, top: 480, width: 52, height: 10, 'font-size': 15,}}>
<span>
Montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 623, top: 496, width: 99, height: 14, 'font-size': 15,}}>
<span>
des suppléments
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 613, top: 512, width: 119, height: 14, 'font-size': 15,}}>
<span>
d’amortissement (2)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 670, top: 526, width: 5, height: 9, 'font-size': 13,}}>
<span>
3
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 761, top: 480, width: 112, height: 11, 'font-size': 15,}}>
<span>
Fraction résiduelle
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 762, top: 496, width: 111, height: 14, 'font-size': 15,}}>
<span>
correspondant aux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 762, top: 512, width: 110, height: 14, 'font-size': 15,}}>
<span>
éléments cédés (3)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 814, top: 526, width: 6, height: 9, 'font-size': 13,}}>
<span>
4
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 649, top: 408, width: 336, height: 14, 'font-size': 15,}}>
<span>
Utilisation de la marge supplémentaire d’amortissement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 679, top: 444, width: 131, height: 10, 'font-size': 15,}}>
<span>
Au cours de l’exercice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 55, top: 409, width: 85, height: 13, 'font-size': 20,}}>
<span>
CADRE A
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 542, width: 164, height: 15, 'font-size': 17,}}>
<span>
1 Concessions, brevets et
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 79, top: 557, width: 98, height: 12, 'font-size': 17,}}>
<span>
droits similaires
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 687, width: 163, height: 15, 'font-size': 17,}}>
<span>
5 Installations techniques
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 79, top: 701, width: 140, height: 12, 'font-size': 17,}}>
<span>
mat. et out. industriels
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 722, width: 161, height: 12, 'font-size': 17,}}>
<span>
6 Autres immobilisations
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 79, top: 738, width: 69, height: 15, 'font-size': 17,}}>
<span>
corporelles
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 585, width: 133, height: 12, 'font-size': 17,}}>
<span>
2 Fonds commercial
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 621, width: 68, height: 12, 'font-size': 17,}}>
<span>
3 Terrains
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 657, width: 105, height: 12, 'font-size': 17,}}>
<span>
4 Constructions
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 766, width: 175, height: 12, 'font-size': 17,}}>
<span>
7 Immobilisations en cours
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 802, width: 102, height: 15, 'font-size': 17,}}>
<span>
8 Participations
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 62, top: 838, width: 172, height: 12, 'font-size': 17,}}>
<span>
9 Autres titres immobilisés
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 53, top: 874, width: 14, height: 12, 'font-size': 17,}}>
<span>
10
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 211, top: 874, width: 70, height: 12, 'font-size': 17,}}>
<span>
TOTAUX
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 47, top: 923, width: 999, height: 14, 'font-size': 15,}}>
<span>
(1) Les augmentations du montant brut et des amortissements à inscrire respectivement aux colonnes 1 et 2 sont celles qui ont été apportées au montant des immobilisations
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 82, top: 939, width: 899, height: 14, 'font-size': 15,}}>
<span>
amortissables réévaluées dans les conditions définies à l’article 238 bis J du code général des impôts et figurant à l’actif de l’entreprise au début de l’exercice.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 82, top: 956, width: 596, height: 13, 'font-size': 15,}}>
<span>
Le montant des écarts est obtenu en soustrayant des montants portés colonne 1, ceux portés colonne 2.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 47, top: 984, width: 867, height: 14, 'font-size': 15,}}>
<span>
(2) Porter dans cette colonne le supplémént de dotation de l’exercice aux comptes d’amortissement (compte de résultat) consécutif à la réévaluation.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 47, top: 1012, width: 1115, height: 14, 'font-size': 15,}}>
<span>
(3) Cette colonne ne concerne que les immobilisations réévaluées cédées au cours de l’exercice. Il convie nt d’y reporter, l’année de la cession de l’élément, le solde non
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 82, top: 1028, width: 296, height: 14, 'font-size': 15,}}>
<span>
utilisé de la marge supplémentaire d’amortissement.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 47, top: 1056, width: 172, height: 14, 'font-size': 15,}}>
<span>
(4) Ce montant comprend :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 82, top: 1072, width: 349, height: 14, 'font-size': 15,}}>
<span>
a) le montant total des sommes portées aux colonnes 3 et 4 ;
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 82, top: 1088, width: 949, height: 14, 'font-size': 15,}}>
<span>
b) le montant cumulé à la fin de l’exercice précédent, dans la mesure où ce montant correspond à des éléments figurant à l’actif de l’entreprise au début de l’exercice.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 47, top: 1116, width: 866, height: 14, 'font-size': 15,}}>
<span>
(5) Le montant total de la provision spéciale en fin d’exercice est à reporter au passif du bilan (tableau n° 2051) à la ligne «Provisions réglementées».
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 76, top: 1425, width: 779, height: 14, 'font-size': 15,}}>
<span>
Le cadre B est servi par les seules entreprises qui ont imputé leurs déficits fiscalement reportables au 31 décembre sur la provision spéciale.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 76, top: 1455, width: 977, height: 14, 'font-size': 15,}}>
<span>
Il est rappelé que cette imputation est purement fiscale et ne modifie pas les montants de la provision spéciale figurant au bilan : de même, les entreprises en cause continuent
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 49, top: 1471, width: 641, height: 14, 'font-size': 15,}}>
<span>
à réintégrer chaque année dans leur résultat comptable le supplément d’amortissement consécutif à la réévaluation.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 76, top: 1501, width: 1093, height: 14, 'font-size': 15,}}>
<span>
Ligne 2, inscrire la partie de ce déficit incluse chaque année dans les montants portés aux colonnes 3 et 4 du cadre A. Cette partie est obtenue en multipliant les montants portés aux colonnes 3 et 4 par
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 49, top: 1517, width: 870, height: 14, 'font-size': 15,}}>
<span>
une fraction dont les éléments sont fixés au moment de l’imputation, le numérateur étant le montant du déficit imputé et le dénominateur celui de la provision.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 55, top: 1215, width: 84, height: 14, 'font-size': 20,}}>
<span>
CADRE B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 55, top: 1237, width: 981, height: 16, 'font-size': 18,}}>
<span>
DÉFICITS REPORTABLES AU 31 DÉCEMBRE 1976 IMPUTÉS SUR LA PROVISION SPÉCIALE AU POINT DE VUE FISCAL
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 55, top: 1274, width: 916, height: 15, 'font-size': 17,}}>
<span>
1 - FRACTION INCLUSE DANS LA PROVISION SPÉCIALE AU DÉBUT DE L’EXERCICE ...........................................................
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 55, top: 1309, width: 918, height: 15, 'font-size': 17,}}>
<span>
2 - FRACTION RATTACHÉE AU RÉSULTAT DE L’EXERCICE ........................................................................................................... -
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 55, top: 1344, width: 918, height: 15, 'font-size': 17,}}>
<span>
3 - FRACTION INCLUSE DANS LA PROVISION SPÉCIALE EN FIN D’EXERCICE .................................................................... =
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 187, top: 276, width: 843, height: 16, 'font-size': 17,}}>
<span>
Les entreprises ayant pratiqué la réévaluation légale de leurs immobilisations amortissables (art. 238 bis J du CGI) doivent joindre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 237, top: 292, width: 742, height: 16, 'font-size': 17,}}>
<span>
ce tableau à leur déclaration jusqu’à (et y compris) l’exercice au cours duquel la provision spéciale (col. 6) devient nulle.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 247, top: 168, width: 51, height: 17, 'font-size': 23,}}>
<span>
5 bis
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 823, top: 247, width: 114, height: 12, 'font-size': 17,}}>
<span>
Exercice N clos le
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 1072, top: 326, width: 39, height: 11, 'font-size': 17,}}>
<span>
Néant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 1158, top: 326, width: 7, height: 7, 'font-size': 17,}}>
<span>
*
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 71, top: 1557, width: 463, height: 15, 'font-size': 16,}}>
<span>
* Des explications concernant cette rubrique sont données dans la notice 2032.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 17, top: 1264, width: 11, height: 287, 'font-size': 13,}}>
<span>
N° 2054 BIS- SD – (SDNC-DGFiP) - Novembre 2018
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="HD"
                   data-field-id="26681432" data-annot-id="25433376" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="BA"
                   data-field-id="26686648" data-annot-id="25761648" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="CA"
                   data-field-id="26686920" data-annot-id="25756192" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="DA"
                   data-field-id="26687224" data-annot-id="25756384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="BB"
                   data-field-id="26687560" data-annot-id="25756576" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="CB"
                   data-field-id="26689304" data-annot-id="25756768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="CB"
                   data-field-id="26689304" data-annot-id="25756960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="BC"
                   data-field-id="26688248" data-annot-id="25905344" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="CC"
                   data-field-id="26688552" data-annot-id="25905536" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="DC"
                   data-field-id="26689928" data-annot-id="25905728" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="BD"
                   data-field-id="26690408" data-annot-id="25905920" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="BE"
                   data-field-id="26690744" data-annot-id="25906112" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="CD"
                   data-field-id="26691080" data-annot-id="25906304" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="DD"
                   data-field-id="26691416" data-annot-id="25906496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="CE"
                   data-field-id="26691752" data-annot-id="25906688" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="DE"
                   data-field-id="26692088" data-annot-id="25906880" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="BF"
                   data-field-id="26692424" data-annot-id="25907072" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="CF"
                   data-field-id="26692760" data-annot-id="25907536" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="DF"
                   data-field-id="26693240" data-annot-id="25907728" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="BG"
                   data-field-id="26693576" data-annot-id="25907920" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="CG"
                   data-field-id="26693912" data-annot-id="25908112" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="DG"
                   data-field-id="26694248" data-annot-id="25908304" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="BH"
                   data-field-id="26694584" data-annot-id="25908496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="CH"
                   data-field-id="26694920" data-annot-id="25908688" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="DH"
                   data-field-id="26695256" data-annot-id="25908880" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="BJ"
                   data-field-id="26695592" data-annot-id="25909072" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="CJ"
                   data-field-id="26695928" data-annot-id="25909264" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="DJ"
                   data-field-id="26696264" data-annot-id="25909456" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="BK"
                   data-field-id="26696600" data-annot-id="25909648" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="CK"
                   data-field-id="26696936" data-annot-id="25909840" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="DK"
                   data-field-id="26697272" data-annot-id="25910032" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="EA"
                   data-field-id="26697608" data-annot-id="25910224" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="EB"
                   data-field-id="26697944" data-annot-id="25910416" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="FA"
                   data-field-id="26698280" data-annot-id="25907264" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="GA"
                   data-field-id="26693096" data-annot-id="25911136" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="FB"
                   data-field-id="26699208" data-annot-id="25911328" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GB"
                   data-field-id="26699544" data-annot-id="25911520" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="EC"
                   data-field-id="26699880" data-annot-id="25911712" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="FC"
                   data-field-id="26700216" data-annot-id="25911904" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="GC"
                   data-field-id="26700552" data-annot-id="25912096" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="ED"
                   data-field-id="26700888" data-annot-id="25912288" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="FD"
                   data-field-id="26701224" data-annot-id="25912480" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GD"
                   data-field-id="26701560" data-annot-id="25912672" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="EE"
                   data-field-id="26701896" data-annot-id="25912864" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="FE"
                   data-field-id="26702232" data-annot-id="25913056" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GE"
                   data-field-id="26702568" data-annot-id="25913248" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="EF"
                   data-field-id="26702904" data-annot-id="25913440" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="FF"
                   data-field-id="26703240" data-annot-id="25913632" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="GF"
                   data-field-id="26703576" data-annot-id="25913824" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="EG"
                   data-field-id="26703912" data-annot-id="25914016" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="FG"
                   data-field-id="26704248" data-annot-id="25914208" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="GG"
                   data-field-id="26704584" data-annot-id="25914400" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="EH"
                   data-field-id="26704920" data-annot-id="25914592" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="FH"
                   data-field-id="26705256" data-annot-id="25914784" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="GH"
                   data-field-id="26705592" data-annot-id="25914976" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="EJ"
                   data-field-id="26705928" data-annot-id="25915168" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="FJ"
                   data-field-id="26706264" data-annot-id="25915360" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="GJ"
                   data-field-id="26706600" data-annot-id="25915552" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="EK"
                   data-field-id="26706936" data-annot-id="25915744" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="FK"
                   data-field-id="26707272" data-annot-id="25915936" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="GK"
                   data-field-id="26707608" data-annot-id="25916128" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="HA"
                   data-field-id="26707944" data-annot-id="25916320" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="HB"
                   data-field-id="26708280" data-annot-id="25916512" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="HC"
                   data-field-id="26708616" data-annot-id="25916704" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field "
                   name="NOM_STEA" data-field-id="26708952" data-annot-id="25916896" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="FIN_EX"
                   data-field-id="26709288" data-annot-id="25899712" type="text"/>

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2054BIS;
