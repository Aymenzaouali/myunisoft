import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {sum, useCompute, parse as p, setIsNothingness} from "helpers/pdfforms";

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const amountOfTheEnd = (values, fields) => (
  p(values[fields[0]]) - p(values[fields[1]])
);

const notNeededFields = {
  'NOM_STEA': true,
  'KF': true
};

const Tax2059D = (props) => {
  const { taxDeclarationForm, change } = props;

  setIsNothingness(taxDeclarationForm, notNeededFields, 'KF', change);

  useCompute('BD', sum, ['BA', 'BC'], taxDeclarationForm, change);
  useCompute('CD', sum, ['CA', 'CC'], taxDeclarationForm, change);
  useCompute('DD', sum, ['DA', 'DC'], taxDeclarationForm, change);
  useCompute('LD', sum, ['LA', 'LC'], taxDeclarationForm, change);
  useCompute('FD', sum, ['FA', 'FC'], taxDeclarationForm, change);

  useCompute('BG', sum, ['BE', 'BF'], taxDeclarationForm, change);
  useCompute('CG', sum, ['CE', 'CF'], taxDeclarationForm, change);
  useCompute('DG', sum, ['DE', 'DF'], taxDeclarationForm, change);
  useCompute('LG', sum, ['LE', 'LF'], taxDeclarationForm, change);
  useCompute('FG', sum, ['FE', 'FF'], taxDeclarationForm, change);

  useCompute('BH', amountOfTheEnd, ['BD', 'BG'], taxDeclarationForm, change);
  useCompute('CH', amountOfTheEnd, ['CD', 'CG'], taxDeclarationForm, change);
  useCompute('DH', amountOfTheEnd, ['DD', 'DG'], taxDeclarationForm, change);
  useCompute('LH', amountOfTheEnd, ['LD', 'LG'], taxDeclarationForm, change);
  useCompute('FH', amountOfTheEnd, ['FD', 'FG'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2059d">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 68, top: 148, width: 102, height: 12, 'font-size': 12, }}>
<span>
formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 161, width: 97, height: 12, 'font-size': 12, }}>
<span>
(article 53A du Code
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 174, width: 92, height: 12, 'font-size': 12, }}>
<span>
général des Impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 231, width: 165, height: 15, 'font-size': 16, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 285, width: 917, height: 17, 'font-size': 18, }}>
<span>
SITUATION DU COMPTE AFFECTÉ À L’ENREGISTREMENT DE LA RÉSERVE SPÉCIALE POUR L’EXERCICE N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 664, top: 323, width: 369, height: 13, 'font-size': 14, }}>
<span>
Sous-comptes de la réserve spéciale des plus-values à long terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 548, top: 360, width: 73, height: 10, 'font-size': 14, }}>
<span>
taxées à 10 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 679, top: 360, width: 73, height: 11, 'font-size': 14, }}>
<span>
taxées à 15 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 811, top: 360, width: 73, height: 10, 'font-size': 14, }}>
<span>
taxées à 18 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 943, top: 360, width: 73, height: 11, 'font-size': 14, }}>
<span>
taxées à 19 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1074, top: 360, width: 74, height: 11, 'font-size': 14, }}>
<span>
taxées à 25 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 384, width: 181, height: 15, 'font-size': 16, }}>
<span>
Montant de la réserve spéciale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 398, width: 259, height: 15, 'font-size': 16, }}>
<span>
à la clôture de l’exercice précédent (N - 1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 423, width: 291, height: 15, 'font-size': 16, }}>
<span>
Réserves figurant au bilan des sociétés absorbées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 437, width: 127, height: 12, 'font-size': 16, }}>
<span>
au cours de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 594, width: 181, height: 15, 'font-size': 16, }}>
<span>
Montant de la réserve spéciale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 608, width: 142, height: 11, 'font-size': 16, }}>
<span>
à la clôture de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 300, top: 600, width: 109, height: 15, 'font-size': 16, }}>
<span>
(ligne 3 - ligne 6)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 348, top: 463, width: 131, height: 16, 'font-size': 16, }}>
<span>
TOTAL (lignes 1 et 2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 348, top: 569, width: 131, height: 15, 'font-size': 16, }}>
<span>
TOTAL (lignes 4 et 5)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 236, top: 490, width: 173, height: 15, 'font-size': 16, }}>
<span>
- donnant lieu à complément
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 245, top: 504, width: 139, height: 15, 'font-size': 16, }}>
<span>
d’impôt sur les sociétés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 236, top: 525, width: 215, height: 15, 'font-size': 16, }}>
<span>
- ne donnant pas lieu à complément
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 245, top: 538, width: 139, height: 15, 'font-size': 16, }}>
<span>
d’impôt sur les sociétés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 527, width: 122, height: 15, 'font-size': 16, }}>
<span>
Prélèvements opérés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 285, width: 4, height: 15, 'font-size': 20, }}>
<span>
I
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 214, top: 486, width: 9, height: 67, 'font-size': 71, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 393, width: 7, height: 13, 'font-size': 20, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 500, top: 427, width: 9, height: 14, 'font-size': 20, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 462, width: 8, height: 13, 'font-size': 20, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 496, width: 9, height: 14, 'font-size': 20, }}>
<span>
4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 531, width: 8, height: 13, 'font-size': 20, }}>
<span>
5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 565, width: 8, height: 14, 'font-size': 20, }}>
<span>
6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 600, width: 8, height: 13, 'font-size': 20, }}>
<span>
7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 844, width: 434, height: 14, 'font-size': 14, }}>
<span>
* Des explications concernant cette rubrique sont données dans la notice n° 2032.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 395, top: 110, width: 462, height: 17, 'font-size': 19, }}>
<span>
RÉSERVE SPÉCIALE DES PLUS-VALUES À LONG TERME
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 326, top: 132, width: 600, height: 17, 'font-size': 19, }}>
<span>
RÉSERVE SPÉCIALE DES PROVISIONS POUR FLUCTUATION DES COURS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 943, top: 125, width: 237, height: 16, 'font-size': 20, }}>
<span>
DGFiP N° 2059-D-SD 201 9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 379, top: 168, width: 494, height: 17, 'font-size': 18, }}>
<span>
(personnes morales soumises à l’impôt sur les sociétés seulement)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 633, width: 735, height: 21, 'font-size': 20, }}>
<span>
II RÉSERVE SPÉCIALE DES PROVISIONS POUR FLUCTUATION DES COURS* (5 , 6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 640, width: 5, height: 6, 'font-size': 9, }}>
<span>
e
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 816, top: 638, width: 25, height: 15, 'font-size': 9, }}>
<span>
e, 7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 842, top: 640, width: 5, height: 6, 'font-size': 9, }}>
<span>
e
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 852, top: 637, width: 266, height: 17, 'font-size': 18, }}>
<span>
alinéas de l’art. 39-1-5 du CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1043, top: 640, width: 5, height: 6, 'font-size': 9, }}>
<span>
e
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 297, top: 678, width: 207, height: 16, 'font-size': 16, }}>
<span>
réserve figurant au bilan des sociétés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 316, top: 695, width: 169, height: 11, 'font-size': 16, }}>
<span>
absorbées au cours de l’année
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 118, top: 678, width: 122, height: 12, 'font-size': 16, }}>
<span>
montant de la réserve
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 695, width: 144, height: 11, 'font-size': 16, }}>
<span>
à l’ouverture de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 642, top: 675, width: 181, height: 15, 'font-size': 16, }}>
<span>
montants prélevés sur la réserve
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 586, top: 704, width: 71, height: 11, 'font-size': 16, }}>
<span>
donnant lieu
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 548, top: 719, width: 129, height: 15, 'font-size': 16, }}>
<span>
à complément d’impôt
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 681, top: 715, width: 14, height: 15, 'font-size': 20, }}>
<span>
D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 789, top: 703, width: 109, height: 15, 'font-size': 16, }}>
<span>
ne donnant pas lieu
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 720, width: 126, height: 15, 'font-size': 16, }}>
<span>
à complément d’impôt
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 901, top: 716, width: 14, height: 15, 'font-size': 20, }}>
<span>
F
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1003, top: 677, width: 123, height: 11, 'font-size': 16, }}>
<span>
montant de la réserve
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 997, top: 693, width: 135, height: 11, 'font-size': 16, }}>
<span>
à la clôture de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1094, top: 234, width: 37, height: 11, 'font-size': 16, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1159, top: 226, width: 7, height: 7, 'font-size': 16, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 165, top: 714, width: 15, height: 15, 'font-size': 20, }}>
<span>
Q
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 391, top: 714, width: 15, height: 15, 'font-size': 20, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1059, top: 714, width: 14, height: 15, 'font-size': 20, }}>
<span>
G
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 278, top: 118, width: 21, height: 16, 'font-size': 21, }}>
<span>
15
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1254, width: 11, height: 262, 'font-size': 12, }}>
<span>
N° 2059-D-SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="KF" data-field-id="12387560" data-annot-id="11841168" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="BA" data-field-id="12397416" data-annot-id="12033744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="CA" data-field-id="12397752" data-annot-id="12033968" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="BH" data-field-id="12399496" data-annot-id="12034160" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="BG" data-field-id="12399832" data-annot-id="12039952" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="BF" data-field-id="12400248" data-annot-id="12040144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="BE" data-field-id="12400584" data-annot-id="12040336" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="BD" data-field-id="12400920" data-annot-id="12040528" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="BC" data-field-id="12401256" data-annot-id="12040720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="CC" data-field-id="12401736" data-annot-id="12040912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="CH" data-field-id="12402072" data-annot-id="12041104" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="CF" data-field-id="12402408" data-annot-id="12041296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="CE" data-field-id="12402744" data-annot-id="12041488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="CG" data-field-id="12403080" data-annot-id="12041680" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="CD" data-field-id="12403416" data-annot-id="12041872" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="DH" data-field-id="12403752" data-annot-id="12042064" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="DG" data-field-id="12404088" data-annot-id="12042256" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="DF" data-field-id="12404568" data-annot-id="12042720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="DE" data-field-id="12404904" data-annot-id="12042912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="DD" data-field-id="12405240" data-annot-id="12043104" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="DC" data-field-id="12405576" data-annot-id="12043296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="DA" data-field-id="12405912" data-annot-id="12043488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="LH" data-field-id="12406248" data-annot-id="12043680" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="LG" data-field-id="12406584" data-annot-id="12043872" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="LF" data-field-id="12406920" data-annot-id="12044064" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="LE" data-field-id="12407256" data-annot-id="12044256" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="LD" data-field-id="12407592" data-annot-id="12044448" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="LC" data-field-id="12407928" data-annot-id="12044640" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="LA" data-field-id="12408264" data-annot-id="12044832" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="FH" data-field-id="12408600" data-annot-id="12045024" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="FG" data-field-id="12408936" data-annot-id="12045216" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="FF" data-field-id="12409272" data-annot-id="12045408" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="FE" data-field-id="12409608" data-annot-id="12045600" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="FD" data-field-id="12404424" data-annot-id="12042448" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="FC" data-field-id="12410536" data-annot-id="12046320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="FA" data-field-id="12410872" data-annot-id="12046512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="KA" data-field-id="12411208" data-annot-id="12046704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="KB" data-field-id="12411544" data-annot-id="12046896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="KC" data-field-id="12411880" data-annot-id="12047088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="KD" data-field-id="12412216" data-annot-id="12047280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="KE" data-field-id="12412552" data-annot-id="12047472" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="27406888" data-annot-id="27040416" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2059D;
