import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {sum, useCompute, parse as p, setValue, sumOfValues, setIsNothingness} from "helpers/pdfforms";
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import { get as _get } from 'lodash';

import './style.scss';

const amountOfTheEnd = (values, fields) => (
  p(values[fields[0]]) + p(values[fields[1]]) - p(values[fields[2]])
);

const getAnotherValue = (values, fields) => (
  p(values[fields[0]])
);

const notNeededFields = {
  'NOM_STEA': true,
  'DEB_EX': true,
  'FIN_EX': true,
  'NBR_M_EX': true,
  'DB': true
};

const Tax2059E = (props) => {
  const { taxDeclarationForm, form2052, change } = props;
  const FO = _get(form2052, 'FO', null);
  const FS = _get(form2052, 'FS', null);
  const FU = _get(form2052, 'FU', null);
  const FL = _get(form2052, 'FL', null);
  const FN = _get(form2052, 'FN', null);

  setIsNothingness(taxDeclarationForm, notNeededFields, 'DB', change);


  useCompute('AF', sum, ['AJ', 'EN', 'EP', 'AE'], taxDeclarationForm, change);
  useCompute('AN', sum, ['EL', 'EE', 'EF', 'AK', 'EQ', 'GD'], taxDeclarationForm, change);
  useCompute('AP', sum, ['AL', 'AM', 'FE', 'FJ', 'FH', 'FK', 'GC', 'FN', 'CM'], taxDeclarationForm, change);
  useCompute('GA', amountOfTheEnd, ['AF', 'AN', 'AP'], taxDeclarationForm, change);

  useCompute('KD', getAnotherValue, ['AF'], taxDeclarationForm, change);

  setValue(taxDeclarationForm, 'EF', FO, change);
  setValue(taxDeclarationForm, 'Al', sumOfValues([FS, FU]), change);
  setValue(taxDeclarationForm, 'AJ', FL, change);
  setValue(taxDeclarationForm, 'EE', FN, change);

  return (
    <div className="form-tax-declaration-2059e">

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 930, top: 82, width: 213, height: 15, 'font-size': 19, }}>
<span>
DGFiP N°2059-E-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 134, width: 732, height: 16, 'font-size': 17, }}>
<span>
Désignation de l&#39;entreprise: ........................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1041, top: 137, width: 42, height: 13, 'font-size': 17, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1112, top: 134, width: 24, height: 16, 'font-size': 21, }}>
<span>
o*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 168, width: 479, height: 13, 'font-size': 17, }}>
<span>
Exercice ouvert le: .............................. et clos le: ..............................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 851, top: 168, width: 205, height: 13, 'font-size': 17, }}>
<span>
Données en nombre de mois
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 194, width: 242, height: 16, 'font-size': 17, }}>
<span>
DÉCLARATION DES EFFECTIFS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 227, width: 217, height: 16, 'font-size': 17, }}>
<span>
Effectif moyen du personnel * :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1001, top: 227, width: 21, height: 13, 'font-size': 17, }}>
<span>
YP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 200, top: 257, width: 103, height: 15, 'font-size': 17, }}>
<span>
Dont apprentis
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1001, top: 257, width: 20, height: 12, 'font-size': 17, }}>
<span>
YF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 200, top: 286, width: 119, height: 16, 'font-size': 17, }}>
<span>
Dont handicapés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1001, top: 286, width: 23, height: 13, 'font-size': 17, }}>
<span>
YG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 316, width: 265, height: 13, 'font-size': 17, }}>
<span>
Effectifs affectés à l&#39;activité artisanale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 316, width: 20, height: 12, 'font-size': 17, }}>
<span>
RL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 342, width: 260, height: 16, 'font-size': 17, }}>
<span>
CALCUL DE LA VALEUR AJOUTÉE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 375, width: 293, height: 13, 'font-size': 17, }}>
<span>
I - Chiffre d&#39;affaires de référence CVAE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 404, width: 499, height: 16, 'font-size': 17, }}>
<span>
Ventes de produits fabriqués, prestations de services et marchandises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 404, width: 23, height: 13, 'font-size': 17, }}>
<span>
OA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 434, width: 443, height: 16, 'font-size': 17, }}>
<span>
Redevances pour concessions, brevets, licences et assimilées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 434, width: 23, height: 13, 'font-size': 17, }}>
<span>
OK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 464, width: 831, height: 16, 'font-size': 17, }}>
<span>
Plus-values de cession d&#39;immobilisations corporelles ou i ncorporelles si rattachées à une activité normale et cour ante
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 464, width: 21, height: 13, 'font-size': 17, }}>
<span>
OL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 493, width: 472, height: 16, 'font-size': 17, }}>
<span>
Refacturations de frais inscrites au compte de transfert de charges
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 493, width: 22, height: 13, 'font-size': 17, }}>
<span>
OT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 923, top: 523, width: 102, height: 13, 'font-size': 17, }}>
<span>
TOTAL 1 OX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 553, width: 470, height: 17, 'font-size': 17, }}>
<span>
II - Autres produits à retenir pour le calcul de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 583, width: 710, height: 16, 'font-size': 17, }}>
<span>
Autres produits de gestion courante (hors quotes-parts de résultat sur opérations faites en commun)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 583, width: 23, height: 13, 'font-size': 17, }}>
<span>
OH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 613, width: 685, height: 16, 'font-size': 17, }}>
<span>
Production immobilisée à hauteur des seules charges déd uctibles ayant concouru à sa formation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 613, width: 22, height: 13, 'font-size': 17, }}>
<span>
OE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 642, width: 237, height: 16, 'font-size': 17, }}>
<span>
Subventions d&#39;exploitation reçues
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 642, width: 21, height: 13, 'font-size': 17, }}>
<span>
OF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 672, width: 202, height: 16, 'font-size': 17, }}>
<span>
Variation positive des stocks
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 672, width: 23, height: 13, 'font-size': 17, }}>
<span>
OD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 701, width: 386, height: 17, 'font-size': 17, }}>
<span>
Transferts de charges déductibles de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 701, width: 15, height: 13, 'font-size': 17, }}>
<span>
OI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 731, width: 586, height: 16, 'font-size': 17, }}>
<span>
Rentrées sur créances amorties lorsqu&#39;elles se rapportent au résultat d&#39;exploitation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1001, top: 731, width: 21, height: 13, 'font-size': 17, }}>
<span>
XT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 923, top: 761, width: 103, height: 13, 'font-size': 17, }}>
<span>
TOTAL 2 OM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 794, width: 420, height: 16, 'font-size': 17, }}>
<span>
III - Charges à retenir pour le calcul de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 488, top: 790, width: 14, height: 12, 'font-size': 12, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 825, width: 49, height: 13, 'font-size': 17, }}>
<span>
Achats
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 825, width: 22, height: 13, 'font-size': 17, }}>
<span>
ON
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 854, width: 209, height: 16, 'font-size': 17, }}>
<span>
Variation négative des stocks
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 854, width: 24, height: 14, 'font-size': 17, }}>
<span>
OQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 884, width: 446, height: 16, 'font-size': 17, }}>
<span>
Services extérieurs, à l&#39;exception des loyers et des redevances
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 884, width: 23, height: 13, 'font-size': 17, }}>
<span>
OR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 913, width: 961, height: 17, 'font-size': 17, }}>
<span>
Loyers et redevances, à l&#39;exception de ceux afférents à des immobilisations corporelles mises à disposition dan s le cadre d&#39;une OS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 932, width: 754, height: 16, 'font-size': 17, }}>
<span>
convention de location-gérance ou de crédit-bail ou encore d&#39;une convention de location de plus de 6 mois
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 962, width: 274, height: 16, 'font-size': 17, }}>
<span>
Taxes déductibles de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 961, width: 21, height: 13, 'font-size': 17, }}>
<span>
OZ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 991, width: 710, height: 16, 'font-size': 17, }}>
<span>
Autres charges de gestion courante (hors quotes-parts de résultat sur opérations faites en commun)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1001, top: 991, width: 28, height: 13, 'font-size': 17, }}>
<span>
OW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1020, width: 622, height: 17, 'font-size': 17, }}>
<span>
Charges déductibles de la valeur ajoutée afférente à la production immobilisée déclarée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 1020, width: 22, height: 14, 'font-size': 17, }}>
<span>
OU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1050, width: 926, height: 14, 'font-size': 15, }}>
<span>
Fraction déductible de la valeur ajoutée des dotations aux amortissements afférentes à des immobilisations corporelles mises à disposition dans le
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1066, width: 737, height: 14, 'font-size': 15, }}>
<span>
cadre d&#39;une convention de location-gérance ou de crédit-bail ou encore d&#39;une convention de location de plus de 6 mois
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 1050, width: 20, height: 13, 'font-size': 17, }}>
<span>
O9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1094, width: 843, height: 16, 'font-size': 17, }}>
<span>
Moins-values de cession d&#39;immobilisations corporelles ou i ncorporelles si rattachées à une activité normale et courante
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 1093, width: 23, height: 14, 'font-size': 17, }}>
<span>
OY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 923, top: 1123, width: 98, height: 13, 'font-size': 17, }}>
<span>
TOTAL 3 OJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1153, width: 207, height: 16, 'font-size': 17, }}>
<span>
IV - Valeur ajoutée produite
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1182, width: 192, height: 17, 'font-size': 17, }}>
<span>
Calcul de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 814, top: 1182, width: 212, height: 17, 'font-size': 17, }}>
<span>
(total 1 + total 2 - total 3) OG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1212, width: 386, height: 16, 'font-size': 17, }}>
<span>
V - Cotisation sur la valeur ajoutée des entreprises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1242, width: 504, height: 16, 'font-size': 17, }}>
<span>
Valeur ajoutée assujettie à la CVAE (à reporter sur les formulaires n
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 1242, width: 10, height: 6, 'font-size': 10, }}>
<span>
os
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 585, top: 1242, width: 438, height: 16, 'font-size': 17, }}>
<span>
1330-CVAE-SD pour les multi-établissements et sur les SA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1260, width: 275, height: 16, 'font-size': 17, }}>
<span>
formulaires nos 1329-AC et 1329-DEF).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 383, top: 1293, width: 444, height: 13, 'font-size': 17, }}>
<span>
Cadre réservé au mono-établissement au sens de la C VAE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1322, width: 1087, height: 16, 'font-size': 17, }}>
<span>
Si vous êtes assujettis à la CVAE et mono-établissement au sens de la CVAE (cf. la notice du formulaire n° 1330-CVAE-SD), veuillez compléter
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1340, width: 786, height: 16, 'font-size': 17, }}>
<span>
le cadre ci-dessous et la case SA, vous serez alors dispensés du dépôt du formulaire n° 1330-CVAE-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 1370, width: 21, height: 13, 'font-size': 17, }}>
<span>
EV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1370, width: 403, height: 15, 'font-size': 17, }}>
<span>
Mono établissement au sens de la CVAE, cocher la case
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1400, width: 420, height: 16, 'font-size': 17, }}>
<span>
Chiffre d&#39;affaires de référence CVAE (report de la ligne OX)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 557, top: 1400, width: 24, height: 13, 'font-size': 17, }}>
<span>
GX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 751, top: 1400, width: 216, height: 13, 'font-size': 17, }}>
<span>
Effectifs au sens de la CVAE *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 1400, width: 21, height: 13, 'font-size': 17, }}>
<span>
EY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1430, width: 870, height: 16, 'font-size': 17, }}>
<span>
Chiffre d&#39;affaires du groupe économique (entreprises répondant aux conditions de détention fixées à l&#39;arti cle 223 A du CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 1430, width: 22, height: 12, 'font-size': 17, }}>
<span>
HX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1459, width: 149, height: 13, 'font-size': 17, }}>
<span>
Période de référence
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 501, top: 1459, width: 22, height: 13, 'font-size': 17, }}>
<span>
GY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 596, top: 1460, width: 7, height: 16, 'font-size': 21, }}>
<span>
/
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 683, top: 1460, width: 7, height: 16, 'font-size': 21, }}>
<span>
/
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 830, top: 1459, width: 22, height: 13, 'font-size': 17, }}>
<span>
GZ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 923, top: 1460, width: 6, height: 16, 'font-size': 21, }}>
<span>
/
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1011, top: 1459, width: 6, height: 15, 'font-size': 19, }}>
<span>
/
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1493, width: 127, height: 13, 'font-size': 17, }}>
<span>
Date de cessation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 831, top: 1493, width: 22, height: 13, 'font-size': 17, }}>
<span>
HR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 923, top: 1494, width: 6, height: 16, 'font-size': 21, }}>
<span>
/
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1011, top: 1494, width: 6, height: 14, 'font-size': 19, }}>
<span>
/
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1530, width: 1083, height: 16, 'font-size': 17, }}>
<span>
(1) Attention, il ne doit pas être tenu compte dans les lignes ON à OW des charges déductibles de la valeur ajoutée, afférente à la production
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 1548, width: 371, height: 16, 'font-size': 17, }}>
<span>
immobilisée déclarée ligne OE, portées en ligne OU.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1567, width: 1085, height: 16, 'font-size': 17, }}>
<span>
* Des explications concernant ces cases sont données dans la notice n° 1330-CVAE-SD § Répartition des salariés et dans la notice n° 2032-NOT-SD au
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1585, width: 185, height: 16, 'font-size': 17, }}>
<span>
§ déclaration des effectifs.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 53, width: 216, height: 14, 'font-size': 15, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 69, width: 177, height: 15, 'font-size': 15, }}>
<span>
du code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 391, top: 46, width: 489, height: 16, 'font-size': 17, }}>
<span>
DÉTERMINATION DES EFFECTIFS ET DE LA VALEUR AJOUTÉE
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="DB" data-field-id="17468296" data-annot-id="15782096" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="AG" data-field-id="17468632" data-annot-id="16302688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="AF" data-field-id="17468968" data-annot-id="16466528" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="AE" data-field-id="17469272" data-annot-id="16466720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="EP" data-field-id="17469608" data-annot-id="16693808" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="EN" data-field-id="17469944" data-annot-id="16694000" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="AJ" data-field-id="17470280" data-annot-id="16694192" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="AD" data-field-id="17470616" data-annot-id="16694384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="AC" data-field-id="17470952" data-annot-id="16694576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="AB" data-field-id="17471432" data-annot-id="16694768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="AN" data-field-id="17471768" data-annot-id="16694960" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="GD" data-field-id="17472104" data-annot-id="16695152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="EQ" data-field-id="17472440" data-annot-id="16695344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="AK" data-field-id="17472776" data-annot-id="16695536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="EF" data-field-id="17473112" data-annot-id="16695728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="EE" data-field-id="17473448" data-annot-id="16695920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="EL" data-field-id="17473784" data-annot-id="16696112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="AP" data-field-id="17474264" data-annot-id="16696576" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="CM" data-field-id="17474600" data-annot-id="16696768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="FN" data-field-id="17474936" data-annot-id="16696960" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="GC" data-field-id="17475272" data-annot-id="16697152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="FK" data-field-id="17475608" data-annot-id="16697344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="FH" data-field-id="17475944" data-annot-id="16697536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="FJ" data-field-id="17476280" data-annot-id="16697728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="FE" data-field-id="17476616" data-annot-id="16697920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="AM" data-field-id="17476952" data-annot-id="16698112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="AL" data-field-id="17477288" data-annot-id="16698304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GA" data-field-id="17477624" data-annot-id="16698496" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="GB" data-field-id="17477960" data-annot-id="16698688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="KD" data-field-id="17478296" data-annot-id="16698880" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="AH" data-field-id="17478632" data-annot-id="16699072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="KB" data-field-id="17478968" data-annot-id="16699264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="KC" data-field-id="17479304" data-annot-id="16699456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="KG" data-field-id="17474120" data-annot-id="16696304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="AQ" data-field-id="17480232" data-annot-id="16700176" type="text" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field pde-form-field-text" name="KA" data-field-id="17480568" data-annot-id="16700368" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component="input" className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="18969144" data-annot-id="18187472" type="text" disabled />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field pde-form-field-text" name="DEB_EX" data-field-id="18969480" data-annot-id="18187664" type="text" disabled />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field pde-form-field-text" name="FIN_EX" data-field-id="18969816" data-annot-id="18187856" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field pde-form-field-text" name="NBR_M_EX" data-field-id="18970152" data-annot-id="18188048" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2059E;
