import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {sum, useCompute, setValue, setIsNothingness} from "helpers/pdfforms";
import { get as _get } from 'lodash';

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const notNeededFields = {
  'NOM_STEA': true,
  'ZT': true
};

const Tax2058C = (props) => {
  const { taxDeclarationForm, form2051, form2052, change } = props;
  const FX = _get(form2052, 'FX', null);
  const FP = _get(form2051, 'FP', null);
  const FQ = _get(form2051, 'FQ', null);

  setIsNothingness(taxDeclarationForm, notNeededFields, 'ZT', change);


  useCompute('AL', sum, ['AA', 'AB', 'AK'], taxDeclarationForm, change);
  useCompute('ZH', sum, ['ZB', 'ZD', 'ZE', 'ZF', 'ZG'], taxDeclarationForm, change);
  useCompute('YX', sum, ['YW', 'AW'], taxDeclarationForm, change);

  setValue(taxDeclarationForm, 'YX', FX, change);
  setValue(taxDeclarationForm, 'AA', FP, change);
  setValue(taxDeclarationForm, 'AB', FQ, change);

  return (
    <div className="form-tax-declaration-2058c">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 385, top: 39, width: 443, height: 22, 'font-size': 25, }}>
<span>
TABLEAU D’AFFECTATION DU RÉSULTAT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 444, top: 66, width: 325, height: 17, 'font-size': 25, }}>
<span>
ET RENSEIGNEMENTS DIVERS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 946, top: 55, width: 57, height: 14, 'font-size': 21, }}>
<span>
DGFiP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1008, top: 52, width: 164, height: 19, 'font-size': 25, }}>
<span>
N° 2058-C-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 23, top: 1110, width: 12, height: 268, 'font-size': 13, }}>
<span>
N° 2058-C-SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 77, width: 174, height: 12, 'font-size': 12, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 89, width: 143, height: 12, 'font-size': 12, }}>
<span>
du Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 282, top: 51, width: 21, height: 19, 'font-size': 25, }}>
<span>
11
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 49, top: 157, width: 193, height: 16, 'font-size': 17, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 248, top: 169, width: 831, height: 1, 'font-size': 13, }}>
<span>
__________________________________________________________________________________________________________________________________________
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1088, top: 161, width: 41, height: 12, 'font-size': 17, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1163, top: 159, width: 7, height: 8, 'font-size': 23, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 257, width: 12, height: 71, 'font-size': 17, }}>
<span>
ORIGINES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 199, width: 320, height: 16, 'font-size': 17, }}>
<span>
Report à nouveau figurant au bilan de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 217, width: 354, height: 16, 'font-size': 17, }}>
<span>
antérieur à celui pour lequel la déclaration est établie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 465, top: 208, width: 21, height: 14, 'font-size': 17, }}>
<span>
ØC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 629, top: 237, width: 12, height: 112, 'font-size': 17, }}>
<span>
AFFECTATIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 260, width: 328, height: 16, 'font-size': 17, }}>
<span>
Résultat de l’exercice précédant celui pour lequel
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 278, width: 160, height: 12, 'font-size': 17, }}>
<span>
la déclaration est établie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 464, top: 269, width: 23, height: 14, 'font-size': 17, }}>
<span>
ØD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 465, top: 328, width: 21, height: 14, 'font-size': 17, }}>
<span>
ØE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 328, width: 194, height: 13, 'font-size': 17, }}>
<span>
Prélèvements sur les réserves
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 375, width: 95, height: 14, 'font-size': 17, }}>
<span>
TOTAL I ØF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 667, top: 206, width: 72, height: 12, 'font-size': 17, }}>
<span>
A e c t n
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 667, top: 227, width: 77, height: 9, 'font-size': 17, }}>
<span>
au r e e
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 677, top: 205, width: 68, height: 13, 'font-size': 17, }}>
<span>
ff t a io s
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 683, top: 224, width: 67, height: 12, 'font-size': 17, }}>
<span>
x é s rv s
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 189, width: 8, height: 58, 'font-size': 73, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 969, top: 197, width: 19, height: 12, 'font-size': 17, }}>
<span>
ZB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 232, width: 21, height: 12, 'font-size': 17, }}>
<span>
ZD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 828, top: 197, width: 120, height: 16, 'font-size': 17, }}>
<span>
– Réserves légales
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 828, top: 232, width: 113, height: 12, 'font-size': 17, }}>
<span>
– Autres réserves
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 970, top: 268, width: 17, height: 12, 'font-size': 17, }}>
<span>
ZE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 970, top: 305, width: 17, height: 12, 'font-size': 17, }}>
<span>
ZF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 341, width: 21, height: 12, 'font-size': 17, }}>
<span>
ZG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 667, top: 267, width: 75, height: 13, 'font-size': 17, }}>
<span>
Dividendes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 667, top: 305, width: 121, height: 15, 'font-size': 17, }}>
<span>
Autres répartitions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 667, top: 341, width: 119, height: 15, 'font-size': 17, }}>
<span>
Report à nouveau
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 670, top: 369, width: 282, height: 12, 'font-size': 13, }}>
<span>
(NB : le total I doit nécessairement être égal au total II)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 888, top: 382, width: 64, height: 13, 'font-size': 17, }}>
<span>
TOTAL II
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 376, width: 21, height: 12, 'font-size': 17, }}>
<span>
ZH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 410, width: 216, height: 13, 'font-size': 17, }}>
<span>
RENSEIGNEMENTS DIVERS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1045, top: 411, width: 80, height: 12, 'font-size': 17, }}>
<span>
Exercice N :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 438, width: 11, height: 103, 'font-size': 15, }}>
<span>
ENGAGEMENTS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 446, width: 267, height: 16, 'font-size': 17, }}>
<span>
– Engagements de crédit-bail mobilier
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 551, top: 444, width: 9, height: 20, 'font-size': 23, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 440, width: 212, height: 14, 'font-size': 15, }}>
<span>
précisez le prix de revient des biens
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 454, width: 103, height: 14, 'font-size': 15, }}>
<span>
pris en crédit-bail
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 797, top: 447, width: 14, height: 15, 'font-size': 17, }}>
<span>
J7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 953, top: 444, width: 10, height: 20, 'font-size': 23, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 482, width: 284, height: 16, 'font-size': 17, }}>
<span>
– Engagements de crédit-bail immobilier
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 520, width: 288, height: 16, 'font-size': 17, }}>
<span>
– Effets portés à l’escompte et non échus
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 658, width: 14, height: 139, 'font-size': 15, }}>
<span>
DÉTAILS DES POSTES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 574, width: 11, height: 198, 'font-size': 15, }}>
<span>
AUTRES ACHATS ET CHARGES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 639, width: 11, height: 67, 'font-size': 15, }}>
<span>
EXTERNES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 559, width: 110, height: 11, 'font-size': 17, }}>
<span>
– Sous-traitance
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 593, width: 333, height: 16, 'font-size': 17, }}>
<span>
– Locations, charges locatives et de copropriété
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 551, top: 590, width: 9, height: 20, 'font-size': 23, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 586, width: 211, height: 15, 'font-size': 15, }}>
<span>
dont montant des loyers des biens pris
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 600, width: 201, height: 15, 'font-size': 15, }}>
<span>
en location pour une durée &#62; 6 mois
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 797, top: 594, width: 14, height: 14, 'font-size': 17, }}>
<span>
J8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 953, top: 590, width: 10, height: 20, 'font-size': 23, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 629, width: 244, height: 16, 'font-size': 17, }}>
<span>
– Personnel extérieur à l’entreprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 665, width: 473, height: 14, 'font-size': 17, }}>
<span>
– Rémunérations d’intermédiaires et honoraires (hors rétrocessions)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 702, width: 388, height: 16, 'font-size': 17, }}>
<span>
– Rétrocessions d’honoraires, commissions et courtages
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 740, width: 122, height: 15, 'font-size': 17, }}>
<span>
– Autres comptes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 551, top: 737, width: 9, height: 19, 'font-size': 23, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 732, width: 211, height: 15, 'font-size': 15, }}>
<span>
dont cotisations versées aux organisa-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 747, width: 195, height: 14, 'font-size': 15, }}>
<span>
tions syndicales et professionnelles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 740, width: 17, height: 11, 'font-size': 17, }}>
<span>
ES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 953, top: 737, width: 10, height: 19, 'font-size': 23, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 291, top: 775, width: 442, height: 16, 'font-size': 17, }}>
<span>
Total du poste correspondant à la ligne FW du tableau n° 2052
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 817, width: 13, height: 73, 'font-size': 15, }}>
<span>
IMPÔTS ET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 833, width: 11, height: 42, 'font-size': 15, }}>
<span>
TAXES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 812, width: 250, height: 16, 'font-size': 17, }}>
<span>
– Taxe professionnelle*, CFE, CVAE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 849, width: 322, height: 16, 'font-size': 17, }}>
<span>
– Autres impôts, taxes et versements assimilés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 551, top: 847, width: 9, height: 20, 'font-size': 23, }}>
<span>
(
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 843, width: 209, height: 14, 'font-size': 15, }}>
<span>
dont taxe intérieure sur les produits
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 857, width: 55, height: 14, 'font-size': 15, }}>
<span>
pétroliers
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 849, width: 17, height: 13, 'font-size': 17, }}>
<span>
ZS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 953, top: 847, width: 10, height: 20, 'font-size': 23, }}>
<span>
)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 281, top: 884, width: 442, height: 17, 'font-size': 17, }}>
<span>
Total du poste correspondant à la ligne FW du tableau n° 2052
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 930, width: 11, height: 31, 'font-size': 15, }}>
<span>
TVA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 921, width: 213, height: 13, 'font-size': 17, }}>
<span>
– Montant de la TVA collectée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 957, width: 879, height: 17, 'font-size': 17, }}>
<span>
– Montant de la TVA déductible comptabilisée au cours de l’exercice au titre des biens et services ne constituant pas des immobilisations YZ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 1067, width: 11, height: 56, 'font-size': 15, }}>
<span>
DIVERS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 995, width: 602, height: 14, 'font-size': 17, }}>
<span>
– Montant brut des salaires (cf. dernière déclaration sociale nominative au titre de 2018 ) *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1030, width: 879, height: 17, 'font-size': 17, }}>
<span>
– Montant de la plus-value constatée en franchise d’impôt lors de la première option pour le régime simplifié d’imposition * ØS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 967, top: 447, width: 23, height: 15, 'font-size': 17, }}>
<span>
YQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 484, width: 21, height: 11, 'font-size': 17, }}>
<span>
YR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 969, top: 520, width: 18, height: 12, 'font-size': 17, }}>
<span>
YS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 556, width: 21, height: 12, 'font-size': 17, }}>
<span>
YT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 967, top: 593, width: 22, height: 15, 'font-size': 17, }}>
<span>
XQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 630, width: 22, height: 11, 'font-size': 17, }}>
<span>
YU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 971, top: 666, width: 15, height: 12, 'font-size': 17, }}>
<span>
SS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 703, width: 22, height: 11, 'font-size': 17, }}>
<span>
YV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 971, top: 739, width: 16, height: 12, 'font-size': 17, }}>
<span>
ST
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 775, width: 14, height: 16, 'font-size': 17, }}>
<span>
ZJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 966, top: 813, width: 26, height: 11, 'font-size': 17, }}>
<span>
YW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 970, top: 848, width: 17, height: 14, 'font-size': 17, }}>
<span>
9Z
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 886, width: 22, height: 11, 'font-size': 17, }}>
<span>
YX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 922, width: 22, height: 11, 'font-size': 17, }}>
<span>
YY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 994, width: 22, height: 14, 'font-size': 17, }}>
<span>
ØB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1069, width: 689, height: 16, 'font-size': 17, }}>
<span>
– Taux d’intérêt le plus élevé servi aux associés à raison des sommes mises à la disposition de la société *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 969, top: 1070, width: 20, height: 12, 'font-size': 17, }}>
<span>
ZK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1154, top: 1070, width: 12, height: 12, 'font-size': 17, }}>
<span>
%
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1106, width: 180, height: 16, 'font-size': 17, }}>
<span>
– Numéro de centre agréé *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 499, top: 1107, width: 19, height: 11, 'font-size': 17, }}>
<span>
XP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 712, top: 1098, width: 285, height: 16, 'font-size': 17, }}>
<span>
– Filiales et participations : (Liste au 2059-G
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 725, top: 1113, width: 259, height: 16, 'font-size': 17, }}>
<span>
prévu par art. 38 II de l’ann. III au CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1017, top: 1099, width: 86, height: 11, 'font-size': 15, }}>
<span>
Si oui cocher 1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1017, top: 1114, width: 44, height: 11, 'font-size': 15, }}>
<span>
Sinon 0
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1117, top: 1106, width: 19, height: 12, 'font-size': 17, }}>
<span>
ZR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1142, width: 880, height: 17, 'font-size': 17, }}>
<span>
– Aides perçues ayant donné droit à la réduction d’impôt prévue au 4 de l’article 238 bis du CGI pour l’entreprise donatric e RG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1180, width: 880, height: 16, 'font-size': 17, }}>
<span>
– Montant de l’investissement reçu qui a donné lieu à amortissement exceptionnel chez l’entreprise investisseur dans le cadre de l’article 217 octies RH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 60, top: 1217, width: 13, height: 148, 'font-size': 15, }}>
<span>
RÉGIME DE GROUPE *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1209, width: 214, height: 13, 'font-size': 17, }}>
<span>
Société : résultat comme si elle
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1223, width: 232, height: 17, 'font-size': 17, }}>
<span>
n’avait jamais été membre du groupe
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 356, top: 1217, width: 17, height: 15, 'font-size': 17, }}>
<span>
JA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 1217, width: 123, height: 15, 'font-size': 15, }}>
<span>
Plus-values à 15 % JK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 869, top: 1217, width: 116, height: 15, 'font-size': 15, }}>
<span>
Plus-values à 0 % JL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 1251, width: 123, height: 15, 'font-size': 15, }}>
<span>
Plus-values à 19 % JM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 869, top: 1252, width: 61, height: 13, 'font-size': 15, }}>
<span>
Imputations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 970, top: 1252, width: 16, height: 14, 'font-size': 17, }}>
<span>
JC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1286, width: 200, height: 16, 'font-size': 17, }}>
<span>
Groupe : résultat d’ensemble
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 355, top: 1287, width: 19, height: 14, 'font-size': 17, }}>
<span>
JD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 1286, width: 123, height: 15, 'font-size': 15, }}>
<span>
Plus-values à 15 % JN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 869, top: 1286, width: 118, height: 15, 'font-size': 15, }}>
<span>
Plus-values à 0 % JO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 1323, width: 121, height: 15, 'font-size': 15, }}>
<span>
Plus-values à 19 % JP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 869, top: 1324, width: 61, height: 13, 'font-size': 15, }}>
<span>
Imputations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 970, top: 1324, width: 15, height: 14, 'font-size': 17, }}>
<span>
JF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1351, width: 371, height: 16, 'font-size': 17, }}>
<span>
Si vous relevez du régime de groupe : indiquer 1 si société mère,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 1365, width: 97, height: 13, 'font-size': 17, }}>
<span>
2 si société filiale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 500, top: 1359, width: 19, height: 15, 'font-size': 17, }}>
<span>
JH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 1359, width: 270, height: 15, 'font-size': 15, }}>
<span>
N° SIRET de la société mère du groupe JJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1399, width: 1082, height: 14, 'font-size': 15, }}>
<span>
(1) Ce cadre est destiné à faire apparaître l’origine et le montant des sommes distribuées ou mises en réserve au cours de l’exercice dont les résultats font l’objet de la déclaration.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 1416, width: 1006, height: 14, 'font-size': 15, }}>
<span>
Il ne concerne donc pas, en principe, les résultats de cet exercice mais ceux des exercices antérieurs, qu’ils aient ou non déjà fait l’objet d’une précédente affectation.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1440, width: 6, height: 6, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1437, width: 846, height: 14, 'font-size': 15, }}>
<span>
Des explications concernant cette rubrique sont données dans la notice n° 2032 (et dans la notice n° 2058-NOT pour le régime de groupe).
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="ZT" data-field-id="26331384" data-annot-id="25317168" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="AA" data-field-id="26331752" data-annot-id="25516992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="ZB" data-field-id="26332088" data-annot-id="25517216" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="AB" data-field-id="26332392" data-annot-id="25517408" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="ZD" data-field-id="26336680" data-annot-id="25517600" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="AK" data-field-id="26337016" data-annot-id="25517792" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="ZE" data-field-id="26337352" data-annot-id="25517984" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="AL" data-field-id="26337688" data-annot-id="25518176" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="ZF" data-field-id="26338024" data-annot-id="25525952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="ZG" data-field-id="26338504" data-annot-id="25526144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="ZH" data-field-id="26338840" data-annot-id="25526336" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="YQ" data-field-id="26339176" data-annot-id="25526528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="EA" data-field-id="26339512" data-annot-id="25526720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="YR" data-field-id="26339848" data-annot-id="25526912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="YS" data-field-id="26340184" data-annot-id="25527104" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="YT" data-field-id="26340520" data-annot-id="25527296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="XQ" data-field-id="26340856" data-annot-id="25527488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="YU" data-field-id="26341336" data-annot-id="25527952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="SS" data-field-id="26341672" data-annot-id="25528144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="YV" data-field-id="26342008" data-annot-id="25528336" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="ST" data-field-id="26342344" data-annot-id="25528528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="EB" data-field-id="26342680" data-annot-id="25528720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="JR" data-field-id="26343016" data-annot-id="25528912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="ZJ" data-field-id="26343352" data-annot-id="25529104" type="text"  disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="ZS" data-field-id="26343688" data-annot-id="25529296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="YW" data-field-id="26344024" data-annot-id="25529488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="YX" data-field-id="26344360" data-annot-id="25529680" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="AW" data-field-id="26344696" data-annot-id="25529872" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="YY" data-field-id="26345032" data-annot-id="25530064" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="YZ" data-field-id="26345368" data-annot-id="25530256" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="BD" data-field-id="26345704" data-annot-id="25530448" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="BC" data-field-id="26346040" data-annot-id="25530640" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="ZK" data-field-id="26346376" data-annot-id="25530832" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="XP" data-field-id="26341192" data-annot-id="25527680" type="text" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="ZR" data-field-id="26347304" data-annot-id="25531552" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="AC" data-field-id="26347640" data-annot-id="25531744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="AD" data-field-id="26347976" data-annot-id="25531936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="JA" data-field-id="26348312" data-annot-id="25532128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="JK" data-field-id="26348648" data-annot-id="25532320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="JL" data-field-id="26348984" data-annot-id="25532512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="JM" data-field-id="26349320" data-annot-id="25532704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="JC" data-field-id="26349656" data-annot-id="25532896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="JN" data-field-id="26349992" data-annot-id="25533088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="JP" data-field-id="26352008" data-annot-id="25533280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="JQ" data-field-id="26350328" data-annot-id="25533472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="JF" data-field-id="26350664" data-annot-id="25533664" type="text" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="JH" data-field-id="26351000" data-annot-id="25533856" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="JJ" data-field-id="26351336" data-annot-id="25534048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="JD" data-field-id="26351672" data-annot-id="25534240" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="26404936" data-annot-id="25586592" type="text" disabled />

          </div>
        </div>

      </div>
    </div>
  );
};

export default Tax2058C;
