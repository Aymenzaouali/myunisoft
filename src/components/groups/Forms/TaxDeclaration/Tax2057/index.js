import React from 'react';
import { Field } from 'redux-form';

import { useCompute, sum, sub, setIsNothingness } from 'helpers/pdfforms';

import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const notNeededFields = {
  'NOM_STEA': true,
  'ZU': true
};

// Component
const Tax2057 = (props) => {
  const { formValues, change } = props;
  setIsNothingness(formValues, notNeededFields, 'ZU', change);

  // Compute
  useCompute('UM', sub, ['UL', 'UN'], formValues, change);
  useCompute('UR', sub, ['UP', 'US'], formValues, change);
  useCompute('UV', sub, ['UT', 'UW'], formValues, change);
  useCompute('WA', sub, ['VA', 'WM'], formValues, change);
  useCompute('WB', sub, ['UX', 'WN'], formValues, change);
  useCompute('BC', sub, ['BB', 'BD'], formValues, change);
  useCompute('WD', sub, ['UY', 'WQ'], formValues, change);
  useCompute('WE', sub, ['UZ', 'WR'], formValues, change);
  useCompute('WF', sub, ['VM', 'WS'], formValues, change);
  useCompute('WG', sub, ['VB', 'WT'], formValues, change);
  useCompute('WH', sub, ['VN', 'WU'], formValues, change);
  useCompute('WI', sub, ['VP', 'WV'], formValues, change);
  useCompute('WJ', sub, ['VC', 'WW'], formValues, change);
  useCompute('WK', sub, ['VR', 'WX'], formValues, change);
  useCompute('WL', sub, ['VS', 'WY'], formValues, change);

  useCompute('VT', sum, ['UL', 'UP', 'UT', 'VA', 'UX', 'BB', 'UY', 'UZ', 'VM', 'VB', 'VN', 'VP', 'VC', 'VR', 'VS'], formValues, change);
  useCompute('VU', sum, ['UM', 'UR', 'UV', 'WA', 'WB', 'BC', 'WD', 'WE', 'WF', 'WG', 'WH', 'WI', 'WJ', 'WK', 'WL'], formValues, change);
  useCompute('VV', sum, ['UN', 'US', 'UW', 'WM', 'WN', 'BD', 'WQ', 'WR', 'WS', 'WT', 'WU', 'WV', 'WW', 'WX', 'WY'], formValues, change);

  useCompute('XL', sub, ['XA', 'YE', 'ZA'], formValues, change);
  useCompute('XM', sub, ['XB', 'YF', 'ZB'], formValues, change);
  useCompute('XN', sub, ['VG', 'YG', 'ZC'], formValues, change);
  useCompute('XP', sub, ['VH', 'YH', 'ZD'], formValues, change);
  useCompute('XQ', sub, ['XC', 'YJ', 'ZE'], formValues, change);
  useCompute('XR', sub, ['XD', 'YK', 'ZF'], formValues, change);
  useCompute('XS', sub, ['XE', 'YL', 'ZG'], formValues, change);
  useCompute('XT', sub, ['XF', 'YM', 'ZH'], formValues, change);
  useCompute('XU', sub, ['XG', 'YN', 'ZJ'], formValues, change);
  useCompute('XV', sub, ['VW', 'YP', 'ZK'], formValues, change);
  useCompute('XW', sub, ['VX', 'YQ', 'ZL'], formValues, change);
  useCompute('XY', sub, ['VQ', 'YR', 'ZM'], formValues, change);
  useCompute('XZ', sub, ['XH', 'YS', 'ZN'], formValues, change);
  useCompute('YA', sub, ['VI', 'YT', 'ZP'], formValues, change);
  useCompute('YB', sub, ['XJ', 'YU', 'ZQ'], formValues, change);
  useCompute('BF', sub, ['BE', 'BG', 'BH'], formValues, change);
  useCompute('YD', sub, ['XK', 'YW', 'ZS'], formValues, change);

  useCompute('VY', sum, ['XA', 'XB', 'VG', 'VH', 'XC', 'XD', 'XE', 'XF', 'XG', 'VW', 'VX', 'VQ', 'XH', 'VI', 'XJ', 'BE', 'XK'], formValues, change);
  useCompute('VZ', sum, ['XL', 'XM', 'XN', 'XP', 'XQ', 'XR', 'XS', 'XT', 'XU', 'XV', 'XW', 'XY', 'XZ', 'YA', 'YB', 'BF', 'YD'], formValues, change);
  useCompute('YX', sum, ['YE', 'YF', 'YG', 'YH', 'YJ', 'YK', 'YL', 'YM', 'YN', 'YP', 'YQ', 'YR', 'YS', 'YT', 'YU', 'BG', 'YW'], formValues, change);
  useCompute('ZT', sum, ['ZA', 'ZB', 'ZC', 'ZD', 'ZE', 'ZF', 'ZG', 'ZH', 'ZJ', 'ZK', 'ZL', 'ZM', 'ZN', 'ZP', 'ZQ', 'BH', 'ZS'], formValues, change);

  // Rendering
  return (
    <div className="tax2057">
      <div className="pdf-page " id="pdf-page-1">
        <div className="pdf-page-inner pdf-page-1">
          <div className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 271, top: 170, width: 180, height: 18, fontSize: 18 }}>
              <span>ÉTAT DES CRÉANCES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 56, top: 174, width: 76, height: 14, fontSize: 18 }}>
              <span>CADRE A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 368, top: 52, width: 484, height: 22, fontSize: 25 }}>
              <span>ÉTAT DES ÉCHÉANCES DES CRÉANCES ET</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 349, top: 74, width: 522, height: 22, fontSize: 25 }}>
              <span>DES DETTES A LA CLÔTURE DE L’EXERCICE *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 211, width: 277, height: 16, fontSize: 17 }}>
              <span>Créances rattachées à des participations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 247, width: 84, height: 14, fontSize: 17 }}>
              <span>Prêts (1) (2)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 283, width: 237, height: 13, fontSize: 17 }}>
              <span>Autres immobilisations financières</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 319, width: 192, height: 16, fontSize: 17 }}>
              <span>Clients douteux ou litigieux</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 355, width: 158, height: 13, fontSize: 17 }}>
              <span>Autres créances clients</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 84, top: 384, width: 153, height: 16, fontSize: 17 }}>
              <span>Créance représentative de titres</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 399, width: 142, height: 15, fontSize: 17 }}>
              <span>prêtés ou remis en garantie *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 240, top: 388, width: 5, height: 26, fontSize: 31 }}>
              <span>(</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 563, top: 388, width: 5, height: 26, fontSize: 31 }}>
              <span>)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 371, top: 390, width: 20, height: 14, fontSize: 21 }}>
              <span>UO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 246, top: 384, width: 117, height: 16, fontSize: 17 }}>
              <span>Provisionpour dépréciation</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 247, top: 399, width: 115, height: 12, fontSize: 17 }}>
              <span>antérieurement constituée *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 427, width: 216, height: 16, fontSize: 17 }}>
              <span>Personnel et comptes rattachés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 463, width: 313, height: 16, fontSize: 17 }}>
              <span>Sécurité sociale et autres organismes sociaux</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 191, top: 499, width: 169, height: 17, fontSize: 17 }}>
              <span>Impôts sur les bénéfices</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 190, top: 536, width: 181, height: 16, fontSize: 17 }}>
              <span>Taxe sur la valeur ajoutée</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 190, top: 572, width: 309, height: 16, fontSize: 17 }}>
              <span>Autres impôts, taxes et versements assimilés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 191, top: 609, width: 44, height: 11, fontSize: 17 }}>
              <span>Divers</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 644, width: 156, height: 16, fontSize: 17 }}>
              <span>Groupe et associés (2)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 673, width: 404, height: 16, fontSize: 17 }}>
              <span>Débiteurs divers (dont créances relatives à des opérations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 687, width: 144, height: 16, fontSize: 17 }}>
              <span>de pension de titres)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 70, top: 716, width: 197, height: 16, fontSize: 17 }}>
              <span>Charges constatées d’avance</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 472, top: 753, width: 66, height: 12, fontSize: 17 }}>
              <span>TOTAUX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 251, top: 788, width: 251, height: 13, fontSize: 17 }}>
              <span>- Prêts accordés en cours d’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 251, top: 825, width: 298, height: 12, fontSize: 17 }}>
              <span>- Remboursements obtenus en cours d’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 119, top: 861, width: 435, height: 16, fontSize: 17 }}>
              <span>Prêts et avances consentis aux associés (personnes physiques)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 37, top: 90, width: 183, height: 12, fontSize: 13 }}>
              <span>Formulaire obligatoire (article 53 A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 102, width: 155, height: 12, fontSize: 13 }}>
              <span>du Code général des impôts).</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 955, top: 79, width: 205, height: 15, fontSize: 21 }}>
              <span>DGFiP N° 2057-SD 2019</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 48, top: 136, width: 195, height: 16, fontSize: 17 }}>
              <span>Désignation de l’entreprise :</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 638, top: 172, width: 75, height: 11, fontSize: 15 }}>
              <span>Montant brut</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 673, top: 184, width: 5, height: 8, fontSize: 13 }}>
              <span>1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 836, top: 172, width: 80, height: 14, fontSize: 15 }}>
              <span>A 1 an au plus</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 873, top: 184, width: 6, height: 8, fontSize: 13 }}>
              <span>2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1034, top: 172, width: 81, height: 14, fontSize: 15 }}>
              <span>A plus d’un an</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1072, top: 184, width: 6, height: 10, fontSize: 13 }}>
              <span>3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 45, top: 216, width: 10, height: 75, fontSize: 15 }}>
              <span>DE L’ACTIF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 213, width: 13, height: 82, fontSize: 15 }}>
              <span>IMMOBILISÉ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 427, width: 10, height: 1211, fontSize: 15 }}>
              <span>DE L’ACTIF CIRCULANTRENVOISRENVOIS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 524, width: 90, height: 14, fontSize: 17 }}>
              <span>État et autres</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 87, top: 554, width: 79, height: 12, fontSize: 17 }}>
              <span>collectivités</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 93, top: 582, width: 68, height: 16, fontSize: 17 }}>
              <span>publiques</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 798, width: 57, height: 11, fontSize: 17 }}>
              <span>Montant</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 816, width: 23, height: 13, fontSize: 17 }}>
              <span>des</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 81, top: 806, width: 19, height: 15, fontSize: 17 }}>
              <span>(1)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 81, top: 861, width: 19, height: 14, fontSize: 17 }}>
              <span>(2)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 81, top: 1601, width: 19, height: 14, fontSize: 17 }}>
              <span>(1)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 896, width: 74, height: 14, fontSize: 18 }}>
              <span>CADRE B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 933, width: 264, height: 16, fontSize: 17 }}>
              <span>Emprunts obligataires convertibles (1)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 969, width: 223, height: 16, fontSize: 17 }}>
              <span>Autres emprunts obligataires (1)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 220, top: 1005, width: 183, height: 16, fontSize: 17 }}>
              <span>à 1 an maximum à l’origine</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 220, top: 1041, width: 157, height: 16, fontSize: 17 }}>
              <span>à plus d’1 an à l’origine</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1077, width: 307, height: 16, fontSize: 17 }}>
              <span>Emprunts et dettes financières divers (1) (2)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1113, width: 235, height: 16, fontSize: 17 }}>
              <span>Fournisseurs et comptes rattachés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1149, width: 216, height: 16, fontSize: 17 }}>
              <span>Personnel et comptes rattachés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 1185, width: 312, height: 16, fontSize: 17 }}>
              <span>Sécurité sociale et autres organismes sociaux</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 162, top: 1221, width: 168, height: 17, fontSize: 17 }}>
              <span>Impôts sur les bénéfices</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 162, top: 1258, width: 180, height: 16, fontSize: 17 }}>
              <span>Taxe sur la valeur ajoutée</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 162, top: 1294, width: 166, height: 16, fontSize: 17 }}>
              <span>Obligations cautionnées</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 161, top: 1330, width: 226, height: 16, fontSize: 17 }}>
              <span>Autres impôts, taxes et assimilés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1366, width: 332, height: 16, fontSize: 17 }}>
              <span>Dettes sur immobilisations et comptes rattachés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 1402, width: 156, height: 16, fontSize: 17 }}>
              <span>Groupe et associés (2)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1431, width: 284, height: 14, fontSize: 17 }}>
              <span>Autres dettes (dont dettes relatives à des</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1445, width: 223, height: 16, fontSize: 17 }}>
              <span>opérations de pension de titres)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1466, width: 279, height: 16, fontSize: 17 }}>
              <span>Dette représentative de titres empruntés</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1483, width: 155, height: 15, fontSize: 17 }}>
              <span>ou remis en garantie *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1510, width: 191, height: 13, fontSize: 17 }}>
              <span>Produits constatés d’avance</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 119, top: 1619, width: 291, height: 16, fontSize: 17 }}>
              <span>Emprunts remboursés en cours d’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 932, width: 16, height: 14, fontSize: 21 }}>
              <span>7Y</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 967, width: 15, height: 15, fontSize: 21 }}>
              <span>7Z</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 1076, width: 16, height: 14, fontSize: 21 }}>
              <span>8A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 1112, width: 15, height: 14, fontSize: 21 }}>
              <span>8B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 1148, width: 15, height: 14, fontSize: 21 }}>
              <span>8C</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 426, top: 1184, width: 17, height: 15, fontSize: 21 }}>
              <span>8D</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 1221, width: 16, height: 14, fontSize: 21 }}>
              <span>8E</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 429, top: 1365, width: 11, height: 19, fontSize: 21 }}>
              <span>8J</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 1437, width: 17, height: 14, fontSize: 21 }}>
              <span>8K</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 428, top: 1509, width: 14, height: 14, fontSize: 21 }}>
              <span>8L</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 1004, width: 21, height: 14, fontSize: 21 }}>
              <span>VG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 1040, width: 21, height: 14, fontSize: 21 }}>
              <span>VH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 423, top: 1257, width: 23, height: 14, fontSize: 21 }}>
              <span>VW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 1293, width: 21, height: 14, fontSize: 21 }}>
              <span>VX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 1329, width: 21, height: 18, fontSize: 21 }}>
              <span>VQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 1401, width: 15, height: 14, fontSize: 21 }}>
              <span>VI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 1473, width: 16, height: 14, fontSize: 21 }}>
              <span>Z2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 425, top: 1545, width: 20, height: 15, fontSize: 21 }}>
              <span>VY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1545, width: 19, height: 15, fontSize: 21 }}>
              <span>VZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 427, top: 1582, width: 15, height: 19, fontSize: 21 }}>
              <span>VJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 1618, width: 21, height: 14, fontSize: 21 }}>
              <span>VK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 324, top: 1547, width: 67, height: 12, fontSize: 17 }}>
              <span>TOTAUX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 209, top: 892, width: 152, height: 18, fontSize: 18 }}>
              <span>ÉTAT DES DETTES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 490, top: 891, width: 75, height: 11, fontSize: 15 }}>
              <span>Montant brut</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 525, top: 907, width: 5, height: 9, fontSize: 13 }}>
              <span>1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 689, top: 891, width: 80, height: 14, fontSize: 15 }}>
              <span>A 1 an au plus</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 727, top: 907, width: 6, height: 9, fontSize: 13 }}>
              <span>2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 836, top: 891, width: 162, height: 14, fontSize: 15 }}>
              <span>A plus d’1 an et 5 ans au plus</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 914, top: 907, width: 6, height: 10, fontSize: 13 }}>
              <span>3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1051, top: 891, width: 85, height: 14, fontSize: 15 }}>
              <span>A plus de 5 ans</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1091, top: 906, width: 6, height: 10, fontSize: 13 }}>
              <span>4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 1220, width: 44, height: 15, fontSize: 17 }}>
              <span>État et</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1260, width: 40, height: 11, fontSize: 17 }}>
              <span>autres</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 1294, width: 80, height: 13, fontSize: 17 }}>
              <span>collectivités</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 60, top: 1330, width: 68, height: 16, fontSize: 17 }}>
              <span>publiques</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 119, top: 1583, width: 285, height: 16, fontSize: 17 }}>
              <span>Emprunts souscrits en cours d’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 999, width: 130, height: 16, fontSize: 17 }}>
              <span>Emprunts et dettes</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 1015, width: 75, height: 16, fontSize: 17 }}>
              <span>auprès des</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 1031, width: 99, height: 13, fontSize: 17 }}>
              <span>établissements</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1047, width: 86, height: 14, fontSize: 17 }}>
              <span>de crédit (1)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 783, top: 210, width: 23, height: 14, fontSize: 21 }}>
              <span>UM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 210, width: 18, height: 14, fontSize: 21 }}>
              <span>UL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 210, width: 21, height: 14, fontSize: 21 }}>
              <span>UN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 246, width: 17, height: 14, fontSize: 21 }}>
              <span>US</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 282, width: 23, height: 14, fontSize: 21 }}>
              <span>UW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 785, top: 246, width: 19, height: 14, fontSize: 21 }}>
              <span>UR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 784, top: 282, width: 21, height: 14, fontSize: 21 }}>
              <span>UV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 785, top: 751, width: 20, height: 15, fontSize: 21 }}>
              <span>VU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 751, width: 21, height: 15, fontSize: 21 }}>
              <span>VV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 246, width: 18, height: 14, fontSize: 21 }}>
              <span>UP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 281, width: 19, height: 15, fontSize: 21 }}>
              <span>UT</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 318, width: 20, height: 14, fontSize: 21 }}>
              <span>VA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 354, width: 20, height: 14, fontSize: 21 }}>
              <span>UX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 577, top: 390, width: 15, height: 14, fontSize: 21 }}>
              <span>Z1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 426, width: 20, height: 14, fontSize: 21 }}>
              <span>UY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 462, width: 19, height: 15, fontSize: 21 }}>
              <span>UZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 574, top: 499, width: 22, height: 14, fontSize: 21 }}>
              <span>VM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 535, width: 19, height: 14, fontSize: 21 }}>
              <span>VB</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 574, top: 571, width: 21, height: 14, fontSize: 21 }}>
              <span>VN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 607, width: 18, height: 14, fontSize: 21 }}>
              <span>VP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 643, width: 19, height: 14, fontSize: 21 }}>
              <span>VC</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 679, width: 20, height: 14, fontSize: 21 }}>
              <span>VR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 715, width: 17, height: 14, fontSize: 21 }}>
              <span>VS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 751, width: 19, height: 15, fontSize: 21 }}>
              <span>VT</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 574, top: 787, width: 21, height: 15, fontSize: 21 }}>
              <span>VD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 823, width: 20, height: 15, fontSize: 21 }}>
              <span>VE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 859, width: 18, height: 15, fontSize: 21 }}>
              <span>VF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 266, top: 69, width: 13, height: 20, fontSize: 27 }}>
              <span>8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 963, top: 1583, width: 18, height: 14, fontSize: 21 }}>
              <span>VL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 647, top: 1619, width: 527, height: 15, fontSize: 15 }}>
              <span>* Des explications concernant cette rubrique sont données dans la notice n° 2032</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1583, width: 18, height: 14, fontSize: 17 }}>
              <span>(2)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 674, top: 1573, width: 280, height: 16, fontSize: 17 }}>
              <span>Montant des divers emprunts et dettes contrac-</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 675, top: 1588, width: 265, height: 16, fontSize: 17 }}>
              <span>tés auprès des associés personnes physiques</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1094, top: 138, width: 40, height: 12, fontSize: 17 }}>
              <span>Néant</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1163, top: 130, width: 5, height: 6, fontSize: 17 }}>
              <span>*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 22, top: 1305, width: 11, height: 263, fontSize: 13 }}>
              <span>N° 2057- SD – (SDNC-DGFiP) - Novembre 2018</span>
            </div>
          </div>
          <div className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot pdf-obj-0-1 pdf-obj-fixed acroform-field" name="ZU" type="checkbox" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-2 pdf-obj-fixed acroform-field" name="VK" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-3 pdf-obj-fixed acroform-field" name="VJ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-4 pdf-obj-fixed acroform-field" name="VY" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-5 pdf-obj-fixed acroform-field" name="VZ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-6 pdf-obj-fixed acroform-field" name="YX" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-7 pdf-obj-fixed acroform-field" name="ZT" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-8 pdf-obj-fixed acroform-field" name="VL" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-9 pdf-obj-fixed acroform-field" name="XK" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-10 pdf-obj-fixed acroform-field" name="BE" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-11 pdf-obj-fixed acroform-field" name="XJ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-12 pdf-obj-fixed acroform-field" name="VI" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-13 pdf-obj-fixed acroform-field" name="XH" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-14 pdf-obj-fixed acroform-field" name="VQ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-15 pdf-obj-fixed acroform-field" name="VX" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-16 pdf-obj-fixed acroform-field" name="VW" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-17 pdf-obj-fixed acroform-field" name="XG" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-18 pdf-obj-fixed acroform-field" name="XF" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-19 pdf-obj-fixed acroform-field" name="XE" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-20 pdf-obj-fixed acroform-field" name="XD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-21 pdf-obj-fixed acroform-field" name="XC" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-22 pdf-obj-fixed acroform-field" name="VH" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-23 pdf-obj-fixed acroform-field" name="VG" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-24 pdf-obj-fixed acroform-field" name="XB" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-25 pdf-obj-fixed acroform-field" name="XA" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-26 pdf-obj-fixed acroform-field" name="YD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-27 pdf-obj-fixed acroform-field" name="BF" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-28 pdf-obj-fixed acroform-field" name="YB" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-29 pdf-obj-fixed acroform-field" name="YA" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-30 pdf-obj-fixed acroform-field" name="XZ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-31 pdf-obj-fixed acroform-field" name="XY" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-32 pdf-obj-fixed acroform-field" name="XW" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-33 pdf-obj-fixed acroform-field" name="XV" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-34 pdf-obj-fixed acroform-field" name="XU" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-35 pdf-obj-fixed acroform-field" name="XT" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-36 pdf-obj-fixed acroform-field" name="XS" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-37 pdf-obj-fixed acroform-field" name="XR" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-38 pdf-obj-fixed acroform-field" name="XQ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-39 pdf-obj-fixed acroform-field" name="XP" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-40 pdf-obj-fixed acroform-field" name="XN" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-41 pdf-obj-fixed acroform-field" name="XM" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-42 pdf-obj-fixed acroform-field" name="XL" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-43 pdf-obj-fixed acroform-field" name="YW" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-44 pdf-obj-fixed acroform-field" name="BG" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-45 pdf-obj-fixed acroform-field" name="YU" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-46 pdf-obj-fixed acroform-field" name="YT" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-47 pdf-obj-fixed acroform-field" name="YS" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-48 pdf-obj-fixed acroform-field" name="YR" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-49 pdf-obj-fixed acroform-field" name="YQ" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-50 pdf-obj-fixed acroform-field" name="YP" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-51 pdf-obj-fixed acroform-field" name="YN" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-52 pdf-obj-fixed acroform-field" name="YM" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-53 pdf-obj-fixed acroform-field" name="YL" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-54 pdf-obj-fixed acroform-field" name="YK" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-55 pdf-obj-fixed acroform-field" name="YJ" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-56 pdf-obj-fixed acroform-field" name="YH" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-57 pdf-obj-fixed acroform-field" name="YG" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-58 pdf-obj-fixed acroform-field" name="YF" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-59 pdf-obj-fixed acroform-field" name="YE" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-60 pdf-obj-fixed acroform-field" name="ZS" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-61 pdf-obj-fixed acroform-field" name="BH" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-62 pdf-obj-fixed acroform-field" name="ZQ" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-63 pdf-obj-fixed acroform-field" name="ZP" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-64 pdf-obj-fixed acroform-field" name="ZN" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-65 pdf-obj-fixed acroform-field" name="ZM" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-66 pdf-obj-fixed acroform-field" name="ZL" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-67 pdf-obj-fixed acroform-field" name="ZK" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-68 pdf-obj-fixed acroform-field" name="ZJ" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-69 pdf-obj-fixed acroform-field" name="ZH" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-70 pdf-obj-fixed acroform-field" name="ZG" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-71 pdf-obj-fixed acroform-field" name="ZF" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-72 pdf-obj-fixed acroform-field" name="ZE" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-73 pdf-obj-fixed acroform-field" name="ZD" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-74 pdf-obj-fixed acroform-field" name="ZC" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-75 pdf-obj-fixed acroform-field" name="ZB" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-76 pdf-obj-fixed acroform-field" name="ZA" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-77 pdf-obj-fixed acroform-field" name="VF" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-78 pdf-obj-fixed acroform-field" name="VE" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-79 pdf-obj-fixed acroform-field" name="VD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-80 pdf-obj-fixed acroform-field" name="VT" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-81 pdf-obj-fixed acroform-field" name="VS" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-82 pdf-obj-fixed acroform-field" name="VR" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-83 pdf-obj-fixed acroform-field" name="VC" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-84 pdf-obj-fixed acroform-field" name="VP" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-85 pdf-obj-fixed acroform-field" name="VN" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-86 pdf-obj-fixed acroform-field" name="VB" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-87 pdf-obj-fixed acroform-field" name="VM" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-88 pdf-obj-fixed acroform-field" name="UZ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-89 pdf-obj-fixed acroform-field" name="UY" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-90 pdf-obj-fixed acroform-field" name="BB" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-91 pdf-obj-fixed acroform-field" name="UX" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-92 pdf-obj-fixed acroform-field" name="VA" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-93 pdf-obj-fixed acroform-field" name="UT" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-94 pdf-obj-fixed acroform-field" name="UP" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-95 pdf-obj-fixed acroform-field" name="UL" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-96 pdf-obj-fixed acroform-field" name="VU" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-97 pdf-obj-fixed acroform-field" name="WL" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-98 pdf-obj-fixed acroform-field" name="WK" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-99 pdf-obj-fixed acroform-field" name="WJ" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-100 pdf-obj-fixed acroform-field" name="WI" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-101 pdf-obj-fixed acroform-field" name="WH" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-102 pdf-obj-fixed acroform-field" name="WG" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-103 pdf-obj-fixed acroform-field" name="WF" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-104 pdf-obj-fixed acroform-field" name="WE" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-105 pdf-obj-fixed acroform-field" name="WD" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-106 pdf-obj-fixed acroform-field" name="BC" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-107 pdf-obj-fixed acroform-field" name="WB" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-108 pdf-obj-fixed acroform-field" name="WA" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-109 pdf-obj-fixed acroform-field" name="UV" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-110 pdf-obj-fixed acroform-field" name="UR" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-111 pdf-obj-fixed acroform-field" name="UM" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-112 pdf-obj-fixed acroform-field" name="VV" disabled />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-113 pdf-obj-fixed acroform-field" name="WY" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-114 pdf-obj-fixed acroform-field" name="WX" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-115 pdf-obj-fixed acroform-field" name="WW" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-116 pdf-obj-fixed acroform-field" name="WV" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-117 pdf-obj-fixed acroform-field" name="WU" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-118 pdf-obj-fixed acroform-field" name="WT" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-119 pdf-obj-fixed acroform-field" name="WR" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-120 pdf-obj-fixed acroform-field" name="WS" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-121 pdf-obj-fixed acroform-field" name="WQ" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-122 pdf-obj-fixed acroform-field" name="BD" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-123 pdf-obj-fixed acroform-field" name="WN" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-124 pdf-obj-fixed acroform-field" name="WM" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-125 pdf-obj-fixed acroform-field" name="UW" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-126 pdf-obj-fixed acroform-field" name="US" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-127 pdf-obj-fixed acroform-field" name="UN" />
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot pdf-obj-0-128 pdf-obj-fixed acroform-field" name="BA" />
            <Field component="input" className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="11631784" data-annot-id="11020608" type="text" disabled />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2057;
