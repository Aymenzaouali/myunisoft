import React from "react";
import { Field } from 'redux-form';
import {useCompute, sum, setIsNothingness, parse as p, setValue, sumOfValues} from "helpers/pdfforms";
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import { get as _get } from 'lodash';

import './style.scss';

const amountOfTheEnd = (values, fields) => (
  p(values[fields[0]]) + p(values[fields[1]]) - p(values[fields[2]])
);

const calculationOfRate = (values, fields) => {
  if (values[fields[0]] < 500000) {
    return values[fields[1]] * 0;
  } else if (values[fields[0]] > 500000 && values[fields[0]] < 3000000) {
    return values[fields[1]] * (0.5 * (values[fields[0]] - 500000) / 2500000);
  } else if (values[fields[0]] > 3000000 && values[fields[0]] < 10000000) {
    return values[fields[1]] * (0.5 + 0.9 * (values[fields[0]] - 3000000) / 7000000);
  } else if (values[fields[0]] > 10000000 && values[fields[0]] < 50000000) {
    return values[fields[1]] * (1.4 + 0.1 * (values[fields[0]] - 10000000) / 40000000);
  } else if (values[fields[0]] > 50000000) {
    return values[fields[1]] * 1.5;
  }
};

const returnAnotherField = (values, fields) => {
  return values[fields[0]]
};

const Tax2033E = (props) => {
  const { taxDeclarationForm, form2033B, change } = props;
  const BJ = _get(form2033B, 'BJ', null);
  const BL = _get(form2033B, 'BL', null);
  const BA = _get(form2033B, 'BA', null);
  const BB = _get(form2033B, 'BB', null);
  const BC = _get(form2033B, 'BC', null);
  const BF = _get(form2033B, 'BF', 0);
  const BE = _get(form2033B, 'BE', 0);


  const notNeededProps = { 
    'NOM_STEA': true,
    'DEB_EX': true,
    'FIN_EX': true,
    'NBR_M_EX': true,
    'DB': true
  };
   
  setIsNothingness(taxDeclarationForm, notNeededProps, 'DB', change);

  useCompute('AF', sum, ['AJ', 'EN', 'EP', 'AE'], taxDeclarationForm, change);
  useCompute('AN', sum, ['EL', 'EE', 'EF', 'AK', 'AQ', 'GD'], taxDeclarationForm, change);
  useCompute('AP', sum, ['AL', 'AM', 'FE', 'FJ', 'FH', 'FK', 'GC', 'FN', 'FM'], taxDeclarationForm, change);

  useCompute('GA', amountOfTheEnd, ['AF', 'AN', 'AP'], taxDeclarationForm, change);
  useCompute('GB', calculationOfRate, ['AF', 'GA'], taxDeclarationForm, change);
  useCompute('KD', returnAnotherField, ['AF'], taxDeclarationForm, change);

  setValue(taxDeclarationForm, 'EF', BF, change);
  setValue(taxDeclarationForm, 'EE', BE, change);

  return (
    <div className="form-tax-declaration-2033e">
      <div id="pdf-document" data-type="pdf-document" data-num-pages="1" data-layout="fixed">

      </div>

      <form id="acroform"></form>

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.293333" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 373, top: 74, width: 16, height: 14, 'font-size': 17, }}>
<span>
19
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 900, top: 74, width: 194, height: 14, 'font-size': 15, }}>
<span>
DGFiP N° 2033-E-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 132, width: 654, height: 15, 'font-size': 15, }}>
<span>
Désignation de l&#39;entreprise: ....................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1000, top: 135, width: 39, height: 12, 'font-size': 15, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1044, top: 133, width: 22, height: 14, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 163, width: 439, height: 12, 'font-size': 15, }}>
<span>
Exercice ouvert le: .............................. et clos le: ..............................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 827, top: 163, width: 188, height: 12, 'font-size': 15, }}>
<span>
Données en nombre de mois
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 188, width: 222, height: 14, 'font-size': 15, }}>
<span>
DÉCLARATION DES EFFECTIFS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 217, width: 199, height: 15, 'font-size': 15, }}>
<span>
Effectif moyen du personnel * :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 958, top: 217, width: 24, height: 12, 'font-size': 15, }}>
<span>
376
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 245, top: 244, width: 94, height: 15, 'font-size': 15, }}>
<span>
Dont apprentis
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 958, top: 244, width: 24, height: 12, 'font-size': 15, }}>
<span>
657
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 245, top: 271, width: 109, height: 15, 'font-size': 15, }}>
<span>
Dont handicapés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 958, top: 271, width: 22, height: 12, 'font-size': 15, }}>
<span>
651
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 299, width: 243, height: 12, 'font-size': 15, }}>
<span>
Effectifs affectés à l&#39;activité artisanale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 958, top: 299, width: 22, height: 12, 'font-size': 15, }}>
<span>
861
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 326, width: 238, height: 12, 'font-size': 15, }}>
<span>
CALCUL DE LA VALEUR AJOUTEE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 353, width: 268, height: 12, 'font-size': 15, }}>
<span>
I - Chiffre d&#39;affaires de référence CVAE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 380, width: 458, height: 14, 'font-size': 15, }}>
<span>
Ventes de produits fabriqués, prestations de services et marchandises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 380, width: 23, height: 12, 'font-size': 15, }}>
<span>
108
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 407, width: 406, height: 15, 'font-size': 15, }}>
<span>
Redevances pour concessions, brevets, licences et assimilées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 407, width: 23, height: 12, 'font-size': 15, }}>
<span>
118
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 434, width: 761, height: 15, 'font-size': 15, }}>
<span>
Plus-values de cession d&#39;immobilisations corporelles ou incorporelles si rattachées à une activité normale et courante
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 434, width: 23, height: 12, 'font-size': 15, }}>
<span>
119
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 461, width: 432, height: 15, 'font-size': 15, }}>
<span>
Refacturations de frais inscrites au compte de transfert de charges
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 461, width: 23, height: 12, 'font-size': 15, }}>
<span>
105
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 887, top: 488, width: 95, height: 12, 'font-size': 15, }}>
<span>
TOTAL 1106
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 516, width: 431, height: 15, 'font-size': 15, }}>
<span>
II - Autres produits à retenir pour le calcul de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 543, width: 651, height: 15, 'font-size': 15, }}>
<span>
Autres produits de gestion courante (hors quotes-parts de résultat sur opérations faites en commun)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 544, width: 23, height: 11, 'font-size': 15, }}>
<span>
115
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 571, width: 628, height: 15, 'font-size': 15, }}>
<span>
Production immobilisée à hauteur des seules charges déductibles ayant concouru à sa formation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 571, width: 23, height: 12, 'font-size': 15, }}>
<span>
143
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 597, width: 218, height: 15, 'font-size': 15, }}>
<span>
Subventions d&#39;exploitation reçues
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 598, width: 23, height: 11, 'font-size': 15, }}>
<span>
113
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 625, width: 21, height: 12, 'font-size': 15, }}>
<span>
111
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 625, width: 185, height: 15, 'font-size': 15, }}>
<span>
Variation positive des stocks
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 652, width: 354, height: 15, 'font-size': 15, }}>
<span>
Transferts de charges déductibles de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 652, width: 23, height: 12, 'font-size': 15, }}>
<span>
116
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 679, width: 537, height: 15, 'font-size': 15, }}>
<span>
Rentrées sur créances amorties lorsqu&#39;elles se rapportent au résultat d&#39;exploitation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 679, width: 23, height: 12, 'font-size': 15, }}>
<span>
153
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 887, top: 706, width: 95, height: 12, 'font-size': 15, }}>
<span>
TOTAL 2144
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 737, width: 385, height: 15, 'font-size': 15, }}>
<span>
III - Charges à retenir pour le calcul de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 493, top: 732, width: 13, height: 11, 'font-size': 11, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 764, width: 45, height: 12, 'font-size': 15, }}>
<span>
Achats
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 764, width: 21, height: 12, 'font-size': 15, }}>
<span>
121
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 792, width: 23, height: 11, 'font-size': 15, }}>
<span>
145
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 792, width: 191, height: 14, 'font-size': 15, }}>
<span>
Variation négative des stocks
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 819, width: 409, height: 14, 'font-size': 15, }}>
<span>
Services extérieurs, à l&#39;exception des loyers et des redevances
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 819, width: 23, height: 12, 'font-size': 15, }}>
<span>
125
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 846, width: 826, height: 14, 'font-size': 15, }}>
<span>
Loyers et redevances, à l&#39;exception de ceux afférents à des immobilisations corporelles mises à disposition dans le cadre d&#39;une
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 863, width: 691, height: 14, 'font-size': 15, }}>
<span>
convention de location-gérance ou de crédit-bail ou encore d&#39;une convention de location de plus de 6 mois
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 846, width: 23, height: 12, 'font-size': 15, }}>
<span>
146
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 890, width: 251, height: 14, 'font-size': 15, }}>
<span>
Taxes déductibles de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 890, width: 23, height: 12, 'font-size': 15, }}>
<span>
133
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 917, width: 651, height: 15, 'font-size': 15, }}>
<span>
Autres charges de gestion courante (hors quotes-parts de résultat sur opérations faites en commun)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 917, width: 23, height: 12, 'font-size': 15, }}>
<span>
148
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 944, width: 570, height: 15, 'font-size': 15, }}>
<span>
Charges déductibles de la valeur ajoutée afférente à la production immobilisée déclarée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 944, width: 23, height: 12, 'font-size': 15, }}>
<span>
128
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 971, width: 844, height: 14, 'font-size': 14, }}>
<span>
Fraction déductible de la valeur ajoutée des dotations aux amortissements afférentes à des immobilisations corporelles mises à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 987, width: 790, height: 14, 'font-size': 14, }}>
<span>
disposition dans le cadre d&#39;une convention de location-gérance ou de crédit-bail ou encore d&#39;une convention de location de plus de 6 mois
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 959, top: 971, width: 23, height: 12, 'font-size': 15, }}>
<span>
135
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1013, width: 878, height: 15, 'font-size': 15, }}>
<span>
Moins-values de cession d&#39;immobilisations corporelles ou incorporelles si rattachées à une activité normale et courante150
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 887, top: 1040, width: 95, height: 12, 'font-size': 15, }}>
<span>
TOTAL 3152
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1067, width: 190, height: 15, 'font-size': 15, }}>
<span>
IV - Valeur ajoutée produite
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1094, width: 172, height: 15, 'font-size': 15, }}>
<span>
Calcul de la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 767, top: 1094, width: 215, height: 15, 'font-size': 15, }}>
<span>
(total 1 + total 2 - total 3)137
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1121, width: 354, height: 15, 'font-size': 15, }}>
<span>
V - Cotisation sur la valeur ajoutée des entreprises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1148, width: 879, height: 15, 'font-size': 15, }}>
<span>
Valeur ajoutée assujettie à la CVAE (à reporter sur les formulaires nos 1330-CVAE-SD pour les multi-établissements et sur les 117
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1165, width: 252, height: 15, 'font-size': 15, }}>
<span>
formulaires nos 1329-AC et 1329-DEF).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 395, top: 1195, width: 406, height: 12, 'font-size': 15, }}>
<span>
Cadre réservé au mono-établissement au sens de la CVAE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1222, width: 988, height: 15, 'font-size': 15, }}>
<span>
Si vous êtes assujettis à la CVAE et mono-établissement au sens de la CVAE (cf. la notice du formulaire n° 1330-CVAE-SD), veuillez
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1239, width: 799, height: 15, 'font-size': 15, }}>
<span>
compléter le cadre ci-dessous et la case 117, vous serez alors dispensés du dépôt du formulaire n° 1330-CVAE-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1266, width: 370, height: 14, 'font-size': 15, }}>
<span>
Mono établissement au sens de la CVAE, cocher la case
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 611, top: 1266, width: 24, height: 12, 'font-size': 15, }}>
<span>
020
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1293, width: 449, height: 15, 'font-size': 15, }}>
<span>
Chiffre d&#39;affaires de référence CVAE (report de la ligne 106)022
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 744, top: 1297, width: 187, height: 12, 'font-size': 15, }}>
<span>
Effectifs au sens de la CVAE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 936, top: 1294, width: 7, height: 7, 'font-size': 19, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 958, top: 1293, width: 24, height: 12, 'font-size': 15, }}>
<span>
023
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1325, width: 878, height: 14, 'font-size': 15, }}>
<span>
Chiffre d&#39;affaires du groupe économique (entreprises répondant aux conditions de détention fixées à l&#39;article 223 A du CGI)026
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1352, width: 136, height: 12, 'font-size': 15, }}>
<span>
Période de référence
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 494, top: 1352, width: 23, height: 12, 'font-size': 15, }}>
<span>
024
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 582, top: 1352, width: 86, height: 15, 'font-size': 19, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 796, top: 1352, width: 23, height: 12, 'font-size': 15, }}>
<span>
160
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 882, top: 1352, width: 81, height: 15, 'font-size': 19, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1383, width: 116, height: 12, 'font-size': 15, }}>
<span>
Date de cessation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 882, top: 1384, width: 81, height: 14, 'font-size': 19, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1416, width: 987, height: 15, 'font-size': 15, }}>
<span>
(1) Attention, il ne doit pas être tenu compte dans les lignes 121 à 148 des charges déductibles de la valeur ajoutée, afférente à la production
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1434, width: 345, height: 14, 'font-size': 15, }}>
<span>
immobilisée déclarée ligne 143, portées en ligne 128.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1450, width: 989, height: 15, 'font-size': 15, }}>
<span>
* Des explications concernant ces cases sont données dans la notice n° 1330-CVAE-SD § Répartition des salariés et dans la notice n° 2033-NOT-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 1467, width: 189, height: 15, 'font-size': 15, }}>
<span>
au § déclaration des effectifs.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 66, width: 224, height: 15, 'font-size': 15, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 83, width: 185, height: 15, 'font-size': 15, }}>
<span>
du code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 394, top: 61, width: 442, height: 12, 'font-size': 15, }}>
<span>
DETERMINATION DES EFFECTIFS ET DE LA VALEUR AJOUTEE
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="DB" data-field-id="22771704" data-annot-id="21642960" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="GB" data-field-id="22772008" data-annot-id="21643152" type="number" disabled />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="KA" data-field-id="22772344" data-annot-id="21814080" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="KD" data-field-id="22772648" data-annot-id="21814272" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="AH" data-field-id="22772984" data-annot-id="22017616" type="text" />

            <Field component={ReduxPicker}   className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field date-picker-input_style" name="KB" data-field-id="22773320" data-annot-id="22017808" type="text" />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field date-picker-input_style" name="KC" data-field-id="22773656" data-annot-id="22018000" type="text" />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field date-picker-input_style" name="KG" data-field-id="22773992" data-annot-id="22018192" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="AQ" data-field-id="22780360" data-annot-id="22018384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="GA" data-field-id="22774328" data-annot-id="22018576" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="AP" data-field-id="22774808" data-annot-id="22018768" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="FM" data-field-id="22775144" data-annot-id="22018960" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="FN" data-field-id="22775480" data-annot-id="22019152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="GC" data-field-id="22775816" data-annot-id="22019344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="FK" data-field-id="22776152" data-annot-id="22019536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="FH" data-field-id="22776488" data-annot-id="22019728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="FE" data-field-id="22776824" data-annot-id="22019920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="FJ" data-field-id="22777160" data-annot-id="22020384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="AM" data-field-id="22777640" data-annot-id="22020576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="AL" data-field-id="22777976" data-annot-id="22020768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="AN" data-field-id="22778312" data-annot-id="22020960" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="GD" data-field-id="22778648" data-annot-id="22021152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="AQ" data-field-id="22780360" data-annot-id="22021344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="AK" data-field-id="22779224" data-annot-id="22021536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="EF" data-field-id="22779528" data-annot-id="22021728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="EE" data-field-id="22780952" data-annot-id="22021920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="EL" data-field-id="22781288" data-annot-id="22022112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="AF" data-field-id="22781624" data-annot-id="22022304" type="number" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="AE" data-field-id="22781960" data-annot-id="22022496" type="number" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="EP" data-field-id="22782296" data-annot-id="22022688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="EN" data-field-id="22782632" data-annot-id="22022880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="AJ" data-field-id="22782968" data-annot-id="22023072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="AD" data-field-id="22783304" data-annot-id="22023264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="AC" data-field-id="22783640" data-annot-id="22020112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="AB" data-field-id="22777496" data-annot-id="22023984" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="AA" data-field-id="22784568" data-annot-id="22024176" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="12732840" data-annot-id="11970544" type="text" disabled />

            <Field component={ReduxPicker} onChange={change} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="DEB_EX" data-field-id="12733176" data-annot-id="11970736" type="text" disabled />

            <Field component={ReduxPicker} onChange={change} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="FIN_EX" data-field-id="12733512" data-annot-id="11970928" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field pde-form-field-text" name="NBR_M_EX" data-field-id="12733848" data-annot-id="11971120" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2033E;
