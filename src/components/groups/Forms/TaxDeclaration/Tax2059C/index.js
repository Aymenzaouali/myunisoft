import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { setIsNothingness } from 'helpers/pdfforms';

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const notNeededFields = {
  'NOM_STEA': true,
  'XM': true
};

const Tax2059C = (props) => {
  const { formValues, change } = props;
  setIsNothingness(formValues, notNeededFields, 'XM', change);
  
  return (
    <div className="form-tax-declaration-2059c">

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 86, top: 147, width: 165, height: 11, 'font-size': 12, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 159, width: 136, height: 12, 'font-size': 12, }}>
<span>
du Code général des impôts).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 396, top: 111, width: 455, height: 21, 'font-size': 23, }}>
<span>
SUIVI DES MOINS-VALUES À LONG TERME
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 941, top: 119, width: 234, height: 15, 'font-size': 20, }}>
<span>
DGFiP N° 2059-C-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 1534, width: 1101, height: 14, 'font-size': 14, }}>
<span>
(1) Les plus-values et les moins-values à long terme afférentes aux titres de SPI cotées imposables à l’impôt sur les sociétés relèvent du taux de 16,5% (article 219 I a du CGI), pour les exercices ou verts à compter
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 1550, width: 115, height: 10, 'font-size': 14, }}>
<span>
du 31 décembre 2007.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 1576, width: 427, height: 13, 'font-size': 14, }}>
<span>
* Des explications concernant cette rubrique sont données dans la notice n° 2032
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1000, top: 422, width: 53, height: 11, 'font-size': 14, }}>
<span>
Solde des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 990, top: 437, width: 73, height: 10, 'font-size': 14, }}>
<span>
moins-values
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1008, top: 451, width: 47, height: 12, 'font-size': 14, }}>
<span>
à 12,8 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1019, top: 485, width: 15, height: 16, 'font-size': 20, }}>
<span>
F
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 262, width: 217, height: 12, 'font-size': 12, }}>
<span>
 Entreprises soumises à l’impôt sur les sociétés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 276, width: 209, height: 12, 'font-size': 12, }}>
<span>
 Entreprises soumises à l’impôt sur le revenu
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 217, top: 355, width: 812, height: 18, 'font-size': 19, }}>
<span>
I - SUIVI DES MOINS-VALUES DES ENTREPRISES SOUMISES À L’IMPÔT SUR LE REVENU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 423, width: 43, height: 13, 'font-size': 14, }}>
<span>
Origine
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 484, width: 15, height: 16, 'font-size': 20, }}>
<span>
Q
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 383, top: 423, width: 73, height: 10, 'font-size': 14, }}>
<span>
Moins-values
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 437, width: 47, height: 12, 'font-size': 14, }}>
<span>
à 12,8 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 415, top: 485, width: 15, height: 15, 'font-size': 20, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 631, top: 422, width: 184, height: 13, 'font-size': 14, }}>
<span>
Imputations sur les plus-values à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 653, top: 437, width: 139, height: 13, 'font-size': 14, }}>
<span>
long terme de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 691, top: 451, width: 64, height: 13, 'font-size': 14, }}>
<span>
imposables
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 705, top: 466, width: 47, height: 13, 'font-size': 14, }}>
<span>
à 12,8 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 716, top: 485, width: 15, height: 15, 'font-size': 20, }}>
<span>
D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 516, width: 103, height: 10, 'font-size': 14, }}>
<span>
Moins-values nettes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 76, top: 199, width: 164, height: 15, 'font-size': 16, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 626, width: 102, height: 10, 'font-size': 14, }}>
<span>
Moins-values nettes à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 641, width: 51, height: 13, 'font-size': 14, }}>
<span>
long terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 655, width: 43, height: 10, 'font-size': 14, }}>
<span>
subies au
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 669, width: 44, height: 10, 'font-size': 14, }}>
<span>
cours des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 683, width: 59, height: 11, 'font-size': 14, }}>
<span>
dix exercices
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 698, width: 46, height: 10, 'font-size': 14, }}>
<span>
antérieurs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 713, width: 46, height: 12, 'font-size': 14, }}>
<span>
(montants
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 727, width: 40, height: 10, 'font-size': 14, }}>
<span>
restant à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 741, width: 54, height: 10, 'font-size': 14, }}>
<span>
déduire à la
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 755, width: 48, height: 11, 'font-size': 14, }}>
<span>
clôture du
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 134, top: 770, width: 34, height: 10, 'font-size': 14, }}>
<span>
dernier
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 784, width: 40, height: 13, 'font-size': 14, }}>
<span>
exercice)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 243, top: 518, width: 9, height: 8, 'font-size': 12, }}>
<span>
N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 552, width: 25, height: 9, 'font-size': 12, }}>
<span>
N – 1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 587, width: 26, height: 8, 'font-size': 12, }}>
<span>
N – 2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 621, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 655, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 690, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 724, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 759, width: 26, height: 8, 'font-size': 12, }}>
<span>
N – 7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 793, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 828, width: 26, height: 8, 'font-size': 12, }}>
<span>
N – 9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 232, top: 862, width: 31, height: 9, 'font-size': 12, }}>
<span>
N – 10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 906, width: 1004, height: 19, 'font-size': 19, }}>
<span>
II - SUIVI DES MOINS-VALUES À LONG TERME DES ENTREPRISES SOUMISES À L’IMPÔT SUR LES SOCIÉTÉS*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1019, width: 43, height: 12, 'font-size': 14, }}>
<span>
Origine
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 984, width: 73, height: 11, 'font-size': 14, }}>
<span>
Moins-values
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 969, width: 68, height: 13, 'font-size': 14, }}>
<span>
Imputations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 748, top: 985, width: 100, height: 13, 'font-size': 14, }}>
<span>
sur les plus-values
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 1000, width: 70, height: 13, 'font-size': 14, }}>
<span>
à long terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1071, top: 198, width: 36, height: 11, 'font-size': 16, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1153, top: 198, width: 6, height: 7, 'font-size': 16, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 454, top: 1028, width: 79, height: 14, 'font-size': 14, }}>
<span>
À 19 % ou 15 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 465, top: 1044, width: 57, height: 13, 'font-size': 14, }}>
<span>
imputables
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 459, top: 1057, width: 70, height: 11, 'font-size': 14, }}>
<span>
sur le résultat
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 461, top: 1071, width: 66, height: 10, 'font-size': 14, }}>
<span>
de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 1084, width: 74, height: 12, 'font-size': 14, }}>
<span>
(article 219 I a
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 474, top: 1098, width: 40, height: 10, 'font-size': 14, }}>
<span>
sexies-0
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 473, top: 1111, width: 41, height: 12, 'font-size': 14, }}>
<span>
du CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 606, top: 1028, width: 79, height: 14, 'font-size': 14, }}>
<span>
À 19 % ou 15 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 617, top: 1044, width: 57, height: 13, 'font-size': 14, }}>
<span>
imputables
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 603, top: 1057, width: 85, height: 11, 'font-size': 14, }}>
<span>
sur le résultat de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 620, top: 1071, width: 51, height: 10, 'font-size': 14, }}>
<span>
l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 608, top: 1084, width: 75, height: 12, 'font-size': 14, }}>
<span>
(article 219 I a
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 617, top: 1097, width: 58, height: 11, 'font-size': 14, }}>
<span>
sexies-0 bis
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 625, top: 1111, width: 41, height: 12, 'font-size': 14, }}>
<span>
du CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 321, top: 1057, width: 42, height: 14, 'font-size': 14, }}>
<span>
À 19 %,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 320, top: 1072, width: 32, height: 12, 'font-size': 14, }}>
<span>
16,5%
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 356, top: 1073, width: 10, height: 7, 'font-size': 9, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 1085, width: 25, height: 11, 'font-size': 14, }}>
<span>
ou à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 1100, width: 26, height: 10, 'font-size': 14, }}>
<span>
15 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 778, top: 1045, width: 39, height: 13, 'font-size': 14, }}>
<span>
À 15 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 791, top: 1072, width: 15, height: 8, 'font-size': 14, }}>
<span>
ou
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 766, top: 1089, width: 50, height: 15, 'font-size': 14, }}>
<span>
À 16,5 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 820, top: 1092, width: 10, height: 7, 'font-size': 9, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 243, top: 1157, width: 9, height: 9, 'font-size': 12, }}>
<span>
N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 234, top: 1192, width: 25, height: 8, 'font-size': 12, }}>
<span>
N – 1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 234, top: 1226, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 234, top: 1261, width: 26, height: 8, 'font-size': 12, }}>
<span>
N – 3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 234, top: 1295, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 234, top: 1329, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 234, top: 1364, width: 26, height: 8, 'font-size': 12, }}>
<span>
N – 6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 234, top: 1398, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 234, top: 1433, width: 26, height: 8, 'font-size': 12, }}>
<span>
N – 8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 234, top: 1467, width: 26, height: 9, 'font-size': 12, }}>
<span>
N – 9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 231, top: 1502, width: 32, height: 8, 'font-size': 12, }}>
<span>
N – 10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 1155, width: 103, height: 10, 'font-size': 14, }}>
<span>
Moins-values nettes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1251, width: 101, height: 10, 'font-size': 14, }}>
<span>
Moins-values nettes à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 1265, width: 51, height: 14, 'font-size': 14, }}>
<span>
long terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 1280, width: 43, height: 10, 'font-size': 14, }}>
<span>
subies au
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 1294, width: 44, height: 10, 'font-size': 14, }}>
<span>
cours des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 119, top: 1308, width: 59, height: 10, 'font-size': 14, }}>
<span>
dix exercices
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 1323, width: 46, height: 10, 'font-size': 14, }}>
<span>
antérieurs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 1337, width: 47, height: 13, 'font-size': 14, }}>
<span>
(montants
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 1352, width: 41, height: 10, 'font-size': 14, }}>
<span>
restant à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 122, top: 1366, width: 53, height: 10, 'font-size': 14, }}>
<span>
déduire à la
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 1380, width: 47, height: 10, 'font-size': 14, }}>
<span>
clôture du
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 132, top: 1395, width: 33, height: 10, 'font-size': 14, }}>
<span>
dernier
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 1409, width: 40, height: 13, 'font-size': 14, }}>
<span>
exercice)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 916, top: 1017, width: 69, height: 12, 'font-size': 14, }}>
<span>
Imputations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 913, top: 1033, width: 77, height: 10, 'font-size': 14, }}>
<span>
sur le résultat
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 914, top: 1050, width: 74, height: 11, 'font-size': 14, }}>
<span>
de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1071, top: 1003, width: 54, height: 10, 'font-size': 14, }}>
<span>
Solde des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1062, top: 1020, width: 73, height: 10, 'font-size': 14, }}>
<span>
moins-values
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1071, top: 1037, width: 58, height: 13, 'font-size': 14, }}>
<span>
à reporter
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1046, top: 1063, width: 105, height: 12, 'font-size': 14, }}>
<span>
col.J=S+D+F–G–H
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 394, top: 255, width: 552, height: 13, 'font-size': 14, }}>
<span>
Gains nets retirés de la cession de titre de sociétés à prépondérance immobilières non cotées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 394, top: 269, width: 345, height: 14, 'font-size': 14, }}>
<span>
exclus du régime du long terme (art. 219 I a sexies-0 bis du CGI) 
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 743, top: 278, width: 3, height: 2, 'font-size': 18, }}>
<span>
.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 394, top: 233, width: 459, height: 14, 'font-size': 14, }}>
<span>
Rappel de la plus ou moins-value de l’exercice relevant du taux de 15 %  ou 12,8 % .
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 394, top: 284, width: 552, height: 14, 'font-size': 14, }}>
<span>
Gains nets retirés de la cession de certains titres dont le prix de revient est supérieur à 22,8 M e
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 395, top: 299, width: 166, height: 14, 'font-size': 14, }}>
<span>
(art. 219 I a sexies-0 du CGI)  .
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 162, top: 1123, width: 15, height: 15, 'font-size': 20, }}>
<span>
Q
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 489, top: 1126, width: 15, height: 15, 'font-size': 20, }}>
<span>
D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1126, width: 15, height: 15, 'font-size': 20, }}>
<span>
F
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 794, top: 1126, width: 15, height: 16, 'font-size': 20, }}>
<span>
G
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1126, width: 15, height: 15, 'font-size': 20, }}>
<span>
H
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1098, top: 1126, width: 15, height: 16, 'font-size': 20, }}>
<span>
J
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 337, top: 1124, width: 15, height: 15, 'font-size': 20, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 298, top: 117, width: 22, height: 16, 'font-size': 21, }}>
<span>
14
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 1257, width: 12, height: 261, 'font-size': 12, }}>
<span>
N° 2059-C-SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="XM" data-field-id="13788248" data-annot-id="12983392" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="AC" data-field-id="13788616" data-annot-id="12807008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="AD" data-field-id="13788952" data-annot-id="12807232" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="AE" data-field-id="13789256" data-annot-id="12807424" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="BA" data-field-id="13789592" data-annot-id="12807616" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="BB" data-field-id="13789928" data-annot-id="12807808" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="BC" data-field-id="13790264" data-annot-id="13183120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="BD" data-field-id="13790600" data-annot-id="13183312" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="BE" data-field-id="13790936" data-annot-id="13183504" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="BF" data-field-id="13791416" data-annot-id="13183696" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="BG" data-field-id="13791752" data-annot-id="13183888" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="BH" data-field-id="13792088" data-annot-id="13184080" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="BJ" data-field-id="13792424" data-annot-id="13184272" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="BK" data-field-id="13792760" data-annot-id="13184464" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="BL" data-field-id="13793096" data-annot-id="13184656" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="CB" data-field-id="13793432" data-annot-id="13184848" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="CC" data-field-id="13793768" data-annot-id="13185040" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="CD" data-field-id="13794248" data-annot-id="13185504" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="CE" data-field-id="13794584" data-annot-id="13185696" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="CF" data-field-id="13794920" data-annot-id="13185888" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="CG" data-field-id="13795256" data-annot-id="13186080" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="CH" data-field-id="13795592" data-annot-id="13186272" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="CJ" data-field-id="13795928" data-annot-id="13186464" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="CK" data-field-id="13796264" data-annot-id="13186656" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="CL" data-field-id="13796600" data-annot-id="13186848" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="DA" data-field-id="13796936" data-annot-id="13187040" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="DB" data-field-id="13797272" data-annot-id="13187232" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="DD" data-field-id="13797608" data-annot-id="13187424" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="DC" data-field-id="13797944" data-annot-id="13187616" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="DE" data-field-id="13798280" data-annot-id="13187808" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="DF" data-field-id="13798616" data-annot-id="13188000" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="DG" data-field-id="13798952" data-annot-id="13188192" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="DH" data-field-id="13799288" data-annot-id="13188384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="DJ" data-field-id="13794104" data-annot-id="13185232" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="DK" data-field-id="13800216" data-annot-id="13189104" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="DL" data-field-id="13800552" data-annot-id="13189296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="KA" data-field-id="13800888" data-annot-id="13189488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="KB" data-field-id="13801224" data-annot-id="13189680" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="KC" data-field-id="13801560" data-annot-id="13189872" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="KD" data-field-id="13801896" data-annot-id="13190064" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="KE" data-field-id="13802232" data-annot-id="13190256" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="KF" data-field-id="13802568" data-annot-id="13190448" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="KG" data-field-id="13802904" data-annot-id="13190640" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="KH" data-field-id="13803240" data-annot-id="13190832" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="KJ" data-field-id="13803576" data-annot-id="13191024" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="KK" data-field-id="13803912" data-annot-id="13191216" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="KL" data-field-id="13804248" data-annot-id="13191408" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="LA" data-field-id="13804584" data-annot-id="13191600" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="LB" data-field-id="13804920" data-annot-id="13191792" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="LC" data-field-id="13805256" data-annot-id="13191984" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="LD" data-field-id="13805592" data-annot-id="13192176" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="LE" data-field-id="13805928" data-annot-id="13192368" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="LF" data-field-id="13806264" data-annot-id="13192560" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="LG" data-field-id="13806600" data-annot-id="13192752" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="LH" data-field-id="13806936" data-annot-id="13192944" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="LJ" data-field-id="13807272" data-annot-id="13193136" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="LK" data-field-id="13807608" data-annot-id="13193328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="LL" data-field-id="13807944" data-annot-id="13193520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="MA" data-field-id="13808280" data-annot-id="13193712" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="MB" data-field-id="13808616" data-annot-id="13193904" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="MC" data-field-id="13808952" data-annot-id="13194096" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="MD" data-field-id="13809288" data-annot-id="13194288" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="ME" data-field-id="13809624" data-annot-id="13194480" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="MF" data-field-id="13809960" data-annot-id="13194672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="MG" data-field-id="13810296" data-annot-id="13194864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="MH" data-field-id="13787848" data-annot-id="13177328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="MJ" data-field-id="13799592" data-annot-id="13188576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="MK" data-field-id="13811544" data-annot-id="13188768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="ML" data-field-id="13811784" data-annot-id="13196096" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="HB" data-field-id="13812120" data-annot-id="13196288" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="HC" data-field-id="13812456" data-annot-id="13196480" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="HD" data-field-id="13812792" data-annot-id="13196672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="HE" data-field-id="13813128" data-annot-id="13196864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="HF" data-field-id="13813464" data-annot-id="13197056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="HG" data-field-id="13813800" data-annot-id="13197248" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="HH" data-field-id="13814136" data-annot-id="13197440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="HJ" data-field-id="13814472" data-annot-id="13197632" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="HK" data-field-id="13814808" data-annot-id="13197824" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="HL" data-field-id="13815144" data-annot-id="13198016" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="VA" data-field-id="13815480" data-annot-id="13198208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="VB" data-field-id="13815816" data-annot-id="13198400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="VC" data-field-id="13816152" data-annot-id="13198592" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="VD" data-field-id="13816488" data-annot-id="13198784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="VE" data-field-id="13816824" data-annot-id="13198976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="VF" data-field-id="13817160" data-annot-id="13199168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="VG" data-field-id="13817496" data-annot-id="13199360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="VH" data-field-id="13817832" data-annot-id="13199552" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="VJ" data-field-id="13818168" data-annot-id="13199744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="VK" data-field-id="13818504" data-annot-id="13199936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="WA" data-field-id="13818840" data-annot-id="13200128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field " name="WB" data-field-id="13819176" data-annot-id="13200320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="WC" data-field-id="13819512" data-annot-id="13200512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="WE" data-field-id="13819848" data-annot-id="13200704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="WF" data-field-id="13820184" data-annot-id="13200896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="WG" data-field-id="13820856" data-annot-id="13201088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="WH" data-field-id="13820520" data-annot-id="13201280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="WJ" data-field-id="13821192" data-annot-id="13201472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="WK" data-field-id="13821528" data-annot-id="13201664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field " name="WL" data-field-id="13821864" data-annot-id="13201856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="VL" data-field-id="13822200" data-annot-id="13202048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field " name="WD" data-field-id="13822536" data-annot-id="13202240" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="40121560" data-annot-id="39500688" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2059C;
