import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {sum, useCompute} from "helpers/pdfforms";

import './style.scss';

const Tax2065BIS = (props) => {
  const { taxDeclarationForm, change } = props;

  useCompute('AJ', sum, ['AA', 'AB', 'AC', 'AD', 'AE1', 'AE2', 'AE3', 'AE4', 'AK', 'AL'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2065bis">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 91, top: 69, width: 116, height: 13, 'font-size': 13, }}>
<span>
Formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1096, top: 68, width: 34, height: 12, 'font-size': 16, }}>
<span>
2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 90, width: 184, height: 13, 'font-size': 13, }}>
<span>
(art 223 du Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 50, top: 139, width: 11, height: 13, 'font-size': 17, }}>
<span>
H
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 170, width: 197, height: 14, 'font-size': 14, }}>
<span>
payées par la société elle-même
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 534, top: 173, width: 8, height: 9, 'font-size': 14, }}>
<span>
a
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 667, top: 170, width: 347, height: 14, 'font-size': 14, }}>
<span>
payées par un établissement chargé du service des titres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1043, top: 171, width: 8, height: 11, 'font-size': 14, }}>
<span>
b
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 204, width: 8, height: 9, 'font-size': 16, }}>
<span>
c
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 236, width: 11, height: 12, 'font-size': 16, }}>
<span>
d
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 271, width: 8, height: 9, 'font-size': 16, }}>
<span>
e
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 293, width: 8, height: 12, 'font-size': 16, }}>
<span>
f
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 321, width: 10, height: 13, 'font-size': 16, }}>
<span>
g
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 343, width: 9, height: 13, 'font-size': 16, }}>
<span>
h
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 962, top: 371, width: 6, height: 13, 'font-size': 16, }}>
<span>
i
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 401, width: 723, height: 15, 'font-size': 16, }}>
<span>
Montant des revenus distribués non éligibles à l&#39;abattement de 40% prévu au 2° du 3 de l&#39;article 158 du CGI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 960, top: 401, width: 8, height: 15, 'font-size': 16, }}>
<span>
j
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 894, top: 429, width: 84, height: 16, 'font-size': 16, }}>
<span>
Total (a à h)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 458, width: 3, height: 13, 'font-size': 17, }}>
<span>
I
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 540, width: 390, height: 14, 'font-size': 14, }}>
<span>
Nom, prénoms, domicile et qualité (art. 48-1 à 6 ann. III au CGI):
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 512, width: 705, height: 14, 'font-size': 14, }}>
<span>
Pour les SARL chaque associé, gérant ou non, désigné col.1, à titre de traitements, émoluments, indemnités,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 566, width: 161, height: 13, 'font-size': 14, }}>
<span>
* SARL, tous les associés;
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 814, top: 566, width: 186, height: 11, 'font-size': 14, }}>
<span>
Montant des sommes versées:
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 875, top: 680, width: 96, height: 10, 'font-size': 13, }}>
<span>
Remboursements
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1066, top: 680, width: 96, height: 10, 'font-size': 13, }}>
<span>
Remboursements
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 252, top: 711, width: 6, height: 12, 'font-size': 14, }}>
<span>
1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 498, top: 713, width: 8, height: 12, 'font-size': 16, }}>
<span>
2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 597, top: 713, width: 9, height: 12, 'font-size': 16, }}>
<span>
3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 707, top: 711, width: 8, height: 12, 'font-size': 14, }}>
<span>
4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 816, top: 713, width: 9, height: 12, 'font-size': 16, }}>
<span>
5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 920, top: 711, width: 8, height: 12, 'font-size': 14, }}>
<span>
6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1018, top: 712, width: 8, height: 11, 'font-size': 14, }}>
<span>
7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1110, top: 713, width: 9, height: 12, 'font-size': 16, }}>
<span>
8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 1042, width: 98, height: 14, 'font-size': 17, }}>
<span>
J DIVERS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 1066, width: 551, height: 16, 'font-size': 16, }}>
<span>
* NOM ET ADRESSE DU PROPRIETAIRE DU FONDS ( en cas de gérance libre)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 50, top: 1235, width: 12, height: 13, 'font-size': 17, }}>
<span>
K
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 652, top: 1289, width: 335, height: 15, 'font-size': 16, }}>
<span>
MVLT restant à reporter à l&#39;ouverture de l&#39;exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 652, top: 1313, width: 277, height: 15, 'font-size': 16, }}>
<span>
MVLT imputée sur les PVLT de l&#39;exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 652, top: 1337, width: 247, height: 13, 'font-size': 16, }}>
<span>
MVLT réalisée au cours de l&#39;exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 652, top: 1362, width: 161, height: 15, 'font-size': 16, }}>
<span>
MVLT restant à reporter
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 472, top: 34, width: 278, height: 21, 'font-size': 22, }}>
<span>
IMPÔT SUR LES SOCI ÉTÉS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1027, top: 39, width: 128, height: 15, 'font-size': 19, }}>
<span>
N° 2065 bis -SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 88, width: 335, height: 15, 'font-size': 19, }}>
<span>
ANNEXE AU FORMULAIRE N° 2065 -SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 87, top: 136, width: 957, height: 19, 'font-size': 17, }}>
<span>
RÉPARTITION DES PRODUITS DES ACTIONS ET PARTS SOCIALES, AINSI QUE DES REVENUS ASSIMIL ÉS DISTRIBUÉS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 170, width: 245, height: 15, 'font-size': 16, }}>
<span>
Montant global brut des distributions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 281, top: 169, width: 11, height: 10, 'font-size': 9, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 201, width: 877, height: 15, 'font-size': 16, }}>
<span>
Montant des distributions correspondant à des rémunérations ou avantages dont la société ne désigne pas le (les) bénéficiaire (s)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 913, top: 201, width: 11, height: 9, 'font-size': 9, }}>
<span>
(2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 227, width: 913, height: 15, 'font-size': 16, }}>
<span>
Montant des prêts, avances ou acomptes consentis aux associés, actionnaires et porteurs de parts, soit directement, soit par
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 244, width: 154, height: 16, 'font-size': 16, }}>
<span>
personnes interposées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 289, width: 169, height: 12, 'font-size': 16, }}>
<span>
Montant des distributions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 306, width: 183, height: 15, 'font-size': 16, }}>
<span>
autres que celles visées en
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 322, width: 180, height: 16, 'font-size': 16, }}>
<span>
(a), (b), (c) et (d) ci-dessus
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 216, top: 322, width: 10, height: 9, 'font-size': 9, }}>
<span>
(3)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 371, width: 699, height: 16, 'font-size': 16, }}>
<span>
Montant des revenus distribués éligibles à l&#39;abattement de 40% prévu au 2° du 3 de l&#39;article 158 du CGI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 738, top: 373, width: 9, height: 8, 'font-size': 8, }}>
<span>
(4)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 429, width: 197, height: 15, 'font-size': 16, }}>
<span>
Montant des revenus répartis
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 235, top: 431, width: 9, height: 8, 'font-size': 8, }}>
<span>
(5)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 87, top: 454, width: 648, height: 17, 'font-size': 17, }}>
<span>
RÉMUNÉRATIONS NETTES VERSÉES AUX MEMBRES DE CERTAINES SOCI ÉTÉS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 740, top: 460, width: 367, height: 14, 'font-size': 14, }}>
<span>
(si ce cadre est insuffisant, joindre un état du même modèle)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 557, top: 497, width: 605, height: 14, 'font-size': 14, }}>
<span>
Sommes versées, au cours de la période retenue pour l&#39;assiette de l&#39;impôt sur les sociétés, à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 557, top: 527, width: 574, height: 12, 'font-size': 14, }}>
<span>
remboursements forfaitaires de frais ou autres rémunérations de ses fonctions dans la société.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 592, width: 89, height: 13, 'font-size': 13, }}>
<span>
Nombre de parts
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 479, top: 606, width: 44, height: 10, 'font-size': 13, }}>
<span>
sociales
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 464, top: 620, width: 75, height: 12, 'font-size': 13, }}>
<span>
appartenant à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 459, top: 633, width: 85, height: 12, 'font-size': 13, }}>
<span>
chaque associé
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 454, top: 647, width: 95, height: 12, 'font-size': 13, }}>
<span>
en toute propriété
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 660, width: 76, height: 10, 'font-size': 13, }}>
<span>
ou en usufruit.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 571, top: 595, width: 58, height: 12, 'font-size': 14, }}>
<span>
Année au
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 575, top: 611, width: 52, height: 11, 'font-size': 14, }}>
<span>
cours de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 626, width: 61, height: 13, 'font-size': 14, }}>
<span>
laquelle le
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 563, top: 641, width: 75, height: 11, 'font-size': 14, }}>
<span>
versement a
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 565, top: 656, width: 71, height: 11, 'font-size': 14, }}>
<span>
été effectué
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 597, width: 151, height: 14, 'font-size': 14, }}>
<span>
* SCA, associés gérants;
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 612, width: 301, height: 13, 'font-size': 14, }}>
<span>
*SNC ou SCS, associés en nom ou commandités
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 627, width: 319, height: 14, 'font-size': 14, }}>
<span>
* SEP et sté de copropriétaires de navires, associés,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 62, top: 642, width: 153, height: 14, 'font-size': 14, }}>
<span>
gérants ou coparticipants
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 684, top: 610, width: 53, height: 12, 'font-size': 14, }}>
<span>
à titre de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 674, top: 625, width: 72, height: 13, 'font-size': 14, }}>
<span>
traitements,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 666, top: 640, width: 90, height: 12, 'font-size': 14, }}>
<span>
émoluments et
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 656, width: 65, height: 11, 'font-size': 14, }}>
<span>
indemnités
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 663, top: 671, width: 95, height: 13, 'font-size': 14, }}>
<span>
proprement dits
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 776, top: 612, width: 197, height: 14, 'font-size': 14, }}>
<span>
à titre de frais de représentation,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 776, top: 627, width: 183, height: 14, 'font-size': 14, }}>
<span>
de mission et de déplacement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 986, top: 591, width: 175, height: 14, 'font-size': 14, }}>
<span>
à titre de frais professionnels
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 991, top: 606, width: 166, height: 14, 'font-size': 14, }}>
<span>
autres que ceux visés dans
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1017, top: 621, width: 113, height: 12, 'font-size': 14, }}>
<span>
les colonnes 5 et 6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 671, width: 66, height: 12, 'font-size': 14, }}>
<span>
Indemnités
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 787, top: 687, width: 64, height: 11, 'font-size': 14, }}>
<span>
forfaitaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 992, top: 668, width: 59, height: 11, 'font-size': 13, }}>
<span>
Indemnités
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 992, top: 682, width: 57, height: 10, 'font-size': 13, }}>
<span>
forfaitaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 1136, width: 709, height: 15, 'font-size': 16, }}>
<span>
* ADRESSES DES AUTRES ETABLISSEMENTS (si ce cadre est insuffisant, joindre un état du même modèle)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 87, top: 1231, width: 848, height: 18, 'font-size': 17, }}>
<span>
CADRE NE CONCERNANT QUE LES ENTREPRISES PLACÉES SOUS LE RÉGIME SIMPLIFIÉ D&#39;IMPOSITION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 271, top: 1260, width: 133, height: 15, 'font-size': 16, }}>
<span>
REMUNÉRATIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 723, top: 1260, width: 368, height: 15, 'font-size': 16, }}>
<span>
MOINS-VALUES A LONG TERME IMPOS ÉES A 15%
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 1294, width: 487, height: 14, 'font-size': 14, }}>
<span>
Montant brut des salaires, abstraction faite des sommes comprises dans les
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 1309, width: 375, height: 14, 'font-size': 14, }}>
<span>
DSN et versées aux apprentis sous contrat et aux handicapés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 416, top: 1309, width: 10, height: 8, 'font-size': 9, }}>
<span>
(a)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 31, top: 1350, width: 370, height: 14, 'font-size': 14, }}>
<span>
Rétrocessions d&#39;honoraires, de commissions et de courtages
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 411, top: 1350, width: 10, height: 8, 'font-size': 9, }}>
<span>
(b)
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="AA" data-field-id="24932696" data-annot-id="24083168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="AB" data-field-id="24944008" data-annot-id="23907056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="AC" data-field-id="24944344" data-annot-id="23907280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="AD" data-field-id="24944648" data-annot-id="24285920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="AE1" data-field-id="24944984" data-annot-id="24286112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="AE2" data-field-id="24945320" data-annot-id="24286304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="AE3" data-field-id="24945656" data-annot-id="24286496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="AE4" data-field-id="24945992" data-annot-id="24286688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="BE1" data-field-id="24946328" data-annot-id="24286880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="BE2" data-field-id="24946808" data-annot-id="24287072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="BE3" data-field-id="24947144" data-annot-id="24287264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="BE4" data-field-id="24947480" data-annot-id="24287456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="AK" data-field-id="24947816" data-annot-id="24287648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="AL" data-field-id="24948152" data-annot-id="24287840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="AJ" data-field-id="24948488" data-annot-id="24288032" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="CAD1" data-field-id="24954888" data-annot-id="24288224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="CG1" data-field-id="24948824" data-annot-id="24288416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="CG2" data-field-id="24949160" data-annot-id="24288880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="CG3" data-field-id="24949640" data-annot-id="24289072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="CG4" data-field-id="24949976" data-annot-id="24289264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="CG5" data-field-id="24950312" data-annot-id="24289456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="CG6" data-field-id="24950648" data-annot-id="24289648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="CH6" data-field-id="24950984" data-annot-id="24289840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="CH5" data-field-id="24951320" data-annot-id="24290032" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="CH4" data-field-id="24951656" data-annot-id="24290224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="CH3" data-field-id="24951992" data-annot-id="24290416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="CH2" data-field-id="24952328" data-annot-id="24290608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="CH1" data-field-id="24952664" data-annot-id="24290800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="CJ6" data-field-id="24953000" data-annot-id="24290992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="CJ5" data-field-id="24953336" data-annot-id="24291184" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="CJ4" data-field-id="24953672" data-annot-id="24291376" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="CJ2" data-field-id="24954008" data-annot-id="24291568" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="CJ3" data-field-id="24954344" data-annot-id="24291760" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="CJ1" data-field-id="24954680" data-annot-id="24288608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="CK6" data-field-id="24949496" data-annot-id="24292480" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="CK5" data-field-id="24955608" data-annot-id="24292672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="CK4" data-field-id="24955944" data-annot-id="24292864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="CK3" data-field-id="24956280" data-annot-id="24293056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="CK2" data-field-id="24956616" data-annot-id="24293248" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="CK1" data-field-id="24956952" data-annot-id="24293440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="CL6" data-field-id="24957288" data-annot-id="24293632" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="CL5" data-field-id="24957624" data-annot-id="24293824" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="CL4" data-field-id="24957960" data-annot-id="24294016" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="CL3" data-field-id="24958296" data-annot-id="24294208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="CL2" data-field-id="24958632" data-annot-id="24294400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="CL1" data-field-id="24958968" data-annot-id="24294592" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="CM6" data-field-id="24959304" data-annot-id="24294784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="CM5" data-field-id="24959640" data-annot-id="24294976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="CM4" data-field-id="24959976" data-annot-id="24295168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="CM3" data-field-id="24960312" data-annot-id="24295360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="CM2" data-field-id="24960648" data-annot-id="24295552" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="CM1" data-field-id="24960984" data-annot-id="24295744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="CN6" data-field-id="24963304" data-annot-id="24295936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="CN5" data-field-id="24961320" data-annot-id="24296128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="CN4" data-field-id="24961656" data-annot-id="24296320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="CN3" data-field-id="24962472" data-annot-id="24296512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="CN2" data-field-id="24962200" data-annot-id="24296704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="CN1" data-field-id="24963800" data-annot-id="24296896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="CV" data-field-id="24964104" data-annot-id="24297088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="CW" data-field-id="24964440" data-annot-id="24297280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="CX" data-field-id="24964776" data-annot-id="24297472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="CY" data-field-id="24965112" data-annot-id="24297664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="CZ" data-field-id="24965448" data-annot-id="24297856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="DC" data-field-id="24965784" data-annot-id="24298048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="DB" data-field-id="24966120" data-annot-id="24298240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="DA" data-field-id="24966456" data-annot-id="24291952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="CAE1" data-field-id="24955192" data-annot-id="24292144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="CAF1" data-field-id="24967832" data-annot-id="24299472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="CAG1" data-field-id="24968168" data-annot-id="24299664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="CAH1" data-field-id="24968504" data-annot-id="24299856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="CAI1" data-field-id="24968840" data-annot-id="24300048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="CAD2" data-field-id="24969176" data-annot-id="24300240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="CAE2" data-field-id="24969512" data-annot-id="24300432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="CAF2" data-field-id="24969848" data-annot-id="24300624" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="CAG2" data-field-id="24970184" data-annot-id="24300816" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="CAH2" data-field-id="24970520" data-annot-id="24301008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="CAI2" data-field-id="24970856" data-annot-id="24301200" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="CAD3" data-field-id="24971192" data-annot-id="24301392" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="CAE3" data-field-id="24971528" data-annot-id="24301584" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="CAF3" data-field-id="24971864" data-annot-id="24301776" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="CAG3" data-field-id="24972200" data-annot-id="24301968" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="CAH3" data-field-id="24972536" data-annot-id="24302160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="CAI3" data-field-id="24972872" data-annot-id="24302352" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="CAD4" data-field-id="24973208" data-annot-id="24302544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="CAE4" data-field-id="24973544" data-annot-id="24302736" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="CAF4" data-field-id="24973880" data-annot-id="24302928" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="CAG4" data-field-id="24974216" data-annot-id="24303120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="CAH4" data-field-id="24974552" data-annot-id="24303312" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="CAI4" data-field-id="24974888" data-annot-id="24303504" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="CAD5" data-field-id="24975224" data-annot-id="24303696" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field " name="CAE5" data-field-id="24975560" data-annot-id="24303888" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="CAF5" data-field-id="24975896" data-annot-id="24304080" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="CAG5" data-field-id="24976232" data-annot-id="24304272" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="CAH5" data-field-id="24976568" data-annot-id="24304464" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="CAI5" data-field-id="24976904" data-annot-id="24304656" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="CAD6" data-field-id="24977240" data-annot-id="24304848" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="CAE6" data-field-id="24977576" data-annot-id="24305040" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="CAF6" data-field-id="24977912" data-annot-id="24305232" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field " name="CAG6" data-field-id="24978248" data-annot-id="24305424" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="CAH6" data-field-id="24978584" data-annot-id="24305616" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field " name="CAI6" data-field-id="24978920" data-annot-id="24305808" type="text" />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2065BIS;
