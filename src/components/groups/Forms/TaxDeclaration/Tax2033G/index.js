import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { setIsNothingness } from 'helpers/pdfforms';

import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';

import './style.scss';

const Tax2033G = (props) => {
  const { taxDeclarationForm, change } = props;
  console.log('taxDeclarationForm :', taxDeclarationForm);
  const notNeededProps = { 
    'FIN_EX': true,
    'SIREN_STET': true,
    'NOM_STEA': true,
    'ADR_STED': true,
    'ADR_STEF': true,
    'ADR_STEG': true,
    'CP_STEH': true,
    'VILLE_STEI': true,
    'GS': true
  };
  setIsNothingness(taxDeclarationForm, notNeededProps, 'GS', change);
  return (
    <div className="form-tax-declaration-2033g">
      <div id="pdf-document" data-type="pdf-document" data-num-pages="1" data-layout="fixed">

      </div>

      <form id="acroform"></form>

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.293333" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 287, top: 51, width: 31, height: 32, 'font-size': 40, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 59, width: 265, height: 15, 'font-size': 21, }}>
<span>
FILIALES ET PARTICIPATIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 56, width: 49, height: 12, 'font-size': 18, }}>
<span>
DGFiP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 929, top: 53, width: 143, height: 17, 'font-size': 21, }}>
<span>
N° 2033-G-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 1260, width: 10, height: 209, 'font-size': 11, }}>
<span>
N° 2033-F-SD – (SDNC-DGFiP) - Janvier 201
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 74, width: 99, height: 11, 'font-size': 11, }}>
<span>
Formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 86, width: 147, height: 8, 'font-size': 10, }}>
<span>
(article 38 de l’annexe III au C.G.I.)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 371, top: 105, width: 459, height: 14, 'font-size': 14, }}>
<span>
( liste des personnes ou groupements de personnes de droit ou de fait
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 398, top: 123, width: 404, height: 14, 'font-size': 14, }}>
<span>
dont la société détient directement au moins 10 % du capital )
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 100, width: 83, height: 16, 'font-size': 16, }}>
<span>
N° de dépôt
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 100, width: 19, height: 13, 'font-size': 14, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 984, top: 115, width: 35, height: 11, 'font-size': 14, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1061, top: 116, width: 6, height: 7, 'font-size': 21, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 172, width: 104, height: 12, 'font-size': 16, }}>
<span>
Exercice clos le
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 817, top: 173, width: 41, height: 11, 'font-size': 16, }}>
<span>
SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 207, width: 196, height: 16, 'font-size': 16, }}>
<span>
Dénomination de l’entreprise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 241, width: 98, height: 14, 'font-size': 16, }}>
<span>
Adresse (voie)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 276, width: 80, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 342, top: 276, width: 31, height: 12, 'font-size': 16, }}>
<span>
Ville
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 301, width: 416, height: 13, 'font-size': 14, }}>
<span>
I - NOMBRE TOTAL DE FILIALES DÉTENUES PAR L’ENTREPRISE :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 607, top: 303, width: 23, height: 13, 'font-size': 16, }}>
<span>
905
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 356, width: 107, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 357, width: 96, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 391, width: 64, height: 11, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 392, width: 160, height: 11, 'font-size': 13, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 390, width: 103, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 423, width: 102, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 424, width: 31, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 455, width: 70, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 456, width: 70, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 456, width: 30, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 498, width: 107, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 499, width: 96, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 533, width: 64, height: 12, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 534, width: 160, height: 12, 'font-size': 13, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 532, width: 103, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 565, width: 102, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 566, width: 31, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 597, width: 70, height: 13, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 598, width: 70, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 599, width: 30, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 641, width: 107, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 641, width: 96, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 676, width: 64, height: 11, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 676, width: 160, height: 12, 'font-size': 13, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 675, width: 103, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 707, width: 102, height: 13, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 708, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 740, width: 70, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 741, width: 70, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 741, width: 30, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 783, width: 107, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 784, width: 96, height: 11, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 818, width: 64, height: 11, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 819, width: 160, height: 11, 'font-size': 13, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 817, width: 103, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 850, width: 102, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 851, width: 31, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 882, width: 70, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 883, width: 70, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 883, width: 30, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 925, width: 107, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 926, width: 96, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 960, width: 64, height: 12, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 961, width: 160, height: 12, 'font-size': 13, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 959, width: 103, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 992, width: 102, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 993, width: 31, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 1024, width: 70, height: 13, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 1025, width: 70, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 1026, width: 30, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1068, width: 107, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 1068, width: 96, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1103, width: 64, height: 11, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 1103, width: 160, height: 12, 'font-size': 13, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 1102, width: 103, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1134, width: 102, height: 13, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 1135, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 1167, width: 70, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 1168, width: 70, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 1168, width: 30, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1210, width: 107, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 1211, width: 96, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1245, width: 64, height: 11, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 1246, width: 160, height: 12, 'font-size': 13, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 1244, width: 103, height: 12, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1277, width: 102, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 1278, width: 31, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 1309, width: 70, height: 13, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 1310, width: 70, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 1310, width: 30, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1353, width: 107, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 351, top: 1353, width: 96, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1387, width: 64, height: 12, 'font-size': 16, }}>
<span>
N° SIREN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 1388, width: 160, height: 12, 'font-size': 13, }}>
<span>
(si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 1386, width: 103, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1419, width: 102, height: 12, 'font-size': 16, }}>
<span>
Adresse : N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 1420, width: 31, height: 11, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 1452, width: 70, height: 12, 'font-size': 16, }}>
<span>
Code Postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 408, top: 1453, width: 70, height: 11, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 1453, width: 30, height: 14, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 1478, width: 980, height: 12, 'font-size': 13, }}>
<span>
(1) Lorsque le nombre de filiales excède le nombre de lignes de l’imprimé, utiliser un ou plusieurs tableaux supplémentaires. Dans ce cas, il convient de numéroter chaque tableau en haut
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 1492, width: 666, height: 12, 'font-size': 13, }}>
<span>
et à gauche de la case prévue à cet effet et de porter le nombre total de tableaux souscrits en bas à droite de cette même case.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 1508, width: 5, height: 5, 'font-size': 16, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1506, width: 423, height: 12, 'font-size': 13, }}>
<span>
Des explications concernant cette rubrique figurent dans la notice 2033-NOT-SD.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 1254, width: 8, height: 5, 'font-size': 11, }}>
<span>
9
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="GS" data-field-id="11599048" data-annot-id="10935376" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="GT" data-field-id="11599384" data-annot-id="11082064" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component="input" className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="GAK1" data-field-id="11600680" data-annot-id="11082256" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="GAA1" data-field-id="11600984" data-annot-id="11082448" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="GAT1" data-field-id="11601320" data-annot-id="11082640" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="GR1" data-field-id="11601656" data-annot-id="10812544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="GAD1" data-field-id="11601992" data-annot-id="10812736" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="GAE1" data-field-id="11602328" data-annot-id="10812928" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="GAF1" data-field-id="11602664" data-annot-id="10813120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="GAG1" data-field-id="11603144" data-annot-id="11092560" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="GAH1" data-field-id="11603480" data-annot-id="11092752" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="GAI1" data-field-id="11603816" data-annot-id="11092944" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="GAJ1" data-field-id="11604152" data-annot-id="11093136" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="GAK2" data-field-id="11604488" data-annot-id="11093328" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="GAA2" data-field-id="11604824" data-annot-id="11093520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="GR2" data-field-id="11605160" data-annot-id="11093712" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="GAJ2" data-field-id="11605496" data-annot-id="11093904" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="GAG2" data-field-id="11605976" data-annot-id="10813312" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="GAI2" data-field-id="11606312" data-annot-id="11094368" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="GAF2" data-field-id="11606648" data-annot-id="11094560" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="GAT2" data-field-id="11606984" data-annot-id="11094752" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="GAD2" data-field-id="11607320" data-annot-id="11094944" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="GAE2" data-field-id="11607656" data-annot-id="11095136" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="GAH2" data-field-id="11607992" data-annot-id="11095328" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="GAK3" data-field-id="11608328" data-annot-id="11095520" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="GAA3" data-field-id="11608664" data-annot-id="11095712" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="GR3" data-field-id="11609000" data-annot-id="11095904" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GAJ3" data-field-id="11609336" data-annot-id="11096096" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="GAG3" data-field-id="11609672" data-annot-id="11096288" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="GAI3" data-field-id="11610008" data-annot-id="11096480" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="GAF3" data-field-id="11610344" data-annot-id="11096672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="GAT3" data-field-id="11610680" data-annot-id="11096864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="GAD3" data-field-id="11611016" data-annot-id="11097056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="GAE3" data-field-id="11605832" data-annot-id="11094096" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="GAH3" data-field-id="11611944" data-annot-id="11097776" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="GAK4" data-field-id="11612280" data-annot-id="11097968" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GAA4" data-field-id="11612616" data-annot-id="11098160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="GR4" data-field-id="11612952" data-annot-id="11098352" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="GAJ4" data-field-id="11613288" data-annot-id="11098544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="GAG4" data-field-id="11613624" data-annot-id="11098736" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="GAI4" data-field-id="11613960" data-annot-id="11098928" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="GAF4" data-field-id="11614296" data-annot-id="11099120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GAT4" data-field-id="11614632" data-annot-id="11099312" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="GAD4" data-field-id="11614968" data-annot-id="11099504" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="GAE4" data-field-id="11615304" data-annot-id="11099696" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GAH4" data-field-id="11615640" data-annot-id="11099888" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GAK5" data-field-id="11615976" data-annot-id="11100080" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="GAA5" data-field-id="11616312" data-annot-id="11100272" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="GR5" data-field-id="11616648" data-annot-id="11100464" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="GAJ5" data-field-id="11616984" data-annot-id="11100656" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="GAG5" data-field-id="11617320" data-annot-id="11100848" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="GAI5" data-field-id="11617656" data-annot-id="11101040" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="GAF5" data-field-id="11617992" data-annot-id="11101232" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="GAT5" data-field-id="11618328" data-annot-id="11101424" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="GAD5" data-field-id="11618664" data-annot-id="11101616" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="GAE5" data-field-id="11619000" data-annot-id="11101808" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="GAH5" data-field-id="11619336" data-annot-id="11102000" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="GAK6" data-field-id="11619672" data-annot-id="11102192" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="GAA6" data-field-id="11620008" data-annot-id="11102384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="GR6" data-field-id="11620344" data-annot-id="11102576" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="GAJ6" data-field-id="11620680" data-annot-id="11102768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="GAG6" data-field-id="11621016" data-annot-id="11102960" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="GAI6" data-field-id="11621352" data-annot-id="11103152" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="GAF6" data-field-id="11621688" data-annot-id="11103344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="GAT6" data-field-id="11622024" data-annot-id="11103536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="GAD6" data-field-id="11599560" data-annot-id="11097248" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="GAE6" data-field-id="11600152" data-annot-id="11097440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="GAH6" data-field-id="11611416" data-annot-id="11104768" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="GAK7" data-field-id="11623272" data-annot-id="11104960" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="GAA7" data-field-id="11623608" data-annot-id="11105152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="GR7" data-field-id="11623944" data-annot-id="11105344" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="GAJ7" data-field-id="11624280" data-annot-id="11105536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="GAG7" data-field-id="11624616" data-annot-id="11105728" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="GAI7" data-field-id="11624952" data-annot-id="11105920" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="GAF7" data-field-id="11625288" data-annot-id="11106112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="GAT7" data-field-id="11625624" data-annot-id="11106304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="GAD7" data-field-id="11625960" data-annot-id="11106496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="GAE7" data-field-id="11626296" data-annot-id="11106688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="GAH7" data-field-id="11626632" data-annot-id="11106880" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="GAK8" data-field-id="11626968" data-annot-id="11107072" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="GAA8" data-field-id="11627304" data-annot-id="11107264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="GR8" data-field-id="11627640" data-annot-id="11107456" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="GAJ8" data-field-id="11627976" data-annot-id="11107648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="GAG8" data-field-id="11628312" data-annot-id="11107840" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="GAI8" data-field-id="11628664" data-annot-id="11108032" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="GAF8" data-field-id="11629000" data-annot-id="11108224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="GAT8" data-field-id="11629336" data-annot-id="11108416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="GAD8" data-field-id="11629672" data-annot-id="11108608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="GAE8" data-field-id="11630008" data-annot-id="11108800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="GAH8" data-field-id="11630344" data-annot-id="11108992" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="22322232" data-annot-id="21795968" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field pde-form-field-text" name="VILLE_STEI" data-field-id="22321896" data-annot-id="21796160" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field pde-form-field-text" name="CP_STEH" data-field-id="22321560" data-annot-id="21796352" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field pde-form-field-text" name="SIREN_STET" data-field-id="22322568" data-annot-id="21796544" type="text" disabled />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="FIN_EX" data-field-id="22319880" data-annot-id="21796736" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field pde-form-field-text" name="GT" data-field-id="22320216" data-annot-id="21796928" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STED" data-field-id="22320552" data-annot-id="21797120" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEF" data-field-id="22320888" data-annot-id="21797312" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEG" data-field-id="22321224" data-annot-id="21797504" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2033G;
