import React from 'react';
import { Field } from 'redux-form';

import { useCompute, parse as p, sum, roundField, setIsNothingness } from 'helpers/pdfforms';

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const notNeededFields = {
  'NOM_STEA': true,
  'UL': true
};


const Tax2055 = (props) => {
  const { formValues, change } = props;
  setIsNothingness(formValues, notNeededFields, 'UL', change);

  // Compute
  useCompute('QU', sum, ['PI', 'PM', 'PR', 'PV', 'PZ', 'QD', 'QH', 'QL', 'QP'], formValues, change);
  useCompute('QV', sum, ['PJ', 'PN', 'PS', 'PW', 'QA', 'QE', 'QI', 'QM', 'QR'], formValues, change);
  useCompute('QW', sum, ['PK', 'PO', 'PT', 'PX', 'QB', 'QF', 'QJ', 'QN', 'QS'], formValues, change);
  useCompute('QX', sum, ['PL', 'PQ', 'PU', 'PY', 'QC', 'QG', 'QK', 'QO', 'QT'], formValues, change);

  useCompute('TA', sum, ['UR', 'PE', 'QU'], formValues, change);
  useCompute('TB', sum, ['US', 'PF', 'QV'], formValues, change);
  useCompute('TC', sum, ['UT', 'PG', 'QW'], formValues, change);
  useCompute('TD', sum, ['UU', 'PH', 'QX'], formValues, change);

  useCompute('BM', sum, ['BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BJ', 'BK', 'BL'], formValues, change);
  useCompute('CM', sum, ['CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CJ', 'CK', 'CL'], formValues, change);
  useCompute('DM', sum, ['DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DJ', 'DK', 'DL'], formValues, change);
  useCompute('EM', sum, ['EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EJ', 'EK', 'EL'], formValues, change);
  useCompute('FM', sum, ['FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FJ', 'FK', 'FL'], formValues, change);
  useCompute('GM', sum, ['GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GJ', 'GK', 'GL'], formValues, change);
  useCompute('HM', sum, ['HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HJ', 'HK', 'HL'], formValues, change);

  useCompute('BN', sum, ['BA', 'BB', 'BM', 'BS'], formValues, change);
  useCompute('CN', sum, ['CA', 'CB', 'CM'], formValues, change);
  useCompute('DN', sum, ['DA', 'DB', 'DM'], formValues, change);
  useCompute('EN', sum, ['EA', 'EB', 'EM', 'ES'], formValues, change);
  useCompute('FN', sum, ['FA', 'FB', 'FM'], formValues, change);
  useCompute('GN', sum, ['GA', 'GB', 'GM'], formValues, change);
  useCompute('HN', sum, ['HA', 'HB', 'HM', 'HS'], formValues, change);

  useCompute('BP', sum, ['BN', 'CN', 'DN'], formValues, change);
  useCompute('BQ', sum, ['EN', 'FN', 'GN'], formValues, change);
  useCompute('BR', ({ BP, BQ }) => p(BP) - p(BQ), ['BP', 'BQ'], formValues, change);

  // Rendering
  return (
    <div className="tax2055">
      <div data-title="" id="pdf-document" />
      <form id="acroform" />
      <div className="pdf-page " id="pdf-page-1">
        <div className="pdf-page-inner pdf-page-1">
          <div className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 483, top: 45, width: 231, height: 19, fontSize: 27 }}>
              <span>AMORTISSEMENTS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 67, width: 166, height: 12, fontSize: 13 }}>
              <span>Formulaire obligatoire (article 53 A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 79, width: 134, height: 12, fontSize: 13 }}>
              <span>du Code général des impôts)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 964, top: 54, width: 204, height: 15, fontSize: 21 }}>
              <span>DGFiP N° 2055-SD 2019</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 1352, width: 103, height: 15, fontSize: 17 }}>
              <span>Frais d’acquisition de</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1366, width: 108, height: 15, fontSize: 17 }}>
              <span>titres de participations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1384, width: 75, height: 12, fontSize: 17 }}>
              <span>TOTAL IV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 413, top: 1442, width: 70, height: 16, fontSize: 17 }}>
              <span>Total général</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 487, top: 1446, width: 37, height: 8, fontSize: 11 }}>
              <span>non ventilé</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 418, top: 1458, width: 102, height: 16, fontSize: 17 }}>
              <span>(NS + NT + NU)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 1370, width: 18, height: 13, fontSize: 19 }}>
              <span>NL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 589, top: 1370, width: 21, height: 13, fontSize: 19 }}>
              <span>NM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1017, top: 1370, width: 20, height: 13, fontSize: 19 }}>
              <span>NO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 131, width: 168, height: 16, fontSize: 17 }}>
              <span>Désignation de l’entreprise</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 378, top: 1664, width: 446, height: 14, fontSize: 15 }}>
              <span>* Des explications concernant cette rubrique sont données dans la notice n° 2032</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 910, width: 87, height: 12, fontSize: 17 }}>
              <span>Frais établissements</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 927, width: 59, height: 11, fontSize: 16 }}>
              <span>TOTAL I</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 946, width: 111, height: 16, fontSize: 17 }}>
              <span>Autres immob. incor-po-</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 962, width: 119, height: 12, fontSize: 17 }}>
              <span>relles TOTAL II</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 993, width: 52, height: 12, fontSize: 17 }}>
              <span>Terrains</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1279, width: 56, height: 16, fontSize: 17 }}>
              <span>Emballages</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1295, width: 76, height: 16, fontSize: 17 }}>
              <span>récup. et divers</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 1241, width: 74, height: 12, fontSize: 17 }}>
              <span>Mat. bureau et</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 1257, width: 83, height: 12, fontSize: 17 }}>
              <span>inform. mobilier</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 64, top: 1202, width: 56, height: 12, fontSize: 17 }}>
              <span>Matériel de</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1221, width: 46, height: 13, fontSize: 17 }}>
              <span>transport</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1164, width: 81, height: 16, fontSize: 17 }}>
              <span>Inst. gales, agenc</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1180, width: 49, height: 12, fontSize: 17 }}>
              <span>am. divers</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1127, width: 79, height: 15, fontSize: 17 }}>
              <span>Inst. techniques</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1143, width: 80, height: 15, fontSize: 17 }}>
              <span>mat. et outillage</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1091, width: 79, height: 15, fontSize: 17 }}>
              <span>Inst. gales, agenc</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1107, width: 78, height: 12, fontSize: 17 }}>
              <span>et am. des const.</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1063, width: 76, height: 12, fontSize: 17 }}>
              <span>Sur sol d’autrui</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 65, top: 1027, width: 71, height: 16, fontSize: 17 }}>
              <span>Sur sol propre</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 1325, width: 74, height: 12, fontSize: 17 }}>
              <span>TOTAL III</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 1407, width: 74, height: 15, fontSize: 17 }}>
              <span>Total général</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 1422, width: 106, height: 16, fontSize: 17 }}>
              <span>(I + II + III + IV)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1443, width: 70, height: 15, fontSize: 17 }}>
              <span>Total général</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 113, top: 1447, width: 38, height: 7, fontSize: 11 }}>
              <span>non ventilé</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 1459, width: 102, height: 15, fontSize: 17 }}>
              <span>(NP + NQ + NR)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 794, top: 1443, width: 70, height: 15, fontSize: 17 }}>
              <span>Total général</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 867, top: 1447, width: 38, height: 7, fontSize: 11 }}>
              <span>non ventilé</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 817, top: 1459, width: 66, height: 15, fontSize: 17 }}>
              <span>(NW – NY)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 782, width: 74, height: 14, fontSize: 18 }}>
              <span>CADRE B</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 852, width: 88, height: 11, fontSize: 15 }}>
              <span>Immobilisations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 58, top: 868, width: 73, height: 11, fontSize: 15 }}>
              <span>amortissables</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 203, top: 858, width: 55, height: 11, fontSize: 15 }}>
              <span>Colonne 1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 174, top: 874, width: 114, height: 11, fontSize: 15 }}>
              <span>Différentiel de durée</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 208, top: 893, width: 46, height: 8, fontSize: 15 }}>
              <span>et autres</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 332, top: 830, width: 82, height: 10, fontSize: 15 }}>
              <span>DOTATIONS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 770, top: 830, width: 63, height: 10, fontSize: 15 }}>
              <span>REPRISES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1039, top: 840, width: 109, height: 11, fontSize: 15 }}>
              <span>Mouvement net des</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1051, top: 857, width: 85, height: 10, fontSize: 15 }}>
              <span>amortissements</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1040, top: 872, width: 108, height: 11, fontSize: 15 }}>
              <span>à la fin de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 346, top: 866, width: 57, height: 11, fontSize: 15 }}>
              <span>Colonne 2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 332, top: 882, width: 85, height: 14, fontSize: 15 }}>
              <span>Mode dégressif</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 488, top: 858, width: 57, height: 11, fontSize: 15 }}>
              <span>Colonne 3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 459, top: 875, width: 115, height: 10, fontSize: 15 }}>
              <span>Amortissement fiscal</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 482, top: 891, width: 69, height: 13, fontSize: 15 }}>
              <span>exceptionnel</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 631, top: 858, width: 58, height: 11, fontSize: 15 }}>
              <span>Colonne 4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 602, top: 874, width: 115, height: 11, fontSize: 15 }}>
              <span>Différentiel de durée</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 637, top: 893, width: 46, height: 8, fontSize: 15 }}>
              <span>et autres</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 866, width: 57, height: 11, fontSize: 15 }}>
              <span>Colonne 5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 760, top: 882, width: 85, height: 14, fontSize: 15 }}>
              <span>Mode dégressif</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 915, top: 858, width: 57, height: 11, fontSize: 15 }}>
              <span>Colonne 6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 886, top: 875, width: 116, height: 10, fontSize: 15 }}>
              <span>Amortissement fiscal</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 910, top: 891, width: 69, height: 13, fontSize: 15 }}>
              <span>exceptionnel</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 201, top: 779, width: 914, height: 18, fontSize: 18 }}>
              <span>VENTILATION DES MOUVEMENTS AFFECTANT LA PROVISION POUR AMORTISSEMENTS DÉROGATOIRES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 1487, width: 74, height: 14, fontSize: 18 }}>
              <span>CADRE C</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 83, top: 1524, width: 255, height: 14, fontSize: 18 }}>
              <span>MOUVEMENTS DE L’EXERCICE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 50, top: 1541, width: 321, height: 17, fontSize: 18 }}>
              <span>AFFECTANT LES CHARGES RÉPARTIES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 1564, width: 244, height: 14, fontSize: 18 }}>
              <span>SUR PLUSIEURS EXERCICES*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 1598, width: 219, height: 15, fontSize: 17 }}>
              <span>Frais d’émission d’emprunt à étaler</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 59, top: 1634, width: 262, height: 16, fontSize: 17 }}>
              <span>Primes de remboursement des obligations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 432, top: 1536, width: 120, height: 11, fontSize: 15 }}>
              <span>Montant net au début</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 459, top: 1552, width: 66, height: 11, fontSize: 15 }}>
              <span>de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 638, top: 1544, width: 83, height: 14, fontSize: 15 }}>
              <span>Augmentations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 805, top: 1536, width: 125, height: 11, fontSize: 15 }}>
              <span>Dotations de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 814, top: 1552, width: 107, height: 11, fontSize: 15 }}>
              <span>aux amortissements</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1025, top: 1536, width: 91, height: 11, fontSize: 15 }}>
              <span>Montant net à la</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1028, top: 1552, width: 85, height: 11, fontSize: 15 }}>
              <span>fin de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1034, width: 10, height: 269, fontSize: 13 }}>
              <span>ConstructionsAutres</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1168, width: 10, height: 107, fontSize: 10 }}>
              <span>immobilisations corporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 987, width: 15, height: 17, fontSize: 19 }}>
              <span>Q3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 987, width: 16, height: 17, fontSize: 19 }}>
              <span>Q4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 592, top: 987, width: 15, height: 17, fontSize: 19 }}>
              <span>Q5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 734, top: 987, width: 16, height: 17, fontSize: 19 }}>
              <span>Q6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 987, width: 15, height: 17, fontSize: 19 }}>
              <span>Q7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1019, top: 987, width: 16, height: 17, fontSize: 19 }}>
              <span>Q8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 1024, width: 13, height: 13, fontSize: 19 }}>
              <span>R1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 1024, width: 15, height: 13, fontSize: 19 }}>
              <span>R2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 592, top: 1024, width: 14, height: 13, fontSize: 19 }}>
              <span>R3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 735, top: 1024, width: 15, height: 13, fontSize: 19 }}>
              <span>R4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 1024, width: 14, height: 13, fontSize: 19 }}>
              <span>R5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1020, top: 1024, width: 14, height: 13, fontSize: 19 }}>
              <span>R6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 165, top: 1135, width: 14, height: 13, fontSize: 19 }}>
              <span>T3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 1135, width: 15, height: 13, fontSize: 19 }}>
              <span>T4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 1135, width: 15, height: 14, fontSize: 19 }}>
              <span>T5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 592, top: 1135, width: 14, height: 13, fontSize: 19 }}>
              <span>T6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 735, top: 1135, width: 14, height: 13, fontSize: 19 }}>
              <span>T7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 1135, width: 14, height: 13, fontSize: 19 }}>
              <span>T8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1020, top: 1135, width: 14, height: 14, fontSize: 19 }}>
              <span>T9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 1172, width: 14, height: 13, fontSize: 19 }}>
              <span>U1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 1172, width: 15, height: 13, fontSize: 19 }}>
              <span>U2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 1172, width: 15, height: 13, fontSize: 19 }}>
              <span>U3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 592, top: 1172, width: 15, height: 13, fontSize: 19 }}>
              <span>U4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 734, top: 1172, width: 15, height: 13, fontSize: 19 }}>
              <span>U5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 1172, width: 15, height: 13, fontSize: 19 }}>
              <span>U6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1020, top: 1172, width: 15, height: 13, fontSize: 19 }}>
              <span>U7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 1248, width: 15, height: 14, fontSize: 19 }}>
              <span>V6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 1249, width: 15, height: 13, fontSize: 19 }}>
              <span>V7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 1249, width: 15, height: 13, fontSize: 19 }}>
              <span>V8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 592, top: 1249, width: 15, height: 13, fontSize: 19 }}>
              <span>V9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 733, top: 1249, width: 17, height: 13, fontSize: 19 }}>
              <span>W1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 874, top: 1249, width: 18, height: 13, fontSize: 19 }}>
              <span>W2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1018, top: 1249, width: 18, height: 13, fontSize: 19 }}>
              <span>W3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 1289, width: 18, height: 14, fontSize: 19 }}>
              <span>W4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 305, top: 1289, width: 18, height: 14, fontSize: 19 }}>
              <span>W5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 447, top: 1289, width: 19, height: 14, fontSize: 19 }}>
              <span>W6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 590, top: 1290, width: 18, height: 13, fontSize: 19 }}>
              <span>W7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 733, top: 1290, width: 18, height: 13, fontSize: 19 }}>
              <span>W8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 874, top: 1289, width: 18, height: 14, fontSize: 19 }}>
              <span>W9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1019, top: 1289, width: 15, height: 13, fontSize: 19 }}>
              <span>X1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 296, width: 117, height: 12, fontSize: 17 }}>
              <span>Frais d’établissement</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 312, width: 118, height: 16, fontSize: 17 }}>
              <span>et de développement</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 312, width: 63, height: 12, fontSize: 17 }}>
              <span>TOTAL I</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 332, width: 130, height: 12, fontSize: 17 }}>
              <span>Autres immobilisations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 348, width: 73, height: 16, fontSize: 17 }}>
              <span>incorporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 279, top: 348, width: 68, height: 12, fontSize: 17 }}>
              <span>TOTAL II</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 379, width: 47, height: 12, fontSize: 17 }}>
              <span>Terrains</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 657, width: 130, height: 16, fontSize: 17 }}>
              <span>Emballages récupérables</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 673, width: 44, height: 12, fontSize: 17 }}>
              <span>et divers</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 621, width: 114, height: 12, fontSize: 17 }}>
              <span>Matériel de bureau et</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 637, width: 120, height: 16, fontSize: 17 }}>
              <span>informatique, mobilier</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 594, width: 113, height: 16, fontSize: 17 }}>
              <span>Matériel de transport</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 549, width: 153, height: 16, fontSize: 17 }}>
              <span>Inst. générales, agencements,</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 565, width: 113, height: 16, fontSize: 17 }}>
              <span>aménagements divers</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 513, width: 186, height: 16, fontSize: 17 }}>
              <span>Installations techniques, matériel et</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 529, width: 102, height: 16, fontSize: 17 }}>
              <span>outillage industriels</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 477, width: 145, height: 16, fontSize: 17 }}>
              <span>Inst. générales, agencements et</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 493, width: 154, height: 16, fontSize: 17 }}>
              <span>aménagements des constructions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 449, width: 85, height: 12, fontSize: 17 }}>
              <span>Sur sol d’autrui</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 413, width: 80, height: 16, fontSize: 17 }}>
              <span>Sur sol propre</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 273, top: 702, width: 75, height: 12, fontSize: 17 }}>
              <span>TOTAL III</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 736, width: 219, height: 19, fontSize: 17 }}>
              <span>TOTAL GÉNÉRAL (I + II + III)</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 50, top: 196, width: 76, height: 14, fontSize: 18 }}>
              <span>CADRE A</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 256, width: 238, height: 11, fontSize: 15 }}>
              <span>IMMOBILISATIONS AMORTISSABLES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 248, width: 150, height: 11, fontSize: 15 }}>
              <span>Montant des amortissements</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 407, top: 264, width: 113, height: 11, fontSize: 15 }}>
              <span>au début de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 596, top: 248, width: 138, height: 14, fontSize: 15 }}>
              <span>Augmentations : dotations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 634, top: 264, width: 63, height: 11, fontSize: 15 }}>
              <span>de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 789, top: 240, width: 157, height: 11, fontSize: 15 }}>
              <span>Diminutions : amortissements</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 794, top: 256, width: 147, height: 11, fontSize: 15 }}>
              <span>afférents aux éléments sortis</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 817, top: 272, width: 101, height: 14, fontSize: 15 }}>
              <span>de l’actif et reprises</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 995, top: 248, width: 150, height: 11, fontSize: 15 }}>
              <span>Montant des amortissements</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1019, top: 264, width: 103, height: 11, fontSize: 15 }}>
              <span>à la fin de l’exercice</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 286, top: 190, width: 745, height: 14, fontSize: 18 }}>
              <span>SITUATIONS ET MOUVEMENTS DE L’EXERCICE DES AMORTISSEMENTS TECHNIQUES</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 470, top: 207, width: 378, height: 15, fontSize: 18 }}>
              <span>(OU VENANT EN DIMINUTION DE L’ACTIF) *</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 368, top: 309, width: 19, height: 14, fontSize: 21 }}>
              <span>CY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 368, top: 345, width: 17, height: 14, fontSize: 21 }}>
              <span>PE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 370, top: 375, width: 13, height: 14, fontSize: 21 }}>
              <span>PI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 366, top: 411, width: 21, height: 14, fontSize: 21 }}>
              <span>PM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 368, top: 447, width: 18, height: 14, fontSize: 21 }}>
              <span>PR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 367, top: 485, width: 19, height: 14, fontSize: 21 }}>
              <span>PV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 367, top: 520, width: 18, height: 15, fontSize: 21 }}>
              <span>PZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 366, top: 556, width: 21, height: 19, fontSize: 21 }}>
              <span>QD</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 366, top: 591, width: 21, height: 19, fontSize: 21 }}>
              <span>QH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 367, top: 629, width: 19, height: 18, fontSize: 21 }}>
              <span>QL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 367, top: 665, width: 19, height: 18, fontSize: 21 }}>
              <span>QP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 375, width: 13, height: 19, fontSize: 21 }}>
              <span>PJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 411, width: 19, height: 15, fontSize: 21 }}>
              <span>PN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 571, top: 447, width: 15, height: 14, fontSize: 21 }}>
              <span>PS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 485, width: 22, height: 14, fontSize: 21 }}>
              <span>PW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 520, width: 21, height: 19, fontSize: 21 }}>
              <span>QA</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 556, width: 21, height: 19, fontSize: 21 }}>
              <span>QE</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 571, top: 591, width: 15, height: 19, fontSize: 21 }}>
              <span>QI</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 629, width: 23, height: 18, fontSize: 21 }}>
              <span>QM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 665, width: 20, height: 18, fontSize: 21 }}>
              <span>QR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 375, width: 20, height: 14, fontSize: 21 }}>
              <span>PK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 411, width: 20, height: 14, fontSize: 21 }}>
              <span>PO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 447, width: 17, height: 14, fontSize: 21 }}>
              <span>PT</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 485, width: 19, height: 14, fontSize: 21 }}>
              <span>PX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 520, width: 19, height: 19, fontSize: 21 }}>
              <span>QB</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 556, width: 19, height: 19, fontSize: 21 }}>
              <span>QF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 591, width: 16, height: 20, fontSize: 21 }}>
              <span>QJ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 770, top: 629, width: 22, height: 18, fontSize: 21 }}>
              <span>QN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 665, width: 17, height: 18, fontSize: 21 }}>
              <span>QS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 375, width: 17, height: 14, fontSize: 21 }}>
              <span>PL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 411, width: 20, height: 18, fontSize: 21 }}>
              <span>PQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 447, width: 19, height: 14, fontSize: 21 }}>
              <span>PU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 485, width: 18, height: 14, fontSize: 21 }}>
              <span>PY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 520, width: 20, height: 19, fontSize: 21 }}>
              <span>QC</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 556, width: 22, height: 19, fontSize: 21 }}>
              <span>QG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 591, width: 22, height: 19, fontSize: 21 }}>
              <span>QK</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 629, width: 22, height: 18, fontSize: 21 }}>
              <span>QO</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 665, width: 20, height: 18, fontSize: 21 }}>
              <span>QT</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 49, top: 450, width: 78, height: 11, fontSize: 17 }}>
              <span>Constructions</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 575, width: 37, height: 12, fontSize: 17 }}>
              <span>Autres</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 611, width: 89, height: 12, fontSize: 17 }}>
              <span>immobilisations</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 647, width: 61, height: 16, fontSize: 17 }}>
              <span>corporelles</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 309, width: 19, height: 14, fontSize: 21 }}>
              <span>EL</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 571, top: 345, width: 17, height: 14, fontSize: 21 }}>
              <span>PF</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 309, width: 22, height: 14, fontSize: 21 }}>
              <span>EM</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 772, top: 345, width: 20, height: 14, fontSize: 21 }}>
              <span>PG</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 309, width: 21, height: 14, fontSize: 21 }}>
              <span>EN</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 345, width: 19, height: 14, fontSize: 21 }}>
              <span>PH</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1019, top: 919, width: 16, height: 13, fontSize: 19 }}>
              <span>N6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 448, top: 919, width: 17, height: 13, fontSize: 19 }}>
              <span>N2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 875, top: 919, width: 16, height: 13, fontSize: 19 }}>
              <span>N5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 591, top: 919, width: 16, height: 13, fontSize: 19 }}>
              <span>N3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 734, top: 919, width: 16, height: 13, fontSize: 19 }}>
              <span>N4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 306, top: 919, width: 15, height: 13, fontSize: 19 }}>
              <span>N1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 306, top: 955, width: 16, height: 13, fontSize: 19 }}>
              <span>N8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1019, top: 955, width: 15, height: 17, fontSize: 19 }}>
              <span>Q1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 877, top: 955, width: 13, height: 13, fontSize: 19 }}>
              <span>P9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 735, top: 955, width: 14, height: 13, fontSize: 19 }}>
              <span>P8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 593, top: 956, width: 13, height: 12, fontSize: 19 }}>
              <span>P7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 450, top: 955, width: 13, height: 13, fontSize: 19 }}>
              <span>P6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 165, top: 1058, width: 14, height: 13, fontSize: 19 }}>
              <span>R7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 1058, width: 14, height: 13, fontSize: 19 }}>
              <span>R8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 1058, width: 15, height: 13, fontSize: 19 }}>
              <span>R9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 593, top: 1058, width: 11, height: 13, fontSize: 19 }}>
              <span>S1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 736, top: 1057, width: 12, height: 13, fontSize: 19 }}>
              <span>S2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 878, top: 1058, width: 11, height: 13, fontSize: 19 }}>
              <span>S3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1021, top: 1058, width: 13, height: 13, fontSize: 19 }}>
              <span>S4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 1100, width: 12, height: 13, fontSize: 19 }}>
              <span>S5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 309, top: 1100, width: 11, height: 13, fontSize: 19 }}>
              <span>S6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 451, top: 1100, width: 12, height: 13, fontSize: 19 }}>
              <span>S7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 594, top: 1100, width: 11, height: 13, fontSize: 19 }}>
              <span>S8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 736, top: 1100, width: 12, height: 13, fontSize: 19 }}>
              <span>S9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 876, top: 1100, width: 13, height: 13, fontSize: 19 }}>
              <span>T1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1020, top: 1100, width: 14, height: 13, fontSize: 19 }}>
              <span>T2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 1326, width: 16, height: 13, fontSize: 19 }}>
              <span>X2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 307, top: 1325, width: 15, height: 13, fontSize: 19 }}>
              <span>X3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 1326, width: 16, height: 13, fontSize: 19 }}>
              <span>X4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 591, top: 1326, width: 16, height: 13, fontSize: 19 }}>
              <span>X5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 734, top: 1326, width: 16, height: 13, fontSize: 19 }}>
              <span>X6</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 875, top: 1326, width: 16, height: 13, fontSize: 19 }}>
              <span>X7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1019, top: 1326, width: 16, height: 13, fontSize: 19 }}>
              <span>X8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1017, top: 1416, width: 20, height: 13, fontSize: 19 }}>
              <span>NV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 873, top: 1416, width: 20, height: 13, fontSize: 19 }}>
              <span>NU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 733, top: 1415, width: 19, height: 14, fontSize: 19 }}>
              <span>NT</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 591, top: 1415, width: 17, height: 14, fontSize: 19 }}>
              <span>NS</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 304, top: 1415, width: 21, height: 17, fontSize: 19 }}>
              <span>NQ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 447, top: 1416, width: 20, height: 13, fontSize: 19 }}>
              <span>NR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 1416, width: 18, height: 13, fontSize: 19 }}>
              <span>NP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 1597, width: 16, height: 14, fontSize: 21 }}>
              <span>Z9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 774, top: 1632, width: 15, height: 15, fontSize: 21 }}>
              <span>SP</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 975, top: 1597, width: 16, height: 14, fontSize: 21 }}>
              <span>Z8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1632, width: 16, height: 15, fontSize: 21 }}>
              <span>SR</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 1214, width: 15, height: 12, fontSize: 19 }}>
              <span>U8</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 306, top: 1213, width: 16, height: 13, fontSize: 19 }}>
              <span>U9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 1214, width: 14, height: 12, fontSize: 19 }}>
              <span>V1</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 591, top: 1214, width: 16, height: 12, fontSize: 19 }}>
              <span>V2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 734, top: 1213, width: 15, height: 13, fontSize: 19 }}>
              <span>V3</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 875, top: 1213, width: 16, height: 13, fontSize: 19 }}>
              <span>V4</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1019, top: 1213, width: 15, height: 13, fontSize: 19 }}>
              <span>V5</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1084, top: 131, width: 40, height: 12, fontSize: 17 }}>
              <span>Néant</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1153, top: 123, width: 7, height: 7, fontSize: 17 }}>
              <span>*</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 366, top: 697, width: 21, height: 19, fontSize: 21 }}>
              <span>QU</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 366, top: 731, width: 20, height: 18, fontSize: 21 }}>
              <span>0/N</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 697, width: 21, height: 19, fontSize: 21 }}>
              <span>QV</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 570, top: 731, width: 17, height: 18, fontSize: 21 }}>
              <span>0/P</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 769, top: 697, width: 25, height: 19, fontSize: 21 }}>
              <span>QW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 731, width: 21, height: 21, fontSize: 21 }}>
              <span>0/Q</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 697, width: 21, height: 19, fontSize: 21 }}>
              <span>QX</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 974, top: 731, width: 19, height: 18, fontSize: 21 }}>
              <span>0/R</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 161, top: 1451, width: 22, height: 13, fontSize: 19 }}>
              <span>NW</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 541, top: 1451, width: 21, height: 14, fontSize: 21 }}>
              <span>NY</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1450, width: 21, height: 15, fontSize: 21 }}>
              <span>NZ</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 920, width: 18, height: 13, fontSize: 19 }}>
              <span>M9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 957, width: 16, height: 12, fontSize: 19 }}>
              <span>N7</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 987, width: 16, height: 17, fontSize: 19 }}>
              <span>Q2</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 1024, width: 16, height: 17, fontSize: 19 }}>
              <span>Q9</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 16, top: 1379, width: 11, height: 264, fontSize: 13 }}>
              <span>N° 2055- SD – (SDNC-DGFiP) - Novembre 2018</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 320, top: 39, width: 36, height: 37, fontSize: 49 }}>
              <span>⑥</span>
            </div>
          </div>
          <div className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot pdf-obj-0-1 pdf-obj-fixed acroform-field" id="pdf-obj-0-1" name="UL" type="checkbox" />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-2 pdf-obj-fixed acroform-field" id="pdf-obj-0-2" name="UR" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-3 pdf-obj-fixed acroform-field" id="pdf-obj-0-3" name="US" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-4 pdf-obj-fixed acroform-field" id="pdf-obj-0-4" name="UT" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-5 pdf-obj-fixed acroform-field" id="pdf-obj-0-5" name="UU" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-6 pdf-obj-fixed acroform-field" id="pdf-obj-0-6" name="PE" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-7 pdf-obj-fixed acroform-field" id="pdf-obj-0-7" name="PF" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-8 pdf-obj-fixed acroform-field" id="pdf-obj-0-8" name="PG" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-9 pdf-obj-fixed acroform-field" id="pdf-obj-0-9" name="PH" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-10 pdf-obj-fixed acroform-field" id="pdf-obj-0-10" name="PI" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-11 pdf-obj-fixed acroform-field" id="pdf-obj-0-11" name="PJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-12 pdf-obj-fixed acroform-field" id="pdf-obj-0-12" name="PK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-13 pdf-obj-fixed acroform-field" id="pdf-obj-0-13" name="PL" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-14 pdf-obj-fixed acroform-field" id="pdf-obj-0-14" name="PM" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-15 pdf-obj-fixed acroform-field" id="pdf-obj-0-15" name="PN" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-16 pdf-obj-fixed acroform-field" id="pdf-obj-0-16" name="PO" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-17 pdf-obj-fixed acroform-field" id="pdf-obj-0-17" name="PQ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-18 pdf-obj-fixed acroform-field" id="pdf-obj-0-18" name="PR" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-19 pdf-obj-fixed acroform-field" id="pdf-obj-0-19" name="PS" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-20 pdf-obj-fixed acroform-field" id="pdf-obj-0-20" name="PT" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-21 pdf-obj-fixed acroform-field" id="pdf-obj-0-21" name="PU" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-22 pdf-obj-fixed acroform-field" id="pdf-obj-0-22" name="PV" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-23 pdf-obj-fixed acroform-field" id="pdf-obj-0-23" name="PW" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-24 pdf-obj-fixed acroform-field" id="pdf-obj-0-24" name="PX" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-25 pdf-obj-fixed acroform-field" id="pdf-obj-0-25" name="PY" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-26 pdf-obj-fixed acroform-field" id="pdf-obj-0-26" name="PZ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-27 pdf-obj-fixed acroform-field" id="pdf-obj-0-27" name="QA" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-28 pdf-obj-fixed acroform-field" id="pdf-obj-0-28" name="QB" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-29 pdf-obj-fixed acroform-field" id="pdf-obj-0-29" name="QC" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-30 pdf-obj-fixed acroform-field" id="pdf-obj-0-30" name="QD" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-31 pdf-obj-fixed acroform-field" id="pdf-obj-0-31" name="QE" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-32 pdf-obj-fixed acroform-field" id="pdf-obj-0-32" name="QF" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-33 pdf-obj-fixed acroform-field" id="pdf-obj-0-33" name="QG" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-34 pdf-obj-fixed acroform-field" id="pdf-obj-0-34" name="QH" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-35 pdf-obj-fixed acroform-field" id="pdf-obj-0-35" name="QI" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-36 pdf-obj-fixed acroform-field" id="pdf-obj-0-36" name="QJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-37 pdf-obj-fixed acroform-field" id="pdf-obj-0-37" name="QK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-38 pdf-obj-fixed acroform-field" id="pdf-obj-0-38" name="QL" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-39 pdf-obj-fixed acroform-field" id="pdf-obj-0-39" name="QM" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-40 pdf-obj-fixed acroform-field" id="pdf-obj-0-40" name="QN" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-41 pdf-obj-fixed acroform-field" id="pdf-obj-0-41" name="QO" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-42 pdf-obj-fixed acroform-field" id="pdf-obj-0-42" name="QP" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-43 pdf-obj-fixed acroform-field" id="pdf-obj-0-43" name="QR" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-44 pdf-obj-fixed acroform-field" id="pdf-obj-0-44" name="QS" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-45 pdf-obj-fixed acroform-field" id="pdf-obj-0-45" name="QT" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-46 pdf-obj-fixed acroform-field" id="pdf-obj-0-46" name="QU" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-47 pdf-obj-fixed acroform-field" id="pdf-obj-0-47" name="QV" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-48 pdf-obj-fixed acroform-field" id="pdf-obj-0-48" name="QW" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-49 pdf-obj-fixed acroform-field" id="pdf-obj-0-49" name="QX" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-50 pdf-obj-fixed acroform-field" id="pdf-obj-0-50" name="TA" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-51 pdf-obj-fixed acroform-field" id="pdf-obj-0-51" name="TB" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-52 pdf-obj-fixed acroform-field" id="pdf-obj-0-52" name="TC" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-53 pdf-obj-fixed acroform-field" id="pdf-obj-0-53" name="TD" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-54 pdf-obj-fixed acroform-field" id="pdf-obj-0-54" name="BA" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-55 pdf-obj-fixed acroform-field" id="pdf-obj-0-55" name="BB" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-56 pdf-obj-fixed acroform-field" id="pdf-obj-0-56" name="BC" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-57 pdf-obj-fixed acroform-field" id="pdf-obj-0-57" name="BD" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-58 pdf-obj-fixed acroform-field" id="pdf-obj-0-58" name="BE" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-59 pdf-obj-fixed acroform-field" id="pdf-obj-0-59" name="BF" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-60 pdf-obj-fixed acroform-field" id="pdf-obj-0-60" name="BG" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-61 pdf-obj-fixed acroform-field" id="pdf-obj-0-61" name="BH" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-62 pdf-obj-fixed acroform-field" id="pdf-obj-0-62" name="BJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-63 pdf-obj-fixed acroform-field" id="pdf-obj-0-63" name="BK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-64 pdf-obj-fixed acroform-field" id="pdf-obj-0-64" name="BL" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-65 pdf-obj-fixed acroform-field" id="pdf-obj-0-65" name="BM" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-66 pdf-obj-fixed acroform-field" id="pdf-obj-0-66" name="BS" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-67 pdf-obj-fixed acroform-field" id="pdf-obj-0-67" name="BN" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-68 pdf-obj-fixed acroform-field" id="pdf-obj-0-68" name="BP" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-69 pdf-obj-fixed acroform-field" id="pdf-obj-0-69" name="BQ" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-70 pdf-obj-fixed acroform-field" id="pdf-obj-0-70" name="BR" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-71 pdf-obj-fixed acroform-field" id="pdf-obj-0-71" name="CN" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-72 pdf-obj-fixed acroform-field" id="pdf-obj-0-72" name="CM" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-73 pdf-obj-fixed acroform-field" id="pdf-obj-0-73" name="CL" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-74 pdf-obj-fixed acroform-field" id="pdf-obj-0-74" name="CK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-75 pdf-obj-fixed acroform-field" id="pdf-obj-0-75" name="CJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-76 pdf-obj-fixed acroform-field" id="pdf-obj-0-76" name="CH" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-77 pdf-obj-fixed acroform-field" id="pdf-obj-0-77" name="CG" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-78 pdf-obj-fixed acroform-field" id="pdf-obj-0-78" name="CF" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-79 pdf-obj-fixed acroform-field" id="pdf-obj-0-79" name="CE" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-80 pdf-obj-fixed acroform-field" id="pdf-obj-0-80" name="CD" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-81 pdf-obj-fixed acroform-field" id="pdf-obj-0-81" name="CC" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-82 pdf-obj-fixed acroform-field" id="pdf-obj-0-82" name="CB" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-83 pdf-obj-fixed acroform-field" id="pdf-obj-0-83" name="CA" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-84 pdf-obj-fixed acroform-field" id="pdf-obj-0-84" name="DM" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-85 pdf-obj-fixed acroform-field" id="pdf-obj-0-85" name="DL" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-86 pdf-obj-fixed acroform-field" id="pdf-obj-0-86" name="DK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-87 pdf-obj-fixed acroform-field" id="pdf-obj-0-87" name="DJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-88 pdf-obj-fixed acroform-field" id="pdf-obj-0-88" name="DH" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-89 pdf-obj-fixed acroform-field" id="pdf-obj-0-89" name="DG" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-90 pdf-obj-fixed acroform-field" id="pdf-obj-0-90" name="DF" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-91 pdf-obj-fixed acroform-field" id="pdf-obj-0-91" name="DE" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-92 pdf-obj-fixed acroform-field" id="pdf-obj-0-92" name="DD" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-93 pdf-obj-fixed acroform-field" id="pdf-obj-0-93" name="DC" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-94 pdf-obj-fixed acroform-field" id="pdf-obj-0-94" name="DB" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-95 pdf-obj-fixed acroform-field" id="pdf-obj-0-95" name="DA" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-96 pdf-obj-fixed acroform-field" id="pdf-obj-0-96" name="DN" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-97 pdf-obj-fixed acroform-field" id="pdf-obj-0-97" name="EN" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-98 pdf-obj-fixed acroform-field" id="pdf-obj-0-98" name="FN" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-99 pdf-obj-fixed acroform-field" id="pdf-obj-0-99" name="GN" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-100 pdf-obj-fixed acroform-field" id="pdf-obj-0-100" name="HN" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-101 pdf-obj-fixed acroform-field" id="pdf-obj-0-101" name="ES" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-102 pdf-obj-fixed acroform-field" id="pdf-obj-0-102" name="EM" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-103 pdf-obj-fixed acroform-field" id="pdf-obj-0-103" name="EL" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-104 pdf-obj-fixed acroform-field" id="pdf-obj-0-104" name="EK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-105 pdf-obj-fixed acroform-field" id="pdf-obj-0-105" name="EJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-106 pdf-obj-fixed acroform-field" id="pdf-obj-0-106" name="EH" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-107 pdf-obj-fixed acroform-field" id="pdf-obj-0-107" name="EG" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-108 pdf-obj-fixed acroform-field" id="pdf-obj-0-108" name="EF" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-109 pdf-obj-fixed acroform-field" id="pdf-obj-0-109" name="EE" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-110 pdf-obj-fixed acroform-field" id="pdf-obj-0-110" name="ED" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-111 pdf-obj-fixed acroform-field" id="pdf-obj-0-111" name="EC" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-112 pdf-obj-fixed acroform-field" id="pdf-obj-0-112" name="EB" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-113 pdf-obj-fixed acroform-field" id="pdf-obj-0-113" name="EA" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-114 pdf-obj-fixed acroform-field" id="pdf-obj-0-114" name="FM" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-115 pdf-obj-fixed acroform-field" id="pdf-obj-0-115" name="FL" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-116 pdf-obj-fixed acroform-field" id="pdf-obj-0-116" name="FK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-117 pdf-obj-fixed acroform-field" id="pdf-obj-0-117" name="FJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-118 pdf-obj-fixed acroform-field" id="pdf-obj-0-118" name="FH" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-119 pdf-obj-fixed acroform-field" id="pdf-obj-0-119" name="FG" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-120 pdf-obj-fixed acroform-field" id="pdf-obj-0-120" name="FF" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-121 pdf-obj-fixed acroform-field" id="pdf-obj-0-121" name="FE" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-122 pdf-obj-fixed acroform-field" id="pdf-obj-0-122" name="FD" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-123 pdf-obj-fixed acroform-field" id="pdf-obj-0-123" name="FC" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-124 pdf-obj-fixed acroform-field" id="pdf-obj-0-124" name="FB" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-125 pdf-obj-fixed acroform-field" id="pdf-obj-0-125" name="FA" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-126 pdf-obj-fixed acroform-field" id="pdf-obj-0-126" name="GM" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-127 pdf-obj-fixed acroform-field" id="pdf-obj-0-127" name="GL" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-128 pdf-obj-fixed acroform-field" id="pdf-obj-0-128" name="GK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-129 pdf-obj-fixed acroform-field" id="pdf-obj-0-129" name="GJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-130 pdf-obj-fixed acroform-field" id="pdf-obj-0-130" name="GH" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-131 pdf-obj-fixed acroform-field" id="pdf-obj-0-131" name="GG" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-132 pdf-obj-fixed acroform-field" id="pdf-obj-0-132" name="GF" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-133 pdf-obj-fixed acroform-field" id="pdf-obj-0-133" name="GE" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-134 pdf-obj-fixed acroform-field" id="pdf-obj-0-134" name="GD" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-135 pdf-obj-fixed acroform-field" id="pdf-obj-0-135" name="GC" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-136 pdf-obj-fixed acroform-field" id="pdf-obj-0-136" name="GB" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-137 pdf-obj-fixed acroform-field" id="pdf-obj-0-137" name="GA" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-138 pdf-obj-fixed acroform-field" id="pdf-obj-0-138" name="HS" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-139 pdf-obj-fixed acroform-field" id="pdf-obj-0-139" name="HM" type="text" disabled />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-140 pdf-obj-fixed acroform-field" id="pdf-obj-0-140" name="HL" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-141 pdf-obj-fixed acroform-field" id="pdf-obj-0-141" name="HK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-142 pdf-obj-fixed acroform-field" id="pdf-obj-0-142" name="HJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-143 pdf-obj-fixed acroform-field" id="pdf-obj-0-143" name="HH" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-144 pdf-obj-fixed acroform-field" id="pdf-obj-0-144" name="HG" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-145 pdf-obj-fixed acroform-field" id="pdf-obj-0-145" name="HF" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-146 pdf-obj-fixed acroform-field" id="pdf-obj-0-146" name="HE" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-147 pdf-obj-fixed acroform-field" id="pdf-obj-0-147" name="HD" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-148 pdf-obj-fixed acroform-field" id="pdf-obj-0-148" name="HC" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-149 pdf-obj-fixed acroform-field" id="pdf-obj-0-149" name="HB" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-150 pdf-obj-fixed acroform-field" id="pdf-obj-0-150" name="HA" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-151 pdf-obj-fixed acroform-field" id="pdf-obj-0-151" name="UM" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-152 pdf-obj-fixed acroform-field" id="pdf-obj-0-152" name="UN" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-153 pdf-obj-fixed acroform-field" id="pdf-obj-0-153" name="UP" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-154 pdf-obj-fixed acroform-field" id="pdf-obj-0-154" name="UQ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-155 pdf-obj-fixed acroform-field" id="pdf-obj-0-155" name="UJ" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-156 pdf-obj-fixed acroform-field" id="pdf-obj-0-156" name="UK" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-157 pdf-obj-fixed acroform-field" id="pdf-obj-0-157" name="SP" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot pdf-obj-0-158 pdf-obj-fixed acroform-field" id="pdf-obj-0-158" name="SR" type="text" onChange={roundField(change)} />
            <Field component="input" className="pde-form-field pdf-annot obj_159 pdf-obj-fixed acroform-field " name="NOM_STEA" type="text" disabled />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2055;
