/* eslint-disable */
import React from "react";
import { Field } from "redux-form";
import {sum, useCompute, setIsNothingness, parse as p} from 'helpers/pdfforms';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';

import "./style.scss";
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const computeGW = ({ FR, GF, GH, GI, GP, GU }) => {
  return p(FR) - p(GF) + p(GH) - p(GI) + p(GP) - p(GU);
};

const notNeededFields = {
  'NOM_STEA': true,
  'JT': true
};

const Tax2052 = (props) => {
  const { change, taxDeclarationForm } = props;

  setIsNothingness(taxDeclarationForm, notNeededFields, 'JT', change);

  useCompute('FC', sum, ['FA', 'FB'], taxDeclarationForm, change);
  useCompute('FJ', sum, ['FA', 'FD', 'FG'], taxDeclarationForm, change);
  useCompute('FK', sum, ['FB', 'FE', 'FH'], taxDeclarationForm, change);
  useCompute('FF', sum, ['FD', 'FE'], taxDeclarationForm, change);
  useCompute('FF', sum, ['FD', 'FE'], taxDeclarationForm, change);
  useCompute('FI', sum, ['FG', 'FH'], taxDeclarationForm, change);
  useCompute('FL', sum, ['FC', 'FF', 'FI'], taxDeclarationForm, change);
  useCompute('FR', sum, ['FL', 'FM', 'FN', 'FO', 'FP', 'FQ'], taxDeclarationForm, change);
  useCompute('GF', sum, ['FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ', 'GA', 'GB', 'GC', 'GD', 'GE'], taxDeclarationForm, change);
  useCompute('GG', ({FR, GF}) => p(FR) - p(GF), ['FR', 'GF'], taxDeclarationForm, change);
  useCompute('GP', sum, ['GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO'], taxDeclarationForm, change);
  useCompute('GU', sum, ['GQ', 'GR', 'GS', 'GT'], taxDeclarationForm, change);
  useCompute('GV', ({GP, GU}) => p(GP) - p(GU), ['GP', 'GU'], taxDeclarationForm, change);
  useCompute('GW', computeGW, ['FR', 'GF', 'GH', 'GI', 'GP', 'GU'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2052">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 325, top: 25, width: 545, height: 27, 'font-size': 25, }}>
<span>
COMPTE DE RÉSULTAT DE L’EXERCICE (En liste)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 61, width: 165, height: 13, 'font-size': 13, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 73, width: 136, height: 12, 'font-size': 13, }}>
<span>
du Code général des impôts).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 930, top: 43, width: 223, height: 15, 'font-size': 21, }}>
<span>
DGFiP N° 2052-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 51, top: 93, width: 172, height: 16, 'font-size': 17, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 336, top: 1677, width: 556, height: 12, 'font-size': 13, }}>
<span>
(RENVOIS : voir tableau n° 2053) * Des explications concernant cette rubrique sont données dans la notice n° 2032.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 207, width: 165, height: 12, 'font-size': 17, }}>
<span>
Ventes de marchandises *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 554, top: 205, width: 18, height: 14, 'font-size': 21, }}>
<span>
FA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 205, width: 16, height: 14, 'font-size': 21, }}>
<span>
FB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 205, width: 17, height: 14, 'font-size': 21, }}>
<span>
FC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 315, width: 155, height: 12, 'font-size': 17, }}>
<span>
Chiffres d’affaires nets *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 555, top: 313, width: 12, height: 19, 'font-size': 21, }}>
<span>
FJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 313, width: 19, height: 14, 'font-size': 21, }}>
<span>
FK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 313, width: 16, height: 14, 'font-size': 21, }}>
<span>
FL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 352, width: 134, height: 12, 'font-size': 17, }}>
<span>
Production stockée *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 350, width: 20, height: 14, 'font-size': 21, }}>
<span>
FM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 388, width: 163, height: 12, 'font-size': 17, }}>
<span>
Production immobilisée *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 386, width: 19, height: 14, 'font-size': 21, }}>
<span>
FN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 424, width: 170, height: 16, 'font-size': 17, }}>
<span>
Subventions d’exploitation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 422, width: 19, height: 14, 'font-size': 21, }}>
<span>
FO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 460, width: 437, height: 16, 'font-size': 17, }}>
<span>
Reprises sur amortissements et provisions, transferts de charges * (9)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 458, width: 16, height: 14, 'font-size': 21, }}>
<span>
FP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 496, width: 150, height: 16, 'font-size': 17, }}>
<span>
Autres produits (1) (11)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 494, width: 19, height: 18, 'font-size': 21, }}>
<span>
FQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 568, width: 347, height: 16, 'font-size': 17, }}>
<span>
Achats de marchandises (y compris droits de douane)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 566, width: 14, height: 14, 'font-size': 21, }}>
<span>
FS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 287, top: 243, width: 45, height: 12, 'font-size': 17, }}>
<span>
biens *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 552, top: 241, width: 19, height: 14, 'font-size': 21, }}>
<span>
FD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 762, top: 241, width: 18, height: 14, 'font-size': 21, }}>
<span>
FE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 981, top: 241, width: 16, height: 14, 'font-size': 21, }}>
<span>
FF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 288, top: 280, width: 61, height: 11, 'font-size': 17, }}>
<span>
services *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 552, top: 277, width: 19, height: 14, 'font-size': 21, }}>
<span>
FG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 982, top: 277, width: 14, height: 14, 'font-size': 21, }}>
<span>
FI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 761, top: 277, width: 19, height: 14, 'font-size': 21, }}>
<span>
FH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 257, width: 118, height: 12, 'font-size': 17, }}>
<span>
Production vendue
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 698, top: 532, width: 255, height: 16, 'font-size': 17, }}>
<span>
Total des produits d’exploitation (2) (I)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 982, top: 530, width: 17, height: 14, 'font-size': 21, }}>
<span>
FR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 698, top: 1038, width: 255, height: 15, 'font-size': 17, }}>
<span>
Total des charges d’exploitation (4) (II)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1035, width: 19, height: 15, 'font-size': 21, }}>
<span>
GF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 742, top: 1398, width: 211, height: 16, 'font-size': 17, }}>
<span>
Total des produits financiers (V)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1396, width: 19, height: 14, 'font-size': 21, }}>
<span>
GP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 734, top: 1579, width: 219, height: 16, 'font-size': 17, }}>
<span>
Total des charges financières (VI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1577, width: 21, height: 14, 'font-size': 21, }}>
<span>
GU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 1070, width: 285, height: 17, 'font-size': 16, }}>
<span>
1 - RÉSULTAT D’EXPLOITATION (I - II)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1071, width: 22, height: 15, 'font-size': 21, }}>
<span>
GG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 1612, width: 253, height: 16, 'font-size': 16, }}>
<span>
2 - RÉSULTAT FINANCIER (V - VI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1613, width: 20, height: 14, 'font-size': 21, }}>
<span>
GV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 1648, width: 504, height: 16, 'font-size': 16, }}>
<span>
3 - RÉSULTAT COURANT AVANT IMPÔTS (I - II + III - IV + V - VI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1649, width: 24, height: 14, 'font-size': 21, }}>
<span>
GW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 834, top: 137, width: 61, height: 10, 'font-size': 15, }}>
<span>
Exercice N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 633, top: 171, width: 37, height: 10, 'font-size': 15, }}>
<span>
France
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 822, top: 165, width: 85, height: 14, 'font-size': 15, }}>
<span>
Exportations et
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 779, top: 177, width: 170, height: 11, 'font-size': 15, }}>
<span>
livraisons intracommunautaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1063, top: 171, width: 30, height: 11, 'font-size': 15, }}>
<span>
Total
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 271, width: 11, height: 207, 'font-size': 15, }}>
<span>
PRODUITS D’EXPLOITATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 707, width: 11, height: 202, 'font-size': 15, }}>
<span>
CHARGES D’EXPLOITATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 1211, width: 11, height: 169, 'font-size': 15, }}>
<span>
PRODUITS FINANCIERS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 1425, width: 11, height: 174, 'font-size': 15, }}>
<span>
CHARGES FINANCIERES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 67, top: 1112, width: 11, height: 43, 'font-size': 11, }}>
<span>
opérations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 87, top: 1105, width: 5, height: 55, 'font-size': 11, }}>
<span>
en commum
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 874, width: 11, height: 86, 'font-size': 15, }}>
<span>
DOTATIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 153, top: 854, width: 10, height: 126, 'font-size': 15, }}>
<span>
D’EXPLOITATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 604, width: 223, height: 16, 'font-size': 17, }}>
<span>
Variation de stock (marchandises)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 602, width: 17, height: 14, 'font-size': 21, }}>
<span>
FT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 640, width: 572, height: 16, 'font-size': 17, }}>
<span>
Achats de matières premières et autres approvisionnements (y compris droits de douane)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 638, width: 18, height: 15, 'font-size': 21, }}>
<span>
FU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 677, width: 406, height: 15, 'font-size': 17, }}>
<span>
Variation de stock (matières premières et approvisionnements)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 674, width: 18, height: 15, 'font-size': 21, }}>
<span>
FV
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 713, width: 283, height: 15, 'font-size': 17, }}>
<span>
Autres achats et charges externes (3) (6 bis)*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 710, width: 21, height: 15, 'font-size': 21, }}>
<span>
FW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 749, width: 247, height: 15, 'font-size': 17, }}>
<span>
Impôts, taxes et versements assimilés *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 746, width: 18, height: 14, 'font-size': 21, }}>
<span>
FX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 785, width: 149, height: 12, 'font-size': 17, }}>
<span>
Salaires et traitements *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 783, width: 18, height: 14, 'font-size': 21, }}>
<span>
FY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 821, width: 132, height: 16, 'font-size': 17, }}>
<span>
Charges sociales (10)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 818, width: 17, height: 15, 'font-size': 21, }}>
<span>
FZ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 1002, width: 123, height: 16, 'font-size': 17, }}>
<span>
Autres charges (12)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 995, width: 20, height: 15, 'font-size': 21, }}>
<span>
GE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1182, width: 249, height: 16, 'font-size': 17, }}>
<span>
Produits financiers de participations (5)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 1180, width: 16, height: 19, 'font-size': 21, }}>
<span>
GJ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1218, width: 467, height: 16, 'font-size': 17, }}>
<span>
Produits des autres valeurs mobilières et créances de l’actif immobilisé (5)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1216, width: 22, height: 14, 'font-size': 21, }}>
<span>
GK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 1254, width: 248, height: 16, 'font-size': 17, }}>
<span>
Autres intérêts et produits assimilés (5)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1252, width: 20, height: 14, 'font-size': 21, }}>
<span>
GL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1290, width: 298, height: 16, 'font-size': 17, }}>
<span>
Reprises sur provisions et transferts de charges
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 1288, width: 23, height: 14, 'font-size': 21, }}>
<span>
GM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1326, width: 201, height: 16, 'font-size': 17, }}>
<span>
Différences positives de change
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1324, width: 21, height: 15, 'font-size': 21, }}>
<span>
GN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1362, width: 391, height: 16, 'font-size': 17, }}>
<span>
Produits nets sur cessions de valeurs mobilières de placement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1360, width: 21, height: 14, 'font-size': 21, }}>
<span>
GO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1435, width: 363, height: 15, 'font-size': 17, }}>
<span>
Dotations financières aux amortissements et provisions *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1432, width: 22, height: 19, 'font-size': 21, }}>
<span>
GQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1471, width: 203, height: 16, 'font-size': 17, }}>
<span>
Intérêts et charges assimilées (6)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1468, width: 20, height: 15, 'font-size': 21, }}>
<span>
GR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1507, width: 204, height: 16, 'font-size': 17, }}>
<span>
Différences négatives de change
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 1504, width: 17, height: 15, 'font-size': 21, }}>
<span>
GS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1543, width: 400, height: 16, 'font-size': 17, }}>
<span>
Charges nettes sur cessions de valeurs mobilières de placement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 1540, width: 20, height: 15, 'font-size': 21, }}>
<span>
GT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1110, width: 243, height: 16, 'font-size': 17, }}>
<span>
Bénéfice attribué ou perte transférée *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 928, top: 1110, width: 25, height: 15, 'font-size': 17, }}>
<span>
(III)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 1107, width: 21, height: 15, 'font-size': 21, }}>
<span>
GH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1146, width: 249, height: 16, 'font-size': 17, }}>
<span>
Perte supportée ou bénéfice transféré *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 929, top: 1146, width: 24, height: 15, 'font-size': 17, }}>
<span>
(IV)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 980, top: 1143, width: 16, height: 15, 'font-size': 21, }}>
<span>
GI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 267, top: 226, width: 8, height: 65, 'font-size': 71, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 857, width: 214, height: 12, 'font-size': 17, }}>
<span>
- dotations aux amortissements *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 854, width: 21, height: 15, 'font-size': 21, }}>
<span>
GA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 893, width: 169, height: 16, 'font-size': 17, }}>
<span>
- dotations aux provisions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 891, width: 20, height: 14, 'font-size': 21, }}>
<span>
GB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 202, top: 929, width: 299, height: 16, 'font-size': 17, }}>
<span>
Sur actif circulant : dotations aux provisions *
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 979, top: 927, width: 19, height: 14, 'font-size': 21, }}>
<span>
GC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 202, top: 965, width: 322, height: 16, 'font-size': 17, }}>
<span>
Pour risques et charges : dotations aux provisions
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 977, top: 963, width: 22, height: 14, 'font-size': 21, }}>
<span>
GD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 202, top: 873, width: 123, height: 12, 'font-size': 17, }}>
<span>
Sur immobilisations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 364, top: 841, width: 8, height: 66, 'font-size': 71, }}>
<span>

<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 271, top: 23, width: 29, height: 29, 'font-size': 39, }}>
<span>
③
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1076, top: 94, width: 39, height: 12, 'font-size': 17, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1163, top: 94, width: 6, height: 7, 'font-size': 17, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 25, top: 1338, width: 11, height: 264, 'font-size': 13, }}>
<span>
N° 2052- SD – (SDNC-DGFiP) - Novembre 2018
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="JT" data-field-id="13291304" data-annot-id="12618144" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="FA" data-field-id="13308856" data-annot-id="12369248" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="FB" data-field-id="13309192" data-annot-id="12767136" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="FC" data-field-id="13309496" data-annot-id="12767328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="FD" data-field-id="13309832" data-annot-id="12767520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="FE" data-field-id="13310168" data-annot-id="12773216" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="FF" data-field-id="13310504" data-annot-id="12773408" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="FG" data-field-id="13310840" data-annot-id="12773600" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="FH" data-field-id="13311176" data-annot-id="12773792" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="FI" data-field-id="13311656" data-annot-id="12774128" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="FJ" data-field-id="13311992" data-annot-id="12774320" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="FK" data-field-id="13312328" data-annot-id="12774512" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="FL" data-field-id="13312664" data-annot-id="12774704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="FM" data-field-id="13313000" data-annot-id="12774896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="FN" data-field-id="13313336" data-annot-id="12775088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="FO" data-field-id="13313672" data-annot-id="12775280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="FP" data-field-id="13314008" data-annot-id="12775472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="FQ" data-field-id="13314488" data-annot-id="12775936" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="FR" data-field-id="13314824" data-annot-id="12776128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="FS" data-field-id="13315160" data-annot-id="12776320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="FT" data-field-id="13315496" data-annot-id="12776512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="FU" data-field-id="13315832" data-annot-id="12776704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="FV" data-field-id="13316168" data-annot-id="12776896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="FW" data-field-id="13316504" data-annot-id="12777088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="FX" data-field-id="13316840" data-annot-id="12777280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="FY" data-field-id="13317176" data-annot-id="12777472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="FZ" data-field-id="13317512" data-annot-id="12777664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GA" data-field-id="13317848" data-annot-id="12777856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="GB" data-field-id="13318184" data-annot-id="12778048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="GC" data-field-id="13318520" data-annot-id="12778240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="GD" data-field-id="13318856" data-annot-id="12778432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="GE" data-field-id="13319192" data-annot-id="12778624" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="GF" data-field-id="13319528" data-annot-id="12778816" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="GG" data-field-id="13314344" data-annot-id="12775664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="GH" data-field-id="13320456" data-annot-id="12779536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="GI" data-field-id="13320792" data-annot-id="12779728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GJ" data-field-id="13321128" data-annot-id="12779920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="GK" data-field-id="13321464" data-annot-id="12780112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="GL" data-field-id="13321800" data-annot-id="12780304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="GM" data-field-id="13322136" data-annot-id="12780496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="GN" data-field-id="13322472" data-annot-id="12780688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="GO" data-field-id="13322808" data-annot-id="12780880" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GP" data-field-id="13323144" data-annot-id="12781072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="GQ" data-field-id="13323480" data-annot-id="12781264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="GR" data-field-id="13323816" data-annot-id="12781456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GS" data-field-id="13324152" data-annot-id="12781648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GT" data-field-id="13324488" data-annot-id="12781840" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="GU" data-field-id="13324824" data-annot-id="12782032" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="GV" data-field-id="13325160" data-annot-id="12782224" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="GW" data-field-id="13325496" data-annot-id="12782416" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="10336776" data-annot-id="9793232" type="text" disabled />

          </div>
        </div>

      </div>
    </div>
  );
};

export default Tax2052;
