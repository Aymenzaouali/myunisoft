import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {sum, useCompute, parse as p, setValue} from "helpers/pdfforms";
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import { get as _get } from 'lodash';

import './style.scss';

const amountMoreZero = (values, fields) => {
  const diff = p(values[fields[0]]) - p(values[fields[1]]);
  return diff > 0 ? diff : 0;
};

const amountLessZero = (values, fields) => {
  const diff = p(values[fields[0]]) - p(values[fields[1]]);
  return diff < 0 ? diff : 0;
};

const Tax2035B = (props) => {
  const { taxDeclarationForm, form2035, form2035A, change } = props;
  const ZY = _get(form2035, 'ZY', null);
  const XB = _get(form2035A, 'XB', null);
  const ZZ = _get(form2035, 'ZZ', null);
  const XA = _get(form2035A, 'XA', null);

  useCompute('CE', sum, ['CA', 'CB', 'CC', 'CD'], taxDeclarationForm, change);
  useCompute('CL', sum, ['CS', 'DJ', 'DG', 'DK', 'DH', 'CT', 'DL', 'DM'], taxDeclarationForm, change);
  useCompute('CN', sum, ['CF', 'CG', 'CH', 'CK', 'CL', 'CM'], taxDeclarationForm, change);
  useCompute('CP', amountMoreZero, ['CE', 'CN'], taxDeclarationForm, change);
  useCompute('CR', amountLessZero, ['CE', 'CN'], taxDeclarationForm, change);
  useCompute('HE', sum, ['GE1', 'GE2', 'GE3', 'GK'], taxDeclarationForm, change);
  useCompute('HC', sum, ['GC1', 'GC2', 'GC3'], taxDeclarationForm, change);

  setValue(taxDeclarationForm, 'CB', ZY, change);
  setValue(taxDeclarationForm, 'CF', XB, change);
  setValue(taxDeclarationForm, 'CK', ZZ, change);
  setValue(taxDeclarationForm, 'CA', XA, change);

  return (
    <div className="form-tax-declaration-2035b">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 509, top: 106, width: 260, height: 15, 'font-size': 16, }}>
<span>
COMPTE DE RÉSULTAT FISCAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 231, top: 55, width: 122, height: 12, 'font-size': 12, }}>
<span>
Formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 231, top: 67, width: 144, height: 11, 'font-size': 12, }}>
<span>
(article 40 A de l’annexe III
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 231, top: 80, width: 156, height: 12, 'font-size': 12, }}>
<span>
au Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 538, top: 48, width: 203, height: 22, 'font-size': 27, }}>
<span>
REVENUS 2018
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 967, top: 49, width: 173, height: 16, 'font-size': 20, }}>
<span>
N° 2035-B SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 965, top: 76, width: 148, height: 10, 'font-size': 10, }}>
<span>
Si ce formulaire est déposé sans
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 965, top: 86, width: 144, height: 9, 'font-size': 10, }}>
<span>
nformations chiffrées, cocher la
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 965, top: 97, width: 97, height: 8, 'font-size': 10, }}>
<span>
case néant ci-contre :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 965, top: 114, width: 142, height: 9, 'font-size': 9, }}>
<span>
Ne porter qu’une somme par ligne
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 965, top: 124, width: 114, height: 9, 'font-size': 9, }}>
<span>
(ne pas porter les centimes)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 98, width: 79, height: 11, 'font-size': 12, }}>
<span>
N° 15945 Ý 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 175, top: 148, width: 333, height: 18, 'font-size': 18, }}>
<span>
NOM ET PRÉNOMS OU DÉNOMINATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 188, width: 74, height: 14, 'font-size': 18, }}>
<span>
N° SIRET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 239, width: 11, height: 16, 'font-size': 22, }}>
<span>
4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 247, width: 219, height: 15, 'font-size': 15, }}>
<span>
34 Excédent (ligne 7 - ligne 33)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 387, top: 256, width: 519, height: 3, 'font-size': 12, }}>
<span>
...................................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 247, width: 20, height: 12, 'font-size': 15, }}>
<span>
CA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 286, width: 204, height: 14, 'font-size': 15, }}>
<span>
35 Plus-values à court terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 371, top: 282, width: 20, height: 20, 'font-size': 22, }}>
<span>
Q
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 395, top: 298, width: 498, height: 2, 'font-size': 12, }}>
<span>
............................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 285, width: 44, height: 15, 'font-size': 15, }}>
<span>
. CB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 318, width: 158, height: 18, 'font-size': 15, }}>
<span>
36 Divers à réintégrer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 325, top: 315, width: 19, height: 20, 'font-size': 22, }}>
<span>
R
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 349, top: 330, width: 561, height: 3, 'font-size': 12, }}>
<span>
................................................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 318, width: 19, height: 12, 'font-size': 15, }}>
<span>
CC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 351, width: 234, height: 17, 'font-size': 15, }}>
<span>
37 Bénéfice Sté civile de moyens
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 401, top: 347, width: 19, height: 20, 'font-size': 22, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 421, top: 363, width: 481, height: 2, 'font-size': 12, }}>
<span>
.......................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 350, width: 19, height: 12, 'font-size': 15, }}>
<span>
CD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 389, width: 16, height: 11, 'font-size': 15, }}>
<span>
38
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 292, top: 388, width: 152, height: 15, 'font-size': 15, }}>
<span>
TOTAL (lignes 34 à 37)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 445, top: 398, width: 459, height: 2, 'font-size': 12, }}>
<span>
................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 928, top: 388, width: 17, height: 12, 'font-size': 15, }}>
<span>
CE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 163, top: 459, width: 238, height: 15, 'font-size': 15, }}>
<span>
39 Insuffisance (ligne 33 - ligne 7)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 406, top: 468, width: 497, height: 3, 'font-size': 12, }}>
<span>
............................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 928, top: 459, width: 17, height: 12, 'font-size': 15, }}>
<span>
CF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 491, width: 175, height: 15, 'font-size': 15, }}>
<span>
40 Frais d&#39;établissement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 343, top: 488, width: 20, height: 19, 'font-size': 22, }}>
<span>
T
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 369, top: 503, width: 532, height: 3, 'font-size': 12, }}>
<span>
.......................................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 491, width: 19, height: 12, 'font-size': 15, }}>
<span>
CG
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 527, width: 231, height: 15, 'font-size': 15, }}>
<span>
41 Dotation aux amortissements
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 399, top: 524, width: 20, height: 19, 'font-size': 22, }}>
<span>
U
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 425, top: 539, width: 474, height: 2, 'font-size': 12, }}>
<span>
.....................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 527, width: 19, height: 12, 'font-size': 15, }}>
<span>
CH
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 563, width: 217, height: 12, 'font-size': 15, }}>
<span>
42 Moins-values à court terme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 385, top: 572, width: 520, height: 2, 'font-size': 12, }}>
<span>
...................................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 562, width: 20, height: 13, 'font-size': 15, }}>
<span>
CK
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 762, width: 225, height: 17, 'font-size': 15, }}>
<span>
44 Déficits Ste civile de moyens
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 394, top: 759, width: 19, height: 19, 'font-size': 22, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 774, width: 491, height: 2, 'font-size': 12, }}>
<span>
..........................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 925, top: 761, width: 22, height: 12, 'font-size': 15, }}>
<span>
CM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 806, width: 15, height: 12, 'font-size': 15, }}>
<span>
45
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 292, top: 806, width: 152, height: 14, 'font-size': 15, }}>
<span>
TOTAL (lignes 39 à 44)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 449, top: 815, width: 456, height: 2, 'font-size': 12, }}>
<span>
...............................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 806, width: 19, height: 12, 'font-size': 15, }}>
<span>
CN
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 872, width: 222, height: 15, 'font-size': 15, }}>
<span>
46 Bénéfice (ligne 38 - ligne 45)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 391, top: 881, width: 513, height: 3, 'font-size': 12, }}>
<span>
.................................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 928, top: 872, width: 18, height: 12, 'font-size': 15, }}>
<span>
CP
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 909, width: 207, height: 14, 'font-size': 15, }}>
<span>
47 Déficit (ligne 45 - ligne 38)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 376, top: 918, width: 526, height: 2, 'font-size': 12, }}>
<span>
.....................................................................................................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 909, width: 19, height: 12, 'font-size': 15, }}>
<span>
CR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 951, width: 10, height: 16, 'font-size': 22, }}>
<span>
5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 184, top: 944, width: 138, height: 14, 'font-size': 14, }}>
<span>
Taxe sur la valeur ajoutée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 382, top: 953, width: 240, height: 11, 'font-size': 14, }}>
<span>
Montant de la TVA afférente aux recettes brutes :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 952, width: 20, height: 12, 'font-size': 15, }}>
<span>
CX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 382, top: 991, width: 417, height: 14, 'font-size': 14, }}>
<span>
Montant de la TVA afférente aux achats (biens et services autres qu’immobilisations) :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 991, width: 20, height: 12, 'font-size': 15, }}>
<span>
CY
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 487, top: 1024, width: 302, height: 11, 'font-size': 14, }}>
<span>
- dont montant de la TVA afférente aux honoraires rétrocédés :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 928, top: 1024, width: 18, height: 12, 'font-size': 15, }}>
<span>
CZ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 1055, width: 11, height: 16, 'font-size': 22, }}>
<span>
6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1058, width: 175, height: 14, 'font-size': 14, }}>
<span>
Contribution économique territoriale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 346, top: 1054, width: 19, height: 20, 'font-size': 22, }}>
<span>
X
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 382, top: 1058, width: 291, height: 13, 'font-size': 14, }}>
<span>
Recettes provenant d’activités exonérées à titre permanent :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 926, top: 1057, width: 20, height: 12, 'font-size': 15, }}>
<span>
AU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 1107, width: 11, height: 15, 'font-size': 22, }}>
<span>
7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1100, width: 599, height: 15, 'font-size': 15, }}>
<span>
Barèmes kilométriques (évaluation forfaitaire des frais de transport : autos et/ou motos) et
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 773, top: 1096, width: 19, height: 20, 'font-size': 22, }}>
<span>
M
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 167, top: 1123, width: 978, height: 14, 'font-size': 15, }}>
<span>
(1) Type : T (véhicule de tourisme) ; M (moto) ; V (vélomoteur, scooter); (2) mettre une croix dans la colonne; (3) indiquer : super, diesel, super sans plomb, GPL.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 333, width: 9, height: 12, 'font-size': 15, }}>
<span>
D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 347, width: 8, height: 15, 'font-size': 15, }}>
<span>
É
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 367, width: 10, height: 12, 'font-size': 15, }}>
<span>
T
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 385, width: 8, height: 11, 'font-size': 15, }}>
<span>
E
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 402, width: 9, height: 11, 'font-size': 15, }}>
<span>
R
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 419, width: 12, height: 12, 'font-size': 15, }}>
<span>
M
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 436, width: 2, height: 12, 'font-size': 15, }}>
<span>
I
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 454, width: 10, height: 11, 'font-size': 15, }}>
<span>
N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 471, width: 11, height: 11, 'font-size': 15, }}>
<span>
A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 488, width: 10, height: 12, 'font-size': 15, }}>
<span>
T
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 505, width: 2, height: 12, 'font-size': 15, }}>
<span>
I
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 522, width: 10, height: 12, 'font-size': 15, }}>
<span>
O
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 540, width: 10, height: 11, 'font-size': 15, }}>
<span>
N
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 611, width: 9, height: 12, 'font-size': 15, }}>
<span>
D
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 629, width: 10, height: 11, 'font-size': 15, }}>
<span>
U
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 678, width: 9, height: 12, 'font-size': 15, }}>
<span>
R
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 692, width: 8, height: 15, 'font-size': 15, }}>
<span>
É
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 713, width: 9, height: 11, 'font-size': 15, }}>
<span>
S
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 730, width: 10, height: 12, 'font-size': 15, }}>
<span>
U
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 747, width: 7, height: 12, 'font-size': 15, }}>
<span>
L
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 765, width: 10, height: 11, 'font-size': 15, }}>
<span>
T
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 782, width: 11, height: 11, 'font-size': 15, }}>
<span>
A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 799, width: 10, height: 11, 'font-size': 15, }}>
<span>
T
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 164, top: 604, width: 15, height: 12, 'font-size': 15, }}>
<span>
43
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 598, width: 168, height: 13, 'font-size': 15, }}>
<span>
dont exonération sur le béné-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 614, width: 162, height: 12, 'font-size': 15, }}>
<span>
 ce  zone franche urbaine -
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 629, width: 138, height: 15, 'font-size': 15, }}>
<span>
territoire entrepreneur »
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 414, top: 616, width: 18, height: 12, 'font-size': 15, }}>
<span>
CS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 598, width: 168, height: 13, 'font-size': 15, }}>
<span>
dont exonération sur le béné-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 613, width: 154, height: 16, 'font-size': 15, }}>
<span>
 ce  pôle de compétivité »
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 573, top: 628, width: 56, height: 13, 'font-size': 15, }}>
<span>
Hors CICE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 762, top: 616, width: 21, height: 12, 'font-size': 15, }}>
<span>
AX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 928, top: 604, width: 17, height: 12, 'font-size': 15, }}>
<span>
CL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 648, width: 168, height: 13, 'font-size': 15, }}>
<span>
dont exonération sur le béné-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 663, width: 153, height: 15, 'font-size': 15, }}>
<span>
 ce  entreprise nouvelle »
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 411, top: 658, width: 24, height: 12, 'font-size': 15, }}>
<span>
AW
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 649, width: 101, height: 12, 'font-size': 15, }}>
<span>
dont abondement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 663, width: 128, height: 15, 'font-size': 15, }}>
<span>
sur l&#39;épargne salariale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 764, top: 658, width: 18, height: 12, 'font-size': 15, }}>
<span>
CT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 684, width: 146, height: 14, 'font-size': 15, }}>
<span>
dont exonération  jeunes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 699, width: 139, height: 14, 'font-size': 15, }}>
<span>
entreprises innovantes »
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 413, top: 693, width: 19, height: 12, 'font-size': 15, }}>
<span>
CU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 682, width: 168, height: 13, 'font-size': 15, }}>
<span>
dont exonération sur le béné-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 699, width: 126, height: 14, 'font-size': 15, }}>
<span>
 ce  jeunes artistes »
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 693, width: 19, height: 12, 'font-size': 15, }}>
<span>
CO
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 718, width: 179, height: 12, 'font-size': 15, }}>
<span>
dont exonération médecins  zones
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 225, top: 734, width: 152, height: 11, 'font-size': 15, }}>
<span>
dé  citaires en offre de soins »
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 417, top: 728, width: 12, height: 12, 'font-size': 15, }}>
<span>
CI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 718, width: 159, height: 12, 'font-size': 15, }}>
<span>
dont déductions  médecins
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 573, top: 728, width: 210, height: 17, 'font-size': 15, }}>
<span>
conventionnés de secteur I » CQ
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 200, top: 629, width: 11, height: 110, 'font-size': 15, }}>
<span>
Divers à déduire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 195, top: 598, width: 19, height: 20, 'font-size': 22, }}>
<span>
V
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 735, top: 1100, width: 7, height: 9, 'font-size': 12, }}>
<span>
B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 1166, width: 184, height: 14, 'font-size': 15, }}>
<span>
Désignation des véhicules :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 432, top: 1177, width: 54, height: 12, 'font-size': 15, }}>
<span>
Puissance
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 437, top: 1194, width: 43, height: 12, 'font-size': 15, }}>
<span>
fiscale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 504, top: 1177, width: 52, height: 12, 'font-size': 15, }}>
<span>
Barème
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 515, top: 1194, width: 30, height: 12, 'font-size': 15, }}>
<span>
BNC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 521, top: 1237, width: 17, height: 13, 'font-size': 15, }}>
<span>
(2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 626, top: 1171, width: 79, height: 12, 'font-size': 15, }}>
<span>
Barème BIC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 790, top: 1166, width: 82, height: 14, 'font-size': 15, }}>
<span>
Kilométrage
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 785, top: 1183, width: 91, height: 14, 'font-size': 15, }}>
<span>
professionnel
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 924, top: 1166, width: 75, height: 12, 'font-size': 15, }}>
<span>
Indemnités
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 916, top: 1183, width: 91, height: 14, 'font-size': 15, }}>
<span>
kilométriques
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 922, top: 1200, width: 78, height: 12, 'font-size': 15, }}>
<span>
déductibles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1035, top: 1166, width: 112, height: 12, 'font-size': 15, }}>
<span>
Amortissements
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1060, top: 1183, width: 63, height: 14, 'font-size': 15, }}>
<span>
pratiqués
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1052, top: 1200, width: 79, height: 15, 'font-size': 15, }}>
<span>
à réintégrer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1037, top: 1213, width: 109, height: 14, 'font-size': 14, }}>
<span>
 si véhicules inscrits
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1063, top: 1228, width: 57, height: 13, 'font-size': 14, }}>
<span>
au registre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1037, top: 1240, width: 109, height: 14, 'font-size': 14, }}>
<span>
des immobilisations)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 1208, width: 66, height: 13, 'font-size': 15, }}>
<span>
Modèle(s)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 369, top: 1219, width: 40, height: 14, 'font-size': 15, }}>
<span>
Types
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 381, top: 1238, width: 16, height: 12, 'font-size': 15, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 592, top: 1239, width: 17, height: 12, 'font-size': 15, }}>
<span>
(2)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 684, top: 1203, width: 33, height: 14, 'font-size': 15, }}>
<span>
Type
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 658, top: 1220, width: 86, height: 11, 'font-size': 15, }}>
<span>
de carburant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 692, top: 1238, width: 17, height: 13, 'font-size': 15, }}>
<span>
(3)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 1380, width: 380, height: 14, 'font-size': 15, }}>
<span>
- Frais réels non couverts par les barèmes kilométriques
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 136, top: 1416, width: 781, height: 14, 'font-size': 15, }}>
<span>
Total A à reporter ligne 23 de l’annexe 2035 A ; Total B à reporter au cadre B de la page 2 de la déclaration 2035 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1038, top: 1416, width: 9, height: 11, 'font-size': 15, }}>
<span>
B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1370, width: 9, height: 67, 'font-size': 11, }}>
<span>
N° 2035-B-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 103, top: 1181, width: 12, height: 183, 'font-size': 12, }}>
<span>
– (SDNC-DGFiP) – Novembre 2018
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="DF" data-field-id="28057640" data-annot-id="27036688" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="CA" data-field-id="28057848" data-annot-id="27328768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="CB" data-field-id="28058152" data-annot-id="26919152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="CC" data-field-id="28058456" data-annot-id="26919344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="CD" data-field-id="28070024" data-annot-id="27332752" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="CE" data-field-id="28070360" data-annot-id="27332944" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="CF" data-field-id="28070696" data-annot-id="27333136" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="CG" data-field-id="28071032" data-annot-id="27333328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="CH" data-field-id="28071368" data-annot-id="27333520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="CK" data-field-id="28071848" data-annot-id="27333712" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="CL" data-field-id="28072184" data-annot-id="27333904" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="CS" data-field-id="28072520" data-annot-id="27334096" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="DH" data-field-id="28072856" data-annot-id="27334288" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="DJ" data-field-id="28073192" data-annot-id="27334480" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="CT" data-field-id="28073528" data-annot-id="27334672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="DG" data-field-id="28073864" data-annot-id="27334864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="DL" data-field-id="28074200" data-annot-id="27335056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="DK" data-field-id="28074680" data-annot-id="27335520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="DM" data-field-id="28075016" data-annot-id="27335712" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="CM" data-field-id="28075352" data-annot-id="27335904" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="CN" data-field-id="28075688" data-annot-id="27336096" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="CP" data-field-id="28076024" data-annot-id="27336288" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="CR" data-field-id="28076360" data-annot-id="27336480" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="CX" data-field-id="28076696" data-annot-id="27336672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="CY" data-field-id="28077032" data-annot-id="27336864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="CZ" data-field-id="28077368" data-annot-id="27337056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="JA" data-field-id="28077704" data-annot-id="27337248" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GA1" data-field-id="28078040" data-annot-id="27337440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="GA2" data-field-id="28078376" data-annot-id="27337632" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="GA3" data-field-id="28078712" data-annot-id="27337824" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="GG1" data-field-id="28079048" data-annot-id="27338016" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="GG3" data-field-id="28079384" data-annot-id="27338208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="GG2" data-field-id="28079720" data-annot-id="27338400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="GH3" data-field-id="28074504" data-annot-id="27335248" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="GH2" data-field-id="28080616" data-annot-id="27339120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="GH1" data-field-id="28080952" data-annot-id="27339312" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GB3" data-field-id="28081288" data-annot-id="27339504" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="GB2" data-field-id="28081624" data-annot-id="27339696" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="GB1" data-field-id="28081960" data-annot-id="27339888" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="GF3" data-field-id="28082296" data-annot-id="27340080" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="GF2" data-field-id="28082632" data-annot-id="27340272" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="GF1" data-field-id="28082968" data-annot-id="27340464" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GJ3" data-field-id="28083304" data-annot-id="27340656" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="GJ2" data-field-id="28083640" data-annot-id="27340848" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="GJ1" data-field-id="28083976" data-annot-id="27341040" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GD3" data-field-id="28084312" data-annot-id="27341232" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GD2" data-field-id="28084648" data-annot-id="27341424" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="GD1" data-field-id="28084984" data-annot-id="27341616" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="GE3" data-field-id="28085320" data-annot-id="27341808" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="GE2" data-field-id="28085656" data-annot-id="27342000" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="GE1" data-field-id="28085992" data-annot-id="27342192" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="GC3" data-field-id="28086328" data-annot-id="27342384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="GC2" data-field-id="28086664" data-annot-id="27342576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="GC1" data-field-id="28087000" data-annot-id="27342768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="GK" data-field-id="28087336" data-annot-id="27342960" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="HC" data-field-id="28087672" data-annot-id="27343152" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="HE" data-field-id="28088008" data-annot-id="27343344" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="34351272" data-annot-id="33690880" type="text" disabled />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="SIRET_STET" data-field-id="34351608" data-annot-id="33691072" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2035B;
