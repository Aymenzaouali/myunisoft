import React from 'react';
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import { setValue, sumOfValues } from "helpers/pdfforms";
import { get as _get } from 'lodash';

import './style.scss';

// Component
const Tax2065 = (props) => {
  const { taxDeclarationForm, form2059E, form2033B, change } = props;
  const EZ = _get(form2059E, 'EZ', null);
  const EL = _get(form2059E, 'EL', null);
  const GD = _get(form2059E, 'GD', null);
  const EF = _get(form2059E, 'EF', null);
  const EE = _get(form2059E, 'EE', null);
  const ED = _get(form2059E, 'ED', null);
  const EM = _get(form2059E, 'EM', null);
  const EP = _get(form2059E, 'EP', null);
  const BH = _get(form2033B, 'BH', null);
  const BF = _get(form2033B, 'BF', null);
  const BG = _get(form2033B, 'BG', null);
  const BD = _get(form2033B, 'BD', null);
  const BE = _get(form2033B, 'BE', null);

  const GA2059E = _get(form2059E, 'GA', null);
  const GA2033B = _get(form2033B, 'GA', null);

  setValue(taxDeclarationForm, 'MD', sumOfValues([EZ, EL, GD, EF, EE, ED, EM, EP, BH, BF, BG, BD, BE]), change);
  setValue(taxDeclarationForm, 'LZ', sumOfValues([GA2059E, GA2033B]), change);

  // Rendering
  return (
    <div className="tax2065">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 429, top: 48, width: 360, height: 16, 'font-size': 15, }}>
<span>
DIRECTION GÉNÉRALE DES FINANCES PUBLIQUES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1042, top: 50, width: 85, height: 13, 'font-size': 18, }}>
<span>
N° 2065-SD
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1077, top: 73, width: 31, height: 11, 'font-size': 15, }}>
<span>
2019
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 96, width: 77, height: 11, 'font-size': 15, }}>
<span>
N° 11084*20
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 120, width: 108, height: 11, 'font-size': 12, }}>
<span>
Formulaire obligatoire
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 139, width: 172, height: 11, 'font-size': 12, }}>
<span>
(art 223 du Code général des impôts)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 994, top: 157, width: 101, height: 8, 'font-size': 11, }}>
<span>
Timbre à date du service
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 178, width: 112, height: 11, 'font-size': 15, }}>
<span>
Exercice ouvert le
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 178, width: 57, height: 11, 'font-size': 15, }}>
<span>
et clos le
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 769, top: 178, width: 182, height: 14, 'font-size': 15, }}>
<span>
Régime simplifié d&#39;imposition
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 200, width: 407, height: 14, 'font-size': 15, }}>
<span>
Déclaration souscrite pour le résultat d&#39;ensemble du groupe
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 798, top: 200, width: 124, height: 14, 'font-size': 15, }}>
<span>
Régime réel normal
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 222, width: 298, height: 14, 'font-size': 15, }}>
<span>
Si PME innovantes, cocher la case ci-contre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 278, width: 307, height: 12, 'font-size': 16, }}>
<span>
A IDENTIFICATION DE L&#39;ENTREPRISE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 236, top: 301, width: 161, height: 14, 'font-size': 15, }}>
<span>
Désignation de la société:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 782, top: 301, width: 155, height: 14, 'font-size': 15, }}>
<span>
Adresse du siège social:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 88, top: 359, width: 42, height: 12, 'font-size': 15, }}>
<span>
SIRET
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 203, top: 382, width: 225, height: 15, 'font-size': 15, }}>
<span>
Adresse du principal établissement:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 726, top: 382, width: 266, height: 15, 'font-size': 15, }}>
<span>
Ancienne adresse en cas de changement:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 470, width: 901, height: 14, 'font-size': 15, }}>
<span>
Les entreprises placées sous le régime des groupes de sociétés doivent déposer cette déclaration en deux exemplaires (art 223 A à U du CGI)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 493, width: 337, height: 14, 'font-size': 15, }}>
<span>
Date d&#39;entrée dans le groupe de la société déclarante
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 627, top: 573, width: 42, height: 11, 'font-size': 15, }}>
<span>
SIRET
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 595, width: 10, height: 12, 'font-size': 16, }}>
<span>
B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 618, width: 115, height: 12, 'font-size': 15, }}>
<span>
Activités exercées
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 763, top: 618, width: 293, height: 15, 'font-size': 15, }}>
<span>
Si vous avez changé d&#39;activité, cochez la case
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 71, top: 642, width: 11, height: 12, 'font-size': 16, }}>
<span>
C
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 666, width: 108, height: 11, 'font-size': 15, }}>
<span>
1 Résultat fiscal
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 972, top: 666, width: 40, height: 11, 'font-size': 15, }}>
<span>
Déficit
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 291, top: 691, width: 168, height: 15, 'font-size': 15, }}>
<span>
Bénéfice imposable à 15%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 56, top: 716, width: 91, height: 11, 'font-size': 15, }}>
<span>
2 Plus-values
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 738, width: 222, height: 15, 'font-size': 15, }}>
<span>
PV à long terme imposables à 15%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 496, top: 738, width: 521, height: 15, 'font-size': 15, }}>
<span>
Résultat net de la concession de licences d&#39;exploitation de brevets au taux de 15%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 984, top: 914, width: 109, height: 14, 'font-size': 15, }}>
<span>
Autres dispositifs
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 76, top: 1013, width: 10, height: 13, 'font-size': 16, }}>
<span>
D
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1034, width: 833, height: 14, 'font-size': 15, }}>
<span>
1. Au titre des revenus mobiliers de source française ou étrangère, ayant donné lieu à la délivrance d&#39;un certificat de crédits d&#39;impôt
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 1093, width: 10, height: 13, 'font-size': 16, }}>
<span>
E
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1116, width: 324, height: 13, 'font-size': 15, }}>
<span>
Recettes nettes soumises à la contribution de 2,5%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1138, width: 9, height: 12, 'font-size': 16, }}>
<span>
F
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 749, top: 1182, width: 85, height: 10, 'font-size': 13, }}>
<span>
Nom / Adresse
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 769, top: 1203, width: 15, height: 12, 'font-size': 15, }}>
<span>
N°
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 749, top: 1247, width: 94, height: 12, 'font-size': 15, }}>
<span>
Nom / Adresse
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 769, top: 1268, width: 15, height: 12, 'font-size': 15, }}>
<span>
N°
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 73, top: 1289, width: 12, height: 12, 'font-size': 16, }}>
<span>
G
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 635, top: 1312, width: 212, height: 14, 'font-size': 15, }}>
<span>
Si oui, indication du logiciel utilisé
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1413, width: 369, height: 14, 'font-size': 15, }}>
<span>
Nom et adresse du professionnel de l&#39;expertise comptable:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 1413, width: 172, height: 11, 'font-size': 15, }}>
<span>
Nom et adresse du conseil:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 362, top: 1461, width: 23, height: 11, 'font-size': 15, }}>
<span>
Tél:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 919, top: 1461, width: 23, height: 11, 'font-size': 15, }}>
<span>
Tél:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1480, width: 79, height: 12, 'font-size': 15, }}>
<span>
OGA/OMGA
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 170, top: 1480, width: 129, height: 12, 'font-size': 15, }}>
<span>
Viseur conventionné
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 347, top: 1480, width: 208, height: 14, 'font-size': 15, }}>
<span>
(Cocher la case correspondante)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 588, top: 1480, width: 131, height: 12, 'font-size': 15, }}>
<span>
Identité du déclarant:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1502, width: 376, height: 12, 'font-size': 15, }}>
<span>
Nom et adresse du CGA/OMGA ou du viseur conventionné:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 1502, width: 33, height: 12, 'font-size': 15, }}>
<span>
Date:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 811, top: 1502, width: 29, height: 12, 'font-size': 15, }}>
<span>
Lieu:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 1523, width: 181, height: 14, 'font-size': 15, }}>
<span>
Qualité et nom du signataire:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1543, width: 323, height: 15, 'font-size': 15, }}>
<span>
N° d&#39;agrément du CGA/OMGA/viseur conventionné
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 1543, width: 64, height: 15, 'font-size': 15, }}>
<span>
Signature:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 150, width: 261, height: 19, 'font-size': 20, }}>
<span>
IMPÔT SUR LES SOCIÉTÉS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 245, width: 828, height: 15, 'font-size': 15, }}>
<span>
Si option pour le régime optionnel de taxation au tonnage, art. 209-0 B (entreprises de transport maritime), cocher la case
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 360, width: 32, height: 11, 'font-size': 15, }}>
<span>
Mél :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 444, width: 242, height: 15, 'font-size': 16, }}>
<span>
RÉGIME FISCAL DES GROUPES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 515, width: 685, height: 14, 'font-size': 15, }}>
<span>
Pour les sociétés filiales, désignation, adresse du lieu d&#39;imposition et n° d&#39;identification de la société mère:
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 111, top: 592, width: 71, height: 15, 'font-size': 16, }}>
<span>
ACTIVITÉ
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 639, width: 375, height: 15, 'font-size': 16, }}>
<span>
RÉCAPITULATION DES ÉLÉMENTS D&#39;IMPOSITION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 492, top: 644, width: 225, height: 13, 'font-size': 13, }}>
<span>
(cf. notice de la déclaration n°2065-SD)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 195, top: 666, width: 264, height: 14, 'font-size': 15, }}>
<span>
Bénéfice imposable à 33 1/3% ou à 31%*
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 663, top: 666, width: 169, height: 14, 'font-size': 15, }}>
<span>
Bénéfice imposable à 28%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 772, width: 92, height: 13, 'font-size': 13, }}>
<span>
PV à long terme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 71, top: 786, width: 104, height: 13, 'font-size': 13, }}>
<span>
imposables à 19%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 321, top: 772, width: 138, height: 13, 'font-size': 13, }}>
<span>
Autres PV imposables à
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 786, width: 25, height: 11, 'font-size': 13, }}>
<span>
19%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 650, top: 772, width: 92, height: 13, 'font-size': 13, }}>
<span>
PV à long terme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 644, top: 786, width: 98, height: 13, 'font-size': 13, }}>
<span>
imposables à 0%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 772, width: 81, height: 11, 'font-size': 13, }}>
<span>
PV exonérées
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 901, top: 786, width: 117, height: 14, 'font-size': 13, }}>
<span>
(art. 238 quindecies)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 810, width: 829, height: 14, 'font-size': 15, }}>
<span>
3 Abattements et exonérations notamment entreprises nouvelles ou implantées en zones d&#39;entreprises ou zones franches
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 835, width: 216, height: 14, 'font-size': 15, }}>
<span>
Entreprise nouvelle, art. 44 sexies
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 359, top: 834, width: 315, height: 14, 'font-size': 15, }}>
<span>
Jeunes entreprises innovantes, art. 44 sexies-0 A
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 839, top: 835, width: 243, height: 14, 'font-size': 15, }}>
<span>
Pôle de compétitivité, art. 44 undecies
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 84, top: 877, width: 221, height: 14, 'font-size': 15, }}>
<span>
Entreprise nouvelle, art. 44 septies
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 877, width: 280, height: 14, 'font-size': 15, }}>
<span>
Zone franche d&#39;activité, art. 44 quaterdecies
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 742, top: 877, width: 351, height: 13, 'font-size': 15, }}>
<span>
Zone de restructuration de la défense, art. 44 terdecies
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 82, top: 914, width: 340, height: 14, 'font-size': 15, }}>
<span>
Bassins urbains à dynamiser (BUD), art.44 sexdecies
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 505, top: 912, width: 408, height: 15, 'font-size': 15, }}>
<span>
Zone franche Urbaine – Territoire entrepreneur, art . 44 octies A
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 951, width: 158, height: 11, 'font-size': 15, }}>
<span>
Société d&#39;investissement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 149, top: 967, width: 104, height: 11, 'font-size': 15, }}>
<span>
immobilier cotée
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 316, top: 951, width: 173, height: 11, 'font-size': 15, }}>
<span>
Bénéfice ou déficit exonéré
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 314, top: 967, width: 181, height: 14, 'font-size': 15, }}>
<span>
(indiquer + ou - selon le cas)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 728, top: 951, width: 218, height: 11, 'font-size': 15, }}>
<span>
Plus-values exonérées relevant du
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 797, top: 967, width: 80, height: 12, 'font-size': 15, }}>
<span>
taux de 15%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 991, width: 287, height: 15, 'font-size': 15, }}>
<span>
4 Option pour le crédit d&#39;impôt outre-mer :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 376, top: 991, width: 277, height: 15, 'font-size': 15, }}>
<span>
dans le secteur productif, art. 244 quater W
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 758, top: 991, width: 337, height: 15, 'font-size': 15, }}>
<span>
dans le secteur du logement social, art. 244 quater X
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1013, width: 107, height: 13, 'font-size': 16, }}>
<span>
IMPUTATIONS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 1016, width: 202, height: 12, 'font-size': 12, }}>
<span>
(cf. notice de la déclaration n° 2065-SD)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1053, width: 961, height: 17, 'font-size': 15, }}>
<span>
2. Au titre des revenus auxquels est attaché, en vertu d&#39;une convention fiscale conclue avec un État étranger, un territoire ou une collectivité territoriale
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1072, width: 563, height: 14, 'font-size': 15, }}>
<span>
d&#39;Outre-mer, un crédit d&#39;impôt représentatif de l&#39;impôt de cet état, territoire ou collectivité.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1093, width: 444, height: 13, 'font-size': 16, }}>
<span>
CONTRIBUTION ANNUELLE SUR LES REVENUS LOCATIFS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 560, top: 1096, width: 202, height: 12, 'font-size': 12, }}>
<span>
(cf. notice de la déclaration n° 2065-SD)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1135, width: 790, height: 15, 'font-size': 16, }}>
<span>
ENTREPRISES SOUMISES OU DÉSIGNEES AU DÉPOT DE LA DÉCLARATION PAYS PAR PAYS CbC/DAC4
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 907, top: 1141, width: 185, height: 11, 'font-size': 12, }}>
<span>
(cf. notice du formulaire n° 2065-SD)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1159, width: 901, height: 14, 'font-size': 15, }}>
<span>
1- Si vous êtes l&#39;entreprise, tête de groupe, soumise au dépôt de la déclaration n° 2258-SD (art. 223 quinquies C-I-1), cocher la case ci-contre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1175, width: 690, height: 15, 'font-size': 15, }}>
<span>
2- Si vous êtes la société tête de groupe et que vous avez désigné une autre entité du groupe pour
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1192, width: 689, height: 14, 'font-size': 15, }}>
<span>
souscrire la déclaration n° 2258-SD, indiquer le nom, adresse et numéro d&#39;identification fiscale de l&#39;entité
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1208, width: 58, height: 13, 'font-size': 15, }}>
<span>
désignée
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1225, width: 988, height: 14, 'font-size': 15, }}>
<span>
3- Si vous êtes l&#39;entreprise désignée au dépôt de la déclaration n° 2258-SD par la société tête de groupe (art. 223 quinquies C-I-2), cocher la case ci-contre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1249, width: 629, height: 14, 'font-size': 15, }}>
<span>
Dans ce cas, veuillez indiquer le nom, adresse et numéro d&#39;identification fiscale de la société tête
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 111, top: 1265, width: 65, height: 15, 'font-size': 15, }}>
<span>
de groupe
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 1286, width: 239, height: 15, 'font-size': 16, }}>
<span>
COMPTABILITÉ INFORMATISÉE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1312, width: 376, height: 14, 'font-size': 15, }}>
<span>
L&#39;entreprise dispose-t-elle d&#39;une comptabilité informatisée ?
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1347, width: 1080, height: 14, 'font-size': 15, }}>
<span>
Vous devez obligatoirement souscrire le formulaire n° 2065-SD par voie dématérialisée. Le non respect de cette obligation est sanctionné par l&#39;application de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 53, top: 1363, width: 1080, height: 15, 'font-size': 15, }}>
<span>
la majoration de 0,2 % prévue par l&#39;article 1738 du CGI. Vous trouverez toutes les informations utiles pour télédéclarer sur le site www.impots.gouv.fr.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1380, width: 753, height: 14, 'font-size': 15, }}>
<span>
S&#39;agissant des notices des liasses fiscales, elles sont accessibles uniquement sur le site www.impots.gouv.fr.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1578, width: 1081, height: 13, 'font-size': 13, }}>
<span>
* Pour les entreprises avec un exercice ouvert à compter du 1er janvier 2019 et clos en cours d&#39;année 2019, le taux normal d&#39;IS est de 31% (au lieu de 33 1/3 %). Dans ce cas précis, le taux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 52, top: 1592, width: 886, height: 13, 'font-size': 13, }}>
<span>
d&#39;impôt sur les sociétés appliqué doit être précisé en annexe libre de la liasse fiscale (cf. la rubrique « Nouveautés » de la notice du formulaire n° 2065-SD).
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 56, width: 41, height: 14, 'font-size': 17, }}>
<span>
cerfa
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <a className="pdf-annot pdf-link-annot obj_1 pdf-obj-fixed" href="http://www.impots.gouv.fr/"
               data-annot-id="29490368">
            </a>
            <a className="pdf-annot pdf-link-annot obj_2 pdf-obj-fixed" href="http://www.impots.gouv.fr/"
               data-annot-id="29490592">
            </a>
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="PH"
                   data-field-id="31021896" data-annot-id="29490816" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="PE"
                   data-field-id="31022200" data-annot-id="29978816" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="AA"
                   data-field-id="31302488" data-annot-id="29979008" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="AB"
                   data-field-id="31302664" data-annot-id="29979200" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="AH"
                   data-field-id="31340456" data-annot-id="29979392" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="AJ"
                   data-field-id="31022536" data-annot-id="29979584" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field "
                   name="PA"
                   data-field-id="31044504" data-annot-id="29979776" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="AP"
                   data-field-id="31299592" data-annot-id="29980112" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="AQ"
                   data-field-id="31022840" data-annot-id="29980304" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field "
                   name="HA"
                   data-field-id="31337640" data-annot-id="29980496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field "
                   name="LC"
                   data-field-id="31340056" data-annot-id="29980688" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field "
                   name="AE"
                   data-field-id="31334792" data-annot-id="29980880" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field "
                   name="AT"
                   data-field-id="31336216" data-annot-id="29981072" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "
                   name="AU"
                   data-field-id="31327640" data-annot-id="29981264" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="LW"
                   data-field-id="31329096" data-annot-id="29981456" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field "
                   name="DAG"
                   data-field-id="31325864" data-annot-id="29981920" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="LN"
                   data-field-id="31330520" data-annot-id="29982112" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field "
                   name="LT"
                   data-field-id="31333368" data-annot-id="29982304" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="LE"
                   data-field-id="31331944" data-annot-id="29982496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field "
                   name="BB"
                   data-field-id="31023176" data-annot-id="29982688" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field "
                   name="LV"
                   data-field-id="31026680" data-annot-id="29982880" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="LL"
                   data-field-id="31023512" data-annot-id="29983072" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="AF"
                   data-field-id="31023848" data-annot-id="29983264" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="HD"
                   data-field-id="31024184" data-annot-id="29983456" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="HJ"
                   data-field-id="31024520" data-annot-id="29983648" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="MA"
                   data-field-id="31025000" data-annot-id="29983840" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="LP"
                   data-field-id="31025336" data-annot-id="29984032" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="LQ"
                   data-field-id="31025672" data-annot-id="29984224" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="LY"
                   data-field-id="31026008" data-annot-id="29984416" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="AX"
                   data-field-id="31026344" data-annot-id="29984608" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="AY"
                   data-field-id="31124040" data-annot-id="29984800" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="PF"
                   data-field-id="31027016" data-annot-id="29981648" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="PG"
                   data-field-id="31027352" data-annot-id="29985520" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field "
                   name="BE"
                   data-field-id="31027832" data-annot-id="29985712" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field "
                   name="BF"
                   data-field-id="31028168" data-annot-id="29985904" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field "
                   name="JA"
                   data-field-id="31028504" data-annot-id="29986096" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field "
                   name="ACA"
                   data-field-id="31040488" data-annot-id="29986288" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field "
                   name="ACT"
                   data-field-id="31041816" data-annot-id="29986480" maxLength="14" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field "
                   name="ACD"
                   data-field-id="31043224" data-annot-id="29986672" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field "
                   name="ACF"
                   data-field-id="31029048" data-annot-id="29986864" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field "
                   name="ACG"
                   data-field-id="31030152" data-annot-id="29987056" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field "
                   name="ACH"
                   data-field-id="31031432" data-annot-id="29987248" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field "
                   name="ACI"
                   data-field-id="31032712" data-annot-id="29987440" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field "
                   name="AKF"
                   data-field-id="31035272" data-annot-id="29987632" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field "
                   name="AKG"
                   data-field-id="31036552" data-annot-id="29987824" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field "
                   name="AKH"
                   data-field-id="31037832" data-annot-id="29988016" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field "
                   name="AKI"
                   data-field-id="31039112" data-annot-id="29988208" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field "
                   name="AKD"
                   data-field-id="31033992" data-annot-id="29988400" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field "
                   name="PDF"
                   data-field-id="31049896" data-annot-id="29988592" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field "
                   name="PDG"
                   data-field-id="31048616" data-annot-id="29988784" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field "
                   name="PDH"
                   data-field-id="31047464" data-annot-id="29988976" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field "
                   name="AKI"
                   data-field-id="31039112" data-annot-id="29989168" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field "
                   name="PDA"
                   data-field-id="31046312" data-annot-id="29989360" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field "
                   name="PDT"
                   data-field-id="31050200" data-annot-id="29989552" maxLength="14" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field "
                   name="DEB_EX"
                   data-field-id="31311096" data-annot-id="29989744" type="text"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field "
                   name="Mél1"
                   data-field-id="31051304" data-annot-id="29989936" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="E"
                   data-field-id="31051640" data-annot-id="29990128" type="text"/>

            <button className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="SIGNATURE"
                    data-field-id="31311432" data-annot-id="29990320" type="button">


            </button>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field "
                   name="FIN_EX"
                   data-field-id="31303000" data-annot-id="29990512" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field "
                   name="Case à cocher4" data-field-id="31296600" data-annot-id="29990704" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field "
                   name="Case à cocher5" data-field-id="31299256" data-annot-id="29990896" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field "
                   name="EAD"
                   data-field-id="31303432" data-annot-id="29991088" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field "
                   name="EAF"
                   data-field-id="31304760" data-annot-id="29991280" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field "
                   name="EAG"
                   data-field-id="31306040" data-annot-id="29984992" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field "
                   name="EAI"
                   data-field-id="31307416" data-annot-id="29985184" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field "
                   name="CAD"
                   data-field-id="31311864" data-annot-id="29992512" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field "
                   name="CAF"
                   data-field-id="31313256" data-annot-id="29992704" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field "
                   name="CAG"
                   data-field-id="31314536" data-annot-id="29992896" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field "
                   name="CAH"
                   data-field-id="31315816" data-annot-id="29993088" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field "
                   name="CAI"
                   data-field-id="31317096" data-annot-id="29993280" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field "
                   name="EAZ"
                   data-field-id="31308744" data-annot-id="29993472" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field "
                   name="CAZ"
                   data-field-id="31310248" data-annot-id="29993664" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field "
                   name="REG_S"
                   data-field-id="31310424" data-annot-id="29993856" value="Yes" type="radio" data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field "
                   name="REG_R"
                   data-field-id="31310760" data-annot-id="29994048" value="Yes" checked type="radio"
                   data-default-value="Yes"/>

            <Field component="input" className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field "
                   name="CAA"
                   data-field-id="31318376" data-annot-id="29994240" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field "
                   name="EAA"
                   data-field-id="31318680" data-annot-id="29994432" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field "
                   name="EAH"
                   data-field-id="31319880" data-annot-id="29994624" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field "
                   name="DAI"
                   data-field-id="31322408" data-annot-id="29994816" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field "
                   name="DAH"
                   data-field-id="31322072" data-annot-id="29995008" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field "
                   name="DAG"
                   data-field-id="31325864" data-annot-id="29995200" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field "
                   name="DAF"
                   data-field-id="31321736" data-annot-id="29995392" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field "
                   name="DAD"
                   data-field-id="31321400" data-annot-id="29995584" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field "
                   name="DAA"
                   data-field-id="31320184" data-annot-id="29995776" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field "
                   name="AGA"
                   data-field-id="31323080" data-annot-id="29995968" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field "
                   name="ADA"
                   data-field-id="31322744" data-annot-id="29996160" type="text"/>

            <Field component={PdfNumberInput} maxLength="11"
                   className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field "
                   name="NUM_FISCALE_STE_GROUPE" data-field-id="31323416" data-annot-id="29996352"
                   type="text"/>

            <Field component={PdfNumberInput} maxLength="11"
                   className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field "
                   name="NUM_FISCALE_STE" data-field-id="31323752" data-annot-id="29996544"  type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field "
                   name="NUM_AGREMENT_CABINET" data-field-id="31324088" data-annot-id="29996896"
                   maxLength="6"
                   type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field "
                   name="FCT_SIGNATAIRE" data-field-id="31324424" data-annot-id="29997088" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field "
                   name="NAME_SIGNATAIRE" data-field-id="31324760" data-annot-id="29997280" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field "
                   name="DATE_SIGNATURE" data-field-id="31325432" data-annot-id="29997472" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field "
                   name="LIEU_SIGNATURE" data-field-id="31325096" data-annot-id="29997664" type="text"/>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2065;
