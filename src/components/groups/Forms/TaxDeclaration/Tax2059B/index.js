import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {sum, useCompute, setIsNothingness} from "helpers/pdfforms";

import './style.scss';
import { PdfCheckbox } from "../../../../reduxForm/Inputs";

const notNeededFields = {
  'NOM_STEA': true,
  'NB': true
};


const Tax2059B = (props) => {
  const { taxDeclarationForm, change } = props;
  setIsNothingness(taxDeclarationForm, notNeededFields, 'NB', change);


  useCompute('BC', sum, ['BA', 'BB', 'MA'], taxDeclarationForm, change);
  useCompute('DC', sum, ['DA', 'DB', 'MB'], taxDeclarationForm, change);
  useCompute('EC', sum, ['EA', 'EB', 'MC'], taxDeclarationForm, change);

  useCompute('BQ', sum, ['BD', 'BE', 'BF', 'BG', 'BH', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BP'], taxDeclarationForm, change);
  useCompute('CQ', sum, ['CD', 'CE', 'CF', 'CG', 'CH', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CP'], taxDeclarationForm, change);
  useCompute('DQ', sum, ['DD', 'DE', 'DF', 'DG', 'DH', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DP'], taxDeclarationForm, change);
  useCompute('EQ', sum, ['ED', 'EE', 'EF', 'EG', 'EH', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EP'], taxDeclarationForm, change);

  useCompute('HQ', sum, ['HN1', 'HN2', 'HN3', 'HN4', 'HN5', 'HN6', 'HN7', 'HN8', 'HN9', 'HN10', 'HN11', 'HN12', 'HN13', 'HN14'], taxDeclarationForm, change);
  useCompute('JQ', sum, ['JN1', 'JN2', 'JN3', 'JN4', 'JN5', 'JN6', 'JN7', 'JN8', 'JN9', 'JN10', 'JN11', 'JN12', 'JN13', 'JN14'], taxDeclarationForm, change);
  useCompute('KQ', sum, ['KN1', 'KN2', 'KN3', 'KN4', 'KN5', 'KN6', 'KN7', 'KN8', 'KN9', 'KN10', 'KN11', 'KN12', 'KN13', 'KN14'], taxDeclarationForm, change);
  useCompute('LQ', sum, ['LN1', 'LN2', 'LN3', 'LN4', 'LN5', 'LN6', 'LN7', 'LN8', 'LN9', 'LN10', 'LN11', 'LN12', 'LN13', 'LN14'], taxDeclarationForm, change);

  return (
    <div className="form-tax-declaration-2059b">

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 353, top: 98, width: 555, height: 21, 'font-size': 23, }}>
<span>
AFFECTATION DES PLUS-VALUES À COURT TERME
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 368, top: 125, width: 526, height: 17, 'font-size': 23, }}>
<span>
ET DES PLUS-VALUES DE FUSION OU D’APPORT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 134, width: 173, height: 12, 'font-size': 12, }}>
<span>
Formulaire obligatoire (article 53 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 112, top: 146, width: 143, height: 12, 'font-size': 12, }}>
<span>
du Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 936, top: 118, width: 221, height: 15, 'font-size': 20, }}>
<span>
DGFiP N° 2059-B-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 182, width: 183, height: 15, 'font-size': 16, }}>
<span>
Désignation de l’entreprise :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 413, top: 1583, width: 428, height: 12, 'font-size': 14, }}>
<span>
* Des explications concernant cette rubrique sont données dans la notice n° 2032
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 323, top: 220, width: 16, height: 17, 'font-size': 23, }}>
<span>
A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 363, top: 223, width: 567, height: 14, 'font-size': 15, }}>
<span>
ÉLÉMENTS ASSUJETTIS AU RÉGIME FISCAL DES PLUS-VALUES À COURT TERME
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 258, top: 241, width: 738, height: 15, 'font-size': 15, }}>
<span>
(à l’exclusion des plus-values de fusion dont l’imposition est prise en charge par les sociétés absorbantes) (cf. cadre B)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 278, top: 275, width: 49, height: 14, 'font-size': 16, }}>
<span>
Origine
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 309, width: 125, height: 14, 'font-size': 16, }}>
<span>
Imposition répartie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 343, width: 134, height: 12, 'font-size': 16, }}>
<span>
Plus-values réalisées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 377, width: 77, height: 12, 'font-size': 16, }}>
<span>
au cours de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 412, width: 62, height: 12, 'font-size': 16, }}>
<span>
l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 553, top: 279, width: 70, height: 10, 'font-size': 14, }}>
<span>
Montant net
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 544, top: 291, width: 88, height: 14, 'font-size': 14, }}>
<span>
des plus-values
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 562, top: 305, width: 53, height: 10, 'font-size': 14, }}>
<span>
réalisées*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 723, top: 279, width: 48, height: 10, 'font-size': 14, }}>
<span>
Montant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 703, top: 292, width: 88, height: 10, 'font-size': 14, }}>
<span>
antérieurement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 721, top: 305, width: 51, height: 13, 'font-size': 14, }}>
<span>
réintégré
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 860, top: 279, width: 97, height: 13, 'font-size': 14, }}>
<span>
Montant compris
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 865, top: 291, width: 88, height: 11, 'font-size': 14, }}>
<span>
dans le résultat
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 873, top: 305, width: 72, height: 10, 'font-size': 14, }}>
<span>
de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1050, top: 279, width: 48, height: 10, 'font-size': 14, }}>
<span>
Montant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1050, top: 292, width: 49, height: 10, 'font-size': 14, }}>
<span>
restant à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1046, top: 305, width: 56, height: 13, 'font-size': 14, }}>
<span>
réintégrer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 343, width: 184, height: 15, 'font-size': 16, }}>
<span>
sur 3 ans (entreprises à l’IR)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 378, width: 67, height: 11, 'font-size': 16, }}>
<span>
sur 10 ans
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 403, width: 211, height: 14, 'font-size': 16, }}>
<span>
sur une durée différente (art. 39
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 419, width: 228, height: 16, 'font-size': 16, }}>
<span>
quaterdecies 1 ter et 1 quater CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 447, width: 58, height: 11, 'font-size': 16, }}>
<span>
TOTAL 1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 289, top: 112, width: 22, height: 16, 'font-size': 22, }}>
<span>
13
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 481, width: 125, height: 15, 'font-size': 16, }}>
<span>
Imposition répartie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 516, width: 23, height: 11, 'font-size': 16, }}>
<span>
N-1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 551, width: 23, height: 11, 'font-size': 16, }}>
<span>
N-2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 585, width: 23, height: 11, 'font-size': 16, }}>
<span>
N-1
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 619, width: 23, height: 12, 'font-size': 16, }}>
<span>
N-2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 654, width: 23, height: 12, 'font-size': 16, }}>
<span>
N-3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 687, width: 23, height: 13, 'font-size': 16, }}>
<span>
N-4
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 722, width: 23, height: 13, 'font-size': 16, }}>
<span>
N-5
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 756, width: 23, height: 13, 'font-size': 16, }}>
<span>
N-6
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 792, width: 23, height: 11, 'font-size': 16, }}>
<span>
N-7
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 826, width: 23, height: 11, 'font-size': 16, }}>
<span>
N-8
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 861, width: 23, height: 12, 'font-size': 16, }}>
<span>
N-9
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 104, top: 653, width: 134, height: 12, 'font-size': 16, }}>
<span>
Plus-values réalisées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 129, top: 687, width: 84, height: 13, 'font-size': 16, }}>
<span>
au cours des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 722, width: 130, height: 12, 'font-size': 16, }}>
<span>
exercices antérieurs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 547, top: 474, width: 82, height: 11, 'font-size': 14, }}>
<span>
Montant net des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 510, top: 488, width: 156, height: 13, 'font-size': 14, }}>
<span>
plus-values réalisées à l’origine
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 706, top: 475, width: 81, height: 10, 'font-size': 14, }}>
<span>
Montant anté-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 688, top: 488, width: 118, height: 13, 'font-size': 14, }}>
<span>
rieurement réintégré
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 849, top: 475, width: 119, height: 13, 'font-size': 14, }}>
<span>
Montant rapporté au
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 849, top: 488, width: 119, height: 10, 'font-size': 14, }}>
<span>
résultat de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1029, top: 475, width: 90, height: 10, 'font-size': 14, }}>
<span>
Montant restant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1041, top: 488, width: 67, height: 13, 'font-size': 14, }}>
<span>
à réintégrer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 532, width: 131, height: 13, 'font-size': 16, }}>
<span>
sur 3 ans au titre de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 676, width: 187, height: 12, 'font-size': 16, }}>
<span>
Sur 10 ans ou sur une durée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 710, width: 194, height: 16, 'font-size': 16, }}>
<span>
différente (art. 39 quaterdecies
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 745, width: 164, height: 15, 'font-size': 16, }}>
<span>
1 ter et 1 quater du CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 779, width: 155, height: 16, 'font-size': 16, }}>
<span>
(à préciser) au titre de :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 895, width: 58, height: 11, 'font-size': 16, }}>
<span>
TOTAL 2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 446, top: 1550, width: 47, height: 11, 'font-size': 16, }}>
<span>
TOTAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 923, width: 14, height: 17, 'font-size': 23, }}>
<span>
B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 302, top: 925, width: 688, height: 15, 'font-size': 15, }}>
<span>
PLUS-VALUES RÉINTÉGRÉES DANS LES RÉSULTATS DES SOCIÉTÉS BÉNÉFICIAIRES DES APPORTS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 217, top: 943, width: 819, height: 14, 'font-size': 15, }}>
<span>
Cette rubrique ne comprend pas les plus-values afférentes aux biens non amortissables ou taxées lors des opérations de fusion ou d’apport.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 208, top: 1018, width: 189, height: 14, 'font-size': 14, }}>
<span>
Origine des plus-values et date
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 220, top: 1031, width: 164, height: 14, 'font-size': 14, }}>
<span>
des fusions ou des apports
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 539, top: 1010, width: 98, height: 11, 'font-size': 14, }}>
<span>
Montant net des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 520, top: 1024, width: 136, height: 13, 'font-size': 14, }}>
<span>
plus-values réalisées à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 563, top: 1037, width: 50, height: 14, 'font-size': 14, }}>
<span>
l’origine
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 704, top: 1018, width: 85, height: 11, 'font-size': 14, }}>
<span>
Montant anté-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 685, top: 1032, width: 124, height: 13, 'font-size': 14, }}>
<span>
rieurement réintégré
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 846, top: 1018, width: 125, height: 14, 'font-size': 14, }}>
<span>
Montant rapporté au
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 846, top: 1031, width: 125, height: 11, 'font-size': 14, }}>
<span>
résultat de l’exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1027, top: 1019, width: 94, height: 10, 'font-size': 14, }}>
<span>
Montant restant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1039, top: 1032, width: 70, height: 13, 'font-size': 14, }}>
<span>
à réintégrer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 177, top: 968, width: 288, height: 14, 'font-size': 14, }}>
<span>
Plus-values de fusion, d’apport partiel ou de scission
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 177, top: 982, width: 362, height: 14, 'font-size': 14, }}>
<span>
(personnes morales soumises à l’impôt sur les sociétés seulement)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 812, top: 968, width: 263, height: 14, 'font-size': 14, }}>
<span>
Plus-values d’apport à une société d’une activité
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 812, top: 982, width: 313, height: 14, 'font-size': 14, }}>
<span>
professionnelle exercée à titre individuel (toutes sociétés)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1049, top: 183, width: 38, height: 11, 'font-size': 16, }}>
<span>
Néant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1131, top: 183, width: 5, height: 6, 'font-size': 16, }}>
<span>
*
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 958, top: 181, width: 16, height: 13, 'font-size': 20, }}>
<span>
EU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 736, top: 180, width: 195, height: 13, 'font-size': 14, }}>
<span>
Formulaire déposé au titre de l’IR
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 1268, width: 11, height: 267, 'font-size': 12, }}>
<span>
N° 2059-B-SD – (SDNC-DGFiP) - Noevembre 2018
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="NB" data-field-id="29399672" data-annot-id="28105392" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="BA" data-field-id="29400344" data-annot-id="28931936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="DA" data-field-id="29400680" data-annot-id="28932160" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="EA" data-field-id="29400984" data-annot-id="28932352" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="BB" data-field-id="29401320" data-annot-id="28936208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="DB" data-field-id="29401656" data-annot-id="28936400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="EB" data-field-id="29401992" data-annot-id="28936592" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="MA" data-field-id="29402328" data-annot-id="28936784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="MB" data-field-id="29402664" data-annot-id="28936976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="MC" data-field-id="29403144" data-annot-id="28937168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="BC" data-field-id="29403480" data-annot-id="28937360" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="DC" data-field-id="29403816" data-annot-id="28937552" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="EC" data-field-id="29404152" data-annot-id="28937744" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="BD" data-field-id="29404488" data-annot-id="28937936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="CD" data-field-id="29404824" data-annot-id="28938128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="DD" data-field-id="29405160" data-annot-id="28938320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="ED" data-field-id="29405496" data-annot-id="28938512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="BE" data-field-id="29405976" data-annot-id="28938976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="CE" data-field-id="29406312" data-annot-id="28939168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="DE" data-field-id="29406648" data-annot-id="28939360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="EE" data-field-id="29406984" data-annot-id="28939552" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="BF" data-field-id="29407320" data-annot-id="28939744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="CF" data-field-id="29407656" data-annot-id="28939936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="DF" data-field-id="29407992" data-annot-id="28940128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="EF" data-field-id="29408328" data-annot-id="28940320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="BG" data-field-id="29408664" data-annot-id="28940512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="CG" data-field-id="29409000" data-annot-id="28940704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="DG" data-field-id="29409336" data-annot-id="28940896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="EG" data-field-id="29409672" data-annot-id="28941088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="BH" data-field-id="29410008" data-annot-id="28941280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="CH" data-field-id="29410344" data-annot-id="28941472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="DH" data-field-id="29410776" data-annot-id="28941664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="EH" data-field-id="29410776" data-annot-id="28941856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="BJ" data-field-id="29412184" data-annot-id="28938704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="CJ" data-field-id="29405704" data-annot-id="28942576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="DJ" data-field-id="29412856" data-annot-id="28942768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="EJ" data-field-id="29413192" data-annot-id="28942960" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="BK" data-field-id="29413528" data-annot-id="28943152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="CK" data-field-id="29413864" data-annot-id="28943344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="DK" data-field-id="29414200" data-annot-id="28943536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="EK" data-field-id="29414536" data-annot-id="28943728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="BL" data-field-id="29414872" data-annot-id="28943920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="CL" data-field-id="29415208" data-annot-id="28944112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="DL" data-field-id="29415544" data-annot-id="28944304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="EL" data-field-id="29415880" data-annot-id="28944496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="BM" data-field-id="29416216" data-annot-id="28944688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="CM" data-field-id="29416552" data-annot-id="28944880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="DM" data-field-id="29416888" data-annot-id="28945072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="EM" data-field-id="29417224" data-annot-id="28945264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="BN" data-field-id="29399416" data-annot-id="28945456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="CN" data-field-id="29400072" data-annot-id="28945648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="DN" data-field-id="29399128" data-annot-id="28945840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="EN" data-field-id="29417672" data-annot-id="28946032" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="BP" data-field-id="29418008" data-annot-id="28946224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="CP" data-field-id="29418344" data-annot-id="28946416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="DP" data-field-id="29418680" data-annot-id="28946608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="EP" data-field-id="29419016" data-annot-id="28946800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="BQ" data-field-id="29419352" data-annot-id="28946992" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="CQ" data-field-id="29419688" data-annot-id="28947184" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="DQ" data-field-id="29420024" data-annot-id="28947376" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="EQ" data-field-id="29420360" data-annot-id="28947568" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="NA" data-field-id="29420696" data-annot-id="28947760" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="GN1" data-field-id="29421032" data-annot-id="28947952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="GN2" data-field-id="29421368" data-annot-id="28948144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="GN4" data-field-id="29423384" data-annot-id="28948336" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="GN5" data-field-id="29423592" data-annot-id="28942048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="GN3" data-field-id="29421992" data-annot-id="28942240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="GN6" data-field-id="29412328" data-annot-id="28949568" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="GN7" data-field-id="29412664" data-annot-id="28949760" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="GN8" data-field-id="29425832" data-annot-id="28949952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="GN9" data-field-id="29426168" data-annot-id="28950144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="GN10" data-field-id="29426504" data-annot-id="28950336" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="GN11" data-field-id="29426840" data-annot-id="28950528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="GN12" data-field-id="29427176" data-annot-id="28950720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="GN13" data-field-id="29427512" data-annot-id="28950912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="GN14" data-field-id="29427848" data-annot-id="28951104" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="MN1" data-field-id="29428184" data-annot-id="28951296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="MN2" data-field-id="29428856" data-annot-id="28951488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="MN3" data-field-id="29428520" data-annot-id="28951680" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="MN4" data-field-id="29429192" data-annot-id="28951872" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="MN5" data-field-id="29429528" data-annot-id="28952064" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="MN6" data-field-id="29429864" data-annot-id="28952256" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="MN7" data-field-id="29430200" data-annot-id="28952448" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="MN8" data-field-id="29430536" data-annot-id="28952640" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="MN9" data-field-id="29430872" data-annot-id="28952832" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="MN10" data-field-id="29431208" data-annot-id="28953024" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="MN11" data-field-id="29431544" data-annot-id="28953216" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="MN12" data-field-id="29431880" data-annot-id="28953408" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="MN13" data-field-id="29432216" data-annot-id="28953600" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="MN14" data-field-id="29432552" data-annot-id="28953792" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field " name="HN1" data-field-id="29432888" data-annot-id="28953984" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="HN2" data-field-id="29433224" data-annot-id="28954176" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="HN3" data-field-id="29433560" data-annot-id="28954368" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="HN4" data-field-id="29433896" data-annot-id="28954560" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="HN5" data-field-id="29434232" data-annot-id="28954752" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="HN6" data-field-id="29434568" data-annot-id="28954944" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="HN7" data-field-id="29434904" data-annot-id="28955136" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="HN8" data-field-id="29435240" data-annot-id="28955328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field " name="HN9" data-field-id="29435576" data-annot-id="28955520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="HN10" data-field-id="29435912" data-annot-id="28955712" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field " name="HN11" data-field-id="29436248" data-annot-id="28955904" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field " name="HN12" data-field-id="29436584" data-annot-id="28956096" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field " name="HN13" data-field-id="29436920" data-annot-id="28956288" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field " name="HN14" data-field-id="29437256" data-annot-id="28956480" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field " name="JN1" data-field-id="29437592" data-annot-id="28956672" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field " name="JN2" data-field-id="29437928" data-annot-id="28956864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field " name="JN3" data-field-id="29438264" data-annot-id="28957056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field " name="JN4" data-field-id="29438600" data-annot-id="28957248" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field " name="JN5" data-field-id="29438936" data-annot-id="28957440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field " name="JN6" data-field-id="29439272" data-annot-id="28957632" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field " name="JN7" data-field-id="29439608" data-annot-id="28957824" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field " name="JN8" data-field-id="29439944" data-annot-id="28958016" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field " name="JN9" data-field-id="29440280" data-annot-id="28958208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field " name="JN10" data-field-id="29440616" data-annot-id="28958400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field " name="JN11" data-field-id="29440952" data-annot-id="28958592" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field " name="JN12" data-field-id="29441288" data-annot-id="28958784" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field " name="JN13" data-field-id="29441624" data-annot-id="28958976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field " name="JN14" data-field-id="29441960" data-annot-id="28959168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field " name="KN1" data-field-id="29442296" data-annot-id="28959360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field " name="KN2" data-field-id="29442632" data-annot-id="28959552" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field " name="KN3" data-field-id="29442968" data-annot-id="28959744" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field " name="KN4" data-field-id="29443304" data-annot-id="28959936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field " name="KN5" data-field-id="29443640" data-annot-id="28960128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field " name="KN6" data-field-id="29443976" data-annot-id="28960320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_125 pdf-obj-fixed acroform-field " name="KN7" data-field-id="29444312" data-annot-id="28960512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_126 pdf-obj-fixed acroform-field " name="KN8" data-field-id="29444648" data-annot-id="28960704" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_127 pdf-obj-fixed acroform-field " name="KN9" data-field-id="29444984" data-annot-id="28960896" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_128 pdf-obj-fixed acroform-field " name="KN10" data-field-id="29445320" data-annot-id="28961088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field " name="KN11" data-field-id="29445656" data-annot-id="28961280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_130 pdf-obj-fixed acroform-field " name="KN12" data-field-id="29445992" data-annot-id="28948528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_131 pdf-obj-fixed acroform-field " name="KN13" data-field-id="29424808" data-annot-id="28948720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_132 pdf-obj-fixed acroform-field " name="KN14" data-field-id="29425144" data-annot-id="28948912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_133 pdf-obj-fixed acroform-field " name="LN1" data-field-id="29425480" data-annot-id="28949104" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_134 pdf-obj-fixed acroform-field " name="LN2" data-field-id="29448392" data-annot-id="28949296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_135 pdf-obj-fixed acroform-field " name="LN3" data-field-id="29448728" data-annot-id="28963536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_136 pdf-obj-fixed acroform-field " name="LN4" data-field-id="29449064" data-annot-id="28963728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_137 pdf-obj-fixed acroform-field " name="LN5" data-field-id="29449400" data-annot-id="28963920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_138 pdf-obj-fixed acroform-field " name="LN6" data-field-id="29449736" data-annot-id="28964112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_139 pdf-obj-fixed acroform-field " name="LN7" data-field-id="29450072" data-annot-id="28964304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_140 pdf-obj-fixed acroform-field " name="LN8" data-field-id="29450408" data-annot-id="28964496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_141 pdf-obj-fixed acroform-field " name="LN9" data-field-id="29450744" data-annot-id="28964688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_142 pdf-obj-fixed acroform-field " name="LN10" data-field-id="29451080" data-annot-id="28964880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_143 pdf-obj-fixed acroform-field " name="LN11" data-field-id="29451416" data-annot-id="28965072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_144 pdf-obj-fixed acroform-field " name="LN12" data-field-id="29451752" data-annot-id="28965264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_145 pdf-obj-fixed acroform-field " name="LN13" data-field-id="29452088" data-annot-id="28965456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field " name="LN14" data-field-id="29452424" data-annot-id="28965648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_147 pdf-obj-fixed acroform-field " name="HQ" data-field-id="29452760" data-annot-id="28965840" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_148 pdf-obj-fixed acroform-field " name="JQ" data-field-id="29453096" data-annot-id="28966032" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_149 pdf-obj-fixed acroform-field " name="KQ" data-field-id="29453432" data-annot-id="28966224" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_150 pdf-obj-fixed acroform-field " name="LQ" data-field-id="29453768" data-annot-id="28966416" type="text" disabled />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_151 pdf-obj-fixed acroform-field " name="FA" data-field-id="29454104" data-annot-id="28966608" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_152 pdf-obj-fixed acroform-field " name="FB" data-field-id="29454440" data-annot-id="28966800" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component="input" className="pde-form-field pdf-annot obj_153 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="38730776" data-annot-id="38242704" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2059B;
