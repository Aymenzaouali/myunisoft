import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';

import './style.scss';

const Tax2035B = (props) => {
  const { change } = props;

  return (
    <div className="form-tax-declaration-2035g">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 492, top: 63, width: 265, height: 15, 'font-size': 20, }}>
<span>
FILIALES ET PARTICIPATIONS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1040, top: 103, width: 23, height: 20, 'font-size': 20, }}>
<span>
(1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 91, top: 108, width: 95, height: 14, 'font-size': 16, }}>
<span>
N° 15945 Ý 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 199, top: 64, width: 121, height: 11, 'font-size': 12, }}>
<span>
Formulaire obligatoire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 199, top: 77, width: 143, height: 11, 'font-size': 12, }}>
<span>
(article 40 A de l’annexe III
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 199, top: 90, width: 157, height: 12, 'font-size': 12, }}>
<span>
au Code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 188, width: 110, height: 17, 'font-size': 20, }}>
<span>
N° DE DÉPÔT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 428, top: 97, width: 446, height: 16, 'font-size': 16, }}>
<span>
(liste des personnes ou groupements de personnes de droit
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 355, top: 115, width: 544, height: 16, 'font-size': 16, }}>
<span>
ou de fait dont la société détient directement au moins 10 % du capital)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 248, width: 285, height: 17, 'font-size': 20, }}>
<span>
DÉNOMINATION DE L’ENTREPRISE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 90, top: 283, width: 132, height: 17, 'font-size': 20, }}>
<span>
ADRESSE (voie)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 91, top: 315, width: 119, height: 15, 'font-size': 20, }}>
<span>
CODE POSTAL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 350, width: 333, height: 14, 'font-size': 18, }}>
<span>
NOMBRE TOTAL DE FILIALES DETENUES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 464, top: 190, width: 157, height: 15, 'font-size': 20, }}>
<span>
EXERCICE CLOS LE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 516, top: 317, width: 47, height: 15, 'font-size': 20, }}>
<span>
VILLE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1043, top: 69, width: 127, height: 12, 'font-size': 15, }}>
<span>
N° 2035-G-SD 2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 831, top: 160, width: 246, height: 14, 'font-size': 14, }}>
<span>
Si ce formulaire est déposé sans informations,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 831, top: 176, width: 165, height: 10, 'font-size': 14, }}>
<span>
cocher la case néant ci-contre :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 998, top: 185, width: 102, height: 1, 'font-size': 8, }}>
<span>
. . . . . . . . . . . . . . . . . . . . . . . . . . .
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 777, top: 220, width: 73, height: 14, 'font-size': 18, }}>
<span>
N° SIRET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 90, top: 1568, width: 1000, height: 14, 'font-size': 14, }}>
<span>
(1) Lorsque le nombre de  liales et participations excède le nombre de lignes de l’imprimé, utiliser un ou plusieurs tableaux supplémentaires. Dans ce cas, il convient de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 90, top: 1583, width: 979, height: 14, 'font-size': 14, }}>
<span>
numéroter chaque tableau en haut et à gauche de la case prévue à cet effet et de porter le nombre total de tableaux souscrits en bas et à droite de cette même case.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 393, width: 114, height: 16, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 541, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 392, top: 393, width: 101, height: 13, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 393, top: 541, width: 101, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 460, width: 67, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 607, width: 67, height: 13, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 460, width: 16, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 607, width: 17, height: 13, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 490, width: 85, height: 16, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 228, top: 638, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 490, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 458, top: 638, width: 75, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 897, top: 490, width: 32, height: 16, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 898, top: 638, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 460, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 607, width: 31, height: 13, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 690, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 392, top: 689, width: 101, height: 13, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 756, width: 67, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 756, width: 16, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 786, width: 85, height: 16, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 786, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 897, top: 787, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 756, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 839, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 392, top: 839, width: 101, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 906, width: 67, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 906, width: 16, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 936, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 936, width: 75, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 897, top: 936, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 906, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 989, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 392, top: 988, width: 101, height: 13, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 1055, width: 67, height: 13, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 1055, width: 16, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 1086, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 1086, width: 75, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 897, top: 1086, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 1055, width: 31, height: 13, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1140, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 392, top: 1139, width: 101, height: 13, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 1205, width: 67, height: 12, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 1205, width: 16, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 1237, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 1237, width: 75, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 897, top: 1237, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 1205, width: 31, height: 12, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1285, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 392, top: 1285, width: 101, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 1351, width: 67, height: 13, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 1351, width: 16, height: 13, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 1382, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 1382, width: 75, height: 13, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 897, top: 1382, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 1351, width: 31, height: 13, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 109, top: 1434, width: 114, height: 15, 'font-size': 16, }}>
<span>
Forme juridique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 392, top: 1434, width: 101, height: 12, 'font-size': 16, }}>
<span>
Dénomination
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1468, width: 276, height: 14, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 105, top: 1499, width: 67, height: 13, 'font-size': 16, }}>
<span>
Adresse :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 229, top: 1499, width: 16, height: 12, 'font-size': 16, }}>
<span>
N°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 609, top: 1468, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 1531, width: 85, height: 15, 'font-size': 16, }}>
<span>
Code postal
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 457, top: 1531, width: 75, height: 12, 'font-size': 16, }}>
<span>
Commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 897, top: 1531, width: 32, height: 15, 'font-size': 16, }}>
<span>
Pays
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 1499, width: 31, height: 13, 'font-size': 16, }}>
<span>
Voie
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1316, width: 276, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 609, top: 1316, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1171, width: 276, height: 14, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 609, top: 1171, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 1023, width: 276, height: 14, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 609, top: 1023, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 873, width: 276, height: 14, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 609, top: 873, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 723, width: 276, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 609, top: 724, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 575, width: 276, height: 14, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 609, top: 575, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 106, top: 427, width: 276, height: 15, 'font-size': 16, }}>
<span>
N° SIREN (si société établie en France)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 609, top: 428, width: 108, height: 13, 'font-size': 16, }}>
<span>
% de détention
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 70, top: 1492, width: 9, height: 68, 'font-size': 11, }}>
<span>
N° 2035-G-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 1316, width: 12, height: 170, 'font-size': 12, }}>
<span>
– (SDNC-DGFiP) – Octobre 2017
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="GAF6" data-field-id="34316328" data-annot-id="33534352" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="GAI6" data-field-id="34316632" data-annot-id="33416544" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="GAG6" data-field-id="34316968" data-annot-id="33416768" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="GAJ6" data-field-id="34317272" data-annot-id="33416960" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="GR6" data-field-id="34317608" data-annot-id="33814928" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="GAT6" data-field-id="34317944" data-annot-id="33417152" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="GAA6" data-field-id="34318280" data-annot-id="33818848" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="GAK6" data-field-id="34318616" data-annot-id="33819040" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="GAH5" data-field-id="34318952" data-annot-id="33819232" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="GAD5" data-field-id="34319432" data-annot-id="33819424" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="GAE5" data-field-id="34319768" data-annot-id="33819616" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="GAF5" data-field-id="34320104" data-annot-id="33819808" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="GAI5" data-field-id="34320440" data-annot-id="33820000" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="GAG5" data-field-id="34320776" data-annot-id="33820192" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="GAJ5" data-field-id="34321112" data-annot-id="33820384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="GR5" data-field-id="34321448" data-annot-id="33820576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="GAT5" data-field-id="34321784" data-annot-id="33820768" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="GAA5" data-field-id="34322264" data-annot-id="33821232" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="GAK5" data-field-id="34322600" data-annot-id="33821424" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="GAH4" data-field-id="34322936" data-annot-id="33821616" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="GAD4" data-field-id="34323272" data-annot-id="33821808" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="GAE4" data-field-id="34323608" data-annot-id="33822000" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="GAF4" data-field-id="34323944" data-annot-id="33822192" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="GAI4" data-field-id="34324280" data-annot-id="33822384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="GAG4" data-field-id="34324616" data-annot-id="33822576" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="GAJ4" data-field-id="34324952" data-annot-id="33822768" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="GR4" data-field-id="34325288" data-annot-id="33822960" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GAT4" data-field-id="34325624" data-annot-id="33823152" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="GAA4" data-field-id="34325960" data-annot-id="33823344" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="GAK4" data-field-id="34326296" data-annot-id="33823536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="GAH3" data-field-id="34326632" data-annot-id="33823728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="GAD3" data-field-id="34326968" data-annot-id="33823920" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="GR2" data-field-id="34327304" data-annot-id="33824112" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="GAT2" data-field-id="34322120" data-annot-id="33820960" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="GAA2" data-field-id="34328232" data-annot-id="33824832" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="GAK2" data-field-id="34328568" data-annot-id="33825024" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GAJ1" data-field-id="34328904" data-annot-id="33825216" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="GR1" data-field-id="34329240" data-annot-id="33825408" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="GAG1" data-field-id="34329576" data-annot-id="33825600" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="GAA1" data-field-id="34329912" data-annot-id="33825792" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="GAI1" data-field-id="34330248" data-annot-id="33825984" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="GAF1" data-field-id="34330584" data-annot-id="33826176" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="GAH1" data-field-id="34330920" data-annot-id="33826368" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="GAE1" data-field-id="34331256" data-annot-id="33826560" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="GAD1" data-field-id="34331592" data-annot-id="33826752" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="GAT1" data-field-id="34331928" data-annot-id="33826944" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="GAK1" data-field-id="34332264" data-annot-id="33827136" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="GAJ2" data-field-id="34332600" data-annot-id="33827328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="GAG2" data-field-id="34332936" data-annot-id="33827520" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="GAI2" data-field-id="34333272" data-annot-id="33827712" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="GAF2" data-field-id="34333608" data-annot-id="33827904" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="GAE2" data-field-id="34333944" data-annot-id="33828096" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="GAD2" data-field-id="34334280" data-annot-id="33828288" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="GAH2" data-field-id="34334616" data-annot-id="33828480" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="GAK3" data-field-id="34334952" data-annot-id="33828672" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="GAA3" data-field-id="34335288" data-annot-id="33828864" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="GAT3" data-field-id="34335624" data-annot-id="33829056" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="GR3" data-field-id="34335960" data-annot-id="33829248" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="GAJ3" data-field-id="34336296" data-annot-id="33829440" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="GAG3" data-field-id="34336632" data-annot-id="33829632" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="GAI3" data-field-id="34336968" data-annot-id="33829824" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="GAF3" data-field-id="34337304" data-annot-id="33830016" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="GAE3" data-field-id="34337640" data-annot-id="33830208" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="GAE6" data-field-id="34337976" data-annot-id="33830400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="GAD6" data-field-id="34338312" data-annot-id="33830592" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="GAH6" data-field-id="34315880" data-annot-id="33824304" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="GAK7" data-field-id="34327640" data-annot-id="33824496" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="GAA7" data-field-id="34339560" data-annot-id="33831824" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="GAT7" data-field-id="34339832" data-annot-id="33832016" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="GR7" data-field-id="34340168" data-annot-id="33832208" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="GAJ7" data-field-id="34340504" data-annot-id="33832400" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="GAG7" data-field-id="34340840" data-annot-id="33832592" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="GAI7" data-field-id="34341176" data-annot-id="33832784" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="GAF7" data-field-id="34341512" data-annot-id="33832976" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="GAE7" data-field-id="34341848" data-annot-id="33833168" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="GAD7" data-field-id="34342184" data-annot-id="33833360" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="GAH7" data-field-id="34342520" data-annot-id="33833552" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="GAK8" data-field-id="34342856" data-annot-id="33833744" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="GAA8" data-field-id="34343192" data-annot-id="33833936" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="GAT8" data-field-id="34343528" data-annot-id="33834128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="GR8" data-field-id="34343864" data-annot-id="33834320" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="GAJ8" data-field-id="34344200" data-annot-id="33834512" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="GAG8" data-field-id="34344536" data-annot-id="33834704" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="GAI8" data-field-id="34344872" data-annot-id="33834896" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="GAF8" data-field-id="34345208" data-annot-id="33835088" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="GAE8" data-field-id="34345544" data-annot-id="33835280" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="GAD8" data-field-id="34345880" data-annot-id="33835472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="GAH8" data-field-id="34346216" data-annot-id="33835664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="GT" data-field-id="34346552" data-annot-id="33835856" type="text" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="GS" data-field-id="34346888" data-annot-id="33836048" value="Yes" type="checkbox" data-default-value="Off" />

            <Field component="input" className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field pde-form-field-text" name="NOM_STEA" data-field-id="16603704" data-annot-id="16089424" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field pde-form-field-text" name="SIRET_STET" data-field-id="16603368" data-annot-id="16089616" type="text" disabled />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="FIN_EX" data-field-id="16603032" data-annot-id="16089808" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STED" data-field-id="16604040" data-annot-id="16090000" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEF" data-field-id="16604376" data-annot-id="16090192" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field pde-form-field-text" name="ADR_STEG" data-field-id="16604712" data-annot-id="16090384" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field pde-form-field-text" name="CP_STEH" data-field-id="16605048" data-annot-id="16090576" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field pde-form-field-text" name="VILLE_STEI" data-field-id="16605384" data-annot-id="16090768" type="text" disabled />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Tax2035B;
