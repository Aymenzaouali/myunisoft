/* eslint-disable */
import React from 'react';
import { Field } from "redux-form";
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import { PdfInputWithProcentChar, PdfInputValuesWithSpaces } from 'components/reduxForm/Inputs';

import "./style.scss";

const Tax2573 = ({ change }) => {

  const YEAR_OF_LAWSUIT = "4";
  const REASON_FOR_ADDITION = "3";

  return (
    <div className="form-refund">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 117, top: 86, width: 460, height: 15, 'font-size': 19, }}>
                              <span>
                                DIRECTION GENERALE DES FINANCES PUBLIQUES
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 983, top: 83, width: 94, height: 18, 'font-size': 24, }}>
                              <span>
                                2573-SD
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 118, top: 122, width: 312, height: 19, 'font-size': 24, }}>
                              <span>
                                IMPOT SUR LES SOCIETES
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 163, width: 296, height: 15, 'font-size': 19, }}>
                              <span>
                                ET CONTRIBUTIONS ASSIMILEES
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 118, top: 196, width: 646, height: 19, 'font-size': 24, }}>
                              <span>
                                DEMANDE DE REMBOURSEMENT DE CREDITS D&#39;IMPOT
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 212, top: 267, width: 142, height: 15, 'font-size': 19, }}>
                              <span>
                                DENOMINATION
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 210, top: 311, width: 69, height: 14, 'font-size': 19, }}>
                              <span>
                                Adresse
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 537, width: 48, height: 13, 'font-size': 16, }}>
                              <span>
                                SIREN
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 580, top: 537, width: 396, height: 16, 'font-size': 16, }}>
                              <span>
                                Société bénéficiant du régime de l&#39;intégration fiscale
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 601, width: 313, height: 12, 'font-size': 15, }}>
                              <span>
                                I – Demande de remboursement de créances
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 840, top: 676, width: 254, height: 14, 'font-size': 14, }}>
                              <span>
                                Demande d&#39;imputation sur échéance future
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 911, top: 753, width: 114, height: 11, 'font-size': 14, }}>
                              <span>
                                Date de l’échéance
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1038, top: 754, width: 80, height: 9, 'font-size': 11, }}>
                              <span>
                                Montant à utiliser
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 798, width: 5, height: 11, 'font-size': 14, }}>
                              <span>
                                1
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 824, width: 8, height: 11, 'font-size': 14, }}>
                              <span>
                                2
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 850, width: 7, height: 11, 'font-size': 14, }}>
                              <span>
                                3
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 875, width: 8, height: 11, 'font-size': 14, }}>
                              <span>
                                4
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 901, width: 7, height: 11, 'font-size': 14, }}>
                              <span>
                                5
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 927, width: 7, height: 10, 'font-size': 14, }}>
                              <span>
                                6
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 953, width: 7, height: 10, 'font-size': 14, }}>
                              <span>
                                7
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 978, width: 7, height: 11, 'font-size': 14, }}>
                              <span>
                                8
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 1004, width: 7, height: 10, 'font-size': 14, }}>
                              <span>
                                9
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 122, top: 1029, width: 15, height: 11, 'font-size': 14, }}>
                              <span>
                                10
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1189, width: 946, height: 15, 'font-size': 15, }}>
                              <span>
                                II – Cession de créances lors de l’entrée dans un groupe de sociétés (article 223 A du CGI) créance née du report en arrière des déficits
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 208, top: 1221, width: 822, height: 14, 'font-size': 14, }}>
                              <span>
                                A remplir par la société membre du groupe et par la société mère et à déposer auprès du service des impôts dont elles dépendent
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 199, top: 1240, width: 209, height: 13, 'font-size': 14, }}>
                              <span>
                                SIREN de la société tête de groupe
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 377, top: 1301, width: 264, height: 13, 'font-size': 14, }}>
                              <span>
                                SIREN de la société à l&#39;origine de la créance
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 1361, width: 5, height: 10, 'font-size': 14, }}>
                              <span>
                                1
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 1378, width: 8, height: 11, 'font-size': 14, }}>
                              <span>
                                2
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 1395, width: 7, height: 11, 'font-size': 14, }}>
                              <span>
                                3
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 1412, width: 8, height: 11, 'font-size': 14, }}>
                              <span>
                                4
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 1429, width: 7, height: 11, 'font-size': 14, }}>
                              <span>
                                5
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 993, top: 165, width: 76, height: 11, 'font-size': 14, }}>
                              <span>
                                N° 12486*11
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 212, top: 373, width: 351, height: 18, 'font-size': 19, }}>
                              <span>
                                Nom et adresse personnelle de l&#39;exploitant
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 212, top: 394, width: 282, height: 18, 'font-size': 19, }}>
                              <span>
                                (pour les entreprises individuelles)
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 704, width: 57, height: 9, 'font-size': 11, }}>
                              <span>
                                Nature de la
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 158, top: 716, width: 38, height: 9, 'font-size': 11, }}>
                              <span>
                                créance
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 698, width: 31, height: 9, 'font-size': 11, }}>
                              <span>
                                Année
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 213, top: 710, width: 54, height: 11, 'font-size': 11, }}>
                              <span>
                                d&#39;origine de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 216, top: 722, width: 49, height: 9, 'font-size': 11, }}>
                              <span>
                                la créance
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 293, top: 680, width: 53, height: 9, 'font-size': 11, }}>
                              <span>
                                Montant de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 289, top: 692, width: 59, height: 11, 'font-size': 11, }}>
                              <span>
                                l’intérêt légal
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 277, top: 704, width: 85, height: 11, 'font-size': 11, }}>
                              <span>
                                (uniquement dans
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 278, top: 716, width: 83, height: 11, 'font-size': 11, }}>
                              <span>
                                le cadre du report
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 289, top: 728, width: 60, height: 8, 'font-size': 11, }}>
                              <span>
                                en arrière de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 303, top: 739, width: 32, height: 11, 'font-size': 11, }}>
                              <span>
                                déficit)
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 698, width: 52, height: 9, 'font-size': 11, }}>
                              <span>
                                Montant du
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 377, top: 710, width: 75, height: 9, 'font-size': 11, }}>
                              <span>
                                remboursement
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 392, top: 722, width: 44, height: 9, 'font-size': 11, }}>
                              <span>
                                demandé
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 470, top: 654, width: 203, height: 13, 'font-size': 14, }}>
                              <span>
                                A compléter exclusivement en cas
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 485, top: 669, width: 174, height: 11, 'font-size': 14, }}>
                              <span>
                                de demande formulée suite à
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 492, top: 684, width: 161, height: 13, 'font-size': 14, }}>
                              <span>
                                l&#39;ouverture d&#39;une procédure
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 545, top: 699, width: 54, height: 11, 'font-size': 14, }}>
                              <span>
                                collective
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 716, top: 649, width: 61, height: 11, 'font-size': 14, }}>
                              <span>
                                Motif de la
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 715, top: 664, width: 62, height: 11, 'font-size': 14, }}>
                              <span>
                                demande :
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 697, top: 679, width: 97, height: 13, 'font-size': 14, }}>
                              <span>
                                jeune entreprise
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 700, top: 694, width: 92, height: 13, 'font-size': 14, }}>
                              <span>
                                innovante (JEI),
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 707, top: 709, width: 79, height: 11, 'font-size': 14, }}>
                              <span>
                                PME au sens
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 700, top: 724, width: 92, height: 10, 'font-size': 14, }}>
                              <span>
                                communautaire
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 726, top: 739, width: 40, height: 13, 'font-size': 14, }}>
                              <span>
                                (PME),
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 690, top: 754, width: 112, height: 13, 'font-size': 14, }}>
                              <span>
                                entreprise nouvelle
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 728, top: 769, width: 37, height: 13, 'font-size': 14, }}>
                              <span>
                                (ENN)
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 481, top: 748, width: 57, height: 9, 'font-size': 11, }}>
                              <span>
                                Nature de la
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 485, top: 760, width: 48, height: 11, 'font-size': 11, }}>
                              <span>
                                procédure
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 568, top: 748, width: 104, height: 9, 'font-size': 11, }}>
                              <span>
                                Date d&#39;ouverture de la
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 596, top: 760, width: 48, height: 11, 'font-size': 11, }}>
                              <span>
                                procédure
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 831, top: 747, width: 52, height: 12, 'font-size': 12, }}>
                              <span>
                                Typologie
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 817, top: 760, width: 80, height: 12, 'font-size': 12, }}>
                              <span>
                                d&#39;impôt ou taxe
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 1264, width: 109, height: 10, 'font-size': 14, }}>
                              <span>
                                Date de clôture de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 172, top: 1279, width: 73, height: 10, 'font-size': 14, }}>
                              <span>
                                l&#39;exercice de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 158, top: 1293, width: 100, height: 11, 'font-size': 14, }}>
                              <span>
                                détermination du
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 148, top: 1308, width: 120, height: 13, 'font-size': 14, }}>
                              <span>
                                crédit, de la créance
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 1324, width: 106, height: 10, 'font-size': 14, }}>
                              <span>
                                ou de la réduction
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 187, top: 1338, width: 43, height: 14, 'font-size': 14, }}>
                              <span>
                                d&#39;impôt
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 296, top: 1286, width: 46, height: 11, 'font-size': 14, }}>
                              <span>
                                Date de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 280, top: 1301, width: 78, height: 11, 'font-size': 14, }}>
                              <span>
                                cession de la
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 295, top: 1316, width: 48, height: 11, 'font-size': 14, }}>
                              <span>
                                créance
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 675, top: 1294, width: 80, height: 10, 'font-size': 14, }}>
                              <span>
                                Montant de la
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 692, top: 1308, width: 45, height: 11, 'font-size': 14, }}>
                              <span>
                                cession
<br/>
                              </span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" disabled className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field "
                   name="NOM_STEA" data-field-id="27472744" data-annot-id="26993248" type="text" autoComplete="nope"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field "
                   name="ADR_STED" data-field-id="27475832" data-annot-id="26993440" type="text" autoComplete="nope"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field "
                   name="ADR_STEF" data-field-id="27476168" data-annot-id="26993632" type="text" autoComplete="nope"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field "
                   name="ADR_STEG" data-field-id="27477176" data-annot-id="26993824" type="text" autoComplete="nope"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field "
                   name="CP_STEH" data-field-id="27476504" data-annot-id="26994016" type="text" autoComplete="nope"/>

            <Field component="input" disabled className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field "
                   name="VILLE_STEI" data-field-id="27476840" data-annot-id="26994208" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces} disabled
                   className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="SIREN_STET"
                   data-field-id="27477512" data-annot-id="26994400" max={9} letterSpacing="1.9em" type="number"
                   />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="AA"
                   data-field-id="27477848" data-annot-id="26994592" value="Yes" type="checkbox"
                   data-default-value="Off" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="AB1"
                   data-field-id="27478280" data-annot-id="26994784" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="AC1"
                   data-field-id="27480936" data-annot-id="26994976" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field "
                   name="AF1" data-field-id="27369512" data-annot-id="26995168" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field "
                   name="AG1" data-field-id="27372872" data-annot-id="26995360" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field "
                   name="AH1" data-field-id="27376488" data-annot-id="26995552" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field "
                   name="AI1" data-field-id="27379848" data-annot-id="26995744" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field upper-case"
                   name="KA1" data-field-id="27383208" data-annot-id="26995936" maxLength={REASON_FOR_ADDITION}
                   type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "
                   name="KB1" data-field-id="27374728" data-annot-id="26996128" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="KC1" data-field-id="27389624" data-annot-id="26996320" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field "
                   name="KD1" data-field-id="27392984" data-annot-id="26996784" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="AB2" data-field-id="27362456" data-annot-id="26996976" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="AC2"
                   data-field-id="27366344" data-annot-id="26997168" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field "
                   name="AF2" data-field-id="27370184" data-annot-id="26997360" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field "
                   name="AG2" data-field-id="27373208" data-annot-id="26997552" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field "
                   name="AH2" data-field-id="27376824" data-annot-id="26997744" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field "
                   name="AI2" data-field-id="27380184" data-annot-id="26997936" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field upper-case"
                   name="KA2" data-field-id="27383544" data-annot-id="26998128" maxLength={REASON_FOR_ADDITION}
                   type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field "
                   name="KB2" data-field-id="27386600" data-annot-id="26998320" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field "
                   name="KC2" data-field-id="27389960" data-annot-id="26998512" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field "
                   name="KD2" data-field-id="27393320" data-annot-id="26998704" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field "
                   name="AB3" data-field-id="27363544" data-annot-id="26998896" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="AC3"
                   data-field-id="27366680" data-annot-id="26999088" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field "
                   name="AF3" data-field-id="27369848" data-annot-id="26999280" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field "
                   name="AG3" data-field-id="27373880" data-annot-id="26999472" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field "
                   name="AH3" data-field-id="27377160" data-annot-id="26999664" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field "
                   name="AI3" data-field-id="27380520" data-annot-id="26996512" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field upper-case"
                   name="KA3" data-field-id="27383880" data-annot-id="27000384" maxLength={REASON_FOR_ADDITION}
                   type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field "
                   name="KB3" data-field-id="27386936" data-annot-id="27000576" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field "
                   name="KC3" data-field-id="27390296" data-annot-id="27000768" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field "
                   name="KD3" data-field-id="27393656" data-annot-id="27000960" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field "
                   name="AB4" data-field-id="27363880" data-annot-id="27001152" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="AC4"
                   data-field-id="27367016" data-annot-id="27001344" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field "
                   name="AF4" data-field-id="27370520" data-annot-id="27001536" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field "
                   name="AG4" data-field-id="27373544" data-annot-id="27001728" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field "
                   name="AH4" data-field-id="27377496" data-annot-id="27001920" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field "
                   name="AI4" data-field-id="27380856" data-annot-id="27002112" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field upper-case"
                   name="KA4" data-field-id="27384216" data-annot-id="27002304" maxLength={REASON_FOR_ADDITION}
                   type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field "
                   name="KB4" data-field-id="27387272" data-annot-id="27002496" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field "
                   name="KC4" data-field-id="27390632" data-annot-id="27002688" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field "
                   name="KD4" data-field-id="27393992" data-annot-id="27002880" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field "
                   name="AB5" data-field-id="27364184" data-annot-id="27003072" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="AC5"
                   data-field-id="27367352" data-annot-id="27003264" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field "
                   name="AF5" data-field-id="27370856" data-annot-id="27003456" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field "
                   name="AG5" data-field-id="27374216" data-annot-id="27003648" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field "
                   name="AH5" data-field-id="27377832" data-annot-id="27003840" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field "
                   name="AI5" data-field-id="27381192" data-annot-id="27004032" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field upper-case"
                   name="KA5" data-field-id="27384552" data-annot-id="27004224" maxLength={REASON_FOR_ADDITION}
                   type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field "
                   name="KB5" data-field-id="27387608" data-annot-id="27004416" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field "
                   name="KC5" data-field-id="27390968" data-annot-id="27004608" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field "
                   name="KD5" data-field-id="27394328" data-annot-id="27004800" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field "
                   name="AB6" data-field-id="27364520" data-annot-id="27004992" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="AC6"
                   data-field-id="27367688" data-annot-id="27005184" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field "
                   name="AF6" data-field-id="27371528" data-annot-id="27005376" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field "
                   name="AG6" data-field-id="27369032" data-annot-id="27005568" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field "
                   name="AH6" data-field-id="27378168" data-annot-id="27005760" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field "
                   name="AI6" data-field-id="27381528" data-annot-id="27005952" type="text"/>

            <Field component="input"
                   className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field  upper-case" name="KA6"
                   data-field-id="27384888" data-annot-id="27006144" maxLength={REASON_FOR_ADDITION} type="text"
                   autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field "
                   name="KB6" data-field-id="27387944" data-annot-id="26999856" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field "
                   name="KC6" data-field-id="27391304" data-annot-id="27000048" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field "
                   name="KD6" data-field-id="27394664" data-annot-id="27007376" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field "
                   name="AB7" data-field-id="27364856" data-annot-id="27007568" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="AC7"
                   data-field-id="27368024" data-annot-id="27007760" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field "
                   name="AF7" data-field-id="27371192" data-annot-id="27007952" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field "
                   name="AG7" data-field-id="27375144" data-annot-id="27008144" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field "
                   name="AH7" data-field-id="27378504" data-annot-id="27008336" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field "
                   name="AI7" data-field-id="27381864" data-annot-id="27008528" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field upper-case"
                   name="KA7" data-field-id="27385224" data-annot-id="27008720" maxLength={REASON_FOR_ADDITION}
                   type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field "
                   name="KB7" data-field-id="27388280" data-annot-id="27008912" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field "
                   name="KC7" data-field-id="27391640" data-annot-id="27009104" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field "
                   name="KD7" data-field-id="27395000" data-annot-id="27009296" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field "
                   name="AB8" data-field-id="27365192" data-annot-id="27009488" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="AC8"
                   data-field-id="27368360" data-annot-id="27009680" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field "
                   name="AF8" data-field-id="27371864" data-annot-id="27009872" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field "
                   name="AG8" data-field-id="27375480" data-annot-id="27010064" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field "
                   name="AH8" data-field-id="27378840" data-annot-id="27010256" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field "
                   name="AI8" data-field-id="27382200" data-annot-id="27010448" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field upper-case"
                   name="KA8" data-field-id="27362312" data-annot-id="27010640" maxLength={REASON_FOR_ADDITION}
                   type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field "
                   name="KB8" data-field-id="27388616" data-annot-id="27010832" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field "
                   name="KC8" data-field-id="27391976" data-annot-id="27011024" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field "
                   name="KD8" data-field-id="27395336" data-annot-id="27011216" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field "
                   name="AB9" data-field-id="27365528" data-annot-id="27011408" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="AC9"
                   data-field-id="27368696" data-annot-id="27011600" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field "
                   name="AF9" data-field-id="27372200" data-annot-id="27011792" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field "
                   name="AG9" data-field-id="27375816" data-annot-id="27011984" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field "
                   name="AH9" data-field-id="27379176" data-annot-id="27012176" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field "
                   name="AI9" data-field-id="27382536" data-annot-id="27012368" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field upper-case"
                   name="KA9" data-field-id="27362600" data-annot-id="27012560" maxLength={REASON_FOR_ADDITION}
                   type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field "
                   name="KB9" data-field-id="27388952" data-annot-id="27012752" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field "
                   name="KC9" data-field-id="27392312" data-annot-id="27012944" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field "
                   name="KD9" data-field-id="27395672" data-annot-id="27013136" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field "
                   name="AB10" data-field-id="27365864" data-annot-id="27013328" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="AC10"
                   data-field-id="27369176" data-annot-id="27013520" max={YEAR_OF_LAWSUIT} letterSpacing="0.1em"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field "
                   name="AF10" data-field-id="27372536" data-annot-id="27013712" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field "
                   name="AG10" data-field-id="27376152" data-annot-id="27013904" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field "
                   name="AH10" data-field-id="27379512" data-annot-id="27014096" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field "
                   name="AI10" data-field-id="27382872" data-annot-id="27014288" type="text"/>

            <Field component="input"
                   className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field upper-case" name="KA10"
                   data-field-id="27374424" data-annot-id="27014480" maxLength={REASON_FOR_ADDITION} type="text"
                   autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field "
                   name="KB10" data-field-id="27389288" data-annot-id="27014672" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field "
                   name="KC10" data-field-id="27392648" data-annot-id="27014864" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field "
                   name="KD10" data-field-id="27396008" data-annot-id="27015056" type="text" autoComplete="nope"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field " name="BA"
                   data-field-id="27481976" data-annot-id="27015248" max={9} letterSpacing="24px" type="number"
                   />

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field "
                   name="BB1" data-field-id="27396344" data-annot-id="27015440" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field "
                   name="BC1" data-field-id="27397016" data-annot-id="27015632" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field " name="BD1"
                   data-field-id="27397352" data-annot-id="27015824" max={9} letterSpacing="24px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field "
                   name="BE1" data-field-id="27397688" data-annot-id="27016016" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field "
                   name="BB2" data-field-id="27396680" data-annot-id="27016208" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field "
                   name="BC2" data-field-id="27398360" data-annot-id="27016400" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field " name="BD2"
                   data-field-id="27398696" data-annot-id="27016592" max={9} letterSpacing="24px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field "
                   name="BE2" data-field-id="27399032" data-annot-id="27016784" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field "
                   name="BB3" data-field-id="27398024" data-annot-id="27016976" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field "
                   name="BC3" data-field-id="27399368" data-annot-id="27017168" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field " name="BD3"
                   data-field-id="27399704" data-annot-id="27017360" max={9} letterSpacing="24px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field "
                   name="BF3" data-field-id="27400040" data-annot-id="27017552" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field "
                   name="BB4" data-field-id="27400376" data-annot-id="27017744" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field "
                   name="BC4" data-field-id="27400712" data-annot-id="27017936" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field " name="BD4"
                   data-field-id="27401048" data-annot-id="27018128" max={9} letterSpacing="24px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_125 pdf-obj-fixed acroform-field "
                   name="BF4" data-field-id="27401384" data-annot-id="27018320" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_126 pdf-obj-fixed acroform-field "
                   name="BB5" data-field-id="27401720" data-annot-id="27018512" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_127 pdf-obj-fixed acroform-field "
                   name="BC5" data-field-id="27402056" data-annot-id="27018704" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_128 pdf-obj-fixed acroform-field " name="BD5"
                   data-field-id="27402408" data-annot-id="27018896" max={9} letterSpacing="24px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field "
                   name="BF5" data-field-id="27402744" data-annot-id="27019088" type="text" autoComplete="nope"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 117, top: 64, width: 542, height: 14, 'font-size': 15, }}>
                              <span>
                                III – Transfert de créances à la société absorbante ou bénéficiaire d&#39;un apport
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 98, width: 955, height: 12, 'font-size': 12, }}>
                              <span>
                                A remplir par la société absorbante ou bénéficiaire d&#39;un apport et par la société absorbée ou apporteuse et à déposer auprès du service des impôts dont elles dépendent
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 174, top: 116, width: 354, height: 13, 'font-size': 14, }}>
                              <span>
                                SIREN de la société absorbante ou bénéficiaire des apports
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 472, top: 196, width: 398, height: 14, 'font-size': 14, }}>
                              <span>
                                SIREN de la société à l&#39;origine de la créance Montant du transfert
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 275, width: 5, height: 10, 'font-size': 14, }}>
                              <span>
                                1
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 292, width: 8, height: 11, 'font-size': 14, }}>
                              <span>
                                2
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 309, width: 7, height: 11, 'font-size': 14, }}>
                              <span>
                                3
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 326, width: 8, height: 11, 'font-size': 14, }}>
                              <span>
                                4
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 343, width: 7, height: 11, 'font-size': 14, }}>
                              <span>
                                5
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 496, width: 559, height: 16, 'font-size': 16, }}>
                              <span>
                                IV – Reversement de crédit d’impôt (pour les entreprises passibles de l’IS)
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 692, width: 5, height: 11, 'font-size': 14, }}>
                              <span>
                                1
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 709, width: 8, height: 11, 'font-size': 14, }}>
                              <span>
                                2
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 727, width: 7, height: 10, 'font-size': 14, }}>
                              <span>
                                3
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 744, width: 8, height: 10, 'font-size': 14, }}>
                              <span>
                                4
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 761, width: 7, height: 10, 'font-size': 14, }}>
                              <span>
                                5
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 116, top: 920, width: 317, height: 15, 'font-size': 19, }}>
                              <span>
                                V – Cadre réservé à l&#39;Administration
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 463, top: 981, width: 162, height: 13, 'font-size': 14, }}>
                              <span>
                                Cachet du poste comptable
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 117, top: 1061, width: 150, height: 19, 'font-size': 19, }}>
                              <span>
                                Date et signature
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 114, top: 1106, width: 10, height: 10, 'font-size': 14, }}>
                              <span>
                                A
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 337, top: 1106, width: 15, height: 10, 'font-size': 14, }}>
                              <span>
                                LE
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 115, top: 1123, width: 77, height: 11, 'font-size': 14, }}>
                              <span>
                                SIGNATURE
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 181, width: 115, height: 13, 'font-size': 14, }}>
                              <span>
                                nature du crédit, de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 196, width: 303, height: 12, 'font-size': 14, }}>
                              <span>
                                la créance ou de la du crédit, de la transfert de la
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 211, width: 108, height: 13, 'font-size': 14, }}>
                              <span>
                                réduction d&#39;impôt :
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 275, top: 151, width: 88, height: 11, 'font-size': 14, }}>
                              <span>
                                date de clôture
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 273, top: 166, width: 92, height: 11, 'font-size': 14, }}>
                              <span>
                                de l&#39;exercice de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 278, top: 181, width: 81, height: 11, 'font-size': 14, }}>
                              <span>
                                détermination
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 277, top: 211, width: 84, height: 11, 'font-size': 14, }}>
                              <span>
                                créance ou de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 226, width: 68, height: 11, 'font-size': 14, }}>
                              <span>
                                la réduction
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 298, top: 241, width: 43, height: 13, 'font-size': 14, }}>
                              <span>
                                d&#39;impôt
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 391, top: 181, width: 46, height: 11, 'font-size': 14, }}>
                              <span>
                                Date du
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 211, width: 48, height: 11, 'font-size': 14, }}>
                              <span>
                                créance
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 596, width: 115, height: 13, 'font-size': 14, }}>
                              <span>
                                nature du crédit, de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 611, width: 303, height: 13, 'font-size': 14, }}>
                              <span>
                                la créance ou de la du crédit, de la transfert de la
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 626, width: 108, height: 13, 'font-size': 14, }}>
                              <span>
                                réduction d&#39;impôt :
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 275, top: 566, width: 88, height: 11, 'font-size': 14, }}>
                              <span>
                                date de clôture
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 273, top: 581, width: 92, height: 11, 'font-size': 14, }}>
                              <span>
                                de l&#39;exercice de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 278, top: 596, width: 81, height: 11, 'font-size': 14, }}>
                              <span>
                                détermination
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 277, top: 626, width: 84, height: 11, 'font-size': 14, }}>
                              <span>
                                créance ou de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 641, width: 68, height: 11, 'font-size': 14, }}>
                              <span>
                                la réduction
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 298, top: 656, width: 43, height: 13, 'font-size': 14, }}>
                              <span>
                                d&#39;impôt
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 391, top: 596, width: 46, height: 11, 'font-size': 14, }}>
                              <span>
                                Date du
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 390, top: 626, width: 48, height: 11, 'font-size': 14, }}>
                              <span>
                                créance
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 476, top: 604, width: 66, height: 11, 'font-size': 14, }}>
                              <span>
                                Montant du
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 472, top: 619, width: 74, height: 10, 'font-size': 14, }}>
                              <span>
                                reversement
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 604, width: 56, height: 11, 'font-size': 14, }}>
                              <span>
                                Intérêt de
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 587, top: 619, width: 34, height: 10, 'font-size': 14, }}>
                              <span>
                                retard
<br/>
                              </span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 115, top: 981, width: 107, height: 11, 'font-size': 14, }}>
                              <span>
                                Numéro MEDOC :
<br/>
                              </span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_131 pdf-obj-fixed acroform-field " name="CA"
                   data-field-id="27571512" data-annot-id="27092560" max={9} letterSpacing="21px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_132 pdf-obj-fixed acroform-field "
                   name="CB1" data-field-id="27403368" data-annot-id="27093712" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_133 pdf-obj-fixed acroform-field "
                   name="CC1" data-field-id="27406312" data-annot-id="27093904" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_134 pdf-obj-fixed acroform-field "
                   name="CD1" data-field-id="27409016" data-annot-id="25896752" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_135 pdf-obj-fixed acroform-field " name="CE1"
                   data-field-id="27411784" data-annot-id="25896944" max={9} letterSpacing="21px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_136 pdf-obj-fixed acroform-field "
                   name="CF1" data-field-id="27414488" data-annot-id="27095984" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_137 pdf-obj-fixed acroform-field "
                   name="CB2" data-field-id="27417784" data-annot-id="27096176" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_138 pdf-obj-fixed acroform-field "
                   name="CC2" data-field-id="27420552" data-annot-id="27096368" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_139 pdf-obj-fixed acroform-field "
                   name="CD2" data-field-id="27423256" data-annot-id="27096560" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_140 pdf-obj-fixed acroform-field " name="CE2"
                   data-field-id="27426024" data-annot-id="27096752" type="text" max={9} letterSpacing="21px"
                   type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_141 pdf-obj-fixed acroform-field "
                   name="CF2" data-field-id="27428728" data-annot-id="27096944" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_142 pdf-obj-fixed acroform-field "
                   name="CB3" data-field-id="27432024" data-annot-id="27097136" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_143 pdf-obj-fixed acroform-field "
                   name="CC3" data-field-id="27385432" data-annot-id="27097328" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_144 pdf-obj-fixed acroform-field "
                   name="CD3" data-field-id="27438520" data-annot-id="27097520" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_145 pdf-obj-fixed acroform-field " name="CE3"
                   data-field-id="27441288" data-annot-id="27097712" max={9} letterSpacing="21px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field "
                   name="CF3" data-field-id="27443992" data-annot-id="27097904" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_147 pdf-obj-fixed acroform-field "
                   name="CB4" data-field-id="27447288" data-annot-id="27098096" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_148 pdf-obj-fixed acroform-field "
                   name="CC4" data-field-id="27450056" data-annot-id="27098560" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_149 pdf-obj-fixed acroform-field "
                   name="CD4" data-field-id="27452760" data-annot-id="27098752" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_150 pdf-obj-fixed acroform-field " name="CE4"
                   data-field-id="27455528" data-annot-id="27098944" max={9} letterSpacing="21px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_151 pdf-obj-fixed acroform-field "
                   name="CF4" data-field-id="27458232" data-annot-id="27099136" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_152 pdf-obj-fixed acroform-field "
                   name="CB5" data-field-id="27461528" data-annot-id="27099328" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_153 pdf-obj-fixed acroform-field "
                   name="CC5" data-field-id="27464296" data-annot-id="27099520" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_154 pdf-obj-fixed acroform-field "
                   name="CD5" data-field-id="27467000" data-annot-id="27099712" type="text"/>

            <Field component={PdfInputValuesWithSpaces}
                   className="pde-form-field pdf-annot obj_155 pdf-obj-fixed acroform-field " name="CE5"
                   data-field-id="27469768" data-annot-id="27099904" max={9} letterSpacing="21px" type="number"
                   />

            <Field component="input" className="pde-form-field pdf-annot obj_156 pdf-obj-fixed acroform-field "
                   name="CF5" data-field-id="27472472" data-annot-id="27100096" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_157 pdf-obj-fixed acroform-field "
                   name="DB1" data-field-id="27492184" data-annot-id="27100288" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_158 pdf-obj-fixed acroform-field "
                   name="DC1" data-field-id="27495288" data-annot-id="27100480" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_159 pdf-obj-fixed acroform-field "
                   name="DD1" data-field-id="27499032" data-annot-id="27100672" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_160 pdf-obj-fixed acroform-field "
                   name="DE1" data-field-id="27502744" data-annot-id="27100864" type="text" autoComplete="nope"/>

            <Field component={PdfInputWithProcentChar}
                   className="pde-form-field pdf-annot obj_161 pdf-obj-fixed acroform-field " name="DF1"
                   data-field-id="27506712" data-annot-id="27101056" type="number" />

            <Field component="input" className="pde-form-field pdf-annot obj_162 pdf-obj-fixed acroform-field "
                   name="DB2" data-field-id="27492440" data-annot-id="27101248" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_163 pdf-obj-fixed acroform-field "
                   name="DB3" data-field-id="27510680" data-annot-id="27101440" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_164 pdf-obj-fixed acroform-field "
                   name="DB4" data-field-id="27513688" data-annot-id="27098288" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_165 pdf-obj-fixed acroform-field "
                   name="DB5" data-field-id="27516712" data-annot-id="27102160" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_166 pdf-obj-fixed acroform-field "
                   name="DC2" data-field-id="27519736" data-annot-id="27102352" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_167 pdf-obj-fixed acroform-field "
                   name="DC3" data-field-id="27522760" data-annot-id="27102544" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_168 pdf-obj-fixed acroform-field "
                   name="DC4" data-field-id="27525784" data-annot-id="27102736" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_169 pdf-obj-fixed acroform-field "
                   name="DC5" data-field-id="27528808" data-annot-id="27102928" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_170 pdf-obj-fixed acroform-field "
                   name="DD2" data-field-id="27540872" data-annot-id="27103120" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_171 pdf-obj-fixed acroform-field "
                   name="DD3" data-field-id="27537880" data-annot-id="27103312" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_172 pdf-obj-fixed acroform-field "
                   name="DD4" data-field-id="27534856" data-annot-id="27103504" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_173 pdf-obj-fixed acroform-field "
                   name="DD5" data-field-id="27531832" data-annot-id="27103696" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_174 pdf-obj-fixed acroform-field "
                   name="DE2" data-field-id="27543944" data-annot-id="27103888" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_175 pdf-obj-fixed acroform-field "
                   name="DE3" data-field-id="27547192" data-annot-id="27104080" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_176 pdf-obj-fixed acroform-field "
                   name="DE4" data-field-id="27550520" data-annot-id="27104272" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_177 pdf-obj-fixed acroform-field "
                   name="DE5" data-field-id="27553768" data-annot-id="27104464" type="text" autoComplete="nope"/>

            <Field component={PdfInputWithProcentChar}
                   className="pde-form-field pdf-annot obj_178 pdf-obj-fixed acroform-field " name="DF2"
                   data-field-id="27557096" data-annot-id="27104656" type="text" />

            <Field component={PdfInputWithProcentChar}
                   className="pde-form-field pdf-annot obj_179 pdf-obj-fixed acroform-field " name="DF3"
                   data-field-id="27560344" data-annot-id="27104848" type="text" />

            <Field component={PdfInputWithProcentChar}
                   className="pde-form-field pdf-annot obj_180 pdf-obj-fixed acroform-field " name="DF4"
                   data-field-id="27563672" data-annot-id="27105040" type="text" />

            <Field component={PdfInputWithProcentChar}
                   className="pde-form-field pdf-annot obj_181 pdf-obj-fixed acroform-field " name="DF5"
                   data-field-id="27566920" data-annot-id="27105232" type="text" />

          </div>
        </div>
      </div>
    </div>
  );
}


export default Tax2573;
