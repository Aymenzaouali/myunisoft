import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  FormSection
} from 'redux-form';
import { withStyles, Typography } from '@material-ui/core';
import I18n from 'assets/I18n';
import { usePrevious } from 'helpers/hooks';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import SubsidiariesList from 'containers/groups/Tables/SubsidiariesList';
import { MoralPersonAutoComplete } from 'containers/reduxForm/Inputs';
import { SubsidiaryDeleteDialog } from 'containers/groups/Dialogs';
import ReduxRadio from 'components/reduxForm/Selections/ReduxRadio';
import _get from 'lodash/get';
import Info from 'components/basics/Info';
import classNames from 'classnames';
import styles from './Subsidiaries.module.scss';

const Subsidiaries = (props) => {
  const {
    loadData,
    classes,
    nbSubsidiariesRef,
    isEditingSubsidiary,
    nbSelectSubsidiary,
    tableNameSubsidiary,
    updateSubsidiary,
    startEditSubsidiary,
    cancelEditSubsidiary,
    societyId,
    errorCapital,
    errorPart,
    list,
    onfilterChangeSociety,
    csvData,
    disabledExport,
    baseFileName
  } = props;
  const prevSocietyId = usePrevious(societyId);
  const [isDeleteOpenSociety, setIsDeleteOpenSociety] = useState(false);
  useEffect(() => {
    if (prevSocietyId !== societyId) {
      loadData();
    }
  }, [societyId]);
  const sRef = useRef(null);

  const onSubsidiaryChange = (value) => {
    const { addSubsidiarySelect } = props;
    addSubsidiarySelect(value.value);
  };

  const cancel = async () => {
    const {
      getMoralPersonList
    } = props;

    await cancelEditSubsidiary();
    await getMoralPersonList(societyId, {}, tableNameSubsidiary);
  };
  const deleteSubsidiarySociety = () => {
    setIsDeleteOpenSociety(true);
  };

  const closeDeleteSubsidiarySociety = () => {
    setIsDeleteOpenSociety(false);
  };
  let button = [
    {
      _type: 'icon',
      iconName: !isEditingSubsidiary ? 'icon-pencil' : 'icon-save',
      iconSize: 28,
      onClick: () => {
        if (!isEditingSubsidiary) {
          startEditSubsidiary();
          setTimeout(() => {
            if (_get(sRef, 'current')) sRef.current.focus();
          });
        } else {
          updateSubsidiary(tableNameSubsidiary);
        }
      }
    },
    {
      _type: 'icon',
      iconName: 'icon-trash',
      iconSize: 28,
      color: 'error',
      titleInfoBulle: I18n.t('tooltips.delete'),
      disabled: (nbSelectSubsidiary === 0 || isEditingSubsidiary),
      onClick: () => deleteSubsidiarySociety()
    },
    {
      _type: 'component',
      disabled: disabledExport,
      color: 'primary',
      component: <CSVButton csvData={csvData} baseFileName={baseFileName} />
    }
  ];

  if (isEditingSubsidiary) {
    button = [{
      _type: 'string',
      text: 'Annuler',
      color: 'default',
      variant: 'outlined',
      onClick: () => cancel('person')
    },
    ...button
    ];
  }
  return (
    <FormSection name="subsidiaries">
      <div className={styles.subsidiaries}>
        <Typography classes={{ root: classes.titleSubsidiaries }}>{`${nbSubsidiariesRef} ${I18n.t('companyCreation.subsidiaries.subsidiariesRef')}`}</Typography>
        <div className={classNames(styles.row, styles.container)}>
          <Field
            name="Addsubsidiarie"
            id="Addsubsidiarie"
            component={MoralPersonAutoComplete}
            label={I18n.t('companyCreation.subsidiaries.Addsubsidiarie')}
            className={classes.selectSubsidiarie}
            onChangeValues={onSubsidiaryChange}
            margin="none"
          />
          <div>
            {errorCapital && <Info message={I18n.t('companyCreation.errors.no_social_part_society')} />}
            {errorPart && <Info message={I18n.t('companyCreation.errors.social_part_pp')} />}
          </div>

          <div>
            <InlineButton buttons={button} />
          </div>
        </div>
        <div className={styles.filter}>
          <Typography classes={{ root: classes.display }}>{I18n.t('companyCreation.associates.associatesFilter.display')}</Typography>
          <Field
            name="filterSociety"
            list={list}
            row
            component={ReduxRadio}
            onChange={onfilterChangeSociety}
          />
        </div>
        <div className={styles.table}>
          <SubsidiariesList inputRef={sRef} />
        </div>
        {isDeleteOpenSociety && (
          <SubsidiaryDeleteDialog
            isOpen={isDeleteOpenSociety}
            onClose={closeDeleteSubsidiarySociety}
          />
        )}
      </div>
    </FormSection>
  );
};

const themeStyle = () => ({
  titleSubsidiaries: {
    width: 241,
    height: 17,
    fontWeight: 500,
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    margin: '30px 0px',
    color: 'black',
    fontSize: 14,
    fontFamily: 'basier_circlemedium'
  },
  display: {
    alignSelf: 'center'
  },
  selectSubsidiarie: {
    width: 280
  }
});

Subsidiaries.propTypes = {
  loadData: PropTypes.func.isRequired,
  classes: PropTypes.shape({}).isRequired,
  nbSubsidiariesRef: PropTypes.number.isRequired,
  addSubsidiarySelect: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired,
  isEditingSubsidiary: PropTypes.bool.isRequired,
  nbSelectSubsidiary: PropTypes.number.isRequired,
  tableNameSubsidiary: PropTypes.string.isRequired,
  updateSubsidiary: PropTypes.func.isRequired,
  startEditSubsidiary: PropTypes.func.isRequired,
  cancelEditSubsidiary: PropTypes.func.isRequired,
  getMoralPersonList: PropTypes.func.isRequired,
  errorCapital: PropTypes.bool.isRequired,
  disabledExport: PropTypes.bool.isRequired,
  csvData: PropTypes.array.isRequired,
  baseFileName: PropTypes.string.isRequired,
  errorPart: PropTypes.bool.isRequired,
  list: PropTypes.arrayOf({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
  }).isRequired,
  onfilterChangeSociety: PropTypes.func.isRequired
};

export default withStyles(themeStyle)(Subsidiaries);
