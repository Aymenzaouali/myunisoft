/* eslint-disable */
import React from "react";
import {Field} from "redux-form";
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {parse as p, useCompute} from "helpers/pdfforms";
import {ReduxPicker} from "components/reduxForm/Inputs";
import { RibAutoComplete } from 'containers/reduxForm/Inputs';
import moment from 'moment';
import lodash from 'lodash';

import "react-datepicker/dist/react-datepicker.css";
import './style.scss';


const T_IDENTIF = (props) => {
  const { change, tvaForm, formCA3 } = props;
  const KE = lodash.get(formCA3, 'KE');

  useCompute('AAT', ({AAT}) => p(AAT).toString().length <= 14 ? AAT : p(AAT).toString().slice(0, -1), ['AAT'], tvaForm, change);
  useCompute('KA', ({GAE, GAC, KD, CA}) => GAE && GAC ? `${KD}-${moment(CA).format('MMYYYY')}-3310CA3` : '', ['GAE', 'GAC', 'KD', 'CA'], tvaForm, change);
  useCompute('KB', ({GBE, GBC, KD, CA}) => GBE && GBC ? `${KD}-${moment(CA).format('MMYYYY')}-3310CA3` : '', ['GBE', 'GBC', 'KD', 'CA'], tvaForm, change);
  useCompute('KC', ({GCE, GCC, KD, CA}) => GCE && GCC ? `${KD}-${moment(CA).format('MMYYYY')}-3310CA3` : '', ['GCE', 'GCC', 'KD', 'CA'], tvaForm, change);
  useCompute('HA', ({GAE, GAC, HA}) => GAE && GAC ? p(HA) ? p(HA) : KE : '', ['GAE', 'GAC', 'HA'], tvaForm, change);

  return (
    <div className="
    t_identif">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 422, top: 67, width: 310, height: 17, 'font-size': 23, }}>
<span>
DONNEES D&#39;IDENTIFICATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1049, top: 68, width: 97, height: 15, 'font-size': 21, }}>
<span>
T-IDENTIF
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 151, width: 337, height: 15, 'font-size': 21, }}>
<span>
IDENTIFICATION DU REDEVABLE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 203, top: 210, width: 229, height: 19, 'font-size': 21, }}>
<span>
DÈsignation du redevable :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 490, top: 494, width: 221, height: 15, 'font-size': 21, }}>
<span>
CESSION/CESSATION :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 194, top: 556, width: 226, height: 15, 'font-size': 21, }}>
<span>
Date de cession / cessation :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 191, top: 636, width: 264, height: 19, 'font-size': 21, }}>
<span>
Date de redressement judiciaire :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 455, top: 720, width: 292, height: 14, 'font-size': 21, }}>
<span>
PERIODE DE DECLARATION :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 802, width: 219, height: 19, 'font-size': 21, }}>
<span>
DÈbut pÈriode dÈclaration :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 860, width: 196, height: 19, 'font-size': 21, }}>
<span>
Fin pÈriode dÈclaration :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 1394, width: 148, height: 19, 'font-size': 21, }}>
<span>
Compte bancaire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 1444, width: 88, height: 19, 'font-size': 21, }}>
<span>
1er compte
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1477, width: 94, height: 19, 'font-size': 21, }}>
<span>
2nd compte
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 1511, width: 107, height: 19, 'font-size': 21, }}>
<span>
3Ëme compte
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 699, top: 210, width: 211, height: 15, 'font-size': 21, }}>
<span>
NumÈro SIRET / SIREN :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 699, top: 249, width: 231, height: 20, 'font-size': 21, }}>
<span>
RÈfÈrence Obligation fiscale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 700, top: 272, width: 52, height: 19, 'font-size': 21, }}>
<span>
(ROF)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 591, width: 25, height: 15, 'font-size': 21, }}>
<span>
Ou
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 748, top: 798, width: 182, height: 23, 'font-size': 21, }}>
<span>
…chÈance (pour 3514):
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 357, top: 935, width: 490, height: 15, 'font-size': 21, }}>
<span>
MODALITES DE DECLARATION ET DE PAIEMENT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 998, width: 861, height: 19, 'font-size': 21, }}>
<span>
DÈclaration : A compter du 1er janvier 2002, les dÈclarations doivent obligatoirement Ítre Ètablies en euro.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1021, width: 828, height: 19, 'font-size': 21, }}>
<span>
Attention : ne portez pas de centimes d&#39;euro (cf. rËgles d&#39;arrondi dans le cahier des charges EDI-TVA).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1066, width: 838, height: 20, 'font-size': 21, }}>
<span>
Paiement : A compter du 1er janvier 2002, les paiements doivent obligatoirement Ítre effectuÈs en euro.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 541, top: 1130, width: 119, height: 15, 'font-size': 21, }}>
<span>
PAIEMENT :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 506, top: 1180, width: 190, height: 19, 'font-size': 21, }}>
<span>
TÈlÈrËglement SEPA :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1263, width: 978, height: 19, 'font-size': 21, }}>
<span>
Si vous avez choisi de payer par tÈlÈrËglement A, veuillez indiquer le montant du (des) prÈlËvement(s), l&#39;identification du
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1285, width: 417, height: 20, 'font-size': 21, }}>
<span>
compte bancaire ainsi que la rÈfÈrence du paiement.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1308, width: 790, height: 19, 'font-size': 21, }}>
<span>
NOTA : A compter du 1er janvier 2002, les prÈlËvements sont obligatoirement effectuÈs en euros.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 368, top: 1378, width: 86, height: 15, 'font-size': 21, }}>
<span>
RÈfÈrence
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 617, top: 1394, width: 187, height: 19, 'font-size': 21, }}>
<span>
Montant prÈlËvement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 1394, width: 171, height: 19, 'font-size': 21, }}>
<span>
RÈfÈrence paiement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 1411, width: 161, height: 19, 'font-size': 21, }}>
<span>
BIC/IBAN (SEPA)
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="AAA" data-field-id="16426344" data-annot-id="16181760" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="AAC" data-field-id="16426536" data-annot-id="15726096" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="AAG" data-field-id="16426744" data-annot-id="15939312" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="AAD" data-field-id="16427016" data-annot-id="15939504" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="AAH" data-field-id="16427352" data-annot-id="15939696" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="AAI" data-field-id="16427688" data-annot-id="15939888" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="AAT" max="14" data-field-id="16428024" data-annot-id="16190000" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="KD" data-field-id="16428360" data-annot-id="16190192" type="text" disabled/>

            <Field component={ReduxPicker} handleChange={change} isClearable className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="BB" data-field-id="16428696" data-annot-id="16190384" type="text"  />

            <Field component={ReduxPicker} handleChange={change} isClearable className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="KE" data-field-id="16429176" data-annot-id="16190576" type="text"  />

            <Field component={ReduxPicker}  handleChange={change} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="CA" data-field-id="16429512" data-annot-id="16190768" type="text" disabled/>

            <Field component={ReduxPicker}  handleChange={change} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="CB" data-field-id="16429848" data-annot-id="16190960" type="text" disabled/>

            <Field component={ReduxPicker} handleChange={change} isClearable className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="KF" data-field-id="16430184" data-annot-id="16191152" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="GAC" data-field-id="16432680" data-annot-id="16191344" type="text"  />

            <Field component="input" className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="GBC" data-field-id="16433688" data-annot-id="16191536" type="text"  />

            <Field component="input" className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="GCC" data-field-id="16434024" data-annot-id="16191728" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="HA" data-field-id="16430520" data-annot-id="16191920" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="HB" data-field-id="16430856" data-annot-id="16192384" type="text"  />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="HC" data-field-id="16431192" data-annot-id="16192576" type="text"  />

            <Field component="input" className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="KA" data-field-id="16431528" data-annot-id="16192768" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="KB" data-field-id="16432008" data-annot-id="16192960" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="KC" data-field-id="16432344" data-annot-id="16193152" type="text" disabled />

            <Field component={RibAutoComplete} onChangeValues={(value) => { change('GAC', value.bic) }} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field blackSpan" name="GAE" />

            <Field component={RibAutoComplete} onChangeValues={(value) => { change('GBC', value.bic) }} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field blackSpan" name="GBE" />

            <Field component={RibAutoComplete} onChangeValues={(value) => { change('GCC', value.bic) }} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field blackSpan" name="GCE" />

          </div>
        </div>
      </div>
    </div>
  );
};

export default T_IDENTIF;
