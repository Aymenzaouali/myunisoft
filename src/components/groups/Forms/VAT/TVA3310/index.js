import React from 'react';
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import {parse as p, sum, useCompute} from "helpers/pdfforms";
import _get from 'lodash/get';

import './style.scss';

const calcCB = (values, fields) => p(values[fields[0]]) * 0.021;
const calcCE = (values, fields) => p(values[fields[0]]) * 0.009;
const calcCF = (values, fields) => p(values[fields[0]]) * 0.021;
const calcCS = (values, fields) => p(values[fields[0]]) * 0.10;
const calcCK = (values, fields) => p(values[fields[0]]) * 0.13;
const calcCP = (values, fields) => p(values[fields[0]]) * 0.0105;
const calcCQ = (values, fields) => p(values[fields[0]]) * 0.0175;
const calcFA = (values, fields) => p(values[fields[0]]) * 0.01;
const calcFB = (values, fields) => p(values[fields[0]]) * 0.05;
const calcFJ = (values, fields) => p(values[fields[0]]) * 7.32 / 1000;
const calcKJ = (values, fields) => p(values[fields[0]]) * 0.02;
const calcKL = (values, fields) => p(values[fields[0]]) * 0.1;
const calcPP = (values, fields) => p(values[fields[0]]) * 0.02;
const calcPR = (values, fields) => p(values[fields[0]]) * 0.1;
const calcFT = (values, fields) => p(values[fields[0]]) * 14.89;
const calcKG = (values, fields) => p(values[fields[0]]) * 0.013;
const calcFV = (values, fields) => p(values[fields[0]]) * 4.57;
const calcHI = (values, fields) => p(values[fields[0]]) * 0.0075;
const calcMY = (values, fields) => p(values[fields[0]]) * 0.004;
const calcNB = (values, fields) => p(values[fields[0]]) * 0.006;
const calcJH = (values, fields) => p(values[fields[0]]) * 0.00141;
const calcJN = (values, fields) => p(values[fields[0]]) * 0.33;
const calcJP = (values, fields) => p(values[fields[0]]) * 0.000642;
const calcKZ = (values, fields) => p(values[fields[0]]) * 0.5;
const calcLA = (values, fields) => p(values[fields[0]]) * 125;
const calcLF = (values, fields) => p(values[fields[0]]) * 0.11;
const calcLE = (values, fields) => p(values[fields[0]]) * 0.06;
const calcLG = (values, fields) => p(values[fields[0]]) * 0.005;
const calcLH = (values, fields) => p(values[fields[0]]) * 0.005;
const calcLK = (values, fields) => p(values[fields[0]]) * 0.053;
const calcLL = (values, fields) => p(values[fields[0]]) * 0.018;
const calcLM = (values, fields) => p(values[fields[0]]) * 0.12;
const calcPD = (values, fields) => p(values[fields[0]]) * 0.03;
const calcNQ = (values, fields) => p(values[fields[0]]) * 0.056;
const calcPB = (values, fields) => p(values[fields[0]]) * 0.095;
const calcPC = (values, fields) => p(values[fields[0]]) * 0.12;
const calcMA = (values, fields) => p(values[fields[0]]) * 0.000363;

const TVA3310 = (props) => {
  const { change, tvaForm, form, errors, setError, validateForm } = props;
  useCompute('CN', sum, ['CA', 'CB', 'CC', 'AF', 'CE', 'CF', 'CS', 'CK', 'CM', 'CP', 'CQ', 'CR'], tvaForm, change);
  useCompute('BN', sum, ['BA', 'BB', 'BC', 'AE', 'BE', 'BF', 'BS', 'BK', 'BM', 'BP', 'BQ', 'BR'], tvaForm, change);
  useCompute('PK', sum, ['PH1', 'PH2', 'PH3', 'PH4', 'PH5', 'PH6', 'PH7', 'PH8', 'PH9'], tvaForm, change);
  useCompute('QE', sum, ['QB1', 'QB2', 'QB3', 'QB4', 'QB5', 'QB6', 'QB7'], tvaForm, change);
  useCompute('HB', sum, ['FA', 'FB', 'FJ', 'KJ', 'KL', 'PP', 'PR', 'HF', 'FR', 'KF', 'FT', 'KG', 'FV', 'HI', 'MY', 'NB', 'JH', 'JN', 'JP', 'KZ', 'LA', 'FG', 'HE', 'JB', 'JC', 'JD', 'JF', 'JJ', 'JK', 'JL',
    'JM', 'JQ', 'JR', 'KT', 'KX', 'KY', 'LC', 'LD', 'LF', 'LE', 'LG', 'LH', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LP', 'LW', 'LX', 'NZ', 'PA', 'PB', 'PC', 'PD', 'MA', 'NQ', 'NR', 'NS', 'NC', 'PK', 'PU', 'PX', 'PZ', 'QE',
    'LS', 'LU', 'LQ', 'LR', 'LT', 'JG'], tvaForm, change);

  useCompute('CB', calcCB, ['BB'], tvaForm, change);
  useCompute('CE', calcCE, ['BE'], tvaForm, change);
  useCompute('CF', calcCF, ['BF'], tvaForm, change);
  useCompute('CS', calcCS, ['BS'], tvaForm, change);
  useCompute('CK', calcCK, ['BK'], tvaForm, change);
  useCompute('CP', calcCP, ['BP'], tvaForm, change);
  useCompute('CQ', calcCQ, ['BQ'], tvaForm, change);
  useCompute('FA', calcFA, ['NM'], tvaForm, change);
  useCompute('FB', calcFB, ['NE'], tvaForm, change);
  useCompute('FJ', calcFJ, ['NF'], tvaForm, change);
  useCompute('KJ', calcKJ, ['MK'], tvaForm, change);
  useCompute('KL', calcKL, ['ML'], tvaForm, change);
  useCompute('PP', calcPP, ['PQ'], tvaForm, change);
  useCompute('PR', calcPR, ['PS'], tvaForm, change);
  useCompute('FT', calcFT, ['MM'], tvaForm, change);
  useCompute('KG', calcKG, ['NH'], tvaForm, change);
  useCompute('FV', calcFV, ['MN'], tvaForm, change);
  useCompute('HI', calcHI, ['MP'], tvaForm, change);
  useCompute('MY', calcMY, ['NJ'], tvaForm, change);
  useCompute('NB', calcNB, ['NK'], tvaForm, change);
  useCompute('JH', calcJH, ['NL'], tvaForm, change);
  useCompute('JN', calcJN, ['NN'], tvaForm, change);
  useCompute('JP', calcJP, ['NP'], tvaForm, change);
  useCompute('KZ', calcKZ, ['PE'], tvaForm, change);
  useCompute('LA', calcLA, ['PF'], tvaForm, change);
  useCompute('LF', calcLF, ['MD'], tvaForm, change);
  useCompute('LE', calcLE, ['ME'], tvaForm, change);
  useCompute('LG', calcLG, ['MF'], tvaForm, change);
  useCompute('LH', calcLH, ['MG'], tvaForm, change);
  useCompute('LK', calcLK, ['MQ'], tvaForm, change);
  useCompute('LL', calcLL, ['MZ'], tvaForm, change);
  useCompute('LM', calcLM, ['NA'], tvaForm, change);
  useCompute('PD', calcPD, ['NX'], tvaForm, change);
  useCompute('NQ', calcNQ, ['NY'], tvaForm, change);
  useCompute('PB', calcPB, ['NV'], tvaForm, change);
  useCompute('PC', calcPC, ['NW'], tvaForm, change);
  useCompute('MA', calcMA, ['MC'], tvaForm, change);

  return (
    <div className="TVA3310">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 298, top: 38, width: 599, height: 22, 'font-size': 23, }}>
<span>
TAXE SUR LA VALEUR AJOUTÉE ET TAXES ASSIMILÉES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 285, top: 89, width: 627, height: 22, 'font-size': 23, }}>
<span>
IMPRIMÉ À FOURNIR EN ANNEXE À LA DÉCLARATION CA3
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 582, top: 120, width: 31, height: 17, 'font-size': 23, }}>
<span>
OU
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 274, top: 141, width: 650, height: 22, 'font-size': 23, }}>
<span>
ISOLÉMENT EN CAS DE NON-ASSUJETTISSEMENT À LA TVA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1087, top: 51, width: 64, height: 16, 'font-size': 21, }}>
<span>
3310 A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 127, top: 224, width: 590, height: 15, 'font-size': 21, }}>
<span>
RETENUE TVA SUR DROITS D&#39;AUT. ET TVA DUE A TX PART.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 47, top: 224, width: 15, height: 15, 'font-size': 21, }}>
<span>
A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 767, top: 260, width: 176, height: 15, 'font-size': 21, }}>
<span>
BASE HORS TAXE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 274, top: 189, width: 648, height: 15, 'font-size': 17, }}>
<span>
ATTENTION : L&#39;arrondissement des bases et des cotisations s&#39;effectue ‡ l&#39;euro le plus proche.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 753, top: 224, width: 412, height: 15, 'font-size': 21, }}>
<span>
REGIME DU REEL OU DU RSI - MINI REEL
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1017, top: 260, width: 103, height: 15, 'font-size': 21, }}>
<span>
TAXE DUE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 294, width: 19, height: 15, 'font-size': 21, }}>
<span>
35
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 296, width: 230, height: 12, 'font-size': 17, }}>
<span>
Retenue de TVA sur droits d&#39;auteur
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 690, top: 294, width: 40, height: 15, 'font-size': 21, }}>
<span>
0990
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 329, width: 567, height: 19, 'font-size': 21, }}>
<span>
OP. IMPOSABLES en France continentale ‡ un tx particulier de :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 364, width: 20, height: 15, 'font-size': 21, }}>
<span>
36
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 365, width: 82, height: 15, 'font-size': 17, }}>
<span>
Taux 2,10 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 692, top: 364, width: 38, height: 15, 'font-size': 21, }}>
<span>
1010
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 399, width: 20, height: 14, 'font-size': 21, }}>
<span>
37
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 400, width: 86, height: 12, 'font-size': 17, }}>
<span>
Anciens taux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 692, top: 399, width: 38, height: 14, 'font-size': 21, }}>
<span>
1020
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 433, width: 20, height: 15, 'font-size': 21, }}>
<span>
38
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 435, width: 334, height: 15, 'font-size': 17, }}>
<span>
Ligne utilisable pour un nouveau taux en mÈtropole
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 468, width: 468, height: 19, 'font-size': 21, }}>
<span>
OP. IMPOSABLES en Corse ‡ un taux particulier de :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 503, width: 20, height: 15, 'font-size': 21, }}>
<span>
39
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 504, width: 82, height: 15, 'font-size': 17, }}>
<span>
Taux 0,90 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 538, width: 20, height: 14, 'font-size': 21, }}>
<span>
40
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 539, width: 82, height: 15, 'font-size': 17, }}>
<span>
Taux 2,10 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 572, width: 18, height: 15, 'font-size': 21, }}>
<span>
41
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 574, width: 66, height: 12, 'font-size': 17, }}>
<span>
Taux 10%
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 692, top: 503, width: 38, height: 15, 'font-size': 21, }}>
<span>
1040
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 692, top: 538, width: 38, height: 14, 'font-size': 21, }}>
<span>
1050
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 692, top: 572, width: 37, height: 15, 'font-size': 21, }}>
<span>
1081
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 607, width: 20, height: 15, 'font-size': 21, }}>
<span>
42
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 609, width: 70, height: 12, 'font-size': 17, }}>
<span>
Taux 13 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 692, top: 607, width: 38, height: 15, 'font-size': 21, }}>
<span>
1090
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 677, width: 515, height: 19, 'font-size': 21, }}>
<span>
OP. IMPOSABLES dans les DOM ‡ un taux particulier de :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 711, width: 20, height: 15, 'font-size': 21, }}>
<span>
44
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 713, width: 82, height: 15, 'font-size': 17, }}>
<span>
Taux 1,05 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 746, width: 19, height: 15, 'font-size': 21, }}>
<span>
45
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 86, top: 748, width: 82, height: 14, 'font-size': 17, }}>
<span>
Taux 1,75 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 781, width: 20, height: 15, 'font-size': 21, }}>
<span>
46
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 782, width: 86, height: 13, 'font-size': 17, }}>
<span>
Anciens taux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 693, top: 712, width: 38, height: 14, 'font-size': 21, }}>
<span>
1110
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 693, top: 746, width: 38, height: 15, 'font-size': 21, }}>
<span>
1120
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 692, top: 781, width: 38, height: 15, 'font-size': 21, }}>
<span>
1030
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 693, top: 642, width: 38, height: 15, 'font-size': 21, }}>
<span>
1100
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 85, top: 643, width: 86, height: 13, 'font-size': 17, }}>
<span>
Anciens taux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 642, width: 19, height: 15, 'font-size': 21, }}>
<span>
43
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 816, width: 528, height: 19, 'font-size': 21, }}>
<span>
TOTAL DES LIGNES 35 ‡ 46 (‡ reporter ligne 14 de la CA3)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 986, width: 182, height: 15, 'font-size': 21, }}>
<span>
COMMENTAIRES :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 502, top: 894, width: 114, height: 16, 'font-size': 17, }}>
<span>
Mention expresse
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field
              component={PdfNumberInput} setError={setError}
              formName={form} error={_get(errors, 'BA', false)}
              className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="BA" data-field-id="26677560"
              data-annot-id="25618576" type="text"
              onBlur={validateForm} />

            <Field component={PdfNumberInput}  setError={setError} formName={form} error={_get(errors, 'CA', false)}  className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="CA" data-field-id="26684408" data-annot-id="25891392" type="text"
                   onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="BB" data-field-id="26680984" data-annot-id="25784192" type="text" />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'BC', false)} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="BC" data-field-id="26749464" data-annot-id="25889952" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'AE', false)} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="AE" data-field-id="26760904" data-annot-id="25890144" type="text"
                   onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="BE" data-field-id="26746040" data-annot-id="25890336" type="text" error={_get(errors, 'BE', false)} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="BF" data-field-id="26742616" data-annot-id="25890528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="BS" data-field-id="26764616" data-annot-id="25894464" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="BK" data-field-id="26739192" data-annot-id="25894656" type="text" />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'BM', false)} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="BM" data-field-id="26735768" data-annot-id="25894848" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="BP" data-field-id="26732344" data-annot-id="25895040" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="BQ" data-field-id="26728920" data-annot-id="25895232" type="text" />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'BR', false)} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="BR" data-field-id="26725496" data-annot-id="25895424" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="BN" data-field-id="26722072" data-annot-id="25895616" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="CN" data-field-id="26718648" data-annot-id="25895808" type="text" disabled />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'CR', false)}  className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "  name="CR" data-field-id="26715224" data-annot-id="25896000" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="CQ" data-field-id="26711800" data-annot-id="25896192" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="CP" data-field-id="26708376" data-annot-id="25896656" type="text" disabled />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'CM', false)}  className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="CM" data-field-id="26704952" data-annot-id="25896848" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="CK" data-field-id="26701528" data-annot-id="25897040" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="CS" data-field-id="26768088" data-annot-id="25897232" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="CF" data-field-id="26698104" data-annot-id="25897424" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="CE" data-field-id="26694680" data-annot-id="25897616" type="text" disabled />

            <Field component={PdfNumberInput}  setError={setError} formName={form} error={_get(errors, 'AF', false)} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="AF" data-field-id="26771560" data-annot-id="25897808" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'CC', false)} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="CC" data-field-id="26691256" data-annot-id="25898000" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="CB" data-field-id="26687832" data-annot-id="25898192" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field pde-form-field-text" name="DB" data-field-id="26775032" data-annot-id="25898384" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field pde-form-field-text" name="DAA" data-field-id="26778504" data-annot-id="25898576" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field pde-form-field-text" name="DAB" data-field-id="26781976" data-annot-id="25898768" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field pde-form-field-text" name="DAC" data-field-id="26785448" data-annot-id="25898960" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field pde-form-field-text" name="DAE" data-field-id="26788920" data-annot-id="25899152" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field pde-form-field-text" name="DAF" data-field-id="26787208" data-annot-id="25899344" type="text" />

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 42, top: 37, width: 11, height: 13, 'font-size': 17, }}>
<span>
B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 367, top: 34, width: 287, height: 16, 'font-size': 17, }}>
<span>
DÉCOMPTE DES TAXES ASSIMILÉES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1021, top: 37, width: 87, height: 16, 'font-size': 17, }}>
<span>
Net à payer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 76, width: 18, height: 13, 'font-size': 17, }}>
<span>
47
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 67, width: 551, height: 16, 'font-size': 17, }}>
<span>
Taxe sur certaines dépenses de publicité (CGI, art. 302 bis MA) au taux de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 76, top: 86, width: 26, height: 13, 'font-size': 17, }}>
<span>
1 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 76, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 76, width: 254, height: 13, 'font-size': 17, }}>
<span>
4213 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 115, width: 717, height: 17, 'font-size': 17, }}>
<span>
48 Taxe sur les retransmissions sportives (CGI, art. 302 bis ZE) au taux de 5 % Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 116, width: 254, height: 12, 'font-size': 17, }}>
<span>
4215 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 145, width: 759, height: 16, 'font-size': 17, }}>
<span>
49 Taxe sur les excédents de provision des entreprises d’assurances de dommages (CGI, art. 235 ter X)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 145, width: 254, height: 13, 'font-size': 17, }}>
<span>
4238 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 184, width: 17, height: 13, 'font-size': 17, }}>
<span>
50
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 175, width: 377, height: 16, 'font-size': 17, }}>
<span>
Taxe sur le chiffre d’affaires des exploitants agricoles
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 193, width: 534, height: 16, 'font-size': 17, }}>
<span>
(CGI, art. 302 bis MB) (cumul de la partie variable et de la partie forfaitaire)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 184, width: 254, height: 13, 'font-size': 17, }}>
<span>
4220 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 223, width: 616, height: 16, 'font-size': 17, }}>
<span>
53 Taxe sur les huiles alimentaires (CGI, art 1609 vicies) [ne concerne pas les DOM]
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 223, width: 254, height: 13, 'font-size': 17, }}>
<span>
3240 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 253, width: 590, height: 16, 'font-size': 17, }}>
<span>
54 Contribution perçue au profit de l’ANSP (ex-INPES) (CGI, art 1609 octovicies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 253, width: 254, height: 12, 'font-size': 17, }}>
<span>
4222 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 292, width: 17, height: 13, 'font-size': 17, }}>
<span>
55
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 282, width: 494, height: 17, 'font-size': 17, }}>
<span>
Taxe due par les concessionnaires d’autoroutes (CGI, art 302 bis ZB)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 301, width: 360, height: 16, 'font-size': 17, }}>
<span>
(7,32 € pour 1000 km) [Ne concerne pas les DOM]
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 658, top: 283, width: 79, height: 12, 'font-size': 17, }}>
<span>
Nombre de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 661, top: 301, width: 73, height: 13, 'font-size': 17, }}>
<span>
kilomètres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 292, width: 254, height: 13, 'font-size': 17, }}>
<span>
4207 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 340, width: 17, height: 13, 'font-size': 17, }}>
<span>
56
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 331, width: 616, height: 16, 'font-size': 17, }}>
<span>
Contribution à l’audiovisuel public (ex-redevance audiovisuelle) (CGI, art 1605 et suiv.)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 349, width: 348, height: 16, 'font-size': 17, }}>
<span>
[cf. fiche de calcul sur le site www. impots.gouv.fr]
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 340, width: 254, height: 13, 'font-size': 17, }}>
<span>
4219 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 388, width: 17, height: 13, 'font-size': 17, }}>
<span>
57
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 379, width: 674, height: 16, 'font-size': 17, }}>
<span>
Contribution à l’audiovisuel public (ex-redevance audiovisuelle) due par les loueurs d’appareils
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 397, width: 161, height: 16, 'font-size': 17, }}>
<span>
(CGI, art.1605 et suiv.)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 388, width: 254, height: 13, 'font-size': 17, }}>
<span>
4221 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 427, width: 650, height: 16, 'font-size': 17, }}>
<span>
Taxe sur la diffusion en vidéo physique et en ligne de contenus audiovisuels à titre onéreux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 446, width: 198, height: 16, 'font-size': 17, }}>
<span>
(CGI, art 1609 sexdecies B)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 476, width: 17, height: 12, 'font-size': 17, }}>
<span>
59
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 475, width: 121, height: 14, 'font-size': 17, }}>
<span>
– au taux de 2 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 490, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 476, width: 254, height: 12, 'font-size': 17, }}>
<span>
4229 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 505, width: 17, height: 13, 'font-size': 17, }}>
<span>
60
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 505, width: 130, height: 13, 'font-size': 17, }}>
<span>
– au taux de 10 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 505, width: 254, height: 13, 'font-size': 17, }}>
<span>
4228 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 535, width: 636, height: 16, 'font-size': 17, }}>
<span>
Taxe sur la diffusion en vidéo physique et en ligne de contenus audiovisuels à titre gratuit
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 553, width: 198, height: 17, 'font-size': 17, }}>
<span>
(CGI, art 1609 sexdecies B)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 583, width: 29, height: 13, 'font-size': 17, }}>
<span>
60A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 583, width: 121, height: 13, 'font-size': 17, }}>
<span>
– au taux de 2 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 598, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 583, width: 254, height: 13, 'font-size': 17, }}>
<span>
4298 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 613, width: 28, height: 13, 'font-size': 17, }}>
<span>
60B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 613, width: 130, height: 13, 'font-size': 17, }}>
<span>
– au taux de 10 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 613, width: 254, height: 13, 'font-size': 17, }}>
<span>
4299 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 643, width: 743, height: 16, 'font-size': 17, }}>
<span>
61 Taxe sur la publicité diffusée par voie de radiodiffusion sonore et de télévision (CGI, art 302 bis KD)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 643, width: 254, height: 12, 'font-size': 17, }}>
<span>
4214 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 672, width: 399, height: 16, 'font-size': 17, }}>
<span>
62 Taxe sur la publicité télévisée (CGI, art 302 bis KA)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 672, width: 254, height: 13, 'font-size': 17, }}>
<span>
4201 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 702, width: 727, height: 16, 'font-size': 17, }}>
<span>
63 Taxe sur la publicité diffusée par les chaînes de télévision (CGI, art 302 bis KG) au taux de 0,5 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 702, width: 254, height: 13, 'font-size': 17, }}>
<span>
4225 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 741, width: 17, height: 13, 'font-size': 17, }}>
<span>
64
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 732, width: 301, height: 16, 'font-size': 17, }}>
<span>
Taxe sur les actes des huissiers de justice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 750, width: 270, height: 16, 'font-size': 17, }}>
<span>
(CGI, art 302 bis Y) (14,89 € par acte)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 642, top: 741, width: 112, height: 13, 'font-size': 17, }}>
<span>
Nombre d’actes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 741, width: 254, height: 13, 'font-size': 17, }}>
<span>
4206 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 799, width: 17, height: 12, 'font-size': 17, }}>
<span>
65
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 780, width: 472, height: 16, 'font-size': 17, }}>
<span>
Taxe sur les services fournis par les opérateurs de communication
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 799, width: 87, height: 15, 'font-size': 17, }}>
<span>
électronique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 817, width: 275, height: 16, 'font-size': 17, }}>
<span>
(CGI, art 302 bis KH) au taux de 1,3 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 799, width: 114, height: 15, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 799, width: 254, height: 12, 'font-size': 17, }}>
<span>
4226 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 856, width: 17, height: 13, 'font-size': 17, }}>
<span>
66
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 847, width: 518, height: 16, 'font-size': 17, }}>
<span>
Taxe sur les embarquements ou débarquements de passagers en Corse
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 865, width: 311, height: 17, 'font-size': 17, }}>
<span>
(CGI, art 1599 vicies) (4,57 € par passager)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 669, top: 847, width: 57, height: 13, 'font-size': 17, }}>
<span>
Nombre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 649, top: 866, width: 97, height: 16, 'font-size': 17, }}>
<span>
de passagers
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 856, width: 254, height: 13, 'font-size': 17, }}>
<span>
4204 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 914, width: 17, height: 13, 'font-size': 17, }}>
<span>
68
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 895, width: 551, height: 16, 'font-size': 17, }}>
<span>
Taxe pour le développement de la formation professionnelle dans les métiers
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 914, width: 551, height: 16, 'font-size': 17, }}>
<span>
de la réparation de l’automobile, du cycle et du motocycle (CGI, art 1609
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 932, width: 204, height: 16, 'font-size': 17, }}>
<span>
sexvicies) au taux de 0,75 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 914, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 914, width: 254, height: 12, 'font-size': 17, }}>
<span>
4217 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 962, width: 743, height: 16, 'font-size': 17, }}>
<span>
69 Taxe sur les ordres annulés dans le cadre d’opérations à haute fréquence (CGI, art. 235 ter ZD bis)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 962, width: 254, height: 13, 'font-size': 17, }}>
<span>
4239 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 992, width: 535, height: 16, 'font-size': 17, }}>
<span>
Contribution sur les activités privées de sécurité (CGI, art. 1609 quintricies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1021, width: 15, height: 13, 'font-size': 17, }}>
<span>
71
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1021, width: 134, height: 15, 'font-size': 17, }}>
<span>
– au taux de 0,4 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1036, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1021, width: 254, height: 13, 'font-size': 17, }}>
<span>
4288 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1051, width: 17, height: 13, 'font-size': 17, }}>
<span>
72
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1051, width: 134, height: 15, 'font-size': 17, }}>
<span>
– au taux de 0,6 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1051, width: 254, height: 13, 'font-size': 17, }}>
<span>
4289 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1081, width: 526, height: 16, 'font-size': 17, }}>
<span>
75 Taxe de risque systémique (CGI, art. 235 ter ZE) au taux de 0,141 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1081, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1081, width: 254, height: 13, 'font-size': 17, }}>
<span>
4240 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1111, width: 741, height: 16, 'font-size': 17, }}>
<span>
76 Contribution due par les gestionnaires des réseaux publics d’électricité (CGCT, art. L 2224-31 I bis)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1111, width: 254, height: 13, 'font-size': 17, }}>
<span>
4236 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1140, width: 525, height: 16, 'font-size': 17, }}>
<span>
78 Taxe sur le résultat des entreprises ferroviaires (CGI, art. 235 ter ZF)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1140, width: 254, height: 13, 'font-size': 17, }}>
<span>
4241 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1170, width: 449, height: 16, 'font-size': 17, }}>
<span>
79 Contribution de solidarité territoriale (CGI, art. 302 bis ZC)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1170, width: 254, height: 13, 'font-size': 17, }}>
<span>
4242 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1200, width: 424, height: 16, 'font-size': 17, }}>
<span>
80 Imposition forfaitaire sur les pylônes (CGI, art. 1519 A)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1200, width: 254, height: 12, 'font-size': 17, }}>
<span>
4243 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1229, width: 401, height: 16, 'font-size': 17, }}>
<span>
81 Taxe sur les éoliennes maritimes (CGI, art. 1519 B)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1229, width: 254, height: 13, 'font-size': 17, }}>
<span>
4244 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1278, width: 17, height: 12, 'font-size': 17, }}>
<span>
82
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1259, width: 551, height: 16, 'font-size': 17, }}>
<span>
Prélèvement sur les films pornographiques ou d’incitation à la violence et sur
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1278, width: 551, height: 16, 'font-size': 17, }}>
<span>
les représentations théâtrales à caractère pornographique
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1296, width: 283, height: 16, 'font-size': 17, }}>
<span>
(CGI, art. 1605 sexies) au taux de 33 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1278, width: 114, height: 15, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1278, width: 254, height: 12, 'font-size': 17, }}>
<span>
4245 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1344, width: 17, height: 13, 'font-size': 17, }}>
<span>
83
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1326, width: 552, height: 16, 'font-size': 17, }}>
<span>
Taxe pour le financement du fonds de soutien aux collectivités territoriales
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1344, width: 550, height: 16, 'font-size': 17, }}>
<span>
ayant contracté des produits structurés (CGI, art. 235 ter ZE bis) au taux de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1363, width: 67, height: 15, 'font-size': 17, }}>
<span>
0,0642 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1345, width: 114, height: 15, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1344, width: 254, height: 13, 'font-size': 17, }}>
<span>
4252 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 1393, width: 503, height: 16, 'font-size': 17, }}>
<span>
84A Redevance sanitaire d’abattage (CGI, art. 302 bis N à 302 bis R)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1393, width: 254, height: 12, 'font-size': 17, }}>
<span>
4253 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 1422, width: 533, height: 16, 'font-size': 17, }}>
<span>
84B Redevance sanitaire de découpage (CGI, art. 302 bis S à 302 bis W)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1422, width: 254, height: 13, 'font-size': 17, }}>
<span>
4254 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1452, width: 777, height: 16, 'font-size': 17, }}>
<span>
85 Redevance sanitaire pour le contrôle de certaines substances et de leurs résidus (CGI, art. 302 bis WC)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1452, width: 254, height: 13, 'font-size': 17, }}>
<span>
4247 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1491, width: 17, height: 13, 'font-size': 17, }}>
<span>
86
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1482, width: 699, height: 15, 'font-size': 17, }}>
<span>
Redevance sanitaire de première mise sur le marché des produits de la pêche ou de l’aquaculture
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1500, width: 158, height: 16, 'font-size': 17, }}>
<span>
(CGI, art. 302 bis WA)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1491, width: 254, height: 13, 'font-size': 17, }}>
<span>
4248 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1539, width: 17, height: 13, 'font-size': 17, }}>
<span>
87
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1530, width: 550, height: 16, 'font-size': 17, }}>
<span>
Redevance sanitaire de transformation des produits de la pêche ou de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1548, width: 378, height: 17, 'font-size': 17, }}>
<span>
l’aquaculture (CGI, art. 302 bis WB) (0,5 € par tonne)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 658, top: 1530, width: 79, height: 13, 'font-size': 17, }}>
<span>
Nombre de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 673, top: 1549, width: 48, height: 12, 'font-size': 17, }}>
<span>
tonnes
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1539, width: 254, height: 13, 'font-size': 17, }}>
<span>
4249 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1597, width: 17, height: 13, 'font-size': 17, }}>
<span>
88
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1578, width: 550, height: 16, 'font-size': 17, }}>
<span>
Redevance pour agrément des établissements du secteur de l’alimentation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1597, width: 266, height: 16, 'font-size': 17, }}>
<span>
animale (CGI, art. 302 bis WD à WG)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1615, width: 182, height: 16, 'font-size': 17, }}>
<span>
(125 € par établissement)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 669, top: 1588, width: 57, height: 12, 'font-size': 17, }}>
<span>
Nombre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 638, top: 1606, width: 119, height: 13, 'font-size': 17, }}>
<span>
d&#39;établissements
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1597, width: 254, height: 13, 'font-size': 17, }}>
<span>
4250 ………………………………
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <a className="pdf-annot pdf-link-annot obj_34 pdf-obj-fixed" href="http://www.impots.gouv.fr/" data-annot-id="26130480">
            </a>
            <a className="pdf-annot pdf-link-annot obj_35 pdf-obj-fixed" href="http://www.impots.gouv.fr/" data-annot-id="25986880">
            </a>
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="NM" data-field-id="26184440" data-annot-id="25987072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="NE" data-field-id="26187384" data-annot-id="26080528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="FA" data-field-id="26190472" data-annot-id="26080720" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="FB" data-field-id="26193624" data-annot-id="26844336" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="JF" data-field-id="26196712" data-annot-id="26844528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="HE" data-field-id="26203064" data-annot-id="26844720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="FG" data-field-id="26199864" data-annot-id="26846224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="JD" data-field-id="26206920" data-annot-id="26846416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="FJ" data-field-id="26210008" data-annot-id="26846608" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="JB" data-field-id="26213160" data-annot-id="26846800" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="JC" data-field-id="26216344" data-annot-id="26846992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="KJ" data-field-id="26219496" data-annot-id="26847184" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="NF" data-field-id="26222648" data-annot-id="26847376" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="MK" data-field-id="26225800" data-annot-id="26847568" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="ML" data-field-id="26228952" data-annot-id="26847760" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="KL" data-field-id="26232104" data-annot-id="26848224" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="PP" data-field-id="26567304" data-annot-id="26848416" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="HF" data-field-id="26235256" data-annot-id="26848608" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="PR" data-field-id="26570456" data-annot-id="26848800" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="FR" data-field-id="26238408" data-annot-id="26848992" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="KF" data-field-id="26241672" data-annot-id="26849184" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="FT" data-field-id="26244600" data-annot-id="26849376" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="KG" data-field-id="26247720" data-annot-id="26849568" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="FV" data-field-id="26250920" data-annot-id="26849760" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="HI" data-field-id="26254040" data-annot-id="26849952" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="JG" data-field-id="26257240" data-annot-id="26850144" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="MY" data-field-id="26260360" data-annot-id="26856896" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="NB" data-field-id="26263560" data-annot-id="26857088" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="JH" data-field-id="26266680" data-annot-id="26857280" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="KT" data-field-id="26269880" data-annot-id="26857472" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="JJ" data-field-id="26273000" data-annot-id="26857664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="JK" data-field-id="26276200" data-annot-id="26847952" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="JL" data-field-id="26279320" data-annot-id="26858384" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="JM" data-field-id="26282520" data-annot-id="26858576" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="JN" data-field-id="26285640" data-annot-id="26858768" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="JP" data-field-id="26289368" data-annot-id="26858960" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="JQ" data-field-id="26292248" data-annot-id="26859152" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="JR" data-field-id="26295368" data-annot-id="26859344" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="KX" data-field-id="26298568" data-annot-id="26859536" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="KY" data-field-id="26301688" data-annot-id="26859728" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="KZ" data-field-id="26304888" data-annot-id="26859920" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="LA" data-field-id="26308008" data-annot-id="26860112" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="MM" data-field-id="26311208" data-annot-id="26860304" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="NH" data-field-id="26314360" data-annot-id="26860496" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="MN" data-field-id="26317512" data-annot-id="26860688" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="MP" data-field-id="26320664" data-annot-id="26860880" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="NJ" data-field-id="26323816" data-annot-id="26861072" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="NK" data-field-id="26326968" data-annot-id="26861264" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="NL" data-field-id="26330120" data-annot-id="26861456" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="NN" data-field-id="26333272" data-annot-id="26861648" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="NP" data-field-id="26336424" data-annot-id="26861840" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="PE" data-field-id="26339576" data-annot-id="26862032" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="PF" data-field-id="26342728" data-annot-id="26862224" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="PQ" data-field-id="26573608" data-annot-id="26862416" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="PS" data-field-id="26576760" data-annot-id="26862608" type="text" />

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-2" data-page-num="2" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-2 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 42, top: 37, width: 11, height: 13, 'font-size': 17, }}>
<span>
B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 367, top: 34, width: 287, height: 16, 'font-size': 17, }}>
<span>
DÉCOMPTE DES TAXES ASSIMILÉES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1021, top: 37, width: 87, height: 16, 'font-size': 17, }}>
<span>
Net à payer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 67, width: 563, height: 16, 'font-size': 17, }}>
<span>
Redevance phytosanitaire à la circulation intracommunautaire et à l’exportation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 86, width: 372, height: 16, 'font-size': 17, }}>
<span>
(Code rural et de la pêche maritime, art. L 251-17-1)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 115, width: 17, height: 13, 'font-size': 17, }}>
<span>
89
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 115, width: 311, height: 16, 'font-size': 17, }}>
<span>
– à la circulation intracommunautaire (PPE)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 115, width: 254, height: 13, 'font-size': 17, }}>
<span>
4273 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 145, width: 17, height: 13, 'font-size': 17, }}>
<span>
90
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 145, width: 113, height: 16, 'font-size': 17, }}>
<span>
– à l’exportation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 145, width: 254, height: 13, 'font-size': 17, }}>
<span>
4274 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 174, width: 724, height: 17, 'font-size': 17, }}>
<span>
Taxe forfaitaire sur les ventes de métaux précieux, de bijoux, d’objets d’art, de collection et d’antiquité
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 193, width: 131, height: 16, 'font-size': 17, }}>
<span>
(CGI, art. 150 VM)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 223, width: 15, height: 13, 'font-size': 17, }}>
<span>
91
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 223, width: 391, height: 16, 'font-size': 17, }}>
<span>
-– sur les ventes de métaux précieux aux taux de 11 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 223, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 223, width: 254, height: 13, 'font-size': 17, }}>
<span>
4268 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 262, width: 17, height: 13, 'font-size': 17, }}>
<span>
92
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 253, width: 479, height: 16, 'font-size': 17, }}>
<span>
-– sur les ventes de bijoux, objets d’art, de collection ou d’antiquité
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 271, width: 107, height: 13, 'font-size': 17, }}>
<span>
au taux de 6 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 262, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 262, width: 254, height: 13, 'font-size': 17, }}>
<span>
4270 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 302, width: 719, height: 16, 'font-size': 17, }}>
<span>
Contribution pour le remboursement de la dette sociale (CRDS) (CGI, art. 1600-0 I) au taux de 0,5 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 333, width: 17, height: 13, 'font-size': 17, }}>
<span>
93
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 333, width: 259, height: 16, 'font-size': 17, }}>
<span>
– sur les ventes de métaux précieux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 348, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 333, width: 254, height: 13, 'font-size': 17, }}>
<span>
4269 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 363, width: 17, height: 13, 'font-size': 17, }}>
<span>
94
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 363, width: 469, height: 16, 'font-size': 17, }}>
<span>
– sur les ventes de bijoux, objets d’art, de collection ou d’antiquité
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 363, width: 254, height: 13, 'font-size': 17, }}>
<span>
4271 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 392, width: 805, height: 17, 'font-size': 17, }}>
<span>
95 Contribution forfaitaire pour alimentation du fonds commun des accidents du travail agricole (CGI, art. 1622)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 393, width: 254, height: 12, 'font-size': 17, }}>
<span>
4272 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 422, width: 263, height: 16, 'font-size': 17, }}>
<span>
Prélèvements sur les paris hippiques
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 452, width: 17, height: 13, 'font-size': 17, }}>
<span>
96
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 449, width: 419, height: 19, 'font-size': 17, }}>
<span>
– au profit de l’État (CGI, art. 302 bis ZG) au taux de 5,3 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 500, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 452, width: 254, height: 13, 'font-size': 17, }}>
<span>
4256 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 491, width: 17, height: 13, 'font-size': 17, }}>
<span>
97
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 482, width: 328, height: 16, 'font-size': 17, }}>
<span>
– au profit des organismes de sécurité sociale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 500, width: 264, height: 16, 'font-size': 17, }}>
<span>
(CSS, art. L137-20) au taux de 1,8 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 491, width: 254, height: 13, 'font-size': 17, }}>
<span>
4259 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 539, width: 17, height: 13, 'font-size': 17, }}>
<span>
98
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 530, width: 479, height: 16, 'font-size': 17, }}>
<span>
– engagés depuis l’étranger sur des courses françaises
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 549, width: 448, height: 16, 'font-size': 17, }}>
<span>
et regroupés en France (CGI, art. 302 bis ZO) au taux de 12 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 539, width: 254, height: 13, 'font-size': 17, }}>
<span>
4255 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 578, width: 497, height: 16, 'font-size': 17, }}>
<span>
Redevance due par les opérateurs agréés de paris hippiques en ligne
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 608, width: 17, height: 13, 'font-size': 17, }}>
<span>
99
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 608, width: 438, height: 16, 'font-size': 17, }}>
<span>
– Enjeux relatifs aux courses de trot (CGI, art. 1609 tertricies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 608, width: 254, height: 13, 'font-size': 17, }}>
<span>
4266 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 638, width: 26, height: 12, 'font-size': 17, }}>
<span>
100
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 638, width: 454, height: 16, 'font-size': 17, }}>
<span>
– Enjeux relatifs aux courses de galop (CGI, art. 1609 tertricies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 638, width: 254, height: 12, 'font-size': 17, }}>
<span>
4267 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 667, width: 246, height: 16, 'font-size': 17, }}>
<span>
Prélèvements sur les paris sportifs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 697, width: 23, height: 13, 'font-size': 17, }}>
<span>
101
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 694, width: 419, height: 19, 'font-size': 17, }}>
<span>
– au profit de l’État (CGI, art. 302 bis ZH) au taux de 5,7 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 745, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 697, width: 254, height: 13, 'font-size': 17, }}>
<span>
4257 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 736, width: 25, height: 13, 'font-size': 17, }}>
<span>
102
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 727, width: 328, height: 16, 'font-size': 17, }}>
<span>
– au profit des organismes de sécurité sociale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 745, width: 264, height: 16, 'font-size': 17, }}>
<span>
(CSS, art. L137-21) au taux de 1,8 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 736, width: 254, height: 13, 'font-size': 17, }}>
<span>
4260 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 784, width: 26, height: 13, 'font-size': 17, }}>
<span>
103
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 775, width: 451, height: 16, 'font-size': 17, }}>
<span>
– au profit de l’agence nationale du sport (ANS) (CGI, art. 1609
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 794, width: 173, height: 16, 'font-size': 17, }}>
<span>
tricies) au taux de 1,8 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 784, width: 254, height: 13, 'font-size': 17, }}>
<span>
4265 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 823, width: 254, height: 16, 'font-size': 17, }}>
<span>
Prélèvements sur les jeux de cercle
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 853, width: 26, height: 13, 'font-size': 17, }}>
<span>
104
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 850, width: 411, height: 19, 'font-size': 17, }}>
<span>
– au profit de l’État (CGI, art. 302 bis ZI) au taux de 1,8 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 877, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 853, width: 254, height: 13, 'font-size': 17, }}>
<span>
4258 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 892, width: 26, height: 13, 'font-size': 17, }}>
<span>
105
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 883, width: 328, height: 16, 'font-size': 17, }}>
<span>
– au profit des organismes de sécurité sociale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 901, width: 264, height: 16, 'font-size': 17, }}>
<span>
(CSS, art. L137-22) au taux de 0,2 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 892, width: 254, height: 13, 'font-size': 17, }}>
<span>
4261 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 932, width: 812, height: 16, 'font-size': 17, }}>
<span>
Prélèvement au profit de l’agence nationale du sport (ANS) sur les jeux commercialisés par la Française des jeux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 950, width: 191, height: 17, 'font-size': 17, }}>
<span>
(CGI, art. 1609 novovicies)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 981, width: 26, height: 13, 'font-size': 17, }}>
<span>
107
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 981, width: 134, height: 15, 'font-size': 17, }}>
<span>
– au taux de 0,3 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 981, width: 254, height: 13, 'font-size': 17, }}>
<span>
4264 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1011, width: 26, height: 13, 'font-size': 17, }}>
<span>
108
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1011, width: 134, height: 15, 'font-size': 17, }}>
<span>
– au taux de 1,8 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1011, width: 254, height: 13, 'font-size': 17, }}>
<span>
4263 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1041, width: 812, height: 16, 'font-size': 17, }}>
<span>
Prélèvements sur les jeux et paris dus par les casinos régis par l’article L321-3 du code de la sécurité intérieure
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1059, width: 237, height: 16, 'font-size': 17, }}>
<span>
(régime des « casinos flottants »)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1098, width: 26, height: 13, 'font-size': 17, }}>
<span>
109
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1089, width: 479, height: 16, 'font-size': 17, }}>
<span>
– Prélèvement progressif assis sur le produit brut des jeux (CGCT,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1107, width: 101, height: 16, 'font-size': 17, }}>
<span>
art. L2333-57)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1098, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1098, width: 254, height: 13, 'font-size': 17, }}>
<span>
4281 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1146, width: 24, height: 13, 'font-size': 17, }}>
<span>
110
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1137, width: 479, height: 16, 'font-size': 17, }}>
<span>
– Prélèvement complémentaire assis sur le produit brut des jeux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1156, width: 159, height: 16, 'font-size': 17, }}>
<span>
(CGCT, art. L2333-57)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1147, width: 114, height: 15, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1146, width: 254, height: 13, 'font-size': 17, }}>
<span>
4282 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1185, width: 393, height: 16, 'font-size': 17, }}>
<span>
Contribution sociale généralisée (CGCT, art. L2333-57)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1224, width: 21, height: 13, 'font-size': 17, }}>
<span>
111
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1215, width: 480, height: 16, 'font-size': 17, }}>
<span>
* sur une fraction égale à 68 % du produit brut des jeux des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 147, top: 1234, width: 245, height: 15, 'font-size': 17, }}>
<span>
machines à sous au taux de 9,5 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1258, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1224, width: 254, height: 13, 'font-size': 17, }}>
<span>
4283 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1282, width: 24, height: 13, 'font-size': 17, }}>
<span>
112
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1263, width: 480, height: 16, 'font-size': 17, }}>
<span>
* sur le montant des gains des machines à sous d’un montant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1282, width: 480, height: 16, 'font-size': 17, }}>
<span>
supérieur ou égal à 1 500 € réglés aux joueurs par le caissier sous
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1300, width: 383, height: 16, 'font-size': 17, }}>
<span>
forme de bons de paiement manuels au taux de 12 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1282, width: 254, height: 13, 'font-size': 17, }}>
<span>
4284 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1349, width: 24, height: 13, 'font-size': 17, }}>
<span>
113
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1330, width: 551, height: 16, 'font-size': 17, }}>
<span>
Contribution pour le remboursement de la dette sociale (CRDS) portant sur
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1349, width: 551, height: 16, 'font-size': 17, }}>
<span>
le montant du produit total des jeux au taux de 3 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1367, width: 460, height: 16, 'font-size': 17, }}>
<span>
(articles 18-III et 19 de l’ordonnance n°96-50 du 24 janvier 1996)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1349, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1349, width: 254, height: 13, 'font-size': 17, }}>
<span>
4285 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1436, width: 24, height: 13, 'font-size': 17, }}>
<span>
115
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1427, width: 551, height: 16, 'font-size': 17, }}>
<span>
Taxe annuelle pour frais de contrôle due par les concessionnaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1445, width: 428, height: 16, 'font-size': 17, }}>
<span>
d’autoroutes (CGI, art. 1609 septtricies) au taux de 0,363 ‰
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1436, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 1436, width: 254, height: 13, 'font-size': 17, }}>
<span>
4277 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1484, width: 24, height: 13, 'font-size': 17, }}>
<span>
116
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1475, width: 549, height: 16, 'font-size': 17, }}>
<span>
Contribution sociale à la charge des fournisseurs agréés de produits du
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1494, width: 391, height: 16, 'font-size': 17, }}>
<span>
tabac (CSS, art. L137-27 et suivants) au taux de 5,6 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1484, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{left: 905, top: 1484, width: 254, height: 13, 'font-size': 17}}>
<span>
4278 ………………………………
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="LC" data-field-id="26345880" data-annot-id="25510528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="LD" data-field-id="26349032" data-annot-id="26857968" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field " name="LF" data-field-id="26352184" data-annot-id="26956960" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="LE" data-field-id="26355336" data-annot-id="26839232" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field " name="LG" data-field-id="26358488" data-annot-id="26957328" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field " name="LH" data-field-id="26361640" data-annot-id="26957520" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="LJ" data-field-id="26364792" data-annot-id="26957712" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field " name="LK" data-field-id="26367944" data-annot-id="26960608" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="LL" data-field-id="26371096" data-annot-id="26960800" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field " name="LM" data-field-id="26374248" data-annot-id="26961136" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field " name="MD" data-field-id="26377400" data-annot-id="26961328" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field " name="ME" data-field-id="26380520" data-annot-id="26961520" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field " name="MF" data-field-id="26383720" data-annot-id="26961712" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field " name="MG" data-field-id="26386840" data-annot-id="26961904" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field " name="MQ" data-field-id="26288840" data-annot-id="26962096" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field " name="MZ" data-field-id="26393672" data-annot-id="26962288" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field " name="NA" data-field-id="26396872" data-annot-id="26962480" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field " name="LN" data-field-id="26399992" data-annot-id="26962944" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field " name="LP" data-field-id="26403192" data-annot-id="26963136" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field " name="LQ" data-field-id="26406312" data-annot-id="26963328" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field " name="LR" data-field-id="26409512" data-annot-id="26963520" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field " name="LS" data-field-id="26412632" data-annot-id="26963712" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field " name="LT" data-field-id="26415832" data-annot-id="26963904" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field " name="LU" data-field-id="26418952" data-annot-id="26964096" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field " name="LW" data-field-id="26422152" data-annot-id="26964288" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field " name="LX" data-field-id="26425272" data-annot-id="26964480" type="text" />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'NZ', false)} className="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field " name="NZ" data-field-id="26428472" data-annot-id="26964672" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'PA', false)}  className="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field " name="PA" data-field-id="26431592" data-annot-id="26964864" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field " name="PB" data-field-id="26434792" data-annot-id="26965056" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field " name="PC" data-field-id="26437912" data-annot-id="26965248" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field " name="PD" data-field-id="26441112" data-annot-id="26965440" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field " name="MA" data-field-id="26444232" data-annot-id="26965632" type="text" disabled />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field " name="NQ" data-field-id="26447432" data-annot-id="26965824" type="text" disabled />

            <Field component={PdfNumberInput} setError={setError} className="pde-form-field pdf-annot obj_125 pdf-obj-fixed acroform-field " error={_get(errors, 'MR', false)} name="MR" data-field-id="26450584" data-annot-id="26962672" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} className="pde-form-field pdf-annot obj_126 pdf-obj-fixed acroform-field " name="MS" data-field-id="26453736" data-annot-id="26966544" type="text" error={_get(errors, 'MS', false)} onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'NT', false)} className="pde-form-field pdf-annot obj_127 pdf-obj-fixed acroform-field " name="NT" data-field-id="26456888" data-annot-id="26966736" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'NU', false)} className="pde-form-field pdf-annot obj_128 pdf-obj-fixed acroform-field " name="NU" data-field-id="26460040" data-annot-id="26966928" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field " name="NV" data-field-id="26463192" data-annot-id="26967120" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_130 pdf-obj-fixed acroform-field " name="NW" data-field-id="26466344" data-annot-id="26967312" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_131 pdf-obj-fixed acroform-field " name="NX" data-field-id="26469496" data-annot-id="26967504" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_132 pdf-obj-fixed acroform-field " name="MC" data-field-id="26472648" data-annot-id="26967696" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_133 pdf-obj-fixed acroform-field " name="NY" data-field-id="26475800" data-annot-id="26967888" type="text" />

          </div>
        </div>

      </div>

      <div data-type="pdf-page" id="pdf-page-3" data-page-num="3" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-3 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 42, top: 37, width: 11, height: 13, 'font-size': 17, }}>
<span>
B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 367, top: 34, width: 287, height: 16, 'font-size': 17, }}>
<span>
DÉCOMPTE DES TAXES ASSIMILÉES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1021, top: 37, width: 87, height: 16, 'font-size': 17, }}>
<span>
Net à payer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 67, width: 361, height: 16, 'font-size': 17, }}>
<span>
Taxe sur les véhicules de sociétés (CGI, art. 1010)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 158, width: 24, height: 13, 'font-size': 17, }}>
<span>
117
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 158, width: 413, height: 14, 'font-size': 17, }}>
<span>
– Véhicules de sociétés taxés selon les émissions de CO2
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 658, top: 97, width: 79, height: 13, 'font-size': 17, }}>
<span>
Nombre de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 664, top: 116, width: 67, height: 13, 'font-size': 17, }}>
<span>
véhicules
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 652, top: 134, width: 91, height: 16, 'font-size': 17, }}>
<span>
possédés ou
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 679, top: 153, width: 38, height: 13, 'font-size': 17, }}>
<span>
loués
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 158, width: 254, height: 13, 'font-size': 17, }}>
<span>
4279 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 658, top: 183, width: 79, height: 12, 'font-size': 17, }}>
<span>
Nombre de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 661, top: 201, width: 73, height: 13, 'font-size': 17, }}>
<span>
kilomètres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 655, top: 220, width: 85, height: 12, 'font-size': 17, }}>
<span>
remboursés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 311, width: 24, height: 12, 'font-size': 17, }}>
<span>
118
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 310, width: 400, height: 16, 'font-size': 17, }}>
<span>
– Véhicules de sociétés taxés selon la puissance fiscale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 658, top: 250, width: 79, height: 12, 'font-size': 17, }}>
<span>
Nombre de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 664, top: 268, width: 67, height: 13, 'font-size': 17, }}>
<span>
véhicules
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 652, top: 287, width: 91, height: 15, 'font-size': 17, }}>
<span>
possédés ou
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 679, top: 305, width: 38, height: 13, 'font-size': 17, }}>
<span>
loués
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 311, width: 254, height: 12, 'font-size': 17, }}>
<span>
4280 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 658, top: 335, width: 79, height: 13, 'font-size': 17, }}>
<span>
Nombre de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 661, top: 353, width: 73, height: 13, 'font-size': 17, }}>
<span>
kilomètres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 655, top: 372, width: 85, height: 13, 'font-size': 17, }}>
<span>
remboursés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 420, width: 24, height: 13, 'font-size': 17, }}>
<span>
119
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 402, width: 549, height: 16, 'font-size': 17, }}>
<span>
Prélèvement progressif dû par les clubs de jeux (V de l&#39;article 34 de la loi n°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 420, width: 550, height: 16, 'font-size': 17, }}>
<span>
2017-257 du 28 février 2017 relative au statut de Paris et à l&#39;aménagement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 439, width: 97, height: 16, 'font-size': 17, }}>
<span>
métropolitain)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 420, width: 114, height: 16, 'font-size': 17, }}>
<span>
Base imposable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 420, width: 254, height: 13, 'font-size': 17, }}>
<span>
4290 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 478, width: 23, height: 12, 'font-size': 17, }}>
<span>
121
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 468, width: 551, height: 16, 'font-size': 17, }}>
<span>
Taxe sur l’exploration d’hydrocarbures calculée selon le barème fixé à
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 487, width: 482, height: 16, 'font-size': 17, }}>
<span>
l’article 1590 du CGI et perçue au profit des collectivités territoriales
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 468, width: 113, height: 13, 'font-size': 17, }}>
<span>
Code INSEE de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 652, top: 487, width: 91, height: 13, 'font-size': 17, }}>
<span>
la collectivité
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 800, top: 478, width: 58, height: 13, 'font-size': 17, }}>
<span>
Montant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 478, width: 254, height: 13, 'font-size': 17, }}>
<span>
4291 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 517, width: 407, height: 16, 'font-size': 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 547, width: 407, height: 16, 'font-size': 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 577, width: 407, height: 16, 'font-size': 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 607, width: 407, height: 16, 'font-size': 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 637, width: 407, height: 15, 'font-size': 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 667, width: 407, height: 15, 'font-size': 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 697, width: 407, height: 15, 'font-size': 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 727, width: 407, height: 15, 'font-size': 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 756, width: 407, height: 16, 'font-size': 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 796, width: 25, height: 12, 'font-size': 17, }}>
<span>
124
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 786, width: 421, height: 16, 'font-size': 17, }}>
<span>
Contribution sur les boissons contenant des sucres ajoutés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 805, width: 130, height: 16, 'font-size': 17, }}>
<span>
(CGI, art.1613 ter)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 669, top: 786, width: 57, height: 13, 'font-size': 17, }}>
<span>
Nombre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 655, top: 805, width: 85, height: 13, 'font-size': 17, }}>
<span>
d’hectolitres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 796, width: 254, height: 12, 'font-size': 17, }}>
<span>
4294 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 844, width: 420, height: 16, 'font-size': 17, }}>
<span>
125 Contribution sur les eaux (CGI, art. 1613 quater II 1°)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 669, top: 835, width: 57, height: 12, 'font-size': 17, }}>
<span>
Nombre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 655, top: 853, width: 85, height: 13, 'font-size': 17, }}>
<span>
d’hectolitres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 844, width: 254, height: 13, 'font-size': 17, }}>
<span>
4296 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 892, width: 25, height: 13, 'font-size': 17, }}>
<span>
126
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 883, width: 492, height: 16, 'font-size': 17, }}>
<span>
Contribution sur les boissons contenant des édulcorants de synthèse
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 901, width: 195, height: 16, 'font-size': 17, }}>
<span>
(CGI, art. 1613 quater II 2°)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 669, top: 883, width: 57, height: 13, 'font-size': 17, }}>
<span>
Nombre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 655, top: 901, width: 85, height: 13, 'font-size': 17, }}>
<span>
d’hectolitres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 892, width: 254, height: 13, 'font-size': 17, }}>
<span>
4295 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 941, width: 25, height: 12, 'font-size': 17, }}>
<span>
128
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 931, width: 326, height: 13, 'font-size': 17, }}>
<span>
Contribution sur les sources d’eaux minérales
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 950, width: 112, height: 16, 'font-size': 17, }}>
<span>
(CGI, art. 1582)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 509, top: 931, width: 114, height: 13, 'font-size': 17, }}>
<span>
Code INSEE de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 523, top: 950, width: 87, height: 13, 'font-size': 17, }}>
<span>
la commune
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 669, top: 931, width: 57, height: 13, 'font-size': 17, }}>
<span>
Nombre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 655, top: 950, width: 85, height: 13, 'font-size': 17, }}>
<span>
d’hectolitres
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 800, top: 941, width: 58, height: 12, 'font-size': 17, }}>
<span>
Montant
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 905, top: 941, width: 254, height: 12, 'font-size': 17, }}>
<span>
4293 ………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 980, width: 193, height: 15, 'font-size': 17, }}>
<span>
– Droits pour la commune :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1010, width: 193, height: 15, 'font-size': 17, }}>
<span>
– Droits pour la commune :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1040, width: 193, height: 15, 'font-size': 17, }}>
<span>
– Droits pour la commune :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1069, width: 193, height: 16, 'font-size': 17, }}>
<span>
– Droits pour la commune :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1100, width: 193, height: 15, 'font-size': 17, }}>
<span>
– Droits pour la commune :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1129, width: 193, height: 16, 'font-size': 17, }}>
<span>
– Droits pour la commune :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 146, top: 1159, width: 193, height: 16, 'font-size': 17, }}>
<span>
– Droits pour la commune :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 446, top: 1189, width: 443, height: 16, 'font-size': 17, }}>
<span>
TOTAL DES LIGNES 47 à 128 (à reporter ligne 29 de la CA3)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 968, top: 1199, width: 191, height: 3, 'font-size': 17, }}>
<span>
………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 1243, width: 1130, height: 16, 'font-size': 17, }}>
<span>
Les dispositions des articles 39 et 40 de la loi n° 78-17 du 6 janvier 1978 relatives à l’informatique, aux fichiers et aux libertés, modifiée par la loi n° 2004-801
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 32, top: 1262, width: 899, height: 16, 'font-size': 17, }}>
<span>
du 6 août 2004 garantissent les droits des personnes physiques à l’égard des traitements des données à caractère personnel.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 32, top: 1292, width: 1134, height: 16, 'font-size': 17, }}>
<span>
Si vous réalisez des opérations intracommunautaires, pensez à la déclaration d’échange de biens (livraisons de biens) ou à la déclaration européenne des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 32, top: 1310, width: 1052, height: 16, 'font-size': 17, }}>
<span>
services (prestations de services) à souscrire auprès de la Direction Générale des Douanes et des Droits indirects (cf. notice de la déclaration CA3)
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'MU', false)} className="pde-form-field pdf-annot obj_135 pdf-obj-fixed acroform-field " name="MU" data-field-id="26478952" data-annot-id="25974096" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'MV', false)} className="pde-form-field pdf-annot obj_136 pdf-obj-fixed acroform-field " name="MV" data-field-id="26482104" data-annot-id="27008272" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'MW', false)} className="pde-form-field pdf-annot obj_137 pdf-obj-fixed acroform-field " name="MW" data-field-id="26485256" data-annot-id="27008496" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'MX', false)} className="pde-form-field pdf-annot obj_138 pdf-obj-fixed acroform-field " name="MX" data-field-id="26488408" data-annot-id="27008688" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'PG', false)} className="pde-form-field pdf-annot obj_139 pdf-obj-fixed acroform-field " name="PG" data-field-id="26491560" data-annot-id="27008880" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'NR', false)}  className="pde-form-field pdf-annot obj_140 pdf-obj-fixed acroform-field " name="NR" data-field-id="26494712" data-annot-id="27009072" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'NS', false)}  className="pde-form-field pdf-annot obj_141 pdf-obj-fixed acroform-field " name="NS" data-field-id="26497864" data-annot-id="27023952" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'NC', false)}  className="pde-form-field pdf-annot obj_142 pdf-obj-fixed acroform-field " name="NC" data-field-id="26501016" data-annot-id="27024144" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_143 pdf-obj-fixed acroform-field " name="PK" data-field-id="26504168" data-annot-id="27024336" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_144 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="PJ1" data-field-id="26507320" data-annot-id="27024528" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_145 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="PJ2" data-field-id="26510472" data-annot-id="27024720" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="PJ3" data-field-id="26513624" data-annot-id="27024912" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_147 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="PJ4" data-field-id="26516744" data-annot-id="27025104" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_148 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="PJ5" data-field-id="26519944" data-annot-id="27025296" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_149 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="PJ6" data-field-id="26523064" data-annot-id="27025488" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_150 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="PJ7" data-field-id="26526264" data-annot-id="27025680" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_151 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="PJ8" data-field-id="26529384" data-annot-id="27025872" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_152 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="PJ9" data-field-id="26532584" data-annot-id="27026336" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_153 pdf-obj-fixed acroform-field " name="PH1" data-field-id="26535704" data-annot-id="27026528" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_154 pdf-obj-fixed acroform-field " name="PH2" data-field-id="26538904" data-annot-id="27026720" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_155 pdf-obj-fixed acroform-field " name="PH3" data-field-id="26542024" data-annot-id="27026912" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_156 pdf-obj-fixed acroform-field " name="PH4" data-field-id="26545224" data-annot-id="27027104" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_157 pdf-obj-fixed acroform-field " name="PH5" data-field-id="26548344" data-annot-id="27027296" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_158 pdf-obj-fixed acroform-field " name="PH6" data-field-id="26551544" data-annot-id="27027488" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_159 pdf-obj-fixed acroform-field " name="PH7" data-field-id="26554664" data-annot-id="27027680" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_160 pdf-obj-fixed acroform-field " name="PH8" data-field-id="26557864" data-annot-id="27027872" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_161 pdf-obj-fixed acroform-field " name="PH9" data-field-id="26560984" data-annot-id="27028064" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_162 pdf-obj-fixed acroform-field " name="HB" data-field-id="26564184" data-annot-id="27028256" type="text" disabled />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'PW', false)} className="pde-form-field pdf-annot obj_163 pdf-obj-fixed acroform-field " name="PW" data-field-id="26603128" data-annot-id="27028448" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'PY', false)} className="pde-form-field pdf-annot obj_164 pdf-obj-fixed acroform-field " name="PY" data-field-id="26606296" data-annot-id="27028640" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'QA', false)} className="pde-form-field pdf-annot obj_165 pdf-obj-fixed acroform-field " name="QA" data-field-id="26609464" data-annot-id="27028832" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'PU', false)} className="pde-form-field pdf-annot obj_166 pdf-obj-fixed acroform-field " name="PU" data-field-id="26579912" data-annot-id="27029024" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'PX', false)} className="pde-form-field pdf-annot obj_167 pdf-obj-fixed acroform-field " name="PX" data-field-id="26583064" data-annot-id="27029216" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} setError={setError} formName={form} error={_get(errors, 'PZ', false)}  className="pde-form-field pdf-annot obj_168 pdf-obj-fixed acroform-field " name="PZ" data-field-id="26586216" data-annot-id="27026064" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_169 pdf-obj-fixed acroform-field " name="QE" data-field-id="26589368" data-annot-id="27029936" type="text" disabled />

            <Field component="input" className="pde-form-field pdf-annot obj_170 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="QC1" data-field-id="26390040" data-annot-id="27030128" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_171 pdf-obj-fixed acroform-field " name="QD1" data-field-id="26596792" data-annot-id="27030320" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_172 pdf-obj-fixed acroform-field " name="QB1" data-field-id="26599960" data-annot-id="27030512" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_173 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="QC2" data-field-id="26612632" data-annot-id="27030704" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_174 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="QC3" data-field-id="26615800" data-annot-id="27030896" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_175 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="QC4" data-field-id="26618968" data-annot-id="27031088" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_176 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="QC5" data-field-id="26622136" data-annot-id="27031280" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_177 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="QC6" data-field-id="26625304" data-annot-id="27031472" type="text" />

            <Field component="input" className="pde-form-field pdf-annot obj_178 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="QC7" data-field-id="26628472" data-annot-id="27031664" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_179 pdf-obj-fixed acroform-field " name="QD2" data-field-id="26631640" data-annot-id="27031856" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_180 pdf-obj-fixed acroform-field " name="QD3" data-field-id="26634808" data-annot-id="27032048" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_181 pdf-obj-fixed acroform-field " name="QD4" data-field-id="26637976" data-annot-id="27032240" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_182 pdf-obj-fixed acroform-field " name="QD5" data-field-id="26641144" data-annot-id="27032432" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_183 pdf-obj-fixed acroform-field " name="QD6" data-field-id="26644312" data-annot-id="27032624" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_184 pdf-obj-fixed acroform-field " name="QD7" data-field-id="26647480" data-annot-id="27032816" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_185 pdf-obj-fixed acroform-field " name="QB2" data-field-id="26650616" data-annot-id="27033008" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_186 pdf-obj-fixed acroform-field " name="QB3" data-field-id="26653736" data-annot-id="27033200" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_187 pdf-obj-fixed acroform-field " name="QB4" data-field-id="26656936" data-annot-id="27033392" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_188 pdf-obj-fixed acroform-field " name="QB5" data-field-id="26660056" data-annot-id="27033584" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_189 pdf-obj-fixed acroform-field " name="QB6" data-field-id="26663256" data-annot-id="27033776" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_190 pdf-obj-fixed acroform-field " name="QB7" data-field-id="26666376" data-annot-id="27033968" type="text" />

          </div>
        </div>
      </div>
    </div>
  );
}

export default TVA3310;
