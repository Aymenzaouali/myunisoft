import React from 'react';
import _get from 'lodash/get';
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import { ReduxPicker } from 'components/reduxForm/Inputs';
import { RibAutoComplete } from 'containers/reduxForm/Inputs';
import { setValue } from 'helpers/pdfforms';

import './style.scss';


const overlapErrorStyle = { background: 'white' };

const TVA3519 = (props) => {
  const {
    tvaFormValues, change, errors, validateForm, validateRadio, tvaCa3
  } = props;

  const DN = _get(tvaCa3, 'JB', 0);
  setValue(tvaFormValues, 'DN', DN, change);

  const DLError = _get(errors, 'DL', false);
  const DMError = _get(errors, 'DM', false);
  const DEMAND = _get(tvaFormValues, 'DEMAND', false);
  const DL = _get(tvaFormValues, 'DL', false);
  const DM = _get(tvaFormValues, 'DM', false);
  if (DEMAND) {
    if (DEMAND === 'DI' && DLError && DM) {
      setValue(tvaFormValues, 'DM', '', change);
    } else if (DEMAND === 'DJ' && DMError && DL) {
      setValue(tvaFormValues, 'DL', '', change);
    } else if (DEMAND === 'DK' && (DL || DM)) {
      setValue(tvaFormValues, 'DM', '', change);
      setValue(tvaFormValues, 'DL', '', change);
    }
  }

  return (
    <div className="tva3519">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div
              className="pdf-obj-fixed"
              style={{
                left: 464, top: 49, width: 373, height: 18, 'font-size': 23
              }}
            >
              <span>
3519 : demande de remboursement
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 436, top: 97, width: 329, height: 15, 'font-size': 21
              }}
            >
              <span>
ADRESSE DE CORRESPONDANCE
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 502, top: 120, width: 195, height: 18, 'font-size': 21
              }}
            >
              <span>
OU NOM, PRENOMS
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 128, top: 142, width: 955, height: 19, 'font-size': 21
              }}
            >
              <span>
OU DENOMINATION ET ADRESSE DU REPRESENTANT FISCAL OU DU LIQUIDATEUR JUDICIAIRE
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 171, width: 193, height: 19, 'font-size': 21
              }}
            >
              <span>
Forme juridique ou titre
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 605, top: 171, width: 202, height: 19, 'font-size': 21
              }}
            >
              <span>
ComplÈment distribution
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 207, width: 41, height: 14, 'font-size': 21
              }}
            >
              <span>
Nom
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 604, top: 206, width: 66, height: 15, 'font-size': 21
              }}
            >
              <span>
LocalitÈ
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 35, top: 241, width: 59, height: 19, 'font-size': 21
              }}
            >
              <span>
QualitÈ
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 605, top: 241, width: 95, height: 19, 'font-size': 21
              }}
            >
              <span>
Code postal
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 275, width: 170, height: 20, 'font-size': 21
              }}
            >
              <span>
N  , Type, nom voie :
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 604, top: 292, width: 48, height: 18, 'font-size': 21
              }}
            >
              <span>
Pays :
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 343, width: 37, height: 15, 'font-size': 21
              }}
            >
              <span>
TÈl :
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 604, top: 343, width: 58, height: 15, 'font-size': 21
              }}
            >
              <span>
Email :
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 417, width: 114, height: 15, 'font-size': 21
              }}
            >
              <span>
NationalitÈ de
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 439, width: 89, height: 19, 'font-size': 21
              }}
            >
              <span>
l&#39;entreprise
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 202, top: 378, width: 160, height: 19, 'font-size': 21
              }}
            >
              <span>
Entreprise franÁaise
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 202, top: 434, width: 119, height: 19, 'font-size': 21
              }}
            >
              <span>
Entreprise non
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 202, top: 457, width: 137, height: 15, 'font-size': 21
              }}
            >
              <span>
Ètablie en France
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 370, top: 420, width: 460, height: 20, 'font-size': 21
              }}
            >
              <span>
n&#39;ayant pas l&#39;obligation de dÈsigner un reprÈsentant fiscal
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 375, top: 470, width: 414, height: 20, 'font-size': 21
              }}
            >
              <span>
ayant l&#39;obligation de dÈsigner un reprÈsentant fiscal
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 421, top: 513, width: 358, height: 15, 'font-size': 21
              }}
            >
              <span>
II. DEMANDE DE REMBOURSEMENT
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 323, top: 552, width: 210, height: 15, 'font-size': 21
              }}
            >
              <span>
Remboursement demandÈ
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 887, top: 553, width: 14, height: 14, 'font-size': 21
              }}
            >
              <span>
R
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 202, top: 586, width: 453, height: 20, 'font-size': 21
              }}
            >
              <span>
Le soussignÈ (titre, nom prÈnom, qualitÈ) du demandeur
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 831, top: 586, width: 39, height: 16, 'font-size': 21
              }}
            >
              <span>
Titre
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 693, width: 185, height: 19, 'font-size': 21
              }}
            >
              <span>
A (lieu de la demande)
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 762, width: 236, height: 20, 'font-size': 21
              }}
            >
              <span>
A crÈditer au compte dÈsignÈ
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 40, top: 832, width: 620, height: 19, 'font-size': 21
              }}
            >
              <span>
(utiliser l&#39;imprimÈ papier 3516 disponible sur le portail www.impots.gouv.fr)
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 797, width: 244, height: 19, 'font-size': 21
              }}
            >
              <span>
A imputer sur ÈchÈance future
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 35, top: 882, width: 1109, height: 19, 'font-size': 21
              }}
            >
              <span>
S&#39;agissant d&#39;une premiere demande de remboursement ou de l&#39;utilisation d&#39;un compte financier nouveau par rapport ‡ celui prÈcÈdemment
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 35, top: 904, width: 732, height: 20, 'font-size': 21
              }}
            >
              <span>
dÈsignÈ pour procÈder ‡ ce type d&#39;opÈration, la rÈfÈrence du compte financier ‡ utiliser est :
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 510, top: 968, width: 179, height: 15, 'font-size': 21
              }}
            >
              <span>
RefÈrence Bancaire :
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 153, top: 1018, width: 102, height: 19, 'font-size': 21
              }}
            >
              <span>
BIC (SEPA)
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 154, top: 1087, width: 159, height: 19, 'font-size': 21
              }}
            >
              <span>
Titulaire du compte
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 154, top: 1052, width: 116, height: 20, 'font-size': 21
              }}
            >
              <span>
IBAN (SEPA)
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 154, top: 1122, width: 345, height: 19, 'font-size': 21
              }}
            >
              <span>
Titulaire du compte (ComplÈment de nom)
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 1260, width: 225, height: 20, 'font-size': 21
              }}
            >
              <span>
Demande dÈposÈe suite ‡ :
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 1300, width: 151, height: 16, 'font-size': 21
              }}
            >
              <span>
Premiere demande
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 35, top: 1337, width: 436, height: 20, 'font-size': 21
              }}
            >
              <span>
Cession, cessation, dÈcËs, entrÈe dans un groupe TVA
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 34, top: 1379, width: 54, height: 15, 'font-size': 21
              }}
            >
              <span>
Autres
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 853, top: 1302, width: 85, height: 16, 'font-size': 21
              }}
            >
              <span>
crÈation le
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 894, top: 1338, width: 31, height: 15, 'font-size': 21
              }}
            >
              <span>
Le†:
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 530, top: 1423, width: 136, height: 15, 'font-size': 21
              }}
            >
              <span>
Commentaires :
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 831, top: 622, width: 108, height: 18, 'font-size': 21
              }}
            >
              <span>
Nom prÈnom
                <br />
              </span>
            </div>
            <div
              className="pdf-obj-fixed"
              style={{
                left: 832, top: 656, width: 59, height: 19, 'font-size': 21
              }}
            >
              <span>
QualitÈ
                <br />
              </span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="NATIONALITY" data-field-id="16192808" data-annot-id="15824704" value="DD" type="radio" data-default-value="Off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="NATIONALITY" data-field-id="16197432" data-annot-id="15923648" value="DE" type="radio" data-default-value="Off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="NATIONALITY" data-field-id="16197768" data-annot-id="15915024" value="DF" type="radio" data-default-value="Off" onBlur={validateForm} />

            <Field
              component="input"
              className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field "
              name="DEMAND"
              data-field-id="16198072"
              data-annot-id="15915216"
              value="DI"
              type="radio"
              data-default-value="Off"
              onChange={e => validateRadio(tvaFormValues, e.target.value)}
            />

            <Field
              component="input"
              className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field "
              name="DEMAND"
              data-field-id="16198408"
              data-annot-id="15814672"
              value="DJ"
              type="radio"
              data-default-value="Off"
              onChange={e => validateRadio(tvaFormValues, e.target.value)}
            />

            <Field
              component="input"
              className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field "
              name="DEMAND"
              data-field-id="16198744"
              data-annot-id="15814864"
              value="DK"
              type="radio"
              data-default-value="Off"
              onChange={e => validateRadio(tvaFormValues, e.target.value)}
            />

            <Field component="input" className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="DCC" data-field-id="16199080" data-annot-id="15639424" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field pde-form-field-text" name="DCA" data-field-id="16199416" data-annot-id="15639616" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field pde-form-field-text" name="DCB" data-field-id="16199752" data-annot-id="15639808" type="text" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field pde-form-field-text" name="DCD" data-field-id="16208104" data-annot-id="15640000" type="text" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field pde-form-field-text" name="DCI" data-field-id="16200232" data-annot-id="15640192" type="text" autocomplete="off" onBlur={validateForm} />

            <Field
              component={PdfNumberInput}
              max="5"
              className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field "
              name="DCH"
              data-field-id="16200568"
              data-annot-id="15640384"
              type="text"
              error={_get(errors, 'DCH', false)}
              errorStyle={overlapErrorStyle}
              onBlur={validateForm}
              noformat
            />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="DN" data-field-id="16200904" data-annot-id="15640576" type="text" disabled onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field pde-form-field-text" name="DGC" data-field-id="16201240" data-annot-id="15640768" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field pde-form-field-text" name="DGA" data-field-id="16201576" data-annot-id="15930608" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field pde-form-field-text" name="DGD" data-field-id="16201912" data-annot-id="15930800" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field pde-form-field-text" name="DGI" data-field-id="16202248" data-annot-id="15930992" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="FK" data-field-id="16202584" data-annot-id="15931456" type="text" onBlur={validateForm} />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="FL" data-field-id="16203064" data-annot-id="15931648" type="text" onBlur={validateForm} />

            <Field component={RibAutoComplete} onChangeValues={(value) => { change('AAE', value.bic) }} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="AAC" data-field-id="16203400" data-annot-id="15931840" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field pde-form-field-text-capital" name="AAE" data-field-id="16203736" data-annot-id="15932032" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field pde-form-field-text" name="AAA" data-field-id="16204072" data-annot-id="15932224" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field pde-form-field-text" name="AAB" data-field-id="16204408" data-annot-id="15932416" type="text" autocomplete="off" onBlur={validateForm} />

            <Field
              component={ReduxPicker} isClearable className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field "
              name="DL" data-field-id="16204744" data-annot-id="15932608" type="text"
              error={_get(errors, 'DL', false)}
              handleChange={(name, date) => {
                change(name, date);
                validateRadio(tvaFormValues, false, {[name]: date})
              }}
              disabled={!DLError}
              isError={DLError}
            />

            <Field
              component={ReduxPicker} isClearable className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field "
              name="DM" data-field-id="16205080" data-annot-id="15932800" type="text"
              error={_get(errors, 'DM', false)}
              handleChange={(name, date) => {
                change(name, date);
                validateRadio(tvaFormValues, false, {[name]: date})
              }}
              disabled={!DMError}
              isError={DMError}
            />

            <Field component="input" className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field pde-form-field-text" name="FJA" data-field-id="16205416" data-annot-id="15932992" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field pde-form-field-text" name="FJB" data-field-id="16205752" data-annot-id="15933184" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field pde-form-field-text" name="FJC" data-field-id="16206088" data-annot-id="15933376" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field pde-form-field-text" name="FJD" data-field-id="16206424" data-annot-id="15933568" type="text" autocomplete="off" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field pde-form-field-text" name="FJE" data-field-id="16206760" data-annot-id="15933760" type="text" autocomplete="off" onBlur={validateForm} />

            <Field
              component={PdfNumberInput}
              max="13"
              className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field "
              name="DCZ"
              data-field-id="16207768"
              data-annot-id="15933952"
              type="text"
              onBlur={validateForm}
            />

            <Field component="input" className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field pde-form-field-text" name="DCJ" autocomplete="off" data-field-id="16207432" data-annot-id="15934144" type="text" onBlur={validateForm} />

            <Field
              component={ReduxTextField}
              className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field pde-form-field-text"
              name="DCW"
              data-field-id="16207096"
              data-annot-id="15934336"
              type="email"
              onBlur={validateForm}
              error={_get(errors, 'DCW', false)}
              errorStyle={overlapErrorStyle}
            />

            <Field component="input" className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field pde-form-field-text" name="DCF" data-field-id="16202920" data-annot-id="15931184" type="text" onBlur={validateForm} />

            <Field component="input" className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field pde-form-field-text" name="DCG" data-field-id="16209032" data-annot-id="15935056" type="text" onBlur={validateForm} />

          </div>
        </div>
      </div>
    </div>
  );
};

export default TVA3519;
