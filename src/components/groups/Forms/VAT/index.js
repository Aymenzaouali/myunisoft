import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import pdfform from 'pdfform.js';
import Button from 'components/basics/Buttons/Button';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { ReduxCheckBox, ReduxSelect } from 'components/reduxForm/Selections';
import Viewer from 'components/basics/Viewer';
import _ from 'lodash';
import styles from './VATForm.module.scss';

class VATForm extends PureComponent {
  state = {
    file: undefined
  }

  submit = (payload) => {
    const values = {};
    const { data } = this.state;
    _.forEach(payload, (value, key) => { values[key] = [value]; });
    const array_buffer = pdfform().transform(data, values);
    const file = new File([array_buffer], 'test.pdf', { type: 'application/pdf' });
    this.setState({ file });
  }

  onFileChange = (e) => {
    try {
      const FileRead = new FileReader();
      FileRead.onload = () => {
        const data = FileRead.result;
        const fields = pdfform().list_fields(data);
        this.setState({ fields, data });
      };
      FileRead.readAsArrayBuffer(e.target.files[0]);
    } catch (err) {
      console.log('err : ', err); // eslint-disable-line
    }
  }

  renderFields = () => {
    const { fields } = this.state;

    const fieldsComponent = [];

    _.forEach(fields, ((field, key) => field.forEach(f => fieldsComponent.push((
      <div>
        <div className={styles.row}>
          <div>{key}</div>
          <div>{key}</div>
        </div>
        {f.type === 'string' && (
          <Field
            name={key}
            component={ReduxTextField}
            margin="none"
            type={key.match('date') ? 'date' : 'text'}
          />
        )}
        {f.type === 'boolean' && (
          <Field
            name={key}
            component={ReduxCheckBox}
            margin="none"
          />
        )}
        {f.type === 'select' && (
          <Field
            name={key}
            component={ReduxSelect}
            list={_.get(f, 'options', []).map(value => ({ value, label: value }))}
            margin="none"
          />
        )}
      </div>
    )))));

    return fieldsComponent;
  }

  render() {
    const { handleSubmit } = this.props;
    const { file } = this.state;

    const fieldsComponent = this.renderFields();

    return (
      <div className={styles.container}>
        <input
          type="file"
          onChange={this.onFileChange}
        />
        <div>
          {file && (
            <Viewer
              file={file}
            />
          )}
          {fieldsComponent.length > 0 && (
            <Fragment>
              {fieldsComponent}
              <Button type="submit" onClick={handleSubmit(this.submit)}>Submit</Button>
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}

VATForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired
};

export default VATForm;
