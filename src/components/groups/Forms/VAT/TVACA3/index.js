/* eslint-disable */
import React, { useState } from 'react';
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { parse as p, sum, useCompute, copy, isEmpty, setValue, sumOfValues } from "helpers/pdfforms";
import { PdfCheckbox } from 'components/reduxForm/Inputs';
import _get from 'lodash/get';
import './style.scss';
import lodash from "lodash";

const roundField = (field) => {
  let floatField = p(field);
  return floatField < 0 ? Math.round(floatField * -1) * -1 : Math.round(floatField)
};

const calcGB = (values, fields) => roundField(roundField(values[fields[0]]) * 0.055);
const calcGM = (values, fields) => roundField(roundField(values[fields[0]]) * 0.085);
const calcGN = (values, fields) => roundField(roundField(values[fields[0]]) * 0.021);
const calcGP = (values, fields) => roundField(roundField(values[fields[0]]) * 0.20);
const calcGR = (values, fields) => roundField(roundField(values[fields[0]]) * 0.10);
const calcKT = (values, fields) => roundField(roundField(values[fields[0]]) * 0.2);

const calcJA = ({ HG, GH }) => {
  const field23 = roundField(HG), field16 = roundField(GH);
  const diff = field23 - field16;
  return diff > 0 ? diff : 0;
};

const calcCJ = (values, fields) => roundField(values[fields[0]]) * 0.2;

const calcKA = (values, fields) => {
  const diff = roundField(values[fields[0]]) - roundField(values[fields[1]]);
  return diff > 0 ? diff : 0;
};

const calcFP = ({KH, CF, CC, KR}) => roundField(KH) + roundField(CF) + roundField(CC) + roundField(KR);
const calcJC = ({JA, JB, KJ}) => roundField(JA) - roundField(JB) - roundField(KJ);

 const calculateGH = ({GP, GB, KS, GR, GM, GN, GC, GD, GG, FK}) => {
   return roundField(GP) + roundField(GB) + roundField(KS) + roundField(GR) + roundField(GM) + roundField(GN) + roundField(GC) + roundField(GD) + roundField(GG) + roundField(FK);
 };


const TVACA3 = (props) => {
  const { change, tvaForm, tva3310Form, form, errors, setError } = props;
  const GD = lodash.get(tva3310Form, 'CN', 0);
  setValue(tvaForm, 'GD', GD, change);
  const [needCalc, setNeedCalc] = useState(false);

  useCompute('GB', calcGB, ['FB'], tvaForm, change);
  useCompute('GM', calcGM, ['FM'], tvaForm, change);
  useCompute('GN', calcGN, ['FN'], tvaForm, change);
  useCompute('GP', calcGP, ['FP'], tvaForm, change);
  useCompute('GR', calcGR, ['FR'], tvaForm, change);
  useCompute('KT', calcKT, ['KR'], tvaForm, change);
  useCompute('GH', calculateGH, ['GP', 'GB', 'KS', 'GR', 'GM', 'GN', 'GC', 'GD', 'GG', 'FK'], tvaForm, change);
  useCompute('CA', ({FP, FB, FR, FM, FN, FJ, FC, FD, CB, KH, KR, CC, CF, CG}) => (roundField(FP) + roundField(FB) + roundField(FR) + roundField(FM) + roundField(FN) + roundField(FJ) + roundField(FC) + roundField(FD)) - (roundField(CB) + roundField(KH) + roundField(KR) + roundField(CC) + roundField(CF) + roundField(CG)),
       ['FB', 'FP', 'FR', 'FM', 'FN', 'FJ', 'FC', 'CB', 'KH', 'KR', 'CC', 'CF', 'CG', 'FD', 'CE'], tvaForm, change);
  useCompute('HG', sum, ['HA', 'HB', 'HC', 'HD', 'KU'], tvaForm, change);
  useCompute('JA', calcJA, ['HG', 'GH'], tvaForm, change);
  useCompute('KA', calcKA, ['GH', 'HG'], tvaForm, change);
  useCompute('JC', calcJC, ['JA', 'JB', 'KJ'], tvaForm, change);
  useCompute('KE', ({ KA, KB, KL }) => roundField(KA) + roundField(KB) - roundField(KL), ['KA', 'KB', 'KL'], tvaForm, change);
  useCompute('FP', calcFP, ['KH', 'CF', 'CC', 'KR'],  tvaForm, needCalc && change);
  useCompute('GJ', calcCJ, ['CC', 'GJ' ], tvaForm, change);


  return (
    <div className="TVACA3">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 43, top: 34, width: 313, height: 14, 'font-size': 15, }}>
<span>
A MONTANT DES OPÉRATIONS RÉALISÉES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 63, width: 227, height: 16, 'font-size': 15, }}>
<span>
OPÉRATIONS IMPOSABLES (H.T.)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 63, width: 224, height: 14, 'font-size': 15, }}>
<span>
OPÉRATIONS NON IMPOSABLES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 94, width: 228, height: 14, 'font-size': 15, }}>
<span>
01 Ventes, prestations de services
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 94, width: 340, height: 14, 'font-size': 15, }}>
<span>
0979 ………………… 04 Exportations hors UE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 94, width: 153, height: 11, 'font-size': 15, }}>
<span>
0032 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 121, width: 219, height: 14, 'font-size': 15, }}>
<span>
02 Autres opérations imposables
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 121, width: 420, height: 14, 'font-size': 15, }}>
<span>
0981 ………………… 05 Autres opérations non imposables
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 121, width: 153, height: 12, 'font-size': 15, }}>
<span>
0033 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 165, width: 18, height: 11, 'font-size': 15, }}>
<span>
2A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 149, width: 211, height: 14, 'font-size': 15, }}>
<span>
Achats de prestations de services
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 165, width: 131, height: 11, 'font-size': 15, }}>
<span>
intracommunautaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 181, width: 263, height: 14, 'font-size': 15, }}>
<span>
(article 283-2 du code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 165, width: 193, height: 11, 'font-size': 15, }}>
<span>
0044 ………………… 5A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 154, width: 343, height: 14, 'font-size': 15, }}>
<span>
Ventes à distance taxables dans un autre État membre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 173, width: 339, height: 14, 'font-size': 15, }}>
<span>
au profit des personnes non assujetties – Ventes BtoC
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 165, width: 153, height: 11, 'font-size': 15, }}>
<span>
0047 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 216, width: 17, height: 12, 'font-size': 15, }}>
<span>
2B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 208, width: 332, height: 14, 'font-size': 15, }}>
<span>
Importations (entreprises ayant opté pour le dispositif
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 225, width: 266, height: 14, 'font-size': 15, }}>
<span>
d’autoliquidation de la TVA à l’importation).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 216, width: 31, height: 12, 'font-size': 15, }}>
<span>
0045
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 489, top: 233, width: 97, height: 3, 'font-size': 15, }}>
<span>
…………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 610, top: 216, width: 15, height: 12, 'font-size': 15, }}>
<span>
06
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 208, width: 325, height: 12, 'font-size': 15, }}>
<span>
Livraisons intracommunautaires à destination d&#39;une
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 225, width: 208, height: 14, 'font-size': 15, }}>
<span>
personne assujettie- Ventes BtoB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 216, width: 153, height: 12, 'font-size': 15, }}>
<span>
0034 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 260, width: 244, height: 14, 'font-size': 15, }}>
<span>
03 Acquisitions intracommunautaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 260, width: 193, height: 11, 'font-size': 15, }}>
<span>
0031 ………………… 6A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 252, width: 348, height: 14, 'font-size': 15, }}>
<span>
Livraisons d’électricité, de gaz naturel, de chaleur ou de
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 268, width: 197, height: 14, 'font-size': 15, }}>
<span>
froid non imposables en France
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 260, width: 153, height: 11, 'font-size': 15, }}>
<span>
0029 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 304, width: 18, height: 11, 'font-size': 15, }}>
<span>
3A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 295, width: 327, height: 15, 'font-size': 15, }}>
<span>
Livraisons d’électricité, de gaz naturel, de chaleur ou
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 312, width: 190, height: 14, 'font-size': 15, }}>
<span>
de froid imposables en France
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 303, width: 331, height: 12, 'font-size': 15, }}>
<span>
0030 ………………… 07 Achats en franchise
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 304, width: 153, height: 11, 'font-size': 15, }}>
<span>
0037 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 355, width: 17, height: 12, 'font-size': 15, }}>
<span>
3B
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 339, width: 340, height: 14, 'font-size': 15, }}>
<span>
Achats de biens ou de prestations de services réalisés
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 355, width: 340, height: 14, 'font-size': 15, }}>
<span>
auprès d’un assujetti non établi en France (article 283-
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 371, width: 189, height: 15, 'font-size': 15, }}>
<span>
1 du code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 355, width: 193, height: 12, 'font-size': 15, }}>
<span>
0040 ………………… 7A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 339, width: 329, height: 14, 'font-size': 15, }}>
<span>
Ventes de biens ou prestations de services réalisées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 355, width: 333, height: 14, 'font-size': 15, }}>
<span>
par un assujetti non établi en France (article 283-1 du
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 371, width: 158, height: 15, 'font-size': 15, }}>
<span>
code général des impôts)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 355, width: 153, height: 12, 'font-size': 15, }}>
<span>
0043 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 399, width: 272, height: 14, 'font-size': 15, }}>
<span>
3C Régularisations (important : cf. notice)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 399, width: 444, height: 14, 'font-size': 15, }}>
<span>
0036 ………………… 7B Régularisations (important : cf. notice)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 399, width: 153, height: 11, 'font-size': 15, }}>
<span>
0039 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 44, top: 424, width: 251, height: 14, 'font-size': 15, }}>
<span>
B DÉCOMPTE DE LA TVA À PAYER
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 454, width: 79, height: 11, 'font-size': 15, }}>
<span>
TVA BRUTE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 934, top: 454, width: 100, height: 11, 'font-size': 15, }}>
<span>
Base hors taxe
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1079, top: 454, width: 60, height: 11, 'font-size': 15, }}>
<span>
Taxe due
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 481, width: 310, height: 14, 'font-size': 15, }}>
<span>
Opérations réalisées en France métropolitaine
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 508, width: 146, height: 12, 'font-size': 15, }}>
<span>
08 Taux normal 20 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 508, width: 278, height: 12, 'font-size': 15, }}>
<span>
0207 ………………… …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 536, width: 142, height: 13, 'font-size': 15, }}>
<span>
09 Taux réduit 5,5 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 536, width: 278, height: 11, 'font-size': 15, }}>
<span>
0105 ………………… …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 563, width: 139, height: 12, 'font-size': 15, }}>
<span>
9B Taux réduit 10 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 563, width: 278, height: 11, 'font-size': 15, }}>
<span>
0151 ………………… …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 590, width: 235, height: 14, 'font-size': 15, }}>
<span>
Opérations réalisées dans les DOM
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 618, width: 149, height: 13, 'font-size': 15, }}>
<span>
10 Taux normal 8,5 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 618, width: 278, height: 11, 'font-size': 15, }}>
<span>
0201 ………………… …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 645, width: 140, height: 13, 'font-size': 15, }}>
<span>
11 Taux réduit 2,1 %
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 645, width: 278, height: 11, 'font-size': 15, }}>
<span>
0100 ………………… …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 672, width: 525, height: 12, 'font-size': 15, }}>
<span>
12 ……………………………………………………………………………………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 936, top: 681, width: 222, height: 3, 'font-size': 15, }}>
<span>
………………… …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 700, width: 478, height: 14, 'font-size': 15, }}>
<span>
Opérations imposables à un autre taux (France métropolitaine ou DOM)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 727, width: 114, height: 11, 'font-size': 15, }}>
<span>
13 Anciens taux
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 727, width: 278, height: 11, 'font-size': 15, }}>
<span>
0900 ………………… …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 754, width: 554, height: 15, 'font-size': 15, }}>
<span>
14 Opérations imposables à un taux particulier (décompte effectué sur annexe 3310 A)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 754, width: 278, height: 12, 'font-size': 15, }}>
<span>
0950 ………………… …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 782, width: 275, height: 11, 'font-size': 15, }}>
<span>
15 TVA antérieurement déduite à reverser
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 782, width: 153, height: 11, 'font-size': 15, }}>
<span>
0600 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 809, width: 453, height: 14, 'font-size': 15, }}>
<span>
5B Sommes à ajouter, y compris acompte congés (exprimées en euro)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 809, width: 153, height: 11, 'font-size': 15, }}>
<span>
0602 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 877, width: 252, height: 15, 'font-size': 15, }}>
<span>
La ligne 11 ne concerne que les DOM.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 893, width: 507, height: 15, 'font-size': 15, }}>
<span>
Les autres opérations relevant du taux de 2,1 % sont déclarées sur l&#39;annexe
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 910, width: 73, height: 11, 'font-size': 15, }}>
<span>
3310 A-SD.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 611, top: 836, width: 304, height: 15, 'font-size': 15, }}>
<span>
16 Total de la TVA brute due (lignes 08 à 5B)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1061, top: 845, width: 97, height: 3, 'font-size': 15, }}>
<span>
…………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 609, top: 872, width: 17, height: 11, 'font-size': 15, }}>
<span>
7C
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 864, width: 345, height: 14, 'font-size': 15, }}>
<span>
Dont TVA sur importations bénéficiant du dispositif
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 880, width: 113, height: 14, 'font-size': 15, }}>
<span>
d’autoliquidation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 872, width: 153, height: 11, 'font-size': 15, }}>
<span>
0046 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 611, top: 908, width: 352, height: 13, 'font-size': 15, }}>
<span>
17 Dont TVA sur acquisitions intracommunautaires
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 908, width: 153, height: 11, 'font-size': 15, }}>
<span>
0035 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 611, top: 935, width: 363, height: 14, 'font-size': 15, }}>
<span>
18 Dont TVA sur opérations à destination de Monaco
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 935, width: 153, height: 11, 'font-size': 15, }}>
<span>
0038 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 960, width: 122, height: 13, 'font-size': 15, }}>
<span>
TVA DÉDUCTIBLE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 990, width: 267, height: 11, 'font-size': 15, }}>
<span>
19 Biens constituant des immobilisations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 990, width: 153, height: 11, 'font-size': 15, }}>
<span>
0703 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1017, width: 183, height: 11, 'font-size': 15, }}>
<span>
20 Autres biens et services
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 1017, width: 153, height: 11, 'font-size': 15, }}>
<span>
0702 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1052, width: 14, height: 11, 'font-size': 15, }}>
<span>
21
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1044, width: 126, height: 12, 'font-size': 15, }}>
<span>
Autre TVA à déduire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1060, width: 680, height: 15, 'font-size': 15, }}>
<span>
(dont régularisation sur de la TVA collectée [cf. notice] ......................................................................................)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 1052, width: 153, height: 12, 'font-size': 15, }}>
<span>
0059 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1088, width: 453, height: 14, 'font-size': 15, }}>
<span>
22 Report du crédit apparaissant ligne 27 de la précédente déclaration
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 1088, width: 153, height: 11, 'font-size': 15, }}>
<span>
8001 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1115, width: 458, height: 14, 'font-size': 15, }}>
<span>
2C Sommes à imputer, y compris acompte congés (exprimées en euro)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 1115, width: 153, height: 12, 'font-size': 15, }}>
<span>
0603 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 1156, width: 26, height: 12, 'font-size': 15, }}>
<span>
22A
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1148, width: 399, height: 14, 'font-size': 15, }}>
<span>
Indiquer le coefficient de taxation unique applicable pour la
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1164, width: 144, height: 14, 'font-size': 15, }}>
<span>
période s&#39;il est différent
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 1156, width: 12, height: 12, 'font-size': 15, }}>
<span>
%
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 610, top: 1143, width: 268, height: 14, 'font-size': 15, }}>
<span>
23 Total TVA déductible (ligne 19 à 2C)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1063, top: 1143, width: 2, height: 14, 'font-size': 15, }}>
<span>
|
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1065, top: 1143, width: 90, height: 14, 'font-size': 15, }}>
<span>
___________ |
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 610, top: 1170, width: 262, height: 14, 'font-size': 15, }}>
<span>
24 Dont TVA déductible sur importations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 1170, width: 32, height: 11, 'font-size': 15, }}>
<span>
0710
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1196, width: 52, height: 14, 'font-size': 15, }}>
<span>
CRÉDIT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1227, width: 248, height: 14, 'font-size': 15, }}>
<span>
25 Crédit de TVA (ligne 23 – ligne 16)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 1227, width: 437, height: 14, 'font-size': 15, }}>
<span>
0705 ………………… 28 TVA nette due (ligne 16 – ligne 23).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1061, top: 1236, width: 97, height: 2, 'font-size': 15, }}>
<span>
…………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1263, width: 15, height: 11, 'font-size': 15, }}>
<span>
26
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1254, width: 331, height: 12, 'font-size': 15, }}>
<span>
Remboursement de crédit demandé sur formulaire n°
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1271, width: 61, height: 14, 'font-size': 15, }}>
<span>
3519 joint
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 1263, width: 539, height: 11, 'font-size': 15, }}>
<span>
8002 ………………… 29 Taxes assimilées calculées sur annexe n° 3310 A-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 1263, width: 153, height: 11, 'font-size': 15, }}>
<span>
9979 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1315, width: 20, height: 11, 'font-size': 15, }}>
<span>
AA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1306, width: 340, height: 14, 'font-size': 15, }}>
<span>
Crédit de TVA transféré à la société tête de groupe sur
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1322, width: 248, height: 14, 'font-size': 15, }}>
<span>
la déclaration récapitulative 3310-CA3G
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 1314, width: 193, height: 12, 'font-size': 15, }}>
<span>
8005 ………………… AB
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1298, width: 341, height: 14, 'font-size': 15, }}>
<span>
Total à payer acquitté par la société tête de groupe sur
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1314, width: 249, height: 14, 'font-size': 15, }}>
<span>
la déclaration récapitulative 3310-CA3G
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1330, width: 97, height: 15, 'font-size': 15, }}>
<span>
(lignes 28 + 29)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1005, top: 1314, width: 153, height: 12, 'font-size': 15, }}>
<span>
9991 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1380, width: 16, height: 11, 'font-size': 15, }}>
<span>
27
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1358, width: 103, height: 14, 'font-size': 15, }}>
<span>
Crédit à reporter
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1374, width: 191, height: 14, 'font-size': 15, }}>
<span>
(ligne 25 – ligne 26 – ligne AA)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1390, width: 282, height: 12, 'font-size': 13, }}>
<span>
(Cette somme est à reporter ligne 22 de la prochaine
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1404, width: 64, height: 12, 'font-size': 13, }}>
<span>
déclaration)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 1380, width: 152, height: 11, 'font-size': 15, }}>
<span>
8003 …………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 610, top: 1412, width: 15, height: 12, 'font-size': 15, }}>
<span>
32
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1404, width: 226, height: 14, 'font-size': 15, }}>
<span>
Total à payer (lignes 28 + 29 – AB)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1420, width: 335, height: 15, 'font-size': 15, }}>
<span>
(N’oubliez pas de joindre le règlement correspondant)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1022, top: 1412, width: 2, height: 14, 'font-size': 15, }}>
<span>
|
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1024, top: 1412, width: 121, height: 14, 'font-size': 15, }}>
<span>
_______________ |
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1430, width: 472, height: 16, 'font-size': 17, }}>
<span>
Attention ! Une situation de TVA créditrice (ligne 25 servie) ne
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 1448, width: 513, height: 17, 'font-size': 17, }}>
<span>
dispense pas du paiement des taxes assimilées déclarées ligne 29.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 29, top: 1476, width: 1134, height: 13, 'font-size': 13, }}>
<span>
Les dispositions des articles 39 et 40 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, garantissent les droits des personnes physiques à l’égard des traitements des données
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 29, top: 1490, width: 119, height: 12, 'font-size': 13, }}>
<span>
à caractère personnel.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 29, top: 1504, width: 1141, height: 13, 'font-size': 13, }}>
<span>
Si vous réalisez des opérations intracommunautaires, pensez à la déclaration d’échanges de biens (livraisons de biens) ou à la déclaration européenne de services (prestations de services) à souscrire auprès de la
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 29, top: 1518, width: 476, height: 12, 'font-size': 13, }}>
<span>
Direction Générale des Douanes et des Droits indirects (cf. notice de la déclaration CA3).
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="CA"
                                 data-field-id="35671256" data-annot-id="34417632" type="text" disabled/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="CB"
                                 data-field-id="35676072" data-annot-id="34423216" type="text"/>

              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="KH"
                                 data-field-id="35690792" data-annot-id="34423408" type="text" onChange={() => setNeedCalc(true)}/> 
               <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="KR"
                                 data-field-id="35603608" data-annot-id="34425856" type="text" onChange={() => setNeedCalc(true)}/>                  
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="CC"
                                 data-field-id="35695768" data-annot-id="34423600" type="text" onChange={() => setNeedCalc(true)}/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="CF"
                                 data-field-id="35697688" data-annot-id="34423024" type="text" onChange={() => setNeedCalc(true)}/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="CG"
                                 data-field-id="35671560" data-annot-id="34420640" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="CE"
                                 data-field-id="35672952" data-annot-id="34420832" type="text"/>

              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="DA"
                                 data-field-id="35673288" data-annot-id="34421024" type="text"/>

              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="DB"
                                 data-field-id="35678728" data-annot-id="34424560" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="DH"
                                 data-field-id="35669144" data-annot-id="34416336" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="DC"
                                 data-field-id="35673624" data-annot-id="34421216" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="DF"
                                 data-field-id="35703256" data-annot-id="34421408" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="DD"
                                 data-field-id="35701864" data-annot-id="34424752" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="DG"
                                 data-field-id="35700472" data-annot-id="34427200" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="DE"
                                 data-field-id="35699080" data-annot-id="34424944" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="FP"
                                 data-field-id="35689512" data-annot-id="34428928" type="text" onChange={() => setNeedCalc(false)}/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="FB"
                                 data-field-id="35668936" data-annot-id="34416144" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="FR"
                                 data-field-id="35681080" data-annot-id="34427008" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="FM"
                                 data-field-id="35678392" data-annot-id="34424368" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="FN"
                                 data-field-id="35688120" data-annot-id="34428736" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="FC"
                                 data-field-id="35686728" data-annot-id="34426816" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="GC"
                                 data-field-id="35680072" data-annot-id="34426240" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="FD"
                                 data-field-id="35665656" data-annot-id="34198592" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field " name="GD"
                                 data-field-id="35676376" data-annot-id="34423792" type="text"/>                   
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="GG"
                                 data-field-id="35679736" data-annot-id="34426048" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="KS"
                                 data-field-id="35684984" data-annot-id="34198400" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="GH"
                                 data-field-id="35683528" data-annot-id="34428544" type="text" disabled/>            
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="KT"
                                 data-field-id="35669480" data-annot-id="34416528" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="GJ"
                                 data-field-id="35669816" data-annot-id="34416720" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field " name="GK"
                                 data-field-id="35673960" data-annot-id="34421600" type="text"/>                               

              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="HA"
                                 data-field-id="35670152" data-annot-id="34416912" type="text"/>           
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field " name="HB"
                                 data-field-id="35674296" data-annot-id="34421792" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="HH"
                                 data-field-id="35702296" data-annot-id="34429312" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="HC"
                                 data-field-id="35679064" data-annot-id="34425136" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field " name="HD"
                                 data-field-id="35674632" data-annot-id="34422256" type="text"/>            
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="KU"
                                 data-field-id="35681416" data-annot-id="34427392" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="HE"
                                 data-field-id="35679400" data-annot-id="34421984" type="text"/>            
              <Field component={PdfNumberInput}
                                 setError={setError} formName={form} error={_get(errors, 'HF', false)}
                                 className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="HF"
                                 data-field-id="35681752" data-annot-id="34427584" type="text"/>
               <Field component={PdfNumberInput}
                                 className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field " name="JB"
                                 data-field-id="35674968" data-annot-id="34422448" type="text"/>
              <Field component={PdfNumberInput}
                                 className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="KJ"
                                 data-field-id="35682088" data-annot-id="34427776" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="KB"
                                 data-field-id="35682760" data-annot-id="34428160" type="text"/> 
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="KL"
                                 data-field-id="35675640" data-annot-id="34422832" type="text"/>       


               <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="HG"
                                 data-field-id="35670488" data-annot-id="34417104" type="text" disabled/>

              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="JC"
                                 data-field-id="35670824" data-annot-id="34417296" type="text" disabled/>

              <Field component={PdfNumberInput}
                                 setError={setError} formName={form} error={_get(errors, 'KA', false)}
                                 className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="KA"
                                 data-field-id="35675304" data-annot-id="34422640" type="text" disabled/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field " name="GR" disabled
                                 data-field-id="35677720" data-annot-id="34423984" type="text"/>

              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="GB" disabled
                                 data-field-id="35678056" data-annot-id="34424176" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="GN" disabled
                                 data-field-id="35680408" data-annot-id="34426432" type="text"/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="GM" disabled
                                 data-field-id="35680744" data-annot-id="34426624" type="text"/>
              <Field component={PdfNumberInput}
                                 setError={setError} formName={form} error={_get(errors, 'JA', false)}
                                 className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="JA"
                                 data-field-id="35682424" data-annot-id="34427968" type="text" disabled/>
              <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="KE"
                                 data-field-id="35683096" data-annot-id="34428352" type="text" disabled/>
                          <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="GP" disabled
                                 data-field-id="35688584" data-annot-id="34429120" type="text"/>



               </div>
        </div>
      </div>
      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 32, top: 50, width: 194, height: 16, 'font-size': 17, }}>
<span>
Paiement par imputation * :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 39, width: 407, height: 14, 'font-size': 15, }}>
<span>
* (joindre l’imprimé n° 3516 disponible sur www.impots.gouv.fr ou
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 55, width: 229, height: 14, 'font-size': 15, }}>
<span>
auprès de votre service des impôts).
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 81, width: 345, height: 16, 'font-size': 17, }}>
<span>
CADRE RÉSERVÉ À LA CORRESPONDANCE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 357, top: 562, width: 485, height: 14, 'font-size': 15, }}>
<span>
Vous devez déclarer et payer votre TVA par transfert de fichier ou par internet.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 255, top: 579, width: 689, height: 14, 'font-size': 15, }}>
<span>
Des informations complémentaires sont disponibles sur le site www.impots.gouv.fr rubrique « professionnels ».
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            
            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="BB"
                   data-field-id="35719832" data-annot-id="33889152" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field "
                   name="BA1"
                   data-field-id="35703688" data-annot-id="33889344" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field "
                   name="BA2"
                   data-field-id="35706712" data-annot-id="33889536" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field "
                   name="BA3"
                   data-field-id="35709736" data-annot-id="33889728" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field "
                   name="BA4"
                   data-field-id="35712760" data-annot-id="33889920" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field "
                   name="BA5"
                   data-field-id="35715784" data-annot-id="33943584" type="text" autoComplete="nope"/>
            <a className="pdf-annot pdf-link-annot obj_55 pdf-obj-fixed" href="http://www.impots.gouv.fr/"
               data-annot-id="33888736">
            </a>
            <a className="pdf-annot pdf-link-annot obj_56 pdf-obj-fixed" href="http://www.impots.gouv.fr/"
               data-annot-id="33888928">
            </a>

          </div>
        </div>
      </div>
    </div>
  );
}

export default TVACA3;
