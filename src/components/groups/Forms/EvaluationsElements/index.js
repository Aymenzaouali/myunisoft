import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  Field
} from 'redux-form';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import {
  withStyles,
  Typography,
  Switch,
  FormControlLabel
} from '@material-ui/core';
import I18n from 'assets/I18n';
import styles from './EvaluationsElements.module.scss';

class EvaluationsElements extends PureComponent {
  render() {
    const { classes } = this.props;
    return (
      <div className={styles.evaluationsElements}>
        <div className={styles.row}>
          <Field
            name="nbEstablisments"
            id="nbEstablisments"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.nbEstablisments')}
            margin="none"
            className={classes.nbEstablisments}
          />
          <Field
            name="nbSalaried"
            id="nbSalaried"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.nbSalaried')}
            margin="none"
            className={classes.nbSalaried}
          />
        </div>
        <div className={styles.row}>
          <Field
            name="capital"
            id="capital"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.capital')}
            margin="none"
            className={classes.capital}
          />
          <Field
            name="exploitationProduct"
            id="exploitationProduct"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.exploitationProduct')}
            margin="none"
            className={classes.exploitationProduct}
          />
        </div>
        <div className={styles.row}>
          <Field
            name="annualDeclaration"
            id="annualDeclaration"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.annualDeclaration')}
            margin="none"
            className={classes.annualDeclaration}
          />
        </div>
        <div className={styles.row}>
          <Field
            name="adequacyReport"
            id="adequacyReport"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.adequacyReport')}
            margin="none"
            className={classes.adequacyReport}
          />
        </div>
        <Typography classes={{ root: classes.subTitle }}>{I18n.t('companyCreation.evaluationsElements.subtitleInvoiceNumber')}</Typography>
        <div className={styles.row}>
          <Field
            name="customerSale"
            id="customerSale"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.customerSale')}
            margin="none"
            className={classes.customerSale}
          />
          <Field
            name="providers"
            id="providers"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.providers')}
            margin="none"
            className={classes.providers}
          />
        </div>
        <div className={styles.row}>
          <Field
            name="nbBankAccount"
            id="nbBankAccount"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.nbBankAccount')}
            margin="none"
            className={classes.nbBankAccount}
          />
        </div>
        <div className={styles.row}>
          <Field
            name="purchaseOfBusiness"
            id="purchaseOfBusiness"
            component={ReduxTextField}
            label={I18n.t('companyCreation.evaluationsElements.purchaseOfBusiness')}
            margin="none"
            className={classes.purchaseOfBusiness}
          />
        </div>
        <div className={styles.switch}>
          <div>
            <Typography classes={{ root: classes.subTitle }}>{I18n.t('companyCreation.evaluationsElements.subtitleCompta')}</Typography>
            <div className={classes.switchContainer}>
              <Typography classes={{ root: classes.body }}>{I18n.t('companyCreation.evaluationsElements.no')}</Typography>
              <FormControlLabel
                classes={{ root: classes.iconeSwitch }}
                control={(
                  <Switch
                    name="switchCompta"
                    id="switchCompta"
                    classes={{
                      switchBase: classes.colorSwitchBase,
                      checked: classes.colorChecked,
                      bar: classes.colorBar,
                      root: classes.iconButton,
                      icon: classes.icon
                    }}
                  />
                )}
              />
              <Typography classes={{ root: classes.body }}>{I18n.t('companyCreation.evaluationsElements.yes')}</Typography>
            </div>
          </div>
          <div>
            <Typography classes={{ root: classes.subTitle }}>{I18n.t('companyCreation.evaluationsElements.subtitleGestion')}</Typography>
            <div className={classes.switchContainer}>
              <Typography classes={{ root: classes.body }}>{I18n.t('companyCreation.evaluationsElements.no')}</Typography>
              <FormControlLabel
                classes={{ root: classes.iconeSwitch }}
                control={(
                  <Switch
                    name="switchGestion"
                    id="switchGestion"
                    classes={{
                      switchBase: classes.colorSwitchBase,
                      checked: classes.colorChecked,
                      bar: classes.colorBar,
                      root: classes.iconButton,
                      icon: classes.icon
                    }}
                  />
                )}
              />
              <Typography classes={{ root: classes.body }}>{I18n.t('companyCreation.evaluationsElements.yes')}</Typography>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const themesStyles = () => ({
  subTitle: {
    width: 214,
    height: 17,
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    margin: '20px 0px 0px 0px',
    color: 'black',
    fontSize: 14,
    fontFamily: 'basier_circleregular'
  },
  nbEstablisments: {
    width: 140
  },
  nbSalaried: {
    width: 140
  },
  capital: {
    width: 140
  },
  annualDeclaration: {
    width: 300
  },
  adequacyReport: {
    width: 300
  },
  customerSale: {
    width: 100
  },
  providers: {
    width: 100
  },
  nbBankAccount: {
    width: 300
  },
  purchaseOfBusiness: {
    width: 195
  },
  switchContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 20
  },
  colorSwitchBase: {
    color: 'white',
    '&$colorChecked': {
      color: '#0bd1d1',
      '& + $colorBar': {
        backgroundColor: '#0bd1d1'
      }
    }
  },
  icon: {
    marginTop: 5,
    width: 14,
    height: 14
  },
  colorBar: {
    marginTop: 0,
    marginLeft: -11,
    color: '#0bd1d1',
    width: 25,
    height: 8,
    backgroundColor: '#d0d0d0'
  },
  colorChecked: {
    transform: 'translateX(16px)'
  },
  iconButton: {
    padding: 0,
    fontWeight: 'bold'
  },
  body: {
    height: 17,
    fontSize: 14
  },
  iconeSwitch: {
    marginRight: 0
  }


});

EvaluationsElements.propTypes = {
  classes: PropTypes.shape({}).isRequired
};

export default withStyles(themesStyles)(EvaluationsElements);
