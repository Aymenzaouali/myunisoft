import React from 'react';
import { Field, Form } from 'redux-form';
import I18n from 'assets/I18n';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { withStyles } from '@material-ui/core';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import {
  RoadTypesAutoComplete,
  CitiesAutoComplete,
  PostalCodeTextField
} from 'containers/reduxForm/Inputs';
import styles from 'components/groups/CompanyCreation/CompanyCreation.module.scss';

import classNames from 'classnames';
import ownStyles from './accountingFirmGeneralInfo.scss';
import { addressBuildingList } from '../NameAddress/utils';

const AccountingFirmGeneralInfo = ({ classes }) => (
  <Form>
    <div className={styles.row}>
      <Field
        color="primary"
        name="siret"
        type="number"
        label={I18n.t('accountingFirmSettings.forms.siret')}
        component={ReduxTextField}
        className={classes.siret}
        errorStyle={ownStyles.error}
        margin="none"
      />
    </div>
    <div className={styles.row}>
      <Field
        color="primary"
        name="cabinetName"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.cabinetName')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={ownStyles.error}
        margin="none"
      />
    </div>
    <div className={styles.row}>
      <Field
        color="primary"
        name="address_number"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.number')}
        className={classNames(classes.baseFieldStyle, classes.number)}
        errorStyle={styles.error}
        margin="none"
      />
      <Field
        name="address_bis"
        id="address_bis"
        label={I18n.t('accountingFirmSettings.forms.comp')}
        margin="none"
        component={ReduxSelect}
        list={addressBuildingList}
        className={classes.comp}
      />
      <Field
        name="road_type"
        id="road_type"
        component={RoadTypesAutoComplete}
        menuPosition="fixed"
        textFieldProps={{
          label: I18n.t('accountingFirmSettings.forms.chanelType'),
          InputLabelProps: {
            shrink: true
          }
        }}
      />
      <Field
        name="street_name"
        id="street_name"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.nameOfRoad')}
        margin="none"
        className={classes.chanelType}
      />
    </div>

    <div className={styles.row}>
      <Field
        color="primary"
        name="addressSupplement"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.addressSupplement')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={ownStyles.error}
        margin="none"
      />
    </div>

    <div className={styles.row}>
      <Field
        name="postalCode"
        id="postalCode"
        type="number"
        component={PostalCodeTextField}
        label={I18n.t('accountingFirmSettings.forms.postalCode')}
        className={classNames(classes.baseFieldStyle, classes.postal)}
        margin="none"
        inputProps={{ maxlength: 5 }}
        errorStyle={classes.error}
      />

      <Field
        name="city"
        id="city"
        component={CitiesAutoComplete}
        menuPosition="fixed"
        textFieldProps={{
          label: I18n.t('accountingFirmSettings.forms.city'),
          InputLabelProps: {
            shrink: true
          }
        }}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
      />
    </div>

    <div className={styles.row}>
      <Field
        color="primary"
        name="email"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.email')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={classes.error}
        margin="none"
      />
    </div>

    <div className={styles.row}>
      <Field
        color="primary"
        name="phone_number"
        type="number"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.tel')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={ownStyles.error}
        margin="none"
      />
      <Field
        color="primary"
        name="fax"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.fax')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={ownStyles.error}
        margin="none"
      />
    </div>

    <div className={styles.row}>
      <Field
        color="primary"
        name="site"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.site')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={ownStyles.error}
        margin="none"
      />
    </div>
  </Form>
);
const themeStyle = () => ({
  siret: {
    width: 120
  },
  baseFieldStyle: {
    marginRight: 20
  },
  baseField: {
    width: 300
  },
  postal: {
    width: 80
  },
  number: {
    width: 40
  },
  comp: {
    width: 60
  },
  chanelType: {
    width: 100
  },
  error: {
    position: 'absolute',
    bottom: -25
  }
});

export default withStyles(themeStyle)(AccountingFirmGeneralInfo);
