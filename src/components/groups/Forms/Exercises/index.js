import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { FieldArray } from 'redux-form';
import { FECDialog } from 'components/groups/Dialogs';
import ExercisesArray from './parts/Exercises';

class Exercises extends PureComponent {
  state = {
    isOpen: false,
    initialValues: {}
  };

  openModal = async (exercise) => {
    const { updateExercises, selectedCompanyId } = this.props;
    try {
      const exercises = await updateExercises();
      const { exercice_id: exerciseId } = exercises
        .find(uExercise => uExercise.label === exercise.label);

      this.setState({
        isOpen: true,
        initialValues: {
          type: 1,
          exerciseId,
          fecImportFrom: '0',
          importMode: 0,
          society_id: selectedCompanyId
        }
      });
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
  };

  onClose = () => {
    this.setState({
      isOpen: false
    });
  };

  render() {
    const {
      exercises, selectedCompanyId, onDialogEvent, autofillCurrentExercise, usedData
    } = this.props;
    const { isOpen, initialValues } = this.state;

    return (
      <Fragment>
        <FieldArray
          name="exercises"
          component={ExercisesArray}
          exercises={exercises}
          openFec={this.openModal}
          autofillCurrentExercise={autofillCurrentExercise}
          currentExercises={usedData}
        />
        <FECDialog
          isOpen={isOpen}
          onEnter={onDialogEvent}
          onExit={onDialogEvent}
          onClose={this.onClose}
          initialValues={initialValues}
          societyId={selectedCompanyId}
        />
      </Fragment>
    );
  }
}

Exercises.propTypes = {
  selectedCompanyId: PropTypes.number.isRequired,
  updateExercises: PropTypes.func.isRequired,
  onDialogEvent: PropTypes.func.isRequired,
  exercises: PropTypes.shape({
    active: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    passive: PropTypes.arrayOf(PropTypes.shape({})).isRequired
  }).isRequired,
  autofillCurrentExercise: PropTypes.func.isRequired,
  usedData: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Exercises;
