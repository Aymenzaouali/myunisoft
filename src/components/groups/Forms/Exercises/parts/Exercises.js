import React from 'react';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import {
  addOneYear, addOneDay, subtractOneYear, subtractOneDay
} from 'helpers/date';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { InlineButton } from 'components/basics/Buttons';
import { DurationTextField } from 'containers/reduxForm/Inputs';
import styles from './Exercises.module.scss';

class Exercises extends React.PureComponent {
  renderExercise = (exercice) => {
    const {
      classes,
      openFec,
      fields,
      autofillCurrentExercise,
      currentExercises
    } = this.props;

    const {
      label,
      key,
      disabled,
      start_date,
      end_date,
      i
    } = exercice;

    const areButtonsDisabled = disabled || !(start_date && end_date);
    const nextExercise = currentExercises[i - 1];
    const previousExercise = currentExercises[i + 1];

    const fillExerciseOnFocus = () => {
      if (previousExercise && previousExercise.start_date !== '' && previousExercise.end_date !== '') {
        autofillCurrentExercise(
          i, exercice,
          addOneDay(previousExercise.end_date),
          addOneYear(previousExercise.end_date)
        );
      } else if (nextExercise && nextExercise.start_date !== '' && nextExercise.end_date !== '') {
        autofillCurrentExercise(
          i, exercice,
          subtractOneYear(nextExercise.start_date),
          subtractOneDay(nextExercise.start_date)
        );
      }
    };

    return (
      <div className={styles.row}>
        <Typography variant="h6">
          {label}
        </Typography>
        <div className={styles.date}>
          <Typography classes={{ root: classes.body2 }} variant="body2">
            {I18n.t('companyCreation.exerciseStartDate')}
          </Typography>
          <Field
            name={`${key}.start_date`}
            id="exerciseStartDate"
            component={ReduxTextField}
            onFocus={() => fillExerciseOnFocus()}
            // label={I18n.t('companyCreation.exerciseStartDate')}
            onBlur={(e, start_date) => {
              if (label === 'N') autofillCurrentExercise(i, exercice, start_date, subtractOneDay(addOneYear(start_date)));
            }}
            type="date"
            margin="none"
            disabled={disabled}
          />
        </div>
        <div className={styles.date}>
          <Typography classes={{ root: classes.body2 }} variant="body2">
            {I18n.t('companyCreation.exerciseCloseDate')}
          </Typography>
          <Field
            name={`${key}.end_date`}
            id="exerciseCloseDate"
            component={ReduxTextField}
            // label={I18n.t('companyCreation.exerciseCloseDate')}
            type="date"
            margin="none"
            disabled={disabled}
          />
        </div>
        <Field
          name={`${key}.duration`}
          id="duration"
          component={DurationTextField}
          label={I18n.t('companyCreation.duration')}
          margin="none"
          disabled
          end_date={end_date}
          start_date={start_date}
        />
        <Field
          name={`${key}.result`}
          id="result"
          component={ReduxTextField}
          label={I18n.t('companyCreation.result')}
          margin="none"
          disabled
        />
        <Field
          name={`${key}.ca`}
          id="ca"
          component={ReduxTextField}
          label={I18n.t('companyCreation.ca')}
          margin="none"
          disabled
        />

        <InlineButton
          marginDirection="none"
          buttons={
            [
              {
                _type: 'string',
                text: I18n.t('companyCreation.importDocument'),
                color: 'default',
                variant: 'outlined',
                disabled: areButtonsDisabled,
                onClick: () => openFec(fields.get(exercice.i))
              },
              {
                _type: 'string',
                text: I18n.t('companyCreation.batchButton'),
                color: 'default',
                variant: 'contained',
                disabled: true,
                onClick: () => {},
                colorError: true
              },
              {
                _type: 'string',
                text: I18n.t('companyCreation.editButton'),
                color: 'default',
                variant: 'contained',
                disabled: true,
                onClick: () => {},
                colorError: true
              },
              {
                _type: 'string',
                text: I18n.t('companyCreation.addNote'),
                color: 'default',
                variant: 'contained',
                disabled: true,
                onClick: () => {},
                colorError: true
              }
            ]
          }
        />
      </div>
    );
  }

  render() {
    const {
      exercises: {
        active,
        passive
      }
    } = this.props;

    return (
      <div className={styles.container}>
        <div>
          <p className={styles.categoryTitle}>{I18n.t('companyCreation.activeExercises')}</p>
          {active.map(exercise => this.renderExercise(exercise))}
        </div>
        <div className={styles.categoryContainer}>
          <p className={styles.categoryTitle}>{I18n.t('companyCreation.pastExercises')}</p>
          {passive.map(exercise => this.renderExercise(exercise))}
        </div>
      </div>
    );
  }
}

const materialStyle = () => ({
  body2: {
    fontSize: '10px'
  }
});

Exercises.propTypes = {
  exercises: PropTypes.shape({
    active: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    passive: PropTypes.arrayOf(PropTypes.shape({})).isRequired
  }).isRequired,
  openFec: PropTypes.func.isRequired,
  classes: PropTypes.shape({}).isRequired,
  fields: PropTypes.any, // eslint-disable-line
  autofillCurrentExercise: PropTypes.func.isRequired,
  currentExercises: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default withStyles(materialStyle)(Exercises);
