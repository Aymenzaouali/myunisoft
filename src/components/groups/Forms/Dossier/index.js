import React, { useEffect } from 'react';
import I18n from 'assets/I18n';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ReduxAutoComplete from 'components/reduxForm/Inputs/AutoComplete';
import {
  Field,
  Form,
  FormSection
} from 'redux-form';
import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import {
  DossierStatusAutoComplete,
  TaxationRegimeAutoComplete,
  RulesAutoComplete,
  ComptabilityHeldAutoComplete,
  ComptabilityTypeAutoComplete,
  GestionCenterAutoComplete
} from 'containers/reduxForm/Inputs';
import styles from 'components/groups/CompanyCreation/CompanyCreation.module.scss';

const Dossier = (props) => {
  const {
    renderFullForm,
    getEDI,
    edi,
    society_id
  } = props;

  useEffect(() => {
    getEDI(society_id);
  }, []);

  return (
    <Form>
      <FormSection name="fiscal_folder">
        <div className={styles.autocomplete}>
          <Field
            name="society_status"
            id="society_status"
            component={DossierStatusAutoComplete}
            textFieldProps={{
              label: I18n.t('companyCreation.dossierFiscal.dossierStatus'),
              InputLabelProps: {
                shrink: true
              }
            }}
          />
          <Field
            name="society_status.effective_date"
            id="society_status.effective_date"
            component={ReduxMaterialDatePicker}
            label={I18n.t('companyCreation.dossierFiscal.date')}
            margin="none"
          />
        </div>
        <FormSection name="other">
          <div className={styles.autocomplete}>
            <Field
              name="sheet_group"
              id="sheet_group"
              component={TaxationRegimeAutoComplete}
              textFieldProps={{
                label: I18n.t('companyCreation.dossierFiscal.taxationRegime'),
                InputLabelProps: {
                  shrink: true
                }
              }}
            />
            <Field
              name="gestion_center"
              id="gestion_center"
              component={GestionCenterAutoComplete}
              textFieldProps={{
                label: I18n.t('companyCreation.dossierFiscal.gestionCenter'),
                InputLabelProps: {
                  shrink: true
                }
              }}
              margin="none"
            />
          </div>
          <div className={classNames(styles.autocomplete, styles.select)}>
            <Field
              name="adherent_code"
              id="adherent_code"
              component={ReduxTextField}
              label={I18n.t('companyCreation.dossierFiscal.adherentCode')}
              margin="none"
            />
            <Field
              name="edi"
              id="edi"
              component={ReduxAutoComplete}
              textFieldProps={{
                label: I18n.t('companyCreation.dossierFiscal.edi'),
                InputLabelProps: {
                  shrink: true
                }
              }}
              options={edi}
              menuPosition="fixed"
              margin="none"
            />
          </div>
        </FormSection>
        {renderFullForm && (
          <FormSection name="info_bnc">
            <div className={styles.row}>
              <Field
                name="membership_year"
                id="membership_year"
                component={ReduxTextField}
                label={I18n.t('companyCreation.dossierFiscal.memberShipYear')}
                margin="none"
              />
            </div>
            <div className={styles.autocomplete}>
              <Field
                name="result_rule"
                id="result_rule"
                component={RulesAutoComplete}
                textFieldProps={{
                  label: I18n.t('companyCreation.dossierFiscal.rules'),
                  InputLabelProps: {
                    shrink: true
                  }
                }}
              />
              <Field
                name="activity_code_pm"
                id="activity_code_pm"
                component={ReduxTextField}
                label={I18n.t('companyCreation.dossierFiscal.activityCode')}
                margin="none"
              />
            </div>
            <div className={styles.autocomplete}>
              <Field
                name="comptability_held"
                id="comptability_held"
                component={ComptabilityHeldAutoComplete}
                textFieldProps={{
                  label: I18n.t('companyCreation.dossierFiscal.comptabilityHeld'),
                  InputLabelProps: {
                    shrink: true
                  }
                }}
              />
              <Field
                name="comptability_type"
                id="comptability_type"
                component={ComptabilityTypeAutoComplete}
                textFieldProps={{
                  label: I18n.t('companyCreation.dossierFiscal.comptabilityType'),
                  InputLabelProps: {
                    shrink: true
                  }
                }}
              />
            </div>
          </FormSection>
        )}
      </FormSection>
    </Form>
  );
};

Dossier.propTypes = {
  renderFullForm: PropTypes.bool.isRequired,
  getEDI: PropTypes.func.isRequired,
  edi: PropTypes.shape({}).isRequired,
  society_id: PropTypes.string.isRequired
};


export default Dossier;
