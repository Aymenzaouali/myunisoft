import React from 'react';
import PropTypes from 'prop-types';
import { Field, formPropTypes } from 'redux-form';
import { SocietyAutoComplete } from 'containers/reduxForm/Inputs';
import { ReduxTextField } from 'components/reduxForm/Inputs';

import I18n from 'assets/I18n';
import styles from './PlanForm.module.scss';

const PlanForm = (props) => {
  const { modify } = props;

  return (
    <div>
      <form>
        {!modify && (
          <Field
            color="primary"
            name="society"
            component={SocietyAutoComplete}
            menuPosition="fixed"
            placeholder={I18n.t('accountingPlans.forms.plan.societyList')}
          />
        )}
        <div className={styles.autoComplete}>
          <Field
            name="label"
            label={I18n.t('accountingPlans.forms.plan.label')}
            component={ReduxTextField}
            className={styles.label}
          />
          <Field
            name="description"
            label={I18n.t('accountingPlans.forms.plan.description')}
            component={ReduxTextField}
            className={styles.description}
          />
        </div>
      </form>
    </div>
  );
};

PlanForm.propTypes = {
  modify: PropTypes.bool.isRequired,
  ...formPropTypes
};

export default PlanForm;
