import React, { Fragment } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';

import I18n from 'assets/I18n';

import { ReduxRadio } from 'components/reduxForm/Selections';
import { ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';

import styles from './DADPReviewInfo.module.scss';

// Component
const DADPReviewInfo = ({ type }) => (
  <Fragment>
    <Field
      name="type"
      component={ReduxRadio}
      list={[
        {
          value: 'SITU',
          label: I18n.t('dadp.review.types.SITU')
        },
        {
          value: 'BIL',
          label: I18n.t('dadp.review.types.BIL')
        }
      ]}
      row
      color="primary"
      label={I18n.t('dadp.review.type')}
      disabled={type === 'BIL'}
      RadioProps={{
        classes: {
          root: styles.radio
        }
      }}
    />
    <Field
      name="start_date"
      component={ReduxMaterialDatePicker}

      label={I18n.t('dadp.review.from')}
      classes={{ root: styles.date }}
      disabled={type === 'BIL'}
      InputProps={{
        classes: { root: styles.input }
      }}
    />
    <Field
      name="end_date"
      component={ReduxMaterialDatePicker}

      label={I18n.t('dadp.review.to')}
      classes={{ root: styles.date }}
      disabled={type === 'BIL'}
      InputProps={{
        classes: { root: styles.input }
      }}
    />
  </Fragment>
);

// Props
DADPReviewInfo.propTypes = {
  type: PropTypes.string
};

DADPReviewInfo.defaultProps = {
  type: null
};

export default DADPReviewInfo;
