import React from 'react';
import { Field } from 'redux-form';
import { ReduxTextField } from 'components/reduxForm/Inputs';

import I18n from 'assets/I18n';
import styles from './PlanDetailForm.module.scss';

// TODO: temp way to validate
const required = value => (value ? undefined : I18n.t('accountingPlans.dialogs.planDetail.empty'));

const PlanDetailForm = () => (
  <div className={styles.autoComplete}>
    <Field
      name="account_number"
      label={I18n.t('accountingPlans.forms.planDetail.account_number')}
      component={ReduxTextField}
      validate={[required]}
      className={styles.account_number}
    />
    <Field
      name="label"
      label={I18n.t('accountingPlans.forms.planDetail.label')}
      component={ReduxTextField}
      validate={[required]}
      className={styles.label}
    />
  </div>
);

PlanDetailForm.propTypes = {};

export default PlanDetailForm;
