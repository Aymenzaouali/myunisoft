import React, {
  useEffect, useState, useRef
} from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  FormSection
} from 'redux-form';
import { PhysicalPersonAutoComplete, MoralPersonAutoComplete } from 'containers/reduxForm/Inputs';
import { withStyles, Typography } from '@material-ui/core';
import I18n from 'assets/I18n';
import { usePrevious } from 'helpers/hooks';
import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import Loader from 'components/basics/Loader';
import PhysicalPerson from 'containers/groups/Tables/PhysicalPerson';
import LegalPerson from 'containers/groups/Tables/LegalPerson';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import classNames from 'classnames';
import ReduxRadio from 'components/reduxForm/Selections/ReduxRadio';
import {
  AssociatePersonPhysicalDeleteDialog,
  AssociatePersonLegalDeleteDialog
} from 'containers/groups/Dialogs';
import { Notifications } from 'common/helpers/SnackBarNotifications';
import _get from 'lodash/get';
import styles from './Associates.module.scss';

const Associates = (props) => {
  const {
    loadData,
    classes,
    nbPhysicalPersonRef,
    nbSocietyRef,
    isLoading,
    isError,
    calculValue,
    changeFormValue,
    nbSelectPhysicalPerson,
    nbSelectSociety,
    updateAssociate,
    societyId,
    tableNamePhysicalPerson,
    tableNameLegalPerson,
    isEditingPhysicalPerson,
    isEditingLegalPerson,
    startEditPhysicalPerson,
    startEditLegalPerson,
    cancelEditPhysicalPerson,
    cancelEditLegalPerson,
    list,
    onfilterChangePerson,
    onfilterChangeSociety,
    enqueueSnackbar,
    disabledPhysicalPersonExport,
    csvPhysicalPersonData,
    basePhysicalPersonFileName,
    disabledLegalPerson,
    csvLegalPerson,
    baseLegalPersonFileName,
    formValues
  } = props;

  const [isDisable, setDisable] = useState(true);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);
  const [isDeleteOpenSociety, setIsDeleteOpenSociety] = useState(false);
  const prevSocietyId = usePrevious(societyId);

  const ppRef = useRef(null);
  const lpRef = useRef(null);
  useEffect(() => {
    if (prevSocietyId !== societyId && societyId !== null) {
      loadData();
    }
  }, [societyId]);

  const onPhysicalPersonChange = (value) => {
    const { addPersonPhysical } = props;
    addPersonPhysical(value.value);
  };
  const onSocietyChange = (value) => {
    const { addSocietySelect } = props;
    addSocietySelect(value.value);
  };

  const onChange = (e, field) => {
    const val = e.target.value;

    if (val === '' || !isNaN(+val)) { // eslint-disable-line no-restricted-globals
      changeFormValue(field, val);
      setDisable(false);
    }

    e.preventDefault();
  };

  const handleInternalError = (error) => {
    enqueueSnackbar(Notifications(error, 'warning'));
  };

  const cancel = async (type) => {
    const {
      getMoralPersonList,
      getPhysicalPersonList
    } = props;

    if (type === 'person') {
      changeFormValue('physicalPerson', '');
      await cancelEditPhysicalPerson();
      await getPhysicalPersonList();
    }
    if (type === 'society') {
      changeFormValue('legalPerson', '');
      await cancelEditLegalPerson();
      await getMoralPersonList(societyId, {}, tableNameLegalPerson);
    }
  };
  const deleteAssociatePerson = () => {
    setIsDeleteOpen(true);
  };

  const closeDeleteAssociatePerson = () => {
    setIsDeleteOpen(false);
  };
  const deleteAssociateSociety = () => {
    setIsDeleteOpenSociety(true);
  };

  const closeDeleteAssociateSociety = () => {
    setIsDeleteOpenSociety(false);
  };
  let buttonPhysicalPerson = [
    {
      _type: 'icon',
      iconName: !isEditingPhysicalPerson ? 'icon-pencil' : 'icon-save',
      iconSize: 28,
      disabled: nbSelectPhysicalPerson === 0,
      onClick: () => {
        if (!isEditingPhysicalPerson) {
          startEditPhysicalPerson();
          setTimeout(() => {
            if (_get(ppRef, 'current')) ppRef.current.focus();
          });
        } else if (_get(formValues, 'associates.effective_date')) {
          updateAssociate('person', tableNamePhysicalPerson);
          changeFormValue('physicalPerson', '');
        } else {
          handleInternalError(I18n.t('companyCreation.associates.errors.missingCapitalDate'));
        }
      }
    },
    {
      _type: 'icon',
      iconName: 'icon-trash',
      iconSize: 28,
      color: 'error',
      titleInfoBulle: I18n.t('tooltips.delete'),
      disabled: (nbSelectPhysicalPerson === 0 || isEditingPhysicalPerson),
      onClick: () => deleteAssociatePerson()
    },
    {
      _type: 'component',
      color: 'primary',
      disabled: disabledPhysicalPersonExport,
      component: <CSVButton
        csvData={csvPhysicalPersonData}
        baseFileName={basePhysicalPersonFileName}
      />
    }
  ];

  if (isEditingPhysicalPerson) {
    buttonPhysicalPerson = [{
      _type: 'string',
      text: 'Annuler',
      color: 'default',
      variant: 'outlined',
      onClick: () => cancel('person')
    },
    ...buttonPhysicalPerson
    ];
  }

  let buttonLegalPerson = [
    {
      _type: 'icon',
      iconName: !isEditingLegalPerson ? 'icon-pencil' : 'icon-save',
      iconSize: 28,
      disabled: nbSelectSociety === 0,
      onClick: () => {
        if (!isEditingLegalPerson) {
          startEditLegalPerson();
          setTimeout(() => {
            if (lpRef && lpRef.current && lpRef.current.focus) {
              lpRef.current.focus();
            }
          }, 0);
        } else if (_get(formValues, 'associates.effective_date')) {
          updateAssociate('society', tableNameLegalPerson);
          changeFormValue('legalPerson', '');
        } else {
          handleInternalError(I18n.t('companyCreation.associates.errors.missingCapitalDate'));
        }
      }
    },
    {
      _type: 'icon',
      iconName: 'icon-trash',
      iconSize: 28,
      color: 'error',
      titleInfoBulle: I18n.t('tooltips.delete'),
      disabled: (nbSelectSociety === 0 || isEditingLegalPerson),
      onClick: () => deleteAssociateSociety()
    },
    {
      _type: 'component',
      color: 'primary',
      disabled: disabledLegalPerson,
      component: <CSVButton
        csvData={csvLegalPerson}
        baseFileName={baseLegalPersonFileName}
      />
    }
  ];

  if (isEditingLegalPerson) {
    buttonLegalPerson = [
      {
        _type: 'string',
        text: 'Annuler',
        color: 'default',
        variant: 'outlined',
        onClick: () => cancel('society')
      },
      ...buttonLegalPerson
    ];
  }
  return (
    <FormSection name="associates">
      <div className={styles.associates}>
        <div className={styles.row}>
          <Field
            name="capital"
            id="capital"
            component={ReduxTextField}
            label={I18n.t('companyCreation.associates.capital')}
            margin="none"
            className={classes.capital}
            onChange={e => onChange(e, 'capital')}
          />
          <Field
            name="social_part"
            id="social_part"
            component={ReduxTextField}
            label={I18n.t('companyCreation.associates.numberSharesTotal')}
            margin="none"
            onChange={e => onChange(e, 'social_part')}
            className={classes.partTotal}
          />
          <Field
            name="social_part_value"
            id="social_part_value"
            component={ReduxTextField}
            label={I18n.t('companyCreation.associates.nominalValue')}
            margin="none"
            className={classes.nominalValue}
            disabled="true"
            value={calculValue}
          />
          <Field
            name="effective_date"
            id="effective_date"
            component={ReduxMaterialDatePicker}
            label={I18n.t('companyCreation.associates.date')}
            margin="none"
            disabled={isDisable}
            className={classes.effective_date}
            infoMessage={I18n.t('companyCreation.associates.dateInfo')}
          />
        </div>
        {(isLoading || isError || (!isLoading && !isError))
              && (
                <div>
                  {isLoading && <Loader />}
                  {(!isLoading && !isError) && (
                    <div>
                      <Typography classes={{ root: classes.titlePerson }}>{`${nbPhysicalPersonRef} ${I18n.t('companyCreation.associates.physicalPersonRef')}`}</Typography>
                      <div className={classNames(styles.row, styles.container)}>
                        <Field
                          name="physicalPerson"
                          id="physicalPerson"
                          component={PhysicalPersonAutoComplete}
                          label={I18n.t('companyCreation.associates.physicalPerson')}
                          className={classes.selectPerson}
                          onChangeValues={onPhysicalPersonChange}
                          margin="none"
                        />
                        <div>
                          <InlineButton buttons={buttonPhysicalPerson} />
                        </div>
                      </div>
                      <div className={styles.filter}>
                        <Typography classes={{ root: classes.display }}>{I18n.t('companyCreation.associates.associatesFilter.display')}</Typography>
                        <Field
                          name="filterPerson"
                          list={list}
                          row
                          component={ReduxRadio}
                          onChange={onfilterChangePerson}
                        />
                      </div>
                      <div className={styles.table}>
                        <PhysicalPerson
                          inputRef={ppRef}
                        />
                      </div>
                      <Typography classes={{ root: classes.titlePerson }}>{`${nbSocietyRef} ${I18n.t('companyCreation.associates.legalPersonRef')}`}</Typography>
                      <div className={classNames(styles.row, styles.container)}>
                        <Field
                          name="legalPerson"
                          id="legalPerson"
                          component={MoralPersonAutoComplete}
                          label={I18n.t('companyCreation.associates.legalPerson')}
                          className={classes.selectPerson}
                          onChangeValues={onSocietyChange}
                          margin="none"
                        />
                        <div>
                          <InlineButton buttons={buttonLegalPerson} />
                        </div>
                      </div>
                      <div className={styles.filter}>
                        <Typography classes={{ root: classes.display }}>{I18n.t('companyCreation.associates.associatesFilter.display')}</Typography>
                        <Field
                          name="filterSociety"
                          list={list}
                          row
                          component={ReduxRadio}
                          onChange={onfilterChangeSociety}
                        />
                      </div>
                      <div className={styles.table}>
                        <LegalPerson
                          inputRef={lpRef}
                        />
                      </div>
                    </div>
                  )}
                </div>
              )
        }
      </div>
      <AssociatePersonPhysicalDeleteDialog
        isOpen={isDeleteOpen}
        onClose={closeDeleteAssociatePerson}
      />
      <AssociatePersonLegalDeleteDialog
        isOpen={isDeleteOpenSociety}
        onClose={closeDeleteAssociateSociety}
      />
    </FormSection>

  );
};

const themeStyle = () => ({
  capital: {
    width: 130
  },
  partTotal: {
    width: 130
  },
  nominalValue: {
    width: 170
  },
  titlePerson: {
    width: 250,
    height: 17,
    fontWeight: 500,
    fontStyle: 'normal',
    fontStretch: 'normal',

    lineHeight: 'normal',
    letterSpacing: 'normal',
    margin: '30px 0px',
    color: 'black',
    fontSize: 14,
    fontFamily: 'basier_circlemedium'
  },
  display: {
    alignSelf: 'center'
  },
  selectPerson: {
    width: 280
  },
  effective_date: {
    width: 150
  }
});

Associates.defaultProps = {
  isLoading: false,
  isError: false,
  calculValue: null
};

Associates.propTypes = {
  changeFormValue: PropTypes.func.isRequired,
  loadData: PropTypes.func.isRequired,
  classes: PropTypes.shape({}).isRequired,
  nbPhysicalPersonRef: PropTypes.number.isRequired,
  nbSocietyRef: PropTypes.number.isRequired,
  isLoading: PropTypes.bool,
  isError: PropTypes.bool,
  calculValue: PropTypes.string,
  addPersonPhysical: PropTypes.func.isRequired,
  nbSelectPhysicalPerson: PropTypes.number.isRequired,
  updateAssociate: PropTypes.func.isRequired,
  addSocietySelect: PropTypes.func.isRequired,
  societyId: PropTypes.number.isRequired,
  nbSelectSociety: PropTypes.number.isRequired,
  tableNamePhysicalPerson: PropTypes.string.isRequired,
  disabledPhysicalPersonExport: PropTypes.bool.isRequired,
  csvPhysicalPersonData: PropTypes.shape({}).isRequired,
  basePhysicalPersonFileName: PropTypes.string.isRequired,
  disabledLegalPerson: PropTypes.bool.isRequired,
  csvLegalPerson: PropTypes.shape({}).isRequired,
  baseLegalPersonFileName: PropTypes.string.isRequired,
  tableNameLegalPerson: PropTypes.string.isRequired,
  isEditingPhysicalPerson: PropTypes.bool.isRequired,
  startEditPhysicalPerson: PropTypes.func.isRequired,
  cancelEditPhysicalPerson: PropTypes.func.isRequired,
  cancelEditLegalPerson: PropTypes.func.isRequired,
  startEditLegalPerson: PropTypes.func.isRequired,
  isEditingLegalPerson: PropTypes.bool.isRequired,
  getMoralPersonList: PropTypes.func.isRequired,
  getPhysicalPersonList: PropTypes.func.isRequired,
  list: PropTypes.arrayOf({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
  }).isRequired,
  onfilterChangePerson: PropTypes.func.isRequired,
  onfilterChangeSociety: PropTypes.func.isRequired,
  enqueueSnackbar: PropTypes.func.isRequired,
  formValues: PropTypes.shape({}).isRequired
};

export default withStyles(themeStyle)(Associates);
