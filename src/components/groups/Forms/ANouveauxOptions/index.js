import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import {
  FormControl, InputLabel,
  Typography
} from '@material-ui/core';
import { Field, formPropTypes } from 'redux-form';

import I18n from 'assets/I18n';

import { Button } from 'components/basics/Buttons';
import { ReduxCheckBox } from 'components/reduxForm/Selections';

import styles from './ANouveauxOptions.module.scss';

// Components
const ANouveauxOptions = (props) => {
  const {
    className,
    do_generate, disabled,

    handleSubmit
  } = props;

  return (
    <form className={classNames(styles.form, className)} onSubmit={handleSubmit}>
      <FormControl classes={{ root: styles.control }}>
        <InputLabel shrink focused={false}>
          <Typography variant="caption" color="inherit">{I18n.t('closingYear.generateLabel')}</Typography>
        </InputLabel>
        <div className={styles.checkboxes}>
          <Field
            name="do_generate"

            formControlProps={{ classes: { root: styles.generate } }}
            component={ReduxCheckBox}
            color="primary"
            label={I18n.t('closingYear.yes')}
            disabled={disabled}
          />
          <Field
            name="details"

            formControlProps={{ classes: { root: styles.details } }}
            component={ReduxCheckBox}
            color="primary"
            label={I18n.t('closingYear.details')}
            disabled={true || disabled || !do_generate}
          />
        </div>
      </FormControl>
      <div className={styles.submitBtn}>
        <Button variant="contained" color="primary" type="submit" disabled={disabled}>{I18n.t('closingYear.submit')}</Button>
      </div>
    </form>
  );
};

// Props
ANouveauxOptions.propTypes = {
  className: PropTypes.string,
  do_generate: PropTypes.bool,
  disabled: PropTypes.bool,

  ...formPropTypes
};

ANouveauxOptions.defaultProps = {
  className: undefined,
  do_generate: true,
  disabled: false
};

export default ANouveauxOptions;
