import React from 'react';
import { Field } from 'redux-form';
import {
  AccountAutoComplete,
  TauxTvaAutoComplete,
  TypeTvaAutoComplete,
  DueTvaAutoComplete
} from 'containers/reduxForm/Inputs';

import { ReduxTextField } from 'components/reduxForm/Inputs';
import I18n from 'assets/I18n';
import { deleteSpace } from 'helpers/format';
import styles from './NewVATForm.module.scss';

const NewVATForm = () => (
  <div className={styles.autoComplete}>
    <form>
      <Field
        format={deleteSpace}
        name="code"
        label="Code"
        component={ReduxTextField}
      />
      <div>
        <Field
          name="account_ded"
          prefixed="4456"
          component={AccountAutoComplete}
          resetOnWrite
          menuPosition="fixed"
          placeholder=""
          selectStyles={{
            input: { paddingLeft: 50 }
          }}
          textFieldProps={{
            label: I18n.t('tva.form.account_ded'),
            InputLabelProps: {
              shrink: true
            }
          }}
        />
      </div>
      <div>
        <Field
          name="account_coll"
          prefixed="4457"
          resetOnWrite
          component={AccountAutoComplete}
          menuPosition="fixed"
          placeholder=""
          selectStyles={{
            input: { paddingLeft: 50 }
          }}
          textFieldProps={{
            label: I18n.t('tva.form.account_coll'),
            InputLabelProps: {
              shrink: true
            }
          }}
        />
      </div>
      <Field
        name="taux"
        component={TauxTvaAutoComplete}
        menuPosition="fixed"
        placeholder=""
        className={styles.autoComplete}
        textFieldProps={{
          label: I18n.t('tva.form.taux'),
          InputLabelProps: {
            shrink: true
          }
        }}
      />
      <Field
        name="type"
        component={TypeTvaAutoComplete}
        menuPosition="fixed"
        placeholder=""
        className={styles.autoComplete}
        textFieldProps={{
          label: I18n.t('tva.form.type'),
          InputLabelProps: {
            shrink: true
          }
        }}
      />
      <Field
        name="exigility"
        component={DueTvaAutoComplete}
        menuPosition="fixed"
        placeholder=""
        className={styles.autoComplete}
        textFieldProps={{
          label: I18n.t('tva.form.exigility'),
          InputLabelProps: {
            shrink: true
          }
        }}
      />
    </form>
  </div>
);

NewVATForm.propTypes = {};

export default NewVATForm;
