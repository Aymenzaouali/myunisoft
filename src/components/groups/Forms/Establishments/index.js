import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Field
} from 'redux-form';
import { withStyles, Typography } from '@material-ui/core';
import I18n from 'assets/I18n';
import EstablishmentList from 'containers/groups/Tables/EstablishmentList';
import { ReduxSelect } from 'components/reduxForm/Selections';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { CitiesAutoComplete, RoadTypesAutoComplete, PostalCodeTextField } from 'containers/reduxForm/Inputs';
import styles from './Establishments.module.scss';

class Establishments extends PureComponent {
  render() {
    const {
      classes,
      nbEstablishmentsRef,
      listEstablishments,
      listROFCAE,
      selectedEstablishment,
      onChange
    } = this.props;

    return (
      <Fragment>
        <div className={styles.subsidiaries}>
          <Typography classes={{ root: classes.titleEstablishments }}>{I18n.t('companyCreation.establishments.establishmentsRef', { count: nbEstablishmentsRef })}</Typography>
          <div className={styles.row}>
            <Field
              name="addEstablishments"
              id="addEstablishments"
              component={ReduxSelect}
              label={I18n.t('companyCreation.establishments.addEstablishments')}
              className={classes.selectEstablishment}
              margin="none"
              list={listEstablishments}
              onChange={onChange}
            />
          </div>
          {nbEstablishmentsRef !== 0 && (
            <div className={styles.table}>
              <EstablishmentList />
            </div>
          )}
        </div>
        {selectedEstablishment !== '' && (
          <div className={styles.subsidiaries}>
            <Typography classes={{ root: classes.titleEstablishments }}>{I18n.t('companyCreation.establishments.subTitleEstablishments')}</Typography>
            <div className={styles.row}>
              <Field
                name="nameEstablishments"
                id="nameEstablishments"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.nameEstablishments')}
                className={classes.selectNameEstablishments}
                margin="none"
                list={listEstablishments}
              />
              <Field
                name="siret"
                id="siret"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.siret')}
                className={classes.selectSiretEstablishment}
                margin="none"
              />
              <Field
                name="RofCae"
                id="RofCae"
                component={ReduxSelect}
                label={I18n.t('companyCreation.establishments.RofCae')}
                className={classes.selectRofCaeEstablishment}
                margin="none"
                list={listROFCAE}
              />
            </div>
            <Typography classes={{ root: classes.title }}>{I18n.t('companyCreation.establishments.Address.title')}</Typography>
            <div className={styles.row}>
              <Field
                name="number"
                id="number"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.Address.number')}
                className={classes.selectAddressNumber}
                margin="none"
              />
              <Field
                name="compAddress"
                id="compAddress"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.Address.comp')}
                className={classes.selectAddressComp}
                margin="none"
              />
              <Field
                name="roadType"
                id="roadType"
                component={RoadTypesAutoComplete}
                classes={{ root: classes.selectAddressType }}
                textFieldProps={{
                  label: I18n.t('companyCreation.establishments.Address.type'),
                  InputLabelProps: {
                    classes: { root: classes.selectAddressTypeLabel },
                    shrink: true
                  }
                }}
              />
              <Field
                name="nameWay"
                id="nameWay"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.Address.nameWay')}
                className={classes.selectNameWay}
              />
              <Field
                name="compAddress"
                id="compAddress"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.Address.compAddress')}
                className={classes.selectCompAddress}
              />
            </div>
            <div className={styles.row}>
              <Field
                name="cp"
                id="cp"
                component={PostalCodeTextField}
                label={I18n.t('companyCreation.establishments.Address.cp')}
                className={classes.selectCp}
              />
              <Field
                name="city"
                id="city"
                component={CitiesAutoComplete}
                className={classes.selectCity}
                textFieldProps={{
                  label: I18n.t('companyCreation.establishments.Address.city'),
                  InputLabelProps: {
                    classes: { root: classes.selectCityLabel },
                    shrink: true
                  }
                }}
              />
              <Field
                name="country"
                id="country"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.Address.country')}
                className={classes.selectCountry}
              />
            </div>
            <Typography classes={{ root: classes.title }}>{I18n.t('companyCreation.establishments.leaderInfo.title')}</Typography>
            <div className={styles.row}>
              <Field
                name="civilityleaderInfo"
                id="civilityleaderInfo"
                label={I18n.t('companyCreation.establishments.leaderInfo.civility')}
                component={ReduxTextField}
                className={classes.selectCivilityleaderInfo}
              />
              <Field
                name="firstNameleaderInfo"
                id="firstNameleaderInfo"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.leaderInfo.firstName')}
                className={classes.selectFirstnameleaderInfo}
              />
              <Field
                name="nameleaderInfo"
                id="nameleaderInfo"
                label={I18n.t('companyCreation.establishments.leaderInfo.name')}
                component={ReduxTextField}
                className={classes.selectNameleaderInfo}
              />
            </div>
            <div className={styles.row}>
              <Field
                name="telLeaderInfo"
                id="telLeaderInfo"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.leaderInfo.tel')}
                className={classes.selectTelLeaderInfo}
              />
              <Field
                name="emailLeaderInfo"
                id="emailLeaderInfo"
                component={ReduxTextField}
                label={I18n.t('companyCreation.establishments.leaderInfo.email')}
                className={classes.selectEmailLeaderInfo}
              />

            </div>
          </div>
        )}
      </Fragment>

    );
  }
}

const themeStyle = () => ({
  titleEstablishments: {
    width: 'auto',
    height: 17,
    fontWeight: 500,
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    margin: '30px 0px',
    color: 'black',
    fontSize: 14,
    fontFamily: 'basier_circlemedium'
  },
  selectEstablishment: {
    width: 280
  },
  title: {
    width: 'auto',
    height: 17,
    fontFamily: 'basier_circlemedium',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#000000'
  },
  selectNameEstablishments: {
    width: 300
  },
  selectSiretEstablishment: {
    width: 140
  },
  selectRofCaeEstablishment: {
    width: 99
  },
  selectAddressNumber: {
    width: 40
  },
  selectAddressComp: {
    width: 60
  },
  selectAddressType: {
    minWidth: 0,
    width: 100,
    marginBottom: 0.5
  },
  selectAddressTypeLabel: {
    fontSize: 14,
    position: 'relative',
    top: 20
  },
  selectNameWay: {
    width: 300,
    marginBottom: 0.5
  },
  selectCompAddress: {
    width: 244,
    marginBottom: 0.5
  },
  selectCp: {
    width: 80
  },
  selectCity: {
    width: 300,
    marginBottom: 8
  },

  selectCityLabel: {
    fontSize: 14,
    position: 'relative',
    top: 20
  },
  selectCountry: {
    width: 300,
    marginBottom: 8
  },
  selectCivilityleaderInfo: {
    width: 80
  },
  selectFirstnameleaderInfo: {
    width: 200
  },
  selectNameleaderInfo: {
    width: 300
  },
  selectTelLeaderInfo: {
    width: 140
  },
  selectEmailLeaderInfo: {
    width: 460
  }
});

Establishments.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  nbEstablishmentsRef: PropTypes.number.isRequired,
  listEstablishments: PropTypes.shape({}).isRequired,
  listROFCAE: PropTypes.shape({}).isRequired,
  selectedEstablishment: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default withStyles(themeStyle)(Establishments);
