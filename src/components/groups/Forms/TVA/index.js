import React from 'react';
import I18n from 'assets/I18n';
import {
  Field,
  Form,
  FormSection
} from 'redux-form';
import PropTypes from 'prop-types';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { RegimeTvaAutoComplete } from 'containers/reduxForm/Inputs';
import { ReduxCheckBox, ReduxSwitch } from 'components/reduxForm/Selections';
import { AbstractIcon } from 'components/basics/Icon';
import { Tooltip, withStyles } from '@material-ui/core';

import styles from 'components/groups/CompanyCreation/CompanyCreation.module.scss';


class TVA extends React.PureComponent {
  renderIcon = () => (
    <Tooltip
      title={I18n.t('companyCreation.dossierFiscal.monoInfo')}
      placement="top-start"
    >
      <AbstractIcon
        iconName="icon-account-info"
        iconSize={19}
      />
    </Tooltip>
  )

  render() {
    const {
      classes
    } = this.props;
    return (
      <Form>
        <FormSection name="fiscal_folder.other">
          <div className={styles.autocomplete}>
            <Field
              name="vat_regime"
              id="vat_regime"
              component={RegimeTvaAutoComplete}
              textFieldProps={{
                label: I18n.t('companyCreation.dossierFiscal.regimeTva'),
                InputLabelProps: {
                  shrink: true
                }
              }}
            />
          </div>
          <div className={styles.row}>
            <Field
              name="tva_intraco"
              id="tva_intraco"
              component={ReduxTextField}
              label={I18n.t('companyCreation.dossierFiscal.tvaIntraco')}
              margin="none"
              className={classes.vat}
            />
            <Field
              name="due_date_tva"
              id="due_date_tva"
              component={ReduxTextField}
              label={I18n.t('companyCreation.dossierFiscal.vatDueDate')}
              margin="none"
            />
          </div>
          <div className={styles.row}>
            <Field
              name="rof_tva"
              id="rof_tva"
              label={I18n.t('companyCreation.dossierFiscal.rof_tva')}
              component={ReduxTextField}
              margin="none"
            />
            <Field
              name="rof_tdfc"
              id="rof_tdfc"
              label={I18n.t('companyCreation.dossierFiscal.rof_tfdc')}
              component={ReduxTextField}
              margin="none"
            />
            <Field
              name="rof_cfe"
              id="rof_cfe"
              label={I18n.t('companyCreation.dossierFiscal.rof_cfe')}
              component={ReduxTextField}
              margin="none"
            />
          </div>
          <div className={styles.row}>
            <Field
              name="mono_etab"
              id="mono_etab"
              color="primary"
              component={ReduxCheckBox}
              label={I18n.t('companyCreation.dossierFiscal.singleEstablishmment')}
              margin="none"
            />
            {this.renderIcon()}
          </div>
          <div className={styles.row}>
            <Field
              name="close_entries_VAT"
              id="close_entries_VAT"
              color="primary"
              component={ReduxSwitch}
              label={I18n.t('companyCreation.dossierFiscal.accountingEntries')}
              labelOn={I18n.t('ediDialogs.buttons.yes')}
              labelOff={I18n.t('ediDialogs.buttons.no')}
              margin="none"
            />
          </div>
        </FormSection>
      </Form>
    );
  }
}

const themeStyles = () => ({
  vat: {
    width: 200
  }
});

TVA.propTypes = {
  classes: PropTypes.shape({}).isRequired
};

export default withStyles(themeStyles)(TVA);
