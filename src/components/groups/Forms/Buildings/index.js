import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Field
} from 'redux-form';
import {
  withStyles,
  Typography,
  Switch,
  FormControlLabel,
  Button
} from '@material-ui/core';
import I18n from 'assets/I18n';
import BuildingList from 'containers/groups/Tables/BuildingList';
import { ReduxSelect, ReduxCheckBox, ReduxRadio } from 'components/reduxForm/Selections';
import { InlineButton } from 'components/basics/Buttons';
import { ReduxTextField, ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import {
  CitiesAutoComplete,
  RoadTypesAutoComplete,
  PostalCodeTextField
} from 'containers/reduxForm/Inputs';
import styles from './Buildings.module.scss';

class Buildings extends PureComponent {
  render() {
    const {
      classes,
      nbBuildings,
      landSystemList,
      schemaList,
      radioNatureList,
      onClick,
      selectedBuilding,
      disabled,
      cancelBuilding
    } = this.props;

    return (
      <Fragment>
        <div className={styles.subsidiaries}>
          <Typography classes={{ root: classes.buildingsRef }}>{I18n.t('companyCreation.buildings.buildingsRef', { count: nbBuildings })}</Typography>
          <Button
            size="large"
            variant="contained"
            color="primary"
            onClick={onClick}
            disabled={disabled}
          >
            {I18n.t('companyCreation.buildings.buttonBuildings')}
          </Button>
          {nbBuildings !== 0 && (
            <div className={styles.table}>
              <BuildingList />
            </div>
          )}
        </div>
        {selectedBuilding !== undefined && (
          <div className={styles.subsidiaries}>
            <Typography classes={{ root: classes.buildingsRef }}>{I18n.t('companyCreation.buildings.subTitleBuilding')}</Typography>
            <div className={styles.row}>
              <Field
                name="nameBuilding"
                id="nameBuilding"
                component={ReduxTextField}
                label={I18n.t('companyCreation.buildings.nameBuilding')}
                className={classes.selectNameBuilding}
                margin="none"
              />
              <Field
                name="localNumber"
                id="localNumber"
                component={ReduxTextField}
                label={I18n.t('companyCreation.buildings.localNumber')}
                className={classes.selectlocalNumber}
                margin="none"
              />
            </div>
            <div className={styles.row}>
              <Field
                name="number"
                id="number"
                component={ReduxTextField}
                label={I18n.t('companyCreation.buildings.number')}
                className={classes.selectAddressNumber}
                margin="none"
              />
              <Field
                name="compAddress"
                id="compAddress"
                component={ReduxTextField}
                label={I18n.t('companyCreation.buildings.comp')}
                className={classes.selectAddressComp}
                margin="none"
              />
              <Field
                name="roadType"
                id="roadType"
                component={RoadTypesAutoComplete}
                classes={{ root: classes.selectAddressType }}
                textFieldProps={{
                  label: I18n.t('companyCreation.buildings.type'),
                  InputLabelProps: {
                    classes: { root: classes.selectAddressTypeLabel },
                    shrink: true
                  }
                }}
              />
              <Field
                name="nameWay"
                id="nameWay"
                component={ReduxTextField}
                label={I18n.t('companyCreation.buildings.nameWay')}
                className={classes.selectNameWay}
              />
              <Field
                name="compAddress"
                id="compAddress"
                component={ReduxTextField}
                label={I18n.t('companyCreation.buildings.compAddress')}
                className={classes.selectCompAddress}
              />
            </div>
            <div className={styles.row}>
              <Field
                name="cp"
                id="cp"
                component={PostalCodeTextField}
                label={I18n.t('companyCreation.buildings.cp')}
                className={classes.selectCp}
              />
              <Field
                name="city"
                id="city"
                component={CitiesAutoComplete}
                className={classes.selectCity}
                textFieldProps={{
                  label: I18n.t('companyCreation.buildings.city'),
                  InputLabelProps: {
                    classes: { root: classes.selectCityLabel },
                    shrink: true
                  }
                }}
              />
              <Field
                name="country"
                id="country"
                component={ReduxTextField}
                label={I18n.t('companyCreation.buildings.country')}
                className={classes.selectCountry}
              />
            </div>
            <div className={styles.row}>
              <Field
                name="landSystem"
                id="landSystem"
                component={ReduxSelect}
                label={I18n.t('companyCreation.buildings.landSystem')}
                className={classes.selectLandSystem}
                list={landSystemList}
              />
              <Field
                name="schema"
                id="schema"
                label={I18n.t('companyCreation.buildings.schema')}
                component={ReduxSelect}
                className={classes.selectSchema}
                list={schemaList}
              />
            </div>
            <div className={styles.row}>
              <Field
                name="acquisitionDate"
                id="acquisitionDate"
                component={ReduxMaterialDatePicker}
                label={I18n.t('companyCreation.buildings.acquisitionDate')}
                className={classes.selectDate}
              />
              <Field
                name="completion"
                id="completion"
                component={ReduxMaterialDatePicker}
                label={I18n.t('companyCreation.buildings.completion')}
                className={classes.selectDate}
              />
              <Field
                component={ReduxCheckBox}
                label={I18n.t('companyCreation.buildings.checkboxNude')}
                labelStyle={{ root: classes.selectCheckboxNude }}
              />
            </div>

            <Typography classes={{ root: classes.title }}>{I18n.t('companyCreation.buildings.natureTitle')}</Typography>
            <Field
              name="radioNature"
              id="radioNature"
              color="primary"
              component={ReduxRadio}
              FormControlLabelProps={{
                classes: { label: classes.selectSchemaLabel }
              }}
              RadioProps={{
                classes: { root: classes.selectSchemaRadio }
              }}

              className={classes.selectSchema}
              list={radioNatureList}
            />
            <Typography classes={{ root: classes.title1 }}>{I18n.t('companyCreation.buildings.situationTitle')}</Typography>
            <div className={classes.switchContainer}>
              <Typography classes={{ root: classes.body }}>{I18n.t('companyCreation.buildings.switchSituation.U')}</Typography>
              <FormControlLabel
                classes={{ root: classes.iconeSwitch }}
                control={(
                  <Switch
                    name="switchSituation"
                    id="switchSituation"
                    classes={{
                      switchBase: classes.colorSwitchBase,
                      checked: classes.colorChecked,
                      bar: classes.colorBar,
                      root: classes.iconButton
                    }}
                  />
                )}
              />
              <Typography classes={{ root: classes.body }}>{I18n.t('companyCreation.buildings.switchSituation.R')}</Typography>
            </div>
            <Typography classes={{ root: classes.title }}>{I18n.t('companyCreation.buildings.featureTitle')}</Typography>
            <div className={classes.switchContainer}>
              <Typography classes={{ root: classes.body }}>{I18n.t('companyCreation.buildings.switchFeature.B')}</Typography>
              <FormControlLabel
                classes={{ root: classes.iconeSwitch }}
                control={(
                  <Switch
                    name="switchFeature"
                    id="switchFeature"
                    classes={{
                      switchBase: classes.colorSwitchBase,
                      checked: classes.colorChecked,
                      bar: classes.colorBar,
                      root: classes.iconButton
                    }}
                  />
                )}
              />
              <Typography classes={{ root: classes.body }}>{I18n.t('companyCreation.buildings.switchFeature.NB')}</Typography>
            </div>
            <div className={styles.button}>
              <InlineButton
                marginDirection="right"
                buttons={[
                  {
                    _type: 'string',
                    text: I18n.t('companyCreation.cancel'),
                    onClick: cancelBuilding,
                    size: 'large',
                    color: '#d0d0d0'
                  },
                  {
                    _type: 'string',
                    text: I18n.t('companyCreation.validate'),
                    onClick: () => {},
                    size: 'large'
                  }]
                }
              />
            </div>

          </div>
        )}
      </Fragment>

    );
  }
}

const themeStyle = () => ({
  buildingsRef: {
    width: 'auto',
    height: 17,
    fontWeight: 500,
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    margin: '30px 0px',
    color: 'black',
    fontSize: 14,
    fontFamily: 'basier_circlemedium'
  },
  title: {
    height: 17,
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: 'black',
    fontSize: 14,
    fontFamily: 'basier_circlemedium'
  },
  title1: {
    height: 17,
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: 'black',
    fontSize: 14,
    fontFamily: 'basier_circlemedium',
    marginTop: 5
  },
  selectNameBuilding: {
    width: 300
  },
  selectlocalNumber: {
    width: 140
  },
  selectAddressNumber: {
    width: 40
  },
  selectAddressComp: {
    width: 60
  },
  selectAddressType: {
    minWidth: 0,
    width: 100,
    marginBottom: 0.5
  },
  selectAddressTypeLabel: {
    fontSize: 14,
    position: 'relative',
    top: 20
  },
  selectNameWay: {
    width: 300,
    marginBottom: 0.5
  },
  selectCompAddress: {
    width: 244,
    marginBottom: 0.5
  },
  selectCp: {
    width: 80
  },
  selectCity: {
    width: 300,
    marginBottom: 8
  },
  selectCityLabel: {
    fontSize: 14,
    position: 'relative',
    top: 20
  },
  selectCountry: {
    width: 300,
    marginBottom: 8
  },
  selectLandSystem: {
    width: 300
  },
  selectSchema: {
    width: 300,
    fontSize: 14
  },
  switchContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 20
  },
  colorSwitchBase: {
    color: '#0bd1d1',
    '&$colorChecked': {
      color: '#0bd1d1',
      '& + $colorBar': {
        backgroundColor: '#0bd1d1'
      }
    }
  },
  selectSchemaLabel: {
    fontSize: 14,
    paddingTop: 7
  },
  selectSchemaRadio: {
    paddingBottom: 5,
    paddingLeft: 0
  },
  colorBar: {
    color: '#0bd1d1',
    backgroundColor: '#0bd1d1'
  },
  colorChecked: {},
  iconButton: {
    padding: 0,
    fontWeight: 'bold'
  },
  body: {
    height: 17,
    fontSize: 14
  },
  iconeSwitch: {
    marginRight: 0
  },
  selectDate: {
    width: 120
  },
  selectCheckboxNude: {
    fontSize: 14
  }
});

Buildings.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  nbBuildings: PropTypes.number.isRequired,
  landSystemList: PropTypes.arrayOf().isRequired,
  schemaList: PropTypes.arrayOf().isRequired,
  radioNatureList: PropTypes.arrayOf().isRequired,
  onClick: PropTypes.func.isRequired,
  cancelBuilding: PropTypes.func.isRequired,
  selectedBuilding: PropTypes.shape({}).isRequired,
  disabled: PropTypes.bool.isRequired
};

export default withStyles(themeStyle)(Buildings);
