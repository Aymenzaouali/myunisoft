import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { ReduxSelect } from 'components/reduxForm/Selections';
import I18n from 'assets/I18n';
import styles from './accountingForm.module.scss';

const Accounting = ({ plansList }) => (
  <>
    <Field
      name="accountPlanId"
      component={ReduxSelect}
      list={plansList}
      margin="none"
      label={I18n.t('companyCreation.accountingPlan')}
      className={styles.accountPlanField}
    />
  </>
);

Accounting.propTypes = {
  plansList: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Accounting;
