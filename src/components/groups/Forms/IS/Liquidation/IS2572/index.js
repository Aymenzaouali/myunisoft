/* eslint-disable */
import React from "react";
import { Field } from "redux-form";
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker, PdfInputValuesWithSpaces } from 'components/reduxForm/Inputs';
import { sum, useCompute, parse as p } from "helpers/pdfforms";

import "./style.scss";

const IS2572 = (props) => {
  const { change, formLiquidationIS2752 } = props;

  useCompute('TA', ({ RA }) => Math.round(p(RA) * 33 / 100), ['RA'], formLiquidationIS2752, change);
  useCompute('TC', ({ SA }) => Math.round(p(SA) * 28 / 100), ['SA'], formLiquidationIS2752, change);
  useCompute('TB', ({ RB }) => Math.round(p(RB) * 15 / 100), ['RB'], formLiquidationIS2752, change);
  useCompute('RZ', ({ RW }) => p(RW) * 3.3 / 100, ['RW'], formLiquidationIS2752, change);
  useCompute('PM', ({ PC }) => p(PC) * 2.5 / 100, ['PC'], formLiquidationIS2752, change);
  useCompute('GE', sum, ['TA', 'TC', 'TB', 'GC', 'GD'], formLiquidationIS2752, change);
  useCompute('RG', sum, ['JA', 'MA', 'MH', 'MD'], formLiquidationIS2752, change);
  useCompute('RE', sum, ['KA', 'NA', 'KB', 'NB', 'KC', 'ND', 'NE', 'NF', 'NG', 'RC', 'NJ', 'NK', 'NM', 'NN', 'OD', 'KD', 'OF', 'OK', 'NO'], formLiquidationIS2752, change);
  useCompute('RH', ({ GE, RG }) => p(GE) - p(RG) > 0 ? p(GE) - p(RG) : 0, ['GE', 'RG'], formLiquidationIS2752, change);
  useCompute('RS', ({ RH, RP }) => p(RH) - p(RP) > 0 ? p(RH) - p(RP) : 0, ['RH', 'RP'], formLiquidationIS2752, change);
  useCompute('RF', ({ RP, RE }) => p(RP) - p(RE) > 0 ? p(RP) - p(RE) : 0, ['RP', 'RE'], formLiquidationIS2752, change);
  useCompute('PN', ({ RF, RT }) => p(RF) - p(RT) > 0 ? p(RF) - p(RT) : 0, ['RF', 'RT'], formLiquidationIS2752, change);
  useCompute('PQ', ({ RF, RT }) => p(RF) - p(RT) < 0 ? p(RF) - p(RT) : 0, ['RF', 'RT'], formLiquidationIS2752, change);
  useCompute('PT', ({ PA, PB }) => p(PA) - p(PB) > 0 ? p(PA) - p(PB) : 0, ['PA', 'PB'], formLiquidationIS2752, change);
  useCompute('PU', ({ PA, PB }) => p(PA) - p(PB) < 0 ? p(PA) - p(PB) : 0, ['PA', 'PB'], formLiquidationIS2752, change);
  useCompute('PV', ({ PM, PD }) => p(PM) - p(PD) > 0 ? p(PM) - p(PD) : 0, ['PM', 'PD'], formLiquidationIS2752, change);
  useCompute('PW', ({ PM, PD }) => p(PM) - p(PD) < 0 ? p(PM) - p(PD) : 0, ['PM', 'PD'], formLiquidationIS2752, change);
  useCompute('CA', ({ AD, BD }) => p(AD) - p(BD) > 0 ? p(AD) - p(BD) : 0, ['AD', 'BD'], formLiquidationIS2752, change);
  useCompute('DA', ({ AD, BD }) => p(AD) - p(BD) > 0 ? p(AD) - p(BD) : 0, ['AD', 'BD'], formLiquidationIS2752, change);
  useCompute('PA', ({ RZ, MB }) => p(RZ) - p(MB), ['RZ', 'MB'], formLiquidationIS2752, change);
  useCompute('EB', ({ DA, EA }) => p(DA) - p(EA), ['DA', 'EA'], formLiquidationIS2752, change);
  useCompute('AD', sum, ['PN', 'PT', 'PV'], formLiquidationIS2752, change);
  useCompute('BD', sum, ['PQ', 'PU', 'PW'], formLiquidationIS2752, change);
  useCompute('RP', ({ LC, RJ, LF, OH, RN, OB, RK, RL, LA, RM, OC, LE, OJ }) =>
    p(LC) + p(RJ) + p(LF) + p(OH) + p(RN) + p(OB) + p(RK) + p(RL) + p(LA) + p(RM) + p(OC) - p(LE) - p(OJ),
    ['LC', 'RJ', 'LF', 'OH', 'RN', 'OB', 'RK', 'RL', 'LA', 'RM', 'OC', 'LE', 'OJ'], formLiquidationIS2752, change);

  return (
    <div className="IS2572 form-liquidation">

      <div id="pdf-document" data-type="pdf-document" data-num-pages="2" data-layout="fixed"></div>

      <form id="acroform"></form>

      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 125, top: 55, width: 315, height: 11, 'font-size': 13, }}>
<span>
DIRECTION GENERALE DES FINANCES PUBLIQUES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 927, top: 51, width: 65, height: 13, 'font-size': 17, }}>
<span>
2572-SD
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 77, width: 213, height: 13, 'font-size': 17, }}>
<span>
IMPOT SUR LES SOCIETES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 103, width: 203, height: 10, 'font-size': 13, }}>
<span>
ET CONTRIBUTIONS ASSIMILEES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 123, width: 116, height: 7, 'font-size': 9, }}>
<span>
Art. 360 de l&#39;annexe III au CGI
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 147, width: 77, height: 11, 'font-size': 13, }}>
<span>
Dénomination
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 330, top: 169, width: 47, height: 11, 'font-size': 13, }}>
<span>
Adresse
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 302, width: 82, height: 10, 'font-size': 12, }}>
<span>
NOUVEAUTES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 126, top: 362, width: 33, height: 9, 'font-size': 11, }}>
<span>
SIREN
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 472, top: 363, width: 95, height: 8, 'font-size': 11, }}>
<span>
Exercice social du
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 730, top: 365, width: 19, height: 6, 'font-size': 11, }}>
<span>
au :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 387, width: 60, height: 10, 'font-size': 13, }}>
<span>
I - IS Brut
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 636, top: 390, width: 21, height: 8, 'font-size': 10, }}>
<span>
Base
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 786, top: 390, width: 23, height: 8, 'font-size': 10, }}>
<span>
Taux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 966, top: 388, width: 35, height: 8, 'font-size': 10, }}>
<span>
Montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 128, top: 409, width: 301, height: 12, 'font-size': 13, }}>
<span>
I-A Impôt sur les Sociétés dû au titre de l’exercice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 428, width: 24, height: 9, 'font-size': 11, }}>
<span>
I-A01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 776, top: 428, width: 43, height: 11, 'font-size': 11, }}>
<span>
33,1/3 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 450, width: 26, height: 9, 'font-size': 11, }}>
<span>
I-A02
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 779, top: 450, width: 37, height: 10, 'font-size': 11, }}>
<span>
31,00%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 779, top: 472, width: 37, height: 10, 'font-size': 11, }}>
<span>
28,00%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 492, width: 213, height: 13, 'font-size': 13, }}>
<span>
Impôt sur les sociétés (au taux réduit)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 780, top: 494, width: 36, height: 10, 'font-size': 11, }}>
<span>
15,00%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 514, width: 180, height: 13, 'font-size': 13, }}>
<span>
Impôt sur les plus-values nettes
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 536, width: 164, height: 13, 'font-size': 13, }}>
<span>
Autre impôt à taux particulier
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 799, top: 558, width: 96, height: 11, 'font-size': 11, }}>
<span>
Total IS Brut 15
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 584, width: 79, height: 11, 'font-size': 13, }}>
<span>
II - Créances
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 966, top: 586, width: 35, height: 8, 'font-size': 10, }}>
<span>
Montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 606, width: 307, height: 13, 'font-size': 13, }}>
<span>
II-A – Créances non reportables et non restituables
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 648, width: 28, height: 9, 'font-size': 11, }}>
<span>
II-A01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 648, width: 297, height: 11, 'font-size': 11, }}>
<span>
Crédits d’impôts sur valeurs mobilières imputable sur l&#39;IS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 646, width: 14, height: 11, 'font-size': 13, }}>
<span>
16
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 670, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-A02
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 670, width: 410, height: 11, 'font-size': 11, }}>
<span>
Crédits d’impôt étrangers, autres que sur valeurs mobilières imputables sur l&#39;IS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 668, width: 14, height: 10, 'font-size': 13, }}>
<span>
17
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 692, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-A03
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 692, width: 359, height: 11, 'font-size': 11, }}>
<span>
VEL - Réduction d&#39;impôt pour mise à disposition d&#39;une flotte de vélos
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 691, width: 14, height: 10, 'font-size': 13, }}>
<span>
18
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 714, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-A04
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 714, width: 243, height: 11, 'font-size': 11, }}>
<span>
Nouvelles créances non répertoriées ci-dessus
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 713, width: 15, height: 10, 'font-size': 13, }}>
<span>
44
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 736, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-A05
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 671, top: 736, width: 204, height: 11, 'font-size': 11, }}>
<span>
Sous total (total des lignes II-A01 à II-A04)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 758, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-A06
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 819, width: 211, height: 11, 'font-size': 11, }}>
<span>
Créances reportables et non restituables
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 842, width: 28, height: 9, 'font-size': 11, }}>
<span>
II-B01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 842, width: 297, height: 11, 'font-size': 11, }}>
<span>
MEC - Réduction d’impôt au titre du mécénat au titre de N
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 680, top: 841, width: 14, height: 10, 'font-size': 13, }}>
<span>
40
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 859, top: 841, width: 36, height: 12, 'font-size': 11, }}>
<span>
) 35
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 864, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B02
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 864, width: 344, height: 12, 'font-size': 11, }}>
<span>
MEC - solde de créance des exercices antérieurs (Exercices N-5 à N-1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 886, width: 188, height: 11, 'font-size': 11, }}>
<span>
Créances reportables et restituables
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 908, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B03
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 908, width: 264, height: 11, 'font-size': 11, }}>
<span>
CIC - Crédit d&#39;impôt compétitivité emploi – Année N
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 507, top: 908, width: 136, height: 12, 'font-size': 11, }}>
<span>
(montant du préfinancement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 680, top: 907, width: 15, height: 10, 'font-size': 13, }}>
<span>
65
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 859, top: 907, width: 36, height: 12, 'font-size': 11, }}>
<span>
) 64
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 931, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B04
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 931, width: 273, height: 11, 'font-size': 11, }}>
<span>
CIC – Uniquement exercices &#62; 12 mois - Année N-1
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 507, top: 930, width: 136, height: 11, 'font-size': 11, }}>
<span>
(montant du préfinancement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 859, top: 930, width: 3, height: 11, 'font-size': 11, }}>
<span>
)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 953, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B05
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 952, width: 339, height: 12, 'font-size': 11, }}>
<span>
CIC - solde de créance des exercices antérieurs (Exercices N-3 à N-1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 975, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B06
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 974, width: 322, height: 12, 'font-size': 11, }}>
<span>
COR - Crédit d&#39;impôt pour investissement en CORSE au titre N
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 973, width: 14, height: 10, 'font-size': 13, }}>
<span>
33
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 997, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B07
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 997, width: 350, height: 11, 'font-size': 11, }}>
<span>
COR - solde de créance des exercices antérieurs (Exercices N-10 à N-1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1019, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B08
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1019, width: 243, height: 11, 'font-size': 11, }}>
<span>
RAD - Report en arrière de déficits au titre de N
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1017, width: 14, height: 11, 'font-size': 13, }}>
<span>
34
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1041, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B09
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1041, width: 343, height: 11, 'font-size': 11, }}>
<span>
RAD - solde de créance des exercices antérieurs (Exercices N-5 à N-1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1063, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B10
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1063, width: 213, height: 11, 'font-size': 11, }}>
<span>
CIR - Crédit impôt recherche au titre de N
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1061, width: 12, height: 10, 'font-size': 13, }}>
<span>
31
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1085, width: 28, height: 9, 'font-size': 11, }}>
<span>
II-B11
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1085, width: 339, height: 11, 'font-size': 11, }}>
<span>
CIR - solde de créance des exercices antérieurs (Exercices N-3 à N-1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1107, width: 29, height: 8, 'font-size': 11, }}>
<span>
II-B12
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1107, width: 216, height: 11, 'font-size': 11, }}>
<span>
Nouvelles créances non répertoriées de N
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1105, width: 14, height: 10, 'font-size': 13, }}>
<span>
50
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1129, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B13
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 1129, width: 222, height: 11, 'font-size': 11, }}>
<span>
Type de créance portée dans la ligne II-B11
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1151, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B14
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 261, top: 1150, width: 611, height: 11, 'font-size': 11, }}>
<span>
Sous total (total II-B01 à II-B12 sauf la ligne II-B08 et moins les données préfinancement dans la limite de la créance définitive)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1173, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-B15
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 1189, width: 406, height: 13, 'font-size': 13, }}>
<span>
II-C – Créances non reportables et restituables au titre de l&#39;exercice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1230, width: 28, height: 9, 'font-size': 11, }}>
<span>
II-C01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1230, width: 301, height: 12, 'font-size': 11, }}>
<span>
FOR - Crédit d’impôt formation des dirigeants d’entreprise
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 1229, width: 15, height: 10, 'font-size': 13, }}>
<span>
22
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1253, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C02
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1253, width: 311, height: 11, 'font-size': 11, }}>
<span>
RAC - Crédit pour le rachat d&#39;une entreprise par ses salariés
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 1251, width: 15, height: 10, 'font-size': 13, }}>
<span>
23
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1275, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C03
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1275, width: 143, height: 11, 'font-size': 11, }}>
<span>
FAM - Crédit d’impôt famille
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 1273, width: 15, height: 10, 'font-size': 13, }}>
<span>
24
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1297, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C04
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1297, width: 412, height: 11, 'font-size': 11, }}>
<span>
CIN - Crédit d&#39;impôt pour dépenses de production d&#39;œuvres cinématographique
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 1295, width: 15, height: 10, 'font-size': 13, }}>
<span>
25
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1319, width: 358, height: 11, 'font-size': 11, }}>
<span>
PTZ - Crédit d&#39;impôt en faveur de la première accession à la propriété
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1317, width: 14, height: 10, 'font-size': 13, }}>
<span>
30
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1341, width: 292, height: 11, 'font-size': 11, }}>
<span>
BIO - Crédit d&#39;impôt en faveur de l&#39;agriculture biologique
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 1339, width: 15, height: 10, 'font-size': 13, }}>
<span>
45
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1363, width: 408, height: 11, 'font-size': 11, }}>
<span>
PHO - Crédit d&#39;impôt pour dépenses de production d&#39;œuvres phonographiques
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 1361, width: 15, height: 11, 'font-size': 13, }}>
<span>
48
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1385, width: 289, height: 11, 'font-size': 11, }}>
<span>
MAI - Crédit d&#39;impôt en faveur des maîtres restaurateurs
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1383, width: 14, height: 11, 'font-size': 13, }}>
<span>
52
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 1407, width: 392, height: 11, 'font-size': 11, }}>
<span>
AUD - Crédit d&#39;impôt pour dépense de productions d’œuvres audiovisuelles
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1405, width: 14, height: 11, 'font-size': 13, }}>
<span>
53
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 1429, width: 246, height: 11, 'font-size': 11, }}>
<span>
ART - Crédit d&#39;impôt en faveur des métiers d&#39;art
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1427, width: 14, height: 11, 'font-size': 13, }}>
<span>
56
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1451, width: 301, height: 11, 'font-size': 11, }}>
<span>
CJV - Crédit d&#39;impôt en faveur des créateurs de jeux vidéo
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1450, width: 14, height: 10, 'font-size': 13, }}>
<span>
58
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1473, width: 577, height: 11, 'font-size': 11, }}>
<span>
CPE - Crédit d&#39;impôt sur les avances remboursables pour travaux d’amélioration de la performance énergétique
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1472, width: 14, height: 10, 'font-size': 13, }}>
<span>
60
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1495, width: 210, height: 11, 'font-size': 11, }}>
<span>
CCI - Crédit d&#39;impôt cinéma international
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1494, width: 12, height: 10, 'font-size': 13, }}>
<span>
61
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 1517, width: 248, height: 11, 'font-size': 11, }}>
<span>
PTR - Crédit d&#39;impôt prêt à taux 0 renforcé PTZ+
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 1516, width: 14, height: 10, 'font-size': 13, }}>
<span>
62
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 104, width: 57, height: 8, 'font-size': 10, }}>
<span>
N° 12404 * 17
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 121, width: 149, height: 10, 'font-size': 13, }}>
<span>
RELEVE DE SOLDE 2020
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 144, top: 225, width: 934, height: 12, 'font-size': 12, }}>
<span>
Le formulaire 2572 est dédié a la liquidation de l’impôt sur les sociétés et des contributions assimilées : paiement du solde ou constatation d&#39;un excédent d’impôt. Les
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 322, top: 239, width: 577, height: 12, 'font-size': 12, }}>
<span>
demandes de remboursement de crédits d’impôt doivent figurer obligatoirement sur le formulaire 2573.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 282, width: 617, height: 12, 'font-size': 12, }}>
<span>
- Suppression du crédit d&#39;impôt apprentissage (APR) pour les exercices ouverts à compter du 1er janvier 2019 ;
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 296, width: 862, height: 12, 'font-size': 12, }}>
<span>
- Suppression du crédit d&#39;impôt pour la compétitivité et l&#39;emploi (CICE) pour les exercices ouverts à compter du 1er janvier 2019 sauf pour les exploitations
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 309, width: 57, height: 12, 'font-size': 12, }}>
<span>
à Mayotte.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 323, width: 840, height: 12, 'font-size': 12, }}>
<span>
- Prise en compte du calcul d&#39;IS au nouveau taux normal à 31 % pour les exercices ouverts à compter du 1er janvier 2019 (voir notice sur les conditions
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 227, top: 337, width: 82, height: 12, 'font-size': 12, }}>
<span>
d&#39;application) .
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 427, width: 284, height: 13, 'font-size': 13, }}>
<span>
Impôt sur les sociétés (au taux normal à 33,1/3 %)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 448, width: 262, height: 13, 'font-size': 13, }}>
<span>
Impôt sur les sociétés (au taux normal à 31 %)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 471, width: 26, height: 9, 'font-size': 11, }}>
<span>
I-A03
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 470, width: 262, height: 13, 'font-size': 13, }}>
<span>
Impôt sur les sociétés (au taux normal à 28 %)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 494, width: 26, height: 9, 'font-size': 11, }}>
<span>
I-A04
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 516, width: 26, height: 9, 'font-size': 11, }}>
<span>
I-A05
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 538, width: 26, height: 9, 'font-size': 11, }}>
<span>
I-A06
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 560, width: 26, height: 9, 'font-size': 11, }}>
<span>
I-A07
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 626, width: 469, height: 11, 'font-size': 11, }}>
<span>
IMPORTANT : Les montants des créances du II-A doivent être portés pour le montant total.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 308, top: 758, width: 568, height: 11, 'font-size': 11, }}>
<span>
IS dû après imputation des créances non reportables et non restituables dans la limite de l&#39;impôt dû (I-A07 – II-A05)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 779, width: 164, height: 12, 'font-size': 13, }}>
<span>
II-B – Créances reportables
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 293, top: 781, width: 444, height: 10, 'font-size': 10, }}>
<span>
(au titre de l&#39;exercice pour le montant total et solde des créances reportables des exercices antérieurs)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 799, width: 830, height: 10, 'font-size': 10, }}>
<span>
IMPORTANT : Les montants des créances de N du II-B doivent être portés pour le montant total. Celui des années antérieures ne doit comporter que le solde restant à imputer.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 528, top: 842, width: 86, height: 11, 'font-size': 11, }}>
<span>
( dont UE ou EEE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 419, top: 1172, width: 457, height: 12, 'font-size': 11, }}>
<span>
IS dû après imputation des créances reportables dans la limite de l&#39;impôt dû (II-A06 – II-B14)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 124, top: 1209, width: 469, height: 11, 'font-size': 11, }}>
<span>
IMPORTANT : Les montants des créances du II-C doivent être portés pour le montant total.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1319, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C05
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1341, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C06
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1363, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C07
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1385, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C08
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1407, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C09
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1429, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C10
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1451, width: 28, height: 9, 'font-size': 11, }}>
<span>
II-C11
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1473, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C12
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1495, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C13
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 1518, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C14
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field pdf-input-text-align-left"
                   name="NOM_STEA" data-field-id="11178776" data-annot-id="9867312" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field "
                   name="ADR_STED" data-field-id="11179016" data-annot-id="9867504" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field "
                   name="ADR_STEF" data-field-id="11179352" data-annot-id="10199456" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field "
                   name="ADR_STEG" data-field-id="11179656" data-annot-id="10199648" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field "
                   name="VILLE_STEI" data-field-id="11179992" data-annot-id="10201616" type="text" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field "
                   name="CP_STEH" data-field-id="11234696" data-annot-id="10201808" type="text" disabled/>

            <Field component={ReduxPicker} dateFormatToShow="DD-MM-YYYY" className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field "
                   name="DEB_EX" data-field-id="11235032" data-annot-id="10202000" type="text" disabled/>

            <Field component={ReduxPicker} dateFormatToShow="DD-MM-YYYY" className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field "
                   name="FIN_EX" data-field-id="11235704" data-annot-id="10202192" type="text" disabled/>

            <Field component={ PdfInputValuesWithSpaces } className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field "
                   name="SIREN_STET" data-field-id="11235368" data-annot-id="10202384" maxLength="9" type="text"
                   disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field "
                   name="RA"
                   data-field-id="11239096" data-annot-id="10202576" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field "
                   name="TA"
                   data-field-id="11236184" data-annot-id="10202768" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field "
                   name="SA"
                   data-field-id="11236520" data-annot-id="10202960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field "
                   name="RB"
                   data-field-id="11236856" data-annot-id="10203152" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field "
                   name="TC"
                   data-field-id="11237192" data-annot-id="10203344" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field "
                   name="TB"
                   data-field-id="11237528" data-annot-id="10203536" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "
                   name="SB"
                   data-field-id="11240760" data-annot-id="10203728" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="TD"
                   data-field-id="11237960" data-annot-id="10203920" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field "
                   name="GC"
                   data-field-id="11241192" data-annot-id="10204384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="GD"
                   data-field-id="11241528" data-annot-id="10204576" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field "
                   name="GE"
                   data-field-id="11241864" data-annot-id="10204768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field "
                   name="JA"
                   data-field-id="11242200" data-annot-id="10204960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field "
                   name="MA"
                   data-field-id="11242536" data-annot-id="10205152" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field "
                   name="MH"
                   data-field-id="11242872" data-annot-id="10205344" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field "
                   name="MD"
                   data-field-id="11243208" data-annot-id="10205536" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field "
                   name="RH"
                   data-field-id="11243544" data-annot-id="10214720" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field "
                   name="RG"
                   data-field-id="11243880" data-annot-id="10214912" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field "
                   name="LD"
                   data-field-id="11244216" data-annot-id="10215104" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field "
                   name="LC"
                   data-field-id="11244552" data-annot-id="10215296" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field "
                   name="RJ"
                   data-field-id="11244888" data-annot-id="10215488" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field "
                   name="LE"
                   data-field-id="11245224" data-annot-id="10215680" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field "
                   name="OJ"
                   data-field-id="11245560" data-annot-id="10215872" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field "
                   name="OH"
                   data-field-id="11245896" data-annot-id="10216064" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field "
                   name="LF"
                   data-field-id="11246232" data-annot-id="10216256" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field "
                   name="RN"
                   data-field-id="11238264" data-annot-id="10204112" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field "
                   name="OB"
                   data-field-id="11247096" data-annot-id="10216976" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field "
                   name="RK"
                   data-field-id="11247432" data-annot-id="10217168" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field "
                   name="LB"
                   data-field-id="11247768" data-annot-id="10217360" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field "
                   name="RL"
                   data-field-id="11248104" data-annot-id="10217552" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field "
                   name="LA"
                   data-field-id="11248440" data-annot-id="10217744" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field "
                   name="RM"
                   data-field-id="11248776" data-annot-id="10217936" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field "
                   name="OC"
                   data-field-id="11249112" data-annot-id="10218128" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field "
                   name="RR"
                   data-field-id="11249448" data-annot-id="10218320" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field "
                   name="RP"
                   data-field-id="11249784" data-annot-id="10218512" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field "
                   name="RS"
                   data-field-id="11250120" data-annot-id="10218704" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field "
                   name="KA"
                   data-field-id="11250456" data-annot-id="10218896" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field "
                   name="NA"
                   data-field-id="11250792" data-annot-id="10219088" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field "
                   name="KB"
                   data-field-id="11251128" data-annot-id="10219280" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field "
                   name="NB"
                   data-field-id="11251464" data-annot-id="10219472" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field "
                   name="ND"
                   data-field-id="11251800" data-annot-id="10219664" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field "
                   name="NE"
                   data-field-id="11252136" data-annot-id="10219856" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field "
                   name="NF"
                   data-field-id="11252472" data-annot-id="10220048" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field "
                   name="NG"
                   data-field-id="11252808" data-annot-id="10220240" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field "
                   name="RC"
                   data-field-id="11253144" data-annot-id="10220432" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field "
                   name="NJ"
                   data-field-id="11253480" data-annot-id="10220624" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field "
                   name="NK"
                   data-field-id="11253816" data-annot-id="10220816" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field "
                   name="NM"
                   data-field-id="11254152" data-annot-id="10221008" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field "
                   name="NN"
                   data-field-id="11254488" data-annot-id="10221200" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field "
                   name="OD"
                   data-field-id="11254824" data-annot-id="10221392" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 207, top: 52, width: 206, height: 11, 'font-size': 11, }}>
<span>
CIO - Crédit d&#39;impôt outre mer Productif
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 507, top: 52, width: 136, height: 11, 'font-size': 11, }}>
<span>
(montant du préfinancement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 680, top: 50, width: 14, height: 10, 'font-size': 13, }}>
<span>
67
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 859, top: 50, width: 36, height: 12, 'font-size': 11, }}>
<span>
) 63
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 74, width: 214, height: 11, 'font-size': 11, }}>
<span>
COL - Crédit d&#39;impôt outre mer Logement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 507, top: 74, width: 136, height: 11, 'font-size': 11, }}>
<span>
(montant du préfinancement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 680, top: 72, width: 14, height: 11, 'font-size': 13, }}>
<span>
68
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 859, top: 72, width: 36, height: 12, 'font-size': 11, }}>
<span>
) 66
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 96, width: 192, height: 11, 'font-size': 11, }}>
<span>
CSV - Crédit d&#39;impôt spectacle vivant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 94, width: 14, height: 10, 'font-size': 13, }}>
<span>
70
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 118, width: 243, height: 11, 'font-size': 11, }}>
<span>
Nouvelles créances non répertoriées ci-dessus
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 116, width: 15, height: 10, 'font-size': 13, }}>
<span>
49
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 139, width: 223, height: 12, 'font-size': 11, }}>
<span>
Type de créance portée dans la ligne II-C19
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 204, width: 168, height: 12, 'font-size': 13, }}>
<span>
II-D – Acompte de l&#39;exercice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 228, width: 28, height: 9, 'font-size': 11, }}>
<span>
II-D01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 228, width: 436, height: 11, 'font-size': 11, }}>
<span>
Versements effectués (acomptes et/ou soldes) moins remboursements déjà obtenus
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 226, width: 14, height: 10, 'font-size': 13, }}>
<span>
69
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 248, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-D02
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 292, width: 28, height: 9, 'font-size': 11, }}>
<span>
II-E01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 292, width: 254, height: 11, 'font-size': 11, }}>
<span>
Montant d’impôt exclu du calcul des acomptes IS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 290, width: 14, height: 10, 'font-size': 13, }}>
<span>
38
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 677, top: 322, width: 21, height: 8, 'font-size': 10, }}>
<span>
Base
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 809, top: 322, width: 18, height: 8, 'font-size': 10, }}>
<span>
taux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 324, width: 35, height: 8, 'font-size': 10, }}>
<span>
Montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 349, width: 135, height: 9, 'font-size': 11, }}>
<span>
Montant de la CSB sur l’IS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 802, top: 348, width: 31, height: 11, 'font-size': 11, }}>
<span>
3,30%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 347, width: 14, height: 10, 'font-size': 13, }}>
<span>
36
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 371, width: 311, height: 11, 'font-size': 11, }}>
<span>
Crédits d’impôt étrangers, autres que sur valeurs mobilières
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 369, width: 14, height: 11, 'font-size': 13, }}>
<span>
19
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 415, width: 436, height: 11, 'font-size': 11, }}>
<span>
Versements effectués (acomptes et/ou soldes) moins remboursements déjà obtenus
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 437, width: 32, height: 9, 'font-size': 11, }}>
<span>
III-A05
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 973, top: 465, width: 35, height: 8, 'font-size': 10, }}>
<span>
Montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 490, width: 446, height: 9, 'font-size': 11, }}>
<span>
Montant du chiffre d&#39;affaire soumis à la contribution annuelle sur les Revenus Locatifs
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 881, top: 510, width: 14, height: 11, 'font-size': 13, }}>
<span>
37
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 206, top: 534, width: 436, height: 11, 'font-size': 11, }}>
<span>
Versements effectués (acomptes et/ou soldes) moins remboursements déjà obtenus
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 586, width: 545, height: 11, 'font-size': 13, }}>
<span>
RECAPITULATIF DES ELEMENTS DECLARES D&#39;IS ET DES CONTRIBUTIONS ASSIMILEES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 647, top: 608, width: 143, height: 13, 'font-size': 14, }}>
<span>
Montant restant à payer
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 857, top: 608, width: 124, height: 11, 'font-size': 14, }}>
<span>
Excédents constatés
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 630, width: 135, height: 13, 'font-size': 13, }}>
<span>
Impôt sur les Sociétés
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 630, width: 13, height: 11, 'font-size': 13, }}>
<span>
01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 839, top: 630, width: 15, height: 11, 'font-size': 13, }}>
<span>
06
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 653, width: 124, height: 10, 'font-size': 13, }}>
<span>
Contribution Sociale
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 653, width: 14, height: 10, 'font-size': 13, }}>
<span>
03
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 839, top: 653, width: 14, height: 10, 'font-size': 13, }}>
<span>
08
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 674, width: 228, height: 11, 'font-size': 13, }}>
<span>
Contribution sur les Revenus Locatifs
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 675, width: 14, height: 10, 'font-size': 13, }}>
<span>
04
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 839, top: 675, width: 14, height: 10, 'font-size': 13, }}>
<span>
09
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 165, top: 697, width: 42, height: 10, 'font-size': 13, }}>
<span>
Totaux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 697, width: 14, height: 10, 'font-size': 13, }}>
<span>
05
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 840, top: 697, width: 13, height: 10, 'font-size': 13, }}>
<span>
10
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 723, width: 212, height: 13, 'font-size': 13, }}>
<span>
Montant à payer (case 05 - case 10)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 723, width: 12, height: 10, 'font-size': 13, }}>
<span>
11
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 413, top: 855, width: 301, height: 13, 'font-size': 13, }}>
<span>
ou montant total de l&#39;excédent (case 10 – case 05)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 840, top: 855, width: 13, height: 10, 'font-size': 13, }}>
<span>
12
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 197, top: 742, width: 347, height: 10, 'font-size': 12, }}>
<span>
Utilisation des excédents d&#39;IS et des contributions assimilées
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 886, width: 419, height: 12, 'font-size': 12, }}>
<span>
Montant de l&#39;excédent imputé sur le premier acompte de l&#39;exercice suivant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 840, top: 886, width: 13, height: 10, 'font-size': 13, }}>
<span>
13
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 909, width: 496, height: 12, 'font-size': 12, }}>
<span>
Demande d&#39;imputation sur échéance future IEF (Impôts ou taxe réglé par cette modalité)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 932, width: 104, height: 10, 'font-size': 12, }}>
<span>
Contribution visée
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 931, width: 142, height: 13, 'font-size': 13, }}>
<span>
Date limite de paiement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 740, top: 931, width: 111, height: 13, 'font-size': 13, }}>
<span>
Montant à Imputer
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 976, width: 539, height: 12, 'font-size': 12, }}>
<span>
Remboursement d&#39;excédent de versement demandé (case 12 - case 13 – Montant à imputer IEF)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 840, top: 975, width: 14, height: 10, 'font-size': 13, }}>
<span>
14
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 165, top: 994, width: 735, height: 9, 'font-size': 9, }}>
<span>
Votre service des impôts vous informera des suites données (rejet ou admission) à votre demande d’imputation. Si elle est acceptée, votre échéance de paiement sera créditée de la somme que vous avez indiquée.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 1016, width: 307, height: 12, 'font-size': 13, }}>
<span>
PAIEMENT, DATE ET SIGNATURE DU REDEVABLE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 1038, width: 33, height: 11, 'font-size': 13, }}>
<span>
Date :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 309, top: 1038, width: 60, height: 13, 'font-size': 13, }}>
<span>
Téléphone
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 1060, width: 44, height: 13, 'font-size': 13, }}>
<span>
Chèque
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 309, top: 1060, width: 78, height: 13, 'font-size': 13, }}>
<span>
Télépaiement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 1083, width: 51, height: 10, 'font-size': 13, }}>
<span>
Virement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 1127, width: 413, height: 12, 'font-size': 13, }}>
<span>
contributions assimilées complétez les cases ci-dessous (TVA, TS, TVS).
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 1149, width: 102, height: 10, 'font-size': 13, }}>
<span>
Contribution visée
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 413, top: 1149, width: 134, height: 12, 'font-size': 13, }}>
<span>
Date limite de paiement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 740, top: 1149, width: 104, height: 12, 'font-size': 13, }}>
<span>
Montant à Imputer
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 1178, width: 245, height: 10, 'font-size': 13, }}>
<span>
CADRE RESERVE A L&#39;ADMINISTRATION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 1198, width: 57, height: 12, 'font-size': 15, }}>
<span>
Somme :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 1202, width: 74, height: 11, 'font-size': 11, }}>
<span>
N° d&#39;opération :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 782, top: 1200, width: 106, height: 13, 'font-size': 13, }}>
<span>
Date de réception :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 52, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C15
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 74, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C16
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 96, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C17
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 118, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C18
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 140, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C19
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 162, width: 29, height: 9, 'font-size': 11, }}>
<span>
II-C20
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 316, top: 161, width: 560, height: 11, 'font-size': 11, }}>
<span>
Sous total (total des lignes II-C01 à II-C18 moins les données préfinancement dans la limite de la créance définitive)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 139, top: 183, width: 28, height: 9, 'font-size': 11, }}>
<span>
II-C21
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 329, top: 183, width: 547, height: 11, 'font-size': 11, }}>
<span>
IS dû après imputation des créances non reportables et restituables dans la limite de l&#39;impôt dû (II-B14–II-C20)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 248, width: 658, height: 11, 'font-size': 11, }}>
<span>
L&#39;IS à payer (ligne II-C21 - ligne II-D01) est reporté en case 01 / L&#39;excédent d&#39;IS ( ligne II-D01 - ligne II-C21) est reporté en case 06
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 268, width: 399, height: 13, 'font-size': 13, }}>
<span>
II-E – Données utiles au calcul des acomptes de l’exercice suivant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 321, width: 411, height: 13, 'font-size': 13, }}>
<span>
III - Montant de la contribution sociale sur l&#39;IS (CSB – art. 235 ter ZC)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 349, width: 30, height: 9, 'font-size': 11, }}>
<span>
III-A01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 371, width: 31, height: 9, 'font-size': 11, }}>
<span>
III-A02
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 393, width: 31, height: 9, 'font-size': 11, }}>
<span>
III-A03
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 393, width: 467, height: 11, 'font-size': 11, }}>
<span>
Montant de la Contribution Sociale sur l’IS due au titre de l’exercice (Ligne III-A01 – III-A02)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 138, top: 415, width: 32, height: 9, 'font-size': 11, }}>
<span>
III-A04
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 436, width: 782, height: 12, 'font-size': 11, }}>
<span>
Le solde de CSB à payer (Ligne III-A03 – Ligne III-A04) est reporté en Ligne 03 – L&#39;excédent de CSB (Ligne III-A04 – Ligne III-A03) est reporté en Ligne 08
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 125, top: 462, width: 421, height: 13, 'font-size': 13, }}>
<span>
IV - Montant de la contribution annuelle sur les revenus locatifs (CRL)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 490, width: 32, height: 9, 'font-size': 11, }}>
<span>
IV-A01
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 512, width: 33, height: 9, 'font-size': 11, }}>
<span>
IV-A02
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 512, width: 430, height: 11, 'font-size': 11, }}>
<span>
Montant de la contribution annuelle sur les Revenus Locatifs (Ligne IV-A01 x 2,5 %)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 534, width: 33, height: 9, 'font-size': 11, }}>
<span>
IV-A03
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 137, top: 556, width: 33, height: 9, 'font-size': 11, }}>
<span>
IV-A04
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 555, width: 786, height: 12, 'font-size': 11, }}>
<span>
Le solde de CRL à payer (Ligne IV-A02 – Ligne IV-A03) est reporté en Ligne 04 – L&#39;excédent de CRL (Ligne IV-A03 – Ligne IV-A02) est reporté en Ligne 09
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 166, top: 954, width: 566, height: 12, 'font-size': 12, }}>
<span>
(Vous pouvez choisir une imputation sur les taxes et impôts suivants : TVA, TS, TVS, FPC, PEEC, TA)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 618, top: 1046, width: 448, height: 13, 'font-size': 13, }}>
<span>
Le télépaiement est obligatoire quel que soit le chiffre d&#39;affaires de votre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 617, top: 1060, width: 288, height: 13, 'font-size': 13, }}>
<span>
entreprise. Une pénalité de 0,2% sera appliquée
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 617, top: 1075, width: 123, height: 13, 'font-size': 13, }}>
<span>
(article 1738 du CGI)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 145, top: 1109, width: 798, height: 13, 'font-size': 13, }}>
<span>
Paiement du relevé de solde par « Imputation ». Si vous souhaitez utiliser un trop versé d&#39;une autre taxe pour acquitter le montant d&#39; IS et des
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 372, top: 1200, width: 33, height: 11, 'font-size': 13, }}>
<span>
Date :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 1235, width: 939, height: 9, 'font-size': 10, }}>
<span>
Les dispositions de l&#39;article 39 et 40 de la loi n° 78-17 du 6 janvier 1978 relative à l&#39;informatique, aux fichiers et aux libertés, modifiée par la loi n° 2004-801 du 6 août 2004, garantissant les droits des personnes physiques à l&#39;égard des
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 515, top: 1245, width: 191, height: 10, 'font-size': 10, }}>
<span>
traitements des données à caractère personnel
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 761, width: 235, height: 13, 'font-size': 13, }}>
<span>
Contribution visée (TVA ou US ou TVS)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 784, width: 142, height: 12, 'font-size': 13, }}>
<span>
Date limite de paiement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 805, width: 110, height: 13, 'font-size': 13, }}>
<span>
Montant à imputer
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 399, top: 828, width: 425, height: 12, 'font-size': 13, }}>
<span>
Montant à payer après imputation d&#39;une autre taxe (case 11 - case 133)
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="OE"
                   data-field-id="11255256" data-annot-id="9098480" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="OG"
                   data-field-id="11258584" data-annot-id="9085552" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="KD"
                   data-field-id="11261864" data-annot-id="9085744" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="OF"
                   data-field-id="11352760" data-annot-id="10473888" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="OK"
                   data-field-id="11355912" data-annot-id="10474080" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="NO"
                   data-field-id="11359192" data-annot-id="10474272" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="RD"
                   data-field-id="11362472" data-annot-id="10474464" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="RE"
                   data-field-id="11246440" data-annot-id="10474656" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="RF"
                   data-field-id="11368952" data-annot-id="10474848" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="RT"
                   data-field-id="11372232" data-annot-id="10471680" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="IA"
                   data-field-id="11375512" data-annot-id="10471872" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="RW"
                   data-field-id="11378792" data-annot-id="10472064" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="RZ"
                   data-field-id="11382072" data-annot-id="10472256" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="MB"
                   data-field-id="11385352" data-annot-id="10472448" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="PA"
                   data-field-id="11388632" data-annot-id="10472640" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="PB"
                   data-field-id="11391912" data-annot-id="10472832" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field " name="PC"
                   data-field-id="11395192" data-annot-id="10473024" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="PM"
                   data-field-id="11398472" data-annot-id="10483024" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="PD"
                   data-field-id="11401752" data-annot-id="10483216" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="PN"
                   data-field-id="11405032" data-annot-id="10483408" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field " name="PT"
                   data-field-id="11408312" data-annot-id="10483600" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field " name="PV"
                   data-field-id="11411592" data-annot-id="10483792" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field " name="AD"
                   data-field-id="11414872" data-annot-id="10483984" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field " name="CA"
                   data-field-id="11418152" data-annot-id="10484176" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field " name="BD"
                   data-field-id="11421320" data-annot-id="10484368" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field " name="PW"
                   data-field-id="11425704" data-annot-id="10484560" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="PU"
                   data-field-id="11438760" data-annot-id="10484752" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="PQ"
                   data-field-id="11442056" data-annot-id="10484944" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="DA"
                   data-field-id="11445352" data-annot-id="10485136" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field " name="EA"
                   data-field-id="11448648" data-annot-id="10485328" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="PG"
                   data-field-id="11504552" data-annot-id="10485520" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field " name="EB"
                   data-field-id="11451672" data-annot-id="10485712" type="text" disabled/>

            <Field component={ReduxPicker} handleChange={change} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field " name="PF"
                   data-field-id="11507688" data-annot-id="10485904" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="PE"
                   data-field-id="11510456" data-annot-id="10482752" type="text"/>

            <Field component={ReduxPicker} handleChange={change} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field "
                   name="DATE_SIGNATURE" data-field-id="11454632" data-annot-id="10486624" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field "
                   name="TEL_REDEVABLE" data-field-id="11457672" data-annot-id="10486816" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field "
                   name="CONTRIBUTION_VISEE" data-field-id="11467992" data-annot-id="10487008" type="text"/>

            <Field component={ReduxPicker} handleChange={change} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field "
                   name="DATE_ECHEANCE" data-field-id="11460712" data-annot-id="10487200" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field "
                   name="CONTRIBUTION_VISEE" data-field-id="11467992" data-annot-id="10487392" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field "
                   name="CHEQUE" data-field-id="11471208" data-annot-id="10487584" value="1" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field "
                   name="VIREMENT" data-field-id="11475720" data-annot-id="10487776" value="1" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field "
                   name="TELEPAIEMENT" data-field-id="11480408" data-annot-id="10487968" value="1" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field "
                   name="PH"
                   data-field-id="11490696" data-annot-id="10488160" type="text"/>

            <Field component={ReduxPicker} handleChange={change} className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field "
                   name="PJ"
                   data-field-id="11493288" data-annot-id="10488352" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field "
                   name="PK"
                   data-field-id="11498936" data-annot-id="10488544" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field "
                   name="PL"
                   data-field-id="11501464" data-annot-id="10488736" type="text"/>

          </div>
        </div>

      </div>
    </div>
  );
};

export default IS2572;
