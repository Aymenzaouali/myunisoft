/* eslint-disable */
import React from "react";
import { Field } from "redux-form";
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, ReduxPicker } from 'components/reduxForm/Inputs';
import PdfInputValuesWithSpaces from 'components/reduxForm/Inputs/PdfInputValuesWithSpaces';
import { useCompute, sum, copy, parse as p } from 'helpers/pdfforms';

import "./style.scss";

const IS2571 = (props) => {
  const { change, isForm } = props;

  useCompute('WA', sum, ['WB', 'WC', 'WD', 'WE', 'WF', 'WG'], isForm, change);
  useCompute('FA', ({ WA }) =>  p(WA),['WA'], isForm, change); // LINE 01 = A15
  useCompute('CC', ({ YC }) =>  p(YC),['YC'], isForm, change); // LINE 07 = B02
  useCompute('CD', ({ YD }) =>  p(YD),['YD'], isForm, change); // LINE 08 = B05
  useCompute('CE', ({YE}) =>  p(YE),['YE'], isForm, change); // LINE 10 = C01
  useCompute('FB', ({WB}) =>  p(WB), ['WB'], isForm, change); // LINE 20 = A09
  useCompute('FC', ({WC}) =>  p(WC), ['WC'], isForm, change); // LINE 21 = A10
  useCompute('FD', ({WD}) =>  p(WD), ['WD'], isForm, change); // LINE 22 = A11
  useCompute('FE', ({WE}) =>  p(WE), ['WE'], isForm, change); // LINE 23 = A12
  useCompute('FF', ({WF}) =>  p(WF), ['WF'], isForm, change); // LINE 24 = A13
  useCompute('FG', ({WG}) =>  p(WG), ['WG'], isForm, change); // LINE 25 = A14
  useCompute('CA', ({ ZR, FA }) =>  p(ZR) - p(FA),['ZR', 'FA'], isForm, change);
  useCompute('ZX', ({ ZW, ZV }) =>  p(ZW) * p(ZV) / 100,['ZW', 'ZV'], isForm, change);
  useCompute('ZW', () =>  0.825,[], isForm, change);
  useCompute('ZU', ({ ZT, ZS }) =>  p(ZT) * p(ZS) / 100,['ZT', 'ZS'], isForm, change);
  useCompute('ZT', () =>  parseFloat('2.50').toFixed(2),[], isForm, change);
  useCompute('ZR', sum, ['ZQ', 'ZP'], isForm, change);
  useCompute('ZP', sum, ['ZC', 'ZF', 'ZI', 'ZL', 'ZO'], isForm, change);
  useCompute('ZO', ({ ZN, ZM }) =>  p(ZN) * p(ZM) / 100,['ZN', 'ZM'], isForm, change);
  useCompute('ZN', () =>  3.75,[], isForm, change);
  useCompute('ZL', ({ ZK, ZJ }) =>  p(ZK) * p(ZJ) / 100,['ZK', 'ZJ'], isForm, change);
  useCompute('ZK', () =>  3.75,[], isForm, change);
  useCompute('ZI', ({ ZH, ZG }) =>  p(ZH) * p(ZG) / 100,['ZH', 'ZG'], isForm, change);
  useCompute('ZH', () =>  parseFloat('7.00').toFixed(2),[], isForm, change);
  useCompute('ZF', ({ ZE, ZD }) =>  p(ZE) * p(ZD) / 100,['ZE', 'ZD'], isForm, change);
  useCompute('ZE', () =>  7.75,[], isForm, change);
  useCompute('ZC', ({ ZB, ZA }) =>  p(ZB) * p(ZA) / 100,['ZB', 'ZA'], isForm, change);
  useCompute('ZB', () =>  parseFloat('8.30').toFixed(2),[], isForm, change);
  useCompute('ZQ', () =>  0,[], isForm, change);
  useCompute('CD', ({ZX, ZY}) =>  p(ZX) + p(ZY),['ZX', 'ZY'], isForm, change);
  useCompute('YH', ({YL, YE}) =>  p(YL) + p(YE),['YL', 'YE'], isForm, change); // LINE C03: amount = C01 + C02
  useCompute('CH', copy, ['YH'], isForm, change); // LINE 12 = C03
  useCompute('CL', copy, ['YL'], isForm, change); // LINE 13 = C02
  useCompute('YD', ({ZX, ZY}) =>  p(ZX) + p(ZY),['ZX', 'ZY'], isForm, change);
  useCompute('YE', ({YA, YD}) =>  p(YA) + p(YD),['YA', 'YD'], isForm, change); // LINE C01: amount = A16 + B05
  useCompute('YA', ({ZR, WA}) =>  p(ZR) - p(WA), ['ZR', 'WA'], isForm, change);
  useCompute('FN', ({CH, FH}) =>  p(CH) - p(FH), ['CH', 'FH'], isForm, change);

  return (
    <div className="IS2571">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 97, top: 58, width: 346, height: 12, 'font-size': 14, }}>
<span>
DIRECTION GENERALE DES FINANCES PUBLIQUES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 976, top: 55, width: 70, height: 14, 'font-size': 18, }}>
<span>
2571-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 93, width: 232, height: 14, 'font-size': 18, }}>
<span>
IMPOT SUR LES SOCIETES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 131, width: 223, height: 11, 'font-size': 14, }}>
<span>
ET CONTRIBUTIONS ASSIMILEES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 96, top: 165, width: 89, height: 14, 'font-size': 18, }}>
<span>
ACOMPTE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 332, top: 227, width: 107, height: 11, 'font-size': 14, }}>
<span>
DENOMINATION
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 266, width: 52, height: 11, 'font-size': 14, }}>
<span>
Adresse
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 101, top: 338, width: 35, height: 10, 'font-size': 12, }}>
<span>
SIRET
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 490, top: 338, width: 102, height: 10, 'font-size': 12, }}>
<span>
Exercice social du
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 767, top: 341, width: 21, height: 7, 'font-size': 12, }}>
<span>
au :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 394, width: 317, height: 12, 'font-size': 14, }}>
<span>
Versements d&#39;IS et des contributions assimilées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 719, top: 427, width: 67, height: 12, 'font-size': 12, }}>
<span>
Imputations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 892, top: 427, width: 98, height: 12, 'font-size': 12, }}>
<span>
Montants à payer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 379, top: 485, width: 6, height: 3, 'font-size': 14, }}>
<span>
..
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 477, width: 257, height: 13, 'font-size': 14, }}>
<span>
Impôt sur les Sociétés……………………
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 477, width: 14, height: 11, 'font-size': 14, }}>
<span>
01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 851, top: 477, width: 15, height: 11, 'font-size': 14, }}>
<span>
03
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1062, top: 477, width: 16, height: 11, 'font-size': 14, }}>
<span>
04
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 238, top: 516, width: 338, height: 14, 'font-size': 14, }}>
<span>
Dont crédit d&#39;impôt pour la compétitivité et l&#39;emploi
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 516, width: 15, height: 11, 'font-size': 14, }}>
<span>
20
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 238, top: 554, width: 208, height: 14, 'font-size': 14, }}>
<span>
Dont report en arrière de déficit
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 555, width: 14, height: 11, 'font-size': 14, }}>
<span>
21
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 238, top: 593, width: 328, height: 14, 'font-size': 14, }}>
<span>
Dont crédit d&#39;impôt pour investissement en Corse
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 593, width: 15, height: 11, 'font-size': 14, }}>
<span>
22
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 238, top: 632, width: 196, height: 14, 'font-size': 14, }}>
<span>
Dont crédit d&#39;impôt recherche
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 632, width: 15, height: 12, 'font-size': 14, }}>
<span>
23
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 238, top: 671, width: 92, height: 11, 'font-size': 14, }}>
<span>
Dont mécénat
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 671, width: 16, height: 11, 'font-size': 14, }}>
<span>
24
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 238, top: 710, width: 245, height: 14, 'font-size': 14, }}>
<span>
Dont excédent du précédent exercice
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 710, width: 16, height: 11, 'font-size': 14, }}>
<span>
25
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 768, width: 250, height: 11, 'font-size': 14, }}>
<span>
Contribution sur les Revenus Locatifs
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 851, top: 768, width: 15, height: 11, 'font-size': 14, }}>
<span>
07
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 807, width: 136, height: 11, 'font-size': 14, }}>
<span>
Contribution Sociale
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 851, top: 807, width: 15, height: 11, 'font-size': 14, }}>
<span>
08
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1062, top: 807, width: 16, height: 11, 'font-size': 14, }}>
<span>
09
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 353, top: 908, width: 456, height: 14, 'font-size': 14, }}>
<span>
Montant d&#39;impôt sur les sociétés et contributions assimilées à payer :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 851, top: 904, width: 15, height: 11, 'font-size': 14, }}>
<span>
10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 955, width: 239, height: 14, 'font-size': 14, }}>
<span>
Plus-value article 208C du CGI (SIIC)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 851, top: 955, width: 15, height: 11, 'font-size': 14, }}>
<span>
13
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 997, width: 146, height: 14, 'font-size': 14, }}>
<span>
Montant total à payer :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 851, top: 993, width: 15, height: 11, 'font-size': 14, }}>
<span>
12
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 1152, width: 268, height: 13, 'font-size': 14, }}>
<span>
Paiement, date et signature du redevable
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 121, top: 1185, width: 35, height: 12, 'font-size': 14, }}>
<span>
Date :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 308, top: 1185, width: 65, height: 14, 'font-size': 14, }}>
<span>
Téléphone
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 1219, width: 48, height: 14, 'font-size': 14, }}>
<span>
Chèque
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 308, top: 1219, width: 84, height: 14, 'font-size': 14, }}>
<span>
Télépaiement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 1253, width: 56, height: 11, 'font-size': 14, }}>
<span>
Virement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 1345, width: 111, height: 11, 'font-size': 14, }}>
<span>
Contribution visée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 426, top: 1345, width: 117, height: 11, 'font-size': 14, }}>
<span>
Date de l&#39;échéance
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 779, top: 1345, width: 112, height: 14, 'font-size': 14, }}>
<span>
Montant à Imputer
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 1394, width: 209, height: 11, 'font-size': 14, }}>
<span>
Cadre reservé à l&#39;administration
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 1432, width: 41, height: 9, 'font-size': 12, }}>
<span>
Somme
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 308, top: 1432, width: 25, height: 9, 'font-size': 12, }}>
<span>
Date
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 1432, width: 81, height: 12, 'font-size': 12, }}>
<span>
N° d&#39;opération :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 802, top: 1432, width: 100, height: 12, 'font-size': 12, }}>
<span>
Date de réception :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 983, top: 132, width: 56, height: 9, 'font-size': 10, }}>
<span>
N° 12403*16
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 956, top: 150, width: 110, height: 7, 'font-size': 9, }}>
<span>
Art. 358 Annexe III au CGI
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 991, top: 166, width: 40, height: 14, 'font-size': 18, }}>
<span>
2019
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1048, top: 413, width: 67, height: 10, 'font-size': 12, }}>
<span>
Minorations
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1046, top: 427, width: 71, height: 12, 'font-size': 12, }}>
<span>
(Art. 1668 du
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1069, top: 441, width: 25, height: 12, 'font-size': 12, }}>
<span>
CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 638, top: 1204, width: 465, height: 14, 'font-size': 14, }}>
<span>
Le télépaiement est obligatoire quelque soit le chiffre d&#39;affaires de votre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 637, top: 1219, width: 296, height: 14, 'font-size': 14, }}>
<span>
entreprise. Une pénalité de 0,2% sera appliquée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 1234, width: 129, height: 15, 'font-size': 14, }}>
<span>
(article 1738 du CGI)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 1306, width: 302, height: 14, 'font-size': 14, }}>
<span>
Paiement du relevé d&#39;acompte par « Imputation »
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 1483, width: 1022, height: 10, 'font-size': 10, }}>
<span>
Les dispositions de l&#39;article 39 et 40 de la loi n° 78-17 du 6 janvier 1978 relative à l&#39;informatique, aux fichiers et aux libertés, modifiée par la loi n° 2004-801 du 6 août 2004, garantissant les droits des personnes physiques à l&#39;égard des
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 508, top: 1494, width: 207, height: 10, 'font-size': 10, }}>
<span>
traitements des données à caractère personnel
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 466, top: 1102, width: 346, height: 14, 'font-size': 14, }}>
<span>
Montant de l&#39;imposition à payer après imputation :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 849, top: 1104, width: 15, height: 11, 'font-size': 14, }}>
<span>
34
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 478, top: 1070, width: 270, height: 12, 'font-size': 14, }}>
<span>
Date échéance de la contribution utilisée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 478, top: 1047, width: 345, height: 15, 'font-size': 14, }}>
<span>
Contribution utilisée pour l&#39;imputation (TS/TVA/TVS)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 478, top: 1024, width: 198, height: 14, 'font-size': 14, }}>
<span>
Dont Paiement par imputation
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 851, top: 1026, width: 14, height: 11, 'font-size': 14, }}>
<span>
31
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 849, top: 1048, width: 16, height: 11, 'font-size': 14, }}>
<span>
32
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 849, top: 1072, width: 15, height: 11, 'font-size': 14, }}>
<span>
33
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="FA"
                   data-field-id="34256888" data-annot-id="33525728" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="CA"
                   data-field-id="34266728" data-annot-id="33890688" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="FB"
                   data-field-id="34259416" data-annot-id="33890880" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="FC"
                   data-field-id="34260824" data-annot-id="33891072" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="FD"
                   data-field-id="34261048" data-annot-id="33891264" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="FE"
                   data-field-id="34262264" data-annot-id="33891456" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="FF"
                   data-field-id="34263672" data-annot-id="33891648" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="FG"
                   data-field-id="34265320" data-annot-id="33891840" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="CC"
                   data-field-id="34268136" data-annot-id="33892032" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="CD"
                   data-field-id="34269544" data-annot-id="33892224" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="CL"
                   data-field-id="34273608" data-annot-id="33892416" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="CH"
                   data-field-id="34275016" data-annot-id="33892608" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="CE"
                   data-field-id="34272200" data-annot-id="33892800" type="text" disabled/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field pdf-input-text-align-left "
                   name="NOM_STEA" data-field-id="34354408" data-annot-id="33892992" type="text" disabled/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field "
                   name="VILLE_STEI" data-field-id="34356888" data-annot-id="33893184" type="text" disabled/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "
                   name="ADR_STED" data-field-id="34357320" data-annot-id="33893376" type="text" disabled/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="CP_STEH" data-field-id="34358216" data-annot-id="33893568" type="text" disabled/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field "
                   name="ADR_STEF" data-field-id="34360712" data-annot-id="33894032" type="text" disabled/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="ADR_STEG" data-field-id="34361992" data-annot-id="33894224" type="text" disabled/>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope" className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="DA"
                   data-field-id="34363272" data-annot-id="33894416" value="1" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope" className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="DB"
                   data-field-id="34362424" data-annot-id="33894608" value="1" type="checkbox"
                   data-default-value="Off"/>

            <Field component={ReduxPicker} handleChange={change} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field "
                   name="DATE_SIGNATURE" data-field-id="34405288" data-annot-id="33894800" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field "
                   name="TEL_REDEVABLEZ" data-field-id="34402376" data-annot-id="33894992" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field "
                   name="CONTRIBUTION_VISEE" data-field-id="34405624" data-annot-id="33895184" type="text"/>

            <Field component={ReduxPicker} handleChange={change} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field "
                   name="DATE_ECHEANCE" data-field-id="34405960" data-annot-id="33895376" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field "
                   name="MONTANT_IMPUTER" data-field-id="34406296" data-annot-id="33895568" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope" className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field "
                   name="CHEQUE" data-field-id="34406632" data-annot-id="33895760" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope" className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field "
                   name="TELEPAIEMENT" data-field-id="34338280" data-annot-id="33895952" value="1" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} autoComplete="nope" className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field "
                   name="VIREMENT" data-field-id="34338584" data-annot-id="33896144" value="1" type="checkbox"
                   data-default-value="Off"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="FH"
                   data-field-id="34408008" data-annot-id="33896336" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="FM"
                   data-field-id="34408936" data-annot-id="33896528" type="text"/>

            <Field component={PdfNumberInput}  autoComplete="nope" className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="FN"
                   data-field-id="34408632" data-annot-id="33896720" type="text"/>

            <select className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="FK"
                    data-field-id="34410584" data-annot-id="33896912" data-default-value=" ">

              <option selected value=" ">
              </option>

              <option value="TS">
                TS
              </option>

              <option value="TVA">
                TVA
              </option>

              <option value="TVS">
                TVS
              </option>

            </select>

            <Field component={ReduxPicker} dateFormatToShow="DD-MM-YYYY" autoComplete="nope" className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field "
                   name="DEB_EX" data-field-id="34411016" data-annot-id="33893760" type="text" disabled/>

            <Field component={ReduxPicker} dateFormatToShow="DD-MM-YYYY" className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field "
                   name="FIN_EX" data-field-id="34412408" data-annot-id="33897632" type="text" disabled/>

            <Field component={PdfInputValuesWithSpaces} className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field "
                   name="SIRET_STET" data-field-id="34413688" data-annot-id="33897824" maxLength="14" type="text" disabled/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 761, top: 220, width: 31, height: 10, 'font-size': 12, }}>
<span>
BASE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 889, top: 220, width: 33, height: 10, 'font-size': 12, }}>
<span>
TAUX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1006, top: 220, width: 58, height: 10, 'font-size': 12, }}>
<span>
MONTANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 213, top: 235, width: 181, height: 12, 'font-size': 12, }}>
<span>
Détermination des acomptes bruts
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 250, width: 20, height: 10, 'font-size': 12, }}>
<span>
A01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 281, width: 22, height: 9, 'font-size': 12, }}>
<span>
A02
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 310, width: 22, height: 10, 'font-size': 12, }}>
<span>
A03
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 340, width: 22, height: 10, 'font-size': 12, }}>
<span>
A04
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 370, width: 437, height: 12, 'font-size': 12, }}>
<span>
A05 Acompte sur résultat net de concession des licences d’exploitation et inventions brevetables
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 248, top: 416, width: 163, height: 12, 'font-size': 12, }}>
<span>
A06 Total (lignes A01 à A05)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 248, top: 446, width: 198, height: 12, 'font-size': 12, }}>
<span>
A07 Régularisation du 1er acompte
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 248, top: 476, width: 283, height: 12, 'font-size': 12, }}>
<span>
A08 Montant d’acompte d’IS dû (ligne (A06 +/- A07)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1006, top: 506, width: 58, height: 10, 'font-size': 12, }}>
<span>
MONTANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 539, width: 22, height: 10, 'font-size': 12, }}>
<span>
A09
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 538, width: 15, height: 11, 'font-size': 14, }}>
<span>
20
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 573, width: 177, height: 14, 'font-size': 12, }}>
<span>
A10 Report en arrière de déficit
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 572, width: 13, height: 11, 'font-size': 14, }}>
<span>
21
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 608, width: 270, height: 14, 'font-size': 12, }}>
<span>
A11 Crédit d’impôt pour investissement en Corse
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 607, width: 15, height: 11, 'font-size': 14, }}>
<span>
22
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 642, width: 165, height: 14, 'font-size': 12, }}>
<span>
A12 Crédit d’impôt recherche
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 641, width: 15, height: 11, 'font-size': 14, }}>
<span>
23
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 677, width: 22, height: 9, 'font-size': 12, }}>
<span>
A13
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 676, width: 15, height: 11, 'font-size': 14, }}>
<span>
24
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 711, width: 585, height: 14, 'font-size': 12, }}>
<span>
A14 Imputation de l’excédent du précédent exercice (ne concerne que le 1er acompte de l&#39;exercice en cours)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 710, width: 15, height: 11, 'font-size': 14, }}>
<span>
25
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 248, top: 746, width: 432, height: 14, 'font-size': 12, }}>
<span>
A15 Total des imputations sur IS (Lignes A09 à A14) à reporter ligne 01 du 2571
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 745, width: 13, height: 11, 'font-size': 14, }}>
<span>
01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1006, top: 778, width: 58, height: 9, 'font-size': 12, }}>
<span>
MONTANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 810, width: 553, height: 14, 'font-size': 12, }}>
<span>
A16 Montant d&#39;IS à payer (Ligne A08 – ligne A15) éventuellement plafonné à reporter ligne 03 du 2571
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 809, width: 15, height: 11, 'font-size': 14, }}>
<span>
03
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 903, width: 355, height: 14, 'font-size': 14, }}>
<span>
Calcul du montant d&#39;acompte des contributions assimilées
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 761, top: 921, width: 31, height: 10, 'font-size': 12, }}>
<span>
BASE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 889, top: 921, width: 33, height: 10, 'font-size': 12, }}>
<span>
TAUX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1006, top: 921, width: 58, height: 10, 'font-size': 12, }}>
<span>
MONTANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 936, width: 92, height: 10, 'font-size': 12, }}>
<span>
B01 Taxe brute
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 969, width: 478, height: 14, 'font-size': 12, }}>
<span>
B02 Montant de contribution sur les revenus locatifs à payer à reporter ligne 07 du 2571
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 968, width: 15, height: 11, 'font-size': 14, }}>
<span>
07
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 761, top: 1001, width: 31, height: 10, 'font-size': 12, }}>
<span>
BASE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 889, top: 1001, width: 33, height: 10, 'font-size': 12, }}>
<span>
TAUX
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1006, top: 1001, width: 58, height: 10, 'font-size': 12, }}>
<span>
MONTANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 1016, width: 92, height: 10, 'font-size': 12, }}>
<span>
B03 Taxe brute
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 1046, width: 22, height: 10, 'font-size': 12, }}>
<span>
B04
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 1079, width: 636, height: 14, 'font-size': 12, }}>
<span>
B05 Montant de Contribution sociale à payer (Ligne B03 +/- B4) éventuellement plafonnée à reporter ligne 08 du 2571
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1078, width: 15, height: 11, 'font-size': 14, }}>
<span>
08
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 1173, width: 229, height: 11, 'font-size': 14, }}>
<span>
Calcul du montant total de versement
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1006, top: 1191, width: 58, height: 9, 'font-size': 12, }}>
<span>
MONTANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 1208, width: 450, height: 14, 'font-size': 12, }}>
<span>
C01 Montant total d&#39;IS et contributions assimilées à reporter sur la ligne 10 du 2571
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1207, width: 15, height: 11, 'font-size': 14, }}>
<span>
10
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 143, top: 1244, width: 333, height: 12, 'font-size': 12, }}>
<span>
Plus-value article 208C (SIIC) à reporter sur la ligne 13 du 2571
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1241, width: 15, height: 11, 'font-size': 14, }}>
<span>
13
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 143, top: 1278, width: 373, height: 12, 'font-size': 12, }}>
<span>
Montant total à payer (cases 10 + 13) à reporter sur la ligne 12 du 2571
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 945, top: 1275, width: 15, height: 11, 'font-size': 14, }}>
<span>
12
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 96, top: 168, width: 328, height: 8, 'font-size': 10, }}>
<span>
AIDE AU CALCUL D&#39;ACOMPTE – IS ET CONTRIBUTIONS ASSIMILEES
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 97, top: 179, width: 864, height: 10, 'font-size': 10, }}>
<span>
Cette page constitue une aide au calcul du paiement de l&#39;IS et des contributions assimilées. Les montants inscrits dans les cases 01 à 25 sont à reporter au resto de l&#39;imprimé à retourner au service.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 142, top: 250, width: 324, height: 12, 'font-size': 12, }}>
<span>
Acompte d’impôt sur les sociétés (au taux normal à 33,1/3 %)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 142, top: 280, width: 303, height: 13, 'font-size': 12, }}>
<span>
Acompte d’impôt sur les sociétés (au taux normal à 31 %)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 142, top: 310, width: 304, height: 12, 'font-size': 12, }}>
<span>
Acompte d’impôt sur les sociétés (au taux normal à 28 %)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 142, top: 340, width: 297, height: 12, 'font-size': 12, }}>
<span>
Acompte d’impôt sur les sociétés (au taux réduit à 15 %)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 143, top: 541, width: 238, height: 12, 'font-size': 12, }}>
<span>
Crédit d’impôt pour la compétitivité et l&#39;emploi
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 143, top: 679, width: 144, height: 12, 'font-size': 12, }}>
<span>
Réduction d’impôt mécénat
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 143, top: 851, width: 983, height: 9, 'font-size': 9, }}>
<span>
Si vous estimez que le montant des acomptes déjà versés (le présent acompte compris) est égal ou supérieur au montant de la cotisation totale dont vous serez redevable pour l’exercice concerné (article 1668 du CGI), La case « 04 » doit être cochée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 143, top: 1046, width: 157, height: 12, 'font-size': 12, }}>
<span>
Régularisation du 1er acompte
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 143, top: 1120, width: 983, height: 9, 'font-size': 9, }}>
<span>
Si vous estimez que le montant des acomptes déjà versés (le présent acompte compris) est égal ou supérieur au montant de la cotisation totale dont vous serez redevable pour l’exercice concerné (article 1668 du CGI), La case « 09 » doit être cochée
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 1242, width: 22, height: 10, 'font-size': 12, }}>
<span>
C02
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 1276, width: 22, height: 10, 'font-size': 12, }}>
<span>
C03
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="YH"
                   data-field-id="34398920" data-annot-id="33796560" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="YL"
                   data-field-id="34395768" data-annot-id="32453552" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="YE"
                   data-field-id="34392616" data-annot-id="32453776" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="YD"
                   data-field-id="34389464" data-annot-id="32453968" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="YC"
                   data-field-id="34386504" data-annot-id="32454160" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="YA"
                   data-field-id="34402072" data-annot-id="32454352" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="WA"
                   data-field-id="34386248" data-annot-id="32454544" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="WG"
                   data-field-id="34379464" data-annot-id="32454736" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="WF"
                   data-field-id="34376312" data-annot-id="32454928" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="WE"
                   data-field-id="34373160" data-annot-id="32455264" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="WD"
                   data-field-id="34370008" data-annot-id="32455456" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="WC"
                   data-field-id="34366856" data-annot-id="32455648" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="WB"
                   data-field-id="34363704" data-annot-id="32455840" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="ZA"
                   data-field-id="34275240" data-annot-id="32456032" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="ZB"
                   data-field-id="34278280" data-annot-id="32456224" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="ZC"
                   data-field-id="34281432" data-annot-id="32456416" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="ZD"
                   data-field-id="34284584" data-annot-id="32456608" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="ZE"
                   data-field-id="34287736" data-annot-id="33008576" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="ZF"
                   data-field-id="34291000" data-annot-id="33008768" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="ZG"
                   data-field-id="34294152" data-annot-id="33008960" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="ZH"
                   data-field-id="34297304" data-annot-id="33009152" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="ZI"
                   data-field-id="34300456" data-annot-id="33009344" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="ZJ"
                   data-field-id="34303608" data-annot-id="33009536" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="ZK"
                   data-field-id="34306760" data-annot-id="33009728" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="ZL"
                   data-field-id="34309912" data-annot-id="33009920" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="ZM"
                   data-field-id="34313064" data-annot-id="33010112" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="ZN"
                   data-field-id="34316216" data-annot-id="33010304" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="ZO"
                   data-field-id="34319368" data-annot-id="33010496" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="ZP"
                   data-field-id="34322520" data-annot-id="33010688" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field " name="ZQ"
                   data-field-id="34325672" data-annot-id="33010880" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field " name="ZR"
                   data-field-id="34328824" data-annot-id="33011072" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field " name="ZS"
                   data-field-id="34331976" data-annot-id="33011264" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field " name="ZT"
                   data-field-id="34335128" data-annot-id="33011456" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field " name="ZU"
                   data-field-id="34338808" data-annot-id="32456800" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field " name="ZV"
                   data-field-id="34341736" data-annot-id="32456992" type="text"/>

            <Field component="input" autoComplete="nope" className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="ZW"
                   data-field-id="34344904" data-annot-id="33012176" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field " name="ZX"
                   data-field-id="34348072" data-annot-id="33012368" type="text" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field " name="ZY"
                   data-field-id="34351240" data-annot-id="33012560" type="text"/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default IS2571;
