import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, TextField } from '@material-ui/core';
import { Field } from 'redux-form';
import { ReduxCheckBox } from 'components/reduxForm/Selections';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import I18n from 'assets/I18n';
import { AccountAutoComplete, CodeTvaAutoComplete } from 'containers/reduxForm/Inputs';
import _ from 'lodash';
import classNames from 'classnames';
import styles from './AccountGeneralInfo.module.scss';

const materialStyles = ({ palette }) => ({
  accountNumber: {
    width: 110,
    marginRight: 20
  },
  accountName: {
    width: 300
  },
  contrepart: {
    width: 300,
    marginTop: 20
  },
  TVA: {
    width: 120,
    marginTop: 15
  },
  presta: {
    paddingLeft: 10,
    paddingRight: 10,
    color: palette.secondary.main
  },
  intra: {
    paddingLeft: 10,
    paddingRight: 10,
    color: palette.secondary.main
  },
  nTvaDed: {
    width: 75,
    marginRight: 10
  },
  nTvaColl: {
    width: 95,
    marginLeft: 10,
    marginRight: 10
  },
  taux: {
    width: 47,
    marginLeft: 10,
    marginRight: 10
  },
  typeTva: {
    width: 200,
    marginLeft: 10,
    marginRight: 10
  },
  dueTva: {
    width: 200,
    marginLeft: 10,
    marginRight: 10
  },
  colorError: {
    color: palette.secondary.main
  }
});

const AccountGeneralInfo = (props) => {
  const {
    classes,
    newAccountForm,
    isContrepartie,
    selectedAccount,
    accountNumberDisabled
  } = props;

  const tva = _.get(newAccountForm, 'infoG.tva');
  return (
    <div className={styles.accountDialog}>
      <div className={styles.blockInline}>
        <Field
          color="primary"
          className={classes.accountNumber}
          name="infoG.account_number"
          component={ReduxTextField}
          label={I18n.t('account.newAccount.number')}
          autoFocus={_.isEmpty(selectedAccount) || !accountNumberDisabled}
          disabled={!_.isEmpty(selectedAccount) || accountNumberDisabled || isContrepartie}
        />
        <Field
          color="primary"
          className={classes.accountName}
          name="infoG.label"
          component={ReduxTextField}
          label={I18n.t('account.label')}
          autoFocus={!_.isEmpty(selectedAccount) || (accountNumberDisabled && !isContrepartie)}
          disabled={isContrepartie}
        />
      </div>
      <Field
        color="primary"
        className={classes.contrepart}
        name="infoG.counterpart_account"
        component={AccountAutoComplete}
        menuPosition="fixed"
        placeholder={I18n.t('account.contrepartie')}
        autoFocus={isContrepartie}
      />
      {/* TODO: Implement in scope of MYUN-637 */}
      <Field
        color="primary"
        className={classes.TVA}
        name="infoG.tva"
        component={CodeTvaAutoComplete}
        menuPosition="fixed"
        placeholder={I18n.t('account.newAccount.TVA')}
      />
      { tva
      && (
        <div className={classNames(styles.blockInline, styles.marginTop)}>
          <TextField
            color="primary"
            className={classes.nTvaDed}
            name="infoG.nTva"
            label={I18n.t('account.newAccount.nTvaDed')}
            disabled
            value={_.get(tva, 'account_ded.account_number', '')}
            InputLabelProps={{
              shrink: true
            }}
          />
          <TextField
            color="primary"
            className={classes.nTvaColl}
            name="infoG.nCptTva"
            label={I18n.t('account.newAccount.nTvaColl')}
            disabled
            value={_.get(tva, 'account_coll.account_number', '')}
            InputLabelProps={{
              shrink: true
            }}
          />
          <TextField
            color="primary"
            className={classes.taux}
            name="infoG.taux"
            label={I18n.t('account.newAccount.taux')}
            disabled
            value={_.get(tva, 'vat.rate', '')}
            InputLabelProps={{
              shrink: true
            }}
          />
          <TextField
            color="primary"
            className={classes.typeTva}
            name="infoG.typeTva"
            label={I18n.t('account.newAccount.typeTva')}
            disabled
            value={_.get(tva, 'vat_type.label', '')}
            InputLabelProps={{
              shrink: true
            }}
          />
          <TextField
            color="primary"
            className={classes.dueTva}
            name="infoG.dueTva"
            label={I18n.t('account.newAccount.dueTva')}
            disabled
            value={_.get(tva, 'vat_exigility.label', '')}
            InputLabelProps={{
              shrink: true
            }}
          />
        </div>
      )
      }
      <div className={styles.checkbox}>
        <Field
          color="primary"
          className={classes.intra}
          name="infoG.provider"
          component={ReduxCheckBox}
          label={I18n.t('account.newAccount.presta')}
          labelStyle={{ label: classes.colorError }}
        />
        <Field
          color="primary"
          className={classes.presta}
          name="infoG.intraco_account"
          component={ReduxCheckBox}
          label={I18n.t('account.intraco')}
          labelStyle={{ label: classes.colorError }}
        />
      </div>
    </div>
  );
};

AccountGeneralInfo.propTypes = {
  classes: PropTypes.shape({}),
  newAccountForm: PropTypes.shape({}).isRequired,
  codeTva: PropTypes.shape({}).isRequired,
  selectedAccount: PropTypes.string.isRequired,
  isContrepartie: PropTypes.bool,
  accountNumberDisabled: PropTypes.bool
};

AccountGeneralInfo.defaultProps = ({
  classes: {},
  isContrepartie: false,
  accountNumberDisabled: false
});

export default withStyles(materialStyles)(AccountGeneralInfo);
