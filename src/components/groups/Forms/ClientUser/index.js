import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import ClientUserTable from 'containers/groups/Tables/ClientUserList';
import { withStyles, Typography } from '@material-ui/core';
import { Field, Form } from 'redux-form';
import I18n from 'i18next';
import { InlineButton, CSVButton } from 'components/basics/Buttons';
import { usePrevious } from 'helpers/hooks';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { USER_TYPES } from 'assets/constants/data';
import styles from './ClientUser.module.scss';


const ClientUser = (props) => {
  const {
    classes, loadData, societyId,
    disabledClientUserExport,
    csvClientUserData,
    baseClientUserFileName,
    accounting_expert_options,
    manager_options,
    collab_options,
    getUserTypeOptions
  } = props;
  const prevSocietyId = usePrevious(societyId);

  /** fetch all user types for all drop-down */
  useEffect(() => {
    getUserTypeOptions(USER_TYPES.ACCOUNTANT);
    getUserTypeOptions(USER_TYPES.RM);
    getUserTypeOptions(USER_TYPES.COLLAB);
  }, []);


  useEffect(() => {
    if (prevSocietyId !== societyId) {
      loadData();
    }
  }, [societyId]);

  return (
    <div className={styles.clientUser}>
      <Form>
        <div className={styles.userTypes}>
          <Field
            name="expert"
            id="expert"
            isClearable
            label={I18n.t('companyCreation.accountingExpert')}
            options={accounting_expert_options || []}
            component={AutoComplete}
          />
          <Field
            isClearable
            options={manager_options || []}
            name="manager"
            component={AutoComplete}
            label={I18n.t('companyCreation.clientUser.manager')}
          />
          <Field
            component={AutoComplete}
            isClearable
            options={collab_options || []}
            name="collab"
            label={I18n.t('companyCreation.clientUser.caseManager')}
          />
        </div>
      </Form>
      <div className={styles.buttons}>
        <Typography classes={{ root: classes.title }}>Utilisateurs du client</Typography>
        <InlineButton buttons={[
          {
            _type: 'component',
            color: 'primary',
            disabled: disabledClientUserExport,
            component: <CSVButton
              csvData={csvClientUserData}
              baseFileName={baseClientUserFileName}
            />
          }
        ]}
        />
      </div>
      <div className={styles.table}>
        <ClientUserTable />
      </div>
    </div>
  );
};

const themeStyle = () => ({
  title: {
    width: 241,
    height: 17,
    fontWeight: 500,
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: 'black',
    fontSize: 14,
    fontFamily: 'basier_circlemedium'
  }
});

ClientUser.propTypes = {
  loadData: PropTypes.func.isRequired,
  getUserTypeOptions: PropTypes.func.isRequired,
  disabledClientUserExport: PropTypes.bool.isRequired,
  csvClientUserData: PropTypes.array.isRequired,
  baseClientUserFileName: PropTypes.string.isRequired,
  classes: PropTypes.shape({}).isRequired,
  accounting_expert_options: PropTypes.array.isRequired,
  manager_options: PropTypes.array.isRequired,
  collab_options: PropTypes.array.isRequired,
  societyId: PropTypes.number.isRequired
};

export default withStyles(themeStyle)(ClientUser);
