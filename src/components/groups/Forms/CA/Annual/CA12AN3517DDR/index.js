/* eslint-disable */
import React, {createRef} from "react";
import { Field } from "redux-form";
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { ReduxPicker } from 'components/reduxForm/Inputs';
import { sum, useCompute, parse as p, setValue } from 'helpers/pdfforms';
import {get as _get} from 'lodash';

import './style.scss';

const Annual = (props) => {
  const { change, ca12AN3517DDRForm, ca12AN3517Form } = props;

  const LB = _get(ca12AN3517Form, 'LB', false);
  const NB = _get(ca12AN3517Form, 'NB', false);
  const AB1 = _get(ca12AN3517Form, 'AB1', false);
  const AB2 = _get(ca12AN3517Form, 'AB2', false);
  const AB3 = _get(ca12AN3517Form, 'AB3', false);
  const AB4 = _get(ca12AN3517Form, 'AB4', false);
  const AB5 = _get(ca12AN3517Form, 'AB5', false);
  const AB6 = _get(ca12AN3517Form, 'AB6', false);
  const AB7 = _get(ca12AN3517Form, 'AB7', false);

  const sumOfAB = [AB1, AB2, AB3, AB4, AB5, AB6, AB7].reduce((a, b) => a + b, 0);

  setValue(ca12AN3517DDRForm, 'AA', LB, change);
  setValue(ca12AN3517DDRForm, 'AB', NB, change);
  useCompute('AC', sum, ['AA', 'AB'], ca12AN3517DDRForm, change);
  useCompute('AE', ({AC, AD}) => p(AC) - p(AD), ['AC', 'AD'], ca12AN3517DDRForm, change);
  setValue(ca12AN3517DDRForm, 'AE', sumOfAB, change);

  return (
    <div className="form-ca-annual-3517DDR">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 106, top: 236, width: 297, height: 13, 'font-size': 17, }}>
<span>
VI – DEMANDE DE REMBOURSEMENT
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 281, width: 876, height: 16, 'font-size': 17, }}>
<span>
Crédit remboursable dégagé à la clôture de l’année ou de l’exercice (≥ 150 €, ou, &#60; à 150 € uniquement en cas de cession,
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 300, width: 325, height: 16, 'font-size': 17, }}>
<span>
décès, entrée dans un groupe TVA) (ligne 29)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 284, width: 9, height: 10, 'font-size': 17, }}>
<span>
a
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 330, width: 299, height: 16, 'font-size': 17, }}>
<span>
Excédent de versement dégagé (ligne 34)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 330, width: 10, height: 12, 'font-size': 17, }}>
<span>
b
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 359, width: 224, height: 16, 'font-size': 17, }}>
<span>
Maximum remboursable (a + b)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 362, width: 10, height: 10, 'font-size': 17, }}>
<span>
c
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 389, width: 190, height: 13, 'font-size': 17, }}>
<span>
Remboursement demandé
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 389, width: 11, height: 13, 'font-size': 17, }}>
<span>
d
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 419, width: 170, height: 16, 'font-size': 17, }}>
<span>
Crédit reportable (c – d)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 961, top: 422, width: 9, height: 10, 'font-size': 17, }}>
<span>
e
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 449, width: 269, height: 16, 'font-size': 17, }}>
<span>
Le soussigné (nom, prénom, qualité) :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 478, width: 411, height: 16, 'font-size': 17, }}>
<span>
demande le remboursement de la somme de (en chiffres)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 475, width: 516, height: 18, 'font-size': 17, }}>
<span>
À …………………………………… , Le ……………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1158, top: 488, width: 7, height: 3, 'font-size': 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 508, width: 222, height: 16, 'font-size': 17, }}>
<span>
– à créditer au compte désigné
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 569, top: 508, width: 52, height: 13, 'font-size': 17, }}>
<span>
Cocher
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 567, top: 527, width: 55, height: 12, 'font-size': 17, }}>
<span>
selon le
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 576, top: 545, width: 37, height: 13, 'font-size': 17, }}>
<span>
choix
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 865, top: 508, width: 77, height: 16, 'font-size': 17, }}>
<span>
Signature :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 538, width: 452, height: 16, 'font-size': 17, }}>
<span>
– à imputer sur une échéance future (joindre l’imprimé n° 3516)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 594, width: 310, height: 16, 'font-size': 17, }}>
<span>
CADRE RÉSERVÉ À L’ADMINISTRATION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 636, width: 275, height: 16, 'font-size': 17, }}>
<span>
L’Inspecteur (1) Le contrôleur (1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 343, top: 636, width: 345, height: 16, 'font-size': 17, }}>
<span>
des finances publiques soussigné émet un avis :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 719, top: 627, width: 86, height: 13, 'font-size': 17, }}>
<span>
– Favorable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 719, top: 646, width: 101, height: 12, 'font-size': 17, }}>
<span>
– Défavorable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 880, top: 636, width: 256, height: 16, 'font-size': 17, }}>
<span>
(1) au remboursement de la somme
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 675, width: 1120, height: 13, 'font-size': 17, }}>
<span>
De ……………………………………………………………………………………………………………………………………………………………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1158, top: 685, width: 7, height: 3, 'font-size': 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 705, width: 1115, height: 16, 'font-size': 17, }}>
<span>
Observations (2) : …………………………………………………………………………………………………………………………………………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1152, top: 715, width: 12, height: 3, 'font-size': 17, }}>
<span>
...
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 36, top: 745, width: 1126, height: 2, 'font-size': 17, }}>
<span>
…………………………………………………………………………………………………………………………………………………………………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 770, width: 198, height: 16, 'font-size': 17, }}>
<span>
Code rejet / Adm partielle |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 238, top: 770, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 260, top: 770, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 799, width: 101, height: 17, 'font-size': 17, }}>
<span>
Type de rejet |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 141, top: 799, width: 22, height: 17, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 216, top: 799, width: 128, height: 17, 'font-size': 17, }}>
<span>
Type de contrôle |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 344, top: 799, width: 22, height: 17, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 829, width: 92, height: 16, 'font-size': 17, }}>
<span>
N° ALPAGE |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 155, top: 829, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 217, top: 829, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 133, top: 829, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 239, top: 829, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 177, top: 829, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 261, top: 829, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 283, top: 829, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 305, top: 829, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 327, top: 829, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 215, top: 829, width: 2, height: 16, 'font-size': 17, }}>
<span>
|
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 654, top: 761, width: 502, height: 18, 'font-size': 17, }}>
<span>
À …………………………………… , Le ……………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1159, top: 774, width: 7, height: 3, 'font-size': 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 780, top: 783, width: 246, height: 16, 'font-size': 17, }}>
<span>
Signature et cachet d’authenticité :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 896, width: 365, height: 16, 'font-size': 17, }}>
<span>
Le directeur soussigné autorise le remboursement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 926, width: 1119, height: 13, 'font-size': 17, }}>
<span>
de la somme de ……………………………………………………………………………………………………………………………………………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1155, top: 936, width: 7, height: 2, 'font-size': 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 955, width: 1122, height: 16, 'font-size': 17, }}>
<span>
au profit de ………………………………………………………………………………………………………………………………………………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 994, width: 210, height: 16, 'font-size': 17, }}>
<span>
La présentation d’une caution
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 268, top: 985, width: 126, height: 16, 'font-size': 17, }}>
<span>
– a été exigée (1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 268, top: 1004, width: 169, height: 16, 'font-size': 17, }}>
<span>
– n’a pas été exigée (1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 654, top: 982, width: 502, height: 18, 'font-size': 17, }}>
<span>
À …………………………………… , Le ……………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1159, top: 995, width: 7, height: 3, 'font-size': 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 780, top: 1004, width: 246, height: 16, 'font-size': 17, }}>
<span>
Signature et cachet d’authenticité :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 220, top: 1033, width: 223, height: 16, 'font-size': 17, }}>
<span>
Décisions prises par délégation
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1063, width: 179, height: 16, 'font-size': 17, }}>
<span>
Nature op. Numéro op.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 264, top: 1063, width: 33, height: 13, 'font-size': 17, }}>
<span>
Date
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 424, top: 1063, width: 118, height: 16, 'font-size': 17, }}>
<span>
Nom – signature
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1198, width: 467, height: 16, 'font-size': 17, }}>
<span>
Le comptable soussigné certifie que l’entreprise demanderesse :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 1246, width: 19, height: 16, 'font-size': 17, }}>
<span>
(1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1228, width: 726, height: 16, 'font-size': 17, }}>
<span>
– ne figure à aucun titre comme reliquataire dans les écritures du service des impôts des entreprises ;
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1246, width: 1013, height: 13, 'font-size': 17, }}>
<span>
– est redevable de la somme de ……………………………………………………………………………………………………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1265, width: 1013, height: 13, 'font-size': 17, }}>
<span>
– au titre de ……………………………………………………………………………………………………………………………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1294, width: 1127, height: 16, 'font-size': 17, }}>
<span>
Observations (3) …………………………………………………………………………………………………………………………………………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 46, top: 1333, width: 224, height: 16, 'font-size': 17, }}>
<span>
N° d’enregistrement MEDOC |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 433, top: 1333, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 455, top: 1333, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 367, top: 1333, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 336, top: 1333, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 411, top: 1333, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 314, top: 1333, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 389, top: 1333, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 292, top: 1333, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 365, top: 1333, width: 2, height: 16, 'font-size': 17, }}>
<span>
|
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 270, top: 1333, width: 22, height: 16, 'font-size': 17, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 654, top: 1321, width: 502, height: 18, 'font-size': 17, }}>
<span>
À …………………………………… , Le ……………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1159, top: 1334, width: 7, height: 3, 'font-size': 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 780, top: 1342, width: 246, height: 17, 'font-size': 17, }}>
<span>
Signature et cachet d’authenticité :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1371, width: 219, height: 13, 'font-size': 13, }}>
<span>
(1) Rayer la mention qui ne convient pas.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1385, width: 485, height: 13, 'font-size': 13, }}>
<span>
(2) Indiquer, notamment, les raisons pour lesquelles il paraît opportun d’exiger une caution.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1399, width: 470, height: 13, 'font-size': 13, }}>
<span>
Préciser, le cas échéant, les motifs de rejet total ou partiel du remboursement demandé.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1413, width: 485, height: 13, 'font-size': 13, }}>
<span>
(3) Indiquer, notamment, les raisons pour lesquelles il paraît opportun d’exiger une caution.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1427, width: 384, height: 12, 'font-size': 13, }}>
<span>
Préciser, le cas échéant, qu’un avis de compensation n° 3382 est établi.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 281, top: 1452, width: 639, height: 12, 'font-size': 13, }}>
<span>
Depuis le 1er octobre 2014, vous avez l’obligation de télédéclarer et télépayer la TVA par transfert de fichier ou Internet.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 388, top: 1466, width: 425, height: 12, 'font-size': 13, }}>
<span>
La somme due est prélevée automatiquement, au plus tôt à la date d’échéance.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 358, top: 1480, width: 485, height: 12, 'font-size': 13, }}>
<span>
Les demandes de remboursement de crédit de TVA doivent également être télétransmises.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 226, top: 1494, width: 749, height: 12, 'font-size': 13, }}>
<span>
Pour plus d’informations, vous pouvez vous connecter sur le site www.impots.gouv.fr, rubrique « Professionnels » ou contacter votre service.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 1517, width: 1042, height: 9, 'font-size': 9, }}>
<span>
Les dispositions des articles 39 et 40 de la loi n° 78-17 du 6 janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés, modifiée par la loi n° 2004-801 du 6 août 2004, garantissent les droits des personnes physiques à l’égard des traitements des données à caractère personnel.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 582, top: 1658, width: 37, height: 13, 'font-size': 17, }}>
<span>
– 6 –
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <button className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="Nom  signature"
                    data-field-id="27020056" data-annot-id="26101936" type="button">


            </button>

            <Field component={PdfNumberInput} disabled={true} className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="AA"
                   data-field-id="27020328" data-annot-id="26097216" type="text"/>

            <Field component={PdfNumberInput} disabled={true} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="AB"
                   data-field-id="27024264" data-annot-id="26089408" type="text"/>

            <Field component={PdfNumberInput} disabled={true} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="AC"
                   data-field-id="27023368" data-annot-id="26087648" type="text"/>

            <Field component={PdfNumberInput} disabled={true} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="AD"
                   data-field-id="27020664" data-annot-id="26048464" type="text"/>

            <Field component={PdfNumberInput} disabled={true} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="AE"
                   data-field-id="27022936" data-annot-id="25637920" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="ZV"
                   data-field-id="27024568" data-annot-id="26062752" type="text"/>

            <Field component={ReduxPicker} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field "
                   name="DATE_SIGNATURE" data-field-id="27025848" data-annot-id="26075376" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field "
                   name="LIEU_SIGNATURE" data-field-id="27026152" data-annot-id="25652144" type="text"/>

            <Field component="input" className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field "
                   name="ZW" data-field-id="27027512" data-annot-id="26112400" value="Yes" type="radio"
                   data-default-value="Off"/>

            <Field component="input" className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field "
                   name="ZZ" data-field-id="27027848" data-annot-id="26161440" value="Yes" type="radio"
                   data-default-value="Off"/>

          </div>
        </div>

      </div>
    </div>
  );
};

export default Annual;
