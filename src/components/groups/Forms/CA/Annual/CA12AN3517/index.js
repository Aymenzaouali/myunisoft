/* eslint-disable */
import React from "react";

import { Field } from "redux-form";
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox, PdfInputValuesWithSpaces } from 'components/reduxForm/Inputs';
import { sum, isEmpty, useCompute, parse as p, copy } from 'helpers/pdfforms';
import {get as _get} from 'lodash';

import './style.scss';

const ca12AN3517Form = (props) => {
  const { change, ca12AN3517Form, form, errors, setError, validateForm } = props;

  useCompute('FW', ({ EW }) => p(EW) * 20 / 100, ['EW'], ca12AN3517Form, change);
  useCompute('FF', ({ EF }) => p(EF) * 5.5 / 100, ['EF'], ca12AN3517Form, change);
  useCompute('FU', ({ EU }) => p(EU) * 8.5 / 100, ['EU'], ca12AN3517Form, change);
  useCompute('FV', ({ EV }) => p(EV) * 2.1 / 100, ['EV'], ca12AN3517Form, change);
  useCompute('GH', ({ GF }) => p(GF) * 10 / 100, ['GF'], ca12AN3517Form, change);

  useCompute('QA', ({ ZR }) => p(ZR) * 5 / 100, ['ZR'], ca12AN3517Form, change);
  useCompute('QD', ({ ZE }) => p(ZE) * 0.2 / 100, ['ZE'], ca12AN3517Form, change);
  useCompute('QE', ({ ZF }) => p(ZF) * 3.25 / 100, ['ZF'], ca12AN3517Form, change);
  useCompute('RF', ({ ZG }) => p(ZG) * 2 / 100, ['ZG'], ca12AN3517Form, change);
  useCompute('RG', ({ ZH }) => p(ZH) * 10 / 100, ['ZH'], ca12AN3517Form, change);
  useCompute('SQ', ({ SR }) => p(SR) * 2 / 100, ['SR'], ca12AN3517Form, change);
  useCompute('SS', ({ ST }) => p(ST) * 10 / 100, ['ST'], ca12AN3517Form, change);
  useCompute('QH', ({ ZJ }) => p(ZJ) * 14.89, ['ZJ'], ca12AN3517Form, change);
  useCompute('QJ', ({ ZK }) => p(ZK) * 4.57, ['ZK'], ca12AN3517Form, change);
  useCompute('VC', ({ ZL }) => p(ZL) * 0.75 / 100, ['ZL'], ca12AN3517Form, change);
  useCompute('ZB', ({ ZS }) => p(ZS) * 1 / 100, ['ZS'], ca12AN3517Form, change);
  useCompute('ZW', ({ ZT }) => p(ZT) * 0.4 / 100, ['ZT'], ca12AN3517Form, change);
  useCompute('ZX', ({ ZU }) => p(ZU) * 0.6 / 100, ['ZU'], ca12AN3517Form, change);
  useCompute('DC', ({ SM }) => p(SM) * 33 / 100, ['SM'], ca12AN3517Form, change);
  useCompute('VY', ({ SN }) => p(SN) * 0.5, ['SN'], ca12AN3517Form, change);
  useCompute('VZ', ({ SP }) => p(SP) * 125, ['SP'], ca12AN3517Form, change);
  useCompute('WF', ({ ZZ }) => p(ZZ) * 11 / 100, ['ZZ'], ca12AN3517Form, change);
  useCompute('WG', ({ YC }) => p(YC) * 6 / 100, ['YC'], ca12AN3517Form, change);
  useCompute('WH', ({ WZ }) => p(WZ) * 0.5 / 100, ['WZ'], ca12AN3517Form, change);
  useCompute('WJ', ({ ZA }) => p(ZA) * 0.5 / 100, ['ZA'], ca12AN3517Form, change);
  useCompute('WK', ({ ZM }) => p(ZM) * 5.3 / 100, ['ZM'], ca12AN3517Form, change);
  useCompute('WL', ({ YA }) => p(YA) * 1.8 / 100, ['YA'], ca12AN3517Form, change);
  useCompute('WM', ({ YB }) => p(YB) * 12 / 100, ['YB'], ca12AN3517Form, change);
  useCompute('YK', ({ YL }) => p(YL) * 5.6 / 100, ['YL'], ca12AN3517Form, change);
  useCompute('WQ', ({ ZN }) => p(ZN) * 5.7 / 100, ['ZN'], ca12AN3517Form, change);
  useCompute('WR', ({ ZN }) => p(ZN) * 1.8 / 100, ['ZN'], ca12AN3517Form, change);
  useCompute('WS', ({ ZN }) => p(ZN) * 1.8 / 100, ['ZN'], ca12AN3517Form, change);
  useCompute('WT', ({ ZP }) => p(ZP) * 1.8 / 100, ['ZP'], ca12AN3517Form, change);
  useCompute('WU', ({ ZP }) => p(ZP) * 0.2 / 100, ['ZP'], ca12AN3517Form, change);

  useCompute('RA', copy, ['NC'], ca12AN3517Form, change);
  useCompute('SA', copy, ['NA'], ca12AN3517Form, change);
  useCompute('SB', sum, ['QA', 'QS', 'QD', 'QE', 'QF', 'RF', 'RG', 'SQ', 'SS', 'QH', 'QJ', 'VC', 'ZB', 'VJ', 'VE', 'VF', 'KL', 'DA', 'KM', 'ZW', 'ZX', 'RN', 'DB', 'DC', 'WB', 'WC', 'VW', 'VX', 'VY', 'VZ', 'WD', 'WE', 'WH', 'WJ', 'WK', 'WL', 'WM', 'WN', 'WP', 'WQ', 'WR', 'WS', 'WT', 'WU', 'WV', 'YK', 'SH', 'SF'], ca12AN3517Form, change);
  useCompute('SC', sum, ['SB', 'SA'], ca12AN3517Form, change);

  useCompute('SH', sum, ['SE1', 'SE2', 'SE3', 'SE4', 'SE5', 'SE6', 'SE7', 'SE8', 'SE9'], ca12AN3517Form, change);
  useCompute('ZY', sum, ['ZW1', 'ZW2', 'ZW3', 'ZW4', 'ZW5', 'ZW6', 'ZW7', 'ZW8', 'ZW9', 'ZW10', 'ZW11', 'ZW12', 'ZW13', 'ZW14'], ca12AN3517Form, change);

  useCompute('FR', sum, ['FW', 'FF', 'GH', 'FD', 'FU', 'FV', 'FD', 'FJ', 'FG', 'FY', 'VP', 'EZ', 'FL', 'FM', 'FN', 'FP'], ca12AN3517Form, change);
  useCompute('GC', sum, ['FR', 'GA', 'GB', 'VS'], ca12AN3517Form, change);
  useCompute('KD', sum, ['VT', 'KB', 'KA', 'JA', 'HC'], ca12AN3517Form, change);

  useCompute('LA', ({ GC, KD },) => p(GC) - p(KD) > 0 && p(GC) - p(KD), ['GC', 'KD'], ca12AN3517Form, change);
  // useCompute('LA', ({GC, KD}) => p(GC) + p(KD) > 0 ? p(GC) + p(KD) : 0, ['GC', 'KD'], ca12AN3517Form, change);

  useCompute('MA', sum, ['MM', 'MN'], ca12AN3517Form, change);
  useCompute('MM', sum, ['MD', 'MF'], ca12AN3517Form, change);
  useCompute('MN', sum, ['ME', 'MG'], ca12AN3517Form, change);

  useCompute('NA', ({ LA, LB, MA }) => p(LA) - (p(LB) + p(MA)) > 0 ? p(LA) - (p(LB) + p(MA)) : 0, ['LA', 'LB', 'MA'], ca12AN3517Form, change);
  useCompute('NC', ({ NB, LB, MA }) => p(NB) > 0 ? p(LB) + p(NB) : p(NB) + p(MA), ['NB', 'LB', 'MA'], ca12AN3517Form, change);
  useCompute('NB', ({ MA, LA }) => p(MA) - p(LA) > 0 ? p(MA) - p(LA) : 0, ['MA', 'LA'], ca12AN3517Form, change);
  useCompute('AE', sum, ['AB1', 'AB2', 'AB3', 'AB4', 'AB5', 'AB6', 'AB7'], ca12AN3517Form, change);

  useCompute('ZA', sum, ['WG', 'WF', 'SF', 'AE', 'RD', 'SX', 'SV', 'SH', 'ZY', 'YK', 'WV', 'WU', 'WT', 'WS', 'WR', 'WQ', 'WP', 'WN', 'WM', 'WL', 'WK', 'WJ', 'WH', 'WE', 'WD', 'VZ', 'VY', 'VX', 'VW', 'WC', 'WB', 'DC', 'DB', 'RN', 'ZX', 'ZW', 'KM', 'DA', 'KL', 'VF', 'VE', 'VJ', 'ZB', 'VC', 'QJ', 'QH', 'SS', 'SQ', 'RG', 'RF', 'QF', 'QE', 'QD', 'QS', 'QA'], ca12AN3517Form, change);

  useCompute('SC', sum, ['SA', 'SB'], ca12AN3517Form, change);
  useCompute('HC', sum, ['HA', 'HB'], ca12AN3517Form, change);
  useCompute('LB', ({ GC, KD }) => p(KD) - p(GC) > 0 && p(KD) - p(GC), ['GC', 'KD'], ca12AN3517Form, change);
  useCompute('VA', ({ FR, EL, EM, EQ, HC }) => p(FR) - (p(EL) + p(EM) + p(EQ) + p(HC)), ['FR', 'EL', 'EM', 'EQ', 'HC'], ca12AN3517Form, change);


  return (
    <div className="form-ca-annual-3517">
      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 107, top: 37, width: 113, height: 13, fontSize: 17, }}>
<span>
I – TVA BRUTE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 123, top: 64, width: 256, height: 16, fontSize: 17, }}>
<span>
OPÉRATIONS NON IMPOSABLES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 480, top: 67, width: 114, height: 13, fontSize: 17, }}>
<span>
Base hors taxe
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 695, top: 64, width: 256, height: 16, fontSize: 17, }}>
<span>
OPÉRATIONS NON IMPOSABLES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1052, top: 67, width: 114, height: 13, fontSize: 17, }}>
<span>
Base hors taxe
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 97, width: 176, height: 13, fontSize: 17, }}>
<span>
01 Achats en franchise
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 430, top: 97, width: 161, height: 13, fontSize: 17, }}>
<span>
0037 …………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 607, top: 126, width: 363, height: 16, fontSize: 17, }}>
<span>
4B réalisées par un assujetti non établi en France
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 639, top: 108, width: 307, height: 16, fontSize: 17, }}>
<span>
Ventes de biens ou prestations de services
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 145, width: 302, height: 16, fontSize: 17, }}>
<span>
(article 283-1 du code général des impôts)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 126, width: 161, height: 13, fontSize: 17, }}>
<span>
0043 …………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 126, width: 187, height: 16, fontSize: 17, }}>
<span>
02 Exportations hors UE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 430, top: 126, width: 161, height: 13, fontSize: 17, }}>
<span>
0032 …………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 156, width: 278, height: 16, fontSize: 17, }}>
<span>
03 Autres opérations non imposables
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 430, top: 156, width: 161, height: 13, fontSize: 17, }}>
<span>
0033 …………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 204, width: 20, height: 13, fontSize: 17, }}>
<span>
3A
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 183, width: 329, height: 16, fontSize: 17, }}>
<span>
Ventes à distance taxables dans un autre État
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 204, width: 336, height: 16, fontSize: 17, }}>
<span>
membre au profit de personnes non assujetties
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 223, width: 103, height: 13, fontSize: 17, }}>
<span>
– Ventes BtoC
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 430, top: 204, width: 161, height: 13, fontSize: 17, }}>
<span>
0047 …………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 607, top: 229, width: 20, height: 12, fontSize: 17, }}>
<span>
4D
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 641, top: 219, width: 295, height: 16, fontSize: 17, }}>
<span>
Livraisons d’électricité, de gaz naturel, de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 640, top: 238, width: 328, height: 16, fontSize: 17, }}>
<span>
chaleur ou de froid non imposables en France
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1002, top: 228, width: 161, height: 13, fontSize: 17, }}>
<span>
0029 …………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 253, width: 358, height: 12, fontSize: 17, }}>
<span>
04 Livraisons intracommunautaires à destination
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 271, width: 290, height: 16, fontSize: 17, }}>
<span>
d’une personne assujettie – Ventes BtoB
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 430, top: 253, width: 161, height: 12, fontSize: 17, }}>
<span>
0034 …………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 298, width: 216, height: 16, fontSize: 17, }}>
<span>
OPÉRATIONS IMPOSABLES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 771, top: 301, width: 115, height: 13, fontSize: 17, }}>
<span>
Base hors taxe
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1024, top: 301, width: 68, height: 13, fontSize: 17, }}>
<span>
Taxe due
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 331, width: 279, height: 15, fontSize: 17, }}>
<span>
– réalisées en France métropolitaine
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 360, width: 164, height: 14, fontSize: 17, }}>
<span>
5A Taux normal 20 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 361, width: 500, height: 12, fontSize: 17, }}>
<span>
0207 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 390, width: 159, height: 15, fontSize: 17, }}>
<span>
06 Taux réduit 5,5 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 390, width: 500, height: 13, fontSize: 17, }}>
<span>
0105 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 420, width: 155, height: 13, fontSize: 17, }}>
<span>
6C Taux réduit 10 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 420, width: 500, height: 13, fontSize: 17, }}>
<span>
0151 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 450, width: 599, height: 12, fontSize: 17, }}>
<span>
6D ……………………………………………………………………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 721, top: 459, width: 441, height: 3, fontSize: 17, }}>
<span>
…………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 479, width: 194, height: 13, fontSize: 17, }}>
<span>
– réalisées dans les DOM
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 509, width: 168, height: 15, fontSize: 17, }}>
<span>
07 Taux normal 8,5 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 509, width: 502, height: 13, fontSize: 17, }}>
<span>
0201 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 538, width: 159, height: 16, fontSize: 17, }}>
<span>
08 Taux réduit 2,1 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 539, width: 502, height: 12, fontSize: 17, }}>
<span>
0100 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 568, width: 599, height: 13, fontSize: 17, }}>
<span>
8B ……………………………………………………………………………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 721, top: 578, width: 441, height: 3, fontSize: 17, }}>
<span>
…………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 598, width: 387, height: 16, fontSize: 17, }}>
<span>
– à un autre taux ( France métropolitaine ou DOM )
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 628, width: 345, height: 16, fontSize: 17, }}>
<span>
09 Opérations imposables à un taux particulier
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 628, width: 500, height: 13, fontSize: 17, }}>
<span>
0950 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 657, width: 126, height: 13, fontSize: 17, }}>
<span>
10 Anciens taux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 657, width: 500, height: 13, fontSize: 17, }}>
<span>
0900 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 687, width: 149, height: 16, fontSize: 17, }}>
<span>
– autres opérations
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 717, width: 578, height: 16, fontSize: 17, }}>
<span>
AA Livraisons d’électricité, de gaz naturel, de chaleur ou de froid imposables en
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 736, width: 50, height: 12, fontSize: 17, }}>
<span>
France
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 717, width: 500, height: 13, fontSize: 17, }}>
<span>
0030 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 765, width: 577, height: 16, fontSize: 17, }}>
<span>
AB Achats de biens ou de prestations de services réalisés auprès d’un assujetti
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 784, width: 453, height: 16, fontSize: 17, }}>
<span>
non établi en France (article 283-1 du code général des impôts)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 765, width: 500, height: 13, fontSize: 17, }}>
<span>
0040 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 37, top: 813, width: 594, height: 16, fontSize: 17, }}>
<span>
AC Achats de prestations de services intracommunautaires (article 283-2 du code
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 832, width: 141, height: 16, fontSize: 17, }}>
<span>
général des impôts)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 813, width: 500, height: 13, fontSize: 17, }}>
<span>
0044 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 42, top: 861, width: 226, height: 13, fontSize: 17, }}>
<span>
11 Cessions d’immobilisations
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 862, width: 500, height: 12, fontSize: 17, }}>
<span>
0970 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 891, width: 195, height: 13, fontSize: 17, }}>
<span>
12 Livraisons à soi-même
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 891, width: 500, height: 13, fontSize: 17, }}>
<span>
0980 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 921, width: 246, height: 16, fontSize: 17, }}>
<span>
13 Autres opérations imposables
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 921, width: 500, height: 13, fontSize: 17, }}>
<span>
0981 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 950, width: 275, height: 16, fontSize: 17, }}>
<span>
14 Acquisitions intracommunautaires
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 662, top: 950, width: 500, height: 13, fontSize: 17, }}>
<span>
0031 …………………………………. ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 980, width: 17, height: 13, fontSize: 17, }}>
<span>
15
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 120, top: 980, width: 209, height: 13, fontSize: 17, }}>
<span>
Dont TVA sur immobilisations
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 377, top: 980, width: 61, height: 16, fontSize: 17, }}>
<span>
0982 |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 438, top: 980, width: 174, height: 16, fontSize: 17, }}>
<span>
___________________ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1012, width: 17, height: 13, fontSize: 17, }}>
<span>
16
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 149, top: 1012, width: 300, height: 16, fontSize: 17, }}>
<span>
TOTAL DE LA TAXE DUE (ligne 5A à 14)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 950, top: 1022, width: 212, height: 2, fontSize: 17, }}>
<span>
………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1043, width: 116, height: 13, fontSize: 17, }}>
<span>
Autre TVA DUE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1073, width: 549, height: 16, fontSize: 17, }}>
<span>
17 Remboursements provisionnels obtenus en cours d’année ou d’exercice
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 891, top: 1073, width: 271, height: 13, fontSize: 17, }}>
<span>
0983 ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1103, width: 311, height: 13, fontSize: 17, }}>
<span>
18 TVA antérieurement déduite à reverser
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 891, top: 1103, width: 271, height: 13, fontSize: 17, }}>
<span>
0600 ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 37, top: 1132, width: 169, height: 17, fontSize: 17, }}>
<span>
AD Sommes à ajouter
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 891, top: 1133, width: 271, height: 12, fontSize: 17, }}>
<span>
0602 ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1164, width: 17, height: 13, fontSize: 17, }}>
<span>
19
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 149, top: 1164, width: 434, height: 16, fontSize: 17, }}>
<span>
TOTAL DE LA TVA BRUTE DUE (lignes 16 + 17 + 18 + AD)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 950, top: 1174, width: 212, height: 3, fontSize: 17, }}>
<span>
………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 107, top: 1193, width: 165, height: 16, fontSize: 17, }}>
<span>
II – TVA DÉDUCTIBLE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1225, width: 231, height: 13, fontSize: 17, }}>
<span>
AUTRES BIENS ET SERVICES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 998, top: 1225, width: 120, height: 13, fontSize: 17, }}>
<span>
Taxe déductible
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1255, width: 228, height: 16, fontSize: 17, }}>
<span>
20 Déductions sur factures (1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 891, top: 1255, width: 271, height: 13, fontSize: 17, }}>
<span>
0702 ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1285, width: 219, height: 16, fontSize: 17, }}>
<span>
21 Déductions forfaitaires (1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 891, top: 1285, width: 271, height: 13, fontSize: 17, }}>
<span>
0704 ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1317, width: 18, height: 12, fontSize: 17, }}>
<span>
22
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 149, top: 1317, width: 171, height: 16, fontSize: 17, }}>
<span>
TOTAL (lignes 20 + 21)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 950, top: 1327, width: 212, height: 2, fontSize: 17, }}>
<span>
………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1348, width: 144, height: 13, fontSize: 17, }}>
<span>
IMMOBILISATIONS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1378, width: 308, height: 16, fontSize: 17, }}>
<span>
23 TVA déductible sur immobilisations (1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 891, top: 1378, width: 271, height: 13, fontSize: 17, }}>
<span>
0703 ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 74, top: 1405, width: 184, height: 16, fontSize: 17, }}>
<span>
AUTRE TVA À DÉDUIRE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1438, width: 326, height: 15, fontSize: 17, }}>
<span>
Crédit antérieur non imputé et non remboursé
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 891, top: 1438, width: 271, height: 12, fontSize: 17, }}>
<span>
0058 ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1467, width: 302, height: 16, fontSize: 17, }}>
<span>
Omissions ou compléments de déductions
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 891, top: 1467, width: 271, height: 13, fontSize: 17, }}>
<span>
0059 ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1497, width: 471, height: 16, fontSize: 17, }}>
<span>
25A (1) Compte-tenu, le cas échéant, du coefficient de déduction
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 627, top: 1507, width: 71, height: 3, fontSize: 17, }}>
<span>
.. …………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 595, top: 1507, width: 30, height: 3, fontSize: 17, }}>
<span>
……
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 705, top: 1497, width: 13, height: 13, fontSize: 17, }}>
<span>
%
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 75, top: 1527, width: 136, height: 16, fontSize: 17, }}>
<span>
Sommes à imputer
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 891, top: 1527, width: 271, height: 13, fontSize: 17, }}>
<span>
0603 ………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 149, top: 1555, width: 479, height: 19, fontSize: 17, }}>
<span>
TOTAL DE LA TVA DÉDUCTIBLE (lignes 22 + 23 + 24 + 25 + AE)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 950, top: 1568, width: 212, height: 3, fontSize: 17, }}>
<span>
………………………………….
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1438, width: 18, height: 12, fontSize: 17, }}>
<span>
24
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1467, width: 18, height: 13, fontSize: 17, }}>
<span>
25
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1527, width: 21, height: 12, fontSize: 17, }}>
<span>
AE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1558, width: 18, height: 13, fontSize: 17, }}>
<span>
26
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 1636, width: 1086, height: 8, fontSize: 9, }}>
<span>
Si vous réalisez des opérations intracommunautaires, pensez à la déclaration d’échanges de biens (livraisons de biens) ou à la déclaration européenne de services (prestations de services) à souscrire auprès de la Direction Générale des Douanes et des Droits indirects (cf. notice de la déclaration CA12).
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 582, top: 1658, width: 37, height: 13, fontSize: 17, }}>
<span>
– 2 –
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="I  TVA BRUTE" data-field-id="25985944" data-annot-id="25804336" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field "
                   name="EA"
                   data-field-id="25989160" data-annot-id="25510320" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field "
                   name="EE"
                   data-field-id="25992312" data-annot-id="25510544" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field "
                   name="EB"
                   data-field-id="25995464" data-annot-id="25510736" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field "
                   name="ED"
                   data-field-id="25998616" data-annot-id="25510928" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field "
                   name="EC"
                   data-field-id="26001800" data-annot-id="25511120" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field "
                   name="VM"
                   data-field-id="26004952" data-annot-id="25511312" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field "
                   name="EX"
                   data-field-id="26008104" data-annot-id="25511504" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field "
                   name="EW"
                   data-field-id="26011256" data-annot-id="25511696" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="FW"
                   data-field-id="26014408" data-annot-id="25512032" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field "
                   name="EF"
                   data-field-id="26017560" data-annot-id="25512224" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="FF"
                   data-field-id="26020712" data-annot-id="25512416" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field "
                   name="GF"
                   data-field-id="26023864" data-annot-id="25512608" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="GH"
                   data-field-id="26027128" data-annot-id="25512800" type="text"/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FA', false)}
                   className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="FA"
                   data-field-id="31770744" data-annot-id="31250928" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FB', false)}
                   className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="FB"
                   data-field-id="31773896" data-annot-id="31251120" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field "
                   name="EU"
                   data-field-id="26036584" data-annot-id="25513376" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="FU"
                   data-field-id="26127352" data-annot-id="25513840" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field "
                   name="EV"
                   data-field-id="26130472" data-annot-id="25514032" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="FV"
                   data-field-id="26133672" data-annot-id="25514224" type="text"/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FC', false)}
                   className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="FC"
                   data-field-id="31877544" data-annot-id="31252352" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FD', false)}
                   className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="FD"
                   data-field-id="31880696" data-annot-id="31252544" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'EJ', false)}
                   className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="EJ"
                   data-field-id="31883848" data-annot-id="31252736" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FJ', false)}
                   className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="FJ"
                   data-field-id="31887000" data-annot-id="31252928" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'EG', false)}
                   className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="EG"
                   data-field-id="31890152" data-annot-id="31253120" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FG', false)}
                   className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="FG"
                   data-field-id="31893304" data-annot-id="31253312" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'EY', false)}
                   className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="EY"
                   data-field-id="31896456" data-annot-id="31253504" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FY', false)}
                   className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="FY"
                   data-field-id="31900136" data-annot-id="31253696" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'VN', false)}
                   className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="VN"
                   data-field-id="31902984" data-annot-id="31256896" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'VP', false)}
                   className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="VP"
                   data-field-id="31906136" data-annot-id="31257088" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'EH', false)}
                   className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="EH"
                   data-field-id="31909288" data-annot-id="31257280" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'EZ', false)}
                   className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="EZ"
                   data-field-id="31912440" data-annot-id="31257472" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'EL', false)}
                   className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="EL"
                   data-field-id="31915592" data-annot-id="31257664" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FL', false)}
                   className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="FL"
                   data-field-id="31918744" data-annot-id="31251504" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'EM', false)}
                   className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="EM"
                   data-field-id="31921896" data-annot-id="31258384" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FM', false)}
                   className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="FM"
                   data-field-id="31925048" data-annot-id="31258576" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'EN', false)}
                   className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="EN"
                   data-field-id="31928200" data-annot-id="31258768" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FN', false)}
                   className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="FN"
                   data-field-id="26190824" data-annot-id="25520944" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'EP', false)}
                   className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="EP"
                   data-field-id="31934504" data-annot-id="31259152" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'FP', false)}
                   className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="FP"
                   data-field-id="31937656" data-annot-id="31259344" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field "
                   name="EQ"
                   data-field-id="26200264" data-annot-id="25521520" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="FR"
                   data-field-id="31943960" data-annot-id="31259728" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field "
                   name="GA"
                   data-field-id="26206584" data-annot-id="25521904" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field "
                   name="GB"
                   data-field-id="26209784" data-annot-id="25522096" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field "
                   name="VS"
                   data-field-id="26212904" data-annot-id="25522288" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="GC"
                   data-field-id="31956568" data-annot-id="31260496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field "
                   name="HA"
                   data-field-id="26219224" data-annot-id="25522672" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="HC"
                   data-field-id="31962872" data-annot-id="31260880" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field "
                   name="HB"
                   data-field-id="26225544" data-annot-id="25523056" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field "
                   name="JA"
                   data-field-id="26228744" data-annot-id="25523248" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field "
                   name="KB"
                   data-field-id="26231864" data-annot-id="25523440" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field "
                   name="KA"
                   data-field-id="26235064" data-annot-id="25523632" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field "
                   name="JB"
                   data-field-id="26238184" data-annot-id="25523824" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field "
                   name="VT"
                   data-field-id="26241384" data-annot-id="25524016" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field " name="KD"
                   data-field-id="31984936" data-annot-id="31262224" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-2" data-page-num="2" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-2 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 102, top: 37, width: 118, height: 13, fontSize: 17, }}>
<span>
III – TVA NETTE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 64, width: 241, height: 17, fontSize: 17, }}>
<span>
RÉSULTAT DE LA LIQUIDATION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1054, top: 67, width: 36, height: 13, fontSize: 17, }}>
<span>
Taxe
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 97, width: 292, height: 16, fontSize: 17, }}>
<span>
28 TVA due : ( Ligne 19 – ligne 26 ) ou
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 107, width: 175, height: 2, fontSize: 17, }}>
<span>
……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 124, width: 270, height: 18, fontSize: 17, }}>
<span>
29 CRÉDIT : ( Ligne 26 – ligne 19 )
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 922, top: 126, width: 231, height: 13, fontSize: 17, }}>
<span>
0705 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 153, width: 267, height: 16, fontSize: 17, }}>
<span>
IMPUTATIONS/RÉGULARISATIONS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 218, width: 66, height: 14, fontSize: 15, }}>
<span>
Acompte 1
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 63, top: 251, width: 68, height: 13, fontSize: 15, }}>
<span>
Acompte 2
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 207, top: 185, width: 100, height: 12, fontSize: 15, }}>
<span>
Col. 1 : Montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 193, top: 202, width: 127, height: 14, fontSize: 15, }}>
<span>
effectivement payé
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 187, top: 227, width: 139, height: 2, fontSize: 15, }}>
<span>
....................................
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 187, top: 259, width: 139, height: 3, fontSize: 15, }}>
<span>
....................................
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 188, top: 283, width: 137, height: 11, fontSize: 15, }}>
<span>
Tot. 1 .........................
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 379, top: 185, width: 99, height: 12, fontSize: 15, }}>
<span>
Col. 2 : Montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 378, top: 202, width: 101, height: 14, fontSize: 15, }}>
<span>
restant à payer
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 358, top: 227, width: 140, height: 2, fontSize: 15, }}>
<span>
....................................
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 358, top: 259, width: 140, height: 3, fontSize: 15, }}>
<span>
....................................
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 359, top: 283, width: 137, height: 11, fontSize: 15, }}>
<span>
Tot. 2 .........................
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 520, top: 186, width: 281, height: 16, fontSize: 17, }}>
<span>
30 Acomptes payés et / ou restant dus
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 549, top: 204, width: 107, height: 16, fontSize: 17, }}>
<span>
(Tot. 1 + Tot. 2)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 922, top: 204, width: 231, height: 13, fontSize: 17, }}>
<span>
0018 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 308, width: 119, height: 16, fontSize: 17, }}>
<span>
RÉSULTAT NET
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 337, width: 424, height: 19, fontSize: 17, }}>
<span>
33 SOLDE À PAYER si ligne 28 – (lignes 29 + 30) ≥ 0 ou
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 350, width: 175, height: 3, fontSize: 17, }}>
<span>
……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 369, width: 447, height: 19, fontSize: 17, }}>
<span>
34 EXCÉDENT DE VERSEMENT : si ligne 30 – ligne 28 ≥ 0
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 978, top: 382, width: 175, height: 3, fontSize: 17, }}>
<span>
……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 401, width: 489, height: 19, fontSize: 17, }}>
<span>
35 SOLDE EXCÉDENTAIRE : (lignes 29 + 34) ou (lignes 29 + 30)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 922, top: 404, width: 231, height: 13, fontSize: 17, }}>
<span>
0020 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 431, width: 319, height: 15, fontSize: 17, }}>
<span>
IV – DÉCOMPTE DES TAXES ASSIMILÉES
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 431, top: 463, width: 121, height: 13, fontSize: 17, }}>
<span>
Nature des taxes
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1034, top: 463, width: 75, height: 13, fontSize: 17, }}>
<span>
Taxe brute
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 493, width: 582, height: 16, fontSize: 17, }}>
<span>
36 Taxe sur les retransmissions sportives au taux de 5 % (CGI, art. 302 bis ZE)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 493, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 493, width: 238, height: 13, fontSize: 17, }}>
<span>
4215 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 523, width: 577, height: 16, fontSize: 17, }}>
<span>
37 Taxe sur le chiffre d’affaires des exploitants agricoles (CGI, art. 302 bis MB)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 523, width: 238, height: 12, fontSize: 17, }}>
<span>
4220 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 562, width: 17, height: 13, fontSize: 17, }}>
<span>
39
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 552, width: 558, height: 17, fontSize: 17, }}>
<span>
Taxe sur l’édition des ouvrages de librairie (CGI, art 1609 undecies et suiv.) au
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 571, width: 99, height: 15, fontSize: 17, }}>
<span>
taux de 0,2 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 562, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 922, top: 562, width: 237, height: 13, fontSize: 17, }}>
<span>
3510 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 610, width: 18, height: 13, fontSize: 17, }}>
<span>
40
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 601, width: 390, height: 16, fontSize: 17, }}>
<span>
Taxe sur les appareils de reproduction ou d’impression
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 619, width: 365, height: 16, fontSize: 17, }}>
<span>
(CGI, art 1609 undecies et suiv.) au taux de 3,25 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 610, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 922, top: 610, width: 237, height: 13, fontSize: 17, }}>
<span>
3520 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 649, width: 671, height: 16, fontSize: 17, }}>
<span>
41 Taxe sur les huiles alimentaires FIPSA (CGI, art 1609 vicies) [ne concerne pas les DOM]
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 922, top: 649, width: 237, height: 13, fontSize: 17, }}>
<span>
3240 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 679, width: 649, height: 16, fontSize: 17, }}>
<span>
Taxe sur la diffusion en vidéo physique et en ligne de contenus audiovisuels à titre onéreux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 697, width: 203, height: 16, fontSize: 17, }}>
<span>
(CGI, art. 1609 sexdecies B)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 727, width: 18, height: 13, fontSize: 17, }}>
<span>
42
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 727, width: 121, height: 13, fontSize: 17, }}>
<span>
– au taux de 2 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 742, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 727, width: 238, height: 13, fontSize: 17, }}>
<span>
4229 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 757, width: 18, height: 13, fontSize: 17, }}>
<span>
43
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 757, width: 130, height: 13, fontSize: 17, }}>
<span>
– au taux de 10 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 757, width: 238, height: 13, fontSize: 17, }}>
<span>
4228 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 786, width: 636, height: 16, fontSize: 17, }}>
<span>
Taxe sur la diffusion en vidéo physique et en ligne de contenus audiovisuels à titre gratuit
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 805, width: 203, height: 16, fontSize: 17, }}>
<span>
(CGI, art. 1609 sexdecies B)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 835, width: 29, height: 13, fontSize: 17, }}>
<span>
43A
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 835, width: 121, height: 13, fontSize: 17, }}>
<span>
– au taux de 2 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 850, width: 113, height: 15, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 835, width: 238, height: 13, fontSize: 17, }}>
<span>
4298 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 865, width: 28, height: 12, fontSize: 17, }}>
<span>
43B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 864, width: 130, height: 14, fontSize: 17, }}>
<span>
– au taux de 10 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 865, width: 238, height: 12, fontSize: 17, }}>
<span>
4299 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 904, width: 18, height: 12, fontSize: 17, }}>
<span>
44
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 894, width: 448, height: 16, fontSize: 17, }}>
<span>
Taxe sur les actes des huissiers de justice (CGI, art. 302 bis Y)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 913, width: 127, height: 16, fontSize: 17, }}>
<span>
(14,89 € par acte)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 679, top: 904, width: 112, height: 12, fontSize: 17, }}>
<span>
Nombre d’actes
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 904, width: 238, height: 12, fontSize: 17, }}>
<span>
4206 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 952, width: 18, height: 13, fontSize: 17, }}>
<span>
45
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 942, width: 560, height: 17, fontSize: 17, }}>
<span>
Taxe sur les embarquements ou débarquements de passagers en Corse (CGI,
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 961, width: 268, height: 16, fontSize: 17, }}>
<span>
art 1599 vicies) (4,57 € par passager)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 695, top: 943, width: 79, height: 12, fontSize: 17, }}>
<span>
Nombre de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 698, top: 964, width: 73, height: 13, fontSize: 17, }}>
<span>
passagers
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 952, width: 238, height: 13, fontSize: 17, }}>
<span>
4204 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1010, width: 18, height: 12, fontSize: 17, }}>
<span>
46
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 991, width: 584, height: 16, fontSize: 17, }}>
<span>
Taxe pour le développement de la formation professionnelle dans les métiers de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1009, width: 557, height: 16, fontSize: 17, }}>
<span>
la réparation de l’automobile, du cycle et du motocycle
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1028, width: 311, height: 16, fontSize: 17, }}>
<span>
(CGI, art 1609 sexvicies) au taux de 0,75 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 1010, width: 113, height: 15, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1010, width: 238, height: 12, fontSize: 17, }}>
<span>
4217 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1067, width: 18, height: 13, fontSize: 17, }}>
<span>
47
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 1058, width: 404, height: 16, fontSize: 17, }}>
<span>
Taxe sur certaines dépenses de publicité au taux de 1 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1076, width: 152, height: 16, fontSize: 17, }}>
<span>
(CGI, art 302 bis MA)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 1067, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1067, width: 238, height: 13, fontSize: 17, }}>
<span>
4213 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1106, width: 600, height: 16, fontSize: 17, }}>
<span>
4A Contribution perçue au profit de l’ANSP (ex-INPES) (CGI, art. 1609 octovicies)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1106, width: 238, height: 13, fontSize: 17, }}>
<span>
4222 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1145, width: 20, height: 13, fontSize: 17, }}>
<span>
4B
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1136, width: 616, height: 16, fontSize: 17, }}>
<span>
Contribution à l’audiovisuel public (ex-redevance audiovisuelle) (CGI, art 1605 et suiv.)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1154, width: 348, height: 16, fontSize: 17, }}>
<span>
[cf. fiche de calcul sur le site www. impots.gouv.fr]
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1145, width: 238, height: 13, fontSize: 17, }}>
<span>
4219 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1193, width: 21, height: 13, fontSize: 17, }}>
<span>
4C
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1184, width: 673, height: 16, fontSize: 17, }}>
<span>
Contribution à l’audiovisuel public (ex-redevance audiovisuelle) due par les loueurs d’appareils
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1202, width: 166, height: 16, fontSize: 17, }}>
<span>
(CGI, art. 1605 et suiv.)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1193, width: 238, height: 13, fontSize: 17, }}>
<span>
4221 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1232, width: 753, height: 16, fontSize: 17, }}>
<span>
4D Taxe sur la publicité diffusée par voie de radiodiffusion sonore et de télévision (CGI, art. 302 bis KD)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1232, width: 238, height: 13, fontSize: 17, }}>
<span>
4214 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1262, width: 410, height: 16, fontSize: 17, }}>
<span>
4E Taxe sur la publicité télévisée (CGI, art. 302 bis KA)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1262, width: 238, height: 13, fontSize: 17, }}>
<span>
4201 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1291, width: 755, height: 16, fontSize: 17, }}>
<span>
4F Taxe sur les excédents de provision des entreprises d’assurance de dommages (CGI, art. 235 ter X)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1291, width: 238, height: 13, fontSize: 17, }}>
<span>
4238 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1321, width: 534, height: 16, fontSize: 17, }}>
<span>
Contribution sur les activités privées de sécurité (CGI, art. 1609 quintricies)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1351, width: 20, height: 12, fontSize: 17, }}>
<span>
4H
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 1351, width: 134, height: 15, fontSize: 17, }}>
<span>
– au taux de 0,4 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 1366, width: 113, height: 15, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1351, width: 238, height: 13, fontSize: 17, }}>
<span>
4288 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 41, top: 1381, width: 13, height: 12, fontSize: 17, }}>
<span>
4I
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 1380, width: 134, height: 15, fontSize: 17, }}>
<span>
– au taux de 0,6 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1381, width: 238, height: 12, fontSize: 17, }}>
<span>
4289 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 38, top: 1410, width: 747, height: 16, fontSize: 17, }}>
<span>
4K Contribution due par les gestionnaires des réseaux publics d’électricité (CGCT, art. L 2224-31 I bis)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1410, width: 238, height: 13, fontSize: 17 }}>
<span>
4236 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 39, top: 1440, width: 748, height: 16, fontSize: 17, }}>
<span>
4L Taxe sur les ordres annulés dans le cadre d’opérations à haute fréquence (CGI, art. 235 ter ZD bis)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1440, width: 238, height: 13, fontSize: 17, }}>
<span>
4239 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 37, top: 1488, width: 22, height: 13, fontSize: 17, }}>
<span>
4M
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1470, width: 572, height: 16, fontSize: 17, }}>
<span>
Prélèvement sur les films pornographiques ou d’incitation à la violence et sur les
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1488, width: 580, height: 16, fontSize: 17, }}>
<span>
représentations théâtrales à caractère pornographique au taux de 33 % (CGI, art.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 81, top: 1507, width: 90, height: 16, fontSize: 17, }}>
<span>
1605 sexies)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 1488, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1488, width: 238, height: 13, fontSize: 17, }}>
<span>
4245 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1536, width: 507, height: 16, fontSize: 17, }}>
<span>
60A Redevance sanitaire d’abattage (CGI, art. 302 bis N à 302 bis R)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1536, width: 238, height: 13, fontSize: 17, }}>
<span>
4253 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1566, width: 537, height: 16, fontSize: 17, }}>
<span>
60B Redevance sanitaire de découpage (CGI, art. 302 bis S à 302 bis W)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1566, width: 238, height: 13, fontSize: 17, }}>
<span>
4254 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1596, width: 781, height: 16, fontSize: 17, }}>
<span>
61 Redevance sanitaire pour le contrôle de certaines substances et de leurs résidus (CGI, art. 302 bis WC)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1596, width: 238, height: 12, fontSize: 17, }}>
<span>
4247 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1625, width: 739, height: 16, fontSize: 17, }}>
<span>
62 Redevance sanitaire de première mise sur le marché des produits de la pêche ou de l’aquaculture
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1625, width: 238, height: 13, fontSize: 17, }}>
<span>
4248 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 582, top: 1658, width: 37, height: 13, fontSize: 17, }}>
<span>
– 3 –
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <a className="pdf-annot pdf-link-annot obj_75 pdf-obj-fixed" href="http://www.impots.gouv.fr/"
               data-annot-id="25427760">
            </a>
            <a className="pdf-annot pdf-link-annot obj_76 pdf-obj-fixed" href="http://www.impots.gouv.fr/"
               data-annot-id="25451760">
            </a>
            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field " name="LA"
                   data-field-id="26247704" data-annot-id="25451984" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field " name="LB"
                   data-field-id="26250856" data-annot-id="25307024" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field " name="MA"
                   data-field-id="26254008" data-annot-id="25307216" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field "
                   name="MD"
                   data-field-id="26257160" data-annot-id="25450944" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field "
                   name="ME"
                   data-field-id="26260312" data-annot-id="25451136" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field "
                   name="MF"
                   data-field-id="26162072" data-annot-id="25451328" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field "
                   name="MG"
                   data-field-id="26267144" data-annot-id="25920624" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field "
                   name="MM"
                   data-field-id="26270296" data-annot-id="25920816" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field "
                   name="MN"
                   data-field-id="26273448" data-annot-id="25921008" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field " name="NA"
                   data-field-id="26276600" data-annot-id="25921200" type="text"/>

            <Field component={PdfNumberInput} disabled={isEmpty('LB', ca12AN3517Form)}
                   className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field " name="NC"
                   data-field-id="26279752" data-annot-id="25921392" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field " name="NB"
                   data-field-id="26282904" data-annot-id="25921584" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field "
                   name="ZR"
                   data-field-id="26286056" data-annot-id="25921776" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field " name="QA"
                   data-field-id="26289208" data-annot-id="25921968" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field "
                   name="QS"
                   data-field-id="26292360" data-annot-id="25931472" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field "
                   name="ZE"
                   data-field-id="26295512" data-annot-id="25931936" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field " name="QD"
                   data-field-id="26298664" data-annot-id="25932128" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field "
                   name="ZF"
                   data-field-id="26301816" data-annot-id="25932320" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field " name="QE"
                   data-field-id="26304968" data-annot-id="25932512" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field "
                   name="QF"
                   data-field-id="26308120" data-annot-id="25932704" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field "
                   name="ZG"
                   data-field-id="26311272" data-annot-id="25932896" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field " name="RF"
                   data-field-id="26314392" data-annot-id="25933088" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field "
                   name="ZH"
                   data-field-id="26317592" data-annot-id="25933280" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field " name="RG"
                   data-field-id="26320712" data-annot-id="25933472" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field "
                   name="SR"
                   data-field-id="26323912" data-annot-id="25933664" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field " name="SQ"
                   data-field-id="26327032" data-annot-id="25933856" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field "
                   name="ST"
                   data-field-id="26330232" data-annot-id="25934048" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field " name="SS"
                   data-field-id="26333352" data-annot-id="25934240" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field "
                   name="ZJ"
                   data-field-id="26336552" data-annot-id="25934432" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field " name="QH"
                   data-field-id="26339672" data-annot-id="25934624" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field "
                   name="ZK"
                   data-field-id="26342872" data-annot-id="25934816" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field " name="QJ"
                   data-field-id="26345992" data-annot-id="25931664" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field "
                   name="ZL"
                   data-field-id="26349192" data-annot-id="25935536" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field " name="VC"
                   data-field-id="26352312" data-annot-id="25935728" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field "
                   name="ZS"
                   data-field-id="26355512" data-annot-id="25935920" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field " name="ZB"
                   data-field-id="26358632" data-annot-id="25936112" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field "
                   name="VJ"
                   data-field-id="26361832" data-annot-id="25936304" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field "
                   name="VE"
                   data-field-id="26364952" data-annot-id="25936496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field "
                   name="VF"
                   data-field-id="26368152" data-annot-id="25936688" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field "
                   name="KL"
                   data-field-id="26371272" data-annot-id="25936880" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field "
                   name="DA"
                   data-field-id="26374472" data-annot-id="25937072" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field "
                   name="KM"
                   data-field-id="26377592" data-annot-id="25937264" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field " name="ZW"
                   data-field-id="26380792" data-annot-id="25937456" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field "
                   name="ZT"
                   data-field-id="26383912" data-annot-id="25937648" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field " name="ZX"
                   data-field-id="26387144" data-annot-id="25937840" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field "
                   name="ZU"
                   data-field-id="26390312" data-annot-id="25938032" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field "
                   name="RN"
                   data-field-id="26393480" data-annot-id="25938224" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field "
                   name="DB"
                   data-field-id="26396648" data-annot-id="25938416" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_125 pdf-obj-fixed acroform-field " name="DC"
                   data-field-id="26399816" data-annot-id="25938608" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_126 pdf-obj-fixed acroform-field "
                   name="SM"
                   data-field-id="26402984" data-annot-id="25938800" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_127 pdf-obj-fixed acroform-field "
                   name="WB"
                   data-field-id="26406152" data-annot-id="25938992" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_128 pdf-obj-fixed acroform-field "
                   name="WC"
                   data-field-id="26409320" data-annot-id="25939184" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field "
                   name="VW"
                   data-field-id="26412488" data-annot-id="25939376" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_130 pdf-obj-fixed acroform-field "
                   name="VX"
                   data-field-id="26415656" data-annot-id="25939568" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-3" data-page-num="3" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-3 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 80, top: 37, width: 158, height: 16, fontSize: 17, }}>
<span>
(CGI, art. 302 bis WA)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 76, width: 17, height: 13, fontSize: 17, }}>
<span>
63
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 67, width: 502, height: 16, fontSize: 17, }}>
<span>
Redevance sanitaire de transformation des produits de la pêche ou de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 85, width: 378, height: 16, fontSize: 17, }}>
<span>
l’aquaculture (0.5 € par tonne) (CGI, art. 302 bis WB)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 695, top: 67, width: 79, height: 13, fontSize: 17, }}>
<span>
Nombre de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 710, top: 86, width: 48, height: 12, fontSize: 17, }}>
<span>
tonnes
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 76, width: 238, height: 13, fontSize: 17, }}>
<span>
4249 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 124, width: 17, height: 13, fontSize: 17, }}>
<span>
64
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 115, width: 533, height: 16, fontSize: 17, }}>
<span>
Redevance pour agrément des établissements du secteur de l’alimentation
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 134, width: 453, height: 16, fontSize: 17, }}>
<span>
animale (125 € par établissement) (CGI, art. 302 bis WD à WG)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 706, top: 115, width: 57, height: 13, fontSize: 17, }}>
<span>
Nombre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 674, top: 134, width: 120, height: 12, fontSize: 17, }}>
<span>
d&#39;établissements
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 124, width: 238, height: 13, fontSize: 17, }}>
<span>
4250 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 163, width: 754, height: 17, fontSize: 17, }}>
<span>
Redevance phytosanitaire à la circulation intracommunautaire et à l’exportation (Code rural et de la pêche
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 182, width: 181, height: 16, fontSize: 17, }}>
<span>
maritime, art. L 251-17-1)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 212, width: 17, height: 13, fontSize: 17, }}>
<span>
65
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 212, width: 311, height: 16, fontSize: 17, }}>
<span>
– à la circulation intracommunautaire (PPE)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 212, width: 238, height: 13, fontSize: 17, }}>
<span>
4273 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 241, width: 17, height: 13, fontSize: 17, }}>
<span>
66
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 241, width: 113, height: 16, fontSize: 17, }}>
<span>
– à l’exportation
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 241, width: 238, height: 13, fontSize: 17, }}>
<span>
4274 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 271, width: 724, height: 16, fontSize: 17, }}>
<span>
Taxe forfaitaire sur les ventes de métaux précieux, de bijoux, d’objets d’art, de collection et d’antiquité
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 289, width: 131, height: 17, fontSize: 17, }}>
<span>
(CGI, art. 150 VM)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 319, width: 17, height: 13, fontSize: 17, }}>
<span>
67
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 319, width: 378, height: 16, fontSize: 17, }}>
<span>
– sur les ventes de métaux précieux au taux de 11 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 319, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 319, width: 238, height: 13, fontSize: 17, }}>
<span>
4268 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 358, width: 17, height: 13, fontSize: 17, }}>
<span>
68
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 349, width: 491, height: 16, fontSize: 17, }}>
<span>
– sur les ventes de bijoux, objets d’art, de collection ou d’antiquité au
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 367, width: 85, height: 14, fontSize: 17, }}>
<span>
taux de 6 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 358, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 358, width: 238, height: 13, fontSize: 17, }}>
<span>
4270 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 397, width: 593, height: 16, fontSize: 17, }}>
<span>
Contribution pour le remboursement de la dette sociale (CRDS) (CGI, art. 1600-0 I)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 427, width: 17, height: 13, fontSize: 17, }}>
<span>
69
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 427, width: 384, height: 16, fontSize: 17, }}>
<span>
– sur les ventes de métaux précieux au taux de 0,5 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 451, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 427, width: 238, height: 13, fontSize: 17, }}>
<span>
4269 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 466, width: 17, height: 13, fontSize: 17, }}>
<span>
70
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 457, width: 491, height: 16, fontSize: 17, }}>
<span>
– sur les ventes de bijoux, objets d’art, de collection ou d’antiquité au
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 475, width: 98, height: 15, fontSize: 17, }}>
<span>
taux de 0,5 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 466, width: 238, height: 13, fontSize: 17, }}>
<span>
4271 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 505, width: 255, height: 16, fontSize: 17, }}>
<span>
Prélèvement sur les paris hippiques
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 535, width: 15, height: 12, fontSize: 17, }}>
<span>
71
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 532, width: 420, height: 19, fontSize: 17, }}>
<span>
– au profit de l’État (CGI, art. 302 bis ZG) au taux de 5,3 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 583, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 535, width: 238, height: 13, fontSize: 17, }}>
<span>
4256 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 574, width: 17, height: 13, fontSize: 17, }}>
<span>
72
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 565, width: 494, height: 16, fontSize: 17, }}>
<span>
– au profit des organismes de sécurité sociale (CSS, art. L137-20) au
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 583, width: 98, height: 15, fontSize: 17, }}>
<span>
taux de 1,8 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 574, width: 238, height: 13, fontSize: 17, }}>
<span>
4259 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 622, width: 17, height: 13, fontSize: 17, }}>
<span>
73
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 613, width: 490, height: 16, fontSize: 17, }}>
<span>
– engagés depuis l’étranger sur des courses françaises et regroupés
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 631, width: 353, height: 16, fontSize: 17, }}>
<span>
en France (CGI, art. 302 bis ZO) au taux de 12 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 622, width: 238, height: 13, fontSize: 17, }}>
<span>
4255 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 661, width: 497, height: 16, fontSize: 17, }}>
<span>
Redevance due par les opérateurs agréés de paris hippiques en ligne
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 691, width: 17, height: 12, fontSize: 17, }}>
<span>
74
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 691, width: 438, height: 16, fontSize: 17, }}>
<span>
– Enjeux relatifs aux courses de trot (CGI, art. 1609 tertricies)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 691, width: 238, height: 13, fontSize: 17, }}>
<span>
4266 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 721, width: 17, height: 12, fontSize: 17, }}>
<span>
75
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 720, width: 454, height: 17, fontSize: 17, }}>
<span>
– Enjeux relatifs aux courses de galop (CGI, art. 1609 tertricies)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 721, width: 238, height: 12, fontSize: 17, }}>
<span>
4267 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 750, width: 238, height: 16, fontSize: 17, }}>
<span>
Prélèvement sur les paris sportifs
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 780, width: 17, height: 13, fontSize: 17, }}>
<span>
76
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 777, width: 419, height: 19, fontSize: 17, }}>
<span>
– au profit de l’État (CGI, art. 302 bis ZH) au taux de 5,7 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 828, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 780, width: 238, height: 13, fontSize: 17, }}>
<span>
4257 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 819, width: 17, height: 13, fontSize: 17, }}>
<span>
77
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 810, width: 494, height: 16, fontSize: 17, }}>
<span>
– au profit des organismes de sécurité sociale (CSS, art. L137-21) au
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 828, width: 98, height: 15, fontSize: 17, }}>
<span>
taux de 1,8 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 819, width: 238, height: 13, fontSize: 17, }}>
<span>
4260 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 867, width: 17, height: 13, fontSize: 17, }}>
<span>
78
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 858, width: 503, height: 16, fontSize: 17, }}>
<span>
– au profit de l’agence nationale du sport (ANS) (CGI, art. 1609 tricies)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 876, width: 121, height: 15, fontSize: 17, }}>
<span>
au taux de 1,8 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 867, width: 238, height: 13, fontSize: 17, }}>
<span>
4265 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 906, width: 254, height: 16, fontSize: 17, }}>
<span>
Prélèvement sur les cercles de jeux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 936, width: 17, height: 13, fontSize: 17, }}>
<span>
79
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 933, width: 411, height: 19, fontSize: 17, }}>
<span>
– au profit de l’État (CGI, art. 302 bis ZI) au taux de 1,8 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 960, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 936, width: 238, height: 13, fontSize: 17, }}>
<span>
4258 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 975, width: 17, height: 13, fontSize: 17, }}>
<span>
80
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 966, width: 494, height: 16, fontSize: 17, }}>
<span>
– au profit des organismes de sécurité sociale (CSS, art. L137-22) au
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 984, width: 98, height: 15, fontSize: 17, }}>
<span>
taux de 0,2 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 975, width: 238, height: 13, fontSize: 17, }}>
<span>
4261 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1033, width: 15, height: 12, fontSize: 17, }}>
<span>
81
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1014, width: 766, height: 16, fontSize: 17, }}>
<span>
Prélèvement au profit des organismes de sécurité sociale sur le produit des appels à des numéros surtaxés
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1032, width: 815, height: 16, fontSize: 17, }}>
<span>
effectués dans le cadre des programmes télévisés et radiodiffusés comportant des jeux et des concours (CSS, art.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1051, width: 189, height: 16, fontSize: 17, }}>
<span>
L137-19) au taux de 9,5 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1033, width: 238, height: 12, fontSize: 17, }}>
<span>
4262 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1090, width: 17, height: 13, fontSize: 17, }}>
<span>
82
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1081, width: 553, height: 16, fontSize: 17, }}>
<span>
Contribution sociale à la charge des fournisseurs agréés de produits du tabac
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1099, width: 346, height: 16, fontSize: 17, }}>
<span>
(CSS, art. L137-27 et suivants) au taux de 5,6 %
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 1090, width: 113, height: 16, fontSize: 17, }}>
<span>
Base imposable
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1090, width: 238, height: 13, fontSize: 17, }}>
<span>
4278 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1138, width: 17, height: 13, fontSize: 17, }}>
<span>
84
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 1129, width: 582, height: 16, fontSize: 17, }}>
<span>
Taxe sur l’exploration d’hydrocarbures calculée selon le barème fixé à l’article
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 81, top: 1148, width: 426, height: 15, fontSize: 17, }}>
<span>
1590 du CGI et perçue au profit des collectivités territoriales
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 695, top: 1129, width: 79, height: 13, fontSize: 17, }}>
<span>
Numéro de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 689, top: 1148, width: 91, height: 15, fontSize: 17, }}>
<span>
département
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 807, top: 1129, width: 95, height: 16, fontSize: 17, }}>
<span>
Droits pour le
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 809, top: 1148, width: 90, height: 15, fontSize: 17, }}>
<span>
département
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1138, width: 238, height: 13, fontSize: 17, }}>
<span>
4291 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1177, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1207, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1237, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1267, width: 407, height: 15, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1296, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1326, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1356, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1386, width: 407, height: 15, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1415, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 1454, width: 17, height: 13, fontSize: 17, }}>
<span>
85
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 1445, width: 583, height: 16, fontSize: 17, }}>
<span>
Taxe sur l’exploration de gîtes géothermiques calculée selon le barème fixé à
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1464, width: 482, height: 15, fontSize: 17, }}>
<span>
l’article 1590 du CGI et perçue au profit des collectivités territoriales
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 678, top: 1445, width: 113, height: 13, fontSize: 17, }}>
<span>
Code INSEE de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 689, top: 1464, width: 91, height: 12, fontSize: 17, }}>
<span>
la collectivité
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 826, top: 1454, width: 57, height: 13, fontSize: 17, }}>
<span>
Montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 1454, width: 238, height: 13, fontSize: 17, }}>
<span>
4292 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1493, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1523, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1553, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1583, width: 407, height: 15, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 1612, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 582, top: 1658, width: 37, height: 13, fontSize: 17, }}>
<span>
– 4 –
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_132 pdf-obj-fixed acroform-field "
                   name="SN"
                   data-field-id="26418824" data-annot-id="25842064" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_133 pdf-obj-fixed acroform-field "
                   name="SP"
                   data-field-id="26421608" data-annot-id="25512768" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_134 pdf-obj-fixed acroform-field " name="VY"
                   data-field-id="26424264" data-annot-id="25512960" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_135 pdf-obj-fixed acroform-field " name="VZ"
                   data-field-id="26426984" data-annot-id="25833296" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_136 pdf-obj-fixed acroform-field "
                   name="WD"
                   data-field-id="26429704" data-annot-id="25833488" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_137 pdf-obj-fixed acroform-field "
                   name="WE"
                   data-field-id="26432424" data-annot-id="25833680" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_138 pdf-obj-fixed acroform-field " name="WH"
                   data-field-id="26435144" data-annot-id="25833872" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_139 pdf-obj-fixed acroform-field "
                   name="WZ"
                   data-field-id="26437864" data-annot-id="25834064" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_140 pdf-obj-fixed acroform-field " name="WJ"
                   data-field-id="26440584" data-annot-id="25834256" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_141 pdf-obj-fixed acroform-field "
                   name="ZA"
                   data-field-id="26548712" data-annot-id="25834448" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_142 pdf-obj-fixed acroform-field " name="WK"
                   data-field-id="26443304" data-annot-id="25844288" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_143 pdf-obj-fixed acroform-field "
                   name="ZM"
                   data-field-id="26446024" data-annot-id="25844480" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_144 pdf-obj-fixed acroform-field " name="WL"
                   data-field-id="26448744" data-annot-id="25844672" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_145 pdf-obj-fixed acroform-field "
                   name="YA"
                   data-field-id="26451432" data-annot-id="25844864" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field " name="WM"
                   data-field-id="26454184" data-annot-id="25845056" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_147 pdf-obj-fixed acroform-field "
                   name="YB"
                   data-field-id="26456952" data-annot-id="25845248" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_148 pdf-obj-fixed acroform-field "
                   name="WN"
                   data-field-id="26263464" data-annot-id="25845440" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_149 pdf-obj-fixed acroform-field "
                   name="WP"
                   data-field-id="26463448" data-annot-id="25845904" type="text"/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'WQ', false)}
                   className="pde-form-field pdf-annot obj_150 pdf-obj-fixed acroform-field " name="WQ"
                   data-field-id="26466184" data-annot-id="25846096" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'ZN', false)}
                   className="pde-form-field pdf-annot obj_151 pdf-obj-fixed acroform-field " name="ZN"
                   data-field-id="26468920" data-annot-id="25846288" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'WR', false)}
                   className="pde-form-field pdf-annot obj_152 pdf-obj-fixed acroform-field " name="WR"
                   data-field-id="26471656" data-annot-id="25846480" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'WS', false)}
                   className="pde-form-field pdf-annot obj_153 pdf-obj-fixed acroform-field " name="WS"
                   data-field-id="26474392" data-annot-id="25846672" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'ZP', false)}
                   className="pde-form-field pdf-annot obj_154 pdf-obj-fixed acroform-field " name="ZP"
                   data-field-id="26477128" data-annot-id="25846864" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'WT', false)}
                   className="pde-form-field pdf-annot obj_155 pdf-obj-fixed acroform-field " name="WT"
                   data-field-id="26479864" data-annot-id="25847056" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'WU', false)}
                   className="pde-form-field pdf-annot obj_156 pdf-obj-fixed acroform-field " name="WU"
                   data-field-id="26482600" data-annot-id="25847248" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_157 pdf-obj-fixed acroform-field "
                   name="WV"
                   data-field-id="26485336" data-annot-id="25847440" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_158 pdf-obj-fixed acroform-field " name="YK"
                   data-field-id="26488072" data-annot-id="25847632" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_159 pdf-obj-fixed acroform-field "
                   name="SE1"
                   data-field-id="26561128" data-annot-id="25847824" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_160 pdf-obj-fixed acroform-field "
                   name="YL"
                   data-field-id="26490808" data-annot-id="25848016" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_161 pdf-obj-fixed acroform-field "
                   name="SF1"
                   data-field-id="26493544" data-annot-id="25848208" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_162 pdf-obj-fixed acroform-field "
                   name="SF2"
                   data-field-id="26496280" data-annot-id="25848400" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_163 pdf-obj-fixed acroform-field "
                   name="SF4"
                   data-field-id="26499016" data-annot-id="25848592" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_164 pdf-obj-fixed acroform-field "
                   name="SF3"
                   data-field-id="26501752" data-annot-id="25848784" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_165 pdf-obj-fixed acroform-field "
                   name="SF5"
                   data-field-id="26504488" data-annot-id="25845632" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_166 pdf-obj-fixed acroform-field "
                   name="SF7"
                   data-field-id="26507224" data-annot-id="25849504" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_167 pdf-obj-fixed acroform-field "
                   name="SF6"
                   data-field-id="26509960" data-annot-id="25849696" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_168 pdf-obj-fixed acroform-field "
                   name="SF8"
                   data-field-id="26512696" data-annot-id="25849888" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_169 pdf-obj-fixed acroform-field "
                   name="SF9"
                   data-field-id="26515432" data-annot-id="25850080" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_170 pdf-obj-fixed acroform-field "
                   name="ZZ"
                   data-field-id="26534488" data-annot-id="25850272" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_171 pdf-obj-fixed acroform-field " name="WF"
                   data-field-id="26537208" data-annot-id="25850464" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_172 pdf-obj-fixed acroform-field "
                   name="YC"
                   data-field-id="26539928" data-annot-id="25850656" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_173 pdf-obj-fixed acroform-field " name="WG"
                   data-field-id="26542648" data-annot-id="25850848" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_174 pdf-obj-fixed acroform-field " name="ZY"
                   data-field-id="26690328" data-annot-id="25851040" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_175 pdf-obj-fixed acroform-field "
                   name="SE2"
                   data-field-id="26561384" data-annot-id="25851232" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_176 pdf-obj-fixed acroform-field "
                   name="SE3"
                   data-field-id="26564600" data-annot-id="25851424" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_177 pdf-obj-fixed acroform-field "
                   name="SE4"
                   data-field-id="26571352" data-annot-id="25851616" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_178 pdf-obj-fixed acroform-field "
                   name="SE5"
                   data-field-id="26574920" data-annot-id="25851808" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_179 pdf-obj-fixed acroform-field "
                   name="SE6"
                   data-field-id="26575176" data-annot-id="25852000" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_180 pdf-obj-fixed acroform-field "
                   name="SE7"
                   data-field-id="26578008" data-annot-id="25852192" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_181 pdf-obj-fixed acroform-field "
                   name="SE8"
                   data-field-id="26584056" data-annot-id="25852384" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_182 pdf-obj-fixed acroform-field "
                   name="SE9"
                   data-field-id="26581032" data-annot-id="25852576" type="text"/>

            <Field component={PdfNumberInput} disabled={true}
                   className="pde-form-field pdf-annot obj_183 pdf-obj-fixed acroform-field " name="SH"
                   data-field-id="26587080" data-annot-id="25852768" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_184 pdf-obj-fixed acroform-field "
                   name="ZZ1"
                   data-field-id="26658472" data-annot-id="25852960" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_185 pdf-obj-fixed acroform-field "
                   name="ZW1"
                   data-field-id="26674488" data-annot-id="25853152" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_186 pdf-obj-fixed acroform-field "
                   name="ZZ2"
                   data-field-id="26661816" data-annot-id="25853344" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_187 pdf-obj-fixed acroform-field "
                   name="ZW2"
                   data-field-id="26677656" data-annot-id="25853536" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_188 pdf-obj-fixed acroform-field "
                   name="ZZ3"
                   data-field-id="26664984" data-annot-id="25853728" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_189 pdf-obj-fixed acroform-field "
                   name="ZW3"
                   data-field-id="26680824" data-annot-id="25853920" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_190 pdf-obj-fixed acroform-field "
                   name="ZZ4"
                   data-field-id="26668152" data-annot-id="25854112" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_191 pdf-obj-fixed acroform-field "
                   name="ZW4"
                   data-field-id="26683992" data-annot-id="25854304" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_192 pdf-obj-fixed acroform-field "
                   name="ZZ5"
                   data-field-id="26671320" data-annot-id="25854496" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_193 pdf-obj-fixed acroform-field "
                   name="ZW5"
                   data-field-id="26687160" data-annot-id="25854688" type="text"/>

          </div>
        </div>
      </div>

      <div data-type="pdf-page" id="pdf-page-4" data-page-num="4" data-ratio="1.415000" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-4 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 151, top: 37, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 67, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 97, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 126, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 156, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 186, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 216, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 245, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 275, width: 407, height: 16, fontSize: 17, }}>
<span>
– Droits pour le département ou la collectivité territoriale :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 314, width: 17, height: 13, fontSize: 17, }}>
<span>
88
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 305, width: 421, height: 16, fontSize: 17, }}>
<span>
Contribution sur les boissons contenant des sucres ajoutés
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 323, width: 130, height: 16, fontSize: 17, }}>
<span>
(CGI, art.1613 ter)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 706, top: 305, width: 57, height: 13, fontSize: 17, }}>
<span>
Nombre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 691, top: 323, width: 86, height: 13, fontSize: 17, }}>
<span>
d’hectolitres
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 314, width: 238, height: 13, fontSize: 17, }}>
<span>
4294 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 362, width: 418, height: 17, fontSize: 17, }}>
<span>
89 Contribution sur les eaux (CGI, art. 1613 quater II 1°)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 706, top: 353, width: 57, height: 13, fontSize: 17, }}>
<span>
Nombre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 691, top: 372, width: 86, height: 13, fontSize: 17, }}>
<span>
d’hectolitres
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 363, width: 238, height: 12, fontSize: 17, }}>
<span>
4296 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 411, width: 17, height: 13, fontSize: 17, }}>
<span>
90
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 401, width: 492, height: 16, fontSize: 17, }}>
<span>
Contribution sur les boissons contenant des édulcorants de synthèse
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 420, width: 195, height: 16, fontSize: 17, }}>
<span>
(CGI, art. 1613 quater II 2°)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 706, top: 402, width: 57, height: 12, fontSize: 17, }}>
<span>
Nombre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 691, top: 420, width: 86, height: 13, fontSize: 17, }}>
<span>
d’hectolitres
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 411, width: 238, height: 13, fontSize: 17, }}>
<span>
4295 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 40, top: 459, width: 17, height: 13, fontSize: 17, }}>
<span>
92
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 450, width: 326, height: 13, fontSize: 17, }}>
<span>
Contribution sur les sources d’eaux minérales
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 468, width: 111, height: 16, fontSize: 17, }}>
<span>
(CGI, art. 1582)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 549, top: 450, width: 114, height: 13, fontSize: 17, }}>
<span>
Code INSEE de
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 562, top: 469, width: 88, height: 12, fontSize: 17, }}>
<span>
la commune
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 706, top: 450, width: 57, height: 13, fontSize: 17, }}>
<span>
Nombre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 691, top: 468, width: 86, height: 13, fontSize: 17, }}>
<span>
d’hectolitres
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 826, top: 459, width: 57, height: 13, fontSize: 17, }}>
<span>
Montant
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 921, top: 459, width: 238, height: 13, fontSize: 17, }}>
<span>
4293 ……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 498, width: 193, height: 16, fontSize: 17, }}>
<span>
– Droits pour la commune :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 528, width: 193, height: 16, fontSize: 17, }}>
<span>
– Droits pour la commune :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 558, width: 193, height: 15, fontSize: 17, }}>
<span>
– Droits pour la commune :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 587, width: 193, height: 16, fontSize: 17, }}>
<span>
– Droits pour la commune :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 617, width: 193, height: 16, fontSize: 17, }}>
<span>
– Droits pour la commune :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 647, width: 193, height: 16, fontSize: 17, }}>
<span>
– Droits pour la commune :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 677, width: 193, height: 15, fontSize: 17, }}>
<span>
– Droits pour la commune :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 152, top: 706, width: 359, height: 16, fontSize: 17, }}>
<span>
TOTAL DES LIGNES 36 à 92 (à reporter ligne 55)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 984, top: 716, width: 175, height: 3, fontSize: 17, }}>
<span>
……………………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 582, top: 1658, width: 37, height: 13, fontSize: 17, }}>
<span>
– 5 –
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 733, width: 166, height: 15, fontSize: 17, }}>
<span>
V - RÉCAPITULATION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 33, top: 765, width: 331, height: 16, fontSize: 17, }}>
<span>
49 Solde excédentaire (report de la ligne 35)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 588, top: 775, width: 7, height: 3, fontSize: 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 475, top: 765, width: 362, height: 16, fontSize: 17, }}>
<span>
………………… 54 TVA (report de la ligne 33)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1035, top: 775, width: 110, height: 3, fontSize: 17, }}>
<span>
…………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1147, top: 775, width: 8, height: 3, fontSize: 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 795, width: 889, height: 16, fontSize: 17, }}>
<span>
50 Remboursement demandé au cadre VI, page 4 8002 ………………… 55 Taxes assimilées (total lignes 36 à 85)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 588, top: 805, width: 7, height: 3, fontSize: 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1035, top: 805, width: 110, height: 3, fontSize: 17, }}>
<span>
…………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1147, top: 805, width: 8, height: 3, fontSize: 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 843, width: 15, height: 13, fontSize: 17, }}>
<span>
51
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 825, width: 321, height: 16, fontSize: 17, }}>
<span>
Crédit à reporter (cette somme est à reporter
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 69, top: 843, width: 312, height: 16, fontSize: 17, }}>
<span>
ligne 24 de la prochaine déclaration CA 12 /
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 68, top: 862, width: 64, height: 16, fontSize: 17, }}>
<span>
CA 12 E)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 416, top: 843, width: 169, height: 13, fontSize: 17, }}>
<span>
8003 …………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 588, top: 853, width: 7, height: 3, fontSize: 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 700, top: 865, width: 409, height: 15, fontSize: 13, }}>
<span>
ATTENTION ! UNE SITUATION DE TVA CRÉDITRICE (LIGNE 49 SERVIE)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 667, top: 879, width: 476, height: 12, fontSize: 13, }}>
<span>
NE DISPENSE PAS DU PAIEMENT DES TAXES ASSIMILÉES DÉCLARÉES LIGNE 55
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 891, width: 331, height: 16, fontSize: 17, }}>
<span>
52 Crédit imputé sur les prochains acomptes
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 416, top: 892, width: 169, height: 12, fontSize: 17, }}>
<span>
8004 …………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 588, top: 902, width: 7, height: 2, fontSize: 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 55, top: 920, width: 517, height: 13, fontSize: 13, }}>
<span>
Acomptes (cochez les cases correspondant aux acomptes déduits I. 30). Précisez l’année.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 946, width: 17, height: 13, fontSize: 17, }}>
<span>
58
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 130, top: 954, width: 42, height: 13, fontSize: 17, }}>
<span>
Juillet
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 201, top: 945, width: 22, height: 26, fontSize: 26, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 224, top: 945, width: 22, height: 26, fontSize: 26, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 246, top: 945, width: 22, height: 26, fontSize: 26, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 177, top: 945, width: 2, height: 26, fontSize: 26, }}>
<span>
|
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 179, top: 945, width: 22, height: 26, fontSize: 26, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 386, top: 954, width: 73, height: 13, fontSize: 17, }}>
<span>
Décembre
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 489, top: 947, width: 23, height: 24, fontSize: 24, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 468, top: 947, width: 21, height: 24, fontSize: 24, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 512, top: 947, width: 22, height: 24, fontSize: 24, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 534, top: 947, width: 22, height: 24, fontSize: 24, }}>
<span>
__ |
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 465, top: 947, width: 2, height: 24, fontSize: 24, }}>
<span>
|
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 616, top: 943, width: 275, height: 19, fontSize: 17, }}>
<span>
56 TOTAL À PAYER (lignes 54 + 55)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 651, top: 964, width: 335, height: 15, fontSize: 15, }}>
<span>
(N’oubliez pas de joindre le règlement correspondant)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1147, top: 973, width: 8, height: 2, fontSize: 17, }}>
<span>
..
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1035, top: 973, width: 110, height: 2, fontSize: 17, }}>
<span>
…………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 298, top: 992, width: 592, height: 13, fontSize: 17, }}>
<span>
BASE DE CALCUL DES ACOMPTES DUS AU TITRE DE L’EXERCICE SUIVANT
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1022, width: 334, height: 16, fontSize: 17, }}>
<span>
57 TVA [ligne 16 – (lignes 11 + 12 + 15 + 22)]
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1035, top: 1032, width: 110, height: 3, fontSize: 17, }}>
<span>
…………………
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1147, top: 1032, width: 8, height: 3, fontSize: 17, }}>
<span>
..
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'SW', false)}
                   className="pde-form-field pdf-annot obj_195 pdf-obj-fixed acroform-field " name="SW"
                   data-field-id="26518232" data-annot-id="26797184" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'SV', false)}
                   className="pde-form-field pdf-annot obj_196 pdf-obj-fixed acroform-field " name="SV"
                   data-field-id="26520888" data-annot-id="25437568" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'XA', false)}
                   className="pde-form-field pdf-annot obj_197 pdf-obj-fixed acroform-field " name="XA"
                   data-field-id="26523608" data-annot-id="26887408" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'SX', false)}
                   className="pde-form-field pdf-annot obj_198 pdf-obj-fixed acroform-field " name="SX"
                   data-field-id="26526328" data-annot-id="26846208" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'XB', false)}
                   className="pde-form-field pdf-annot obj_199 pdf-obj-fixed acroform-field " name="XB"
                   data-field-id="26529048" data-annot-id="26846400" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput}
                   setError={setError} formName={form} error={_get(errors, 'RD', false)}
                   className="pde-form-field pdf-annot obj_200 pdf-obj-fixed acroform-field " name="RD"
                   data-field-id="26531768" data-annot-id="27107376" type="text" onBlur={validateForm}/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_201 pdf-obj-fixed acroform-field "
                   name="AE"
                   data-field-id="26554568" data-annot-id="27107568" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_202 pdf-obj-fixed acroform-field "
                   name="ZA"
                   data-field-id="26548712" data-annot-id="27107760" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_203 pdf-obj-fixed acroform-field "
                   name="AC1"
                   data-field-id="26593848" data-annot-id="27107952" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_204 pdf-obj-fixed acroform-field "
                   name="AC2"
                   data-field-id="26594104" data-annot-id="27108144" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_205 pdf-obj-fixed acroform-field "
                   name="AC3"
                   data-field-id="26597112" data-annot-id="27110352" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_206 pdf-obj-fixed acroform-field "
                   name="AC4"
                   data-field-id="26600232" data-annot-id="27110544" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_207 pdf-obj-fixed acroform-field "
                   name="AC5"
                   data-field-id="26603432" data-annot-id="27110736" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_208 pdf-obj-fixed acroform-field "
                   name="AC6"
                   data-field-id="26606552" data-annot-id="27110928" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_209 pdf-obj-fixed acroform-field "
                   name="AC7"
                   data-field-id="26609752" data-annot-id="27111120" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_210 pdf-obj-fixed acroform-field "
                   name="AD1"
                   data-field-id="26616536" data-annot-id="27111312" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_211 pdf-obj-fixed acroform-field "
                   name="AD2"
                   data-field-id="26616792" data-annot-id="27111504" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_212 pdf-obj-fixed acroform-field "
                   name="AD3"
                   data-field-id="26619800" data-annot-id="27111968" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_213 pdf-obj-fixed acroform-field "
                   name="AD4"
                   data-field-id="26622920" data-annot-id="27112160" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_214 pdf-obj-fixed acroform-field "
                   name="AD5"
                   data-field-id="26626120" data-annot-id="27112352" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_215 pdf-obj-fixed acroform-field "
                   name="AD6"
                   data-field-id="26629240" data-annot-id="27112544" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_216 pdf-obj-fixed acroform-field "
                   name="AD7"
                   data-field-id="26632440" data-annot-id="27112736" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_217 pdf-obj-fixed acroform-field "
                   name="AB1"
                   data-field-id="26639224" data-annot-id="27112928" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_218 pdf-obj-fixed acroform-field "
                   name="AB2"
                   data-field-id="26639480" data-annot-id="27113120" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_219 pdf-obj-fixed acroform-field "
                   name="AB3"
                   data-field-id="26642488" data-annot-id="27113312" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_220 pdf-obj-fixed acroform-field "
                   name="AB4"
                   data-field-id="26645608" data-annot-id="27113504" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_221 pdf-obj-fixed acroform-field "
                   name="AB5"
                   data-field-id="26648808" data-annot-id="27113696" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_222 pdf-obj-fixed acroform-field "
                   name="AB6"
                   data-field-id="26651928" data-annot-id="27113888" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_223 pdf-obj-fixed acroform-field "
                   name="AB7"
                   data-field-id="26655128" data-annot-id="27114080" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_224 pdf-obj-fixed acroform-field "
                   name="ZZ6"
                   data-field-id="26693496" data-annot-id="27114272" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_225 pdf-obj-fixed acroform-field "
                   name="ZW6"
                   data-field-id="26721976" data-annot-id="27114464" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_226 pdf-obj-fixed acroform-field "
                   name="ZZ7"
                   data-field-id="26696664" data-annot-id="27114656" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_227 pdf-obj-fixed acroform-field "
                   name="ZW7"
                   data-field-id="26725096" data-annot-id="27114848" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_228 pdf-obj-fixed acroform-field "
                   name="ZZ8"
                   data-field-id="26699832" data-annot-id="27111696" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_229 pdf-obj-fixed acroform-field "
                   name="ZW8"
                   data-field-id="26728296" data-annot-id="27115568" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_230 pdf-obj-fixed acroform-field "
                   name="ZZ9"
                   data-field-id="26703000" data-annot-id="27115760" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_231 pdf-obj-fixed acroform-field "
                   name="ZW9"
                   data-field-id="26731416" data-annot-id="27115952" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_232 pdf-obj-fixed acroform-field "
                   name="ZZ10" data-field-id="26706168" data-annot-id="27116144" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_233 pdf-obj-fixed acroform-field "
                   name="ZW10" data-field-id="26734616" data-annot-id="27116336" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_234 pdf-obj-fixed acroform-field "
                   name="ZZ11" data-field-id="26709336" data-annot-id="27116528" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_235 pdf-obj-fixed acroform-field "
                   name="ZW11" data-field-id="26737736" data-annot-id="27116720" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_236 pdf-obj-fixed acroform-field "
                   name="ZZ12" data-field-id="26712504" data-annot-id="27116912" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_237 pdf-obj-fixed acroform-field "
                   name="ZW12" data-field-id="26740936" data-annot-id="27117104" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_238 pdf-obj-fixed acroform-field "
                   name="ZZ13" data-field-id="26715672" data-annot-id="27117296" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_239 pdf-obj-fixed acroform-field "
                   name="ZW13" data-field-id="26744056" data-annot-id="27117488" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_240 pdf-obj-fixed acroform-field "
                   name="ZW14" data-field-id="26747256" data-annot-id="27117680" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_241 pdf-obj-fixed acroform-field " name="ZZ14"
                   data-field-id="26718840" data-annot-id="27117872" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_242 pdf-obj-fixed acroform-field "
                   name="SA" data-field-id="26750376" data-annot-id="27118064" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_243 pdf-obj-fixed acroform-field "
                   name="SB" data-field-id="26753528" data-annot-id="27118256" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_244 pdf-obj-fixed acroform-field "
                   name="SC" data-field-id="26756680" data-annot-id="27118448" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_245 pdf-obj-fixed acroform-field "
                   name="VA" data-field-id="26759832" data-annot-id="27118640" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_246 pdf-obj-fixed acroform-field "
                   name="RE" data-field-id="26762984" data-annot-id="27118832" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_247 pdf-obj-fixed acroform-field "
                   name="RC" data-field-id="26766136" data-annot-id="27119024" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_248 pdf-obj-fixed acroform-field "
                   name="RB" data-field-id="26769288" data-annot-id="27119216" type="text"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_249 pdf-obj-fixed acroform-field "
                   name="RA" data-field-id="26772440" data-annot-id="27119408" type="text"/>

            <Field component={PdfInputValuesWithSpaces} className="pde-form-field pdf-annot obj_250 pdf-obj-fixed acroform-field "
                   name="UD" data-field-id="26775592" data-annot-id="27119600" max="4" type="text"/>

            <Field component={PdfInputValuesWithSpaces} className="pde-form-field pdf-annot obj_251 pdf-obj-fixed acroform-field "
                   name="UB" data-field-id="26779352" data-annot-id="27119792" max="4" type="text"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_252 pdf-obj-fixed acroform-field "
                   name="TD" data-field-id="26783064" data-annot-id="27119984" value="Yes" type="checkbox"
                   data-default-value="Off"/>

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_253 pdf-obj-fixed acroform-field "
                   name="TB" data-field-id="26787768" data-annot-id="27120176" value="Yes" type="checkbox"
                   data-default-value="Off"/>

          </div>
        </div>
      </div>
    </div>
  );
};

export default ca12AN3517Form;
