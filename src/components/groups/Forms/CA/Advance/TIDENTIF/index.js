/* eslint-disable */
import React from "react";
import { Field } from "redux-form";
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { parse as p, useCompute } from "helpers/pdfforms";
import { ReduxPicker } from "components/reduxForm/Inputs";
import { RibAutoComplete } from 'containers/reduxForm/Inputs';
import moment from 'moment';

import "react-datepicker/dist/react-datepicker.css";
import './style.scss';


const T_IDENTIF = (props) => {
  const { change, tIdentif } = props;

  useCompute('AAT', ({ AAT }) => p(AAT).toString().length <= 14 ? AAT : p(AAT).toString().slice(0, -1), ['AAT'], tIdentif, change);
  useCompute('KA', ({ GAE, GAC, KD, CA }) => GAE && GAC ? `${KD}-${moment(CA).format('MMYYYY')}-3310CA3` : '', ['GAE', 'GAC', 'KD', 'CA'], tIdentif, change);
  useCompute('KB', ({ GBE, GBC, KD, CA }) => GBE && GBC ? `${KD}-${moment(CA).format('MMYYYY')}-3310CA3` : '', ['GBE', 'GBC', 'KD', 'CA'], tIdentif, change);
  useCompute('KC', ({ GCE, GCC, KD, CA }) => GCE && GCC ? `${KD}-${moment(CA).format('MMYYYY')}-3310CA3` : '', ['GCE', 'GCC', 'KD', 'CA'], tIdentif, change);

  return (
    <div className="
    t_identif">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 422, top: 67, width: 310, height: 17, 'font-size': 23, }}>
<span>
DONNEES D&#39;IDENTIFICATION
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1049, top: 68, width: 97, height: 15, 'font-size': 21, }}>
<span>
T-IDENTIF
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 434, top: 151, width: 337, height: 15, 'font-size': 21, }}>
<span>
IDENTIFICATION DU REDEVABLE
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 203, top: 210, width: 229, height: 19, 'font-size': 21, }}>
<span>
DÈsignation du redevable :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 490, top: 494, width: 221, height: 15, 'font-size': 21, }}>
<span>
CESSION/CESSATION :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 194, top: 556, width: 226, height: 15, 'font-size': 21, }}>
<span>
Date de cession / cessation :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 191, top: 636, width: 264, height: 19, 'font-size': 21, }}>
<span>
Date de redressement judiciaire :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 455, top: 720, width: 292, height: 14, 'font-size': 21, }}>
<span>
PERIODE DE DECLARATION :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 802, width: 219, height: 19, 'font-size': 21, }}>
<span>
DÈbut pÈriode dÈclaration :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 140, top: 860, width: 196, height: 19, 'font-size': 21, }}>
<span>
Fin pÈriode dÈclaration :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 72, top: 1394, width: 148, height: 19, 'font-size': 21, }}>
<span>
Compte bancaire
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 102, top: 1444, width: 88, height: 19, 'font-size': 21, }}>
<span>
1er compte
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 98, top: 1477, width: 94, height: 19, 'font-size': 21, }}>
<span>
2nd compte
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 92, top: 1511, width: 107, height: 19, 'font-size': 21, }}>
<span>
3Ëme compte
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 699, top: 210, width: 211, height: 15, 'font-size': 21, }}>
<span>
NumÈro SIRET / SIREN :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 699, top: 249, width: 231, height: 20, 'font-size': 21, }}>
<span>
RÈfÈrence Obligation fiscale
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 700, top: 272, width: 52, height: 19, 'font-size': 21, }}>
<span>
(ROF)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 154, top: 591, width: 25, height: 15, 'font-size': 21, }}>
<span>
Ou
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 748, top: 798, width: 182, height: 23, 'font-size': 21, }}>
<span>
…chÈance (pour 3514):
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 357, top: 935, width: 490, height: 15, 'font-size': 21, }}>
<span>
MODALITES DE DECLARATION ET DE PAIEMENT
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 998, width: 861, height: 19, 'font-size': 21, }}>
<span>
DÈclaration : A compter du 1er janvier 2002, les dÈclarations doivent obligatoirement Ítre Ètablies en euro.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1021, width: 828, height: 19, 'font-size': 21, }}>
<span>
Attention : ne portez pas de centimes d&#39;euro (cf. rËgles d&#39;arrondi dans le cahier des charges EDI-TVA).
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1066, width: 838, height: 20, 'font-size': 21, }}>
<span>
Paiement : A compter du 1er janvier 2002, les paiements doivent obligatoirement Ítre effectuÈs en euro.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 541, top: 1130, width: 119, height: 15, 'font-size': 21, }}>
<span>
PAIEMENT :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 506, top: 1180, width: 190, height: 19, 'font-size': 21, }}>
<span>
TÈlÈrËglement SEPA :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1263, width: 978, height: 19, 'font-size': 21, }}>
<span>
Si vous avez choisi de payer par tÈlÈrËglement A, veuillez indiquer le montant du (des) prÈlËvement(s), l&#39;identification du
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 35, top: 1285, width: 417, height: 20, 'font-size': 21, }}>
<span>
compte bancaire ainsi que la rÈfÈrence du paiement.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 34, top: 1308, width: 790, height: 19, 'font-size': 21, }}>
<span>
NOTA : A compter du 1er janvier 2002, les prÈlËvements sont obligatoirement effectuÈs en euros.
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 368, top: 1378, width: 86, height: 15, 'font-size': 21, }}>
<span>
RÈfÈrence
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 617, top: 1394, width: 187, height: 19, 'font-size': 21, }}>
<span>
Montant prÈlËvement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 931, top: 1394, width: 171, height: 19, 'font-size': 21, }}>
<span>
RÈfÈrence paiement
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 331, top: 1411, width: 161, height: 19, 'font-size': 21, }}>
<span>
BIC/IBAN (SEPA)
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="AAA"
                   data-field-id="20022472" data-annot-id="19777888" type="text" autoComplete="nope" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="AAC"
                   data-field-id="20022664" data-annot-id="19322224" type="text" autoComplete="nope" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="AAG"
                   data-field-id="20022872" data-annot-id="19535440" type="text" autoComplete="nope" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="AAD"
                   data-field-id="20023144" data-annot-id="19535632" type="text" autoComplete="nope" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="AAH"
                   data-field-id="20023480" data-annot-id="19535824" type="text" autoComplete="nope" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="AAI"
                   data-field-id="20023816" data-annot-id="19536016" type="text" autoComplete="nope" disabled/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field "
                   name="AAT"
                   data-field-id="20024152" data-annot-id="19786128" type="text" autoComplete="nope" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="KD"
                   data-field-id="20024488" data-annot-id="19786320" type="text" autoComplete="nope" disabled/>

            <Field component={ReduxPicker} handleChange={change} isClearable
                   className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="BB"
                   data-field-id="20024824" data-annot-id="19786512" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} handleChange={change} isClearable
                   className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="KE"
                   data-field-id="20025304" data-annot-id="19786704" type="text" autoComplete="nope"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="CA"
                   data-field-id="20025640" data-annot-id="19786896" type="text" autoComplete="nope" disabled/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="CB"
                   data-field-id="20025976" data-annot-id="19787088" type="text" autoComplete="nope" disabled/>

            <Field component={ReduxPicker} handleChange={change} isClearable
                   className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="KF"
                   data-field-id="20026312" data-annot-id="19787280" type="text" autoComplete="nope" disabled/>

            <Field component="input" className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field "
                   name="GAC"
                   data-field-id="20028808" data-annot-id="19787472" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field "
                   name="GBC"
                   data-field-id="20029816" data-annot-id="19787664" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "
                   name="GCC"
                   data-field-id="20030152" data-annot-id="19787856" type="text" autoComplete="nope"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="HA"
                   data-field-id="20026648" data-annot-id="19788048" type="text" autoComplete="nope"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field "
                   name="HB"
                   data-field-id="20026984" data-annot-id="19788512" type="text" autoComplete="nope"/>

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="HC"
                   data-field-id="20027320" data-annot-id="19788704" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field " name="KA"
                   disabled
                   data-field-id="20027656" data-annot-id="19788896" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field " name="KB"
                   disabled
                   data-field-id="20028136" data-annot-id="19789088" type="text" autoComplete="nope"/>

            <Field component="input" className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field " name="KC"
                   disabled
                   data-field-id="20028472" data-annot-id="19789280" type="text" autoComplete="nope"/>

            <Field component={RibAutoComplete} onChangeValues={(value) => { change('GAC', value.bic) }}
                   className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field " name="GAE"
                   data-field-id="20029144" data-annot-id="19789472" type="text" autoComplete="nope"/>

            <Field component={RibAutoComplete} onChangeValues={(value) => { change('GBC', value.bic) }}
                   className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field " name="GBE"
                   data-field-id="20029480" data-annot-id="19789664" type="text" autoComplete="nope"/>

            <Field component={RibAutoComplete} onChangeValues={(value) => { change('GCC', value.bic) }}
                   className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field " name="GCE"
                   data-field-id="20030488" data-annot-id="19789856" type="text" autoComplete="nope"/>

          </div>
        </div>
      </div>
    </div>
  );
};

export default T_IDENTIF;
