/* eslint-disable */
import React from "react";
import { Field } from "redux-form";
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { PdfCheckbox } from 'components/reduxForm/Inputs';
import { useCompute, parse as p } from "helpers/pdfforms";

import "./style.scss";

const CA12AC3514 = (props) => {
  const { change, ca12AC3514Form } = props;

  useCompute('HC', ({ HA, HB }) => p(HA) - (HB), ['HA', 'HB'], ca12AC3514Form, change);
  useCompute('IC', ({ IB, IA }) => p(IB) - (IA), ['IB', 'IA'], ca12AC3514Form, change);

  return (
    <div className="form-ca-advance-3514">
      <div data-type="pdf-page" id="pdf-page-1" data-page-num="1" data-ratio="1.414167" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-1 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 44, top: 32, width: 96, height: 15, "font-size": 19, }}>
<span>
N° 3514-SD
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 940, top: 30, width: 215, height: 17, "font-size": 17, }}>
<span>
EXEMPLAIRE À RENVOYER
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 67, width: 1111, height: 15, "font-size": 15, }}>
<span>
Les dispositions des articles 39 et 40 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, modifiée par la loi n° 2004-801 du 6 août 2004,
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 43, top: 84, width: 688, height: 15, "font-size": 15, }}>
<span>
garantissent les droits des personnes physiques à l’égard des traitements des données à caractère personnel.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 411, top: 120, width: 378, height: 18, "font-size": 19, }}>
<span>
DÉCLARATION D’AVIS D’ACOMPTE DE TVA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 55, top: 173, width: 1089, height: 19, "font-size": 17, }}>
<span>
1 – JE SOUHAITE M’ACQUITTER DU MONTANT DE L’ACOMPTE ATTENDU, LE MODULER OU LE SUSPENDRE ET IMPUTER LE CAS ÉCHÉANT
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 89, top: 195, width: 240, height: 16, "font-size": 17, }}>
<span>
UN ÉVENTUEL CRÉDIT DE TVA
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 243, width: 848, height: 16, "font-size": 17, }}>
<span>
01 Indiquer le montant de l’acompte attendu ou modulé ........................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 304, width: 17, height: 12, "font-size": 17, }}>
<span>
02
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 293, width: 816, height: 17, "font-size": 17, }}>
<span>
Montant du crédit ou de l’excédent de TVA, figurant sur la précédente déclaration CA12 / CA12 E, et à déduire
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 313, width: 805, height: 16, "font-size": 17, }}>
<span>
éventuellement du montant de l’acompte attendu ou modulé ................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 361, width: 848, height: 16, "font-size": 17, }}>
<span>
03 Montant total de la TVA à payer à la place de l’acompte de TVA (ligne 01 – ligne 02) ...........................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 412, width: 848, height: 16, "font-size": 17, }}>
<span>
04 Je demande la suspension de l’acompte restant à courir ....................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1006, top: 416, width: 110, height: 16, "font-size": 17, }}>
<span>
(case à cocher)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 473, width: 790, height: 17, "font-size": 17, }}>
<span>
2 – JE SOUHAITE BÉNÉFICIER D’UN REMBOURSEMENT DE CRÉDIT DE TVA SUR INVESTISSEMENTS
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 522, width: 848, height: 14, "font-size": 17, }}>
<span>
05 Montant de la TVA collectée au titre du semestre ............................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 573, width: 848, height: 13, "font-size": 17, }}>
<span>
06 Montant de la TVA déductible au titre du semestre ..........................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 624, width: 848, height: 17, "font-size": 17, }}>
<span>
07 Crédit de TVA dégagé au titre du semestre (ligne 06 – ligne 05) ..............................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 57, top: 699, width: 17, height: 13, "font-size": 17, }}>
<span>
08
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 679, width: 803, height: 16, "font-size": 17, }}>
<span>
Remboursement de la TVA sur investissements. (indiquez le montant du remboursement qui est sollicité dans
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 698, width: 684, height: 17, "font-size": 17, }}>
<span>
les conditions habituelles sur le formulaire n° 3519). Ce montant doit être supérieur à 760 euros.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 100, top: 718, width: 805, height: 16, "font-size": 17, }}>
<span>
Mon acompte attendu au titre du semestre est égal à 0 ..................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 565, top: 786, width: 68, height: 15, "font-size": 19, }}>
<span>
NOTICE
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 841, width: 863, height: 16, "font-size": 17, }}>
<span>
A.   J’estime  que  la  taxe  qui  sera  finalement  due  sur  ma  prochaine  déclaration  annuelle  de  TVA  CA12  sera 
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 861, width: 838, height: 16, "font-size": 17, }}>
<span>
supérieure d’au moins 10 % à celle qui a servi de base aux acomptes (montant calculé en ligne 57 de ma dernière
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 880, width: 839, height: 17, "font-size": 17, }}>
<span>
déclaration annuelle de régularisation CA12). Je module donc à la hausse le montant de l’acompte attendu que je
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 79, top: 900, width: 827, height: 16, "font-size": 17, }}>
<span>
porte sur la : ..............................................................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 900, width: 57, height: 16, "font-size": 17, }}>
<span>
ligne 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 55, top: 949, width: 887, height: 17, "font-size": 17, }}>
<span>
B. Je souhaite imputer le crédit de TVA dégagé sur ma précédente déclaration sur le montant de l’acompte attendu :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 984, width: 827, height: 17, "font-size": 17, }}>
<span>
1. j’indique le montant de l’acompte attendu (ou modulé selon les modalités décrites au point C) sur la : ....
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 985, width: 57, height: 16, "font-size": 17, }}>
<span>
ligne 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 1023, width: 829, height: 16, "font-size": 17, }}>
<span>
2. puis j’indique le montant du crédit de TVA disponible sur la : ............................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 1023, width: 59, height: 16, "font-size": 17, }}>
<span>
ligne 02
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 1060, width: 839, height: 16, "font-size": 17, }}>
<span>
3.   enfin,  je  m’acquitte  de  la  différence  entre  le  montant  de  l’acompte  attendu  et  le  crédit  de  TVA  disponible.
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 99, top: 1079, width: 807, height: 17, "font-size": 17, }}>
<span>
Ce montant est reporté sur la : ........................................................................ . .
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 1105, width: 821, height: 16, "font-size": 17, }}>
<span>
(Si le crédit imputable excède le montant de l’acompte, le montant sera porté à « 0 », j’impute le reliquat sur le
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 1124, width: 132, height: 16, "font-size": 17, }}>
<span>
prochain acompte)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 1080, width: 59, height: 16, "font-size": 17, }}>
<span>
ligne 03
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 54, top: 1176, width: 862, height: 16, "font-size": 17, }}>
<span>
C. Je souhaite moduler à la baisse l’acompte attendu car la taxe due à raison des opérations réalisées au cours
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 1195, width: 838, height: 17, "font-size": 17, }}>
<span>
de ce semestre, après imputation de la taxe déductible au titre des immobilisations, est inférieure d’au moins
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 80, top: 1215, width: 825, height: 16, "font-size": 17, }}>
<span>
10 % au montant de l’acompte qui m’est réclamé. J’indique le montant de l’acompte modulé sur la : ............
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 1240, width: 486, height: 16, "font-size": 17, }}>
<span>
(Le montant de l’acompte de TVA à payer est reporté sur la ligne 03)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 1215, width: 57, height: 16, "font-size": 17, }}>
<span>
ligne 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 55, top: 1300, width: 360, height: 16, "font-size": 17, }}>
<span>
D. Je souhaite suspendre un acompte de TVA :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 1325, width: 839, height: 16, "font-size": 17, }}>
<span>
– compte tenu de l’acompte déjà versé, j’estime que j’ai acquitté la TVA due au titre de l’exercice en cours en
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 94, top: 1344, width: 812, height: 17, "font-size": 17, }}>
<span>
totalité, je souhaite donc suspendre l’acompte restant à courir. J’indique « 0 » sur la : .....................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 1369, width: 521, height: 17, "font-size": 17, }}>
<span>
(Le montant de l’acompte de TVA à payer est porté à « 0 » sur la ligne 03)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 1344, width: 57, height: 17, "font-size": 17, }}>
<span>
ligne 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 1411, width: 840, height: 17, "font-size": 17, }}>
<span>
– je me serai acquitté de la totalité de la TVA due au titre de l’exercice en cours après avoir diminué le présent
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 94, top: 1431, width: 812, height: 16, "font-size": 17, }}>
<span>
acompte. J’indique le montant minoré sur la : ............................................................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 1456, width: 811, height: 16, "font-size": 17, }}>
<span>
Puis je coche la case « demande de suspension de l’acompte restant à courir » : ..........................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 1481, width: 487, height: 16, "font-size": 17, }}>
<span>
(Le montant de l’acompte de TVA à payer est reporté sur la ligne 03)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 1431, width: 57, height: 16, "font-size": 17, }}>
<span>
ligne 01
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 1456, width: 60, height: 16, "font-size": 17, }}>
<span>
ligne 04
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 55, top: 1526, width: 861, height: 17, "font-size": 17, }}>
<span>
E. Je suis en situation créditrice (montant de la TVA déductible sur immobilisations supérieur à la différence entre
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 1546, width: 708, height: 16, "font-size": 17, }}>
<span>
la TVA collectée et la TVA déductible sur les achats de biens et services autres qu’immobilisations) :
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 78, top: 1571, width: 827, height: 16, "font-size": 17, }}>
<span>
1. j’indique le montant de la TVA collectée et de la TVA déductible au titre du semestre concerné sur les : ...
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 77, top: 1596, width: 829, height: 16, "font-size": 17, }}>
<span>
2. j’indique le montant du remboursement de la TVA sur immobilisations demandé sur la : ..................
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 95, top: 1621, width: 649, height: 16, "font-size": 17, }}>
<span>
(En cas de remboursement de TVA sur immobilisations, mon acompte à payer est égal à 0)
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 987, top: 1571, width: 110, height: 16, "font-size": 17, }}>
<span>
lignes 05 et 06
<br />
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1012, top: 1596, width: 60, height: 16, "font-size": 17, }}>
<span>
ligne 08
<br />
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="HA" data-field-id="29810872" data-annot-id="28843120" maxLength="10" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="HB" data-field-id="29823560" data-annot-id="29217904" maxLength="10" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="HC" data-field-id="29826744" data-annot-id="29749008" maxLength="10" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="IA" data-field-id="29829944" data-annot-id="29750384" maxLength="10" type="text" />

            <Field component={PdfNumberInput} className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="IB" data-field-id="29832968" data-annot-id="29750576" maxLength="10" type="text" />

            <Field disabled component={PdfNumberInput} className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="IC" data-field-id="29836136" data-annot-id="29750768" maxLength="10" type="text" />

            <Field component={PdfNumberInput} min={760} className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="ID" data-field-id="29839352" data-annot-id="29755200" maxLength="10" type="text" />

            <Field component={PdfCheckbox} onCustomChange={change} className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="DE" data-field-id="29842472" data-annot-id="29755392" value="Yes" type="checkbox" data-default-value="Off" />

          </div>
        </div>

      </div>
    </div>
  );
};

export default CA12AC3514;
