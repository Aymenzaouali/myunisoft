/* eslint-disable */
import React from "react";
import { Field } from 'redux-form';
import PdfNumberInput from 'components/reduxForm/Inputs/PdfNumberInput';
import { ReduxPicker, AutoCompleteCell } from 'components/reduxForm/Inputs';
import './style.scss';

const Decloyer = (props) => {
  const { decloyerForm, change, CAList } = props;

  return (
    <div className="decloyer-form">
      <div data-type="pdf-page" id="pdf-page-0" data-page-num="0" data-ratio="0.706667" className="pdf-page ">
        <div data-type="pdf-page-inner" data-page-width="1200" className="pdf-page-inner pdf-page-0 ">
          <div data-type="pdf-page-text" className="pdf-page-text">
            <div className="pdf-obj-fixed" style={{ left: 240, top: 249, width: 92, height: 11, 'font-size': 14, }}>
<span>
Adresse du local
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 380, top: 249, width: 112, height: 14, 'font-size': 14, }}>
<span>
Propriétaire du local
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 489, top: 183, width: 192, height: 16, 'font-size': 17, }}>
<span>
Caracteristiques des locaux
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1062, top: 81, width: 63, height: 9, 'font-size': 11, }}>
<span>
DECLOYER
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 836, top: 145, width: 202, height: 13, 'font-size': 14, }}>
<span>
Déclaration des loyers au 1er janvier
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 906, top: 162, width: 61, height: 11, 'font-size': 14, }}>
<span>
de l&#39;année :
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 370, top: 93, width: 391, height: 13, 'font-size': 17, }}>
<span>
DECLARATIONS DES LOYERS PROFESSIONNELS
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 395, top: 114, width: 345, height: 16, 'font-size': 17, }}>
<span>
Mise à jour permanente des loyers professionnels
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 409, top: 210, width: 39, height: 12, 'font-size': 17, }}>
<span>
Local
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 902, top: 209, width: 169, height: 16, 'font-size': 17, }}>
<span>
Mise à jour permanente
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 37, top: 241, width: 73, height: 10, 'font-size': 14, }}>
<span>
Référence du
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 60, top: 258, width: 27, height: 11, 'font-size': 14, }}>
<span>
local
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 131, top: 241, width: 66, height: 10, 'font-size': 14, }}>
<span>
Invariant du
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 151, top: 258, width: 27, height: 11, 'font-size': 14, }}>
<span>
local
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 516, top: 241, width: 138, height: 10, 'font-size': 14, }}>
<span>
Precision sur l&#39;adresse du
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 572, top: 258, width: 27, height: 11, 'font-size': 14, }}>
<span>
local
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 664, top: 249, width: 164, height: 14, 'font-size': 14, }}>
<span>
Nouveau propriétaire du local
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 834, top: 241, width: 207, height: 13, 'font-size': 14, }}>
<span>
Mode d&#39;occupation Montant du loyer
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 855, top: 258, width: 63, height: 13, 'font-size': 14, }}>
<span>
du local (*)
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 971, top: 258, width: 44, height: 11, 'font-size': 14, }}>
<span>
du local
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1062, top: 232, width: 62, height: 11, 'font-size': 14, }}>
<span>
Date de fin
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1050, top: 250, width: 87, height: 13, 'font-size': 14, }}>
<span>
d&#39;occupation du
<br/>
</span>
            </div>
            <div className="pdf-obj-fixed" style={{ left: 1080, top: 267, width: 27, height: 10, 'font-size': 14, }}>
<span>
local
<br/>
</span>
            </div>
          </div>

          <div data-type="pdf-page-annots" className="pdf-page-annots">
            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_1 pdf-obj-fixed acroform-field " name="CD"
                   data-field-id="36925768" data-annot-id="36926624" disabled type="text"/>

            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_2 pdf-obj-fixed acroform-field " name="BA1"
                   data-field-id="35989496" data-annot-id="36926816" disabled/>


            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_3 pdf-obj-fixed acroform-field " name="BB1"
                   data-field-id="37006792" data-annot-id="36927008" disabled type="text"/>

            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_4 pdf-obj-fixed acroform-field " name="BA2"
                   data-field-id="37039256" data-annot-id="36927200" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_5 pdf-obj-fixed acroform-field " name="BA3"
                   data-field-id="37080728" data-annot-id="36927392" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_6 pdf-obj-fixed acroform-field " name="BA4"
                   data-field-id="37080872" data-annot-id="36927584" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_7 pdf-obj-fixed acroform-field " name="BA5"
                   data-field-id="37081016" data-annot-id="36927776" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_8 pdf-obj-fixed acroform-field " name="BA6"
                   data-field-id="37081160" data-annot-id="36927968" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_9 pdf-obj-fixed acroform-field " name="BA7"
                   data-field-id="37081304" data-annot-id="36928160" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_10 pdf-obj-fixed acroform-field " name="BA8"
                   data-field-id="37081656" data-annot-id="36928352" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_11 pdf-obj-fixed acroform-field " name="BA9"
                   data-field-id="37081992" data-annot-id="36928544" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_12 pdf-obj-fixed acroform-field " name="BA10"
                   data-field-id="37082328" data-annot-id="36928736" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_13 pdf-obj-fixed acroform-field " name="BA11"
                   data-field-id="37082664" data-annot-id="36928928" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_14 pdf-obj-fixed acroform-field " name="BA12"
                   data-field-id="37083000" data-annot-id="36929120" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_15 pdf-obj-fixed acroform-field " name="BA13"
                   data-field-id="37083336" data-annot-id="36929312" disabled/>


            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_16 pdf-obj-fixed acroform-field "
                   name="BB2"
                   data-field-id="37083672" data-annot-id="36929504" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_17 pdf-obj-fixed acroform-field "
                   name="BB3"
                   data-field-id="37084008" data-annot-id="36929696" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_18 pdf-obj-fixed acroform-field "
                   name="BB4"
                   data-field-id="37084488" data-annot-id="36930160" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_19 pdf-obj-fixed acroform-field "
                   name="BB5"
                   data-field-id="37084824" data-annot-id="36930352" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_20 pdf-obj-fixed acroform-field "
                   name="BB6"
                   data-field-id="37085160" data-annot-id="36930544" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_21 pdf-obj-fixed acroform-field "
                   name="BB7"
                   data-field-id="37085496" data-annot-id="36930736" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_22 pdf-obj-fixed acroform-field "
                   name="BB8"
                   data-field-id="37085832" data-annot-id="36930928" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_23 pdf-obj-fixed acroform-field "
                   name="BB9"
                   data-field-id="37086168" data-annot-id="36931120" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_24 pdf-obj-fixed acroform-field "
                   name="BB10"
                   data-field-id="37086504" data-annot-id="36931312" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_25 pdf-obj-fixed acroform-field "
                   name="BB11"
                   data-field-id="37086840" data-annot-id="36931504" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_26 pdf-obj-fixed acroform-field "
                   name="BB13"
                   data-field-id="37087176" data-annot-id="36931696" disabled type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_27 pdf-obj-fixed acroform-field "
                   name="BB12"
                   data-field-id="37087512" data-annot-id="36931888" disabled type="text"/>

            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_28 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL1"
                   data-field-id="37088184" data-annot-id="36932080" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_29 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL2"
                   data-field-id="37087848" data-annot-id="36932272" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_30 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL3"
                   data-field-id="37088520" data-annot-id="36932464" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_31 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL4"
                   data-field-id="37088856" data-annot-id="36932656" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_32 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL5"
                   data-field-id="37089192" data-annot-id="36932848" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_33 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL6"
                   data-field-id="37089528" data-annot-id="36933040" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_34 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL7"
                   data-field-id="37084344" data-annot-id="36929888" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_35 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL8"
                   data-field-id="37090456" data-annot-id="36933760" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_36 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL9"
                   data-field-id="37090792" data-annot-id="36933952" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_37 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL10"
                   data-field-id="37091128" data-annot-id="36934144" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_38 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL11"
                   data-field-id="37091464" data-annot-id="36934336" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_39 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL12"
                   data-field-id="37091800" data-annot-id="36934528" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_40 pdf-obj-fixed acroform-field " name="ADRESS_LOCAL13"
                   data-field-id="37092136" data-annot-id="36934720" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_41 pdf-obj-fixed acroform-field " name="PROP_LOCAL1"
                   data-field-id="37092472" data-annot-id="36934912" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_42 pdf-obj-fixed acroform-field " name="PROP_LOCAL2"
                   data-field-id="37092808" data-annot-id="36935104" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_43 pdf-obj-fixed acroform-field " name="PROP_LOCAL3"
                   data-field-id="37093144" data-annot-id="36935296" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_44 pdf-obj-fixed acroform-field " name="PROP_LOCAL4"
                   data-field-id="37093480" data-annot-id="36935488" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_45 pdf-obj-fixed acroform-field " name="PROP_LOCAL5"
                   data-field-id="37093816" data-annot-id="36935680" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_46 pdf-obj-fixed acroform-field " name="PROP_LOCAL6"
                   data-field-id="37094152" data-annot-id="36935872" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_47 pdf-obj-fixed acroform-field " name="PROP_LOCAL7"
                   data-field-id="37094488" data-annot-id="36936064" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_48 pdf-obj-fixed acroform-field " name="PROP_LOCAL8"
                   data-field-id="37094824" data-annot-id="36936256" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_49 pdf-obj-fixed acroform-field " name="PROP_LOCAL9"
                   data-field-id="37095160" data-annot-id="36936448" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_50 pdf-obj-fixed acroform-field " name="PROP_LOCAL10"
                   data-field-id="37095496" data-annot-id="36936640" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_51 pdf-obj-fixed acroform-field " name="PROP_LOCAL11"
                   data-field-id="37095832" data-annot-id="36936832" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_52 pdf-obj-fixed acroform-field " name="PROP_LOCAL12"
                   data-field-id="37096168" data-annot-id="36937024" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_53 pdf-obj-fixed acroform-field " name="PROP_LOCAL13"
                   data-field-id="37096504" data-annot-id="36937216" disabled/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_54 pdf-obj-fixed acroform-field " name="CE1"
                   data-field-id="37096840" data-annot-id="36937408"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_55 pdf-obj-fixed acroform-field " name="CE2"
                   data-field-id="37097176" data-annot-id="36937600"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_56 pdf-obj-fixed acroform-field " name="CE3"
                   data-field-id="37097512" data-annot-id="36937792"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_57 pdf-obj-fixed acroform-field " name="CE4"
                   data-field-id="37097848" data-annot-id="36937984"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_58 pdf-obj-fixed acroform-field " name="CE5"
                   data-field-id="37098184" data-annot-id="36938176"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_59 pdf-obj-fixed acroform-field " name="CE6"
                   data-field-id="37098520" data-annot-id="36938368"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_60 pdf-obj-fixed acroform-field " name="CE7"
                   data-field-id="37098856" data-annot-id="36938560"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_61 pdf-obj-fixed acroform-field " name="CE8"
                   data-field-id="37099192" data-annot-id="36938752"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_62 pdf-obj-fixed acroform-field " name="CE9"
                   data-field-id="37099528" data-annot-id="36938944"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_63 pdf-obj-fixed acroform-field " name="CE10"
                   data-field-id="37099864" data-annot-id="36939136"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_64 pdf-obj-fixed acroform-field " name="CE11"
                   data-field-id="37100200" data-annot-id="36939328"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_65 pdf-obj-fixed acroform-field " name="CE12"
                   data-field-id="37100536" data-annot-id="36939520"/>


            <Field component="textarea" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_66 pdf-obj-fixed acroform-field " name="CE13"
                   data-field-id="37089736" data-annot-id="36933232"/>


            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_67 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA1"
                   data-field-id="37090072" data-annot-id="36933424" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_68 pdf-obj-fixed acroform-field "
                   name="CB1"
                   data-field-id="37101848" data-annot-id="36940752" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_69 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA2"
                   data-field-id="37102152" data-annot-id="36940944" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_70 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA3"
                   data-field-id="37102488" data-annot-id="36941136" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_71 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA4"
                   data-field-id="37102824" data-annot-id="36941328" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_72 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA5"
                   data-field-id="37103160" data-annot-id="36941520" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_73 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA6"
                   data-field-id="37103496" data-annot-id="36941712" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_74 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA7"
                   data-field-id="37103832" data-annot-id="36941904" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_75 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA8"
                   data-field-id="37104168" data-annot-id="36942096" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_76 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA9"
                   data-field-id="37104504" data-annot-id="36942288" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_77 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA10"
                   data-field-id="37104840" data-annot-id="36942480" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_78 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA11"
                   data-field-id="37105176" data-annot-id="36942672" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_79 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA12"
                   data-field-id="37105512" data-annot-id="36942864" type="text"/>

            <Field component={AutoCompleteCell} options={CAList} autoComplete="nope"
                   placeholder='' menuPosition="absolute" border
                   className="pde-form-field pdf-annot obj_80 pdf-obj-fixed acroform-field auto-complete-field"
                   name="CA13"
                   data-field-id="37105848" data-annot-id="36943056" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_81 pdf-obj-fixed acroform-field "
                   name="CB2"
                   data-field-id="37106184" data-annot-id="36943248" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_82 pdf-obj-fixed acroform-field "
                   name="CB3"
                   data-field-id="37106520" data-annot-id="36943440" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_83 pdf-obj-fixed acroform-field "
                   name="CB4"
                   data-field-id="37106856" data-annot-id="36943632" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_84 pdf-obj-fixed acroform-field "
                   name="CB5"
                   data-field-id="37107192" data-annot-id="36943824" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_85 pdf-obj-fixed acroform-field "
                   name="CB6"
                   data-field-id="37107528" data-annot-id="36944016" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_86 pdf-obj-fixed acroform-field "
                   name="CB7"
                   data-field-id="37107864" data-annot-id="36944208" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_87 pdf-obj-fixed acroform-field "
                   name="CB8"
                   data-field-id="37108200" data-annot-id="36944400" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_88 pdf-obj-fixed acroform-field "
                   name="CB9"
                   data-field-id="37108536" data-annot-id="36944592" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_89 pdf-obj-fixed acroform-field "
                   name="CB10"
                   data-field-id="37108872" data-annot-id="36944784" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_90 pdf-obj-fixed acroform-field "
                   name="CB11"
                   data-field-id="37109208" data-annot-id="36944976" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_91 pdf-obj-fixed acroform-field "
                   name="CB12"
                   data-field-id="37109544" data-annot-id="36945168" type="text"/>

            <Field component={PdfNumberInput}
                   className="pde-form-field pdf-annot obj_92 pdf-obj-fixed acroform-field "
                   name="CB13"
                   data-field-id="37109880" data-annot-id="36945360" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_93 pdf-obj-fixed acroform-field "
                   name="CC1"
                   data-field-id="37110216" data-annot-id="36945552" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_94 pdf-obj-fixed acroform-field "
                   name="CC2"
                   data-field-id="37110552" data-annot-id="36945744" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_95 pdf-obj-fixed acroform-field "
                   name="CC3"
                   data-field-id="37110888" data-annot-id="36945936" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_96 pdf-obj-fixed acroform-field "
                   name="CC4"
                   data-field-id="37111224" data-annot-id="36946128" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_97 pdf-obj-fixed acroform-field "
                   name="CC5"
                   data-field-id="37111560" data-annot-id="36946320" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_98 pdf-obj-fixed acroform-field "
                   name="CC6"
                   data-field-id="37111896" data-annot-id="36946512" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_99 pdf-obj-fixed acroform-field "
                   name="CC7"
                   data-field-id="37112232" data-annot-id="36946704" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_100 pdf-obj-fixed acroform-field "
                   name="CC8"
                   data-field-id="37112568" data-annot-id="36946896" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_101 pdf-obj-fixed acroform-field "
                   name="CC9"
                   data-field-id="37112904" data-annot-id="36947088" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_102 pdf-obj-fixed acroform-field "
                   name="CC10" data-field-id="37113240" data-annot-id="36947280" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_103 pdf-obj-fixed acroform-field "
                   name="CC11" data-field-id="37113576" data-annot-id="36947472" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_104 pdf-obj-fixed acroform-field "
                   name="CC12" data-field-id="37113912" data-annot-id="36947664" type="text"/>

            <Field component={ReduxPicker} handleChange={change}
                   className="pde-form-field pdf-annot obj_105 pdf-obj-fixed acroform-field "
                   name="CC13" data-field-id="37114248" data-annot-id="36947856" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_106 pdf-obj-fixed acroform-field "
                   name="DBA1" data-field-id="37116328" data-annot-id="36948048" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_107 pdf-obj-fixed acroform-field "
                   name="DBB1" data-field-id="37116760" data-annot-id="36948240" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_108 pdf-obj-fixed acroform-field "
                   name="DBT1" data-field-id="37117912" data-annot-id="36948432" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_109 pdf-obj-fixed acroform-field "
                   name="DBK1" data-field-id="37119320" data-annot-id="36948624" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_110 pdf-obj-fixed acroform-field "
                   name="DBA2" data-field-id="37119624" data-annot-id="36948816" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_111 pdf-obj-fixed acroform-field "
                   name="DBB2" data-field-id="37120728" data-annot-id="36949008" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_112 pdf-obj-fixed acroform-field "
                   name="DBT2" data-field-id="37123464" data-annot-id="36949200" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_113 pdf-obj-fixed acroform-field "
                   name="DBK2" data-field-id="37123800" data-annot-id="36949392" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_114 pdf-obj-fixed acroform-field "
                   name="DBT3" data-field-id="37121832" data-annot-id="36949584" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_115 pdf-obj-fixed acroform-field "
                   name="DBA3" data-field-id="37121064" data-annot-id="36949776" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_116 pdf-obj-fixed acroform-field "
                   name="DBB3" data-field-id="37121400" data-annot-id="36949968" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_117 pdf-obj-fixed acroform-field "
                   name="DBK3" data-field-id="37122136" data-annot-id="36950160" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_118 pdf-obj-fixed acroform-field "
                   name="DBT4" data-field-id="37124808" data-annot-id="36950352" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_119 pdf-obj-fixed acroform-field "
                   name="DBA4" data-field-id="37124136" data-annot-id="36950544" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_120 pdf-obj-fixed acroform-field "
                   name="DBB4" data-field-id="37124472" data-annot-id="36950736" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_121 pdf-obj-fixed acroform-field "
                   name="DBK4" data-field-id="37125144" data-annot-id="36950928" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_122 pdf-obj-fixed acroform-field "
                   name="DBT5" data-field-id="37126488" data-annot-id="36951120" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_123 pdf-obj-fixed acroform-field "
                   name="DBA5" data-field-id="37125480" data-annot-id="36951312" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_124 pdf-obj-fixed acroform-field "
                   name="DBB5" data-field-id="37126152" data-annot-id="36951504" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_125 pdf-obj-fixed acroform-field "
                   name="DBK5" data-field-id="37125816" data-annot-id="36951696" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_126 pdf-obj-fixed acroform-field "
                   name="DBT6" data-field-id="37127496" data-annot-id="36951888" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_127 pdf-obj-fixed acroform-field "
                   name="DBA6" data-field-id="37126824" data-annot-id="36952080" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_128 pdf-obj-fixed acroform-field "
                   name="DBB6" data-field-id="37127160" data-annot-id="36952272" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_129 pdf-obj-fixed acroform-field "
                   name="DBK6" data-field-id="37127832" data-annot-id="36952464" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_130 pdf-obj-fixed acroform-field "
                   name="DBT7" data-field-id="37101384" data-annot-id="36939712" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_131 pdf-obj-fixed acroform-field "
                   name="DBA7" data-field-id="37100744" data-annot-id="36939904" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_132 pdf-obj-fixed acroform-field "
                   name="DBB7" data-field-id="37101048" data-annot-id="36940096" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_133 pdf-obj-fixed acroform-field "
                   name="DBK7" data-field-id="37130104" data-annot-id="36940288" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_134 pdf-obj-fixed acroform-field "
                   name="DBT8" data-field-id="37133816" data-annot-id="36940480" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_135 pdf-obj-fixed acroform-field "
                   name="DBA8" data-field-id="37130664" data-annot-id="36954720" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_136 pdf-obj-fixed acroform-field "
                   name="DBB8" data-field-id="37132264" data-annot-id="36954912" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_137 pdf-obj-fixed acroform-field "
                   name="DBK8" data-field-id="37136616" data-annot-id="36955104" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_138 pdf-obj-fixed acroform-field "
                   name="DBT8" data-field-id="37133816" data-annot-id="36955296" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_139 pdf-obj-fixed acroform-field "
                   name="DBA8" data-field-id="37130664" data-annot-id="36955488" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_140 pdf-obj-fixed acroform-field "
                   name="DBB8" data-field-id="37132264" data-annot-id="36955680" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_141 pdf-obj-fixed acroform-field "
                   name="DBK8" data-field-id="37136616" data-annot-id="36955872" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_142 pdf-obj-fixed acroform-field "
                   name="DBT9" data-field-id="37137624" data-annot-id="36956064" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_143 pdf-obj-fixed acroform-field "
                   name="DBA9" data-field-id="37136984" data-annot-id="36956256" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_144 pdf-obj-fixed acroform-field "
                   name="DBB9" data-field-id="37137288" data-annot-id="36956448" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_145 pdf-obj-fixed acroform-field "
                   name="DBK9" data-field-id="37137960" data-annot-id="36956640" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_146 pdf-obj-fixed acroform-field "
                   name="DBT10" data-field-id="37138968" data-annot-id="36956832" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_147 pdf-obj-fixed acroform-field "
                   name="DBA10" data-field-id="37138296" data-annot-id="36957024" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_148 pdf-obj-fixed acroform-field "
                   name="DBB10" data-field-id="37138632" data-annot-id="36957216" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_149 pdf-obj-fixed acroform-field "
                   name="DBK10" data-field-id="37139304" data-annot-id="36957408" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_150 pdf-obj-fixed acroform-field "
                   name="DBT11" data-field-id="37140312" data-annot-id="36957600" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_151 pdf-obj-fixed acroform-field "
                   name="DBA11" data-field-id="37139640" data-annot-id="36957792" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_152 pdf-obj-fixed acroform-field "
                   name="DBB11" data-field-id="37139976" data-annot-id="36957984" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_153 pdf-obj-fixed acroform-field "
                   name="DBK11" data-field-id="37140648" data-annot-id="36958176" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_154 pdf-obj-fixed acroform-field "
                   name="DBT12" data-field-id="37141656" data-annot-id="36958368" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_155 pdf-obj-fixed acroform-field "
                   name="DBA12" data-field-id="37140984" data-annot-id="36958560" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_156 pdf-obj-fixed acroform-field "
                   name="DBB12" data-field-id="37141320" data-annot-id="36958752" type="text"/>

            <Field component="input" autoComplete="nope"
                   className="pde-form-field pdf-annot obj_157 pdf-obj-fixed acroform-field "
                   name="DBK12" data-field-id="37141992" data-annot-id="36958944" type="text"/>

          </div>
        </div>
      </div>
    </div>
  );
};

export default Decloyer;
