import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { Typography } from '@material-ui/core';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import ReduxRadio from 'components/reduxForm/Selections/ReduxRadio';
import UserDialog from 'containers/groups/Dialogs/User';
import I18n from 'assets/I18n';
import { lowercaseFormat, phoneFormat } from 'helpers/format';
import './styles.scss';
import HelperInfo from 'components/basics/HelperInfo';

const NewUser = (props) => {
  const {
    genders,
    civility_code,
    error,
    mailExist,
    isOpen,
    list,
    onCheck,
    closeUserDialog,
    warningMessage,
    selectedUser
  } = props;

  return (
    <div className="new-user">
      <Typography variant="h6">{I18n.t('newUserForm.userCreation.title')}</Typography>
      {error && <Typography variant="h6" color="error">{error}</Typography>}
      <div className="new-user__form">
        <Field
          color="primary"
          name="civility_code"
          component={ReduxRadio}
          list={genders}
          className="new-user__form-radios"
          row
        />
        <Field
          name="name"
          component={ReduxTextField}
          type="text"
          label={I18n.t('newUserForm.userCreation.input.lastName')}
          className="new-user__form-field"
        />
        {civility_code === 'Mrs'
            && (
              <Field
                name="maiden_name"
                component={ReduxTextField}
                type="text"
                label={I18n.t('newUserForm.userCreation.input.maidenName')}
                className="new-user__form-field"
              />
            )
        }
        <Field
          name="firstname"
          component={ReduxTextField}
          type="text"
          label={I18n.t('newUserForm.userCreation.input.firstName')}
          className="new-user__form-field"
        />
        <Field
          format={lowercaseFormat}
          name="mail"
          component={ReduxTextField}
          type="text"
          label={I18n.t('newUserForm.userCreation.input.mail')}
          className="new-user__form-field"
          onCustomBlur={onCheck}
        />
        <HelperInfo
          className="new-user__form-warning"
          isVisible={mailExist && selectedUser}
          infoMessages={warningMessage}
        />
        <Field
          name="tel_fix"
          component={ReduxTextField}
          type="text"
          label={I18n.t('newUserForm.userCreation.input.fixedPhone')}
          className="new-user__form-field"
          format={phoneFormat}
        />
        <Field
          name="tel_portable"
          component={ReduxTextField}
          type="text"
          label={I18n.t('newUserForm.userCreation.input.cellPhone')}
          className="new-user__form-field"
          format={phoneFormat}
        />
      </div>
      {isOpen && (
        <UserDialog
          isOpen={isOpen}
          list={list}
          onClose={closeUserDialog}
        />
      )}
    </div>
  );
};

NewUser.defaultProps = {
  selectedUser: {},
  mailExist: null,
  isOpen: false,
  list: [],
  onCheck: () => {},
  closeUserDialog: () => {},
  error: ''
};

NewUser.propTypes = {
  selectedUser: PropTypes.shape({
    user_id: PropTypes.number,
    civility_id: PropTypes.number,
    civility: PropTypes.string,
    name: PropTypes.string,
    maiden_name: PropTypes.string,
    firstname: PropTypes.string,
    mail: PropTypes.shape({}),
    tel_fix: PropTypes.shape({
      id: PropTypes.number,
      coordonnee: PropTypes.string
    }),
    tel_portable: PropTypes.shape({
      id: PropTypes.number,
      coordonnee: PropTypes.string
    }),
    access_list: PropTypes.arrayOf(PropTypes.shape({}))
  }),
  genders: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    value: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string
  })).isRequired,
  warningMessage: PropTypes.arrayOf(PropTypes.shape({
    message: PropTypes.string,
    warning: PropTypes.string
  })).isRequired,
  civility_code: PropTypes.string.isRequired,
  error: PropTypes.string,
  mailExist: PropTypes.number,
  isOpen: PropTypes.bool,
  list: PropTypes.array,
  onCheck: PropTypes.func,
  closeUserDialog: PropTypes.func
};

export default NewUser;
