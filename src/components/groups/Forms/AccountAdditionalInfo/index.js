import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import I18n from 'assets/I18n';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import { RoadTypesAutoComplete, PostalCodeTextField, CitiesAutoComplete } from 'containers/reduxForm/Inputs';
import { Field } from 'redux-form';
import IBAN from 'components/groups/Forms/IBAN';
import styles from './AccountAdditionalInfo.module.scss';

const materialStyles = () => ({
  resp: {
    width: 200,
    marginRight: 20
  },
  SIRET: {
    width: 140
  },
  addressNumber: {
    width: 40,
    marginRight: 20
  },
  Comp: {
    width: 60,
    marginRight: 20
  },
  streetType: {
    width: 150,
    marginRight: 20,
    marginBottom: 0
  },
  streetTypeLabel: {
    fontSize: 14,
    position: 'relative',
    marginBottom: -17
  },
  streetName: {
    width: 300,
    marginRight: 20
  },
  addressCompl: {
    width: 300
  },
  CP: {
    width: 80,
    marginRight: 20
  },
  city: {
    width: 300
  },
  tel: {
    width: 100,
    marginRight: 20
  },
  email: {
    width: 300
  },
  comment: {
    width: 380
  },
  sectionTitle: {
    margin: 0,
    marginTop: 16
  }
});

const AccountAdditionalInfo = (props) => {
  const {
    classes
  } = props;


  return (
    <div className={styles.accountDialog}>
      <div className={styles.blockInline}>
        <Field
          color="primary"
          className={classes.resp}
          name="infoC.person_in_charge"
          component={ReduxTextField}
          label={I18n.t('account.newAccount.resp')}
        />
        <Field
          color="primary"
          className={classes.SIRET}
          name="infoC.SIRET"
          component={ReduxTextField}
          label={I18n.t('account.newAccount.SIRET')}
        />
      </div>
      <p className={classes.sectionTitle}>{I18n.t('account.newAccount.address.label')}</p>
      <div className={styles.blockInline}>
        <Field
          color="primary"
          className={classes.addressNumber}
          name="infoC.address.N"
          component={ReduxTextField}
          label={I18n.t('account.newAccount.address.N')}
        />
        <Field
          color="primary"
          className={classes.Comp}
          name="infoC.address.comp"
          component={ReduxTextField}
          label={I18n.t('account.newAccount.address.comp')}
        />
        <div className={styles.street}>
          <Field
            color="primary"
            className={classes.streetType}
            name="infoC.address.streetType"
            placeholder=""
            component={RoadTypesAutoComplete}
            menuPosition="fixed"
            textFieldProps={{
              label: I18n.t('account.newAccount.address.streetType'),
              InputLabelProps: {
                classes: { root: classes.streetTypeLabel }
              }
            }}
          />
        </div>
        <Field
          color="primary"
          className={classes.streetName}
          name="infoC.address.streetName"
          component={ReduxTextField}
          label={I18n.t('account.newAccount.address.streetName')}
        />
      </div>
      <Field
        color="primary"
        className={classes.addressCompl}
        name="infoC.address.addressCompl"
        component={ReduxTextField}
        label={I18n.t('account.newAccount.address.addressCompl')}
      />
      <div className={styles.blockInline}>
        <Field
          color="primary"
          className={classes.CP}
          name="infoC.address.CP"
          component={PostalCodeTextField}
          label={I18n.t('account.newAccount.address.CP')}
        />
        <div className={styles.city}>
          <Field
            color="primary"
            className={classes.city}
            name="infoC.address.city"
            component={CitiesAutoComplete}
            menuPosition="fixed"
            placeholder={I18n.t('account.newAccount.address.city')}
          />
        </div>
      </div>
      <div className={styles.blockInline}>
        <Field
          color="primary"
          className={classes.tel}
          name="infoC.address.tel"
          component={ReduxTextField}
          label={I18n.t('account.newAccount.address.tel')}
        />
        <Field
          color="primary"
          className={classes.email}
          name="infoC.address.email"
          component={ReduxTextField}
          label={I18n.t('account.newAccount.address.email')}
        />
      </div>
      <Field
        color="primary"
        className={classes.comment}
        name="infoC.address.comment"
        component={ReduxTextField}
        placeholder={I18n.t('account.newAccount.address.comment')}
      />
      <IBAN />
    </div>
  );
};

AccountAdditionalInfo.propTypes = {
  classes: PropTypes.shape({})
};

AccountAdditionalInfo.defaultProps = ({
  classes: {}
});

export default withStyles(materialStyles)(AccountAdditionalInfo);
