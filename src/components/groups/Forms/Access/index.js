import React from 'react';
import I18n from 'assets/I18n';
import {
  Field,
  Form
} from 'redux-form';
import {
  ReduxSecureTextField,
  ReduxTextField
} from 'components/reduxForm/Inputs';

import styles from 'components/groups/CompanyCreation/CompanyCreation.module.scss';

class Access extends React.PureComponent {
  render() {
    return (
      <Form>
        <p>{I18n.t('companyCreation.accessInformationTitle')}</p>
        <div className={styles.row}>
          <Field
            name="password"
            id="password"
            component={ReduxSecureTextField}
            label={I18n.t('companyCreation.password')}
            margin="none"
          />
        </div>
        <div className={styles.row}>
          <Field
            name="confirmation"
            id="confirmation"
            component={ReduxSecureTextField}
            label={I18n.t('companyCreation.confirmation')}
            margin="none"
          />
        </div>
        <div className={styles.row}>
          <Field
            name="comment"
            id="comment"
            multiline
            component={ReduxTextField}
            label={I18n.t('companyCreation.comment')}
            margin="none"
          />
        </div>
      </Form>
    );
  }
}

export default Access;
