import React from 'react';
import { Field, Form } from 'redux-form';
import I18n from 'assets/I18n';
import { withStyles } from '@material-ui/core';
import ReduxTextField from 'components/reduxForm/Inputs/ReduxTextField';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './accountingFirmSilae.scss';

const AccountingFirmSilae = ({
  classes, pw_silae_secured, pw_ws_silae_secured, isCreateNew
}) => (
  <Form>
    <div className={styles.row}>
      <Field
        autocomplete="false"
        color="primary"
        name="loginSilae"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.loginSilae')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={styles.error}
        margin="none"
      />
    </div>
    <div className={styles.row}>
      <Field
        autocomplete="false"
        color="primary"
        name="passwordSilaeConfirm"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.passwordSilae')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={styles.error}
        margin="none"
        {...(!isCreateNew && pw_silae_secured === true && { placeholder: '*******' })}
        shrink={pw_silae_secured}
      />
    </div>
    <div className={styles.row}>
      <Field
        color="primary"
        name="loginWsSilae"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.loginWsSilae')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={styles.error}
        margin="none"
      />
    </div>
    <div className={styles.row}>
      <Field
        color="primary"
        name="passwordSilaeConfirmed"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.passwordWSSilae')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={styles.error}
        margin="none"
        {...(!isCreateNew && pw_ws_silae_secured && { placeholder: '*******' })}
        shrink={pw_ws_silae_secured}
      />
    </div>
    <div className={styles.row}>
      <Field
        color="primary"
        name="addressWsSilae"
        component={ReduxTextField}
        label={I18n.t('accountingFirmSettings.forms.addressWsSilae')}
        className={classNames(classes.baseField, classes.baseFieldStyle)}
        errorStyle={styles.error}
        margin="none"
      />
    </div>

  </Form>
);
const themeStyle = () => ({
  siret: {
    width: 120
  },
  baseFieldStyle: {
    marginRight: 20,
    marginBottom: 30
  },
  baseField: {
    width: 300
  },
  postal: {
    width: 80
  },
  number: {
    width: 40
  },
  comp: {
    width: 60
  },
  chanelType: {
    width: 100
  }
});

AccountingFirmSilae.propTypes = {
  pw_silae_secured: PropTypes.bool.isRequired,
  pw_ws_silae_secured: PropTypes.bool.isRequired,
  isCreateNew: PropTypes.bool.isRequired
};

export default withStyles(themeStyle)(AccountingFirmSilae);
