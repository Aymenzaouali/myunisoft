import React, { useContext } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import _ from 'lodash';

import { buildSlaveRoute } from 'helpers/routes';
import { useWindowState } from 'helpers/hooks';

import OpenerContext from 'context/OpenerContext';
import SplitContext, { splitDefaults } from 'context/SplitContext';

import { SPLIT_INACTIVE } from 'redux/tabs/split/constants';

import Window from 'components/basics/Window';
import WindowTitle from 'components/basics/WindowTitle';

import styles from './split.module.scss';

// Components
const Split = (props) => {
  const {
    active, swapped, popup, popuped, args,
    tab_id, tab_label, master_id, master_args, children,
    slaves,
    push,
    onActivateSplit,
    onSwapSplit: _onSwapSplit, updateArgsSplit,
    openPopupSplit, closePopupSplit,
    onCloseSplit,
    closeSlave
  } = props;


  // Contexts
  const { focusOpener } = useContext(OpenerContext);

  // State
  const win = useWindowState({
    open: popup,

    onOpen: () => {},
    onClose: closePopupSplit
  });

  // Manage split init message
  const onLoaded = () => {
    win.send({
      target: 'split-slave',

      active,
      swapped,
      args
    });
  };

  const windowProps = {
    key: '__window',
    window_id: `${tab_id}_split_${master_id}`,
    path: buildSlaveRoute(window.location.pathname),

    onLoaded,

    ...win.props
  };

  // Inactive
  if (active === SPLIT_INACTIVE || _.isEmpty(slaves) || slaves[active] === undefined) {
    return (
      <div className={styles.spaced}>
        <SplitContext.Provider
          key={master_id}
          value={{
            ...splitDefaults,
            ...props,

            master_id,

            slaves: _.map(slaves, (slave, id) => ({
              id,
              label: slave.label,
              args: slave.args || {}
            })),

            onActivateSplit
          }}
        >
          { children }
        </SplitContext.Provider>
        <Window {...windowProps} open={false} />
      </div>
    );
  }

  // Active
  const slave = slaves[active];
  const swappable = _.get(slave, 'swappable', true);
  const popupable = _.get(slave, 'popupable', true);
  const swapKeepArgs = _.get(slave, 'swapKeepArgs', false);

  const onPopupSplit = () => {
    if (!popupable) return;

    openPopupSplit();
  };

  const onSwapSplit = () => {
    if (!swappable) return;

    _onSwapSplit({
      ...(swapKeepArgs ? args : {}), // apply previous args
      ...(swapped ? slave.args : master_args) // apply new args
    });
  };

  const focusOther = () => {
    if (popup) {
      win.focus();
    } else if (popuped) {
      focusOpener();
    }
  };

  // Rendering
  const renderMaster = (compo, key, onCloseSplit) => (
    <SplitContext.Provider
      key={key}
      value={{
        ...splitDefaults,
        ...props,

        master_id: swapped ? active : master_id,
        master: true,
        active: true,
        popuped: popup || popuped,
        popupable,
        swappable,
        swapped,
        args,

        onSwapSplit,
        onPopupSplit,
        focusOther,
        updateArgsSplit,
        onCloseSplit,
        closeSlave
      }}
    >
      { compo }
    </SplitContext.Provider>
  );

  const renderSlave = (compo, key, onCloseSplit) => (
    <SplitContext.Provider
      key={key}
      value={{
        ...splitDefaults,
        ...props,

        master_id: swapped ? active : master_id,
        slave: true,
        active: true,
        popuped: popup || popuped,
        popupable,
        swappable,
        swapped,
        args,

        onSwapSplit,
        onPopupSplit,
        focusOther,
        onCloseSplit
      }}
    >
      { compo }
    </SplitContext.Provider>
  );

  if (swapped) {
    return (
      <div className={(popup || popuped) ? classNames(styles.spaced, 'split') : styles.container}>
        { !popup && renderMaster(slave.component(), active, onCloseSplit) }
        { !popuped && renderSlave(children, master_id, () => { push(slave.url); }) }
        <Window {...windowProps} />
      </div>
    );
  }

  return (
    <div className={(popup || popuped) ? classNames(styles.spaced, 'split') : styles.container}>
      { !popuped && renderMaster(children, master_id, () => { push(slave.url); }) }
      { !popup && renderSlave(slave.component(), active, onCloseSplit) }
      <Window {...windowProps} />

      { popuped && <WindowTitle title={`${tab_label} ${slave.label}`} /> }
    </div>
  );
};

// Props
Split.propTypes = {
  // from redux
  tab_id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired, /** id of the tab (in the app) */
  tab_label: PropTypes.string,
  active: PropTypes.string.isRequired, /** true if slave is active */
  swapped: PropTypes.bool.isRequired, /** true if split is swapped (master became slave) */
  popup: PropTypes.bool.isRequired, /** true if slave is gone to an other navigator tab */
  popuped: PropTypes.bool.isRequired, /** true if master is in an other navigator tab */
  args: PropTypes.shape({}).isRequired, /** current slave arguments */

  // about the master
  children: PropTypes.element.isRequired, /** the master component */
  master_id: PropTypes.string.isRequired, /** master's id (allow the slave to know who is the master) */ // eslint-disable-line max-len
  master_args: PropTypes.shape({}), /** master's default args (when the split is swapped) */

  // about the slaves
  slaves: PropTypes.objectOf(
    PropTypes.shape({
      url: PropTypes.string.isRequired, /** url to redirect when closing the master */
      label: PropTypes.string.isRequired, /** slave's label */
      component: PropTypes.func.isRequired, /** the slave component */
      popupable: PropTypes.bool, /** allow master-slave swap (default: true) */
      swappable: PropTypes.bool, /** allow master-slave swap (default: true) */
      swapKeepArgs: PropTypes.bool, /** allow to keep args when swap with this slave (default: false) */ // eslint-disable-line max-len

      args: PropTypes.shape({}) /** slave's default args */
    }).isRequired
  ).isRequired,

  // actions
  push: PropTypes.func.isRequired,

  onActivateSplit: PropTypes.func.isRequired,
  onSwapSplit: PropTypes.func.isRequired,
  updateArgsSplit: PropTypes.func.isRequired,
  openPopupSplit: PropTypes.func.isRequired,
  closePopupSplit: PropTypes.func.isRequired,
  onCloseSplit: PropTypes.func.isRequired,
  closeSlave: PropTypes.func.isRequired
};

Split.defaultProps = {
  tab_label: undefined,
  master_args: {}
};

export default Split;
