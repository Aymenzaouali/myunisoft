import React, { Fragment, useContext, useState } from 'react';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Shortcuts } from 'react-shortcuts/lib';

import I18n from 'assets/I18n';
import SplitContext, { splitShape } from 'context/SplitContext';
import { getAccountingLabelByValue } from 'helpers/accounting';

import Attachments from 'containers/groups/Attachments';
import { AccountingFilterContainer } from 'containers/groups/Filters';
import AccountingTableContainer from 'containers/groups/Tables/Accounting';
import { AccountParameters } from 'containers/groups/Dialogs';

import SplitToolbar from 'components/groups/ToolBar/SplitToolbar';
import { InlineButton } from 'components/basics/Buttons';

import styles from './accouting.module.scss';

// Component
const Accounting = (props) => {
  const { slaves, onActivateSplit, onCloseSplit } = useContext(SplitContext);

  const {
    split,
    filters: { type: filterType = 'e' },
    location,
    societyId,
    openPopupSplit,
    routeForIBSetting
  } = props;

  const [open, setOpen] = useState(false);

  const handleClickParams = () => {
    if (filterType === 'ib') {
      window.open(routeForIBSetting.path.replace(':id', societyId), `window${new Date().getTime()}`, `width=${window.innerWidth},height=${window.innerHeight}`);
    } else if (filterType === 'o' || filterType === 'p') {
      setOpen(true);
    }
  };

  const handleShortcuts = (action) => {
    switch (action) {
    case 'NEW_TAB':
      window.open(location.pathname, `tab${new Date().getTime()}`);
      break;
    case 'NEW_WINDOW':
      window.open(location.pathname, `window${new Date().getTime()}`, `width=${window.innerWidth},height=${window.innerHeight}`);
      break;
    case 'LAUNCH_CONSULTING':
      if (slaves.length) {
        onActivateSplit(slaves[0].id, slaves[0].args);
        openPopupSplit('accounting');
        onCloseSplit();
      }
      openPopupSplit('accounting');
      onCloseSplit();
      break;
    default:
      break;
    }

    return null;
  };

  const renderMaster = () => (
    <Fragment>
      { (filterType === 'm') ? (
        <div className={styles.overflowSticky}>
          <AccountingFilterContainer />
          <Attachments />
        </div>
      ) : (
        <div className={styles.overflowContained}>
          { !(split.slave && split.master_id === 'consulting') && (
            <AccountingFilterContainer />
          ) }
          <AccountingTableContainer />
        </div>
      ) }
      <AccountParameters
        isOpen={open}
        onClose={() => setOpen(false)}
      />
    </Fragment>
  );

  const renderSlave = () => {
    if (split.master_id === 'consulting') {
      if (!split.args.entry_id) {
        return (
          <p className={styles.emptySlave}>{I18n.t('accounting.slave.consulting.empty')}</p>
        );
      }
    }

    return renderMaster();
  };

  const sectionTitle = getAccountingLabelByValue(filterType);

  return (
    <Shortcuts
      name="ACCOUNTING"
      className={styles.accountingContainer}
      handler={handleShortcuts}
      alwaysFireHandler
      stopPropagation={false}
      preventDefault={false}
      targetNodeSelector="body"
    >
      <div className={styles.header}>
        <Typography className={styles.title} variant="h1">{ sectionTitle }</Typography>
        {(filterType === 'o' || filterType === 'p' || filterType === 'ib')
        && (
          <InlineButton buttons={[
            {
              _type: 'icon',
              iconName: 'icon-parametres',
              iconSize: 32,
              onClick: () => handleClickParams(),
              titleInfoBulle: I18n.t('tooltips.accountParameters')
            }
          ]}
          />
        )
        }
        {filterType !== 'm' && <SplitToolbar /> }
      </div>
      { split.slave ? renderSlave() : renderMaster() }
    </Shortcuts>
  );
};

// Props
Accounting.propTypes = {
  openPopupSplit: PropTypes.func.isRequired,
  routeForIBSetting: PropTypes.shape({}).isRequired,
  filters: PropTypes.shape({
    type: PropTypes.string
  }),
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  societyId: PropTypes.number.isRequired,

  split: splitShape.isRequired
};

Accounting.defaultProps = {
  filters: {}
};

export default Accounting;
