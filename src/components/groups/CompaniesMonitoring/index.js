import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Typography, Checkbox,
  FormGroup, FormControlLabel
} from '@material-ui/core';
import { Button } from 'components/basics/Buttons';
import { PeriodSelector } from 'containers/basics/Selector';
import { DashboardCollabToggle } from 'containers/groups/Toggle';
import TrackingSociety from 'containers/groups/TrackingSociety';
import moment from 'moment';
import I18n from 'assets/I18n';
import './styles.scss';

const currentYear = moment().format('YYYY');

const CompaniesMonitoring = (props) => {
  const {
    changeTrackingMode,
    currentMode,
    getTrackingSocieties,
    clearSelectedPeriod,
    clearProgressionTableSort,
    isTrackingSocietiesLoading
  } = props;

  // States
  const [historyFilters, setHistoryFilters] = useState({
    year: currentYear,
    checkboxes: {
      statement: true,
      vat: true,
      is: true,
      cvae: true,
      ago: true
    }
  });

  const handleCheckboxHistoryFilter = (ev, checked) => {
    const filterName = ev.target.value;

    setHistoryFilters(({ year, checkboxes }) => ({
      year,
      checkboxes: {
        ...checkboxes,
        [filterName]: checked
      }
    }));
  };

  const handleChangeType = (type) => {
    if (isTrackingSocietiesLoading) {
      return;
    }
    clearProgressionTableSort();
    clearSelectedPeriod();
    changeTrackingMode(type);
    getTrackingSocieties();
  };

  return (
    <div className="companies-monitoring">
      <div className="companies-monitoring__header">
        <Typography className="companies-monitoring__header__title" variant="h1">{I18n.t('companiesMonitoring.title')}</Typography>
        {
          currentMode !== 1 && (<PeriodSelector type={currentMode === 2 ? 'month' : 'year'} />)
        }
        <div className="companies-monitoring__header__buttons">
          <Button
            variant="outlined"
            className="companies-monitoring__header__buttons__history"
            color={currentMode === 0 ? 'primary' : 'default'}
            onClick={
              () => {
                handleChangeType(0);
              }
            }
          >
            {I18n.t('companiesMonitoring.displayType.history')}
          </Button>
          <DashboardCollabToggle {...props} />
        </div>
      </div>
      {currentMode === 0 && (
        <div className="companies-monitoring__history-filters">
          <FormGroup className="companies-monitoring__history-filters__checkboxes" row>
            {Object.keys(historyFilters.checkboxes).map(key => (
              <FormControlLabel
                key={`history-filter-${key}`}
                data-spec="history-filter-checkbox"
                control={(
                  <Checkbox
                    color="primary"
                    checked={historyFilters.checkboxes[key]}
                    onChange={handleCheckboxHistoryFilter}
                    value={key}
                  />
                )}
                label={I18n.t(`companiesMonitoring.historyFilters.${key}`)}
                className="companies-monitoring__history-filters___checkboxes__checkbox-label"
              />
            ))}
          </FormGroup>
          <div className="companies-monitoring__history-filters__flexbox-hack" />
        </div>
      )}
      <div className="companies-monitoring__table">
        <TrackingSociety />
      </div>
    </div>
  );
};

// Props
CompaniesMonitoring.propTypes = {
  changeTrackingMode: PropTypes.func.isRequired,
  getTrackingSocieties: PropTypes.func.isRequired,
  currentMode: PropTypes.number.isRequired,
  clearSelectedPeriod: PropTypes.func.isRequired,
  clearProgressionTableSort: PropTypes.func.isRequired,
  isTrackingSocietiesLoading: PropTypes.bool.isRequired
};

CompaniesMonitoring.defaultProps = {};

export default CompaniesMonitoring;
