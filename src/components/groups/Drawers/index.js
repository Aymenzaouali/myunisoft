import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FontIcon from 'components/basics/Icon/Font';
import Version from 'components/groups/Version';
import { MenuDialog } from 'containers/groups/Dialogs';
import { ReactComponent as Logo } from 'assets/images/logo.svg';
import I18n from 'assets/I18n';
import { routesByKey, getRouteByKey } from 'helpers/routes';
import './styles.scss';
import menus from './menus';

class MiniDrawer extends React.Component {
  state = {
    selectedIndex: null,
    isDialogOpen: false,
    dialogOpenedKey: null
  };

  handleListItemClick = (selectedIndex, { key, items }) => {
    const { isDialogOpen } = this.state;
    const { push } = this.props;
    if (!items) {
      if (Object.values(isDialogOpen).some(d => d));
      this.handleClose();
      push(routesByKey[key], getRouteByKey(key).state);
    } else {
      this.setState({ selectedIndex });
      this.openDrawer(key);
    }
  };

  handleClose = () => {
    this.setState({
      selectedIndex: null,
      isDialogOpen: false,
      dialogOpenedKey: null
    });
  };

  openDrawer = (dialog) => {
    this.setState({
      isDialogOpen: true,
      dialogOpenedKey: dialog
    });
  };

  findRootMenuKey = (items, key) => {
    for (let i = 0; i < items.length; i += 1) {
      if (items[i].key === key) return items[i].key;
      if (items[i].items && this.findRootMenuKey(items[i].items, key)) return items[i].key;
    }
    return null;
  }

  renderListItem = (menu, index, rootMenuKey) => {
    const { classes } = this.props;
    const { selectedIndex } = this.state;
    const isSelected = (selectedIndex === index) || (!selectedIndex && menu.key === rootMenuKey);

    return (
      <ListItem
        button
        tabIndex={-1}
        key={menu.key}
        selected={!isSelected}
        onClick={() => this.handleListItemClick(index, menu)}
        classes={{ root: classes.selectedItem }}
        style={{ borderLeft: `4px solid ${isSelected ? '#0bd1d1' : 'transparent'}` }}
      >
        <ListItemIcon style={{ color: !isSelected ? 'white' : '#0bd1d1' }}>
          <FontIcon
            name={menu.icon}
            size="30px"
            color="primary"
          />
        </ListItemIcon>
        <ListItemText primary={I18n.t(`drawer.${menu.key}`)} classes={{ primary: classes.itemContent }} />
      </ListItem>
    );
  }

  render() {
    const {
      classes, menus, push, routeKey, isMenuOpen, toggleMenuHandler
    } = this.props;
    const { isDialogOpen, dialogOpenedKey } = this.state;

    const currentDialogData = menus.find(m => m.key === dialogOpenedKey);
    const rootMenuKey = this.findRootMenuKey(menus, routeKey);

    return (
      <>
        <Drawer
          variant="permanent"
          className={classNames('drawer', {
            [classes.drawerOpen]: isMenuOpen,
            [classes.drawerClose]: !isMenuOpen
          })}
          classes={{
            paper: classNames({
              [classes.drawerOpen]: isMenuOpen,
              [classes.drawerClose]: !isMenuOpen
            })
          }}
          open={isMenuOpen}
        >
          <div className="drawer__logo">
            <IconButton onClick={toggleMenuHandler}>
              <Logo fill="white" height="42px" />
            </IconButton>
          </div>
          <List>
            {menus.map((menu, index) => this.renderListItem(menu, index, rootMenuKey))}
          </List>
          { isMenuOpen && <Version className="drawer__version" /> }
        </Drawer>
        <MenuDialog
          onClose={this.handleClose}
          isOpen={isDialogOpen}
          data={currentDialogData}
          push={push}
          isDrawerOpen={isMenuOpen}
        />
      </>
    );
  }
}

const drawerWidth = 240;

const styles = theme => ({
  drawerOpen: {
    width: drawerWidth,
    backgroundColor: '#000000',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: 'hidden',
    backgroundColor: '#000000',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1
    }
  },
  itemContent: {
    color: 'white',
    fontSize: '12px',
    fontWeight: 500
  },
  selectedItem: {
    backgroundColor: '#181818'
  }
});


MiniDrawer.propTypes = {
  isMenuOpen: PropTypes.bool.isRequired,
  toggleMenuHandler: PropTypes.func.isRequired,
  classes: PropTypes.shape({}).isRequired,
  theme: PropTypes.shape({}).isRequired,
  push: PropTypes.func.isRequired,
  routeKey: PropTypes.string.isRequired,
  menus: PropTypes.arrayOf(PropTypes.shape({}))
};

MiniDrawer.defaultProps = {
  menus
};

export default withStyles(styles, { withTheme: true })(MiniDrawer);
