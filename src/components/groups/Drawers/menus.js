/**
 * Menus array
 * @example <caption>Add entry into src/components/groups/Drawers/menus.js array</caption>
 *
 * const menus = [..., { key: 'myNewRoute' }]
 *
 * export default menus;
 * @example <caption>Add translation into src/assets/I18n/locales/(en,fr).js "drawer" key</caption>
 *
 * export default {
 * ...,
 * drawer: {
 *  ...,
 *  myNewRoute: "Label de ma nouvelle route"
 * }
 * @example <caption>Add route into src/helpers/routes.js routes or subroutes</caption>
 *
 * const routes = [
 *   ...,
 *   {
 *     key: 'myNewRoute',
 *     path: '/tab/:id/myNewRoutePath',
 *     component: myNewComponent
 *   }
 * ];
 *
 * // If you don't want this route to be add automatically in NavBar.js Switch,
 * // use subroutes var instead.
 *
 * const subroutes = {
 *  ...,
 *  myNewRoute: '/tab/:id/myNewRoutePath'
 * };
 *
 * // Then add a <Route path={routeByKey.myNewRoute} component={myNewComponent} />
 * // manually where you want ;)
 */
const menus = [
  {
    key: 'dashboard',
    icon: 'icon-overview'
  },
  {
    key: 'keeping',
    icon: 'icon-tenue',
    items: [
      {
        key: 'entry',
        items: [
          { key: 'newAccounting', isEnable: true },
          { key: 'accountConsultation', isEnable: true },
          { key: 'collection', isEnable: false }, // encaissement
          { key: 'bankLink', isEnable: true },
          { key: 'ged', isEnable: true }
        ]
      },
      {
        key: 'flow',
        items: [
          { key: 'ocr', isEnable: true },
          { key: 'ocrTracking', isEnable: true },
          { key: 'collector', isEnable: true },
          { key: 'import', isEnable: true },
          { key: 'export', isEnable: true },
          { key: 'dynamicsLinks', isEnable: true }
        ]
      },
      {
        key: 'currentEditions',
        items: [
          {
            key: 'balance', routeKey: 'currentEditions', routeParams: { current: 'balance' }, isEnable: true
          },
          {
            key: 'ledger', routeKey: 'currentEditions', routeParams: { current: 'ledger' }, isEnable: true
          },
          {
            key: 'sig', routeKey: 'currentEditions', routeParams: { current: 'sig' }, isEnable: true
          },
          {
            key: 'statement', routeKey: 'currentEditions', routeParams: { current: 'statement' }, isEnable: false
          },
          {
            key: 'currentEditionDiary', routeKey: 'currentEditions', routeParams: { current: 'currentEditionDiary' }, isEnable: false
          },
          {
            key: 'margin', routeKey: 'currentEditions', routeParams: { current: 'diary' }, isEnable: false
          },
          { key: 'chainEditions', isEnable: false },
          { key: 'raise', isEnable: false },
          { key: 'deadline', isEnable: false }
        ]
      }
    ]
  },
  {
    key: 'declaration',
    icon: 'icon-declarations',
    items: [
      {
        key: 'vat',
        items: [
          { key: 'tvaca3', isEnable: true },
          { key: 'ca12Advance', isEnable: true },
          { key: 'annualCa12', isEnable: true }
        ]
      },
      {
        key: 'is',
        items: [
          { key: 'downPayment', isEnable: true },
          { key: 'closeOut', isEnable: true },
          { key: 'refundTaxCredit', isEnable: true }
        ]
      },
      {
        key: 'cvae_cfe',
        items: [
          { key: 'downPaymentCvae', isEnable: true },
          { key: 'balanceCvae', isEnable: true },
          { key: 'cfeM', isEnable: false },
          { key: 'cfeCreation', isEnable: false }
        ]
      },
      {
        key: 'other',
        items: [
          { key: 'decloyer', isEnable: true },
          { key: 'das2', isEnable: false },
          { key: 'other2561', isEnable: false },
          { key: 'other2777', isEnable: false },
          { key: 'tascom', isEnable: false }
        ]
      }
      /*  {
        key: 'declarationTracking',
        items: [
          { key: 'declarationTracking', isEnable: false },
          { key: 'declarationTrackingSettings', isEnable: false } // temporarily disable
        ]
      } */
    ]
  },
  {
    key: 'reviewing',
    icon: 'icon-calculator',
    items: [
      {
        key: 'da_dp',
        items: [

          {
            key: 'da_balanceSheet', routeKey: 'dadp', routeParams: { current: 'da_balanceSheet' }, isEnable: true
          },
          {
            key: 'da_statusPending', routeKey: 'dadp', routeParams: { current: 'da_statusPending' }, isEnable: false
          },
          {
            key: 'da_newSituation', routeKey: 'dadp', routeParams: { current: 'da_newSituation' }, isEnable: false
          }
        ]
      },
      {
        key: 'worksheet',
        items: [
          { key: 'fixed_assets', isEnable: true },
          { key: 'vehicle', isEnable: false },
          { key: 'loan', isEnable: false },
          { key: 'cb', isEnable: false },
          { key: 'overhead_income', isEnable: false },
          { key: 'AAE', isEnable: true },
          { key: 'AAR', isEnable: true },
          { key: 'FAE', isEnable: true },
          { key: 'CAPPersonal', isEnable: true },
          { key: 'CAPInt', isEnable: true },
          { key: 'newLetters', isEnable: true }
        ]
      },
      {
        items: [
          { key: 'CAPCs', isEnable: true },
          { key: 'CAPDivers', isEnable: true },
          { key: 'CAPIt', isEnable: true },
          { key: 'pca', isEnable: true },
          { key: 'cca', isEnable: true },
          { key: 'FNP', isEnable: true }
        ]
      },
      {
        key: 'balance_sheet',
        items: [
          { key: 'taxDeclaration', isEnable: true },
          { key: 'booklet', isEnable: true }
        ]
      },
      {
        key: 'closing',
        items: [
          { key: 'closingMonth', isEnable: false },
          {
            key: 'closingFiscalYear', routeKey: 'closing-year', routeParams: { current: 'closingFiscalYear' }, isEnable: true
          }
        ]
      }
    ]
  },
  {
    key: 'ecosystem',
    icon: 'icon-ecosysteme',
    items: [
      {
        key: 'exchanges',
        items: [
          { key: 'discussionsList', isEnable: true },
          { key: 'letters', colorError: true, isEnable: true },
          { key: 'comments', isEnable: false },
          { key: 'memo', isEnable: false },
          { key: 'mail', isEnable: false },
          { key: 'support', isEnable: false }
        ]
      },
      {
        key: 'crm',
        items: [
          { key: 'userList', isEnable: true },
          { key: 'generalLM', isEnable: false },
          { key: 'specificLM', isEnable: false },
          { key: 'billing', isEnable: false },
          { key: 'statisticsCrm', isEnable: false },
          { key: 'archiveDeleteSociety', isEnable: false }
        ]
      },
      {
        key: 'internal_management',
        items: [
          { key: 'timeManagement', isEnable: false },
          { key: 'chargePlan', isEnable: false },
          { key: 'internalStatistics', isEnable: false }
        ]
      }
    ]
  },
  {
    key: 'parameters',
    icon: 'icon-parametres',
    items: [
      {
        key: 'keeping',
        items: [
          { key: 'chartAccountTva', isEnable: true },
          { key: 'accountingPlans', isEnable: true },
          { key: 'analytics', isEnable: false },
          { key: 'diary', isEnable: true },
          { key: 'rib', isEnable: true },
          { key: 'connector', isEnable: true },
          { key: 'collector', isEnable: true, isExternal: false },
          { key: 'ocr_unibox', isEnable: false },
          { key: 'budgetary', isEnable: false },
          { key: 'bankIntegrationSettings', isEnable: true },
          { key: 'scanAssociate', isEnable: true }
        ]
      },
      {
        key: 'reviewing',
        items: [
          { key: 'da_dp', isEnable: false },
          { key: 'balance_sheet', isEnable: false },
          { key: 'settingsWorksheets', isEnable: true },
          { key: 'reportsAndForms', isEnable: true, colorError: true }
        ]
      },
      {
        key: 'ecosystem',
        items: [
          { key: 'rightManagement', isEnable: false },
          { key: 'billing', isEnable: false },
          { key: 'function', isEnable: true },
          { key: 'portfolioListView', isEnable: true },
          { key: 'settingsStandardMailView', colorError: true, isEnable: true },
          { key: 'accountingFirmSettings', isEnable: true }
        ]
      }
    ]
  }
];

export default menus;
