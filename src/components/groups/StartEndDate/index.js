import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { TextField } from '@material-ui/core';
import styles from './StartEndDate.module.scss';

const StartEndDate = (props) => {
  const {
    className,
    startProps,
    endProps,
    startNode,
    endNode,
    onFocus,
    onBlur
  } = props;

  const onStartFocus = (e) => {
    if (startProps && startProps.onFocus) startProps.onFocus(e);
    if (onFocus) onFocus(e);
  };

  const onEndFocus = (e) => {
    if (endProps && endProps.onFocus) endProps.onFocus(e);
    if (onFocus) onFocus(e);
  };

  const onStartBlur = (e) => {
    if (startProps && startProps.onBlur) startProps.onBlur(e);
    if (onBlur) onBlur(e);
  };

  const onEndBlur = (e) => {
    if (endProps && endProps.onBlur) endProps.onBlur(e);
    if (onBlur) onBlur(e);
  };

  return (
    <div
      className={classnames(className, styles.startEndDate)}
    >
      {startNode}
      <TextField
        {...startProps}
        className={styles.field}
        onFocus={onStartFocus}
        onBlur={onStartBlur}
      />
      {endNode}
      <TextField {...endProps} className={styles.field} onFocus={onEndFocus} onBlur={onEndBlur} />
    </div>
  );
};

StartEndDate.propTypes = {
  className: PropTypes.string,
  startProps: PropTypes.object,
  endProps: PropTypes.object,
  startNode: PropTypes.node,
  endNode: PropTypes.node,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func
};

StartEndDate.defaultProps = {
  className: undefined,
  startProps: undefined,
  endProps: undefined,
  startNode: undefined,
  endNode: undefined,
  onBlur: undefined,
  onFocus: undefined
};

export default StartEndDate;
