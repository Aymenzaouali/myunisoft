import React from 'react';
import PropTypes from 'prop-types';
import {
  FormGroup, Typography, FormControlLabel, RadioGroup
} from '@material-ui/core';

import Radio from 'components/basics/Radio';

import styles from './reduxRadioCard.module.scss';

const ReduxRadioCard = (props) => {
  const {
    meta: { touched, error, warning },
    input,
    list,
    color
  } = props;

  return (
    <div>
      <FormGroup component="fieldset">
        <RadioGroup
          name={input.name}
          value={input.value}
          onChange={input.onChange}
        >
          {list.map(item => (
            <div className={styles.contentContainer}>
              <FormControlLabel key={`radio-${item.value}`} value={item.value && item.value.toString()} control={<Radio color={color} />} />
              <div>
                <Typography variant="body">
                  {`${item.firstname} ${item.lastname}`}
                </Typography>
                <Typography variant="body">{item.element_1}</Typography>
                <Typography variant="body">{item.element_2}</Typography>
                <Typography variant="body">{item.element_3}</Typography>
              </div>
            </div>
          ))}
        </RadioGroup>
      </FormGroup>
      <div>
        {touched && ((error && <Typography color="error">{error}</Typography>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  );
};

ReduxRadioCard.defaultProps = {
  color: '',
  meta: {
    touched: false
  }
};

ReduxRadioCard.propTypes = {
  list: PropTypes.arrayOf.isRequired,
  input: PropTypes.shape({}).isRequired,
  color: PropTypes.string,
  meta: {
    touched: PropTypes.bool.isRequired
  }
};

export default ReduxRadioCard;
