import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  FormControl, InputLabel, FormHelperText,
  Select, MenuItem
} from '@material-ui/core';

import I18n from 'assets/I18n';
import Loader from 'components/basics/Loader';

import styles from './ReduxAsyncSelect.module.scss';

// Component
const ReduxAsyncSelect = (props) => {
  const {
    input, meta,
    label, options, disabled, placeholder,
    load, isLoading, isError
  } = props;

  // Effects
  useEffect(() => {
    if (!isLoading && (options == null || isError)) load();
  }, []);

  // Rendering
  const error = meta.touched && meta.error;

  return (
    <FormControl fullWidth margin="none" disabled={isLoading || isError || disabled} error={isError}>
      <InputLabel shrink={input.value !== ''}>
        { isLoading && (<div className={styles.loader}><Loader size={25} /></div>) }
        <span className={styles.label}>
          { isError ? <em>{I18n.t('select.load_error')}</em> : label }
        </span>
      </InputLabel>
      <Select value={input.value} onChange={input.onChange}>
        { placeholder && (
          <MenuItem value=""><em>{I18n.t('select.placeholder')}</em></MenuItem>
        ) }
        { options && options.map((opt, i) => (
          <MenuItem key={i} value={opt.value}>{ opt.label }</MenuItem>
        )) }
      </Select>
      { (error) && (
        <FormHelperText>{ error }</FormHelperText>
      ) }
    </FormControl>
  );
};

// Props
ReduxAsyncSelect.propTypes = {
  // Redux
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool.isRequired,
    error: PropTypes.string
  }).isRequired,

  // Content
  label: PropTypes.string.isRequired, /** the label of the select */
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired, /** the label of the option */
      value: PropTypes.any.isRequired /** the value of the option */
    }).isRequired
  ), /** options (will call load if null) */
  disabled: PropTypes.bool,
  placeholder: PropTypes.bool,

  // Load
  load: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  isError: PropTypes.bool
};

ReduxAsyncSelect.defaultProps = {
  options: null,
  disabled: false,
  placeholder: false,

  isLoading: false,
  isError: false
};

export default ReduxAsyncSelect;
