import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import PropTypes from 'prop-types';

import CheckBox from 'components/basics/CheckBox';

const ReduxArrayCheckBox = (props) => {
  const {
    input,
    meta: { touched, error, warning },
    label,
    labelStyle,
    ...customProps
  } = props;

  return (
    <div>
      <FormControlLabel
        control={(
          <CheckBox
            onChange={() => { input.onChange(); props.customOnChange(); }}
            {...customProps}
          />
        )}
        className={labelStyle}
        label={label}
      />
      <div>
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  );
};

ReduxArrayCheckBox.defaultProps = {
  customOnChange: () => {},
  labelStyle: '',
  label: '',
  meta: {}
};

ReduxArrayCheckBox.propTypes = {
  customOnChange: PropTypes.func,
  labelStyle: PropTypes.string,
  label: PropTypes.string,
  meta: PropTypes.shape({}),
  input: PropTypes.shape({}).isRequired
};
export default ReduxArrayCheckBox;
