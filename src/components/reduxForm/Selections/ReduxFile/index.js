import React from 'react';
import PropTypes from 'prop-types';
import {
  InputLabel, TextField, Typography, withStyles
} from '@material-ui/core';
import _ from 'lodash';
import I18n from 'assets/I18n';
import Button from 'components/basics/Buttons/Button';
import styles from './ReduxFile.module.scss';

class ReduxFile extends React.Component {
  render() {
    const {
      button,
      text,
      input,
      classes,
      inputId,
      meta: { touched, error, warning }
    } = this.props;

    return (
      <div className={styles.container}>
        <TextField
          label={I18n.t('imports.import_input')}
          value={_.get(input, 'value.name')}
          InputLabelProps={{ className: classes.buttonText, shrink: true }}
          className={text.className || styles.textContainer}
        />
        <div className={styles.inputContainer}>
          <input
            type="file"
            style={{ display: 'none' }}
            id={inputId}
            onChange={(event) => {
              input.onChange(event.target.files[0]);
            }}
          />
          <InputLabel htmlFor={inputId}>
            <Button
              classes={{ root: classes.buttonImport, label: classes.buttonText }}
              variant={button.variant || 'raised'}
              color={button.color || 'primary'}
              component={button.component || 'span'}
              size={button.size || 'small'}
            >
              ...
            </Button>
          </InputLabel>
        </div>
        <div>
          {touched && ((error && <Typography color="error">{error}</Typography>) || (warning && <Typography>{warning}</Typography>))}
        </div>
      </div>
    );
  }
}

ReduxFile.propTypes = {
  inputId: PropTypes.string.isRequired,
  button: PropTypes.shape({}),
  text: PropTypes.shape({}),
  input: PropTypes.shape({}),
  meta: PropTypes.shape({}),
  classes: {}
};

ReduxFile.defaultProps = {
  button: {},
  classes: {},
  text: {},
  input: {},
  meta: {}
};

const materialStyles = () => ({
  buttonImport: {
    'min-width': '31px'
  },
  buttonText: {
    'font-size': '14px'
  },
  inputText: {
    'font-size': '16px'
  }
});

export default withStyles(materialStyles)(ReduxFile);
