import React, { Fragment } from 'react';
import { fieldPropTypes } from 'redux-form';
import PropTypes from 'prop-types';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import CheckBox from 'components/basics/CheckBox';

// Component
const ReduxCheckBox = (props) => {
  const {
    input,
    meta: { touched, error, warning },
    label,
    labelStyle,
    onCustomChange,
    formControlProps,
    checked,
    ...customProps
  } = props;

  return (
    <Fragment>
      <FormControlLabel
        {...formControlProps}

        control={(
          <CheckBox
            {...customProps}

            checked={!!input.value || checked}
            onChange={(ev, checked) => { input.onChange(checked); onCustomChange(checked); }}
            value={input.name}
            name={input.name}
          />
        )}
        className={labelStyle}
        label={label}
      />
      {touched && (error || warning) && (
        <div>
          {error && <span>{error}</span>}
          {warning && <span>{warning}</span>}
        </div>
      )}
    </Fragment>
  );
};

// Props
ReduxCheckBox.propTypes = {
  formControlProps: PropTypes.shape({}),
  label: PropTypes.node,
  labelStyle: PropTypes.string,
  onCustomChange: PropTypes.func,

  ...fieldPropTypes
};

ReduxCheckBox.defaultProps = {
  label: '',
  labelStyle: '',
  formControlProps: {},
  onCustomChange: () => {}
};

export default ReduxCheckBox;
