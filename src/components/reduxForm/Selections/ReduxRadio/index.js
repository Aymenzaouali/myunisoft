import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { fieldPropTypes } from 'redux-form';

import {
  withStyles,
  RadioGroup,
  FormControlLabel, FormControl, FormLabel, FormHelperText,
  Typography
} from '@material-ui/core';

import Radio from 'components/basics/Radio';

const stylesReduxRadio = ({ palette }) => ({
  direction: {
    flexDirection: 'row'
  },
  colorError: {
    color: palette.secondary.main
  }
});

const ReduxRadio = (props) => {
  const {
    classes,
    row,
    input, meta,
    list,
    color,
    label,
    onCustomChange,
    className,
    FormControlLabelProps,
    RadioProps,
    disabled
  } = props;

  return (
    <FormControl disabled={disabled}>
      { label && (
        <FormLabel>
          <Typography variant="caption" color="inherit">{ label }</Typography>
        </FormLabel>
      ) }
      <RadioGroup
        className={classnames(className, row && classes.direction)}
        name={input.name}
        value={input.value}
        onChange={(val) => { input.onChange(val); onCustomChange(val); }}
      >
        {list.map(item => (
          <FormControlLabel
            key={`radio-${item.value}`}
            value={`${item.value}`}
            control={<Radio color={(item.color === 'colorError' ? 'secondary' : color)} {...RadioProps} />}
            label={item.label}
            classes={{ label: classnames({ [classes.colorError]: item.color === 'colorError' }) }}
            {...FormControlLabelProps}
          />
        ))}
      </RadioGroup>
      { meta.touched && (meta.error || meta.warning) && (
        <FormHelperText error={!!meta.error}>
          { meta.error || meta.warning }
        </FormHelperText>
      ) }
    </FormControl>
  );
};

ReduxRadio.defaultProps = {
  className: '',
  color: 'primary',
  row: false,
  onCustomChange: () => {},
  FormControlLabelProps: {},
  RadioProps: {},
  list: [],
  label: null,
  disabled: false
};

ReduxRadio.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape({})),
  className: PropTypes.string,
  classes: PropTypes.shape({}).isRequired,
  color: PropTypes.string,
  row: PropTypes.bool,
  onCustomChange: PropTypes.func,
  label: PropTypes.string,
  FormControlLabelProps: PropTypes.shape({}),
  RadioProps: PropTypes.shape({}),
  disabled: PropTypes.bool,

  ...fieldPropTypes
};

export default withStyles(stylesReduxRadio)(ReduxRadio);
