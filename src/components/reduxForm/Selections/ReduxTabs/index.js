import React from 'react';
import PropTypes from 'prop-types';
import { Tabs } from '@material-ui/core';

const ReduxTabs = (props) => {
  const {
    input, meta, //eslint-disable-line
    children, //eslint-disable-line
    data,
    ...customProps
  } = props;

  let value = 0;
  if (data && data.length > 0 && input && input.value) {
    value = data.indexOf(input.value);
  } else if (data && data.length > 0 && input && !input.value) {
    input.onChange(data[0]);
  }
  if (!data && input && input.value) {
    value = input.value; // eslint-disable-line
  }

  return (
    <Tabs
      value={value}
      indicatorColor="primary"
      textColor="primary"
      onChange={(e, value) => {
        if (data) {
          input.onChange(data[value]);
        } else {
          input.onChange(value);
        }
      }
      }
      {...customProps}
    >
      {children}
    </Tabs>
  );
};

ReduxTabs.defaultProps = {
  data: undefined
};

ReduxTabs.propTypes = {
  data: PropTypes.array
};

export default ReduxTabs;
