import React from 'react';
import { fieldPropTypes } from 'redux-form';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  FormControl, FormHelperText,
  InputLabel,
  Select,
  MenuItem
} from '@material-ui/core';

const ReduxSelect = (props) => {
  const {
    margin,
    input, meta,
    list,
    label,
    className,
    classes,
    fullWidth,
    disabled,
    errorStyle,
    ...otherProps
  } = props;

  // Rendering
  const hasError = (meta.dirty || meta.submitFailed) && !!(meta.error || meta.warning);
  const helperText = meta.error || meta.warning;

  return (
    <FormControl classes={classes} margin={margin} fullWidth={fullWidth} error={hasError}>
      <InputLabel shrink={input.value !== ''}>
        {label}
      </InputLabel>
      <Select
        value={input.value}
        onChange={input.onChange}
        className={className}
        disabled={disabled}
      >
        {list.map((item, i) => (
          <MenuItem
            key={`select-${item.label}${i}`}
            value={item.value}
            disabled={_.get(item, 'disabled', false)}
            label={item.label}
            style={{ color: _.get(item, 'color', 'black') }}
            {...otherProps}
          >
            {item.label}
          </MenuItem>
        ))}
      </Select>
      { hasError && (
        <FormHelperText style={errorStyle}>{ helperText }</FormHelperText>
      ) }
    </FormControl>
  );
};

ReduxSelect.defaultProps = {
  margin: 'normal',
  label: '',
  classes: {},
  className: undefined,
  errorStyle: {},
  fullWidth: false,
  disabled: false
};

ReduxSelect.propTypes = {
  margin: PropTypes.string,
  disabled: PropTypes.bool,
  list: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  fullWidth: PropTypes.bool,
  label: PropTypes.string,
  errorStyle: PropTypes.object,
  classes: PropTypes.object,
  className: PropTypes.string,
  ...fieldPropTypes
};

export default ReduxSelect;
