import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Switch } from '@material-ui/core';
import styles from './ReduxSwitch.module.scss';

const ReduxSwitch = (props) => {
  const {
    label,
    labelOn,
    labelOff,
    input,
    ...otherProps
  } = props;
  return (
    <div className={styles.switchContainer}>
      {label && <Typography variant="body1">{label}</Typography>}
      {labelOff && <Typography variant="body1" className={styles.paddings}>{ labelOff }</Typography>}
      <Switch
        checked={input.value}
        onChange={e => input.onChange(e.target.checked)}
        classes={{
          switchBase: styles.switch,
          checked: styles.checked,
          bar: styles.bar
        }}
        {...otherProps}
      />
      {labelOn && <Typography variant="body1" className={styles.paddings}>{ labelOn }</Typography>}
    </div>
  );
};

ReduxSwitch.defaultProps = {
  label: null,
  labelOn: null,
  labelOff: null
};

ReduxSwitch.propTypes = {
  label: PropTypes.string,
  labelOn: PropTypes.string,
  labelOff: PropTypes.string,
  input: PropTypes.object.isRequired
};

export default ReduxSwitch;
