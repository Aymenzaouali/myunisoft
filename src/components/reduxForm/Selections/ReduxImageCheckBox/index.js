import React from 'react';
import { ImageSelections } from 'components/basics/Selections';
import PropTypes from 'prop-types';

const ReduxImageCheckbox = (props) => {
  const {
    input,
    meta: { touched, error, warning },
    ...customProps
  } = props;

  return (
    <div>
      <ImageSelections
        checkboxProps={{
          onChange: input.onChange,
          checked: input.value
        }}
        imageProps={{
          onClick: input.onChange.bind(this, !input.value),
          isChecked: input.value
        }}
        {...customProps}
      />
      <div>
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  );
};

ReduxImageCheckbox.propTypes = {
  meta: PropTypes.shape({}),
  input: PropTypes.shape({}).isRequired
};

ReduxImageCheckbox.defaultProps = {
  meta: {}
};

export default ReduxImageCheckbox;
