import React, { useState, useEffect } from 'react';
import { Typography, Input, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { borderInputStyles } from 'assets/theme/styles';

const ReduxInput = (props) => {
  const {
    input,
    onCustomBlur = () => {},
    onCustomFocus = () => {},
    onCustomChange = () => {},
    meta: { touched, error, warning },
    border,
    classes,
    ...customProps
  } = props;

  const [inputValue, setInputValue] = useState(input.value);

  useEffect(() => {
    setInputValue(input.value);
  }, [input.value]);

  const {
    focusedInputBorder: focused,
    underlineBorder: underline
  } = classes;

  return (
    <div>
      <Input
        classes={border && { focused, underline }}
        onChange={(event) => {
          const { target: { value } } = event;
          setInputValue(value);
          onCustomChange(event);
        }}
        onFocus={() => { onCustomFocus(); }}
        value={inputValue}
        {...customProps}
        onBlur={(a) => {
          if (!_.isEqual(input.value, inputValue)) {
            input.onChange(inputValue);
            input.onBlur(a);
            onCustomBlur(a);
          }
        }}
      />
      <div>
        {touched && ((error && <Typography color="error">{error}</Typography>) || (warning && <Typography>{warning}</Typography>))}
      </div>
    </div>
  );
};

ReduxInput.defaultProps = {
  onCustomBlur: () => {},
  onCustomFocus: () => {},
  onCustomChange: () => {},
  meta: {},
  border: false,
  classes: {}
};

ReduxInput.propTypes = {
  onCustomBlur: PropTypes.func,
  onCustomFocus: PropTypes.func,
  onCustomChange: PropTypes.func,
  meta: PropTypes.shape({}),
  input: PropTypes.shape({}).isRequired,
  border: PropTypes.bool,
  classes: PropTypes.shape({})
};

export default withStyles(borderInputStyles)(ReduxInput);
