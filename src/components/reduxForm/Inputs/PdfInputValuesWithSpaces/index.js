import React from 'react';
import { fieldPropTypes } from 'redux-form';
import PropTypes from 'prop-types';

const PdfInputValuesWithSpaces = (props) => {
  const {
    input,
    className,
    disabled,
    max,
    letterSpacing
  } = props;


  const formatInput = (val, max) => {
    const returnedInputVal = val % 1 === 0 ? val : val.slice(val.length - 1);
    return returnedInputVal.length > max ? returnedInputVal.slice(0, max) : returnedInputVal;
  };

  const isContainPoint = (val) => {
    const str = val.toString();
    return Boolean(str.match(/\./));
  };

  const handleChange = (event) => {
    event.preventDefault();

    const { value } = event.target;
    if (isContainPoint(value)) return;

    // eslint-disable-next-line no-restricted-globals
    if (value === '' || !isNaN(+value)) {
      const formattedValue = formatInput(value, max);
      input.onChange(formattedValue);
    }
  };
  const style = {
    letterSpacing: letterSpacing || '1.2em',
    paddingRight: '0'
  };

  return (
    <input
      name={input.name}
      value={input.value}
      className={className}
      disabled={disabled}
      onChange={handleChange}
      onBlur={input.onBlur}
      style={style}
      autoComplete="nope"
    />
  );
};

// Props
PdfInputValuesWithSpaces.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  ...fieldPropTypes
};

PdfInputValuesWithSpaces.defaultProps = {
  className: undefined,
  disabled: false
};

export default PdfInputValuesWithSpaces;
