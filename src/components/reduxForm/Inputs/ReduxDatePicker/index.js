import React from 'react';
import _ from 'lodash';
import { TextField, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Shortcuts } from 'react-shortcuts';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './styles.scss';

const styles = () => ({
  root: {
    width: '25px'
  },
  focusedInputBorder: {
    border: '1px solid #0bd1d1',
    'background-color': 'white'
  },
  underlineBorder: {
    '&:before': {
      'border-bottom': 'none'
    },
    '&&&&:hover:before': {
      'border-bottom': '2px solid #0bd1d1'
    }
  }
});

class ReduxDatePicker extends React.PureComponent {
  render() {
    const day = _.get(this.props, `${this.props.names[0]}`); //eslint-disable-line
    const month = _.get(this.props, `${this.props.names[1]}`); //eslint-disable-line
    const year = _.get(this.props, `${this.props.names[2]}`); //eslint-disable-line
    const {
      onDayBlur = () => {},
      onMonthBlur = () => {},
      onYearBlur = () => {},
      onCustomChange = () => {},
      border,
      classes,
      customRef,
      switchToNextInput,
      className
    } = this.props;

    const onChangeGlobal = () => {
      onCustomChange();
    };

    const {
      focusedInputBorder: focused,
      underlineBorder: underline
    } = classes;

    return (
      <div>
        <div className={classNames('reduxDatePicker__container', className)}>
          <div>
            <Shortcuts
              handler={(action) => { if (action === 'ENTER') { this.month.focus(); this.month.select(); } }}
              name="NEW_ACCOUNTING"
              alwaysFireHandler
              stopPropagation={false}
              preventDefault={false}
            >
              <TextField
                InputProps={{
                  classes: border && { focused, underline }
                }}
                className={classes.root}
                {...day.input}
                onChange={(val) => {
                  onChangeGlobal();
                  day.input.onChange(val);
                  if (val.target.value.length === 2
              && !(this.day.selectionStart === 0 && this.day.selectionEnd === 2)) {
                    this.month.focus();
                    setTimeout(() => this.month.select(), 0);
                  }
                }}
                type="text"
                min="01"
                max="31"
          inputProps={{ // eslint-disable-line
                  maxLength: 2
                }}
                placeholder="jj"
                onBlur={(val) => {
                  day.input.onBlur();
                  onDayBlur(val);
                }}
                inputRef={(inputElement) => { this.day = inputElement; customRef(inputElement); }}
              />
            </Shortcuts>
          </div>
          <span className="reduxDatePicker__date-separator">/</span>
          <div>
            <Shortcuts
              handler={(action) => { if (action === 'ENTER') { this.year.focus(); this.year.select(); } }}
              name="NEW_ACCOUNTING"
              alwaysFireHandler
              stopPropagation={false}
              preventDefault={false}
            >
              <TextField
                InputProps={{
                  classes: border && { focused, underline }
                }}
                className={classes.root}
                {...month.input}
                onChange={(val) => {
                  onChangeGlobal();
                  month.input.onChange(val);
                  if (val.target.value.length === 2
              && !(this.month.selectionStart === 0 && this.month.selectionEnd === 2)) {
                    this.year.focus();
                    setTimeout(() => this.year.select(), 0);
                  }
                }}
                type="text"
                min="01"
                max="12"
              inputProps={{ // eslint-disable-line
                  maxLength: 2
                }}
                placeholder="mm"
                onBlur={(val) => {
                  month.input.onBlur();
                  onMonthBlur(val);
                }}
                inputRef={(inputElement) => { this.month = inputElement; }}
              />

            </Shortcuts>
          </div>
          <span className="reduxDatePicker__date-separator">/</span>
          <div>
            <Shortcuts
              handler={(action) => { if (action === 'ENTER') { switchToNextInput(); } }}
              name="NEW_ACCOUNTING"
              alwaysFireHandler
              stopPropagation={false}
              preventDefault={false}
            >
              <TextField
                InputProps={{
                  classes: border && { focused, underline }
                }}
                {...year.input}
                type="text"
          inputProps={{ // eslint-disable-line
                  maxLength: 4
                }}
                onChange={(val) => {
                  onChangeGlobal();
                  year.input.onChange(val);
                }}
                placeholder="aaaa"
                onBlur={(val) => { year.input.onBlur(); onYearBlur(val); }}
                inputRef={(inputElement) => { this.year = inputElement; }}
              />

            </Shortcuts>
          </div>
        </div>
        <div>
          {day.meta.touched && ((day.meta.error && <Typography color="error">{day.meta.error}</Typography>) || (day.meta.warning && <Typography>{day.meta.warning}</Typography>))}
        </div>
        <div>
          {month.meta.touched && ((month.meta.error && <Typography color="error">{month.meta.error}</Typography>) || (month.meta.warning && <Typography>{month.meta.warning}</Typography>))}
        </div>
        <div>
          {year.meta.touched && ((year.meta.error && <Typography color="error">{year.meta.error}</Typography>) || (year.meta.warning && <Typography>{year.meta.warning}</Typography>))}
        </div>
      </div>
    );
  }
}

ReduxDatePicker.defaultProps = {
  onDayBlur: () => {},
  switchToNextInput: () => {},
  customRef: () => {},
  onMonthBlur: () => {},
  onYearBlur: () => {},
  onCustomChange: () => {},
  meta: {},
  border: false,
  input: {},
  classes: '',
  className: {}
};

ReduxDatePicker.propTypes = {
  onDayBlur: PropTypes.func,
  switchToNextInput: PropTypes.func,
  customRef: PropTypes.func,
  onMonthBlur: PropTypes.func,
  onYearBlur: PropTypes.func,
  onCustomChange: PropTypes.func,
  meta: PropTypes.shape({}),
  input: PropTypes.shape({}),
  border: PropTypes.bool,
  classes: PropTypes.string,
  className: PropTypes.shape({})
};

export default withStyles(styles, { withTheme: true })(ReduxDatePicker);
