This component is a DatePicker binded with redux-form that you can fully customize unlike the basic datepicker input from HTML.

| Props             | Type            | Required | Description                                                                                |
|-------------------|-----------------|-----------------|---------------------------------------------------------------------------------------------|
| `names` | `array`        | Yes | Array of field names, the first one is for day, the 2nd one is for month, the 3rd is for year |
| `onDayBlur`            | `func` | No | On day blur |
| `onMonthBlur`            | `func` | No | On month blur |
| `onYearBlur`            | `func` | No | On year blur |

```js
<ReduxDatePicker
  names={['day', 'month', 'year']}
  day={{input: { onBlur: () => {}, onChange: () => {}}, meta: {}}}
  month={{input: { onBlur: () => {}, onChange: () => {}}, meta: {}}}
  year={{input: { onBlur: () => {}, onChange: () => {}}, meta: {}}}
  onDayBlur={() => {}}
  onMonthBlur={() => {}}
  onYearBlur={() => {}}
/>
```
