import React from 'react';
import { fieldPropTypes } from 'redux-form';
import PropTypes from 'prop-types';

const PdfInputWithProcentChar = (props) => {
  const {
    input,
    className,
    disabled
  } = props;

  let formattedValue = '';

  const formatValue = (value) => {
    let withOutProcentChar = value;
    withOutProcentChar = value.replace('%', '');
    const ifLastCharProcent = value.indexOf('%');
    const ifFormattedValueHasProcent = formattedValue.indexOf('%') !== -1;

    if (!value.length || (ifFormattedValueHasProcent && value.length === 1)) {
      return false;
    }
    if (value.length > 1 && ifLastCharProcent === -1) {
      withOutProcentChar = withOutProcentChar.substring(0, withOutProcentChar.length - 1);
    }
    return `${withOutProcentChar}%`;
  };

  const handleChange = (event) => {
    event.preventDefault();
    const { value, keyCode } = event.target;
    formattedValue = formatValue(value, keyCode);
    if (formattedValue) {
      input.onChange(formattedValue);
    } else {
      input.onChange('');
    }
  };

  return (
    <input
      name={input.name}
      value={input.value}
      className={className}
      disabled={disabled}
      onChange={handleChange}
      onBlur={input.onBlur}
      autoComplete="nope"
    />
  );
};

// Props
PdfInputWithProcentChar.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  ...fieldPropTypes
};

PdfInputWithProcentChar.defaultProps = {
  className: undefined,
  disabled: false
};

export default PdfInputWithProcentChar;
