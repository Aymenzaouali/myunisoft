import React from 'react';
import _ from 'lodash';
import { TextField, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import stylesReduxDayPicker from './ReduxDayPickerCell.module.scss';

const styles = () => ({
  day: {
    '& div': {
      fontSize: 12
    },
    width: 20,
    '& input': {
      textAlign: 'center'
    }
  },
  underlineBorder: {
    '&:before': {
      'border-bottom': 'none'
    },
    '&:after': {
      'border-bottom': 'none'
    },
    '&&&&:hover:before': {
      'border-bottom': 'none'
    }
  }
});

class ReduxDayPickerCell extends React.PureComponent {
  render() {
    const day = _.get(this.props, `${this.props.names[0]}`); //eslint-disable-line
    const {
      onDayBlur = () => {},
      border,
      classes,
      customRef,
      className,
      onKeyDown
    } = this.props;

    const {
      underlineBorder
    } = classes;
    return (
      <div>
        <div className={classNames(stylesReduxDayPicker.reduxDayPicker__container, className)}>
          <div>
            <TextField
              InputProps={{
                classes: border && { underline: underlineBorder }
              }}
              className={classes.day}
              {...day.input}
              onChange={(val) => {
                day.input.onChange(val);
              }}
              type="text"
              min="01"
              max="31"
                inputProps={{ // eslint-disable-line
                maxLength: 2
              }}
              placeholder="jj"
              onBlur={(val) => {
                day.input.onBlur();
                onDayBlur(val);
              }}
              inputRef={(inputElement) => { this.day = inputElement; customRef(inputElement); }}
              onKeyDown={onKeyDown}
            />
          </div>
        </div>
        <div>
          {day.meta.touched && ((day.meta.error && <Typography color="error">{day.meta.error}</Typography>) || (day.meta.warning && <Typography>{day.meta.warning}</Typography>))}
        </div>
      </div>
    );
  }
}

ReduxDayPickerCell.defaultProps = {
  onDayBlur: () => {},
  switchToNextInput: () => {},
  customRef: () => {},
  meta: {},
  border: false,
  input: {},
  classes: '',
  className: {},
  onKeyDown: () => {}
};

ReduxDayPickerCell.propTypes = {
  onDayBlur: PropTypes.func,
  switchToNextInput: PropTypes.func,
  customRef: PropTypes.func,
  meta: PropTypes.shape({}),
  input: PropTypes.shape({}),
  border: PropTypes.bool,
  classes: PropTypes.string,
  className: PropTypes.shape({}),
  onKeyDown: PropTypes.func
};

export default withStyles(styles, { withTheme: true })(ReduxDayPickerCell);
