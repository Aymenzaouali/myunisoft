import React, { useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import { borderInputStyles } from 'assets/theme/styles';

const PdfCheckbox = (props) => {
  const {
    input,
    input: { name = '', value },
    onCustomChange,
    ...customProps
  } = props;

  const [checkBoxValue, setInputValue] = useState(false);

  useEffect(() => {
    setInputValue(Number(value));
  }, [value]);

  const style = {
    width: '15px'
  };

  return (
    <input
      onChange={(event) => {
        const isChecked = event.target?.checked;
        setInputValue(isChecked);
        onCustomChange(name, isChecked);
      }}
      name={name}
      checked={checkBoxValue}
      {...customProps}
      style={style}
    />
  );
};

PdfCheckbox.defaultProps = {
  onCustomChange: () => {}
};

PdfCheckbox.propTypes = {
  onCustomChange: PropTypes.func,
  input: PropTypes.shape({}).isRequired
};

export default withStyles(borderInputStyles)(PdfCheckbox);
