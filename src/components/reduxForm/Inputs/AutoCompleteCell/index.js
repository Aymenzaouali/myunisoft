import React, { PureComponent } from 'react';
import classnames from 'classnames';
import AutoCompleteCell from 'components/basics/Cells/AutoCompleteCell';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import { normalizeFieldValue } from 'helpers/normalizeFieldValue';
import _ from 'lodash';
import styles from './AutoCompleteCell.module.scss';

class ReduxAutocompleteCell extends PureComponent {
  componentDidMount() {
    const { loadData } = this.props;
    if (loadData) {
      loadData();
    }
  }

  onBlur = (val) => {
    const { input, onCustomBlur } = this.props;

    if (input.onBlur) {
      input.onBlur();
    }

    if (onCustomBlur) {
      onCustomBlur(val);
    }
  };

  render() {
    const {
      input,
      onCustomBlur, // = () => {},
      meta: {
        active, touched, error, warning
      },
      onChangeValues = () => {},
      options,
      autoFill,
      onCustomFocus,
      onInputChange,
      resetOnWrite,
      prefixed,
      className,
      errorStyle,
      type,
      ...customProps
    } = this.props;

    let { value: filledValue } = input;
    if (
      autoFill
      /**
       * Auto fill only if there is a single
       * choice in the options
       */
      && options.length === 1
      /**
       * Creatable adds __isNew__ to a newly created option
       * We want it to be prioritized over other options
       * i.e. not overridden
       */
      && (!filledValue || !filledValue.__isNew__)
    ) {
      // eslint-disable-next-line prefer-destructuring
      filledValue = options[0];

      if (input.onChange) {
        input.onChange(filledValue);
      }
    }
    // console.log('RAC', options);
    return (
      <div className={classnames(styles.autocomplete, className)}>
        {prefixed
          && (
            <span
              className={filledValue ? styles.notPrefix : styles.prefix}
            >
              {prefixed}
            </span>
          )
        }
        <AutoCompleteCell
          onBlur={this.onBlur}
          onFocus={(e) => {
            if (input.onFocus) input.onFocus(e);
            onCustomFocus(e, input.value);
          }}
          value={filledValue}
          margin="normal"
          options={options}
          menuIsOpen={
            (active && autoFill && options.length > 1 && _.isEmpty(filledValue)) || undefined
          }
          onInputChange={(q) => {
            if (resetOnWrite && !_.isEmpty(q)) {
              input.onChange('');
            }
            onInputChange(q);
          }}
          onChange={(value) => {
            if (input.onChange) {
              input.onChange(normalizeFieldValue(value));
            }
            onChangeValues(normalizeFieldValue(value));
          }}
          // TODO: Pas dans un composant générique, 
          // car du coup ça ne fonctionne que pour Journal && Account !
          // Il faut changer ça. Tu pouvais le faire passer directement
          // par les containers concerner (Diary pour journal et Account pour les comptes)
          getValueLabel={!type ? value => value.code || value.account_number : undefined}
          {...customProps}
        />
        <div style={errorStyle}>
          {touched && ((error && <Typography color="error">{error}</Typography>) || (warning && <Typography>{warning}</Typography>))}
        </div>
      </div>
    );
  }
}

ReduxAutocompleteCell.propTypes = {
  onCustomBlur: PropTypes.func,
  onChangeValues: PropTypes.func,
  onCustomChange: PropTypes.func,
  meta: PropTypes.shape({}),
  input: PropTypes.shape({}),
  border: PropTypes.bool,
  classes: PropTypes.shape({}),
  errorStyle: PropTypes.shape({}),
  loadData: PropTypes.func,
  onCustomFocus: PropTypes.func,
  onInputChange: PropTypes.func,
  resetOnWrite: PropTypes.bool,
  autoFill: PropTypes.bool,
  prefixed: PropTypes.string,
  className: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  type: PropTypes.bool
};

ReduxAutocompleteCell.defaultProps = {
  input: { value: '' },
  className: null,
  onCustomBlur: null,
  onChangeValues: () => {},
  onCustomChange: () => {},
  onCustomFocus: () => {},
  onInputChange: () => {},
  meta: {},
  border: false,
  classes: {},
  errorStyle: {},
  loadData: null,
  autoFill: false,
  resetOnWrite: false,
  prefixed: undefined,
  type: false
};

export default ReduxAutocompleteCell;
