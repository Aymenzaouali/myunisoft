import React, { useState } from 'react';
import { fieldPropTypes } from 'redux-form';
import PropTypes from 'prop-types';
// import { Tooltip } from '@material-ui/core';
import classnames from 'classnames';

import styles from './PdfNumberInput.module.scss';

// Component
const PdfNumberInput = (props) => {
  const {
    input,
    meta,
    className,
    disabled,
    max,
    min,
    error,
    formName,
    setError,
    noformat
  } = props;

  // State
  const [minus, setMinus] = useState(false);

  // Functions
  const handleChange = (event) => {
    event.preventDefault();

    // Check if accepted value
    const { value, name } = event.target;
    // TODO: What is setError for ?
    if (error && value && setError) setError(formName, name, false);

    // eslint-disable-next-line no-restricted-globals
    if (value === '' || !isNaN(+value)) {
      input.onChange(value);
    }
    setMinus(value === '-');
  };
  // Format value
  let formatted = input.value;
  let maxLength = 10;
  if (max) {
    maxLength = max;
  }
  if (formatted.length > maxLength) {
    formatted = formatted.slice(0, maxLength);
  }
  if (!noformat && !meta.active) {
    if (formatted < 0) formatted = Math.round(parseFloat(formatted) * -1) * -1;
    formatted = Math.round(parseFloat(formatted));
    // eslint-disable-next-line no-restricted-globals
    if (isNaN(formatted)) formatted = '';
    else if (formatted < min) input.onChange('');
    else if (formatted < 0) formatted = `(${Math.abs(formatted).toLocaleString()})`;
    else formatted = formatted.toLocaleString();
  }

  if (!noformat && meta.active && minus) {
    formatted = '-';
  }
  const focus = () => {
    if (!noformat && parseFloat(formatted) === 0) {
      formatted = '';
      input.onChange(formatted);
    }
    input.onFocus();
  };

  const classError = error ? styles.error : '';
  // Rendering
  return (
    <input
      name={input.name}
      value={formatted}
      className={classnames(className, classError)}
      disabled={disabled}
      onChange={handleChange}
      onFocus={focus}
      onBlur={input.onBlur}
      autoComplete="nope"
      title={error}
    />
  );
};

// Props
PdfNumberInput.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,

  ...fieldPropTypes
};

PdfNumberInput.defaultProps = {
  className: undefined,
  disabled: false
};

export default PdfNumberInput;
