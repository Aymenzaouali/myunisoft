import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const defaultStyles = {
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    fontSize: '15px',
    display: 'flex',
    alignItems: 'center',
    height: '100%'
  },
  date: {
    flexGrow: '1',
    textAlign: 'center'
  },
  month: {
    flexGrow: '1',
    textAlign: 'center'
  },
  year: {
    flexGrow: '2',
    textAlign: 'center'
  }
};

export const PdfDateInputFormat = ({
  input: { value },
  className,
  data_format: format,
  separator,
  customStyles
}) => {
  const containerStyles = { ...defaultStyles, ...customStyles };
  const date = moment(value).format(format);
  const dates = date.split(separator);
  const styleIdentefer = {
    YYYY: 'year',
    MM: 'month',
    DD: 'date'
  };
  const formatValues = format.split(separator);
  // TODO implement Separator

  const renderDateItems = (item, index) => (
    <div style={containerStyles[styleIdentefer[item]]}>
      {dates[index]}
    </div>
  );
  const renderItems = () => {
    const validDate = moment(value).isValid();
    // check if data returned from beck, not showing Invalid date 
    if (validDate) {
      return formatValues.map(renderDateItems);
    }
    return false;
  };


  return (
    <div
      className={className}
    >
      <div style={containerStyles.itemContainer}>
        {renderItems()}
      </div>
    </div>
  );
};

PdfDateInputFormat.propTypes = {
  customStyles: PropTypes.object,
  input: PropTypes.object.isRequired,
  className: PropTypes.string.isRequired,
  separator: PropTypes.string,
  data_format: PropTypes.string.isRequired

};
PdfDateInputFormat.defaultProps = {
  customStyles: {},
  separator: ' '
};

export default PdfDateInputFormat;
