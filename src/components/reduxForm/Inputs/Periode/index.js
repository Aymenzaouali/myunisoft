import React, { Fragment } from 'react';
import I18n from 'assets/I18n';
import { findDate } from 'helpers/date';
import PropTypes from 'prop-types';
import { ExerciceSelect, ExerciceSelectExport } from 'containers/reduxForm/Inputs';
import { ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';
import moment from 'moment';
import { Field, fieldPropTypes } from 'redux-form';
import { findExercicesByDateRange } from 'helpers/exercices';
import styles from './Periode.module.scss';

class Periode extends React.Component {
  // Function return
  changeExercice = (value) => {
    const {
      changeDateStart,
      changeDateEnd,
      societyId
    } = this.props;

    const date = findDate(value);
    changeDateStart(date.start, societyId);
    changeDateEnd(date.end, societyId);
  };

  // Function return
  changeDate = (border, value) => {
    const {
      changeDateStart,
      changeDateEnd,
      updateSelectedExercices,
      dateStart,
      dateEnd,
      exercicesData
    } = this.props;

    if (!value) {
      updateSelectedExercices([]);
      return;
    }


    if (border === 'start' && value) {
      const newDateStart = moment(value).format('YYYYMMDD');
      changeDateStart(newDateStart);
      const exercices = findExercicesByDateRange(exercicesData, newDateStart, dateEnd);
      updateSelectedExercices(exercices);
    }
    if (border === 'end' && value) {
      const newDateEnd = moment(value).format('YYYYMMDD');
      const exercices = findExercicesByDateRange(exercicesData, dateStart, newDateEnd);
      changeDateEnd(newDateEnd);
      updateSelectedExercices(exercices);
    }
  };

  // Render ExerciceSelect
  exercice = props => (
    <ExerciceSelect
      {...props}
      onChangeValues={this.changeExercice}
      isClearable
    />
  );

  // Render ExerciceSelectExport
  exerciceExport = props => (
    <ExerciceSelectExport
      {...props}
      onChangeValues={this.changeExercice}
      isClearable
    />
  );

  render() {
    const {
      displayExercice,
      row,
      leftDatePicker,
      rightDatePicker,
      separator,
      hidePeriodDate,
      exerciceField,
      menuPosition,
      isMulti,
      disableDateSelection,
      exportField
    } = this.props;

    // align date pickers labels with exercise autocomplete label
    const InputProps = {};
    if (displayExercice) {
      InputProps.style = { minHeight: 36 };
    }

    return (
      <Fragment>
        {
          !hidePeriodDate && (
            <div className={row ? styles.container : null}>
              <Field
                component={ReduxMaterialDatePicker}
                name="dateStart"
                onChangeCustom={(value) => {
                  this.changeDate('start', value);
                }}
                label={I18n.t('consulting.filter.from')}
                disabled={disableDateSelection}
                className={leftDatePicker}
              />
              {separator && <div className={styles.separator} />}
              <Field
                component={ReduxMaterialDatePicker}
                name="dateEnd"
                onChangeCustom={(value) => {
                  this.changeDate('end', value);
                }}
                label={I18n.t('consulting.filter.to')}
                disabled={disableDateSelection}
                className={rightDatePicker}
              />
            </div>
          )
        }
        {
          displayExercice
          && (
            <Field
              component={exportField ? this.exerciceExport : this.exercice}
              name="exercice"
              className={exerciceField}
              menuPosition={menuPosition}
              isMulti={isMulti}
              exportField={exportField}
            />
          )
        }
      </Fragment>
    );
  }
}

Periode.defaultProps = {
  getExercice: () => {},
  updateSelectedExercices: () => {},
  onChangeValues: () => {},
  changeDateStart: () => {},
  changeDateEnd: () => {},
  border: false,
  hidePeriodDate: false,
  classes: {},
  societyId: '',
  formData: '',
  displayExercice: false,
  disableDateSelection: false,
  exercice: {},
  row: false,
  leftDatePicker: {},
  rightDatePicker: {},
  separator: false,
  isMulti: true,
  menuPosition: 'absolute', // default react-select value,
  exercicesData: [],
  dateStart: '',
  dateEnd: ''
};

Periode.propTypes = {
  separator: PropTypes.bool,
  leftDatePicker: PropTypes.object,
  rightDatePicker: PropTypes.object,
  row: PropTypes.bool,
  hidePeriodDate: PropTypes.bool,
  getExercice: PropTypes.func,
  updateSelectedExercices: PropTypes.func,
  dateStart: PropTypes.string,
  dateEnd: PropTypes.string,
  onChangeValues: PropTypes.func,
  changeDateStart: PropTypes.func,
  changeDateEnd: PropTypes.func,
  border: PropTypes.bool,
  classes: PropTypes.shape({
    dateField: PropTypes.string
  }),
  societyId: PropTypes.string,
  formData: PropTypes.string,
  displayExercice: PropTypes.bool,
  disableDateSelection: PropTypes.bool,
  exercice: PropTypes.object,
  menuPosition: PropTypes.string,
  isMulti: PropTypes.bool,
  exercicesData: PropTypes.array,
  ...fieldPropTypes
};

export default Periode;
