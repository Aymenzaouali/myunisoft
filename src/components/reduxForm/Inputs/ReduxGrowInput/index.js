import React from 'react';
import PropTypes from 'prop-types';
import ContentEditable from 'react-contenteditable';

const ReduxGrowInput = (props) => {
  const {
    input, // eslint-disable-line
    customRef,
    onCustomChange = () => {},
    onCustomBlur = () => {},
    ...otherProps
  } = props; // eslint-disable-line

  return (
    <ContentEditable
      innerRef={customRef}
      html={input.value}
      onChange={(e) => {
        const shouldChange = onCustomChange(e);
        if (shouldChange) input.onChange(e.target.value);
      }}
      {...otherProps}
      onBlur={() => {
        onCustomBlur();
      }}
    />
  );
};

ReduxGrowInput.propTypes = {
  customRef: PropTypes.node,
  onCustomChange: PropTypes.func.isRequired,
  onCustomBlur: PropTypes.func.isRequired
};

ReduxGrowInput.defaultProps = {
  customRef: null
};

export default ReduxGrowInput;
