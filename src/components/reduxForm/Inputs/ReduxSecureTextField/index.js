import React, { useState } from 'react';
import { fieldPropTypes } from 'redux-form';
import PropTypes from 'prop-types';
import {
  FormControl, FormHelperText,
  Input, InputLabel, InputAdornment,
  IconButton
} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

// Component
const ReduxSecureTextField = (props) => {
  const {
    id, label,
    input, meta,
    ...custom
  } = props;

  // State
  const [show, setShow] = useState(false);

  // Rendering
  return (
    <FormControl margin="normal" {...custom}>
      <InputLabel htmlFor={id}>{ label }</InputLabel>
      <Input
        id={id}
        type={show ? 'text' : 'password'}
        value={input.value}

        onChange={input.onChange}
        onBlur={input.onBlur}

        endAdornment={(
          <InputAdornment position="end">
            <IconButton onClick={() => setShow(!show)}>
              { show ? <VisibilityOff /> : <Visibility /> }
            </IconButton>
          </InputAdornment>
        )}
      />
      { meta.touched && (meta.error || meta.warning) && (
        <FormHelperText error={!!meta.error}>
          { meta.error || meta.warning }
        </FormHelperText>
      ) }
    </FormControl>
  );
};

// Props
ReduxSecureTextField.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,

  ...fieldPropTypes
};

export default ReduxSecureTextField;
