import React, { useState, useEffect } from 'react';
import DatePicker, { registerLocale } from 'react-datepicker';
import fr from 'date-fns/locale/fr';
import moment from 'moment';
import PropTypes from 'prop-types';

import './styles.scss';

registerLocale('fr', fr);

const dateFormatToSave = 'YYYY-MM-DD';

const isDate = (date) => {
  const dateArr = date ? date.split('-') : [];
  return dateArr.length === 3 && dateArr[0].length === 4;
};

const ReduxPicker = (props) => {
  const {
    handleChange,
    input: { name = '', value = null },
    input,
    className,
    disabled = false,
    isClearable = false,
    isError,
    dateFormatToShow
  } = props;

  const [valueToShow, setValueToShow] = useState(null);
  const [selected, setSelected] = useState(null);

  useEffect(() => {
    const valueValidated = isDate(value) ? moment(value).format(dateFormatToShow) : null;
    const selectedValidated = isDate(value) ? new Date(value) : null;
    setValueToShow(valueValidated);

    setSelected(selectedValidated);
  }, [value]);

  const change = (date) => {
    handleChange(name, date ? moment(date).format(dateFormatToSave) : '');
    setSelected(date);
    setValueToShow(date ? moment(date).format(dateFormatToShow) : null);
  };

  return (
    <div className={`redux-picker-wrap ${className} ${isError ? 'error' : ''}`}>
      <DatePicker
        {...input}
        onChange={date => change(date)}
        onBlur={() => {}}
        dateFormat={dateFormatToShow}
        locale="fr"
        selected={selected}
        isClearable={isClearable}
        value={valueToShow}
        disabled={disabled}
        autoComplete="nope"
      />
    </div>
  );
};

ReduxPicker.defaultProps = {
  handleChange: () => {},
  input: {},
  className: {},
  disabled: false,
  isClearable: false,
  isError: false,
  dateFormatToShow: 'DD/MM/YYYY'
};

ReduxPicker.propTypes = {
  handleChange: PropTypes.func,
  dateFormatToShow: PropTypes.func,
  input: PropTypes.object,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  isClearable: PropTypes.bool,
  isError: PropTypes.bool
};

export default ReduxPicker;
