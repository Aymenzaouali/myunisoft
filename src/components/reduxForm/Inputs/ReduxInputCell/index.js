import React from 'react';
import { Typography, Input, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import stylesReduxInputCell from './ReduxInputCell.module.scss';

const ReduxInputCell = (props) => {
  const {
    input,
    disabled,
    onCustomBlur = () => {},
    onCustomFocus = () => {},
    onCustomChange = () => {},
    meta: { touched, error, warning },
    border,
    classes,
    ...customProps
  } = props;

  const {
    underline,
    root,
    notUnderline
  } = classes;

  return (
    <div>
      <Input
        disabled={disabled}
        className={stylesReduxInputCell.unstyleAppearance}
        classes={{
          root,
          underline: classnames({ [underline]: border }, { [notUnderline]: disabled })
        }}
        onChange={(event) => {
          input.onChange(event);
          onCustomChange(event);
        }}
        onFocus={() => { onCustomFocus(); }}
        value={input.value}
        {...customProps}
        onBlur={(a) => {
          input.onBlur(a);
          onCustomBlur(a);
        }}
      />
      <div>
        {touched && ((error && <Typography color="error">{error}</Typography>) || (warning && <Typography>{warning}</Typography>))}
      </div>
    </div>
  );
};

const styles = () => ({
  root: {
    fontSize: 12,
    width: 72
  },
  underline: {
    '&:before': {
      'border-bottom': 'none'
    },
    '&:after': {
      'border-bottom': 'none'
    },
    '&&&&:hover:before': {
      'border-bottom': 'none'
    }
  },
  indicator: {
    display: 'none',
    '-webkit-appearance': 'none'
  },
  notUnderline: {
    '&:before': {
      'border-bottom': 'none'
    },
    '&&&&:hover:before': {
      'border-bottom': 'none'
    }
  }
});

ReduxInputCell.defaultProps = {
  onCustomBlur: () => {},
  onCustomFocus: () => {},
  onCustomChange: () => {},
  meta: {},
  border: false,
  classes: {},
  disabled: false,
  customProps: {}
};

ReduxInputCell.propTypes = {
  onCustomBlur: PropTypes.func,
  onCustomFocus: PropTypes.func,
  onCustomChange: PropTypes.func,
  meta: PropTypes.shape({}),
  input: PropTypes.shape({}).isRequired,
  border: PropTypes.bool,
  classes: PropTypes.shape({}),
  disabled: PropTypes.bool,
  customProps: PropTypes.shape({})
};

export default withStyles(styles)(ReduxInputCell);
