import React from 'react';
import PropTypes from 'prop-types';
import { fieldPropTypes } from 'redux-form';

import { DatePicker } from 'components/basics/Inputs';
import { AbstractIcon } from 'components/basics/Icon';
import { Tooltip, withStyles } from '@material-ui/core';

// Component
const ReduxMaterialDatePicker = (props) => {
  const {
    input, meta,
    onChangeCustom,
    disabled,
    infoMessage,
    classes,

    ...customProps
  } = props;

  // Rendering
  const hasError = (meta.dirty || meta.submitFailed) && !!meta.error;
  const helperText = meta.error || meta.warning;

  return (
    <div className={classes.wrapper}>
      <DatePicker
        value={input.value}

        autoOk
        clearable
        disabled={disabled}

        error={hasError}
        helperText={hasError && helperText}

        {...customProps}

        onChange={(value) => {
          input.onChange(value);
          onChangeCustom(value);
        }}
      />
      {
        infoMessage && (
          <div className={classes.infoMessage}>
            <Tooltip
              title={infoMessage}
              placement="top-start"
              classes={{ tooltip: classes.tooltip }}
            >
              <AbstractIcon
                iconName="icon-account-info"
                iconSize={19}
                classesButton={{ root: classes.button }}
              />
            </Tooltip>
          </div>
        )
      }
    </div>
  );
};

ReduxMaterialDatePicker.propTypes = {
  onChangeCustom: PropTypes.func,
  disabled: PropTypes.bool,

  ...fieldPropTypes
};

ReduxMaterialDatePicker.defaultProps = {
  onChangeCustom: () => {},
  disabled: false
};

const theme = () => ({
  button: {
    backgroundColor: '#00d1d2',
    padding: 0,
    borderRadius: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    transition: '0.3s all ease',

    '&:hover': {
      backgroundColor: '#d0d0d0'
    }
  },
  wrapper: {
    display: 'flex',
    alignItems: 'inherit'
  },
  infoMessage: {
    marginLeft: 10,
    marginBottom: 10
  },
  tooltip: {
    maxWidth: 250
  }
});

export default withStyles(theme)(ReduxMaterialDatePicker);
