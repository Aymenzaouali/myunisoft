This component is a material date picker adaptation for redux-form Field usage.

Example

```js
  const MomentUtils = require('@date-io/moment');
  const MuiPickersUtilsProvider = require('material-ui-pickers/MuiPickersUtilsProvider');

  <MuiPickersUtilsProvider>
    <ReduxMaterialDatePicker
      name="accounting_date"
      input={{onBlur: () => {}, onChange: () => {}}}
      meta={{}}
    />
  </MuiPickersUtilsProvider>
```