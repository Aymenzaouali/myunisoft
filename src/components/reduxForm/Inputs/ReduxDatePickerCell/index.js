import React from 'react';
import _ from 'lodash';
import { TextField, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Shortcuts } from 'react-shortcuts';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import stylesReduxDatePicker from './ReduxDatePickerCell.module.scss';

const styles = () => ({
  day: {
    '& div': {
      fontSize: 12
    },
    width: 20,
    '& input': {
      textAlign: 'center'
    }
  },
  month: {
    width: 21,
    '& input': {
      textAlign: 'center'
    },
    '& div': {
      fontSize: 12
    }
  },
  year: {
    width: 30,
    '& input': {
      textAlign: 'center'
    },
    '& div': {
      fontSize: 12
    }
  },
  underlineBorder: {
    '&:before': {
      'border-bottom': 'none'
    },
    '&:after': {
      'border-bottom': 'none'
    },
    '&&&&:hover:before': {
      'border-bottom': 'none'
    }
  }
});

class ReduxDatePickerCell extends React.PureComponent {
  render() {
    const day = _.get(this.props, `${this.props.names[0]}`); //eslint-disable-line
    const month = _.get(this.props, `${this.props.names[1]}`); //eslint-disable-line
    const year = _.get(this.props, `${this.props.names[2]}`); //eslint-disable-line
    const {
      onDayBlur = () => {},
      onMonthBlur = () => {},
      onYearBlur = () => {},
      border,
      classes,
      customRef,
      switchToNextInput,
      className
    } = this.props;

    const {
      underlineBorder
    } = classes;
    return (
      <div>
        <div className={classNames(stylesReduxDatePicker.reduxDatePicker__container, className)}>
          <div>
            <Shortcuts
              handler={(action) => { if (action === 'ENTER') { this.month.focus(); this.month.select(); } }}
              name="NEW_ACCOUNTING"
              alwaysFireHandler
              stopPropagation={false}
              preventDefault={false}
            >
              <TextField
                InputProps={{
                  classes: border && { underline: underlineBorder }
                }}
                className={classes.day}
                {...day.input}
                onChange={(val) => {
                  day.input.onChange(val);
                  if (val.target.value.length === 2
              && !(this.day.selectionStart === 0 && this.day.selectionEnd === 2)) {
                    setTimeout(() => this.month.focus(), 0);
                    setTimeout(() => this.month.select(), 0);
                  }
                }}
                type="text"
                min="01"
                max="31"
                inputProps={{ // eslint-disable-line
                  maxLength: 2
                }}
                placeholder="jj"
                onBlur={(val) => {
                  day.input.onBlur();
                  onDayBlur(val);
                }}
                inputRef={(inputElement) => { this.day = inputElement; customRef(inputElement); }}
              />
            </Shortcuts>
          </div>
          <span className={stylesReduxDatePicker.reduxDatePicker__date_separator}>/</span>
          <div>
            <Shortcuts
              handler={(action) => { if (action === 'ENTER') { this.year.focus(); this.year.select(); } }}
              name="NEW_ACCOUNTING"
              alwaysFireHandler
              stopPropagation={false}
              preventDefault={false}
            >
              <TextField
                InputProps={{
                  classes: border && { underline: underlineBorder }
                }}
                className={classes.month}
                {...month.input}
                onChange={(val) => {
                  month.input.onChange(val);
                  if (val.target.value.length === 2
              && !(this.month.selectionStart === 0 && this.month.selectionEnd === 2)) {
                    setTimeout(() => this.year.focus(), 0);
                    setTimeout(() => this.year.select(), 0);
                  }
                }}
                type="text"
                min="01"
                max="12"
                inputProps={{ // eslint-disable-line
                  maxLength: 2
                }}
                placeholder="mm"
                onBlur={(val) => {
                  month.input.onBlur();
                  onMonthBlur(val);
                }}
                inputRef={(inputElement) => { this.month = inputElement; }}
              />

            </Shortcuts>
          </div>
          <span className={stylesReduxDatePicker.reduxDatePicker__date_separator}>/</span>
          <div>
            <Shortcuts
              handler={(action) => { if (action === 'ENTER') { switchToNextInput(); } }}
              name="NEW_ACCOUNTING"
              alwaysFireHandler
              stopPropagation={false}
              preventDefault={false}
            >
              <TextField
                InputProps={{
                  classes: border && { underline: underlineBorder }
                }}
                className={classes.year}
                {...year.input}
                type="text"
                inputProps={{ // eslint-disable-line
                  maxLength: 4
                }}
                onChange={(val) => {
                  year.input.onChange(val);
                }}
                placeholder="aaaa"
                onBlur={(e) => {
                  let value = e.target.value; // eslint-disable-line
                  if (value.length === 2) {
                    const yConverted = moment(value, 'YY');
                    if (yConverted.isValid()) {
                      value = yConverted.format('YYYY');
                    }
                  }
                  year.input.onChange(value);
                  year.input.onBlur();
                  onYearBlur(value);
                }}
                inputRef={(inputElement) => { this.year = inputElement; }}
              />

            </Shortcuts>
          </div>
        </div>
        <div>
          {day.meta.touched && ((day.meta.error && <Typography color="error">{day.meta.error}</Typography>) || (day.meta.warning && <Typography>{day.meta.warning}</Typography>))}
        </div>
        <div>
          {month.meta.touched && ((month.meta.error && <Typography color="error">{month.meta.error}</Typography>) || (month.meta.warning && <Typography>{month.meta.warning}</Typography>))}
        </div>
        <div>
          {year.meta.touched && ((year.meta.error && <Typography color="error">{year.meta.error}</Typography>) || (year.meta.warning && <Typography>{year.meta.warning}</Typography>))}
        </div>
      </div>
    );
  }
}

ReduxDatePickerCell.defaultProps = {
  onDayBlur: () => {},
  switchToNextInput: () => {},
  customRef: () => {},
  onMonthBlur: () => {},
  onYearBlur: () => {},
  meta: {},
  border: false,
  input: {},
  classes: '',
  className: {}
};

ReduxDatePickerCell.propTypes = {
  onDayBlur: PropTypes.func,
  switchToNextInput: PropTypes.func,
  customRef: PropTypes.func,
  onMonthBlur: PropTypes.func,
  onYearBlur: PropTypes.func,
  meta: PropTypes.shape({}),
  input: PropTypes.shape({}),
  border: PropTypes.bool,
  classes: PropTypes.string,
  className: PropTypes.shape({})
};

export default withStyles(styles, { withTheme: true })(ReduxDatePickerCell);
