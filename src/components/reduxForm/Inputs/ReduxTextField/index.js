import React from 'react';
import { fieldPropTypes } from 'redux-form';
import PropTypes from 'prop-types';
import {
  TextField, withStyles
} from '@material-ui/core';
import _ from 'lodash';
import {
  InputLabelNoWrap,
  borderInputStyles
} from 'assets/theme/styles';
import './ReduxTextField.module.scss';

// Component
const ReduxTextField = (props) => {
  const {
    label, error,
    disabled,
    border,
    className, classes,
    input,
    meta = {},
    InputProps,
    onCustomBlur,
    onCustomChange,
    inputRef,
    errorStyle,
    shrink,
    ...customProps
  } = props;
  const shouldLabelShrink = !_.isEmpty(input.value) || typeof input.value === 'number' || shrink;
  const {
    focusedInputBorder: focused,
    underlineBorder: underline,
    inputLabelRootOverride
  } = classes;

  let inputPropsClasses = {};
  if (border) {
    inputPropsClasses = { focused, underline };
  }

  const hasError = (meta.dirty || meta.submitFailed || meta.touched) && !!(meta.error || error);
  const helperText = meta.error || error || meta.warning;

  return (
    <TextField
      inputRef={inputRef}
      label={label}
      value={input.value}
      disabled={disabled}

      error={hasError}
      helperText={hasError && helperText}

      className={className}
      margin="normal"

      {...customProps}
      FormHelperTextProps={{
        classes: { error: errorStyle },
        style: typeof errorStyle === 'object' ? errorStyle : {}
      }}
      InputLabelProps={{
        classes: { root: inputLabelRootOverride },
        shrink: shouldLabelShrink
      }}
      InputProps={{
        classes: inputPropsClasses,
        ...InputProps
      }}

      onFocus={() => input.onFocus()}

      onChange={(event) => {
        const { target: { value } } = event;
        input.onChange(event);
        onCustomChange(value);
      }}
      onBlur={(event) => {
        input.onBlur(event);
        onCustomBlur(event);
      }}
    />
  );
};

// Props
ReduxTextField.propTypes = {
  onCustomBlur: PropTypes.func,
  onCustomChange: PropTypes.func,
  className: PropTypes.string,
  border: PropTypes.bool,
  disabled: PropTypes.bool,
  classes: PropTypes.shape({}).isRequired,
  InputProps: PropTypes.shape({}),
  label: PropTypes.string,
  error: PropTypes.string,
  errorStyle: PropTypes.shape({}),

  ...fieldPropTypes
};

ReduxTextField.defaultProps = {
  errorStyle: {},
  className: '',
  border: false,
  disabled: false,
  InputProps: {},
  label: '',
  error: undefined,

  onCustomBlur: () => {},
  onCustomChange: () => {}
};

export default withStyles({
  ...borderInputStyles(),
  ...InputLabelNoWrap
})(ReduxTextField);
