import React from 'react';
import PropTypes from 'prop-types';
import { fieldPropTypes } from 'redux-form';

import { RangeDatePicker } from 'components/basics/Inputs';

// Component
const ReduxMaterialRangeDatePicker = (props) => {
  const {
    input, meta,
    onChangeCustom,
    disabled,

    ...customProps
  } = props;

  // Rendering
  const hasError = (meta.dirty || meta.submitFailed) && !!meta.error;
  const helperText = meta.error || meta.warning;

  return (
    <RangeDatePicker
      value={input.value}

      clearable
      disabled={disabled}

      error={hasError}
      helperText={hasError && helperText}

      {...customProps}

      onChange={(value) => {
        input.onChange(value);
        onChangeCustom(value);
      }}

      onBlur={(value) => {
        input.onBlur(value);
        input.onChange(value);
        onChangeCustom(value);
      }}
    />
  );
};

ReduxMaterialRangeDatePicker.propTypes = {
  onChangeCustom: PropTypes.func,
  disabled: PropTypes.bool,

  ...fieldPropTypes
};

ReduxMaterialRangeDatePicker.defaultProps = {
  onChangeCustom: () => {},
  disabled: false
};

export default ReduxMaterialRangeDatePicker;
