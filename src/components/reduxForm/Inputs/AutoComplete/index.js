import React, { PureComponent } from 'react';
import classnames from 'classnames';
import AutoComplete from 'components/basics/Inputs/AutoComplete';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import { normalizeFieldValue } from 'helpers/normalizeFieldValue';
import _ from 'lodash';
import styles from './AutoComplete.module.scss';

class ReduxAutocomplete extends PureComponent {
  componentDidMount() {
    const { loadData } = this.props;
    if (loadData) {
      loadData();
    }
  }

  onBlur = (val) => {
    const { input, onCustomBlur } = this.props;

    if (input.onBlur) {
      input.onBlur();
    }

    if (onCustomBlur) {
      onCustomBlur(val);
    }
  };

  render() {
    const {
      input,
      onCustomBlur, // = () => {},
      meta: {
        active, touched, error, warning
      },
      onChangeValues = () => {},
      options,
      autoFill,
      onCustomFocus,
      onInputChange,
      resetOnWrite,
      prefixed,
      className,
      errorStyle,
      onDoubleClick,
      ...customProps
    } = this.props;

    let { value: filledValue } = input;
    if (
      autoFill
      /**
       * Auto fill only if there is a single
       * choice in the options
       */
      && options.length === 1
      /**
       * Creatable adds __isNew__ to a newly created option
       * We want it to be prioritized over other options
       * i.e. not overridden
       */
      && (!filledValue || !filledValue.__isNew__)
    ) {
      // eslint-disable-next-line prefer-destructuring
      filledValue = options[0];

      if (input.onChange) {
        input.onChange(filledValue);
      }
    }
    return (
      <div className={classnames(styles.autocomplete, className)}>
        {prefixed
          && (
            <span
              className={filledValue ? styles.notPrefix : styles.prefix}
            >
              {prefixed}
            </span>
          )
        }
        <AutoComplete
          onBlur={this.onBlur}
          onFocus={(e) => {
            if (input.onFocus) input.onFocus(e);
            onCustomFocus(e, input.value);
          }}
          value={filledValue}
          margin="normal"
          options={options}
          onDoubleClick={onDoubleClick}
          {...customProps}
          menuIsOpen={
            (active && autoFill && options.length > 1 && _.isEmpty(filledValue)) || undefined
          }
          onInputChange={(q) => {
            if (resetOnWrite && !_.isEmpty(q)) {
              input.onChange('');
            }
            onInputChange(q);
          }}
          onChangeValues={(value) => {
            if (input.onChange) {
              input.onChange(normalizeFieldValue(value));
            }
            onChangeValues(normalizeFieldValue(value));
          }}
        />
        <div style={errorStyle}>
          {touched && ((error && <Typography color="error">{error}</Typography>) || (warning && <Typography>{warning}</Typography>))}
        </div>
      </div>
    );
  }
}

ReduxAutocomplete.propTypes = {
  onCustomBlur: PropTypes.func,
  onChangeValues: PropTypes.func,
  onCustomChange: PropTypes.func,
  meta: PropTypes.shape({}),
  input: PropTypes.shape({}),
  border: PropTypes.bool,
  classes: PropTypes.shape({}),
  errorStyle: PropTypes.shape({}),
  loadData: PropTypes.func,
  onCustomFocus: PropTypes.func,
  onInputChange: PropTypes.func,
  resetOnWrite: PropTypes.bool,
  autoFill: PropTypes.bool,
  prefixed: PropTypes.string,
  className: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  maxHeight: PropTypes.number,
  onDoubleClick: PropTypes.func
};

ReduxAutocomplete.defaultProps = {
  input: { value: '' },
  className: null,
  onCustomBlur: null,
  onChangeValues: () => {},
  onCustomChange: () => {},
  onCustomFocus: () => {},
  onInputChange: () => {},
  meta: {},
  border: false,
  classes: {},
  errorStyle: {},
  loadData: null,
  autoFill: false,
  resetOnWrite: false,
  prefixed: undefined,
  maxHeight: 250,
  onDoubleClick: () => {}
};

export default ReduxAutocomplete;
