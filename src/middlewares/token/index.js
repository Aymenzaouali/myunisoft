// eslint-disable-next-line import/no-named-as-default
import LoginService from 'common/redux/login';
import {
  GET_TOKEN_ATTEMPT,
  SIGN_IN_ATTEMPT,
  DELETE_PUSH_TO_SERVER_ATTEMPT
} from 'common/redux/login/constants';
import { GET_GROUPMEMBER_ATTEMPT } from 'common/redux/group/constants';
import _ from 'lodash';
import moment from 'moment';

const ACTIONS_TO_SKIP = [
  GET_TOKEN_ATTEMPT,
  SIGN_IN_ATTEMPT,
  GET_GROUPMEMBER_ATTEMPT,
  DELETE_PUSH_TO_SERVER_ATTEMPT
];

const MIN_TIME_BEFORE_REFRESH_TOKEN_IN_MINUTES = 2;
const isTokenExpiringSoon = (expirationDate) => {
  const timeLeftUntilTokenExpirationInMs = moment(expirationDate).diff(moment().utc());
  return moment.duration(timeLeftUntilTokenExpirationInMs)
    .asMinutes() <= MIN_TIME_BEFORE_REFRESH_TOKEN_IN_MINUTES;
};
const shouldTokenBeChecked = typeString => !ACTIONS_TO_SKIP.includes(typeString)
    && typeString.includes('_ATTEMPT');

export function tokenMiddleware({ dispatch, getState }) {
  return next => async (action) => {
    const expirationDate = _.get(getState(), 'login.token.expire_in');
    if (shouldTokenBeChecked(action.type)
    && isTokenExpiringSoon(expirationDate)
    && !ACTIONS_TO_SKIP.includes(action.type)) {
      await dispatch(LoginService.refreshToken());
    }
    return next(action);
  };
}
