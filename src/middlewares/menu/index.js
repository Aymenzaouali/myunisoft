import { generatePath, matchPath } from 'react-router';
import { CALL_HISTORY_METHOD } from 'connected-react-router';
import { get } from 'lodash';

/**
 * Middleware for handle a push of a route pathname without ":id" params initialized,
 * and replace it by the current "match.params.id" location.
 */
export function menuMiddleware(store) {
  return next => (action) => {
    if (action.type === CALL_HISTORY_METHOD
      && action.payload.method === 'push'
      && get(action, 'payload.args[0]', '').includes(':id')
    ) {
      const state = store.getState();
      const { params: { id } } = matchPath(get(state, 'router.location.pathname', ''), { path: '/tab/:id' });

      const newAction = {
        ...action,
        payload: {
          ...action.payload,
          args: action.payload.args.map((arg, i) => (i ? arg : generatePath(arg, { id })))
        }
      };
      return next(newAction);
    }
    return next(action);
  };
}
