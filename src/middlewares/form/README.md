# Form managing middleware
## Description
`formManagingMiddleware` is used to:
- Intercept actions that may in one way or another make the user lose currently made changes to the form e.g. redirection
- Notify the user of some changes that could affect his current form information

If you don't have a specific cancellation logic for your form, the only thing you have to add is
`setCurrentForm()` thunk to your form mounting component or container. This will be cleaned up
later in the middleware. See `CompanyCreation.js` component as an example

## Example
```javascript
/**
 * 1. Add to middlewares
 *
 * Note that because how middlewares work
 * (passing an action through all middlewares
 * from top to bottom), order is important.
 *
 * `formManagingMiddleware` acts as a
 * blocker for react router location
 * change action
 *
 */
applyMiddleware(
  thunk,
  formManagingMiddleware,
);

/**
 * 2. Dispatch closeCurrentFormAction
 * Typically in mapDispatchToProps
 */
const mapDispatchToProps = dispatch => ({
  closeCurrentForm: props => dispatch(closeCurrentFormAction(props))
});

/**
 * 3. Pass props to the thunk
 *
 * Currently, you have to pass only 1-3 props
 * {
 *    expected?: {}; // optional => ?:
 *    discard?: () => void;
 *    formName: string;
 * }
 */

// Real life example
// Company creation form cancel button callback
const cancelForm = () => closeCurrentForm({
  /**
   * Important to have the exact name of the
   * form because the logic for choosing the correct
   * initial values is based solely on the formName
   *
   * Read further to know about discardCb
   */
  formName: 'companyCreationForm',
  discardCb: () => {
    selectCompany();
    initializeForm();
    redirect(routesByKey.companyList);
  },
  /**
   * By default the `expected` value is going
   * to be the forms `initial` value.
   *
   * If you need a different starting form
   * state, then you will pass it here
   *
   * You can read  a more detailed explanation
   * of the reason behind the current approach
   * in the JSDocs of formManagingMiddleware
   */
  expected: selectedCompany
});
```

After dispatching the action above, middleware will dispatch its internal thunk `closeCurrentForm`.
```javascript
// formManagingMiddleware/index.js

const closeCurrentForm = props => (dispatch, getState) => {/* */};

// ...

export function formManagingMiddleware({ dispatch, getState }) {
  return next => (action) => {
    switch (action.type) {
      // ...
      case CLOSE_CURRENT_FORM: {
        const propsObject = {
          // ...
        };

        dispatch(closeCurrentForm(propsObject)
        // ...
      }
    }
  }
}
```



# closeCurrentForm thunk props
There is 1 prop which is an object with the following keys:
- formName
- discardCb
- expected

Some of these are not handled by the user and instead are derived using `formName`.
Read further for more info.

## formName: string;
Used to access various callback maps and access form in redux store

## discardCb?: () => void (optional)
Used for removing values, cleaning up the state or anything else that may affect future usage of the form.

For example, most of the redux forms in MyUnisoft have the config prop `destroyOnUnmount` as `false` which will keep the form data in redux store. In some cases, this might not be the desired effect, but changing `destroyOnUnmount` from `false` to `true` is a too dramatic change. So instead you would use `discardCb`

```
discardCb: () => {
  customThunk();
  anotherCustomThunk();
},
```

## expected?: {} (optional)
Your expected form state. This will be compared to the current state of your form using `state.myForm.values` object.

By default this is set as `state.myForm.initial`

Sometimes you can't pass a custom expected state, but need one. E.g. when **REACT_ROUTER_LOCATION_CHANGE** action is dispatched. In this case, for the form `expected` and `actual` states to be properly compared you have to add a custom path to the location of this custom `expected` state for your form. You have to add the `formName` to `expectedFormValuesPath`.

```
expected: _.get(getState(), expectedFormValuesPath[currentFormName]),
```
