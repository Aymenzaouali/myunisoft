import { LOCATION_CHANGE } from 'connected-react-router';

import _ from 'lodash';
import I18n from 'assets/I18n';

import { getTabState, isChangingTab } from 'helpers/tabs';

import { openDialog } from 'redux/dialogs';
import { CLOSE_TAB } from 'redux/navigation/constants';
import { closeCurrentForm } from 'redux/tabs/form/actions';
import { CLOSE_CURRENT_FORM } from 'redux/tabs/form/constants';
import { reset } from 'redux-form';

/**
 * @author Rolands Jegorovs 06.02.19
 *
 * Thunk to handle closing currently open form closing
 * The logic works by performing a deep equal between
 * the `expected` and `actual` states of a form.
 *
 * By default the expected state is the `initial` value
 * of a form under `store.form.<YourFormName>.initial`
 *
 * You can specify a different expected state by adding
 * it to `expectedFormValuesPath` as key using your formName
 *
 * ⚠️ Ideally, `expected` should always be the `initial` value,
 * however, as of this writing, because of a redux-form bug, this is not possible
 * in certain situations
 * @see https://github.com/erikras/redux-form/issues/2971#issuecomment-459311439
 *
 * @param {() => void} continueCb - Callback to continue action (close form and or navigate)
 * @param {() => void} cancelCb - Callback to cancel action
 * @param {{}} form - navigation form state
 */
const checkForm = (form, continueCb, cancelCb) => {
  const { name, initial, ignore } = form;

  return (dispatch, getState) => {
    const { values: actual } = getState().form[name] || {};

    // Ignored fields
    const filtredI = _.omit(initial, ignore);
    const filtredA = _.omit(actual, ignore);

    // TODO: Investigate and fix pop-up opening when data have not changed.
    // TODO: Add deep omit on falsy values
    // console.log(filtredI, filtredA);
    // function difference(object, base) {
    //   function changes(object, base) {
    //     return _.transform(object, (result, value, key) => {
    //       if (!_.isEqual(value, base[key])) {
    //         result[key] = (_.isObject(value) && _.isObject(base[key])) ?
    //  changes(value, base[key]) : value;
    //       }
    //     });
    //   }
    //   return changes(object, base);
    // }

    // console.log(difference(filtredA, filtredI));

    if (actual === undefined || _.isEqual(filtredI, filtredA)) {
      continueCb();
    } else {
      dispatch(openDialog({
        title: I18n.t('notifications.warning'),
        body: I18n.t('notifications.modificationsNotSaved'),
        buttons: [
          {
            _type: 'string',
            text: I18n.t('notifications.continue'),
            onClick: continueCb,
            color: 'error',
            variant: 'outlined'
          },
          {
            _type: 'string',
            text: I18n.t('notifications.return'),
            onClick: cancelCb,
            color: 'default',
            variant: 'outlined'
          }
        ]
      }));
    }
  };
};

const formManagingMiddleware = ({ dispatch, getState }) => next => (action) => {
  const state = getState();

  switch (action.type) {
  case CLOSE_TAB: {
    // Tab has form ?
    const tab = parseInt(action.id, 10);

    return dispatch(closeCurrentForm(() => next(action), tab));
  }
  case LOCATION_CHANGE: {
    if (!isChangingTab(action, state)) {
      const { id: tab } = state.navigation;
      return dispatch(closeCurrentForm(() => next(action), tab));
    }

    return next(action);
  }
  case CLOSE_CURRENT_FORM: {
    const { form } = getTabState(state, action.tab_id || state.navigation.id);

    if (form !== undefined && !_.isEmpty(form)) {
      dispatch(checkForm(
        form,
        () => {
          next(action);
          if (form.name) {
            dispatch(reset(form.name));
          }
          if (action.continueCb) action.continueCb();
        },
        () => {}
      ));

      return undefined;
    }


    const r = next(action);
    if (action.continueCb) action.continueCb();

    return r;
  }

  default:
    return next(action);
  }
};

export {
  formManagingMiddleware
};
