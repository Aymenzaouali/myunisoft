import { LOCATION_CHANGE } from 'connected-react-router';

import _ from 'lodash';

import { isChangingTab, getTabState } from 'helpers/tabs';

import { CLOSE_TAB } from 'redux/navigation/constants';
import { closeAllSplit } from 'redux/tabs/split/actions';
import { CLOSE_SPLIT, CLOSE_ALL_SPLIT } from 'redux/tabs/split/constants';
import { closeWindow } from 'redux/windows/actions';

// Middleware
const splitMiddleware = ({ dispatch, getState }) => next => (action) => {
  const state = getState();

  switch (action.type) {
  case LOCATION_CHANGE: {
    const { location: { pathname } } = action.payload;

    if (pathname !== window.location.pathname && !isChangingTab(action, state)) {
      dispatch(closeAllSplit());
    }

    break;
  }

  case CLOSE_TAB: {
    const tab = parseInt(action.id, 10);

    dispatch({
      ...closeAllSplit(),
      tab_id: tab
    });

    break;
  }

  case CLOSE_SPLIT: {
    const tab_id = action.tab_id || state.navigation.id;
    const window_id = `${tab_id}_split_${action.master_id}`;

    dispatch(closeWindow(window_id));

    break;
  }

  case CLOSE_ALL_SPLIT: {
    const tab_id = action.tab_id || state.navigation.id;
    const tabstate = getTabState(state, tab_id);

    _.forIn(tabstate.split, (s, master_id) => {
      const window_id = `${tab_id}_split_${master_id}`;

      dispatch(closeWindow(window_id));
    });

    break;
  }

  default:
    break;
  }

  return next(action);
};

export {
  splitMiddleware
};
