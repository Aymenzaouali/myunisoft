import { useEffect } from 'react';
import { roundNumber } from './number';

// Utils
export const roundField = change => (event, value, _, name) => {
  if (value === '') return;
  event.preventDefault();

  const round = Math.round(parseFloat(value));

  if (!isNaN(round)) { // eslint-disable-line no-restricted-globals
    change(name, round);
  }
};

export const isEmpty = (field, values = {}) => !values[field] && true;

export const parse = val => parseFloat(val) || 0;
export const copy = (values, fields) => parse(values[fields[0]]);
export const sum = (values, fields) => fields.reduce(
  // A + B + C + ... (fields ['A', 'B', 'C', ...])
  (result, key) => result + parse(values[key]), 0
);
export const sumOfValues = values => values.reduce((sum, value) => sum + parse(value), 0);

export const sub = (values, fields) => fields.reduce(
  // A - B - C - ... (fields ['A', 'B', 'C', ...])
  (result, key, i) => (i === 0 ? result : result - parse(values[key])), parse(values[fields[0]])
);

// Hooks
export const useCompute = (field, compute, fields, values = {}, change) => {
  useEffect(() => {
    const pval = parseFloat(values[field]);
    const val = compute(values, fields);

    if (val !== pval) {
      if (change) {
        change(field, roundNumber(val));
      }
    }
  }, fields.map(k => values[k]));
};

export const setValue = (form = {}, field, value, change) => {
  useEffect(() => {
    const pval = parseFloat(form[field]);
    const val = parseFloat(value);

    if (val !== pval) {
      change(field, val);
    }
  });
};

export const setIsNothingness = (values, notNeedFields, field, change) => {
  if (values) {
    const neededProps = Object.keys(values).reduce((arr, key) => {
      if (!notNeedFields[key]) {
        arr.push(key);
      }
      return arr;
    }, []);
    // eslint-disable-next-line no-restricted-globals
    const parseNumArray = neededProps.map(i => parseFloat(values[i])).filter(i => !isNaN(i));
    const needSetIsNothingness = parseNumArray.reduce((a, b) => Math.abs(a) + Math.abs(b), 0) === 0;
    if (!!needSetIsNothingness !== !!values[field]) {
      change(field, needSetIsNothingness);
    }
  }
};
