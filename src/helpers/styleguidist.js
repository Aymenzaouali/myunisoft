module.exports = {
  reduxForm: () => (Comp) => { if (Comp) return Comp; return () => {}; }
};
