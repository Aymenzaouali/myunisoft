/**
 * Explicit simplified
 * stack implementation
 *
 * Usage example: in dialog stacking
 */
export class Stack {
  constructor(stack = []) {
    this.elements = [...stack];
  }

  push(data) {
    this.elements.push(data);
  }

  pop() {
    this.elements.pop();
  }
}
