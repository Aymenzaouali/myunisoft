import { autofill } from 'redux-form';
import { normalizeData } from './normalizeData';

export const autoFillFormValues = (formName, data) => (dispatch) => {
  /**
     * A replacement of
     * await dispatch(initialize('companyCreationForm', data));
     * because of a bug on redux-form
     * @see https://github.com/erikras/redux-form/issues/2971#issuecomment-459311439
     *
     * More investigation needed.
     */
  for (const field in normalizeData(data)) { //eslint-disable-line
    if (data.hasOwnProperty(field)) { //eslint-disable-line
      dispatch(autofill(formName, field, data[field]));
    }
  }
};
