import _ from 'lodash';
import { roundNumber, formatNumber } from './number';

/**
 * Function used to calcul all params of an object
 *
 * @param {object} values
 */
export const calculTotal = (values) => {
  let total = 0;
  _.each(values, (val) => { if (val) total += roundNumber(parseFloat(val)); });
  return total;
};

export const total = (entries, type, format = true) => {
  let total = 0;
  entries.forEach((entry) => {
    total += roundNumber(_.get(entry, type, 0));
  });

  total = roundNumber(total);
  if (format) {
    total = formatNumber(total, '0,0.00 $');
  }

  // remove ',00' if present
  total = total.replace(/[.,]00/, '');

  return total;
};

export const totalWithoutSelectedLine = (entries, selectedLine, type, format = true) => {
  let total = 0;

  entries.forEach((entry, i) => {
    if (selectedLine !== i) {
      total += roundNumber(_.get(entry, type, 0));
    }
  });

  return format ? formatNumber(roundNumber(total), '0,0.00 $') : roundNumber(total);
};

/**
 * Function used to defined which field between credit and
 * debit the user must go when creating a new Entry
 *
 * @param {number} diary_id
 * @param {object} currentRow
 * @param {array} newEntries
 */
export const defaultAccountType = (diary_id, currentRow, newEntries) => {
  const currentAccountNumber = _.get(currentRow, 'account.account_number', '');
  if (diary_id === 1) {
    if (currentAccountNumber.match(/^40/)) {
      if (currentRow.debit) {
        return 'debit';
      }
      return 'credit';
    }
    const account40 = newEntries.find(entry => _.get(entry, 'account.account_number', '').match(/^40/));
    if (account40) {
      if (account40.debit) {
        return 'credit';
      }
      return 'debit';
    }
  }
  if (diary_id === 2) {
    if (currentAccountNumber.match(/^41/)) {
      if (currentRow.credit) {
        return 'credit';
      }
      return 'debit';
    }
    const account41 = newEntries.find(entry => _.get(entry, 'account.account_number', '').match(/^41/));
    if (account41) {
      if (account41.credit) {
        return 'debit';
      }
      return 'credit';
    }
  }
  if (diary_id === 3) {
    if (_.get(currentRow, 'account.account_number', '').match(/^41/)) {
      return 'credit';
    }
  }
  if (currentAccountNumber.match(/^7/)) {
    return 'credit';
  }
  return 'debit';
};
