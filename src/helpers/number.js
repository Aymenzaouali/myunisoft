import numeral from 'numeral';

/**
 * Function used to make a round of two after the . following rules of last bigger number.
 *
 * @param {*} num
 */
export const roundNumber = (num, decimals = 2) => num && +(`${Math.round(`${String(num).replace(/,/g, '.').replace(/ /g, '')}e+${decimals}`)}e-${decimals}`);

/**
 * Function used to format number to display thousands and decimals
 * (num || 0) because num can be undefined or null,
 * so default value as parameter wont work with null
 * +(+( beacause it can also be an object
 *
 * @param {*} num
 * @param {string} format - The format of number according to (http://numeraljs.com/#format)
 */
export const formatNumber = (num, format = '0,0.00') => numeral(+(+(num || 0)).toFixed(2)).format(format);

/**
 * Return the formatted number or an empty string if equals to 0
 *
 * @param {*} num
 * @param {string} format - The format of number according to (http://numeraljs.com/#format)
 * @returns {string} - the formatted value
 */
export const formatNumberIfNotNull = (num, format = '0,0.00') => (Math.abs(num || 0) < 0.01 ? '' : formatNumber(num, format));

export const formatNumberIfNotNullAndNoDecimals = (num, format = '0,0') => (Math.abs(num || 0) < 0.01 ? '' : formatNumber(num, format));

export const formatPercentage = (num, format = '0,0') => {
  if (num !== null) {
    return (
      `${formatNumber(num, format)}%`
    );
  } return '';
};

export const formatStringNumber = valueString => parseInt(valueString.replace(/\s/g, ''), 10);
