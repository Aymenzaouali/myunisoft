import I18n from 'assets/I18n';
import _ from 'lodash';

export const quotes = [
  I18n.t('citations.1'),
  I18n.t('citations.2'),
  I18n.t('citations.3'),
  I18n.t('citations.4'),
  I18n.t('citations.5'),
  I18n.t('citations.6'),
  I18n.t('citations.7'),
  I18n.t('citations.8'),
  I18n.t('citations.9'),
  I18n.t('citations.10'),
  I18n.t('citations.11')
];

export const getQuote = () => quotes[_.random(0, quotes.length - 1)];
