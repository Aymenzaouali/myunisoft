import moment from 'moment';
import _ from 'lodash';
import { fileToBase64 } from 'helpers/file';
import { roundNumber } from './number';

export const accountFormat = (value, type) => {
  const data = value.map(e => ({
    label: _.get(e, `${type}.label`),
    value: _.get(e, `${type}.id`),
    number: _.get(e, `${type}.number`)
  }));
  return data;
};

export const socialSecurityFormat = (value) => {
  let socialSecurityInput = '';

  if (value) {
    const valueWithoutSpace = value.length > 21 ? value.slice(0, 21).replace(/\s/g, '') : value.replace(/\s/g, '');

    for (let i = 0; i < valueWithoutSpace.length; i += 1) {
      if (i === 1 || i === 3 || i === 5 || i === 7 || i === 10 || i === 13) {
        socialSecurityInput = `${socialSecurityInput} ${valueWithoutSpace[i]}`;
      } else {
        socialSecurityInput = `${socialSecurityInput}${valueWithoutSpace[i]}`;
      }
    }
  }

  return socialSecurityInput;
};

export const lowercaseFormat = (value) => {
  const lowercaseInput = value ? value.toLocaleLowerCase() : '';
  return lowercaseInput;
};

export const titleCase = value => value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();

export const deleteSpace = (value) => {
  const stringWithoutSpace = value && value.replace(/ /g, '');
  return stringWithoutSpace;
};

/**
 * Function to format form into back data for entries
 * @param {number} societyId - current society id
 * @param {object} payload - form to convert
 */
export const formatEntry = async (payload, societyId, manualDocs = []) => {
  let entry_pj_list = [];
  const createDoc = async file => ({
    content: await fileToBase64(file),
    name: file.name,
    type: file.type
  });
  if (_.get(payload, 'pj_list.length', 0) > 0) {
    entry_pj_list = await Promise.all(payload.pj_list.map(
      async (doc) => {
        if (doc.document_id) {
          return doc;
        }
        const d1 = await createDoc(doc);
        return d1;
      }
    ));
  }
  const pj_list = [];
  _.values(manualDocs).forEach((docs) => {
    if (docs.document_attached_id) {
      pj_list.push({ document_attached_id: docs.document_attached_id });
    }

    docs.documents.forEach((doc) => {
      pj_list.push({ document_id: doc.document_id });
    });
  });

  if (pj_list.length === 0) {
    // TODO: Should it trigger : Attention vous venez de saisir une écriture sans pièce jointe ?
  }

  entry_pj_list = [...entry_pj_list, ...pj_list];
  // entry_pj_list = [...entry_pj_list, ..._.get(manualDocs, 'documents', [])];
  const entry_list = await Promise.all(payload.entry_list.map(async (entry) => {
    const entry_row = {
      ...entry,
      account_id: _.get(entry, 'account.account_id', -1),
      deadline: _.get(entry, 'deadline', '') || '',
      debit: roundNumber(entry.debit) || 0,
      credit: roundNumber(entry.credit) || 0,
      payment_type_id: _.get(entry, 'payment_type.payment_type_id', -1)
    };
    if (_.get(entry_row, 'pj_list.length', 0) > 0) {
      entry_row.pj_list = await Promise.all(entry.pj_list.map(
        async (doc) => {
          if (doc.document_id) {
            return doc;
          }
          const d1 = await createDoc(doc);
          return d1;
        }
      ));
    }
    return entry_row;
  }));

  const {
    year,
    month,
    day
  } = payload;

  const entry = {
    ...payload,
    pj_list: entry_pj_list,
    delete_list: payload.delete_list,
    diary_id: _.get(payload, 'diary.diary_id') || _.get(payload, 'diary.id'),
    date_piece: `${year.value}-${month.value}-${day}`,
    etablissement_id: societyId,
    period_to: moment().format('YYYY-MM-DD'),
    period_from: moment().subtract(1, 'month').format('YYYY-MM-DD'),
    entry_list
  };
  return entry;
};

/**
 * Function to format data
 * @param {object} values - form to convert
 */
export const formatFiscalFolder = async (values) => {
  const {
    due_date_tva,
    adherent_code,
    rof_cfe,
    rof_tdfc,
    rof_tva,
    mono_etab,
    tva_intraco,
    vat_regime,
    gestion_center,
    society_status,
    info_bnc,
    sheet_group,
    account_edi,
    close_entries_VAT
  } = values;

  const fiscal_folder = {
    other: {
      edi: account_edi && {
        id_compte_edi: account_edi.id,
        libelle: account_edi.label,
        mail: account_edi.value,
        enable: true,
        value: account_edi.id,
        label: account_edi.value
      },
      sheet_group: sheet_group && {
        ...sheet_group
      },
      due_date_tva,
      adherent_code,
      rof_cfe,
      rof_tdfc,
      rof_tva,
      mono_etab,
      tva_intraco,
      close_entries_VAT,
      vat_regime: vat_regime && {
        ...vat_regime
      },
      gestion_center: gestion_center && {
        ...gestion_center
      }
    },
    society_status: society_status && {
      ...society_status
    },
    info_bnc: info_bnc && {
      activity_code_pm: _.get(info_bnc, 'activity_code_pm', null),
      comptability_held: _.get(info_bnc, 'comptability_held', null),
      comptability_type: _.get(info_bnc, 'comptability_type', null),
      info_bnc_id: _.get(info_bnc, 'info_bnc_id', null),
      membership_year: _.get(info_bnc, 'membership_year', null),
      result_rule: _.get(info_bnc, 'result_rule', null)
    }
  };
  return fiscal_folder;
};

export const phoneFormat = value => (value ? value.replace(/[&/\\#,!@%$~%'":*?<>{}a-zA-Z ]/g, '') : value);
