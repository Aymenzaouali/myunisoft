import _ from 'lodash';

export class Normalize {
  static PG = (allValues) => {
    const PG = parseFloat(_.get(allValues, 'PG', 0));
    return Math.round(PG);
  }

  static HA = (allValues) => {
    const HA = parseFloat(_.get(allValues, 'HA', 0));
    return Math.round(HA);
  }

  static HB = (allValues) => {
    const HB = parseFloat(_.get(allValues, 'HB', 0));
    return Math.round(HB);
  }

  static HC = (allValues) => {
    const HC = parseFloat(_.get(allValues, 'HC', 0));
    return Math.round(HC);
  }

  static HD = (allValues) => {
    const HA = parseFloat(_.get(allValues, 'HA', 0));
    const HB = parseFloat(_.get(allValues, 'HB', 0));
    const HC = parseFloat(_.get(allValues, 'HC', 0));
    return Math.round(HA + HB + HC);
  }

  static HE = (allValues) => {
    const HE = parseFloat(_.get(allValues, 'HE', 0));
    return Math.round(HE);
  }

  static HF = (allValues) => {
    const HF = parseFloat(_.get(allValues, 'HF', 0));
    return Math.round(HF);
  }

  static HG = (allValues) => {
    const HG = parseFloat(_.get(allValues, 'HG', 0));
    return Math.round(HG);
  }

  static HH = (allValues) => {
    const HE = parseFloat(_.get(allValues, 'HE', 0));
    const HF = parseFloat(_.get(allValues, 'HF', 0));
    const HG = parseFloat(_.get(allValues, 'HG', 0));
    return Math.round(HE + HF + HG);
  }

  static HI = (allValues) => {
    const HD = parseFloat(_.get(allValues, 'HD', 0));
    const HH = parseFloat(_.get(allValues, 'HH', 0));
    return Math.round(HD - HH);
  }

  static HJ = (allValues) => {
    const HJ = parseFloat(_.get(allValues, 'HJ', 0));
    return Math.round(HJ);
  }

  static HK = (allValues) => {
    const HK = parseFloat(_.get(allValues, 'HK', 0));
    return Math.round(HK);
  }

  static HL = (allValues) => {
    const HD = parseFloat(_.get(allValues, 'HD', 0));
    const FR = 0;
    const GH = 0;
    const GP = 0;
    return Math.round(HD + FR + GH + GP);
  }

  static HM = (allValues) => {
    const HH = parseFloat(_.get(allValues, 'HH', 0));
    const HJ = parseFloat(_.get(allValues, 'HJ', 0));
    const HK = parseFloat(_.get(allValues, 'HK', 0));
    const GF = 0;
    const GI = 0;
    const GU = 0;
    return Math.round(GF + GI + GU + HH + HJ + HK);
  }

  static HN = (allValues) => {
    const HN = parseFloat(_.get(allValues, 'HN', 0));
    return Math.round(HN);
  }

  static HO = (allValues) => {
    const HO = parseFloat(_.get(allValues, 'HO', 0));
    return Math.round(HO);
  }

  static HY = (allValues) => {
    const HY = parseFloat(_.get(allValues, 'HY', 0));
    return Math.round(HY);
  }

  static KA = (allValues) => {
    const KA = parseFloat(_.get(allValues, 'KA', 0));
    return Math.round(KA);
  }

  static HP = (allValues) => {
    const HP = parseFloat(_.get(allValues, 'HP', 0));
    return Math.round(HP);
  }

  static HQ = (allValues) => {
    const HQ = parseFloat(_.get(allValues, 'HQ', 0));
    return Math.round(HQ);
  }

  static KB = (allValues) => {
    const KB = parseFloat(_.get(allValues, 'KB', 0));
    return Math.round(KB);
  }

  static KC = (allValues) => {
    const KC = parseFloat(_.get(allValues, 'KC', 0));
    return Math.round(KC);
  }

  static KD = (allValues) => {
    const KD = parseFloat(_.get(allValues, 'KD', 0));
    return Math.round(KD);
  }

  static HX = (allValues) => {
    const HX = parseFloat(_.get(allValues, 'HX', 0));
    return Math.round(HX);
  }

  static AM = (allValues) => {
    const AM = parseFloat(_.get(allValues, 'AM', 0));
    return Math.round(AM);
  }

  static AN = (allValues) => {
    const AN = parseFloat(_.get(allValues, 'AN', 0));
    return Math.round(AN);
  }

  static LH = (allValues) => {
    const LH = parseFloat(_.get(allValues, 'LH', 0));
    return Math.round(LH);
  }

  static LJ = (allValues) => {
    const LJ = parseFloat(_.get(allValues, 'LJ', 0));
    return Math.round(LJ);
  }

  static LK = (allValues) => {
    const LK = parseFloat(_.get(allValues, 'LK', 0));
    return Math.round(LK);
  }

  static LL = (allValues) => {
    const LL = parseFloat(_.get(allValues, 'LL', 0));
    return Math.round(LL);
  }

  static LM = (allValues) => {
    const LM = parseFloat(_.get(allValues, 'LM', 0));
    return Math.round(LM);
  }

  static LN = (allValues) => {
    const LN = parseFloat(_.get(allValues, 'LN', 0));
    return Math.round(LN);
  }

  static MA1 = (allValues) => {
    const MA1 = parseFloat(_.get(allValues, 'MA1', 0));
    return Math.round(MA1);
  }

  static MA2 = (allValues) => {
    const MA2 = parseFloat(_.get(allValues, 'MA2', 0));
    return Math.round(MA2);
  }

  static MA3 = (allValues) => {
    const MA3 = parseFloat(_.get(allValues, 'MA3', 0));
    return Math.round(MA3);
  }

  static MA4 = (allValues) => {
    const MA4 = parseFloat(_.get(allValues, 'MA4', 0));
    return Math.round(MA4);
  }

  static NA1 = (allValues) => {
    const NA1 = parseFloat(_.get(allValues, 'NA1', 0));
    return Math.round(NA1);
  }

  static NA2 = (allValues) => {
    const NA2 = parseFloat(_.get(allValues, 'NA2', 0));
    return Math.round(NA2);
  }

  static NA3 = (allValues) => {
    const NA3 = parseFloat(_.get(allValues, 'NA3', 0));
    return Math.round(NA3);
  }

  static NA4 = (allValues) => {
    const NA4 = parseFloat(_.get(allValues, 'NA4', 0));
    return Math.round(NA4);
  }

  static PA1 = (allValues) => {
    const PA1 = parseFloat(_.get(allValues, 'PA1', 0));
    return Math.round(PA1);
  }

  static PA2 = (allValues) => {
    const PA2 = parseFloat(_.get(allValues, 'PA2', 0));
    return Math.round(PA2);
  }

  static PA3 = (allValues) => {
    const PA3 = parseFloat(_.get(allValues, 'PA3', 0));
    return Math.round(PA3);
  }

  static PA4 = (allValues) => {
    const PA4 = parseFloat(_.get(allValues, 'PA4', 0));
    return Math.round(PA4);
  }

  static MF1 = (allValues) => {
    const MF1 = parseFloat(_.get(allValues, 'MF1', 0));
    return Math.round(MF1);
  }

  static MF2 = (allValues) => {
    const MF2 = parseFloat(_.get(allValues, 'MF2', 0));
    return Math.round(MF2);
  }

  static MF3 = (allValues) => {
    const MF3 = parseFloat(_.get(allValues, 'MF3', 0));
    return Math.round(MF3);
  }

  static MF4 = (allValues) => {
    const MF4 = parseFloat(_.get(allValues, 'MF4', 0));
    return Math.round(MF4);
  }

  static NF1 = (allValues) => {
    const NF1 = parseFloat(_.get(allValues, 'NF1', 0));
    return Math.round(NF1);
  }

  static NF2 = (allValues) => {
    const NF2 = parseFloat(_.get(allValues, 'NF2', 0));
    return Math.round(NF2);
  }

  static NF3 = (allValues) => {
    const NF3 = parseFloat(_.get(allValues, 'NF3', 0));
    return Math.round(NF3);
  }

  static NF4 = (allValues) => {
    const NF4 = parseFloat(_.get(allValues, 'NF4', 0));
    return Math.round(NF4);
  }

  static PF1 = (allValues) => {
    const PF1 = parseFloat(_.get(allValues, 'PF1', 0));
    return Math.round(PF1);
  }

  static PF2 = (allValues) => {
    const PF2 = parseFloat(_.get(allValues, 'PF2', 0));
    return Math.round(PF2);
  }

  static PF3 = (allValues) => {
    const PF3 = parseFloat(_.get(allValues, 'PF3', 0));
    return Math.round(PF3);
  }

  static PF4 = (allValues) => {
    const PF4 = parseFloat(_.get(allValues, 'PF4', 0));
    return Math.round(PF4);
  }
}
