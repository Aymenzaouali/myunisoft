/* eslint-disable */
/**
 * Code reverse of pdfix solution
 */
export class Pdfix {
  static instance;

  static getInstance() {
    if (!Pdfix.instance) Pdfix.instance = new Pdfix();
    return Pdfix.instance;
  }

  getNextSibling = (c, b, d) => {
    let a = c.nextSibling;
    while (a !== null) {
      if (a.nodeType == 1 && (b == '' || a.nodeName.toLowerCase() == b) && (d == '' || a.getAttribute('data-type') == d)) {
        break;
      }
      a = a.nextSibling;
    }
    return a;
  };

  pdfixUpdatePages = () => {
    const a = document.getElementById('pdf-document');
    if (a === 'undefined' || a.getAttribute('data-type') != 'pdf-document') {
      return;
    }
    let b = this.getNextSibling(a, 'div', 'pdf-page');
    while (b !== null) {
      this.pdfixUpdatePage(b); b = this.getNextSibling(b, 'div', 'pdf-page');
    }
  };

  pdfixUpdateLayout = () => {
    this.pdfixUpdatePages();
  };

  getFirstChild = (c, b, d) => {
    let a = c.firstChild;
    while (a !== null) {
      if (a.nodeType == 1 && (b == '' || a.nodeName.toLowerCase() == b) && (d == '' || a.getAttribute('data-type') == d)) { break; }
      a = a.nextSibling;
    } return a;
  };

  updatePageTextsOnce = (d) => {
    if (d.getAttribute('data-text-scaled') != '1') {
      let a = d.firstChild;
      while (a != null) {
        if (a.nodeType == 1) {
          const i = this.getFirstChild(a, 'span', '');
          if (i != null) {
            const b = parseFloat(i.offsetWidth);
            const h = parseFloat(a.offsetWidth);
            const c = parseFloat(i.offsetHeight);
            const f = parseFloat(a.offsetHeight) - 1;
            const g = h / b;
            const e = f / c;
            a.style.transform = `scaleX(${g}) scaleY(${e})`;
            a.style.transformOrigin = '0px 0px 0px';
          }
        }
        a = a.nextSibling;
      }
      d.setAttribute('data-text-scaled', '1');
    }
  };

  pdfixUpdatePageFixed = (g) => {
    const e = g.getAttribute('data-ratio');
    const d = parseFloat(g.offsetWidth); g.style.height = `${d * e}px`;
    const c = this.getFirstChild(g, 'div', 'pdf-page-inner');
    if (c !== null) {
      const a = parseFloat(c.getAttribute('data-page-width'));
      const b = d / a;
      const f = this.getFirstChild(c, 'div', 'pdf-page-text');
      if (f !== null) {
        this.updatePageTextsOnce(f);
      }c.style.transform = `scale(${b})`;
      c.style.transformOrigin = '0px 0px 0px';
    }
  };

  pdfixUpdatePageResponsive = (e) => {
    const g = e.getElementsByTagName('div');
    for (let c = 0, b = g.length; c < b; c++) {
      if (g[c].getAttribute('data-type') == 'pdf-image') {
        const d = g[c];
        const j = d.parentElement;
        const a = parseFloat(d.getAttribute('data-image-width'));
        const k = parseFloat(d.getAttribute('data-ratio'));
        const m = parseFloat(j.offsetWidth); let f = m / a; if (f > 1) { f = 1; }d.style.height = `${a * f / k}px`; const h = this.getFirstChild(d, 'div', 'pdf-image-inner'); const l = this.getFirstChild(h, 'div', 'pdf-image-childs'); if (l != null) { this.updatePageTextsOnce(l); }h.style.transform = `scale(${f})`; h.style.transformOrigin = '0px 0px 0px';
      }
    }
  };

  pdfixUpdatePage = (b) => {
    const a = b.getAttribute('data-layout');
    if (a == 'responsive') {
      this.pdfixUpdatePageResponsive(b);
    } else {
      this.pdfixUpdatePageFixed(b);
    }
  };
}

export default Pdfix.getInstance();