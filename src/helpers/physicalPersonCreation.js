import _ from 'lodash';

export const parseEmployee = employee => ({
  id: employee.society_id,
  value: employee,
  name: employee.name,
  function: _.get(employee, 'function', {}),
  signatory_function: _.get(employee, 'signatory_function', {}),
  start_date: _.get(employee, 'start_date', ''),
  end_date: _.get(employee, 'end_date', '')
});

export const parseSociety = society => ({
  id: society.society_id,
  value: society,
  label: society.name,
  name: society.name,
  address: _.get(society, 'address', ''),
  siret: _.get(society, 'siret', ''),
  capital: _.get(society, 'capital', null),
  function: _.get(society, 'function', {}),
  signatory_function: _.get(society, 'signatory_function', {}),
  start_date: _.get(society, 'start_date', ''),
  end_date: _.get(society, 'end_date', ''),
  social_part: society.social_part ? {
    PP: _.get(society.social_part, 'PP', null),
    US: _.get(society.social_part, 'US', null),
    NP: _.get(society.social_part, 'NP', null),
    percent: _.get(society.social_part, 'percent', null)
  } : {}
});
