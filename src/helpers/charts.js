import moment from 'moment';

// Functions
const seuil = (min, max) => {
  if (Math.abs(min) < 1 && Math.abs(max) < 1) {
    return 0.5;
  }

  const v = Math.max(Math.abs(min), Math.abs(max));
  const l = Math.floor(Math.log10(v)) - 1;

  return 5 * (10 ** l);
};
const roundUp = (val, seuil) => {
  if (val === 0) return seuil;

  if (val < 0) {
    return val - (val % seuil);
  }

  return val + (seuil - (val % seuil));
};
const roundDown = (val, seuil) => {
  if (val === 0) return -seuil;

  if (val < 0) {
    return val - (seuil + (val % seuil));
  }

  return val - (val % seuil);
};

const roundMinMax = (min, max) => {
  const s = seuil(min, max);

  return [roundDown(min, s), roundUp(max, s)];
};

const formatPeriod = (start, end) => {
  const startYear = moment(start).format('YYYY');
  const endYear = moment(end).format('YYYY');

  return (startYear === endYear) ? endYear : `${startYear}/${endYear}`;
};

const percentageFormat = (denominator, numerator) => {
  const format = (value) => {
    if (isNaN(value)) return null; // eslint-disable-line no-restricted-globals

    return Math.round(value * 100) / 100;
  };

  const denominatorValues = denominator.map(e => ({
    month: e.x,
    amount: e.y,
    period: e.z
  }));
  const numeratorValues = numerator.map(e => ({
    month: e.x,
    amount: e.y,
    period: e.z
  }));

  const data = [];

  for (let i = 0; i < numeratorValues.length; i += 1) {
    data.push({
      x: numeratorValues[i].month,
      y: format((numeratorValues[i].amount / denominatorValues[i].amount) * 100),
      z: denominatorValues[i].period
    });
  }
  return data;
};

export {
  formatPeriod,
  roundMinMax,
  percentageFormat
};
