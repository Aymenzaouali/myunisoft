import { LOCATION_CHANGE } from 'connected-react-router';

import { TAB_REGEX } from 'assets/constants/regex';

// Functions
const formatTabId = (tabId) => {
  let id = tabId.toString();
  if (id === '-2') {
    id = 'collab';
  }

  return id;
};

const getTabState = (state, tabId) => state.tabs[formatTabId(tabId)] || {};
const getCurrentTabState = state => getTabState(state, state.navigation.id);

const tabFormName = (name, tabId) => `${name}_${tabId}`;

const getNewTab = (action) => {
  if (action.type === LOCATION_CHANGE) {
    const { location: { pathname } } = action.payload;

    if (TAB_REGEX.test(pathname)) {
      return formatTabId(RegExp.$1);
    }
  }

  return undefined;
};

const isChangingTab = (action, state) => formatTabId(state.navigation.id) !== getNewTab(action);

export {
  formatTabId,
  getTabState,
  getCurrentTabState,

  tabFormName,

  getNewTab,
  isChangingTab
};
