export const getCharSize = (text, font) => {
  const element = document.createElement('canvas');
  const context = element.getContext('2d');
  context.font = font;
  const size = {
    width: context.measureText(text).width,
    height: parseInt(context.font, 10)
  };
  return size;
};
// 320 // 820
export const splitIntoPages = (text = '', maxHeight = [470, 800], maxWidth = 470) => {
  let page = [];
  let computedText = '';
  let computedHeight = 15;
  let computedWidth = 0;
  [...text.replace(/&nbsp;/g, ' ')].forEach((char) => {
    computedWidth += getCharSize(char, '12px basier_circleregular').width;
    if (computedWidth > maxWidth) {
      computedHeight += 12 + 3;
      const lastWord = computedText.match(/\s(\w+)$/);
      if (lastWord) {
        computedWidth = 0;
        [...lastWord[1]].forEach((c) => {
          computedWidth += getCharSize(c, '12px basier_circleregular').width;
        });
      } else {
        computedWidth = getCharSize(char, '12px basier_circleregular').width;
      }
      if (computedHeight >= maxHeight) {
        page = [...page, computedText];
        // i += 1;
        computedHeight = 15;
        computedText = char;
      } else {
        computedText += char;
      }
    } else {
      computedText += char;
    }
  });
  page = [...page, computedText];
  return page;
};

export const moveCaretToChar = (ref, char = 0) => {
  let selection; // character at which to place caret
  try {
    if (document.selection) {
      selection = document.selection.createRange();
      selection.moveStart('character', char);
      selection.select();
    } else {
      selection = window.getSelection();
      selection.collapse(ref.lastChild, char);
    }
  } catch (e) {
    console.log(e);
  }
};
