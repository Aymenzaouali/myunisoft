import {
  useCallback, useDebugValue, useEffect, useRef, useState
} from 'react';
import _ from 'lodash';

// Function
const compute = (node, cb) => {
  if (node != null) cb(node.clientWidth);
};

// Hooks
export const usePrevious = (value, initialValue = null) => {
  const ref = useRef(initialValue);

  useEffect(() => {
    ref.current = value;
  });

  return ref.current;
};

export const useDebouncedEffect = (effect, wait, deps) => {
  useEffect(() => {
    const debounced = _.debounce(effect, wait);
    debounced();

    return debounced.cancel;
  }, deps);
};

export const useWidth = (cb) => {
  // Refs
  const containerRef = useRef(null);

  // Callbacks
  const containerCb = useCallback((node) => {
    containerRef.current = node;
    compute(node, cb);
  });

  // Effects
  useEffect(() => {
    compute(containerRef.current, cb);
  });

  useEffect(() => {
    const handleLoad = () => compute(containerRef.current, cb);
    const handleResize = _.debounce(() => compute(containerRef.current, cb), 100);

    window.addEventListener('load', handleLoad);
    window.addEventListener('resize', handleResize);

    return () => {
      handleResize.cancel();

      window.removeEventListener('load', handleLoad);
      window.removeEventListener('resize', handleResize);
    };
  }, [cb]);

  return containerCb;
};

export const useWindowState = (props = {}) => {
  const [win, setWin] = useState({ open: false, focusing: false });
  useDebugValue((props.open || win.open) ? 'Opened' : 'Closed');

  return {
    open: (message, focus = false) => setWin(old => ({
      ...old,
      message,
      open: true,
      focusing: focus
    })),
    send: (message, focus = false) => setWin(old => ({
      ...old,
      message,
      focusing: focus
    })),
    focus: () => setWin(old => ({ ...old, focusing: true })),
    close: () => setWin(old => ({ ...old, open: false })),

    props: {
      ...props,
      open: props.open || win.open,
      focusing: win.focusing,
      message: win.message,

      onOpen: () => {
        if (props.open === undefined) {
          setWin(old => ({ ...old, open: true }));
          if (props.onOpen) props.onOpen();
        } else {
          props.onOpen();
        }
      },
      onFocusChanged: () => {
        setWin(old => ({ ...old, focusing: false }));
        if (props.onFocusChanged) props.onFocusChanged();
      },
      onClose: () => {
        if (props.open === undefined) {
          setWin(old => ({ ...old, open: false, focusing: false }));
          if (props.onClose) props.onClose();
        } else {
          props.onClose();
        }
      }
    }
  };
};
