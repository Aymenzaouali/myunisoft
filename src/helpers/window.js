export class WindowHelper {
  static instance;

  static getInstance() {
    if (!WindowHelper.instance) WindowHelper.instance = new WindowHelper();
    return WindowHelper.instance;
  }

  handleOpen(files) {
    if (this.pjWindow) {
      this.refresh(files);
    } else {
      this.openWith(files);
    }
  }

  forceOpen(files) {
    if (!this.pjWindow || (this.pjWindow && this.pjWindow.closed)) {
      this.openWith(files);
    } else {
      this.refresh(files);
    }
  }

  openWith(files) {
    this.pjWindow = window.open('/overview', 'pjs', `width=${window.innerWidth},height=${window.innerHeight}`);
    this.pjWindow.onload = () => setTimeout(() => { this.pjWindow.postMessage({ type: 'files', files }, '*'); }, 0);
  }

  refresh(files) {
    this.pjWindow.postMessage({ type: 'files', files }, '*');
  }

  resetFiles() {
    if (this.pjWindow) {
      this.pjWindow.postMessage({ type: 'files', files: [] }, '*');
    }
  }
}

export default WindowHelper.getInstance();
