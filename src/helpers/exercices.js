import moment from 'moment';

export const findExercicesByDateRange = (exercices, dateStart, dateEnd) => exercices
  .filter(({ start_date, end_date }) => dateStart === moment(start_date).format('YYYYMMDD')
            && dateEnd === moment(end_date).format('YYYYMMDD'));
