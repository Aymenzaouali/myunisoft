import _ from 'lodash';

/**
 * Used to clean an object from `null | "" | [emptyArray]` values which
 * are useless but still are present in the store
 *
 * This becomes a problem when we need to compare 2
 * objects and the only difference is a number of
 * `null` and `""` key values
 *
 * Imagine a deep equal check
 * `{ name: "Paul", age: null } !== { name: "Paul" }`
 *
 * This would return a false positive comparison result.
 *
 * @param {object} data - response data i.e. form values
 * @returns {object} - Cleaned data copy
 */
export const normalizeData = data => _.pickBy(
  data,
  value => (Array.isArray(value) && value.length) || (value !== null && value !== '')
);
