export const generateHeaderSteps = (stepArray) => {
  const mySteps = stepArray.map((step, id) => ({
    id,
    ...step
  }));
  return mySteps;
};
