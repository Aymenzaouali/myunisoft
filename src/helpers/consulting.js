import { objectToQueryString } from 'helpers/url';

export const openConsultingPopup = (societyId, params) => window.open(`/tab/${societyId}/consulting/?${objectToQueryString(params)}`);
