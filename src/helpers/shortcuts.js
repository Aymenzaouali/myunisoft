const correspShortcutsName = [
  {
    key: 'accounting',
    value: 'ACCOUNTING'
  },
  {
    key: 'newAccounting',
    value: 'NEW_ACCOUNTING'
  },
  {
    key: 'ib',
    value: 'NEW_ACCOUNTING'
  },
  {
    key: 'accountConsultation',
    value: 'CONSULTING'
  },
  {
    key: 'ged',
    value: 'GED'
  },
  {
    key: 'balance',
    value: 'BALANCE'
  }
];

export const getCorrespByKey = key => correspShortcutsName.find(
  shortchutsName => shortchutsName.key === key
) || {};
