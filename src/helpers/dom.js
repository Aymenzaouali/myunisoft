/* eslint-disable */

// solution from https://stackoverflow.com/a/6691294/1948511
function pasteHtmlAtCaret(html) {
  const sel = window.getSelection();

  if (sel.getRangeAt && sel.rangeCount) {
    let range = sel.getRangeAt(0);
    range.deleteContents();

    // Range.createContextualFragment() would be useful here but is
    // non-standard and not supported in all browsers (IE9, for one)
    const el = document.createElement('div');
    el.innerHTML = html;
    const frag = document.createDocumentFragment();
    let node;
    let lastNode;
    while ((node = el.firstChild)) {     // eslint-disable-line
      lastNode = frag.appendChild(node);
    }
    range.insertNode(frag);

    // Preserve the selection
    if (lastNode) {
      range = range.cloneRange();
      range.setStartAfter(lastNode);
      range.collapse(true);
      sel.removeAllRanges();
      sel.addRange(range);
    }
  }
}

// get element caret position
// (modified cut from https://gist.github.com/isLishude/6ccd1fbf42d1eaac667d6873e7b134f8 )
function getCaretPos(target) {
  // for contentedit field
  if (target.contentEditable) {
    target.focus();
    const _range = document.getSelection().getRangeAt(0);
    const range = _range.cloneRange();
    range.selectNodeContents(target);
    range.setEnd(_range.endContainer, _range.endOffset);
    const pos = range.toString().length;
    target.blur();
    return pos;
  }
  // for texterea/input element
  return target.selectionStart;
}


// editable selection recipy (from https://stackoverflow.com/a/6242538/1948511)
function getTextNodesIn(node) {
  const textNodes = [];
  if (node.nodeType == 3) {
    textNodes.push(node);
  } else {
    const children = node.childNodes;
    for (let i = 0, len = children.length; i < len; ++i) {
      textNodes.push(...getTextNodesIn(children[i]));
    }
  }
  return textNodes;
}

// set selection range for element
function setSelectionRange(el, start, end) {
  if (document.createRange && window.getSelection) {
    const range = document.createRange();
    range.selectNodeContents(el);
    const textNodes = getTextNodesIn(el);
    let foundStart = false;
    let charCount = 0; let
      endCharCount;

    for (var i = 0, textNode; textNode = textNodes[i++];) {
      endCharCount = charCount + textNode.length;
      if (!foundStart && start >= charCount
               && (start < endCharCount
                   || (start == endCharCount && i <= textNodes.length))) {
        range.setStart(textNode, start - charCount);
        foundStart = true;
      }
      if (foundStart && end <= endCharCount) {
        range.setEnd(textNode, end - charCount);
        break;
      }
      charCount = endCharCount;
    }

    const sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  } else if (document.selection && document.body.createTextRange) {
    const textRange = document.body.createTextRange();
    textRange.moveToElementText(el);
    textRange.collapse(true);
    textRange.moveEnd('character', end);
    textRange.moveStart('character', start);
    textRange.select();
  }
}

export { pasteHtmlAtCaret, getCaretPos, setSelectionRange };
