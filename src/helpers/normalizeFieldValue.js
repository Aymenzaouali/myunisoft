/**
 * Helper to make sure redux-form
 * removes values that are
 * null or empty array
 *
 * @param {*} value
 */
export function normalizeFieldValue(value) {
  if (Array.isArray(value) && !value.length) {
    return '';
  }

  if (value === null) {
    return '';
  }

  return value;
}
