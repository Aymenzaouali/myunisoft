import _get from 'lodash/get';

export const allowAccess = (rights, reference) => {
  const selectedRight = rights.find(e => e.reference_front === reference);
  if (_get(selectedRight, 'irights') === 1) { return false; } return true;
};
