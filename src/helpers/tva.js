import I18n from 'assets/I18n';
import _ from 'lodash';

export const getTvaInfo = (tvaCode, codeTvaState) => {
  const info = _.isArray(codeTvaState)
    ? codeTvaState.find(c => parseInt(c.code, 10) === tvaCode)
    : [];

  if (info && info.length > 0) {
    return `${tvaCode}: ${info.account_ded.label} ${info.vat_exigility.label} ${I18n.t('fnp.vat_rate', { count: info.vat.rate })}`;
  }

  return null;
};
