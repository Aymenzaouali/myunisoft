import printJS from 'print-js';
import pdfjs from 'pdfjs-dist/webpack';
import JSpdf from 'jspdf';

/**
 * File to base64 (best description)
 * @param {File} file
 * @returns {String} base64
 */
const fileToBase64 = (file) => {
  const temporaryFileReader = new FileReader();

  return new Promise((resolve, reject) => {
    temporaryFileReader.onerror = () => {
      temporaryFileReader.abort();
      reject();
    };

    temporaryFileReader.onload = () => {
      resolve(temporaryFileReader.result);
    };

    temporaryFileReader.readAsDataURL(file);
  });
};

export { fileToBase64 };

export const getCloudUrl = token => `https://cloud.myunisoft.fr/index.php/s/${token}`;


/**
 * Matches the filename part in Content-Disposition header
 * @example attachment;filename="Balance_1_1381_20190128_17175738_153.pdf"
 * @matches "Balance_1_1381_20190128_17175738_153.pdf"
 */
// const filenameRegex = /(?:[A-Za-z])(\w|-)+(?:\.[a-z]+)/; // OBSOLETE

export const downloadFile = (data, filename, type = 'application/pdf') => {
  // Create a new Blob object using the
  // response data of the onload object
  const blob = new Blob([data], { type });
  // Create a link element, hide it, direct
  // it towards the blob, and then 'click' it programatically
  const a = document.createElement('a');
  a.style = 'display: none';
  document.body.appendChild(a);
  // Create a DOMString representing the blob
  // and point the link element towards it
  const url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = filename;
  // programatically click the link to trigger the download
  a.click();
  // release the reference to the file by revoking the Object URL
  setTimeout(() => {
    // For Firefox it is necessary to delay revoking the ObjectURL
    window.URL.revokeObjectURL(url);
    document.body.removeChild(a);
  }, 100);
};

export const printOrDownload = (data, type, fileName) => {
  if (type === 'print') {
    const blob = new Blob([data], { type: 'application/pdf' });
    const url = window.URL.createObjectURL(blob);

    printJS({
      printable: url,
      showModal: true
    });

    // release the reference to the file by revoking the Object URL
    setTimeout(() => {
      // For Firefox it is necessary to delay revoking the ObjectURL
      window.URL.revokeObjectURL(url);
    }, 100);
  } else {
    downloadFile(data, fileName);
  }
};

export const convertPdfPageToImage = async (name, doc, numPage, resolution = 300) => {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  const page = await doc.getPage(numPage);
  const printUnits = resolution / 72;

  const { view: [,, width, height] } = page;
  const renderContext = {
    canvasContext: context,
    viewport: page.getViewport({
      scale: printUnits
    })
  };

  canvas.width = width * printUnits;
  canvas.height = height * printUnits;

  const renderTask = page.render(renderContext);
  await renderTask.promise;

  const blob = await new Promise((resolve) => {
    canvas.toBlob(blob => resolve(blob), 'image/jpeg', 0.80);
  });

  const file = new File([blob], name.replace(/\.pdf|\.PDF/, `${numPage}.jpg`), {
    type: 'image/jpeg'
  });
  file.width = canvas.width;
  file.height = canvas.height;

  return file;
};

export const convertPDFtoImages = async (pdf) => {
  const url = URL.createObjectURL(pdf);
  const loadTask = pdfjs.getDocument(url);
  const doc = await loadTask.promise;
  const { numPages } = doc;

  const images = await Promise.all(
    Array(numPages)
      .fill(undefined)
      .map((v, i) => convertPdfPageToImage(pdf.name, doc, i + 1))
  );

  URL.revokeObjectURL(url);

  return images;
};

export const convertImagesToPdf = async (documents) => {
  const doc = new JSpdf('p', 'pt', 'a4');
  const { width } = doc.internal.pageSize;

  await Promise.all(
    Object.values(documents)
      .map(async (document) => {
        const { blob } = document;
        const base64Image = await fileToBase64(blob);
        const height = (blob.height * width) / blob.width;
        doc.addImage(base64Image, 'JPEG', 0, 0, width, height);
        doc.addPage();
      })
  );

  doc.deletePage(doc.internal.getNumberOfPages());

  return doc.output('blob');
};


export const getImageDimensionsFromUrl = url => new Promise((resolve) => {
  const img = new Image();
  img.onload = () => resolve({ width: img.width, height: img.height });
  img.src = url;
});
