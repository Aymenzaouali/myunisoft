import { matchPath } from 'react-router';

import { TAB_REGEX } from 'assets/constants/regex';

import AccountingScreen from 'containers/controllers/Accounting';
import AccountingPlansScreen from 'containers/controllers/AccountingPlans';
import DashBoardScreen from 'containers/controllers/Dashboard';
import OCRScreen from 'containers/controllers/OCR';
import CollectorScreen from 'containers/controllers/Collector';
import ImportScreen from 'containers/controllers/Imports';
import ExportScreen from 'containers/controllers/Export';
import CurrentEditionsScreen from 'containers/controllers/CurrentEditions';
import TrackingOCR from 'containers/groups/TrackingOCR';
import DashboardCollabScreen from 'containers/controllers/DashboardCollab';
import ConsultingScreen from 'containers/controllers/Consulting';
import ChartAccount from 'containers/controllers/ChartAccounts';
import CompanyCreation from 'containers/groups/CompanyCreation';
import VATScreen from 'components/screens/VAT';
import VATReturnScreen from 'containers/controllers/VATReturn';
import CAAdvance from 'containers/controllers/CAAdvance';
import CVAE from 'containers/controllers/CVAE';
import CVAEAdvance from 'containers/controllers/CVAEAdvance';
import TaxDeclaration from 'containers/controllers/TaxDeclaration';
import CAAnnual from 'containers/controllers/CAAnnual';
import ISDeclaration from 'containers/controllers/ISDeclaration';
import ISLiquidationScreen from 'containers/controllers/ISLiquidation';
import DocumentManagement from 'containers/controllers/DocumentManagement';
import SettingsWorksheets from 'components/screens/SettingsWorksheets';
import CompanyList from 'containers/groups/CompanyList';
import UserCreation from 'containers/groups/UserCreation';
import DiaryScreen from 'containers/controllers/Diary';
import PhysicalPersonCreation from 'containers/groups/PhysicalPersonCreation';
import UserList from 'containers/groups/UserList';
import Discussions from 'containers/groups/Discussions';
import PhysicalPersonList from 'containers/groups/PhysicalPersonList';
import ShortcutsScreen from 'containers/groups/ShortcutsList';
import DADPContainer from 'containers/controllers/DADP';
import RentDeclarationsScreen from 'containers/controllers/RentDeclarations';
import BankLinkScreen from 'containers/controllers/BankLink';

import FNPContainer from 'containers/groups/WorkingPage/FNP';
import AAEContainer from 'containers/groups/WorkingPage/AAE';
import AARContainer from 'containers/groups/WorkingPage/AAR';
import FAEContainer from 'containers/groups/WorkingPage/FAE';
import FixedAssets from 'containers/groups/FixedAssets';
import CCAContainer from 'containers/groups/WorkingPage/CCA';
import PCAContainer from 'containers/groups/WorkingPage/PCA';
import CAPPersonalContainer from 'containers/groups/WorkingPage/CAPPersonal';
import CAPIntContainer from 'containers/groups/WorkingPage/CAPInt';
import CAPCsContainer from 'containers/groups/WorkingPage/CAPCs';
import CAPDiversContainer from 'containers/groups/WorkingPage/CAPDiv';
import CAPIt from 'containers/groups/WorkingPage/CAPIt';

import NewLetter from 'containers/controllers/NewLetter';
import Letters from 'containers/controllers/Letters';
import RIB from 'containers/groups/Forms/RIB';
import Connector from 'containers/controllers/Connector';
import FunctionScreen from 'containers/controllers/Function';
import ClosingYearScreen from 'containers/controllers/ClosingYearScreen';
import PortfolioListView from 'components/screens/PortfolioListView';
import BankIntegrationSettings from 'components/screens/BankIntegrationSettings';
import ReportsAndForms from 'containers/controllers/ReportsAndForms';
import SettingStandardMail from 'containers/controllers/SettingStandardMail';
import ScanAssociateScrean from 'components/screens/ScanAssociate';
import AccountingFirmSettingsScreen from 'containers/controllers/AccountingFirmSettingsScreen';
import AccountingFirmCreation from 'containers/groups/AccountingFirmCreation';

import DynamicLinks from 'containers/groups/DynamicLinks';
import CIRefund from 'containers/controllers/CIRefund';

// import ReportsAndForms from 'components/screens/ReportsAndForms';

/**
 * Routes array, you can use prop of Route component, key is a selector.
 * @example <caption>Dashboard exemple route</caption>
 * import DashBoardScreen from 'containers/controllers/Dashboard';
 *
 * const routes = [
 *   ...,
 *   {
 *     key: 'dashboard',
 *     path: '/tab/:id/dashBoard',
 *     component: DashBoardScreen
 *   }
 * ];
 */
const routes = [
  {
    key: 'da_balanceSheet',
    path: '/tab/:id/dadp/da_balanceSheet',
    component: DADPContainer,
    isNotCollab: true,
    state: {
      type: 'da_balanceSheet'
    }
  },
  {
    key: 'da_statusPending',
    path: '/tab/:id/dadp/da_statusPending',
    component: DADPContainer,
    isNotCollab: true,
    state: {
      type: 'da_statusPending'
    }
  },
  {
    key: 'da_newSituation',
    path: '/tab/:id/dadp/da_newSituation',
    component: DADPContainer,
    isNotCollab: true,
    state: {
      type: 'da_newSituation'
    }
  },
  {
    key: 'dashboardCollab',
    path: '/tab/collab/dashBoard',
    component: DashboardCollabScreen,
    isNotCollab: false
  },
  {
    key: 'ocr',
    path: '/tab/:id/ocr',
    component: OCRScreen
  },
  {
    key: 'ocrTracking',
    path: '/tab/:id/ocrTracking',
    component: TrackingOCR
  },
  {
    key: 'collector',
    path: '/tab/:id/collector',
    isNotCollab: true,
    component: CollectorScreen
  },
  {
    key: 'import',
    path: '/tab/:id/import',
    isNotCollab: false,
    component: ImportScreen
  },
  {
    key: 'export',
    path: '/tab/:id/export',
    isNotCollab: false,
    component: ExportScreen
  },
  {
    key: 'dynamicsLinks',
    path: '/tab/:id/dynamicsLinks',
    isNotCollab: false,
    component: DynamicLinks
  },
  {
    key: 'bankLink',
    path: '/tab/:id/bankLink',
    component: BankLinkScreen,
    isNotCollab: true
  },
  {
    key: 'dashboard',
    path: '/tab/:id/dashBoard',
    component: DashBoardScreen
  },
  {
    key: 'newAccounting',
    path: '/tab/:id/accounting/new',
    component: AccountingScreen,
    isNotCollab: true,
    state: {
      type: 'e'
    }
  },
  {
    key: 'ib',
    path: '/tab/:id/accounting/new',
    component: AccountingScreen,
    isNotCollab: true,
    state: {
      type: 'ib'
    }
  },
  {
    key: 'm',
    path: '/tab/:id/accounting/new',
    component: AccountingScreen,
    isNotCollab: true,
    state: {
      type: 'm'
    }
  },
  {
    key: 'o',
    path: '/tab/:id/accounting/new',
    component: AccountingScreen,
    isNotCollab: true,
    state: {
      type: 'o'
    }
  },
  {
    key: 'userCreation',
    path: '/tab/:id/UserCreation',
    component: UserCreation
  },
  {
    key: 'userList',
    path: '/tab/:id/UserList', // TODO: Check if must really have id on v1
    component: UserList
  },
  // TODO: Fix route
  {
    key: 'accounting',
    path: '/tab/:id/accounting',
    component: AccountingScreen,
    isNotCollab: true
  },
  {
    key: 'accountConsultation',
    path: '/tab/:id/consulting',
    component: ConsultingScreen,
    isNotCollab: true
  },
  {
    key: 'companyList',
    path: '/tab/:id/companies',
    component: CompanyList
  },
  {
    key: 'companyCreation',
    path: '/tab/collab/companyCreation',
    component: CompanyCreation,
    isNotCollab: false
  },
  {
    key: 'physicalPersonList',
    path: '/tab/:id/physicalPersons',
    component: PhysicalPersonList
  },
  {
    key: 'physicalPersonCreation',
    path: '/tab/:id/physicalPersonCreation',
    component: PhysicalPersonCreation
  },
  {
    key: 'chartAccountTva',
    path: '/tab/:id/chartAccount/tva',
    component: ChartAccount,
    isNotCollab: true
  },
  {
    key: 'accountingPlans',
    path: '/tab/collab/accountingPlans',
    component: AccountingPlansScreen,
    isNotCollab: false
  },
  {
    key: 'diary',
    path: '/tab/:id/diary',
    component: DiaryScreen,
    isNotCollab: true
  },
  {
    key: 'vat',
    path: '/tab/:id/tva',
    component: VATScreen
  },
  {
    key: 'tvaca3',
    path: '/tab/:id/declarationTva',
    component: VATReturnScreen,
    isNotCollab: true
  },
  {
    key: 'ca12Advance',
    path: '/tab/:id/declarationTvaCaAdvance',
    component: CAAdvance,
    isNotCollab: true
  },
  {
    key: 'taxDeclaration',
    path: '/tab/:id/taxDeclaration',
    component: TaxDeclaration,
    isNotCollab: true
  },
  {
    key: 'booklet',
    path: '/tab/:id/dadp/da_balanceSheet',
    component: null,
    isNotCollab: true,
    state: {
      type: 'booklet'
    }
  },
  {
    key: 'annualCa12',
    path: '/tab/:id/declarationTvaCaAnnual',
    component: CAAnnual,
    isNotCollab: true
  },
  {
    key: 'downPaymentCvae',
    path: '/tab/:id/advanceCvae',
    component: CVAEAdvance,
    isNotCollab: true
  },
  {
    key: 'balanceCvae',
    path: '/tab/:id/declarationCvae',
    component: CVAE,
    isNotCollab: true
  },
  {
    key: 'downPayment',
    path: '/tab/:id/declarationIs',
    component: ISDeclaration,
    isNotCollab: true
  },
  {
    key: 'closeOut',
    path: '/tab/:id/liquidationIs',
    component: ISLiquidationScreen,
    isNotCollab: true
  },
  {
    key: 'ged',
    path: '/tab/:id/document-management',
    component: DocumentManagement,
    isNotCollab: true
  },
  {
    key: 'settingsWorksheets',
    path: '/tab/:id/settings-worksheets',
    component: SettingsWorksheets,
    isNotCollab: true
  },
  {
    key: 'decloyer',
    path: '/tab/:id/rentDeclarations',
    component: RentDeclarationsScreen,
    isNotCollab: true
  },
  {
    key: 'shortcuts',
    path: '/tab/:id/shortcuts',
    component: ShortcutsScreen
  },
  {
    key: 'balance',
    path: '/tab/:id/current-editions/balance',
    component: CurrentEditionsScreen,
    isNotCollab: true,
    state: {
      type: 'balance'
    }
  },
  {
    key: 'ledger',
    path: '/tab/:id/current-editions/ledger',
    component: CurrentEditionsScreen,
    isNotCollab: true,
    state: {
      type: 'ledger'
    }
  },
  {
    key: 'sig',
    path: '/tab/:id/current-editions/sig',
    component: CurrentEditionsScreen,
    isNotCollab: true,
    state: {
      type: 'sig'
    }
  },
  {
    key: 'fixed_assets',
    path: '/tab/:id/fixed-assets',
    component: FixedAssets,
    isNotCollab: true
  },
  {
    key: 'FNP',
    path: '/tab/:id/fnp',
    component: FNPContainer,
    isNotCollab: true
  },
  {
    key: 'AAE',
    path: '/tab/:id/aae',
    component: AAEContainer,
    isNotCollab: true
  },
  {
    key: 'AAR',
    path: '/tab/:id/aar',
    component: AARContainer,
    isNotCollab: true
  },
  {
    key: 'FAE',
    path: '/tab/:id/fae',
    component: FAEContainer,
    isNotCollab: true
  },
  {
    key: 'CAPPersonal',
    path: '/tab/:id/cap_personal',
    component: CAPPersonalContainer,
    isNotCollab: true
  },
  {
    key: 'CAPInt',
    path: '/tab/:id/cap_int',
    component: CAPIntContainer,
    isNotCollab: true
  },
  {
    key: 'CAPCs',
    path: '/tab/:id/cap_cs',
    component: CAPCsContainer,
    isNotCollab: true
  },
  {
    key: 'CAPDivers',
    path: '/tab/:id/cap_divers',
    component: CAPDiversContainer,
    isNotCollab: true
  },
  {
    key: 'CAPIt',
    path: '/tab/:id/cap_it',
    component: CAPIt,
    isNotCollab: true
  },
  {
    key: 'cca',
    path: '/tab/:id/cca',
    component: CCAContainer,
    isNotCollab: true
  },
  {
    key: 'pca',
    path: '/tab/:id/pca',
    component: PCAContainer,
    isNotCollab: true
  },
  {
    key: 'letters',
    path: '/tab/:id/letters',
    component: Letters,
    isNotCollab: true
  },
  {
    key: 'nLetter',
    path: '/tab/:id/newLetter/:idLetter',
    component: NewLetter,
    isNotCollab: true
  },
  {
    key: 'discussionsList',
    path: '/tab/:id/discussions',
    component: Discussions
  },
  { key: 'linkToSupport', path: 'https://support.myunisoft.fr/' },
  {
    key: 'rib',
    path: '/tab/:id/rib',
    component: RIB,
    isNotCollab: true
  },
  {
    key: 'connector',
    path: '/tab/:id/connector',
    component: Connector
  },
  {
    key: 'function',
    path: '/tab/:id/function',
    component: FunctionScreen
  },
  {
    key: 'closingFiscalYear',
    path: '/tab/:id/closingFiscalYear',
    component: ClosingYearScreen,
    isNotCollab: true
  },
  {
    key: 'portfolioListView',
    path: '/tab/:id/portfolioListView',
    component: PortfolioListView
  },
  {
    key: 'bankIntegrationSettings',
    path: '/tab/:id/bankIntegrationSettings',
    component: BankIntegrationSettings,
    isNotCollab: true
  },
  {
    key: 'settingsStandardMailView',
    path: '/tab/:id/settingStandardMail',
    component: SettingStandardMail,
    isNotCollab: true
  },
  {
    key: 'reportsAndForms',
    path: '/tab/:id/reportsAndForms',
    component: ReportsAndForms
  },
  {
    key: 'accountingFirmSettings',
    path: '/tab/:id/accountingFirmSettings',
    component: AccountingFirmSettingsScreen
  },
  {
    key: 'accountingFirmSettingsBody',
    path: '/tab/:id/AccountingFirmSettingsScreenBody',
    component: AccountingFirmCreation,
    isNotCollab: true
  },
  {
    key: 'scanAssociate',
    path: '/tab/:id/scanAssociate',
    component: ScanAssociateScrean
  },
  {
    key: 'refundTaxCredit',
    path: '/tab/:id/CIRefund',
    component: CIRefund,
    isNotCollab: true
  }
];

/**
 * Put your subroute here like { myRouteKey: path },
 * because we don't want this routes in NavBar component,
 * But we need it for working with menu, or to get key from other components
 *
 * @example <caption>Dashboard subroute exemple route</caption>
 *
 * const subroutes = {
 *   dashboardSub: '/tab/:id/dashBoard/sub',
 * };
 */
const subroutes = {

};

/**
 * routesBykey, format routes array into { key: path } object.
 * @example  <caption>Example usage of route system for make redirection.</caption>
 * import { routesByKey } from 'helpers/routes';
 * import { push } from 'connected-react-router';
 * import { connect } from 'react-redux';
 *
 * const mapDispatchToProps = dispatch => { push: (path) => dispatch(push(path)) };
 *
 * const RamdomComponent = (props) =>
 *  <button onClick={() => props.push(routesByKey.dashboard)}>Redirect me</button>
 *
 * export default connect(() => {}, mapDispatchToProps)(RandomComponent);
 */
export const routesByKey = routes.reduce((p, { key, path }) => ({
  ...p,
  [key]: path
}), { ...subroutes });

export const findRouteKeyByPathname = (pathname) => {
  const route = routes.find(({ path }) => matchPath(pathname, { path }));

  return route && route.key;
};

export const getRouteByKey = key => routes.find(route => route.key === key) || {};

const isSpecificRoute = (pathname, startsWith) => {
  if (TAB_REGEX.test(pathname || window.location.pathname)) {
    return RegExp.$2.startsWith(startsWith);
  }

  return false;
};

const buildSpecificRoute = (pathname, contains) => {
  if (TAB_REGEX.test(pathname)) {
    if (RegExp.$2.startsWith(contains)) {
      return pathname;
    }

    return `/tab/${RegExp.$1}/${contains}/${RegExp.$2}`;
  }

  return pathname;
};

export const buildSlaveRoute = pathname => buildSpecificRoute(pathname, 'slave');

export const isSlaveRoute = pathname => isSpecificRoute(pathname, 'slave');

export default routes;
