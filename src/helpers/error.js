import { SubmissionError } from 'redux-form';
import _ from 'lodash';
import I18n from 'assets/I18n';

export const handleFormError = (error, default_error = 'default') => {
  const code = _.get(error, 'response.data.code', false) || _.get(error, 'data.code', false) || _.get(error, 'code', '');
  const message = _.get(error, 'response.data.message', false) || _.get(error, 'data.message', false) || _.get(error, 'message', '');

  throw new SubmissionError({
    _error: I18n.t([`errors.${code}.${message}`, `errors.${default_error}`])
  });
};

export const handleServerMessage = (error, default_error = 'default') => {
  const message = _.get(error, 'response.data.message', false) || _.get(error, 'data.message', false) || _.get(error, 'message', '');

  throw new SubmissionError({
    _error: message || I18n.t(`errors.${default_error}`)
  });
};

export const dismissError = (errorMessage) => {
  switch (errorMessage) {
  case 'already_reversed':
  case 'bad_email_password':
  case 'ce SIREN correspond à une société déjà existante':
  case 'Ce siret est déja enregistré dans la base de données.':
  case 'Impossible de supprimer une écriture qui possède une ligne lettrée.':
    return false;
  default:
    return true;
  }
};
