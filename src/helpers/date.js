import _ from 'lodash';
import I18n from 'assets/I18n';
import moment from 'moment';

export const checkAndFormatDate = (date) => {
  const setDate = date === moment(date).format('YYYYMMDD') ? date : moment(date).format('YYYYMMDD');
  return setDate;
};

export const addOneYear = startDate => moment(startDate).add(1, 'years').format('YYYY-MM-DD');
export const addOneDay = date => moment(date).add(1, 'days').format('YYYY-MM-DD');
export const subtractOneYear = date => moment(date).subtract(1, 'years').format('YYYY-MM-DD');
export const subtractOneDay = date => moment(date).subtract(1, 'days').format('YYYY-MM-DD');

export const findDate = (table = []) => {
  if (table.length === 0) {
    return {
      start: '',
      end: ''
    };
  }
  let start = null;
  let end = null;

  if (!table.length) {
    return {
      start: moment(table.start_date).format('YYYYMMDD'),
      end: moment(table.end_date).format('YYYYMMDD')
    };
  }

  if (table.length === 1) {
    return {
      start: moment(table[0].start_date).format('YYYYMMDD'),
      end: moment(table[0].end_date).format('YYYYMMDD')
    };
  }

  table.forEach((data) => {
    if (_.isNull(start) || moment(data.start_date).format('YYYYMMDD') < start) start = moment(data.start_date).format('YYYYMMDD');
    if (_.isNull(end) || moment(data.end_date).format('YYYYMMDD') > end) end = moment(data.end_date).format('YYYYMMDD');
  });

  return { start, end };
};

export const fromNow = datetime => moment(datetime, 'YYYY-MM-DD HH:mm').calendar(null, {
  sameDay: `[${I18n.t('date.sameDay')}] HH:mm`,
  nextDay: `[${I18n.t('date.nextDay')}] HH:mm`,
  nextWeek: `[${I18n.t('date.nextWeek')}] HH:mm`,
  lastDay: `[${I18n.t('date.lastDay')}] HH:mm`,
  lastWeek: 'DD/MM/YYYY HH:mm',
  sameElse: 'DD/MM/YYYY HH:mm'
});

export const getMoment = date => moment(date).calendar(null, {
  sameDay: I18n.t('discussions.messageDateNames.sameDay'),
  nextDay: I18n.t('discussions.messageDateNames.nextDay'),
  nextWeek: I18n.t('discussions.messageDateNames.nextWeek'),
  lastDay: I18n.t('discussions.messageDateNames.lastDay'),
  lastWeek: I18n.t('discussions.messageDateNames.lastWeek'),
  sameElse: I18n.t('discussions.messageDateNames.sameElse')
});

export const monthsList = [
  { label: 'Janvier', value: '01' },
  { label: 'Février', value: '02' },
  { label: 'Mars', value: '03' },
  { label: 'Avril', value: '04' },
  { label: 'Mai', value: '05' },
  { label: 'Juin', value: '06' },
  { label: 'Juillet', value: '07' },
  { label: 'Août', value: '08' },
  { label: 'Septembre', value: '09' },
  { label: 'Octobre', value: '10' },
  { label: 'Novembre', value: '11' },
  { label: 'Décembre', value: '12' }
];

export const yearsList = [
  { label: '2013', value: '2013' },
  { label: '2014', value: '2014' },
  { label: '2015', value: '2015' },
  { label: '2016', value: '2016' },
  { label: '2017', value: '2017' },
  { label: '2018', value: '2018' },
  { label: '2019', value: '2019' },
  { label: '2020', value: '2020' },
  { label: '2021', value: '2021' },
  { label: '2022', value: '2022' }
];

export const getSelectedMonth = date => _.find(monthsList, m => m.value === moment(date).format('MM'));
export const getSelectedYear = date => _.find(yearsList, y => y.value === moment(date).format('YYYY'));
