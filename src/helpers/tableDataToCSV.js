import _ from 'lodash';
import moment from 'moment';

const createFormattedFileName = (currentCompanyName = '', tableName = '') => `${currentCompanyName}-${tableName}-${moment().format('DDMMYYYY-HH[H]mm')}.csv`;

const formatTableDataToCSVData = (formatted_table_data) => {
  let exportHeader = [];
  let exportBody = [];
  if (formatted_table_data.header) {
    exportHeader = _.map(
      formatted_table_data.header.row, row => _.reduce(row.value, (results, val) => {
        if (val._type !== 'checkbox') results.push(val.props.children || val.props.label);
        return results;
      }, [])
    );
  }

  if (formatted_table_data.body) {
    exportBody = _.map(formatted_table_data.body.row, row => _.reduce(row.value,
      (results, val, index) => {
        if (val._type !== 'checkbox') {
          const isDiv = _.get(val, 'props.label.type', false) === 'div';
          if (isDiv) {
            results.push(val.props.children || val.props.labelToExport);
          } else if (val.props && val.props.labelToExport) {
            results.push(val.props.labelToExport);
          } else if (val.props && val.props.label) {
            const toExport = val.props.detail ? `${val.props.label}\n${val.props.detail}` : val.props.label;
            results.push(toExport);
          } else if (val.component) {
            results.push(val.props.placeholder || val.props.defaultValue || val.props.children);
          } else if ('children' in val) {
            results.push(val.children);
          } else if (val._type === 'icon') {
            results.push(val.props.name);
          } else {
            results.push(null);
          }
        } else if (val._type === 'checkbox' && index !== 0) {
          results.push(val.props.checked ? '☑' : '☐');
        }
        return results;
      }, []));
  }
  return [...exportHeader, ...exportBody];
};

export {
  formatTableDataToCSVData,
  createFormattedFileName
};
