const getUrlsParams = () => {
  const { host, href } = window.location;
  let client_id;
  let urlBudgetAdd;
  let urlBudgetModif;

  switch (host) {
  case 'app.myunisoft.fr': // prod
    urlBudgetAdd = 'https://MyUnisoft-bill.biapi.pro/2.0/auth/webview/connect?';
    urlBudgetModif = 'https://MyUnisoft-bill.biapi.pro/2.0/auth/webview/fr/manage?';
    client_id = '96882669';
    break;
  case 'palpatine.myunisoft.fr:5000': // dev
    urlBudgetAdd = 'https://myunisoft-bills-dev-sandbox.biapi.pro/2.0/auth/webview-beta/fr/connect?';
    urlBudgetModif = 'https://myunisoft-bills-dev-sandbox.biapi.pro/2.0/auth/webview-beta/fr/manage?';
    client_id = '8704789';
    break;
  case 'palpatine.myunisoft.fr:5001': // preprod
    urlBudgetAdd = 'https://myunisoft-preprod-sandbox.biapi.pro/2.0/auth/webview-beta/fr/connect?';
    urlBudgetModif = 'https://myunisoft-preprod-sandbox.biapi.pro/2.0/auth/webview-beta/fr/manage?';
    client_id = '74822743';
    break;
  default:
    urlBudgetAdd = 'https://myunisoft-bills-dev-sandbox.biapi.pro/2.0/auth/webview-beta/fr/connect?';
    urlBudgetModif = 'https://myunisoft-bills-dev-sandbox.biapi.pro/2.0/auth/webview-beta/fr/manage?';
    client_id = '8704789';
    break;
  }

  return {
    client_id,
    urlBudgetAdd,
    urlBudgetModif,
    href
  };
};

export const getBudgetInsightUrl = (token, societyId) => {
  const {
    client_id,
    urlBudgetAdd,
    href
  } = getUrlsParams();

  return (`${urlBudgetAdd}connector_capabilities=document&client_id=${client_id}&redirect_uri=${href}/tab/${societyId}/collector#${token}`);
};

export const getBudgetInsightModif = (token, societyId) => {
  const {
    client_id,
    urlBudgetModif,
    href
  } = getUrlsParams();

  return (`${urlBudgetModif}connector_capabilities=document&client_id=${client_id}&redirect_uri=${href}/tab/${societyId}/collector#${token}`);
};

export const objectToQueryString = params => Object.keys(params).map(key => `${key}=${params[key]}`).join('&');
