import _ from 'lodash';
import I18n from 'assets/I18n';
import moment from 'moment';

import { MAIL_FORMAT, PHONE_NUMBER_FORMAT } from 'assets/constants/regex';
import { roundNumber } from 'helpers/number';
import { windev } from 'helpers/api';

export const required = value => (value ? undefined : I18n.t('validations.newPassword.required'));
export const passwordMatch = (value, allValues) => (value !== allValues.newPassword ? I18n.t('validations.newPassword.match') : undefined);

export const emailValidation = value => ((value && !MAIL_FORMAT.test(value)) ? I18n.t('errors.invalid_email') : undefined);
export const loginValidation = values => ((values.mail && !MAIL_FORMAT.test(values.mail)) ? { mail: I18n.t('errors.invalid_email') } : {});

export const newPasswordValidation = (values) => {
  const errors = {};
  if (!values.old) {
    errors.old = I18n.t('validations.newPassword.required');
  }
  if (!values.newPassword) {
    errors.old = I18n.t('validations.newPassword.required');
  }
  if (!values.confirmation) {
    errors.old = I18n.t('validations.newPassword.required');
  }
  if (values.newPassword && values.confirmation && values.newPassword !== values.confirmation) {
    errors.confirmation = I18n.t('validations.newPassword.match');
  }
  return errors;
};

export const societiesRedirect = (values) => {
  const errors = {};
  if (!values.society) {
    errors.society = I18n.t('societiesRedirect.selectSociety');
  }
  return errors;
};

export const newAccountingValidation = (values) => { // eslint-disable-line
  const errors = {};
  const newEntryErrors = [];

  const date = moment(`${_.get(values, 'year.value')}-${_.get(values, 'month.value')}-${values.day}`, 'YYYY-MM-DD');

  if (!values.diary) {
    errors.diary = I18n.t('validations.newAccounting.diary');
  }
  if (!date.isValid()) {
    errors.year = I18n.t('validations.newAccounting.date');
  }
  if (!values.day) {
    errors.day = I18n.t('validations.newAccounting.date');
    errors.month = '';
    errors.year = '';
  }
  if (!values.month) {
    errors.day = '';
    errors.month = I18n.t('validations.newAccounting.date');
    errors.year = '';
  }
  if (!values.year) {
    errors.day = '';
    errors.month = '';
    errors.year = I18n.t('validations.newAccounting.date');
  }
  const diary_type_code = _.get(values, 'diary.diary_type_code');
  if (moment().diff(date) < 0
    && ((diary_type_code === 'ACH')
    || (diary_type_code === 'VTE')
    || (diary_type_code === 'BQ')
    )) {
    errors.year = I18n.t('validations.newAccounting.date_futur');
  }
  _.each(values.entry_list, (entry, i) => {
    const e = {};
    if (_.get(entry, 'piece.length', 0) > 15) {
      e.piece = I18n.t('validations.newAccounting.piece');
    }
    if (_.get(entry, 'piece2.length', 0) > 10) {
      e.piece2 = I18n.t('validations.newAccounting.piece2');
    }
    if (_.isEmpty(entry.account, 10) || !entry.account) {
      e.account = I18n.t('validations.newAccounting.account_number');
    }
    if (!entry.debit && !entry.credit) {
      e.debit = I18n.t('validations.newAccounting.debit');
      e.credit = I18n.t('validations.newAccounting.credit');
    }
    if (values.day && values.month && values.year && entry.deadline) {
      if (moment(`${values.year.value}-${values.month.value}-${values.day}`, 'YYYY-MM-DD').diff(moment(entry.deadline, 'YYYY-MM-DD', 'month')) > 0) {
        e.deadline = I18n.t('validations.newAccounting.deadline');
      }
    }
    newEntryErrors[i] = e;
  });
  errors.entry_list = newEntryErrors;
  return errors;
};

export const societyAssociateValidation = (values) => {
  const errors = {};
  const societyErrors = [];

  _.each(values.society, (firm, index) => {
    const e = {};
    if (!firm.PP) { e.PP = I18n.t('validations.societyAssociateValidation.PP'); }
    if (!firm.NP) { e.NP = I18n.t('validations.societyAssociateValidation.NP'); }
    if (!firm.US) { e.US = I18n.t('validations.societyAssociateValidation.US'); }

    societyErrors[index] = e;
  });
  errors.society = societyErrors;
  return errors;
};

export const newPhysicalPersonValidation = (values) => {
  const {
    civility_id,
    firstname,
    name
  } = values;

  const errors = {};

  if (!civility_id) {
    errors.civility_id = I18n.t('validations.newUser.gender');
  }
  if (!firstname) {
    errors.firstname = I18n.t('validations.newUser.first_name');
  }
  if (!name) {
    errors.name = I18n.t('validations.newUser.last_name');
  }
  return errors;
};

export const newUserValidation = (values) => {
  const errors = {};

  if (!_.get(values, 'civility_code', false)) {
    errors.civility_code = I18n.t('validations.newUser.gender');
  }
  if (!_.get(values, 'name', false)) {
    errors.name = I18n.t('validations.newUser.last_name');
  }
  if (!_.get(values, 'firstname', false)) {
    errors.firstname = I18n.t('validations.newUser.first_name');
  }
  if (!_.get(values, 'mail', false)) {
    errors.mail = I18n.t('validations.newUser.mail');
  }
  if (!MAIL_FORMAT.test(_.get(values, 'mail', false))) {
    errors.mail = I18n.t('validations.newUser.mail_format');
  }
  const tel = _.get(values, 'tel', false);
  if (!tel) {
    errors.tel = I18n.t('validations.newUser.phone_number');
  } else if (!PHONE_NUMBER_FORMAT.test(tel)) {
    errors.tel = I18n.t('validations.newUser.phone_number_format');
  }

  return errors;
};

export const newUserFunctionValidation = (values) => {
  const errors = {};

  if (!_.get(values, 'newUserType', false)) {
    errors.newUserType = I18n.t('validations.newUser.right.type');
  }
  if (!_.get(values, 'newUserProfil', false)) {
    errors.newUserProfil = I18n.t('validations.newUser.right.profil');
  }

  return errors;
};

export const importFecValidation = (values) => {
  const errors = {};

  if (!_.get(values, 'fecImportFrom', false)) {
    errors.fecImportFrom = I18n.t('validations.importEntry.fec_source');
  }
  return errors;
};

export const importComptaSaValidation = (values) => {
  const errors = {};
  const { member_code, society_code } = values;

  if (!member_code) {
    errors.member_code = 'Veuillez renseigner le champ';
  } else if (!society_code) {
    errors.society_code = 'Veuillez renseigner le champ';
  }

  return errors;
};


export const accountValidation = (values) => {
  const errors = {
    account: {}
  };
  if (!_.get(values, 'account.account_number', false)) {
    errors.account.account_number = I18n.t('validations.infoGValidation.account_required');
  }
  if (!_.get(values, 'account.label', false)) {
    errors.account.label = I18n.t('validations.infoGValidation.account_entitled');
  }
  if (_.get(values, 'account.account_number', '').length > 10) {
    errors.account.account_number = I18n.t('validations.infoGValidation.account_length');
  }
  return errors;
};

export const infoGValidation = (values) => {
  const errors = {
    infoG: {}
  };
  const reg = new RegExp('^[a-zA-Z_]$');
  const start_value = _.get(values, 'infoG.account_number[0]');

  if (!_.get(values, 'infoG.account_number', false)) {
    errors.infoG.account_number = I18n.t('validations.infoGValidation.account_required');
  }
  if (+(start_value) === 0 || (+(start_value)) > 7 || reg.test(start_value)) {
    errors.infoG.account_number = I18n.t('validations.infoGValidation.account_begin');
  }
  if (!_.get(values, 'infoG.label', false)) {
    errors.infoG.label = I18n.t('validations.infoGValidation.account_entitled');
  }
  if (_.get(values, 'infoG.account_number', '').length > 10) {
    errors.infoG.account_number = I18n.t('validations.infoGValidation.account_length');
  }

  return errors;
};

export const ribAsyncValidation = async (
  values,
  _dispatch,
  blurredFieldName
) => {
  /**
   * Values validated match the list of
   * asyncBlurFields in companyCreationForm config
   */
  const {
    rib_key,
    banque_code,
    guichet_code,
    banque_account_number
  } = values;
  const body = {
    banque_code,
    guichet_code,
    banque_account_number,
    rib_key
  };
  const errors = {};

  /**
   * In order to avoid multiple calls we
   * recheck the currently blurred field only
   */
  switch (blurredFieldName.asyncBlurFields[0]) {
  case 'rib_key': {
    try {
      if (rib_key && rib_key.length === 2) {
        const { data } = await windev.makeApiCall('/rib/check_rib_key', 'post', {}, body);
        if (data.result === false) {
          errors.rib_key = I18n.t('errors.ArgumentError.invalid_rib_key');
        }
      }
    } catch (error) {
      throw error;
    }
    break;
  }
  default:
    break;
  }

  /**
   * Has to throw an error
   */
  if (Object.keys(errors).length) {
    throw errors;
  }
};

export const bankLinkValidation = (values) => {
  const errors = {};

  const arrayValues = ['day', 'month', 'year', 'bank', 'accountNumber'];
  arrayValues.forEach((item) => {
    if (!values[item]) {
      errors[item] = I18n.t('diary.dialog.empty');
    }
  });

  const {
    year,
    bank,
    accountNumber
  } = values;

  if (_.isEmpty(year)) {
    errors.year = 'Le champ ne doit pas etre vide';
  } else if (year.length !== 4 || Number.isNaN(parseInt(year, 10))) {
    errors.year = 'Format incorrect (AAAA)';
  }
  if (_.isEmpty(bank)) {
    errors.bank = 'Pas de journal de banque saisi pour cette société';
  }
  if (_.isEmpty(accountNumber)) {
    errors.accountNumber = 'Aucun compte n\'a été paramétré pour ce journal';
  }
  return errors;
};

export const lettrageValidation = (values) => {
  const errors = {};
  const regex = /[0-9]/g;
  const lettrage = _.get(values, 'value', '');
  if ((lettrage.search(regex) !== -1) || lettrage.length !== 3) {
    errors.value = I18n.t('lettrageDialog.errorLength');
  }
  return errors;
};

export const functionValidation = (values) => {
  const errors = {};
  if (!_.get(values, 'libelle', false)) {
    errors.libelle = I18n.t('validations.groupFunction.libelle');
  }
  return errors;
};

export const ribValidation = (values) => {
  const errors = {};

  if (!_.get(values, 'banque_account_number', false)) {
    errors.banque_account_number = 'Le numéro de compte est obligatoire';
  }
  if (_.get(values, 'banque_account_number', '').length > 11) {
    errors.banque_account_number = 'Le numéro de compte doit comporter au maximum 11 caractères';
  }
  if (!_.get(values, 'banque_code', false)) {
    errors.banque_code = 'Le numéro de banque est obligatoire';
  }
  if (_.get(values, 'banque_code', '').length !== 5) {
    errors.banque_code = 'Le numéro de banque doit comporter 5 caractères';
  }
  if (!_.get(values, 'guichet_code', false)) {
    errors.guichet_code = 'Le numéro de guichet est obligatoire';
  }
  if (_.get(values, 'guichet_code', '').length !== 5) {
    errors.guichet_code = 'Le numéro de guichet doit comporter 5 caractères';
  }
  if (!_.get(values, 'bic', false)) {
    errors.bic = 'Le numéro BIC est obligatoire';
  }
  if (_.get(values, 'bic', '').length > 11) {
    errors.bic = 'Le numéro BIC doit comporter au maximum 11 caractères';
  }
  if (_.get(values, 'guichet_code', '').length !== 5) {
    errors.guichet_code = 'Le numéro de guichet doit comporter 5 caractères';
  }
  if (!_.get(values, 'iban', false)) {
    errors.iban = 'L\'Iban est obligatoire';
  }
  if (_.get(values, 'iban', '').length > 34) {
    errors.iban = 'L\'Iban doit comporter 34 caractères au maximum';
  }
  if (!_.get(values, 'rib_key', false)) {
    errors.rib_key = 'La clé rib est obligatoire';
  }
  if (_.get(values, 'rib_key', '').toString().length !== 2) {
    errors.rib_key = 'La clé rib doit comporter 2 caractères';
  }
  if (!_.get(values, 'diary_id', false)) {
    errors.diary_id = 'Le journal est obligatoire';
  }
  return errors;
};

export const verifyDebitCredit = (entryList) => {
  let credit = 0;
  let debit = 0;

  entryList.forEach((entry) => {
    credit += roundNumber(_.get(entry, 'credit', 0));
    debit += roundNumber(_.get(entry, 'debit', 0));
  });

  return roundNumber(debit) === roundNumber(credit);
};

const numberRegex = (/^\d+$/);
const parseExerciseDates = date => moment(date, 'YYYYMMDD');

export const companyCreationValidation = (values) => {
  const errors = {};
  const {
    password,
    confirmation,
    siret,
    name,
    owner_company,
    postal_code,
    exercises,
    capital,
    partTotal,
    nominalValue,
    associates
  } = values;

  if (exercises) {
    errors.exercises = exercises.reduce((exerciseErrors, exercise, i) => {
      const currentExerciseError = {};

      const {
        duration,
        start_date,
        end_date
      } = exercise;
      if (!moment(start_date).isValid() && start_date !== '') {
        currentExerciseError.start_date = I18n.t('companyCreation.errors.formatDateError');
      } else if (!moment(end_date).isValid() && end_date !== '') {
        currentExerciseError.end_date = I18n.t('companyCreation.errors.formatDateError');
      }
      // Duration error check
      if (duration < 0 || duration >= 24) {
        currentExerciseError.duration = I18n.t('companyCreation.errors.duration');
      }

      /**
       * Date overlapping check
       *
       * Checks the overlap of the current and next exercise
       * N.B. "next" exercise is actually the previous entry in the exercises array
       */
      const nextExercise = exercises[i - 1] || {};
      const nextExerciseMomentStartDate = nextExercise.start_date
        && parseExerciseDates(typeof nextExercise.start_date === 'string' ? +nextExercise.start_date : nextExercise.start_date);
      const momentStartDate = start_date
        && parseExerciseDates(typeof start_date === 'string' ? +start_date : start_date);
      const momentEndDate = end_date
        && parseExerciseDates(typeof end_date === 'string' ? +end_date : end_date);

      if (momentStartDate && nextExerciseMomentStartDate) {
        if (momentStartDate.isSameOrAfter(nextExerciseMomentStartDate)) {
          currentExerciseError.start_date = I18n.t('companyCreation.errors.start_date');
        }
      }

      if (momentEndDate && nextExerciseMomentStartDate) {
        if (momentEndDate.isSameOrAfter(nextExerciseMomentStartDate)) {
          currentExerciseError.end_date = I18n.t('companyCreation.errors.end_date');
        }
      }

      exerciseErrors.push(currentExerciseError);

      return exerciseErrors;
    }, []);
  }

  if (!name) {
    errors.name = I18n.t('companyCreation.errors.name');
  }

  if (!owner_company) {
    errors.owner_company = I18n.t('companyCreation.errors.owner_company');
  }

  if (capital) {
    if (!numberRegex.test(capital)) {
      errors.capital = I18n.t('companyCreation.errors.shareValue');
    }
  }

  if (partTotal) {
    if (!numberRegex.test(partTotal)) {
      errors.partTotal = I18n.t('companyCreation.errors.shareValue');
    }
  }

  if (nominalValue) {
    if (!numberRegex.test(nominalValue)) {
      errors.nominalValue = I18n.t('companyCreation.errors.shareValue');
    }
  }

  if (postal_code) {
    if (!numberRegex.test(postal_code) || postal_code.length !== 5) {
      errors.postal_code = I18n.t('companyCreation.errors.postal_code');
    }
  }

  if (siret) {
    const notANumber = !numberRegex.test(siret);
    const incorrectLength = !(siret.length === 9 || siret.length === 14);

    if (notANumber || incorrectLength) {
      errors.siret = I18n.t('companyCreation.errors.siret');
    }
  }

  if (
    (!password && confirmation)
    || (password && password !== confirmation)
  ) {
    errors.confirmation = I18n.t('companyCreation.errors.password');
  }

  if (associates) {
    const arrayPerson = _.get(associates, 'arrayPerson', {});
    errors.associates = {
      arrayPerson: {},
      arraySociety: {}
    };

    if (associates.capital !== null && !associates.effective_date) {
      errors.associates.effective_date = I18n.t('companyCreation.errors.effective_date');
    }

    Object.keys(arrayPerson).forEach((key) => {
      const associatesError = {
        social_part: {}
      };

      const personPhysical = arrayPerson[key];
      if (personPhysical) {
        const {
          signatory_function,
          effective_date,
          social_part
        } = personPhysical;

        if (!signatory_function) {
          associatesError.signatory_function = I18n.t('companyCreation.errors.function_signatory');
        }
        if (_.get(personPhysical, 'function.id', null) === null) {
          associatesError.function = I18n.t('companyCreation.errors.function');
        }
        if (!effective_date) {
          associatesError.effective_date = I18n.t('companyCreation.errors.effective_date');
        }
        if (!social_part || !social_part.PP) {
          associatesError.social_part.PP = I18n.t('companyCreation.errors.social_part.PP');
        }
        if (!social_part || !social_part.NP) {
          associatesError.social_part.NP = I18n.t('companyCreation.errors.social_part.NP');
        }
        if (!social_part || !social_part.US) {
          associatesError.social_part.US = I18n.t('companyCreation.errors.social_part.US');
        }
      }

      errors.associates.arrayPerson[key] = associatesError;
    });

    const arraySociety = _.get(associates, 'arraySociety', {});

    Object.keys(arraySociety).forEach((key) => {
      const associatesError = {
        social_part: {}
      };

      const society = arraySociety[key];
      if (society) {
        const {
          signatory_function,
          effective_date,
          social_part
        } = society;

        if (!signatory_function) {
          associatesError.signatory_function = I18n.t('companyCreation.errors.function_signatory');
        }
        if (!effective_date) {
          associatesError.effective_date = I18n.t('companyCreation.errors.effective_date');
        }
        if (!social_part || !social_part.PP) {
          associatesError.social_part.PP = I18n.t('companyCreation.errors.social_part.PP');
        }
        if (!social_part || !social_part.NP) {
          associatesError.social_part.NP = I18n.t('companyCreation.errors.social_part.NP');
        }
        if (!social_part || !social_part.US) {
          associatesError.social_part.US = I18n.t('companyCreation.errors.social_part.US');
        }
      }

      errors.associates.arraySociety[key] = associatesError;
    });
  }
  return errors;
};


export const companyCreationAsyncValidation = async (
  values,
  _dispatch,
  props,
  blurredFieldName
) => {
  /**
   * Values validated match the list of
   * asyncBlurFields in companyCreationForm config
   */
  const {
    siret
  } = values;

  const {
    asyncErrors = {}
  } = props;

  const errors = { ...asyncErrors };

  /**
   * In order to avoid multiple calls we
   * recheck the currently blurred field only
   */
  switch (blurredFieldName) {
  case 'siret': {
    try {
      if (siret && (siret.length === 9 || siret.length === 14)) {
        await windev.makeApiCall('/validate_SIRET', 'get', { siret });
      }
    } catch (error) {
      errors.siret = _.get(error, 'response.data.message');
    }
    break;
  }
  default:
  }

  /**
   * Has to throw an error
   */
  if (Object.keys(errors).length) {
    throw errors;
  }
};

export const vatFormValidation = (values) => {
  const errors = {};

  const arrayValues = ['account_coll', 'account_ded', 'exigility', 'taux', 'type'];
  arrayValues.forEach((item) => {
    if (!values[item]) {
      errors[item] = I18n.t('vatForm.empty');
    }
  });

  const {
    code
  } = values;

  if (_.isEmpty(code)) {
    errors.code = I18n.t('vatForm.empty');
  } else if (code.length !== 2 && code.length !== 1) {
    errors.code = I18n.t('vatForm.errorSizeCode');
  }

  return errors;
};

export const diaryFormValidation = (values) => {
  const errors = {};
  const {
    diary_code,
    diary_label,
    diary_type,
    diary_accountNumber
  } = values;

  if (!diary_code) {
    errors.diary_code = I18n.t('diary.dialog.empty_code');
  } else if (diary_code.length < 2 || diary_code.length > 4) {
    errors.diary_code = I18n.t('diary.dialog.errorSizeCode');
  }
  if (!diary_label) {
    errors.diary_label = I18n.t('diary.dialog.empty_label');
  }
  if (!diary_type) {
    errors.diary_type = I18n.t('diary.dialog.empty_type');
  }
  if (!diary_accountNumber) {
    errors.diary_accountNumber = I18n.t('diary.dialog.empty');
  }
  return errors;
};

export const flagFormValidation = (values) => {
  const errors = {};
  const {
    dateStart,
    dateEnd,
    vat_account,
    ttc_account,
    dossier_revision,
    date,
    duration
  } = values;

  if (!dateStart) { errors.dateStart = I18n.t('flagForm.validation.dateStart'); }
  if (!dateEnd) { errors.dateEnd = I18n.t('flagForm.validation.dateEnd'); }
  if (!vat_account) { errors.vat_account = I18n.t('flagForm.validation.vat_account'); }
  if (!ttc_account) { errors.ttc_account = I18n.t('flagForm.validation.ttc_account'); }
  if (!dossier_revision) { errors.dossier_revision = I18n.t('flagForm.validation.dossier_revision'); }

  if (!date) { errors.date = I18n.t('flagForm.validation.dateStart'); }
  if (duration < 0) { errors.duration = I18n.t('flagForm.validation.duration'); }
  if (!duration) { errors.duration = I18n.t('flagForm.validation.missing_duration'); }

  return errors;
};

export const worksheetSettingsValidation = (values) => {
  const errors = {};
  const {
    id_worksheet_type,
    id_account_ttc,
    id_account_tva
  } = values;

  const requaredTVA = ['FNP', 'AAR', 'AAE'];

  if (!id_worksheet_type) {
    errors.id_worksheet_type = I18n.t('diary.dialog.empty');
  }
  if (!id_account_ttc) {
    errors.id_account_ttc = I18n.t('diary.dialog.empty');
  }

  const tva_requared = id_worksheet_type
    ? requaredTVA.find(item => item === id_worksheet_type.code) : null;
  if ((id_worksheet_type && tva_requared) && !id_account_tva) {
    errors.id_account_tva = I18n.t('diary.dialog.empty');
  }
  return errors;
};

export const bankIntegrationValidation = (values) => {
  const errors = {};
  const {
    account_id,
    code_interbancaire,
    allCodes,
    label_to_find
  } = values;

  if (!account_id) { errors.account_id = I18n.t('bankIntegrationSettings.popUp.empty'); }
  if (!code_interbancaire && !allCodes) { errors.list_id_code_interbancaire = I18n.t('bankIntegrationSettings.popUp.empty'); }
  if (!label_to_find) { errors.label_to_find = I18n.t('bankIntegrationSettings.popUp.empty'); }
  return errors;
};

export const portfolioDialogValidation = (values) => {
  const errors = {};
  const {
    libelle,
    societies
  } = values;

  if (!libelle) { errors.libelle = I18n.t('portfolioListView.dialog.portfolioNameError'); }
  if (!societies) { errors.societies = I18n.t('portfolioListView.dialog.portfolioNameError'); }

  return errors;
};

export const gedSearchValidation = (values) => {
  const errors = {};
  const {
    dateStart,
    dateEnd
  } = values;

  if (!dateStart) { errors.date_period_from = I18n.t('ged.filter.emptyError'); }
  if (!dateEnd) { errors.date_period_to = I18n.t('ged.filter.emptyError'); }

  return errors;
};

export const reportsAndFormsDialogValidation = () => {
};

export const scanAssociateFormValidation = (values) => {
  const errors = {};
  const {
    associationCode
  } = values;

  if (!associationCode) { errors.associationCode = I18n.t('scanAssociate.form.emptyError'); }

  return errors;
};

export const validateSilae = (values) => {
  const errors = {};
  const {
    loginSilae,
    passwordSilaeConfirm,
    loginWsSilae,
    passwordSilaeConfirmed
  } = values;

  if (!loginSilae) { errors.loginSilae = I18n.t('accountingFirmSettings.emptyError'); }
  if (!loginWsSilae) { errors.loginWsSilae = I18n.t('accountingFirmSettings.emptyError'); }
  if (!passwordSilaeConfirm) { errors.passwordSilaeConfirm = I18n.t('accountingFirmSettings.emptyError'); }
  if (!passwordSilaeConfirmed) { errors.passwordSilaeConfirmed = I18n.t('accountingFirmSettings.emptyError'); }
  if (loginSilae !== loginWsSilae) { errors.loginWsSilae = I18n.t('accountingFirmSettings.emptyError'); }
  if (passwordSilaeConfirm !== passwordSilaeConfirmed) { errors.passwordSilaeConfirmed = I18n.t('accountingFirmSettings.equal'); }
  return errors;
};

export const validateAccountingFirmGeneralInfo = (values) => {
  const errors = {};
  const {
    cabinetName,
    postalCode,
    fax,
    email,
    phone_number
  } = values;

  if (!cabinetName) { errors.cabinetName = I18n.t('accountingFirmSettings.emptyError'); }
  if (postalCode && postalCode.length > 5) { errors.postalCode = I18n.t('accountingFirmSettings.tooLong'); }
  if (phone_number && phone_number.length > 13) { errors.phone_number = I18n.t('accountingFirmSettings.tooLong'); }
  if (fax && fax.length > 13) { errors.fax = I18n.t('accountingFirmSettings.tooLong'); }
  if (emailValidation(email)) { errors.email = emailValidation(email); }
  return errors;
};

export const settingStandardMailFormValidation = (values) => {
  const errors = {};
  const {
    name
  } = values;

  if (!name) { errors.name = I18n.t('ged.filter.emptyError'); }
  return errors;
};

export const accountingFirmDeclareFormValidation = (values) => {
  const errors = {};
  const {
    mail
  } = values;
  if (emailValidation(mail)) { errors.mail = emailValidation(mail); }
  return errors;
};

export const optional = () => {};
export const empty = v => v !== 0 && !v && I18n.t('common.errors.empty');
export const negative = v => parseFloat(v) < 0 && I18n.t('common.errors.negative');
export const invalidDate = v => v && (!moment(v, 'YYYY-MM-DD').isValid() && !moment(v, 'DD/MM/YYYY').isValid()) && I18n.t('common.errors.invalidDate');
export const isNaN = v => !v && typeof v !== 'number' && I18n.t('common.errors.isNaN');
export const validateImmo = immo => ({
  id_type_amort: immo.id_type_amort !== 1 && immo.id_type_amort !== 2 ? I18n.t('common.errors.invalid') : undefined,
  provider: empty(immo.provider),
  label: empty(immo.label),
  purchase_date: empty(immo.purchase_date) || invalidDate(immo.purchase_date),
  purchase_value: empty(immo.purchase_value),
  month: empty(immo.month),
  start_date: empty(immo.start_date) || invalidDate(immo.start_date),
  end_date: invalidDate(immo.end_date),
  sale_value: empty(immo.sale_value)
});

export const planFormValidate = (values) => {
  const errors = {};
  if (!values.society) {
    errors.society = I18n.t('societiesRedirect.selectSociety');
  }
  if (!values.label) {
    errors.label = I18n.t('accountingPlans.dialogs.plan.empty');
  }

  return errors;
};

export const connectorFormValidation = (values) => {
  const errors = {};
  const {
    id_society,
    id_third_party_api,
    code
  } = values;
  if (!id_society) {
    errors.id_society = I18n.t('common.errors.empty');
  }
  if (!id_third_party_api) {
    errors.id_third_party_api = I18n.t('common.errors.empty');
  }
  if (!code) {
    errors.code = I18n.t('common.errors.empty');
  }
  return errors;
};

export const planDetailFormValidate = (values) => {
  const errors = {};

  const arrayValues = ['account_number', 'label'];
  arrayValues.forEach((item) => {
    if (!values[item]) {
      errors[item] = I18n.t('accountingPlans.dialogs.planDetail.empty');
    }
  });

  return errors;
};

export const ODFormValidation = (values) => {
  const errors = {};
  const { amount_0, amount_1, amount_2 } = values;
  if (+amount_1 > +amount_0 && +amount_1 !== 0) {
    errors.amount_1 = 'Le montant saisi ne peut être supérieur au montant initial';
  }
  if (+amount_2 > +amount_1 && +amount_2 !== 0) {
    errors.amount_2 = 'Le montant saisi ne peut être supérieur au montant initial';
  }
  return errors;
};
