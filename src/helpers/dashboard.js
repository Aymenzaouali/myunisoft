// Import
import moment from 'moment';
import I18n from 'assets/I18n';
import { getRouteByKey } from 'helpers/routes';

// Const
export const DISPLAYTYPEFIELDS = {
  0: ['company_name', 'closure', 'month01', 'month02', 'month03', 'month04', 'month05', 'month06', 'month07', 'month08', 'month09', 'month10', 'month11', 'month12'],
  1: ['company_name', 'endAt', 'vat', 'bank_flow', 'ocr_flow', 'manual_flow', 'lettering', 'amount'],
  2: ['company_name', 'closure', 'vat', 'first_is_deposit', 'cvae'],
  3: ['company_name', 'closure', 'is', 'balance_sheet', 'cvae', 'cfe', 'ago']
};

export const HISTORYABBREVIATIONSBYKEY = {
  statement: 'BLN',
  vat: 'TVA',
  is: 'IS',
  is1: 'IS1',
  is2: 'IS2',
  is3: 'IS3',
  is4: 'IS4',
  cvae: 'CVAE',
  ago: 'AGO'
};

export const HISTORYMONTHVALUE = {
  month_01: '01',
  month_02: '02',
  month_03: '03',
  month_04: '04',
  month_05: '05',
  month_06: '06',
  month_07: '07',
  month_08: '08',
  month_09: '09',
  month_10: '10',
  month_11: '11',
  month_12: '12'
};

export const TOGGLEVALUE = {
  0: '',
  1: 'dashboardCollabWeek',
  2: 'dashboardCollabMonth',
  3: 'dashboardCollabYear'
};

// Utils
export const formatDashboardData = (mode, data) => {
  let formatedData;

  switch (mode) {
  case 0: // Historic
    formatedData = data.map((row) => {
      const {
        company_id,
        company_name,
        endAt,
        month_01,
        month_02,
        month_03,
        month_04,
        month_05,
        month_06,
        month_07,
        month_08,
        month_09,
        month_10,
        month_11,
        month_12
      } = row;

      return {
        company: { type: 'folderName', company_id, company_name },
        endAt,
        month_01: month_01 === null ? month_01 : { ...month_01, company_id },
        month_02: month_02 === null ? month_02 : { ...month_02, company_id },
        month_03: month_03 === null ? month_03 : { ...month_03, company_id },
        month_04: month_04 === null ? month_04 : { ...month_04, company_id },
        month_05: month_05 === null ? month_05 : { ...month_05, company_id },
        month_06: month_06 === null ? month_06 : { ...month_06, company_id },
        month_07: month_07 === null ? month_07 : { ...month_07, company_id },
        month_08: month_08 === null ? month_08 : { ...month_08, company_id },
        month_09: month_09 === null ? month_09 : { ...month_09, company_id },
        month_10: month_10 === null ? month_10 : { ...month_10, company_id },
        month_11: month_11 === null ? month_11 : { ...month_11, company_id },
        month_12: month_12 === null ? month_12 : { ...month_12, company_id }
      };
    });

    break;
  case 1: // Week
    formatedData = data.map((row) => {
      const {
        company_id,
        company_name,
        endAt,
        vat,
        bank_flow,
        ocr_flow,
        manual_flow,
        amount,
        lettering,
        secured
      } = row;

      return {
        company: {
          type: 'folderName', company_id, company_name, secured
        },
        endAt,
        vat: vat === null ? vat : { ...vat, company_id },
        bank_flow: bank_flow === null ? bank_flow : { ...bank_flow, company_id },
        ocr_flow: ocr_flow === null ? ocr_flow : { ...ocr_flow, company_id },
        manual_flow: manual_flow === null ? manual_flow : { ...manual_flow, company_id },
        lettering,
        amount: amount === null ? amount : { ...amount, company_id }
      };
    });
    break;
  case 2: // Month
    formatedData = data.map((row) => {
      const {
        company_id,
        company_name,
        closure,
        vat,
        first_is_deposit,
        cvae,
        secured
      } = row;

      return {
        company: {
          type: 'folderName', company_id, company_name, secured
        },
        closure,
        vat: vat === null ? vat : { ...vat, company_id },
        first_is_deposit: first_is_deposit === null
          ? first_is_deposit
          : { ...first_is_deposit, company_id },
        cvae: cvae === null ? cvae : { ...cvae, company_id }
      };
    });

    break;
  case 3: // Year
    formatedData = data.map((row) => {
      const {
        company_id,
        company_name,
        closure,
        is,
        balance_sheet,
        cvae,
        cfe,
        ago,
        secured
      } = row;

      return {
        company: {
          type: 'folderName', company_id, company_name, secured
        },
        closure,
        is: is === null
          ? is
          : { ...is, company_id },
        balance_sheet: balance_sheet === null ? balance_sheet : { ...balance_sheet, company_id },
        cvae: cvae === null ? cvae : { ...cvae, company_id },
        cfe: cfe === null ? cfe : { ...cfe, company_id },
        ago: ago === null ? ago : { ...ago, company_id }
      };
    });
    break;
  default:
    break;
  }

  return formatedData;
};

export const getRouteKeyForDashboard = (type) => {
  switch (type) {
  case 'is':
    return 'downPayment';
  case 'vat':
    return 'tvaca3';
  case 'cvae':
    return 'balanceCvae';
  case 'bank_flow':
    return 'ib';
  case 'ocr_flow':
    return 'o';
  case 'manual_flow':
    return 'm';
  case 'amount':
    return 'e';
  case 'first_is_deposit':
    return 'downPayment';
  case 'balance_sheet':
    return 'taxDeclaration';
  case 'cfe':
    return '';
  case 'ago':
    return '';
  default:
    return null;
  }
};

export const getMonthBounds = (month) => {
  const start = `20${month.substring(2)}${month.substring(0, 2)}01`;
  const end = moment(start).endOf('month').format('YYYYMMDD');
  return { start, end };
};

export const initPeriod = async (period, initFilter, initHeader, headerName) => {
  await initFilter({ period });
  await initHeader(headerName, period);
};

export const genParamLink = (id_diligence, link_type, month, company_id) => (
  {
    id_diligence,
    'MM-AAAA': moment(month).format('MM-YYYY'),
    link_type,
    society_id: company_id
  }
);

export const rightClickActions = (
  MMAA,
  id_diligence,
  company_id,
  redirectFromCollab = () => {},
  getLink = () => {},
  type,
  substractMonth,
  initFilter,
  initHeader,
  headerName
) => {
  const date = getMonthBounds(MMAA);
  const currentMonth = substractMonth ? moment(date.start).subtract(1, 'month').format('YYYY-MM') : moment(date.start).format('YYYY-MM');

  const routeKey = getRouteKeyForDashboard(type);

  return (
    [
      {
        label: I18n.t(`dashboard.society.rightClick.modify${type}`),
        onClick: () => {
          initPeriod(currentMonth, initFilter, initHeader, headerName);
          const route = getRouteByKey(routeKey);
          redirectFromCollab(route.path, '', company_id);
        }
      },
      {
        label: I18n.t('dashboard.society.rightClick.dwnldPDF'),
        onClick: async () => {
          const link = await getLink(genParamLink(id_diligence, 1, currentMonth, company_id));

          if (link !== '') window.open(link);
        },
        disabled: !id_diligence
      },
      {
        label: I18n.t('dashboard.society.rightClick.summary'),
        onClick: async () => {
          const link = await getLink(genParamLink(id_diligence, 2, currentMonth, company_id));

          if (link !== '') window.open(link);
        },
        disabled: !id_diligence
      }
    ]
  );
};
