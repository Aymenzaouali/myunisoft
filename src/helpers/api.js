import {
  windev_conf,
  service_auth_conf,
  service_notification_conf,
  mock_conf,
  service_discussion_conf,
  service_ged_conf
} from 'common/config/server';
import Axios from 'axios';
import _get from 'lodash/get';
import { store, persistor } from 'redux/store';
import { enqueueSnackbar } from 'common/redux/notifier/action';
import { Notifications } from 'common/helpers/SnackBarNotifications';
import { push } from 'connected-react-router';
import loginActions from 'common/redux/login/actions';

import { dismissError } from 'helpers/error';

const config = config => config;
const error = async (error) => {
  const errorMessage = _get(error, 'response.data.message');
  if (errorMessage === 'invalidated_token') {
    await persistor.purge();
    await store.dispatch(loginActions.signOutSuccess());
    store.dispatch(push('/'));
  } else if (dismissError(errorMessage)) {
    store.dispatch(
      enqueueSnackbar(
        Notifications(
          _get(error, 'response.data.message')
          || _get(error, 'response.data')
          || _get(error, 'message')
          || _get(error),
          'warning'
        )
      )
    );
  }
  return Promise.reject(error);
};

const windevInstance = Axios.create({
  baseURL: `${windev_conf.protocol}://${windev_conf.baseUrl}`
});

windevInstance.interceptors.response.use(config, error);

const serviceAuthInstance = Axios.create({
  baseURL: `${service_auth_conf.protocol}://${service_auth_conf.baseUrl}:${service_auth_conf.port}/api`
});

serviceAuthInstance.interceptors.response.use(config, error);

const serviceNotificationInstance = Axios.create({
  baseURL: `${service_notification_conf.protocol}://${service_notification_conf.baseUrl}:${service_notification_conf.port}/api`
});

const serviceDiscussionInstance = Axios.create({
  baseURL: `${service_discussion_conf.protocol}://${service_discussion_conf.baseUrl}:${service_discussion_conf.port}`
});

serviceDiscussionInstance.interceptors.response.use(config, error);

const serviceGEDInstance = Axios.create({
  baseURL: `${service_ged_conf.protocol}://${service_ged_conf.baseUrl}:${service_ged_conf.port}`
});

serviceGEDInstance.interceptors.response.use(config, error);

const mockInstance = Axios.create({
  baseURL: `${mock_conf.protocol}://${mock_conf.baseUrl}`
});


const prepareRequest = (route, method, payload = {}, data, { ...customParams }) => {
  const state = store.getState();
  const token = _get(state, 'login.token.access_token', false);
  const societyId = _get(state, 'navigation.id', '');
  const req = {
    url: route,
    method,
    params: payload,
    // headers: { 'Content-Type': 'application/json' },
    headers: {
      'Content-Type': 'application/json',
      'society-id': societyId
    },
    ...customParams
  };
  if (data) {
    req.data = data;
  }
  if (token) {
    req.headers = { ...req.headers, Authorization: `Bearer ${token}` };
  }
  return req;
};

const makeApiCall = async (route, method, payload = {}, data, { ...customParams }) => {
  const req = prepareRequest(route, method, payload, data, { ...customParams });

  return process.env.REACT_APP_MOCK ? mockInstance.request(req) : windevInstance.request(req);
};

const makeServiceDiscussionApiCall = async (
  route, method, payload = {}, data, { ...customParams }
) => {
  const req = prepareRequest(route, method, payload, data, { ...customParams });
  return (
    (process.env.REACT_APP_MOCK && mockInstance.request(req))
      || serviceDiscussionInstance.request(req)
  );
};

const makeServiceGEDApiCall = async (
  route, method, payload = {}, data, { ...customParams }
) => {
  const req = prepareRequest(route, method, payload, data, { ...customParams });
  return serviceGEDInstance.request(req);
};

const makeServiceAuthApiCall = async (route, method, payload = {}, data, { ...customParams }) => {
  // console.log(route, method, payload, data);
  const req = prepareRequest(`${process.env.REACT_APP_MOCK ? `/api${route}` : route}`, method, payload, data, { ...customParams });
  return process.env.REACT_APP_MOCK ? mockInstance.request(req) : serviceAuthInstance.request(req);
};

const makeMockCall = async (route, method, payload = {}, data, { ...customParams }) => {
  const req = prepareRequest(route, method, payload, data, { ...customParams });
  return mockInstance.request(req);
};

const makeServiceNotificationApiCall = async (
  route,
  method,
  payload = {},
  data,
  { ...customParams }
) => {
  const req = prepareRequest(route, method, payload, data, { ...customParams });
  return process.env.REACT_APP_MOCK
    ? mockInstance.request(req)
    : serviceNotificationInstance.request(req);
};

export const mock = {
  makeMockCall
};

export const windev = {
  makeApiCall
};

export const service_discussion = {
  makeApiCall: makeServiceDiscussionApiCall
};

export const service_ged = {
  makeApiCall: makeServiceGEDApiCall
};

export const service_auth = {
  makeApiCall: makeServiceAuthApiCall
};

export const service_notification = {
  makeApiCall: makeServiceNotificationApiCall
};
