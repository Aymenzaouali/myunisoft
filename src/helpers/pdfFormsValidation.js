import I18n from 'assets/I18n';
import { isEmpty } from 'lodash';
import { emailValidation } from 'helpers/validation';
// import { sum } from 'helpers/pdfforms';

// const tvaca3 = ['KT', 'GJ', 'GK'];

export const tvaValidation = (values, form) => {
  const errors = {};

  switch (form) {
  case 'ca12Annual_3517SCA12':
    if (values.FB && !values.FA) errors.FA = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 6D
    if (values.FD && !values.FC) errors.FC = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 8B
    if (values.FP && !values.EP) errors.EP = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 14
    if ((values.WQ || values.WR || values.WS) && !values.ZN) errors.ZN = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 76 77 78
    if ((values.WU || values.WT) && !values.ZP) errors.ZP = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 80
    if (values.SV && !values.SW) errors.SW = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 88
    if (values.SX && !values.XA) errors.XA = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 89
    if (values.RD && !values.XB) errors.XB = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 90
    if (values.FJ && !values.EJ) errors.EJ = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 9
    if (values.FG && !values.EG) errors.EG = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 10
    if (values.FY && !values.EY) errors.EY = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line AA
    if (values.VP && !values.VN) errors.VN = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line AB
    if (values.EZ && !values.EH) errors.EH = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line AC
    if (values.FL && !values.EL) errors.EL = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 11
    if (values.FM && !values.EM) errors.EM = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 12
    if (values.FN && !values.EN) errors.EN = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 13
    if (values.EP && !values.FP) errors.FP = I18n.t('pdfValidation.tvaca12Annual.empty'); // Line 14
    break;
  case 'tva3310':
    if (values.CA && !values.BA) errors.BA = I18n.t('pdfValidation.tva3310.empty'); // Line 35
    if (values.BA && !values.CA) errors.CA = I18n.t('pdfValidation.tva3310.empty'); // Line 35
    if (values.CC && !values.BC) errors.BC = I18n.t('pdfValidation.tva3310.empty'); // Line 37
    if (values.BC && !values.CC) errors.CC = I18n.t('pdfValidation.tva3310.empty'); // Line 37
    if (values.AF && !values.AE) errors.AE = I18n.t('pdfValidation.tva3310.empty'); // Line 38
    if (values.AE && !values.AF) errors.AF = I18n.t('pdfValidation.tva3310.empty'); // Line 38
    if (values.CM && !values.BM) errors.BM = I18n.t('pdfValidation.tva3310.empty'); // Line 43
    if (values.BM && !values.CM) errors.CM = I18n.t('pdfValidation.tva3310.empty'); // Line 43
    if (values.CR && !values.BR) errors.BR = I18n.t('pdfValidation.tva3310.empty'); // Line 46
    if (values.BR && !values.CR) errors.CR = I18n.t('pdfValidation.tva3310.empty'); // Line 46
    if ((values.LS || values.LR || values.LQ) && !values.MR) errors.MR = I18n.t('pdfValidation.tva3310.empty'); // Line 101 102 103
    if ((values.LU || values.LT) && !values.MS) errors.MS = I18n.t('pdfValidation.tva3310.empty'); // Line 104 105
    if (values.NZ && !values.NT) errors.NT = I18n.t('pdfValidation.tva3310.empty'); // Line 109
    if (values.NT && !values.NZ) errors.NZ = I18n.t('pdfValidation.tva3310.empty'); // Line 109
    if (values.PA && !values.NU) errors.NU = I18n.t('pdfValidation.tva3310.empty'); // Line 110
    if (values.NU && !values.PA) errors.PA = I18n.t('pdfValidation.tva3310.empty'); // Line 110
    if (values.NR && !values.MU) errors.MU = I18n.t('pdfValidation.tva3310.empty'); // Line 117
    if (values.NR && !values.MV) errors.MV = I18n.t('pdfValidation.tva3310.empty'); // Line 117
    if ((values.MU || values.MV) && !values.NR) errors.NR = I18n.t('pdfValidation.tva3310.empty'); // Line 117
    if (values.NS && !values.MW) errors.MW = I18n.t('pdfValidation.tva3310.empty'); // Line 118
    if (values.NS && !values.MX) errors.MX = I18n.t('pdfValidation.tva3310.empty'); // Line 118
    if ((values.MW || values.MX) && !values.NS) errors.NS = I18n.t('pdfValidation.tva3310.empty'); // Line 118
    if (values.NC && !values.PG) errors.PG = I18n.t('pdfValidation.tva3310.empty'); // Line 119
    if (values.PG && !values.NC) errors.NC = I18n.t('pdfValidation.tva3310.empty'); // Line 119
    if (values.PU && !values.PW) errors.PW = I18n.t('pdfValidation.tva3310.empty'); // Line 124
    if (values.PW && !values.PU) errors.PU = I18n.t('pdfValidation.tva3310.empty'); // Line 124

    if (values.PX && !values.PY) errors.PY = I18n.t('pdfValidation.tva3310.empty'); // Line 125
    if (values.PY && !values.PX) errors.PX = I18n.t('pdfValidation.tva3310.empty'); // Line 125

    if (values.PZ && !values.QA) errors.QA = I18n.t('pdfValidation.tva3310.empty'); // Line 126
    if (values.QA && !values.PZ) errors.PZ = I18n.t('pdfValidation.tva3310.empty'); // Line 126
    break;
  case 'tvaca3':
    // tvaca3.forEach((item) => {
    //   if (!values[item]) {
    //     errors[item] = I18n.t('pdfValidation.tvaca3.empty');
    //   }
    // });
    // if (sum(values, ['KT', 'GJ', 'GK']) > values.GH) {
    //   errors.GH = I18n.t('pdfValidation.tvaca3.GH');
    // }
    // if (sum(values, ['KJ', 'JB']) > values.JA) {
    //   errors.JA = I18n.t('pdfValidation.tvaca3.JA');
    // }
    // if (sum(values, ['KB', 'KL']) > values.KA) {
    //   errors.KA = I18n.t('pdfValidation.tvaca3.KA');
    // }
    // if (values.HG < values.HF) {
    //   errors.HF = I18n.t('pdfValidation.tvaca3.HF');
    // }
    break;

  case 'tva3519':
    if (values.DCW && emailValidation(values.DCW)) {
      errors.DCW = I18n.t('pdfValidation.invalidEmail');
    }
    if (values.DCH && values.DCH.length < 5) {
      errors.DCH = I18n.t('pdfValidation.postalCode');
    }
    break;
  default:
    break;
  }
  if (!isEmpty(errors)) throw errors;
};
