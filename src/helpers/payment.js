/**
 *
 * @param payments
 * @param length
 * @returns {[]}
 */
export const buildPaymentsList = (payments, length) => {
  const paymentsList = [];
  let i = 0;
  while (i < length) {
    if (payments[i]) {
      paymentsList.push({
        id_rib: payments[i].rib.value,
        amount: payments[i].amount
      });
    } else {
      paymentsList.push({
        id_rib: '',
        amount: '0'
      });
    }
    i += 1;
  }
  return paymentsList;
};
