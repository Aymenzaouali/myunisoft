export const getDefaultValues = (fields = []) => {
  const defaultValue = 0;
  const defaultValues = {};
  fields.forEach((field) => {
    defaultValues[field] = defaultValue;
  });
  return defaultValues;
};
