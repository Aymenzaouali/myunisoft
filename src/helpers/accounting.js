import _get from 'lodash/get';
import moment from 'moment';
import {
  accountingTypes,
  buyingDiaryAccounts,
  sellingDiaryAccounts,
  odDiaryAccounts
} from 'assets/constants/data';

// Utils
export const parseFilters = (filters) => {
  if (filters.entry_id) {
    return [{ name: 'id_entry', value: filters.entry_id }];
  }

  return Object.keys(filters).reduce((acc, key) => {
    if (filters[key] === undefined) return acc;

    switch (key) {
    case 'diary':
      if (_get(filters, 'diary.code')) {
        acc.push({ name: 'diary', value: filters.diary.code });
      }
      break;

    case 'dateStart': {
      const date = moment(filters.dateStart);
      if (date.isValid()) {
        acc.push({ name: 'start_date', value: date.format('YYYY-MM-DD') });
      }

      break;
    }

    case 'dateEnd': {
      const date = moment(filters.dateEnd);
      if (date.isValid()) {
        acc.push({ name: 'end_date', value: date.format('YYYY-MM-DD') });
      }

      break;
    }

    case 'account':
      acc.push({ name: 'account', value: filters.account.account_number });
      break;

    case 'type':
    case 'label':
    case 'piece':
    case 'debit':
    case 'credit':
    case 'plus_less':
    case 'saisi_par':
      acc.push({ name: key, value: filters[key] });
      break;

    default:
    }

    return acc;
  }, []);
};

export const convertFormEntry = entry => Object.keys(entry).reduce((acc, key) => {
  switch (key) {
  case 'day':
  case 'month':
  case 'year':
    acc.entry_date = `${entry.year}-${entry.month}-${entry.day}`;
    break;

  case 'entry_list':
    acc.entry_list = entry.entry_list.map(e => Object.keys(e).reduce((acc, key) => {
      switch (key) {
      case 'credit':
      case 'debit':
        acc[key] = e[key] || null;
        break;

      case 'payment_type_id':
        acc.payment_type_id = e.payment_type_id === -1 ? null : e.payment_type_id;
        break;

      case 'payment_type':
        acc[key] = e[key];
        break;

      case 'account':
      case 'deadline':
      case 'flags':
      case 'label':
      case 'lettrage':
      case 'line_entry_id':
      case 'piece':
      case 'piece_2':
      case 'pj_list':
        acc[key] = e[key];
        break;

      default:
      }

      return acc;
    }, {}));
    break;

  case 'comment':
  case 'date_piece':
  case 'diary':
  case 'etablissement_id':
  case 'entry_id':
  case 'period_from':
  case 'period_to':
  case 'pj_list':
    acc[key] = entry[key];
    break;

  default:
  }

  return acc;
}, {});

export const addEntry = (entries, added) => {
  const debit = added.entry_list.reduce((acc, e) => acc + (e.debit || 0), 0);
  const credit = added.entry_list.reduce((acc, e) => acc + (e.credit || 0), 0);

  return {
    ...entries,
    entry_array: [added, ...entries.entry_array],
    debit_total: entries.debit_total + debit,
    credit_total: entries.credit_total + credit
  };
};
export const updateEntry = (entries = { entry_array: [] }, updated) => {
  let odebit = 0;
  const ndebit = updated.entry_list.reduce((acc, e) => acc + (e.debit || 0), 0);

  let ocredit = 0;
  const ncredit = updated.entry_list.reduce((acc, e) => acc + (e.credit || 0), 0);

  const result = {
    ...entries,
    entry_array: entries.entry_array.map((entry) => {
      if (entry.entry_id === updated.entry_id) {
        odebit = entry.entry_list.reduce((acc, e) => acc + (e.debit || 0), 0);
        ocredit = entry.entry_list.reduce((acc, e) => acc + (e.credit || 0), 0);

        return updated;
      }

      return entry;
    })
  };

  result.debit_total += ndebit - odebit;
  result.credit_total += ncredit - ocredit;

  return result;
};
export const deleteEntry = (entries, deleted) => {
  let debit = 0;
  let credit = 0;

  const result = {
    ...entries,
    entry_array: entries.entry_array.reduce((acc, entry) => {
      if (deleted.indexOf(entry.entry_id) !== -1) {
        debit += entry.entry_list.reduce((acc, e) => acc + (e.debit || 0), 0);
        credit += entry.entry_list.reduce((acc, e) => acc + (e.credit || 0), 0);
      } else {
        acc.push(entry);
      }

      return acc;
    }, [])
  };

  result.debit_total -= debit;
  result.credit_total -= credit;

  return result;
};

export const getAccountingLabelByValue = (accountingValue) => {
  const accountingType = accountingTypes.find(({ value }) => value === accountingValue);

  return accountingType.label;
};

const checkAccountStartFigures = (prefixArray, account) => {
  const checkList = prefixArray.map(p => account.startsWith(p));
  if (checkList.includes(true)) {
    return true;
  } return false;
};

export const diaryAndAccountCompatibility = (diaryCode, accountInput) => {
  if (diaryCode === 'ACH' && checkAccountStartFigures(buyingDiaryAccounts, accountInput)) {
    return true;
  } if (diaryCode === 'VTE' && checkAccountStartFigures(sellingDiaryAccounts, accountInput)) {
    return true;
  } if (diaryCode === 'OD' && !checkAccountStartFigures(odDiaryAccounts, accountInput)) {
    return true;
  } return false;
};

export const checkDateAndDiary = (diaryType) => {
  const notAuthorizedType = ['ACH', 'VTE', 'BQ'];
  if (notAuthorizedType.includes(diaryType)) { return true; } return false;
}; // Helper for checking whether it is allowed or not to enter a later day than the current day
// e.g. if the selected diary type is 'ACH', 'VTE' or 'BQ' it is not allowed to have a later day
