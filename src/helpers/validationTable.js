import _ from 'lodash';
import I18n from 'assets/I18n';

export const validationSocietyTable = (value) => {
  let hasError = false;
  const errors = value.reduce((acc, sub) => {
    let itemErrors;
    if (!_.get(sub, 'effective_date')) {
      hasError = true;
      itemErrors = { ...itemErrors, effective_date: I18n.t('common.errors.empty') };
    }
    if ((!_.get(sub, 'social_part.NP', 0) && !_.get(sub, 'social_part.US', 0) && !_.get(sub, 'social_part.PP', 0)) && !_.get(sub, 'end_date')) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_NP: I18n.t('common.errors.empty'),
        social_part_PP: I18n.t('common.errors.empty'),
        social_part_US: I18n.t('common.errors.empty')
      };
    }
    if (_.get(sub, 'social_part.NP', 0) < 0) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_NP: I18n.t('common.errors.invalid')
      };
    }
    if (_.get(sub, 'social_part.US', 0) < 0) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_US: I18n.t('common.errors.invalid')
      };
    }
    if (_.get(sub, 'social_part.PP', 0) < 0) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_PP: I18n.t('common.errors.invalid')
      };
    }
    return itemErrors ? { ...acc, [sub.society_id]: itemErrors } : acc;
  }, {});
  return { hasError, errors };
};

export const validationPersonTable = (value) => {
  let hasError = false;
  const errors = value.reduce((acc, sub) => {
    let itemErrors;
    if (!_.get(sub, 'effective_date')) {
      hasError = true;
      itemErrors = { ...itemErrors, effective_date: I18n.t('common.errors.empty') };
    }
    if (!_.get(sub, 'function_id')) {
      hasError = true;
      itemErrors = { ...itemErrors, function: I18n.t('common.errors.empty') };
    }
    if ((!_.get(sub, 'social_part.NP', 0) && !_.get(sub, 'social_part.US', 0) && !_.get(sub, 'social_part.PP', 0)) && !_.get(sub, 'end_date')) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_NP: I18n.t('common.errors.empty'),
        social_part_PP: I18n.t('common.errors.empty'),
        social_part_US: I18n.t('common.errors.empty')
      };
    }
    if (_.get(sub, 'social_part.NP', 0) < 0) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_NP: I18n.t('common.errors.invalid')
      };
    }
    if (_.get(sub, 'social_part.US', 0) < 0) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_US: I18n.t('common.errors.invalid')
      };
    }
    if (_.get(sub, 'social_part.PP', 0) < 0) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_PP: I18n.t('common.errors.invalid')
      };
    }
    return itemErrors ? { ...acc, [sub.physical_person_id]: itemErrors } : acc;
  }, {});
  return { hasError, errors };
};
export const validationSubsidaryTable = (value) => {
  let hasError = false;
  const errors = value.reduce((acc, sub) => {
    let itemErrors;
    if (!_.get(sub, 'effective_date')) {
      hasError = true;
      itemErrors = { ...itemErrors, effective_date: I18n.t('common.errors.empty') };
    }
    if (!_.get(sub, 'social_part.NP', 0) && !_.get(sub, 'social_part.US', 0) && !_.get(sub, 'social_part.PP', 0) && _.get(sub, 'capital', 0)) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_NP: I18n.t('common.errors.empty'),
        social_part_PP: I18n.t('common.errors.empty'),
        social_part_US: I18n.t('common.errors.empty')
      };
    }
    return itemErrors ? { ...acc, [sub.society_id]: itemErrors } : acc;
  }, {});
  return { hasError, errors };
};

export const validationSocietyPPTable = (value) => {
  let hasError = false;
  const errors = value.reduce((acc, sub) => {
    let itemErrors;
    if (!_.get(sub, 'effective_date')) {
      hasError = true;
      itemErrors = { ...itemErrors, effective_date: I18n.t('common.errors.empty') };
    }
    if (!_.get(sub, 'function_id')) {
      hasError = true;
      itemErrors = { ...itemErrors, function: I18n.t('common.errors.empty') };
    }
    if (!_.get(sub, 'social_part.NP', 0) && !_.get(sub, 'social_part.US', 0) && !_.get(sub, 'social_part.PP', 0)) {
      hasError = true;
      itemErrors = {
        ...itemErrors,
        social_part_NP: I18n.t('common.errors.empty'),
        social_part_PP: I18n.t('common.errors.empty'),
        social_part_US: I18n.t('common.errors.empty')
      };
    }
    return itemErrors ? { ...acc, [sub.physical_person_id]: itemErrors } : acc;
  }, {});
  return { hasError, errors };
};

export const validationEmployeePPTable = (value) => {
  let hasError = false;
  const errors = value.reduce((acc, sub) => {
    let itemErrors;
    if (!_.get(sub, 'effective_date')) {
      hasError = true;
      itemErrors = { ...itemErrors, effective_date: I18n.t('common.errors.empty') };
    }
    if (!_.get(sub, 'function_id')) {
      hasError = true;
      itemErrors = { ...itemErrors, function: I18n.t('common.errors.empty') };
    }
    return itemErrors ? { ...acc, [sub.physical_person_id]: itemErrors } : acc;
  }, {});
  return { hasError, errors };
};
