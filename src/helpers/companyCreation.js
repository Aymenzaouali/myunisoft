import _ from 'lodash';

export const parsePhysicalPerson = physicalPerson => ({
  id: physicalPerson.pers_physique_id,
  value: physicalPerson,
  label: `${physicalPerson.firstname} ${physicalPerson.name}`,
  name: physicalPerson.name,
  firstname: physicalPerson.firstname,
  function: _.get(physicalPerson, 'function', {}),
  signatory_function: _.get(physicalPerson, 'signatory_function', {}),
  start_date: _.get(physicalPerson, 'start_date', ''),
  end_date: _.get(physicalPerson, 'end_date', ''),
  social_part: physicalPerson.social_part ? {
    PP: _.get(physicalPerson, 'social_part.PP', null),
    US: _.get(physicalPerson, 'social_part.US', null),
    NP: _.get(physicalPerson, 'social_part.NP', null),
    percent: _.get(physicalPerson, 'social_part.percent', null)
  } : {}
});

export const parseSociety = society => ({
  id: society.society_id,
  value: society,
  label: society.name,
  name: society.name,
  address: _.get(society, 'address', ''),
  siret: _.get(society, 'siret', ''),
  capital: _.get(society, 'capital', null),
  function: _.get(society, 'function', {}),
  signatory_function: _.get(society, 'signatory_function', {}),
  start_date: _.get(society, 'start_date', ''),
  end_date: _.get(society, 'end_date', ''),
  social_part: society.social_part ? {
    PP: _.get(society.social_part, 'PP', null),
    US: _.get(society.social_part, 'US', null),
    NP: _.get(society.social_part, 'NP', null),
    percent: _.get(society.social_part, 'percent', null)
  } : {}
});
