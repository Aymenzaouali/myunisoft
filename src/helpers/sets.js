// operations with Sets
// https://stackoverflow.com/a/31129482/1948511
// https://exploringjs.com/impatient-js/ch_sets.html#missing-set-operations

// Create function to check if an element is in a specified set.
function _isIn(s) { return elt => s.has(elt); }

// Check s1 contains s2
export function setIncludes(s1, s2) { return [...s2].every(_isIn(s1)); }

export function setsEqual(a, b) { return a.size === b.size && setIncludes(a, b); }

export const setsUnite = (a, b) => new Set([...a, ...b]);

export const setsDifference = (a, b) => new Set([...a].filter(x => !b.has(x)));
