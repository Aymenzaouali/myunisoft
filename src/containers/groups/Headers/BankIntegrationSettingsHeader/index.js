import { connect } from 'react-redux';
import { reset } from 'redux-form';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import _ from 'lodash';
import { BankIntegrationSettingsHeader } from 'components/groups/Headers/';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    tableName,
    currentCompanyName: _.get(currentTab, 'label'),
    selectedStatements: _.get(state, `bankIntegrationSettings.selectedStatements[${state.navigation.id}]`, []),
    BIDataToCsv: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {}),
    interbankCodes: _.get(state, `bankIntegrationSettings.interbankCodes[${state.navigation.id}].interbankCodes_list`, [])
  };
};

const mapDispatchToProps = dispatch => ({
  initializeForm: (form, data) => dispatch(autoFillFormValues(form, data)),
  resetForm: () => dispatch(reset('BISettingsForm'))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedStatements, interbankCodes, currentCompanyName, tableName, BIDataToCsv
  } = stateProps;
  const { initializeForm } = dispatchProps;

  const initForModification = async () => {
    if (selectedStatements.slice(-1).pop()) {
      const {
        debit = null,
        credit = null,
        label_to_find = null,
        list_code_interbancaire = null,
        forced_label = null,
        account = null
      } = selectedStatements.slice(-1).pop();

      const account_id = {
        account_id: account.id,
        account_number: account.number,
        label: account.label
      };

      const selected_codes = _.map(list_code_interbancaire,
        code => ({ label: code.intitule, value: code.id }));
      const codes = selected_codes.length === interbankCodes.length ? [] : selected_codes;

      const data = {
        account_id,
        debit,
        credit,
        label_to_find,
        code_interbancaire: codes,
        forced_label
      };

      initializeForm('BISettingsForm', data);
    }
  };
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);
  const dataToExport = formatTableDataToCSVData(BIDataToCsv);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initForModification,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(BankIntegrationSettingsHeader);
