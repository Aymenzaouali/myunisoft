import { connect } from 'react-redux';
import _ from 'lodash';
import { ReportsAndFormsHeader } from 'components/groups/Headers/';
import { REPORT_AND_FORMS_TABLE, getTableName } from 'assets/constants/tableName';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, REPORT_AND_FORMS_TABLE);
  return {
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`, 0)
  };
};

export default connect(
  mapStateToProps,
  null
)(ReportsAndFormsHeader);
