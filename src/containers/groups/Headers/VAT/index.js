import { VATHeader } from 'components/groups/Headers';
import { connect } from 'react-redux';
import {
  getVatCycle as getVatCycleThunk,
  printOrDownloadCycle as printOrDownloadCycleThunk,
  sendVat as sendVatThunk,
  sendVatEdi as sendVatEdiThunk,
  autoFillVatCA3 as autoFillVatCA3Thunk,
  sendIsNothingness as sendIsNothingnessThunk
} from 'redux/cycle';
import tvaActions from 'redux/tva/actions';

import {
  openDialog as openDialogThunk,
  closeDialog as closeDialogThunk
} from 'redux/ediDialog';
import { reduxForm, getFormValues, change } from 'redux-form';
import moment from 'moment';
import _ from 'lodash';
import { tvaValidation } from 'helpers/pdfFormsValidation';
import pdfActions from 'redux/pdfForms/actions';
import I18n from 'assets/I18n';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const selectedFormId = _.get(state, `cycle.selectedFormId[${societyId}]`, 'tvaca3');
  const { isNothingness } = state.cycle;
  return {
    societyId,
    form: `${societyId}VATHeader`,
    headerValues: getFormValues(`${societyId}VATHeader`)(state),
    filterFromDashboard: _.get(state, 'dashboardWeb.filterVat'),
    selectedFormId,
    isNothingness,
    tvaFormValues: getFormValues(`${societyId}${selectedFormId}`)(state),
    dialog: _.get(state, `ediDialog.dialog_vat_edi_${societyId}`)
  };
};

const mapDispatchToProps = dispatch => ({
  changePeriod: (form, value) => dispatch(change(form, 'period', value)),
  getVatCycle: month => dispatch(getVatCycleThunk(month)),
  sendVat: month => dispatch(sendVatThunk(month)),
  sendVatEdi: (month, confirmed) => dispatch(sendVatEdiThunk(month, confirmed)),
  autoFillVatCA3: month => dispatch(autoFillVatCA3Thunk(month)),
  printOrDownloadCycle: (month, type) => dispatch(printOrDownloadCycleThunk(month, type)),
  setAllLinesErrors: (formId, errors) => dispatch(pdfActions.setAllLinesErrors(formId, errors)),
  sendIsNothingness: val => dispatch(sendIsNothingnessThunk(val)),
  changeIsNothing: val => dispatch(tvaActions.changeIsNothing(val)),
  openDialog: (screen, message, controls) => dispatch(openDialogThunk(screen, message, controls)),
  closeDialog: screen => dispatch(closeDialogThunk(screen))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    headerValues,
    societyId,
    filterFromDashboard,
    selectedFormId,
    tvaFormValues,
    dialog
  } = stateProps;
  const {
    changePeriod,
    sendVat,
    sendVatEdi,
    setAllLinesErrors,
    autoFillVatCA3,
    openDialog,
    closeDialog,
    getVatCycle,
    sendIsNothingness
  } = dispatchProps;

  const initialValues = !filterFromDashboard ? {
    period: moment().subtract(1, 'month').format('YYYY-MM')
  } : {
    period: filterFromDashboard.period
  };
  const period = _.get(headerValues, 'period', initialValues.period);

  const changeIsNothingness = (val) => {
    const month = headerValues.period;
    const cycle_code = 'TVA';
    const params = {
      neant: val,
      month,
      cycle_code
    };
    sendIsNothingness(params);
    if (!val) getVatCycle(period);
  };

  const form = `${societyId}VATHeader`;
  const isCurrentMonthOrMore = parseInt(moment().diff(moment(period, 'YYYY-MM'), 'month', true), 10) <= 0;

  const previousPeriod = () => {
    const previousMonth = moment(period, 'YYYY-MM').subtract(1, 'month').format('YYYY-MM');
    changePeriod(form, previousMonth);
    getVatCycle(previousMonth);
  };

  const nextPeriod = () => {
    if (!isCurrentMonthOrMore) {
      const nextMonth = moment(period, 'YYYY-MM').add(1, 'month').format('YYYY-MM');
      changePeriod(form, nextMonth);
      getVatCycle(nextMonth);
    }
  };

  const onClickAutofill = async () => {
    await autoFillVatCA3(period);
  };

  const buttonsLabel = {
    validate: I18n.t('ediDialogs.buttons.yes'),
    cancel: I18n.t('ediDialogs.buttons.no')
  };

  const onSubmit = async (method) => {
    try {
      setAllLinesErrors(`${societyId}${selectedFormId}`, {});
      tvaValidation(tvaFormValues, selectedFormId);
      switch (method) {
      case 'sendVat':
        await sendVat(period);
        break;
      case 'sendVatEdi':
        await sendVatEdi(period);
        break;
      default:
        break;
      }
    } catch (errors) {
      const error = _.get(errors, 'response.data.code', false);
      switch (error) {
      case 'ENVOIEDI1': {
        openDialog('vat_edi', I18n.t('ediDialogs.ENVOIEDI1.message'), buttonsLabel);
        break;
      }
      case 'ENVOIEDI2': {
        openDialog('vat_edi', I18n.t('ediDialogs.ENVOIEDI2.message'), buttonsLabel);
        break;
      }
      case 'ENVOIEDI3': {
        openDialog('vat_edi', I18n.t('ediDialogs.ENVOIEDI3.message'), buttonsLabel);
        break;
      }
      case 'ENVOIEDI4': {
        openDialog('vat_edi', I18n.t('ediDialogs.ENVOIEDI4.message'), buttonsLabel);
        break;
      }
      default:
        break;
      }
      setAllLinesErrors(`${societyId}${selectedFormId}`, errors);
    }
  };

  const onCloseDialog = async () => {
    closeDialog('vat_edi');
  };

  const onConfirm = async (confirmed) => {
    await sendVatEdi(period, confirmed);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initialValues,
    period,
    dialog,
    isCurrentMonthOrMore,
    previousPeriod,
    nextPeriod,
    changeIsNothingness,
    onSubmit,
    onClickAutofill,
    onCloseDialog,
    onConfirm
  };
};

const wrappedVATHeader = reduxForm({
  destroyOnUnmount: false
})(VATHeader);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrappedVATHeader);
