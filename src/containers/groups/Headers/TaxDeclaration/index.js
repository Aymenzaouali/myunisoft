import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm, getFormValues } from 'redux-form';
import moment from 'moment';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';
import { TaxDeclaration } from 'components/groups/Headers';
import {
  getExercises as getExercisesThunk
} from 'redux/tabs/exercises';
import {
  getTaxForms as getTaxFormsThunk,
  sendTaxForms as sendTaxFormsThunk,
  refreshAuto as refreshAutoThunk,
  sendTaxFormEdi as sendTaxFormEdiThunk,
  printOrDownloadTax as printOrDownloadTaxThunk,
  controlsTax as controlsTaxThunk
} from 'redux/tax';

const currentDate = moment().format('MM/DD/YYYY');
const defaultExercise = 'N';

const mapStateToProps = (state) => {
  const { exercises } = getCurrentTabState(state);

  return {
    societyId: state.navigation.id,
    form: `${state.navigation.id}TaxDeclarationHeader`,
    headerValues: getFormValues(`${state.navigation.id}TaxDeclarationHeader`)(state),
    exercises: exercises ? exercises.map((
      {
        label, start_date, end_date, id_exercice
      }
    ) => ({
      start_date,
      end_date,
      label,
      value: label,
      exerciseId: id_exercice
    })) : []
  };
};

const mapDispatchToProps = dispatch => ({
  getTaxForm: (
    exerciseId, code_sheet_group
  ) => dispatch(getTaxFormsThunk(exerciseId, code_sheet_group)),
  sendTaxForm: (
    exerciseId, code_sheet_group
  ) => dispatch(sendTaxFormsThunk(exerciseId, code_sheet_group)),
  refreshAuto: exerciseId => dispatch(refreshAutoThunk(exerciseId)),
  getExercises: () => dispatch(getExercisesThunk()),
  sendTaxFormEdi: () => dispatch(sendTaxFormEdiThunk()),
  printOrDownloadTax: (
    exerciseId, type, code_sheet_group
  ) => dispatch(printOrDownloadTaxThunk(exerciseId, type, code_sheet_group)),
  controlsTax: (exerciseId, type) => dispatch(controlsTaxThunk(exerciseId, type))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    headerValues,
    exercises
  } = stateProps;
  const {
    sendTaxForm,
    getTaxForm,
    sendTaxFormEdi,
    printOrDownloadTax,
    controlsTax,
    refreshAuto
  } = dispatchProps;
  const { code_sheet_group = '' } = ownProps;

  const dateStart = _.get(headerValues, 'dateStart', currentDate);
  const dateEnd = _.get(headerValues, 'dateEnd', currentDate);
  const exercise = _.get(headerValues, 'exercise', defaultExercise);

  const currentYearExercise = exercises.find(exerciseItem => exerciseItem.label === exercise) || {};
  const initialValues = {
    dateStart: currentYearExercise.start_date,
    dateEnd: currentYearExercise.end_date,
    exercise: currentYearExercise.label
  };
  const exerciseId = _.get(currentYearExercise, 'exerciseId', null);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    dateStart,
    dateEnd,
    exercise: currentYearExercise.label,
    getTaxForm: async () => {
      if (exerciseId) {
        await getTaxForm(exerciseId, code_sheet_group);
      }
    },
    sendTaxForm: async () => {
      if (exerciseId) {
        await sendTaxForm(exerciseId, code_sheet_group);
      }
    },
    refreshAuto: async () => {
      if (exerciseId) {
        await refreshAuto(exerciseId);
      }
    },
    sendTaxFormEdi,
    printOrDownloadTax: (type) => {
      if (exerciseId) {
        printOrDownloadTax(exerciseId, type, code_sheet_group);
      }
    },
    getControls: (type) => {
      if (exerciseId) {
        controlsTax(exerciseId, type);
      }
    },
    initialValues
  };
};

const wrappedHeader = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  wrappedHeader
)(TaxDeclaration);
