import { connect } from 'react-redux';
import _ from 'lodash';
import { SettingsWorksheetsHeader } from 'components/groups/Headers/';
import actions from 'redux/settingsWorksheets/actions';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    tableName,
    currentCompanyName: _.get(currentTab, 'label'),
    societyId: state.navigation.id,
    selectedWorksheet: _.get(state, `settingsWorksheets.selectedWorksheet[${state.navigation.id}]`, []),
    formatted_SettingWorksheets_table_data: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  selectWorksheet: settingSelectedLine => dispatch(actions.selectWorksheet(settingSelectedLine))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    tableName, currentCompanyName, formatted_SettingWorksheets_table_data
  } = stateProps;

  const baseFileName = createFormattedFileName(currentCompanyName, tableName);
  const transformedData = formatTableDataToCSVData(formatted_SettingWorksheets_table_data);

  const disabledExport = _.isEmpty(transformedData);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    csvData: transformedData,
    disabledExport,
    baseFileName
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(SettingsWorksheetsHeader);
