import { connect } from 'react-redux';
import _ from 'lodash';
import { BankLinkHeader } from 'components/groups/Headers/';
import { compose } from 'redux';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import {
  reduxForm, formValueSelector, getFormValues
} from 'redux-form';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import {
  getBankLink as getBankLinkThunk,
  printBankLink as printBankLinkThunk,
  putBankLink as putBankLinkThunk,
  updateBankBalance as updateBankBalanceThunk,
  addPjBankLink as addPJBankLinkThunk,
  postValidate,
  getDiaries as getDiariesThunk,
  getDate as getDateThunk
} from 'redux/bankLink';
import actions from 'redux/bankLink/actions';
import {
  bankLinkValidation
} from 'helpers/validation';
import { updateArgsSplit } from 'redux/tabs/split/actions';
import moment from 'moment';


const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const formName = `${state.navigation.id}banklinkFilter`;
  const newAccountingDiaryData = _.get(state, `form.${formName}.values.bank`, {});
  const newAccountingAccountData = _.get(state, `form.${formName}.values.bank.value.account`, {});
  const formValues = formValueSelector(formName)(state, 'day', 'month', 'year', 'bank', 'bank_balance', 'undotted_balance', 'accounting_balance', 'checkBox', 'gap');
  const month = moment().format('MMMM');
  const monthLabel = `${month.charAt(0).toLocaleUpperCase()}${month.substring(1)}`;
  const { value: diaryValue } = newAccountingDiaryData;
  const splitData = {
    diary: { ...diaryValue, label: diaryValue && `${diaryValue.code} - ${diaryValue.name}` },
    month: { label: monthLabel, value: moment().format('MM') },
    year: { label: moment().format('YYYY'), value: moment().format('YYYY') },
    entry_list: [
      {
        account: {
          ...newAccountingAccountData,
          account_id: newAccountingAccountData && newAccountingAccountData.id,
          account_number: newAccountingAccountData && newAccountingAccountData.number
        }
      }
    ]
  };

  return {
    form: formName,
    societyId: state.navigation.id,
    bankCodeList: state.bankLink.diaries,
    bankLinkData: _.get(state, `bankLink.bankLink[${state.navigation.id}]`),
    formValues,
    splitData,
    formValuesData: getFormValues(`${state.navigation.id}banklinkFilter`)(state),
    id_diligence: _.get(state, `bankLink.bankLink[${state.navigation.id}].diligence.id`),
    closed_diligence: _.get(state, `bankLink.bankLink[${state.navigation.id}].diligence.closed`),
    dottedLines: _.get(state, `bankLink.bankLink[${state.navigation.id}].dotted_lines`, []),
    undottedLines: _.get(state, `bankLink.bankLink[${state.navigation.id}].undotted_lines[${state.navigation.id}]`, []),
    selectedBills: _.get(state, `bankLink.selectedBill[${state.navigation.id}]`, []),
    undotted_balance: _.get(state, `bankLink.bankLink[${state.navigation.id}].undotted_balance`, 0),
    formattedTableData: _.get(state, `bankLink.bankLinkTableData[${state.navigation.id}].table_data`, []),
    pjList: _.get(state, 'bankLink.pj_list', []),
    date: _.get(state, 'bankLink.date'),
    isDateLoading: _.get(state, 'bankLink.isDateLoading'),
    currentCompanyName: _.get(currentTab, 'label'),
    isDateError: _.get(state, 'bankLink.isDateError'),
    initialValues: {
      dotted: '0'
    }
  };
};

const mapDispatchToProps = dispatch => ({
  getEntries: (date, id_diary, dotted) => dispatch(
    getBankLinkThunk(date, id_diary, dotted)
  ),
  printBankLink: (societyId, date, id_diary, bank_balance, dotted, mode) => dispatch(
    printBankLinkThunk(societyId, date, id_diary, bank_balance, dotted, mode)
  ),
  addPJBankLink: (file, date, id_diligence) => dispatch(
    addPJBankLinkThunk(file, date, id_diligence)
  ),
  putBankLink: (
    id_diary, balance_date, balance, month, year
  ) => dispatch(
    putBankLinkThunk(
      id_diary, balance_date, balance, month, year
    )
  ),
  updateBankBalance: (
    id_diary, balance_date, balance, month, year
  ) => dispatch(
    updateBankBalanceThunk(
      id_diary, balance_date, balance, month, year
    )
  ),
  fillValues: (societyId, data) => dispatch(autoFillFormValues(`${societyId}newAccounting`, data)),
  postValidate: (
    date, balance, id_diary
  ) => dispatch(
    postValidate(
      date, balance, id_diary
    )
  ),
  getDiaries: () => dispatch(getDiariesThunk()),
  getDate: () => dispatch(getDateThunk()),
  resetAllSelectedLines: societyId => dispatch(actions.resetAllBills(societyId)),
  updateArgsSplit: (args) => {
    dispatch(updateArgsSplit('bankLink', args));
  }
});


const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    splitData,
    bankCodeList,
    currentCompanyName,
    formattedTableData,
    formValues
  } = stateProps;

  const {
    fillValues,
    resetAllSelectedLines,
    updateBankBalance,
    updateArgsSplit
  } = dispatchProps;

  const bankCode = bankCodeList.map(bank => ({
    label: bank.code,
    value: bank
  }));

  const dottedMode = [
    {
      value: 0,
      label: 'Non pointées'
    },
    {
      value: 1,
      label: 'Pointées'
    },
    {
      value: 2,
      label: 'Toutes'
    }
  ];
  const baseFileName = createFormattedFileName(currentCompanyName, 'Rapprochement_bancaire');

  const resetAllBills = () => {
    resetAllSelectedLines(societyId);
  };

  const _updateBankBalance = () => {
    const id_diary = _.get(formValues, 'bank.value.diary_id');
    const balance_date = `${formValues.year}${formValues.month}${formValues.day}`;
    const balance = formValues.bank_balance.replace(/,/g, '.');
    const month = `${formValues.month}`;
    const year = moment(`${formValues.year}`).format('YY');

    updateBankBalance(id_diary, balance_date, balance, month, year);
  };

  const updateDiaryOnSplit = async (diary) => {
    const {
      account
    } = diary;

    const data = {
      diary: { ...diary, label: diary && `${diary.code} - ${diary.name}` },
      entry_list: [
        {
          account: {
            ...account,
            account_id: account.id,
            account_number: account.number
          }
        }
      ]
    };

    updateArgsSplit({ diary });
    await fillValues(societyId, data);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    bankCode,
    dottedMode,
    csvData: formatTableDataToCSVData(formattedTableData),
    baseFileName,
    fillValues: () => setTimeout(() => {
      fillValues(societyId, splitData);
      updateArgsSplit({ diary: splitData.diary });
    }, 0),
    resetAllBills,
    _updateBankBalance,
    updateDiaryOnSplit
  };
};


const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    destroyOnUnmount: false,
    enableReinitialize: true,
    validate: bankLinkValidation
  }),
);

export default enhance(BankLinkHeader);
