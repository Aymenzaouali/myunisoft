import { connect } from 'react-redux';
import StepperHeader from 'components/groups/Headers/Stepper';
import I18n from 'assets/I18n';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = state => ({
  activeTab: _.get(getCurrentTabState(state), 'companyCreation.selectedTab', 0),
  tabId: state.navigation.id
});
const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { openTab, goToStep } = ownProps;

  const steps = [
    {
      label: I18n.t('companyCreation.tabs.general'),
      disabled: false,
      onClick: () => openTab(0)
    },
    {
      label: I18n.t('companyCreation.tabs.exercices'),
      disabled: false,
      onClick: goToStep
    },
    {
      label: I18n.t('companyCreation.tabs.taxRecords'),
      disabled: false,
      onClick: goToStep
    },
    {
      label: I18n.t('companyCreation.tabs.associates'),
      disabled: false,
      onClick: goToStep
    },
    {
      label: I18n.t('companyCreation.tabs.subsidiaries'),
      disabled: false,
      onClick: goToStep
    },
    {
      label: I18n.t('companyCreation.tabs.clientUser'),
      disabled: false,
      onClick: goToStep
    },
    {
      label: I18n.t('companyCreation.tabs.establishements'),
      disabled: true,
      onClick: goToStep
    },
    {
      label: I18n.t('companyCreation.tabs.buildings'),
      disabled: true,
      onClick: goToStep
    },
    {
      label: I18n.t('companyCreation.tabs.evaluationElements'),
      disabled: true,
      onClick: goToStep
    },
    {
      label: I18n.t('companyCreation.tabs.billing'),
      disabled: true,
      onClick: goToStep
    }
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    steps
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(StepperHeader);
