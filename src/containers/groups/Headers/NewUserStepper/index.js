import { connect } from 'react-redux';
import StepperHeader from 'components/groups/Headers/Stepper';
import actions from 'redux/tabs/user/actions';
import I18n from 'assets/I18n';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = state => ({
  activeTab: _.get(getCurrentTabState(state), 'user.selectedTab', 0)
});

const mapDispatchToProps = dispatch => ({
  selectTab: payload => dispatch(actions.selectTab(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const steps = [
    {
      label: I18n.t('newUserForm.header.steps.identity')
    },
    {
      label: I18n.t('newUserForm.header.steps.right')
    }
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    steps
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(StepperHeader);
