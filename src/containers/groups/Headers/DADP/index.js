import { connect } from 'react-redux';

import { getCurrentTabState } from 'helpers/tabs';

import { setCategory } from 'redux/tabs/dadp/actions';

import { DADPHeader } from 'components/groups/Headers';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);

  return {
    category: dadp.category
  };
};

const mapDispatchToProps = dispatch => ({
  setCategory: category => dispatch(setCategory(category))
});

export default connect(mapStateToProps, mapDispatchToProps)(DADPHeader);
