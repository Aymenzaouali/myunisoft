import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm, getFormValues } from 'redux-form';
import moment from 'moment';
import { get as _get } from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';
import { Bundle } from 'components/groups/Headers';
import {
  getExercises as getExercisesThunk
} from 'redux/tabs/exercises';

import {
  getDeadline as getDeadlineThunk,
  getBundleForms as getBundleFormsThunk,
  sendBundleForms as sendBundleFormsThunk,
  sendBundleFormsEdi as sendBundleFormsEdiThunk,
  printOrDownloadBundle as printOrDownloadBundleThunk
} from 'redux/bundle';
import {
  getBundleStatements as getBundleStatementsThunk,
  sendBundleStatements as sendBundleStatementsThunk
} from 'redux/statements';

import {
  getTaxForms as getTaxFormsThunk,
  refreshAuto as refreshAutoThunk
} from 'redux/tax';

const currentDate = moment().format('MM/DD/YYYY');
const defaultExercise = 'N';

const mapStateToProps = (state) => {
  const { exercises } = getCurrentTabState(state);
  const societyId = state.navigation.id;
  const deadlines = _get(state, `bundle.deadline.${societyId}.list`, []);
  const selectedDeadline = _get(state, `bundle.selectedDeadline.${societyId}.deadline`, null);
  return {
    societyId,
    form: `${societyId}BundleHeader`,
    headerValues: getFormValues(`${societyId}BundleHeader`)(state),
    exercises: exercises ? exercises.map((
      {
        label, start_date, end_date, id_exercice
      }
    ) => ({
      start_date,
      end_date,
      label,
      value: label,
      exercice_id: id_exercice
    })) : [],
    deadlines: deadlines ? deadlines.map(deadline => ({
      label: moment(deadline).format('DD-MM-YYYY'),
      value: deadline
    })) : [],
    selectedDeadline
  };
};

const mapDispatchToProps = dispatch => ({
  getTaxForms: exercice_id => dispatch(getTaxFormsThunk(exercice_id)),
  getDeadline: args => dispatch(getDeadlineThunk(args)),
  getBundleForms: args => dispatch(getBundleFormsThunk(args)),
  sendBundleForms: args => dispatch(sendBundleFormsThunk(args)),
  sendBundleFormsEdi: args => dispatch(sendBundleFormsEdiThunk(args)),
  printOrDownloadBundle: (args, type) => dispatch(printOrDownloadBundleThunk(args, type)),
  getExercises: () => dispatch(getExercisesThunk()),
  getBundleStatements: (
    code_sheet_group, date_declare
  ) => dispatch(getBundleStatementsThunk(code_sheet_group, date_declare)),
  sendBundleStatements: (
    code_sheet_group, date_declare, isFull
  ) => dispatch(sendBundleStatementsThunk(code_sheet_group, date_declare, isFull)),
  refreshAutoIS: (exerciseId, date, code_sheet_group) => dispatch(
    refreshAutoThunk(exerciseId, date, code_sheet_group)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { code_sheet_group } = ownProps;
  const {
    headerValues,
    exercises,
    deadlines,
    selectedDeadline
  } = stateProps;
  const {
    getTaxForms,
    getDeadline,
    getBundleForms,
    sendBundleForms,
    sendBundleFormsEdi,
    printOrDownloadBundle,
    getBundleStatements,
    sendBundleStatements,
    refreshAutoIS
  } = dispatchProps;
  const {
    isTaxForms
  } = ownProps;

  const defaultDeadline = _get(deadlines, '[0].value', '');
  const dateStart = _get(headerValues, 'dateStart', currentDate);
  const dateEnd = _get(headerValues, 'dateEnd', currentDate);
  const exercise = _get(headerValues, 'exercise', defaultExercise);
  const deadline = _get(headerValues, 'deadline', defaultDeadline);

  const currentYearExercise = exercises.find(exerciseItem => exerciseItem.label === exercise) || {};
  const currentDeadline = deadlines.find(deadlineItem => deadlineItem.value === deadline) || {};

  const initialValues = {
    dateStart: currentYearExercise.start_date,
    dateEnd: currentYearExercise.end_date,
    exercise: currentYearExercise.label,
    deadline: currentDeadline.value
  };
  const exercice_id = _get(currentYearExercise, 'exercice_id', null);
  const date_declare = _get(currentDeadline, 'value', null);

  const requestArgs = {
    exercice_id,
    code_sheet_group,
    date_declare
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    dateStart,
    dateEnd,
    exercise: currentYearExercise.label,
    deadline: currentDeadline.value,
    getTaxForms: () => {
      if (isTaxForms && exercice_id) {
        getTaxForms(exercice_id);
      }
    },
    getDeadline: async () => {
      if (exercice_id) {
        await getDeadline(requestArgs);
      }
    },
    getBundleForms: async () => {
      if (exercice_id && selectedDeadline !== date_declare) {
        await getBundleForms(requestArgs);
      }
    },
    sendBundleForms: () => {
      if (exercice_id && date_declare) {
        sendBundleForms(requestArgs);
      }
    },
    sendBundleFormsEdi: () => {
      if (exercice_id && date_declare) {
        sendBundleFormsEdi(requestArgs);
      }
    },
    printOrDownloadBundle: (type) => {
      if (exercice_id && date_declare) {
        printOrDownloadBundle(requestArgs, type);
      }
    },
    getBundleStatements: () => {
      if (exercice_id && date_declare) {
        getBundleStatements(code_sheet_group, date_declare);
      }
    },
    sendBundleStatements: (isFull) => {
      if (code_sheet_group && date_declare) {
        sendBundleStatements(code_sheet_group, date_declare, isFull);
      }
    },
    refreshAutoIS: async () => {
      if (exercice_id) {
        await refreshAutoIS(exercice_id, date_declare, code_sheet_group);
      }
    },
    initialValues
  };
};

const wrappedHeader = reduxForm({
  destroyOnUnmount: true,
  enableReinitialize: true
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  wrappedHeader
)(Bundle);
