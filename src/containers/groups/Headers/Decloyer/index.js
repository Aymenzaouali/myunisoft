import { DecloyerHeader } from 'components/groups/Headers';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

const mapStateToProps = state => ({
  societyId: state.navigation.id
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId
  } = stateProps;


  const steps = [
    { id: 0, label: 'Local 1' },
    { id: 1, label: 'Local 2' },
    { id: 2, label: 'Local 3' }
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    societyId,
    steps
  };
};

const wrappedVATHeader = reduxForm({
  destroyOnUnmount: false
})(DecloyerHeader);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrappedVATHeader);
