import { connect } from 'react-redux';
import { DocumentManagementHeader } from 'components/groups/Headers/';
import { formValueSelector, reduxForm, reset } from 'redux-form';
import DocManService from 'redux/docManagement';
import { gedSearchValidation } from 'helpers/validation';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import _ from 'lodash';
import { compose } from 'redux';
import { splitWrap } from 'context/SplitContext';
import { openPopupSplit } from 'redux/tabs/split/actions';

const formName = 'documentManagementHeaderSearch';
const selector = formValueSelector(formName);

const mapStateToProps = state => ({
  activeTabId: state.docManagement.activeTabId,
  prevActiveTabId: state.docManagement.prevActiveTabId,
  searchSettings: state.docManagement.searchSettings,
  searchData: state.docManagement.searchData,
  activeFolder: state.docManagement.activeFolder,
  account: formValueSelector(`${state.navigation.id}consultingFilter`)(state, 'Account'),
  allExercices: _.get(state, `accountingWeb.exercice.${state.navigation.id}.exercice`),
  society_id: state.navigation.id,
  exercice: selector(state, 'exercice'),
  debit: selector(state, 'debit'),
  format_document: selector(state, 'format_document')
});

const mapDispatchToProps = dispatch => ({
  openPopupSplit: masterId => dispatch(openPopupSplit(masterId)),
  setActiveTabId: tabId => dispatch(
    DocManService.setActiveTab(tabId)
  ),
  getDocuments: leaf => dispatch(
    DocManService.getDocuments(leaf)
  ),
  saveDocumentsSearchValues: () => dispatch(
    DocManService.saveDocumentsSearchValues()
  ),
  updateTree: (tabId, searchParams) => dispatch(
    DocManService.getTree(tabId, searchParams)
  ),
  setDocumentsMode: isSearch => dispatch(
    DocManService.setDocumentsMode(isSearch)
  ),
  resetDocumentsSearchValues: () => dispatch(
    reset('documentManagementHeaderSearch')
  ),
  initializeDate: (dateStart, dateEnd, exercice, format_document) => {
    dispatch(autoFillFormValues('documentManagementHeaderSearch', {
      dateStart, dateEnd, exercice, format_document
    }));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    setDocumentsMode,
    updateTree,
    getDocuments
  } = dispatchProps;
  const {
    searchData,
    activeTabId,
    society_id,
    activeFolder,
    allExercices = [],
    exercice
  } = stateProps;
  const onSubmit = async () => {
    try {
      await setDocumentsMode(true);
      await updateTree(activeTabId, {});
      await getDocuments(activeFolder);
    } catch (error) {
      console.log(error);
    }
  };
  const exercice_N = exercice && exercice.length > 0 ? exercice : allExercices.filter(ex => ex.label === 'N').map(i => Object.assign(i, { value: i.label }));
  const start_date = _.get(exercice_N, '[0].start_date');
  const end_date = _.get(exercice_N, '[0].end_date');

  const savedSearch = _.get(searchData[society_id], `[${activeTabId}]searchValues`);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initialValues: {
      search_type: 'exercise',
      ...savedSearch
    },
    start_date,
    end_date,
    exercice_N,
    onSubmit
  };
};

const WrappedDocumentManagementHeader = reduxForm({
  form: formName,
  destroyOnUnmount: false,
  enableReinitialize: false,
  validate: gedSearchValidation
})(DocumentManagementHeader);


const enhance = compose(
  splitWrap,
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )
);

export default enhance(WrappedDocumentManagementHeader);
