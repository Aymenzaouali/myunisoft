import { connect } from 'react-redux';
import ProgressionHeader from 'components/groups/Headers/Progression';
import {
  getProgressionSociety as getProgressionSocietyThunk,
  getCharts as getDashboardThunk
} from 'redux/dashboard';
import actions from 'redux/dashboard/actions';
import _ from 'lodash';

const mapStateToProps = state => ({
  exercisesList: _.get(state, `dashboardWeb.exercisesList.${state.navigation.id}`, []),
  selectedExercise: _.get(state, `dashboardWeb.selectedExercise.${state.navigation.id}`, {}),
  currentExercise: _.get(state, `dashboardWeb.currentExercise.${state.navigation.id}`),
  societyId: state.navigation.id
});

const mapDispatchToProps = dispatch => ({
  getProgressionSociety: () => dispatch(getProgressionSocietyThunk()),
  getDashboard: societyId => dispatch(getDashboardThunk(societyId)),
  changeSelectedExercise: (exercise, societyId) => {
    dispatch(actions.changeSelectedExercise(exercise, societyId));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ProgressionHeader);
