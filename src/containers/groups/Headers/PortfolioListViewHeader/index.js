import { connect } from 'react-redux';
import _ from 'lodash';
import { PortfolioListViewHeader } from 'components/groups/Headers/';
import PortfolioListSettingsService from 'redux/portfolioListSettings';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    selectedWallets: _.get(state, 'portfolioListSettings.selectedWallets', []),
    walletSelectedLine: state.portfolioListSettings.walletSelectedLine,
    currentCompanyName: _.get(currentTab, 'label'),
    tableName,
    formattedTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  deleteWallets: () => dispatch(
    PortfolioListSettingsService.deleteWallets()
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentCompanyName, formattedTableData, tableName } = stateProps;
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);

  const dataToExport = formatTableDataToCSVData(formattedTableData);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(PortfolioListViewHeader);
