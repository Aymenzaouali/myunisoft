import { Cycle } from 'components/groups/Headers';
import { connect } from 'react-redux';
import { compose } from 'redux';

import {
  getDeadline as getDeadlineThunk,
  getCycleForms as getCycleFormsThunk,
  sendCycleForms as sendCycleFormsThunk,
  sendCycleFormsEdi as sendCycleFormsEdiThunk,
  printOrDownloadCycleForms as printOrDownloadCycleFormsThunk,
  sendIsNothingness as sendIsNothingnessThunk
} from 'redux/cycle';
import { reduxForm, getFormValues, change } from 'redux-form';
import moment from 'moment';
import { get as _get } from 'lodash';
import { tvaValidation } from 'helpers/pdfFormsValidation';
import pdfActions from 'redux/pdfForms/actions';
import {
  getExercises as getExercisesThunk
} from 'redux/tabs/exercises';
import { getCurrentTabState } from 'helpers/tabs';

const currentDate = moment().format('MM/DD/YYYY');
const defaultExercise = 'N';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const { exercises } = getCurrentTabState(state);
  const caForms = {
    ca12Annual_3517SCA12: getFormValues(`${societyId}_ca12Annual_3517SCA12`)(state)
  };
  const deadlines = _get(state, `cycle.deadline.${societyId}.list`, []);
  const isNothingness = _get(state, 'cycle.isNothingness', 0);
  return {
    societyId,
    caForms,
    isNothingness,
    form: `${societyId}cycleHeader`,
    headerValues: getFormValues(`${societyId}cycleHeader`)(state),
    filterFromDashboard: _get(state, 'dashboardWeb.filterVat'),
    exercises: exercises ? exercises.map((
      {
        label, start_date, end_date, id_exercice
      }
    ) => ({
      start_date,
      end_date,
      label,
      value: label,
      exerciseId: id_exercice
    })) : [],
    deadlines: deadlines ? deadlines.map(deadline => ({
      label: deadline,
      value: deadline
    })) : []
  };
};

const mapDispatchToProps = dispatch => ({
  changePeriod: (form, value) => dispatch(change(form, 'period', value)),
  getDeadline: (
    exerciseId, cycle_code
  ) => dispatch(getDeadlineThunk(exerciseId, cycle_code)),
  getCycleForms: (
    exerciseId, cycle_code, deadline, code_sheet_group
  ) => dispatch(getCycleFormsThunk(exerciseId, cycle_code, deadline, code_sheet_group)),
  sendCycleForms: (
    exerciseId, cycle_code, deadline, code_sheet_group
  ) => dispatch(sendCycleFormsThunk(exerciseId, cycle_code, deadline, code_sheet_group)),
  sendCycleFormsEdi: (
    exerciseId, cycle_code, deadline, code_sheet_group
  ) => dispatch(sendCycleFormsEdiThunk(exerciseId, cycle_code, deadline, code_sheet_group)),
  printOrDownloadCycleForms: (
    month, type, cycle_code
  ) => dispatch(printOrDownloadCycleFormsThunk(month, type, cycle_code)),
  getExercises: societyId => dispatch(getExercisesThunk(societyId)),
  setAllLinesErrors: (
    formId, errors
  ) => dispatch(pdfActions.setAllLinesErrors(formId, errors)),
  sendIsNothingness: val => dispatch(sendIsNothingnessThunk(val))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    headerValues,
    societyId,
    exercises,
    deadlines,
    caForms
  } = stateProps;
  const {
    getExercises,
    getDeadline,
    getCycleForms,
    sendCycleForms,
    sendCycleFormsEdi,
    setAllLinesErrors,
    printOrDownloadCycleForms,
    sendIsNothingness
  } = dispatchProps;
  const {
    cycle_code,
    code_sheet_group,
    cycleForm
  } = ownProps;

  const deadultDeadline = _get(deadlines, '[0].value', '');
  const dateStart = _get(headerValues, 'dateStart', currentDate);
  const dateEnd = _get(headerValues, 'dateEnd', currentDate);
  const exercise = _get(headerValues, 'exercise', defaultExercise);
  const deadline = _get(headerValues, 'deadline', deadultDeadline);

  const currentYearExercise = exercises.find(exerciseItem => exerciseItem.label === exercise) || {};
  const currentDeadline = deadlines.find(deadlineItem => deadlineItem.label === deadline) || {};

  const initialValues = {
    dateStart: currentYearExercise.start_date,
    dateEnd: currentYearExercise.end_date,
    exercise: currentYearExercise.label,
    deadline: currentDeadline.label
  };
  const exerciseId = _get(currentYearExercise, 'exerciseId', null);
  const deadlineValue = _get(currentDeadline, 'value', null);

  const period = _get(headerValues, 'period', moment().format('YYYY-MM'));

  const validateForm = async () => {
    try {
      tvaValidation(caForms[cycleForm], cycleForm);
    } catch (errors) {
      setAllLinesErrors(`${societyId}${cycleForm}`, errors);
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initialValues,
    period,
    dateStart,
    dateEnd,
    exercise: currentYearExercise.label,
    deadline: currentDeadline.label,
    exerciseId,
    change,
    validateForm,
    getExercises: () => {
      if (societyId) {
        getExercises(societyId);
      }
    },
    getDeadline: () => {
      if (exerciseId) {
        getDeadline(exerciseId, cycle_code);
      }
    },
    getCycleForms: () => {
      if (exerciseId && deadlineValue) {
        getCycleForms(exerciseId, cycle_code, deadlineValue, code_sheet_group);
      }
    },
    sendCycleForms: () => {
      if (exerciseId && deadlineValue) {
        sendCycleForms(exerciseId, cycle_code, deadlineValue, code_sheet_group);
      }
    },
    sendCycleFormsEdi: () => {
      if (exerciseId && deadlineValue) {
        sendCycleFormsEdi(exerciseId, cycle_code, deadlineValue, code_sheet_group);
      }
    },
    printOrDownloadCycleForms: (type) => {
      if (exerciseId && deadlineValue) {
        printOrDownloadCycleForms(deadlineValue, type, cycle_code);
      }
    },
    changeIsNothingness: (check) => {
      const neant = check ? 1 : 0;
      const params = {
        neant,
        month: deadlineValue,
        cycle_code
      };
      sendIsNothingness(params);
      if (!check && exerciseId && deadlineValue) {
        getCycleForms(exerciseId, cycle_code, deadlineValue, code_sheet_group);
      }
    }
  };
};

const wrappedCAHeader = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  wrappedCAHeader
)(Cycle);
