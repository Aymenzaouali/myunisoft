import _ from 'lodash';
import { connect } from 'react-redux';
import { MailParagraphTableHeader } from 'components/groups/Headers/';
import { PARAGRAPH_TABLE, getTableName } from 'assets/constants/tableName';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import actions from 'redux/settingStandardMail/actions';
import I18n from 'i18next';

const mapStateToProps = (state) => {
  const { id: society_id, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === society_id));
  const tableName = getTableName(society_id, PARAGRAPH_TABLE);
  const tableExportName = `${I18n.t('settingStandardMail.title')}_bottom`;
  return {
    currentCompanyName: _.get(currentTab, 'label'),
    tableExportName,
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    selectedTypes: _.get(state, `tables.${tableName}.data`, []),
    exportTableData: _.get(state, `tables.transformedTableData[${tableExportName}][${society_id}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  setParagraphsVisible: status => dispatch(actions.setParagraphsVisible(status))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    currentCompanyName,
    tableExportName,
    exportTableData,
    selectedTypes,
    selectedRows,
    selectedRowsCount
  } = stateProps;
  const dataToExport = formatTableDataToCSVData(exportTableData);
  const baseFileName = createFormattedFileName(currentCompanyName, tableExportName);

  /** can't delete when paragraph type === global and row is not selected */
  const disableDelete = selectedTypes
    .filter((type, index) => selectedRows[index + 1])
    .some(item => item.type === 'global') || !selectedRowsCount;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disableDelete,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(MailParagraphTableHeader);
