import VATHeader from './VAT';
import DADPHeader from './DADP';
import DiaryHeader from './Diary';
import PlansHeader from './Plans';
import PlanDetailsHeader from './PlanDetails';
import DecloyerHeader from './Decloyer';
import BankLinkHeaderContainer from './BankLink';
import DocumentManagementHeaderContainer from './DocumentManagement';
import SettingsWorksheetsHeaderContainer from './SettingsWorksheetsHeader';
import PortfolioListViewHeader from './PortfolioListViewHeader';
import TaxDeclarationHeader from './TaxDeclaration';
import BankIntegrationSettingsHeader from './BankIntegrationSettingsHeader';
import ReportsAndFormsHeader from './ReportsAndFormsHeader';
import ProgressionHeader from './ProgressionHeader';
import SettingStandardMailHeader from './SettingStandardMailHeader';
import MailParagraphTableHeader from './MailParagraphTableHeader';
import AccountingFirmSettingsHeader from './AccountingFirmSettingsHeader';
import BundleHeader from './Bundle';
import WorkingPageHeader from './WorkingPageHeader';
import FixedAssetsHeader from './FixedAssetsHeader';
import CycleHeader from './Cycle';
import RentDeclarations from './RentDeclarations';

export {
  VATHeader,
  DADPHeader,
  DecloyerHeader,
  BankLinkHeaderContainer,
  DocumentManagementHeaderContainer,
  DiaryHeader,
  PlansHeader,
  PlanDetailsHeader,
  PortfolioListViewHeader,
  TaxDeclarationHeader,
  SettingsWorksheetsHeaderContainer,
  BankIntegrationSettingsHeader,
  ReportsAndFormsHeader,
  ProgressionHeader,
  SettingStandardMailHeader,
  MailParagraphTableHeader,
  AccountingFirmSettingsHeader,
  BundleHeader,
  WorkingPageHeader,
  FixedAssetsHeader,
  CycleHeader,
  RentDeclarations
};
