import { connect } from 'react-redux';
import { AccountingFirmSettingsHeader } from 'components/groups/Headers';
import AccountingFirmSettingsService from 'redux/accountingFirmSettings';
import _ from 'lodash';
import { ACCOUNTING_FIRM_TOP_MAIN_TABLE, getTableName } from 'assets/constants/tableName';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, ACCOUNTING_FIRM_TOP_MAIN_TABLE);
  return {
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`)
  };
};

const mapDispatchToProps = dispatch => ({
  setCreateNewStatus: isCreate => dispatch(
    AccountingFirmSettingsService.setCreateNewStatus(isCreate)
  )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountingFirmSettingsHeader);
