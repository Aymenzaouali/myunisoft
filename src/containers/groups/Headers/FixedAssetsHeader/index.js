import FixedAssetsHeader from 'components/groups/Headers/FixedAssets';
import { connect } from 'react-redux';
import _ from 'lodash';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    currentCompanyName: _.get(currentTab, 'label'),
    tableName,
    formattedTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};


const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentCompanyName, formattedTableData, tableName } = stateProps;
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);

  const dataToExport = formatTableDataToCSVData(formattedTableData);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disabledExport: _.isEmpty(formattedTableData),
    csvData: dataToExport,
    baseFileName
  };
};

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(FixedAssetsHeader);
