import WorkingPageHeader from 'components/groups/Headers/WorkingPage';
import _ from 'lodash';
import { connect } from 'react-redux';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import { getTableName } from 'assets/constants/tableName';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const pageName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  const tableName = getTableName(societyId, pageName);
  return {
    currentCompanyName: _.get(currentTab, 'label'),
    tableName,
    formattedTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  initializeForm: (form, data) => dispatch(autoFillFormValues(form, data))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentCompanyName, formattedTableData, tableName } = stateProps;
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);

  const dataToExport = formatTableDataToCSVData(formattedTableData);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disabledExport: _.isEmpty(formattedTableData),
    csvData: dataToExport,
    baseFileName
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(WorkingPageHeader);
