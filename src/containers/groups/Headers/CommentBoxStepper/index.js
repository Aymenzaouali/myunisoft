import { connect } from 'react-redux';
import StepperHeader from 'components/groups/Headers/Stepper';
import I18n from 'assets/I18n';

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const steps = [
    {
      label: I18n.t('commentBox.steps.libre'),
      disabled: false,
      onClick: () => ownProps.onClick(0)
    },
    {
      label: I18n.t('commentBox.steps.normal'),
      disabled: true,
      onClick: () => {}
    }
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    steps,
    title: I18n.t('commentBox.title')
  };
};

export default connect(undefined, undefined, mergeProps)(StepperHeader);
