import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm, getFormValues } from 'redux-form';
import moment from 'moment';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';
import { RentDeclarations } from 'components/groups/Headers';
import { getExercises as getExercisesThunk } from 'redux/tabs/exercises';
import {
  getLandTenure as getLandTenureThunk,
  getRentDeclarationsForms as getRentDeclarationsFormsThunk,
  sendRentDeclarationsForms as sendRentDeclarationsFormsThunk,
  sendEdiRentDeclarationsForms as sendEdiRentDeclarationsFormsThunk,
  printOrDownloadRentDeclarations as printOrDownloadRentDeclarationsThunk
} from 'redux/rentDeclarations';

const currentDate = moment().format('MM/DD/YYYY');
const defaultExercise = 'N';

const mapStateToProps = (state) => {
  const { exercises } = getCurrentTabState(state);

  return {
    societyId: state.navigation.id,
    form: `${state.navigation.id}RentDeclarationsHeader`,
    headerValues: getFormValues(`${state.navigation.id}RentDeclarationsHeader`)(state),
    exercises: exercises ? exercises.map((
      {
        label, start_date, end_date, id_exercice
      }
    ) => ({
      start_date,
      end_date,
      label,
      value: label,
      exerciseId: id_exercice
    })) : []
  };
};

const mapDispatchToProps = dispatch => ({
  getExercises: () => dispatch(getExercisesThunk()),
  getLandTenure: () => dispatch(getLandTenureThunk()),
  getRentDeclarationsForms: exerciseId => dispatch(getRentDeclarationsFormsThunk(exerciseId)),
  sendRentDeclarationsForms: exerciseId => dispatch(sendRentDeclarationsFormsThunk(exerciseId)),
  sendEdiRentDeclarationsForms:
      exerciseId => dispatch(sendEdiRentDeclarationsFormsThunk(exerciseId)),
  printOrDownloadRentDeclarations: (
    exerciseId, type
  ) => dispatch(printOrDownloadRentDeclarationsThunk(exerciseId, type))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    headerValues,
    exercises
  } = stateProps;
  const {
    getLandTenure,
    getRentDeclarationsForms,
    sendRentDeclarationsForms,
    sendEdiRentDeclarationsForms,
    printOrDownloadRentDeclarations
  } = dispatchProps;

  const dateStart = _.get(headerValues, 'dateStart', currentDate);
  const dateEnd = _.get(headerValues, 'dateEnd', currentDate);
  const exercise = _.get(headerValues, 'exercise', defaultExercise);

  const currentYearExercise = exercises.find(exerciseItem => exerciseItem.label === exercise) || {};
  const initialValues = {
    dateStart: currentYearExercise.start_date,
    dateEnd: currentYearExercise.end_date,
    exercise: currentYearExercise.label
  };
  const exerciseId = _.get(currentYearExercise, 'exerciseId', null);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    dateStart,
    dateEnd,
    exercise: currentYearExercise.label,
    getRentDeclarationsForms: async () => {
      if (exerciseId) {
        await getLandTenure();
        await getRentDeclarationsForms(exerciseId);
      }
    },
    sendRentDeclarationsForms: async () => {
      if (exerciseId) {
        await sendRentDeclarationsForms(exerciseId);
      }
    },
    sendEdiRentDeclarationsForms: async () => {
      if (exerciseId) {
        await sendEdiRentDeclarationsForms(exerciseId);
      }
    },
    printOrDownloadRentDeclarations: (type) => {
      if (exerciseId) {
        printOrDownloadRentDeclarations(exerciseId, type);
      }
    },
    initialValues
  };
};

const wrappedHeader = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  wrappedHeader
)(RentDeclarations);
