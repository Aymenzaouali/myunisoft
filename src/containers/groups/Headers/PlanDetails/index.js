import { connect } from 'react-redux';
import { PlanDetailsHeader } from 'components/groups/Headers';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(PlanDetailsHeader);
