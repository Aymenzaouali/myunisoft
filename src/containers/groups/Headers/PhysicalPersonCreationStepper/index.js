import { connect } from 'react-redux';
import StepperHeader from 'components/groups/Headers/Stepper';
import I18n from 'assets/I18n';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const { physicalPersonCreation } = getCurrentTabState(state);

  return {
    newlyCreatedPersonId: _.get(physicalPersonCreation, 'newlyCreatedPersonId'),
    selectedPhysicalPerson: _.get(physicalPersonCreation, 'selected_physical_person'),
    activeTab: _.get(getCurrentTabState(state), 'physicalPersonCreation.selectedTab', 0)
  };
};

const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { openTab, isUser } = ownProps;
  const { newlyCreatedPersonId, selectedPhysicalPerson } = stateProps;

  const userIsCreated = newlyCreatedPersonId !== null || selectedPhysicalPerson !== null;

  const steps = [
    {
      label: I18n.t('physicalPersonCreation.tabs.general'),
      disabled: false,
      onClick: () => openTab(0)
    },
    {
      label: I18n.t('physicalPersonCreation.tabs.family'),
      disabled: !userIsCreated
    },
    {
      label: I18n.t('physicalPersonCreation.tabs.company'),
      disabled: !userIsCreated
    },
    {
      label: I18n.t('physicalPersonCreation.tabs.employee'),
      disabled: !userIsCreated
    },
    {
      label: I18n.t('physicalPersonCreation.tabs.document'),
      disabled: !userIsCreated
    },
    isUser && ({
      label: I18n.t('physicalPersonCreation.tabs.user_access'),
      disabled: false
    })
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    steps
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(StepperHeader);
