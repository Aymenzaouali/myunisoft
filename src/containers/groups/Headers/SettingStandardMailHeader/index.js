import { connect } from 'react-redux';
import _ from 'lodash';
import { SettingStandardMailHeader } from 'components/groups/Headers/';
import { PARAGRAPHS_TYPES_TABLE, getTableName } from 'assets/constants/tableName';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import I18n from 'i18next';

const mapStateToProps = (state) => {
  const { id: society_id, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === society_id));
  const tableName = getTableName(society_id, PARAGRAPHS_TYPES_TABLE);
  const tableExportName = I18n.t('settingStandardMail.title');
  return {
    tableExportName,
    currentCompanyName: _.get(currentTab, 'label'),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedTypes: _.get(state, `tables.${tableName}.data`, []),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    exportTableData: _.get(state, `tables.transformedTableData[${tableExportName}][${society_id}].params`, {})
  };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedRows,
    selectedTypes,
    currentCompanyName,
    tableExportName,
    exportTableData,
    selectedRowsCount
  } = stateProps;
  const isDeletable = selectedTypes
    .filter((type, index) => selectedRows[index + 1])
    .some(item => item.blocked) || !selectedRowsCount;

  const dataToExport = formatTableDataToCSVData(exportTableData);

  const baseFileName = createFormattedFileName(currentCompanyName, tableExportName);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isDeletable,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName
  };
};

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(SettingStandardMailHeader);
