import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import moment from 'moment';
import Brochure from 'components/groups/Brochure';
import { setPlaquetteTreeDialog } from 'redux/tabs/dadp/actions';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);
  const societyId = state.navigation.id;
  return {
    form: `dadpPlaquette_${societyId}`,
    reviews_list: dadp.reviews.data || [],
    isOpen: dadp.isPlaquetteTreeDialogOpen,
    initialValues: {
      reviewId: dadp.reviews.selectedId
    }
  };
};

const mapDispatchToProps = dispatch => ({
  setPlaquetteTreeDialog: isOpen => dispatch(setPlaquetteTreeDialog(isOpen))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { reviews_list } = stateProps;
  const { setPlaquetteTreeDialog } = dispatchProps;
  const reviews = reviews_list.map(r => ({
    label: r.type === 'BIL'
      ? `Bilan du ${moment(r.start_date).format('DD/MM/YY')} au ${moment(r.end_date).format('DD/MM/YY')}`
      : `Situation du ${moment(r.start_date).format('DD/MM/YY')} au ${moment(r.end_date).format('DD/MM/YY')}`,
    value: r.id_dossier_revision
  }));

  const onGenerate = () => setPlaquetteTreeDialog(true);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    reviews,
    onGenerate
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  reduxForm({
    enableReinitialize: true
  })
)(Brochure);
