import { connect } from 'react-redux';
import {
  TOGGLEVALUE
} from 'helpers/dashboard';
import Toggle from 'components/groups/Toggle';
import I18n from 'assets/I18n';
import _ from 'lodash';

const mapStateToProps = state => ({
  currentMode: _.get(state, 'dashboardWeb.currentMode', 1)
});

const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    currentMode
  } = stateProps;

  const {
    clearProgressionTableSort,
    clearSelectedPeriod,
    changeTrackingMode,
    getTrackingSocieties,
    isTrackingSocietiesLoading
  } = ownProps;

  const handleChangeType = (type) => {
    if (isTrackingSocietiesLoading) {
      return;
    }
    clearProgressionTableSort();
    clearSelectedPeriod();
    changeTrackingMode(type);
    getTrackingSocieties();
  };

  const data = [{
    label: I18n.t('companiesMonitoring.displayType.week'),
    onClick: () => {
      handleChangeType(1);
    },
    value: TOGGLEVALUE[1]
  },
  {
    label: I18n.t('companiesMonitoring.displayType.month'),
    onClick: () => {
      handleChangeType(2);
    },
    value: TOGGLEVALUE[2]
  },
  {
    label: I18n.t('companiesMonitoring.displayType.year'),
    onClick: () => {
      handleChangeType(3);
    },
    value: TOGGLEVALUE[3]
  }];


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    currentChoice: TOGGLEVALUE[currentMode],
    data
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Toggle);
