import { connect } from 'react-redux';
import _ from 'lodash';
import { compose } from 'redux';
import { reduxForm, getFormValues } from 'redux-form';
import moment from 'moment';

import I18n from 'assets/I18n';
import { supervisorList } from 'assets/constants/data';

import { getCurrentTabState } from 'helpers/tabs';

import { openDialog, closeDialog } from 'redux/dialogs';
import {
  getComments as getCommentsThunk,
  postAccountsComment as postAccountsCommentThunk,
  putAccountsComment as putAccountsCommentThunk
} from 'redux/comments';

import ConsultingComment from 'components/groups/Consulting/Comment';

import { CommentBox } from 'components/groups/Comments';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const { dadp } = getCurrentTabState(state);
  return {
    societyId,
    form: `consultingComment_${societyId}`,
    consultingFormValues: getFormValues(`${societyId}consultingFilter`)(state) || {},
    reviewsList: _.get(dadp, 'reviews.data', []),
    comment_information: _.get(state, `comments.${societyId}.comment_info[0]`) || {},
    isCommentLoading: _.get(state, `comments.${state.navigation.id}.isLoading`, false)
  };
};

const mapDispatchToProps = dispatch => ({
  openCommentsDialog: dialogProps => dispatch(openDialog(dialogProps)),
  closeCommentsDialog: dialogProps => dispatch(closeDialog(dialogProps)),
  getComments: (
    account_id, dossier_revision_id
  ) => dispatch(getCommentsThunk(account_id, dossier_revision_id)),
  postAccountsComment: (comment, params) => dispatch(postAccountsCommentThunk(comment, params)),
  putAccountsComment: (comment, params) => dispatch(putAccountsCommentThunk(comment, params))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    consultingFormValues: { Account, dateStart, dateEnd },
    reviewsList
  } = stateProps;
  const {
    openCommentsDialog, closeCommentsDialog,
    getComments, postAccountsComment, putAccountsComment
  } = dispatchProps;

  const currentRevision = _.find(reviewsList, review => (
    moment(dateStart).format('YYYY-MM-DD') === review.start_date && moment(dateEnd).format('YYYY-MM-DD') === review.end_date
  ));

  const commentTypeList = [
    { value: 0, label: I18n.t('consulting.comment.accountCommentType') },
    { value: 1, label: I18n.t('consulting.comment.daCommentType') }
  ];
  const getAndOpenComments = async (editMode) => {
    if (editMode) {
      openCommentsDialog({
        body: {
          Component: CommentBox,
          props: {
            onEdit: async (payload) => {
              await putAccountsComment(payload, {
                account_id: _.get(Account, 'account_id'),
                dossier_revision_id: _.get(currentRevision, 'id_dossier_revision')
              });
              closeCommentsDialog();
              getComments(_.get(Account, 'account_id'), _.get(currentRevision, 'id_dossier_revision'));
            },
            isNotEditor: true,
            createMode: false,
            getComments: () => getComments(_.get(Account, 'account_id'), _.get(currentRevision, 'id_dossier_revision'))
          }
        }
      });
    } else {
      openCommentsDialog({
        body: {
          Component: CommentBox,
          props: {
            isNotEditor: false,
            createMode: true,
            getComments: () => {},
            onSubmit: async (comment) => {
              await postAccountsComment(comment, {
                account_id: _.get(Account, 'account_id'),
                dossier_revision_id: _.get(currentRevision, 'id_dossier_revision')
              });
              getComments(_.get(Account, 'account_id'), _.get(currentRevision, 'id_dossier_revision'));
            }
          }
        }
      });
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    supervisorList,
    commentTypeList,
    getAndOpenComments,
    currentRevision,
    Account
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  reduxForm({
    enableReinitialize: true,
    initialValues: {
      commentType: 0
    }
  })
)(ConsultingComment);
