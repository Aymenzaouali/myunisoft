import { connect } from 'react-redux';
import { compose } from 'redux';
import { getFormValues } from 'redux-form';

import _ from 'lodash';

import { getConsultationEntrie as getEntriesThunk } from 'redux/consulting';
import { setCurrentIndex } from 'redux/consulting/actions';
import { pushLettering as pushLetteringThunk } from 'redux/lettrage';
import { getReviews } from 'redux/tabs/dadp';

import { splitWrap } from 'context/SplitContext';

import ConsultingScreen from 'components/groups/Consulting';
import { openPopupSplit } from 'redux/tabs/split/actions';

// Component
const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    consulting: state.consulting[societyId],
    filters: getFormValues(`${societyId}consultingFilter`)(state),
    accountID: _.get(getFormValues(`${societyId}consultingFilter`)(state), 'Account.account_id', null),
    Lettrage: _.get(state.form, 'Lettrage'),
    selection: _.get(state.consulting, `${societyId}.select`, []),
    index: state.consulting.currentIndex,
    dialogODisOpen: _.get(state.consulting, `${societyId}.isODDialogOpen`, false)
  };
};

const mapDispatchToProps = dispatch => ({
  getEntries: (societyId, rows, page, prevnext) => dispatch(
    getEntriesThunk(societyId, rows, page, prevnext)
  ),
  openPopupSplit: masterId => dispatch(openPopupSplit(masterId)),
  push: (society_id, lettering, lines, accountID) => dispatch(
    pushLetteringThunk(society_id, lettering, lines, accountID)
  ),
  setCurrentIndex: index => dispatch(setCurrentIndex(index)),
  getReviews: () => dispatch(getReviews())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { consulting, accountID } = stateProps;
  let letter = 0;
  let notLetter = 0;
  let total = 0;

  const select = _.get(consulting, `select.${accountID}`, []);
  select.forEach((item) => {
    const credit = _.get(item, 'credit', 0) || 0;
    const debit = _.get(item, 'debit', 0) || 0;
    const lettering = _.get(item, 'lettrage', '');

    letter += lettering ? Number(debit) - Number(credit) : 0;
    notLetter = +(notLetter + (!lettering ? Number(debit) - Number(credit) : 0)).toFixed(2);
  });
  total = letter + notLetter;

  const letterArray = select.filter(item => item.lettrage !== null);
  return ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    letter,
    notLetter,
    total,
    nbSelect: select.length,
    nbLetter: letterArray.length
  });
};

// Enhance
const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  splitWrap
);

export default enhance(ConsultingScreen);
