import { connect } from 'react-redux';
import _ from 'lodash';
import BankLinkToolBar from 'components/groups/ToolBar/BankLink';
import { getFormValues } from 'redux-form';
import { setFormValues, dotOrUndot } from 'redux/bankLink';

const mapStateToProps = state => ({
  selectedBills: _.get(state, `bankLink.selectedBill[${state.navigation.id}]`, []),
  societyId: state.navigation.id,
  formValues: getFormValues(`${state.navigation.id}banklinkFilter`)(state)
});

const mapDispatchToProps = dispatch => ({
  dotOrUndot: (
    societe_id, line_id, aa, mm, id_diary, balance_date, balance, month, year
  ) => dispatch(
    dotOrUndot(societe_id, line_id, aa, mm, id_diary, balance_date, balance, month, year)
  ),
  setFormValues: formValues => dispatch(setFormValues(formValues))
});
export default connect(mapStateToProps, mapDispatchToProps)(BankLinkToolBar);
