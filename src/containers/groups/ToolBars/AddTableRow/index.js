import { connect } from 'react-redux';
import ToolBar from 'components/groups/ToolBar';
import I18n from 'assets/I18n';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    isSaveEnabled,
    isEditEnabled,
    isDeleteEnabled,
    count
  } = ownProps;
  const text = I18n.t('attachments.toolBar.total', { count });

  const mainActionButton = isEditEnabled
    ? {
      _type: 'icon',
      iconName: 'icon-save',
      iconSize: '25px',
      disabled: !isSaveEnabled,
      onClick: () => {},
      titleInfoBulle: I18n.t('tooltips.attach')
    } : {
      _type: 'icon',
      iconName: 'icon-pencil',
      iconSize: '25px',
      disabled: !isEditEnabled,
      onClick: () => {},
      titleInfoBulle: I18n.t('tooltips.close')
    };

  const deleteButton = {
    _type: 'icon',
    iconName: 'icon-trash',
    color: 'error',
    iconSize: '25px',
    disabled: !isDeleteEnabled,
    onClick: () => {},
    titleInfoBulle: I18n.t('tooltips.delete')
  };

  const buttons = [
    mainActionButton,
    deleteButton
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    buttons,
    text
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ToolBar);
