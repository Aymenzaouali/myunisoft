import LetterEditor from 'components/groups/LetterEditor';
import { reduxForm } from 'redux-form';


export default reduxForm({
  form: 'newLetterForm',
  destroyOnUnmount: false
})(LetterEditor);
