import RAFCreateStatePage from 'components/groups/ReportsAndForms/RAFCreateStatePage';
import { getProps } from 'containers/groups/ReportAndForms';

export default getProps('RAFCreateState')(RAFCreateStatePage);
