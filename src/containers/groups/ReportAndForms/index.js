import _ from 'lodash';
import { formValueSelector, change } from 'redux-form';
import { getCodeTva as getCodeTvaThunk } from 'redux/tva';
import ReportsAndFormsService from 'redux/reportsAndForms';
import { connect } from 'react-redux';
import { getTableName } from 'assets/constants/tableName';
import tables from 'redux/tables/actions';

export const getProps = (pageName, ownProps) => {
  const mapStateToProps = (state) => {
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, pageName);
    const formName = 'reportAndFormCreateNewState';
    const formValues = formValueSelector(formName)(state, 'accountNumberField');
    return {
      society_id,
      tableName,
      statesList: _.get(state, `tables.${tableName}.data`, []),
      editedStates: _.get(state, `tables.${tableName}.editedRows`, {}),
      selectedStates: _.get(state, `tables.${tableName}.selectedRows`, {}),
      selectedStatesCount: _.get(state, `tables.${tableName}.selectedRowsCount`, 0),
      createdStatesCount: _.get(state, `tables.${tableName}.createdRowsCount`, 0),
      lastSelectedState: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
      isEditing: _.get(state, `tables.${tableName}.isEditing`, false),
      errors: _.get(state, `tables.${tableName}.errors`, {}),
      formValues
    };
  };
  const mapDispatchToProps = dispatch => ({
    loadData: () => {
      dispatch(getCodeTvaThunk());
    },
    changeForm: (form, field, value) => dispatch(change(form, field, value)),
    deleteState: (society_id, bills, params) => dispatch(
      ReportsAndFormsService.deleteState(society_id, bills, params, pageName)
    ),
    setEditing: (tableName, isEditing) => {
      if (isEditing) {
        dispatch(tables.startEdit(tableName));
      } else {
        dispatch(tables.endEdit(tableName));
      }
    },
    addState: (society_id, id) => dispatch(
      tables.selectRow(getTableName(society_id, pageName), id)
    ),
    removeState: (society_id, id) => dispatch(
      tables.unselectRow(getTableName(society_id, pageName), id)
    ),
    addAllStates: (society_id, ids) => dispatch(
      tables.selectAllRows(getTableName(society_id, pageName), ids)
    ),
    resetAllStates: society_id => dispatch(
      tables.unselectAllRows(getTableName(society_id, pageName))
    ),
    setErrors: (tableName, id, errors) => dispatch(
      tables.setErrors(tableName, id, errors)
    ),
    createRow: (tableName) => {
      const generatedId = Date.now();
      return dispatch(tables.createRow(tableName, {
        defaultValue: {
          id: generatedId,
          state_id: generatedId,
          isCreated: true
        }
      }));
    },
    editRow: (tableName, newState) => dispatch(tables.editRow(tableName, newState)),
    cancel: tableName => dispatch(tables.cancel(tableName)),
    getStates: (society_id, params, opts = {}) => dispatch(ReportsAndFormsService.getStates(
      society_id, params, opts, pageName
    )),
    editOrCreateState: (society_id, params) => dispatch(
      ReportsAndFormsService.editOrCreateState(society_id, params, pageName)
    )
  });

  return connect(mapStateToProps, mapDispatchToProps, ownProps);
};
