import { connect } from 'react-redux';
import DynamicLink from 'components/groups/DynamicLinks';
import { getMember as getMemberThunk } from 'redux/dynamicLinks';

const mapStateToProps = (state) => {
  const { member_id, loading } = state.memberGroup;
  return {
    member_id,
    loading
  };
};

const mapDispatchToProps = dispatch => ({
  getMember: () => dispatch(getMemberThunk())
});

export default connect(mapStateToProps, mapDispatchToProps)(DynamicLink);
