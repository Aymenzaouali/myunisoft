import { connect } from 'react-redux';
import Search from 'components/groups/Discussions/search';
import { reduxForm } from 'redux-form';

const mapStateToProps = (/* state */) => ({
});

const mapDispatchToProps = (/* dispatch */) => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const SearchWrapper = reduxForm({
  form: 'searchForm'
})(Search);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(SearchWrapper);
