import { connect } from 'react-redux';
import NewDiscussion from 'components/groups/Discussions/NewDiscussion';
import { reduxForm } from 'redux-form';

const mapStateToProps = (/* state */) => ({
  // TODO
});

const mapDispatchToProps = (/* dispatch */) => ({
  // TODO
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const NewDiscussionWrapper = reduxForm({
  form: 'newDiscussionForm'
})(NewDiscussion);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(NewDiscussionWrapper);
