import Discussions from 'components/groups/Discussions';
import { connect } from 'react-redux';

import {
  getFolders, getRoom, getRoomMessages, createMessage, /* deleteMessage, */
  reactMessage, unreactMessage, roomAddUsers, roomDeleteUsers,
  getRoomAvailableUsers, getRoomAvailableImages, createRoom, patchRoom,
  messagePatch, setActiveRoomId
} from 'common/redux/discussions';
import actions from '../../../redux/docManagement/actions';

const mapStateToProps = (state) => {
  const activeSocietyId = String(state.navigation.id); // active society id
  const isPersonalTab = activeSocietyId === '-2';

  return {
    folders: state.discussions.folders,
    rooms: state.discussions.rooms,
    roomMessages: state.discussions.roomMessages,
    lastCreatedMessage: state.discussions.lastCreatedMessage,
    roomUsers: state.discussions.roomUsers,
    roomAvailableUsers: state.discussions.roomAvailableUsers,
    roomAvailableImages: state.discussions.roomAvailableImages,
    lastCreatedRoom: state.discussions.lastCreatedRoom,
    loggedUser: state.login.user,
    errors: state.error.errors,
    activeSocietyId,
    isPersonalTab,
    activeRoomId: state.activeRoomId,
    selectedDocumentToCopy: state.docManagement.selectedDocumentToCopy
  };
};

const mapDispatchToProps = dispatch => ({
  getFolders: () => dispatch(getFolders()),
  setDocumentToCopy: docInfo => dispatch(
    actions.setDocumentToCopy(docInfo)
  ),
  getRoom: roomId => dispatch(getRoom(roomId)),
  getRoomMessages: roomId => dispatch(getRoomMessages(roomId)),
  getRoomAvailableImages: roomId => dispatch(getRoomAvailableImages(roomId)),
  createMessage: (roomId, data) => dispatch(createMessage(roomId, data)),
  // deleteMessage: id => dispatch(deleteMessage(id)),
  roomAddUsers: data => dispatch(roomAddUsers(data)),
  roomDeleteUsers: data => dispatch(roomDeleteUsers(data)),
  reactMessage: (roomId, messageId, reactionChar) => (
    dispatch(reactMessage(roomId, messageId, reactionChar))
  ),
  unreactMessage: (roomId, messageId, reactionId) => (
    dispatch(unreactMessage(roomId, messageId, reactionId))
  ),
  getRoomAvailableUsers: (roomId, roomTypeId, society_id, force) => dispatch(
    getRoomAvailableUsers(roomId, roomTypeId, society_id, force)
  ),
  createRoom: data => dispatch(createRoom(data)),
  patchRoom: (roomId, data) => dispatch(patchRoom(roomId, data)),
  messagePatch: (roomId, messageId, data) => dispatch(messagePatch(roomId, messageId, data)),
  setActiveRoomId: roomId => dispatch(setActiveRoomId(roomId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Discussions);
