import { connect } from 'react-redux';
import NewDirectMessage from 'components/groups/Discussions/NewDirectMessage';
import { reduxForm } from 'redux-form';

const mapStateToProps = state => ({
  loggedUser: state.login.user
});

const mapDispatchToProps = (/* dispatch */) => ({
  // TODO
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const NewDirectMessageWrapper = reduxForm({
  form: 'newDirectMessageForm'
})(NewDirectMessage);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(NewDirectMessageWrapper);
