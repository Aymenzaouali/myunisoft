import { connect } from 'react-redux';
import FileImportDialog from 'components/groups/Discussions/FileImportDialog';
import { reduxForm } from 'redux-form';

import { getRoomGEDItems } from 'common/redux/discussions';

const mapStateToProps = state => ({
  roomGEDItems: state.discussions.roomGEDItems
});

const mapDispatchToProps = dispatch => ({
  getRoomGEDItems: roomId => dispatch(getRoomGEDItems(roomId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const FileImportDialogWrapper = reduxForm({
  form: 'fileImportDialogForm'
})(FileImportDialog);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FileImportDialogWrapper);
