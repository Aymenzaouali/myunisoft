import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { EmployeePPDeleteDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import _ from 'lodash';
import {
  EMPLOYEE_PP_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import { getCurrentTabState } from 'helpers/tabs';
import {
  delEmployee as delEmployeeThunk
} from 'redux/tabs/physicalPersonCreation';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const { physicalPersonCreation } = getCurrentTabState(state);
  const tableName = getTableName(society_id, EMPLOYEE_PP_TABLE_NAME);
  return {
    physicalPersonCreation,
    selectedPhysicalPerson: _.get(physicalPersonCreation, 'selected_physical_person'),
    newlyCreatedPersonId: _.get(physicalPersonCreation, 'newlyCreatedPersonId'),
    selectedEmployeePP: _.get(state, `tables.${tableName}.selectedRows`, {}),
    tableName
  };
};

const mapDispatchToProps = dispatch => ({
  delEmployeePP: (
    idPP, societyPP, tableName
  ) => dispatch(delEmployeeThunk(idPP, societyPP, tableName))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedEmployeePP,
    tableName,
    physicalPersonCreation,
    selectedPhysicalPerson,
    newlyCreatedPersonId
  } = stateProps;
  const {
    delEmployeePP
  } = dispatchProps;
  const {
    onClose
  } = ownProps;
  const title = I18n.t('physicalPersonCreation.society_form.popUp.title');
  const message = I18n.t('physicalPersonCreation.society_form.popUp.deleteMessage');

  const deleteEmployeePP = async () => {
    const idPP = physicalPersonCreation
      ? selectedPhysicalPerson.pers_physique_id
      : newlyCreatedPersonId;
    const employeePP = await Object.keys(selectedEmployeePP).reduce((acc, key) => {
      acc.push({ society_id: parseInt(key, 10) });
      return acc;
    }, []);

    await delEmployeePP(idPP, employeePP, tableName);
    onClose();
  };


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    message,
    title,
    onValidate: deleteEmployeePP
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(EmployeePPDeleteDialog);
