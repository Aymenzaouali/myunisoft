import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { ConnectorDialog } from 'components/groups/Dialogs';
import { reduxForm, reset } from 'redux-form';
import { getSociety } from 'redux/navigation';
import {
  getThirdApi as getThirdApiThunk,
  getConnector as getConnectorThunk,
  putConnector as putConnectorThunk,
  postConnector as postConnectorThunk
} from 'redux/connector/index';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import {
  connectorFormValidation
} from 'helpers/validation';
import I18n from 'assets/I18n';
import _ from 'lodash';

const mapStateToProps = state => ({
  society_id: state.navigation.id,
  societies: _.get(state, 'navigation.societies', []).map(e => ({ label: e.name, value: e.society_id })),
  api: _.get(state, 'connector.api', []).map(e => ({ label: e.label, value: e.id_third_party_api }))
});

const mapDispatchToProps = dispatch => ({
  getSociety: () => dispatch(getSociety()),
  getThirdPartyApi: () => dispatch(getThirdApiThunk()),
  getConnector: () => dispatch(getConnectorThunk()),
  putConnector: payload => dispatch(putConnectorThunk(payload)),
  postConnector: payload => dispatch(postConnectorThunk(payload)),
  initializeConnectorForm: id_society => dispatch(autoFillFormValues('connectorForm', { id_society })),
  resetForm: () => dispatch(reset('connectorForm'))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { society_id, societies } = stateProps;
  const {
    getSociety, getThirdPartyApi, putConnector,
    postConnector, getConnector, initializeConnectorForm, resetForm
  } = dispatchProps;
  const {
    creationMode,
    onClose,
    reset,
    handleSubmit
  } = ownProps;

  const loadData = async () => {
    await initializeConnectorForm(society_id);
    await getSociety();
    await getThirdPartyApi();
  };

  const title = creationMode
    ? I18n.t('connector.dialog.creationTitle')
    : I18n.t('connector.dialog.modificationTitle');

  const onSubmitConnector = async (payload) => {
    try {
      if (creationMode) {
        await postConnector(payload);
        await getConnector();
        await resetForm();
        await onClose();
      } else {
        await putConnector(payload);
        await getConnector();
        onClose();
      }
    } catch (error) {
      console.log(error); // eslint-disable-line
    }
  };

  const closeDialog = () => {
    onClose();
    reset();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData,
    title,
    onSubmitConnector,
    firms: societies,
    onClose: closeDialog,
    onValidate: handleSubmit(onSubmitConnector)
  };
};

const enhance = compose(
  reduxForm({
    form: 'connectorForm',
    destroyOnUnmount: false,
    enableReinitialize: true,
    validate: connectorFormValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(ConnectorDialog);
