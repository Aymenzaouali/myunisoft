import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { reduxForm } from 'redux-form';
import { AccessDashboardSociety } from 'components/groups/Dialogs';
import { postPassword as postPasswordThunk } from 'redux/tabs/companyCreation';
import { enqueueSnackbar as enqueueSnackbarAction } from 'common/redux/notifier/action';
import { Notifications } from 'common/helpers/SnackBarNotifications';
import I18n from 'assets/I18n';
import _get from 'lodash/get';

const mapDispatchToProps = dispatch => ({
  enqueueSnackbar: notification => dispatch(
    enqueueSnackbarAction(notification)
  ),
  postPassword: (society_id, password) => dispatch(postPasswordThunk(society_id, password))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    postPassword,
    enqueueSnackbar
  } = dispatchProps;

  const {
    isOpen,
    society,
    handleSubmit,
    onValidate,
    reset,
    onClose
  } = ownProps;

  const societySelected = society && (society.name || society.label);
  const validPassword = async (values) => {
    try {
      const data = await postPassword(society.society_id, _get(values, 'password'));
      if (_get(data, 'code') === 'Failure') {
        enqueueSnackbar(Notifications(I18n.t('errors.MismatchError.bad_password'), 'warning'));
      } else if (_get(data, 'code') === 'Success') {
        onValidate();
        reset();
      }
    } catch (err) {
      console.log(err); // eslint-disable-line
      enqueueSnackbar(Notifications(I18n.t('errors.MismatchError.bad_password'), 'warning'));
    }
  };

  const onCloseDialog = () => {
    reset();
    onClose();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    societySelected,
    isOpen,
    onValidate: handleSubmit(validPassword),
    onClose: onCloseDialog
  };
};

const enhance = compose(
  reduxForm({
    form: 'accessDashboard'
  }),
  connect(null, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(AccessDashboardSociety);
