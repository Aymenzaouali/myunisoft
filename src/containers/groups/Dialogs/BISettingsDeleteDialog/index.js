import { connect } from 'react-redux';
import { compose } from 'redux';
import { BISettingsDeleteDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import BISettingsService from 'redux/bankIntegrationSettings';
import { withShortcut } from 'hoc/dialogShortcut';

const mapDispatchToProps = dispatch => ({
  deleteStatement: () => dispatch(BISettingsService.deleteStatement())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const title = I18n.t('bankIntegrationSettings.popUp.warning');
  const massage = I18n.t('bankIntegrationSettings.popUp.deleteMessage');
  const {
    deleteStatement
  } = dispatchProps;
  const {
    onClose
  } = ownProps;

  const onValidate = () => {
    deleteStatement();
    onClose();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    massage,
    title,
    onValidate
  };
};

const enhance = compose(
  connect(null, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(BISettingsDeleteDialog);
