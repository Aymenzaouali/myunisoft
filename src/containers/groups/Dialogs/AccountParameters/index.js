import { connect } from 'react-redux';
import { AccountParameters } from 'components/groups/Dialogs';
import { reduxForm } from 'redux-form';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { withShortcut } from 'hoc/dialogShortcut';
import { compose } from 'redux';
import _ from 'lodash';
import {
  postAccountParam as postAccountParamThunks,
  getAccountParam as getAccountParamThunks
} from 'redux/accounting';

const mapStateToProps = state => ({
  navigation: state.navigation,
  societyId: state.navigation.id,
  list: _.get(state, 'accountingWeb.silae.list', []),
  loading: state.accountingWeb.silae.loading
});

const mapDispatchToProps = dispatch => ({
  initCurrentAccount: data => dispatch(autoFillFormValues('accountParamDialog', data)),
  postAccountParam: () => dispatch(postAccountParamThunks()),
  getAccountParam: societyId => dispatch(getAccountParamThunks(societyId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { navigation: { tabs, id } } = stateProps;
  const {
    handleSubmit, onClose, currentAccount, previousAccount
  } = ownProps;
  const {
    postAccountParam,
    getAccountParam,
    initCurrentAccount
  } = dispatchProps;
  const value = tabs.find(value => id === parseInt(value.id, 10)) || {};

  const initFieldAccount = () => {
    if (previousAccount) {
      initCurrentAccount({
        account_number_original: previousAccount,
        final_account: currentAccount
      });
    }
  };

  const onValidate = () => {
    handleSubmit(postAccountParam)();
    getAccountParam();
    onClose();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initFieldAccount,
    label: value.label,
    postAccountParam: handleSubmit(postAccountParam),
    onValidate
  };
};


const enhance = compose(
  reduxForm({
    form: 'accountParamDialog',
    destroyOnUnmount: false
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  withShortcut()
);

export default enhance(AccountParameters);
