import { connect } from 'react-redux';
import { compose } from 'redux';
import { DeleteConfirmation } from 'components/groups/Dialogs';
import { withShortcut } from 'hoc/dialogShortcut';
import I18n from 'assets/I18n';

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const title = I18n.t('deleteDialog.warning');
  const message = I18n.t('deleteDialog.mainMessage');
  const secondMessage = I18n.t('deleteDialog.secondMessage');
  const {
    deleteReportAndForm,
    onClose
  } = ownProps;

  const onValidate = () => {
    deleteReportAndForm();
    onClose();
  };

  return {
    title,
    message,
    secondMessage,
    onValidate,
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};

const enhance = compose(
  connect(null, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(DeleteConfirmation);
