import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { GroupsDialog } from 'components/groups/Dialogs';
// eslint-disable-next-line import/no-named-as-default
import LoginService from 'common/redux/login';
import actions from 'common/redux/group/actions';
import { push } from 'connected-react-router';
import navigationActions from 'redux/navigation/actions';
import I18n from 'assets/I18n';
import { withShortcut } from 'hoc/dialogShortcut';

const mapStateToProps = state => ({
  items: state.group.list.map(group => ({ value: group.id, label: group.name, ...group })),
  user: state.login.user
});

const mapDispatchToProps = dispatch => ({
  signIn: payload => dispatch(LoginService.signIn(payload)),
  changeCurrentGroup: group => dispatch(actions.changeCurrentGroup(group)),
  initTabs: user => dispatch(navigationActions.initTabs(user)),
  push: path => dispatch(push(path))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    items,
    user
  } = stateProps;

  const {
    changeCurrentGroup,
    signIn,
    push, // eslint-disable-line
    initTabs
  } = dispatchProps;

  const {
    onClose,
    handleSubmit
  } = ownProps;

  const title = I18n.t('login.chooseGroup');

  const onSubmitGroup = async (payload) => {
    try {
      await changeCurrentGroup(items.find(group => group.id === payload.group));
      await signIn(payload);
      await initTabs(user);
      await push('/tab/collab/dashBoard');
    } catch (error) {
      console.log(error); // eslint-disable-line
      onClose();
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    title,
    onValidate: handleSubmit(onSubmitGroup)
  };
};

const enhance = compose(
  reduxForm({
    form: 'loginForm',
    enableReinitialize: true,
    destroyOnUnmount: false
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(GroupsDialog);
