import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { SocietyPPDeleteDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import _ from 'lodash';
import {
  SOCIETY_PP_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import { getCurrentTabState } from 'helpers/tabs';
import {
  delSociety as delSocietyThunk
} from 'redux/tabs/physicalPersonCreation';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const { physicalPersonCreation } = getCurrentTabState(state);
  const tableName = getTableName(society_id, SOCIETY_PP_TABLE_NAME);
  return {
    physicalPersonCreation,
    selectedPhysicalPerson: _.get(physicalPersonCreation, 'selected_physical_person'),
    newlyCreatedPersonId: _.get(physicalPersonCreation, 'newlyCreatedPersonId'),
    selectedSocietyPP: _.get(state, `tables.${tableName}.selectedRows`, {}),
    tableName
  };
};

const mapDispatchToProps = dispatch => ({
  delSocietyPP: (
    idPP, societyPP, tableName
  ) => dispatch(delSocietyThunk(idPP, societyPP, tableName))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedSocietyPP,
    tableName,
    physicalPersonCreation,
    selectedPhysicalPerson,
    newlyCreatedPersonId
  } = stateProps;
  const {
    delSocietyPP
  } = dispatchProps;
  const {
    onClose
  } = ownProps;
  const title = I18n.t('physicalPersonCreation.society_form.popUp.title');
  const message = I18n.t('physicalPersonCreation.society_form.popUp.deleteMessage');

  const deleteSocietyPP = async () => {
    const idPP = physicalPersonCreation
      ? selectedPhysicalPerson.pers_physique_id
      : newlyCreatedPersonId;
    const societyPP = await Object.keys(selectedSocietyPP).reduce((acc, key) => {
      acc.push({ society_id: parseInt(key, 10) });
      return acc;
    }, []);
    await delSocietyPP(idPP, societyPP, tableName);
    onClose();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    message,
    title,
    onValidate: deleteSocietyPP
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(SocietyPPDeleteDialog);
