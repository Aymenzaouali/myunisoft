import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import _ from 'lodash';
import EditRib from 'components/groups/Dialogs/RIB';
import {
  getDiariesList as getDiariesListThunk,
  putRib as putRibThunk,
  createRib as createRibThunk
} from 'redux/rib';
import {
  ribValidation,
  ribAsyncValidation
} from 'helpers/validation';
import { getFormValues, reduxForm } from 'redux-form';

const mapStateToProps = state => ({
  newRib: getFormValues('ribEdit')(state),
  diariesList: _.get(state, 'rib.diariesList', []),
  selectRib: _.get(state, 'rib.ribSelected', {}),
  ribSelected: _.get(state, 'rib.ribSelected', {})
});

const mapDispatchToProps = dispatch => ({
  getDiariesList: () => dispatch(getDiariesListThunk()),
  updateRib: rib_id => dispatch(putRibThunk(rib_id)),
  createRib: newRib => dispatch(createRibThunk(newRib)),
  loadData: () => dispatch(getDiariesListThunk())
});
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    diariesList,
    selectRib,
    ribSelected,
    newRib
  } = stateProps;
  const {
    updateRib,
    createRib
  } = dispatchProps;
  const {
    onClose,
    isEdit,
    reset,
    handleSubmit
  } = ownProps;

  const closeDialog = () => {
    onClose();
    reset();
  };

  const ribId = _.get(selectRib, 'rib_id', {});
  const onSubmitRib = async (rib) => {
    if (!rib) return;

    try {
      if (isEdit) {
        await updateRib(ribId);
      } else {
        await createRib(newRib);
      }
      closeDialog();
    } catch (err) {
      console.log(err);
    }
  };

  const paperList = diariesList.map(diary => ({
    value: diary.diary_id,
    label: diary.name
  }));


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    paperList,
    onValidate: handleSubmit(onSubmitRib),
    onClose: closeDialog,
    ribSelected
  };
};

const enhance = compose(
  reduxForm({
    form: 'ribEdit',
    destroyOnUnmount: true,
    validate: ribValidation,
    asyncValidate: ribAsyncValidation,
    asyncBlurFields: ['rib_key']
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  withShortcut()
);

export default enhance(EditRib);
