import { connect } from 'react-redux'; // TODO: C'est quoi cette fenêtre ? Ne semble pas utiliser ?
import { compose } from 'redux';
import { GroupsDialog } from 'components/groups/Dialogs';
import { reduxForm, getFormValues } from 'redux-form';
import I18n from 'assets/I18n';
import {
  redirectFromCollab as redirectFromCollabThunk
} from 'redux/navigation';
import _ from 'lodash';
import { withShortcut } from 'hoc/dialogShortcut';

const mapStateToProps = state => ({
  form: 'societySelection',
  formValues: getFormValues('societySelection')(state),
  items: state.login.societies.map(societies => ({
    value: societies.id_societe,
    label: societies.name,
    ...societies
  })),
  dialogRoute: state.navigation.dialogRoute
});

const mapDispatchToProps = dispatch => ({
  redirectDashCollab: (type, societyId) => dispatch(redirectFromCollabThunk(type, societyId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { formValues, dialogRoute } = stateProps;
  const { redirectFromCollab } = dispatchProps;
  const { onClose, handleSubmit } = ownProps;

  const societyId = _.get(formValues, 'group');

  const title = I18n.t('navBar.dialogTitle');

  const onSubmitGroup = async () => {
    onClose();
    redirectFromCollab(dialogRoute.state.type, societyId);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    title,
    onValidate: handleSubmit(onSubmitGroup)
  };
};

const enhance = compose(
  reduxForm({
    form: 'societySelection'
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(GroupsDialog);
