import { connect } from 'react-redux';
import { reduxForm, reset } from 'redux-form';
import { MailParagraphDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import { settingStandardMailFormValidation } from 'helpers/validation';
import SettingStandardMailService from 'redux/settingStandardMail';
import { PARAGRAPH_TABLE, getTableName } from 'assets/constants/tableName';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, PARAGRAPH_TABLE);
  return {
    society_id: state.navigation.id,
    selectedTypes: _.get(state, `tables.${tableName}.data`, []),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null)
  };
};

const mapDispatchToProps = dispatch => ({
  onSubmitParagraph: () => dispatch(SettingStandardMailService.sendParagraph()),
  resetForm: form => dispatch(reset(form)),
  modifyParagraph: () => dispatch(SettingStandardMailService.modifyParagraph()),
  getParagraphs: () => dispatch(SettingStandardMailService.getParagraphs())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { creationMode, onClose } = ownProps;
  const {
    onSubmitParagraph,
    getParagraphs,
    resetForm,
    modifyParagraph
  } = dispatchProps;

  const { lastSelectedRow, selectedTypes } = stateProps;
  const selected = _.filter(selectedTypes, type => type.unique_id === lastSelectedRow)[0];
  const title = creationMode
    ? I18n.t('settingStandardMail.dialogs.newParagraphTitle')
    : I18n.t('settingStandardMail.dialogs.modifyParagraphTitle');

  const onPostStatement = async () => {
    try {
      if (!creationMode) {
        await modifyParagraph();
      } else {
        await onSubmitParagraph();
      }
      await getParagraphs();
      await onClose();
      resetForm('mailParagraphDialogForm');
    } catch (error) {
      console.log(error);
    }
  };

  let initialVal = {};

  if (selected) {
    initialVal = !creationMode ? {
      ...selected
    } : {};
  }


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onPostStatement,
    title,
    creationMode,
    initialValues: { ...initialVal }
  };
};

const WrappedWithWorksheetsForm = reduxForm({
  form: 'mailParagraphDialogForm',
  destroyOnUnmount: false,
  enableReinitialize: true,
  validate: settingStandardMailFormValidation
})(MailParagraphDialog);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WrappedWithWorksheetsForm);
