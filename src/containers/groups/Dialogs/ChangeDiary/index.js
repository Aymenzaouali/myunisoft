import { connect } from 'react-redux';
import { compose } from 'redux';
import { ChangeDiary } from 'components/groups/Dialogs';
import { reduxForm, formValueSelector } from 'redux-form';
import { withShortcut } from 'hoc/dialogShortcut';
import {
  changeDiary as changeDiaryThunk
} from 'redux/consulting';
import _ from 'lodash';

const mapStateToProps = state => ({
  currentDiary: formValueSelector('changeDiary')(state, 'diary')
});

const mapDispatchToProps = dispatch => ({
  changeDiary: (societyId, entry_id, line_entry, date) => {
    dispatch(changeDiaryThunk(societyId, entry_id, line_entry, date));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    currentDiary
  } = stateProps;
  const {
    societyId,
    data = [],
    onClose
  } = ownProps;
  const {
    changeDiary
  } = dispatchProps;

  const onValidate = async () => {
    const currentDiaryId = _.get(currentDiary, 'diary_id');
    try {
      await changeDiary(societyId, currentDiaryId, data.map(({ entry_line_id, date }) => ({
        line_entry_id: entry_line_id,
        date_piece: date
      })));
      onClose();
    } catch (error) {
      console.log(error); // Toast maybe ?
    }
  };

  const firstSelectedLine = data[0] || {};

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initialValues: {
      diary: {
        label: firstSelectedLine.diary_code,
        value: firstSelectedLine.diary_code,
        diary_id: firstSelectedLine.diary_id
      }
    },
    onValidate
  };
};


const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    form: 'changeDiary',
    enableReinitialize: true
  }),
  withShortcut()
);


export default enhance(ChangeDiary);
