import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { SubsidiaryDeleteDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import _ from 'lodash';
import {
  SUBSIDIARY_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import {
  deleteSubsidiaries as deleteSubsidiariesThunk
} from 'redux/tabs/companyCreation';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, SUBSIDIARY_TABLE_NAME);
  return {
    selectedSubsidiaries: _.get(state, `tables.${tableName}.selectedRows`, {}),
    tableName
  };
};

const mapDispatchToProps = dispatch => ({
  deleteSubsidiaries: (
    subsidiary, mode, tableName
  ) => dispatch(deleteSubsidiariesThunk(subsidiary, mode, tableName))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedSubsidiaries,
    tableName
  } = stateProps;
  const {
    deleteSubsidiaries
  } = dispatchProps;
  const {
    onClose
  } = ownProps;
  const title = I18n.t('companyCreation.subsidiaries.popUp.title');
  const message = I18n.t('companyCreation.subsidiaries.popUp.deleteMessage');

  const deleteSubsidiary = async () => {
    const subsidiary = await Object.keys(selectedSubsidiaries).reduce((acc, key) => {
      acc.push({ society_id: parseInt(key, 10) });
      return acc;
    }, []);
    await deleteSubsidiaries(subsidiary, tableName);
    onClose();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    message,
    title,
    onValidate: deleteSubsidiary
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(SubsidiaryDeleteDialog);
