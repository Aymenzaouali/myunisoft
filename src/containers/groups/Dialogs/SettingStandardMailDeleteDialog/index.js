import { connect } from 'react-redux';
import { SettingStandardMailDeleteDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import SettingStandardMailService from 'redux/settingStandardMail';

const mapDispatchToProps = dispatch => ({
  deleteParagraphsTypes: () => dispatch(SettingStandardMailService.deleteParagraphsTypes())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const title = I18n.t('settingStandardMail.deletePopUp.warning');
  const message = I18n.t('settingStandardMail.deletePopUp.mainMessage');

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    message,
    title
  };
};

export default connect(null, mapDispatchToProps, mergeProps)(SettingStandardMailDeleteDialog);
