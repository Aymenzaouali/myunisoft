import { connect } from 'react-redux';
import _ from 'lodash';
import { MenuDialog } from 'components/groups/Dialogs';
import {
  redirectFromCollab as redirectFromCollabThunk,
  toggleSocietyDialog
} from 'redux/navigation';
import {
  getbudgetInsightToken as getBudgetInsightRedirectionThunk
} from 'redux/collector';
import { setSubCategory } from 'redux/tabs/dadp/actions';
import actions from 'redux/navigation/actions';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  isDialogOpen: state.navigation.societyDialog.isOpen,
  rights: _.get(state, 'rights.rights', [])
});

const mapDispatchToProps = dispatch => ({
  toggleDialog: (value, route) => dispatch(toggleSocietyDialog(value, route)),
  closeDialog: value => dispatch(actions.closeDialog(value)),
  redirectFromCollab: (route, type, id) => dispatch(redirectFromCollabThunk(route, type, id)),
  getBudgetInsightRedirection: societyId => dispatch(getBudgetInsightRedirectionThunk(societyId)),

  setSubCategory: subCategory => dispatch(setSubCategory(subCategory))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { isDialogOpen } = stateProps;
  const { toggleDialog, closeDialog } = dispatchProps;

  const toSocietyDialog = (route) => {
    toggleDialog(!isDialogOpen, route);
  };

  const onCloseDialog = () => {
    closeDialog(!isDialogOpen);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    toSocietyDialog,
    onCloseDialog
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(MenuDialog);
