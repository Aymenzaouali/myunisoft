import { withShortcut } from 'hoc/dialogShortcut';
import { RAFConfirmDialog } from 'components/groups/Dialogs';


export default withShortcut()(RAFConfirmDialog);
