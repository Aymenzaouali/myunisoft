import { connect } from 'react-redux';
import TextFieldDialog from 'components/groups/Dialogs/TextField';
import { postMemo as postMemoThunk } from 'redux/dashboard';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { withShortcut } from 'hoc/dialogShortcut';
import _ from 'lodash';

const mapStateToProps = state => ({
  idSociety: _.get(state, 'navigation.id', null),
  currentPostMemo: _.get(state, 'form.memoCreationForm.values.memoPost', null)
});

const mapDispatchToProps = dispatch => ({
  onCreateMemo: (newMemo, idSociety) => dispatch(postMemoThunk(newMemo, idSociety))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentPostMemo, idSociety } = stateProps;
  const { onCreateMemo } = dispatchProps;
  const { onClose, reset } = ownProps;


  const onCloseModal = () => {
    onClose();
    reset();
  };

  const onValidate = async () => {
    await onCreateMemo(currentPostMemo, idSociety);
    onCloseModal();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate,
    onCloseModal
  };
};

export default compose(
  reduxForm({
    form: 'memoCreationForm'
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  withShortcut()
)(TextFieldDialog);
