import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { FunctionDialog } from 'components/groups/Dialogs';
import {
  getGroupFunction as getGroupFunctionThunk,
  postGroupFunction as postGroupFunctionThunk,
  putGroupFunction as putGroupFunctionThunk
} from 'redux/function';
import { functionValidation } from 'helpers/validation';
import { reduxForm } from 'redux-form';
import I18n from 'assets/I18n';
import { handleFormError } from 'helpers/error';
import _ from 'lodash';

const mapStateToProps = state => ({
  selectedFunction: _.get(state, 'groupFunctions.selectedFunction', []),
  allFunctions: _.get(state, 'groupFunctions.functions', [])
});

const mapDispatchToProps = dispatch => ({
  getFunction: () => dispatch(getGroupFunctionThunk()),
  postFunction: payload => dispatch(postGroupFunctionThunk(payload)),
  putFunction: (payload, idgroup) => dispatch(putGroupFunctionThunk(payload, idgroup))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { selectedFunction, allFunctions } = stateProps;
  const { getFunction, postFunction, putFunction } = dispatchProps;
  const {
    creationMode, onClose, handleSubmit, reset
  } = ownProps;

  const title = creationMode
    ? I18n.t('function.dialog.creationTitle')
    : I18n.t('function.dialog.modificationTitle');


  const availableName = (name) => {
    const available = !allFunctions.some(f => f.libelle.toLowerCase() === name.toLowerCase());

    return available;
  };
  const closeDialog = () => {
    onClose();
    reset();
  };

  const onSubmitFunction = async (payload) => {
    if (availableName(payload.libelle)) {
      if (creationMode) {
        postFunction(payload);
      } else if (!creationMode) {
        putFunction(payload, selectedFunction);
      }
      await getFunction();
      closeDialog();
    } else {
      handleFormError();
    }
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onClose: closeDialog,
    onValidate: handleSubmit(onSubmitFunction),
    title
  };
};

const enhance = compose(
  reduxForm({
    form: 'functionForm',
    validate: functionValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(FunctionDialog);
