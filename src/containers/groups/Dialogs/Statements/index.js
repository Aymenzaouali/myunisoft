import { connect } from 'react-redux';
import { getFormValues, reduxForm, SubmissionError } from 'redux-form';
import { compose } from 'redux';
import I18n from 'assets/I18n';
import _ from 'lodash';

import {
  removeRib as removeRibThunk
} from 'redux/statements';

import Statements from 'components/groups/Dialogs/Statements';
import { withShortcut } from 'hoc/dialogShortcut';

const mapStateToProps = (state) => {
  const initializeRibList = () => {
    const ribList = _.get(state, 'rib.ribList', []);
    return ribList.map((item) => {
      const { rib_id, iban } = item;

      return {
        value: rib_id,
        label: iban
      };
    });
  };

  const ribList = initializeRibList();
  const statementsForm = getFormValues('Statements')(state);
  const payment = _.get(statementsForm, 'payment', []);
  const totalAmount = payment.reduce((
    accumulator, currentValue
  ) => accumulator + parseFloat(currentValue.amount), 0);

  return {
    ribList,
    totalAmount,
    payment,
    isLoading: _.get(state, 'statements.isLoading', false),
    isError: _.get(state, 'statements.isError', false),
    statementsForm,
    amountToBePaid: _.get(statementsForm, 'amountToBePaid', 0)
  };
};

const mapDispatchToProps = dispatch => ({
  removeRib: id => dispatch(removeRibThunk(id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    totalAmount,
    amountToBePaid,
    ribList,
    payment
  } = stateProps;

  const {
    handleSubmit,
    onClose,
    sendBundleStatements
  } = ownProps;

  const selectedRibs = [];
  payment.forEach(item => selectedRibs.push(Number(item.rib.value)));

  const filteredRibsList = ribList.filter(item => selectedRibs.indexOf(item.value) === -1 && item);

  const onSubmit = async (data) => {
    const errors = {};
    const { type: { value: typeValue } } = data;
    if (totalAmount) {
      switch (typeValue) {
      case 'part':
        if (totalAmount > amountToBePaid) {
          errors.amountToBePaid = I18n.t('statement.errors.superior');
        } else {
          await sendBundleStatements(false);
          await onClose();
        }
        break;
      case 'full':
        if (totalAmount !== amountToBePaid) {
          errors.amountToBePaid = I18n.t('statement.errors.different');
        } else {
          await sendBundleStatements(true);
          await onClose();
        }
        break;
      default:
        break;
      }
    }
    throw new SubmissionError(errors);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    filteredRibsList,
    countOfRibs: ribList.length,
    onValidate: handleSubmit(onSubmit)
  };
};

const enhance = compose(
  reduxForm({
    form: 'Statements'
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(Statements);
