import { connect } from 'react-redux';
import { ConfirmationDialog } from 'components/groups/Dialogs';
import _get from 'lodash/get';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    dialog: _get(state, `fixedAssets.${societyId}.dialog`, {})
  };
};

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { dialog } = stateProps;
  const buttonsLabel = {
    cancel: 'Annuler',
    validate: 'Valider'
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    ...dialog,
    buttonsLabel
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ConfirmationDialog);
