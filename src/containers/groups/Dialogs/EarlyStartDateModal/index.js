import { connect } from 'react-redux';
import EarlyStartDateModal from 'components/groups/Dialogs/EarlyStartDateModal';
import actions from 'redux/fixedAssets/actions';
import { get as _get } from 'lodash';

const mapStateToProps = (state) => {
  const { id: societyId } = state.navigation;
  const selectedAccount = _get(state, `fixedAssets.${societyId}.selectedAccount`);
  const immos = _get(selectedAccount, 'arrayImmo', []);
  return {
    societyId,
    selectedAccount,
    modalEarlyStartImmoIndex: _get(state, `fixedAssets.${societyId}.modalEarlyStartImmoIndex`),
    immos
  };
};

const mapDispatchToProps = dispatch => ({
  editImmo: (societyId, immoId, value) => dispatch(actions.editImmo(societyId, immoId, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    immos,
    modalEarlyStartImmoIndex
  } = stateProps;
  const {
    editImmo
  } = dispatchProps;
  const {
    onClose
  } = ownProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    closeDialog: () => {
      onClose();
      editImmo(societyId, { ...immos[modalEarlyStartImmoIndex], start_date: '' });
    },
    immo: immos[modalEarlyStartImmoIndex]
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(EarlyStartDateModal);
