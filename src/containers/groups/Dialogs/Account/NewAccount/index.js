import { connect } from 'react-redux';
import { reduxForm, change, getFormValues } from 'redux-form';
import { compose } from 'redux';
import _ from 'lodash';

import {
  switchTabAccountCreation as switchTabAccountCreationThunk
} from 'redux/accountCreation';
import {
  getChartAccount as getChartAccountThunk
} from 'redux/chartAccounts';
import {
  createAccount as createAccountThunk,
  updateAccount as updateAccountThunk
} from 'redux/accounting';

import { infoGValidation } from 'helpers/validation';

import NewAccount from 'components/groups/Dialogs/Account/NewAccount';
import { withShortcut } from 'hoc/dialogShortcut';

// Component
const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    activeTab: _.get(state, 'accountCreationWeb.selectTab', 0),
    newAccountingValues: getFormValues(`${societyId}newAccounting`)(state)
  };
};

const mapDispatchToProps = dispatch => ({
  switchTab: value => dispatch(switchTabAccountCreationThunk(value)),
  createAccount: account => dispatch(createAccountThunk(account)),
  updateAccount: account => dispatch(updateAccountThunk(account)),
  getChartAccount: () => dispatch(getChartAccountThunk()),
  changeForm: (form, field, value) => dispatch(change(form, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId
  } = stateProps;

  const {
    onValidate = () => {},
    onClose,
    handleSubmit,
    currentAccount,
    isContrepartie
  } = ownProps;
  const {
    createAccount,
    updateAccount,
    changeForm
  } = dispatchProps;

  const onSubmitAccount = async (payload) => {
    const infoG = _.get(payload, 'infoG', false);
    if (!infoG) {
      const errors = infoGValidation({});
      changeForm('newAccount', {}, errors);
      return;
    }
    try {
      const account = {
        provider: _.get(infoG, 'provider'),
        intraco_account: _.get(infoG, 'intraco_account'),
        account_number: _.get(infoG, 'account_number'),
        label: _.get(infoG, 'label', ''),
        society_id: societyId,
        vat_param_id: _.get(infoG, 'tva.vat_param_id', -1),
        counterpart_account_id: _.get(infoG, 'counterpart_account.account_id', -1)
      };

      const { account_id } = infoG;
      if (account_id) {
        // update a simple account
        await updateAccount({ ...account, account_id });
        await onValidate({ ...payload.infoG, value: payload.infoG.label });
      } else {
        const { account_id } = await createAccount(account);
        // Create a new conterpartie account and link it to account
        if (isContrepartie) {
          await updateAccount({
            account_id: currentAccount.account_id,
            counterpart_account_id: account_id
          });
          await onValidate({
            account_id: currentAccount.account_id,
            vat_param_id: account.vat_param_id,
            counterpart_account: {
              account_id,
              account_number: infoG.account_number,
              label: infoG.account_number,
              value: infoG.value
            }
          });
        } else { // Create simple account
          await onValidate({ ...payload.infoG, value: payload.infoG.label, account_id });
        }
      }
      onClose();
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate: handleSubmit(onSubmitAccount)
  };
};


// Enhance
const enhance = compose(
  reduxForm({
    form: 'newAccount',
    enableReinitialize: true,
    validate: infoGValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(NewAccount);
