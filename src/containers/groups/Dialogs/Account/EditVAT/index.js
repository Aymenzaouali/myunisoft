import { connect } from 'react-redux';
import { reduxForm, getFormValues } from 'redux-form';

import { closeDialog } from 'redux/dialogs';
import {
  updateAccount as updateAccountThunk
} from 'redux/accounting';

import {
  getChartAccount as getChartAccountThunk
} from 'redux/chartAccounts';

import EditVAT from 'components/groups/Dialogs/Account/EditVAT';

// Component
const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    editVatForm: getFormValues(`${societyId}editAccounts`)(state) || {},
    form: `${societyId}editAccounts`,
    societyId
  };
};

const mapDispatchToProps = dispatch => ({
  closeDialog: dialogProps => dispatch(closeDialog(dialogProps)),
  updateAccount: account => dispatch(updateAccountThunk(account)),
  getChartAccount: society_id => dispatch(getChartAccountThunk(society_id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId, editVatForm: { vatCode } } = stateProps;
  const { closeDialog, updateAccount, getChartAccount } = dispatchProps;
  const { selectedAccount } = ownProps;

  const onValidate = async () => {
    await selectedAccount.map(e => updateAccount({
      account_id: e,
      vat_param_id: vatCode.vat_param_id
    }));
    getChartAccount(societyId);
    closeDialog();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onClose: closeDialog,
    onValidate
  };
};

// Enhance
const wrappedEditVAT = reduxForm({
  enableReinitialize: true
})(EditVAT);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrappedEditVAT);
