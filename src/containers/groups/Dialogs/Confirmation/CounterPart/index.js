import { connect } from 'react-redux'; // TODO: Refacto with HOC
import { ConfirmationDialog } from 'components/groups/Dialogs';
import { updateAccount as updateAccountThunk } from 'redux/accounting';
import { reduxForm } from 'redux-form';
import I18n from 'assets/I18n';
import _ from 'lodash';

const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
  updateAccount: account => dispatch(updateAccountThunk(account))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    account,
    newAccount,
    onCloseValidate,
    counterpart_account,
    fromParamAuto
  } = ownProps;
  const { updateAccount } = dispatchProps;

  const onValidate = async () => {
    try {
      await updateAccount(newAccount);
      onCloseValidate();
    } catch (error) {
      console.log(error);
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    title: I18n.t('counterPart.title'),
    message: !fromParamAuto
      ? I18n.t('counterPart.message', { label: _.get(account, 'label', '') })
      : I18n.t('counterPart.msgParamAuto', { accountNumber: _.get(account, 'account_number', ''), counterPartAccount: _.get(counterpart_account, 'account_number', '') }),
    onValidate
  };
};

const WrappedWithFormCounterPart = reduxForm({
  form: 'counterPartForm',
  destroyOnUnmount: false,
  enableReinitialize: true
})(ConfirmationDialog);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WrappedWithFormCounterPart);
