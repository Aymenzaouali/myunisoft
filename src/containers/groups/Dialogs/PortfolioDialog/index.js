import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import _ from 'lodash';
import { PortfolioDialog } from 'components/groups/Dialogs';
import { change, reduxForm, reset } from 'redux-form';
import I18n from 'assets/I18n';
import { portfolioDialogValidation } from 'helpers/validation';
import PortfolioListSettingsService from 'redux/portfolioListSettings';
import { getSociety } from 'redux/navigation';

const mapStateToProps = state => ({
  societies: _.get(state, 'navigation.societies', []),
  walletList: _.get(state, 'portfolioListSettings.wallets.wallets_list', []),
  walletSelectedLine: state.portfolioListSettings.walletSelectedLine
});

const mapDispatchToProps = dispatch => ({
  onSubmitPortfolio: () => dispatch(PortfolioListSettingsService.sendWallet()),
  getSociety: () => dispatch(getSociety()),
  resetForm: form => dispatch(reset(form)),
  initializePortfolioLibelle: (form, data) => dispatch(change(form, 'libelle', data)),
  initializePortfolioSocieties: (form, data) => dispatch(change(form, 'societies', data)),
  getWalletList: () => dispatch(PortfolioListSettingsService.getWalletList()),
  modifyWallet: () => dispatch(PortfolioListSettingsService.modifyWallet())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { walletSelectedLine, walletList, societies } = stateProps;
  const {
    creationMode,
    onClose,
    reset,
    handleSubmit
  } = ownProps;
  const {
    initializePortfolioLibelle,
    initializePortfolioSocieties,
    onSubmitPortfolio,
    getWalletList,
    resetForm,
    modifyWallet
  } = dispatchProps;

  const formName = 'portfolioListSettingsForm';

  const title = creationMode
    ? I18n.t('portfolioListView.dialog.titleNew')
    : I18n.t('portfolioListView.dialog.titleModify');

  const onPostWallet = async () => {
    try {
      if (!creationMode) {
        await modifyWallet();
      } else {
        await onSubmitPortfolio();
      }
      await getWalletList();
      await onClose();
      resetForm('portfolioListSettingsForm');
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
  };

  const wallet = walletList.find(w => w.id_wallet === walletSelectedLine);

  const libelle = wallet && wallet.libelle;
  const societies_selected_list = wallet && wallet.list_society
    .map(society => ({ ...society, label: society.name, value: society.id_society }));

  const initialVal = !creationMode ? {
    libelle,
    societies: societies_selected_list
  } : {};

  const initializePortfolioData = () => {
    initializePortfolioLibelle(formName, _.get(initialVal, 'libelle', ''));
    initializePortfolioSocieties(formName, _.get(initialVal, 'societies', []));
  };

  const closeDialog = () => {
    onClose();
    reset();
  };

  const formatSocietiesForAutoComplete = societies.map(
    society => ({ ...society, label: society.name, value: society.society_id })
  );

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onPostWallet,
    title,
    initializePortfolioData,
    societies: formatSocietiesForAutoComplete,
    creationMode,
    onClose: closeDialog,
    onValidate: handleSubmit(onPostWallet),
    initialValues: { ...initialVal }
  };
};

const enhance = compose(
  reduxForm({
    form: 'portfolioListSettingsForm',
    destroyOnUnmount: false,
    enableReinitialize: true,
    validate: portfolioDialogValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(PortfolioDialog);
