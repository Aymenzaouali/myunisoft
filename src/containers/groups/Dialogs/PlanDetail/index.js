import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';

import { withShortcut } from 'hoc/dialogShortcut';
import { PlanDetailDialog } from 'components/groups/Dialogs';

import {
  createPlanDetail as createPlanDetailThunk,
  modifyPlanDetail as modifyPlanDetailThunk
} from 'redux/planDetails';
import { handleServerMessage } from 'helpers/error';

const mapStateToProps = (state, ownProps) => ({
  initialValues: {
    ...ownProps.values
  }
});

const mapDispatchToProps = dispatch => ({
  createPlanDetail: () => dispatch(createPlanDetailThunk()),
  modifyPlanDetail: () => dispatch(modifyPlanDetailThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    createPlanDetail,
    modifyPlanDetail
  } = dispatchProps;
  const {
    onClose,
    modify,
    reset,
    handleSubmit
  } = ownProps;

  const onSubmit = async () => {
    try {
      if (modify) {
        await modifyPlanDetail();
      } else {
        await createPlanDetail();
      }

      onClose();
    } catch (error) {
      handleServerMessage(error);
    }
  };

  const closeDialog = () => {
    onClose();
    reset();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    modify,
    onValidate: handleSubmit(onSubmit),
    onClose: closeDialog
  };
};

const enhance = compose(
  reduxForm({
    form: 'planDetailForm'
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(PlanDetailDialog);
