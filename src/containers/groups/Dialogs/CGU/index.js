import { connect } from 'react-redux';
import { compose } from 'redux';
import { CguDialog } from 'components/groups/Dialogs';
import { change } from 'redux-form';
import { withShortcut } from 'hoc/dialogShortcut';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  cguToggle: () => dispatch(change('loginForm', 'cgu', true))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    onClose
  } = ownProps;

  const {
    cguToggle
  } = dispatchProps;

  const onValidate = () => {
    cguToggle();
    onClose();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate,
    title: 'CGU - policy privacy',
    src: 'https://www.myunisoft.fr/cgu-privacy-policy'
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(CguDialog);
