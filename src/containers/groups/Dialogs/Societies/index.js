import { connect } from 'react-redux'; // TODO: Vérifier si sert a qqchse de garder
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { SocietiesDialog } from 'components/groups/Dialogs';
import actions from 'redux/navigation/actions';
import { push } from 'connected-react-router';

const mapStateToProps = state => ({
  societies: state.login.societies.map(society => ({
    value: society.id_societe,
    label: society.name,
    ...society
  })),
  tabs: state.navigation.tabs
});

const mapDispatchToProps = dispatch => ({
  addTabs: tabs => dispatch(actions.addTabs(tabs)),
  push: path => dispatch(push(path))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societies,
    tabs
  } = stateProps;
  const {
    addTabs,
    push
  } = dispatchProps;
  const { onClose } = ownProps;

  const onValidate = async (societiesList) => {
    const tabs = societiesList.map(society => ({
      id: society.value,
      label: society.label,
      path: `/tab/${society.value}/dashBoard`,
      isClosable: true,
      secured: society.secured
    }));
    await addTabs(tabs);
    push(tabs[0].path);
    onClose();
  };

  const filteredSocieties = societies.filter(society => !tabs.find(tab => tab.id === society.id_societe)); // eslint-disable-line

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate,
    societies: filteredSocieties
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(SocietiesDialog);
