import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { FormsAndReportsDeleteDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import ReportsAndFormsService from 'redux/reportsAndForms';

const mapDispatchToProps = dispatch => ({
  deleteReportAndForm: () => dispatch(ReportsAndFormsService.deleteReportAndForm())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const title = I18n.t('reportsAndForms.deletePopUp.warning');
  const message = I18n.t('reportsAndForms.deletePopUp.mainMessage');
  const {
    deleteReportAndForm
  } = dispatchProps;
  const {
    onClose
  } = ownProps;

  const onValidate = () => {
    deleteReportAndForm();
    onClose();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    message,
    title,
    onValidate
  };
};

const enhance = compose(
  connect(null, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(FormsAndReportsDeleteDialog);
