import { connect } from 'react-redux';
import { reduxForm, reset } from 'redux-form';
import { SettingStandardMailDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import { settingStandardMailFormValidation } from 'helpers/validation';
import SettingStandardMailService from 'redux/settingStandardMail';
import { PARAGRAPHS_TYPES_TABLE, getTableName } from 'assets/constants/tableName';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, PARAGRAPHS_TYPES_TABLE);
  return {
    society_id: state.navigation.id,
    selectedTypes: _.get(state, `tables.${tableName}.data`, []),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null)
  };
};

const mapDispatchToProps = dispatch => ({
  onSubmitParagraphsTypes: () => dispatch(SettingStandardMailService.sendParagraphsTypes()),
  resetForm: form => dispatch(reset(form)),
  modifyParagraphsTypes: (id, type) => dispatch(
    SettingStandardMailService.modifyParagraphsTypes(id, type)
  ),
  geParagraphsTypes: () => dispatch(SettingStandardMailService.geParagraphsTypes())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { creationMode, onClose } = ownProps;
  const {
    onSubmitParagraphsTypes,
    geParagraphsTypes,
    resetForm,
    modifyParagraphsTypes
  } = dispatchProps;

  const { lastSelectedRow, selectedTypes } = stateProps;
  const selected = _.filter(selectedTypes, type => type.unique_id === lastSelectedRow)[0];
  const title = creationMode
    ? I18n.t('settingStandardMail.dialogs.newTypeTitle')
    : I18n.t('settingStandardMail.dialogs.modifyTypeTitle');

  const onPostStatement = async () => {
    try {
      if (!creationMode) {
        await modifyParagraphsTypes(selected.id_paragraph_type, selected.origine);
      } else {
        await onSubmitParagraphsTypes();
      }
      await geParagraphsTypes();
      await onClose();
      resetForm('settingStandardMailForm');
    } catch (error) {
      console.log(error);
    }
  };

  let initialVal = {};

  if (selected) {
    initialVal = !creationMode ? {
      name: selected.name,
      label_as_object: selected.label_as_object
    } : {};
  }


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onPostStatement,
    title,
    creationMode,
    initialValues: { ...initialVal }
  };
};

const WrappedWithWorksheetsForm = reduxForm({
  form: 'settingStandardMailForm',
  destroyOnUnmount: false,
  enableReinitialize: true,
  validate: settingStandardMailFormValidation
})(SettingStandardMailDialog);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WrappedWithWorksheetsForm);
