import { connect } from 'react-redux';
import { WorksheetsDialog } from 'components/groups/Dialogs';
import { reduxForm, reset } from 'redux-form';
import I18n from 'assets/I18n';
import SettingsWorksheetsService from 'redux/settingsWorksheets';
import { worksheetSettingsValidation } from 'helpers/validation';


const mapStateToProps = state => ({
  society_id: state.navigation.id,
  worksheets: state.settingsWorksheets.worksheets,
  worksheets_types: state.settingsWorksheets.worksheets_types,
  selectedWorksheet: state.settingsWorksheets.selectedWorksheet,
  accountNumbersTTC: state.settingsWorksheets.accountNumbersTTC,
  accountNumbersTVA: state.settingsWorksheets.accountNumbersTVA
});

const mapDispatchToProps = dispatch => ({
  getWorksheetsTypes: () => dispatch(SettingsWorksheetsService.getWorksheetsTypes()),
  resetWorksheetsAccountNumbers: () => dispatch(
    SettingsWorksheetsService.resetWorksheetsAccountNumbers()
  ),
  resetForm: form => dispatch(reset(form)),
  getWorksheetsAccountNumbers: (accountTVC, accountTVA) => dispatch(
    SettingsWorksheetsService.getWorksheetsAccountNumbers(accountTVC, accountTVA)
  ),
  onSubmitWorksheets: () => dispatch(SettingsWorksheetsService.sendWorksheets()),
  modifyWorksheets: () => dispatch(SettingsWorksheetsService.modifyWorksheets()),
  getWorksheets: () => dispatch(SettingsWorksheetsService.getWorksheets())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { society_id, selectedWorksheet, worksheets_types } = stateProps;
  const selected_list = selectedWorksheet[society_id];
  const selected = selected_list && selected_list[(selected_list.length - 1)];
  const {
    onSubmitWorksheets, getWorksheets, resetForm, modifyWorksheets
  } = dispatchProps;
  const { creationMode, onClose } = ownProps;

  const selectedTypeId = selected && worksheets_types.find(type => type.code === selected.type);
  const selectedAccountTTC = selected && selected.account_ttc && {
    account_id: selected.account_ttc.id,
    label: selected.account_ttc.label,
    account_number: selected.account_ttc.number,
    counterpart_account: null,
    value: selected.account_ttc.number
  };

  const selectedAccountTVA = selected && selected.account_tva && {
    account_id: selected.account_tva.id,
    label: selected.account_tva.label,
    account_number: selected.account_tva.number,
    counterpart_account: null,
    value: selected.account_tva.number
  };

  const title = creationMode
    ? I18n.t('settingsWorksheets.popUp.createNew')
    : I18n.t('settingsWorksheets.popUp.modify');

  const onPostWorksheets = async () => {
    try {
      if (!creationMode) {
        await modifyWorksheets();
      } else {
        await onSubmitWorksheets();
      }
      await getWorksheets();
      await onClose();
      resetForm('worksheetsForm');
    } catch (error) {
      console.log(error);
    }
  };

  const initialVal = !creationMode ? {
    id_worksheet_type: selectedTypeId,
    id_account_ttc: selectedAccountTTC,
    id_account_tva: selectedAccountTVA
  } : {};

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onPostWorksheets,
    title,
    creationMode,
    initialValues: { ...initialVal }
  };
};

const WrappedWithWorksheetsForm = reduxForm({
  form: 'worksheetsForm',
  destroyOnUnmount: false,
  enableReinitialize: true,
  validate: worksheetSettingsValidation
})(WorksheetsDialog);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WrappedWithWorksheetsForm);
