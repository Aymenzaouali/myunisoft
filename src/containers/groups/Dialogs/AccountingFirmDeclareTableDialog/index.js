import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm, reset } from 'redux-form';
import { AccountingFirmDeclareTableDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import { handleFormError } from 'helpers/error';
import { withShortcut } from 'hoc/dialogShortcut';

const mapStateToProps = state => ({
  society_id: state.navigation.id
});

const mapDispatchToProps = dispatch => ({
  _resetForm: form => dispatch(reset(form))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    creationMode,
    onClose,
    onAdd,
    onModify,
    handleSubmit
  } = ownProps;
  const {
    getStatementsList,
    _resetForm
  } = dispatchProps;

  const resetForm = () => {
    _resetForm('accountingFirmSettingsDeclareForm');
  };

  const title = creationMode
    ? I18n.t('accountingFirmSettings.dialog.newTitle')
    : I18n.t('accountingFirmSettings.dialog.modifyTitle');

  const onCloseDialog = () => {
    resetForm();
    onClose();
  };

  const onPostStatement = async () => {
    try {
      if (!creationMode) {
        await onModify();
      } else {
        await onAdd();
      }
      await onCloseDialog();
      await getStatementsList();
    } catch (error) {
      handleFormError(error);
    }
  };


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    resetForm,
    onPostStatement,
    title,
    onValidate: handleSubmit(onPostStatement),
    onClose: onCloseDialog
  };
};

const enhance = compose(
  reduxForm({
    form: 'accountingFirmSettingsDeclareForm',
    destroyOnUnmount: false,
    enableReinitialize: true
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);


export default enhance(AccountingFirmDeclareTableDialog);
