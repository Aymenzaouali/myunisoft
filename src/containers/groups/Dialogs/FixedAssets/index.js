import { connect } from 'react-redux';
import FixedAssetsModal from 'components/groups/Dialogs/FixedAssets';
import { compose } from 'redux';
import _get from 'lodash/get';
import {
  reduxForm,
  change,
  getFormValues
} from 'redux-form';
import fixedActions from 'redux/fixedAssets/actions';
import { roundNumber } from 'helpers/number';

const mapStateToProps = (state) => {
  const societyId = _get(state, 'navigation.id');
  const selectedImmos = _get(state, `fixedAssets[${societyId}].selectedImmos`, {});
  const selectedAccount = _get(state, `fixedAssets[${societyId}].selectedAccount`, {});
  const immos = _get(selectedAccount, 'arrayImmo', []);
  const lastSelectedImmo = _get(state, `fixedAssets[${societyId}].lastSelectedImmo`);
  const form = getFormValues('fixedAssetsModal')(state);
  const [firstImmo, ...restImmos] = _get(form, 'immos', []);
  const totalAmount = _get(form, 'immos', []).reduce((acc, im) => acc + +im.purchase_value || 0, 0);
  const restImmosValue = restImmos.reduce((acc, item) => acc + +_get(item, 'purchase_value', 0), 0) || 0;
  const isNegative = restImmos.some(immo => +immo.purchase_value < 0);
  const isFocusForm = _get(state, 'form.fixedAssetsModal.active', '');
  return {
    isFocusForm,
    form,
    totalAmount,
    firstImmo,
    restImmos,
    societyId,
    lastSelectedImmo,
    selectedImmos,
    immos,
    restImmosValue,
    isNegative
  };
};

const mapDispatchToProps = dispatch => ({
  onChange: value => dispatch(change('fixedAssetsModal', 'immos[0].purchase_value', value)),
  onImmoChange: (societyId, immo) => dispatch(fixedActions.editImmoValider(societyId, immo)),
  addNewImmos: (societyId, immos) => dispatch(fixedActions.addImmoLine(societyId, immos))
});

const mergeToProps = (stateToProps, dispatchToProps, ownProps) => {
  const {
    societyId, restImmos, restImmosValue, firstImmo
  } = stateToProps;
  const {
    onChange, addNewImmos, onImmoChange
  } = dispatchToProps;
  const { onClose, defaultImmos } = ownProps;

  const onCalculateAmount = () => {
    const newValue = (defaultImmos.purchase_value - restImmosValue) > 0
      ? (defaultImmos.purchase_value - restImmosValue)
      : 0;
    onChange(roundNumber(newValue));
  };

  const onConfirm = () => {
    restImmos.filter(
      ({ purchase_value, label }) => !!purchase_value || label
    )
      .map(item => (item.purchase_value
        ? addNewImmos(societyId, item)
        : addNewImmos(societyId, { ...item, purchase_value: 0 })));
    onImmoChange(societyId, firstImmo);
    onClose();
  };

  const isValide = _get(defaultImmos, 'purchase_value', 0) - restImmosValue >= 0;

  return {
    ...stateToProps,
    ...dispatchToProps,
    ...ownProps,
    onCalculateAmount,
    isValide,
    onConfirm
  };
};

const enhance = compose(
  reduxForm({
    form: 'fixedAssetsModal',
    destroyOnUnmount: false
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeToProps)
);

export default enhance(FixedAssetsModal);
