import { connect } from 'react-redux';
import { compose } from 'redux';
import { AssociateDeleteDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import _ from 'lodash';
import {
  LEGAL_PERSON_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import {
  deleteAssociatePersonLegal as deleteAssociatePersonLegalThunk
} from 'redux/tabs/companyCreation';
import { withShortcut } from 'hoc/dialogShortcut';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, LEGAL_PERSON_TABLE_NAME);
  return {
    selectedLegalPerson: _.get(state, `tables.${tableName}.selectedRows`, {}),
    tableName
  };
};

const mapDispatchToProps = dispatch => ({
  deleteAssociate: (
    associate, tableName
  ) => dispatch(deleteAssociatePersonLegalThunk(associate, tableName))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedLegalPerson,
    tableName
  } = stateProps;
  const {
    onClose
  } = ownProps;
  const {
    deleteAssociate
  } = dispatchProps;
  const title = I18n.t('companyCreation.associates.popUp.title');
  const message = I18n.t('companyCreation.associates.popUp.deleteMessage');

  const deleteAssociates = async () => {
    const associate = await Object.keys(selectedLegalPerson).reduce((acc, key) => {
      acc.push({ society_id: parseInt(key, 10) });
      return acc;
    }, []);
    try {
      await deleteAssociate(associate, tableName);
      onClose();
    } catch (error) {
      console.log(error);
    }
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    message,
    title,
    onValidate: deleteAssociates
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(AssociateDeleteDialog);
