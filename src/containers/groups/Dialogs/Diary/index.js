import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { DiaryDialog } from 'components/groups/Dialogs';
import {
  reduxForm,
  reset,
  getFormValues
} from 'redux-form';
import {
  postDiary as postDiaryThunk,
  getDiaries as getDiariesThunk,
  modifyDiary as modifyDiaryThunk
} from 'redux/diary';
import actions from 'redux/diary/actions';
import {
  diaryFormValidation
} from 'helpers/validation';
import _ from 'lodash';

const mapStateToProps = (state, ownProps) => ({
  initialValues: {
    ...ownProps.values
  },
  societyId: _.get(state, 'navigation.id'),
  diaryForm: getFormValues('diaryForm')(state),
  diaryCodeAvailable: _.get(state, 'diary.diaryCodeAvailable', true)
});

const mapDispatchToProps = dispatch => ({
  resetDiarySelected: () => dispatch(actions.selectDiary(null)),
  clearDiaryDetails: () => dispatch(actions.clearDiaryDetails()),
  getDiaries: () => dispatch(getDiariesThunk()),
  postDiary: () => dispatch(postDiaryThunk()),
  modifyDiary: data => dispatch(modifyDiaryThunk(data)),
  resetForm: form => dispatch(reset(form))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { diaryForm } = stateProps;

  const {
    postDiary,
    getDiaries,
    modifyDiary,
    resetForm,
    resetDiarySelected,
    clearDiaryDetails
  } = dispatchProps;

  const {
    onClose,
    modify,
    handleSubmit,
    diaryId,
    onValidate
  } = ownProps;

  const _resetForm = () => {
    resetForm('diaryForm');
  };

  const oldDiaryCode = _.get(diaryForm, 'code', undefined);

  const onCloseDialog = () => {
    onClose();
    resetDiarySelected();
    clearDiaryDetails();
    _resetForm();
  };

  const onSave = async () => {
    try {
      if (modify) {
        await modifyDiary({
          diaryId
        });
      } else {
        await postDiary();
      }
      resetDiarySelected();
      clearDiaryDetails();
      getDiaries();
      if (onValidate) {
        onValidate(diaryForm);
      }
      onCloseDialog();
      _resetForm();
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate: handleSubmit(onSave),
    modify,
    onCloseDialog,
    oldDiaryCode
  };
};

const enhance = compose(
  reduxForm({
    form: 'diaryForm',
    destroyOnUnmount: false,
    enableReinitialize: true,
    validate: diaryFormValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(DiaryDialog);
