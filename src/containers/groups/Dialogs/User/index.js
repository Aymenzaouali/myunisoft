import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import UserDialog from 'components/groups/Dialogs/User';
import { reduxForm, change, getFormValues } from 'redux-form';
import { getUser as getUserThunk } from 'redux/tabs/user';
import I18n from 'assets/I18n';
import actions from 'redux/tabs/user/actions';
import _ from 'lodash';
import { tabFormName, getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);
  return {
    form: formName,
    user_id: _.get(state, `form.${formName}.values.personne_physique_id`),
    physicalPerson: _.get(getCurrentTabState(state), 'physicalPersonList.physicalPersonsData.array_pers_physique[0]', []),
    formValues: getFormValues(formName)(state)
  };
};

const mapDispatchToProps = dispatch => ({
  _resetPersPhysiqueValue: form => dispatch(change(form, 'personne_physique_id', '')),
  changeMailUser: (form, mail) => dispatch(change(form, 'mail', mail)),
  selectTab: tabIndex => dispatch(actions.selectTab(tabIndex)),
  getUser: id => dispatch(getUserThunk(id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    form, user_id, physicalPerson, formValues
  } = stateProps;
  const {
    selectTab,
    _resetPersPhysiqueValue,
    getUser,
    changeMailUser
  } = dispatchProps;
  const {
    onClose
  } = ownProps;

  const {
    coordonnee
  } = physicalPerson;

  const resetPersPhysiqueValue = () => _resetPersPhysiqueValue(form);

  const contact = coordonnee && coordonnee.map(e => ({
    typeOfCoord: e.type.label,
    coordValue: e.value
  }));

  const isMail = contact && _.filter(contact, { typeOfCoord: 'Mail' });

  const onValidate = async () => {
    try {
      const {
        mail
      } = formValues;
      await getUser(user_id);
      if (_.isEmpty(isMail)) changeMailUser(form, mail);
      await selectTab(1);
      onClose();
    } catch (error) {
      onClose();
    }
  };

  const onCancel = async () => {
    await resetPersPhysiqueValue();
    await onClose();
    selectTab(1);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate,
    onClose: onCancel,
    UserDialogTitle: I18n.t('existingPerson.title')
  };
};

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    destroyOnUnmount: false
  }),
  withShortcut()
);

export default enhance(UserDialog);
