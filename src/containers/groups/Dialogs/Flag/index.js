import { compose } from 'redux';
import { connect } from 'react-redux';
import {
  reduxForm, change, arrayPush
} from 'redux-form';
import { withShortcut } from 'hoc/dialogShortcut';
import moment from 'moment';
import _ from 'lodash';
import I18n from 'assets/I18n';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { accountFormat } from 'helpers/format';
import { flagFormValidation } from 'helpers/validation';
import { FlagDialog } from 'components/groups/Dialogs';
// Component
const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    worksheetParam: _.get(state, `accountingWeb.worksheetParam.${societyId}.param`, []),
    dossier: _.get(state, `tabs.${societyId}.dadp.reviews.data`, []),
    society_id: state.navigation.id,
    linePosition: state.accountingWeb.entryLinePosition
  };
};
const mapDispatchToProps = dispatch => ({
  initializeTtcAccountField: account_id => dispatch(autoFillFormValues('flagDialogForm', { ttc_account: account_id })),
  initializeTvaAccountField: account_id => dispatch(autoFillFormValues('flagDialogForm', { vat_account: account_id })),
  addFlag: (societyId, payload, i) => dispatch(arrayPush(`${societyId}newAccounting`, `entry_list[${i}].flags.worksheet`, payload)),
  addImmo: (societyId, payload, i) => {
    dispatch(change(`${societyId}newAccounting`, `entry_list[${i}].flags.immo`, payload));
  }
});
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    worksheetParam,
    dossier = [],
    society_id,
    linePosition
  } = stateProps;
  const {
    initializeTtcAccountField,
    initializeTvaAccountField,
    addFlag,
    addImmo
  } = dispatchProps;
  const {
    onClose,
    reset,
    handleSubmit,
    flagType
  } = ownProps;
  const initDialogTtcField = async (id) => {
    const selectedWorksheet = await worksheetParam.filter((item) => {
      if (item.account_tva.id === id) { return item; }
      return null;
    });
    await initializeTtcAccountField(selectedWorksheet[0].account_ttc.id);
  };
  const initDialogTvaField = async (id) => {
    const selectedWorksheet = await worksheetParam.filter((item) => {
      if (item.account_ttc.id === id) { return item; }
      return null;
    });
    await initializeTvaAccountField(selectedWorksheet[0].account_tva.id);
  };
  const vatList = accountFormat(worksheetParam, 'account_tva');
  const ttcList = accountFormat(worksheetParam, 'account_ttc');
  const dossierList = !_.isEmpty(dossier)
    ? dossier.map((d) => {
      const date = d.type === 'BIL' ? d.end_date : d.start_date;
      return {
        value: d.id_dossier_revision,
        label: `${I18n.t(`dadp.review.types.${d.type}`)} ${moment(date).format('DD/MM/YYYY')}`
      };
    })
    : [];
  const onSubmitFlag = async (payload) => {
    const worksheet = {
      id_dossier_revision: _.get(payload, 'dossier_revision.value') || dossierList[0].value,
      period_start: moment(payload.dateStart).format('YYYY-MM-DD'),
      period_end: moment(payload.dateEnd).format('YYYY-MM-DD'),
      id_param_account_worksheet: _.get(worksheetParam, '[0].id_param_account_worksheet')
    };
    await addFlag(society_id, worksheet, linePosition);
  };
  const onSubmitImmo = async (payload) => {
    const immo = {
      duration: parseInt(_.get(payload, 'duration'), 10),
      amortissement_type: _.get(payload, 'amortissement'),
      start_date: moment(payload.date).format('YYYYMMDD')
    };
    await addImmo(society_id, immo, linePosition);
  };

  const closeDialog = () => {
    onClose();
    reset();
  };

  const submit = async (data) => {
    if (flagType !== 'IMMO') {
      await onSubmitFlag(data);
    } else {
      await onSubmitImmo(data);
    }
    closeDialog();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initDialogTtcField,
    initDialogTvaField,
    vatOptions: vatList,
    ttcOptions: ttcList,
    amortissementOptions: [
      { value: 1, label: I18n.t('newAccounting.flagDialog.amortissementOptions.linear') },
      { value: 2, label: I18n.t('newAccounting.flagDialog.amortissementOptions.degressive') }
    ],
    dossierOptions: dossierList || [],
    onClose: closeDialog,
    onValidate: handleSubmit(submit)
  };
};
// Enhance
const enhance = compose(
  reduxForm({
    form: 'flagDialogForm',
    validate: flagFormValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);
export default enhance(FlagDialog);
