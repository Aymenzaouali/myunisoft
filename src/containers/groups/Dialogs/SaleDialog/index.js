import { connect } from 'react-redux';
import SaleDialog from 'components/groups/Dialogs/SaleDialog';
import { compose } from 'redux';
import { get as _get, isEmpty } from 'lodash';
import actions from 'redux/fixedAssets/actions';


const mapStateToProps = (state) => {
  const societyId = _get(state, 'navigation.id');
  const sales = _get(state, `fixedAssets.${societyId}.sales.data.entry_list`);
  const selectedSale = _get(state, `fixedAssets.${societyId}.sales.selectedSale`);
  const isLoading = _get(state, `fixeddAssets.${societyId}.sales.isSaleLoading`);
  return {
    societyId,
    sales,
    isValide: !isEmpty(selectedSale),
    selectedSale,
    isLoading
  };
};

const mapDispatchToProps = dispatch => ({
  selectSale: (societyId, sale) => dispatch(actions.selectSale(societyId, sale)),
  unselectSale: societyId => dispatch(actions.unselectSale(societyId))
});

const mergeToProps = (stateToProps, dispatchProps, ownProps) => {
  const { societyId } = stateToProps;
  const { selectSale, unselectSale } = dispatchProps;
  return {
    ...stateToProps,
    ...dispatchProps,
    ...ownProps,
    selectSale: selectSale.bind(null, societyId),
    unselectSale: unselectSale.bind(null, societyId)
  };
};


const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeToProps)
);

export default enhance(SaleDialog);
