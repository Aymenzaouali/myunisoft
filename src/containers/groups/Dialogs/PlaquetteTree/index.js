import { connect } from 'react-redux';
import { compose } from 'redux';
import { formValueSelector } from 'redux-form';
import { PlaquetteTree } from 'components/groups/Dialogs';
import { getPlaquetteTreeView, postPlaquette } from 'redux/tabs/dadp';
import { setPlaquetteTreeDialog } from 'redux/tabs/dadp/actions';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);
  const societyId = state.navigation.id;
  return {
    plaquette_tree: dadp.treeView || [],
    societyId,
    reviewId: formValueSelector(`dadpPlaquette_${societyId}`)(state, 'reviewId')
  };
};

const mapDispatchToProps = dispatch => ({
  getPlaquetteTreeView: () => dispatch(getPlaquetteTreeView()),
  setPlaquetteTreeDialog: isOpen => dispatch(setPlaquetteTreeDialog(isOpen)),
  postPlaquette: payload => dispatch(postPlaquette(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { reviewId } = stateProps;
  const { setPlaquetteTreeDialog, postPlaquette } = dispatchProps;
  const onClose = () => setPlaquetteTreeDialog(false);
  const onValidate = (payload) => {
    postPlaquette({ payload, reviewId });
    onClose();
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate,
    onClose
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
)(PlaquetteTree);
