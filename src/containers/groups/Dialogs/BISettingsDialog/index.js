import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  reduxForm,
  reset,
  getFormValues
} from 'redux-form';
import { BISettingsDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import { bankIntegrationValidation } from 'helpers/validation';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import BISettingsService from 'redux/bankIntegrationSettings';
import _ from 'lodash';
import { withShortcut } from 'hoc/dialogShortcut';
import { getVat } from 'redux/tva/index';

const mapStateToProps = state => ({
  society_id: state.navigation.id,
  selectedStatements: _.get(state, `bankIntegrationSettings.selectedStatements[${state.navigation.id}]`, []),
  interbankCodes: _.get(state, `bankIntegrationSettings.interbankCodes[${state.navigation.id}].interbankCodes_list`, []),
  currentParam: getFormValues('BISettingsForm')(state),
  tvaCodes: _.get(state, `tva.vat.${state.navigation.id}.list`, []),
  accounts: _.get(state, 'accountingWeb.account', [])
});

const mapDispatchToProps = dispatch => ({
  onSubmitStatement: () => dispatch(BISettingsService.sendStatement()),
  initCurrentAccount: data => dispatch(autoFillFormValues('BISettingsForm', data)),
  _resetForm: form => dispatch(reset(form)),
  getStatementsList: () => dispatch(BISettingsService.getStatementsList()),
  modifyStatement: () => dispatch(BISettingsService.modifyStatement()),
  getInterbankCodes: () => dispatch(BISettingsService.getInterbankCodes()),
  getVat: () => dispatch(getVat())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    creationMode,
    onClose,
    handleSubmit,
    currentAccount,
    previousAccount,
    keyLabel
  } = ownProps;
  const {
    onSubmitStatement,
    getStatementsList,
    _resetForm,
    modifyStatement,
    initCurrentAccount
  } = dispatchProps;

  const { interbankCodes, selectedStatements } = stateProps;

  const interbank_codes = interbankCodes ? interbankCodes
    .map(code => ({ label: code.intitule, value: code.id })) : [];

  const resetForm = () => {
    _resetForm('BISettingsForm');
  };

  const initFieldAccount = () => {
    if (previousAccount) {
      initCurrentAccount({
        account_id: currentAccount,
        label_to_find: keyLabel || previousAccount.label,
        forced_label: keyLabel ? '' : currentAccount.label,
        debit: true,
        credit: true
      });
    }
  };

  const title = creationMode
    ? I18n.t('bankIntegrationSettings.popUp.titleNew')
    : I18n.t('bankIntegrationSettings.popUp.titleModify');

  const onPostStatement = async () => {
    try {
      if (!creationMode) {
        await modifyStatement();
      } else {
        await onSubmitStatement();
      }
      await getStatementsList();
      resetForm();
      await onClose();
    } catch (error) {
      console.log(error);
    }
  };

  const onCloseDialog = () => {
    onClose();
    resetForm();
  };

  const isAllCodesSelected = _.get(selectedStatements.slice(-1).pop(), 'list_code_interbancaire', []).length
    === interbankCodes.length;

  const params = [
    {
      label: I18n.t('bankIntegrationSettings.popUp.none'),
      value: 'none'
    },
    {
      label: I18n.t('bankIntegrationSettings.popUp.calculateTVA'),
      value: 'calculateTVA'
    },
    {
      label: I18n.t('bankIntegrationSettings.popUp.tvaAndComm'),
      value: 'tvaAndComm'
    }
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    resetForm,
    interbank_codes,
    onPostStatement,
    isAllCodesSelected,
    title,
    initFieldAccount,
    onValidate: handleSubmit(onPostStatement),
    onClose: onCloseDialog,
    params
  };
};

const enhance = compose(
  reduxForm({
    form: 'BISettingsForm',
    destroyOnUnmount: false,
    enableReinitialize: true,
    validate: bankIntegrationValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);


export default enhance(BISettingsDialog);
