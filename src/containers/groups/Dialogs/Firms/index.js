import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import Societies from 'components/groups/Dialogs/Societies';
import { societiesRedirect } from 'helpers/validation';
import {
  redirectFromCollab as redirectFromCollabThunk
} from 'redux/navigation';
import _ from 'lodash';
import actions from 'redux/navigation/actions';

const mapStateToProps = (state) => {
  const societies = _.get(state, 'navigation.societies', []);

  return {
    societies: societies.map(society => ({
      value: society.society_id,
      label: society.name,
      ...society
    })),
    dialogRoute: state.navigation.societyDialog.path,
    tabs: state.navigation.tabs
  };
};

const mapDispatchToProps = dispatch => ({
  redirectFromCollab: (route, type, societyId) => dispatch(
    redirectFromCollabThunk(route, type, societyId)
  ),
  addTabs: tabs => dispatch(actions.addTabs(tabs))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { dialogRoute } = stateProps;
  const { redirectFromCollab, addTabs } = dispatchProps;
  const { onClose } = ownProps;

  const onValidate = ({ society }) => {
    const { society_id } = society;
    const type = _.get(dialogRoute, 'state.type');
    const route = _.get(dialogRoute, 'path');

    onClose();
    redirectFromCollab(route, type, society_id);
  };

  const onValidateAndAddTab = async ({ society }) => {
    const tab = [{
      id: society.society_id,
      label: society.name,
      path: `/tab/${society.society_id}/dashBoard`,
      isClosable: true,
      secured: society.secured
    }];
    await addTabs(tab);
    onValidate({ society });
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate,
    onValidateAndAddTab
  };
};

const enhance = compose(
  reduxForm({
    form: 'societiesRedirect',
    validate: societiesRedirect
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(Societies);
