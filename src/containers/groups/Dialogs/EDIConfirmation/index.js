import { connect } from 'react-redux';
import { compose } from 'redux';
import { ConfirmationDialog } from 'components/groups/Dialogs';
import { withShortcut } from 'hoc/dialogShortcut';

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    onConfirm,
    onClose
  } = ownProps;

  const onValidate = async () => {
    await onConfirm(true);
    await onClose();
  };

  return {
    onValidate,
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};

const enhance = compose(
  connect(null, null, mergeProps),
  withShortcut()
);

export default enhance(ConfirmationDialog);
