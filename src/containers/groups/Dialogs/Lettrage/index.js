import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { LettrageDialog } from 'components/groups/Dialogs';
import _ from 'lodash';
import {
  lettrageValidation
} from 'helpers/validation';
import { reduxForm } from 'redux-form';
import {
  getSuggestion as getSuggestionThunk,
  pushLettering as pushLetteringThunk
} from 'redux/lettrage';


const mapStateToProps = state => ({
  accountID: _.get(state.form[`${state.navigation.id}consultingFilter`], 'values.Account.account_id', null),
  letter: _.get(state.lettrage.suggestion, 'letter'),
  letterFetched: _.get(state.lettrage.suggestion, 'fetched'),
  letterFetching: _.get(state.lettrage.suggestion, 'fetching'),
  Lettrage: _.get(state.form, 'Lettrage', ''),
  societyId: state.navigation.id,
  selection: _.get(state.consulting, `${state.navigation.id}.select`, [])
});

const mapDispatchToProps = dispatch => ({
  getSuggestion: accountID => dispatch(getSuggestionThunk(accountID)),
  push: (society_id, lettering, lines, accountID) => dispatch(pushLetteringThunk(society_id, lettering, lines, accountID)) //eslint-disable-line
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { getSuggestion, push } = dispatchProps;
  const {
    accountID,
    selection,
    Lettrage,
    societyId
  } = stateProps;
  const { lettering, onClose, handleSubmit } = ownProps;
  const pushLettering = () => (lettering
    ? push(societyId, _.get(Lettrage, 'values.value', ''), selection[accountID], accountID)
    : push(societyId, '', selection[accountID], accountID));
  const validate = async () => {
    await pushLettering();
    onClose();
  };

  return ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    getLetter: () => getSuggestion(accountID),
    entry: _.get(selection, `${accountID}`, []),
    onValidate: handleSubmit(validate)
  });
};

const enhance = compose(
  reduxForm({
    form: 'Lettrage',
    destroyOnUnmount: false,
    enableReinitialize: true,
    initialValues: { value: '' },
    validate: lettrageValidation
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  withShortcut()
);

export default enhance(LettrageDialog);
