import { connect } from 'react-redux';
import { ConfirmationDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';

const mapStateToProps = state => ({
  isOpen: state.autoReverseDialog.isOpen
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    onValidate
  } = ownProps;

  const buttonsLabel = {
    cancel: 'Annuler',
    validate: 'Valider'
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    buttonsLabel,
    title: I18n.t('newAccounting.autoReverseDialog.title'),
    message: I18n.t('newAccounting.autoReverseDialog.message'),
    onValidate
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ConfirmationDialog);
