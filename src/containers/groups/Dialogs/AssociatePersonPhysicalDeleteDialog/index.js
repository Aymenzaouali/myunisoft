import { connect } from 'react-redux';
import { compose } from 'redux';
import { AssociateDeleteDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import _ from 'lodash';
import {
  PHYSICAL_PERSON_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import {
  deleteAssociatePersonPhysical as deleteAssociatePersonPhysicalThunk
} from 'redux/tabs/companyCreation';
import { withShortcut } from 'hoc/dialogShortcut';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, PHYSICAL_PERSON_TABLE_NAME);
  return {
    selectedPhysicalPerson: _.get(state, `tables.${tableName}.selectedRows`, {}),
    tableName
  };
};

const mapDispatchToProps = dispatch => ({
  deleteAssociate: (
    associate, tableName
  ) => dispatch(deleteAssociatePersonPhysicalThunk(associate, tableName))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedPhysicalPerson,
    tableName
  } = stateProps;
  const {
    onClose
  } = ownProps;
  const {
    deleteAssociate
  } = dispatchProps;
  const title = I18n.t('companyCreation.associates.popUp.title');
  const message = I18n.t('companyCreation.associates.popUp.deleteMessage');

  const deleteAssociates = async () => {
    const associate = await Object.keys(selectedPhysicalPerson).reduce((acc, key) => {
      acc.push({ physical_person_id: parseInt(key, 10) });
      return acc;
    }, []);
    try {
      await deleteAssociate(associate, tableName);
      onClose();
    } catch (error) {
      console.log(error);
    }
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    message,
    title,
    onValidate: deleteAssociates
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(AssociateDeleteDialog);
