import { connect } from 'react-redux';
import { SuccessfulDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import { closeSuccessDialog } from 'redux/successDialog/action';
import { withShortcut } from 'hoc/dialogShortcut';
import { compose } from 'redux';

const mapStateToProps = state => ({
  isOpen: state.successDialog.isOpen
});

const mapDispatchToProps = dispatch => ({
  onClose: payload => dispatch(closeSuccessDialog(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const title = 'successDialog.header';
  const message = I18n.t('successDialog.message');

  return {
    title,
    message,
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(SuccessfulDialog);
