import { connect } from 'react-redux';
import { compose } from 'redux';
import { getFormValues, reduxForm, reset } from 'redux-form';
import _ from 'lodash';

import { withShortcut } from 'hoc/dialogShortcut';
import { PlanDialog } from 'components/groups/Dialogs';
import { planFormValidate } from 'helpers/validation';

import {
  createPlan as createPlanThunk,
  modifyPlan as modifyPlanThunk
} from 'redux/accountingPlans';
import { handleServerMessage } from 'helpers/error';

const mapStateToProps = state => ({
  newPlan: getFormValues('planForm')(state),
  selectedPlan: _.get(state, 'accountingPlans.selectedPlan', null)
});

const mapDispatchToProps = dispatch => ({
  createPlan: newPlan => dispatch(createPlanThunk(newPlan)),
  modifyPlan: planId => dispatch(modifyPlanThunk(planId)),
  _resetForm: form => dispatch(reset(form))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { selectedPlan, newPlan } = stateProps;
  const { modifyPlan, createPlan } = dispatchProps;
  const {
    modify,
    handleSubmit,
    onClose,
    reset
  } = ownProps;

  const closeDialog = () => {
    onClose();
    reset();
  };

  const onSubmit = async () => {
    try {
      if (modify) {
        if (!selectedPlan) return;
        await modifyPlan(selectedPlan);
      } else {
        await createPlan(newPlan);
      }
      closeDialog();
    } catch (error) {
      handleServerMessage(error);
      throw error;
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    modify,
    onValidate: handleSubmit(onSubmit),
    onClose: closeDialog
  };
};

const enhance = compose(
  reduxForm({
    form: 'planForm',
    destroyOnUnmount: false,
    enableReinitialize: true,
    validate: planFormValidate
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(PlanDialog);
