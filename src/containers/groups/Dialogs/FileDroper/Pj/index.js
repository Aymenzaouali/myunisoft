import { connect } from 'react-redux';
import { getFormValues, change } from 'redux-form';
import _ from 'lodash';

import FilesDropperDialog from 'components/groups/Dialogs/FilesDropper';

// Component
const mapStateToProps = (state, own) => {
  const entries = getFormValues(`${state.navigation.id}newAccounting`)(state);

  return {
    societyId: state.navigation.id,
    files: _.get(entries, `${own.field}.pj_list`, [])
  };
};

const mapDispatchToProps = dispatch => ({
  changeValue: (form, field, value) => dispatch(change(form, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId, files: initials } = stateProps;
  const { field } = ownProps;
  const { changeValue } = dispatchProps;

  const formName = `${societyId}newAccounting`;

  const onAddFiles = (files) => {
    changeValue(formName, `${field}.pj_list`, files);
    changeValue(formName, `${field}.delete_list`, initials.filter(f => !files.includes(f)));
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onAddFiles
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FilesDropperDialog);
