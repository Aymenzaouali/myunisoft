import { connect } from 'react-redux';
import { compose } from 'redux';
import { CopyDocumentToDiscussion } from 'components/groups/Dialogs';
import { withShortcut } from 'hoc/dialogShortcut';
import { buildFolderStructure } from 'containers/groups/DocManagement/DMSidebar/folderStructureBuilder';
import {
  getFolders
} from 'common/redux/discussions';
import actions from '../../../../redux/docManagement/actions';

const mapStateToProps = (state) => {
  const { id: societyId } = state.navigation;
  const { folders } = state.discussions;
  return {
    folders,
    societyId,
    selectedDocument: state.docManagement.selectedDocument
  };
};

const mapDispatchToProps = dispatch => ({
  getDiscussionFolders: () => dispatch(getFolders()),
  showSendInDiscussionModal: status => dispatch(
    actions.showSendInDiscussionModal(status)
  ),
  setDocumentToCopy: docInfo => dispatch(
    actions.setDocumentToCopy(docInfo)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { folders, societyId } = stateProps;
  const { showSendInDiscussionModal, setDocumentToCopy } = dispatchProps;
  const { history } = ownProps;

  const onRoomChosen = (location) => {
    history.push(`/tab/${societyId}/discussions`);
    setDocumentToCopy({ location });
    showSendInDiscussionModal(false);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    tree: buildFolderStructure(folders),
    onRoomChosen
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(CopyDocumentToDiscussion);
