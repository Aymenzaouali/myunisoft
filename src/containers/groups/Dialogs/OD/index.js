import { compose } from 'redux';
import moment from 'moment';
import { connect } from 'react-redux';
import { reduxForm, change, getFormValues } from 'redux-form';

import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { ODFormValidation } from 'helpers/validation';
import { formatNumber } from 'helpers/number';

import { getDiaries as getDiariesThunk } from 'redux/diary';
import { closeDialog } from 'redux/dialogs';

import { ODDialog } from 'components/groups/Dialogs';

// Component
const mapStateToProps = state => ({
  societyId: state.navigation.id,
  ODvalues: getFormValues('ODDialogForm')(state) || {}
});
const mapDispatchToProps = dispatch => ({
  initODForm: data => dispatch(autoFillFormValues('ODDialogForm', data)),
  changeODForm: (field, value) => dispatch(change('ODDialogForm', field, formatNumber(value))),
  closeODDialog: dialogProps => dispatch(closeDialog(dialogProps)),
  getDiaries: (opts, societyId) => dispatch(getDiariesThunk(opts, societyId))
});
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { notLetter, selectedAccount, societyId } = ownProps;
  const { initODForm: _initODForm, closeODDialog, getDiaries } = dispatchProps;

  const initODForm = async () => {
    const OD_diary = await getDiaries({ code: '20', type: 'OD' }, societyId);
    const { diary_id, name } = OD_diary[0];
    _initODForm({
      date: moment().format('YYYY-MM-DD'),
      amount_0: formatNumber(notLetter),
      account_id_0: selectedAccount,
      diary: { diary_id, value: diary_id, label: name },
      amount_total: formatNumber(notLetter)
    });
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initODForm,
    onClose: closeODDialog,
    onValidate: payload => console.log("Génération de l'OD", payload) // TODO: implementing back request
  };
};

// Enhance
const enhance = compose(
  reduxForm({
    form: 'ODDialogForm',
    validate: ODFormValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
);
export default enhance(ODDialog);
