import { connect } from 'react-redux';
import { compose } from 'redux';
import { ChangeAccount } from 'components/groups/Dialogs';
import {
  reduxForm,
  formValueSelector
} from 'redux-form';
import _ from 'lodash';
import { withShortcut } from 'hoc/dialogShortcut';

import {
  changeAccount as changeAccountThunk
} from 'redux/consulting';

const mapStateToProps = (state, { societyId }) => ({
  account: formValueSelector(`${societyId}consultingFilter`)(state, 'Account'),
  currentAccount: formValueSelector('changeAccount')(state, 'account')
});

const mapDispatchToProps = dispatch => ({
  changeAccount: (societyId, entry_id, line_entry) => {
    dispatch(changeAccountThunk(societyId, entry_id, line_entry));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    account,
    currentAccount
  } = stateProps;

  const {
    changeAccount
  } = dispatchProps;

  const {
    data,
    onClose,
    societyId
  } = ownProps;

  const onValidate = async () => {
    const currentAccountId = _.get(currentAccount, 'account_id');
    const selectedAccountId = _.get(account, 'account_id');
    if (currentAccountId !== selectedAccountId) {
      try {
        await changeAccount(societyId, currentAccountId, data);
        onClose();
      } catch (error) {
        console.log(error); // Toast maybe ?
      }
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initialValues: {
      account
    },
    onValidate
  };
};

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    form: 'changeAccount',
    enableReinitialize: true
  }),
  withShortcut()
);

export default enhance(ChangeAccount);
