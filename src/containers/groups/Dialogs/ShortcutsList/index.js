import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { ShortcurtsListDialog } from 'components/groups/Dialogs';
import { push } from 'connected-react-router';
import { getCorrespByKey } from 'helpers/shortcuts';
import keymap from 'assets/config/shortcuts/keymap';
import I18n from 'assets/I18n';

const mapStateToProps = state => ({
  currentScreen: state.navigation.routeKey
});

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    currentScreen
  } = stateProps;

  const macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'];
  const userPlatform = window.navigator.platform;
  const shortcutName = getCorrespByKey(currentScreen);

  const allShortcuts = Object.entries(keymap);

  let shortcutsList = {
    name: 'empty',
    number: 0,
    shortcuts: [{}]
  };

  allShortcuts.forEach((oneShortcut) => {
    const [
      key,
      shortcutsKeys
    ] = oneShortcut;

    let shortcuts = [];

    const index = oneShortcut.findIndex(key => key === shortcutName.value);

    if (index !== -1) {
      const shortcutsByItem = shortcutsKeys;

      Object.entries(shortcutsByItem).forEach((shortcut) => {
        const [
          name,
          combination
        ] = shortcut;

        shortcuts = [...shortcuts, { name, combination }];
      });

      String.prototype.allReplace = function (obj) { //eslint-disable-line
        let retStr = this;
        for (const x in obj) { //eslint-disable-line
          retStr = retStr.replace(new RegExp(x, 'g'), obj[x]);
        }
        return retStr;
      };
      shortcutsList = {
        name: key,
        number: shortcuts.length,
        shortcuts: shortcuts.map((label) => {
          const { name, combination } = label;
          const formattedCombination = combination => combination.allReplace({
            minus: '"-"',
            plus: '"+"',
            up: I18n.t('shortcuts.arrow_up'),
            down: I18n.t('shortcuts.arrow_down')
          });
          if (typeof combination === 'string') {
            return {
              name,
              combination: formattedCombination(combination)
            };
          }
          if (macosPlatforms.includes(userPlatform)) {
            return {
              name,
              combination: formattedCombination(combination.osx)
            };
          }
          return {
            name,
            combination: formattedCombination(combination.windows)
          };
        })
      };
    }
  });

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    shortcutsList,
    nameShortcut: 'NEW_ACCOUNTING',
    hiddenShortCuts: [
      'NEW_ACCOUNTING.EQUAL',
      'NEW_ACCOUNTING.PLUS',
      'NEW_ACCOUNTING.MINUS',
      'NEW_ACCOUNTING.MULTIPLE',
      'NEW_ACCOUNTING.DIVIDE',
      'NEW_ACCOUNTING.ENTER'
    ]
  };
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(ShortcurtsListDialog);
