import { connect } from 'react-redux';
import { compose } from 'redux';
import { NewPasswordDialog } from 'components/groups/Dialogs';
import _ from 'lodash';
import { reduxForm } from 'redux-form';
import { changePassword } from 'common/redux/profile';
import { newPasswordValidation } from 'helpers/validation';
import { withShortcut } from 'hoc/dialogShortcut';

const mapStateToProps = state => ({
  error: _.get(state.form.NewPassword, 'error', null)
});

const mapDispatchToProps = dispatch => ({
  changePassword: payload => dispatch(changePassword(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { changePassword } = dispatchProps;

  const change = async (payload) => {
    const { old, newPassword } = payload;
    try {
      await changePassword({
        old_password: old,
        new_password: newPassword
      });
      return true;
    } catch (error) {
      throw error;
    }
  };

  return ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    changePassword: change
  });
};

const enhance = compose(
  reduxForm({
    enableReinitialize: true,
    form: 'NewPassword',
    validate: newPasswordValidation
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  withShortcut()
);


export default enhance(NewPasswordDialog);
