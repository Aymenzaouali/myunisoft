import GroupsDialog from './Groups';
import SocietiesDialog from './Societies';
import ChangeAccount from './ChangeAccount';
import ChangeDiary from './ChangeDiary';
import CounterPartDialog from './Confirmation/CounterPart';
import MenuDialog from './Menu';
import VATDialog from './VAT';
import PjDialog from './FileDroper/Pj';
import ShortcurtsListDialog from './ShortcutsList';
import LettrageDialog from './Lettrage';
import NewAccountDialog from './Account/NewAccount';
import NewPasswordDialog from './NewPassword';
import AbstractDialog from './AbstractDialogContainer';
import AccountParameters from './AccountParameters';
import DiaryDialog from './Diary';
import ConnectorDialog from './Connector';
import FunctionDialog from './Function';
import FlagDialog from './Flag';
import BISettingsDeleteDialog from './BISettingsDeleteDialog';
import PortfolioDialog from './PortfolioDialog';
import Worksheets from './Worksheets';
import WorksheetsDeleteDialog from './WorksheetsDeleteDialog';
import ReportsAndFormsDialog from './ReportsAndFormsDialog';
import RAFConfirmDialog from './RAFConfirmDialog';
import BISettingsDialog from './BISettingsDialog';
import AccessDashboard from './AccessDashboard';
import AssociatePersonPhysicalDeleteDialog from './AssociatePersonPhysicalDeleteDialog';
import AssociatePersonLegalDeleteDialog from './AssociatePersonLegalDeleteDialog';
import SocietyPPDeleteDialog from './SocietyPPDeleteDialog';
import EmployeePPDeleteDialog from './EmployeePPDeleteDialog';
import SubsidiaryDeleteDialog from './SubsidiaryDeleteDialog';
import FormsAndReportsDeleteDialog from './FormsAndReportsDeleteDialog';
import SettingStandardMailDialog from './SettingStandardMailDialog';
import SettingStandardMailDeleteDialog from './SettingStandardMailDeleteDialog';
import MailParagraphDialog from './MailParagraphDialog';
import MailParagraphDeleteDialog from './MailParagraphDeleteDialog';
import DeleteConfirmation from './DeleteConfirmation';
import SuccessfulDialog from './SuccessfulDialog';
import AccountingFirmDeclareTableDialog from './AccountingFirmDeclareTableDialog';
import PlanDialog from './Plan';
import PlanDetailDialog from './PlanDetail';
import SaleDialog from './SaleDialog';
import EDIConfirmationDialog from './EDIConfirmation';
import PlaquetteTree from './PlaquetteTree';
import CopyDocumentToDiscussion from './CopyDocumentToDiscussion';
import ODDialog from './OD';
import EarlyStartDateModal from './EarlyStartDateModal';

export {
  GroupsDialog,
  SocietiesDialog,
  ChangeDiary,
  CounterPartDialog,
  ChangeAccount,
  DeleteConfirmation,
  MenuDialog,
  VATDialog,
  PjDialog,
  ShortcurtsListDialog,
  LettrageDialog,
  NewAccountDialog,
  NewPasswordDialog,
  AbstractDialog,
  AccountParameters,
  ConnectorDialog,
  FlagDialog,
  DiaryDialog,
  FunctionDialog,
  BISettingsDialog,
  PortfolioDialog,
  BISettingsDeleteDialog,
  Worksheets,
  AccessDashboard,
  AssociatePersonPhysicalDeleteDialog,
  AssociatePersonLegalDeleteDialog,
  SocietyPPDeleteDialog,
  EmployeePPDeleteDialog,
  SubsidiaryDeleteDialog,
  WorksheetsDeleteDialog,
  ReportsAndFormsDialog,
  RAFConfirmDialog,
  SettingStandardMailDialog,
  SettingStandardMailDeleteDialog,
  MailParagraphDialog,
  MailParagraphDeleteDialog,
  FormsAndReportsDeleteDialog,
  SuccessfulDialog,
  AccountingFirmDeclareTableDialog,
  PlanDialog,
  PlanDetailDialog,
  EDIConfirmationDialog,
  PlaquetteTree,
  SaleDialog,
  CopyDocumentToDiscussion,
  ODDialog,
  EarlyStartDateModal
};
