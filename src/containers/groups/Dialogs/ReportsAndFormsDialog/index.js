import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import _ from 'lodash';
import { ReportsAndFormsDialog } from 'components/groups/Dialogs';
import { reduxForm, reset, untouch } from 'redux-form';
import I18n from 'assets/I18n';
import { reportsAndFormsDialogValidation } from 'helpers/validation';
import ReportsAndFormsService from 'redux/reportsAndForms';
import { getSociety } from 'redux/navigation';
import { REPORT_AND_FORMS_TABLE, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import reportsAndFormsActions from 'redux/reportsAndForms/actions';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, REPORT_AND_FORMS_TABLE);
  return {
    societies: _.get(state, 'navigation.societies', []),
    society_id: _.get(state, 'navigation.id', false),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    stateOrForm: state.reportsAndForms.stateOrForm,
    selectedStateOrFormData: _.get(state, `tables.${tableName}.data`, []),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`, 0),
    selectedStateOrForm: _.get(state, `reportsAndForms.selectedStateOrForm[${state.navigation.id}].selectedStateOrForm_list`, {}),
    createdStatesCount: _.get(state, `tables.RAFCreateState-${state.navigation.id}.createdRowsCount`, 0),
    selectedformAndReports: _.get(state, `reportsAndForms.selectedformAndReports[${state.navigation.id}]`, []),
    reportsAndForms_list: _.get(state, `tables.${tableName}.data`, []),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null)
  };
};

const mapDispatchToProps = dispatch => ({
  onSubmitReportAndForm: () => dispatch(ReportsAndFormsService.sendReportAndForm()),
  getSociety: () => dispatch(getSociety()),
  resetForm: form => dispatch(reset(form)),
  getFormsAndReports: () => dispatch(ReportsAndFormsService.getFormsAndReports()),
  getStateOrFormDialogTable: id => dispatch(ReportsAndFormsService.getStateOrFormDialogTable(id)),
  modifyReportAndForm: () => dispatch(ReportsAndFormsService.modifyReportAndForm()),
  setSelectedStateOrFormData: (societyId, selected, selectedStateOrFormId) => dispatch(
    reportsAndFormsActions.setSelectedStateOrFormData(societyId, selected, selectedStateOrFormId)
  ),
  resetSelectedStateOrFormData: societyId => dispatch(
    reportsAndFormsActions.resetSelectedStateOrFormData(societyId)
  ),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  resetCreatedStates: societyId => dispatch(tableActions.deleteRows(`RAFCreateState-${societyId}`))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societies,
    selectedRows,
    selectedStateOrFormData,
    society_id,
    reportsAndForms_list,
    lastSelectedRow
  } = stateProps;
  const { creationMode, onClose } = ownProps;
  const {
    onSubmitReportAndForm,
    getFormsAndReports,
    resetForm,
    modifyReportAndForm,
    setSelectedStateOrFormData,
    resetSelectedStateOrFormData
  } = dispatchProps;

  const onAcceptConfirmDialog = (selectedStateOrFormId) => {
    const selectedStateOrFormData_list = selectedStateOrFormData
      .filter((item, index) => item && selectedRows[index]);
    setSelectedStateOrFormData(society_id, selectedStateOrFormData_list, selectedStateOrFormId);
    untouch('reportsAndFormsForm', 'selectStateOrForm');
  };

  const formatSocietiesForAutoComplete = societies.map(
    society => ({ ...society, label: society.name, value: society.society_id })
  );

  const stateSelection = [
    { label: I18n.t('reportsAndForms.dialog.fromExistingState'), value: 0 },
    { label: I18n.t('reportsAndForms.dialog.newState'), value: 1 }
  ];

  const title = creationMode
    ? I18n.t('reportsAndForms.dialog.createNew')
    : I18n.t('reportsAndForms.dialog.modify');

  const onPostReportAndForm = async () => {
    try {
      if (!creationMode) {
        await modifyReportAndForm();
      } else {
        await onSubmitReportAndForm();
        resetSelectedStateOrFormData(society_id);
      }
      await getFormsAndReports();
      await onClose();
      resetForm('reportsAndFormsForm');
    } catch (error) {
      console.log(error); // eslint-disable-line
    }
  };

  const selectedRow = reportsAndForms_list[lastSelectedRow];
  const initialVal = !creationMode ? {
    ...selectedRow
  } : {};

  // TODO: Create onValidate so the HOC shortcut work
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    stateSelection,
    initialValues: { ...initialVal },
    societies: formatSocietiesForAutoComplete,
    resetSelectedStateOrFormData: () => resetSelectedStateOrFormData(society_id),
    onPostReportAndForm,
    onAcceptConfirmDialog,
    title,
    creationMode
  };
};

const enhance = compose(
  reduxForm({
    form: 'reportsAndFormsForm',
    destroyOnUnmount: false,
    enableReinitialize: true,
    validate: reportsAndFormsDialogValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(ReportsAndFormsDialog);
