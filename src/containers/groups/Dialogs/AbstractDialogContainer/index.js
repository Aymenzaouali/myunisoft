import { connect } from 'react-redux';
import { AbstractDialog } from 'components/groups/Dialogs';
import { closeDialog as closeDialogThunk } from 'redux/dialogs';

const mapStateToProps = state => ({
  dialogs: state.dialogs.elements
});

const mapDispatchToProps = dispatch => ({
  closeDialog: () => dispatch(closeDialogThunk())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AbstractDialog);
