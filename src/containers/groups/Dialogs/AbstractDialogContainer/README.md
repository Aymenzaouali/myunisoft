# Dialog Container
## Description
Connected component for opening a dialog anywhere in the app
It's intended to replace eventually all custom mounted components i.e. `<MyFooDialog />` in random places of the app

## Usage

```javascript
import { openDialog } from 'redux/dialogs';

/**
 * Param interface
 *
 * openDialog({
 *   title: string | JSX.Element;
 *   body: string | JSX.Element;
 *   adaptToContainer?: boolean;
 *   buttons?: InlineButtonProps[];
 * })
 *
 */

// 1. Example usage with custom buttons
dispatch(openDialog({
  title: 'Important',
  body: 'Winter is coming!',
  buttons: [{}, {}]
}));

// 2. Example minimal, no buttons
dispatch(openDialog({
  title: 'The weather outsied is frighthful',
  body: 'But the fire is so delightful'
}));

// 3. Example usage with components for title and body
dispatch(openDialog({
  title: <MyCustomTitle />,
  body: <MyCustomBody/>
}));

// 4. Example with adapting to dialog content instead of having fixed width
dispatch(openDialog({
  title: 'Important',
  body: 'Winter is over!',
  adaptToContainer: true
}));
```
