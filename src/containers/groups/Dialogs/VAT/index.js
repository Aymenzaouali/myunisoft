import { connect } from 'react-redux';
import { compose } from 'redux';
import { withShortcut } from 'hoc/dialogShortcut';
import { VATDialog } from 'components/groups/Dialogs';
import { reduxForm, formValueSelector } from 'redux-form';
import { createVat as createVatThunk } from 'redux/tva';
import { vatFormValidation } from 'helpers/validation';
import _ from 'lodash';

const selector = formValueSelector('vatForm');

const mapStateToProps = (state, ownProps) => ({
  initialValues: {
    code: '',
    ...ownProps.values
  },
  societyId: _.get(state, 'navigation.id'),
  vatSelectedLine: _.get(state.tva, 'vatSelectedLine', 0),
  account_ded: selector(state, 'account_ded'),
  account_coll: selector(state, 'account_coll')
});
const mapDispatchToProps = dispatch => ({
  createVat: (values, methode) => dispatch(createVatThunk(values, methode))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { createVat } = dispatchProps;
  const {
    onClose, modify, reset, handleSubmit
  } = ownProps;
  const { societyId } = stateProps;

  const onSave = async (values) => {
    const vat_param = {
      account_ded_id: _.get(values, 'account_ded.account_id', 0), // TODO: Change id by account_id
      account_coll_id: _.get(values, 'account_coll.account_id', 0), // TODO: Change id by account_id
      vat_id: _.get(values, 'taux.id', 0),
      vat_exigility_id: _.get(values, 'exigility.id', 0),
      vat_type_id: _.get(values, 'type.id', 0),
      code: _.get(values, 'code', 0),
      society_id: societyId,
      vat_param_id: _.get(values, 'vat_param_id', 0)
    };

    try {
      await createVat(vat_param, modify);
      onClose();
    } catch (error) {
      console.log(error);
    }
  };

  const closeDialog = () => {
    onClose();
    reset();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate: handleSubmit(onSave),
    modify,
    onClose: closeDialog
  };
};


const enhance = compose(
  reduxForm({
    form: 'vatForm',
    destroyOnUnmount: false,
    enableReinitialize: true,
    validate: vatFormValidation
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  withShortcut()
);

export default enhance(VATDialog);
