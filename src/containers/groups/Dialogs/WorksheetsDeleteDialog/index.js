import { connect } from 'react-redux';
import { WorksheetsDeleteDialog } from 'components/groups/Dialogs';
import I18n from 'assets/I18n';
import SettingsWorksheetsService from 'redux/settingsWorksheets';

const mapDispatchToProps = dispatch => ({
  deleteWorksheets: () => dispatch(SettingsWorksheetsService.deleteWorksheets())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const title = I18n.t('settingsWorksheets.popUp.attention');
  const massage = I18n.t('settingsWorksheets.popUp.massagePossible');

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    massage,
    title
  };
};

export default connect(null, mapDispatchToProps, mergeProps)(WorksheetsDeleteDialog);
