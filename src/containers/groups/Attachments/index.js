import { connect } from 'react-redux';

import { getCurrentTabState } from 'helpers/tabs';

import {
  getDocuments,
  attachDocuments, detachDocuments,
  deleteDocuments
} from 'redux/tabs/documents';
import {
  changeLimitDocuments, changePageDocuments,
  selectDocument, expandDocument, resetDocumentSelection
} from 'redux/tabs/documents/actions';
import { addManualDocs } from 'redux/tabs/accounting/actions';

import Attachments from 'components/groups/Attachments';

// Component
const mapStateToProps = (state) => {
  const { accounting, documents } = getCurrentTabState(state);

  return {
    docs: documents.docs,
    loading: documents.loading,
    error: documents.error,
    limit: documents.limit,
    rows: documents.rows,
    page: documents.page,
    pages: documents.pages,

    selected: documents.selected,
    attached: accounting.manual_docs,
    expanded: documents.expanded
  };
};

const mapDispatchToProps = dispatch => ({
  getDocuments: (page, limit) => dispatch(getDocuments(page, limit)),
  selectDocument: (document_id, value) => dispatch(selectDocument(document_id, value)),
  expandDocument: (document_id, value) => dispatch(expandDocument(document_id, value)),

  onChangeRowsPerPage: event => dispatch(changeLimitDocuments(event.target.value)),
  onChangePage: page => dispatch(changePageDocuments(page)),

  onJoin: (docs) => {
    dispatch(resetDocumentSelection());
    return dispatch(addManualDocs(docs));
  },
  onAttach: () => dispatch(attachDocuments()),
  onDetach: () => dispatch(detachDocuments()),
  onDelete: () => dispatch(deleteDocuments())
});

export default connect(mapStateToProps, mapDispatchToProps)(Attachments);
