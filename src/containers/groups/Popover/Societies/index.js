import { connect } from 'react-redux';
import { Society } from 'components/groups/Popover';
import actions from 'redux/navigation/actions';
import { push } from 'connected-react-router';
import I18n from 'assets/I18n';
import { getSociety as getSocietyThunk } from 'redux/navigation';
import _ from 'lodash';

const mapStateToProps = state => ({
  tabs: state.navigation.tabs,
  societies: _.get(state, 'navigation.societies', [])
});

const mapDispatchToProps = dispatch => ({
  addTabs: tabs => dispatch(actions.addTabs(tabs)),
  push: path => dispatch(push(path)),
  getSociety: () => dispatch(getSocietyThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    tabs,
    societies
  } = stateProps;

  const {
    addTabs,
    push
  } = dispatchProps;

  const {
    onClose
  } = ownProps;

  const onValidate = async (societies) => {
    const tabs = societies.map(society => ({
      id: society.society_id,
      label: society.name,
      path: `/tab/${society.society_id}/dashBoard`,
      isClosable: true,
      secured: society.secured
    }));
    await addTabs(tabs);
    push(tabs[0].path);
    onClose();
  };

  const filteredSocieties = societies.filter(
    society => !tabs.find(tab => tab.id === society.society_id)
  );

  const search = (event) => {
    const searchWord = event.target.value.toLowerCase();
    return filteredSocieties.filter(
      (e) => {
        const name = e.name.toLowerCase();
        return name.includes(searchWord)
            || name.replace(/\./g, '')
              .includes(searchWord.replace(/\./g, ''));
      }
    );
  };

  const title = I18n.t('Popover.Society.title');
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onValidate,
    search,
    title,
    societies: filteredSocieties
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Society);
