import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { Avatar } from 'components/groups/Popover';
// eslint-disable-next-line import/no-named-as-default
import LoginService from 'common/redux/login';
import { persistor } from 'redux/store';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  pushLogin: () => dispatch(push('/')),
  signOut: () => dispatch(LoginService.signOut())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    pushLogin,
    signOut
  } = dispatchProps;

  const disconnect = async () => {
    try {
      await persistor.purge();
      await pushLogin();
      await signOut();
      return true;
    } catch (error) {
      return error;
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disconnect
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Avatar);
