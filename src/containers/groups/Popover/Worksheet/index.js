import { connect } from 'react-redux';
import { Flag } from 'components/groups/Popover';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    entries: state.accountingWeb.entries,
    societyId
  };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

export default connect(mapStateToProps, mergeProps)(Flag);
