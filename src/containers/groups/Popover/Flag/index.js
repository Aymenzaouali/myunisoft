import { connect } from 'react-redux';
import { change } from 'redux-form';
import { Flag } from 'components/groups/Popover';
import { getWorksheetParam as getWorksheetParamThunk } from 'redux/accounting';
import { getReviews as getReviewsThunk } from 'redux/tabs/dadp';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import moment from 'moment';
import _ from 'lodash';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  flagType: _.get(state, 'accountingWeb.worksheetType.worksheet_types', []).map(e => ({
    code: e.label,
    flag_id: e.id,
    name: e.code
  })),
  linePosition: state.accountingWeb.entryLinePosition
});

const mapDispatchToProps = dispatch => ({
  getWorksheetParam: flagCode => dispatch(getWorksheetParamThunk(flagCode)),
  getDossier: () => dispatch(getReviewsThunk()),
  cancelFlag: (societyId, i) => {
    dispatch(change(`${societyId}newAccounting`, `entry_list[${i}].flags.worksheet`, {}));
    dispatch(change(`${societyId}newAccounting`, `entry_list[${i}].flags.immo`, {}));
    dispatch(change(`${societyId}newAccounting`, `entry_list[${i}].worksheet.worksheet`, {}));
    dispatch(change(`${societyId}newAccounting`, `entry_list[${i}].worksheet.immo`, {}));
  },
  initializeImmoForm: data => dispatch(autoFillFormValues('flagDialogForm', data))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { flagType } = stateProps;
  const { getWorksheetParam, getDossier, initializeImmoForm } = dispatchProps;
  const { onClose } = ownProps;

  const onValidate = async (flag) => {
    if (flag[0].code === 'Immobilisation') {
      await initializeImmoForm({ amortissement: 1, date: moment() });
    } else {
      await getWorksheetParam(flag[0].name);
      await getDossier();
    }
    onClose();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    flags: [
      ...flagType,
      {
        code: 'aucun',
        flag_id: -1,
        name: 'Aucun'
      }
    ],
    onValidate
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Flag);
