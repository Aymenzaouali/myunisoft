import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { allowAccess } from 'helpers/rights';
import NavBar from 'components/groups/NavBar';
import _ from 'lodash';
import actions from 'redux/navigation/actions';
import {
  redirectFromCollab as redirectFromCollabThunk,
  toggleSocietyDialog,
  getSociety as getSocietyThunk
} from 'redux/navigation';
import { getRights } from 'redux/rights';
import I18n from 'assets/I18n';
import { initUnreadMessagesStat } from 'common/redux/discussions';
import { getUnreadMessagesStatMemo } from 'common/redux/discussions/common';


const mapStateToProps = (state) => {
  const activeSocietyId = String(state.navigation.id); // active society id

  // calculate discussions unread messages
  const {
    unreadMessagesOfActiveSociety, unreadMessagesTotal
  } = getUnreadMessagesStatMemo(state);

  return {
    unreadMessagesOfActiveSociety,
    unreadMessagesTotal,
    tabs: state.navigation.tabs,
    routeKey: state.navigation.routeKey,
    societyId: state.navigation.id,
    isOpen: state.navigation.isDialogOpen,
    isDialogOpen: state.navigation.societyDialog.isOpen,
    user: state.login.user,
    rights: state.rights.rights,
    activeSocietyId
  };
};

const mapDispatchToProps = dispatch => ({
  closeTab: id => dispatch(actions.closeTab(id)),
  addTabs: tabs => dispatch(actions.addTabs(tabs)),
  push: (path, state) => dispatch(push(path, state)),
  redirect: route => dispatch(push(route)),
  toggleDialog: (value, route) => dispatch(toggleSocietyDialog(value, route)),
  redirectFromCollab: (route, type, id) => dispatch(redirectFromCollabThunk(route, type, id)),
  getSociety: () => dispatch(getSocietyThunk()),
  getRights: () => dispatch(getRights()),
  initUnreadMessagesStat: () => dispatch(initUnreadMessagesStat())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    tabs, societyId, isOpen, user, isCollab, isDialogOpen,
    unreadMessagesTotal, unreadMessagesOfActiveSociety, rights
  } = stateProps;

  const {
    closeTab, push, toggleDialog, redirectFromCollab
  } = dispatchProps;

  const withCutsRedirect = (route) => {
    if (societyId !== -2) {
      redirectFromCollab(route.path, _.get(route, 'state.type'), societyId);
    } else if (societyId === -2 && !route.isNotCollab) {
      push(route.path);
    } else {
      toggleDialog(true, route);
    }
  };

  const withDefaultTabs = [
    {
      label: user.prenom,
      id: -2,
      path: '/tab/collab/dashBoard',
      isClosable: false
    },
    ...tabs,
    {
      label: '+',
      id: '+',
      path: '/dashBoard',
      isClosable: false,
      info: I18n.t('tooltips.newTab')
    }
  ];

  const filteredTabs = withDefaultTabs.map((tab) => {
    if (parseInt(tab.id, 10) === societyId) {
      return { ...tab, isSelected: true };
    }
    return tab;
  });

  const closeTabRedirect = async (id) => {
    const tabIndex = filteredTabs.findIndex(tab => parseInt(tab.id, 10) === parseInt(id, 10));
    const selectedTab = filteredTabs.find(tab => tab.isSelected === true);
    if (selectedTab && parseInt(selectedTab.id, 10) === parseInt(id, 10)) {
      push(filteredTabs[tabIndex - 1].path);
    }
    await closeTab(id);
  };

  const toSocietyDialog = (route) => {
    toggleDialog(!isDialogOpen, route);
  };

  const onCloseDialog = () => {
    toggleDialog(!isOpen);
  };

  const location = _.get(ownProps, 'history.location.pathname');
  const isNewAccountingActive = new RegExp(/accounting\/new/).test(location);
  const isBalanceActive = new RegExp(/current-editions\/balance/).test(location);
  const isUserActive = new RegExp(/UserList/).test(location);
  const isPPActive = new RegExp(/physicalPersons/).test(location);
  const isCompanieActive = new RegExp(/companies/).test(location);
  const isCRMActive = isUserActive || isPPActive || isCompanieActive;
  const isOCRActive = new RegExp(/ocr/).test(location);
  const isDiscussionActive = new RegExp(/discussions/).test(location);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    toSocietyDialog,
    tabs: filteredTabs,
    closeTab: closeTabRedirect,
    onClose: onCloseDialog,
    withCutsRedirect,
    navBarButtons: [
      {
        key: 'shortcuts',
        iconName: 'icon-shortcuts1',
        ariaLabel: 'shortcuts',
        collab: isCollab,
        info: I18n.t('tooltips.shortcuts')
      },
      {
        key: 'newAccounting',
        iconName: 'icon-edit',
        ariaLabel: 'delete',
        collab: isCollab,
        info: I18n.t('tooltips.newSeizure'),
        disabled: allowAccess(rights, 'newAccounting'),
        isActive: isNewAccountingActive
      },
      {
        key: 'balance',
        iconName: 'icon-balance',
        ariaLabel: 'balance',
        collab: isCollab,
        info: I18n.t('tooltips.balance'),
        disabled: allowAccess(rights, 'balance'),
        isActive: isBalanceActive
      },
      {
        key: 'companyList',
        iconName: 'icon-crm',
        ariaLabel: 'delete',
        collab: isCollab,
        info: I18n.t('tooltips.listPerson'),
        disabled: allowAccess(rights, 'userList'),
        isActive: isCRMActive
      },
      {
        key: 'ocr',
        iconName: 'icon-scan',
        ariaLabel: 'delete',
        collab: isCollab,
        info: I18n.t('tooltips.sendingNewDoc'),
        disabled: allowAccess(rights, 'ocr'),
        isActive: isOCRActive
      },
      {
        key: 'discussionsList',
        iconName: 'icon-exchanges',
        ariaLabel: 'discussions',
        collab: isCollab,
        info: I18n.t('tooltips.exchanges'),
        disabled: allowAccess(rights, 'discussionsList'),
        withLabels: true,
        topLabel: {
          value: unreadMessagesOfActiveSociety,
          routeKey: 'discussionsList',
          fromCollab: false
        },
        bottomLabel: {
          value: unreadMessagesTotal,
          routeKey: 'discussionsList',
          fromCollab: true
        },
        fontSize: '32px',
        isActive: isDiscussionActive
      },
      {
        key: 'linkToSupport',
        iconName: 'icon-support',
        ariaLabel: 'support',
        collab: isCollab,
        info: I18n.t('tooltips.support')
      }
      // {
      //   key: undefined,
      //   iconName: 'icon-stop-time',
      //   ariaLabel: 'delete',
      //   collab: isCollab,
      //   info: I18n.t('tooltips.stopTime')
      // }
    ]
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(NavBar);
