import { openDialog, closeDialog as closeDialogThunk } from 'redux/dialogs';
import { connect } from 'react-redux';
import TextEditor from 'components/groups/TextEditor';

const mapDispatchToProps = dispatch => ({
  openConfirmationDialog: dialogProps => dispatch(openDialog(dialogProps)),
  closeDialog: () => dispatch(closeDialogThunk())
});

export default connect(
  undefined,
  mapDispatchToProps
)(TextEditor);
