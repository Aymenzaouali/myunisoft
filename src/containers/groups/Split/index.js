import { compose } from 'redux';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import _ from 'lodash';

import { openerWrap } from 'context/OpenerContext';
import { windowWrap } from 'context/WindowContext';
import { getCurrentTabState } from 'helpers/tabs';
import { SPLIT_INACTIVE } from 'redux/tabs/split/constants';
import {
  activateSplit,
  swapSplit, updateArgsSplit,
  openPopupSplit, closePopupSplit,
  closeSplit
} from 'redux/tabs/split/actions';

import Split from 'components/groups/Split';

// Component
const buildWindowId = (state, ownProps) => {
  const { master_id } = ownProps;
  const { navigation: { id: tab_id } } = state;

  return {
    window_id: `${tab_id}_split_${master_id}`
  };
};

const mapStateToProps = (state, ownProps) => {
  const { opener: { isPopup }, master_id, default_args = {} } = ownProps;
  const { navigation: { id: tab_id } } = state;

  const tabstate = getCurrentTabState(state);
  const { [master_id]: split, ...others } = tabstate.split;
  if (split) {
    split.args = { ...default_args, ..._.get(split, 'args', {}) };
  }

  return {
    tab_id,
    tab_label: _.get(state.navigation.tabs.find(tab => (parseInt(tab.id, 10) === tab_id)), 'label'),
    active: _.get(split, 'active', SPLIT_INACTIVE),
    swapped: _.get(split, 'swapped', false),
    popup: _.get(split, 'popup', false),
    popuped: isPopup,
    args: _.get(split, 'args', {}),
    active_slaves: _.map(others, 'active')
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { opener, windowState, master_id } = ownProps;
  const sendAction = opener.isPopup ? opener.sendAction : windowState.sendAction;

  const dispachAndSend = (action) => {
    sendAction(action);
    return dispatch(action);
  };

  return {
    push: url => dispatch(push(url)),
    onSwapSplit: args => dispachAndSend(swapSplit(master_id, args)),
    updateArgsSplit: args => dispachAndSend(updateArgsSplit(master_id, args)),
    onCloseSplit: () => dispachAndSend(closeSplit(master_id)),

    onActivateSplit: (slave, args, swapped = false) => dispatch(activateSplit(master_id, slave, swapped, args)), // eslint-disable-line max-len
    openPopupSplit: () => dispatch(openPopupSplit(master_id)),
    closePopupSplit: () => dispatch(closePopupSplit(master_id))
  };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { opener: { isPopup, message, to }, window_id, slaves } = ownProps;
  const { active, active_slaves } = stateProps;
  const { onActivateSplit, onCloseSplit } = dispatchProps;

  if (isPopup && active === SPLIT_INACTIVE) {
    if (message.target === 'split-slave' && to === window_id) {
      setTimeout(() => {
        onActivateSplit(message.active, message.args, message.swapped);
      }, 0);
    }
  }

  const closeSlave = () => {
    if (active) {
      onCloseSplit(active);
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,

    closeSlave,
    slaves: _.omit(slaves, active_slaves)
  };
};

// Enhance
const enhance = compose(
  connect(buildWindowId),
  windowWrap(),
  openerWrap,
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(Split);
