import { connect } from 'react-redux';
import ComponentItem from 'components/groups/CompanyItem';
import { push } from 'connected-react-router';
import _ from 'lodash';
import { getCompany as getCompanyThunk } from 'redux/tabs/companyList';
import {
  getExercises as getExercisesThunk,
  getFiscalFolder as getFiscalFolderThunk
} from 'redux/tabs/companyCreation';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import { initialize } from 'redux-form';

const mapStateToProps = state => ({
  tabId: state.navigation.id,
  selectedCompany: _.get(getCurrentTabState(state), 'companyList.selectedCompany')
});


const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  initializeForm: (data, tab) => dispatch(initialize(tabFormName('companyCreationForm', tab), data)),
  getCompany: tabId => dispatch(getCompanyThunk(tabId)),
  getExercises: tabId => dispatch(getExercisesThunk(tabId)),
  getFiscalFolder: tabId => dispatch(getFiscalFolderThunk(tabId))
});

const margeProps = (stateProps, dispatchProps, ownProps) => {
  const { tabId } = stateProps;
  const {
    initializeForm: _initializeForm,
    getCompany,
    getExercises,
    getFiscalFolder
  } = dispatchProps;

  const initializeForm = data => _initializeForm(data, tabId);
  const loadData = async () => {
    await getCompany(tabId);
    await getExercises(tabId);
    await getFiscalFolder(tabId);
  };


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initializeForm,
    loadData
  };
};


export default connect(mapStateToProps, mapDispatchToProps, margeProps)(ComponentItem);
