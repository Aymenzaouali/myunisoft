import { connect } from 'react-redux';
import _ from 'lodash';
import { push } from 'connected-react-router';
import { initialize } from 'redux-form';
import { routesByKey } from 'helpers/routes';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import CompanyList from 'components/groups/CompanyList';
import { selectCompanyAction } from 'redux/tabs/companyList/actions';
import {
  getCompany as getCompanyThunk,
  deleteSociety as deleteSocietyThunk
} from 'redux/tabs/companyList';
import { setDeletionDialog } from 'redux/tabs/crm/actions';
import navigationActions from 'redux/navigation/actions';

const mapStateToProps = state => ({
  tabId: state.navigation.id,
  selectedCompany: _.get(getCurrentTabState(state), 'companyList.selectedCompany'),
  deletionInformation: _.get(getCurrentTabState(state), 'crm.deletionInformation', {})
});

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  initializeForm: (data, tab) => dispatch(initialize(tabFormName('companyCreationForm', tab), data)),
  selectCompany: data => dispatch(selectCompanyAction(data)),
  getCompany: tabId => dispatch(getCompanyThunk(tabId)),
  setDeletionDialog: (mode, id) => dispatch(setDeletionDialog(mode, id)),
  deleteSociety: id => dispatch(deleteSocietyThunk(id)),
  closeTab: id => dispatch(navigationActions.closeTab(id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { tabId } = stateProps;
  const {
    push,
    selectCompany,
    initializeForm: _initializeForm,
    setDeletionDialog,
    deleteSociety,
    closeTab
  } = dispatchProps;

  const initializeForm = data => _initializeForm(data, tabId);

  const onAddNewCompany = async () => {
    /**
     * Ensures that the selected company
     * does not polute new company creation form
     */
    await selectCompany();
    await initializeForm();
    push(routesByKey.companyCreation);
  };

  const deleteAndClose = async (id) => {
    await deleteSociety(id);
    await closeTab(id);
    setDeletionDialog();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initializeForm,
    onAddNewCompany,
    deleteAndClose
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CompanyList);
