import { connect } from 'react-redux';
import { reduxForm, initialize, getFormValues } from 'redux-form';
import _ from 'lodash';
import {
  selectTab,
  createCompany,
  getDataBySiren as getDataBySirenThunk,
  updateExercises as updateExercisesThunk,
  putTaxRecord as putTaxRecordThunk,
  postAssociates as postAssociatesThunk,
  resetFormAndGetCompanyAndResetExercises
} from 'redux/tabs/companyCreation';
import {
  getCompanyList as getCompanyListThunk,
  getCompany as getCompanyThunk
} from 'redux/tabs/companyList';
import { getAccountingPlans as getAccountingPlansThunk } from 'redux/accountingPlans';
import navigationActions from 'redux/navigation/actions';
import loginActions from 'common/redux/login/actions';
import CompanyCreation from 'components/groups/CompanyCreation';
import {
  companyCreationValidation,
  companyCreationAsyncValidation
} from 'helpers/validation';
import {
  PHYSICAL_PERSON_TABLE_NAME,
  LEGAL_PERSON_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import { push } from 'connected-react-router';
import { selectCompanyAction } from 'redux/tabs/companyList/actions';
import actions from 'redux/tabs/companyCreation/actions';
import { routesByKey } from 'helpers/routes';
import { closeCurrentForm } from 'redux/tabs/form/actions';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tabState = getCurrentTabState(state);
  const tableNamePhysicalPerson = getTableName(society_id, PHYSICAL_PERSON_TABLE_NAME);
  const tableNameLegalPerson = getTableName(society_id, LEGAL_PERSON_TABLE_NAME);
  const form = tabFormName('companyCreationForm', state.navigation.id);
  const formValues = getFormValues(form)(state);

  return {
    form,
    formValues,
    activeTab: _.get(tabState, 'companyCreation.selectedTab', 0),
    selectedCompany: _.get(tabState, 'companyList.selectedCompany'),
    existingCompany: _.get(tabState, 'companyCreation.existingCompany'),
    society_id,
    tableNamePhysicalPerson,
    tableNameLegalPerson,
    isEditingPerson: _.get(state, `tables.${tableNamePhysicalPerson}.isEditing`, false),
    isEditingSociety: _.get(state, `tables.${tableNameLegalPerson}.isEditing`, false),
    errorsBackPerson: _.get(state, `tables.${tableNamePhysicalPerson}.errors.0.updateError`),
    errorsBackSociety: _.get(state, `tables.${tableNameLegalPerson}.errors.0.updateError`)
  };
};

const mapDispatchToProps = dispatch => ({
  selectTab: payload => dispatch(selectTab(payload)),
  getCompany: id => dispatch(getCompanyThunk(id)),
  initializeForm: (data = {}, form) => dispatch(initialize(form, data)),
  createCompany: values => dispatch(createCompany(values)),
  selectCompany: () => dispatch(selectCompanyAction()),
  setExistingCompany: society_id => dispatch(actions.setExistingCompany(society_id)),
  getDataBySiren: siren => dispatch(getDataBySirenThunk(siren)),
  updateExercises: () => dispatch(updateExercisesThunk()),
  redirect: route => dispatch(push(route)),
  updateTaxRecord: () => dispatch(putTaxRecordThunk()),
  setCurrentForm: formName => dispatch(navigationActions.setCurrentForm(formName)),
  closeCurrentForm: discardCb => dispatch(closeCurrentForm(discardCb)),
  getCompanyList: payload => dispatch(getCompanyListThunk(payload)),
  updateAssociates: (type, tableName) => dispatch(postAssociatesThunk(type, tableName)),
  updateSocietiesList: newSociety => dispatch(loginActions.updateSocietiesList(newSociety)),
  getAccountingPlans: () => dispatch(getAccountingPlansThunk({ limit: 100 })),
  resetFormAndSetSociety: (formName, society_id) => dispatch(
    resetFormAndGetCompanyAndResetExercises(formName, society_id)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    activeTab,
    selectedCompany,
    form,
    formValues,
    society_id,
    isEditingPerson,
    isEditingSociety,
    tableNamePhysicalPerson,
    tableNameLegalPerson,
    errorsBackPerson,
    errorsBackSociety
  } = stateProps;
  const {
    initializeForm: _initializeForm,
    redirect,
    selectCompany,
    createCompany,
    selectTab,
    closeCurrentForm,
    updateExercises,
    updateTaxRecord,
    getCompanyList,
    updateAssociates,
    updateSocietiesList
  } = dispatchProps;

  const initializeForm = data => _initializeForm(data, form);

  const selectedSocietyId = _.get(selectedCompany, 'society_id', false);
  const siren = _.get(formValues, 'siret', false);

  const isError = (errorsBackPerson !== undefined || errorsBackSociety !== undefined);

  const ensureUpdateAssociate = async (params) => {
    const {
      type = '',
      tableName = ''
    } = params;
    const { response } = await updateAssociates(type, tableName);
    if (response && response.status === 400) {
      throw response.data;
    }
  };

  const validateForm = async (values) => {
    try {
      if (selectedCompany) {
        if (activeTab === 1) { await updateExercises(); }
        if (activeTab === 2) { await updateTaxRecord(); }
        if (activeTab === 3) {
          let params = '';
          if (!isEditingPerson && !isEditingSociety) {
            await ensureUpdateAssociate(params);
          }
          if (isEditingPerson) {
            params = { type: 'person', tableName: tableNamePhysicalPerson };
            await ensureUpdateAssociate(params);
          }
          if (isEditingSociety) {
            params = { type: 'society', tableName: tableNameLegalPerson };
            await ensureUpdateAssociate(params);
          }
        }
      }
      const companyValues = { ...values };
      if (selectedSocietyId) {
        companyValues.society_id = selectedSocietyId;
      }
      const society = await createCompany(companyValues);

      await updateSocietiesList({ id_societe: society.society_id.toString(), name: society.name });
      // If we are in collab tab, need to close modification + redirect to company list
      // Otherwise, company stay the one we are in
      if (society_id === -2) {
        await selectCompany();
        await getCompanyList();
        await redirect(routesByKey.companyList);
      }
    } catch (err) {
      console.log(err); //eslint-disable-line
    }
  };

  const cancelForm = () => closeCurrentForm(
    () => {
      selectCompany();
      initializeForm();
      redirect(routesByKey.companyList);
    }
  );

  const updateForm = data => initializeForm({ ...formValues, ...data });

  const ensureModification = async (values) => {
    if (activeTab === 0) { await createCompany(values); }
    if (activeTab === 1) { await updateExercises(); }
    if (activeTab === 2) { await updateTaxRecord(); }
    if (activeTab === 3) {
      let params = '';
      if (!isEditingPerson && !isEditingSociety) {
        await ensureUpdateAssociate(params);
      }
      if (isEditingPerson) {
        params = { type: 'person', tableName: tableNamePhysicalPerson };
        await ensureUpdateAssociate(params);
      }
      if (isEditingSociety) {
        params = { type: 'society', tableName: tableNameLegalPerson };
        await ensureUpdateAssociate(params);
      }
    }
  };

  const goToStep = async (values, id) => {
    try {
      await ensureModification(values);
      selectTab(id);
    } catch (err) {
      throw err;
    }
  };

  const nextStep = async (values) => {
    try {
      await ensureModification(values);
      selectTab(activeTab + 1);
    } catch (err) {
      console.log(err); //eslint-disable-line
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initializeForm,
    updateForm,
    cancelForm,
    validateForm,
    nextStep,
    goToStep,
    society_id,
    siren,
    isError
  };
};

const withForm = reduxForm({
  destroyOnUnmount: false,
  validate: companyCreationValidation,
  asyncValidate: companyCreationAsyncValidation,
  asyncBlurFields: ['siret'],
  initialValues: {
    country: 'France'
  }
})(CompanyCreation);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(withForm);
