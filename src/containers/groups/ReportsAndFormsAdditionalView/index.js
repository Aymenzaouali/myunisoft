import ReportsAndFormsAdditionalView from 'components/groups/ReportsAndFormsAdditionalView';
import { connect } from 'react-redux';
import actions from 'redux/reportsAndForms/actions';

const mapDispatchToProps = dispatch => ({
  closeAdditionalReportsAndForms: () => dispatch(
    actions.setAdditionalReportsAndForms(false)
  )
});


export default connect(null, mapDispatchToProps)(ReportsAndFormsAdditionalView);
