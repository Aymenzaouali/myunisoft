import { connect } from 'react-redux';
import Progression from 'components/groups/Progression';

const mapStateToProps = () => ({
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Progression);
