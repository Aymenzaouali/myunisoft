import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import {
  reduxForm, change, reset,
  getFormValues
} from 'redux-form';
import _ from 'lodash';
import moment from 'moment';
import I18n from 'assets/I18n';

import { formatEntry } from 'helpers/format';
import { routesByKey } from 'helpers/routes';
import { getCurrentTabState } from 'helpers/tabs';
import { newAccountingValidation } from 'helpers/validation';
import { Notifications } from 'common/helpers/SnackBarNotifications';
import { autoFillFormValues } from 'helpers/autoFillFormValues';

import {
  createEntry as createEntryThunk,
  modifyEntry as modifyEntryThunk,
  counterPartLines as counterPartLinesThunk,
  getWorksheetType as getWorksheetTypeThunk
} from 'redux/accounting';
import actions from 'redux/accounting/actions';
import { setCurrentForm } from 'redux/tabs/form/actions';
import { enqueueSnackbar as enqueueSnackbarAction } from 'common/redux/notifier/action';

import NewAccountingScreen from 'components/groups/NewAccounting';
import { closeAutoReverseDialog } from 'redux/autoReverseDialog/actions';
import { confirmed } from 'redux/autoReverseDialog';
import { getDiaryDetail } from 'redux/diary';

import chartAccountActions from 'redux/chartAccounts/actions';
import { getAccountDetail as getAccountDetailThunk } from 'redux/chartAccounts';

// Component
const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const { accounting } = getCurrentTabState(state);
  const formName = `${state.navigation.id}newAccounting`;
  const { entries: { [societyId]: entryArray } } = state.accountingWeb;
  return ({
    lastEntry: _.get(entryArray, 'entry_array[0]'),
    manualDocs: accounting.manual_docs,
    filters: getFormValues(`${state.navigation.id}accountingFilter`)(state),
    entryArray: _.get(entryArray, 'entry_array', []),
    societyId: state.navigation.id,
    form: formName,
    formValues: getFormValues(formName)(state),
    initialValues: {
      pj_list: [],
      entry_list: [{
        date_entry: moment().format('YYYY-MM-DD')
      }]
    },
    diaries: state.accountingWeb.diaries || []
  });
};
const mapDispatchToProps = dispatch => ({
  createEntry: entry => dispatch(createEntryThunk(entry)),
  changeFormValue: (form, field, value) => dispatch(change(form, field, value)),
  resetForm: form => dispatch(reset(form)),
  modifyEntry: (entry, formName) => dispatch(modifyEntryThunk(entry, formName)),
  counterPartLines: payload => dispatch(counterPartLinesThunk(payload)),
  enableCreateCounterPartConfirmation: (enable) => {
    dispatch(actions.enableCreateCounterPartConfirmation(enable));
  },
  enableLinkDiaryAccountPermission: (enable) => {
    dispatch(actions.enableLinkDiaryAccountPermission(enable));
  },
  enableCounterPartInformation: (enable) => {
    dispatch(actions.enableCounterPartInformation(enable));
  },
  push: path => dispatch(push(path)),
  setCurrentForm: (name, initial) => dispatch(setCurrentForm(name, initial, ['day', 'month', 'year', 'diary'])),
  getWorksheetType: () => dispatch(getWorksheetTypeThunk()),
  enqueueSnackbar: notification => dispatch(enqueueSnackbarAction(notification)),
  initializeEntryForm: (form, data) => dispatch(autoFillFormValues(form, data)),
  confirmed: (entry, formName) => dispatch(confirmed(entry, formName)),
  closeDialog: () => dispatch(closeAutoReverseDialog()),
  getDiaryDetail: diaryId => dispatch(getDiaryDetail(diaryId)),
  getAccountDetail: () => dispatch(getAccountDetailThunk()),
  selectAccount: accountSelected => dispatch(chartAccountActions.selectAccount(accountSelected))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    filters,
    manualDocs,
    form,
    initialValues,
    formValues
  } = stateProps;

  const {
    createEntry,
    resetForm,
    modifyEntry,
    changeFormValue,
    confirmed,
    enqueueSnackbar,
    initializeEntryForm
  } = dispatchProps;

  const { filterType, master_id, args } = ownProps;

  const initFieldForBankLinkSplit = async () => {
    const { diary } = args;
    const { account } = diary;

    const month = moment().format('MMMM');
    const monthLabel = `${month.charAt(0).toLocaleUpperCase()}${month.substring(1)}`;

    const data = {
      diary: { ...diary },
      month: { label: monthLabel, value: moment().format('MM') },
      year: { label: moment().format('YYYY'), value: moment().format('YYYY') },
      entry_list: [
        {
          account: {
            ...account,
            account_id: account.id,
            account_number: account.number,
            value: account.number
          }
        }
      ]
    };

    await resetForm(form);
    await initializeEntryForm(form, data);
  };

  const typeEntry = filterType || _.get(filters, 'type', 'e');

  const disableAutocompleteCounterpart = () => {
    const entry_list = _.get(formValues, 'entry_list') || [];

    if (entry_list.length > 1 || !_.get(entry_list[0], 'account')) {
      return true;
    }

    return false;
  };

  const isModification = _.get(formValues, 'entry_id');

  const onSubmitEntry = async (payload, callback) => {
    const entry = await formatEntry(payload, societyId, manualDocs);
    const {
      diary, day, month, year
    } = entry;

    const diaryAccount = _.get(diary, 'account');

    const entry_list = master_id === 'bankLink'
      ? [{
        account: {
          ...diaryAccount,
          account_id: diaryAccount && diaryAccount.id,
          account_number: diaryAccount && diaryAccount.number
        },
        date_entry: moment().format('YYYY-MM-DD')
      }]
      : [{ date_entry: moment().format('YYYY-MM-DD') }];

    if (typeEntry === 'm' && _.isEmpty(entry.pj_list)) {
      changeFormValue(form, 'type', 'e');
      enqueueSnackbar(Notifications(I18n.t('errors.ArgumentError.no_pj'), 'warning'));
    }
    try {
      if (entry.entry_id) {
        await modifyEntry(entry, `${societyId}accountingFilter`, typeEntry);
      } else {
        const dateStart = moment(`${year.value}${month.value}${day}`).startOf('month');
        const dateEnd = moment(`${year.value}${month.value}${day}`).endOf('month');
        await changeFormValue(`${societyId}accountingFilter`, 'diary', diary);
        await changeFormValue(`${societyId}accountingFilter`, 'dateStart', dateStart);
        await changeFormValue(`${societyId}accountingFilter`, 'dateEnd', dateEnd);
        await createEntry(entry);
      }
      resetForm(form);
      initializeEntryForm(form, {
        diary, day, month, year, entry_list
      });
      callback();
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
    return true;
  };

  const onSubmitAutoReverseDialog = async (payload) => {
    const entry = await formatEntry(payload, societyId, manualDocs);
    await confirmed(entry, `${societyId}accountingFilter`);
    resetForm(form);
    setCurrentForm(form, initialValues);
  };

  const onValidateFlux = async (payload) => {
    const entry = await formatEntry(payload, societyId);
    const {
      diary, day, month, year
    } = entry;
    try {
      if (!entry.entry_id) {
        await changeFormValue(`${societyId}accountingFilter`, 'type', 'e');
      }
      await createEntry(entry);
      resetForm(form);
      initializeEntryForm(form, {
        diary, day, month, year
      });
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
    return true;
  };

  const redirectChartAccount = () => {
    window.open(routesByKey.chartAccountTva.replace(':id', societyId), `window${new Date().getTime()}`, `width=${window.innerWidth - 200},height=${window.innerHeight - 200}`);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmitEntry,
    typeEntry,
    onValidateFlux,
    redirectChartAccount,
    onSubmitAutoReverseDialog,
    disableAutocompleteCounterpart,
    isModification,
    initFieldForBankLinkSplit
  };
};

const WrappedFormNewAccounting = reduxForm({
  destroyOnUnmount: false,
  validate: newAccountingValidation
})(NewAccountingScreen);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WrappedFormNewAccounting);
