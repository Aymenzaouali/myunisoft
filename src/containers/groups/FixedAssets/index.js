import { connect } from 'react-redux';
import _, { identity, pickBy, get as _get } from 'lodash';
import FixedAssetsScreen from 'components/screens/FixedAssets';
import asyncActions from 'redux/fixedAssets';
import actions from 'redux/fixedAssets/actions';
import { push } from 'connected-react-router';
import { routesByKey } from 'helpers/routes';
import { removeRedirectLink } from 'redux/tabs/dadp/actions';
import { initialize, reset } from 'redux-form';
import { openDialog } from 'redux/dialogs';
import I18n from 'assets/I18n';
import {
  getReviews as getReviewsThunk,
  getDiligences,
  getDilligenceIdByDossierRevision,
  sendDiligencePJ
} from 'redux/tabs/dadp';
import DiligenceCommentBox from 'containers/groups/Comments/Box/FixedAssetsDiligence';
import tableActions from 'redux/tables/actions';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
// TODO Remove unnecessary imports
// import { WorksheetCommentBox } from 'containers/groups/Comments';
import { getExercice } from 'redux/accounting';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const diligence = _get(state, `tabs.${societyId}.dadp.redirectedLink`, {});
  const redirected = _get(state, `tabs[${societyId}].dadp.redirectedLink`, {});
  const { end_date = '', start_date = '' } = redirected;
  const dossierRevisionId = _get(state, `tabs.${societyId}.dadp.reviews.selectedId`);
  const reviews = _get(state, `tabs.${societyId}.dadp.reviews.data`, []);
  const isEditing = _get(state, `fixedAssets.${societyId}.isEditing`);
  const review = reviews.find(r => r.id_dossier_revision === dossierRevisionId);
  const selectedAccount = _get(state, `fixedAssets.${societyId}.selectedAccount`);
  const immos = _get(selectedAccount, 'arrayImmo', []);
  const selectedImmos = _get(state, `fixedAssets.${societyId}.selectedImmos`, []);
  const currentImmoModal = immos.filter(item => Object.keys(pickBy(selectedImmos, identity)).includes(`${item.id_immo}`));
  const diligence_id = _get(diligence, 'diligence_id') || _get(state, `tabs.${societyId}.dadp.pages.fixed_assets.id_diligence`, 0);
  const diligences = _get(state, `tabs.${societyId}.dadp.diligences.data[0].children`, []);
  const currentDiligence = diligences.length
    ? diligences.find(diligence => diligence.diligence_id === diligence_id) : {};
  const headerPjList = _get(currentDiligence, 'pj_list', []);
  const isSelectedImmoHasEntry = currentImmoModal.every(immo => immo.hasEntry);
  const valideStatus = _get(state, `tabs.${societyId}.dadp.diligences.data[0].children`, []).find(child => child.diligence_id === diligence_id);
  const exercices = _get(state, `accountingWeb.exercice.${societyId}.exercice`, []);
  const editedImmos = _get(state, `fixedAssets.${societyId}.editedImmos`, {});
  const isEditedImmosGenerated = Object.keys(editedImmos).some(
    editedImmo => editedImmos[editedImmo] && !!editedImmos[editedImmo].hasEntry
  );
  const commentCount = _get(state, `fixedAssets.${societyId}.comments.diligence.data.data`, []).length;
  const exportAdditionalTableName = `${I18n.t('fixedAssets.title')}_detail`;
  const sales = _get(state, `fixedAssets.${societyId}.sales.data`);
  const isValidedSales = _get(state, `fixedAssets.${societyId}.sales.isValided`, false);
  const selectedSale = _get(state, `fixedAssets.${societyId}.sales.selectedSale`);

  return {
    diligence,
    societyId,
    isEditedImmosGenerated,
    currentCompanyName: _.get(currentTab, 'label'),
    exportAdditionalTableName,
    isSelectedImmoHasEntry,
    diligence_id,
    exercices,
    valideStatus: (valideStatus && valideStatus.valid_collab) === 'ok',
    currentDiligence,
    headerPjList,
    commentCount,
    redirected: !!Object.keys(redirected).length,
    accounts: _get(state, `fixedAssets.${societyId}.accounts`),
    showEarlyStartDateModal: _get(state, `fixedAssets.${societyId}.showEarlyStartDateModal`),
    isLoading: _get(state, `fixedAssets.${societyId}.isLoading`),
    selectedAccount,
    immos,
    selectedImmos,
    currentImmoModal,
    isEditing,
    lastSelectedImmo: _get(state, `fixedAssets.${societyId}.lastSelectedImmo`),
    editedImmos: _get(state, `fixedAssets.${societyId}.editedImmos`),
    createdImmos: _get(state, `fixedAssets.${societyId}.createdImmos`),
    errors: _get(state, `fixedAssets.${societyId}.errors`),
    selectedDotations: _get(state, `fixedAssets.${societyId}.selectedDotations`),
    dossierRevisionId,
    startDate: start_date || _get(review, 'start_date'),
    endDate: end_date || _get(review, 'end_date'),
    formattedAdditionalTableData: _.get(state, `tables.transformedTableData[${exportAdditionalTableName}][${societyId}].params`, {}),
    sales,
    isValidedSales,
    selectedSale
  };
};

const mapDispatchToProps = dispatch => ({
  getAccounts: (societyId, dossierRevisionId) => dispatch(
    asyncActions.getAccounts(societyId, dossierRevisionId)
  ),
  initialImmoModal: data => dispatch(initialize('fixedAssetsModal', data)),
  saveImmos: () => dispatch(asyncActions.saveImmos()),
  resetForm: () => dispatch(reset('fixedAssetsModal')),
  deleteImmos: societyId => dispatch(asyncActions.deleteImmos(societyId)),
  startEdit: societyId => dispatch(actions.startEdit(societyId)),
  toggleEarlyStartDateModal: (status, societyId) => dispatch(
    actions.showEarlyStartDateModal(status, societyId)
  ),
  endEdit: societyId => dispatch(actions.endEdit(societyId)),
  selectAccount: (societyId, account) => dispatch(actions.selectAccount(societyId, account)),
  unselectAccount: societyId => dispatch(actions.unselectAccount(societyId)),
  selectImmo: (societyId, immo) => dispatch(actions.selectImmo(societyId, immo)),
  editImmo: (societyId, immoId, value) => dispatch(actions.editImmo(societyId, immoId, value)),
  setErrors: (societyId, id, errors) => dispatch(actions.setErrors(societyId, id, errors)),
  getDilligenceIdByDossierRevision: (diligence_code, dossier_revision_id, page) => dispatch(
    getDilligenceIdByDossierRevision(diligence_code, dossier_revision_id, page)
  ),
  unselectImmo: (societyId, immo) => dispatch(actions.unselectImmo(societyId, immo)),
  addImmoLine: societyId => dispatch(actions.addImmoLine(societyId)),
  addPJ: (file, immo_id) => dispatch(
    asyncActions.sendImmoPJ(file, immo_id)
  ),
  sendDiligencePJ: (file, diligence_id) => dispatch(sendDiligencePJ(file, diligence_id)),
  selectDotation:
    (societyId, dotation) => dispatch(actions.selectDotation(societyId, dotation)),
  unselectDotation: (societyId, dotation) => dispatch(
    actions.unselectDotation(societyId, dotation)
  ),
  getDossier: () => dispatch(getReviewsThunk()),
  openCommentsDialog: dialogProps => dispatch(openDialog(dialogProps)),
  getExercice: societyId => dispatch(getExercice(societyId)),
  redirect: () => dispatch(push(routesByKey.da_balanceSheet)),
  getGlobalComments: diligence_id => dispatch(asyncActions.getDiligenceComments(diligence_id)),
  removeRedirected: () => dispatch(removeRedirectLink()),
  getComments: immo_id => dispatch(asyncActions.getImmoComments(immo_id)),
  cancelImmos: societyId => dispatch(actions.cancel(societyId)),
  openDialogs: (society_id, dialog) => dispatch(
    actions.openDialog(society_id, dialog)
  ),
  closeDialogs: societyId => dispatch(
    actions.closeDialog((societyId))
  ),
  getDiligences: (category, section_id, cycle_id, start_date, end_date, workSheetOnly) => dispatch(
    getDiligences(category, section_id, cycle_id, start_date, end_date, workSheetOnly)
  ),
  getSale: (immoId, lineId) => dispatch(
    asyncActions.getSale(immoId, lineId)
  ),
  postSale: () => dispatch(asyncActions.postSale()),
  saleModalSubmit: societyId => dispatch(actions.saleModalSubmit(societyId)),
  selectSale: (societyId, sale) => dispatch(actions.selectSale(societyId, sale)),
  unselectSale: societyId => dispatch(actions.unselectSale(societyId)),
  setFixedTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  ),
  setAdditionalTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps) => {
  const {
    societyId, dossierRevisionId, isSelectedImmoHasEntry,
    valideStatus, currentImmoModal, exercices,
    currentCompanyName, exportAdditionalTableName, formattedAdditionalTableData,
    startDate, endDate,
    isEditedImmosGenerated
  } = stateProps;
  const {
    getAccounts,
    deleteImmos,
    startEdit,
    addPJ,
    endEdit,
    selectAccount,
    unselectAccount,
    selectImmo,
    editImmo,
    unselectImmo,
    addImmoLine,
    selectDotation,
    unselectDotation,
    setErrors,
    getDossier,
    getGlobalComments,
    openCommentsDialog,
    getExercice,
    setFixedTableData,
    setAdditionalTableData,
    openDialogs,
    closeDialogs,
    saveImmos,
    sendDiligencePJ,
    saleModalSubmit,
    selectSale,
    unselectSale
  } = dispatchProps;

  const isClosedYear = () => {
    const currentExercices = exercices.length && startDate && endDate
      && exercices.find(
        exercise => exercise.start_date === startDate && exercise.end_date === endDate
      );
    return currentExercices ? currentExercices.closed : false;
  };

  const removeChecker = [
    !isSelectedImmoHasEntry,
    !valideStatus,
    !isClosedYear(),
    !!currentImmoModal.length
  ].every(
    item => item === true
  );

  const loadData = dossierRevisionId
    ? getAccounts.bind(null, societyId, dossierRevisionId)
    : getDossier;

  const sendPjs = (file, id, key) => {
    const PjsKeys = {
      immo: {
        onSend: id => addPJ(file, id)
      },
      diligence: {
        onSend: id => sendDiligencePJ(id, file)
      }
    };
    return PjsKeys[key].onSend(id);
  };

  const getAndOpenComments = async (diligence_id) => {
    if (diligence_id !== undefined) {
      openCommentsDialog({
        body: {
          Component: DiligenceCommentBox,
          props: {
            diligence_id,
            createMode: false,
            getComments: () => getGlobalComments(diligence_id)
          }
        }
      });
    } else {
      openCommentsDialog({
        body: {
          Component: DiligenceCommentBox,
          props: {
            diligence_id,
            createMode: true
          }
        }
      });
    }
  };
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();

  const baseFileName = createFormattedFileName(currentCompanyName, exportAdditionalTableName);
  const additionalDataToExport = formatTableDataToCSVData(formattedAdditionalTableData);

  const onValideteGenerateModify = () => {
    saveImmos();
    closeDialogs(societyId);
  };

  const dialogs = [
    {
      name: 'generatedModify',
      message: I18n.t('fixedAssets.dialogs.generatedModify.message'),
      onClose: closeDialogs.bind(null, societyId),
      onValidate: onValideteGenerateModify
    }
  ];

  const onImmosGenerated = (cb) => {
    if (isEditedImmosGenerated) {
      const generatedModifyModal = dialogs.find(({ name }) => name === 'generatedModify');
      openDialogs(societyId, generatedModifyModal);
    } else {
      (
        cb()
      );
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    onImmosGenerated,
    loadData,
    disabledExport: _.isEmpty(additionalDataToExport),
    csvDataAdditional: additionalDataToExport,
    baseFileName,
    setTableData: params => setFixedTableData(tableName, societyId, params),
    setAdditionalTableData:
        params => setAdditionalTableData(exportAdditionalTableName, societyId, params),
    removeChecker,
    getExercice: getExercice.bind(null, societyId),
    sendPjs,
    getAccounts: getAccounts.bind(null, societyId),
    deleteImmos: deleteImmos.bind(null, societyId),
    startEdit: startEdit.bind(null, societyId),
    endEdit: endEdit.bind(null, societyId),
    selectAccount: selectAccount.bind(null, societyId),
    unselectAccount: unselectAccount.bind(null, societyId),
    selectImmo: selectImmo.bind(null, societyId),
    setErrors: setErrors.bind(null, societyId),
    editImmo: editImmo.bind(null, societyId),
    unselectImmo: unselectImmo.bind(null, societyId),
    addImmoLine: addImmoLine.bind(null, societyId),
    selectDotation: selectDotation.bind(null, societyId),
    unselectDotation: unselectDotation.bind(null, societyId),
    getAndOpenComments,
    saleModalSubmit: saleModalSubmit.bind(null, societyId),
    selectSale: selectSale.bind(null, societyId),
    unselectSale: unselectSale.bind(null, societyId)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(FixedAssetsScreen);
