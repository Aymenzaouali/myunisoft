import { connect } from 'react-redux';

import CurrentEditions from 'components/groups/CurrentEditions';
import { getExercice } from 'redux/accounting';
import { openPopupSplit } from 'redux/tabs/split/actions';
import _ from 'lodash';

const mapStateToProps = state => ({
  currentOption: state.balance.currentOption,
  societyId: _.get(state, 'navigation.id', -2),
  isLoading: state.balance.isLoading
});

const mapDispatchToProps = dispatch => ({
  openPopupSplit: masterId => dispatch(openPopupSplit(masterId)),
  getExercice: societyId => dispatch(getExercice(societyId))
});

export default connect(mapStateToProps, mapDispatchToProps)(CurrentEditions);
