import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';

import { getCurrentTabState } from 'helpers/tabs';
import { getExercises as getExercisesThunk } from 'redux/tabs/exercises';

import CurrentEditionsFilters from 'components/groups/CurrentEditions/CurrentEditionsFilters';

const form = 'currentEditionsFiltersForm';
const formSelector = formValueSelector(form);

const mapStateToProps = (state) => {
  const { exercises } = getCurrentTabState(state);

  return {
    formValues: formSelector(state, 'exercices', 'from', 'to', 'accountNumberType', 'accountNumberValue', 'situationWrites'),
    exercises: exercises ? exercises.map(({ label, start_date, end_date }) => ({
      start_date,
      end_date,
      label,
      value: label
    })) : []
  };
};

const mapDispatchToProps = dispatch => ({
  getExercises: () => dispatch(getExercisesThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { exercises } = stateProps;
  const previousYearExercise = exercises.find(exercise => exercise.label === 'N-1') || {};
  const currentYearExercise = exercises.find(exercise => exercise.label === 'N') || {};

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    destroyOnUnmount: false,
    initialValues: {
      exercices: ['N', 'N-1'],
      from: previousYearExercise.start_date,
      to: currentYearExercise.end_date,
      accountNumberType: 'all',
      accountNumberValue: '',
      situationWrites: false
    }
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  reduxForm({
    form,
    enableReinitialize: true
  })
)(CurrentEditionsFilters);
