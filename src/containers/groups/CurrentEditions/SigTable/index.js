import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import _ from 'lodash';

import { getSigData as getSigDataThunk } from 'redux/balance';

import SigTable from 'components/groups/CurrentEditions/SigTable';

const currentEditionsTableFiltersFormSelector = formValueSelector('currentEditionsTableFiltersForm');
const currentEditionsFiltersFormSelector = formValueSelector('currentEditionsFiltersForm');

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const currentEditionsAccountFilter = formValueSelector(`${societyId}currentEditionsAccountFilter`);
  return {
    isAllMonthVisible: currentEditionsTableFiltersFormSelector(state, 'isAllMonthVisible'),
    exercices: currentEditionsFiltersFormSelector(state, 'exercices'),
    societyId,
    balanceData: _.get(state, 'balance.sig', {}),
    isLoading: _.get(state, 'balance.sig.isLoading', false),
    isError: _.get(state, 'balance.sig.isError', false),
    selectedExercice: currentEditionsAccountFilter(state, 'exercice')
  };
};
const mapDispatchToProps = dispatch => ({
  getSigData: (start_date, end_date) => {
    dispatch(getSigDataThunk(start_date, end_date));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { error } = stateProps;
  const errorMessage = _.get(error, '[0].message');

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    errorMessage
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(SigTable);
