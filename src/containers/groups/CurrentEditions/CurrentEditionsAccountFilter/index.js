import { connect } from 'react-redux';
import { reduxForm, getFormValues } from 'redux-form';
import moment from 'moment';

import CurrentEditionsAccountFilter from 'components/groups/CurrentEditions/CurrentEditionsAccountFilter';

import { autoFillFormValues } from 'helpers/autoFillFormValues';

import {
  getBalanceData as getBalanceDataThunk,
  getSigData as getSigDataThunk
} from 'redux/balance';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;

  return {
    societyId,
    currentOption: state.balance.currentOption,
    form: `${societyId}currentEditionsAccountFilter`,
    accountFilterFormValues: getFormValues(`${societyId}currentEditionsAccountFilter`)(state),
    exercices: _.get(state, `accountingWeb.exercice.${societyId}.exercice`)
  };
};

const mapDispatchToProps = dispatch => ({
  getBalanceData: (filter, start_date, end_date, id_exercice) => {
    dispatch(getBalanceDataThunk(filter, start_date, end_date, undefined, id_exercice));
  },
  getSigData: (start_date, end_date) => {
    dispatch(getSigDataThunk(start_date, end_date));
  },
  initializeDate: (id, dateStart, dateEnd, exercice) => {
    dispatch(autoFillFormValues(`${id}currentEditionsAccountFilter`, { dateStart, dateEnd, exercice }));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    accountFilterFormValues = {},
    exercices = []
  } = stateProps;
  const {
    getBalanceData,
    getSigData
  } = dispatchProps;
  const {
    filter = '', dateStart = '', dateEnd = '', exercice = {}
  } = accountFilterFormValues;

  let exercice_N = exercices.find(ex => ex.label === 'N');
  if (exercice_N) {
    exercice_N = { ...exercice_N, value: exercice_N.label };
  }
  const start_date = (_.get(exercice_N, 'start_date') && moment(exercice_N.start_date, 'YYYY-MM-DD').format('YYYYMMDD')) || '';
  const end_date = (_.get(exercice_N, 'end_date') && moment(exercice_N.end_date, 'YYYY-MM-DD').format('YYYYMMDD')) || '';
  const getTable = () => {
    if (accountFilterFormValues.selectionType === 'AllAccounts') {
      getBalanceData(undefined, dateStart, dateEnd, _.get(exercice, 'id_exercice'));
    } else {
      getBalanceData(filter, dateStart, dateEnd, _.get(exercice, 'id_exercice'));
    }
  };

  const getSigTable = () => {
    const start_date = moment(dateStart).format('YYYYMMDD');
    const end_date = moment(dateEnd).format('YYYYMMDD');
    getSigData(start_date, end_date);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    getTable,
    getSigTable,
    start_date,
    end_date,
    exercice_N,
    initialValues: {
      selectionType: 'AllAccounts'
    }
  };
};

const wrappedWithReduxForm = reduxForm({
  destroyOnUnmount: false
})(CurrentEditionsAccountFilter);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrappedWithReduxForm);
