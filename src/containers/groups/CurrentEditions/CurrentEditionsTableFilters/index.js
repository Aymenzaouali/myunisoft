import { compose } from 'redux';
import { connect } from 'react-redux';
import {
  reduxForm, formValueSelector, getFormValues
} from 'redux-form';
import {
  getBalanceData as getBalanceDataThunk,
  getSigData as getSigDataThunk
} from 'redux/balance';
import CurrentEditionsTableFilters from 'components/groups/CurrentEditions/CurrentEditionsTableFilters';

const form = 'currentEditionsTableFiltersForm';
const formSelector = formValueSelector(form);

const mapStateToProps = state => ({
  currentOption: state.balance.currentOption,
  formValues: formSelector(state, 'isAllMonthVisible', 'isRevisionsHidden'),
  headerValues: getFormValues(`${state.navigation.id}currentEditionsAccountFilter`)(state)
});

const mapDispatchToProps = dispatch => ({
  getBalanceData: (filter, start_date, end_date, cumul) => {
    dispatch(getBalanceDataThunk(filter, start_date, end_date, cumul));
  },
  getSigData: (start_date, end_date, cumul) => {
    dispatch(getSigDataThunk(start_date, end_date, cumul));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  modeTypes: [
    { label: 'Mois', value: 0 },
    { label: 'Cumul', value: 1 }
  ]
});

export default compose(
  reduxForm({
    form,
    destroyOnUnmount: false,
    initialValues: {
      isRevisionsHidden: false,
      isAllMonthVisible: false,
      displayType: '0'
    }
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
)(CurrentEditionsTableFilters);
