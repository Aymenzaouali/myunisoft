import { compose } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import { reduxForm, formValueSelector, getFormValues } from 'redux-form';
import {
  submitPrintOrExportBalance as submitPrintOrExportBalanceThunk,
  submitPrintorExportSig as submitPrintorExportSigThunk
} from 'redux/balance';
import CurrentEditionsTableToolsDialog from 'components/groups/CurrentEditions/CurrentEditionsTableToolsDialog';

const form = 'currentEditionsTableToolsDialog';
const formSelector = formValueSelector(form);
const tableFilterFormSelector = formValueSelector('currentEditionsTableFiltersForm');

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    currentOption: state.balance.currentOption,
    isAllMonthVisible: tableFilterFormSelector(state, 'isAllMonthVisible'),
    filterFormValues: getFormValues(`${societyId}currentEditionsAccountFilter`)(state),
    selectedMode: formValueSelector('currentEditionsHeaderForm')(state, 'type'),
    values: formSelector(
      state,
      'lines',
      'onlyUnresolvedAccount',
      'watermark',
      'exportExtension',
      'exportFiligree',
      'exportFiligreeOpacity'
    )
  };
};

const mapDispatchtoProps = dispatch => ({
  submitPrintOrExportBalance: (type, params) => {
    dispatch(submitPrintOrExportBalanceThunk(type, params));
  },
  submitPrintOrExportSig: (type, params) => {
    dispatch(submitPrintorExportSigThunk(type, params));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    values, filterFormValues, currentOption, selectedMode,
    isAllMonthVisible
  } = stateProps;
  const { submitPrintOrExportBalance, submitPrintOrExportSig } = dispatchProps;
  const { watermark: id_watermark } = values;

  const {
    dateStart: start_date,
    dateEnd: end_date,
    selectionType,
    filter
  } = filterFormValues;

  const printOrExportDocument = async (type) => {
    let selectedFilter;
    if (selectedMode === 'customer') selectedFilter = '41-419999';
    if (selectedMode === 'supplier') selectedFilter = '40-40999';
    if (selectionType === 'PartialAccounts') selectedFilter = filter;

    try {
      if (currentOption === 'balance') {
        await submitPrintOrExportBalance(type, {
          filter: selectedFilter,
          start_date,
          end_date,
          id_watermark,
          all_month: isAllMonthVisible,
          // TODO: Not yet implemented on the BE and FE
          lettered_only: false
        });
      } else if (currentOption === 'sig') {
        await submitPrintOrExportSig(type, {
          start_date: moment(start_date).format('YYYYMMDD'),
          end_date: moment(end_date).format('YYYYMMDD'),
          mode: 'PDF',
          id_watermark,
          all_month: isAllMonthVisible
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    customSubmit: type => printOrExportDocument(type)
  };
};

// Enhance
const enhance = compose(
  reduxForm({
    form,
    destroyOnUnmount: false,
    initialValues: {
      lines: 'all',
      onlyUnresolvedAccount: false
    }
  }),
  connect(mapStateToProps, mapDispatchtoProps, mergeProps),
);

export default enhance(CurrentEditionsTableToolsDialog);
