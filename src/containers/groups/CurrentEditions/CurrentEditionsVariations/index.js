import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';

import CurrentEditionsVariations from 'components/groups/CurrentEditions/CurrentEditionsVariations';

const form = 'currentEditionsVariationsForm';
const formSelector = formValueSelector(form);

const mapStateToProps = state => ({
  formValues: formSelector(
    state,
    'displayVariations',
    'percentageAmount',
    'variations',
    'euroAmount'
  )
});

export default compose(
  reduxForm({
    form,
    destroyOnUnmount: false,
    initialValues: {
      displayVariations: false,
      euroAmount: '',
      variations: '',
      percentageAmount: ''
    }
  }),
  connect(mapStateToProps)
)(CurrentEditionsVariations);
