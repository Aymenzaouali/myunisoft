import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import _ from 'lodash';
import { getBalanceData as getBalanceDataThunk } from 'redux/balance';
import {
  getComments as getCommentsThunk,
  postAccountsComment as postAccountsCommentThunk,
  putAccountsComment as putAccountsCommentThunk
} from 'redux/comments';
import { openDialog, closeDialog } from 'redux/dialogs';
import { getReviews } from 'redux/tabs/dadp';
import { getCurrentTabState } from 'helpers/tabs';
import CurrentEditionsTable from 'components/groups/CurrentEditions/CurrentEditionsTable';
import moment from 'moment';


const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const currentEditionsTableFiltersFormSelector = formValueSelector('currentEditionsTableFiltersForm');
  const currentEditionsFiltersFormSelector = formValueSelector('currentEditionsFiltersForm');
  const currentEditionsHeaderFormSelector = formValueSelector('currentEditionsHeaderForm');
  const currentEditionsAccountFilter = formValueSelector(`${societyId}currentEditionsAccountFilter`);
  const { dadp } = getCurrentTabState(state);
  return {
    tableFiltersValues: currentEditionsTableFiltersFormSelector(state, 'isRevisionsHidden', 'isAllMonthVisible', 'displayType'),
    filtersValues: currentEditionsFiltersFormSelector(state, 'exercices', 'situationWrites'),
    headerValues: currentEditionsHeaderFormSelector(state, 'current', 'type', 'dateStart', 'dateEnd'),
    balanceData: _.get(state, 'balance', {}),
    isLoading: _.get(state, 'balance.isLoading', false),
    isError: _.get(state, 'balance.isError', false),
    exercices: _.get(state, `accountingWeb.exercice.${societyId}.exercice`),
    error: _.get(state, 'error.errors'),
    societyId,
    selectedExercice: currentEditionsAccountFilter(state, 'exercice', 'dateStart', 'dateEnd'),
    reviewsList: _.get(dadp, 'reviews.data', [])
  };
};

const mapDispatchToProps = dispatch => ({
  getBalanceData: (filter, start_date, end_date, id_exercice) => {
    dispatch(getBalanceDataThunk(filter, start_date, end_date, undefined, id_exercice));
  },
  getReviews: () => dispatch(getReviews()),

  openCommentsDialog: dialogProps => dispatch(openDialog(dialogProps)),
  closeCommentsDialog: dialogProps => dispatch(closeDialog(dialogProps)),

  getComments: (
    account_id, dossier_revision_id
  ) => dispatch(getCommentsThunk(account_id, dossier_revision_id)),
  postAccountsComment: (comment, params) => dispatch(postAccountsCommentThunk(comment, params)),
  putAccountsComment: (comment, params) => dispatch(putAccountsCommentThunk(comment, params))
});

const balanceTypeAccountNumberMap = {
  customer: '41',
  supplier: '40'
};

const getBalanceTypeData = (data, type) => ({
  account_List: [],
  exercice_values: [],
  month_values: [],
  ...(data
    ? data.find(entry => entry.account_number.startsWith(balanceTypeAccountNumberMap[type]))
    : {}
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    headerValues = {},
    balanceData,
    exercices = [],
    error,
    selectedExercice,
    reviewsList
  } = stateProps;
  const {
    postAccountsComment,
    closeCommentsDialog,
    putAccountsComment,
    getBalanceData
  } = dispatchProps;

  const currentRevision = _.find(reviewsList, review => (
    moment(selectedExercice.dateStart).format('YYYY-MM-DD') === review.start_date && moment(selectedExercice.dateEnd).format('YYYY-MM-DD') === review.end_date
  ));
  const { type } = headerValues;

  const exercice_N = exercices.filter(ex => ex.label === 'N');
  let formatedStartDate;
  let formatedEndDate;
  if (!_.isEmpty(exercice_N) && !(selectedExercice.dateStart && selectedExercice.dateEnd)) {
    const {
      start_date,
      end_date
    } = exercice_N[0];

    formatedStartDate = moment(start_date).format('YYYYMMDD');
    formatedEndDate = moment(end_date).format('YYYYMMDD');
  } else {
    formatedStartDate = selectedExercice.dateStart;
    formatedEndDate = selectedExercice.dateEnd;
  }

  const errorMessage = _.get(error, '[0].message');

  function getCustomData(balanceType) {
    /**
     * TODO: When backend is fully ready,
     * based on the headerValues,
     * we should make a request for the specific
     * values instead of filtering the full
     * balance data
     */
    const data = balanceData.data.find(d => d.account_number === '4');
    const accountList = _.get(data, 'account_List', []);

    return getBalanceTypeData(accountList, balanceType);
  }

  function enhanceBalanceData() {
    switch (type) {
    case 'general': {
      return balanceData;
    }
    case 'supplier':
    case 'customer': {
      const {
        account_List,
        exercice_values,
        month_values
      } = getCustomData(type);

      return {
        ...balanceData,
        data: account_List,
        results: {
          exercice_values,
          month_values
        }
      };
    }
    default:
      return balanceData;
    }
  }

  const postNewComment = async (payload, data) => {
    await postAccountsComment(
      payload,
      {
        account_id: data.id_account,
        dossier_revision_id: currentRevision.id_dossier_revision
      }
    );
    await getBalanceData(undefined, formatedStartDate, formatedEndDate, _.get(selectedExercice, 'exercice.id_exercice'));
    closeCommentsDialog();
  };

  const editComment = async (payload, data) => {
    await putAccountsComment(
      payload,
      {
        account_id: data.id_account,
        dossier_revision_id: currentRevision.id_dossier_revision
      }
    );
    await getBalanceData(undefined, formatedStartDate, formatedEndDate, _.get(selectedExercice, 'exercice.id_exercice'));
    closeCommentsDialog();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    balanceData: enhanceBalanceData(),
    start_date: formatedStartDate,
    end_date: formatedEndDate,
    errorMessage,
    currentRevision,
    postNewComment,
    editComment
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CurrentEditionsTable);
