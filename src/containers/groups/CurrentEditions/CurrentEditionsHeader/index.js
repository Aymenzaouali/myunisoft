import { connect } from 'react-redux';
import {
  reduxForm, getFormValues, change
} from 'redux-form';
import CurrentEditionsHeader from 'components/groups/CurrentEditions/CurrentEditionsHeader';
import { selectCurrentOption } from 'redux/balance/actions';
import {
  getLedger as getLedgerThunk,
  getBalanceData as getBalanceDataThunk
} from 'redux/balance';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import moment from 'moment';
import _ from 'lodash';


const mapStateToProps = state => ({
  societyId: state.navigation.id,
  currentOption: state.balance.currentOption,
  currentEditionsFormValues: getFormValues('currentEditionsHeaderForm')(state),
  exercices: _.get(state, `accountingWeb.exercice.${state.navigation.id}.exercice`),
  initialValues: {
    current: 'balance',
    type: 'general',
    attachment: true,
    comment_account: false,
    comment_entry: false,
    not_lettered_only: false,
    not_solde_only: false,
    selectionType: 'AllAccounts'
  }
});

const mapDispatchToProps = dispatch => ({
  selectOption: (option) => {
    dispatch(change('currentEditionsTableFiltersForm', 'isAllMonthVisible', false));
    dispatch(selectCurrentOption(option));
  },
  getLedger: (type, params) => dispatch(getLedgerThunk(type, params)),
  getBalanceData: filter => dispatch(getBalanceDataThunk(filter)),
  initializeHeader: (dateStart, dateEnd, exercice) => dispatch(autoFillFormValues('currentEditionsHeaderForm', { dateStart, dateEnd, exercice })),
  setAccountFilter: (filter, selectionType) => {
    dispatch(autoFillFormValues('currentEditionsAccountFilter', { filter, selectionType }));
    dispatch(autoFillFormValues('currentEditionsHeaderForm', { filter, selectionType }));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    currentEditionsFormValues = {},
    exercices = []
  } = stateProps;
  const {
    getLedger,
    setAccountFilter
  } = dispatchProps;

  const exercice_N = exercices.filter(ex => ex.label === 'N');
  const dateStart = _.get(exercice_N, '[0].start_date');
  const dateEnd = _.get(exercice_N, '[0].end_date');

  const {
    selectionType,
    attachment,
    comment_entry, comment_account,
    not_lettered_only, not_solde_only,
    filter
  } = currentEditionsFormValues;

  const saveOrPrintLedger = (type) => {
    getLedger(
      type,
      {
        society_id: societyId,
        start_date: moment(currentEditionsFormValues.dateStart).format('YYYY-MM-DD'),
        end_date: moment(currentEditionsFormValues.dateEnd).format('YYYY-MM-DD'),
        attachment,
        comment_entry,
        comment_account,
        not_lettered_only,
        not_solde_only,
        account_number: filter
      }
    );
  };

  const getBalance = (type) => {
    const { target } = type;
    if (target.value === 'customer') {
      setAccountFilter('41-419999', 'PartialAccounts');
    } else if (target.value === 'supplier') {
      setAccountFilter('40-409999', 'PartialAccounts');
    } else if (target.value === 'general') {
      setAccountFilter(undefined, 'AllAccounts');
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    saveOrPrintLedger,
    getBalance,
    dateStart,
    dateEnd,
    exercice_N,
    selectionType
  };
};

const wrappedWithReduxForm = reduxForm({
  form: 'currentEditionsHeaderForm',
  destroyOnUnmount: false
})(CurrentEditionsHeader);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrappedWithReduxForm);
