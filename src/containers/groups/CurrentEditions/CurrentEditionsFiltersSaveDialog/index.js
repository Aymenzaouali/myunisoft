import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';

import CurrentEditionsFiltersSaveDialog from 'components/groups/CurrentEditions/CurrentEditionsFiltersSaveDialog';

const form = 'currentEditionsFiltersSaveDialog';
const formSelector = formValueSelector(form);

const mapStateToProps = state => ({
  formValues: formSelector(state, 'mode', 'saveName', 'accountNumberValue', 'saveId')
});

export default compose(
  reduxForm({
    form,
    destroyOnUnmount: false
  }),
  connect(mapStateToProps)
)(CurrentEditionsFiltersSaveDialog);
