import AccountingFirmSettings from 'components/groups/AccountingFirmSettings';
import { connect } from 'react-redux';
import AccountingFirmSettingsService from 'redux/accountingFirmSettings';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  setCreateNewStatus: isCreate => dispatch(
    AccountingFirmSettingsService.setCreateNewStatus(isCreate)
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(AccountingFirmSettings);
