import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { initialize } from 'redux-form';
import _ from 'lodash';
import { EMPLOYEE_PP_TABLE_NAME, SOCIETY_PP_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import PhysicalPersonList from 'components/groups/PhysicalPersonList';
import actions from 'redux/tabs/physicalPersonCreation/actions';
import { setDeletionDialog } from 'redux/tabs/crm/actions';
import {
  deletePersonnePhysique as deletePersonnePhysiqueThunk
} from 'redux/tabs/physicalPersonList';
import { routesByKey } from 'helpers/routes';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = state => ({
  tabId: state.navigation.id,
  tableNameEmployeePP: getTableName(state.navigation.id, EMPLOYEE_PP_TABLE_NAME),
  tableNameSocietyPP: getTableName(state.navigation.id, SOCIETY_PP_TABLE_NAME),
  formName: tabFormName('physicalPersonCreationForm', state.navigation.id),
  selectedPersPhysique: _.get(getCurrentTabState(state), 'physicalPersonCreation.selected_physical_person', null),
  deletionInformation: _.get(getCurrentTabState(state), 'crm.deletionInformation', {})
});

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  initializeForm: (data, tab) => dispatch(initialize(tabFormName('physicalPersonCreationForm', tab), data)),
  resetSelectedPersPhysique: () => dispatch(actions.resetSelectedPersPhysique()),
  resetTable: tableName => dispatch(tableActions.setData(tableName, [])),
  deletePersonnePhysique: id => dispatch(deletePersonnePhysiqueThunk(id)),
  setDeletionDialog: (mode, id) => dispatch(setDeletionDialog(mode, id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    tabId,
    tableNameEmployeePP,
    tableNameSocietyPP
  } = stateProps;
  const {
    push,
    initializeForm: _initializeForm,
    resetSelectedPersPhysique,
    resetTable,
    deletePersonnePhysique,
    setDeletionDialog
  } = dispatchProps;

  const initializeForm = data => _initializeForm(data, tabId);

  const resetTables = () => {
    resetTable(tableNameEmployeePP);
    resetTable(tableNameSocietyPP);
  };

  const onAddNewPhysicalPerson = async () => {
    resetTables();
    await resetSelectedPersPhysique();
    await initializeForm();
    push(routesByKey.physicalPersonCreation);
  };

  const deleteAndClose = async (id) => {
    await deletePersonnePhysique(id);
    setDeletionDialog();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initializeForm,
    onAddNewPhysicalPerson,
    deleteAndClose
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(PhysicalPersonList);
