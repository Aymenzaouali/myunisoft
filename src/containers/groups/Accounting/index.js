import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';

import { getRouteByKey } from 'helpers/routes';

import { splitWrap } from 'context/SplitContext';

import Accounting from 'components/groups/Accounting';
import { openPopupSplit } from 'redux/tabs/split/actions';

// Component
const mapStateToProps = state => ({
  societyId: state.navigation.id,
  entries: state.accountingWeb.entries,
  filters: getFormValues(`${state.navigation.id}accountingFilter`)(state),
  routeForIBSetting: getRouteByKey('bankIntegrationSettings')
});

const mapDispatchToProps = dispatch => ({
  openPopupSplit: masterId => dispatch(openPopupSplit(masterId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { split } = ownProps;
  const { filters } = stateProps;

  if (filters && split.master) {
    const isNotEcriture = (filters.type !== 'e');

    if (split.args.isNotEcriture !== isNotEcriture) {
      split.updateArgsSplit({
        isNotEcriture,
        account: undefined
      });
    }
  }

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};

const enhance = compose(
  splitWrap,
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(Accounting);
