import { connect } from 'react-redux';
import List from 'components/groups/List';
import _ from 'lodash';
import { push } from 'connected-react-router';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import I18n from 'assets/I18n';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    tableName,
    currentCompanyName: _.get(currentTab, 'label', I18n.t('companyList.default')),
    userListToCsv: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {}),
    routeKey: _.get(state, 'navigation.routeKey')
  };
};

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { userListToCsv, currentCompanyName, tableName } = stateProps;

  const dataToExport = formatTableDataToCSVData(userListToCsv);
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(List);
