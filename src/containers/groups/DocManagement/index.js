import DMSidebar from './DMSidebar';
import DocTable from './DocTable';

export {
  DMSidebar,
  DocTable
};
