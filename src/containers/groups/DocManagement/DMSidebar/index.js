import { connect } from 'react-redux';
import { DMSidebar } from 'components/groups/DocManagement';
import DocManService from 'redux/docManagement';
import _ from 'lodash';
import { buildFolderStructure } from './folderStructureBuilder';
import actions from '../../../../redux/docManagement/actions';


const mapStateToProps = state => ({
  society_id: state.navigation.id,
  gedTableData: state.docManagement.gedTableData,
  folderTree: state.docManagement.tree,
  activeTabId: state.docManagement.activeTabId,
  activeFolder: state.docManagement.activeFolder
});

const mapDispatchToProps = dispatch => ({
  getDocuments: leafId => dispatch(DocManService.getDocuments(leafId)),
  setActiveDocFolder: folder => dispatch(actions.setActiveDocFolder(folder))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    folderTree,
    activeTabId
  } = stateProps;

  const tree_list = _.get(folderTree[society_id], `[${activeTabId}]tree_list`, []);
  const isLoading = _.get(folderTree[society_id], `[${activeTabId}]isLoading`, true);
  const isError = _.get(folderTree[society_id], `[${activeTabId}]isError`, false);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isTreeLoad: isLoading,
    isTreeError: isError,
    tree: buildFolderStructure(tree_list)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DMSidebar);
