import _ from 'lodash';

/**
 *
 * @param folder object
 * @returns folder name according existed params
 */

const getFolderName = ({ title, count, label }) => title || (label && `${label} ${(count || '')}`) || '';


const deepFoldersMap = (obj) => {
  const title = getFolderName(obj);
  if (!obj.children && (_.isEmpty(obj.rooms) && _.isEmpty(obj.folders))) {
    return {
      id: Math.floor(Math.random() * 100000),
      ...obj,
      title: obj.society_name ? obj.society_name : title,
      isFolder: !!obj.society_name
    };
  }

  /** map children array and return transformed array of objects for TreeListItem component */
  const children = _.map(obj.children, (child) => {
    /**
     * check if child has children or rooms and folders for discussion page
     * if hasn't return existed leaf
     * */
    if (child && !child.children && (_.isEmpty(child.rooms) && _.isEmpty(child.folders))) {
      const title = getFolderName(child);
      return {
        id: Math.floor(Math.random() * 100000),
        ...child,
        isFolder: !!child.society_name,
        title
      };
    }
    /**
     * if child has children or rooms or/and folders in discussion tab
     * call deepFoldersMap for recursive mapping
     * concat folders and rooms in children if we working with discussion tab
     * */
    return child && deepFoldersMap({
      ...child,
      ...((child.rooms || child.folders) && {
        children: _.concat(child.folders || [], child.rooms || [])
      })
    });
  });
  return {
    id: Math.floor(Math.random() * 100000),
    ...obj,
    title: obj && obj.society_name ? obj.society_name : title,
    isFolder: !!obj.society_name,
    ...(!_.isEmpty(children) ? { children } : {})
  };
};


/**
 *
 * @param unStructuredTree - tree array returned from API
 * @returns {Array} structured array with children for TreeListItem component
 */
export const buildFolderStructure = unStructuredTree => _.map(unStructuredTree, (child) => {
  /** return item when item hasn't children */
  if (child && !child.children && (_.isEmpty(child.rooms) && _.isEmpty(child.folders))) {
    const title = getFolderName(child);
    return {
      id: Math.floor(Math.random() * 100000),
      ...child,
      isFolder: !!child.society_name,
      title
    };
  }
  /** format children of the current object */
  return child && deepFoldersMap({
    ...child,
    /** format children for Discussions tab */
    ...((child.rooms || child.folders) && {
      children: _.concat(child.folders || [], child.rooms || [])
    })
  });
});
