import { connect } from 'react-redux';
import { DocTable } from 'components/groups/DocManagement';
import DocManService from 'redux/docManagement';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import { GEDTabs } from 'assets/constants/data';
import _ from 'lodash';
import actions from '../../../../redux/docManagement/actions';


const mapStateToProps = (state) => {
  const { id: society_id, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === society_id));
  const { activeTabId } = state.docManagement;
  const GEDActiveTable = GEDTabs.find(tab => activeTabId === tab.id).label;
  return {
    society_id: state.navigation.id,
    activeTabId: state.docManagement.activeTabId,
    formatted_table_data: _.get(state, `tables.transformedTableData[${GEDActiveTable}][${society_id}].params`, {}),
    currentCompanyName: _.get(currentTab, 'label')
  };
};

const mapDispatchToProps = dispatch => ({
  getDocuments: leafId => dispatch(DocManService.getDocuments(leafId)),
  setActiveDocFolder: folder => dispatch(actions.setActiveDocFolder(folder))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    formatted_table_data,
    currentCompanyName,
    activeTabId
  } = stateProps;

  const GEDActiveTable = GEDTabs.find(tab => activeTabId === tab.id).label;

  const baseGEDFileName = createFormattedFileName(currentCompanyName, GEDActiveTable);

  return {
    baseFileName: baseGEDFileName,
    csvData: formatTableDataToCSVData(formatted_table_data),
    disableExport: _.isEmpty(formatted_table_data),
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DocTable);
