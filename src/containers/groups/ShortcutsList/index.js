import { connect } from 'react-redux';
import ShortcutsList from 'components/groups/ShortcutsList';
import { push } from 'connected-react-router';
import keymap from 'assets/config/shortcuts/keymap';
import I18n from 'assets/I18n';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'];
  const userPlatform = window.navigator.platform;

  let shortcutsTab = [];
  const allShortcuts = Object.entries(keymap);

  allShortcuts.forEach((oneShortcut) => {
    let shortcuts = [];

    const [
      key,
      shortcutsKeys
    ] = oneShortcut;


    Object.entries(shortcutsKeys).forEach((key) => {
      const [
        name,
        combination
      ] = key;

      shortcuts = [...shortcuts, { name, combination }];
    });

    String.prototype.allReplace = function (obj) { //eslint-disable-line
      let retStr = this;
      for (const x in obj) { //eslint-disable-line
        retStr = retStr.replace(new RegExp(x, 'g'), obj[x]);
      }
      return retStr;
    };

    const obj = {
      name: key,
      shortcuts: shortcuts.map((label) => {
        const { name, combination } = label;
        const formattedCombination = combination => combination.allReplace({
          minus: '"-"',
          plus: '"+"',
          up: I18n.t('shortcuts.arrow_up'),
          down: I18n.t('shortcuts.arrow_down')
        });
        if (typeof combination === 'string') {
          return {
            name,
            combination: formattedCombination(combination)
          };
        }
        if (macosPlatforms.includes(userPlatform)) {
          return {
            name,
            combination: formattedCombination(combination.osx)
          };
        }
        return {
          name,
          combination: formattedCombination(combination.windows)
        };
      })
    };

    shortcutsTab = [...shortcutsTab, obj];
  });

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    shortcutsTab,
    hiddenShortCuts: [
      'NEW_ACCOUNTING.EQUAL',
      'NEW_ACCOUNTING.PLUS',
      'NEW_ACCOUNTING.MINUS',
      'NEW_ACCOUNTING.MULTIPLE',
      'NEW_ACCOUNTING.DIVIDE',
      'NEW_ACCOUNTING.ENTER',
      'DIALOG_CONFIRMATION',
      'DIALOG_CONFIRMATION.ENTER'
    ]
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ShortcutsList);
