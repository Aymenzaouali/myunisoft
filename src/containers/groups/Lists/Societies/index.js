import { connect } from 'react-redux';
import Societies from 'components/groups/Lists/Societies';
import {
  getSociety as getSocietyThunk
} from 'redux/navigation';
import PortfolioListSettingsService from 'redux/portfolioListSettings';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import {
  getFormValues, reduxForm, change, reset, formValueSelector
} from 'redux-form';
import actions from 'redux/tabs/user/actions';
import _ from 'lodash';
import { tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);
  const selector = formValueSelector(formName);

  return {
    form: formName,
    wallets: _.get(state, 'portfolioListSettings.wallets', []),
    user_id: selector(state, 'user_id'),
    physical_person_id: selector(state, 'physical_person_id'),
    societies: _.get(state, 'navigation.societies', []),
    newUserValues: getFormValues(formName)(state)
  };
};

const mapDispatchToProps = dispatch => ({
  getSociety: () => dispatch(getSocietyThunk()),
  getWalletList: () => dispatch(
    PortfolioListSettingsService.getWalletList()
  ),
  _changeFormValue: (form, field, value) => dispatch(change(form, field, value)),
  selectTab: tabIndex => dispatch(actions.selectTab(tabIndex)),
  _resetForm: form => dispatch(reset(form)),
  initSocietyType: (formName, data) => dispatch(autoFillFormValues(formName, data))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    form: formName,
    newUserValues,
    societies,
    wallets
  } = stateProps;
  const {
    getSociety,
    _changeFormValue,
    getWalletList
  } = dispatchProps;

  const changeFormValue = (field, value) => _changeFormValue(formName, field, value);

  const profil_id = _.get(newUserValues, 'newUserProfil.id');
  const profil_label = _.get(newUserValues, 'newUserProfil.value');
  const type_id = _.get(newUserValues, 'newUserType.id');
  const type_label = _.get(newUserValues, 'newUserType.value');

  const selectedSocieties = _.get(newUserValues, 'societies', false);
  const selectedWallet = _.get(newUserValues, 'selectedWallet', false);
  const access_list = _.get(newUserValues, 'access_list', []);
  const society_access_list = _.get(newUserValues, 'society_access_list', []);
  const wallet_access_list = _.get(newUserValues, 'wallet_access_list', []);

  const onGranted = () => {
    const randomNum = _.random(0, 500);
    const temporaryAccessId = `temp${randomNum}`;
    let newGranted = [];

    if (selectedSocieties) {
      const accessTypeSociety = {
        id: type_id,
        value: type_label,
        label: type_label
      };
      const accessProfilSociety = {
        id: profil_id,
        value: profil_label,
        label: profil_label
      };

      const newSocieties = selectedSocieties.map(society => ({
        ...society, profil_id, type_id, acces_id: temporaryAccessId
      }));

      changeFormValue([`accessProfilSociety-${temporaryAccessId}`], accessProfilSociety);
      changeFormValue([`accessTypeSociety-${temporaryAccessId}`], accessTypeSociety);

      newGranted = [...newGranted, ...newSocieties];
      changeFormValue('society_access_list', [...society_access_list, ...newSocieties]);
    }

    if (selectedWallet) {
      const accessTypeWallet = {
        id: type_id,
        value: type_label,
        label: type_label
      };
      const accessProfilWallet = {
        id: profil_id,
        value: profil_label,
        label: profil_label
      };

      const newWallets = selectedWallet.map(wallet => (
        {
          acces_id: temporaryAccessId,
          wallet_id: wallet.id,
          wallet_label: wallet.label,
          society_id: 0,
          profil_id,
          type_id
        }));


      newGranted = [...newGranted, ...newWallets];

      changeFormValue([`accessProfilWallet-${temporaryAccessId}`], accessProfilWallet);
      changeFormValue([`accessTypeWallet-${temporaryAccessId}`], accessTypeWallet);
      changeFormValue('wallet_access_list', [...wallet_access_list, ...newWallets]);
    }

    changeFormValue('access_list', [...access_list, ...newGranted.map(granted => ({ ...granted }))]);

    changeFormValue('accessTypeSociety', '');
    changeFormValue('accessProfilSociety', '');
    changeFormValue('societies', '');
    changeFormValue('selectedWallet', '');
  };

  const societiesWithoutGranted = societies.filter(
    society => !access_list.find(s => s.society_id === society.society_id)
  );

  const walletsWithoutGranted = _.get(wallets, 'wallets_list', []).filter(
    wallet => !access_list.find(w => w.wallet_id === wallet.id_wallet)
  );

  const formatSocietiesForAutoComplete = societiesWithoutGranted.map(
    society => ({ ...society, label: society.name, value: society.society_id })
  );

  const walletsForAutoComplete = walletsWithoutGranted.map(
    wallet => ({ id: wallet.id_wallet, label: wallet.libelle, value: wallet.libelle })
  );

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    societies: formatSocietiesForAutoComplete,
    onGranted,
    loadSocieties: getSociety,
    loadWallets: getWalletList,
    changeFormValue,
    wallets: walletsForAutoComplete,
    selectedSocieties,
    selectedWallet,
    profil_id,
    type_id
  };
};

const wrappedSocieties = reduxForm({
  destroyOnUnmount: false
})(Societies);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrappedSocieties);
