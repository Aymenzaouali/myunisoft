import { connect } from 'react-redux';
import MemoList from 'components/groups/Lists/MemoList';
import {
  putMemo as putMemoThunk,
  getMemo as getMemoThunk
} from 'redux/dashboard';
import _ from 'lodash';

const mapStateToProps = state => ({
  idSociety: _.get(state, 'navigation.id'),
  memoItemsList: _.get(state, 'dashboardWeb.memoItems')
});

const mapDispatchToProps = dispatch => ({
  putMemo: (...values) => dispatch(putMemoThunk(...values)),
  getMemo: idSociety => dispatch(getMemoThunk(idSociety))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(MemoList);
