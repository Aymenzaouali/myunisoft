import { connect } from 'react-redux';
import I18n from 'i18next';

import { deleteAccountingPlans as deleteAccountingPlansThunk } from 'redux/accountingPlans';
import PlansScreen from 'components/screens/Plans';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

import _ from 'lodash';

const mapStateToProps = (state) => {
  const { id: societyId } = state.navigation;
  const tableName = I18n.t('accountingPlans.title');
  return {
    societyId,
    tableName,
    currentCompanyName: state.login.user.prenom,
    plans: _.get(state, 'accountingPlans.plans', {}),
    selectedPlans: _.get(state, 'accountingPlans.selectedPlans', []),
    planSelectedLine: state.accountingPlans.selectedLine,
    plan: _.get(state, `accountingPlans.plans.list.${state.accountingPlans.selectedLine}`, {}),
    plansEtalonTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  deleteAccountingPlans: () => dispatch(deleteAccountingPlansThunk()),
  initializePlanData: data => dispatch(autoFillFormValues('planForm', data))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { plansEtalonTableData, currentCompanyName, tableName } = stateProps;

  const dataToExport = formatTableDataToCSVData(plansEtalonTableData);
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName
  };
};
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(PlansScreen);
