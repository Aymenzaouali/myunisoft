import { connect } from 'react-redux';
import { compose } from 'redux';
import { autofill, formValues } from 'redux-form';

import Periode, { DEFAULT_START_DATE_NAME, DEFAULT_END_DATE_NAME, DEFAULT_EXERCISE_NAME } from 'components/groups/Periode';

// Component
const mapDispatchToProps = (dispatch, ownProps) => ({
  autofill: (field, value) => dispatch(autofill(ownProps.formName, field, value))
});

// Enhance
const enhance = compose(
  connect(null, mapDispatchToProps),
  formValues(props => ({
    startDate: props.startDateName || DEFAULT_START_DATE_NAME,
    endDate: props.endDateName || DEFAULT_END_DATE_NAME,
    exercises: props.exerciseName || DEFAULT_EXERCISE_NAME
  }))
);

export default enhance(Periode);
