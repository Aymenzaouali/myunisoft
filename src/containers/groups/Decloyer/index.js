import Decloyer from 'components/groups/Decloyer';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  societyId: state.navigation.id
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId
  } = stateProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    societyId
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Decloyer);
