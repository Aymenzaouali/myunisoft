import { connect } from 'react-redux';
import {
  HISTORYMONTHVALUE,
  initPeriod
} from 'helpers/dashboard';
import {
  redirectFromCollab as redirectFromCollabThunk
} from 'redux/navigation';
import TrackingSociety from 'components/groups/TrackingSociety';
import actions from 'redux/dashboard/actions';
import { change } from 'redux-form';
import { windev } from 'helpers/api';
import moment from 'moment';
import _ from 'lodash';

const mapStateToProps = state => ({
  society_id: state.navigation.id,
  currentMode: _.get(state, 'dashboardWeb.currentMode', 1),
  selectedPeriod: _.get(state, 'dashboardWeb.selectedPeriod', {})
});

const mapDispatchToProps = dispatch => ({
  initHeaderPeriod: (form, value) => dispatch(change(form, 'period', value)),
  initFilterForIs: period => dispatch(actions.initFilterForIs(period)),
  initFilterForVat: period => dispatch(actions.initFilterForVat(period)),
  initFilterForCvae: period => dispatch(actions.initFilterForCvae(period)),
  redirectFromCollab: (route, type, society_id) => {
    dispatch(redirectFromCollabThunk(route, type, society_id));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    currentMode,
    selectedPeriod
  } = stateProps;

  const {
    initFilterForVat,
    initFilterForIs,
    initFilterForCvae,
    initHeaderPeriod
  } = dispatchProps;

  const _initPeriod = (type, period, society_id) => {
    switch (type) {
    case 'is':
      return initPeriod(period, initFilterForIs, initHeaderPeriod, `${society_id}ISDeclarationHeader`);
    case 'vat':
      return initPeriod(period, initFilterForVat, initHeaderPeriod, `${society_id}VATHeader`);
    case 'cvae':
      return initPeriod(period, initFilterForCvae, initHeaderPeriod, `${society_id}BundleHeader`);
    default:
      return undefined;
    }
  };

  const getInfoForInitPeriod = (type, company_id) => {
    switch (type) {
    case 'is':
      return {
        initFilter: initFilterForIs,
        initHeader: initHeaderPeriod,
        headerName: `${company_id}ISDeclarationHeader`
      };
    case 'vat':
      return {
        initFilter: initFilterForVat,
        initHeader: initHeaderPeriod,
        headerName: `${company_id}VATHeader`
      };
    case 'cvae':
      return {
        initFilter: initFilterForCvae,
        initHeader: initHeaderPeriod,
        headerName: `${company_id}BundleHeader`
      };
    default:
      return {};
    }
  };

  const getLink = async (params) => {
    const response = await windev.makeApiCall('/dashboard/society/vat_link', 'get', params, {});
    const { data } = response;

    return data.link;
  };

  const getPeriod = (month) => {
    switch (currentMode) {
    case 0: // historic
      return moment(`${selectedPeriod.period}-${HISTORYMONTHVALUE[month]}`).format('YYYY-MM');
    case 1: // Week
      return moment().subtract(1, 'month').format('YYYY-MM');
    case 2: // Month
      return moment(selectedPeriod.period).subtract(1, 'month').format('YYYY-MM');
    case 3: // Year
      return `${selectedPeriod.period}-${HISTORYMONTHVALUE[month]}`; // Pas forcément bon pour l'instant mais pas encore fait coté back donc à voir
    default:
      return null;
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    getPeriod,
    getLink,
    _initPeriod,
    getInfoForInitPeriod
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TrackingSociety);
