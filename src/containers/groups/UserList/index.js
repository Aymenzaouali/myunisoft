import { connect } from 'react-redux';
import UserList from 'components/groups/UserList';
import { push } from 'connected-react-router';
import { setDeletionDialog } from 'redux/tabs/crm/actions';
import {
  getUser,
  deleteUser as deleteUserThunk
} from 'redux/tabs/user';
import actions from 'redux/tabs/user/actions';
import { routesByKey } from 'helpers/routes';
import { reset } from 'redux-form';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = state => ({
  formName: tabFormName('newUser', state.navigation.id),
  selectedUser: _.get(getCurrentTabState(state), 'user.selectedUser.user_id'),
  deletionInformation: _.get(getCurrentTabState(state), 'crm.deletionInformation', {})
});

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  _resetForm: form => dispatch(reset(form)),
  selectUser: data => dispatch(getUser(data)),
  resetSelectedUser: () => dispatch(actions.resetSelectedUser()),
  deleteUser: id => dispatch(deleteUserThunk(id)),
  setDeletionDialog: (mode, id) => dispatch(setDeletionDialog(mode, id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { formName } = stateProps;
  const {
    push,
    _resetForm,
    resetSelectedUser,
    deleteUser,
    setDeletionDialog
  } = dispatchProps;

  const resetForm = () => _resetForm(formName);

  const onAddNewUser = async () => {
    await resetForm();
    await resetSelectedUser();
    push(routesByKey.userCreation);
  };

  const deleteAndClose = async (id) => {
    await deleteUser(id);
    setDeletionDialog();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onAddNewUser,
    resetForm,
    deleteAndClose
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(UserList);
