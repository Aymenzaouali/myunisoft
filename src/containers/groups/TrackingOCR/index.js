import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';
import TrackingOCR from 'components/groups/TrackingOCR';
import { getTrackingOcr as getTrackingOcrThunk } from 'redux/ocr';
import I18n from 'assets/I18n';
import { push } from 'connected-react-router';

const mapStateToProps = state => ({
  society_id: state.navigation.id,
  formValue: getFormValues(`${state.navigation.id}TrackingOcrFilter`)(state)
});

const mapDispatchToProps = dispatch => ({
  getTrackingOcr: societyId => dispatch(getTrackingOcrThunk(societyId)),
  push: path => dispatch(push(path))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    formValue
  } = stateProps;

  const sendByFilterItem = [
    {
      value: 1,
      label: I18n.t('OCR.trackingTable.filter.documentSend.byMe')
    },
    {
      value: 2,
      label: I18n.t('OCR.trackingTable.filter.documentSend.byMe')
    }
  ];

  const dashboardCollab = society_id === -2;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    sendByFilterItem,
    society_id,
    dashboardCollab,
    formValue
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TrackingOCR);
