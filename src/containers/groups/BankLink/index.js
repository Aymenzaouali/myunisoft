import { connect } from 'react-redux';
import { compose } from 'redux';
import BankLink from 'components/groups/BankLink';
import { splitWrap } from 'context/SplitContext';

const mapStateToProps = state => ({
  societyId: state.navigation.id
});

const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

// Enhance
const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  splitWrap
);

export default enhance(BankLink);
