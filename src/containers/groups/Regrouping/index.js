import { connect } from 'react-redux';

import {
  groupDocuments,
  ungroupDocuments,
  selectDocument,
  deleteDocuments,
  expandDocument
} from 'redux/document/actions';

import Attachments from 'components/groups/Attachments';

function documentsArrayToObject(obj, document_id) {
  obj[document_id] = this.find(doc => doc.document_id === document_id); // eslint-disable-line

  return obj;
}

// Component
const mapStateToProps = ({ documentWeb: { selected, expanded } }, { files }) => {
  const docs = files
    .filter(({ documents }) => documents.find(({ status }) => status === 'waiting'))
    .map(groupDocument => ({
      ...groupDocument,
      documents: groupDocument.documents.map(doc => ({
        ...doc,
        thumbnail: doc.blob
      }))
    }));

  return {
    selected: selected
      .reduce(documentsArrayToObject.bind(docs), {}),
    selectedDocuments: docs.filter(doc => selected?.includes(doc.document_id)),
    expanded: expanded
      .reduce(documentsArrayToObject.bind(docs), {}),
    docs,
    attached: {}
  };
};

const mapDispatchToProps = dispatch => ({
  selectDocument: (document_id, value) => dispatch(selectDocument(document_id, value)),
  expandDocument: (document_id, value) => dispatch(expandDocument(document_id, value)),
  onAttach: () => dispatch(groupDocuments()),
  deleteDocuments: id => dispatch(deleteDocuments(id)),
  onDetach: () => dispatch(ungroupDocuments())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { selectedDocuments } = stateProps;
  const { deleteDocuments } = dispatchProps;

  const selectedIds = selectedDocuments.map(doc => doc.document_id);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onDelete: () => deleteDocuments(selectedIds)
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Attachments);
