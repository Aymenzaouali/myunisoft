import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import moment from 'moment';

import I18n from 'assets/I18n';

import { getCurrentTabState, tabFormName } from 'helpers/tabs';

import { getReviews } from 'redux/tabs/dadp';
import { selectReview } from 'redux/tabs/dadp/actions';

import { AsyncTabsButton } from 'components/basics/Buttons';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);
  const societyId = state.navigation.id;

  const reviewSelector = formValueSelector(tabFormName('dadpReviewInfo', societyId));
  const reviewAttr = (review, attr) => {
    if (review.id_dossier_revision === dadp.reviews.selectedId) {
      return reviewSelector(state, attr) || review[attr];
    }

    return review[attr];
  };
  const reviewLabel = (review) => {
    const type = reviewAttr(review, 'type');
    const date = reviewAttr(review, type === 'BIL' ? 'end_date' : 'start_date');

    return `${I18n.t(`dadp.review.types.${type}`)} ${moment(date).format('DD/MM/YYYY')}`;
  };

  return {
    isLoading: dadp.reviews.loading,
    isError: dadp.reviews.error,

    tabs: dadp.reviews.data.map(review => ({
      id: review.id_dossier_revision,
      label: reviewLabel(review),

      isSelected: review.id_dossier_revision === dadp.reviews.selectedId,
      isClosable: true
    }))
  };
};

const mapDispatchToProps = dispatch => ({
  load: () => dispatch(getReviews()),

  onAdd: () => {}, // TODO: add review
  onClick: (event, tab) => dispatch(selectReview(tab.id)),
  onClose: () => {} // TODO: close review
});

export default connect(mapStateToProps, mapDispatchToProps)(AsyncTabsButton);
