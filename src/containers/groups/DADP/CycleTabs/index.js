import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import _ from 'lodash';

import { getCurrentTabState, tabFormName } from 'helpers/tabs';

import { getCycles } from 'redux/tabs/dadp';
import { selectCycle } from 'redux/tabs/dadp/actions';

import CycleTabs from 'components/groups/DADP/CycleTabs';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);
  const societyId = state.navigation.id;

  const selector = formValueSelector(tabFormName('dadpSection', societyId));
  const reviewSelector = formValueSelector(tabFormName('dadpReviewInfo', societyId));

  return {
    cycles: dadp.cycles.data,
    loading: dadp.cycles.loading,
    selectedId: dadp.cycles.selectedId,

    category: dadp.category,
    reviewId: dadp.reviews.selectedId,
    startDate: reviewSelector(state, 'start_date'),
    endDate: reviewSelector(state, 'end_date'),
    sectionId: _.get(selector(state, 'section'), 'id_section')
  };
};

const mapDispatchToProps = dispatch => ({
  getCycles: (category, review_id, start_date, end_date, section_id) => dispatch(
    getCycles(category, review_id, start_date, end_date, section_id)
  ),
  selectCycle: cycleId => dispatch(selectCycle(cycleId))
});

export default connect(mapStateToProps, mapDispatchToProps)(CycleTabs);
