import React from 'react';
import { connect } from 'react-redux';
import {
  getFormValues, change, arrayPush, arrayRemove, Field
} from 'redux-form';
import TableFactory from 'components/groups/Tables/Factory';
import {
  UserProfilAutoComplete,
  UserTypeAutoComplete
} from 'containers/reduxForm/Inputs';
import _ from 'lodash';
import { tabFormName } from 'helpers/tabs';
import I18n from 'assets/I18n';

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);

  return {
    formName,
    newUserValues: getFormValues(formName)(state)
  };
};

const mapDispatchToProps = dispatch => ({
  _changeFormValue: (form, field, value) => dispatch(change(form, field, value)),
  _updateAccessList: (form, field, value) => dispatch(change(form, field, value)),
  _removeWalletFromList: (form, acces_id, index) => {
    dispatch(arrayPush(form, 'delete_list', { acces_id }));
    dispatch(arrayRemove(form, 'access_list', index));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { formName, newUserValues } = stateProps;
  const { _updateAccessList, _removeWalletFromList, _changeFormValue } = dispatchProps;
  const { fields, classes } = ownProps;

  const access_list = _.get(newUserValues, 'access_list', []);
  const wallet_access_list = _.get(newUserValues, 'wallet_access_list', []);

  const updateAccessList = (field, value) => _updateAccessList(formName, field, value);

  const removeWalletFromList = (acces_id) => {
    const index = access_list.findIndex(wallet => wallet.acces_id === acces_id);

    _removeWalletFromList(formName, acces_id, index);
  };

  const changeFormValue = (field, value) => _changeFormValue(formName, field, value);

  const onUpdate = (wallet, value) => {
    const profil_id = _.get(value, 'id', -1);
    const id_type_profil = _.get(newUserValues, `accessTypeWallet-${wallet.acces_id}.id`, -1);
    const new_access_list = access_list.map(soc => (soc.wallet_id === wallet.wallet_id
      ? {
        ...soc, id_type_profil, profil_id
      }
      : soc));
    const new_wallet_access_list = wallet_access_list.map(soc => (soc.wallet_id === wallet.wallet_id
      ? { ...soc, id_type_profil, profil_id }
      : soc));
    updateAccessList('access_list', new_access_list);
    updateAccessList('wallet_access_list', new_wallet_access_list);
  };

  const walletsAccess = fields.getAll();

  const body = walletsAccess ? walletsAccess.map((wallet, i) => {
    const accessTypeName = `accessTypeWallet-${wallet.acces_id}`;
    const accessProfilName = `accessProfilWallet-${wallet.acces_id}`;
    const { acces_id } = wallet;

    return {
      keyRow: '',
      value: [
        {
          _type: 'string',
          keyCell: 'wallet',
          props: {
            label: wallet.wallet_label,
            labelClassName: classes.bodyLabel
          }
        },
        {
          _type: 'string',
          keyCell: { accessTypeName },
          props: {
            label: (
              <Field
                name={accessTypeName}
                component={UserTypeAutoComplete}
                placeholder=""
                maxMenuHeight={220}
                onChangeValues={() => changeFormValue(accessProfilName, '')}
              />
            )
          }
        },
        {
          _type: 'string',
          keyCell: { accessProfilName },
          props: {
            label: (
              <Field
                name={accessProfilName}
                component={UserProfilAutoComplete}
                placeholder=""
                maxMenuHeight={220}
                id_type={_.get(newUserValues, `${accessTypeName}.id`, '')}
                onChangeValues={(value) => {
                  onUpdate(wallet, value);
                }
                }
              />
            )
          }
        },
        {
          _type: 'string',
          keyCell: 'wallet_removal',
          props: {
            iconRight: 'icon-trash',
            onClick: () => {
              removeWalletFromList(acces_id);
              fields.remove(i);
            }
          },
          cellProp: {
            style: { width: '20px' }
          }
        }
      ]
    };
  }) : [];

  const param = {
    header: {
      row: [
        {
          keyRow: 1,
          value: [
            {
              _type: 'string',
              keyCell: 'wallet',
              props: {
                label: I18n.t('newUserForm.rightManagement.walletList.wallet'),
                typoVariant: 'h4'
              }
            },
            {
              _type: 'string',
              keyCell: 'type',
              props: {
                label: I18n.t('newUserForm.rightManagement.walletList.type'),
                typoVariant: 'h4'
              }
            },
            {
              _type: 'string',
              keyCell: 'profil',
              props: {
                label: I18n.t('newUserForm.rightManagement.walletList.profil'),
                typoVariant: 'h4'
              }
            },
            {
              _type: 'string',
              keyCell: 'wallet_removal',
              cellProp: {
                style: { width: '20px' }
              }
            }
          ]
        }
      ]
    },
    body: {
      props: {},
      row: body
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    param
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TableFactory);
