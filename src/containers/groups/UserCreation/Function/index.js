import { connect } from 'react-redux';
import {
  reduxForm,
  formValueSelector,
  change
} from 'redux-form';
import { updateCurrentFormData } from 'redux/tabs/form/actions';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { newUserFunctionValidation } from 'helpers/validation';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import NewUserFunction from 'components/groups/UserCreation/Function';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);
  const selector = formValueSelector(formName);

  return {
    form: formName,
    user_id: selector(state, 'user_id'),
    physical_person_id: selector(state, 'physical_person_id'),
    id_type_profil: _.get(getCurrentTabState(state), 'user.selectedUser.id_type_profil', -1),
    libelle_type_profil: _.get(getCurrentTabState(state), 'user.selectedUser.libelle_type_profil', -1),
    id_profil: _.get(getCurrentTabState(state), 'user.selectedUser.id_profil', -1),
    libelle_profil: _.get(getCurrentTabState(state), 'user.selectedUser.libelle_profil', -1),
    groupslst: _.get(getCurrentTabState(state), 'user.selectedUser.groupslst', [])
  };
};

const mapDispatchToProps = dispatch => ({
  _changeFormValue: (form, field, value) => dispatch(change(form, field, value)),
  initForm: (formName, data) => dispatch(autoFillFormValues(formName, data)),
  updateCurrentFormData: data => dispatch(updateCurrentFormData(data))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    user_id,
    physical_person_id,
    form,
    id_type_profil,
    libelle_type_profil,
    id_profil,
    libelle_profil,
    groupslst
  } = stateProps;

  const {
    initForm,
    _changeFormValue,
    updateCurrentFormData
  } = dispatchProps;

  const _initForm = data => initForm(form, data);

  const resetGeneralProfil = () => _changeFormValue(form, 'newUserProfil', '');

  const isModificationMode = user_id || physical_person_id || null;

  if (isModificationMode) {
    const newUserType = {
      id: id_type_profil,
      value: libelle_type_profil,
      label: libelle_type_profil
    };

    const newUserProfil = {
      id: id_profil,
      value: libelle_profil,
      label: libelle_profil
    };

    const newUserFunction = groupslst
      ? groupslst.map(func => (
        {
          id: func.id_fonction,
          value: func.fonctions,
          label: func.fonctions
        }
      ))
      : [];

    _initForm({ newUserType });
    _initForm({ newUserProfil });
    _initForm({ newUserFunction });
    updateCurrentFormData({ newUserType, newUserProfil, newUserFunction });
  }

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    resetGeneralProfil
  };
};

const wrappedNewUserFunction = reduxForm({
  destroyOnUnmount: false,
  validate: newUserFunctionValidation
})(NewUserFunction);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrappedNewUserFunction);
