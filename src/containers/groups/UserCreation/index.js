import { connect } from 'react-redux';
import {
  formValueSelector,
  initialize,
  getFormValues,
  reset,
  reduxForm
} from 'redux-form';
import User from 'components/groups/UserCreation';
import actions from 'redux/tabs/user/actions';
import { push } from 'connected-react-router';
import { routesByKey } from 'helpers/routes';
import { newUserValidation } from 'helpers/validation';
import {
  getPersonnePhysiqueList as getPersonnePhysiqueListThunk
} from 'redux/tabs/physicalPersonList';
import { getUserByMail as getUserByMailThunk } from 'redux/tabs/user';
import { closeCurrentForm } from 'redux/tabs/form/actions';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);

  return {
    form: formName,
    formName,
    activeTab: _.get(getCurrentTabState(state), 'user.selectedTab', 0),
    user_id: formValueSelector(formName)(state, 'user_id'),
    selectedUser: _.get(getCurrentTabState(state), 'user.selectedUser'),
    userFormValue: getFormValues(formName)(state)
  };
};

const mapDispatchToProps = dispatch => ({
  onSelectTab: tabIndex => dispatch(actions.selectTab(tabIndex)),
  redirect: route => dispatch(push(route)),
  _initializeUserForm: (form, data) => dispatch(initialize(form, data)),
  goToUserList: () => dispatch(push(routesByKey.userList)),
  _resetForm: formName => dispatch(reset(formName)),
  resetSelectedUser: () => dispatch(actions.resetSelectedUser()),
  getPersonnePhysiqueList: (
    firstname,
    name
  ) => dispatch(getPersonnePhysiqueListThunk(firstname, name)),
  closeCurrentForm: continueCb => dispatch(closeCurrentForm(continueCb)),
  getUserByMail: value => dispatch(getUserByMailThunk(value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    formName,
    form,
    user_id,
    userFormValue,
    selectedUser
  } = stateProps;
  const {
    onSelectTab,
    redirect,
    _initializeUserForm,
    resetSelectedUser,
    closeCurrentForm,
    goToUserList,
    getPersonnePhysiqueList,
    _resetForm,
    getUserByMail
  } = dispatchProps;

  const initializeUserForm = data => _initializeUserForm(formName, data);
  const resetForm = () => _resetForm(form);
  const isModificationMode = user_id || null;

  const cancelForm = () => {
    closeCurrentForm(() => {
      resetSelectedUser();
      initializeUserForm();
      onSelectTab(0);
      redirect(routesByKey.userList);
    });
  };

  const onSubmitUser = async (values) => {
    try {
      const listOfPersonnePhysiqueData = await getPersonnePhysiqueList({
        firstname: values.firstname,
        name: values.name
      });
      const { array_pers_physique } = listOfPersonnePhysiqueData;

      if (!_.isEmpty(array_pers_physique)) {
        return array_pers_physique.map(person => ({
          value: person.pers_physique_id,
          firstname: _.get(person, 'firstname'),
          lastname: _.get(person, 'name'),
          element_1: _.get(_.get(person, 'coordonnee', []).find(coord => coord.type.label === 'Mail'), 'value', ''),
          element_2: _.get(_.get(person, 'coordonnee', []).find(coord => coord.type.label === 'Tel. fixe'), 'value', ''),
          element_3: _.get(person, 'date_birth', null) === '0000-00-00' ? null : _.get(person, 'date_birth', null)
        }));
      }
      return null;
    } catch (error) {
      console.log(error); //eslint-disable-line
      return error;
    }
  };

  const onCheckMail = async () => {
    if (selectedUser) {
      return null;
    }
    try {
      const listOfUserData = await getUserByMail(_.get(userFormValue, 'mail', ''));
      return listOfUserData;
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
    return true;
  };

  const onCancel = () => {
    closeCurrentForm(() => {
      resetSelectedUser();
      resetForm();
      onSelectTab(0);
      goToUserList();
    });
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    cancelForm,
    selectedUser,
    isModificationMode,
    onCheckMail,
    onCancel,
    onSubmitUser
  };
};

const WrapperNewUser = reduxForm({
  validate: newUserValidation,
  destroyOnUnmount: false
})(User);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WrapperNewUser);
