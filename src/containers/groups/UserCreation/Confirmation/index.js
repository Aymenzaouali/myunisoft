import { connect } from 'react-redux';
import Confirmation from 'components/groups/UserCreation/Confirmation';
import { push } from 'connected-react-router';
import { routesByKey } from 'helpers/routes';
import actions from 'redux/tabs/user/actions';
import { formValueSelector, reset } from 'redux-form';
import { closeCurrentForm } from 'redux/tabs/form/actions';
import { tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);

  return {
    formName,
    user_id: formValueSelector(formName)(state, 'user_id')
  };
};

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  closeDialog: () => dispatch(actions.closeDialog()),
  _resetForm: form => dispatch(reset(form)),
  closeCurrentForm: continueCb => dispatch(closeCurrentForm(continueCb)),
  resetSelectedUser: () => dispatch(actions.resetSelectedUser()),
  selectTab: tabIndex => dispatch(actions.selectTab(tabIndex))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { formName } = stateProps;
  const {
    push, closeDialog, _resetForm, selectTab,
    resetSelectedUser, closeCurrentForm
  } = dispatchProps;

  const resetForm = () => _resetForm(formName);

  const toUserList = async () => {
    closeCurrentForm(async () => {
      await resetForm(formName);
      await resetSelectedUser();
      await selectTab(0);
      closeDialog();
      push(routesByKey.userList);
    });
  };

  const toNewUser = async () => {
    closeCurrentForm(async () => {
      await resetForm(formName);
      await resetSelectedUser();
      await selectTab(0);
      closeDialog();
      push(routesByKey.userCreation);
    });
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    primaryRedirection: toNewUser,
    secondaryRedirection: toUserList,
    resetForm
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Confirmation);
