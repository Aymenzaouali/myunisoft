import { connect } from 'react-redux';
import UserRightManagement from 'components/groups/UserCreation/RightManagement';
import {
  getFormValues, reduxForm, change, reset,
  formValueSelector
} from 'redux-form';
import actions from 'redux/tabs/user/actions';
import {
  getUserList as getUserListThunk,
  createUser as createUserThunk,
  putUser as putUserThunk
} from 'redux/tabs/user';
import { handleFormError } from 'helpers/error';
import { quitCurrentForm } from 'redux/tabs/form/actions';
import { routesByKey } from 'helpers/routes';
import { push } from 'connected-react-router';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);
  const selector = formValueSelector(formName);
  const currentTabState = getCurrentTabState(state);

  return {
    form: formName,
    societies: _.get(state, 'navigation.societies', []),
    initial_access_list: _.get(currentTabState, 'user.selectedUser.access_list', []),
    initial_society_list: _.get(currentTabState, 'user.selectedUser.society_access_list', []),
    initial_wallet_list: _.get(currentTabState, 'user.selectedUser.wallet_access_list', []),
    newUserValues: getFormValues(formName)(state),
    user_id: selector(state, 'user_id'),
    physical_person_id: selector(state, 'physical_person_id'),
    isOpen: getCurrentTabState(state).user.isConfirmationDialogOpen
  };
};

const mapDispatchToProps = dispatch => ({
  createUser: user => dispatch(createUserThunk(user)),
  _changeFormValue: (form, field, value) => dispatch(change(form, field, value)),
  _resetForm: form => dispatch(reset(form)),
  putUser: user => dispatch(putUserThunk(user)),
  getUserList: () => dispatch(getUserListThunk()),
  resetSelectedUser: () => dispatch(actions.resetSelectedUser()),
  quitCurrentForm: () => dispatch(quitCurrentForm()),
  goToUserList: () => dispatch(push(routesByKey.userList)),
  selectTab: payload => dispatch(actions.selectTab(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    form: formName,
    newUserValues,
    user_id,
    physical_person_id,
    initial_access_list,
    initial_society_list,
    initial_wallet_list
  } = stateProps;

  const {
    createUser,
    putUser,
    getUserList,
    resetSelectedUser,
    quitCurrentForm,
    selectTab,
    goToUserList
  } = dispatchProps;

  const {
    _changeFormValue,
    _resetForm
  } = dispatchProps;

  const changeFormValue = (field, value) => _changeFormValue(formName, field, value);
  const resetForm = () => _resetForm(formName);

  const isModificationMode = user_id || physical_person_id || null;
  const selectedSocieties = _.get(newUserValues, 'societies', false);

  const onSubmitUser = async (values) => {
    if (!_.isEmpty(selectedSocieties)) {
      const error = { code: 'ArgumentError', message: 'society_without_permission' };
      handleFormError(error);
    } else {
      try {
        if (!isModificationMode) {
          await createUser(values);
        } else {
          await putUser(values);
        }
        await resetForm();
        await resetSelectedUser();
        quitCurrentForm(); // NOT WORKING WHEN CREATING
      } catch (error) {
        console.log(error); // eslint-disable-line
      }
    }
  };

  const onCancel = async () => {
    await resetForm();
    resetSelectedUser();
    quitCurrentForm();
    goToUserList();
    await getUserList();
    selectTab(0);
  };

  const resetGranted = async () => {
    if (isModificationMode) {
      changeFormValue('access_list', initial_access_list);
      changeFormValue('society_access_list', initial_society_list);
      changeFormValue('wallet_access_list', initial_wallet_list);
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmitUser,
    resetGranted,
    onCancel,
    isModificationMode
  };
};

const wrappedUserRightManagement = reduxForm({
  destroyOnUnmount: false
})(UserRightManagement);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(wrappedUserRightManagement);
