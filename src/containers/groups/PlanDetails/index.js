import { connect } from 'react-redux';
import { compose } from 'redux';
import { initialize, reset } from 'redux-form';

import { splitWrap } from 'context/SplitContext';
import PlanDetailsScreen from 'components/screens/PlanDetails';
import { deletePlanDetails as deletePlanDetailsThunk } from 'redux/planDetails';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

import _ from 'lodash';
import I18n from 'i18next';

const mapStateToProps = (state) => {
  const { id: societyId } = state.navigation;
  const tableName = I18n.t('accountingPlans.tables.planDetails.title');
  return {
    societyId,
    tableName,
    currentCompanyName: state.login.user.prenom,
    planDetails: _.get(state, `planDetails.planDetails.${state.accountingPlans.selectedPlan}`, {}),
    selectedPlanDetails: _.get(state, `planDetails.selectedPlanDetails.${state.accountingPlans.selectedPlan}`, []),
    selectedPlanDetailLine: state.planDetails.selectedLine,
    selectedPlan: state.accountingPlans.selectedPlan,
    planDetail: _.get(state, `planDetails.planDetails.${state.accountingPlans.selectedPlan}.list.${state.planDetails.selectedLine}`, {}),
    plansComptableTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  deletePlanDetails: () => dispatch(deletePlanDetailsThunk()),
  changeValue: (a, w, e) => dispatch(initialize(a, w, e)),
  initializeForm: data => dispatch(autoFillFormValues('planDetailForm', data)),
  resetPlanDetailForm: () => dispatch(reset('planDetailForm'))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { plansComptableTableData, currentCompanyName, tableName } = stateProps;

  const dataToExport = formatTableDataToCSVData(plansComptableTableData);
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName
  };
};

const enhance = compose(
  splitWrap,
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(PlanDetailsScreen);
