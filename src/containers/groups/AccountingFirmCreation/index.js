import { connect } from 'react-redux';
import AccountingFirmCreation from 'components/groups/AccountingFirmCreation';
import AccountingFirmSettingsService from 'redux/accountingFirmSettings';
import {
  getFormValues,
  initialize,
  reduxForm,
  reset
} from 'redux-form';
import { ACCOUNTING_FIRM_TOP_MAIN_TABLE, getTableName } from 'assets/constants/tableName';
import { tabFormName } from 'helpers/tabs';
import { splitWrap } from 'context/SplitContext';
import _ from 'lodash';
import {
  validateAccountingFirmGeneralInfo
} from 'helpers/validation';
import { compose } from 'redux';
import tableActions from 'redux/tables/actions';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, ACCOUNTING_FIRM_TOP_MAIN_TABLE);
  const form = tabFormName('accountingFirmCreationForm', society_id);
  const formValues = getFormValues(form)(state);
  return {
    form,
    formValues,
    tableName,
    topTableLastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    topTableData: _.get(state, `tables.${tableName}.data`),
    activeTabId: state.accountingFirmSettings.activeTabId,
    isCreateNew: state.accountingFirmSettings.isCreateNew,
    selected: _.get(state, `tables.AccountingFirmTopMainTable-${society_id}.lastSelectedRow`, false)
  };
};

const mapDispatchToProps = dispatch => ({
  initializeForm: (data = {}, form) => dispatch(initialize(form, data)),
  setCreateNewStatus: isCreate => dispatch(
    AccountingFirmSettingsService.setCreateNewStatus(isCreate)
  ),
  sendMember: () => dispatch(
    AccountingFirmSettingsService.sendMember()
  ),
  _resetForm: form => dispatch(reset(form)),
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  changeMember: member_id => dispatch(
    AccountingFirmSettingsService.changeMember(member_id)
  ),
  setActiveTab: tabId => dispatch(
    AccountingFirmSettingsService.setActiveTab(tabId)
  ),
  getMembers: () => dispatch(
    AccountingFirmSettingsService.getMembers()
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    form,
    topTableData,
    isCreateNew,
    topTableLastSelectedRow,
    tableName,
    activeTabId
  } = stateProps;
  const {
    split: { args: { lastSelectedRow }, closeSlave }
  } = ownProps;
  const {
    initializeForm: _initializeForm,
    sendMember,
    changeMember,
    setCreateNewStatus,
    setActiveTab,
    getMembers,
    selectRow,
    _resetForm
  } = dispatchProps;

  const initializeForm = data => _initializeForm(data, form);

  const resetForms = () => {
    initializeForm({}, form);
  };

  const selectedRow = _.find(topTableData,
    row => (topTableLastSelectedRow
      ? row.member_id === topTableLastSelectedRow : row.member_id === lastSelectedRow))
    || undefined;

  const initialValues = selectedRow && !isCreateNew ? {
    siret: selectedRow.siret,
    cabinetName: selectedRow.name,
    address_number: selectedRow.street_number,
    address_bis: selectedRow.repetition_index,
    road_type: selectedRow.typevoie
      && {
        value: _.get(selectedRow, 'typevoie.id_type_voie', null),
        label: _.get(selectedRow, 'typevoie.lib', null)
      },
    street_name: selectedRow.adress,
    addressSupplement: selectedRow.address_complement,
    postalCode: selectedRow.postal_code,
    city: selectedRow.city
      && {
        value: _.get(selectedRow, 'city', null),
        label: _.get(selectedRow, 'city', null)
      },
    email: selectedRow.email,
    phone_number: selectedRow.phone_number,
    fax: selectedRow.fax,
    site: selectedRow.website,
    loginSilae: selectedRow.login_silae,
    ...(!selectedRow.pw_silae_secured && { passwordSilaeConfirm: selectedRow.pw_silae }),
    loginWsSilae: selectedRow.login_ws_silae,
    ...(!selectedRow.pw_ws_silae_secured && { passwordSilaeConfirmed: selectedRow.pw_ws_silae }),
    addressWsSilae: selectedRow.address_ws_silae
  } : {};

  const saveForms = async () => {
    if (!selectedRow) {
      await sendMember();
    } else {
      await changeMember(selectedRow.member_id);
    }
  };

  const goToTab = async (id) => {
    try {
      await setActiveTab(id);
    } catch (err) {
      throw err;
    }
  };

  const validateForms = async () => {
    try {
      await saveForms();
    } catch (err) {
      throw err;
    }
    await getMembers();
    await selectRow(tableName, selectedRow.member_id);
    await setCreateNewStatus(false);
    await goToTab(activeTabId);
    _resetForm(form);
    closeSlave();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    resetForms,
    validateForms,
    pw_silae_secured: selectedRow && selectedRow.pw_silae_secured,
    pw_ws_silae_secured: selectedRow && selectedRow.pw_ws_silae_secured,
    goToTab,
    selectedRow,
    initialValues
  };
};

const withForm = reduxForm({
  destroyOnUnmount: true,
  enableReinitialize: true,
  validate: validateAccountingFirmGeneralInfo
})(AccountingFirmCreation);

const enhance = compose(
  splitWrap,
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )
);

export default enhance(withForm);
