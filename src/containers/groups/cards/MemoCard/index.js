import { connect } from 'react-redux';
import MemoCard from 'components/groups/Card/MemoCard';
import { getMemo as getMemoThunk } from 'redux/dashboard';
import _ from 'lodash';

const mapStateToProps = state => ({
  items: _.get(state, 'dashboardWeb.memoItems', []),
  idSociety: _.get(state, 'navigation.id')
});

const mapDispatchToProps = dispatch => ({
  getMemo: idSociety => dispatch(getMemoThunk(idSociety))
});

export default connect(mapStateToProps, mapDispatchToProps)(MemoCard);
