import { connect } from 'react-redux';
import ManagementCard from 'components/groups/Card/ManagementCard';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  usersInformation: [
    {
      user_id: 1,
      avatar_source: 'https://randomuser.me/api/portraits/women/66.jpg',
      function: 'ExpertComptable',
      completeName: 'Alexandra Berouin',
      first_name: 'Alexandra',
      last_name: 'Berouin',
      spent_time: '3h30'
    },
    {
      user_id: 2,
      avatar_source: 'https://randomuser.me/api/portraits/women/28.jpg',
      function: 'Superviseur',
      user_name: 'Meriem Assad',
      first_name: 'Meriem',
      last_name: 'Assad',
      spent_time: '1h'
    },
    {
      user_id: 3,
      avatar_source: 'https://randomuser.me/api/portraits/men/28.jpg',
      function: 'Collaborateur',
      user_name: 'Thomas Villedière',
      first_name: 'Thomas',
      last_name: 'Villedière',
      spent_time: '30min'
    },
    {
      user_id: 4,
      avatar_source: 'https://randomuser.me/api/portraits/men/26.jpg',
      function: 'Comptable',
      user_name: 'Bernard Miguier',
      first_name: 'Bernard',
      last_name: 'Miguier',
      spent_time: '15min'
    }
  ],
  dataType: {
    type: 'En-cours',
    amount: 10890,
    bills: 4,
    reminders: 7
  }
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ManagementCard);
