import { connect } from 'react-redux';
import _ from 'lodash';

import I18n from 'assets/I18n';

import ChartCard from 'components/groups/Card/ChartCard';

// Component
const mapStateToProps = (state) => {
  const data = _.get(state, 'dashboardWeb.tresorerie');

  return {
    title: I18n.t('dashboard.charts.treasury'),
    data: _.get(data, 'record.year', []),

    isLoading: _.get(data, 'isLoading')
  };
};

export default connect(mapStateToProps)(ChartCard);
