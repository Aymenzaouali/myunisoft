import { connect } from 'react-redux';
import _ from 'lodash';

import I18n from 'assets/I18n';

import PercentageCard from 'components/groups/Card/PercentageCard';

// Component
const mapStateToProps = (state) => {
  const data = _.get(state, 'dashboardWeb.client');

  return {
    title: I18n.t('dashboard.charts.client'),
    data: _.get(data, 'record.year', []),
    over: _.get(state, 'dashboardWeb.client.record.year', []),
    isLoading: _.get(data, 'isLoading', false)
  };
};

export default connect(mapStateToProps)(PercentageCard);
