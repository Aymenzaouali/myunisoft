import { reduxForm } from 'redux-form';
import Page from 'components/screens/WorkingPage/worksheetLikeFNPPage';
import { getProps } from 'containers/groups/WorkingPage';
import I18n from 'assets/I18n';

const ownProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  pageTitle: I18n.t('cap_it.title'),
  buttonTitle: I18n.t('cap_it.tableButton'),
  headerBtn1: I18n.t('cap_it.comment'),
  headerBtn2: I18n.t('cap_it.newEntry'),
  accountFieldLabel: I18n.t('cap_it.accountNumber')
});

const WrappedFilterFormAccountNumber = reduxForm({
  form: 'AccountNumberWorksheet',
  destroyOnUnmount: false,
  enableReinitialize: true
})(Page);

export default getProps('CAP_IT', ownProps)(WrappedFilterFormAccountNumber);
