import { reduxForm } from 'redux-form';
import { getProps } from 'containers/groups/WorkingPage';
import Page from 'components/screens/WorkingPage/worksheetLikeCCAPage';
import I18n from 'assets/I18n';

const ownProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  pageTitle: I18n.t('cca.title'),
  buttonTitle: I18n.t('fnp.tableButton'),
  headerBtn1: I18n.t('fnp.comment'),
  headerBtn2: I18n.t('fnp.newEntry'),
  accountFieldLabel: I18n.t('cca.accountNumber')
});

const WrappedFilterFormAccountNumber = reduxForm({
  form: 'AccountNumberWorksheet',
  destroyOnUnmount: false,
  enableReinitialize: true
})(Page);


export default getProps('CCA', ownProps)(WrappedFilterFormAccountNumber);
