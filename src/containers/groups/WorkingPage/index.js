import _ from 'lodash';
import { formValueSelector, change } from 'redux-form';
import { getCodeTva as getCodeTvaThunk } from 'redux/tva';
import {
  deleteBills,
  getBills,
  editOrCreateBill,
  postGenerate,
  getAccountNumber,
  getComments,
  getGlobalComments,
  addPJ
} from 'redux/workingPage';
import { connect } from 'react-redux';
import chartAccountActions from 'redux/chartAccounts/actions';
import { openDialog } from 'redux/dialogs';
import { getTableName } from 'assets/constants/tableName';
import tables from 'redux/tables/actions';
import { WorksheetCommentBox } from 'containers/groups/Comments';

export const getProps = (pageName, customMergeProps) => {
  const mapStateToProps = (state) => {
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, pageName);
    const formName = 'AccountNumberWorksheet';
    const formValues = formValueSelector(formName)(state, 'accountNumberField');
    const accountNumberList = _.get(state, `workingPage.accountNumber.${society_id}.${pageName}[0]`, []);
    const accountNumber = _.isArray(accountNumberList)
      ? accountNumberList.reduce((acc, account) => (
        account.worksheet_type[0].code === pageName
          ? [...acc, {
            label: `${account.account_ttc.number} - ${account.account_ttc.label}`,
            value: account.id_param_account_worksheet
          }] : acc), [])
      : [];
    const initialValues = {
      accountNumberField: accountNumber
      && accountNumber.length > 0 ? accountNumber[0].value : undefined
    };
    const dossierRevisionId = _.get(state, `tabs.${society_id}.dadp.reviews.selectedId`);
    const reviews = _.get(state, `tabs.${society_id}.dadp.reviews.data`, []);
    const review = reviews.find(r => r.id_dossier_revision === dossierRevisionId);
    const billsList = _.get(state, `tables.${tableName}.data`, []);
    const selectedBills = Object.entries(_.get(state, `tables.${tableName}.selectedRows`, {}))
      .filter(([, value]) => value)
      .map(([key]) => key);

    return {
      society_id,
      tableName,
      codeTva: _.get(state, 'tva.codeTva', []),
      isLoading: _.get(state, `workingPage.billsList[${state.navigation.id}].${pageName}.isLoading`, false),
      isError: _.get(state, `workingPage.selectedBill[${state.navigation.id}].${pageName}.isError`, false),
      id_param_account_worksheet: _.get(state, 'form.AccountNumberWorksheet.values.accountNumberField', 0),
      begginingAccount: _.get(state, `workingPage.accountNumber[${state.navigation.id}].${pageName}[0][0].account_beginning`, ''),
      billsList,
      editedBills: _.get(state, `tables.${tableName}.editedRows`, {}),
      selectedBills,
      paramWorksheetId: _.get(state, `workingPage.accountNumber[${society_id}].${pageName}[0][0].id_param_account_worksheet`),
      selectedBillsCount: _.get(state, `tables.${tableName}.selectedRowsCount`, 0),
      createdBillsCount: _.get(state, `tables.${tableName}.createdRowsCount`, 0),
      createdData: _.get(state, `tables.${tableName}.data`, 0),
      lastSelectedBill: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
      isEditing: _.get(state, `tables.${tableName}.isEditing`, false),
      errors: _.get(state, `tables.${tableName}.errors`, {}),
      accountNumber,
      formValues,
      initialValues,
      billsInfos: _.get(state, `workingPage.billsList[${society_id}].${pageName}`, {}),
      diligence_id: _.get(state, `workingPage.billsList[${state.navigation.id}].${pageName}.diligence_id`),
      startDate: _.get(review, 'start_date') || _.get(state, `workingPage.billsList[${society_id}].${pageName}.dossier_revision_start_date`),
      endDate: _.get(review, 'end_date') || _.get(state, `workingPage.billsList[${society_id}].${pageName}.dossier_revision_end_date`),
      commentCount: _.get(state, `workingPage.billsList[${state.navigation.id}].${pageName}.comment_count`)
    };
  };
  const mapDispatchToProps = dispatch => ({
    loadData: (society_id) => {
      dispatch(getAccountNumber(society_id, pageName));
      dispatch(getCodeTvaThunk());
    },
    changeForm: (form, field, value) => dispatch(change(form, field, value)),
    selectAccount: accountSelected => dispatch(chartAccountActions.selectAccount(accountSelected)),
    deleteBills: (society_id, bills) => dispatch(
      deleteBills(society_id, bills, pageName)
    ),
    addPJ: (file, date) => dispatch(
      addPJ(file, date, pageName)
    ),
    setEditing: (tableName, isEditing) => {
      if (isEditing) {
        dispatch(tables.startEdit(tableName));
      } else {
        dispatch(tables.endEdit(tableName));
      }
    },
    addBill: (society_id, id) => dispatch(
      tables.selectRow(getTableName(society_id, pageName), id)
    ),
    removeBill: (society_id, id) => dispatch(
      tables.unselectRow(getTableName(society_id, pageName), id)
    ),
    addAllBills: (society_id, ids) => dispatch(
      tables.selectAllRows(getTableName(society_id, pageName), ids)
    ),
    resetAllBills: society_id => dispatch(
      tables.unselectAllRows(getTableName(society_id, pageName))
    ),
    setErrors: (tableName, id, errors) => {
      dispatch(tables.setErrors(tableName, id, errors));
    },
    createRow: (tableName) => {
      const generatedId = Date.now();
      return dispatch(tables.createRow(tableName, {
        defaultValue: {
          id: generatedId,
          bill_id: generatedId,
          date: new Date().toISOString().slice(0, 10),
          isCreated: true,
          worksheet_account: {
            id: generatedId
          }
        }
      }));
    },
    setTableData: (tableName, society_id, formattedTableData) => dispatch(
      tables.setTransformedTableData(tableName, society_id, formattedTableData)
    ),
    editRow: (tableName, newBill) => dispatch(tables.editRow(tableName, newBill)),
    cancel: tableName => dispatch(tables.cancel(tableName)),
    getBills: (society_id, params) => dispatch(getBills(
      society_id, params, pageName
    )),
    editOrCreateBill: (society_id, params) => dispatch(
      editOrCreateBill(society_id, params, pageName)
    ),
    postGenerate: (society_id, selectedBills) => dispatch(
      postGenerate(society_id, selectedBills, pageName)
    ),
    openCommentsDialog: dialogProps => dispatch(openDialog(dialogProps)),
    getComments: bill_id => dispatch(getComments(bill_id)),
    getGlobalComments: diligence_id => dispatch(getGlobalComments(diligence_id))
  });

  const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { diligence_id, tableName } = stateProps;
    const {
      getGlobalComments,
      openCommentsDialog,
      setErrors
    } = dispatchProps;
    const { location } = ownProps;

    const getAndOpenComments = async (diligence_id) => {
      if (diligence_id !== undefined) {
        openCommentsDialog({
          body: {
            Component: WorksheetCommentBox,
            props: {
              diligence_id,
              createMode: false,
              getComments: () => {
                getGlobalComments(diligence_id);
              }
            }
          }
        });
      } else {
        openCommentsDialog({
          body: {
            Component: WorksheetCommentBox,
            props: {
              diligence_id,
              createMode: true
            }
          }
        });
      }
    };

    return {
      ...customMergeProps(stateProps, dispatchProps),
      setErrors: (() => (id, errors) => setErrors(tableName, id, errors))(),
      getAndOpenComments: getAndOpenComments.bind(null, diligence_id),
      location
    };
  };

  return connect(mapStateToProps, mapDispatchToProps, mergeProps);
};
