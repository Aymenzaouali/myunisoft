import { reduxForm } from 'redux-form';
import Page from 'components/screens/WorkingPage/worksheetLikeFNPPage';
import { getProps } from 'containers/groups/WorkingPage';
import I18n from 'assets/I18n';

const ownProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  pageTitle: I18n.t('aar.title'),
  buttonTitle: I18n.t('fnp.tableButton'),
  headerBtn1: I18n.t('fnp.comment'),
  headerBtn2: I18n.t('fnp.newEntry'),
  accountFieldLabel: I18n.t('aar.accountNumber')
});

const WrappedFilterFormAccountNumber = reduxForm({
  form: 'AccountNumberWorksheet',
  destroyOnUnmount: false,
  enableReinitialize: true
})(Page);

export default getProps('AAR', ownProps)(WrappedFilterFormAccountNumber);
