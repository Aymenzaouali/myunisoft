import { reduxForm } from 'redux-form';
import Page from 'components/screens/WorkingPage/worksheetLikeFNPPage';
import { getProps } from 'containers/groups/WorkingPage';
import I18n from 'assets/I18n';

const ownProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  pageTitle: I18n.t('cap_cs.title'),
  buttonTitle: I18n.t('cap_cs.tableButton'),
  headerBtn1: I18n.t('cap_cs.comment'),
  headerBtn2: I18n.t('cap_cs.newEntry'),
  accountFieldLabel: I18n.t('cap_cs.accountNumber')
});

const WrappedFilterFormAccountNumber = reduxForm({
  form: 'AccountNumberWorksheet',
  destroyOnUnmount: false,
  enableReinitialize: true
})(Page);

export default getProps('CAP_CS', ownProps)(WrappedFilterFormAccountNumber);
