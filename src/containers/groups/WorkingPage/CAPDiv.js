import { reduxForm } from 'redux-form';
import Page from 'components/screens/WorkingPage/worksheetLikeFNPPage';
import { getProps } from 'containers/groups/WorkingPage';
import I18n from 'assets/I18n';

const ownProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  pageTitle: I18n.t('cap_div.title'),
  buttonTitle: I18n.t('cap_div.tableButton'),
  headerBtn1: I18n.t('cap_div.comment'),
  headerBtn2: I18n.t('cap_div.newEntry'),
  accountFieldLabel: I18n.t('cap_div.accountNumber')
});

const WrappedFilterFormAccountNumber = reduxForm({
  form: 'AccountNumberWorksheet',
  destroyOnUnmount: false,
  enableReinitialize: true
})(Page);

export default getProps('CAP_DIV', ownProps)(WrappedFilterFormAccountNumber);
