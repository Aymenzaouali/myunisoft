import { reduxForm } from 'redux-form';
import Page from 'components/screens/WorkingPage/worksheetLikeFNPPage';
import { getProps } from 'containers/groups/WorkingPage';
import I18n from 'assets/I18n';

const ownProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  pageTitle: I18n.t('cap_int.title'),
  buttonTitle: I18n.t('cap_int.tableButton'),
  headerBtn1: I18n.t('cap_int.comment'),
  headerBtn2: I18n.t('cap_int.newEntry'),
  accountFieldLabel: I18n.t('cap_int.accountNumber')
});

const WrappedFilterFormAccountNumber = reduxForm({
  form: 'AccountNumberWorksheet',
  destroyOnUnmount: false,
  enableReinitialize: true
})(Page);

export default getProps('CAP_INT', ownProps)(WrappedFilterFormAccountNumber);
