import { connect } from 'react-redux';
import { ScanAssociate } from 'components/groups/ScanAssociate';
import { scanAssociateFormValidation } from 'helpers/validation';
import ScanAssociateService from 'redux/scanAssociate';
import * as I18n from 'i18next';
import { reduxForm, reset } from 'redux-form';

const mapDispatchToProps = dispatch => ({
  sendPinCode: () => dispatch(ScanAssociateService.sendPinCode()),
  resetForm: form => dispatch(reset(form))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    resetForm
  } = dispatchProps;
  const title = I18n.t('scanAssociate.title');
  const initialValues = {
    typeOfScan: {
      value: 'dematbox', label: 'dematbox'
    }
  };
  const resetCode = () => resetForm('scanAssociateForm');
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    resetCode,
    initialValues,
    title
  };
};

const WrappedScanAssociate = reduxForm({
  form: 'scanAssociateForm',
  destroyOnUnmount: false,
  enableReinitialize: true,
  validate: scanAssociateFormValidation
})(ScanAssociate);

export default connect(
  null,
  mapDispatchToProps,
  mergeProps
)(WrappedScanAssociate);
