import { connect } from 'react-redux';
import { reduxForm, getFormValues } from 'redux-form';
import _ from 'lodash';
import { newPhysicalPersonValidation } from 'helpers/validation';
import PhysicalPersonCreation from 'components/groups/PhysicalPersonCreation';
import actions from 'redux/tabs/physicalPersonCreation/actions';
import {
  postPersPhysique as postPersPhysiqueThunk,
  putPersPhysique as putPersPhysiqueThunk,
  getFamilyLink as getFamilyLinkThunk,
  getLinkType as getLinkTypeThunk,
  postFamilyLink as postFamilyLinkThunk,
  getDocument as getDocumentThunk,
  postDocument as postDocumentThunk,
  getSocietyPP as getSocietyPPThunk,
  putSocietyAccess as putSocietyThunk,
  getSocietyAccess as getSocietyAccessThunk,
  getEmployee as getEmployeeThunk,
  postEmployee as postEmployeeThunk,
  postAssociate as postAssociateThunk
} from 'redux/tabs/physicalPersonCreation';
import { getSociety as getSocietyThunk } from 'redux/navigation';
import { getPersonnePhysiqueList as getPersonnePhysiqueListThunk } from 'redux/tabs/physicalPersonList';
import { updateCurrentFormData, closeCurrentForm } from 'redux/tabs/form/actions';
import { routesByKey } from 'helpers/routes';
import { push } from 'connected-react-router';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const { physicalPersonCreation } = getCurrentTabState(state);
  const societyId = state.navigation.id;
  return {
    form: tabFormName('physicalPersonCreationForm', societyId),

    activeTab: _.get(physicalPersonCreation, 'selectedTab', 0),
    selectedPhysicalPerson: _.get(physicalPersonCreation, 'selected_physical_person'),
    newlyCreatedPersonId: _.get(physicalPersonCreation, 'newlyCreatedPersonId'),

    userSociety: _.get(state, 'form.userArray.values', []),
    physicalPersonInfo: getFormValues(tabFormName('physicalPersonCreationForm', societyId))(state)
  };
};
const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  getSociety: () => dispatch(getSocietyThunk()),
  selectTab: payload => dispatch(actions.selectTab(payload)),
  resetSelectedPersPhysique: () => dispatch(actions.resetSelectedPersPhysique()),
  getLinkType: () => dispatch(getLinkTypeThunk()),
  createPersPhysique: values => dispatch(postPersPhysiqueThunk(values)),
  putPersPhysique: (personId, values) => dispatch(putPersPhysiqueThunk(personId, values)),
  getPersPhysique: payload => dispatch(getPersonnePhysiqueListThunk(payload)),

  getFamilyLink: personId => dispatch(getFamilyLinkThunk(personId)),
  postFamilyLink: (id, values) => dispatch(postFamilyLinkThunk(id, values)),

  getDocument: personId => dispatch(getDocumentThunk(personId)),

  postDocument: (personId, values) => dispatch(postDocumentThunk(personId, values)),

  get_person_society: personId => dispatch(getSocietyPPThunk(personId)),

  putSociety: (id, addedCompanies) => dispatch(putSocietyThunk(id, addedCompanies)),

  getSocietyAccess: id => dispatch(getSocietyAccessThunk(id)),

  updateCurrentFormData: data => dispatch(updateCurrentFormData(data)),
  closeCurrentForm: continueCb => dispatch(closeCurrentForm(continueCb)),

  getEmployee: id => dispatch(getEmployeeThunk(id)),

  postEmployee: (personId, values) => dispatch(postEmployeeThunk(personId, values)),
  postAssociate: personId => dispatch(postAssociateThunk(personId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedPhysicalPerson,
    activeTab,
    newlyCreatedPersonId,
    physicalPersonInfo
  } = stateProps;

  const {
    selectTab,
    resetSelectedPersPhysique,
    createPersPhysique,
    putPersPhysique,
    postFamilyLink,
    postDocument,
    updateCurrentFormData,
    closeCurrentForm,
    postEmployee,
    postAssociate,
    push,
    getPersPhysique
  } = dispatchProps;

  const pers_physique_id = _.get(selectedPhysicalPerson, 'pers_physique_id');
  const isUser = _.get(selectedPhysicalPerson, 'is_user', false);

  const cancelForm = async () => {
    await closeCurrentForm(() => resetSelectedPersPhysique());
    await push(routesByKey.physicalPersonList);
  };

  const validateForm = async (values) => {
    try {
      if (selectedPhysicalPerson) {
        if (activeTab === 0) { await putPersPhysique(pers_physique_id, values); }
        if (activeTab === 1) { await postFamilyLink(pers_physique_id, _.get(physicalPersonInfo, 'family')); }
        if (activeTab === 2) { await postAssociate(pers_physique_id); }
        if (activeTab === 3) { await postEmployee(pers_physique_id, _.get(physicalPersonInfo, 'society')); }
        if (activeTab === 4) { await _.get(physicalPersonInfo, 'documentInfo').map(e => postDocument(pers_physique_id, e)); }
      } else {
        await createPersPhysique(values);
      }
      await resetSelectedPersPhysique();
      await getPersPhysique();
      await push(routesByKey.physicalPersonList);
    } catch (error) {
      console.log(error); // eslint-disable-line
    }
  };

  const goToNextTab = async (values) => {
    try {
      switch (activeTab) {
      case 0:
        if (selectedPhysicalPerson) {
          await putPersPhysique(pers_physique_id, values);
        } else {
          await createPersPhysique(values);
        }
        updateCurrentFormData(values);
        await selectTab(1);
        break;
      case 1:
        if (selectedPhysicalPerson) {
          await postFamilyLink(pers_physique_id, _.get(physicalPersonInfo, 'family'));
        } else {
          await postFamilyLink(newlyCreatedPersonId, _.get(physicalPersonInfo, 'family'));
        }
        await selectTab(2);
        break;
      case 2:
        if (selectedPhysicalPerson) {
          await postAssociate(pers_physique_id);
        } else {
          await postAssociate(newlyCreatedPersonId);
        }
        await selectTab(3);
        break;
      case 3:
        if (selectedPhysicalPerson) {
          postEmployee(pers_physique_id, _.get(physicalPersonInfo, 'society'));
        } else {
          postEmployee(newlyCreatedPersonId, _.get(physicalPersonInfo, 'society'));
        }
        await selectTab(4);
        break;
      case 4:
        if (selectedPhysicalPerson) {
          _.get(physicalPersonInfo, 'documentInfo').map(e => postDocument(pers_physique_id, e));
        } else {
          _.get(physicalPersonInfo, 'documentInfo').map(e => postDocument(newlyCreatedPersonId, e));
        }
        await selectTab(5);
        break;
      default:
      }
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    goToNextTab,
    cancelForm,
    validateForm,
    isUser
  };
};

const withForm = reduxForm({
  destroyOnUnmount: false,
  validate: newPhysicalPersonValidation
})(PhysicalPersonCreation);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(withForm);
