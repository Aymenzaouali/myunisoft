import { connect } from 'react-redux';
import { change } from 'redux-form';
import EmployeeInfo from 'components/groups/PhysicalPersonCreation/EmployeeInfo';
import {
  getPersonneMoraleAllList as getPersonneMoraleAllListThunk
} from 'redux/tabs/companyList';
import {
  postEmployee as postEmployeeThunk,
  getEmployee as getEmployeeThunk
} from 'redux/tabs/physicalPersonCreation';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import { parseEmployee } from 'helpers/physicalPersonCreation';
import tableActions from 'redux/tables/actions';
import {
  EMPLOYEE_PP_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import { formatTableDataToCSVData } from 'helpers/tableDataToCSV';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const { physicalPersonCreation } = getCurrentTabState(state);
  const tableNameEmployeePP = getTableName(society_id, EMPLOYEE_PP_TABLE_NAME);
  const exportTableName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${EMPLOYEE_PP_TABLE_NAME}`;

  return {
    tableNameEmployeePP,
    exportTableName,
    society_id,
    physicalPersonCreation,
    form: tabFormName('physicalPersonCreationForm', society_id),
    selectedPhysicalPerson: _.get(physicalPersonCreation, 'selected_physical_person'),
    newlyCreatedPersonId: _.get(physicalPersonCreation, 'newlyCreatedPersonId'),
    isEditingEmployeePP: _.get(state, `tables.${tableNameEmployeePP}.isEditing`, false),
    errorsBackEmployee: _.get(state, `tables.${tableNameEmployeePP}.errors.0.updateError`, {}),
    formattedTableData: _.get(state, `tables.transformedTableData[${exportTableName}][${society_id}].params`, {}),
    nbSelectEmployee: _.get(state, `tables.${tableNameEmployeePP}.selectedRowsCount`, 0)
  };
};

const mapDispatchToProps = dispatch => ({
  _reinitAddedSociety: (form, value) => dispatch(change(form, 'addedSociety', value)),
  getEmployee: idPP => dispatch(getEmployeeThunk(idPP)),
  // function table
  createRow: (tableName, item) => dispatch(tableActions.createRow(tableName, item)),
  startEdit: tableName => dispatch(tableActions.startEdit(tableName)),
  cancelEdit: tableName => dispatch(tableActions.cancel(tableName)),
  // Select List
  getCompanyList: (
    societyIdTab, payload, tableName
  ) => dispatch(getPersonneMoraleAllListThunk(societyIdTab, payload, tableName)),
  updateEmployeePP: idPP => dispatch(postEmployeeThunk(idPP))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    tableNameEmployeePP,
    society_id,
    selectedPhysicalPerson,
    newlyCreatedPersonId,
    formattedTableData,
    exportTableName,
    form
  } = stateProps;
  const {
    createRow,
    getCompanyList,
    startEdit,
    cancelEdit,
    _reinitAddedSociety
  } = dispatchProps;

  const loadData = async () => {
    await getCompanyList(society_id, {}, tableNameEmployeePP);
  };

  const reinitAddedSociety = () => {
    _reinitAddedSociety(form, {});
  };

  const addEmployeeSelect = async (societyInfo) => {
    try {
      await createRow(
        tableNameEmployeePP,
        {
          defaultValue: parseEmployee(societyInfo)
        }
      );
      await getCompanyList(society_id, {}, tableNameEmployeePP);
    } catch (error) {
      console.log(error);
    }
  };

  const transformedEmployeeInfoData = formatTableDataToCSVData(formattedTableData);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    idPP: selectedPhysicalPerson ? selectedPhysicalPerson.pers_physique_id : newlyCreatedPersonId,
    startEditEmployeePP: startEdit.bind(null, tableNameEmployeePP),
    cancelEditEmployeePP: cancelEdit.bind(null, tableNameEmployeePP),
    disabledEmployeeInfoExport: _.isEmpty(transformedEmployeeInfoData),
    csvEmployeeInfoData: transformedEmployeeInfoData,
    baseEmployeeInfoFileName: exportTableName,
    addEmployeeSelect,
    loadData,
    reinitAddedSociety
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(EmployeeInfo);
