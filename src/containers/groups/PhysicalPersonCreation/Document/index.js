import { connect } from 'react-redux';
import DocumentInfo from 'components/groups/PhysicalPersonCreation/Document';
import { arrayPush } from 'redux-form';
import { tabFormName } from 'helpers/tabs';

const mapStateToProps = state => ({
  societyId: state.navigation.id
});
const mapDispatchToProps = dispatch => ({
  addDocument: (id, file) => dispatch(
    arrayPush(tabFormName('physicalPersonCreationForm', id),
      'documentInfo',
      file)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(DocumentInfo);
