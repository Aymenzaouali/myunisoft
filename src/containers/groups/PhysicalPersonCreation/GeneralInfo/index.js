import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import GeneralInfo from 'components/groups/PhysicalPersonCreation/GeneralInfo';
import {
  getCivility as getCivilityThunk,
  getMaritalSituation as getMaritalSituatioThunk,
  getMaritalRegime as getMaritalRegimeThunk,
  getPersPhysiqueType as getPersPhysiqueTypeThunk,
  getSignatoryFunction as getSignatoryFunctionThunk,
  getEmploymentFunction as getEmploymentFunctionThunk
} from 'redux/tabs/physicalPersonCreation';
import {
  getRoadTypes as getRoadTypesThunk
} from 'redux/tabs/companyCreation';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const { physicalPersonCreation, companyCreation } = getCurrentTabState(state);
  const selector = formValueSelector(tabFormName('physicalPersonCreationForm', state.navigation.id));
  return {
    civility_selected: selector(state, 'civility_id'),
    marital_situation_selected: selector(state, 'marital_situation_id'),
    person_type_selected: selector(state, 'physical_person_type_id'),
    civility_type: _.get(physicalPersonCreation, 'civilityTypes'),
    maritalSituation_type: _.get(physicalPersonCreation, 'maritalSituationTypes'),
    maritalRegime_type: _.get(physicalPersonCreation, 'maritalRegimeTypes'),
    physicalPerson_type: _.get(physicalPersonCreation, 'physicalPersonTypes'),
    road_type: _.get(companyCreation, 'roadTypes')
  };
};

const mapDispatchToProps = dispatch => ({
  getCivility: () => dispatch(getCivilityThunk()),
  getMaritalSituation: () => dispatch(getMaritalSituatioThunk()),
  getMaritalRegime: () => dispatch(getMaritalRegimeThunk()),
  getPersPhysiqueType: () => dispatch(getPersPhysiqueTypeThunk()),
  getRoadTypes: () => dispatch(getRoadTypesThunk()),
  getSignatoryFunction: () => dispatch(getSignatoryFunctionThunk()),
  getEmploymentFunction: () => dispatch(getEmploymentFunctionThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    civility_type,
    maritalSituation_type,
    maritalRegime_type,
    physicalPerson_type,
    road_type
  } = stateProps;

  const {
    getCivility,
    getMaritalSituation,
    getMaritalRegime,
    getPersPhysiqueType,
    getRoadTypes,
    getSignatoryFunction,
    getEmploymentFunction
  } = dispatchProps;

  const formatArray = e => ({
    label: e.name,
    value: e.id
  });

  const civility_type_list = civility_type.map(formatArray);
  const maritalSituation_type_list = maritalSituation_type.map(formatArray);
  const maritalRegime_type_list = maritalRegime_type.map(formatArray);
  const physicalPerson_type_list = physicalPerson_type.map(formatArray);
  const road_type_list = road_type.map(formatArray);

  const getFormInformation = () => {
    getCivility();
    getMaritalSituation();
    getMaritalRegime();
    getPersPhysiqueType();
    getRoadTypes();
    getSignatoryFunction();
    getEmploymentFunction();
  };

  const comp_list = [
    { label: 'None', value: 1 },
    { label: 'Bis', value: 2 },
    { label: 'Ter', value: 3 },
    { label: 'Quater', value: 4 },
    { label: 'Quinquies', value: 5 }
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    civility_type_list,
    maritalSituation_type_list,
    maritalRegime_type_list,
    physicalPerson_type_list,
    road_type_list,
    getFormInformation,
    comp_list
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(GeneralInfo);
