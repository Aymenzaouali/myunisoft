import { connect } from 'react-redux';
import { change } from 'redux-form';
import SocietyInfo from 'components/groups/PhysicalPersonCreation/Society';
import {
  getPersonneMoraleAllList as getPersonneMoraleAllListThunk
} from 'redux/tabs/companyList';
import {
  postAssociate as postAssociateThunk,
  getSocietyPP as getSocietyPPThunk
} from 'redux/tabs/physicalPersonCreation';
import _ from 'lodash';
import { parseSociety } from 'helpers/physicalPersonCreation';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import tableActions from 'redux/tables/actions';
import {
  SOCIETY_PP_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import { formatTableDataToCSVData } from 'helpers/tableDataToCSV';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const { physicalPersonCreation } = getCurrentTabState(state);
  const tableNameSocietyPP = getTableName(society_id, SOCIETY_PP_TABLE_NAME);
  const exportTableName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${SOCIETY_PP_TABLE_NAME}`;
  return {
    tableNameSocietyPP,
    exportTableName,
    society_id,
    physicalPersonCreation,
    form: tabFormName('physicalPersonCreationForm', society_id),
    selectedPhysicalPerson: _.get(physicalPersonCreation, 'selected_physical_person'),
    newlyCreatedPersonId: _.get(physicalPersonCreation, 'newlyCreatedPersonId'),
    isEditingSocietyPP: _.get(state, `tables.${tableNameSocietyPP}.isEditing`, false),
    errorsBackSociety: _.get(state, `tables.${tableNameSocietyPP}.errors.0.updateError`, {}),
    formattedTableData: _.get(state, `tables.transformedTableData[${exportTableName}][${society_id}].params`, {}),
    nbSelectSociety: _.get(state, `tables.${tableNameSocietyPP}.selectedRowsCount`, 0)
  };
};

const mapDispatchToProps = dispatch => ({
  _reinitAddedSociety: (form, value) => dispatch(change(form, 'addedSociety', value)),
  getSocietyPP: idPP => dispatch(getSocietyPPThunk(idPP)),
  // function table
  createRow: (tableName, item) => dispatch(tableActions.createRow(tableName, item)),
  startEdit: tableName => dispatch(tableActions.startEdit(tableName)),
  cancelEdit: tableName => dispatch(tableActions.cancel(tableName)),
  // Select List
  getCompanyList: (
    societyIdTab, payload, tableName
  ) => dispatch(getPersonneMoraleAllListThunk(societyIdTab, payload, tableName)),
  updateSocietyPP: idPP => dispatch(postAssociateThunk(idPP))
});


const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    tableNameSocietyPP,
    society_id,
    errorsBackSociety,
    selectedPhysicalPerson,
    newlyCreatedPersonId,
    form,
    formattedTableData,
    exportTableName
  } = stateProps;
  const {
    createRow,
    getCompanyList,
    startEdit,
    cancelEdit,
    _reinitAddedSociety
  } = dispatchProps;

  const loadData = async () => {
    await getCompanyList(society_id, {}, tableNameSocietyPP);
  };

  const reinitAddedSociety = () => {
    _reinitAddedSociety(form, {});
  };

  const addSocietySelect = async (societyInfo) => {
    try {
      await createRow(
        tableNameSocietyPP,
        {
          defaultValue: parseSociety(societyInfo)
        }
      );
      await getCompanyList(society_id, {}, tableNameSocietyPP);
    } catch (error) {
      console.log(error); // eslint-disable-line
    }
  };

  const errorPart = errorsBackSociety.message === 'social_part_pp';

  const transformedSocietyInfoData = formatTableDataToCSVData(formattedTableData);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    idPP: selectedPhysicalPerson ? selectedPhysicalPerson.pers_physique_id : newlyCreatedPersonId,
    startEditSocietyPP: startEdit.bind(null, tableNameSocietyPP),
    cancelEditSocietyPP: cancelEdit.bind(null, tableNameSocietyPP),
    disabledSocietyInfoExport: _.isEmpty(transformedSocietyInfoData),
    csvSocietyInfoData: transformedSocietyInfoData,
    baseSocietyInfoFileName: exportTableName,
    addSocietySelect,
    loadData,
    errorPart,
    reinitAddedSociety
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(SocietyInfo);
