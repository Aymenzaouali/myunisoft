import { connect } from 'react-redux';
import SocietyInfo from 'components/groups/PhysicalPersonCreation/User';
import { getUser as getUserThunk } from 'redux/tabs/user';
import actionsUser from 'redux/tabs/user/actions';
import actionsPP from 'redux/tabs/physicalPersonCreation/actions';
import { closeCurrentForm } from 'redux/tabs/form/actions';
import { push } from 'connected-react-router';
import { routesByKey } from 'helpers/routes';
import { reset } from 'redux-form';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const { physicalPersonCreation } = getCurrentTabState(state);
  const societyId = state.navigation.id;

  return {
    societyId,
    selectedPersPhysique: _.get(physicalPersonCreation, 'selected_physical_person', {})
  };
};

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  resetForm: form => dispatch(reset(form)),
  getUser: user_id => dispatch(getUserThunk(user_id)),
  selectTabUser: idTab => dispatch(actionsUser.selectTab(idTab)),
  resetSelectedUser: () => dispatch(actionsUser.resetSelectedUser()),
  resetSelectedPersPhysique: () => dispatch(actionsPP.resetSelectedPersPhysique()),
  closeCurrentForm: continueCb => dispatch(closeCurrentForm(continueCb))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    push,
    getUser,
    selectTabUser,
    closeCurrentForm,
    resetForm,
    resetSelectedPersPhysique,
    resetSelectedUser
  } = dispatchProps;

  const {
    selectedPersPhysique,
    societyId
  } = stateProps;

  const formNamePP = tabFormName('physicalPersonCreationForm', societyId);
  const formNameUser = tabFormName('newUser', societyId);

  const redirectToRightManagement = async () => {
    const { user_id } = selectedPersPhysique;
    await resetForm(formNamePP);
    await resetForm(formNameUser);
    await resetSelectedPersPhysique();
    await resetSelectedUser();
    await closeCurrentForm(async () => {
      await push(routesByKey.userList);
      await selectTabUser(1);
      await getUser(user_id);
    });
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    redirectToRightManagement
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(SocietyInfo);
