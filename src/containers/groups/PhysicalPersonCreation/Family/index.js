import { connect } from 'react-redux';
import FamilyInfo from 'components/groups/PhysicalPersonCreation/Family';
import { getPersonnePhysiqueList as getPersonnePhysiqueListThunk } from 'redux/tabs/physicalPersonList';
import { arrayPush, getFormValues } from 'redux-form';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import { formatTableDataToCSVData } from 'helpers/tableDataToCSV';
import I18n from 'assets/I18n';

const mapStateToProps = (state) => {
  const { physicalPersonList, physicalPersonCreation } = getCurrentTabState(state);
  const society_id = state.navigation.id;
  const exportFamilyInfoTableName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_Contacts`;
  return {
    societyId: state.navigation.id,
    exportFamilyInfoTableName,
    allPhysicalPersons: _.get(physicalPersonList, 'physicalPersonsData.array_pers_physique', []),
    selectedPersPhysique: _.get(physicalPersonCreation, 'selected_physical_person', {}),
    physicalPersonInfo: getFormValues(tabFormName('physicalPersonCreationForm', society_id))(state),
    formattedTableData: _.get(state, `tables.transformedTableData[${exportFamilyInfoTableName}][${society_id}].params`, {})
  };
};
const mapDispatchToProps = dispatch => ({
  getPersonnePhysiqueList: payload => dispatch(getPersonnePhysiqueListThunk(payload)),
  addRelative: (personInfo, id) => {
    dispatch(arrayPush(tabFormName('physicalPersonCreationForm', id), 'family', {
      civility: personInfo.civility,
      firstname: personInfo.firstname,
      name: personInfo.name,
      pers_physique_id_B: personInfo.pers_physique_id_B
    }));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    allPhysicalPersons, exportFamilyInfoTableName, formattedTableData, physicalPersonInfo
  } = stateProps;
  const { getPersonnePhysiqueList } = dispatchProps;

  const arrayofPhysicalPersons = allPhysicalPersons.map(e => ({
    value: e.pers_physique_id,
    label: `${e.firstname} ${e.name}`,
    pers_physique_id_B: e.pers_physique_id,
    firstname: e.firstname,
    name: e.name,
    civility: _.get(e, 'civility.value')
  }));

  const onCustomFocus = async () => {
    if (_.isEmpty(arrayofPhysicalPersons)) {
      await getPersonnePhysiqueList();
    }
  };

  const transformedFamilyInfoData = formatTableDataToCSVData(formattedTableData);

  const headerData = [
    I18n.t('physicalPersonCreation.familyBoundaries_form.genderHeader'),
    I18n.t('physicalPersonCreation.familyBoundaries_form.nameHeader'),
    I18n.t('physicalPersonCreation.familyBoundaries_form.firstnameHeader'),
    I18n.t('physicalPersonCreation.familyBoundaries_form.linkHeader')
  ];

  const transformData = () => {
    const familyData = _.get(physicalPersonInfo, 'family', []);
    const transformed = [headerData];
    familyData.forEach(family => transformed.push([family.civility, family.name, family.firstname, _.get(family, 'link_type.label')]));
    return transformed;
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    arrayofPhysicalPersons,
    disabledFamilyInfoExport: _.isEmpty(transformedFamilyInfoData),
    csvFamilyInfoData: transformData(),
    baseFamilyInfoFileName: exportFamilyInfoTableName,
    onCustomFocus
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FamilyInfo);
