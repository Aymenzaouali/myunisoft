import { connect } from 'react-redux';
import AccountingFirmTabs from 'components/groups/AccountingFirmTabs';
import AccountingFirmSettingsService from 'redux/accountingFirmSettings';

import I18n from 'assets/I18n';

const mapStateToProps = state => ({
  activeTabId: state.accountingFirmSettings.activeTabId
});

const mapDispatchToProps = dispatch => ({
  setActiveTab: tabId => dispatch(
    AccountingFirmSettingsService.setActiveTab(tabId)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { goToTab } = ownProps;
  const { setActiveTab } = dispatchProps;

  const tabs = [
    {
      label: I18n.t('accountingFirmSettings.tabs.generalInfo'),
      id: 0,
      disabled: false,
      onClick: () => setActiveTab(0)
    },
    {
      label: I18n.t('accountingFirmSettings.tabs.letters'),
      id: 1,
      disabled: true,
      onClick: goToTab
    },
    {
      label: I18n.t('accountingFirmSettings.tabs.silae'),
      id: 2,
      disabled: false,
      onClick: goToTab
    },
    {
      label: I18n.t('accountingFirmSettings.tabs.declare'),
      id: 3,
      step: true,
      disabled: true,
      onClick: goToTab
    }
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    tabs
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AccountingFirmTabs);
