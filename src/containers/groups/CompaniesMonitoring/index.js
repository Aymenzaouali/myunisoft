import { connect } from 'react-redux';
import actions from 'redux/dashboard/actions';
import {
  getTrackingSocieties as getTrackingSocietiesThunk
} from 'redux/dashboard';
import CompaniesMonitoring from 'components/groups/CompaniesMonitoring';
import _ from 'lodash';

const mapStateToProps = state => ({
  currentMode: _.get(state, 'dashboardWeb.currentMode', 1)
});

const mapDispatchToProps = dispatch => ({
  getTrackingSocieties: () => dispatch(getTrackingSocietiesThunk()),
  changeTrackingMode: mode => dispatch(actions.changeTrackingMode(mode)),
  clearSelectedPeriod: () => dispatch(actions.clearSelectedPeriod()),
  clearFilterForVat: () => dispatch(actions.clearFilterForVat()),
  clearFilterForIs: () => dispatch(actions.clearFilterForIs()),
  clearProgressionTableSort: () => dispatch(actions.clearProgressionTableSort())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CompaniesMonitoring);
