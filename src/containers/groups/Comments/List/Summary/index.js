import { connect } from 'react-redux';

import { getCurrentTabState } from 'helpers/tabs';

import { getSummary, addComment, updateComment } from 'redux/tabs/dadp';

import CommentsList from 'components/groups/Comments/List';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);

  return {
    category: dadp.category,
    reviewId: dadp.reviews.selectedId,

    summary: true,
    comments: dadp.summary.data,
    isLoading: dadp.summary.loading,
    isUpdating: dadp.summary.updating
  };
};

const mapDispatchToProps = dispatch => ({
  getSummary: (category, revision_id) => dispatch(getSummary(category, revision_id)),
  addComment: (revision_id, section_code, cycle_id, comment) => dispatch(
    addComment(revision_id, section_code, cycle_id, comment)
  ),
  updateComment: (revision_id, section_code, cycle_id, comment_id, comment) => dispatch(
    updateComment(revision_id, section_code, cycle_id, comment_id, comment)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { category, reviewId } = stateProps;
  const { getSummary, addComment, updateComment } = dispatchProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,

    loadData: () => {
      if (reviewId != null) getSummary(category, reviewId);
    },
    deps: [category, reviewId],

    onAdd: ({ body }, section_code, cycle_id) => addComment(
      reviewId, section_code, cycle_id, body
    ),
    onEdit: ({ id, body }, section_code, cycle_id) => updateComment(
      reviewId, section_code, cycle_id, id, body
    )
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CommentsList);
