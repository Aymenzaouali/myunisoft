import { connect } from 'react-redux';

import { getCurrentTabState } from 'helpers/tabs';

import { getComments, addComment, updateComment } from 'redux/tabs/dadp';

import CommentsList from 'components/groups/Comments/List';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);

  return {
    reviewId: dadp.reviews.selectedId,
    cycleId: dadp.cycles.selectedId,

    comments: dadp.comments.data,
    isLoading: dadp.comments.loading,
    isUpdating: dadp.comments.updating
  };
};

const mapDispatchToProps = dispatch => ({
  getComments: (revision_id, cycle_id) => dispatch(getComments(revision_id, cycle_id)),
  addComment: (revision_id, section_code, cycle_id, comment) => dispatch(
    addComment(revision_id, section_code, cycle_id, comment)
  ),
  updateComment: (revision_id, section_code, cycle_id, comment_id, comment) => dispatch(
    updateComment(revision_id, section_code, cycle_id, comment_id, comment)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { reviewId, cycleId } = stateProps;
  const { getComments, addComment, updateComment } = dispatchProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,

    loadData: () => {
      getComments(reviewId, cycleId);
    },
    deps: [reviewId, cycleId],

    onAdd: ({ body }) => addComment(reviewId, null, cycleId, body),
    onEdit: ({ id, body }) => updateComment(reviewId, null, cycleId, id, body)
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CommentsList);
