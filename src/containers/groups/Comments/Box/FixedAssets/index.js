import { connect } from 'react-redux';
import { CommentBox } from 'components/groups/Comments';
import actions from 'redux/fixedAssets';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;

  return {
    comments: _.get(state, `fixedAssets.${societyId}.comments.fixedAssets.data.data`),
    newEntryComment: _.get(state, 'accountingWeb.comment', '<p></p>')
  };
};


const mapDispatchToProps = (dispatch, ownProps) => ({
  prepareComment: comment => dispatch(actions.prepareCommentForNewEntry(comment)),
  postComment: comment => dispatch(actions.postFixedAssetsComment(comment, ownProps.immo_id)),
  onEdit: comment => dispatch(actions.putComment(ownProps.entryId, comment))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    newEntryComment
  } = stateProps;

  const { createMode } = ownProps;

  const {
    postComment,
    prepareComment
  } = dispatchProps;

  const onSubmit = (comment) => {
    if (createMode) {
      prepareComment(comment);
    } else {
      postComment(comment);
    }
  };

  const defaultBody = createMode ? newEntryComment.body : '<p></p>';

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmit,
    defaultBody
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CommentBox);
