import { connect } from 'react-redux';
import { CommentBox } from 'components/groups/Comments';
import {
  postWorksheetComment,
  postGlobalWorksheetComment,
  putWorksheetComment,
  putGlobalWorksheetComment
} from 'redux/workingPage';
import { closeDialog } from 'redux/dialogs';
import actions from 'redux/accounting/actions';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    comments: _.get(state, `workingPage.billsList.${societyId}.comments`),
    newEntryComment: _.get(state, 'accountingWeb.comment', '<p></p>')
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  prepareComment: comment => dispatch(actions.prepareCommentForNewEntry(comment)),
  postWorksheetComment: async (comment) => {
    dispatch(postWorksheetComment(comment, ownProps.bill_id));
    dispatch(closeDialog());
  },
  onEdit: comment => dispatch(putWorksheetComment(
    comment, ownProps.bill_id
  )),
  postGlobalWorksheetComment: async (comment) => {
    dispatch(postGlobalWorksheetComment(comment, ownProps.diligence_id));
    dispatch(closeDialog());
  },
  onEditGlobalWorksheet: comment => dispatch(putGlobalWorksheetComment(
    comment, ownProps.diligence_id
  ))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    newEntryComment
  } = stateProps;

  const {
    createMode,
    bill_id,
    diligence_id
  } = ownProps;

  const {
    postWorksheetComment,
    postGlobalWorksheetComment,
    prepareComment
  } = dispatchProps;

  const onSubmit = (comment) => {
    if (createMode) {
      prepareComment(comment);
    } else if (bill_id) {
      console.log('bill_id');
      postWorksheetComment(comment, bill_id);
    } else if (diligence_id) {
      console.log('diligence_id');
      postGlobalWorksheetComment(comment, diligence_id);
    }
  };

  const defaultBody = createMode ? newEntryComment.body : '<p></p>';

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmit,
    defaultBody
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CommentBox);
