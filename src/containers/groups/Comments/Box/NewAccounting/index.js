import { connect } from 'react-redux';
import { CommentBox } from 'components/groups/Comments';
import {
  postComment,
  putComment
} from 'redux/accounting';
import actions from 'redux/accounting/actions';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;

  return {
    comments: _.get(state, `accountingWeb.entries[${societyId}].comments`),
    newEntryComment: _.get(state, 'accountingWeb.comment', '<p></p>')
  };
};


const mapDispatchToProps = (dispatch, ownProps) => ({
  postComment: comment => dispatch(postComment(ownProps.entryId, comment)),
  prepareComment: comment => dispatch(actions.prepareCommentForNewEntry(comment)),
  onEdit: comment => dispatch(putComment(ownProps.entryId, comment))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    newEntryComment
  } = stateProps;

  const { createMode } = ownProps;

  const {
    postComment,
    prepareComment
  } = dispatchProps;

  const onSubmit = (comment) => {
    if (createMode) {
      prepareComment(comment);
    } else {
      postComment(comment);
    }
  };

  const defaultBody = createMode ? newEntryComment.body : '<p></p>';

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmit,
    defaultBody
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CommentBox);
