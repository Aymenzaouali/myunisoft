import { connect } from 'react-redux';
import { CommentBox } from 'components/groups/Comments';
import {
  putComment
} from 'redux/accounting';
import actions from 'redux/accounting/actions';
import _ from 'lodash';
import { postAccountsComment } from 'redux/comments';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;

  return {
    comments: _.get(state, `fixedAssets.${societyId}.comments.fixedAssets.data.data`),
    newEntryComment: _.get(state, 'accountingWeb.comment', '<p></p>')
  };
};


const mapDispatchToProps = (dispatch, ownProps) => ({
  postComment: (comment, params) => dispatch(postAccountsComment(comment, params)),
  prepareComment: comment => dispatch(actions.prepareCommentForNewEntry(comment)),
  onEdit: comment => dispatch(putComment(ownProps.entryId, comment))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    newEntryComment
  } = stateProps;

  const {
    createMode, dossier_revision_id,
    account_id
  } = ownProps;

  const {
    postComment,
    prepareComment
  } = dispatchProps;

  const onSubmit = (comment) => {
    if (createMode) {
      prepareComment(comment);
    } else {
      postComment(comment, {
        dossier_revision_id,
        account_id
      });
    }
  };

  const defaultBody = createMode ? newEntryComment.body : '<p></p>';

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmit,
    defaultBody
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CommentBox);
