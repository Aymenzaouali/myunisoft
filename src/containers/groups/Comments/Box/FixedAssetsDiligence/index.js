import { connect } from 'react-redux';
import { CommentBox } from 'components/groups/Comments';
import { closeDialog } from 'redux/dialogs';
import actions from 'redux/fixedAssets';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    comments: _.get(state, `fixedAssets.${societyId}.comments.diligence.data.data`),
    newEntryComment: _.get(state, 'accountingWeb.comment', '<p></p>')
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  prepareComment: comment => dispatch(actions.prepareCommentForNewEntry(comment)),
  postComment: (comment, diligence_id) => dispatch(
    actions.postDiligenceComments(comment, diligence_id)
  ),
  onEdit: comment => dispatch(actions.putComment(comment, ownProps))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    newEntryComment
  } = stateProps;

  const { createMode, diligence_id } = ownProps;

  const {
    postComment,
    prepareComment
  } = dispatchProps;

  const onSubmit = (comment) => {
    if (createMode) {
      prepareComment(comment);
      closeDialog();
    } else {
      postComment(comment, diligence_id);
      closeDialog();
    }
  };

  const defaultBody = createMode ? newEntryComment.body : '<p></p>';

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmit,
    defaultBody
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CommentBox);
