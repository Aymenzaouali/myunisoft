import { connect } from 'react-redux';

import { getDiligenceComments, submitDiligenceComment, editDiligenceComment } from 'redux/tabs/dadp';
import { closeDialog } from 'redux/dialogs';

import { CommentBox } from 'components/groups/Comments';

// Component
const mapDispatchToProps = (dispatch, own) => ({
  getComments: async () => {
    if (typeof own.diligence.comments === 'number') {
      // eslint-disable-next-line no-return-await
      return await dispatch(getDiligenceComments(own.diligence.diligence_id));
    }

    return own.diligence.comments;
  },
  onEdit: async (comment) => {
    await dispatch(editDiligenceComment(own.diligence.diligence_id, comment));
    dispatch(closeDialog());
  },
  onSubmit: async (comment) => {
    await dispatch(submitDiligenceComment(own.diligence.diligence_id, comment));
    dispatch(closeDialog());
  }
});

const mergeProps = (_, dispatch, own) => ({
  ...dispatch,
  ...own,
  createMode: false
});

export default connect(null, mapDispatchToProps, mergeProps)(CommentBox);
