import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm, formValueSelector } from 'redux-form';

import { tabFormName } from 'helpers/tabs';

import ANouveauxOptions from 'components/groups/Forms/ANouveauxOptions';

// Component
const mapStateToProps = (state, ownProps) => {
  const form = tabFormName('aNouveauxOptions', state.navigation.id);
  const selector = formValueSelector(form);

  return {
    form,
    onSubmit: ownProps.onSubmit,
    do_generate: selector(state, 'do_generate')
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false,
    initialValues: {
      do_generate: true,
      details: true
    }
  })
);

export default enhance(ANouveauxOptions);
