import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';
import Dossier from 'components/groups/Forms/Dossier';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import {
  getEDI
} from 'redux/tabs/companyCreation';

const mapStateToProps = (state) => {
  const formName = tabFormName('companyCreationForm', state.navigation.id);
  const formValues = getFormValues(formName)(state);
  const fiscaFolderForm = _.get(formValues, 'fiscal_folder', {});
  const folder_form = _.get(fiscaFolderForm, 'folder', {});
  return {
    bilan: folder_form ? folder_form.sheet_group : '',
    edi: _.get(getCurrentTabState(state), 'companyCreation.edi', []),
    fiscaFolderForm,
    society_id: _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null)
  };
};

const mapDispatchToProps = dispatch => ({
  getEDI: society_id => dispatch(getEDI(society_id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    fiscaFolderForm,
    edi
  } = stateProps;

  const transformedEdi = edi.map(edi => ({ ...edi, value: edi.id_compte_edi, label: edi.mail }));

  const bilan = _.get(fiscaFolderForm, 'other.sheet_group', undefined);
  const renderFullForm = bilan && bilan.value && bilan.value.indexOf('BNC') !== -1;
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    edi: transformedEdi,
    renderFullForm
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Dossier);
