import { connect } from 'react-redux';
import Establishments from 'components/groups/Forms/Establishments';
import actions from 'redux/tabs/companyCreation/actions';
import _ from 'lodash';

const mapStateToProps = state => ({
  selectedEstablishment: _.get(state, 'tabs.collab.companyCreation.selectedEstablishment', '')
});

const mapDispatchToProps = dispatch => ({
  selectEstablishment: payload => dispatch(actions.selectedEstablishment(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedEstablishment
  } = stateProps;
  const { selectEstablishment } = dispatchProps;
  const nbEstablishmentsRef = 0;
  const listEstablishments = [
    {
      value: 'Etablissement 1',
      label: 'Etablissement 1'
    },
    {
      value: 'Etablissement 2',
      label: 'Etablissement 2'
    }
  ];

  const listROFCAE = [
    {
      value: 'ROF CAE 1',
      label: 'ROF CAE 1'
    },
    {
      value: 'ROF CAE 2',
      label: 'ROF CAE 2'
    }
  ];

  const onChange = (event) => {
    selectEstablishment(event.target.value);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    nbEstablishmentsRef,
    listEstablishments,
    selectedEstablishment,
    listROFCAE,
    onChange
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Establishments);
