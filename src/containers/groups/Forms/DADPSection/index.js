import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

import { tabFormName } from 'helpers/tabs';

import { SUMMARY_SECTION } from 'helpers/dadp';
import { setSubCategory } from 'redux/tabs/dadp/actions';

import DADPSection from 'components/groups/Forms/DADPSection';

// Component
const mapStateToProps = state => ({
  form: tabFormName('dadpSection', state.navigation.id)
});

const mapDispatchToProps = dispatch => ({
  resetSubCategory: () => dispatch(setSubCategory(undefined))
});

// Enhance
const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    destroyOnUnmount: false,
    initialValues: {
      section: SUMMARY_SECTION
    }
  })
);

export default enhance(DADPSection);
