import { connect } from 'react-redux';
import NewUser from 'components/groups/Forms/NewUser';
import I18n from 'assets/I18n';
import { newUserValidation } from 'helpers/validation';
import { routesByKey } from 'helpers/routes';
import {
  reduxForm,
  formValueSelector,
  reset
} from 'redux-form';
import {
  getPersonnePhysiqueList as getPersonnePhysiqueListThunk
} from 'redux/tabs/physicalPersonList';
import actions from 'redux/tabs/user/actions';
import { push } from 'connected-react-router';
import { getUserByMail as getUserByMailThunk } from 'redux/tabs/user';
import { tabFormName } from 'helpers/tabs';
import { closeCurrentForm } from 'redux/tabs/form/actions';

const selector = formName => formValueSelector(formName);

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);

  return {
    form: formName,
    civility_code: selector(formName)(state, 'civility_code')
  };
};

const mapDispatchToProps = dispatch => ({
  selectTab: tabIndex => dispatch(actions.selectTab(tabIndex)),
  _resetForm: formName => dispatch(reset(formName)),
  getPersonnePhysiqueList: (firstname, name) => dispatch(getPersonnePhysiqueListThunk(firstname, name)), // eslint-disable-line
  goToUserList: () => dispatch(push(routesByKey.userList)),
  getUserByMail: value => dispatch(getUserByMailThunk(value)),
  resetSelectedUser: () => dispatch(actions.resetSelectedUser()),
  closeCurrentForm: continueCb => dispatch(closeCurrentForm(continueCb))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    isOpen,
    list,
    onCheck,
    mailExist,
    closeUserDialog
  } = ownProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isOpen,
    list,
    onCheck,
    mailExist,
    closeUserDialog,
    warningMessage: [
      {
        message: I18n.t('newUserForm.userCreation.input.mailWarning'),
        type: 'error'
      }
    ],
    genders: [
      {
        id: 1, value: 'Mrs', label: I18n.t('newUserForm.userCreation.input.gender.mrs'), name: 'Mme'
      },
      {
        id: 2, value: 'Mr', label: I18n.t('newUserForm.userCreation.input.gender.mr'), name: 'Mr'
      }
    ]
  };
};

const WrapperNewUser = reduxForm({
  validate: newUserValidation,
  destroyOnUnmount: false
})(NewUser);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WrapperNewUser);
