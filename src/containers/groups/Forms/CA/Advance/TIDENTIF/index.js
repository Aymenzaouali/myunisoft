import { connect } from 'react-redux';
import TIDENTIF from 'components/groups/Forms/CA/Advance/TIDENTIF';
import { reduxForm, getFormValues } from 'redux-form';

const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}_ca12Advance_T-IDENTIF`;
  const tIdentif = getFormValues(formName)(state);

  return {
    form: formName,
    tIdentif
  };
};

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const wrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(TIDENTIF);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrapper);
