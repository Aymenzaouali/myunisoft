import { connect } from 'react-redux';
import CA12AC3514 from 'components/groups/Forms/CA/Advance/CA12AC3514';
import { change, getFormValues, reduxForm } from 'redux-form';

const mapStateToProps = (state) => {
  const ca12AC3514 = `${state.navigation.id}_ca12Advance_3514`;
  const ca12AC3514Form = getFormValues(ca12AC3514)(state);
  return {
    form: ca12AC3514,
    ca12AC3514Form,
    society_id: state.navigation.id
  };
};

const mapDispatchToProps = dispatch => ({
  updateISForm: (society_id, field, value) => dispatch(change(`${society_id}caAdvance`, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const isWrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(CA12AC3514);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(isWrapper);
