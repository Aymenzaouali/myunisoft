import { connect } from 'react-redux';
import CA12AN3517DDR from 'components/groups/Forms/CA/Annual/CA12AN3517DDR';
import { change, getFormValues, reduxForm } from 'redux-form';

const mapStateToProps = (state) => {
  const ca12AN3517 = `${state.navigation.id}_ca12Annual_3517SCA12`;
  const ca12AN3517DDR = `${state.navigation.id}_ca12Annual_3517DDR`;
  const ca12AN3517Form = getFormValues(ca12AN3517)(state);
  const ca12AN3517DDRForm = getFormValues(ca12AN3517DDR)(state);
  return {
    form: ca12AN3517DDR,
    ca12AN3517DDRForm,
    ca12AN3517Form,
    society_id: state.navigation.id
  };
};

const mapDispatchToProps = dispatch => ({
  updateISForm: (society_id, field, value) => dispatch(change(`${society_id}caAnnual`, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const isWrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(CA12AN3517DDR);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(isWrapper);
