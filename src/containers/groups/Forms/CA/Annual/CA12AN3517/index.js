import { connect } from 'react-redux';
import CA12AN3517 from 'components/groups/Forms/CA/Annual/CA12AN3517';
import { change, getFormValues, reduxForm } from 'redux-form';
import _get from 'lodash/get';
import pdfActions from 'redux/pdfForms/actions';
import { tvaValidation } from 'helpers/pdfFormsValidation';

const mapStateToProps = (state) => {
  const ca12AN3517 = `${state.navigation.id}_ca12Annual_3517SCA12`;
  const ca12AN3517Form = getFormValues(ca12AN3517)(state);
  return {
    form: ca12AN3517,
    ca12AN3517Form,
    society_id: state.navigation.id,
    errors: _get(state, `pdfForms.${ca12AN3517}.errors`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  updateISForm: (society_id, field, value) => dispatch(change(`${society_id}caAnnual`, field, value)),
  setError: (formId, id, errors) => dispatch(pdfActions.setError(formId, id, errors)),
  setAllLinesErrors: (formId, errors) => dispatch(pdfActions.setAllLinesErrors(formId, errors))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    ca12AN3517Form
  } = stateProps;
  const {
    setAllLinesErrors
  } = dispatchProps;
  return {
    validateForm: () => {
      setAllLinesErrors(`${society_id}_ca12Annual_3517SCA12`, {});
      try {
        tvaValidation(ca12AN3517Form, 'ca12Annual_3517SCA12');
      } catch (errors) {
        setAllLinesErrors(`${society_id}_ca12Annual_3517SCA12`, errors);
      }
    },
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};

const isWrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(CA12AN3517);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(isWrapper);
