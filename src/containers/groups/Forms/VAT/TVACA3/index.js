import { connect } from 'react-redux';
import TVACA3 from 'components/groups/Forms/VAT/TVACA3';
import { reduxForm, getFormValues, change } from 'redux-form';
import _ from 'lodash';
import pdfActions from 'redux/pdfForms/actions';

const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}tvaca3`;
  const tvaForm = getFormValues(`${state.navigation.id}tvaca3`)(state);
  const tva3310Form = getFormValues(`${state.navigation.id}tva3310`)(state);
  return {
    form: formName,
    tvaForm,
    tva3310Form,
    society_id: state.navigation.id,
    errors: _.get(state, `pdfForms.${formName}.errors`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  updateIdentifForm: (societyId, field, value) => dispatch(change(`${societyId}T_IDENTIF`, field, value)),
  updateTva3519Form: (societyId, field, value) => dispatch(change(`${societyId}tva3519`, field, value)),
  setError: (formId, id, errors) => dispatch(pdfActions.setError(formId, id, errors))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const tvaWrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(TVACA3);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(tvaWrapper);
