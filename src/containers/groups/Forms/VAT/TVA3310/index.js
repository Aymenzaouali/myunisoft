import { connect } from 'react-redux';
import TVA3310 from 'components/groups/Forms/VAT/TVA3310';
import { reduxForm, getFormValues, change } from 'redux-form';
import pdfActions from 'redux/pdfForms/actions';
import _ from 'lodash';
import { tvaValidation } from 'helpers/pdfFormsValidation';


const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}tva3310`;
  const tvaForm = getFormValues(formName)(state);
  const societyId = state.navigation.id;
  const selectedFormId = _.get(state, `cycle.selectedFormId[${societyId}]`, 'tvaca3');
  return {
    form: formName,
    tvaForm,
    selectedFormId,
    societyId,
    errors: _.get(state, `pdfForms.${formName}.errors`, {}),
    tvaFormValues: getFormValues(`${societyId}${selectedFormId}`)(state)
  };
};

const mapDispatchToProps = dispatch => ({
  updateCA3Form: (society_id, field, value) => dispatch(change(`${society_id}tvaca3`, field, value)),
  setError: (formId, id, errors) => dispatch(pdfActions.setError(formId, id, errors)),
  setAllLinesErrors: (formId, errors) => dispatch(pdfActions.setAllLinesErrors(formId, errors))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    tvaFormValues,
    selectedFormId
  } = stateProps;
  const {
    setAllLinesErrors
  } = dispatchProps;
  return {
    validateForm: () => {
      setAllLinesErrors(`${societyId}${selectedFormId}`, {});
      try {
        tvaValidation(tvaFormValues, selectedFormId);
      } catch (errors) {
        setAllLinesErrors(`${societyId}${selectedFormId}`, errors);
      }
    },
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};

const wrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(TVA3310);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrapper);
