import _ from 'lodash';
import { connect } from 'react-redux';
import TVA3519 from 'components/groups/Forms/VAT/TVA3519';
import { reduxForm, getFormValues } from 'redux-form';
import pdfActions from 'redux/pdfForms/actions';
import { tvaValidation } from 'helpers/pdfFormsValidation';
import I18n from 'assets/I18n';

const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}tva3519`;
  const tvaForm = getFormValues(formName)(state);
  const tvaCa3 = getFormValues(`${state.navigation.id}tvaca3`)(state);
  const societyId = state.navigation.id;
  const selectedFormId = _.get(state, `cycle.selectedFormId[${societyId}]`, 'tva3519');
  const errors = _.get(state, `pdfForms.${formName}.errors`, {});

  return {
    form: formName,
    tvaForm,
    selectedFormId,
    societyId,
    errors,
    tvaCa3,
    tvaFormValues: getFormValues(`${societyId}${selectedFormId}`)(state)
  };
};

const mapDispatchToProps = dispatch => ({
  setError: (formId, id, errors) => dispatch(pdfActions.setError(formId, id, errors)),
  setAllLinesErrors: (formId, errors) => dispatch(pdfActions.setAllLinesErrors(formId, errors))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    tvaFormValues,
    selectedFormId
  } = stateProps;
  const {
    setAllLinesErrors
  } = dispatchProps;

  const validateRadio = (values, radioValue, date) => {
    let errors = {};
    if (values) {
      if (radioValue && !date) {
        if (radioValue === 'DI') {
          if (!values.DL && (!values.DM || values.DM === '0')) errors.DL = I18n.t('pdfValidation.tva3310.empty');
          else if (values.DM && (!values.DL || values.DL === '0')) errors.DL = I18n.t('pdfValidation.tva3310.empty');
          else if (!values.DL || values.DL === '0') errors.DL = I18n.t('pdfValidation.tva3310.empty');
        } else if (radioValue === 'DJ') {
          if (!values.DM && (!values.DL || values.DL === '0')) errors.DM = I18n.t('pdfValidation.tva3310.empty');
          else if (values.DL && (!values.DM || values.DM === '0')) errors.DM = I18n.t('pdfValidation.tva3310.empty');
          else if (!values.DM || values.DM === '0') errors.DM = I18n.t('pdfValidation.tva3310.empty');
        }
      } else if (!radioValue && date) {
        if (values.DEMAND === 'DI') {
          if (!date.DL && (!values.DM || values.DM === '0')) errors.DL = I18n.t('pdfValidation.tva3310.empty');
          else if (values.DM && (!values.DL || values.DL === '0')) errors.DL = I18n.t('pdfValidation.tva3310.empty');
          else if (date.DL === '' || !date.DL) errors.DL = I18n.t('pdfValidation.tva3310.empty');
          else if (date.DL) errors = {};
        } else if (values.DEMAND === 'DJ') {
          if (!date.DM && (!values.DL || values.DL === '0')) errors.DM = I18n.t('pdfValidation.tva3310.empty');
          else if (values.DL && (!values.DM || values.DM === '0')) errors.DM = I18n.t('pdfValidation.tva3310.empty');
          else if (date.DM === '' || !date.DM) errors.DM = I18n.t('pdfValidation.tva3310.empty');
          else if (date.DM) errors = {};
        }
      }
    }
    setAllLinesErrors(`${societyId}${selectedFormId}`, errors);
  };

  return {
    validateForm: () => {
      setAllLinesErrors(`${societyId}${selectedFormId}`, {});
      try {
        tvaValidation(tvaFormValues, selectedFormId);
      } catch (errors) {
        setAllLinesErrors(`${societyId}${selectedFormId}`, errors);
      }
    },
    validateRadio,
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};

const wrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(TVA3519);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrapper);
