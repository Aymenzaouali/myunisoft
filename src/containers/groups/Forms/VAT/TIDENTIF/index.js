import { connect } from 'react-redux';
import TIDENTIF from 'components/groups/Forms/VAT/TIDENTIF';
import { reduxForm, getFormValues } from 'redux-form';

const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}T_IDENTIF`;
  const tvaForm = getFormValues(formName)(state);

  const formCA3 = getFormValues(`${state.navigation.id}tvaca3`)(state);
  return {
    form: formName,
    tvaForm,
    formCA3
  };
};

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const wrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(TIDENTIF);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(wrapper);
