import { connect } from 'react-redux';
import Buildings from 'components/groups/Forms/Buildings';
import I18n from 'assets/I18n';
import actions from 'redux/tabs/companyCreation/actions';
import _ from 'lodash';

const mapStateToProps = state => ({
  selectedBuilding: _.get(state, 'companyCreation.selectedBuilding'),
  disabledButton: _.get(state, 'companyCreation.disabledButton')
});

const mapDispatchToProps = dispatch => ({
  onClickNewBuilding: payload => dispatch(actions.selectBuilding(payload)),
  cancelFormBuilding: payload => dispatch(actions.cancelBuilding(payload))
});


const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { disabledButton } = stateProps;
  const {
    onClickNewBuilding,
    cancelFormBuilding
  } = dispatchProps;
  const nbBuildings = 1;
  const landSystemList = [
    {
      value: 'Propriété 1',
      label: 'Propriété 1'
    },
    {
      value: 'Propriété 2',
      label: 'Propriété 2'
    }
  ];
  const schemaList = [
    {
      value: 'dispositif 1',
      label: 'dispositif 1'
    },
    {
      value: 'dispositif 2',
      label: 'dispositif 2'
    },
    {
      value: 'dispositif',
      label: 'dispositif'
    }
  ];

  const radioNatureList = [
    {
      value: I18n.t('companyCreation.buildings.radioNature.IR'),
      label: I18n.t('companyCreation.buildings.radioNature.IR'),
      name: I18n.t('companyCreation.buildings.radioNature.IR')
    },
    {
      value: I18n.t('companyCreation.buildings.radioNature.P'),
      label: I18n.t('companyCreation.buildings.radioNature.P'),
      name: I18n.t('companyCreation.buildings.radioNature.P')
    },
    {
      value: I18n.t('companyCreation.buildings.radioNature.AP'),
      label: I18n.t('companyCreation.buildings.radioNature.AP'),
      name: I18n.t('companyCreation.buildings.radioNature.AP')
    },
    {
      value: I18n.t('companyCreation.buildings.radioNature.M'),
      label: I18n.t('companyCreation.buildings.radioNature.M'),
      name: I18n.t('companyCreation.buildings.radioNature.M')
    },
    {
      value: I18n.t('companyCreation.buildings.radioNature.A'),
      label: I18n.t('companyCreation.buildings.radioNature.A'),
      name: I18n.t('companyCreation.buildings.radioNature.A')
    }
  ];

  const onClick = (payload) => {
    onClickNewBuilding(payload);
  };


  const disabled = disabledButton;

  const cancelBuilding = (payload) => {
    cancelFormBuilding(payload);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    nbBuildings,
    landSystemList,
    schemaList,
    radioNatureList,
    onClick,
    disabled,
    cancelBuilding
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Buildings);
