import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

import { getCurrentTabState, tabFormName } from 'helpers/tabs';

import DADPReviewInfo from 'components/groups/Forms/DADPReviewInfo';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);

  const { reviews } = dadp;
  const review = reviews.data.find(r => r.id_dossier_revision === reviews.selectedId);

  return {
    type: review && review.type,

    form: tabFormName('dadpReviewInfo', state.navigation.id),
    initialValues: review && {
      type: review.type,
      start_date: review.start_date,
      end_date: review.end_date
    }
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    enableReinitialize: true
  })
);

export default enhance(DADPReviewInfo);
