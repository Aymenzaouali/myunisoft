import { connect } from 'react-redux';
import IS2572 from 'components/groups/Forms/IS/Liquidation/IS2572';
import { change, getFormValues, reduxForm } from 'redux-form';

const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}_isSolde_2572`;
  const formLiquidationIS2752 = getFormValues(formName)(state);
  return {
    form: formName,
    formLiquidationIS2752,
    society_id: state.navigation.id
  };
};

const mapDispatchToProps = dispatch => ({
  updateISForm: (society_id, field, value) => dispatch(change(`${society_id}_isSolde_2572`, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const isWrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(IS2572);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(isWrapper);
