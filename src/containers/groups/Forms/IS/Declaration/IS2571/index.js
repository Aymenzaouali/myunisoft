import { connect } from 'react-redux';
import IS2571 from 'components/groups/Forms/IS/Declaration/IS2571';
import { change, getFormValues, reduxForm } from 'redux-form';

function guessBoolean(value) {
  return [1, '1', true].includes(value);
}
/* eslint-disable no-return-assign */

const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}_isAdvance_2571`;
  const isForm = getFormValues(formName)(state);

  // convert "1"/"0" checkbox values to true/false
  if (isForm) {
    ['CHEQUE', 'TELEPAIEMENT', 'VIREMENT', 'DA', 'DB'].map(
      item => isForm[item] = guessBoolean(isForm[item])
    );
  }

  return {
    form: formName,
    isForm,
    society_id: state.navigation.id
  };
};

const mapDispatchToProps = dispatch => ({
  updateISForm: (society_id, field, value) => dispatch(change(`${society_id}_isAdvance_2571`, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const isWrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(IS2571);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(isWrapper);
