import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';
import { get as _get } from 'lodash';

import Decloyer from 'components/groups/Forms/RentDeclarations/Decloyer';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}_rentDeclarations_DECLOYER`;
  const landTenure = _get(state, `rentDeclarations.landTenure.${state.navigation.id}.list`, []);
  const selectedFormId = _get(state, `rentDeclarations.selectedForm.${state.navigation.id}.id_sheet`, null);
  const ediLIst = _get(landTenure.filter(item => item.id_sheet === selectedFormId), '[0].edi_list', []);
  const CAList = _get(ediLIst.filter((item => item.code === 'CA')), '[0].tab', []);

  return {
    form: formName,
    decloyerForm: getFormValues(formName)(state),
    CAList: CAList.map(edi => ({
      value: edi.code_edi,
      label: edi.label_edi
    }))
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false,
    enableReinitialize: true
  })
);

export default enhance(Decloyer);
