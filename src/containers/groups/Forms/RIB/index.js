import { connect } from 'react-redux';
import RIB from 'components/screens/RIB';
import _ from 'lodash';
import actions from 'redux/rib/actions';
import {
  deleteRib as deleteRibThunk
} from 'redux/rib';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

const mapStateToProps = (state) => {
  const { id: society_id, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === society_id));
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    tableName,
    currentCompanyName: _.get(currentTab, 'label'),
    formatted_rib_table_data: _.get(state, `tables.transformedTableData[${tableName}][${society_id}].params`, {}),
    selectedRibs: _.get(state, `rib.selectedRib[${state.navigation.id}]`, []),
    ribData: _.get(state, 'rib.ribSelected', {}),
    society_id: state.navigation.id,
    ribList: _.get(state, 'rib.ribList', [])
  };
};

const mapDispatchToProps = dispatch => ({
  deleteRib: (
    ribList,
    ribDeleteList
  ) => dispatch(deleteRibThunk(ribList, ribDeleteList)),
  initializeRib: data => dispatch(autoFillFormValues('ribEdit', data)),
  resetAllRib: society_id => dispatch(actions.resetAllRib(society_id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    ribList,
    selectedRibs,
    ribData,
    tableName,
    society_id,
    formatted_rib_table_data,
    currentCompanyName
  } = stateProps;
  const {
    deleteRib,
    initializeRib,
    resetAllRib
  } = dispatchProps;

  const resetSelectRib = () => {
    resetAllRib(society_id);
  };

  const ribDelete = () => {
    deleteRib(ribList, selectedRibs);
  };
  const baseRIBFileName = createFormattedFileName(currentCompanyName, tableName);
  const dataToExport = formatTableDataToCSVData(formatted_rib_table_data);


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    selectedRibs,
    ribDelete,
    ribList,
    ribData,
    initializeRib,
    resetSelectRib,
    csvData: dataToExport,
    baseFileName: baseRIBFileName,
    disabledExport: _.isEmpty(dataToExport)
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RIB);
