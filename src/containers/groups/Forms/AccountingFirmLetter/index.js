import { connect } from 'react-redux';
import AccountingFirmLetter from 'components/groups/Forms/AccountingFirmLetter';
import {
  addDocuments
} from 'redux/document/actions';
import _ from 'lodash';

const mapStateToProps = (state, {
  match
}) => {
  const id = _.get(match, 'params.id', undefined);
  const societies = _.get(state, 'navigation.societies', []);
  const collabSociety = _.get(state, 'documentWeb.collabSociety', {});
  const idSelectedSociety = _.get(state, 'navigation.id', []);
  const isCollab = id === 'collab';
  const society = isCollab ? collabSociety : societies.find(
    s => parseInt(s.society_id, 10) === parseInt(idSelectedSociety, 10)
  );
  return ({
    society
  });
};

const mapDispatchToProps = ({
  addDocuments
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountingFirmLetter);
