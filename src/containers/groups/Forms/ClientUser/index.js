import { connect } from 'react-redux';
import _ from 'lodash';
import I18n from 'assets/I18n';
import ClientUser from 'components/groups/Forms/ClientUser';
import {
  getClientUser as getClientUserThunk,
  getUserTypeOptions
} from 'redux/tabs/companyCreation/index';
import { CLIENT_USER_TABLE_NAME } from 'assets/constants/tableName';
import { handleFormError } from 'helpers/error';
import { getCurrentTabState } from 'helpers/tabs';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import { reduxForm, initialize } from 'redux-form';
import { compose } from 'redux';
import { USER_TYPES } from 'assets/constants/data';

const FORM_NAME = 'clientUserForm';

const mapStateToProps = (state) => {
  const { tabs } = state.navigation;
  const societyId = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id');
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${CLIENT_USER_TABLE_NAME}`;
  return {
    societyId,
    tableName,
    currentCompanyName: _.get(currentTab, 'label', I18n.t('companyList.default')),
    clientUserInfo: _.get(getCurrentTabState(state), 'companyCreation.clientUserList'),
    userTypes: _.get(getCurrentTabState(state), 'companyCreation.userTypes', {}),
    selectedCompany: _.get(getCurrentTabState(state), 'companyList.selectedCompany'),
    formattedUserExportTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  getClientUser: payload => dispatch(getClientUserThunk(payload)),
  getUserTypeOptions: type => dispatch(getUserTypeOptions(type)),
  initializeData: (data) => {
    dispatch(initialize(FORM_NAME, data));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    tableName,
    formattedUserExportTableData,
    currentCompanyName,
    userTypes,
    selectedCompany
  } = stateProps;
  const { getClientUser, initializeData } = dispatchProps;

  const loadData = async () => {
    try {
      await getClientUser({ societyId });
    } catch (err) {
      handleFormError(err);
      throw err;
    }
  };

  const accounting_expert_options = userTypes[USER_TYPES.ACCOUNTANT]
    ?.map(item => ({ value: item.id, label: `${item.name} ${item.firstname}` }));
  const manager_options = userTypes[USER_TYPES.RM]
    ?.map(item => ({ value: item.id, label: `${item.name} ${item.firstname}` }));
  const collab_options = userTypes[USER_TYPES.COLLAB]
    ?.map(item => ({ value: item.id, label: `${item.name} ${item.firstname}` }));

  const dataToExport = formatTableDataToCSVData(formattedUserExportTableData);

  const initialValues = selectedCompany ? {
    expert: accounting_expert_options
      ?.find(expert => expert.value === selectedCompany.id_accountant),
    manager: manager_options
      ?.find(expert => expert.value === selectedCompany.id_rm),
    collab: collab_options
      ?.find(expert => expert.value === selectedCompany.id_collab)
  } : {};

  /** set initial values for dropdown if data exist */
  if (selectedCompany) initializeData(initialValues);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disabledClientUserExport: _.isEmpty(dataToExport),
    csvClientUserData: dataToExport,
    initialValues,
    accounting_expert_options,
    manager_options,
    collab_options,
    baseClientUserFileName: createFormattedFileName(currentCompanyName, tableName),
    loadData
  };
};

const WrappedClientUser = reduxForm({
  form: FORM_NAME,
  destroyOnUnmount: false,
  enableReinitialize: false
})(ClientUser);

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )
);

export default enhance(WrappedClientUser);
