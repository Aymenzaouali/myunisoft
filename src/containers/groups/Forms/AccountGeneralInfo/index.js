import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';

import AccountGeneralInfo from 'components/groups/Forms/AccountGeneralInfo';

const mapStateToProps = state => ({
  newAccountForm: getFormValues('newAccount')(state),
  selectedAccount: state.chartAccountsWeb.accountSelected
});

export default connect(mapStateToProps)(AccountGeneralInfo);
