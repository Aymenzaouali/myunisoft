import { connect } from 'react-redux';
import CVAE1329DEF from 'components/groups/Forms/CVAE/Solde/CVAE1329DEF';
import { reduxForm, getFormValues, change } from 'redux-form';
import { getCurrentTabState } from 'helpers/tabs';
import { get as _get } from 'lodash';

const defaultExercise = 'N';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const { exercises } = getCurrentTabState(state);
  const form = `${society_id}_cvaeSolde_1329DEF`;
  const cvaeForm = getFormValues(form)(state);
  const tax2059EForm = getFormValues(`${society_id}taxDeclaration2059E`)(state);
  const tax2033EForm = getFormValues(`${society_id}taxDeclaration2033E`)(state);
  const tax2035EForm = getFormValues(`${society_id}taxDeclaration2035E`)(state);
  return {
    form,
    cvaeForm,
    tax2059EForm,
    tax2033EForm,
    tax2035EForm,
    headerValues: getFormValues(`${society_id}BundleHeader`)(state),
    exercises: exercises ? exercises.map(exercise => exercise) : []
  };
};

const mapDispatchToProps = dispatch => ({
  updateCVAEForm: (society_id, field, value) => dispatch(change(`${society_id}_cvaeSolde_1329DEF`, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { exercises, headerValues } = stateProps;
  const exercise = _get(headerValues, 'exercise', defaultExercise);
  const currentYearExercise = exercises.find(exerciseItem => exerciseItem.label === exercise) || {};
  return {
    currentYearExercise,
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};

const cvaeWrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(CVAE1329DEF);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(cvaeWrapper);
