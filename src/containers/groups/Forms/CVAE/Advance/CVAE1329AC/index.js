import { connect } from 'react-redux';
import CVAE1329AC from 'components/groups/Forms/CVAE/Advance/CVAE1329AC';
import { reduxForm, getFormValues, change } from 'redux-form';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const form = `${society_id}_cvaeAdvance_1329AC`;
  const cvae1329ACForm = getFormValues(form)(state);
  return {
    form,
    cvae1329ACForm
  };
};

const mapDispatchToProps = dispatch => ({
  updateCVAEACForm: (society_id, field, value) => dispatch(change(`${society_id}_cvaeAdvance_1329AC`, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const cvaeWrapper = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true
})(CVAE1329AC);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(cvaeWrapper);
