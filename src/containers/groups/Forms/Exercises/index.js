import { connect } from 'react-redux';
import _ from 'lodash';
import {
  updateExercises as updateExercisesThunk
} from 'redux/tabs/companyCreation';
import Exercises from 'components/groups/Forms/Exercises';
import { change, formValueSelector } from 'redux-form';
import { defaultExercises } from 'redux/tabs/companyCreation/utils';
import { getTabState, tabFormName } from 'helpers/tabs';
import { resetImport } from 'redux/tabs/import/actions';
import { withSnackbar } from 'notistack';

const mapStateToProps = (state) => {
  const tabId = state.navigation.id;
  const formName = tabFormName('companyCreationForm', tabId);

  return {
    formName,
    selectedCompanyId: _.get(getTabState(state, tabId), 'companyList.selectedCompany.society_id', tabId),
    formExercisesValues: formValueSelector(formName)(state, 'exercises')
  };
};

const mapDispatchToProps = dispatch => ({
  updateExercises: () => dispatch(updateExercisesThunk()),
  setExercises: (data, form) => dispatch(change(form, 'exercises', data)),
  setFecExercice: data => dispatch(change('importFecFile', 'exercice', data)),
  resetImport: societyId => dispatch(resetImport(societyId)),
  autofillCurrentExercise: (index, exercice, start_date, end_date, form) => dispatch(change(form, `exercises[${index}]`, {
    ...exercice, start_date, end_date, duration: 12
  }))
});


const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { formName, formExercisesValues, selectedCompanyId } = stateProps;
  const {
    setExercises: _setExercises, autofillCurrentExercise: autoFill, resetImport
  } = dispatchProps;
  const usedData = formExercisesValues || defaultExercises;

  const setExercises = data => _setExercises(data, formName);
  const autofillCurrentExercise = (
    index, exercice, start_date, end_date
  ) => autoFill(index, exercice, start_date, end_date, formName);

  setExercises(usedData);

  const sortedExercises = usedData.reduce((result, exercise, i) => {
    result[exercise.closed ? 'passive' : 'active'].push({
      key: `exercises[${i}]`,
      i,
      ...exercise
    });
    return result;
  }, { active: [], passive: [] });

  let exercises = sortedExercises;

  if (sortedExercises.active.length && sortedExercises.passive.length) {
    let isExerciceFilled = sortedExercises.active
      .some(item => item.label === 'N' && item.start_date && item.end_date);

    if (!isExerciceFilled) {
      sortedExercises.active[0].disabled = true;
    }

    sortedExercises.passive = sortedExercises.passive.map((exercise) => {
      const disabled = !isExerciceFilled;
      if ((!exercise.start_date || !exercise.end_date) && isExerciceFilled) {
        isExerciceFilled = false;
      }

      return {
        ...exercise,
        disabled
      };
    });

    exercises = {
      active: sortedExercises.active,
      passive: sortedExercises.passive
    };
  }

  const onDialogEvent = () => {
    resetImport(selectedCompanyId);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    exercises,
    setExercises,
    onDialogEvent,
    autofillCurrentExercise,
    usedData
  };
};

const connectedExercices = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Exercises);

export default withSnackbar(connectedExercices);
