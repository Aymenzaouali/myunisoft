import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2033E from 'components/groups/Forms/TaxDeclaration/Tax2033E';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2033E`;
  const taxDeclarationForm = getFormValues(formName)(state);
  const form2033B = getFormValues(`${state.navigation.id}taxDeclaration2033B`)(state);

  return {
    form: formName,
    taxDeclarationForm,
    form2033B,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2033E);
