import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';


import Tax2056 from 'components/groups/Forms/TaxDeclaration/Tax2056';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2056`;

  return {
    form: formName,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2056);
