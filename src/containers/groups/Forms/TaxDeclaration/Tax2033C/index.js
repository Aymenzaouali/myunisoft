import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2033C from 'components/groups/Forms/TaxDeclaration/Tax2033C';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2033C`;
  const taxDeclarationForm = getFormValues(formName)(state);

  return {
    form: formName,
    taxDeclarationForm,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2033C);
