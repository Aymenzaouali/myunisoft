import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2058B from 'components/groups/Forms/TaxDeclaration/Tax2058B';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2058B`;
  const taxDeclarationForm = getFormValues(formName)(state);
  const form2058A = getFormValues(`${state.navigation.id}taxDeclaration2058A`)(state);

  return {
    form: formName,
    taxDeclarationForm,
    form2058A,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2058B);
