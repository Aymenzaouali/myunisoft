import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2035 from 'components/groups/Forms/TaxDeclaration/Tax2035';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2035`;
  const taxDeclarationForm = getFormValues(formName)(state);
  const form2035B = getFormValues(`${state.navigation.id}taxDeclaration2035B`)(state);

  return {
    form: formName,
    form2035B,
    taxDeclarationForm,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2035);
