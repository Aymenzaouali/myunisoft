import { connect } from 'react-redux';
import { compose } from 'redux';
import { getFormValues, reduxForm } from 'redux-form';
import Tax2033A from 'components/groups/Forms/TaxDeclaration/Tax2033A';

const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2033A`;
  const taxDeclarationForm = getFormValues(formName)(state);
  return {
    form: formName,
    taxDeclarationForm,
    society_id: state.navigation.id
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2033A);
