import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';


import Tax2069A from 'components/groups/Forms/TaxDeclaration/Tax2069A';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2069A`;

  return {
    form: formName,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2069A);
