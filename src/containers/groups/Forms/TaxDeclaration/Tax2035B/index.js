import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2035B from 'components/groups/Forms/TaxDeclaration/Tax2035B';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2035B`;
  const taxDeclarationForm = getFormValues(formName)(state);
  const form2035 = getFormValues(`${state.navigation.id}taxDeclaration2035`)(state);
  const form2035A = getFormValues(`${state.navigation.id}taxDeclaration2035A`)(state);

  return {
    form: formName,
    taxDeclarationForm,
    form2035,
    form2035A,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2035B);
