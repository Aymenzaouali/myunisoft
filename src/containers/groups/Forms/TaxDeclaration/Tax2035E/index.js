import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2035E from 'components/groups/Forms/TaxDeclaration/Tax2035E';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2035E`;
  const form235A = `${state.navigation.id}taxDeclaration2035A`;

  return {
    form: formName,
    form235A,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2035E);
