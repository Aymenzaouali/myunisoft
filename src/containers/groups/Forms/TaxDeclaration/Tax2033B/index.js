import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';
import Tax2033B from 'components/groups/Forms/TaxDeclaration/Tax2033B';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2033B`;
  const taxDeclarationForm = getFormValues(formName)(state);
  const form2033D = `${state.navigation.id}taxDeclaration2033D`;
  const taxForm2033D = getFormValues(form2033D)(state);

  return {
    form: formName,
    taxDeclarationForm,
    taxForm2033D,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2033B);
