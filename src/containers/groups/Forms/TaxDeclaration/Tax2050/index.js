import { connect } from 'react-redux';
import Tax2050 from 'components/groups/Forms/TaxDeclaration/Tax2050';
import { getFormValues, reduxForm } from 'redux-form';
import { compose } from 'redux';

const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2050`;
  const taxDeclarationForm = getFormValues(formName)(state);
  return {
    form: formName,
    taxDeclarationForm,
    society_id: state.navigation.id
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2050);
