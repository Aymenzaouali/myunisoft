import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2054BIS from 'components/groups/Forms/TaxDeclaration/Tax2054BIS';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2054BIS`;

  return {
    form: formName,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false,
    enableReinitialize: true
  })
);

export default enhance(Tax2054BIS);
