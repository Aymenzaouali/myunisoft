import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2065BIS from 'components/groups/Forms/TaxDeclaration/Tax2065BIS';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2065BIS`;
  const taxDeclarationForm = getFormValues(formName)(state);

  return {
    form: formName,
    taxDeclarationForm,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2065BIS);
