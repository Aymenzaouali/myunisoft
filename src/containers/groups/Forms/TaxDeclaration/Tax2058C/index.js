import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2058C from 'components/groups/Forms/TaxDeclaration/Tax2058C';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2058C`;
  const taxDeclarationForm = getFormValues(formName)(state);
  const form2051 = getFormValues(`${state.navigation.id}taxDeclaration2051`)(state);
  const form2052 = getFormValues(`${state.navigation.id}taxDeclaration2052`)(state);

  return {
    form: formName,
    taxDeclarationForm,
    form2051,
    form2052,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2058C);
