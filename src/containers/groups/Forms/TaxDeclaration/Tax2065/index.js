import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import Tax2065 from 'components/groups/Forms/TaxDeclaration/Tax2065';

// Component
const mapStateToProps = (state) => {
  const formName = `${state.navigation.id}taxDeclaration2065`;
  const taxDeclarationForm = getFormValues(formName)(state);
  const form2059E = getFormValues(`${state.navigation.id}taxDeclaration2059E`)(state);
  const form2033B = getFormValues(`${state.navigation.id}taxDeclaration2033B`)(state);

  return {
    form: formName,
    taxDeclarationForm,
    form2059E,
    form2033B,
    formValues: getFormValues(formName)(state)
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(Tax2065);
