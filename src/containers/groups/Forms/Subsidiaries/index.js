import { connect } from 'react-redux';
import Subsidiaries from 'components/groups/Forms/Subsidiaries';
import _ from 'lodash';
import {
  SUBSIDIARY_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import { parseSociety } from 'helpers/companyCreation';
import {
  getPersonneMoraleAllList as getPersonneMoraleAllListThunk,
  getSubsidiariesList as getSubsidiariesListThunk,
  postSubsidiaries as postSubsidiariesThunk
} from 'redux/tabs/companyCreation/index';
import { subsidiariesFilter } from 'assets/constants/data';
import {
  getSignatoryFunction as getSignatoryFunctionThunk,
  getEmploymentFunction as getEmploymentFunctionThunk
} from 'redux/tabs/physicalPersonCreation';
import { getCurrentTabState } from 'helpers/tabs';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

const mapStateToProps = (state) => {
  const { id: society_id } = state.navigation;
  const tableNameSubsidiary = getTableName(society_id, SUBSIDIARY_TABLE_NAME);
  const tableName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${SUBSIDIARY_TABLE_NAME}`;
  return {
    tableNameSubsidiary,
    currentCompanyName: state.login.user.prenom,
    tableName,
    societyId: _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null),
    subsidiaryList: _.get(state, `tables.${tableNameSubsidiary}.data`, []),
    isEditingSubsidiary: _.get(state, `tables.${tableNameSubsidiary}.isEditing`, false),
    nbSelectSubsidiary: _.get(state, `tables.${tableNameSubsidiary}.selectedRowsCount`, 0),
    formattedTableData: _.get(state, `tables.transformedTableData[${tableName}][${society_id}].params`, {}),
    errorsBack: _.get(state, `tables.${tableNameSubsidiary}.errors.0.updateError`, [])
  };
};

const mapDispatchToProps = dispatch => ({
  // Select List
  getMoralPersonList: (
    societyIdTab, payload, tableName
  ) => dispatch(getPersonneMoraleAllListThunk(societyIdTab, payload, tableName)),
  // Data table
  getSubsidiariesList: () => dispatch(getSubsidiariesListThunk()),
  getSignatoryFunction: () => dispatch(getSignatoryFunctionThunk()),
  getEmploymentFunction: () => dispatch(getEmploymentFunctionThunk()),
  // Function table
  createRow: (tableName, item) => dispatch(tableActions.createRow(tableName, item)),
  startEdit: tableName => dispatch(tableActions.startEdit(tableName)),
  cancelEdit: tableName => dispatch(tableActions.cancel(tableName)),
  updateSubsidiaries: tableName => dispatch(postSubsidiariesThunk(tableName))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    subsidiaryList,
    societyId,
    tableNameSubsidiary,
    errorsBack,
    tableName,
    formattedTableData,
    currentCompanyName
  } = stateProps;

  const {
    getSubsidiariesList,
    createRow,
    startEdit,
    cancelEdit,
    getMoralPersonList,
    updateSubsidiaries,
    getEmploymentFunction,
    getSignatoryFunction
  } = dispatchProps;


  const loadData = async () => {
    try {
      await getSubsidiariesList();
      await getMoralPersonList(societyId, {}, tableNameSubsidiary);
      await getSignatoryFunction();
      await getEmploymentFunction();
    } catch (error) {
      console.log(error);
    }
  };

  const nbSubsidiariesRef = subsidiaryList && subsidiaryList.length;

  const addSubsidiarySelect = async (societyInfo) => {
    try {
      await createRow(
        tableNameSubsidiary,
        {
          defaultValue: parseSociety(societyInfo)
        }
      );
      await getMoralPersonList(societyId, {}, tableNameSubsidiary);
    } catch (error) {
      console.log(error);
    }
  };

  const updateSubsidiary = async (tableName) => {
    try {
      await updateSubsidiaries(tableName);
    } catch (error) {
      console.log(error);
    }
  };

  const errorCapital = errorsBack.message === 'no_social_part_society';
  const errorPart = errorsBack.message === 'social_part_pp';
  const dataToExport = formatTableDataToCSVData(formattedTableData);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData,
    nbSubsidiariesRef,
    addSubsidiarySelect,
    updateSubsidiary,
    list: subsidiariesFilter,
    startEditSubsidiary: startEdit.bind(null, tableNameSubsidiary),
    cancelEditSubsidiary: cancelEdit.bind(null, tableNameSubsidiary),
    errorCapital,
    errorPart,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName: createFormattedFileName(currentCompanyName, tableName),
    onfilterChangeSociety: () => {}
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Subsidiaries);
