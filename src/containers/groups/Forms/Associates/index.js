import { connect } from 'react-redux';
import _ from 'lodash';
import {
  getFormValues,
  change
} from 'redux-form';
import Associates from 'components/groups/Forms/Associates';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import {
  getPersonneMoraleAllList as getPersonneMoraleAllListThunk,
  getPersonnePhysiqueAllList as getPersonnePhysiqueAllListThunk,
  getAssociatesList as getAssociatesListThunk,
  postAssociates as postAssociatesThunk
} from 'redux/tabs/companyCreation';
import { associatesFilter } from 'assets/constants/data';
import {
  getSignatoryFunction as getSignatoryFunctionThunk,
  getEmploymentFunction as getEmploymentFunctionThunk
} from 'redux/tabs/physicalPersonCreation';
import { parsePhysicalPerson, parseSociety } from 'helpers/companyCreation';
import {
  PHYSICAL_PERSON_TABLE_NAME,
  LEGAL_PERSON_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import { formatTableDataToCSVData } from 'helpers/tableDataToCSV';
import { enqueueSnackbar } from 'common/redux/notifier/action';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableNamePhysicalPerson = getTableName(society_id, PHYSICAL_PERSON_TABLE_NAME);
  const tableNameLegalPerson = getTableName(society_id, LEGAL_PERSON_TABLE_NAME);
  const formName = tabFormName('companyCreationForm', society_id);
  const formValues = getFormValues(formName)(state);
  const filterPerson = _.get(formValues, 'associates.filterPerson');
  const filterSociety = _.get(formValues, 'associates.filterSociety');
  const basePhysicalPersonFileName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${PHYSICAL_PERSON_TABLE_NAME}`;
  const baseLegalPersonFileName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${LEGAL_PERSON_TABLE_NAME}`;
  return {
    state,
    society_id,
    filterPerson,
    basePhysicalPersonFileName,
    baseLegalPersonFileName,
    filterSociety,
    tableNamePhysicalPerson,
    tableNameLegalPerson,
    societyId: _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null),
    formattedPhysicalPersonTableData: _.get(state, `tables.transformedTableData[${basePhysicalPersonFileName}][${society_id}].params`, {}),
    formattedLegalPersonTableData: _.get(state, `tables.transformedTableData[${baseLegalPersonFileName}][${society_id}].params`, {}),
    formValues,
    associatesList: _.get(getCurrentTabState(state), 'companyCreation.associates', {}),
    associatesPersonList: _.get(state, `tables.${tableNamePhysicalPerson}.data`, []),
    associatesSocietyList: _.get(state, `tables.${tableNameLegalPerson}.data`, []),
    isEditingPhysicalPerson: _.get(state, `tables.${tableNamePhysicalPerson}.isEditing`, false),
    isEditingLegalPerson: _.get(state, `tables.${tableNameLegalPerson}.isEditing`, false),
    nbSelectPhysicalPerson: _.get(state, `tables.${tableNamePhysicalPerson}.selectedRowsCount`, 0),
    nbSelectSociety: _.get(state, `tables.${tableNameLegalPerson}.selectedRowsCount`, 0),
    selectedPhysicalPerson: _.get(state, `tables.${tableNamePhysicalPerson}.selectedRows`, {}),
    selectSociety: _.get(state, `tables.${tableNameLegalPerson}.selectedRows`, {}),
    createdRowsPhysicalPerson: _.get(state, `tables.${tableNamePhysicalPerson}.createdRows`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  enqueueSnackbar: text => dispatch(enqueueSnackbar(text)),
  // closeSnackbar: () => dispatch(closeSnackbar),
  _changeFormValue: (form, field, value) => dispatch(change(form, field, value)),
  // Select List
  getMoralPersonList: (
    societyIdTab, payload, tableName
  ) => dispatch(getPersonneMoraleAllListThunk(societyIdTab, payload, tableName)),
  getPhysicalPersonList: (
    payload, tableName
  ) => dispatch(getPersonnePhysiqueAllListThunk(payload, tableName)),
  // Data tables
  getAssociatesList: payload => dispatch(getAssociatesListThunk(payload)),
  getSignatoryFunction: () => dispatch(getSignatoryFunctionThunk()),
  getEmploymentFunction: () => dispatch(getEmploymentFunctionThunk()),
  // function table
  createRow: (tableName, item) => dispatch(tableActions.createRow(tableName, item)),
  startEdit: tableName => dispatch(tableActions.startEdit(tableName)),
  cancelEdit: tableName => dispatch(tableActions.cancel(tableName)),
  updateAssociates: (type, tableName) => dispatch(postAssociatesThunk(type, tableName))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    associatesPersonList,
    formValues,
    societyId,
    associatesSocietyList,
    associatesList,
    society_id,
    tableNamePhysicalPerson,
    tableNameLegalPerson,
    nbSelectPhysicalPerson,
    formattedPhysicalPersonTableData,
    basePhysicalPersonFileName,
    formattedLegalPersonTableData,
    baseLegalPersonFileName,
    nbSelectSociety
  } = stateProps;


  const {
    getMoralPersonList,
    getPhysicalPersonList,
    getAssociatesList,
    _changeFormValue,
    updateAssociates,
    getSignatoryFunction,
    getEmploymentFunction,
    startEdit,
    cancelEdit,
    createRow
  } = dispatchProps;

  const {
    isLoading,
    isError
  } = associatesList;

  const loadData = async () => {
    await getAssociatesList();
    await getMoralPersonList(societyId, {}, tableNameLegalPerson);
    await getPhysicalPersonList({}, tableNamePhysicalPerson);
    await getSignatoryFunction();
    await getEmploymentFunction();
  };

  const capital = _.get(formValues, 'associates.capital', null);
  const social_part = _.get(formValues, 'associates.social_part', null);
  const nbPhysicalPersonRef = associatesPersonList && associatesPersonList.length;
  const nbSocietyRef = associatesSocietyList && associatesSocietyList.length;

  const changeFormValue = (field, value) => {
    if (field === 'capital') {
      _changeFormValue(tabFormName('companyCreationForm', society_id), `associates.${field}`, value);
      _changeFormValue(tabFormName('companyCreationForm', society_id), 'associates.social_part_value', (value / social_part).toFixed(2));
    } else if (field === 'social_part') {
      _changeFormValue(tabFormName('companyCreationForm', society_id), `associates.${field}`, value);
      _changeFormValue(tabFormName('companyCreationForm', society_id), 'associates.social_part_value', (capital / value).toFixed(2));
    } else {
      _changeFormValue(tabFormName('companyCreationForm', society_id), `associates.${field}`, value);
    }
  };

  const calculValue = _.get(formValues, 'associates.social_part_value', null);

  const addPersonPhysical = async (physicalPersonInfo) => {
    try {
      await createRow(
        tableNamePhysicalPerson,
        {
          defaultValue: parsePhysicalPerson(physicalPersonInfo)
        }
      );
      await getPhysicalPersonList({}, tableNamePhysicalPerson);
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
  };
  const addSocietySelect = async (societyInfo) => {
    try {
      await createRow(
        tableNameLegalPerson,
        {
          defaultValue: parseSociety(societyInfo)
        }
      );
      await getMoralPersonList(societyId, {}, tableNameLegalPerson);
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
  };

  const updateAssociate = async (type, tableName) => {
    try {
      await updateAssociates(type, tableName);
    } catch (error) {
      console.log(error); //eslint-disable-line
    }
  };

  const exportPhysicalPersonData = formatTableDataToCSVData(formattedPhysicalPersonTableData);

  const exportLegalPersonData = formatTableDataToCSVData(formattedLegalPersonTableData);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isLoading,
    isError,
    nbSocietyRef,
    loadData,
    nbPhysicalPersonRef,
    changeFormValue,
    calculValue,
    addPersonPhysical,
    nbSelectPhysicalPerson,
    updateAssociate,
    addSocietySelect,
    societyId,
    nbSelectSociety,
    startEditPhysicalPerson: startEdit.bind(null, tableNamePhysicalPerson),
    startEditLegalPerson: startEdit.bind(null, tableNameLegalPerson),
    cancelEditPhysicalPerson: cancelEdit.bind(null, tableNamePhysicalPerson),
    cancelEditLegalPerson: cancelEdit.bind(null, tableNameLegalPerson),
    associatesPersonList,
    associatesSocietyList,
    list: associatesFilter,
    disabledPhysicalPersonExport: _.isEmpty(exportPhysicalPersonData),
    csvPhysicalPersonData: exportPhysicalPersonData,
    basePhysicalPersonFileName,
    disabledLegalPerson: _.isEmpty(exportLegalPersonData),
    csvLegalPerson: exportLegalPersonData,
    baseLegalPersonFileName,
    onfilterChangeSociety: () => {},
    onfilterChangePerson: () => {}
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Associates);
