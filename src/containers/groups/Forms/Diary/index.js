import { connect } from 'react-redux';
import { getFormValues, change } from 'redux-form';
import {
  getAccount as getAccountThunk
} from 'redux/accounting';
import {
  getDiaryDetail as getDiaryDetailThunk,
  checkDiaryCode as checkDiaryCodeThunk
} from 'redux/diary';
import { DiaryForm } from 'components/groups/Forms';
import _ from 'lodash';

const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  diaryForm: getFormValues('diaryForm')(state),
  diaryDetail: _.get(state, 'diary.diaryDetail', {}),
  diaryCodeAvailable: _.get(state, 'diary.diaryCodeAvailable', true)
});

const mapDispatchToProps = dispatch => ({
  getDiaryDetail: diaryCode => dispatch(getDiaryDetailThunk(diaryCode)),
  isDiaryCodeAvailable: diaryCodeAvailable => dispatch(checkDiaryCodeThunk(diaryCodeAvailable)),
  getAccount: (societyId, q) => dispatch(getAccountThunk(societyId, q)),
  changeForm: (form, field, value) => dispatch(change(form, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    diaryForm
  } = stateProps;

  const {
    isDiaryCodeAvailable,
    changeForm
  } = dispatchProps;

  const diary_type = _.get(diaryForm, 'diary_type');

  const bankType = _.get(diary_type, 'value', '') === 'BQ';
  const cashType = _.get(diary_type, 'value', '') === 'CAISSE';

  const resetDiaryAccountNumber = () => {
    changeForm('diaryForm', 'diary_accountNumber', '');
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    resetDiaryAccountNumber,
    bankType,
    cashType,
    checkDiaryCode: isDiaryCodeAvailable
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DiaryForm);
