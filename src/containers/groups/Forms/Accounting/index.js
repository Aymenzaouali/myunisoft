import { connect } from 'react-redux';
import Accounting from 'components/groups/Forms/Accounting';
import _ from 'lodash';

const mapStateToProps = state => ({
  accountingPlans: _.get(state, 'accountingPlans.plans.list', []).map(p => ({
    value: p.id,
    label: p.description || p.label
  }))
});

const mergeProps = (stateProps) => {
  const { accountingPlans } = stateProps;
  const plansList = [
    ...accountingPlans,
    { value: -1, label: 'Aucun' }
  ];
  return {
    ...stateProps,
    plansList
  };
};

export default connect(mapStateToProps, undefined, mergeProps)(Accounting);
