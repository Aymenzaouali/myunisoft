import { connect } from 'react-redux';
import { TableFactory } from 'components/groups/Tables';
import { getVat as getVatThunk } from 'redux/tva';
import { vatHeaderKeys } from 'assets/constants/keys';
import actions from 'redux/tva/actions';
import I18n from 'assets/I18n';
import tableActions from 'redux/tables/actions';
import _ from 'lodash';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  vats: _.get(state, `tva.vat.${state.navigation.id}`, {}),
  selectedVat: _.get(state, `tva.selectedVat[${state.navigation.id}]`, []),
  vatSelectedLine: state.tva.vatSelectedLine
});

const mapDispatchToProps = dispatch => ({
  getVat: () => dispatch(getVatThunk()),
  addVat: (vatId, societyId) => dispatch(actions.addVat(vatId, societyId)),
  removeVat: (vatId, societyId) => dispatch(actions.removeVat(vatId, societyId)),
  addAllVat: societyId => dispatch(actions.addAllVat(societyId)),
  resetAllVat: societyId => dispatch(actions.resetAllVat(societyId)),
  selectVat: vatSelectedLine => dispatch(actions.selectVat(vatSelectedLine)),
  resetSelectVat: () => dispatch(actions.selectVat('')),
  setVATTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    vats,
    selectedVat,
    vatSelectedLine
  } = stateProps;

  const {
    getVat,
    addVat,
    removeVat,
    selectVat,
    resetAllVat,
    addAllVat,
    setVATTableData
  } = dispatchProps;

  const loadData = async () => {
    await getVat();
  };

  const vatAvailable = _.get(vats, 'list', []).filter(vat => !vat.blocked);

  let isAllSelected = vatAvailable.length > 0;

  vatAvailable.forEach((vat) => {
    if (!selectedVat.includes(vat.vat_param_id)) {
      isAllSelected = false;
    }
  });

  const headers = [{
    keyRow: `${societyId}vatTableRowHeader`,
    value: vatHeaderKeys.map((key, i) => (
      key === 'checkbox'
        ? {
          _type: 'checkbox',
          keyCell: `${societyId}vatTableHeaderCell.${key}${i}`,
          props: {
            checked: isAllSelected,
            disabled: vatAvailable.length === 0,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllVat(societyId);
              } else {
                addAllVat(societyId);
              }
            }
          }
        }
        : {
          _type: 'sortCell',
          keyCell: `${societyId}vatTableHeaderCell.${key}${i}`,
          props: {
            children: I18n.t(`tables.vat.${key}`)
          }
        }))
  }];

  const body = _.get(vats, 'list', []).map((vat, i) => (
    {
      keyRow: `${societyId}vatTableRowBody${i}`,
      props: {
        style: { backgroundColor: vat.vat_param_id === vatSelectedLine ? '#f4f4f4' : '#fff' },
        onClick: () => {
          selectVat(vat.vat_param_id === vatSelectedLine ? null : vat.vat_param_id);
        }
      },
      value: [
        {
          _type: 'checkbox',
          keyCell: `${societyId}vatTableCellBodyCodeCheckBox${i}`,
          props: {
            checked: selectedVat.includes(vat.vat_param_id),
            disabled: vat.blocked,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (selectedVat.includes(vat.vat_param_id)) {
                if (vat.vat_param_id === vatSelectedLine) selectVat(null);
                removeVat(vat.vat_param_id, societyId);
              } else {
                selectVat(vat.vat_param_id);
                addVat(vat.vat_param_id, societyId);
              }
            },
            id: vat.vat_param_id.toString()
          }
        },
        {
          _type: 'string',
          keyCell: `${societyId}vatTableCellBodyCode${i}`,
          props: {
            label: vat.code
          }
        },
        {
          _type: 'string',
          keyCell: `${societyId}vatTableCellBodyDed${i}`,
          props: {
            label: _.get(vat, 'account_ded.account_number', '')
          }
        },
        {
          _type: 'string',
          keyCell: `${societyId}vatTableCellBodyColl${i}`,
          props: {
            label: _.get(vat, 'account_coll.account_number', '')
          }
        },
        {
          _type: 'string',
          keyCell: `${societyId}vatTableCellBodyTaux${i}`,
          props: {
            label: _.get(vat, 'vat.rate', '')
          }
        },
        {
          _type: 'string',
          keyCell: `${societyId}vatTableCellBodyType${i}`,
          props: {
            label: _.get(vat, 'vat_type.label', '')
          }
        },
        {
          _type: 'string',
          keyCell: `${societyId}vatTableCellBodyDue${i}`,
          props: {
            label: _.get(vat, 'vat_exigility.label', '')
          }
        }
      ]
    }
  ));

  const transformedParam = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  const tableName = I18n.t('tva.list');
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData,
    isLoading: vats.isLoading,
    isError: vats.isError,
    param: transformedParam,
    onRefresh: () => setVATTableData(tableName, societyId, transformedParam)
  };
};
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TableFactory);
