import React from 'react';
import { connect } from 'react-redux';

import I18n from 'assets/I18n';
import { status } from 'assets/constants/data';
import { anayticReviewKeys } from 'assets/constants/keys';

import { formatNumber } from 'helpers/number';
import { getCurrentTabState } from 'helpers/tabs';

import { getAnalyticReview, updateAnalyticReview } from 'redux/tabs/dadp';

import { StatusString } from 'components/basics/Strings';
import AnalyticReview from 'components/groups/Tables/AnalyticReview';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';
import { openConsultingPopup } from 'helpers/consulting';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);
  const societyId = state.navigation.id;

  return {
    cycleId: dadp.cycles.selectedId,
    reviewId: dadp.reviews.selectedId,
    analyticReview: dadp.analyticReview,
    societyId
  };
};

const mapDispatchToProps = dispatch => ({
  getAnalyticReview: (review_id, cycle_id) => dispatch(getAnalyticReview(review_id, cycle_id)),

  updateAnalyticReview: (review_id, account_number, type, state, prev_state) => dispatch(
    updateAnalyticReview(review_id, account_number, type, state, prev_state)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { reviewId, analyticReview, societyId } = stateProps;
  const { updateAnalyticReview } = dispatchProps;

  const balanceSheet = analyticReview.data.balance_sheet || {};
  const balanceSheetList = balanceSheet.data || [];

  const accountResults = analyticReview.data.accounts_results || {};
  const accountResultsList = accountResults.data || [];

  const getHeaders = () => [{
    keyRow: 'header',
    value: [...anayticReviewKeys.map(key => ({
      _type: 'string',
      keyCell: key.keyLabel,
      props: {
        isAmount: key.isAmount,
        typoVariant: 'inherit',
        label: I18n.t(`tables.analyticReview.${key.keyLabel}`)
      }
    }))]
  }];

  const renderSelectValue = value => (
    <StatusString status={value} label={I18n.t(`dadp.status.${value}`)} />
  );

  const renderSelectList = option => (
    <StatusString status={option.value} label={option.label} />
  );

  const formatData = data => ({
    keyRow: `${data.account_id}`,
    props: {
      hover: true
    },
    value: [
      {
        _type: 'string',
        keyCell: 'account_number',
        props: {
          label: data.account_number
        },
        cellProp: {
          className: cellStyles.hasLink,
          onClick: () => {
            openConsultingPopup(societyId, { accountId: data.account_id });
          }
        }
      },
      {
        _type: 'string',
        keyCell: 'label',
        props: {
          label: data.label
        }
      },
      {
        _type: 'string',
        keyCell: 'solde_n',
        props: {
          isAmount: true,
          label: formatNumber(data.solde_n),
          noWrap: true
        },
        cellProp: {
          className: cellStyles.hasLink,
          onClick: () => {
            openConsultingPopup(societyId, {
              accountId: data.account_id,
              exerciceLabel: 'N'
            });
          }
        }
      },
      {
        _type: 'string',
        keyCell: 'va',
        props: {
          isAmount: true,
          label: formatNumber(data.solde_n_1),
          noWrap: true
        },
        cellProp: {
          className: cellStyles.hasLink,
          onClick: () => {
            openConsultingPopup(societyId, {
              accountId: data.account_id,
              exerciceLabel: 'N-1'
            });
          }
        }
      },
      {
        _type: 'string',
        keyCell: 'solde_n_1',
        props: {
          isAmount: true,
          label: formatNumber(data.evo),
          noWrap: true
        }
      },
      {
        _type: 'string',
        keyCell: 'evo',
        props: {
          isAmount: true,
          label: formatNumber(data.va),
          noWrap: true
        }
      },
      {
        _type: 'select',
        keyCell: 'valid_collab',
        props: {
          list: status.filter(s => s.value !== 'to_validate'),
          value: data.valid_collab,
          renderValue: value => renderSelectValue(value),
          onChange: (e) => {
            if (e.target.value !== data.valid_collab) {
              updateAnalyticReview(
                reviewId, data.account_id, 'valid_collab', e.target.value, data.valid_collab
              );
            }
          },
          renderList: renderSelectList
        }
      },
      {
        _type: 'select',
        keyCell: 'valid_rm',
        props: {
          list: status,
          value: data.valid_rm,
          renderValue: value => renderSelectValue(value),
          onChange: (e) => {
            if (e.target.value !== data.valid_rm) {
              updateAnalyticReview(
                reviewId, data.account_id, 'valid_rm', e.target.value, data.valid_rm
              );
            }
          },
          renderList: renderSelectList,
          disabled: data.valid_collab !== 'ok'
        }
      },
      {
        _type: 'string',
        keyCell: 'nb_docs',
        props: {
          label: data.nb_docs
        }
      },
      {
        _type: 'string',
        keyCell: 'com',
        props: {
          label: ''
        }
      }
    ]
  });

  const getFooter = data => [{
    keyRow: 'footer',
    value: [
      {
        _type: '',
        keyCell: 'filler1',
        cellProp: {
          colSpan: 2
        }
      },
      {
        _type: 'string',
        keyCell: 'label',
        cellProp: {
          isTotal: true
        },
        props: {
          typoVariant: 'h6',
          label: formatNumber(data.solde_n_total),
          noWrap: true
        }
      },
      {
        _type: 'string',
        keyCell: 'solde_n',
        cellProp: {
          isTotal: true
        },
        props: {
          typoVariant: 'h6',
          label: formatNumber(data.solde_n_1_total),
          noWrap: true
        }
      },
      {
        _type: 'string',
        keyCell: 'va',
        cellProp: {
          isTotal: true
        },
        props: {
          typoVariant: 'h6',
          label: formatNumber(data.evo_total),
          noWrap: true
        }
      },
      {
        _type: 'string',
        keyCell: 'solde_n_1',
        cellProp: {
          isTotal: true
        },
        props: {
          typoVariant: 'h6',
          label: formatNumber(data.va_total),
          noWrap: true
        }
      },
      {
        _type: '',
        keyCell: 'filler2',
        cellProp: {
          colSpan: 4
        }
      }
    ]
  }];

  const balanceSheetBody = balanceSheetList.map(
    data => formatData(data)
  );
  const accountsResultsBody = accountResultsList.map(
    data => formatData(data)
  );

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    balanceSheetTable: {
      param: {
        header: {
          props: {},
          row: getHeaders('balanceSheet')
        },
        body: {
          row: balanceSheetBody
        },
        footer: {
          row: getFooter(balanceSheet, 'balanceSheet')
        }
      },
      isLoading: analyticReview.loading,
      isError: analyticReview.error
    },
    accountsResultsTable: {
      param: {
        header: {
          props: {},
          row: getHeaders('accountResults')
        },
        body: {
          row: accountsResultsBody
        },
        footer: {
          row: getFooter(accountResults, 'accountResults')
        }
      },
      isLoading: analyticReview.loading,
      isError: analyticReview.error
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AnalyticReview);
