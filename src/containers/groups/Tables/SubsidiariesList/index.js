import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import SubsidiaryTable from 'components/groups/Tables/SubsidiaryTable';
import { subsidiaryRefKeys } from 'assets/constants/keys';
import {
  SignatoryFunctionReduxSelect
} from 'containers/reduxForm/Inputs';
import { SUBSIDIARY_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import { tabFormName } from 'helpers/tabs';
import classnames from 'classnames';
import {
  TextFieldCell,
  CheckBoxCell,
  DatePickerCell,
  SortCell
} from 'components/basics/Cells';
import moment from 'moment';
import { getAutoCompleteProps } from 'redux/tables/utils';
import _ from 'lodash';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';


const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const current_society_id = _.get(state, `form.${tabFormName('companyCreationForm', society_id)}.values.society_id`, 0);
  const tableName = getTableName(society_id, SUBSIDIARY_TABLE_NAME);
  return {
    subsidiary_list: _.get(state, `tables.${tableName}.data`, []),
    isEditing: _.get(state, `tables.${tableName}.isEditing`, false),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`),
    society_id: current_society_id,
    navigation_society_id: society_id,
    errors: _.get(state, `tables.${tableName}.errors`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  editRow: (tableName, item) => dispatch(tableActions.editRow(tableName, item)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors)),
  setSubsidiaryTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    subsidiary_list,
    isEditing,
    navigation_society_id,
    selectedRows,
    selectedRowsCount,
    errors
  } = stateProps;

  const {
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows,
    editRow,
    setSubsidiaryTableData,
    setErrors
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const tableName = getTableName(navigation_society_id, SUBSIDIARY_TABLE_NAME);
  const exportTableName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${SUBSIDIARY_TABLE_NAME}`;

  const isAllSelected = subsidiary_list.length === selectedRowsCount;

  const headerKeys = subsidiaryRefKeys;

  const headers = [{
    keyRow: `${society_id}subsidiaryTableHeaderRow0`,
    value: headerKeys.map((data, i) => {
      const { type, keyLabel, isAmount } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: `${society_id}subsidiaryTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = subsidiary_list.map(lp => lp.id);
                selectAllRows(tableName, ids);
              }
            }
          }
        });
      } else {
        cell = {
          component: SortCell,
          keyCell: `${society_id}subsidiaryTableHeaderCell.${keyLabel}`,
          cellProp: {// TODO: Add sticky on theme
            isAmount
          },
          props: {
            children: I18n.t(`companyCreation.subsidiaries.subsidiaryArrayTitle.${keyLabel}`),
            onClick: () => {}
          }
        };
      }
      return cell;
    })
  }];
  const body = subsidiary_list.map((sub) => {
    const { id } = sub;
    const generateKey = (cellName = '') => `subsidiary${cellName}${id}`;
    const isChecked = selectedRows[id] === true;
    const isEditingRow = isChecked && isEditing;
    const startDate = _.get(sub, 'start_date', '');
    const endDate = _.get(sub, 'end_date', '');
    const formattedStartDate = moment(startDate).isValid() ? moment(startDate).format('DD/MM/YYYY') : '';
    const formattedEndDate = moment(endDate).isValid() ? moment(endDate).format('DD/MM/YYYY') : '';
    const subsidiaryErrors = _.get(errors, sub.id, {});
    const capitalNull = _.get(sub, 'society.capital') || _.get(sub, 'capital');
    const handleRowSelect = () => {
      if (isChecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    return {
      keyRow: generateKey(),
      props: {
        hover: true,
        selected: isChecked
      },
      value: [
        {
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: generateKey('CheckBox'),
          props: {
            checked: isChecked,
            onChange: handleRowSelect
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Name'),
          props: {
            children: _.get(sub, 'society.name') || _.get(sub, 'name', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Siret'),
          props: {
            children: _.get(sub, 'society.siret') || _.get(sub, 'siret', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Adress'),
          props: {
            children: _.get(sub, 'society.address') || _.get(sub, 'address', '')
          },
          cellProp: {
            className: cellStyles.multilineCell
          }
        },
        {
          component: isEditingRow ? SignatoryFunctionReduxSelect : 'div',
          keyCell: generateKey('SignatoryFunction'),
          error: _.get(subsidiaryErrors, 'signatory_function'),
          props: isEditingRow
            ? {
              ...getAutoCompleteProps(editRow, sub, 'signatory_function.label', 'signatory_function', tableName),
              onChange: (value) => {
                editRow(tableName, { ...sub, signatory_function: value });
                setErrors(tableName, sub.id, { signatory_function: undefined });
              }
            }
            : {
              children: _.get(sub, 'signatory_function.label', '')
            }
        },
        {
          component: isEditingRow ? DatePickerCell : 'div',
          keyCell: generateKey('StartDate'),
          error: _.get(subsidiaryErrors, 'effective_date'),
          props: isEditingRow ? {
            value: startDate,
            onChange: (value) => {
              editRow(tableName, {
                ...sub,
                start_date: moment(value).format('YYYY-MM-DD')
              });
              setErrors(tableName, sub.id, { effective_date: undefined });
            }
          } : {
            children: formattedStartDate
          }
        },
        {
          component: isEditingRow ? DatePickerCell : 'div',
          keyCell: generateKey('DateEnd'),
          props: isEditingRow ? {
            value: endDate,
            onChange: value => editRow(tableName, {
              ...sub,
              end_date: moment(value).format('YYYY-MM-DD')
            })
          } : {
            children: formattedEndDate
          }
        },
        {
          component: isEditingRow && !!capitalNull ? TextFieldCell : 'div',
          error: _.get(subsidiaryErrors, 'social_part_PP'),
          keyCell: generateKey('NbPP'),
          props: isEditingRow
            ? {
              defaultValue: _.get(sub, 'social_part.PP'),
              placeholder: _.get(sub, 'social_part.PP'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...sub,
                  social_part: {
                    ...sub.social_part,
                    PP: e.target.value
                  }
                });
                setErrors(tableName, sub.id, {
                  social_part_PP: undefined
                });
              }
            }
            : {
              children: _.get(sub, 'social_part.PP', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: isEditingRow && !!capitalNull ? TextFieldCell : 'div',
          error: _.get(subsidiaryErrors, 'social_part_US'),
          keyCell: generateKey('NbUS'),
          props: isEditingRow
            ? {
              defaultValue: _.get(sub, 'social_part.US'),
              placeholder: _.get(sub, 'social_part.US'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...sub,
                  social_part: {
                    ...sub.social_part,
                    US: e.target.value
                  }
                });
                setErrors(tableName, sub.id, {
                  social_part_US: undefined
                });
              }
            } : {
              children: _.get(sub, 'social_part.US', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: isEditingRow && !!capitalNull ? TextFieldCell : 'div',
          error: _.get(subsidiaryErrors, 'social_part_NP'),
          keyCell: generateKey('NbNP'),
          props: isEditingRow
            ? {
              defaultValue: _.get(sub, 'social_part.NP'),
              placeholder: _.get(sub, 'social_part.NP'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...sub,
                  social_part: {
                    ...sub.social_part,
                    NP: e.target.value
                  }
                });
                setErrors(tableName, sub.id, {
                  social_part_NP: undefined
                });
              }
            } : {
              children: _.get(sub, 'social_part.NP', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Percent'),
          props: {
            children: _.get(sub, 'social_part.percent')
          },
          cellProp: {
            className: classnames(cellStyles.numberCell, cellStyles.smallCell)
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Capital'),
          props: {
            children: _.get(sub, 'society.capital') || _.get(sub, 'capital', '')
          },
          cellProp: {
            className: classnames(cellStyles.numberCell, cellStyles.smallCell)
          }
        }
      ]
    };
  });

  const transformedParams = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    setTableData:
      () => setSubsidiaryTableData(exportTableName, navigation_society_id, transformedParams),
    param: transformedParams
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(SubsidiaryTable);
