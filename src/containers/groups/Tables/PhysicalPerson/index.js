import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import classnames from 'classnames';
import PhysicalPerson from 'components/groups/Tables/PhysicalPerson';
import { physicalPersonRefKeys } from 'assets/constants/keys';
import moment from 'moment';
import {
  FunctionReduxSelect,
  SignatoryFunctionReduxSelect
} from 'containers/reduxForm/Inputs';
import _ from 'lodash';
import { tabFormName } from 'helpers/tabs';
import {
  TextFieldCell,
  CheckBoxCell,
  DatePickerCell,
  SortCell
} from 'components/basics/Cells';
import tableActions from 'redux/tables/actions';
import { PHYSICAL_PERSON_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import { getAutoCompleteProps } from 'redux/tables/utils';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const current_society_id = _.get(state, `form.${tabFormName('companyCreationForm', society_id)}.values.society_id`, 0);
  const tableName = getTableName(society_id, PHYSICAL_PERSON_TABLE_NAME);
  return {
    associate_list: _.get(state, `tables.${tableName}.data`, []),
    society_id: current_society_id,
    navigation_society_id: society_id,
    isEditing: _.get(state, `tables.${tableName}.isEditing`, false),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    errors: _.get(state, `tables.${tableName}.errors`, {}),
    initialData: _.get(state, `tables.${tableName}.initialData`)
  };
};

const mapDispatchToProps = dispatch => ({
  editRow: (tableName, item) => dispatch(tableActions.editRow(tableName, item)),
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors)),
  setPhysicalPersonTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    navigation_society_id,
    associate_list,
    selectedRows,
    isEditing,
    lastSelectedRow,
    selectedRowsCount,
    errors,
    initialData
  } = stateProps;

  const tableName = getTableName(navigation_society_id, PHYSICAL_PERSON_TABLE_NAME);

  const {
    selectRow,
    unselectRow,
    editRow,
    selectAllRows,
    unselectAllRows,
    setErrors,
    setPhysicalPersonTableData
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const headerKeys = physicalPersonRefKeys;

  const isAllSelected = selectedRowsCount === associate_list.length;

  const headers = [{
    keyRow: `${society_id}physicalPersonTableHeaderRow0`,
    value: headerKeys.map((data, i) => {
      const { type, keyLabel, isAmount } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: `${society_id}physicalPersonnTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = associate_list.map(al => al.id);
                selectAllRows(tableName, ids);
              }
            }
          }
        });
      } else {
        cell = {
          component: SortCell,
          keyCell: `${society_id}physicalPersonnTableHeaderCell.${keyLabel}${i}`,
          cellProp: {
            isAmount
          },
          props: {
            children: I18n.t(`companyCreation.associates.physicalPersonArrayTitle.${keyLabel}`),
            onClick: () => {}
          }
        };
      }
      return cell;
    })
  }];

  const body = associate_list.map((personPhysical) => {
    const { id } = personPhysical;
    const ischecked = selectedRows[id] === true;
    const handleRowSelect = () => {
      if (ischecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    const startDate = _.get(personPhysical, 'start_date', '');
    const endDate = _.get(personPhysical, 'end_date', '');
    const formattedStartDate = moment(startDate).isValid() ? moment(startDate).format('DD/MM/YYYY') : '';
    const formattedEndDate = moment(endDate).isValid() ? moment(endDate).format('DD/MM/YYYY') : '';
    const isEditingRow = isEditing && ischecked;
    const isLastSelected = lastSelectedRow === id;
    const generateKey = (cellName = '') => `personPhysical${cellName}${id}`;
    const associateErrors = _.get(errors, personPhysical.id, {});
    const ini = initialData.filter(e => e.id === personPhysical.id);
    const dateEndPresente = _.get(ini, 'end_date', '');
    const isCreatedLine = _.get(personPhysical, 'isCreated', false);

    let socialPartsAreEnable = formattedEndDate === '';

    const endDateRender = () => {
      const isEnableCell = ((isEditingRow && !dateEndPresente) && !isCreatedLine);

      const endDateCell = {
        component: isEnableCell ? DatePickerCell : 'div',
        keyCell: generateKey('EndDate'),
        props: isEnableCell ? {
          value: endDate,
          onChange: value => editRow(tableName, {
            ...personPhysical,
            end_date: moment(value).format('YYYY-MM-DD')
          }),
          onBlur: isEnableCell ? (e) => {
            if (e.target.value !== '' && moment(e.target.value).isValid()) {
              socialPartsAreEnable = false;
              editRow(tableName, {
                ...personPhysical,
                social_part: {
                  PP: 0,
                  NP: 0,
                  US: 0,
                  percent: 0
                }
              });
            } else {
              socialPartsAreEnable = true;
              editRow(tableName, {
                ...personPhysical,
                end_date: ''
              });
            }
          } : {}
        } : {
          children: formattedEndDate
        }
      };

      return endDateCell;
    };

    const socialPartIsEditable = (socialPartsAreEnable && isEditingRow);

    return {
      keyRow: generateKey(),
      props: {
        hover: true,
        selected: ischecked
      },
      value: [
        {
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: generateKey('CheckBox'),
          props: {
            checked: ischecked,
            onChange: handleRowSelect
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Name'),
          props: {
            children: personPhysical.name
          }
        },
        {
          component: 'div',
          keyCell: generateKey('FirstName'),
          props: {
            children: personPhysical.firstname
          }
        },
        {
          component: isEditingRow ? FunctionReduxSelect : 'div',
          error: _.get(associateErrors, 'function'),
          keyCell: generateKey('Function'),
          props: isEditingRow
            ? {
              autoFocus: isLastSelected,
              ...getAutoCompleteProps(editRow, personPhysical, 'function.label', 'function', tableName),
              onChange: (value) => {
                editRow(tableName, { ...personPhysical, function: value });
                setErrors(tableName, personPhysical.id, { function: undefined });
              }
            }
            : {
              children: _.get(personPhysical, 'function.label')
            }
        },
        {
          component: isEditingRow ? SignatoryFunctionReduxSelect : 'div',
          error: _.get(associateErrors, 'signatory_function'),
          keyCell: generateKey('FunctionSignatory'),
          props: isEditingRow
            ? {
              ...getAutoCompleteProps(editRow, personPhysical, 'signatory_function.label', 'signatory_function', tableName),
              onChange: (value) => {
                editRow(tableName, { ...personPhysical, signatory_function: value });
                setErrors(tableName, personPhysical.id, { signatory_function: undefined });
              }
            } : {
              children: _.get(personPhysical, 'signatory_function.label')
            }
        },
        {
          component: isEditingRow ? DatePickerCell : 'div',
          keyCell: generateKey('DateStart'),
          error: _.get(associateErrors, 'effective_date'),
          props: isEditingRow ? {
            value: startDate,
            onChange: (value) => {
              editRow(tableName, {
                ...personPhysical,
                start_date: moment(value).format('YYYY-MM-DD')
              });
              setErrors(tableName, personPhysical.id, { effective_date: undefined });
            }
          } : {
            children: formattedStartDate
          }
        },
        endDateRender(),
        {
          component: socialPartIsEditable ? TextFieldCell : 'div',
          error: _.get(associateErrors, 'social_part_PP'),
          keyCell: generateKey('NbPP'),
          isDisabled: socialPartIsEditable,
          props: socialPartIsEditable
            ? {
              defaultValue: _.get(personPhysical, 'social_part.PP'),
              placeholder: _.get(personPhysical, 'social_part.PP'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...personPhysical,
                  social_part: {
                    ...personPhysical.social_part,
                    PP: e.target.value
                  }
                });
                if (socialPartIsEditable) {
                  setErrors(tableName, personPhysical.id, {
                    social_part_PP: undefined
                  });
                }
              }
            }
            : {
              children: _.get(personPhysical, 'social_part.PP', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: socialPartIsEditable ? TextFieldCell : 'div',
          error: _.get(associateErrors, 'social_part_US'),
          keyCell: generateKey('NbUS'),
          props: socialPartIsEditable
            ? {
              defaultValue: _.get(personPhysical, 'social_part.US'),
              placeholder: _.get(personPhysical, 'social_part.US'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...personPhysical,
                  social_part: {
                    ...personPhysical.social_part,
                    US: e.target.value
                  }
                });
                if (socialPartIsEditable) {
                  setErrors(tableName, personPhysical.id, {
                    social_part_US: undefined
                  });
                }
              }
            } : {
              children: _.get(personPhysical, 'social_part.US', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: socialPartIsEditable ? TextFieldCell : 'div',
          error: _.get(associateErrors, 'social_part_NP'),
          keyCell: generateKey('NbPNP'),
          props: socialPartIsEditable
            ? {
              defaultValue: _.get(personPhysical, 'social_part.NP'),
              placeholder: _.get(personPhysical, 'social_part.NP'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...personPhysical,
                  social_part: {
                    ...personPhysical.social_part,
                    NP: e.target.value
                  }
                });
                if (socialPartIsEditable) {
                  setErrors(tableName, personPhysical.id, {
                    social_part_NP: undefined
                  });
                }
              }
            } : {
              children: _.get(personPhysical, 'social_part.NP', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Percent'),
          props: {
            children: _.get(personPhysical, 'social_part.percent')
          },
          cellProp: {
            className: classnames(cellStyles.numberCell, cellStyles.smallCell)
          }
        }
      ]
    };
  });

  const transformedProps = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  const basePhysicalPersonFileName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${PHYSICAL_PERSON_TABLE_NAME}`;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    setTableData:
      () => setPhysicalPersonTableData(
        basePhysicalPersonFileName, navigation_society_id, transformedProps
      ),
    param: transformedProps
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(PhysicalPerson);
