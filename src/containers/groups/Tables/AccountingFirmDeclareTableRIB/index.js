import { connect } from 'react-redux';
import { AccountingFirmDeclareTable } from 'components/groups/Tables';
import { AccountingFirmDeclareTableKeys } from 'assets/constants/keys';
import AccountingFirmSettingsService from 'redux/accountingFirmSettings';
import {
  CheckBoxCell
} from 'components/basics/Cells';
import { ACCOUNTING_FIRM_DECLARE_TABLE_RIB, ACCOUNTING_FIRM_TOP_MAIN_TABLE, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import _ from 'lodash';
import I18n from 'assets/I18n';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, ACCOUNTING_FIRM_DECLARE_TABLE_RIB);
  const acountFirmTopTableName = getTableName(society_id, ACCOUNTING_FIRM_TOP_MAIN_TABLE);
  return {
    associate_list: _.get(state, `tables.${tableName}.data`, []),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    errors: _.get(state, `tables.${tableName}.errors`, {}),
    initialData: _.get(state, `tables.${tableName}.initialData`),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
    society_id: state.navigation.id,
    isLoading: _.get(state, `accountingFirmSettings.${society_id}.isLoading`, false),
    isError: _.get(state, `accountingFirmSettings.${society_id}.isError`, false),
    topTableLastSelectedRow: _.get(state, `tables.${acountFirmTopTableName}.lastSelectedRow`, null),
    topTableData: _.get(state, `tables.${acountFirmTopTableName}.data`),
    rbTableLastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null)
  };
};

const mapDispatchToProps = dispatch => ({
  selectOnlyOneRow: (tableName, id) => dispatch(tableActions.selectOnlyOneRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  fillValues: data => dispatch(autoFillFormValues('accountingFirmSettingsDeclareForm', data)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors)),
  sendRBAccount: (member_id, memberIndex) => dispatch(
    AccountingFirmSettingsService.sendRBAccount(member_id, memberIndex)
  ),
  modifyRBAccount: (account_id, memberIndex, rbIndex) => dispatch(
    AccountingFirmSettingsService.modifyRBAccount(account_id, memberIndex, rbIndex)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    selectedRowsCount,
    selectedRows,
    isLoading,
    isError,
    topTableData,
    topTableLastSelectedRow,
    rbTableLastSelectedRow,
    lastSelectedRow
  } = stateProps;

  const tableName = getTableName(society_id, ACCOUNTING_FIRM_DECLARE_TABLE_RIB);

  const {
    selectOnlyOneRow,
    unselectRow,
    selectAllRows,
    sendRBAccount,
    modifyRBAccount,
    deleteRBAccount,
    fillValues,
    unselectAllRows
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  let selectedMember = {};
  let selectedMemberIndex = null;
  topTableData.forEach((row, index) => {
    if (row.member_id === topTableLastSelectedRow) {
      selectedMember = row;
      selectedMemberIndex = index;
    }
  });

  const rbData = selectedMember.compte_rb || [];
  let selectedRBIndex = null;

  rbData.forEach((row, index) => {
    if (row.id_compte_rb === rbTableLastSelectedRow) {
      selectedRBIndex = index;
    }
  });

  const selectedRow = _.filter(topTableData,
    row => row.member_id === topTableLastSelectedRow)[0] || {};
  const ribData = selectedRow.compte_rb || [];

  tableActions.setData(tableName, ribData);

  const isAllSelected = selectedRowsCount === ribData.length;

  const headers = [{
    keyRow: 'reportsAndFormsHeaderRow',
    value: AccountingFirmDeclareTableKeys.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `reportsAndFormsHeaderCell${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = ribData.map(al => al.id_compte_rb);
                selectAllRows(tableName, ids);
              }
            },
            id: keyLabel.toString()
          },
          cellProp: {
            style: {
              maxWidth: '44px'
            }
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `reportsAndFormsHeaderCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`accountingFirmSettings.table.${keyLabel}`)
        }
      };
    })

  }];

  const bodyRow = ribData.map((compte) => {
    const { id_compte_rb } = compte;
    const ischecked = selectedRows[id_compte_rb] === true;
    const handleRowSelect = () => {
      if (ischecked) {
        unselectRow(tableName, id_compte_rb);
      } else {
        selectOnlyOneRow(tableName, id_compte_rb);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    const generateKey = (cellName = '') => `${ACCOUNTING_FIRM_DECLARE_TABLE_RIB}${cellName}${id_compte_rb}`;
    return {
      keyRow: generateKey(),
      props: {
        hover: true,
        selected: ischecked
      },
      value: [
        {
          component: CheckBoxCell,
          keyCell: generateKey('CheckBox'),
          props: {
            checked: ischecked,
            onChange: handleRowSelect
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('mail'),
          props: {
            children: _.get(compte, 'mail', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('password'),
          props: {
            children: _.get(compte, 'password_secured', false) ? '********' : _.get(compte, 'password', '')
          }
        },
        {
          _type: 'checkbox',
          keyCell: generateKey('enabled'),
          props: {
            checked: !!_.get(compte, 'enabled', false)
          }
        }
      ]
    };
  });

  const lastRow = _.find(ribData, item => item.id_compte_rb === lastSelectedRow) || {};

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isLoading,
    isError,
    onAdd: () => sendRBAccount(selectedMember.member_id, selectedMemberIndex),
    onModify: () => modifyRBAccount(lastSelectedRow, selectedMemberIndex, selectedRBIndex),
    onDelete: () => deleteRBAccount(lastSelectedRow),
    fillDefaultsValues: () => fillValues(lastRow),
    title: I18n.t('accountingFirmSettings.rib'),
    disableButtons: !selectedRowsCount,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AccountingFirmDeclareTable);
