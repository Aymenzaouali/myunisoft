import React from 'react';
import { TableFactory } from 'components/groups/Tables';
import classnames from 'classnames';
import { reportsAndFormsOrder } from 'assets/constants/keys';
import I18n from 'assets/I18n';
import { formatNumber } from 'helpers/number';
import styles from 'components/screens/WorkingPage/workingPage.module.scss';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';
import CheckBoxCell from 'components/basics/Cells/CheckBoxCell';
import TextFieldCell from 'components/basics/Cells/TextFieldCell';

const getProps = (props) => {
  const {
    tableName,
    statesList: currentStates,
    society_id,
    selectedStates,
    selectedStatesCount,
    lastSelectedState,
    isEditing,
    inputRef,
    editRow,
    onEditStart,
    addState,
    removeState,
    resetAllStates,
    addAllStates,
    createRow
  } = props;

  const focus = () => {
    setTimeout(() => {
      if (inputRef && inputRef.current && inputRef.current.focus) {
        inputRef.current.focus();
      }
    }, 0);
  };
  const isAllSelected = selectedStatesCount && selectedStatesCount === currentStates.length;

  const headers = [{
    keyRow: 'unreachedBillsHeaderRow',
    value: reportsAndFormsOrder.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;
      let cell;

      if (type === 'checkbox') {
        cell = {
          _type: 'checkbox',
          keyCell: `RAFCreateNewStateHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllStates(society_id);
              } else {
                addAllStates(society_id, currentStates.map(bill => bill.id));
                focus();
              }
            },
            id: keyLabel
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        };
      } else {
        cell = {
          _type: 'sortCell',
          keyCell: `RAFCreateNewStateHeaderCell.${keyLabel}${i}`,
          props: {
            children: I18n.t(`reportsAndForms.orderTable.${keyLabel}`),
            colorErrorIcon: true
          }
        };
      }
      return cell;
    })
  }];

  const states = currentStates.reduce((acc, state) => {
    const isStateSelected = selectedStates[state.id] || state.isCreated;
    const isEditableState = isEditing && isStateSelected;
    const isLastSelectedState = lastSelectedState === state.id;
    const generateKey = (cellName = '') => `RAFCreateNewState${cellName}${state.state_id}`;
    return {
      body: [
        ...acc.body,
        {
          keyRow: generateKey(),
          props: {
            isSelected: isStateSelected,
            className: styles.row
          },
          value: [
            {
              component: CheckBoxCell,
              keyCell: generateKey('RAFCreateNewStateCheckbox'),
              props: {
                checked: isStateSelected || state.isCreated,
                onClick: (e) => {
                  e.stopPropagation();
                },
                onChange: () => {
                  if (isStateSelected) {
                    removeState(society_id, state.id);
                  } else {
                    addState(society_id, state.id);

                    if (selectedStates.length === 0) {
                      onEditStart();
                    }
                  }

                  focus();
                },
                id: `key ${state.state_id}`
              },
              cellProp: {
                className: classnames(styles.checkboxCell, cellStyles.checkboxCell)
              }
            },
            {
              component: isEditableState ? TextFieldCell : 'div',
              keyCell: generateKey('orderNumber'),
              props: isEditableState ? {
                type: 'number',
                defaultValue: state.orderNumber,
                onBlur: e => editRow(tableName, {
                  ...state,
                  orderNumber: parseFloat(e.target.value || 0)
                })
              } : {
                children: formatNumber(state.orderNumber)
              },
              cellProp: {
                className: cellStyles.labelCell
              }
            },
            {
              component: isEditableState ? TextFieldCell : 'div',
              keyCell: generateKey('codeEDI'),
              props: isEditableState ? {
                inputRef: isLastSelectedState ? inputRef : undefined,
                placelholder: state.codeEDI,
                defaultValue: state.codeEDI,
                onBlur: e => editRow(tableName, {
                  ...state,
                  codeEDI: e.target.value
                })
              } : {
                children: state.codeEDI
              },
              cellProp: {
                className: cellStyles.labelCell
              }
            },
            {
              component: isEditableState ? TextFieldCell : 'div',
              keyCell: generateKey('wording'),
              props: isEditableState ? {
                inputRef: isLastSelectedState ? inputRef : undefined,
                placelholder: state.wording,
                defaultValue: state.wording,
                onBlur: e => editRow(tableName, {
                  ...state,
                  wording: e.target.value
                })
              } : {
                children: state.wording
              },
              cellProp: {
                className: cellStyles.labelCell
              }
            },
            {
              component: isEditableState ? TextFieldCell : 'div',
              keyCell: generateKey('dataType'),
              props: isEditableState ? {
                inputRef: isLastSelectedState ? inputRef : undefined,
                placelholder: state.dataType,
                defaultValue: state.dataType,
                onBlur: e => editRow(tableName, {
                  ...state,
                  dataType: e.target.value
                })
              } : {
                children: state.dataType
              },
              cellProp: {
                className: cellStyles.labelCell
              }
            },
            {
              component: isEditableState ? TextFieldCell : 'div',
              keyCell: generateKey('viewing'),
              props: isEditableState ? {
                inputRef: isLastSelectedState ? inputRef : undefined,
                placelholder: state.viewing,
                defaultValue: state.viewing,
                onBlur: e => editRow(tableName, {
                  ...state,
                  viewing: e.target.value
                })
              } : {
                children: state.viewing
              },
              cellProp: {
                className: cellStyles.labelCell
              }
            },
            {
              component: isEditableState ? TextFieldCell : 'div',
              keyCell: generateKey('dataSource'),
              props: isEditableState ? {
                inputRef: isLastSelectedState ? inputRef : undefined,
                placelholder: state.dataSource,
                defaultValue: state.dataSource,
                onBlur: e => editRow(tableName, {
                  ...state,
                  dataSource: e.target.value
                })
              } : {
                children: state.dataSource
              },
              cellProp: {
                className: cellStyles.labelCell
              }
            },
            {
              component: isEditableState ? TextFieldCell : 'div',
              keyCell: generateKey('references'),
              props: isEditableState ? {
                type: 'number',
                defaultValue: state.references,
                onBlur: e => editRow(tableName, {
                  ...state,
                  references: parseFloat(e.target.value || 0)
                })
              } : {
                children: formatNumber(state.references)
              },
              cellProp: {
                className: cellStyles.labelCell
              }
            }
          ]
        }
      ]
    };
  },
  {
    body: []
  });

  return {
    ...props,
    addBodyRowCb: {
      onClick: () => {
        createRow(tableName);
        onEditStart();
      }
    },
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: states.body
      }
    }
  };
};

const RAFCreateNewStateTable = props => (
  <TableFactory {...getProps(props)} />
);

export default RAFCreateNewStateTable;
