import React from 'react';
import { connect } from 'react-redux';
import { TableFactory } from 'components/groups/Tables';
import {
  addFunction, removeFunction, resetAllFunction, addAllFunction, selectFunction
} from 'redux/function/actions';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';
import { functionHeaderKeys } from 'assets/constants/keys';
import I18n from 'assets/I18n';
import { StatusString } from 'components/basics/Strings';
import _ from 'lodash';
import tableActions from 'redux/tables/actions';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  listFunction: _.get(state, 'groupFunctions.list_selectedFunction', []),
  allFunctionList: _.get(state, 'groupFunctions.functions', []),
  isLoading: _.get(state, 'groupFunctions.isLoading', true),
  selectedFunction: _.get(state, 'groupFunctions.selectedFunction')
});

const mapDispatchToProps = dispatch => ({
  addFunction: func => dispatch(addFunction(func)),
  removeFunction: func => dispatch(removeFunction(func)),
  resetAllFunction: () => dispatch(resetAllFunction()),
  addAllFunction: () => dispatch(addAllFunction()),
  selectFunction: func => dispatch(selectFunction(func)),
  setFunctionTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    listFunction, allFunctionList, selectedFunction, societyId
  } = stateProps;
  const {
    addFunction, removeFunction, resetAllFunction, addAllFunction, selectFunction,
    setFunctionTableData
  } = dispatchProps;

  const headerKeys = functionHeaderKeys;

  const functionAvailable = allFunctionList.filter(func => !func.blocked);

  let isAllSelected = functionAvailable.length > 0;

  functionAvailable.forEach((func) => {
    if (!listFunction.some(item => item.id_group === func.id_group)) {
      isAllSelected = false;
    }
  });

  const handleRowClick = (func) => {
    selectFunction(func.id_group === selectedFunction
      ? undefined
      : func.id_group);
  };

  const renderLabel = (count, labelText, nbUser) => {
    const style = nbUser === 0 ? { paddingLeft: '5px' } : {};

    return (
      <div style={style}>
        <span style={{ color: '#0bd1d1' }}>{count}</span>
        <span>{labelText}</span>
      </div>
    );
  };

  const renderSelectList = option => (
    <StatusString status={option.value} label={option.label} />
  );

  const renderSelectValue = value => (
    <StatusString status={value} label={value} />
  );

  const headers = [{
    keyRow: 'functionTableHeaderRow',
    value: headerKeys.map((data, i) => {
      const { type, keyLabel } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          _type: type,
          keyCell: `functionTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllFunction();
              } else {
                addAllFunction();
              }
            }

          },
          cellProp: {
            className: cellStyles.stickyHeader
          }
        });
      } else {
        cell = {
          _type: 'sortCell',
          keyCell: `functionTableHeaderCell.${keyLabel}${i}`,
          cellProp: {
            className: cellStyles.stickyHeader
          },
          props: {
            children: I18n.t(`function.functionArrayTitle.${keyLabel}`),
            onClick: () => {}
          }
        };
      }
      return cell;
    })
  }];

  const body = allFunctionList.map((func, i) => {
    const nbUser = _.get(func, 'nb_pers_physique', '');
    const label = nbUser > 0 ? ` ${I18n.t('function.users')}` : ` ${I18n.t('function.user')}`;

    return {
      keyRow: `function${i}`,
      props: {
        hover: true,
        onClick: () => {
          handleRowClick(func);
        }
      },
      value: [
        {
          _type: 'checkbox',
          keyCell: `functionCheckbox${i}`,
          props: {
            checked: listFunction.includes(func),
            disabled: func.blocked,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (listFunction.includes(func)) {
                removeFunction(func);
              } else {
                selectFunction(func.id_group);
                addFunction(func);
              }
            },
            id: func.id_group
          }
        },
        {
          _type: 'string',
          keyCell: `functioLibelle${i}`,
          props: {
            label: func.libelle
          }
        },
        {
          _type: func.list_users ? 'select' : 'string',
          keyCell: `functionNbPersPhysique${i}`,
          props: func.list_users ? {
            value: renderLabel(nbUser, label, nbUser),
            labelToExport: `${nbUser} ${label}`,
            list: func.list_users.map(
              user => ({ label: user.name, value: user.name })
            ),
            renderValue: value => renderSelectValue(value),
            onChange: () => {

            },
            renderList: renderSelectList
          } : {
            label: renderLabel(nbUser, label, nbUser),
            labelToExport: `${nbUser} ${label}`
          },
          cellProp: {
            style: {
              width: '190px'
            }
          }
        }
      ]
    };
  });

  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();

  const tableData = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onRefresh: () => setFunctionTableData(tableName, societyId, tableData),
    param: tableData
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TableFactory);
