import React, { useEffect } from 'react';
import { TableFactory } from 'components/groups/Tables';
import classnames from 'classnames';
import _ from 'lodash';
import { workingPageDefaultKeys } from 'assets/constants/keys';
import I18n from 'assets/I18n';
import { getTvaInfo } from 'helpers/tva';
import WindowHandler from 'helpers/window';
import { formatNumber } from 'helpers/number';
import { AccountAutoCompleteCell } from 'containers/reduxForm/Inputs';
import styles from 'components/screens/WorkingPage/workingPage.module.scss';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';
import CheckBoxCell from 'components/basics/Cells/CheckBoxCell';
import TextFieldCell from 'components/basics/Cells/TextFieldCell';
import StatusCell from 'components/basics/Cells/StatusCell';
import IconCell from 'components/basics/Cells/IconCell';
import { WorksheetCommentBox } from 'containers/groups/Comments';
import _get from 'lodash/get';
import { empty } from 'helpers/validation';


const getProps = (props) => {
  const {
    tableName,
    codeTva,
    billsList: currentBills,
    society_id,
    selectedBills,
    selectedBillsCount,
    lastSelectedBill,
    isEditing,
    inputRef,
    editRow,
    onEditStart,
    billsInfos,
    addBill,
    removeBill,
    resetAllBills,
    addAllBills,
    createRow,
    isLoading,
    isError,
    openCommentsDialog,
    getComments,
    begginingAccount,
    openPJDialog,
    errors,
    setErrors
  } = props;


  const focus = () => {
    setTimeout(() => {
      if (inputRef && inputRef.current && inputRef.current.focus) {
        inputRef.current.focus();
      }
    }, 0);
  };

  const getAndOpenComments = async (bill_id) => {
    if (bill_id !== undefined) {
      openCommentsDialog({
        body: {
          Component: WorksheetCommentBox,
          props: {
            bill_id,
            createMode: false,
            getComments: () => getComments(bill_id)
          }
        }
      });
    } else {
      openCommentsDialog({
        body: {
          Component: WorksheetCommentBox,
          props: {
            bill_id,
            createMode: true
          }
        }
      });
    }
  };

  const isAllSelected = selectedBillsCount && selectedBillsCount === currentBills.length;

  const headers = [{
    keyRow: 'unreachedBillsHeaderRow',
    value: workingPageDefaultKeys.map((data, i) => {
      const {
        type,
        keyLabel,
        isAmount,
        isCentered,
        paddingLeft
      } = data;
      let cell;

      if (type === 'checkbox') {
        cell = {
          _type: 'checkbox',
          keyCell: `unreachedBillsHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllBills(society_id);
              } else {
                addAllBills(society_id, currentBills.map(bill => bill.id));
                focus();
              }
            },
            id: keyLabel
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        };
      } else {
        cell = {
          _type: type || 'string',
          keyCell: `unreachedBillsHeaderCell.${keyLabel}${i}`,
          cellProp: {
            paddingLeft,
            isAmount,
            isCentered
          },
          props: {
            children: I18n.t(`tables.fnp.${keyLabel}`),
            colorErrorIcon: true
          }
        };
      }
      return cell;
    })
  }];
  const beginnings = begginingAccount.split(',');
  const bills = currentBills.reduce((acc, bill) => {
    const accountNumber = bill.worksheet_account ? bill.worksheet_account.number : '';
    let accountLabel = accountNumber;
    const isbillSelected = selectedBills.find(id => id === bill.id.toString());
    const isEditableBill = isEditing && isbillSelected;
    const isLastSelectedBill = lastSelectedBill === bill.id;
    const errorsById = _get(errors, bill.id, {});

    const handleAccountNumberChange = (worksheet_account) => {
      editRow(tableName, {
        ...bill,
        worksheet_account
      });

      setErrors(bill.bill_id, { accountNumber: empty(worksheet_account) });

      document.getElementById(`unreachedBillsLabel${bill.bill_id}`).focus();
    };

    if (_.get(bill, 'worksheet_account.account_number')) {
      accountLabel = bill.worksheet_account.account_number;
    } else if (_.get(bill, 'worksheet_account.number')) {
      accountLabel = bill.worksheet_account.number;
    }

    return {
      body: [
        ...acc.body,
        {
          keyRow: `unreachedBills${bill.bill_id}`,
          props: {
            isSelected: isbillSelected,
            className: styles.row
          },
          value: [
            {
              component: CheckBoxCell,
              _type: 'checkbox',
              keyCell: `unreachedBillsBox${bill.bill_id}`,
              props: {
                checked: isbillSelected,
                onClick: (e) => {
                  e.stopPropagation();
                },
                onChange: () => {
                  if (isbillSelected) {
                    removeBill(society_id, bill.id);
                  } else {
                    addBill(society_id, bill.id);

                    if (selectedBills.length === 0) {
                      onEditStart();
                    }
                  }

                  focus();
                },
                id: `key ${bill.bill_id}`
              },
              cellProp: {
                className: classnames(styles.checkboxCell, cellStyles.checkboxCell)
              }
            },
            {
              component: isEditableBill ? AccountAutoCompleteCell : 'div',
              keyCell: `unreachedBillsAccountNumber${bill.bill_id}`,
              error: _get(errorsById, 'accountNumber'),
              props: isEditableBill ? {
                customRef: isLastSelectedBill ? inputRef : undefined,
                className: cellStyles.autocomplete,
                placeholder: accountLabel,
                input: {
                  value: {
                    ...bill.worksheet_account,
                    value: bill.worksheet_account.account_number
                  }
                },
                onChangeValues: handleAccountNumberChange,
                getOptions: accounts => accounts.filter(
                  account => beginnings.some(b => account.account_number.startsWith(b))
                ).map(acc => ({
                  ...acc,
                  value: acc.account_number
                })),
                getOptionLabel: option => option.account_number
              } : {
                children: accountLabel
              },
              cellProp: {
                className: classnames(styles.autocompleteCell, cellStyles.editableCell)
              }
            },
            {
              component: isEditableBill ? TextFieldCell : 'div',
              keyCell: `unreachedBillsLabel${bill.bill_id}`,
              error: _get(errorsById, 'label'),
              props: isEditableBill ? {
                placelholder: bill.label,
                defaultValue: bill.label,
                onBlur: (e) => {
                  editRow(tableName, {
                    ...bill,
                    label: e.target.value
                  });
                  setErrors(bill.bill_id, { label: empty(e.target.value) });
                },
                id: `unreachedBillsLabel${bill.bill_id}`
              } : {
                children: bill.label
              },
              cellProp: {
                className: cellStyles.labelCell
              }
            },
            {
              component: isEditableBill ? TextFieldCell : 'div',
              keyCell: `unreachedBillsHt${bill.bill_id}`,
              props: isEditableBill ? {
                type: 'number',
                placeholder: bill.total_excl_taxes,
                defaultValue: bill.total_excl_taxes,
                onBlur: e => editRow(tableName, {
                  ...bill,
                  total_excl_taxes: parseFloat(e.target.value || 0),
                  total_incl_taxes: parseFloat(bill.vat_total || 0)
                    + parseFloat(e.target.value || 0)
                })
              } : {
                children: formatNumber(bill.total_excl_taxes)
              },
              cellProp: {
                className: classnames(
                  styles.numberCell, cellStyles.editableCell, cellStyles.numberCell
                )
              }
            },
            {
              keyCell: `unreachedBillsTvaCode${bill.bill_id}`,
              children: bill.vat_code,
              cellProp: {
                titleInfoBulle: getTvaInfo(bill.vat_code, codeTva),
                className: styles.tvaCodeCell
              }
            },
            {
              component: isEditableBill ? TextFieldCell : 'div',
              keyCell: `unreachedBillsTva${bill.bill_id}`,
              props: isEditableBill ? {
                type: 'number',
                placeholder: bill.vat_total,
                defaultValue: bill.vat_total,
                onBlur: e => editRow(tableName, {
                  ...bill,
                  vat_total: parseFloat(e.target.value || 0),
                  total_incl_taxes: parseFloat(e.target.value || 0)
                    + parseFloat(bill.total_excl_taxes || 0)
                })
              } : {
                children: formatNumber(bill.vat_total)
              },
              cellProp: {
                className: classnames(
                  styles.numberCell, cellStyles.editableCell, cellStyles.numberCell
                )
              }
            },
            {
              keyCell: `unreachedBillsTtc${bill.bill_id}`,
              children: formatNumber(bill.total_incl_taxes),
              cellProp: {
                className: classnames(styles.numberCell, cellStyles.numberCell)
              }
            },
            {
              component: StatusCell,
              keyCell: `fnp.pj${bill.bill_id}`,
              props: {
                paddingLeft: true,
                data: [{
                  status: 'pjs',
                  value: _.get(bill, 'pj_list.length', 0),
                  pjs: bill.pj_list,
                  onClick: () => WindowHandler.forceOpen(bill.pj_list)
                }]
              },
              cellProp: {
                className: classnames(styles.iconCell, cellStyles.iconCell)
              }
            },
            {
              component: (!bill.isCreated && IconCell),
              keyCell: `fnp.AddPj${bill.bill_id}`,
              props: {
                name: 'icon-plus',
                onClick: openPJDialog.bind(null, bill.bill_id)
              },
              cellProp: {
                className: classnames(styles.iconCell, cellStyles.iconCell)
              }
            },
            {
              component: (bill.generated && IconCell),
              keyCell: `unreachedBillsCompta${bill.bill_id}`,
              props: {
                name: 'icon-check',
                color: '#0bd1d1'
              },
              cellProp: {
                className: classnames(styles.iconCell, cellStyles.iconCell)
              }
            },
            {
              component: (!bill.isCreated && IconCell),
              keyCell: `unreachedBillsCom${bill.bill_id}`,
              props: {
                name: 'icon-comments',
                comment: bill.comment || '',
                onClick: () => getAndOpenComments(bill.bill_id)
              },
              cellProp: {
                className: classnames(styles.iconCell, cellStyles.iconCell)
              }
            }
          ]
        }
      ],
      total: {
        vat_total: parseFloat(acc.total.vat_total || 0) + parseFloat(bill.vat_total || 0),
        total_excl_taxes: parseFloat(acc.total.total_excl_taxes || 0)
          + parseFloat(bill.total_excl_taxes || 0),
        total_incl_taxes: parseFloat(acc.total.total_incl_taxes || 0)
          + parseFloat(bill.total_incl_taxes || 0)
      }
    };
  },
  {
    body: [],
    total: {
      vat_total: 0,
      total_excl_taxes: 0,
      total_incl_taxes: 0
    }
  });

  const footer = [
    {
      props: {
        className: styles.footerRow
      },
      value: [
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: I18n.t('fnp.total'),
            className: styles.footerCell
          },
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: formatNumber(bills.total.total_excl_taxes),
            className: styles.footerCell
          },
          cellProp: {
            className: styles.totalFooterCell
          }
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: formatNumber(bills.total.vat_total),
            className: styles.footerCell
          },
          cellProp: {
            className: styles.totalFooterCell
          }
        },
        {
          _type: 'string',
          props: {
            label: formatNumber(bills.total.total_incl_taxes),
            className: styles.footerCell
          },
          cellProp: {
            className: styles.totalFooterCell
          }
        }
      ]
    },
    {
      props: {
        className: styles.footerRow
      },
      value: [
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: `${I18n.t('fnp.balance')}`,
            className: styles.footerCell
          },
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: formatNumber(billsInfos.accounting_balance),
            className: styles.footerCell
          },
          cellProp: {
            className: styles.backgroundFooterCell
          }
        }
      ]
    },
    {
      props: {
        className: styles.footerRow
      },
      value: [
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: I18n.t('fnp.gap'),
            className: styles.footerCell
          },
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: formatNumber(Math.abs(
              bills.total.total_incl_taxes - billsInfos.accounting_balance
            )),
            className: styles.footerCell
          },
          cellProp: {
            className: styles.backgroundFooterCell
          }
        }
      ]
    }
  ];

  return {
    ...props,
    addBodyRowCb: {
      onClick: () => {
        createRow(tableName);
        onEditStart();
      }
    },
    getAndOpenComments,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bills.body
      },
      footer: currentBills && currentBills.length > 0 && {
        props: {},
        row: footer
      }
    },
    isLoading,
    isError
  };
};

const FNPTable = (props) => {
  const tableProps = getProps(props);
  const {
    society_id, setTableData, param, isLoading, tableName, createdData
  } = tableProps;
  useEffect(() => {
    setTableData(tableName, society_id, param);
  }, [isLoading, createdData]);

  return (
    <TableFactory
      {...tableProps}
    />
  );
};

export default FNPTable;
