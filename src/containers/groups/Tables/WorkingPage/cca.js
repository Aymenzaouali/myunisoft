import { TableFactory } from 'components/groups/Tables';
import React, { useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import { CCAKeys } from 'assets/constants/keys';
import I18n from 'assets/I18n';
import moment from 'moment';
import WindowHandler from 'helpers/window';
import { formatNumber } from 'helpers/number';
import styles from 'components/screens/WorkingPage/workingPage.module.scss';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';
import { AccountAutoCompleteCell } from 'containers/reduxForm/Inputs';
import { getFieldProps } from 'redux/tables/utils';
import CheckBoxCell from 'components/basics/Cells/CheckBoxCell';
import TextFieldCell from 'components/basics/Cells/TextFieldCell';
import StatusCell from 'components/basics/Cells/StatusCell';
import IconCell from 'components/basics/Cells/IconCell';
import StartEndDateCell from 'components/basics/Cells/StartEndDateCell';
import { AccountingCommentBox } from 'containers/groups/Comments';
import _get from 'lodash/get';
import { empty } from 'helpers/validation';

const getProps = (props) => {
  const {
    tableName,
    billsList: currentBills,
    society_id,
    selectedBills,
    selectedBillsCount,
    lastSelectedBill,
    isEditing,
    inputRef,
    editRow,
    billsInfos,
    addBill,
    removeBill,
    resetAllBills,
    addAllBills,
    onEditStart,
    createRow,
    isLoading,
    isError,
    setErrors,
    errors,
    openCommentsDialog,
    getComments,
    begginingAccount,
    openPJDialog
  } = props;

  const focus = () => {
    setTimeout(() => {
      if (inputRef && inputRef.current && inputRef.current.focus) {
        inputRef.current.focus();
      }
    }, 0);
  };
  const beginnings = begginingAccount.split(',');
  const isAllSelected = selectedBillsCount && selectedBillsCount === currentBills.length;

  const getAndOpenComments = async (bill_id) => {
    if (bill_id !== undefined) {
      openCommentsDialog({
        body: {
          Component: AccountingCommentBox,
          props: {
            bill_id,
            createMode: false,
            getComments: () => getComments(bill_id)
          }
        }
      });
    } else {
      openCommentsDialog({
        body: {
          Component: AccountingCommentBox,
          props: {
            bill_id,
            createMode: true
          }
        }
      });
    }
  };

  const headers = [{
    keyRow: 'ccaHeaderRow',
    value: CCAKeys.map((data, i) => {
      const {
        type,
        keyLabel,
        isAmount,
        isCentered,
        paddingLeft,
        colSpan = 1
      } = data;
      let cell;

      if (type === 'checkbox') {
        cell = {
          _type: 'checkbox',
          keyCell: `unreachedBillsHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllBills(society_id);
              } else {
                addAllBills(society_id, currentBills.map(b => b.bill_id));
              }

              focus();
            },
            id: keyLabel
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        };
      } else {
        cell = {
          _type: type || 'string',
          keyCell: `unreachedBillsHeaderCell.${keyLabel}${i}`,
          cellProp: {
            paddingLeft,
            isAmount,
            isCentered,
            colSpan
          },
          props: {
            children: I18n.t(`tables.CCA.${keyLabel}`),
            colorErrorIcon: true
          }
        };
      }
      return cell;
    })
  }];

  // TODO remove dupplicated code and refactor WP with misterious logic...
  const bills = currentBills.reduce((acc, bill) => {
    const accountNumber = bill.worksheet_account ? bill.worksheet_account.number : '';
    let accountLabel = accountNumber;
    const isbillSelected = selectedBills.find(id => id === bill.id.toString());
    const isEditableBill = isEditing && isbillSelected;
    const isLastSelectedBill = lastSelectedBill === bill.id;
    const billsErrors = _get(errors, bill.id, {});

    const handleAccountNumberChange = (worksheet_account) => {
      editRow(tableName, {
        ...bill,
        worksheet_account
      });

      setErrors(bill.bill_id, { accountNumber: empty(worksheet_account) });

      document.getElementById(`CCALabel${bill.id}`).focus();
    };

    if (_.get(bill, 'worksheet_account.account_number')) {
      accountLabel = bill.worksheet_account.account_number;
    } else if (_.get(bill, 'worksheet_account.number')) {
      accountLabel = bill.worksheet_account.number;
    }

    return {
      body: [
        ...acc.body,
        {
          keyRow: `CCA${bill.id}`,
          props: {
            isbillSelected
          },
          value: [
            {
              component: CheckBoxCell,
              _type: 'checkbox',
              keyCell: `CCABox${bill.id}`,
              props: {
                checked: isbillSelected || bill.isCreated,
                onClick: (e) => {
                  e.stopPropagation();
                },
                onChange: () => {
                  if (isbillSelected) {
                    removeBill(society_id, bill.id);
                  } else {
                    addBill(society_id, bill.id);

                    if (selectedBills.length === 0) {
                      onEditStart();
                    }
                  }

                  focus();
                },
                id: `key ${bill.id}`
              },
              cellProp: {
                className: classnames(styles.checkboxCell, cellStyles.checkboxCell)
              }
            },
            {
              component: isEditableBill ? AccountAutoCompleteCell : 'div',
              keyCell: `CCAAccountNumber${bill.id}`,
              error: _get(billsErrors, 'accountNumber'),
              props: isEditableBill ? {
                customRef: isLastSelectedBill ? inputRef : undefined,
                className: cellStyles.autocomplete,
                placeholder: accountLabel,
                input: {
                  value: {
                    ...bill.worksheet_account,
                    value: bill.worksheet_account.account_number
                  }
                },
                onChangeValues: handleAccountNumberChange,
                getOptions: accounts => accounts.filter(
                  account => beginnings.some(b => account.account_number.startsWith(b))
                ).map(acc => ({
                  ...acc,
                  value: acc.account_number
                })),
                getOptionLabel: option => option.account_number
              } : {
                children: accountLabel
              },
              cellProp: {
                className: classnames(styles.autocompleteCell, cellStyles.editableCell)
              }
            },
            {
              component: isEditableBill ? TextFieldCell : 'div',
              keyCell: `CCALabel${bill.bill_id}`,
              error: _get(billsErrors, 'label'),
              props: isEditableBill ? {
                placelholder: bill.label,
                defaultValue: bill.label,
                onBlur: (e) => {
                  editRow(tableName, {
                    ...bill,
                    label: e.target.value
                  });
                  setErrors(bill.bill_id, { label: empty(e.target.value) });
                },
                id: `CCALabel${bill.bill_id}`
              } : {
                children: bill.label
              },
              cellProp: {
                className: cellStyles.labelCell
              }
            },
            {
              component: isEditableBill ? TextFieldCell : 'div',
              keyCell: `CCAAmount${bill.id}`,
              props: isEditableBill
                ? getFieldProps(editRow, bill, 'total_incl_taxes', tableName)
                : {
                  children: formatNumber(bill.total_incl_taxes)
                },
              cellProp: {
                className: classnames(styles.numberCell, cellStyles.numberCell)
              }
            },
            {
              component: isEditableBill ? StartEndDateCell : 'div',
              keyCell: `CCAPeriod${bill.id}`,
              error: billsErrors.errorEndDate || billsErrors.errorStartDate,
              props: isEditableBill ? {
                startProps: {
                  placeholder: bill.start_period && moment(bill.start_period).isValid() ? moment(bill.start_period).format('DD/MM/YYYY') : bill.start_period,
                  defaultValue: bill.start_period && moment(bill.start_period).isValid() ? moment(bill.start_period).format('DD/MM/YYYY') : bill.start_period,
                  onBlur: (e) => {
                    editRow(tableName, {
                      ...bill,
                      start_period: moment(e.target.value, 'DD/MM/YYYY').isValid() ? moment(e.target.value, 'DD/MM/YYYY').format('YYYY-MM-DD') : e.target.value
                    });
                    if (!moment(e.target.value, 'DD/MM/YYYY').isValid() || moment(e.target.value, 'DD/MM/YYYY').isAfter(moment(bill.end_period).format('DD/MM/YYYY'))) {
                      setErrors(tableName, bill.id, {
                        errorStartDate: 'Format de date invalide'
                      });
                    } else {
                      setErrors(tableName, bill.id, {
                        errorStartDate: undefined
                      });
                    }
                  }
                },
                endProps: {
                  placeholder: bill.end_period && moment(bill.end_period).isValid() ? moment(bill.end_period).format('DD/MM/YYYY') : bill.end_period,
                  defaultValue: bill.end_period && moment(bill.end_period).isValid() ? moment(bill.end_period).format('DD/MM/YYYY') : bill.end_period,
                  onBlur: (e) => {
                    editRow(tableName, {
                      ...bill,
                      end_period: moment(e.target.value, 'DD/MM/YYYY').isValid() ? moment(e.target.value, 'DD/MM/YYYY').format('YYYY-MM-DD') : e.target.value
                    });
                    if (!moment(e.target.value, 'DD/MM/YYYY').isValid() || moment(e.target.value, 'DD/MM/YYYY').isBefore(moment(bill.start_period).format('DD/MM/YYYY'))) {
                      setErrors(tableName, bill.id, {
                        errorEndDate: 'Format de date invalide'
                      });
                    } else {
                      setErrors(tableName, bill.id, {
                        errorEndDate: undefined
                      });
                    }
                  }
                },
                endNode: I18n.t('common.to')
              } : {
                children: `${moment(bill.start_period).format('DD/MM/YYYY')} ${I18n.t('common.to')} ${moment(bill.end_period).format('DD/MM/YYYY')}`
              },
              cellProp: {
                className: classnames(cellStyles.periodCell, cellStyles.labelCell)
              }
            },
            {
              component: isEditableBill ? TextFieldCell : 'div',
              keyCell: `CCA${bill.id}`,
              props: isEditableBill
                ? getFieldProps(editRow, bill, 'total_jours', tableName)
                : {
                  children: bill.total_jours
                },
              cellProp: {
                className: classnames(styles.numberCell, cellStyles.numberCell)
              }
            },
            {
              component: isEditableBill ? TextFieldCell : 'div',
              keyCell: `ccajours_avance${bill.id}`,
              props: isEditableBill
                ? getFieldProps(editRow, bill, 'jours_avance', tableName)
                : {
                  children: bill.jours_avance
                },
              cellProp: {
                className: classnames(styles.numberCell, cellStyles.numberCell)
              }
            },
            {
              component: isEditableBill ? TextFieldCell : 'div',
              keyCell: `ccaTtc${bill.id}`,
              props: isEditableBill
                ? getFieldProps(editRow, bill, 'cca', tableName)
                : {
                  children: bill.cca
                },
              cellProp: {
                className: classnames(styles.numberCell, cellStyles.numberCell)
              }
            },
            {
              component: StatusCell,
              keyCell: `cca.pj${bill.id}`,
              props: {
                data: [{
                  status: 'pjs',
                  value: _.get(bill, 'pj_list.length', 0),
                  pjs: bill.pj_list,
                  onClick: () => WindowHandler.forceOpen(bill.pj_list)
                }]
              },
              cellProp: {
                className: classnames(styles.iconCell, cellStyles.iconCell)
              }
            },
            {
              component: (!bill.isCreated && IconCell),
              keyCell: `cca.AddPj${bill.bill_id}`,
              props: {
                name: 'icon-plus',
                onClick: openPJDialog.bind(null, bill.bill_id)
              },
              cellProp: {
                className: classnames(styles.iconCell, cellStyles.iconCell)
              }
            },
            {
              component: (bill.generated && IconCell),
              keyCell: `ccaCompta${bill.id}`,
              props: {
                name: 'icon-check',
                color: '#0bd1d1'
              },
              cellProp: {
                className: classnames(styles.iconCell, cellStyles.iconCell)
              }
            },
            {
              component: (!bill.isCreated && IconCell),
              keyCell: `unreachedBillsCom${bill.bill_id}`,
              props: {
                name: 'icon-comments',
                comment: bill.comment || '',
                onClick: () => getAndOpenComments(bill.bill_id)
              },
              cellProp: {
                className: classnames(styles.iconCell, cellStyles.iconCell)
              }
            }
          ]
        }
      ],
      total: {
        cca: parseFloat(acc.total.cca || 0) + parseFloat(bill.cca || 0)
      }
    };
  },
  {
    body: [],
    total: {
      cca: 0
    }
  });

  const footer = [
    {
      props: {
      },
      value: [
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: I18n.t('fnp.total'),
            className: styles.footerCell
          },
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: formatNumber(bills.total.cca),
            className: styles.footerCell
          },
          cellProp: {
            className: styles.totalFooterCell
          }
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        }
      ]
    },
    {
      props: {
      },
      value: [
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: `${I18n.t('fnp.balance')}`,
            className: styles.footerCell
          },
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: formatNumber(Math.abs(
              billsInfos.accounting_balance
            )),
            className: styles.footerCell
          },
          cellProp: {
            className: styles.backgroundFooterCell
          }
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        }
      ]
    },
    {
      props: {
      },
      value: [
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: `${I18n.t('fnp.gap')}`,
            className: styles.footerCell
          },
          cellProp: {}
        },
        {
          _type: 'string',
          props: {
            label: formatNumber(Math.abs(
              bills.total.cca - billsInfos.accounting_balance
            )),
            className: styles.footerCell
          },
          cellProp: {
            className: styles.backgroundFooterCell
          }
        },
        {
          _type: 'string',
          props: {},
          cellProp: {}
        }
      ]
    }
  ];

  return {
    ...props,
    addBodyRowCb: {
      onClick: () => {
        createRow(tableName);
        onEditStart();
      }
    },
    getAndOpenComments,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bills.body
      },
      footer: currentBills && currentBills.length > 0 && {
        props: {},
        row: footer
      }
    },
    isLoading,
    isError
  };
};

const CCATable = (props) => {
  const tableProps = getProps(props);
  const {
    society_id, setTableData, param, isLoading, tableName, createdData
  } = tableProps;
  useEffect(() => {
    setTableData(tableName, society_id, param);
  }, [isLoading, createdData]);
  return (
    <TableFactory
      {...tableProps}
    />
  );
};

export default CCATable;
