import { compose } from 'redux';
import { connect } from 'react-redux';
import { getFormValues, reduxForm } from 'redux-form';

import moment from 'moment';
import _ from 'lodash';

import I18n from 'assets/I18n';
import { splitWrap } from 'context/SplitContext';
import { formatNumber, formatNumberIfNotNull } from 'helpers/number';

import { changeSort, getConsultationEntrie, updateEntry } from 'redux/consulting';
import {
  updateSelect, addAllEntriesSelect, resetAllEntriesSelect, setCurrentIndex
} from 'redux/consulting/actions';
import tableActions from 'redux/tables/actions';

import TableConsulting from 'components/groups/Tables/Consulting';
import style from 'components/groups/Tables/Consulting/consulting.module.scss';

import { headerData, bodyData, footerData } from './arrayConfig';

// Component
const mapStateToProps = state => ({
  consulting: _.get(state, 'consulting', []),
  societyId: state.navigation.id,
  accountID: _.get(getFormValues(`${state.navigation.id}consultingFilter`)(state), 'Account.account_id', null)
});

const mapDispatchToProps = dispatch => ({
  changeSort: (societyId, key, direction) => dispatch(changeSort(societyId, key, direction)),
  getEntries: societyId => dispatch(getConsultationEntrie(societyId)),
  updateEntry: (societyId, line_entry_id) => dispatch(updateEntry(societyId, line_entry_id)),
  setConsultingTableData: (tableName, societyId, tableData) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, tableData)
  ),
  updateSelect: (societyId, sel, accountID) => dispatch(updateSelect(societyId, sel, accountID)),
  addAllEntriesSelect: (
    societyId, accountId
  ) => dispatch(addAllEntriesSelect(societyId, accountId)),
  resetAllEntriesSelect: (
    societyId, accountId
  ) => dispatch(resetAllEntriesSelect(societyId, accountId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { consulting, societyId, accountID } = stateProps;
  const {
    changeSort,
    getEntries,
    updateSelect,
    addAllEntriesSelect,
    resetAllEntriesSelect,
    setConsultingTableData
  } = dispatchProps;
  const { split } = ownProps;

  let header = [];
  let body = [];
  let footer;
  const tableError = _.get(consulting, `[${societyId}].error`, false);
  const tableLoading = _.get(consulting, `[${societyId}].loading`, false);
  let tableLoad;
  let sort = { key: '', value: '' };
  let page_select;
  let credit;
  let debit;
  let rowsPage;
  let refresh = () => {};
  let selectedItems = [];


  if (_.get(consulting, `[${societyId}].entries.length`, 0) > 0) {
    const {
      entries,
      filter: { sort: sortData },
      load,
      page,
      total_credit,
      total_debit,
      rowsPerPage,
      select
    } = consulting[societyId];

    selectedItems = _.get(select, accountID, []);

    tableLoad = load;
    sort = sortData;
    page_select = page;
    credit = total_credit;
    debit = total_debit;
    rowsPage = rowsPerPage;

    refresh = () => getEntries(societyId, rowsPage, page_select);
    const isAllSelected = _.get(select, accountID, []).length === entries.length;
    /* Header generation */
    header = [{
      keyRow: 'headerConsulting',
      value: headerData.map((data) => {
        const {
          keyLabel,
          type,
          isAmount,
          paddingLeft
        } = data;
        let cell;

        if (type === 'sortCell') {
          // gestion arrow
          const newDirection = keyLabel !== sort.key
          || sort.direction === ''
          || sort.direction !== 'asc'
            ? 'asc'
            : 'desc';

          const direction = keyLabel === sort.key ? sort.direction : '';

          cell = ({
            _type: type,
            keyCell: keyLabel,
            cellProp: {
              paddingLeft,
              isAmount
            },
            props: {
              children: I18n.t(`consulting.${keyLabel}`),
              direction,
              onClick: () => {
                changeSort(societyId, keyLabel, newDirection);
              },
              disabled: tableLoading
            }
          });
        } else {
          let classe;
          if (keyLabel === 'credit' || keyLabel === 'solde' || keyLabel === 'debit') classe = style.number;
          cell = {
            _type: type,
            keyCell: keyLabel,
            props: {
              label: I18n.t(`consulting.${keyLabel}`),
              cellClassName: classe,
              typoVariant: 'h5',
              checked: type === 'checkbox' && isAllSelected,
              onClick: type === 'checkbox' && (e => e.stopPropagation()),
              onChange: type === 'checkbox' && (() => {
                if (isAllSelected) {
                  resetAllEntriesSelect(societyId, accountID);
                } else {
                  addAllEntriesSelect(societyId, accountID);
                }
              })
            }
          };
        }
        return cell;
      })
    }];

    /* Body generation */
    body = entries.map((entry, index) => ({
      keyRow: `consulting-${entry.entry_line_id}`,
      data: entry,
      props: {
        onClick: () => {
          setCurrentIndex(index);
          if (split.master) {
            split.updateArgsSplit({ entry_id: entry.entry_id });
          }
        }
      },
      value: bodyData.map((head) => {
        let cell;
        let classe;

        if (head.typeCell === 'string') {
          let labelString;
          switch (head.entrie) {
          case 'date':
          case 'deadline':
            labelString = entry[head.entrie] && moment(entry[head.entrie]).format('DD/MM/YYYY');
            break;
          case 'credit':
          case 'debit':
          case 'solde':
            labelString = formatNumberIfNotNull(entry[head.entrie]);
            classe = style.number;
            break;
          case 'payment_type_id':
            labelString = formatNumberIfNotNull(entry[head.entrie]);
            break;
          default:
            labelString = entry[head.entrie] ? entry[head.entrie].toString() : '';
            break;
          }
          cell = {
            _type: 'string',
            keyCell: `consulting-${head.entrie}-${entry.entry_line_id}`,
            props: {
              label: labelString,
              cellClassName: classe
            }
          };
        } else if (head.typeCell === 'pj') {
          const pjs = _.get(entry, head.entrie, []);

          cell = {
            _type: pjs.length > 0 ? 'status' : 'string',
            keyCell: `consulting-${head.entrie}-${entry.entry_line_id}`,
            props: {
              data: {
                status: 'pjs',
                value: pjs.length,
                pjs
              }
            }
          };
        } else {
          const currentSelect = _.get(select, accountID, []);
          cell = {
            _type: head.typeCell,
            keyCell: `consulting-${head.entrie}-${entry.entry_line_id}`,
            props: {
              id: `consultingEntryCheckbox${index}`,
              checked: currentSelect.filter(
                e => e.entry_line_id === entry.entry_line_id
              ).length > 0,
              onClick: (event) => {
                event.stopPropagation();
                setCurrentIndex(index);
                if (currentSelect.includes(entry)) {
                  updateSelect(
                    societyId,
                    _.filter(
                      currentSelect,
                      line => entry.entry_line_id !== line.entry_line_id
                    ),
                    accountID
                  );
                } else {
                  currentSelect.push(entry);
                  updateSelect(societyId, currentSelect, accountID);
                }
              },
              customColor: !entry.lettrage && 'yellow'
            }
          };
        }
        return cell;
      })
    }));
    footer = {
      props: {},
      row: [{
        keyRow: 'consulting-footer',
        value: footerData.map((head, i) => {
          let cell = {};
          if (head.typeCell === 'string') {
            let labelString;
            let classe;
            switch (head.entrie) {
            case 'credit':
              labelString = formatNumber(total_credit);
              classe = style.number;
              break;
            case 'debit':
              labelString = formatNumber(total_debit);
              classe = style.number;
              break;
            case 'totalValue':
              labelString = formatNumber(total_debit - total_credit);
              classe = style.number;
              break;
            default:
              labelString = I18n.t('totals');
              break;
            }
            cell = {
              _type: 'string',
              keyCell: `consulting-footer-${head.entrie}-${i}`,
              props: {
                label: labelString,
                cellClassName: classe,
                typoVariant: 'h6'
              }
            };
          } else {
            cell = {
              _type: 'string',
              keyCell: `consulting-footer-${head.entrie}-${i}`,
              props: {
                label: '',
                typoVariant: 'h6'
              },
              cellProp: {
                colSpan: head.value
              }
            };
          }
          return cell;
        })
      }
      ]
    };
  }

  const tableName = 'Consultation compte';

  const tableData = {
    header: {
      props: {},
      row: header
    },
    body: {
      props: {},
      row: body
    },
    footer
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    arrayData: tableData,
    tableError,
    tableLoading,
    tableLoad,
    societyId,
    total_credit: credit,
    setTableData: data => setConsultingTableData(
      tableName, societyId, data
    ),
    total_debit: debit,
    refresh,
    selectedItems
  };
};

// Enhance
const enhance = compose(
  splitWrap,
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  reduxForm({
    form: 'consultingForm',
    destroyOnUnmount: false,
    enableReinitialize: true
  })
);

export default enhance(TableConsulting);
