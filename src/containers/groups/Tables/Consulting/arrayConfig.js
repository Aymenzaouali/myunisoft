const headerData = [
  { type: 'checkbox', keyLabel: 'check_all' },
  { type: 'string', keyLabel: 'diary_code' },
  { type: 'sortCell', keyLabel: 'date' },
  { type: 'sortCell', keyLabel: 'piece' },
  { type: 'sortCell', keyLabel: 'piece2' },
  { type: 'sortCell', keyLabel: 'label' },
  { type: 'sortCell', keyLabel: 'debit', isAmount: true },
  { type: 'sortCell', keyLabel: 'credit', isAmount: true },
  { type: 'sortCell', keyLabel: 'solde', isAmount: true },
  { type: 'sortCell', keyLabel: 'lettrage', paddingLeft: true },
  { type: 'string', keyLabel: 'pj' },
  { type: 'string', keyLabel: 'Comm' },
  { type: 'string', keyLabel: 'deadline' },
  { type: 'string', keyLabel: 'payment_type_id' }
  // Disable for v.0
  // 
  // { type: 'string', keyLabel: 'axe1' },
  // { type: 'string', keyLabel: 'axe2' },
  // { type: 'string', keyLabel: 'axe3' },
  // { type: 'string', keyLabel: 'type' },
];

const bodyData = [
  { typeCell: 'checkbox', entrie: 'check' },
  { typeCell: 'string', entrie: 'diary_code' },
  { typeCell: 'string', entrie: 'date' },
  { typeCell: 'string', entrie: 'piece' },
  { typeCell: 'string', entrie: 'piece2' },
  { typeCell: 'string', entrie: 'label' },
  { typeCell: 'string', entrie: 'debit' },
  { typeCell: 'string', entrie: 'credit' },
  { typeCell: 'string', entrie: 'solde' },
  { typeCell: 'string', entrie: 'lettrage' },
  { typeCell: 'pj', entrie: 'pj_list' },
  { typeCell: 'string', entrie: 'comment' },
  { typeCell: 'string', entrie: 'deadline' },
  { typeCell: 'string', entrie: 'payment_type_id' }
  // Disable for v.0
  // { typeCell: 'string', entrie: 'comment' },
  // { typeCell: 'string', entrie: 'axe_1_id' },
  // { typeCell: 'string', entrie: 'axe_2_id' },
  // { typeCell: 'string', entrie: 'axe_3_id' },
  // { typeCell: 'string', entrie: 'comment' },
];

const footerData = [
  { typeCell: 'colSpan', value: 5 },
  { typeCell: 'string', entrie: 'total' },
  { typeCell: 'string', entrie: 'debit' },
  { typeCell: 'string', entrie: 'credit' },
  { typeCell: 'string', entrie: 'totalValue' },
  // Disable for v.0
  // { typeCell: 'colSpan', value: 9 }
  { typeCell: 'colSpan', value: 4 }
];

export { headerData, bodyData, footerData };
