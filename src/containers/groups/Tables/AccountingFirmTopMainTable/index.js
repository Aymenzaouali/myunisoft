import { connect } from 'react-redux';
import { AccountingFirmTopMainTable } from 'components/groups/Tables/';
import AccountingFirmSettingsService from 'redux/accountingFirmSettings';
import { AccountingFirmTopMainTableKeys } from 'assets/constants/keys';
import {
  CheckBoxCell
} from 'components/basics/Cells';
import { ACCOUNTING_FIRM_TOP_MAIN_TABLE, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import _ from 'lodash';
import I18n from 'assets/I18n';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, ACCOUNTING_FIRM_TOP_MAIN_TABLE);
  return {
    associate_list: _.get(state, `tables.${tableName}.data`, []),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    errors: _.get(state, `tables.${tableName}.errors`, {}),
    initialData: _.get(state, `tables.${tableName}.initialData`),
    society_id: state.navigation.id,
    isLoading: _.get(state, `accountingFirmSettings.${society_id}.isLoading`, false),
    isError: _.get(state, `accountingFirmSettings.${society_id}.isError`, false),
    pages: _.get(state, 'accountingFirmSettings.members.pages', 0),
    tableColumnDirections: _.get(state, 'accountingFirmSettings.tableColumnDirections', {}),
    rows_number: _.get(state, 'accountingFirmSettings.members.rows_number', 0),
    items_qty: _.get(state, 'accountingFirmSettings.items_qty', 10),
    page_number: _.get(state, 'accountingFirmSettings.page_number', 10)
  };
};

const mapDispatchToProps = dispatch => ({
  getMembers: order => dispatch(
    AccountingFirmSettingsService.getMembers(order)
  ),
  setItemsPerPage: items_qty => dispatch(
    AccountingFirmSettingsService.setItemsPerPage(items_qty)
  ),
  setCurrentPage: page => dispatch(
    AccountingFirmSettingsService.setCurrentPage(page)
  ),
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectOnlyOneRow: (tableName, ids) => dispatch(tableActions.selectOnlyOneRow(tableName, ids)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  setAccountFirmDirection: tableName => dispatch(
    AccountingFirmSettingsService.setAccountFirmDirection(tableName)
  ),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    selectedRowsCount,
    tableColumnDirections,
    associate_list,
    selectedRows,
    isLoading,
    isError
  } = stateProps;

  const tableName = getTableName(society_id, ACCOUNTING_FIRM_TOP_MAIN_TABLE);

  const {
    getMembers,
    unselectRow,
    selectAllRows,
    unselectAllRows,
    selectOnlyOneRow,
    setAccountFirmDirection
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const isAllSelected = selectedRowsCount === associate_list.length;

  const headers = [{
    keyRow: 'reportsAndFormsHeaderRow',
    value: AccountingFirmTopMainTableKeys.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;
      const dir = _.get(tableColumnDirections, `${[keyLabel]}`, 'desc');

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `reportsAndFormsHeaderCell${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = associate_list.map(al => al.member_id);
                selectAllRows(tableName, ids);
              }
            },
            id: keyLabel.toString()
          },
          cellProp: {
            style: {
              maxWidth: '44px'
            }
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `reportsAndFormsHeaderCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`accountingFirmSettings.table.${keyLabel}`),
          direction: dir,
          onSort: (order) => {
            getMembers({ [keyLabel]: order });
            setAccountFirmDirection({
              [keyLabel]: order === 'desc' ? 'asc' : 'desc'
            });
          }
        }
      };
    })

  }];

  const bodyRow = associate_list.map((member) => {
    const { member_id } = member;
    const ischecked = selectedRows[member_id] === true;
    const handleRowSelect = () => {
      if (ischecked) {
        unselectRow(tableName, member_id);
      } else {
        selectOnlyOneRow(tableName, member_id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    const generateKey = (cellName = '') => `${ACCOUNTING_FIRM_TOP_MAIN_TABLE}${cellName}${member_id}`;
    return {
      keyRow: generateKey(),
      props: {
        onClick: handleRowSelect,
        hover: true,
        selected: ischecked
      },
      value: [
        {
          component: CheckBoxCell,
          keyCell: generateKey('CheckBox'),
          props: {
            checked: ischecked,
            onChange: handleRowSelect
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('siret'),
          props: {
            children: _.get(member, 'siret', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('name'),
          props: {
            children: _.get(member, 'name', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('postal_code'),
          props: {
            children: _.get(member, 'postal_code', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('city'),
          props: {
            children: _.get(member, 'city', '')
          }
        }
      ]
    };
  });

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData: getMembers,
    isLoading,
    isError,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AccountingFirmTopMainTable);
