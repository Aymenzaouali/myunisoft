import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import FactoryTable from 'components/groups/Tables/Factory';
import _ from 'lodash';
import { push } from 'connected-react-router';
import { routesByKey } from 'helpers/routes';
import { clientUserRefKeys } from 'assets/constants/keys';
import tableActions from 'redux/tables/actions';
import { CLIENT_USER_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import {
  CheckBoxCell,
  SortCell
} from 'components/basics/Cells';
import {
  getClientUser as getClientUserThunk
} from 'redux/tabs/companyCreation/index';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const societyId = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id');
  const tableName = getTableName(societyId, CLIENT_USER_TABLE_NAME);
  const clientUserExportName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${CLIENT_USER_TABLE_NAME}`;
  return {
    clientUserInfo: _.get(getCurrentTabState(state), 'companyCreation.clientUserList'),
    clientUserExportName,
    clientUser: getCurrentTabState(state).companyCreation || {},
    societyId,
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    isLoading: _.get(getCurrentTabState(state), 'companyCreation.isLoading'),
    isError: _.get(getCurrentTabState(state), 'companyCreation.isError')
  };
};

const mapDispatchToProps = dispatch => ({
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  getClientUser: payload => dispatch(getClientUserThunk(payload)),
  push: path => dispatch(push(path)),
  setUserExportTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    selectedRows,
    selectedRowsCount,
    clientUserInfo,
    clientUser,
    isLoading,
    isError,
    clientUserExportName
  } = stateProps;

  const {
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows,
    getClientUser,
    setUserExportTableData,
    push
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const {
    array_users: clientUserList = []
  } = clientUserInfo;

  const headerKeys = clientUserRefKeys;

  const tableName = getTableName(societyId, CLIENT_USER_TABLE_NAME);

  const isAllSelected = clientUserList.length === selectedRowsCount;

  const orderDirection = _.get(clientUser, 'sort.direction') === 'asc' ? 'desc' : 'asc';

  const headers = [{
    keyRow: `${societyId}subsidiaryTableHeaderRow`,
    value: headerKeys.map((data, i) => {
      const { type, keyLabel } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: `${societyId}clientUserTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = clientUserList.map(lp => lp.id_pers_physique);
                selectAllRows(tableName, ids);
              }
            }
          }
        });
      } else {
        cell = {
          component: SortCell,
          keyCell: `${societyId}clientUserTableHeaderCell.${keyLabel}`,
          props: {
            children: I18n.t(`companyCreation.clientUser.clientUserArrayTitle.${keyLabel}`),
            onClick: () => {
              getClientUser({ societyId, sort: { column: keyLabel, direction: orderDirection } });
            }
          }
        };
      }
      return cell;
    })
  }];

  const body = clientUserList.map((e) => {
    const {
      user, profil, profil_type, id_pers_physique
    } = e;
    const isChecked = selectedRows[id_pers_physique] === true;
    const generateKey = (cellName = '') => `clientUser${cellName}${id_pers_physique}`;
    const handleRowSelect = () => {
      if (isChecked) {
        unselectRow(tableName, id_pers_physique);
      } else {
        selectRow(tableName, id_pers_physique);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };

    return {
      keyRow: generateKey(),
      props: {
        hover: true,
        selected: isChecked,
        onClick: () => push(routesByKey.userList)
      },
      value: [
        {
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: generateKey('CheckBox'),
          props: {
            onClick: e => e.stopPropagation(),
            checked: isChecked,
            onChange: handleRowSelect
          }
        },
        {
          component: 'div',
          keyCell: generateKey('users'),
          props: {
            children: user
          }
        },
        {
          component: 'div',
          keyCell: generateKey('profiltype'),
          props: {
            children: profil_type
          }
        },
        {
          component: 'div',
          keyCell: generateKey('profil'),
          props: {
            children: profil
          }
        }
      ]
    };
  });

  const transformedParam = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isLoading,
    isError,
    param: transformedParam,
    onRefresh: () => setUserExportTableData(
      clientUserExportName, societyId, transformedParam
    ),
    cellHeight: 32,
    maxVisibleItems: 10
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FactoryTable);
