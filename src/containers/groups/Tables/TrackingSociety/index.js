import { connect } from 'react-redux';
import TrackingSocietyTable from 'components/groups/Tables/TrackingSociety';
import I18n from 'assets/I18n';
import {
  getTrackingSocieties as getTrackingSocietiesThunk
} from 'redux/dashboard';
import {
  getCompany as getCompanyThunk
} from 'redux/tabs/companyList';
import { getExercises as getExercisesThunk } from 'redux/tabs/companyCreation';
import {
  formatDashboardData,
  getRouteKeyForDashboard,
  DISPLAYTYPEFIELDS
} from 'helpers/dashboard';
import { reset } from 'redux-form';
import { push } from 'connected-react-router';
import { getRouteByKey } from 'helpers/routes';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import _ from 'lodash';
import actionsNavigation from 'redux/navigation/actions';

const mapStateToProps = state => ({
  society_id: state.navigation.id,
  tracking_societies: _.get(state, 'dashboardWeb', []),
  currentMode: _.get(state, 'dashboardWeb.currentMode', 1),
  selectedPeriod: _.get(state, 'dashboardWeb.selectedPeriod', {}),
  formName: tabFormName('companyCreationForm', state.navigation.id),
  companyList: getCurrentTabState(state).companyList || {},
  tabs: state.navigation.tabs
});

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  getTrackingSocieties: opts => dispatch(getTrackingSocietiesThunk(opts)),
  getCompany: society_id => dispatch(getCompanyThunk(society_id)),
  getExercises: id => dispatch(getExercisesThunk(id)),
  resetCompanyCreationForm: form => dispatch(reset(form)),
  addTabs: tabs => dispatch(actionsNavigation.addTabs(tabs))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    currentMode,
    tracking_societies,
    tabs
  } = stateProps;
  const {
    getTrackingSocieties,
    addTabs
  } = dispatchProps;

  const {
    getLink,
    getPeriod,
    _initPeriod,
    redirectFromCollab
  } = ownProps;

  const loadData = async () => {
    await getTrackingSocieties();
  };

  const currentTrackingSocieties = tracking_societies || {};
  const { isLoading, isError } = currentTrackingSocieties;
  const trackingSocieties = _.get(currentTrackingSocieties, 'trackingSocieties', []);
  const sort = _.get(currentTrackingSocieties, 'sort', {});
  const formatedData = formatDashboardData(currentMode, trackingSocieties);

  const handleRedirect = async (path, param, soc_id, society) => {
    redirectFromCollab(path, param, soc_id);
    const isSocietyIntabs = tabs.find(t => parseInt(t.id, 10) === parseInt(society.company_id, 10));
    if (!isSocietyIntabs) {
      const tab = [{
        id: society.company_id,
        label: society.company_name,
        path: `/tab/${society.company_id}/dashBoard`,
        isClosable: true,
        secured: society.secured
      }];
      await addTabs(tab);
    }
  };

  const handleFolderClick = async (society_id, society) => {
    const route = getRouteByKey('dashboard');
    handleRedirect(route.path, '', society_id, society);
  };

  const handleBubbleClick = (flux, society_id, month = '', society) => {
    const dataType = flux.substring(0, 2) === 'is' ? 'is' : flux;
    const routeKey = getRouteKeyForDashboard(dataType);
    const route = getRouteByKey(routeKey);

    const period = getPeriod(month);

    switch (dataType) {
    case 'vat':
      _initPeriod(dataType, period, society_id);
      handleRedirect(route.path, '', society_id, society);
      break;
    case 'bank_flow':
      handleRedirect(route.path, 'ib', society_id, society);
      break;
    case 'ocr_flow':
      handleRedirect(route.path, 'o', society_id, society);
      break;
    case 'manual_flow':
      handleRedirect(route.path, 'm', society_id, society);
      break;
    case 'amount':
      handleRedirect(route.path, 'e', society_id, society);
      break;
    case 'first_is_deposit':
      handleRedirect(route.path, '', society_id, society);
      break;
    case 'cvae':
      _initPeriod(dataType, period, society_id);
      handleRedirect(route.path, '', society_id, society);
      break;
    case 'is':
      _initPeriod(dataType, period, society_id);
      handleRedirect(route.path, '', society_id, society);
      break;
    case 'balance_sheet':
      handleRedirect(route.path, '', society_id, society);
      break;
    case 'cfe':
      // not implemented yet
      // handleRedirect(route.path, '', society_id, society);
      break;
    case 'ago':
      // not implemented yet
      // handleRedirect(route.path, '', society_id, society);
      break;
    default:
      break;
    }
  };


  const headers = [{
    keyRow: 'companies-monitoring-table-header-row',
    value: DISPLAYTYPEFIELDS[currentMode].map((cell) => {
      const direction = _.get(sort, 'column') === cell ? sort.direction : 'desc';
      let headerCell;

      if (!cell.includes('month')) {
        headerCell = {
          _type: 'sortCell',
          keyCell: `trackingSocietyHeader.${cell}`,
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 2
            }
          },
          props: {
            children: I18n.t(`companiesMonitoring.fields.${cell}`),
            direction,
            onClick: () => getTrackingSocieties(
              {
                sort: {
                  column: `${cell}`,
                  direction: direction === 'asc' ? 'desc' : 'asc'
                }
              }
            )
          }
        };
      } else {
        headerCell = {
          _type: 'string',
          keyCell: `trackingSocietyHeader.${cell}`,
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 2
            }
          },
          props: {
            label: I18n.t(`companiesMonitoring.fields.${cell}`),
            typoVariant: 'h5'
          }
        };
      }

      return headerCell;
    })
  }];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    getLink,
    isLoading,
    formatedData,
    handleFolderClick,
    handleBubbleClick,
    isError,
    loadData,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {}
        // row: body
      }
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TrackingSocietyTable);
