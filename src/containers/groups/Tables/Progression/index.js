import { connect } from 'react-redux';
import { change } from 'redux-form';
import { windev } from 'helpers/api';
import ProgressionTable from 'components/groups/Tables/Progression';
import {
  getProgressionSociety as getProgressionSocietyThunk,
  initAccountingFilter as initAccountingFilterThunk
} from 'redux/dashboard';
import {
  redirectFromCollab as redirectFromCollabThunk
} from 'redux/navigation';
import {
  initPeriod
} from 'helpers/dashboard';
import actions from 'redux/dashboard/actions';
import _ from 'lodash';

const mapStateToProps = state => ({
  consultingFilterForm: `${state.navigation.id}consultingFilter`,
  society_id: state.navigation.id,
  progression_society: _.get(state, 'dashboardWeb', [])
});

const mapDispatchToProps = dispatch => ({
  initHeaderPeriod: (form, value) => dispatch(change(form, 'period', value)),
  initFilterForVat: period => dispatch(actions.initFilterForVat(period)),
  initFilterForIs: period => dispatch(actions.initFilterForIs(period)),
  initFilterForCvae: period => dispatch(actions.initFilterForCvae(period)),
  clearDataForAccounting: () => dispatch(actions.clearDataForAccounting()),
  clearFilterForVat: () => dispatch(actions.clearFilterForVat()),
  clearFilterForIs: () => dispatch(actions.clearFilterForIs()),
  clearFilterForCvae: () => dispatch(actions.clearFilterForCvae()),
  getProgressionSociety: opts => dispatch(getProgressionSocietyThunk(opts)),
  setFilter: filter => dispatch(initAccountingFilterThunk(filter)),
  redirectFromCollab: (route, type, society_id) => {
    dispatch(redirectFromCollabThunk(route, type, society_id));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { progression_society, society_id } = stateProps;
  const {
    setFilter, getProgressionSociety, initHeaderPeriod,
    initFilterForVat, initFilterForIs, initFilterForCvae
  } = dispatchProps;

  const loadData = async () => {
    await getProgressionSociety();
  };

  const _initPeriod = (type, period) => {
    switch (type) {
    case 'is':
      return initPeriod(period, initFilterForIs, initHeaderPeriod, `${society_id}ISDeclarationHeader`);
    case 'vat':
      return initPeriod(period, initFilterForVat, initHeaderPeriod, `${society_id}VATHeader`);
    case 'cvae':
      return initPeriod(period, initFilterForCvae, initHeaderPeriod, `${society_id}BundleHeader`);
    default:
      return undefined;
    }
  };

  const getInfoForInitPeriod = (type) => {
    switch (type) {
    case 'is':
      return {
        initFilter: initFilterForIs,
        initHeader: initHeaderPeriod,
        headerName: `${society_id}ISDeclarationHeader`
      };
    case 'vat':
      return {
        initFilter: initFilterForVat,
        initHeader: initHeaderPeriod,
        headerName: `${society_id}VATHeader`
      };
    case 'cvae':
      return {
        initFilter: initFilterForCvae,
        initHeader: initHeaderPeriod,
        headerName: `${society_id}BundleHeader`
      };
    default:
      return undefined;
    }
  };

  const getLink = async (params) => {
    const response = await windev.makeApiCall('/dashboard/society/vat_link', 'get', params, {});
    const { data } = response;

    return data.link;
  };

  const changeAccountingFilter = (startDate, endDate, diary_code) => {
    const filter = {
      start_date: startDate,
      end_date: endDate,
      diary_code
    };

    setFilter(filter);
  };

  const currentProgression = progression_society || {};
  const { isLoading, isError } = currentProgression;
  const progression = _.get(currentProgression, 'progression', []);
  const sort = _.get(currentProgression, 'sort', {});
  const account = _.get(currentProgression, 'account', {});
  const headers = _.get(progression, 'period', []);
  const body = _.get(progression, 'data', []);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isLoading,
    isError,
    loadData,
    headers,
    body,
    sort,
    account,
    changeAccountingFilter,
    _initPeriod,
    getLink,
    getInfoForInitPeriod
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ProgressionTable);
