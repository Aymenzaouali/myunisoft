import { connect } from 'react-redux';
import UserTable from 'components/groups/Tables/Utilisateur';
import { arrayPush } from 'redux-form';
import I18n from 'assets/I18n';
import { tabFormName } from 'helpers/tabs';
import _ from 'lodash';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  societies: _.get(state, 'navigation.societies', [])
});

const mapDispatchToProps = dispatch => ({
  addAllowedCompany: (societyId, company, level) => {
    dispatch(arrayPush(tabFormName('physicalPersonCreationForm', societyId), 'user', { company, level }));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societies } = stateProps;

  const formatSocietiesForAutoComplete = societies.map(
    society => ({ ...society, label: society.name, value: society.society_id })
  );

  const accessLevel = [
    { label: I18n.t('newUserForm.societyGranting.accessLevel.firstLevel'), value: -1, name: 'access_list_1' }, // value 8 par défaut selon back
    { label: I18n.t('newUserForm.societyGranting.accessLevel.secondLevel'), value: -1, name: 'access_list_2' },
    { label: I18n.t('newUserForm.societyGranting.accessLevel.thirdLevel'), value: -1, name: 'access_list_3' }
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    societies: formatSocietiesForAutoComplete,
    accessLevel
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(UserTable);
