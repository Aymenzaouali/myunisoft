import { connect } from 'react-redux';
import {
  arrayRemove,
  getFormValues
} from 'redux-form';
import UtilisateurArray from 'components/groups/Tables/Utilisateur/UtilisateurArray';
import { delSocietyAccess as delSocietyAccessThunk } from 'redux/tabs/physicalPersonCreation';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    physicalPersonForm: getFormValues(tabFormName('physicalPersonCreationForm', societyId))(state),
    user_id: _.get(getCurrentTabState(state), 'physicalPersonCreation.selected_physical_person.user_id')
  };
};
const mapDispatchToProps = dispatch => ({
  removeCompany: (societyId, index, companyInfo, id) => {
    dispatch(arrayRemove(tabFormName('physicalPersonCreationForm', societyId), 'user', index));
    dispatch(delSocietyAccessThunk(id, companyInfo));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { physicalPersonForm = {} } = stateProps;
  const { user = [] } = physicalPersonForm;
  const { access_list = [] } = user;

  const firms = access_list.map(e => ({
    company: { name: e.label, society_id: e.society_id },
    level: { label: e.profil_name }
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    firms
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(UtilisateurArray);
