import { connect } from 'react-redux';
import { BankIntegrationSettingsTable } from 'components/groups/Tables/';
import BISettingsService from 'redux/bankIntegrationSettings';
import { bankIntegrationSettingsColumns } from 'assets/constants/keys';
import actions from 'redux/bankIntegrationSettings/actions';
import tableActions from 'redux/tables/actions';
import _ from 'lodash';
import I18n from 'assets/I18n';
import styles from './bankIntegrationSettingsTable.module.scss';


const mapStateToProps = state => ({
  societyId: state.navigation.id,
  statementSelectedLine: state.bankIntegrationSettings.statementSelectedLine,
  statements: state.bankIntegrationSettings.statements,
  selectedStatements: _.get(state, `bankIntegrationSettings.selectedStatements[${state.navigation.id}]`, [])
});

const mapDispatchToProps = dispatch => ({
  getStatementsList: () => dispatch(
    BISettingsService.getStatementsList()
  ),
  addAllStatements: society_Id => dispatch(actions.addAllStatements(society_Id)),
  resetAllStatements: society_Id => dispatch(actions.resetAllStatements(society_Id)),
  selectStatement: society_Id => dispatch(actions.selectStatement(society_Id)),
  addStatement: (statmentId, society_Id) => dispatch(actions.addStatement(statmentId, society_Id)),
  removeStatement: (statmentId, society_Id) => dispatch(
    actions.removeStatement(statmentId, society_Id)
  ),
  setBITableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    statementSelectedLine,
    statements,
    selectedStatements
  } = stateProps;
  const {
    getStatementsList,
    addAllStatements,
    resetAllStatements,
    selectStatement,
    addStatement,
    removeStatement,
    setBITableData
  } = dispatchProps;

  let isAllSelected = true;

  const currentStatements = statements[societyId] || {};

  const { isLoading, isError } = currentStatements;

  const statements_list = _.get(currentStatements, 'statements_list', []);

  statements_list.forEach((wallet) => {
    if (!selectedStatements.some(item => item.id === wallet.id)) {
      isAllSelected = false;
    }
  });

  const headers = [{
    keyRow: 'settingsHeaderRow',
    value: bankIntegrationSettingsColumns.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `settingsHeaderCell${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllStatements(societyId);
              } else {
                addAllStatements(societyId);
              }
            },
            id: keyLabel.toString()
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `settingsHeaderCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`bankIntegrationSettings.table.${keyLabel}`)
        }
      };
    })

  }];

  const bodyRow = statements_list.map((statement, i) => ({
    keyRow: `settingsRow${i}`,
    props: {
      hover: true
    },
    value: [
      {
        _type: 'checkbox',
        keyCell: `settingsCheckBox${i}`,
        props: {
          checked: selectedStatements
            .some(
              item => item.id_cfg_import_releve_bancaire
                === statement.id_cfg_import_releve_bancaire
            ),
          onClick: (e) => {
            e.stopPropagation();
          },
          onChange: () => {
            if (selectedStatements.some(
              item => item.id_cfg_import_releve_bancaire
                === statement.id_cfg_import_releve_bancaire
            )) {
              if (statement.id_cfg_import_releve_bancaire
                === _.get(statementSelectedLine, 'id', '')) selectStatement({});
              removeStatement(statement.id_cfg_import_releve_bancaire, societyId);
            } else {
              selectStatement({ id: statement.id_cfg_import_releve_bancaire });
              addStatement(statement, societyId);
            }
          },
          id: i.toString()
        },
        cellProp: {
          className: styles.tableCell
        }
      },
      {
        _type: 'string',
        keyCell: `settingsCode${i}`,
        props: {
          label: _.get(statement, 'forced_label', '')
        },
        cellProp: {
          className: styles.tableCell
        }
      },
      {
        _type: 'string',
        keyCell: `settingsCode${i}`,
        props: {
          label: _.get(statement, 'account.number', '')
        },
        cellProp: {
          className: styles.tableCell
        }
      },
      {
        _type: 'checkbox',
        keyCell: `settingsCheckBox${i}`,
        props: {
          checked: statement.debit,
          onClick: (e) => {
            e.stopPropagation();
          },
          id: i.toString()
        },
        cellProp: {
          className: styles.tableCell
        }
      },
      {
        _type: 'checkbox',
        keyCell: `settingsCheckBox${i}`,
        props: {
          checked: statement.credit,
          onClick: (e) => {
            e.stopPropagation();
          },
          id: i.toString()
        },
        cellProp: {
          className: styles.tableCell
        }
      },
      {
        _type: 'string',
        keyCell: `settingsCode${i}`,
        props: {
          label: _.get(statement, 'list_code_interbancaire', []).map(code => code.intitule).join(', ')
        },
        cellProp: {
          className: styles.tableCell
        }
      },
      {
        _type: 'string',
        keyCell: `settingsCode${i}`,
        props: {
          label: _.get(statement, 'label_to_find', '')
        },
        cellProp: {
          className: styles.tableCell
        }
      }
    ]
  }));

  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData: getStatementsList,
    setTableData: param => setBITableData(tableName, societyId, param),
    isLoading,
    isError,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(BankIntegrationSettingsTable);
