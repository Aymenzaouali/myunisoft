import { connect } from 'react-redux';
import DocManTable from 'components/groups/Tables/DocManTable';
import DocManService from 'redux/docManagement';
import tableActions from 'redux/tables/actions';
import { GEDTabs } from 'assets/constants/data';

import actions from '../../../../redux/docManagement/actions';

const mapStateToProps = state => ({
  gedTableData: state.docManagement.gedTableData,
  society_id: state.navigation.id,
  activeTabId: state.docManagement.activeTabId,
  documents: state.docManagement.documents,
  documentMode: state.docManagement.documentMode,
  headersDocs: state.docManagement.headers,
  currentPage: state.docManagement.currentPage,
  itemsPerPage: state.docManagement.itemsPerPage,
  totalDocs: state.docManagement.totalDocs,
  activeFolder: state.docManagement.activeFolder,
  linked_docs: state.docManagement.linked_docs
});

const mapDispatchToProps = dispatch => ({
  getDocuments: (leaf, page, rowsPerPage, sort_name, sort_order) => dispatch(
    DocManService.getDocuments(leaf, page, rowsPerPage, sort_name, sort_order)
  ),
  setDocumentToCopy: docInfo => dispatch(
    actions.setDocumentToCopy(docInfo)
  ),
  showSendInDiscussionModal: (status, document) => dispatch(
    actions.showSendInDiscussionModal(status, document)
  ),
  setGEDTableData: (tableName, society_id, data) => dispatch(
    tableActions.setTransformedTableData(tableName, society_id, data)
  ),
  setItemsPerPage: rowsPerPage => dispatch(
    actions.setItemsPerPage(rowsPerPage)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    activeTabId
  } = stateProps;

  const { setGEDTableData } = dispatchProps;

  const GEDActiveTable = GEDTabs.find(tab => activeTabId === tab.id).label;

  return {
    setTableData: data => setGEDTableData(GEDActiveTable, society_id, data),
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DocManTable);
