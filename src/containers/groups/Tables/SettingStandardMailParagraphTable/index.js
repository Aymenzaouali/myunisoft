import { connect } from 'react-redux';
import { SettingStandardMailParagraphTable } from 'components/groups/Tables/';
import SettingStandardMailService from 'redux/settingStandardMail';
import { paragraphKeys } from 'assets/constants/keys';
import {
  CheckBoxCell
} from 'components/basics/Cells';
import { PARAGRAPH_TABLE, PARAGRAPHS_TYPES_TABLE, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import _ from 'lodash';
import I18n from 'assets/I18n';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, PARAGRAPH_TABLE);
  const tableNameParagraphsTypes = getTableName(society_id, PARAGRAPHS_TYPES_TABLE);
  return {
    associate_list: _.get(state, `tables.${tableName}.data`, []),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    errors: _.get(state, `tables.${tableName}.errors`, {}),
    initialData: _.get(state, `tables.${tableName}.initialData`),
    society_id: state.navigation.id,
    selectedParagraphsTypesRowsCount: _.get(state, `tables.${tableNameParagraphsTypes}.selectedRowsCount`),
    initialParagraphsTypesData: _.get(state, `tables.${tableName}.initialData`)
  };
};

const mapDispatchToProps = dispatch => ({
  getParagraphs: id => dispatch(
    SettingStandardMailService.getParagraphs(id)
  ),
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors)),
  setTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    selectedRowsCount,
    associate_list,
    selectedRows
  } = stateProps;

  const tableName = getTableName(society_id, PARAGRAPH_TABLE);

  const {
    getParagraphs,
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows,
    setTableData
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const isAllSelected = selectedRowsCount === associate_list.length;

  const headers = [{
    keyRow: 'paragraphsTypesTableHeaderRow',
    value: paragraphKeys.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `paragraphsTypesTableHeaderCell${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = associate_list.map(al => al.unique_id);
                selectAllRows(tableName, ids);
              }
            },
            id: keyLabel.toString()
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `paragraphsTypesTableHeaderCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`settingStandardMail.tables.${keyLabel}`)
        }
      };
    })

  }];

  const bodyRow = associate_list.map((statement) => {
    const { unique_id: id } = statement;
    const ischecked = selectedRows[id] === true;
    const handleRowSelect = () => {
      if (ischecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    const generateKey = (cellName = '') => `${PARAGRAPH_TABLE}${cellName}${id}`;
    return {
      keyRow: generateKey(),
      props: {
        onDoubleClick: () => getParagraphs(statement.id),
        hover: true,
        selected: ischecked
      },
      value: [
        {
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: generateKey('CheckBox'),
          props: {
            checked: ischecked,
            onChange: handleRowSelect
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('label'),
          props: {
            children: _.get(statement, 'label', ''),
            style: {
              maxWidth: '250px',
              whiteSpace: 'no-wrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis'
            }
          }
        },
        {
          component: 'div',
          keyCell: generateKey('paragraph_text'),
          props: {
            children: _.get(statement, 'paragraph_text', ''),
            style: {
              maxWidth: '350px',
              whiteSpace: 'no-wrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis'
            }
          }
        }
      ]
    };
  });
  const transformedParam = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: bodyRow
    }
  };
  const tableExportName = `${I18n.t('settingStandardMail.title')}_bottom`;
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    param: transformedParam,
    setTable: () => setTableData(tableExportName, society_id, transformedParam)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(SettingStandardMailParagraphTable);
