import _get from 'lodash/get';
import moment from 'moment';
import { connect } from 'react-redux';
import { TableFactory } from 'components/groups/Tables';
import { collectorKeys } from 'assets/constants/keys';
import I18n from 'assets/I18n';
import tables from 'redux/tables/actions';
import { getCollector } from 'redux/collector';
import { getTableName } from 'assets/constants/tableName';

const pageName = 'collector';
const mapStateToProps = state => ({
  society_id: state.navigation.id,
  documentsList: state.collector.documentsList[state.navigation.id],
  isLoading: _get(state, `collector.documentsList[${state.navigation.id}].isLoading`, false),
  isError: _get(state, `collector.documentsList[${state.navigation.id}].isError`, false),
  selectedBills: _get(state, `tables[${getTableName(state.navigation.id, pageName)}].selectedRows`, {}),
  selectedBillsCount: _get(state, `tables[${getTableName(state.navigation.id, pageName)}].selectedRowsCount`, 0)
});

const mapDispatchToProps = dispatch => ({
  loadData: () => dispatch(getCollector()),
  addBill: (id, society_id) => dispatch(
    tables.selectRow(getTableName(society_id, pageName), id)
  ),
  removeBill: (id, society_id) => dispatch(
    tables.unselectRow(getTableName(society_id, pageName), id)
  ),
  resetAllBills: society_id => dispatch(tables.unselectAllRows(getTableName(society_id, pageName))),
  addAllBills: (society_id, bills) => dispatch(
    tables.selectAllRows(getTableName(society_id, pageName), bills)
  ),
  setCollectorTableData: (tableName, societyId, params) => dispatch(
    tables.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps) => {
  const {
    documentsList,
    isLoading,
    isError,
    selectedBills,
    selectedBillsCount,
    society_id
  } = stateProps;

  const {
    addBill,
    removeBill,
    resetAllBills,
    addAllBills,
    setCollectorTableData
  } = dispatchProps;

  const { loadData } = dispatchProps;
  const currentDocuments = _get(documentsList, 'documents', []);
  const isAllSelected = selectedBillsCount && selectedBillsCount === currentDocuments.length;
  const headers = [{
    keyRow: 'collectorHeaderRow',
    value: collectorKeys.map((key, i) => {
      if (key === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `collectorHeaderCell.${key}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllBills(society_id);
              } else {
                addAllBills(society_id, currentDocuments.map(d => d.id));
              }
            },
            id: key
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `collectorHeaderCell.${key}${i}`,
        props: {
          children: I18n.t(`tables.collector.${key}`),
          colorErrorIcon: true
        }
      };
    })
  }];
  const body = currentDocuments.map((collector, i) => {
    const isSelected = selectedBills[collector.id];
    return {
      keyRow: `collector${i}`,
      props: {
        hover: true
      },
      value: [
        {
          _type: 'checkbox',
          keyCell: `collectorBox${i}`,
          props: {
            checked: isSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isSelected) {
                removeBill(collector.id, society_id);
              } else {
                addBill(collector.id, society_id);
              }
            },
            id: i
          }
        },
        {
          _type: 'string',
          keyCell: `collectorProvider${i}`,
          props: {
            label: collector.ProviderName
          }
        },
        {
          _type: 'string',
          keyCell: `collectorDate${i}`,
          props: {
            label: moment(collector.BillDate).format('DD/MM/YYYY'),
            labelClassName: 'collector__table-label'
          }
        },
        {
          _type: 'string',
          keyCell: `collectorName${i}`,
          props: {
            label: collector.BillName
          }
        },
        {
          _type: 'string',
          keyCell: `collectorAmount${i}`,
          props: {
            label: collector.BillAmount,
            labelClassName: 'collector__tableAmount'
          },
          cellProp: {
            style: {
              textAlign: 'right'
            }
          }
        }
      ]
    };
  });

  const formattedTableData = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  const tableName = I18n.t('collector.title');

  return {
    ...dispatchProps,
    loadData,
    param: formattedTableData,
    onRefresh: () => setCollectorTableData(tableName, society_id, formattedTableData),
    isLoading,
    isError
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TableFactory);
