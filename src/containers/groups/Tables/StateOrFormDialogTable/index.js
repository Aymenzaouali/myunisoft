import { connect } from 'react-redux';
import { StateOrFormDialogTable } from 'components/groups/Tables/';
import { reportsAndFormsOrder } from 'assets/constants/keys';
import ReportsAndFormsService from 'redux/reportsAndForms';
import _ from 'lodash';
import {
  CheckBoxCell
} from 'components/basics/Cells';
import I18n from 'assets/I18n';
import { STATE_OR_FORM_DIALOG_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import { tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const current_society_id = _.get(state, `form.${tabFormName('reportsAndFormsForm', society_id)}.values.society_id`, 0);
  const tableName = getTableName(society_id, STATE_OR_FORM_DIALOG_TABLE_NAME);
  return {
    associate_list: _.get(state, `tables.${tableName}.data`, []),
    society_id: current_society_id,
    societyId: state.navigation.id,
    navigation_society_id: society_id,
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    stateAndFormsForDialog: state.reportsAndForms.stateAndFormsForDialog
  };
};

const mapDispatchToProps = dispatch => ({
  getStateOrFormDialogTable: id => dispatch(
    ReportsAndFormsService.getStateOrFormDialogTable(id)
  ),
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedRows,
    navigation_society_id,
    associate_list,
    selectedRowsCount
  } = stateProps;
  const tableName = getTableName(navigation_society_id, STATE_OR_FORM_DIALOG_TABLE_NAME);

  const {
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;


  const isAllSelected = selectedRowsCount === associate_list.length;

  const headers = [{
    keyRow: 'stateAndFormsForDialogRow',
    value: reportsAndFormsOrder.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      if (type === 'checkbox') {
        return {
          component: CheckBoxCell,
          keyCell: `stateAndFormsForDialogCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = associate_list.map(al => al.id);
                selectAllRows(tableName, ids);
              }
            }
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `stateAndFormsForDialogCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`reportsAndForms.orderTable.${keyLabel}`)
        }
      };
    })

  }];

  const bodyRow = associate_list.map((statement) => {
    const { id } = statement;
    const ischecked = selectedRows[id] === true;
    const handleRowSelect = () => {
      if (ischecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    const generateKey = (cellName = '') => `stateAndFormsForDialogRow${cellName}${id}`;
    return {
      keyRow: generateKey(),
      props: {
        hover: true,
        selected: ischecked
      },
      value: [
        {
          component: CheckBoxCell,
          keyCell: generateKey('CheckBox'),
          props: {
            checked: ischecked,
            onChange: handleRowSelect
          }
        }, {
          component: 'div',
          keyCell: generateKey('orderNumber'),
          props: {
            children: _.get(statement, 'orderNumber', '')
          }
        }, {
          component: 'div',
          keyCell: generateKey('codeEDI'),
          props: {
            children: _.get(statement, 'codeEDI', '')
          }
        }, {
          component: 'div',
          keyCell: generateKey('wording'),
          props: {
            children: _.get(statement, 'wording', '')
          }
        }, {
          component: 'div',
          keyCell: generateKey('dataType'),
          props: {
            children: _.get(statement, 'dataType', '')
          }
        }, {
          component: 'div',
          keyCell: generateKey('viewing'),
          props: {
            children: _.get(statement, 'viewing', '')
          }
        }, {
          component: 'div',
          keyCell: generateKey('dataSource'),
          props: {
            children: _.get(statement, 'dataSource', '')
          }
        }, {
          component: 'div',
          keyCell: generateKey('references'),
          props: {
            children: _.get(statement, 'references', '')
          }
        }
      ]
    };
  });

  return {
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(StateOrFormDialogTable);
