import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import PhysicalPerson from 'components/groups/Tables/PhysicalPerson';
import { physicalPersonEmployeeRefKeys } from 'assets/constants/keys';
import {
  FunctionReduxSelect,
  SignatoryFunctionReduxSelect
} from 'containers/reduxForm/Inputs';
import { EMPLOYEE_PP_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import {
  CheckBoxCell,
  DatePickerCell,
  SortCell
} from 'components/basics/Cells';
import moment from 'moment';
import { getAutoCompleteProps } from 'redux/tables/utils';
import _ from 'lodash';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, EMPLOYEE_PP_TABLE_NAME);
  return {
    employee_list_pp: _.get(state, `tables.${tableName}.data`, []),
    isEditing: _.get(state, `tables.${tableName}.isEditing`, false),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`),
    initialData: _.get(state, `tables.${tableName}.initialData`),
    society_id,
    errors: _.get(state, `tables.${tableName}.errors`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  editRow: (tableName, item) => dispatch(tableActions.editRow(tableName, item)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors)),
  setPhysicalPersonEmployeeTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    employee_list_pp,
    isEditing,
    selectedRows,
    selectedRowsCount,
    lastSelectedRow,
    errors,
    initialData
  } = stateProps;

  const {
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows,
    editRow,
    setErrors,
    setPhysicalPersonEmployeeTableData
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const tableName = getTableName(society_id, EMPLOYEE_PP_TABLE_NAME);

  const isAllSelected = employee_list_pp.length === selectedRowsCount;

  const headerKeys = physicalPersonEmployeeRefKeys;

  const headers = [{
    keyRow: `${society_id}EmployeePPTableHeaderRow0`,
    value: headerKeys.map((data, i) => {
      const { type, keyLabel, isAmount } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: `${society_id}EmployeePPTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = employee_list_pp.map(employee => employee.id);
                selectAllRows(tableName, ids);
              }
            }
          }
        });
      } else {
        cell = {
          component: SortCell,
          keyCell: `${society_id}EmployeePPTableHeaderCell.${keyLabel}`,
          cellProp: {
            isAmount
          },
          props: {
            children: I18n.t(`physicalPersonCreation.employee_form.employeePPArrayTitle.${keyLabel}`),
            onClick: () => {}
          }
        };
      }
      return cell;
    })
  }];
  const body = employee_list_pp.map((employee) => {
    const { id } = employee;
    const generateKey = (cellName = '') => `EmployeePP${cellName}${id}`;
    const isChecked = selectedRows[id] === true;
    const isEditingRow = isChecked && isEditing;
    const startDate = _.get(employee, 'start_date', '');
    const endDate = _.get(employee, 'end_date', '');
    const formattedStartDate = moment(startDate).isValid() ? moment(startDate).format('DD/MM/YYYY') : '';
    const formattedEndDate = moment(endDate).isValid() ? moment(endDate).format('DD/MM/YYYY') : '';
    const isLastSelected = lastSelectedRow === id;
    const employeeErrors = _.get(errors, employee.id, {});
    const ini = initialData.filter(e => e.id === employee.id);
    const dateEndPresente = _.get(ini, 'end_date', '');
    const handleRowSelect = () => {
      if (isChecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    return {
      keyRow: generateKey(),
      props: {
        hover: true,
        selected: isChecked
      },
      value: [
        {
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: generateKey('CheckBox'),
          props: {
            checked: isChecked,
            onChange: handleRowSelect
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Name'),
          props: {
            children: _.get(employee, 'society.name') || _.get(employee, 'name', '')
          }
        },
        {
          component: isEditingRow ? FunctionReduxSelect : 'div',
          error: _.get(employeeErrors, 'function'),
          keyCell: generateKey('Function'),
          props: isEditingRow
            ? {
              autoFocus: isLastSelected,
              ...getAutoCompleteProps(editRow, employee, 'function.label', 'function', tableName),
              onChange: (value) => {
                editRow(tableName, { ...employee, function: value });
                setErrors(tableName, employee.id, { function: undefined });
              }
            }
            : {
              children: _.get(employee, 'function.label')
            }
        },
        {
          component: isEditingRow ? SignatoryFunctionReduxSelect : 'div',
          keyCell: generateKey('SignatoryFunction'),
          error: _.get(employeeErrors, 'signatory_function'),
          props: isEditingRow
            ? {
              ...getAutoCompleteProps(editRow, employee, 'signatory_function.label', 'signatory_function', tableName),
              onChange: (value) => {
                editRow(tableName, { ...employee, signatory_function: value });
                setErrors(tableName, employee.id, { signatory_function: undefined });
              },
              menuPosition: 'absolute'
            }
            : {
              children: _.get(employee, 'signatory_function.label', '')
            }
        },
        {
          component: isEditingRow ? DatePickerCell : 'div',
          keyCell: generateKey('StartDate'),
          error: _.get(employeeErrors, 'effective_date'),
          props: isEditingRow ? {
            value: startDate,
            onChange: (value) => {
              editRow(tableName, {
                ...employee,
                start_date: moment(value).format('YYYY-MM-DD')
              });
              setErrors(tableName, employee.id, { effective_date: undefined });
            }
          } : {
            children: formattedStartDate
          }
        },
        {
          component: isEditingRow && !dateEndPresente ? DatePickerCell : 'div',
          keyCell: generateKey('DateEnd'),
          props: isEditingRow ? {
            value: endDate,
            onChange: value => editRow(tableName, {
              ...employee,
              end_date: moment(value).format('YYYY-MM-DD')
            })
          } : {
            children: formattedEndDate
          }
        }
      ]
    };
  });

  const formattedParams = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };
  const exportTableName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${EMPLOYEE_PP_TABLE_NAME}`;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    param: formattedParams,
    setTableData: () => setPhysicalPersonEmployeeTableData(
      exportTableName, society_id, formattedParams
    )
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(PhysicalPerson);
