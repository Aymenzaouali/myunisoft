import { connect } from 'react-redux';
import DairyTable from 'components/groups/Tables/DairyTable';
import { getDiaries as getDiariesThunk } from 'redux/diary';
import actions from 'redux/diary/actions';
import tables from 'redux/tables/actions';
import { diaryKeys } from 'assets/constants/keys';
import I18n from 'assets/I18n';
import _ from 'lodash';
import { getTableName } from 'assets/constants/tableName';

const pageName = 'diary';
const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    diaries: state.diary.diaries,
    selectedDiary: _.get(state, `tables[${getTableName(societyId, pageName)}].selectedRows`, {}),
    selectedDiaryCount: _.get(state, `tables[${getTableName(societyId, pageName)}].selectedRowsCount`, 0),
    diarySelectedLine: state.diary.diarySelectedLine
  };
};

const mapDispatchToProps = dispatch => ({
  getDiaries: opts => dispatch(getDiariesThunk(opts)),
  addDiary: (society_Id, diaryId) => dispatch(
    tables.selectRow(getTableName(society_Id, pageName), diaryId)
  ),
  removeDiary: (society_Id, diaryId) => dispatch(
    tables.unselectRow(getTableName(society_Id, pageName), diaryId)
  ),
  addAllDiary: (society_Id, ids) => dispatch(
    tables.selectAllRows(getTableName(society_Id, pageName), ids)
  ),
  setDairyTableData: (tableName, society_id, formattedTableData) => dispatch(
    tables.setTransformedTableData(tableName, society_id, formattedTableData)
  ),
  resetAllDiary: society_Id => dispatch(tables.unselectAllRows(getTableName(society_Id, pageName))),
  selectDiary: diarySelectedLine => dispatch(actions.selectDiary(diarySelectedLine))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    diaries,
    selectedDiary,
    selectedDiaryCount,
    diarySelectedLine
  } = stateProps;
  const {
    getDiaries,
    addDiary,
    removeDiary,
    addAllDiary,
    resetAllDiary,
    selectDiary,
    setDairyTableData
  } = dispatchProps;
  const { forExport, selectedSociety } = ownProps;
  const currentDiaries = diaries[selectedSociety] || {};
  const { isLoading, isError } = currentDiaries;
  const diaries_list = _.get(currentDiaries, 'diary_list', []);
  const sort = _.get(currentDiaries, 'sort', {});

  const selectableDiariesIds = diaries_list
    .reduce((acc, diary) => ((!forExport && diary.blocked) ? acc : [...acc, diary.diary_id]), []);

  const isAllSelected = selectedDiaryCount && selectedDiaryCount === selectableDiariesIds.length;

  const handleRowClick = (diary) => {
    selectDiary(diary.code === _.get(diarySelectedLine, 'code', '')
      ? undefined
      : { code: diary.code, id: diary.diary_id });
  };

  const headers = [{
    keyRow: 'diaryHeaderRow',
    value: diaryKeys.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      const direction = _.get(sort, 'column') === keyLabel ? sort.direction : 'desc';

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `${societyId}diaryHeaderCell${keyLabel}${i}`,
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 1
            }
          },
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllDiary(societyId);
              } else {
                addAllDiary(societyId, selectableDiariesIds);
              }
            },
            id: keyLabel.toString()
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `${societyId}diaryHeaderCell${keyLabel}${i}`,
        cellProp: {
          style: {
            position: 'sticky',
            top: 0,
            backgroundColor: '#fff',
            zIndex: 1
          }
        },
        props: {
          children: I18n.t(`tables.diary.${keyLabel}`),
          direction,
          onClick: () => getDiaries(
            {
              sort: {
                column: `${keyLabel}`,
                direction: sort.direction === 'asc' ? 'desc' : 'asc'
              }
            }
          )
        }
      };
    })

  }];

  const bodyRow = diaries_list.map((diary, i) => ({
    keyRow: `${societyId}diaryRow${i}`,
    props: {
      hover: true,
      onClick: () => handleRowClick(diary)
    },
    value: [
      {
        _type: 'checkbox',
        keyCell: `${societyId}diaryCheckBox${i}`,
        props: {
          checked: selectedDiary[diary.diary_id],
          disabled: !forExport && diary.blocked,
          onClick: (e) => {
            e.stopPropagation();
          },
          onChange: () => {
            if (selectedDiary[diary.diary_id]) {
              if (diary.diary_id === _.get(diarySelectedLine, 'id', '')) selectDiary(undefined);
              removeDiary(societyId, diary.diary_id);
            } else {
              selectDiary({ code: diary.code, id: diary.diary_id });
              addDiary(societyId, diary.diary_id);
            }
          },
          id: i.toString()
        },
        cellProp: {
          style: {
            width: '44px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}diaryCode${i}`,
        props: {
          label: _.get(diary, 'code', '')
        },
        cellProp: {
          style: {
            width: '106px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}diaryLabel${i}`,
        props: {
          label: _.get(diary, 'name', '')
        },
        cellProp: {
          style: {
            width: '190px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}diaryType${i}`,
        props: {
          label: _.get(diary, 'diary_type_name', '')
        },
        cellProp: {
          style: {
            width: '112px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}diaryAccountNumber${i}`,
        props: {
          label: _.get(diary, 'account.number', '')
        },
        cellProp: {
          style: {
            width: '104px'
          }
        }
      }
    ]
  }));

  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData: societyId !== -2 ? getDiaries : () => {},
    setTableData: async (formattedDataToExport) => {
      await setDairyTableData(
        tableName, societyId, formattedDataToExport
      );
      if (forExport) addAllDiary(societyId, selectableDiariesIds);
    },
    isLoading,
    isError,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(DairyTable);
