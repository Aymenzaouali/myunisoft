import React from 'react';
import { connect } from 'react-redux';
import {
  getFormValues, change, arrayPush, arrayRemove, Field
} from 'redux-form';
import TableFactory from 'components/groups/Tables/Factory';
import {
  UserProfilAutoComplete,
  UserTypeAutoComplete
} from 'containers/reduxForm/Inputs';
import _ from 'lodash';
import { tabFormName } from 'helpers/tabs';
import I18n from 'assets/I18n';

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);

  return {
    formName,
    newUserValues: getFormValues(formName)(state)
  };
};

const mapDispatchToProps = dispatch => ({
  _changeFormValue: (form, field, value) => dispatch(change(form, field, value)),
  _updateAccessList: (form, field, value) => dispatch(change(form, field, value)),
  _removeSocietyFromList: (form, acces_id, index) => {
    dispatch(arrayPush(form, 'delete_list', { acces_id }));
    dispatch(arrayRemove(form, 'access_list', index));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { formName, newUserValues } = stateProps;
  const { _updateAccessList, _removeSocietyFromList, _changeFormValue } = dispatchProps;
  const { fields, classes } = ownProps;

  const access_list = _.get(newUserValues, 'access_list', []);
  const society_access_list = _.get(newUserValues, 'society_access_list', []);

  const updateAccessList = (field, value) => _updateAccessList(formName, field, value);

  const removeSocietyFromList = (acces_id) => {
    const index = access_list.findIndex(society => society.acces_id === acces_id);

    _removeSocietyFromList(formName, acces_id, index);
  };

  const changeFormValue = (field, value) => _changeFormValue(formName, field, value);

  const onUpdate = (society, value) => {
    const profil_id = _.get(value, 'id', -1);
    const id_type_profil = _.get(newUserValues, `accessTypeSociety-${society.acces_id}.id`, -1);
    const new_access_list = access_list.map(soc => (soc.society_id === society.society_id
      ? {
        ...soc, id_type_profil, profil_id
      }
      : soc));
    const new_sty_access_list = society_access_list.map(s => (s.society_id === society.society_id
      ? { ...s, id_type_profil, profil_id }
      : s));
    updateAccessList('access_list', new_access_list);
    updateAccessList('society_access_list', new_sty_access_list);
  };

  const societiesAccess = fields.getAll();

  const body = societiesAccess ? societiesAccess.map((society, i) => {
    const accessTypeName = `accessTypeSociety-${society.acces_id}`;
    const accessProfilName = `accessProfilSociety-${society.acces_id}`;
    const { acces_id } = society;

    return {
      keyRow: `societyAccessRow${society.society_id}`,
      value: [
        {
          _type: 'string',
          keyCell: 'society',
          props: {
            label: society.label,
            labelClassName: classes.bodyLabel
          }
        },
        {
          _type: 'string',
          keyCell: `accessTypeSociety-${society}`,
          props: {
            label: (
              <Field
                name={accessTypeName}
                component={UserTypeAutoComplete}
                placeholder=""
                onChangeValues={() => changeFormValue(accessProfilName, '')}
              />
            )
          }
        },
        {
          _type: 'string',
          keyCell: `accessProfilSociety-${society}`,
          props: {
            label: (
              <Field
                name={accessProfilName}
                component={UserProfilAutoComplete}
                placeholder=""
                id_type={_.get(newUserValues, `${accessTypeName}.id`, '')}
                onChangeValues={(value) => {
                  onUpdate(society, value);
                }
                }
              />
            )
          }
        },
        {
          _type: 'string',
          keyCell: 'society_removal',
          props: {
            iconRight: 'icon-trash',
            onClick: () => {
              removeSocietyFromList(acces_id);
              fields.remove(i);
            }
          }
        }
      ]
    };
  }) : [];

  const param = {
    header: {
      row: [
        {
          keyRow: 1,
          value: [
            {
              _type: 'string',
              keyCell: 'society',
              props: {
                label: I18n.t('newUserForm.rightManagement.societyList.society'),
                typoVariant: 'h4'
              }
            },
            {
              _type: 'string',
              keyCell: 'type',
              props: {
                label: I18n.t('newUserForm.rightManagement.societyList.type'),
                typoVariant: 'h4'
              }
            },
            {
              _type: 'string',
              keyCell: 'profil',
              props: {
                label: I18n.t('newUserForm.rightManagement.societyList.profil'),
                typoVariant: 'h4'
              }
            },
            {
              _type: 'string',
              keyCell: 'society_removal',
              cellProp: {
                style: { width: '20px' }
              }
            }
          ]
        }
      ]
    },
    body: {
      props: {},
      row: body
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    param
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TableFactory);
