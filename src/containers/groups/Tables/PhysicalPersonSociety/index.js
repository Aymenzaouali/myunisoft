import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import PhysicalPerson from 'components/groups/Tables/PhysicalPerson';
import { physicalPersonSocietyRefKeys } from 'assets/constants/keys';
import {
  FunctionReduxSelect,
  SignatoryFunctionReduxSelect
} from 'containers/reduxForm/Inputs';
import { SOCIETY_PP_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import classnames from 'classnames';
import {
  TextFieldCell,
  CheckBoxCell,
  DatePickerCell,
  SortCell
} from 'components/basics/Cells';
import moment from 'moment';
import { getAutoCompleteProps } from 'redux/tables/utils';
import _ from 'lodash';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, SOCIETY_PP_TABLE_NAME);
  return {
    society_pp_list: _.get(state, `tables.${tableName}.data`, []),
    isEditing: _.get(state, `tables.${tableName}.isEditing`, false),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`),
    initialData: _.get(state, `tables.${tableName}.initialData`),
    society_id,
    errors: _.get(state, `tables.${tableName}.errors`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  editRow: (tableName, item) => dispatch(tableActions.editRow(tableName, item)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors)),
  setPhysicalPersonSocietyTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    society_pp_list,
    isEditing,
    selectedRows,
    selectedRowsCount,
    lastSelectedRow,
    errors,
    initialData
  } = stateProps;

  const {
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows,
    editRow,
    setErrors,
    setPhysicalPersonSocietyTableData
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const tableName = getTableName(society_id, SOCIETY_PP_TABLE_NAME);

  const isAllSelected = society_pp_list.length === selectedRowsCount;

  const headerKeys = physicalPersonSocietyRefKeys;

  const headers = [{
    keyRow: `${society_id}SocietyPPTableHeaderRow0`,
    value: headerKeys.map((data, i) => {
      const { type, keyLabel, isAmount } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: `${society_id}SocietyPPTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = society_pp_list.map(society => society.id);
                selectAllRows(tableName, ids);
              }
            }
          }
        });
      } else {
        cell = {
          component: SortCell,
          keyCell: `${society_id}SocietyPPTableHeaderCell.${keyLabel}`,
          cellProp: {
            isAmount
          },
          props: {
            children: I18n.t(`physicalPersonCreation.society_form.societyPPArrayTitle.${keyLabel}`),
            onClick: () => {}
          }
        };
      }
      return cell;
    })
  }];
  const body = society_pp_list.map((society) => {
    const { id } = society;
    const generateKey = (cellName = '') => `SocietyPP${cellName}${id}`;
    const isChecked = selectedRows[id] === true;
    const isEditingRow = isChecked && isEditing;
    const startDate = _.get(society, 'start_date', '');
    const endDate = _.get(society, 'end_date', '');
    const formattedStartDate = moment(startDate).isValid() ? moment(startDate).format('DD/MM/YYYY') : '';
    const formattedEndDate = moment(endDate).isValid() ? moment(endDate).format('DD/MM/YYYY') : '';
    const isLastSelected = lastSelectedRow === id;
    const associateErrors = _.get(errors, society.id, {});
    const ini = initialData.filter(e => e.id === society.id);
    const dateEndPresente = _.get(ini, 'end_date', '');
    const handleRowSelect = () => {
      if (isChecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    return {
      keyRow: generateKey(),
      props: {
        hover: true,
        selected: isChecked
      },
      value: [
        {
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: generateKey('CheckBox'),
          props: {
            checked: isChecked,
            onChange: handleRowSelect
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Name'),
          props: {
            children: _.get(society, 'society.name') || _.get(society, 'name', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Adress'),
          props: {
            children: _.get(society, 'society.address') || _.get(society, 'address', '')
          },
          cellProp: {
            className: cellStyles.multilineCell
          }
        },
        {
          component: isEditingRow ? FunctionReduxSelect : 'div',
          error: _.get(associateErrors, 'function'),
          keyCell: generateKey('Function'),
          props: isEditingRow
            ? {
              autoFocus: isLastSelected,
              ...getAutoCompleteProps(editRow, society, 'function.label', 'function', tableName),
              onChange: (value) => {
                editRow(tableName, { ...society, function: value });
                setErrors(tableName, society.id, { function: undefined });
              }
            }
            : {
              children: _.get(society, 'function.label')
            }
        },
        {
          component: isEditingRow ? SignatoryFunctionReduxSelect : 'div',
          keyCell: generateKey('SignatoryFunction'),
          error: _.get(associateErrors, 'signatory_function'),
          props: isEditingRow
            ? {
              ...getAutoCompleteProps(editRow, society, 'signatory_function.label', 'signatory_function', tableName),
              onChange: (value) => {
                editRow(tableName, { ...society, signatory_function: value });
                setErrors(tableName, society.id, { signatory_function: undefined });
              }
            }
            : {
              children: _.get(society, 'signatory_function.label', '')
            }
        },
        {
          component: isEditingRow ? DatePickerCell : 'div',
          keyCell: generateKey('StartDate'),
          error: _.get(associateErrors, 'effective_date'),
          props: isEditingRow ? {
            value: startDate,
            onChange: (value) => {
              editRow(tableName, {
                ...society,
                start_date: moment(value).format('YYYY-MM-DD')
              });
              setErrors(tableName, society.id, { effective_date: undefined });
            }
          } : {
            children: formattedStartDate
          }
        },
        {
          component: isEditingRow && !dateEndPresente ? DatePickerCell : 'div',
          keyCell: generateKey('DateEnd'),
          props: isEditingRow ? {
            value: endDate,
            onChange: value => editRow(tableName, {
              ...society,
              end_date: moment(value).format('YYYY-MM-DD')
            })
          } : {
            children: formattedEndDate
          }
        },
        {
          component: isEditingRow ? TextFieldCell : 'div',
          error: _.get(associateErrors, 'social_part_PP'),
          keyCell: generateKey('NbPP'),
          props: isEditingRow
            ? {
              defaultValue: _.get(society, 'social_part.PP'),
              placeholder: _.get(society, 'social_part.PP'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...society,
                  social_part: {
                    ...society.social_part,
                    PP: e.target.value
                  }
                });
                setErrors(tableName, society.id, {
                  social_part_PP: undefined
                });
              }
            }
            : {
              children: _.get(society, 'social_part.PP', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: isEditingRow ? TextFieldCell : 'div',
          error: _.get(associateErrors, 'social_part_US'),
          keyCell: generateKey('NbUS'),
          props: isEditingRow
            ? {
              defaultValue: _.get(society, 'social_part.US'),
              placeholder: _.get(society, 'social_part.US'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...society,
                  social_part: {
                    ...society.social_part,
                    US: e.target.value
                  }
                });
                setErrors(tableName, society.id, {
                  social_part_US: undefined
                });
              }
            } : {
              children: _.get(society, 'social_part.US', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: isEditingRow ? TextFieldCell : 'div',
          error: _.get(associateErrors, 'social_part_NP'),
          keyCell: generateKey('NbNP'),
          props: isEditingRow
            ? {
              defaultValue: _.get(society, 'social_part.NP'),
              placeholder: _.get(society, 'social_part.NP'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...society,
                  social_part: {
                    ...society.social_part,
                    NP: e.target.value
                  }
                });
                setErrors(tableName, society.id, {
                  social_part_NP: undefined
                });
              }
            } : {
              children: _.get(society, 'social_part.NP', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Percent'),
          props: {
            children: _.get(society, 'social_part.percent')
          },
          cellProp: {
            className: classnames(cellStyles.numberCell, cellStyles.smallCell)
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Capital'),
          props: {
            children: _.get(society, 'society.capital') || _.get(society, 'capital', '')
          },
          cellProp: {
            className: classnames(cellStyles.numberCell, cellStyles.smallCell)
          }
        }
      ]
    };
  });
  const exportTableName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${SOCIETY_PP_TABLE_NAME}`;

  const formattedParams = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    setTableData: () => setPhysicalPersonSocietyTableData(
      exportTableName, society_id, formattedParams
    ),
    param: formattedParams
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(PhysicalPerson);
