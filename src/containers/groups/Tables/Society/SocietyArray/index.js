import { connect } from 'react-redux';
import SocietyArray from 'components/groups/Tables/Society/SocietyArray';
import { arrayRemove, getFormValues } from 'redux-form';
import { delSociety as delSocietyThunk } from 'redux/tabs/physicalPersonCreation';
// import { societyAssociateValidation } from 'helpers/validation';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    persPhysiqueInfo: getFormValues(tabFormName('physicalPersonCreationForm', societyId))(state),
    employmentFunctionTypes: _.get(getCurrentTabState(state), 'physicalPersonCreation.employmentFunctionTypes', []).map(e => ({
      value: e.id,
      label: e.name
    })),
    signatoryFunctionTypes: _.get(getCurrentTabState(state), 'physicalPersonCreation.signatoryFunctionTypes', []).map(e => ({
      value: e.id,
      label: e.name
    }))
  };
};
const mapDispatchToProps = dispatch => ({
  removeSociety: (societyId, index, linkId) => {
    dispatch(delSocietyThunk(linkId));
    dispatch(arrayRemove(tabFormName('physicalPersonCreationForm', societyId), 'firm', index));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { persPhysiqueInfo = {} } = stateProps;
  const { firm = [] } = persPhysiqueInfo;

  const society = firm.map(e => ({
    ...e,
    name: e.name || _.get(e, 'society.name')
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    society
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(SocietyArray);
