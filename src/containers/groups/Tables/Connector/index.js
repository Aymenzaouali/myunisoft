import { connect } from 'react-redux';
import Connector from 'components/groups/Tables/Connector';
import {
  addConnector, removeConnector, resetAllConnector, addAllConnector
} from 'redux/connector/actions';
import { connectorHeaderKeys } from 'assets/constants/keys';
import I18n from 'assets/I18n';
import _ from 'lodash';
import tableActions from 'redux/tables/actions';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = state => ({
  society_id: state.navigation.id,
  listConnector: _.get(state, `connector.list_selectedConnector[${state.navigation.id}]`, []),
  allConnectorList: _.get(state, 'connector.connector'),
  isLoading: _.get(state, 'connector.isLoading')
});

const mapDispatchToProps = dispatch => ({
  addConnector: (connector, society_id) => dispatch(
    addConnector(connector, society_id)
  ),
  removeConnector: (connector, society_id) => dispatch(
    removeConnector(connector, society_id)
  ),
  resetAllConnector: society_id => dispatch(resetAllConnector(society_id)),
  addAllConnector: society_id => dispatch(addAllConnector(society_id)),
  setConnectorTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id, listConnector, allConnectorList, isLoading
  } = stateProps;
  const {
    addConnector, resetAllConnector, removeConnector, addAllConnector, setConnectorTableData
  } = dispatchProps;

  const headerKeys = connectorHeaderKeys;
  const allConnector = allConnectorList && allConnectorList.map(e => ({
    ...e,
    society_id: e.id_society,
    connector_id: e.id_connector
  }));
  const connectorData = (society_id === -2)
    ? allConnector
    : _.filter(allConnector, { society_id });

  const isAllSelected = connectorData.length === listConnector.length;

  const headers = [{
    keyRow: `${society_id}connectorTableHeaderRow`,
    value: headerKeys.map((data, i) => {
      const { type, keyLabel } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          _type: type,
          keyCell: `${society_id}connectorTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllConnector(society_id);
              } else {
                addAllConnector(society_id);
              }
            }

          },
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 1
            }
          }
        });
      } else {
        cell = {
          _type: 'sortCell',
          keyCell: `${society_id}connectorTableHeaderCell.${keyLabel}${i}`,
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 1
            }
          },
          props: {
            children: I18n.t(`connector.connectorArrayTitle.${keyLabel}`),
            onClick: () => { }
          }
        };
      }
      return cell;
    })
  }];

  const body = connectorData.map((connector, i) => ({
    keyRow: `connector${i}`,
    props: {
      hover: true,
      onClick: () => {
        if (listConnector.some(item => item.id_connector === connector.id_connector)) {
          removeConnector(connector, society_id);
        } else {
          addConnector(connector, society_id);
        }
      }
    },
    value: [
      {
        _type: 'checkbox',
        keyCell: `ribCheckbox${i}`,
        props: {
          checked: listConnector.some(item => item.id_connector === connector.id_connector),
          onClick: (e) => {
            e.stopPropagation();
          },
          onChange: () => {
            if (listConnector.some(item => item.id_connector === connector.id_connector)) {
              removeConnector(connector, society_id);
            } else {
              addConnector(connector, society_id);
            }
          },
          id: connector.id_connector
        }
      },
      {
        _type: 'string',
        keyCell: `connectorSocietyName${i}`,
        props: {
          label: connector.nom_ste
        }
      },
      {
        _type: 'string',
        keyCell: `connectorSoftware${i}`,
        props: {
          label: connector.label
        }
      },
      {
        _type: 'string',
        keyCell: `connectorCode${i}`,
        props: {
          label: connector.code
        }
      },
      {
        _type: 'string',
        keyCell: `connectorComplementaryInformation${i}`,
        props: {
          label: connector.complementary_information
        },
        cellProp: {
          className: cellStyles.multilineCell
        }
      }
    ]
  }));

  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isLoading,
    setTableData: param => setConnectorTableData(tableName, society_id, param),
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: body
      }
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Connector);
