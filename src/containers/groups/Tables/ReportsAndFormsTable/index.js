import React from 'react';
import { connect } from 'react-redux';
import { ReportsAndFormsTable } from 'components/groups/Tables/';
import ReportsAndFormsService from 'redux/reportsAndForms';
import { reportsAndForms } from 'assets/constants/keys';
import {
  CheckBoxCell
} from 'components/basics/Cells';
import { REPORT_AND_FORMS_TABLE, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import _ from 'lodash';
import I18n from 'assets/I18n';
import moment from 'moment';
import { StatusString } from 'components/basics/Strings';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, REPORT_AND_FORMS_TABLE);
  return {
    associate_list: _.get(state, `tables.${tableName}.data`, []),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    errors: _.get(state, `tables.${tableName}.errors`, {}),
    initialData: _.get(state, `tables.${tableName}.initialData`),
    society_id: state.navigation.id
  };
};

const mapDispatchToProps = dispatch => ({
  getFormsAndReports: () => dispatch(
    ReportsAndFormsService.getFormsAndReports()
  ),
  getFormsAndReportsAdditional: id => dispatch(
    ReportsAndFormsService.getFormsAndReportsAdditional(id)
  ),
  editRow: (tableName, item) => dispatch(tableActions.editRow(tableName, item)),
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors))
});

const renderSelectValue = value => (
  <StatusString status={value} label={value} />
);

const renderLabel = (count, labelText) => (
  <div>
    <span style={{ color: '#0bd1d1' }}>{count}</span>
    <span>{labelText}</span>
  </div>
);

const renderSelectList = option => (
  <StatusString status={option.value} label={option.label} />
);

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    selectedRowsCount,
    associate_list,
    selectedRows
  } = stateProps;

  const tableName = getTableName(society_id, REPORT_AND_FORMS_TABLE);

  const {
    getFormsAndReports,
    getFormsAndReportsAdditional,
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const isAllSelected = selectedRowsCount === associate_list.length;

  const headers = [{
    keyRow: 'reportsAndFormsHeaderRow',
    value: reportsAndForms.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `reportsAndFormsHeaderCell${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = associate_list.map(al => al.id);
                selectAllRows(tableName, ids);
              }
            },
            id: keyLabel.toString()
          },
          cellProp: {
            style: {
              maxWidth: '44px'
            }
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `reportsAndFormsHeaderCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`reportsAndForms.table.${keyLabel}`)
        }
      };
    })

  }];

  const bodyRow = associate_list.map((statement) => {
    const { id } = statement;
    const ischecked = selectedRows[id] === true;
    const handleRowSelect = () => {
      if (ischecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    const generateKey = (cellName = '') => `${REPORT_AND_FORMS_TABLE}${cellName}${statement.id}`;
    return {
      keyRow: generateKey(),
      props: {
        onDoubleClick: () => getFormsAndReportsAdditional(statement.id),
        hover: true,
        selected: ischecked
      },
      value: [
        {
          component: CheckBoxCell,
          keyCell: generateKey('CheckBox'),
          props: {
            checked: ischecked,
            onChange: handleRowSelect
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('type'),
          props: {
            children: _.get(statement, 'type', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('name'),
          props: {
            children: _.get(statement, 'name', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('decription'),
          props: {
            children: _.get(statement, 'decription', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('dateStartdateEnd'),
          props: {
            children: `${moment(statement.dateStart).format('L')} au ${moment(statement.dateEnd).format('L')}`
          }
        },
        {
          _type: statement.enterprise_list ? 'select' : 'string',
          keyCell: generateKey('enterprise_list'),
          props: statement.enterprise_list ? {
            list: statement.enterprise_list
              ? statement.enterprise_list
                .map(user => ({ label: user.name, value: user.name })) : [],
            value: renderLabel(`${statement.enterprise_list.length} `, statement.enterpriseName),
            renderValue: value => renderSelectValue(value),
            onChange: () => {

            },
            renderList: renderSelectList
          } : {
            label: <div style={{ paddingLeft: 6 }}>{statement.enterpriseName}</div>
          },
          cellProp: {
            style: {
              width: '170px'
            }
          }
        }
      ]
    };
  });

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData: getFormsAndReports,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ReportsAndFormsTable);
