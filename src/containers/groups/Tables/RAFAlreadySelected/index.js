import { connect } from 'react-redux';
import { RAFAlreadySelected } from 'components/groups/Tables/';
import { RAFAlreadySelectedKeys } from 'assets/constants/keys';
import _ from 'lodash';
import I18n from 'assets/I18n';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  selectedStateOrForm: state.reportsAndForms.selectedStateOrForm
});

const mergeProps = (stateProps) => {
  const {
    societyId,
    selectedStateOrForm
  } = stateProps;

  const currentStatements = selectedStateOrForm[societyId] || {};

  const RAFAlreadySelectedData = _.get(currentStatements, 'selectedStateOrForm_list', {});

  const selectedData = [];
  Object.values(RAFAlreadySelectedData).forEach((value) => {
    value.data.forEach(sel => selectedData.push({ ...value.name, ...sel }));
  });
  const generateKey = (cellName = '') => `alreadySelectedTable${cellName}`;

  const headers = [{
    keyRow: generateKey('header'),
    value: RAFAlreadySelectedKeys.map((data, i) => {
      const {
        keyLabel
      } = data;
      return {
        component: 'div',
        keyCell: generateKey(i),
        cellProp: {},
        props: {
          children: I18n.t(`reportsAndForms.alreadySelectedTable.${keyLabel}`)
        }
      };
    })

  }];

  const bodyRow = selectedData.map((statement, i) => ({
    keyRow: generateKey(`Row${i}`),
    props: {
      hover: true
    },
    value: [
      {
        _type: 'string',
        keyCell: generateKey('label'),
        props: {
          label: _.get(statement, 'label', '')
        }
      },
      {
        _type: 'string',
        keyCell: generateKey('orderNumber'),
        props: {
          label: `${_.get(statement, 'wording', '')} and ${_.get(statement, 'orderNumber', '')}`
        }
      }
    ]
  }));

  return {
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(RAFAlreadySelected);
