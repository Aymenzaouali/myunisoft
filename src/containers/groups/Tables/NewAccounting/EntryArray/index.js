import { compose } from 'redux';
import { connect } from 'react-redux';
import {
  change, arrayPush, getFormValues, reset, formValueSelector
} from 'redux-form';
import { withSnackbar } from 'notistack';
import EntryArrayComponent from 'components/groups/Tables/NewAccounting/EntryArray';
import I18n from 'assets/I18n';
import { splitWrap } from 'context/SplitContext';
import {
  getDiaries as getDiariesThunk,
  getAccount as getAccountThunk,
  getPaymentType as getPaymentTypeThunk,
  counterPartLines as counterPartLinesThunk,
  getEntryComments as getEntryCommentsThunk
} from 'redux/accounting';
import {
  getAccountDetail as getAccountDetailThunk
} from 'redux/chartAccounts';

import { enqueueSnackbar as enqueueSnackbarActionFoo } from 'common/redux/notifier/action';
import { Notifications } from 'common/helpers/SnackBarNotifications';

import { autoFillFormValues } from 'helpers/autoFillFormValues';
import actions from 'redux/accounting/actions';
import chartAccountActions from 'redux/chartAccounts/actions';
import _ from 'lodash';
import { openDialog } from 'redux/dialogs';
import { AccountingCommentBox } from 'containers/groups/Comments';
import { getDiaryDetail } from 'redux/diary';

const mapStateToProps = (state, ownProps) => {
  const { formName } = ownProps;
  const societyId = state.navigation.id;

  return ({
    diaries: state.accountingWeb.diaries,
    account: state.accountingWeb.account,
    openConfirmFromHeader: state.accountingWeb.openConfirmFromHeader,
    openPermissionFromHeader: state.accountingWeb.openPermissionFromHeader,
    paymentType: state.accountingWeb.paymentType,
    formVal: getFormValues(formName)(state),
    comment: _.get(state, 'accountingWeb.comment'),
    societyId,
    accountData: _.get(state, 'chartAccountsWeb.accountDetail.account_array', []),
    entriesType: _.get(state, `accountingWeb.entries.${societyId}.type`, ''),
    currentEntry: _.get(state, `accountingWeb.currentEntry.${societyId}`),
    diaryValue: formValueSelector(formName)(state, 'diary'),
    entryValue: formValueSelector(formName)(state, 'entry_list')
  });
};

const mapDispatchToProps = dispatch => ({
  openCommentsDialog: dialogProps => dispatch(openDialog(dialogProps)),
  getEntryComments: id => dispatch(getEntryCommentsThunk(id)),

  changeForm: (form, field, value) => dispatch(change(form, field, value)),
  pushInArray: (form, field, value) => dispatch(arrayPush(form, field, value)),
  getDiaries: (society_id, q) => dispatch(getDiariesThunk(society_id, q)),
  getDiaryDetail: diaryId => dispatch(getDiaryDetail(diaryId)),
  getAccount: (society_id, q) => dispatch(getAccountThunk(society_id, q)),
  getAccountDetail: () => dispatch(getAccountDetailThunk()),
  getPaymentType: (society_id, q) => dispatch(getPaymentTypeThunk(society_id, q)),
  enableCreateCounterPartConfirmation: (enable) => {
    dispatch(actions.enableCreateCounterPartConfirmation(enable));
  },
  enableLinkDiaryAccountPermission: (enable) => {
    dispatch(actions.enableLinkDiaryAccountPermission(enable));
  },
  counterPartLines: payload => dispatch(counterPartLinesThunk(payload)),
  initializeAccount: data => dispatch(autoFillFormValues('newAccount', data)),
  selectAccount: accountSelected => dispatch(chartAccountActions.selectAccount(accountSelected)),
  resetAccount: () => dispatch(reset('newAccount')),
  resetAccountSelected: () => dispatch(chartAccountActions.selectAccount('')),

  enableEditFlag: (isOpen, flagType) => dispatch(actions.enableEditFlag(isOpen, flagType)),

  setLinePosition: i => dispatch(actions.setCurrentEntryLine(i)),

  enqueueSnackbarWarning: notification => dispatch(
    enqueueSnackbarActionFoo(Notifications(notification, 'warning'))
  ),

  initializeImmoForm: data => dispatch(autoFillFormValues('flagDialogForm', data))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    pushInArray,
    changeForm,
    openCommentsDialog,
    getEntryComments
  } = dispatchProps;
  const { formName } = ownProps;
  const {
    formVal
  } = stateProps;

  const changeFormValue = (field, value) => {
    changeForm(formName, field, value);
  };

  const onLabelBlur = (e, i) => {
    const label = !e.target.value ? _.get(formVal.entry_list[i], 'account.label', '') : e.target.value;
    return changeForm(formName, `entry_list[${i}].label`, label);
  };

  const pushEntry = (field, value) => {
    pushInArray(formName, field, value);
  };

  const getAndOpenComments = async (entryId) => {
    if (entryId !== undefined) {
      openCommentsDialog({
        body: {
          Component: AccountingCommentBox,
          props: {
            entryId,
            createMode: false,
            getComments: () => getEntryComments(entryId)
          }
        }
      });
    } else {
      openCommentsDialog({
        body: {
          Component: AccountingCommentBox,
          props: {
            entryId,
            createMode: true
          }
        }
      });
    }
  };

  const account = _.get(formVal, 'entry_list[0].account.account_number');

  const confirmation = {
    message: I18n.t('newAccounting.noCounterPartDialog.message', { account }),
    title: I18n.t('newAccounting.noCounterPartDialog.title'),
    buttonsLabel: {
      validate: I18n.t('newAccounting.noCounterPartDialog.accept'),
      cancel: I18n.t('newAccounting.noCounterPartDialog.deny')
    }
  };

  const permission = {
    message: I18n.t('newAccounting.linkDiaryAccountDialog.message', { account }),
    title: I18n.t('newAccounting.linkDiaryAccountDialog.title'),
    buttonsLabel: {
      validate: I18n.t('newAccounting.linkDiaryAccountDialog.accept'),
      cancel: I18n.t('newAccounting.linkDiaryAccountDialog.deny')
    }
  };

  const information = {
    title: I18n.t('newAccounting.informationDialog.title'),
    message: I18n.t('newAccounting.informationDialog.message'),
    buttonsLabel: {
      cancel: I18n.t('common.close')
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    changeFormValue,
    pushEntry,
    confirmation,
    permission,
    information,
    getAndOpenComments,
    onLabelBlur
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  splitWrap
);

export default withSnackbar(enhance(EntryArrayComponent));
