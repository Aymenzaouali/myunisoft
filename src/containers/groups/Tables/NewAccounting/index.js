import { connect } from 'react-redux';
import {
  change, arrayPush, getFormValues, arrayRemove,
  reset
} from 'redux-form';
import _ from 'lodash';

import { yearsList, monthsList } from 'helpers/date';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { getCurrentTabState } from 'helpers/tabs';

import { getDiariesType as getDiariesTypeThunk } from 'redux/diary';
import {
  counterPartLines as counterPartLinesThunk,
  getDiaries as getDiariesThunk
} from 'redux/accounting';
import { removeManualDoc } from 'redux/tabs/accounting/actions';

import NewAccountingTable from 'components/groups/Tables/NewAccounting';

// Component
const mapStateToProps = (state, ownProps) => {
  const { accounting } = getCurrentTabState(state);
  const { formName } = ownProps;

  return ({
    newEntries: state.accountingWeb.newEntries,
    societyId: state.navigation.id,
    formVal: getFormValues(formName)(state),
    filters: getFormValues(`${state.navigation.id}accountingFilter`)(state),
    manualDocs: accounting.manual_docs,
    formName
  });
};

const mapDispatchToProps = dispatch => ({
  changeForm: (form, field, value) => dispatch(change(form, field, value)),
  pushInArray: (form, field, value) => dispatch(arrayPush(form, field, value)),
  deleteInArray: (form, field, value) => dispatch(arrayRemove(form, field, value)),
  resetForm: form => dispatch(reset(form)),
  counterPartLines: payload => dispatch(counterPartLinesThunk(payload)),
  removeManualDoc: doc => dispatch(removeManualDoc(doc)),
  getDiaries: societyId => dispatch(getDiariesThunk(societyId, undefined, null)),
  getDiariesType: () => dispatch(getDiariesTypeThunk()),
  setDiaryDialog: diary => dispatch(autoFillFormValues('diaryForm', ({
    diary_code: _.get(diary, 'code'),
    diary_label: _.get(diary, 'label'),
    diary_type: {
      id: diary.diary_type_id || diary.diary_type.id,
      label: diary.diary_type_name || diary.diary_type.label,
      value: diary.diary_type_code || diary.diary_type.value
    },
    diary_accountNumber: {
      account_id: _.get(diary, 'account.id'),
      account_number: _.get(diary, 'account.number'),
      label: _.get(diary, 'account.label'),
      value: _.get(diary, 'account.number')
    }
  }))),
  changeNewAccountingForm: (societyId, newDiary) => dispatch(change(`${societyId}newAccounting`, 'diary', newDiary))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId, manualDocs } = stateProps;
  const { formName } = ownProps;
  const {
    changeForm, pushInArray, deleteInArray, resetForm,
    changeNewAccountingForm: _changeNewAccountingForm
  } = dispatchProps;

  const changeNewAccountingForm = newDiary => _changeNewAccountingForm(societyId, newDiary);

  const changeFormValue = (field, value) => {
    changeForm(formName, field, value);
  };

  const filterFormName = `${societyId}accountingFilter`;
  const changeFilterForm = (field, value) => {
    changeForm(filterFormName, field, value);
  };

  const pushEntry = (field, value) => {
    pushInArray(formName, field, value);
  };

  const deleteEntry = (field, value) => {
    deleteInArray(formName, field, value);
  };

  const manualDoc = _.get(manualDocs, societyId, {});

  const resetNewAccounting = () => resetForm(formName);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    changeFormValue,
    pushEntry,
    deleteEntry,
    manualDoc,
    changeFilterForm,
    resetNewAccounting,
    monthsList,
    yearsList,
    changeNewAccountingForm
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(NewAccountingTable);
