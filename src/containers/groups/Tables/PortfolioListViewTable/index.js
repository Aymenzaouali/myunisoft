import { connect } from 'react-redux';
import { PortfolioListViewTable } from 'components/groups/Tables/';
import PortfolioListSettingsService from 'redux/portfolioListSettings';
import { portfolioListViewTableKeys } from 'assets/constants/keys';
import actions from 'redux/portfolioListSettings/actions';
import _ from 'lodash';
import I18n from 'assets/I18n';
import React from 'react';
import tableActions from 'redux/tables/actions';
import { StatusString } from 'components/basics/Strings';


const mapStateToProps = state => ({
  wallets: state.portfolioListSettings.wallets,
  societyId: state.navigation.id,
  walletSelectedLine: state.portfolioListSettings.walletSelectedLine,
  selectedWallets: _.get(state, 'portfolioListSettings.selectedWallets', [])
});

const mapDispatchToProps = dispatch => ({
  getPortfolioList: () => dispatch(
    PortfolioListSettingsService.getWalletList()
  ),
  resetAllWallets: () => dispatch(actions.resetAllWallets()),
  addAllWallets: () => dispatch(actions.addAllWallets()),
  selectWallet: walletSelectedLine => dispatch(actions.selectWallet(walletSelectedLine)),
  addWallet: walletId => dispatch(actions.addWallet(walletId)),
  removeWallet: walletId => dispatch(actions.removeWallet(walletId)),
  setPortfolioTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    wallets,
    selectedWallets,
    societyId,
    walletSelectedLine
  } = stateProps;
  const {
    getPortfolioList,
    resetAllWallets,
    addAllWallets,
    selectWallet,
    addWallet,
    removeWallet,
    setPortfolioTableData
  } = dispatchProps;

  let isAllSelected = true;

  const currentWallets = wallets || {};

  const { isLoading, isError } = currentWallets;
  const wallets_list = _.get(currentWallets, 'wallets_list', []);

  wallets_list.forEach((wallet) => {
    if (!selectedWallets.some(item => item.id === wallet.id)) {
      isAllSelected = false;
    }
  });

  const headers = [{
    keyRow: 'settingsHeaderRow',
    value: portfolioListViewTableKeys.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `settingsHeaderCell${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllWallets();
              } else {
                addAllWallets();
              }
            },
            id: keyLabel.toString()
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `settingsHeaderCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`portfolioListView.table.${keyLabel}`)
        }
      };
    })

  }];

  const renderSelectValue = value => (
    <StatusString status={value} label={value} />
  );

  const renderLabel = (count, labelText, nbSociety) => {
    const style = nbSociety === 0 ? { paddingLeft: '5px' } : {};

    return (
      <div style={style}>
        <span style={{ color: '#0bd1d1' }}>{count}</span>
        <span>{labelText}</span>
      </div>
    );
  };

  const renderSelectList = option => (
    <StatusString status={option.value} label={option.label} />
  );

  const handleRowClick = (wallet) => {
    if (!wallet.main_wallet) {
      selectWallet(wallet.id_wallet === walletSelectedLine
        ? undefined
        : wallet.id_wallet);
    }
  };

  const bodyRow = wallets_list.map((wallet, i) => {
    const nbSociety = _.get(wallet, 'nb_society', '');
    const nbUser = _.get(wallet, 'nb_users', '');
    const labelSociety = nbSociety > 0 ? ` ${I18n.t('portfolioListView.societies')}` : ` ${I18n.t('portfolioListView.society')}`;
    const labelUser = nbUser > 0 ? ` ${I18n.t('portfolioListView.users')}` : ` ${I18n.t('portfolioListView.user')}`;

    return {
      keyRow: `settingsRow${i}`,
      props: {
        hover: true,
        onClick: () => handleRowClick(wallet)
      },
      value: [
        {
          _type: 'checkbox',
          keyCell: `settingsCheckBox${i}`,
          props: {
            checked: selectedWallets.some(item => item.id_wallet === wallet.id_wallet),
            disabled: wallet.blocked || wallet.main_wallet,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (selectedWallets.some(item => item.id_wallet === wallet.id_wallet)) {
                if (wallet.id_wallet === walletSelectedLine) selectWallet(undefined);
                removeWallet(wallet.id_wallet);
              } else {
                selectWallet(wallet.id_wallet);
                addWallet(wallet);
              }
            },
            id: i.toString()
          },
          cellProp: {
            style: {
              width: '44px'
            }
          }
        },
        {
          _type: 'string',
          keyCell: `settingsCode${i}`,
          props: {
            label: _.get(wallet, 'libelle', '')
          },
          cellProp: {
            style: {
              width: '106px'
            }
          }
        },
        {
          _type: wallet.list_society ? 'select' : 'string',
          keyCell: `settingsLabel${i}`,
          props: wallet.list_society ? {
            value: renderLabel(nbSociety, labelSociety),
            labelToExport: `${nbSociety} ${labelSociety}`,
            list: wallet.list_society.map(
              society => ({ label: society.name, value: society.name })
            ),
            renderValue: value => renderSelectValue(value),
            onChange: () => {

            },
            renderList: renderSelectList
          } : {
            label: renderLabel(nbSociety, labelSociety),
            labelToExport: `${nbSociety} ${labelSociety}`
          },
          cellProp: {
            style: {
              width: '190px'
            }
          }
        },
        {
          _type: wallet.list_users ? 'select' : 'string',
          keyCell: `settingsType${i}`,
          props: wallet.list_users ? {
            list: wallet.list_users
              .map(user => ({ label: user.name, value: user.id_pers_physique })),
            value: renderLabel(nbUser, labelUser),
            labelToExport: `${nbUser} ${labelUser}`,
            renderValue: value => renderSelectValue(value),
            onChange: () => {
            },
            renderList: renderSelectList
          } : {
            label: (
              <div style={{ paddingLeft: 6 }}>
                {renderLabel(nbUser, labelUser)}
              </div>
            ),
            labelToExport: `${nbUser} ${labelUser}`
          },
          cellProp: {
            style: {
              width: '112px'
            }
          }
        }
      ]
    };
  });

  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData: getPortfolioList,
    setTableData: param => setPortfolioTableData(tableName, societyId, param),
    isLoading,
    isError,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(PortfolioListViewTable);
