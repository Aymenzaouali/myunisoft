import { TableFactory } from 'components/groups/Tables';
import { connect } from 'react-redux';
import { getPlanDetails as getPlanDetailsThunk } from 'redux/planDetails';
import actions from 'redux/planDetails/actions';

import _ from 'lodash';
import { accountingPlanDetailHeaderKeys } from 'assets/constants/keys';
import { CheckBoxCell, SortCell } from 'components/basics/Cells';
import tableActions from 'redux/tables/actions';
import I18n from 'assets/I18n';

const mapStateToProps = state => ({
  planId: state.accountingPlans.selectedPlan,
  planDetails: _.get(state, `planDetails.planDetails.${state.accountingPlans.selectedPlan}`, {}),
  selectedPlanDetails: _.get(state, `planDetails.selectedPlanDetails.${state.accountingPlans.selectedPlan}`, []),
  selectedPlanDetail: state.planDetails.selectedPlanDetail,
  societyId: state.navigation.id
});

const mapDispatchToProps = dispatch => ({
  getPlanDetails: options => dispatch(getPlanDetailsThunk(options)),
  selectLine: (id, line) => dispatch(actions.selectPlanDetail(id, line)),
  addAllPlanDetail: planId => dispatch(actions.addAllPlanDetail(planId)),
  resetAllPlanDetail: planId => dispatch(actions.resetAllPlanDetail(planId)),
  addPlanDetail: (id, planId) => dispatch(actions.addPlanDetail(id, planId)),
  removePlanDetail: (id, planId) => dispatch(actions.removePlanDetail(id, planId)),
  setPlansComptableTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps) => {
  const {
    getPlanDetails,
    selectLine,
    addPlanDetail,
    removePlanDetail,
    addAllPlanDetail,
    resetAllPlanDetail,
    setPlansComptableTableData
  } = dispatchProps;

  const {
    planId,
    planDetails,
    selectedPlanDetails,
    selectedPlanDetail,
    societyId
  } = stateProps;

  const planDetailsList = _.get(planDetails, 'list', []);
  const sort = _.get(planDetails, 'sort', []);

  let isAllSelected = planDetailsList.length > 0;
  planDetailsList.forEach((item) => {
    if (!selectedPlanDetails.includes(item.id)) {
      isAllSelected = false;
    }
  });

  const headers = [{
    keyRow: 'planDetailsHeaderRow',
    value: accountingPlanDetailHeaderKeys.map((data, i) => {
      const {
        type,
        keyLabel,
        isAmount
      } = data;
      let cell;
      const direction = _.get(sort, 'column') === keyLabel ? sort.direction : 'desc';

      if (type === 'checkbox') {
        cell = ({
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: `planDetailsHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllPlanDetail(planId);
              } else {
                addAllPlanDetail(planId);
              }
            }
          }
        });
      } else {
        cell = {
          component: SortCell,
          keyCell: `planDetailsCell.${keyLabel}${i}`,
          cellProp: {
            isAmount
          },
          props: {
            children: I18n.t(`accountingPlans.tables.planDetails.column.${keyLabel}`),
            colorErrorIcon: true,
            direction,
            onClick: () => getPlanDetails(
              {
                sort: {
                  column: `${keyLabel}`,
                  direction: sort.direction === 'asc' ? 'desc' : 'asc'
                }
              }
            )
          }
        };
      }
      return cell;
    })
  }];

  const body = planDetailsList.map((item, i) => ({
    keyRow: `planDetailsLink${i}`,
    props: {
      hover: true,
      onClick: () => {
        if (item.id === selectedPlanDetail) {
          selectLine(null, null);
        } else {
          selectLine(item.id, i);
        }
      }
    },
    value: [
      {
        component: CheckBoxCell,
        _type: 'checkbox',
        keyCell: `planDetailBox${i}`,
        props: {
          id: i,
          checked: selectedPlanDetails.includes(item.id),
          onClick: (e) => {
            e.stopPropagation();
          },
          onChange: () => {
            if (selectedPlanDetails.includes(item.id)) {
              if (item.id === selectedPlanDetail) selectLine(null, null);
              removePlanDetail(item.id, planId);
            } else {
              selectLine(item.id, i);
              addPlanDetail(item.id, planId);
            }
          }
        },
        cellProp: {
          style: {
            width: '44px'
          }
        }
      },
      {
        component: 'div',
        keyCell: `planDetailNumber${i}`,
        props: {
          children: _.get(item, 'account_number', '')
        }
      },
      {
        component: 'div',
        keyCell: `planDetailLabel${i}`,
        props: {
          children: _.get(item, 'label', '')
        }
      }
    ]
  }));

  const transformedParams = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  const tableName = I18n.t('accountingPlans.tables.planDetails.title');

  return {
    ...stateProps,
    ...dispatchProps,
    isLoading: planDetails.isLoading,
    isError: planDetails.isError,
    param: transformedParams,
    onRefresh: () => setPlansComptableTableData(tableName, societyId, transformedParams),
    cellHeight: 38,
    maxVisibleItems: 10
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TableFactory);
