import { connect } from 'react-redux';
import { getFormValues, reset, change } from 'redux-form';
import { setDeletionDialog } from 'redux/tabs/crm/actions';
import {
  getPersonnePhysiqueList as getPersonnePhysiqueListThunk
} from 'redux/tabs/physicalPersonList';
import {
  getPersPhysiqueById as getPersPhysiqueByIdThunk,
  getFamilyLink as getFamilyLinkThunk,
  getDocument as getDocumentThunk,
  getSocietyPP as getSocietyPPThunk,
  getSocietyAccess as getSocietyAccessThunk,
  getEmployee as getEmployeeThunk
} from 'redux/tabs/physicalPersonCreation';
import TableFactory from 'components/groups/Tables/Factory';
import stringStyles from 'components/basics/Cells/StringCell/StringCell.module.scss';
import I18n from 'assets/I18n';
import _ from 'lodash';
import { physicalPersonListTranslationKeys } from 'assets/constants/keys';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import { setCurrentForm, closeCurrentForm } from 'redux/tabs/form/actions';
import tableActions from 'redux/tables/actions';
import 'assets/colors/colors.scss';

const mapStateToProps = (state) => {
  const { physicalPersonCreation, physicalPersonList } = getCurrentTabState(state);
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    tableName,
    societyId: state.navigation.id,
    selectedPhysicalPerson: _.get(physicalPersonCreation, 'selected_physical_person'),
    physicalPersonList: physicalPersonList || {},
    rowsPerPage: physicalPersonList.limit,
    physicalPersonListFilter: getFormValues(tabFormName('physicalPersonListFilter', state.navigation.id))(state),
    isUserAdmin: state.login.user.isadmin
  };
};
const mapDispatchToProps = dispatch => ({
  getPersonnePhysiqueList: payload => dispatch(getPersonnePhysiqueListThunk(payload)),

  getPersonnePhysiqueId: id => dispatch(getPersPhysiqueByIdThunk(id)),
  getFamilyLink: personId => dispatch(getFamilyLinkThunk(personId)),
  getDocument: personId => dispatch(getDocumentThunk(personId)),
  getSocietyAccess: id => dispatch(getSocietyAccessThunk(id)),
  get_person_society: personId => dispatch(getSocietyPPThunk(personId)),
  getEmployee: id => dispatch(getEmployeeThunk(id)),

  resetFormValues: form => dispatch(reset(form)),
  setCurrentForm: (name, initial) => dispatch(setCurrentForm(name, initial)),
  closeCurrentForm: discardCb => dispatch(closeCurrentForm(discardCb)),
  setPhysicalTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  ),

  initializeFamilySection: (form, data) => dispatch(change(form, 'family', data)),
  initializeEmployeeSection: (form, data) => dispatch(change(form, 'society', data)),
  initializeDocumentSection: (form, data) => dispatch(change(form, 'documentInfo', data)),
  initializeSocietySection: (form, data) => dispatch(change(form, 'firm', data)),
  initializeUserSection: (form, data) => dispatch(change(form, 'user', data)),

  setDeletionDialog: (mode, id) => dispatch(setDeletionDialog(mode, id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    physicalPersonList,
    selectedPhysicalPerson,
    tableName,
    societyId,
    isUserAdmin
  } = stateProps;
  const {
    getPersonnePhysiqueList,

    getPersonnePhysiqueId,
    getFamilyLink,
    getDocument,
    getSocietyAccess,
    get_person_society,
    getEmployee,

    resetFormValues,
    setCurrentForm,
    closeCurrentForm,
    setPhysicalTableData,

    initializeFamilySection,
    initializeEmployeeSection,
    initializeDocumentSection,
    initializeSocietySection,
    initializeUserSection,

    setDeletionDialog
  } = dispatchProps;

  const formName = tabFormName('physicalPersonCreationForm', societyId);

  const loadData = async () => {
    await getPersonnePhysiqueList();
  };

  const {
    physicalPersonsData,
    isLoading,
    isError
  } = physicalPersonList;

  const currentPhysicalPersons = _.get(physicalPersonsData, 'array_pers_physique', []);

  const headers = [{
    keyRow: 'physicalPersonListHeaderRow',
    value: physicalPersonListTranslationKeys.map((key, i) => {
      const {
        sort: {
          column,
          direction: oldDirection
        }
      } = physicalPersonList;

      const defaultPhysicalPersonDirection = 'asc';
      const selectedPhysicalPersonDirection = oldDirection === 'desc' ? 'asc' : 'desc';
      const direction = column === key.label
        ? selectedPhysicalPersonDirection
        : defaultPhysicalPersonDirection;

      const sort = {
        column: `${key.label}`,
        direction
      };

      if (key.type === 'string') {
        return {
          _type: 'string',
          keyCell: `physicalPersonListHeaderCell.${key.label}${i}`,
          props: {
            labelClassName: stringStyles.inlineFlex,
            cellClassName: stringStyles.inlineFlex
          }
        };
      }

      return {
        _type: 'sortCell',
        keyCell: `physicalPersonListHeaderCell.${key.label}${i}`,
        props: {
          children: I18n.t(`tables.physicalPersonList.${key.label}`),
          direction,
          onClick: () => getPersonnePhysiqueList({ sort })
        }
      };
    })
  }];

  const resetFormAndGetPersonnePhysique = async (pers_physique_id) => {
    await resetFormValues(formName);

    const generalInfo = await getPersonnePhysiqueId(pers_physique_id);
    const familyInfo = await getFamilyLink(pers_physique_id);
    const society = await get_person_society(pers_physique_id);
    const employeeInfo = await getEmployee(pers_physique_id);
    const documentInfo = await getDocument(pers_physique_id);
    const access = await getSocietyAccess(pers_physique_id);

    const family = await familyInfo.map(e => ({
      ...e,
      civility: _.get(e, 'civility.value'),
      link_type_id: e.link_type.id
    }));

    const employee = employeeInfo.map(e => ({
      ...e,
      name: e.society.name,
      signatory_function_id: _.get(e, 'signatory_function.id', -1),
      function_id: e.function.id
    }));

    setCurrentForm(formName, {
      ...generalInfo,
      family,
      documentInfo,
      access,
      society,
      employee
    });

    initializeFamilySection(formName, family);
    initializeEmployeeSection(formName, employee);
    initializeDocumentSection(formName, documentInfo);
    initializeSocietySection(formName, society);
    initializeUserSection(formName, access);
  };

  const checkChangesAndGetPersonnePhysique = (pers_physique_id) => {
    if (selectedPhysicalPerson) {
      closeCurrentForm(() => resetFormAndGetPersonnePhysique(pers_physique_id));
    } else {
      resetFormAndGetPersonnePhysique(pers_physique_id);
    }
  };

  const body = currentPhysicalPersons.map((physicalPerson, i) => {
    const {
      pers_physique_id,
      civility,
      name,
      firstname,
      physical_person_type,
      coordonnee
    } = physicalPerson;

    const status = _.get(physical_person_type, 'label');

    const contact = coordonnee.map(e => ({
      typeOfCoord: e.type.label,
      coordValue: e.value
    }));

    const isMail = _.filter(contact, { typeOfCoord: 'Mail' });
    const additionalMailCount = isMail.length - 1;
    const mail = _.get(isMail[0], 'coordValue'); // if there are several mails, it will only display the first one

    const isCellphoneNumber = _.filter(contact, { typeOfCoord: 'Tel. port.' });
    const isFixedPhoneNumber = _.filter(contact, { typeOfCoord: 'Tel. fixe' });
    const additionalPhonenumberCount = [...isCellphoneNumber, ...isFixedPhoneNumber].length - 1;

    const cellphoneNumber = _.get(isCellphoneNumber[0], 'coordValue'); // if there are several phone numbers, it will only display the first one
    const fixedPhoneNumber = _.get(isFixedPhoneNumber[0], 'coordValue'); // if there are several phone numbers, it will only display the first one

    const phoneNumber = fixedPhoneNumber || cellphoneNumber;

    return {
      keyRow: `physicalPerson${i}`,
      props: {
        hover: true,
        onClick: () => checkChangesAndGetPersonnePhysique(pers_physique_id)
      },
      value: [
        {
          _type: 'string',
          keyCell: `physicalPersonGender${i}`,
          props: {
            label: _.get(civility, 'label')
          }
        },
        {
          _type: 'string',
          keyCell: `physicalPersonName${i}`,
          props: {
            label: name
          }
        },
        {
          _type: 'string',
          keyCell: `physicalPersonFirstname${i}`,
          props: {
            label: firstname
          }
        },
        {
          _type: 'string',
          keyCell: `physicalPersonCompany${i}`,
          props: {
            label: '' // To be defined
          }
        },
        {
          _type: 'string',
          keyCell: `physicalPersonPhoneNumber${i}`,
          props: {
            label: phoneNumber
          }
        },
        {
          _type: additionalPhonenumberCount > 0 ? 'status' : null,
          keyCell: `physicalPersonPhoneNumberCount${i}`,
          props: {
            data: [
              {
                value: additionalPhonenumberCount > 0 ? `+${additionalPhonenumberCount}` : null,
                status: 'count'
              }
            ]
          }
        },
        {
          _type: 'string',
          keyCell: `physicalPersonMail${i}`,
          props: {
            label: mail
          }
        },
        {
          _type: additionalMailCount > 0 ? 'status' : null,
          keyCell: `physicalPersonMailCount${i}`,
          props: {
            data: [
              {
                value: additionalMailCount > 0 ? `+${additionalMailCount}` : null,
                status: 'count'
              }
            ]
          }
        },
        {
          _type: 'string',
          keyCell: `physicalPersonRole${i}`,
          props: {
            label: status
          }
        },
        {
          _type: 'iconButton',
          keyCell: `deletePhysicalPerson${i}`,
          props: {
            name: 'icon-trash',
            hidden: !isUserAdmin,
            onClick: (e) => {
              e.stopPropagation();
              setDeletionDialog('physicalPerson', physicalPerson.pers_physique_id);
            }
          }
        }
      ]
    };
  });

  const transformedParams = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData,
    isLoading,
    isError,
    onRefresh: () => setPhysicalTableData(tableName, societyId, transformedParams),
    param: transformedParams,
    cellHeight: 32,
    maxVisibleItems: 10
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TableFactory);
