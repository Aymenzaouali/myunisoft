import { connect } from 'react-redux';
import FamilyArray from 'components/groups/Tables/Family/FamilyArray';
import {
  arrayRemove,
  change,
  getFormValues
} from 'redux-form';
import { delFamilyLink as delFamilyLinkThunk } from 'redux/tabs/physicalPersonCreation';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    linkTypes: _.get(getCurrentTabState(state), 'physicalPersonCreation.linkTypes', []),
    relatives: getFormValues(tabFormName('physicalPersonCreationForm', societyId))(state)
  };
};
const mapDispatchToProps = dispatch => ({
  changeTypeRelative: (id, index, type) => dispatch(change(tabFormName('physicalPersonCreationForm', id), `family[${index}].link_type_id`, type)),
  removeRelative: (id, index, idFamily) => {
    dispatch(arrayRemove(tabFormName('physicalPersonCreationForm', id), 'family', index));
    dispatch(delFamilyLinkThunk([{ id: idFamily }]));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { linkTypes } = stateProps;

  const familyLinkTypes = linkTypes.map(type => ({
    value: type.id,
    label: type.label,
    name: type.value
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    familyLinkTypes
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FamilyArray);
