import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import FactoryTable from 'components/groups/Tables/Factory';
import { LETTERS_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import { lettersKeys } from 'assets/constants/keys';
import Pagination from 'components/basics/Pagination';
import tableActions from 'redux/tables/actions';
import { getLetters } from 'redux/letters';
import {
  CheckBoxCell,
  SortCell
} from 'components/basics/Cells';
import _get from 'lodash/get';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, LETTERS_TABLE_NAME);
  return {
    letters: _get(state, `tables.${tableName}.data`, []),
    society_id,
    navigation_society_id: society_id,
    isEditing: _get(state, `tables.${tableName}.isEditing`, false),
    lastSelectedRow: _get(state, `tables.${tableName}.lastSelectedRow`, null),
    selectedRowsCount: _get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _get(state, `tables.${tableName}.selectedRows`, {}),
    errors: _get(state, `tables.${tableName}.errors`, {}),
    initialData: _get(state, `tables.${tableName}.initialData`),
    pagination: _get(state, `tables.${tableName}.pagination`, { limit: 20, offset: 0 })
  };
};

const mapDispatchToProps = dispatch => ({
  editRow: (tableName, item) => dispatch(tableActions.editRow(tableName, item)),
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors)),
  setData: (tableName, data) => dispatch(tableActions.setData(tableName, data)),
  loadData: payload => dispatch(getLetters(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    letters,
    selectedRows,
    selectedRowsCount,
    pagination
  } = stateProps;

  const tableName = getTableName(society_id, LETTERS_TABLE_NAME);

  const {
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows,
    loadData
  } = dispatchProps;

  const headerKeys = lettersKeys;

  const isAllSelected = selectedRowsCount === letters.length;

  const headers = [{
    keyRow: `${society_id}lettersHeaderRow0`,
    value: headerKeys.map((data, i) => {
      const { type, keyLabel, isAmount } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          component: CheckBoxCell,
          keyCell: `${society_id}lettersHeader.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = letters.map(al => al.id);
                selectAllRows(tableName, ids);
              }
            }
          }
        });
      } else {
        cell = {
          component: SortCell,
          keyCell: `${society_id}lettersHeader.${keyLabel}${i}`,
          cellProp: {
            isAmount
          },
          props: {
            children: I18n.t(keyLabel),
            onClick: () => {}
          }
        };
      }
      return cell;
    })
  }];

  const body = letters.map((letter) => {
    const { id } = letter;
    const ischecked = selectedRows[id] === true;
    const handleRowSelect = () => {
      if (ischecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
      }
    };
    const generateKey = (cellName = '') => `letterRow${cellName}${id}`;
    return {
      keyRow: generateKey(),
      props: {
        hover: true,
        selected: ischecked
      },
      value: [
        {
          component: CheckBoxCell,
          keyCell: generateKey('CheckBox'),
          props: {
            checked: ischecked,
            onChange: handleRowSelect
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Label'),
          props: {
            children: letter.title
          }
        },
        {
          component: 'div',
          keyCell: generateKey('CreatedAt'),
          props: {
            children: letter.created_at
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Type'),
          props: {
            children: letter.id_local_paragraph_type
          }
        },
        {
          component: 'div',
          keyCell: generateKey('CreatedBy'),
          props: {
            children: `${_get(letter, 'created_by.firstname', '')} ${_get(letter, 'created_by.lastname', '')}`
          }
        },
        {
          component: 'div',
          keyCell: generateKey('ModifiedBy'),
          props: {
            children: `${_get(letter, 'modified_by.firstname', '')} ${_get(letter, 'modified_by.lastname', '')}`
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Status'),
          props: {
            children: letter.status
          }
        },
        {
          component: 'div',
          keyCell: generateKey('SendDate'),
          props: {
            children: letter.send_at
          }
        }
      ]
    };
  });

  const footer = [
    {
      keyRow: 'pagination',
      value: [{
        component: Pagination,
        keyCell: `lettersFooter${society_id}0`,
        cellProp: {
          colSpan: 18
        },
        props: {
          rows: letters.length,
          rowsPerPage: pagination.limits,
          page: parseInt(pagination.offset / pagination.limit, 10),
          pages: 1,
          onChangePage: page => loadData({ limit: pagination.limit, offset: page * 20 })
        }
      }]
    }
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: body
      },
      footer: {
        props: {},
        row: footer
      }
    }
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FactoryTable);
