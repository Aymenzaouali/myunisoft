import { connect } from 'react-redux';
import { SettingStandardMailTable } from 'components/groups/Tables/';
import SettingStandardMailService from 'redux/settingStandardMail';
import { paragraphsTypesKeys } from 'assets/constants/keys';
import {
  CheckBoxCell
} from 'components/basics/Cells';
import { PARAGRAPHS_TYPES_TABLE, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import _ from 'lodash';
import I18n from 'assets/I18n';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, PARAGRAPHS_TYPES_TABLE);
  return {
    associate_list: _.get(state, `tables.${tableName}.data`, []),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    errors: _.get(state, `tables.${tableName}.errors`, {}),
    initialData: _.get(state, `tables.${tableName}.initialData`),
    society_id: state.navigation.id,
    paragraphs_types: state.settingStandardMail.paragraphs_types
  };
};

const mapDispatchToProps = dispatch => ({
  geParagraphsTypes: () => dispatch(
    SettingStandardMailService.geParagraphsTypes()
  ),
  getParagraphs: id => dispatch(
    SettingStandardMailService.getParagraphs(id)
  ),
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors)),
  setTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    selectedRowsCount,
    associate_list,
    selectedRows,
    paragraphs_types
  } = stateProps;

  const tableName = getTableName(society_id, PARAGRAPHS_TYPES_TABLE);
  const isLoading = _.get(paragraphs_types, `${society_id}.isLoading`, true);
  const isError = _.get(paragraphs_types, `${society_id}.isError`, undefined);

  const {
    geParagraphsTypes,
    getParagraphs,
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows,
    setTableData
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const isAllSelected = selectedRowsCount === associate_list.length;

  const headers = [{
    keyRow: 'paragraphsTypesTableHeaderRow',
    value: paragraphsTypesKeys.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `paragraphsTypesTableHeaderCell${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = associate_list.map(al => al.unique_id);
                selectAllRows(tableName, ids);
              }
            },
            id: keyLabel.toString()
          },
          cellProp: {
            style: {
              maxWidth: '44px'
            }
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `paragraphsTypesTableHeaderCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`settingStandardMail.tables.${keyLabel}`)
        }
      };
    })

  }];

  const bodyRow = associate_list.map((statement) => {
    const { unique_id: id } = statement;
    const ischecked = selectedRows[id] === true;
    const handleRowSelect = () => {
      if (ischecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    const generateKey = (cellName = '') => `${PARAGRAPHS_TYPES_TABLE}${cellName}${id}`;
    return {
      keyRow: generateKey(),
      props: {
        onClick: () => getParagraphs(statement.id),
        hover: true,
        selected: ischecked
      },
      value: [
        {
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: generateKey('CheckBox'),
          props: {
            checked: ischecked,
            onChange: handleRowSelect
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('name'),
          props: {
            children: _.get(statement, 'name', '')
          }
        },
        {
          _type: 'checkbox',
          keyCell: generateKey('label_as_object'),
          props: {
            checked: _.get(statement, 'label_as_object', false)
          }
        }
      ]
    };
  });

  const transformedParam = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: bodyRow
    }
  };
  const tableExportName = I18n.t('settingStandardMail.title');

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData: geParagraphsTypes,
    isLoading,
    isError,
    param: transformedParam,
    setTable: () => setTableData(tableExportName, society_id, transformedParam)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(SettingStandardMailTable);
