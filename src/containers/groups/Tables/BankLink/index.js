import { TableFactory } from 'components/groups/Tables';
import { connect } from 'react-redux';
import { change } from 'redux-form';
import _ from 'lodash';
import moment from 'moment';
import actions from 'redux/bankLink/actions';
import { getEntries } from 'redux/accounting';
import { updateArgsSplit } from 'redux/tabs/split/actions';
import { setCurrentForm } from 'redux/tabs/form/actions';
import accountingActions from 'redux/accounting/actions';
import { bankLinkKeys } from 'assets/constants/keys';
import {
  getBankLink as getBankLinkThunk,
  getComments
} from 'redux/bankLink';
import { openDialog } from 'redux/dialogs';
import I18n from 'assets/I18n';
import { formatNumber } from 'helpers/number';
import { getSelectedMonth, getSelectedYear } from 'helpers/date';

const mapStatetoProps = state => ({
  society_id: state.navigation.id,
  closed_diligence: _.get(state, `bankLink.bankLink[${state.navigation.id}].diligence.closed`),
  linesList: _.get(state, `bankLink.bankLink[${state.navigation.id}]`, []),
  isLoading: _.get(state, 'bankLink.isLoading', false),
  isError: _.get(state, 'bankLink.isError', false),
  selectedLine: _.get(state, 'bankLink.billSelected', []),
  selectedBills: _.get(state, `bankLink.selectedBill[${state.navigation.id}]`, []),
  initialSort: _.get(state, `bankLink.bankLink[${state.navigation.id}].sort`, { column: 'label', direction: 'asc' })
});

const mapDispatchToProps = dispatch => ({
  changeFormValue: (form, field, value) => dispatch(change(form, field, value)),
  getData: sort => dispatch(getBankLinkThunk(
    undefined,
    undefined,
    undefined,
    sort
  )),
  addBill: (account_id, society_id) => dispatch(actions.addBill(account_id, society_id)),
  removeBill: (
    account_id,
    society_id
  ) => dispatch(actions.removeBill(account_id, society_id)),
  resetAllBills: society_id => dispatch(actions.resetAllBills(society_id)),
  addAllBills: (society_id, bills) => dispatch(actions.addAllBills(society_id, bills)),
  setBankLinkTableData: (society_id, tableData) => dispatch(
    actions.setBankLinkTableData(society_id, tableData)
  ),
  openCommentsDialog: dialogProps => dispatch(openDialog(dialogProps)),
  getComments: bill_id => dispatch(getComments(bill_id)),
  updateArgsSplit: (args) => {
    dispatch(updateArgsSplit('bankLink', args));
  },
  setCurrentForm: (formName, initial) => dispatch(setCurrentForm(formName, initial, ['day', 'month', 'year'])),
  getEntryForSplit: opts => dispatch(getEntries(opts)),
  setCurrentEntry: (currentEntry, society_id) => {
    dispatch(accountingActions.setCurrentEntry(currentEntry, society_id));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    linesList,
    society_id,
    selectedBills,
    isLoading,
    isError,
    initialSort,
    closed_diligence
  } = stateProps;

  const {
    addBill,
    removeBill,
    resetAllBills,
    setBankLinkTableData,
    addAllBills,
    getData,
    updateArgsSplit,
    changeFormValue,
    setCurrentEntry,
    getEntryForSplit
  } = dispatchProps;

  const { split } = ownProps;

  const formNameForSplit = `${society_id}newAccounting`;

  const initEntryForSplit = async (entryId) => {
    const opts = {
      entry_id: entryId
    };

    const entry = await getEntryForSplit(opts);

    const { entry_array } = entry;

    const entry_list = _.get(entry_array[0], 'entry_list', []);
    const entry_data = entry_array[0] || {};

    const lastEntryDate = _.get(entry_data, 'date_piece', '');
    const newEntries = entry_list.map((e) => {
      const counterpart_account = _.get(e.account, 'counterpart_account') && {
        ..._.get(e.account, 'counterpart_account'),
        account_id: _.get(e.account, 'counterpart_account.account_id_contrepart', ''),
        account_number: _.get(e.account, 'counterpart_account.account_number_contrepart', ''),
        label: _.get(e.account, 'counterpart_account.label_counterpart', ''),
        value: _.get(e.account, 'counterpart_account.label_counterpart', '')
      };

      const payment_type = {
        ...e.payment_type,
        value: _.get(e, 'payment_type.payment_type_id'),
        label: _.get(e, 'payment_type.name')
      };

      return ({
        ...e,
        account: {
          ...e.account,
          value: _.get(e.account, 'account_number'),
          counterpart_account
        },
        label: e.label,
        deadline: e.deadline,
        debit: e.debit && formatNumber(e.debit),
        credit: e.credit && formatNumber(e.credit),
        payment_type,
        piece: e.piece,
        piece2: e.piece2,
        worksheet: e.flags
      });
    });
    changeFormValue(formNameForSplit, 'entry_list', newEntries);
    changeFormValue(formNameForSplit, 'pj_list', entry_data.pj_list);
    changeFormValue(formNameForSplit, 'comment', entry_data.comment);
    changeFormValue(formNameForSplit, 'diary', entry_data.diary);
    changeFormValue(formNameForSplit, 'day', moment(entry_data.date_piece, 'YYYYMMDD').format('DD'));
    changeFormValue(formNameForSplit, 'month', getSelectedMonth(lastEntryDate));
    changeFormValue(formNameForSplit, 'year', getSelectedYear(lastEntryDate));
    changeFormValue(formNameForSplit, 'type', 'e');
    changeFormValue(formNameForSplit, 'entry_id', entry_data.entry_id);

    const data = {
      entry_id: entry_data.entry_id,
      entry_list: newEntries,
      pj_list: entry_data.pj_list,
      diary: entry_data.diary,
      type: 'e'
    };

    if (entry_data.comment) {
      data.comment = entry_data.comment;
    }

    setCurrentEntry(data, society_id);
    setCurrentForm(formNameForSplit, data);
  };

  const lines = _.get(linesList, 'line_writing', []);
  const isAllSelected = lines.every(bill => selectedBills.some(
    b => b.id_line_entries === bill.id_line_entries
  ));

  const headers = [{
    keyRow: 'bankLinkHeaderRow',
    value: bankLinkKeys.map((data, i) => {
      const {
        type,
        keyLabel,
        isAmount
      } = data;

      const {
        column,
        direction: oldDirection
      } = initialSort;

      const defaultUserDirection = 'desc';
      const selectedUserDirection = oldDirection === 'asc' ? 'desc' : 'asc';
      const direction = column === keyLabel
        ? selectedUserDirection
        : defaultUserDirection;

      const sort = {
        column: `${keyLabel}`,
        direction
      };

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `bankLinkHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllBills(society_id);
              } else {
                addAllBills(society_id, lines);
              }
            },
            disabled: closed_diligence,
            id: keyLabel
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `bankLinkHeaderCell.${keyLabel}${i}`,
        cellProp: {
          isAmount
        },
        props: {
          children: I18n.t(`tables.bankLink.${keyLabel}`),
          direction,
          onClick: () => getData(sort)
        }
      };
    })

  }];

  const bodyRow = lines.map((bankLink, i) => ({
    keyRow: `bankLink${i}`,
    props: {
      hover: true,
      onClick: () => {
        if (split.master) {
          updateArgsSplit({ entry_id: bankLink.entry_id, blocked: closed_diligence });
          initEntryForSplit(bankLink.entry_id);
        }
      }
    },
    value: [
      {
        _type: 'checkbox',
        keyCell: `bankLinkBox${i}`,
        props: {
          checked: selectedBills.some(b => b.id_line_entries === bankLink.id_line_entries),
          onClick: (e) => {
            e.stopPropagation();
          },
          onChange: () => {
            if (selectedBills.some(b => b.id_line_entries === bankLink.id_line_entries)) {
              removeBill(bankLink, society_id);
            } else {
              addBill(bankLink, society_id);
            }
          },
          disabled: closed_diligence,
          customColor: selectedBills.some(db => db.id_line_entries === bankLink.id_line_entries && db.isDotted) && 'yellow',
          id: i
        },
        cellProp: {
          style: {
            width: '44px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `bankLinkDate${i}`,
        props: {
          label: moment(bankLink.piece_date).format('DD/MM/YYYY')
        },
        cellProp: {
          style: {
            width: '80px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `bankLinkAccount${i}`,
        props: {
          label: bankLink.no_compte
        },
        cellProp: {
          style: {
            width: '131px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `bankLinkPiece1${i}`,
        props: {
          label: bankLink.piece
        },
        cellProp: {
          style: {
            width: '92px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `bankLinkPiece2${i}`,
        props: {
          label: bankLink.piece_2
        },
        cellProp: {
          style: {
            width: '75px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `bankLinkLetter${i}`,
        props: {
          label: bankLink.dotting_mm && bankLink.dotting_aa && (
            bankLink.dotting_mm + bankLink.dotting_aa
          )
        },
        cellProp: {
          style: {
            width: '75px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `bankLinkName${i}`,
        props: {
          label: bankLink.label
        },
        cellProp: {
          style: {
            width: '246px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `bankLinkDebit${i}`,
        props: {
          label: bankLink.debit !== 0 && bankLink.debit !== null && formatNumber(bankLink.debit),
          isAmount: true
        },
        cellProp: {
          style: {
            width: '120px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `bankLinkCredit${i}`,
        props: {
          label: bankLink.credit !== 0 && bankLink.credit !== null && formatNumber(bankLink.credit),
          isAmount: true
        },
        cellProp: {
          style: {
            width: '120px'
          }
        }
      }
    ]
  }));

  const formattedBankLinkTableData = {
    ...dispatchProps,
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: bodyRow
    }
  };

  return {
    ...dispatchProps,
    param: { ...formattedBankLinkTableData },
    isLoading,
    isError,
    cellHeight: 38,
    maxVisibleItems: 10,
    onRefresh: () => setBankLinkTableData(society_id, formattedBankLinkTableData)
  };
};

export default connect(mapStatetoProps, mapDispatchToProps, mergeProps)(TableFactory);
