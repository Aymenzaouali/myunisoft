import { connect } from 'react-redux';
import _ from 'lodash';
import { reset, getFormValues } from 'redux-form';
import I18n from 'assets/I18n';
import { companyListTranslationKeys } from 'assets/constants/keys';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import { CompanyTable } from 'components/groups/Tables';
import {
  getCompanyList as getCompanyListThunk,
  getCompany as getCompanyThunk
} from 'redux/tabs/companyList';
import {
  getExercises as getExercisesThunk,
  getFiscalFolder as getFiscalFolderThunk
} from 'redux/tabs/companyCreation';
import { setDeletionDialog } from 'redux/tabs/crm/actions';
import { setCurrentForm, closeCurrentForm } from 'redux/tabs/form/actions';
import tableActions from 'redux/tables/actions';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  companyList: getCurrentTabState(state).companyList || {},
  rowsPerPage: getCurrentTabState(state).companyList.limit,
  companyListFilter: getFormValues(`companyListFilter_${state.navigation.id}`)(state),
  isUserAdmin: state.login.user.isadmin
});

const mapDispatchToProps = dispatch => ({
  getExercises: id => dispatch(getExercisesThunk(id)),
  getCompanyList: payload => dispatch(getCompanyListThunk(payload)),
  getCompany: id => dispatch(getCompanyThunk(id)),
  resetFormValues: form => dispatch(reset(form)),
  getFiscalFolder: id => dispatch(getFiscalFolderThunk(id)),
  setCurrentForm: (name, initial) => dispatch(setCurrentForm(name, initial)),
  closeCurrentForm: discardCb => dispatch(closeCurrentForm(discardCb)),
  setTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  ),
  setDeletionDialog: (mode, id) => dispatch(setDeletionDialog(mode, id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId, companyList } = stateProps;
  const {
    getCompanyList,
    getCompany,
    resetFormValues,
    getExercises,
    getFiscalFolder,
    setCurrentForm,
    closeCurrentForm,
    setTableData
  } = dispatchProps;

  const formName = tabFormName('companyCreationForm', societyId);

  const loadData = async () => {
    await getCompanyList({ societyId });
  };

  const {
    companiesData,
    isLoading,
    isError,
    selectedCompany
  } = companyList;

  const currentCompanies = _.get(companiesData, 'society_array', []);
  const headers = [{
    keyRow: 'companyListHeaderRow',
    value: companyListTranslationKeys.map((data, i) => {
      const {
        type,
        keyLabel,
        colorError,
        isDisabled
      } = data;
      const {
        sort: {
          column,
          direction: oldDirection
        }
      } = companyList;

      const defaultCompanyDirection = 'desc';
      const selectedCompanyDirection = oldDirection === 'asc' ? 'desc' : 'asc';
      const direction = column === keyLabel
        ? selectedCompanyDirection
        : defaultCompanyDirection;

      const sort = {
        column: `${keyLabel}`,
        direction
      };

      if (type === 'string') {
        return {
          _type: 'string',
          keyCell: `companyListHeaderCell.${keyLabel}${i}`,
          props: {
            id: keyLabel
          }
        };
      }
      if (type === 'label') {
        return {
          _type: 'string',
          keyCell: `companyListHeaderCell.${keyLabel}${i}`,
          props: {
            label: I18n.t(`tables.companyList.${keyLabel}`),
            typoVariant: 'p'
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `companyListHeaderCell.${keyLabel}${i}`,
        props: {
          children: I18n.t(`tables.companyList.${keyLabel}`),
          direction,
          onClick: () => getCompanyList({ societyId, sort }),
          colorError,
          isDisabled
        }
      };
    })
  }];

  const resetFormAndGetCompanyAndResetExercises = async (society_id) => {
    await resetFormValues(formName);
    const society = await getCompany(society_id);
    const exercises = await getExercises(society_id);
    const fiscal_folder = await getFiscalFolder(society_id);

    // TODO: Investigate and fix pop-up opening when data have not changed.
    // const {
    //   society_status,
    //   info_bnc,
    //   ...other
    // } = fiscal_folder;

    setCurrentForm(formName, {
      ...society,
      exercises,
      fiscal_folder
      // fiscal_folder: {
      //   society_status,
      //   info_bnc: {
      //     ...info_bnc,
      //     comptability_held: null,
      //     comptability_type: null,
      //     result_rule: null
      //   },
      //   other: {
      //     ...other,
      //     edi: undefined,
      //     sheet_group: undefined,
      //     vat_regime: undefined,
      //     gestion_center: undefined
      //   }
      // }
    });
  };

  const checkChangesAndGetCompany = async (society_id) => {
    if (selectedCompany) {
      closeCurrentForm(() => resetFormAndGetCompanyAndResetExercises(society_id));
    } else {
      resetFormAndGetCompanyAndResetExercises(society_id);
    }
  };

  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData,
    setCompanyTableData: param => setTableData(tableName, societyId, param),
    isLoading,
    isError,
    currentCompanies,
    checkChangesAndGetCompany,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {}
      }
    },
    cellHeight: 32,
    maxVisibleItems: 10
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CompanyTable);
