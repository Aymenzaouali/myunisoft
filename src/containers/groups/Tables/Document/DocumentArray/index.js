import { connect } from 'react-redux';
import DocumentArray from 'components/groups/Tables/Document/DocumentArray';
import { arrayRemove, getFormValues } from 'redux-form';
import { delDocument as delDocumentThunk } from 'redux/tabs/physicalPersonCreation';
import moment from 'moment';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    physicalPersonForm: getFormValues(tabFormName('physicalPersonCreationForm', societyId))(state),
    selectedPersPhysique: _.get(getCurrentTabState(state), 'physicalPersonCreation.selected_physical_person', {})
  };
};
const mapDispatchToProps = dispatch => ({
  removeDocument: (societyId, index, documentInfo, id) => {
    // dispatch(arrayRemove('documentArray', 'documents', index));
    dispatch(arrayRemove(tabFormName('physicalPersonCreationForm', societyId), 'documentInfo', index));
    dispatch(delDocumentThunk(documentInfo, id));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { physicalPersonForm = {} } = stateProps;
  const { documentInfo = [] } = physicalPersonForm;

  const documents = documentInfo.map(e => ({
    ...e,
    date_time: moment(e.date_time).format('DD/MM/YYYY')
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    documents
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(DocumentArray);
