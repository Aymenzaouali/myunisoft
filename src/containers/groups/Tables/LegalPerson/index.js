import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import LegalPerson from 'components/groups/Tables/LegalPerson';
import { legalPersonRefKeys } from 'assets/constants/keys';
import {
  SignatoryFunctionReduxSelect
} from 'containers/reduxForm/Inputs';
import { LEGAL_PERSON_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import { tabFormName } from 'helpers/tabs';
import classnames from 'classnames';
import {
  TextFieldCell,
  CheckBoxCell,
  DatePickerCell,
  SortCell
} from 'components/basics/Cells';
import moment from 'moment';
import { getAutoCompleteProps } from 'redux/tables/utils';
import _ from 'lodash';
import cellStyles from 'components/basics/Cells/BodyCell/BodyCell.module.scss';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const current_society_id = _.get(state, `form.${tabFormName('companyCreationForm', society_id)}.values.society_id`, 0);
  const tableName = getTableName(society_id, LEGAL_PERSON_TABLE_NAME);
  const baseLegalPersonFileName = `${window.location.href.split('/').slice(-1).pop().toUpperCase()}_${LEGAL_PERSON_TABLE_NAME}`;
  return {
    baseLegalPersonFileName,
    associate_list: _.get(state, `tables.${tableName}.data`, []),
    isEditing: _.get(state, `tables.${tableName}.isEditing`, false),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`),
    initialData: _.get(state, `tables.${tableName}.initialData`),
    society_id: current_society_id,
    navigation_society_id: society_id,
    errors: _.get(state, `tables.${tableName}.errors`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  selectRow: (tableName, id) => dispatch(tableActions.selectRow(tableName, id)),
  unselectRow: (tableName, id) => dispatch(tableActions.unselectRow(tableName, id)),
  selectAllRows: (tableName, ids) => dispatch(tableActions.selectAllRows(tableName, ids)),
  unselectAllRows: tableName => dispatch(tableActions.unselectAllRows(tableName)),
  editRow: (tableName, item) => dispatch(tableActions.editRow(tableName, item)),
  setErrors: (tableName, id, errors) => dispatch(tableActions.setErrors(tableName, id, errors)),
  setLegalPersonTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    society_id,
    navigation_society_id,
    associate_list,
    isEditing,
    selectedRows,
    selectedRowsCount,
    lastSelectedRow,
    errors,
    initialData,
    baseLegalPersonFileName
  } = stateProps;

  const {
    selectRow,
    unselectRow,
    selectAllRows,
    unselectAllRows,
    editRow,
    setLegalPersonTableData,
    setErrors
  } = dispatchProps;

  const {
    inputRef
  } = ownProps;

  const tableName = getTableName(navigation_society_id, LEGAL_PERSON_TABLE_NAME);

  const isAllSelected = associate_list.length === selectedRowsCount;

  const headerKeys = legalPersonRefKeys;

  const headers = [{
    keyRow: `${society_id}legalPersonTableHeaderRow0`,
    value: headerKeys.map((data, i) => {
      const { type, keyLabel, isAmount } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: `${society_id}legalPersonnTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onChange: () => {
              if (isAllSelected) {
                unselectAllRows(tableName);
              } else {
                const ids = associate_list.map(lp => lp.id);
                selectAllRows(tableName, ids);
              }
            }
          }
        });
      } else {
        cell = {
          component: SortCell,
          keyCell: `${society_id}legalPersonnTableHeaderCell.${keyLabel}`,
          cellProp: {// TODO: Add sticky on theme
            isAmount
          },
          props: {
            children: I18n.t(`companyCreation.associates.legalPersonArrayTitle.${keyLabel}`),
            onClick: () => {}
          }
        };
      }
      return cell;
    })
  }];
  const body = associate_list.map((society) => {
    const { id } = society;
    const generateKey = (cellName = '') => `legalPerson${cellName}${id}`;
    const isChecked = selectedRows[id] === true;
    const isEditingRow = isChecked && isEditing;
    const startDate = _.get(society, 'start_date', '');
    const endDate = _.get(society, 'end_date', '');
    const formattedStartDate = moment(startDate).isValid() ? moment(startDate).format('DD/MM/YYYY') : '';
    const formattedEndDate = moment(endDate).isValid() ? moment(endDate).format('DD/MM/YYYY') : '';
    const isLastSelected = lastSelectedRow === id;
    const associateErrors = _.get(errors, society.id, {});
    const ini = initialData.filter(e => e.id === society.id);
    const dateEndPresente = _.get(ini, 'end_date', '');
    const isCreatedLine = _.get(society, 'isCreated', false);

    let socialPartsAreEnable = formattedEndDate === '';

    const endDateRender = () => {
      const isEnableCell = ((isEditingRow && !dateEndPresente) && !isCreatedLine);

      const endDateCell = {
        component: isEnableCell ? DatePickerCell : 'div',
        keyCell: generateKey('EndDate'),
        props: isEnableCell ? {
          value: endDate,
          onChange: value => editRow(tableName, {
            ...society,
            end_date: moment(value).format('YYYY-MM-DD')
          }),
          onBlur: isEnableCell ? (e) => {
            if (e.target.value !== '' && moment(e.target.value).isValid()) {
              socialPartsAreEnable = false;
              editRow(tableName, {
                ...society,
                social_part: {
                  PP: 0,
                  NP: 0,
                  US: 0,
                  percent: 0
                }
              });
            } else {
              socialPartsAreEnable = true;
              editRow(tableName, {
                ...society,
                end_date: ''
              });
            }
          } : {}
        } : {
          children: formattedEndDate
        }
      };

      return endDateCell;
    };

    const socialPartIsEditable = (socialPartsAreEnable && isEditingRow);

    const handleRowSelect = () => {
      if (isChecked) {
        unselectRow(tableName, id);
      } else {
        selectRow(tableName, id);
        setTimeout(() => {
          if (_.get(inputRef, 'current')) { inputRef.current.focus(); }
        }, 0);
      }
    };
    return {
      keyRow: generateKey(),
      props: {
        hover: true,
        selected: isChecked
      },
      value: [
        {
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: generateKey('CheckBox'),
          props: {
            checked: isChecked,
            onChange: handleRowSelect
          },
          cellProp: {
            className: cellStyles.checkboxCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Name'),
          props: {
            children: _.get(society, 'society.name') || _.get(society, 'name', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Siret'),
          props: {
            children: _.get(society, 'society.siret') || _.get(society, 'siret', '')
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Adress'),
          props: {
            children: _.get(society, 'society.address') || _.get(society, 'address', '')
          },
          cellProp: {
            className: cellStyles.multilineCell
          }
        },
        {
          component: isEditingRow ? SignatoryFunctionReduxSelect : 'div',
          keyCell: generateKey('SignatoryFunction'),
          error: _.get(associateErrors, 'signatory_function'),
          props: isEditingRow
            ? {
              autoFocus: isLastSelected,
              ...getAutoCompleteProps(editRow, society, 'signatory_function.label', 'signatory_function', tableName),
              onChange: (value) => {
                editRow(tableName, { ...society, signatory_function: value });
                setErrors(tableName, society.id, { signatory_function: undefined });
              }
            }
            : {
              children: _.get(society, 'signatory_function.label', '')
            }
        },
        {
          component: isEditingRow ? DatePickerCell : 'div',
          keyCell: generateKey('StartDate'),
          error: _.get(associateErrors, 'effective_date'),
          props: isEditingRow ? {
            value: startDate,
            onChange: (value) => {
              editRow(tableName, {
                ...society,
                start_date: moment(value).format('YYYY-MM-DD')
              });
              setErrors(tableName, society.id, { effective_date: undefined });
            }
          } : {
            children: formattedStartDate
          }
        },
        endDateRender(),
        {
          component: socialPartIsEditable ? TextFieldCell : 'div',
          error: _.get(associateErrors, 'social_part_PP'),
          keyCell: generateKey('NbPP'),
          props: socialPartIsEditable
            ? {
              defaultValue: _.get(society, 'social_part.PP'),
              placeholder: _.get(society, 'social_part.PP'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...society,
                  social_part: {
                    ...society.social_part,
                    PP: e.target.value
                  }
                });
                setErrors(tableName, society.id, {
                  social_part_PP: undefined
                });
              }
            }
            : {
              children: _.get(society, 'social_part.PP', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: socialPartIsEditable ? TextFieldCell : 'div',
          error: _.get(associateErrors, 'social_part_US'),
          keyCell: generateKey('NbUS'),
          props: socialPartIsEditable
            ? {
              defaultValue: _.get(society, 'social_part.US'),
              placeholder: _.get(society, 'social_part.US'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...society,
                  social_part: {
                    ...society.social_part,
                    US: e.target.value
                  }
                });
                setErrors(tableName, society.id, {
                  social_part_US: undefined
                });
              }
            } : {
              children: _.get(society, 'social_part.US', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: socialPartIsEditable ? TextFieldCell : 'div',
          error: _.get(associateErrors, 'social_part_NP'),
          keyCell: generateKey('NbNP'),
          props: socialPartIsEditable
            ? {
              defaultValue: _.get(society, 'social_part.NP'),
              placeholder: _.get(society, 'social_part.NP'),
              onBlur: (e) => {
                editRow(tableName, {
                  ...society,
                  social_part: {
                    ...society.social_part,
                    NP: e.target.value
                  }
                });
                setErrors(tableName, society.id, {
                  social_part_NP: undefined
                });
              }
            } : {
              children: _.get(society, 'social_part.NP', '')
            },
          cellProp: {
            className: cellStyles.numberCell
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Percent'),
          props: {
            children: _.get(society, 'social_part.percent')
          },
          cellProp: {
            className: classnames(cellStyles.numberCell, cellStyles.smallCell)
          }
        },
        {
          component: 'div',
          keyCell: generateKey('Capital'),
          props: {
            children: _.get(society, 'society.capital') || _.get(society, 'capital', '')
          },
          cellProp: {
            className: classnames(cellStyles.numberCell, cellStyles.smallCell)
          }
        }
      ]
    };
  });

  const transformedParams = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    setTableData:
      () => setLegalPersonTableData(
        baseLegalPersonFileName, navigation_society_id, transformedParams
      ),
    param: transformedParams
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(LegalPerson);
