import { connect } from 'react-redux';
import TableFactory from 'components/groups/Tables/Factory';
import I18n from 'assets/I18n';
import moment from 'moment';
import { push } from 'connected-react-router';
import { trackingOcrHeaderKeys } from 'assets/constants/keys';
import { getTrackingOcr as getTrackingOcrThunk } from 'redux/ocr';
import { formatNumber } from 'helpers/number';
import tableActions from 'redux/tables/actions';
import WindowHelper from 'helpers/window'; //eslint-disable-line
import _ from 'lodash';

const mapStateToProps = state => ({
  ocr_tracking: state.ocrWeb.ocr_tracking,
  userName: state.login.user.nom,
  userFirstName: state.login.user.prenom,
  societyId: state.navigation.id
});

const mapDispatchToProps = dispatch => ({
  getTrackingOcr: societyId => dispatch(getTrackingOcrThunk(societyId)),
  push: path => dispatch(push(path)),
  setOcrTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    ocr_tracking,
    userFirstName,
    userName,
    societyId
  } = stateProps;

  const {
    getTrackingOcr,
    setOcrTableData
  } = dispatchProps;

  const {
    row_number,
    pages_number
  } = ocr_tracking;

  const dashboardCollab = societyId === -2;

  const openWindow = (e, ocr_row) => {
    const {
      document_id,
      document_label,
      last_status_date_time,
      token
    } = ocr_row;

    e.preventDefault();
    e.stopPropagation();

    const files = [{
      date: last_status_date_time,
      document_id,
      name: document_label,
      thumbnail: `https://cloud.myunisoft.fr/index.php/apps/files_sharing/ajax/publicpreview.php?x=90&y=120&a=true&t=${token}&scalingup=0`,
      token
    }];

    if (token) {
      WindowHelper.forceOpen(files);
    }
  };

  const currentOcrTrackingArray = ocr_tracking[societyId] || {};
  const { isLoading, isError } = currentOcrTrackingArray;
  const currentOcrTracking = _.get(currentOcrTrackingArray, 'ocr_follow_up_array', []);
  const sort = _.get(currentOcrTrackingArray, 'sort', {});
  const currentUser = `${userFirstName} ${userName}`;

  const headerKeys = trackingOcrHeaderKeys;

  const headerValues = [];
  headerKeys.forEach((key, i) => {
    const head = {
      _type: 'sortCell',
      keyCell: `${societyId}accountingTableHeaderCell.${key}${i}`,
      cellProp: {
        className: 'trackingOcr__table-header'
      },
      props: {
        children: I18n.t(`OCR.trackingTable.${key}`),
        direction: _.get(sort, 'column') === key ? sort.direction : 'desc',
        onClick: () => {
          getTrackingOcr(
            {
              sort: {
                column: `${key}`,
                direction: sort.direction === 'asc' ? 'desc' : 'asc'
              }
            }
          );
        }
      }
    };

    const displaySocietyCell = !(!dashboardCollab && (key === 'society'));

    if (displaySocietyCell) {
      headerValues.push(head);
    }
  });

  const header = [{
    keyRow: `${societyId}trackingOcrTableHeaderRow0`,
    value: headerValues
  }];

  //   const body = ocr_tracking.ocr_follow_up_array.map((ocr_row) => {
  //     let iconName;
  //     const sendDate = ocr_row.created_date_time 
  //      ? moment(ocr_row.created_date_time, 'YYYYMMDDhhmm').format('DD/MM/YYYY kk:mm') 
  //      : '';
  //     const lastStatusDate = ocr_row.last_status_date_time 
  //      ? moment(ocr_row.last_status_date_time, 'YYYYMMDDhhmm').format('DD/MM/YYYY kk:mm') 
  //      : '';

  //     switch (ocr_row.status) {
  //     case 'done':
  //       iconName = 'icon-check';
  //       break;
  //     case 'ocrRunning':
  //       iconName = 'icon-progress';
  //       break;
  //     case 'error':
  //       iconName = 'icon-alert';
  //       break;
  //     default:
  //       iconName = '';
  //     }

  //     return {
  //       keyRow: `${societyId}trackingOcrTableBodyRow${ocr_row.row_number}`,
  //       value: [
  //         {
  //           _type: 'string',
  //           keyCell: 'society',
  //           props: {
  //             label: {
  //               value: ocr_row.society_name,
  //               className: 'trackingOcr__table-label'
  //             }
  //           }
  //         },
  //         {
  //           _type: 'string',
  //           keyCell: 'fileName',
  //           props: {
  //             label: {
  //               value: ocr_row.document_label,
  //               className: 'trackingOcr__table-label'
  //             }
  //           }
  //         },
  //         {
  //           _type: 'string',
  //           keyCell: 'type',
  //           props: {
  //             label: {
  //               value: I18n.t(`OCR.trackingTable.${ocr_row.invoice_label_id}`),
  //               className: 'trackingOcr__table-label'
  //             }
  //           }
  //         },
  //         {
  //           _type: 'string',
  //           keyCell: 'sendBy',
  //           props: {
  //             label: {
  //               value: ocr_row.agent_name === currentUser 
  //                ? I18n.t('OCR.trackingTable.me') 
  //                : ocr_row.agent_name,
  //               className: 'trackingOcr__table-sendByName'
  //             },
  //             detail: {
  //               value: sendDate,
  //               className: 'trackingOcr__table-date'
  //             },
  //             cellClassName: 'trackingOcr__table-sendBy'
  //           }
  //         },
  //         {
  //           _type: 'string',
  //           keyCell: 'status',
  //           props: {
  //             label: {
  //               value: I18n.t(`OCR.trackingTable.${ocr_row.status}`),
  //               className: 'trackingOcr__table-status-label'
  //             },
  //             detail: {
  //               value: lastStatusDate,
  //               className: 'trackingOcr__table-date'
  //             },
  //             icon: {
  //               left: {
  //                 name: iconName,
  //                 className: `trackingOcr__table-status-icon-${ocr_row.status}`
  //               }
  //             },
  //             cellClassName: 'trackingOcr__table-status',
  //             stringClassName: 'trackingOcr__table-status-string'
  //           }
  //         },
  //         {
  //           _type: 'string',
  //           keyCell: 'total',
  //           props: {
  //             label: {
  //               value: ocr_row.total_incl_taxes !== 0 ? `${ocr_row.total_incl_taxes}` : '',
  //               className: 'trackingOcr__table-amount'
  //             }
  //           }
  //         },
  //         {
  //           _type: 'string',
  //           keyCell: 'netTotal',
  //           props: {
  //             label: {
  //               value: ocr_row.total_excl_taxes !== 0 ? `${ocr_row.total_excl_taxes}` : '',
  //               className: 'trackingOcr__table-amount'
  //             }
  //           }
  //         },
  //         {
  //           _type: 'string',
  //           keyCell: 'vatTotal',
  //           props: {
  //             label: {
  //               value: ocr_row.vat_total !== 0 ? `${ocr_row.vat_total}` : '',
  //               className: 'trackingOcr__table-amount'
  //             }
  //           }
  //         }
  //       ]
  //     };
  //   });
  // }
  const body = currentOcrTracking.map((ocr_row) => {
    let iconName;
    const sendDate = ocr_row.created_date_time ? moment(ocr_row.created_date_time, 'YYYYMMDDhhmm').format('DD/MM/YYYY kk:mm') : '';
    const lastStatusDate = ocr_row.last_status_date_time ? moment(ocr_row.last_status_date_time, 'YYYYMMDDhhmm').format('DD/MM/YYYY kk:mm') : '';

    switch (ocr_row.status) {
    case 'done':
      iconName = 'icon-check';
      break;
    case 'ocrRunning':
      iconName = 'icon-progress';
      break;
    case 'error':
      iconName = 'icon-alert';
      break;
    case 'notRecognized':
      iconName = 'icon-close';
      break;
    default:
      iconName = '';
    }

    let bodyValues = [
      {
        _type: 'string',
        keyCell: `${societyId}trackingOcrTableFileName${ocr_row.row_number}`,
        props: {
          label: ocr_row.document_label,
          labelClassName: 'trackingOcr__table-label'
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}trackingOcrTableType${ocr_row.row_number}`,
        props: {
          label: I18n.t(`OCR.trackingTable.${ocr_row.invoice_label_id}`),
          labelClassName: 'trackingOcr__table-label'
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}trackingOcrTableSendBy${ocr_row.row_number}`,
        props: {
          label: ocr_row.agent_name === currentUser ? I18n.t('OCR.trackingTable.me') : ocr_row.agent_name,
          labelClassName: 'trackingOcr__table-sendByName',
          detail: sendDate,
          detailClassName: 'trackingOcr__table-date',
          cellClassName: 'trackingOcr__table-sendBy'
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}trackingOcrTableStatus${ocr_row.row_number}`,
        props: {
          label: I18n.t(`OCR.trackingTable.${ocr_row.status}`),
          labelClassName: 'trackingOcr__table-status-label',
          detail: lastStatusDate,
          detailClassName: 'trackingOcr__table-date',
          iconLeft: iconName,
          iconLeftClassName: `trackingOcr__table-status-icon-${ocr_row.status}`,
          cellClassName: 'trackingOcr__table-status',
          stringClassName: 'trackingOcr__table-status-string'
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}trackingOcrTableTotalExclTaxes${ocr_row.row_number}`,
        props: {
          label: ocr_row.total_excl_taxes !== 0 ? formatNumber(_.get(ocr_row, 'total_excl_taxes', 0), '0,0.00') : '',
          labelClassName: 'trackingOcr__table-amount'
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}trackingOcrTableVatTotal${ocr_row.row_number}`,
        props: {
          label: ocr_row.vat_total !== 0 ? formatNumber(_.get(ocr_row, 'vat_total', 0), '0,0.00') : '',
          labelClassName: 'trackingOcr__table-amount'
        }
      },
      {
        _type: 'string',
        keyCell: `${societyId}trackingOcrTableTotalInclTaxes${ocr_row.row_number}`,
        props: {
          label: ocr_row.total_incl_taxes !== 0 ? formatNumber(_.get(ocr_row, 'total_incl_taxes', 0), '0,0.00') : '',
          labelClassName: 'trackingOcr__table-amount'
        }
      }
    ];

    if (dashboardCollab) {
      const societyValue = {
        _type: 'string',
        keyCell: `${societyId}trackingOcrTableSociety${ocr_row.row_number}`,
        props: {
          label: ocr_row.society_name,
          labelClassName: 'trackingOcr__table-label'
        }
      };
      bodyValues = [societyValue, ...bodyValues];
    }

    return {
      keyRow: `${societyId}trackingOcrTableBodyRow${ocr_row.row_number}`,
      props: {
        hover: true,
        onClick: e => openWindow(e, ocr_row)
      },
      value: bodyValues
    };
  });


  // Define headers
  const param = {
    header: {
      props: {},
      row: header
    },
    body: {
      props: {},
      row: _.flatten(body)
    }
  };

  const tableName = I18n.t('OCR.trackingTable.title');
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isLoading,
    isError,
    param,
    onRefresh: () => setOcrTableData(tableName, societyId, param),
    row_number,
    pages_number
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TableFactory);
