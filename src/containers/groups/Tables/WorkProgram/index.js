import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector, change } from 'redux-form';
import _ from 'lodash';

import { Badge, IconButton } from '@material-ui/core';

import I18n from 'assets/I18n';
import { status } from 'assets/constants/data';
import { workProgramKeys } from 'assets/constants/keys';

import { getCurrentTabState, tabFormName } from 'helpers/tabs';

import { openDialog } from 'redux/dialogs';
import {
  getDiligences,
  updateDiligence,
  bulkUpdateDiligence,
  sendDiligencePJ
} from 'redux/tabs/dadp';
import {
  toggleDiligence,
  setRedirectLink,
  addSelectedDiligence,
  removeSelectedDiligence,
  toggleTimelyDialog,
  togglePermanentDialog
} from 'redux/tabs/dadp/actions';

import DiligenceCommentBox from 'containers/groups/Comments/Box/Diligence';

import FontIcon from 'components/basics/Icon/Font';
import { StatusString } from 'components/basics/Strings';
import WorkProgram from 'components/groups/Tables/WorkProgram';
import PJCell from 'components/groups/Tables/WorkProgram/PJCell';

import Link from 'components/basics/Link';
import { routesByKey } from 'helpers/routes';

import moment from 'moment';
import styles from './WorkProgram.module.scss';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);

  const societyId = state.navigation.id;
  const { openPermanent, openTimely } = dadp;

  const form = tabFormName('dadpWorkProgram', societyId);
  const selector = formValueSelector(form);
  const { end_date = '', start_date = '' } = _.get(
    state,
    `form.dadpReviewInfo_${societyId}.values`,
    {}
  );

  const reviewSelector = formValueSelector(
    tabFormName('dadpReviewInfo', societyId)
  );
  const sectionSelector = formValueSelector(
    tabFormName('dadpSection', societyId)
  );

  return {
    category: dadp.category,
    reviewId: dadp.reviews.selectedId,
    startDate: reviewSelector(state, 'start_date'),
    endDate: reviewSelector(state, 'end_date'),
    section: sectionSelector(state, 'section'),
    cycleId: dadp.cycles.selectedId,
    diligences: dadp.diligences,
    form,
    societyId,
    end_date,
    selectedDiligence: dadp.selectedDiligence,
    start_date,
    openPermanent,
    openTimely,
    workSheetOnly: selector(state, 'workSheetOnly'),
    initialValues: {
      startDate: reviewSelector(state, 'start_date'),
      endDate: reviewSelector(state, 'end_date')
    }
  };
};

const mapDispatchToProps = dispatch => ({
  getDiligences: (
    category,
    section_id,
    cycle_id,
    start_date,
    end_date,
    workSheetOnly
  ) => dispatch(
    getDiligences(
      category,
      section_id,
      cycle_id,
      start_date,
      end_date,
      workSheetOnly
    )
  ),
  updateDateFrom: value => dispatch(change('dateFrom', value)),

  updateDiligence: (diligence_id, type, state, prev_state) => dispatch(
    updateDiligence(diligence_id, type, state, prev_state)
  ),
  bulkUpdateDiligence: updates => dispatch(bulkUpdateDiligence(updates)),
  sendDiligencePJ: (diligence_id, files) => dispatch(sendDiligencePJ(diligence_id, files)),

  toggleDiligence: diligenceId => dispatch(toggleDiligence(diligenceId)),

  openCommentsDialog: diligence => dispatch(
    openDialog({
      body: {
        Component: DiligenceCommentBox,
        props: {
          diligence
        }
      }
    })
  ),
  // modal poctuel
  toggleTimelyDialog: diligence => dispatch(toggleTimelyDialog(diligence)),

  // modal permanent
  togglePermanentDialog: diligence => dispatch(togglePermanentDialog(diligence)),

  setRedirectLink: link => dispatch(setRedirectLink(link)),

  addSelectedDiligence: data => dispatch(addSelectedDiligence(data)),
  removeSelectedDiligence: data => dispatch(removeSelectedDiligence(data))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    diligences, societyId, end_date, selectedDiligence,
    start_date
  } = stateProps;
  const {
    updateDiligence, bulkUpdateDiligence, toggleDiligence,
    openCommentsDialog, setRedirectLink,
    addSelectedDiligence, removeSelectedDiligence,
    toggleTimelyDialog, togglePermanentDialog
  } = dispatchProps;

  const currentDiligences = diligences.data;
  const isLoading = diligences.loading;
  const isError = diligences.error;

  const headers = [
    {
      keyRow: 'header',
      value: workProgramKeys.map(key => ({
        _type: 'string',
        keyCell: key,
        cellProp: {
          colSpan: key === 'dilligence' ? 2 : 1
        },
        props: {
          typoVariant: 'inherit',
          label: I18n.t(`tables.workProgram.${key}`)
        }
      }))
    }
  ];

  const hasChildren = diligence => !diligence.parent_id && diligence.children.length > 0;
  const isExpanded = diligence_id => diligences.expanded[diligence_id];
  const showModal = (diligence) => {
    if (diligence.periodicity !== 'PONC') {
      return togglePermanentDialog(diligence);
    }
    return toggleTimelyDialog(diligence);
  };
  const renderSelectValue = (diligence, value) => (
    <StatusString
      status={value}
      label={I18n.t(`dadp.status.${value}`)}
      disabled={diligence.na}
    />
  );
  const renderSelectList = option => (
    <StatusString status={option.value} label={option.label} />
  );

  const validCollabCell = (diligence) => {
    if (!diligence.parent_id) {
      if (diligence.valid_rm == null) {
        return {
          _type: '',
          keyCell: 'valid_collab'
        };
      }

      return {
        _type: 'string',
        keyCell: 'valid_collab',
        cellProp: {
          classes: { root: styles.cellValid }
        },
        props: {
          label: I18n.t(`dadp.status.${diligence.valid_collab}`),

          cellClassName: styles.status,
          labelClassName: styles[diligence.valid_collab]
        }
      };
    }

    return {
      _type: 'select',
      keyCell: 'valid_collab',
      cellProp: {
        classes: { root: styles.cellValid }
      },
      props: {
        list: status.filter(s => s.value !== 'to_validate'),
        value: diligence.valid_collab,
        renderValue: value => renderSelectValue(diligence, value),
        onChange: (event) => {
          if (event.target.value !== diligence.valid_collab) {
            updateDiligence(
              diligence.diligence_id,
              'valid_collab',
              event.target.value,
              diligence.valid_collab
            );
          }
        },
        renderList: renderSelectList,
        disabled: diligence.na
      }
    };
  };
  const validRMCell = (diligence) => {
    let cell = {
      keyCell: 'valid_rm'
    };

    if (!diligence.parent_id && diligence.valid_rm == null) {
      cell._type = '';
    } else {
      cell = {
        ...cell,
        _type: 'select',
        cellProp: {
          classes: { root: styles.cellValid }
        },
        props: {
          list: status,
          value: diligence.valid_rm,
          renderValue: value => renderSelectValue(diligence, value),
          onChange: (event) => {
            if (event.target.value !== diligence.valid_rm) {
              updateDiligence(
                diligence.diligence_id,
                'valid_rm',
                event.target.value,
                diligence.valid_rm
              );
            }
          },
          renderList: renderSelectList,
          disabled: diligence.na || diligence.valid_collab !== 'ok'
        }
      };
    }

    if (!diligence.parent_id) {
      cell.isParent = true;
      cell.allSame = _.every(
        diligence.children,
        child => child.valid_rm === diligence.children[0].valid_rm
      );
      cell.bulkUpdate = (validation_state) => {
        bulkUpdateDiligence(
          diligence.children.map(child => ({
            diligence_id: child.diligence_id,
            validation_type: 'valid_rm',
            validation_state
          }))
        );
      };
    }

    return cell;
  };

  const commentsCell = (diligence) => {
    const nb_comments = Array.isArray(diligence.comments)
      ? diligence.comments.length
      : diligence.comments;
    if (nb_comments === undefined) {
      return {
        _type: '',
        keyCell: 'comments',
        cellProp: {
          noFocus: true,
          className: styles.cellComment
        }
      };
    }

    return {
      keyCell: 'comments',
      component: IconButton,
      cellProp: {
        noFocus: true,
        className: styles.cellComment
      },
      props: {
        classes: { root: styles.btn },
        children: (
          <Badge
            color="primary"
            classes={{ badge: styles.badge }}
            badgeContent={nb_comments}
          >
            <FontIcon name="icon-comments" />
          </Badge>
        ),
        onClick: (e) => {
          e.stopPropagation();
          openCommentsDialog(diligence);
        }
      }
    };
  };
  const pjsCell = (diligence) => {
    if (diligence.pj_list === undefined) {
      return {
        _type: '',
        keyCell: 'pjs',
        cellProp: {
          noFocus: true,
          className: styles.cellPJ
        }
      };
    }

    return {
      keyCell: 'pjs',
      component: PJCell,
      cellProp: {
        noFocus: true,
        className: styles.cellPJ
      },
      props: {
        documents: diligence.pj_list,
        diligence_id: diligence.diligence_id,
        classes: { buttons: styles.btn, badge: styles.badge }
      }
    };
  };

  const toogleCheckbox = (diligence) => {
    if (selectedDiligence.some(item => item === diligence.ref_id)) {
      removeSelectedDiligence(diligence.ref_id);
    } else {
      addSelectedDiligence(diligence.ref_id);
    }
  };
  const diligencePaths = {
    'AC-G-01': routesByKey.fixed_assets,
    'AC-H-16': routesByKey.CAPPersonal,
    'AC-H-17': routesByKey.CAPCs,
    'AC-I-13': routesByKey.CAPIt,
    'AC-L-03': routesByKey.CAPDivers,
    'AC-B-09': routesByKey.CAPInt,
    'AC-C-07': routesByKey.FNP,
    'AC-E-10': routesByKey.FAE,
    'AC-E-12': routesByKey.PCA,
    'AC-C-08': routesByKey.AAR,
    'AC-E-11': routesByKey.AAE,
    'AC-D-08': routesByKey.cca
  };

  const formatDiligence = diligence => (!diligence.parent_id || isExpanded(diligence.parent_id))
  && {
    keyRow: diligence.parent_id
      ? `${diligence.parent_id},${diligence.diligence_id}`
      : diligence.diligence_id,
    props: {
      hover: true,
      onClick: () => {
        if (hasChildren(diligence)) {
          toggleDiligence(diligence.diligence_id);
        }
      }
    },
    value: [
      {
        _type: hasChildren(diligence) ? 'icon' : '',
        keyCell: 'expand',
        props: {
          name: isExpanded(diligence.diligence_id)
            ? 'icon-minus'
            : 'icon-plus',
          color: 'white',
          className: styles.expandBtn,
          size: 16
        }
      },
      {
        component: diligencePaths[diligence.parent_ref] ? Link : 'div', // 49 for immo
        keyCell: 'label',
        props: {
          onClick: () => (diligence.parent_ref ? showModal(diligence) : null
                  && diligencePaths[diligence.parent_ref]
                  && setRedirectLink({
                    ...diligence,
                    end_date: moment(end_date).format('YYYY-MM-D'),
                    start_date: moment(start_date).format('YYYY-MM-D'),
                    link: diligencePaths[diligence.parent_ref].replace(
                      ':id',
                      societyId
                    )
                  })),

          path: diligencePaths[diligence.parent_ref],
          children: diligence.name
        }
      },
      {
        _type: 'icon',
        keyCell: 'guides',
        props: {
          name: 'icon-account-info',
          onClick: () => console.log('Guide ! XD')
        }
      },
      pjsCell(diligence),
      commentsCell(diligence),
      {
        _type: diligence.parent_ref ? '' : 'checkbox',
        keyCell: 'na',
        cellProp: {
          classes: { root: styles.cellNA }
        },
        props: {
          checked: selectedDiligence.some(item => item === diligence.ref_id),
          onClick: event => event.stopPropagation(),
          onChange: () => { toogleCheckbox(diligence); }
        }
      },
      validCollabCell(diligence),
      validRMCell(diligence),
      {
        _type: 'string',
        keyCell: 'ref',
        props: {
          label: diligence.ref
        }
      }
    ]
  };

  const body = currentDiligences.map((diligence) => {
    const { children = [] } = diligence;

    const childrenDiligences = _.compact(children.map(formatDiligence));

    return [formatDiligence(diligence), ...childrenDiligences];
  });

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    headers,
    table: {
      param: {
        header: {
          props: {},
          row: headers
        },
        body: {
          row: _.flatten(body)
        }
      },
      isLoading,
      isError
    }
  };
};

// Enhance
const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  reduxForm({
    initialValues: {
      workSheetOnly: '0'
    }
  })
);

export default enhance(WorkProgram);
