import { compose } from 'redux';
import { connect } from 'react-redux';
import { reset, change, getFormValues } from 'redux-form';

import moment from 'moment';
import _ from 'lodash';

import I18n from 'assets/I18n';
import { accountingHeaderKeys } from 'assets/constants/keys';

import { accountingTypes } from 'assets/constants/data';

import { parseFilters } from 'helpers/accounting';
import { formatNumber } from 'helpers/number';
import { getSelectedMonth, getSelectedYear } from 'helpers/date';
import { getCurrentTabState } from 'helpers/tabs';
import tableActions from 'redux/tables/actions';

import { splitWrap } from 'context/SplitContext';

import {
  getEntries,
  deleteEntry as deleteEntryThunk,
  deleteEntryTemp as deleteEntryTempThunk,
  validateEntries as validateEntriesThunk,
  getEntryComments as getEntryCommentsThunk
} from 'redux/accounting';
import actions from 'redux/accounting/actions';
import { openDialog, closeDialog } from 'redux/dialogs';
import { setCurrentForm } from 'redux/tabs/form/actions';

import { AccountingTable } from 'components/groups/Tables';
import { ConfirmationDialog } from 'components/groups/Dialogs';

import { AccountingCommentBox } from 'containers/groups/Comments';

import styles from 'components/groups/Tables/Accounting/accountingTable.module.scss';

// Component
const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const { accounting } = getCurrentTabState(state);
  const isRedirected = new URLSearchParams(window.location.search).get('redirected');
  const filters = getFormValues(`${societyId}accountingFilter`)(state) || {};

  return {
    societyId,
    entries: state.accountingWeb.entries,
    selectedEntries: state.accountingWeb.selectedEntries,
    deleteConfirmation: state.accountingWeb.deleteConfirmation,
    isRowLoading: _.get(state, `accountingWeb.${societyId}.isCreateEntryLoading`, false),
    modifyEntryId: _.get(state, `accountingWeb.${societyId}.modifyEntryId`),
    isRedirected,
    filters,
    isLoadingFilter: _.isEmpty(accounting.default_filter),
    shouldReload: !_.isEqual(parseFilters(filters), _.get(state, `accountingWeb.entries[${societyId}].filters`, [])),
    accountingFilterType: _.get(state, `form.${societyId}accountingFilter.values.type`, ''),
    isInformationDialogOpen: state.accountingWeb.isInformationDialogOpen || false
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  getEntries: (opts) => {
    const { split } = ownProps;
    if (split.slave && split.master_id === 'consulting') {
      return dispatch(getEntries({ ...opts, entry_id: split.args.entry_id }));
    }

    return dispatch(getEntries(opts));
  },
  openCommentsDialog: dialogProps => dispatch(openDialog(dialogProps)),
  closeCommentsDialog: dialogProps => dispatch(closeDialog(dialogProps)),
  getEntryComments: id => dispatch(getEntryCommentsThunk(id)),
  deleteEntry: (entry_id, confirmation) => dispatch(deleteEntryThunk(entry_id, confirmation)),
  deleteEntryTemp: entry_id => dispatch(deleteEntryTempThunk(entry_id)),
  reset: form => dispatch(reset(form)),
  changeFormValue: (form, field, value) => dispatch(change(form, field, value)),
  addEntry: (entry, societyId) => dispatch(actions.addEntry(entry, societyId)),
  removeEntry: (entry, societyId) => dispatch(actions.removeEntry(entry, societyId)),
  resetAllEntries: societyId => dispatch(actions.resetAllEntries(societyId)),
  validateEntries: entries => dispatch(validateEntriesThunk(entries)),
  addAllEntries: societyId => dispatch(actions.addAllEntries(societyId)),
  openShouldDeleteEntries: (entries, societyId, entriesNames) => {
    dispatch(actions.openShouldDeleteEntries(entries, societyId, entriesNames));
  },
  closeShouldDeleteEntries: societyId => dispatch(actions.closeShouldDeleteEntries(societyId)),
  setAccountingTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  ),
  setCurrentForm: (formName, initial) => dispatch(setCurrentForm(formName, initial, ['day', 'month', 'year'])),
  setCurrentEntry: (currentEntry, society_id) => {
    dispatch(actions.setCurrentEntry(currentEntry, society_id));
  },
  setInformationDialog: isOpen => dispatch(actions.setInformationDialog(isOpen))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { split } = ownProps;
  const {
    filters,
    societyId,
    entries,
    selectedEntries,
    deleteConfirmation,
    accountingFilterType,
    modifyEntryId,
    isLoadingFilter
  } = stateProps;
  const {
    openShouldDeleteEntries,
    closeShouldDeleteEntries,
    getEntries,
    deleteEntry,
    reset,
    changeFormValue,
    addEntry,
    removeEntry,
    resetAllEntries,
    addAllEntries,
    validateEntries,
    setCurrentForm,
    openCommentsDialog,
    getEntryComments,
    deleteEntryTemp,
    setAccountingTableData,
    setCurrentEntry,
    setInformationDialog,
    closeCommentsDialog
  } = dispatchProps;

  /* TODO it works but should not be there,
      the container should not know about view (and also the parent one !)
   The buttons structure should not be there too.
   There should not have a container per component for performances and readability.
  */

  const getEntriesAndReset = (...params) => {
    resetAllEntries(societyId);
    getEntries(...params);
  };

  const entryArrayHeader = document.getElementById(`${societyId}newAccounting`);

  const onPopoverExited = () => {
    // PATCH TODO should not be defined there and also hacked with raf
    requestAnimationFrame(() => {
      entryArrayHeader.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    });
  };

  const getAndOpenComments = async (entryId) => {
    if (entryId !== undefined) {
      openCommentsDialog({
        body: {
          Component: AccountingCommentBox,
          props: {
            entryId,
            createMode: false,
            getComments: () => getEntryComments(entryId)
          }
        }
      });
    } else {
      openCommentsDialog({
        body: {
          Component: AccountingCommentBox,
          props: {
            entryId,
            createMode: true
          }
        }
      });
    }
  };

  const currentAccounting = entries[societyId] || {};
  const currentSelectedEntries = _.get(selectedEntries, societyId, []);
  const { isLoading = true, isError } = currentAccounting;
  const currentEntries = _.get(currentAccounting, 'entry_array', []);
  const currentEntryId = _.get(currentEntries, '[0].entry_id', 0);
  const formName = `${societyId}newAccounting`;

  const headers = [{
    keyRow: 'header',
    value: accountingHeaderKeys.reduce((acc, data) => {
      const {
        type,
        keyLabel,
        isAmount,
        paddingLeft
      } = data;

      const isAllSelected = currentEntries.every(
        entry => currentSelectedEntries.includes(entry.entry_id)
      );

      const oppositeSort = currentAccounting.direction === 'asc' ? 'desc' : 'asc';
      if (keyLabel === 'date_accounting' && filters.type !== 'e') return acc;

      if (type === 'checkbox') {
        acc.push({
          _type: 'checkbox',
          keyCell: keyLabel,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllEntries(societyId);
              } else {
                addAllEntries(societyId);
              }
            }
          }
        });
      } else {
        const sorted = currentAccounting.sort === keyLabel;
        acc.push({
          _type: 'sortCell',
          keyCell: keyLabel,
          cellProp: {
            paddingLeft,
            isAmount
          },
          props: {
            children: I18n.t(`tables.accounting.${keyLabel}`),
            direction: sorted ? currentAccounting.direction : '',
            onClick: () => getEntries({
              page: 0,
              sort: keyLabel,
              direction: sorted ? oppositeSort : 'desc'
            })
          }
        });
      }
      return acc;
    }, [])
  }];

  const headersOnClickActions = [{
    label: I18n.t('tables.accounting.rightClick.reset'),
    onClick: () => {
      getEntries({ page: 0, sort: 'diary', direction: 'asc' });
    }
  }];

  const getEntry = (row) => {
    const entryId = parseInt(_.get(row, 'value[0].props.id', row), 10);
    return entries[societyId].entry_array.find(e => e.entry_id === entryId);
  };

  const duplicate = (row, type, mode = 'new') => {
    if (row) {
      const entry = getEntry(row);

      const lastEntryDate = _.get(entry, 'date_piece', '');
      const newEntries = entry.entry_list.map((e) => {
        const counterpart_account = _.get(e.account, 'counterpart_account') && {
          ..._.get(e.account, 'counterpart_account'),
          account_id: _.get(e.account, 'counterpart_account.account_id_contrepart', ''),
          account_number: _.get(e.account, 'counterpart_account.account_number_contrepart', ''),
          label: _.get(e.account, 'counterpart_account.label_counterpart', ''),
          value: _.get(e.account, 'counterpart_account.label_counterpart', '')
        };

        const payment_type = {
          ...e.payment_type,
          value: _.get(e, 'payment_type.payment_type_id'),
          label: _.get(e, 'payment_type.name')
        };

        return ({
          ...e,
          account: {
            ...e.account,
            value: _.get(e.account, 'account_number'),
            counterpart_account
          },
          label: e.label,
          deadline: e.deadline,
          debit: e.debit && formatNumber(e.debit),
          credit: e.credit && formatNumber(e.credit),
          payment_type,
          piece: e.piece,
          piece2: e.piece2,
          worksheet: e.flags
        });
      });
      changeFormValue(formName, 'entry_list', newEntries);
      changeFormValue(formName, 'pj_list', entry.pj_list);
      if (mode !== 'duplicate') changeFormValue(formName, 'comment', entry.comment);
      changeFormValue(formName, 'diary', entry.diary);
      changeFormValue(formName, 'day', moment(entry.date_piece, 'YYYYMMDD').format('DD'));
      changeFormValue(formName, 'month', getSelectedMonth(lastEntryDate));
      changeFormValue(formName, 'year', getSelectedYear(lastEntryDate));
      changeFormValue(formName, 'type', type);

      if (mode === 'modify') {
        changeFormValue(formName, 'entry_id', entry.entry_id);

        const data = {
          entry_id: entry.entry_id,
          entry_list: newEntries,
          pj_list: entry.pj_list,
          diary: entry.diary,
          type
        };

        if (entry.comment) {
          data.comment = entry.comment;
        }

        setCurrentEntry(data, societyId);
        setCurrentForm(formName, data);
      } else {
        setCurrentEntry({}, societyId);
        setCurrentForm(formName, {}); // duplicate => new entry which need to be saved !
      }
    }
  };
  const body = currentEntries.map(entry => entry.entry_list.map((e, j) => {
    const totalPjsEntry = _.get(entry, 'pj_list.length', 0) + _.get(entry, 'attachments.length', 0);
    const totalPjsRow = _.get(e, 'pj_list.length', 0);
    const pjStatusCellData = [];
    const debit = _.get(e, 'debit', false);
    const credit = _.get(e, 'credit', false);
    const worksheet_flag = _.get(e, 'flags.worksheet', null);
    const immo_flag = _.get(e, 'flags.immo', null);
    const flag = !!((worksheet_flag || immo_flag));
    const info = entry.doublon;
    const isCurrentEntryBeeingModified = modifyEntryId === entry.entry_id;
    let comment;
    if (entry.comment) {
      comment = {
        _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
        keyCell: 'comment',
        isComment: true,
        props: {
          iconRight: 'icon-comments',
          comment: entry.comment || '',
          onClick: () => getAndOpenComments(entry.entry_id)
        }
      };
    } else {
      comment = {
        _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
        keyCell: 'comment',
        props: {
          label: ''
        }
      };
    }

    if (totalPjsEntry > 0 && j === 0) {
      pjStatusCellData.push({ status: 'entry_pjs', value: totalPjsEntry, pjs: [..._.get(entry, 'pj_list', []), ..._.get(entry, 'attachments', [])] });
    }
    if (totalPjsRow > 0) {
      pjStatusCellData.push({ status: 'pjs', value: totalPjsRow, pjs: [..._.get(e, 'pj_list', [])] });
    }

    return {
      keyRow: `${entry.entry_id}-${e.line_entry_id}`,
      props: {
        selected: currentSelectedEntries.includes(entry.entry_id),
        hover: true,
        className: (j === (entry.entry_list.length - 1)) ? styles.endEntry : undefined,
        blocked: entry.blocked,
        onDoubleClick: () => {
          if (!entry.blocked) {
            reset(formName);
            duplicate(entry.entry_id, filters.type, 'modify');
            if (split.active) {
              split.updateArgsSplit({
                showNewAccounting: true
              });
              // TODO fix later this ugly trick...
              setTimeout(() => {
                document.getElementById(`${societyId}newAccounting`).scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
              }, 0);
            }
          } else {
            setInformationDialog(true);
          }
          return false;
        }
      },
      value: [
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'checkbox',
          keyCell: 'checkbox',
          props: {
            checked: currentSelectedEntries.includes(entry.entry_id),
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (currentSelectedEntries.includes(entry.entry_id)) {
                removeEntry(entry, societyId);
              } else {
                addEntry(entry, societyId);
              }
            },
            id: entry.entry_id.toString()
          }
        },
        {
          _type: 'string',
          keyCell: 'flag',
          isFlag: true,
          flagInfo: immo_flag ? [{ ...immo_flag, worksheet_type_code: 'Immo' }] : worksheet_flag,
          props: {
            iconLeft: flag ? 'icon-Flag' : null,
            label: flag !== 0 ? flag : null,
            cellClassName: styles.flag,
            labelClassName: styles.label,
            iconLeftClassName: styles.icon
          }
        },
        {
          _type: 'string',
          keyCell: 'info',
          isInfo: true,
          info: entry.alerte,
          props: {
            iconLeft: info ? 'icon-alert' : null,
            cellClassName: styles.alert,
            iconLeftClassName: styles.icon
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'diary',
          props: {
            label: _.get(entry, 'diary.code', '')
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'date_piece',
          props: {
            label: moment(entry.date_piece, 'YYYYMMDD').format('DD/MM/YYYY')
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'account_number',
          account: _.get(e, 'account.account_number', ''),
          props: {
            label: _.get(e, 'account.account_number', '')
          },
          cellProp: {
            onClick: () => {
              if (split.master) {
                split.updateArgsSplit({
                  account: e.account
                });
              }
            }
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'label',
          props: {
            label: _.get(e, 'label', ''),
            labelWidth: '150px'
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'piece',
          props: {
            label: _.get(e, 'piece', '')
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'piece2',
          props: {
            label: _.get(e, 'piece2', '')
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'debit',
          props: {
            label: debit && formatNumber(debit),
            style: { textAlign: 'right' }
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'credit',
          props: {
            label: credit && formatNumber(credit),
            style: { textAlign: 'right' },
            paddingLeft: true
          }
        },
        comment,
        {
          _type: isCurrentEntryBeeingModified //eslint-disable-line
            ? 'loadingCell'
            : (pjStatusCellData.length > 0 ? 'status' : 'string'),
          keyCell: 'pjs',
          props: {
            data: pjStatusCellData
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'counterpart',
          props: {
            label: _.get(e, 'account.counterpart_account.account_number_contrepart', '')
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'deadline',
          props: {
            label: e.deadline && moment(e.deadline, 'YYYY-MM-DD').format('DD/MM/YYYY')
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'entry_date',
          props: {
            label: moment(entry.entry_date, 'YYYYMMDD').format('DD/MM/YYYY')
          }
        },
        {
          _type: isCurrentEntryBeeingModified ? 'loadingCell' : 'string',
          keyCell: 'payment_type',
          props: {
            label: _.get(e, 'payment_type.name', '')
          }
        }
      ].filter(cell => filters.type === 'e' || cell.keyCell !== 'entry_date')
    };
  }));

  const bodyOnRightClickActions = [
    {
      label: I18n.t('tables.accounting.rightClick.delete'),
      disabled: true,
      onClick: async (row) => {
        let entries = [];
        let entriesName = [];
        if (_.get(currentSelectedEntries, 'length', 0) === 0) {
          const entry = getEntry(row);
          entries = [entry.entry_id];
          entriesName = [entry.entry_list[0].label];
        } else {
          entries = currentSelectedEntries;
          entriesName = currentEntries.reduce((acc, entry) => {
            if (currentSelectedEntries.some(key => entry.entry_id === key)) {
              acc.push(_.get(entry, 'entry_list[0].label', ''));
            }
            return acc;
          }, []);
        }

        openShouldDeleteEntries(entries, societyId, entriesName);
      }
    }
  ];

  const currentDeleteConfirmation = _.get(deleteConfirmation, societyId, { isOpen: false });
  const deleteEntriesSize = _.get(currentDeleteConfirmation, 'entries.length', 0);
  const deleteEntriesNames = _.get(currentDeleteConfirmation, 'entriesName', []);
  const confirmation = {
    ...currentDeleteConfirmation,
    title: I18n.t('tables.accounting.deleteConfirmationTitle'),
    message: currentDeleteConfirmation.canValidate ? `${I18n.t('tables.accounting.deleteConfirmation', { count: deleteEntriesSize })} : ${deleteEntriesNames}` : I18n.t('tables.accounting.flagDelete'),
    onValidate: async () => {
      if (filters.type === 'o' || filters.type === 'ib' || filters.type === 'p' || filters.type === 'ext') {
        await deleteEntryTemp(currentDeleteConfirmation.entries);
      } else {
        // Explanation: if the case the entry is lettered, the api returns an error message
        // and a dialog appears to confirm the delete of the entry or not.
        const requestAnswer = await deleteEntry(currentDeleteConfirmation.entries);
        if (requestAnswer.response && requestAnswer.response.data.message === I18n.t('tables.accounting.letteredEntry_errorMessage')) {
          openCommentsDialog({
            body: {
              Component: ConfirmationDialog,
              props: {
                isOpen: true,
                title: I18n.t('tables.accounting.deleteConfirmationTitle_lettered'),
                message: I18n.t('tables.accounting.deleteConfirmation_lettered'),
                onValidate: async () => {
                  await deleteEntry(currentDeleteConfirmation.entries, { confirmed: true });
                  closeCommentsDialog();
                },
                onClose: closeCommentsDialog
              }
            }
          });
        }
      }
      closeShouldDeleteEntries(societyId);
    },

    onClose: () => {
      closeShouldDeleteEntries(societyId);
    }
  };

  if (_.get(currentSelectedEntries, 'length', 0) <= 1 && !split.active) {
    bodyOnRightClickActions.unshift(
      {
        label: I18n.t('tables.accounting.rightClick.modify'),
        disabled: true,
        onClick: (row) => {
          const entry_id = parseInt(_.get(row, 'value[0].props.id'), 10);
          reset(formName);
          duplicate(entry_id, filters.type, 'modify');
          onPopoverExited();
        }
      }
    );
  }
  // It's impossible duplication ocr/ir/flux
  if (filters.type === 'e' && !split.active) {
    bodyOnRightClickActions.unshift(
      {
        label: I18n.t('tables.accounting.rightClick.duplicate'),
        onClick: (row) => {
          const entry_id = parseInt(_.get(row, 'value[0].props.id'), 10);
          reset(formName);
          duplicate(entry_id, 'e', 'duplicate');
          onPopoverExited();
        }
      }
    );
  }

  if (filters.type === 'o' || filters.type === 'ib' || filters.type === 'p' || filters.type === 'ext') {
    bodyOnRightClickActions.unshift(
      {
        label: I18n.t('tables.accounting.rightClick.validate'),
        onClick: (row) => {
          let flux = [];
          if (_.get(currentSelectedEntries, 'length', 0) === 0) {
            const entry = getEntry(row);
            flux = {
              flux_ocr: [{
                id: entry.entry_id
              }]
            };
          } else {
            flux = { flux_ocr: currentSelectedEntries.map(id => ({ id })) };
          }
          try {
            const response = validateEntries(flux);
            resetAllEntries(societyId);
            return response;
          } catch (err) {
            console.log(err); //eslint-disable-line
            return err;
          }
        }
      }
    );
  }

  const footer = [
    {
      keyRow: `${societyId}accountingTableTotalFooterRowCell`,
      value: [
        {
          _type: 'string',
          keyCell: 'total_label',
          cellProp: {
            colSpan: 10
          },
          props: {
            label: I18n.t('tables.accounting.total'),
            typoVariant: 'h6',
            style: { textAlign: 'right', paddingRight: 8 }
          }
        },
        {
          _type: 'string',
          keyCell: 'total_debit',
          cellProp: {
            isNumber: true
          },
          props: {
            label: formatNumber(_.get(currentAccounting, 'debit_total', 0), '0,0.00 $'),
            typoVariant: 'h6',
            noWrap: true,
            style: {
              border: '1px solid #0bd1d1'
            }
          }
        },
        {
          _type: 'string',
          keyCell: 'total_credit',
          cellProp: {
            isNumber: true
          },
          props: {
            label: formatNumber(_.get(currentAccounting, 'credit_total', 0), '0,0.00 $'),
            typoVariant: 'h6',
            noWrap: true,
            style: {
              border: '1px solid #0bd1d1'
            }
          }
        },
        {
          _type: 'string',
          keyCell: `${societyId}accountingTableTotalFooterCellAfterTot`,
          cellProp: {
            colSpan: 6
          }
        }
      ]
    }
  ];

  const noRightClick = split.slave && split.master_id === 'consulting';
  const mergedParams = {
    header: {
      rightClickActions: noRightClick ? undefined : headersOnClickActions,
      props: {},
      row: headers
    },
    body: {
      rightClickActions: noRightClick ? undefined : bodyOnRightClickActions,
      props: {},
      row: _.flatten(body)
    },
    footer: {
      props: {},
      row: footer
    }
  };

  const activeType = accountingTypes.find(type => type.value === accountingFilterType);
  const activeTypeName = _.get(activeType, 'label', '');

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isLoading: isLoading || (isLoadingFilter && currentEntries.length === 0 && !(split.slave && split.master_id === 'consulting')),
    isError,
    currentEntries,
    currentEntryId,
    getEntries: getEntriesAndReset,
    setTableData: params => setAccountingTableData(activeTypeName, societyId, params),
    confirmation,
    duplicate,
    getAndOpenComments,
    param: { ...mergedParams }
  };
};

// Enhance
const enhance = compose(
  splitWrap,
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(AccountingTable);
