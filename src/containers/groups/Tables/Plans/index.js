import { TableFactory } from 'components/groups/Tables';
import { connect } from 'react-redux';
import { getAccountingPlans as getAccountingPlansThunk } from 'redux/accountingPlans';
import { getPlanDetails as getPlanDetailsThunk } from 'redux/planDetails';
import actions from 'redux/accountingPlans/actions';

import _ from 'lodash';
import { accountingPlansHeaderKeys } from 'assets/constants/keys';
import { CheckBoxCell, SortCell } from 'components/basics/Cells';
import I18n from 'assets/I18n';
import tableActions from 'redux/tables/actions';

const mapStateToProps = state => ({
  plans: _.get(state, 'accountingPlans.plans', {}),
  selectedPlans: _.get(state, 'accountingPlans.selectedPlans', []),
  selectedPlan: state.accountingPlans.selectedPlan,
  societyId: state.navigation.id
});

const mapDispatchToProps = dispatch => ({
  getAccountingPlans: options => dispatch(getAccountingPlansThunk(options)),
  getPlanDetails: () => dispatch(getPlanDetailsThunk()),
  selectPlan: (selectedPlan, selectedLine) => {
    dispatch(actions.selectPlan(selectedPlan, selectedLine));
  },
  addPlan: planId => dispatch(actions.addPlan(planId)),
  removePlan: planId => dispatch(actions.removePlan(planId)),
  addAllPlans: () => dispatch(actions.addAllPlans()),
  resetAllPlans: () => dispatch(actions.resetAllPlans()),
  setPansEtalonTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});

const mergeProps = (stateProps, dispatchProps) => {
  const {
    plans,
    selectedPlan,
    selectedPlans,
    societyId
  } = stateProps;
  const {
    getAccountingPlans,
    getPlanDetails,
    selectPlan,
    addPlan,
    removePlan,
    addAllPlans,
    resetAllPlans,
    setPansEtalonTableData
  } = dispatchProps;
  const loadData = async () => {
    await getAccountingPlans();
  };
  const plansList = _.get(plans, 'list', []);

  const sort = _.get(plans, 'sort', {});

  let isAllSelected = plansList.length > 0;
  plansList.forEach((plan) => {
    if (!selectedPlans.includes(plan.id)) {
      isAllSelected = false;
    }
  });
  const headers = [{
    keyRow: 'plansHeaderRow',
    value: accountingPlansHeaderKeys.map((data, i) => {
      const {
        type,
        keyLabel,
        isAmount
      } = data;
      let cell;

      const direction = _.get(sort, 'column') === keyLabel ? sort.direction : 'desc';

      if (type === 'checkbox') {
        cell = ({
          component: CheckBoxCell,
          _type: 'checkbox',
          keyCell: `accountingPlansHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllPlans();
              } else {
                addAllPlans();
              }
            }
          }
        });
      } else {
        cell = {
          component: SortCell,
          keyCell: `accountingPlansCell.${keyLabel}${i}`,
          cellProp: {
            isAmount
          },
          props: {
            children: I18n.t(`accountingPlans.tables.plans.column.${keyLabel}`),
            colorErrorIcon: true,
            direction,
            onClick: () => getAccountingPlans(
              {
                sort: {
                  column: `${keyLabel}`,
                  direction: sort.direction === 'asc' ? 'desc' : 'asc'
                }
              }
            )
          }
        };
      }
      return cell;
    })
  }];
  const body = plansList.map((plan, i) => ({
    keyRow: `accountingPlanLink${i}`,
    props: {
      hover: true,
      onClick: async () => {
        if (plan.id === selectedPlan) {
          await selectPlan(null, null);
        } else {
          await selectPlan(plan.id, i);
          await getPlanDetails();
        }
      }
    },
    value: [
      {
        component: CheckBoxCell,
        _type: 'checkbox',
        keyCell: `accountingPlanBox${i}`,
        props: {
          id: i,
          checked: selectedPlans.includes(plan.id),
          onClick: (e) => {
            e.stopPropagation();
          },
          onChange: () => {
            if (selectedPlans.includes(plan.id)) {
              if (plan.id === selectedPlan) selectPlan(null, null);
              removePlan(plan.id);
            } else {
              selectPlan(plan.id, i);
              if (plan.id !== selectedPlan) getPlanDetails();
              addPlan(plan.id);
            }
          }
        },
        cellProp: {
          style: {
            width: '44px'
          }
        }
      },
      {
        component: 'div',
        keyCell: `accountingPlanLabel${i}`,
        props: {
          children: _.get(plan, 'label', '')
        }
      },
      {
        component: 'div',
        keyCell: `accountingPlanDescription${i}`,
        props: {
          children: _.get(plan, 'description', '')
        }
      }
    ]
  }));

  const tableTransformedParams = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  const tableName = I18n.t('accountingPlans.title');

  return {
    ...stateProps,
    ...dispatchProps,
    loadData,
    isLoading: plans.isLoading,
    isError: plans.isError,
    param: tableTransformedParams,
    onRefresh: () => setPansEtalonTableData(tableName, societyId, tableTransformedParams),
    cellHeight: 38,
    maxVisibleItems: 10
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TableFactory);
