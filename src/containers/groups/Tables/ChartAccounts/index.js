import { connect } from 'react-redux';
import { getFormValues, change } from 'redux-form';
import { get as _get, isEmpty, flatten } from 'lodash';

import I18n from 'assets/I18n';
import { accountListHeaderKeys } from 'assets/constants/keys';

import { formatNumber } from 'helpers/number';

import {
  getChartAccount as getChartAccountThunk,
  deleteAccount as deleteAccountThunk
} from 'redux/chartAccounts';
import { openDialog } from 'redux/dialogs';
import actions from 'redux/chartAccounts/actions';
import tableActions from 'redux/tables/actions';

import FactoryTable from 'components/groups/Tables/Factory';

import EditVAT from 'containers/groups/Dialogs/Account/EditVAT';

const mapStateToProps = state => ({
  society_id: state.navigation.id,
  accountList: state.chartAccountsWeb.accountList,
  isLoading: state.chartAccountsWeb.chartAccountIsLoading,
  formValues: getFormValues('accountingForm')(state),
  selectedLine: state.chartAccountsWeb.accountSelected,
  selectedAccount: _get(state, `chartAccountsWeb.selectedAccount[${state.navigation.id}]`, [])
});

const mapDispatchToProps = dispatch => ({
  getChartAccount: society_id => dispatch(getChartAccountThunk(society_id)),
  changeFormValue: (field, value) => dispatch(change('accountingForm', field, value)),
  selectAccount: accountSelected => dispatch(actions.selectAccount(accountSelected)),
  deleteAccount: account_id => dispatch(deleteAccountThunk(account_id)),
  addAccount: (account_id, society_id) => dispatch(actions.addAccount(account_id, society_id)),
  removeAccount: (
    account_id,
    society_id
  ) => dispatch(actions.removeAccount(account_id, society_id)),
  resetAllAccounts: society_id => dispatch(actions.resetAllAccounts(society_id)),
  addAllAccounts: society_id => dispatch(actions.addAllAccounts(society_id)),
  setChartTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  ),
  openDialog: dialogProps => dispatch(openDialog(dialogProps))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    getChartAccount,
    selectAccount,
    addAccount,
    removeAccount,
    resetAllAccounts,
    addAllAccounts,
    setChartTableData,

    openDialog
  } = dispatchProps;

  const {
    accountList,
    society_id,
    selectedAccount,
    selectedLine
  } = stateProps;

  const openVATDialog = () => {
    openDialog({
      body: {
        Component: EditVAT,
        props: {
          selectedAccount,
          isOpen: true
        }
      }
    });
  };

  const loadData = async () => {
    await getChartAccount(society_id);
  };

  const currentAccountList = accountList[society_id] || {};
  const { isLoading, isError } = currentAccountList;
  const currentAccounts = _get(currentAccountList, 'account_array', []);
  const sort = _get(currentAccountList, 'sort', {});

  const accountAvailable = currentAccounts.filter(account => !account.blocked);

  let isAllSelected = accountAvailable.length > 0;

  accountAvailable.forEach((account) => {
    if (!selectedAccount.includes(account.account_id)) {
      isAllSelected = false;
    }
  });

  const headerKeys = accountListHeaderKeys;

  const headers = [{
    keyRow: `${society_id}accountingTableHeaderRow0`,
    value: headerKeys.map((data, key, i) => {
      const { type, keyLabel } = data;
      const direction = _get(sort, 'column') === keyLabel ? sort.direction : 'desc';
      let cell;

      if (type === 'checkbox') {
        cell = ({
          _type: type,
          keyCell: `${society_id}accountingTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            disabled: accountAvailable.length === 0,
            id: `${society_id}accountingTableSelectAllCheckbox`,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllAccounts(society_id);
              } else {
                addAllAccounts(society_id);
              }
            }
          },
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 1
            }
          }
        });
      } else {
        cell = {
          _type: 'sortCell',
          keyCell: `${society_id}accountingTableHeaderCell.${keyLabel}${i}`,
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 1
            }
          },
          props: {
            children: I18n.t(`ChartAccount.array.column.${keyLabel}`),
            direction,
            onClick: () => getChartAccount(
              {
                sort: {
                  column: `${keyLabel}`,
                  direction: sort.direction === 'asc' ? 'desc' : 'asc'
                }
              }
            )
          }
        };
      }
      return cell;
    })
  }];


  const body = currentAccounts.map((account, index) => ({
    keyRow: `${society_id}accountListTableBodyRowCell${index}`,
    props: {
      style: { backgroundColor: account.account_id === selectedLine ? '#f4f4f4' : '#fff' },
      onClick: () => selectAccount(account.account_id === selectedLine ? null : account.account_id)
    },
    value: [
      {
        _type: 'checkbox',
        keyCell: `${society_id}accountListTableBodyCellCheckBox${index}`,
        props: {
          name: `${society_id}AccountListId${account.account_id}`,
          id: account.account_id.toString(),
          checked: selectedAccount.includes(account.account_id),
          disabled: account.blocked,
          onClick: (e) => {
            e.stopPropagation();
          },
          onChange: () => {
            if (selectedAccount.includes(account.account_id)) {
              if (account.account_id === selectedLine) selectAccount(null);
              removeAccount(account, society_id);
            } else {
              selectAccount(account.account_id);
              addAccount(account, society_id);
            }
          }
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellAccountNumber${index}`,
        props: {
          label: _get(account, 'account_number', '')
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellLabel${index}`,
        props: {
          label: _get(account, 'label', '')
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellSolde${index}`,
        props: {
          label: account.solde !== 0 && account.solde !== null && formatNumber(account.solde)
        },
        cellProp: {
          style: { textAlign: 'right' }
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellSens${index}`,
        props: {
          label: _get(account, 'sens', '')
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellCounterPart${index}`,
        props: {
          label: _get(account, 'counterpart_account.account_number', '')
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellVat${index}`,
        cellProp: {
          onContextMenu: () => !isEmpty(selectedAccount) && openVATDialog()
        },
        props: {
          label: _get(account, 'vat_param.code', '')
        }
      },
      {
        _type: 'checkbox',
        keyCell: `${society_id}accountListTableBodyCellIntraco${index}`,
        props: {
          checked: _get(account, 'intraco', ''),
          id: `${society_id}CheckBoxIntraco${index}`,
          disabled: true
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellPresta${index}`,
        cellProp: {
          style: { backgroundColor: '#d0d0d0' }
        },
        props: {
          label: ''
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellDas2${index}`,
        cellProp: {
          style: { backgroundColor: '#d0d0d0' }
        },
        props: {
          label: ''
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellAnaliticSection${index}`,
        props: {
          label: account.analytic_section !== 0
            && account.analytic_section !== null
            && formatNumber(account.analytic_section)
        }
      },
      {
        _type: 'string',
        keyCell: `${society_id}accountListTableBodyCellAnaliticImputation${index}`,
        props: {
          label: account.analytic_imputation !== 0
            && account.analytic_imputation !== null
            && formatNumber(account.analytic_imputation)
        }
      }
    ]
  }));

  const tableName = I18n.t('ChartAccount.title');
  const transformedData = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: flatten(body)
    }
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isLoading,
    isError,
    loadData,
    param: transformedData,
    onRefresh: () => setChartTableData(tableName, society_id, transformedData),
    cellHeight: 31,
    maxVisibleItems: 10
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FactoryTable);
