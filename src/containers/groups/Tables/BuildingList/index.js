import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import FactoryTable from 'components/groups/Tables/Factory';
import { buildingRefKeys } from 'assets/constants/keys';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const society_id = 1;

  const headerKeys = buildingRefKeys;

  const headers = [{
    keyRow: `${society_id}buildingsTableHeaderRow0`,
    value: headerKeys.map((data, key, i) => {
      const { type, keyLabel } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          _type: type,
          keyCell: `${society_id}subsidiariesTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: false,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {}
          },
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 1
            }
          }
        });
      } else {
        cell = {
          _type: 'sortCell',
          keyCell: `${society_id}buildingsTableHeaderCell.${keyLabel}${i}`,
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 1
            }
          },
          props: {
            children: I18n.t(`companyCreation.buildings.buildingArrayTitle.${keyLabel}`),
            onClick: () => {}
          }
        };
      }
      return cell;
    })
  }];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    param: {
      header: {
        props: {},
        row: headers
      }
    }
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FactoryTable);
