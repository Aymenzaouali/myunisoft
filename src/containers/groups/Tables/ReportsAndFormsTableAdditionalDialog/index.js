import { connect } from 'react-redux';
import { ReportsAndFormsTableAdditionalDialog } from 'components/groups/Tables/';
import { reportsAndFormsOrder } from 'assets/constants/keys';
import ReportsAndFormsService from 'redux/reportsAndForms';
import _ from 'lodash';
import I18n from 'assets/I18n';

const mapStateToProps = state => ({
  society_id: state.navigation.id,
  statementSelectedLineAdditional: state.reportsAndForms.statementSelectedLineAdditional,
  formAndReportsAdditional: state.reportsAndForms.formAndReportsAdditional
});

const mapDispatchToProps = dispatch => ({
  getFormsAndReportsAdditional: () => dispatch(
    ReportsAndFormsService.getFormsAndReportsAdditional()
  )
});

const mergeProps = (stateProps, dispatchProps) => {
  const {
    society_id,
    formAndReportsAdditional
  } = stateProps;
  const {
    getFormsAndReportsAdditional
  } = dispatchProps;

  const currentStatements = formAndReportsAdditional[society_id] || {};

  const { isLoading, isError } = currentStatements;

  const formAndReportsAdditional_list = _.get(currentStatements, 'formAndReportsAdditional_list', []);

  const headers = [{
    keyRow: 'settingsHeaderRow',
    value: reportsAndFormsOrder.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `reportsAndFormsCell${keyLabel}${i}`,
          props: {
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
            },
            id: keyLabel.toString()
          },
          cellProp: {
            style: {
              maxWidth: '44px'
            }
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `settingsHeaderCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`reportsAndForms.orderTable.${keyLabel}`)
        }
      };
    })

  }];

  const bodyRow = formAndReportsAdditional_list.map((statement, i) => {
    const generateKey = (cellName = '') => `reportsAndFormsTableAdditionalDialog${cellName}${statement.id}`;
    return {
      keyRow: generateKey(),
      props: {
        hover: true
      },
      value: [
        {
          _type: 'checkbox',
          keyCell: generateKey('reportsAndFormsTableAdditionalDialogCheckBox'),
          props: {
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
            },
            id: i.toString()
          },
          cellProp: {
            style: {
              width: '44px'
            }
          }
        },
        {
          _type: 'string',
          keyCell: generateKey('orderNumber'),
          props: {
            label: _.get(statement, 'orderNumber', '')
          },
          cellProp: {
            style: {
              width: '220px'
            }
          }
        },
        {
          _type: 'string',
          keyCell: generateKey('codeEDI'),
          props: {
            label: _.get(statement, 'codeEDI', '')
          },
          cellProp: {
            style: {
              width: '110px'
            }
          }
        },
        {
          _type: 'string',
          keyCell: generateKey('wording'),
          props: {
            label: _.get(statement, 'wording', '')
          },
          cellProp: {
            style: {
              width: '170px'
            }
          }
        },
        {
          _type: 'string',
          keyCell: generateKey('dataType'),
          props: {
            label: _.get(statement, 'dataType', '')
          },
          cellProp: {
            style: {
              width: '170px'
            }
          }
        },
        {
          _type: 'string',
          keyCell: generateKey('viewing'),
          props: {
            label: _.get(statement, 'viewing', '')
          },
          cellProp: {
            style: {
              width: '170px'
            }
          }
        },
        {
          _type: 'string',
          keyCell: generateKey('dataSource'),
          props: {
            label: _.get(statement, 'dataSource', '')
          },
          cellProp: {
            style: {
              width: '170px'
            }
          }
        },
        {
          _type: 'string',
          keyCell: generateKey('references'),
          props: {
            label: _.get(statement, 'references', '')
          },
          cellProp: {
            style: {
              width: '170px'
            }
          }
        }
      ]
    };
  });

  return {
    loadData: getFormsAndReportsAdditional,
    isLoading,
    isError,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ReportsAndFormsTableAdditionalDialog);
