import { connect } from 'react-redux';
import {
  getUserList as getUserListThunk,
  getUser as getUserThunk
} from 'redux/tabs/user';
import { setDeletionDialog } from 'redux/tabs/crm/actions';
import TableFactory from 'components/groups/Tables/Factory';
import I18n from 'assets/I18n';
import _ from 'lodash';
import { userListTranslationKeys } from 'assets/constants/keys';
import { getCurrentTabState } from 'helpers/tabs';
import tableActions from 'redux/tables/actions';

const mapStateToProps = state => ({
  userList: getCurrentTabState(state).user || {},
  user: state.login.user,
  societyId: state.navigation.id,
  isUserAdmin: state.login.user.isadmin
});

const mapDispatchToProps = dispatch => ({
  getUserList: payload => dispatch(getUserListThunk(payload)),
  getUser: id => dispatch(getUserThunk(id)),
  setPansUserListTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  ),
  setDeletionDialog: (mode, id) => dispatch(setDeletionDialog(mode, id))
});
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    userList, user, societyId, isUserAdmin
  } = stateProps;
  const {
    getUserList, getUser, setPansUserListTableData, setDeletionDialog
  } = dispatchProps;

  const loadData = async () => {
    await getUserList();
  };

  const {
    usersData, isLoading, isError
  } = userList;
  const currentUsers = usersData && usersData.user_array ? usersData.user_array : [];

  const userHeaders = userListTranslationKeys.filter((key) => {
    if (key === 'libelle_type_profil' && user.isadmin) {
      return false;
    } return key;
  });

  const headers = [{
    keyRow: 'userListHeaderRow',
    value: userHeaders.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      const {
        sort: {
          column,
          direction: oldDirection
        }
      } = userList;

      const defaultUserDirection = 'desc';
      const selectedUserDirection = oldDirection === 'asc' ? 'desc' : 'asc';
      const direction = column === keyLabel
        ? selectedUserDirection
        : defaultUserDirection;

      const sort = {
        column: `${keyLabel}`,
        direction
      };
      if (type === 'string') {
        return {
          _type: 'string',
          keyCell: `userListHeaderCell.${keyLabel}${i}`,
          props: {
            id: keyLabel
          }
        };
      }

      return {
        _type: 'sortCell',
        keyCell: `userListHeaderCell.${keyLabel}${i}`,
        props: {
          children: I18n.t(`tables.userList.${keyLabel}`),
          direction,
          onClick: () => getUserList({ sort })
        }
      };
    })
  }];

  const body = currentUsers.map((user, i) => {
    const {
      user_id,
      civility,
      name, maiden_name,
      firstname,
      mail,
      tel_fix, tel_portable, tel,
      libelle_type_profil,
      libelle_profil
    } = user;

    return {
      keyRow: `company${i}`,
      props: {
        hover: true,
        onClick: () => getUser(user_id)
      },
      value: [
        {
          _type: 'string',
          keyCell: `userCivility${i}`,
          props: {
            label: civility
          }
        },
        {
          _type: 'string',
          keyCell: `userName${i}`,
          props: {
            label: name
          }
        },
        {
          _type: 'string',
          keyCell: `userMaidenName${i}`,
          props: {
            label: maiden_name
          }
        },
        {
          _type: 'string',
          keyCell: `userFirstname${i}`,
          props: {
            label: firstname
          }
        },
        {
          _type: 'string',
          keyCell: `userMail${i}`,
          props: {
            label: _.get(mail, 'coordonnee')
          }
        },
        {
          _type: 'string',
          keyCell: `userPhone${i}`,
          props: {
            label: _.get(tel_fix, 'coordonnee') || _.get(tel_portable, 'coordonnee') || _.get(tel, 'coordonnee')
          }
        },
        {
          _type: 'string',
          keyCell: `userTypeProfil${i}`,
          props: {
            label: libelle_type_profil
          }
        },
        {
          _type: 'string',
          keyCell: `userLibelleProfil${i}`,
          props: {
            label: libelle_profil
          }
        },
        {
          _type: 'iconButton',
          keyCell: `deleteCompany${i}`,
          props: {
            name: 'icon-trash',
            hidden: !isUserAdmin,
            onClick: (e) => {
              e.stopPropagation();
              setDeletionDialog('user', user.user_id);
            }
          }
        }
      ]
    };
  });

  const transformedUserListData = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    ...ownProps,
    loadData,
    isLoading,
    isError,
    onRefresh: () => setPansUserListTableData(tableName, societyId, transformedUserListData),
    param: transformedUserListData,
    cellHeight: 32,
    maxVisibleItems: 10
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(TableFactory);
