import { connect } from 'react-redux';
import { arrayRemove, arrayUnshift } from 'redux-form';
import {
  getCoordType as getCoordTypeThunk
} from 'redux/tabs/physicalPersonCreation';
import ContactArray from 'components/groups/Tables/PhysicalPersonContacts/ContactArray';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = state => ({
  selectedPerson_coords: _.get(getCurrentTabState(state), 'physicalPersonCreation.selected_physical_person.coordonnee'),
  coordTypes: _.get(getCurrentTabState(state), 'physicalPersonCreation.coordTypes'),
  societyId: state.navigation.id
});

const mapDispatchToProps = dispatch => ({
  getCoordType: () => dispatch(getCoordTypeThunk()),
  removeContact: (tab, index, contactId) => {
    dispatch(arrayRemove(tabFormName('physicalPersonCreationForm', tab), 'coordonnee', index));
    dispatch(arrayUnshift(tabFormName('physicalPersonCreationForm', tab), 'delete_coordonnee', { id: contactId }));
  },
  addContact: (tab) => {
    dispatch(arrayUnshift(tabFormName('physicalPersonCreationForm', tab), 'coordonnee'));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { coordTypes } = stateProps;

  const coord_types = coordTypes.map(e => ({
    value: e.id,
    label: e.name
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    coord_types
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ContactArray);
