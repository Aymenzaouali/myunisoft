import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import FactoryTable from 'components/groups/Tables/Factory';
import _ from 'lodash';
import actions from 'redux/rib/actions';
import {
  getRibsList as getRibsListThunk
} from 'redux/rib';
import { ribRefKeys } from 'assets/constants/keys';
import tableActions from 'redux/tables/actions';

const mapStateToProps = state => ({
  ribList: _.get(state, 'rib.ribList', []),
  society_id: state.navigation.id,
  selectedLineRib: _.get(state, 'rib.ribSelected', 0),
  selectedRib: _.get(state, `rib.selectedRib[${state.navigation.id}]`, []),
  rib: _.get(state, 'rib', {})
});

const mapDispatchToProps = dispatch => ({
  loadData: society_id => dispatch(getRibsListThunk(society_id)),
  selectRib: ribSelected => dispatch(actions.selectRib(ribSelected)),
  addRib: (
    rib_id,
    society_id
  ) => dispatch(actions.addRib(rib_id, society_id)),
  removeRib: (
    rib_id,
    society_id
  ) => dispatch(actions.removeRib(rib_id, society_id)),
  resetAllRib: society_id => dispatch(actions.resetAllRib(society_id)),
  addAllRib: society_id => dispatch(actions.addAllRib(society_id)),
  setRIBTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  )
});


const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    ribList,
    society_id,
    selectedLineRib,
    selectedRib,
    rib
  } = stateProps;
  const {
    selectRib,
    addRib,
    removeRib,
    resetAllRib,
    addAllRib,
    loadData,
    setRIBTableData
  } = dispatchProps;

  let isAllSelected = true;
  ribList.forEach((rib) => {
    if (!selectedRib.includes(rib)) {
      isAllSelected = false;
    }
  });

  const headerKeys = ribRefKeys;

  const headers = [{
    keyRow: `${society_id}ribTableHeaderRow0`,
    value: headerKeys.map((data, i) => {
      const { type, keyLabel } = data;
      let cell;

      if (type === 'checkbox') {
        cell = ({
          _type: type,
          keyCell: `${society_id}ribTableHeaderCell.${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllRib(society_id);
              } else {
                addAllRib(society_id);
              }
            }

          },
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 1
            }
          }
        });
      } else {
        cell = {
          _type: 'sortCell',
          keyCell: `${society_id}ribTableHeaderCell.${keyLabel}${i}`,
          cellProp: {
            style: {
              position: 'sticky',
              top: 0,
              backgroundColor: '#fff',
              zIndex: 1
            }
          },
          props: {
            children: I18n.t(`rib.ribArrayTitle.${keyLabel}`),
            onClick: () => {},
            colorErrorIcon: true
          }
        };
      }
      return cell;
    })
  }];


  const body = ribList.map((rib, i) => ({
    keyRow: `rib${i}`,
    props: {
      hover: true,
      onClick: () => selectRib(rib)
    },
    value: [
      {
        _type: 'checkbox',
        keyCell: `ribCheckbox${i}`,
        props: {
          checked: selectedRib.includes(rib),
          onClick: (e) => {
            e.stopPropagation();
          },
          onChange: () => {
            if (selectedRib.includes(rib)) {
              if (rib === selectedLineRib) selectRib('');
              removeRib(rib, society_id);
            } else {
              selectRib(rib);
              addRib(rib, society_id);
            }
          },
          id: rib.rib_id
        }
      },
      {
        _type: 'string',
        keyCell: `ribCodeBanque${i}`,
        props: {
          label: rib.banque_code
        }
      },
      {
        _type: 'string',
        keyCell: `ribCodeGuichet${i}`,
        props: {
          label: rib.guichet_code
        }
      },
      {
        _type: 'string',
        keyCell: `ribNumber${i}`,
        props: {
          label: rib.banque_account_number
        }
      },
      {
        _type: 'string',
        keyCell: `ribIban${i}`,
        props: {
          label: rib.iban
        }
      },
      {
        _type: 'string',
        keyCell: `ribBic${i}`,
        props: {
          label: rib.bic
        }
      },
      {
        _type: 'string',
        keyCell: `ribPaper${i}`,
        props: {
          label: rib.diary_label
        }
      }
    ]
  }));

  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  const tableData = {
    header: {
      props: {},
      row: headers
    },
    body: {
      props: {},
      row: body
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isLoading: rib.isLoading,
    isError: rib.isError,
    loadData: loadData.bind(null, society_id),
    onRefresh: () => setRIBTableData(tableName, society_id, tableData),
    param: tableData
  };
};


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FactoryTable);
