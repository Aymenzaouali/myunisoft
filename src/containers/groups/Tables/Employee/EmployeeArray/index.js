import { connect } from 'react-redux';
import EmployeeArray from 'components/groups/Tables/Employee/EmployeeArray';
import { arrayRemove, getFormValues } from 'redux-form';
import { delEmployee as delEmployeeThunk } from 'redux/tabs/physicalPersonCreation';
import _ from 'lodash';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    physicalPersonForm: getFormValues(tabFormName('physicalPersonCreationForm', societyId))(state),
    employmentFunctionTypes: _.get(getCurrentTabState(state), 'physicalPersonCreation.employmentFunctionTypes', []).map(e => ({
      value: e.id,
      label: e.name
    })),
    signatoryFunctionTypes: _.get(getCurrentTabState(state), 'physicalPersonCreation.signatoryFunctionTypes', []).map(e => ({
      value: e.id,
      label: e.name
    }))
  };
};
const mapDispatchToProps = dispatch => ({
  removeSociety: (id, index, data) => {
    dispatch(delEmployeeThunk(data));
    dispatch(arrayRemove(tabFormName('physicalPersonCreationForm', id), 'society', index));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { physicalPersonForm = {} } = stateProps;
  const { society = [] } = physicalPersonForm;

  const firm = society.map(e => ({
    ...e,
    label: _.get(e, 'society.name'),
    name: _.get(e, 'society.name'),
    function_id: _.get(e, 'function.id'),
    signatory_function_id: _.get(e, 'signatory_function.id', -1)
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    firm
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(EmployeeArray);
