import { connect } from 'react-redux';
import { SettingsWorksheetsTable } from 'components/groups/Tables/';
import actions from 'redux/settingsWorksheets/actions';
import _ from 'lodash';
import I18n from 'assets/I18n';
import { settingsWorksheetsKeys } from 'assets/constants/keys';
import tableActions from 'redux/tables/actions';
import SettingsWorksheetsService from 'redux/settingsWorksheets'; // eslint-disable-line

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  settings_id: state.settingsWorksheets.settings_id,
  worksheets: state.settingsWorksheets.worksheets,
  settingsWorksheetsTable: state.settingsWorksheets.settingsWorksheetsTable,
  settingSelectedLine: state.settingsWorksheets.settingSelectedLine,
  selectedWorksheet: _.get(state, `settingsWorksheets.selectedWorksheet[${state.navigation.id}]`, [])
});

const mapDispatchToProps = dispatch => ({
  selectWorksheet: settingSelectedLine => dispatch(actions.selectWorksheet(settingSelectedLine)),
  addWorksheet: (settingId, society_Id) => dispatch(actions.addWorksheet(settingId, society_Id)),
  removeWorksheet: (settingId, society_Id) => {
    dispatch(actions.removeWorksheet(settingId, society_Id));
  },
  addAllWorksheet: society_Id => dispatch(actions.addAllWorksheet(society_Id)),
  resetAllWorksheet: society_Id => dispatch(actions.resetAllWorksheet(society_Id)),
  setWorksheetTableData: (tableName, societyId, params) => dispatch(
    tableActions.setTransformedTableData(tableName, societyId, params)
  ),
  getWorksheets: society_Id => dispatch(SettingsWorksheetsService.getWorksheets(society_Id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedWorksheet,
    societyId,
    worksheets,
    settingSelectedLine
  } = stateProps;
  const {
    selectWorksheet,
    addWorksheet,
    removeWorksheet,
    addAllWorksheet,
    resetAllWorksheet,
    getWorksheets,
    setWorksheetTableData
  } = dispatchProps;

  let isAllSelected = true;

  const currentWorksheets = worksheets[societyId] || {};

  const { isLoading, isError } = currentWorksheets;
  const worksheets_list = _.get(currentWorksheets, 'worksheets_list', []);

  worksheets_list.forEach((worksheet) => {
    if (!selectedWorksheet.some(item => item.id === worksheet.id)) {
      isAllSelected = false;
    }
  });

  const headers = [{
    keyRow: 'settingsHeaderRow',
    value: settingsWorksheetsKeys.map((data, i) => {
      const {
        type,
        keyLabel
      } = data;

      if (type === 'checkbox') {
        return {
          _type: 'checkbox',
          keyCell: `settingsHeaderCell${keyLabel}${i}`,
          props: {
            checked: isAllSelected,
            onClick: (e) => {
              e.stopPropagation();
            },
            onChange: () => {
              if (isAllSelected) {
                resetAllWorksheet(societyId);
              } else {
                addAllWorksheet(societyId);
              }
            },
            id: keyLabel.toString()
          }
        };
      }
      return {
        _type: 'sortCell',
        keyCell: `settingsHeaderCell${keyLabel}${i}`,
        cellProp: {},
        props: {
          children: I18n.t(`settingsWorksheets.table.${keyLabel}`)
        }
      };
    })

  }];

  const bodyRow = worksheets_list.map((worksheet, i) => ({
    keyRow: `settingsRow${i}`,
    props: {
      hover: true,
      onClick: () => {
        selectWorksheet(worksheet.id === _.get(settingSelectedLine, 'id', '')
          ? {}
          : { id: worksheet.id, code: worksheet.code });
      }
    },
    value: [
      {
        _type: 'checkbox',
        keyCell: `settingsCheckBox${i}`,
        props: {
          checked: selectedWorksheet.some(item => item.id === worksheet.id),
          disabled: worksheet.blocked,
          onClick: (e) => {
            e.stopPropagation();
          },
          onChange: () => {
            if (selectedWorksheet.some(item => item.id === worksheet.id)) {
              if (worksheet.id === _.get(settingSelectedLine, 'id', '')) selectWorksheet({});
              removeWorksheet(worksheet.id, societyId);
            } else {
              selectWorksheet({ id: worksheet.id, code: worksheet.code });
              addWorksheet(worksheet, societyId);
            }
          },
          id: i.toString()
        },
        cellProp: {
          style: {
            width: '44px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `settingsCode${i}`,
        props: {
          label: _.get(worksheet, 'type', '')
        },
        cellProp: {
          style: {
            width: '106px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `settingsLabel${i}`,
        props: {
          label: _.get(worksheet, 'account_ttc.number', '')
        },
        cellProp: {
          style: {
            width: '190px'
          }
        }
      },
      {
        _type: 'string',
        keyCell: `settingsType${i}`,
        props: {
          label: _.get(worksheet, 'account_tva.number', '')
        },
        cellProp: {
          style: {
            width: '112px'
          }
        }
      }
    ]
  }));

  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData: getWorksheets,
    setTableData: param => setWorksheetTableData(tableName, societyId, param),
    isLoading,
    isError,
    param: {
      header: {
        props: {},
        row: headers
      },
      body: {
        props: {},
        row: bodyRow
      }
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(SettingsWorksheetsTable);
