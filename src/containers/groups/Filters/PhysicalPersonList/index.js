import { connect } from 'react-redux';
import { ListFilter } from 'components/groups/Filters';
import { reduxForm } from 'redux-form';
import { getPersonnePhysiqueList as getPersonnePhysiqueListThunk } from 'redux/tabs/physicalPersonList';
import { compose } from 'redux';
import I18n from 'assets/I18n';
import { tabFormName } from 'helpers/tabs';

const mapStateToProps = state => ({
  form: tabFormName('physicalPersonListFilter', state.navigation.id)
});

const mapDispatchToProps = dispatch => ({
  getPersonnePhysiqueList: payload => dispatch(getPersonnePhysiqueListThunk(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { getPersonnePhysiqueList } = dispatchProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    filterTag: 'physicalPersonSearchFilter',
    filterLabel: I18n.t('physicalPersonList.searchFilter'),
    initialValues: {},
    toFilter: (query) => {
      const params = {
        q: query.q,
        page: 0
      };

      getPersonnePhysiqueList(params);
    }
  };
};

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    destroyOnUnmount: true
  })
);

export default enhance(ListFilter);
