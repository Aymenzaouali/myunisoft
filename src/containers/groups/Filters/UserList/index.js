import { connect } from 'react-redux';
import { ListFilter } from 'components/groups/Filters';
import { reduxForm } from 'redux-form';
import { getUserList as getUserListThunk } from 'redux/tabs/user';
import { compose } from 'redux';
import I18n from 'assets/I18n';
import { tabFormName } from 'helpers/tabs';

const mapStateToProps = state => ({
  form: tabFormName('userListFilter', state.navigation.id)
});

const mapDispatchToProps = dispatch => ({
  getUserList: payload => dispatch(getUserListThunk(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { getUserList } = dispatchProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    filterTag: 'userSearchFilter',
    filterLabel: I18n.t('userList.searchFilter'),
    initialValues: {},
    toFilter: (query) => {
      const params = {
        q: query.q,
        page: 0
      };

      getUserList(params);
    }
  };
};

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    destroyOnUnmount: true
  })
);

export default enhance(ListFilter);
