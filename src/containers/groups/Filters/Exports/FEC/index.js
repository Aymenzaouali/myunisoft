import { connect } from 'react-redux';
import _ from 'lodash';
import { formValueSelector } from 'redux-form';
import FecExportFilter from 'components/groups/Filters/Exports/FEC';
import { getExercice as getExerciceThunk } from 'redux/accounting';
import { setSociety } from 'redux/tabs/export/actions';
import { getDiaries as getDiariesThunk } from 'redux/diary';
import { tabFormName } from 'helpers/tabs';
import { autoFillFormValues } from 'helpers/autoFillFormValues';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const currentSocietyId = societyId === -2 ? 'collab' : societyId;
  const matchingSociety = state.navigation.tabs.find(e => e.id === currentSocietyId);
  const selectedSociety = _.get(state, `tabs.${currentSocietyId}.exports.selectedSociety`) || _.get(matchingSociety, 'id');
  const selectedType = _.get(state, `tabs.${currentSocietyId}.exports.selectedType`) || _.get(matchingSociety, 'id');
  return {
    societyId,
    exercices: formValueSelector(tabFormName('exportForm', societyId))(state, 'exercises'),
    selectedSociety,
    isLoading: _.get(state, `accountingWeb.exercice.${selectedSociety}.loading`, []),
    selectedType,
    diaryList: _.get(state, `diary.diaries.${selectedSociety}.diary_list`, []),
    fecType: formValueSelector(tabFormName('exportForm', societyId))(state, 'type')
  };
};

const mapDispatchToProps = dispatch => ({
  setSociety: id => dispatch(setSociety(id)),
  getExercice: (societyId, id) => dispatch(getExerciceThunk(societyId, id)),
  initializeDate: (id, dateStart, dateEnd, exercice) => {
    dispatch(autoFillFormValues(tabFormName('exportForm', id), { dateStart, dateEnd, exercice }));
  },
  getDiaries: societyId => dispatch(getDiariesThunk(undefined, societyId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { exercices = [], societyId } = stateProps;
  let exercice_N = exercices.find(ex => ex.label === 'N');
  if (exercice_N) {
    exercice_N = { ...exercice_N, value: exercice_N.label };
  }
  const start_date = _.get(exercice_N, 'start_date', '');
  const end_date = _.get(exercice_N, 'end_date', '');
  const isFromCollab = societyId !== -2;
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    start_date,
    end_date,
    exercice_N,
    isFromCollab
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FecExportFilter);
