import { connect } from 'react-redux';
import ChartAccountsFilter from 'components/groups/Filters/ChartAccount';
import {
  getChartAccount as getChartAccountThunk,
  getAccountDetail as getAccountDetailThunk,
  deleteAccount as deleteAccountThunk
} from 'redux/chartAccounts';
import _ from 'lodash';
import actions from 'redux/chartAccounts/actions';
import { reduxForm, reset } from 'redux-form';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { compose } from 'redux';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import I18n from 'i18next';


const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const tableName = I18n.t('ChartAccount.title');
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  return {
    societyId,
    tableName,
    form: `${state.navigation.id}chartAccountFilter`,
    accountSelected: _.get(state, 'chartAccountsWeb.accountSelected', null),
    selectedAccount: _.get(state, `chartAccountsWeb.selectedAccount[${state.navigation.id}]`, []),
    accountData: _.get(state, 'chartAccountsWeb.accountDetail.account_array', []),
    currentCompanyName: _.get(currentTab, 'label'),
    chartAccountTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  getChartAccount: (opts, societyId) => dispatch(getChartAccountThunk(opts, societyId)),
  getAccountDetail: () => dispatch(getAccountDetailThunk()),
  initializeAccount: data => dispatch(autoFillFormValues('newAccount', data)),
  resetAccount: () => dispatch(reset('newAccount')),
  resetAccountSelected: () => dispatch(actions.selectAccount(null)),
  deleteAccount: accountList => dispatch(deleteAccountThunk(accountList)),
  resetAllAccounts: society_id => dispatch(actions.resetAllAccounts(society_id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    accountData,
    societyId,
    selectedAccount,
    currentCompanyName,
    chartAccountTableData,
    tableName
  } = stateProps;
  const {
    initializeAccount, deleteAccount, getChartAccount, resetAllAccounts, resetAccountSelected
  } = dispatchProps;

  const deleteAccountProps = async () => {
    resetAllAccounts(societyId);
    resetAccountSelected();
    await deleteAccount(selectedAccount);
    await getChartAccount();
  };
  const dataToExport = formatTableDataToCSVData(chartAccountTableData);

  const baseFileName = createFormattedFileName(currentCompanyName, tableName);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    initializeAccount,
    accountData,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName,
    deleteAccountProps,
    initialValues: {
      classFilterChartAccount: {
        value: '',
        label: 'Tous'
      }
    }
  };
};

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(ChartAccountsFilter);
