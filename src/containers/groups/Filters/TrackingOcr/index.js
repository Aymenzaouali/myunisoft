import { connect } from 'react-redux';
import { TrackingOcrFilter } from 'components/groups/Filters';
import { getTrackingOcr as getTrackingOcrThunk } from 'redux/ocr';
import { reset, reduxForm } from 'redux-form';
import { compose } from 'redux';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import moment from 'moment';
import _ from 'lodash';
import I18n from 'i18next';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = I18n.t('OCR.trackingTable.title');
  return {
    tableName,
    currentCompanyName: _.get(currentTab, 'label'),
    society_id: state.navigation.id,
    form: `${state.navigation.id}TrackingOcrFilter`,
    trackingOcrData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  getTrackingOcr: opts => dispatch(getTrackingOcrThunk(opts)),
  reset: form => dispatch(reset(form))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const currentDate = moment().format('YYYYMMDD');

  const {
    society_id,
    trackingOcrData,
    tableName,
    currentCompanyName
  } = stateProps;

  const dashboardCollab = society_id === -2;
  const dataToExport = formatTableDataToCSVData(trackingOcrData);
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    dashboardCollab,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName,
    initialValues: {
      start_date: moment(moment(currentDate).subtract(1, 'month')).format('YYYYMMDD'),
      end_date: currentDate,
      sendBy: '1',
      search: '',
      societyFilter: []
    }
  };
};

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    destroyOnUnmount: false
  })
);

export default enhance(TrackingOcrFilter);
