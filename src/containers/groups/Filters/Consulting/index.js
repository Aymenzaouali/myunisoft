import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, getFormValues } from 'redux-form';
import { get as _get, find } from 'lodash';
import moment from 'moment';

import I18n from 'assets/I18n';
import { splitWrap } from 'context/SplitContext';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import { findExercicesByDateRange } from 'helpers/exercices';

import { ConsultingFilter } from 'components/groups/Filters';
import { ODDialog } from 'containers/groups/Dialogs';

import { getConsultationEntrie as getEntriesThunk } from 'redux/consulting';
import { openDialog } from 'redux/dialogs';
import { updateArgsSplit } from 'redux/tabs/split/actions';


const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = 'Consultation compte';
  return {
    form: `${state.navigation.id}consultingFilter`,
    tableName,
    currentCompanyName: _get(currentTab, 'label'),
    societyId,
    consultingDataToCsv: _get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {}),
    accounts: state.accountingWeb.account,
    formValues: getFormValues(`${state.navigation.id}consultingFilter`)(state) || {},
    accountingExercice: state.accountingWeb.exercice
  };
};

const mapDispatchToProps = dispatch => ({
  getEntries: (societyId, prevnext) => {
    dispatch(getEntriesThunk(societyId, undefined, undefined, prevnext));
  },
  updateArgsSplit: (args) => {
    dispatch(updateArgsSplit('consulting', args));
  },
  openODDialog: dialogProps => dispatch(openDialog(dialogProps))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    accountingExercice, societyId,
    currentCompanyName, consultingDataToCsv, tableName,
    accounts,
    formValues
  } = stateProps;
  const { openODDialog } = dispatchProps;
  const { split, notLetter } = ownProps;

  const paramAccountId = new URLSearchParams(window.location.search).get('accountId');
  const exerciceParamId = new URLSearchParams(window.location.search).get('exerciceId');
  const exerciceParamLabel = new URLSearchParams(window.location.search).get('exerciceLabel')?.replace(' ', '+');
  const dateStartParam = new URLSearchParams(window.location.search).get('dateStart');
  const dateEndParam = new URLSearchParams(window.location.search).get('dateEnd');
  const exercicesData = _get(accountingExercice, `${societyId}.exercice`, []);
  let exercicesDataFiltered = formValues.exercice?.length ? formValues?.exercice : exercicesData.filter(value => (value.label === 'N' || value.label === 'N+1'));
  let dateStart = exercicesDataFiltered[0]?.start_date || formValues.dateStart;
  let dateEnd = exercicesDataFiltered[0]?.end_date || formValues.dateEnd;

  const Account = paramAccountId
    ? accounts.find(({ account_id }) => account_id === parseInt(paramAccountId, 10))
    : split?.args?.account || formValues.Account;

  if ((exerciceParamId || exerciceParamLabel) && exercicesData.length) {
    exercicesDataFiltered = exercicesData.filter(({ id_exercice, label }) => (exerciceParamId
      ? id_exercice === parseInt(exerciceParamId, 10)
      : label === exerciceParamLabel));
  } else if ((dateStartParam && dateEndParam && exercicesData.length)) {
    exercicesDataFiltered = findExercicesByDateRange(exercicesData, dateStartParam, dateEndParam);
    dateStart = dateStartParam;
    dateEnd = dateEndParam;
  }

  const initialValues = {
    exercice: exercicesDataFiltered.map(item => ({ ...item, value: item.label })),
    selectLettrage: formValues.selectLettrage || {
      label: I18n.t('consulting.filter.lettrage'),
      value: null
    },
    dateStart: dateStart && moment(dateStart).format('YYYYMMDD'),
    dateEnd: dateEnd && moment(dateEnd).format('YYYYMMDD'),
    Account
  };

  const baseFileName = createFormattedFileName(currentCompanyName, tableName);
  const dataToExport = formatTableDataToCSVData(consultingDataToCsv);

  const setODDialog = () => {
    openODDialog(
      {
        body: {
          Component: ODDialog,
          props: {
            isOpen: true,
            notLetter,
            selectedAccount: _get(formValues, 'Account')
          }
        }
      }
    );
  };
  return ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    csvData: dataToExport,
    baseFileName,
    initialValues,
    exercicesData,
    setODDialog
  });
};

// Enhance
const enhance = compose(
  splitWrap,
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  reduxForm({ destroyOnUnmount: false, enableReinitialize: true })
);

export default enhance(ConsultingFilter);
