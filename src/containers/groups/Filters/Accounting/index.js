import { compose } from 'redux';
import {
  reduxForm,
  getFormValues, getFormInitialValues,
  reset, change
} from 'redux-form';
import { connect } from 'react-redux';
import moment from 'moment';
import _ from 'lodash';

import { accountingTypes } from 'assets/constants/data';

import { getCurrentTabState } from 'helpers/tabs';

import actions from 'redux/accounting/actions';
import actionsDashboard from 'redux/dashboard/actions';
import { getDefaultFilter } from 'redux/tabs/accounting';
import { resetManualDocs } from 'redux/tabs/accounting/actions';
import { refreshDocuments } from 'redux/tabs/documents';

import { AccountingFilter } from 'components/groups/Filters';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

// Utils
const formatDate = date => date && moment(date).format('YYYY-MM-DD');

// Component
const mapStateToProps = (state) => {
  const { accounting } = getCurrentTabState(state);
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const accounts = state.accountingWeb.account;
  const searchUrlParams = new URLSearchParams(window.location.search);
  const accountId = Number(searchUrlParams.get('id'));
  const startDate = searchUrlParams.get('start_date');
  const purchaseValue = Number(searchUrlParams.get('purchase_value'));
  const initialAccount = accounts.find(item => item.account_id === accountId);
  const filterFromDashboard = _.get(state, 'dashboardWeb.filterAccounting');
  const accountingFilterType = _.get(state, `form.${societyId}accountingFilter.values.type`, '');
  const activeType = accountingTypes.find(type => type.value === accountingFilterType);
  const activeTypeName = _.get(activeType, 'label', '');

  let diary;
  let dateStart;
  let dateEnd;

  let filters = getFormValues(`${societyId}accountingFilter`)(state) || {};

  if (filterFromDashboard) {
    const {
      start_date,
      end_date,
      select_diary
    } = filterFromDashboard;

    diary = select_diary;
    dateStart = start_date;
    dateEnd = end_date;

    filters = {
      ...filters,
      dateStart,
      dateEnd
    };
  } else {
    const currentInitialValues = getFormInitialValues(`${societyId}accountingFilter`)(state) || {};
    diary = _.get(filters, 'diary') || _.get(accounting, 'default_filter.diary', {});
    dateStart = _.get(filters, 'dateStart') || formatDate(_.get(accounting, 'default_filter.start_date', currentInitialValues.dateStart));
    dateEnd = _.get(filters, 'dateEnd') || formatDate(_.get(accounting, 'default_filter.end_date', currentInitialValues.dateEnd));
  }

  return {
    societyId,
    initialAccount,
    startDate,
    purchaseValue,
    form: `${societyId}accountingFilter`,
    currentCompanyName: _.get(currentTab, 'label'),
    accountTableData: _.get(state, `tables.transformedTableData[${activeTypeName}][${societyId}].params`, {}),
    filters,
    entries: getFormValues(`${societyId}newAccounting`)(state),
    activeTypeName,
    initialValues: {
      type: filters.type || 'e',
      ...(filters.type === 'e' ? {
        diary: !diary.diary_id ? undefined : {
          ...diary,
          value: diary.diary_id,
          label: `${diary.code} - ${diary.name}`
        },
        dateStart,
        dateEnd
      } : {
        dateStart: filterFromDashboard ? dateStart : '',
        dateEnd: filterFromDashboard ? dateEnd : ''
      }),
      od_start_date: moment().subtract(1, 'month').startOf('month'),
      od_end_date: moment().subtract(1, 'month').endOf('month')
    }
  };
};

const mapDispatchToProps = dispatch => ({
  clearDataForAccounting: () => dispatch(actionsDashboard.clearDataForAccounting()),
  resetForm: form => dispatch(reset(form)),
  resetAllEntries: societyId => dispatch(actions.resetAllEntries(societyId)),
  refreshDocuments: () => dispatch(refreshDocuments()),
  getDefaultFilter: () => dispatch(getDefaultFilter()),
  changeForm: (form, field, data) => dispatch(change(form, field, data)),
  resetManDocs: () => dispatch(resetManualDocs())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    entries,
    activeTypeName,
    currentCompanyName,
    accountTableData
  } = stateProps;


  const {
    resetForm,
    refreshDocuments,
    resetAllEntries,
    resetManDocs
  } = dispatchProps;

  const onTypeChange = () => {
    resetAllEntries(societyId);
    resetManDocs();
    if (entries && entries.entry_id) {
      resetForm(`${societyId}newAccounting`);
    }
  };


  const baseFileName = createFormattedFileName(currentCompanyName, activeTypeName);

  const refreshData = () => {
    refreshDocuments();
  };

  const dataToExport = formatTableDataToCSVData(accountTableData);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    baseFileName,
    onTypeChange,
    types: accountingTypes,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    refreshData
  };
};

// Enhance
const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    destroyOnUnmount: false,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
  })
);

export default enhance(AccountingFilter);
