import { connect } from 'react-redux';
import odPayFilter from 'components/groups/Filters/Accounting/odPay';
import {
  loadOdpay as loadOdpayThunks,
  getEntries as getEntriesThunk
} from 'redux/accounting';

const mapDispatchToProps = dispatch => ({
  loadOdpay: () => dispatch(loadOdpayThunks()),
  getEntries: () => dispatch(getEntriesThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    getEntries,
    loadOdpay
  } = dispatchProps;

  const loadData = async () => {
    await loadOdpay();
    await getEntries();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData
  };
};


export default connect(null, mapDispatchToProps, mergeProps)(odPayFilter);
