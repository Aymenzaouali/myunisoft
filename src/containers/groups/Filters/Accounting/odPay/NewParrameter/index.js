import { connect } from 'react-redux';
import odPayFilter from 'components/groups/Filters/Accounting/odPay';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(odPayFilter);
