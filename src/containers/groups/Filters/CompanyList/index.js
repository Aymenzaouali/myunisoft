import { connect } from 'react-redux';
import { ListFilter } from 'components/groups/Filters';
import { reduxForm } from 'redux-form';
import { getCompanyList as getCompanyListThunk } from 'redux/tabs/companyList';
import { compose } from 'redux';
import I18n from 'assets/I18n';
import { tabFormName } from 'helpers/tabs';

const mapStateToProps = state => ({
  form: tabFormName('companyListFilter', state.navigation.id)
});

const mapDispatchToProps = dispatch => ({
  getCompanyList: payload => dispatch(getCompanyListThunk(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { getCompanyList } = dispatchProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    filterTag: 'companySearchFilter',
    filterLabel: I18n.t('companyList.searchFilter'),
    initialValues: {},
    toFilter: (query) => {
      const params = {
        q: query.q,
        page: 0
      };

      getCompanyList(params);
    }
  };
};

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  reduxForm({
    destroyOnUnmount: true
  })
);

export default enhance(ListFilter);
