import { connect } from 'react-redux';
import DrawerComponent from 'components/groups/Drawers';
import actions from 'redux/navigation/actions';
import _ from 'lodash';

const mapStateToProps = state => ({
  isMenuOpen: _.get(state, 'navigation.isMenuOpen', ''),
  rights: state.rights.rights
});

const mapDispatchToProps = dispatch => ({
  toggleMenu: isMenuOpen => dispatch(actions.toggleMenu(isMenuOpen))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { isMenuOpen } = stateProps;
  const { toggleMenu } = dispatchProps;

  const toggleMenuHandler = () => {
    toggleMenu(!isMenuOpen);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    toggleMenuHandler
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(DrawerComponent);
