import { connect } from 'react-redux';
import _ from 'lodash';
import {
  getTrackingSocieties as getTrackingSocietiesThunk
} from 'redux/dashboard';
import moment from 'moment';
import actions from 'redux/dashboard/actions';
import Selector from 'components/basics/Selector';


const mapStateToProps = state => ({
  selectedPeriod: _.get(state, 'dashboardWeb.selectedPeriod', {})
});

const mapDispatchToProps = dispatch => ({
  getTrackingSocieties: opts => dispatch(getTrackingSocietiesThunk(opts)),
  changeSelectedPeriod: period => dispatch(actions.changeSelectedPeriod(period))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { selectedPeriod } = stateProps;
  const { changeSelectedPeriod, getTrackingSocieties } = dispatchProps;
  const { type } = ownProps;

  const dateParam = type === 'month' ? 'YYYY-MM' : 'YYYY';
  const formatLabel = type === 'month' ? 'MMMM YYYY' : 'YYYY';

  const getDateLabel = date => moment(date).format(formatLabel);

  const currentDate = {
    period: moment().format(dateParam),
    label: getDateLabel(moment().format(dateParam))
  };

  const genPeriodList = () => {
    const periods = [currentDate];
    const max = type === 'month' ? 36 : 5;
    const substractParam = type === 'month' ? 'months' : 'years';

    for (let i = 1; i <= max; i += 1) {
      periods.push({
        period: moment(periods[i - 1].period).subtract(1, substractParam).format(dateParam),
        label: getDateLabel(moment(periods[i - 1].period)
          .subtract(1, substractParam).format(dateParam))
      });
    }

    return periods;
  };

  const current = selectedPeriod && selectedPeriod.label;
  const periodList = genPeriodList().reverse();
  const currentIndex = selectedPeriod
  && periodList.findIndex(item => item.period === selectedPeriod.period);

  const onClickNext = async () => {
    const nextPeriod = periodList[currentIndex + 1];
    await changeSelectedPeriod(nextPeriod);
    await getTrackingSocieties();
  };

  const onClickPrevious = async () => {
    const previousPeriod = periodList[currentIndex - 1];
    await changeSelectedPeriod(previousPeriod);
    await getTrackingSocieties();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    current,
    data: periodList,
    currentIndex,
    onClickNext,
    onClickPrevious
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Selector);
