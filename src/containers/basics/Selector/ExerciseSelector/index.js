import { connect } from 'react-redux';
import _ from 'lodash';
import {
  getProgressionSociety as getProgressionSocietyThunk
} from 'redux/dashboard';
import moment from 'moment';
import actions from 'redux/dashboard/actions';
import Selector from 'components/basics/Selector';


const mapStateToProps = state => ({
  exercisesList: _.get(state, `dashboardWeb.exercisesList.${state.navigation.id}`, []),
  selectedExercise: _.get(state, `dashboardWeb.selectedExercise.${state.navigation.id}`, {}),
  societyId: state.navigation.id
});

const mapDispatchToProps = dispatch => ({
  getProgressionSociety: () => dispatch(getProgressionSocietyThunk()),
  changeSelectedExercise: (exercise, societyId) => {
    dispatch(actions.changeSelectedExercise(exercise, societyId));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { selectedExercise, exercisesList, societyId } = stateProps;
  const { changeSelectedExercise, getProgressionSociety } = dispatchProps;
  const { getDashboard } = ownProps;

  const current = selectedExercise
    && `${selectedExercise.label} (Du ${moment(selectedExercise.start_date).format('DD/MM/YYYY')} au ${moment(selectedExercise.end_date).format('DD/MM/YYYY')})`;
  const currentIndex = selectedExercise
  && exercisesList.findIndex(item => item.id_exercice === selectedExercise.id_exercice);

  const refreshDashboard = () => {
    getProgressionSociety();
    getDashboard();
  };

  const onClickNext = async () => {
    const nextExercise = exercisesList[currentIndex + 1];
    await changeSelectedExercise(nextExercise, societyId);
    refreshDashboard();
  };

  const onClickPrevious = async () => {
    const previousExercise = exercisesList[currentIndex - 1];
    await changeSelectedExercise(previousExercise, societyId);
    refreshDashboard();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    current,
    data: exercisesList,
    currentIndex,
    onClickNext,
    onClickPrevious
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Selector);
