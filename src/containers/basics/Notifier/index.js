import { connect } from 'react-redux';
import Notifier from 'components/basics/Notifier';
import { withSnackbar } from 'notistack';
import { bindActionCreators } from 'redux';
import { removeSnackbar } from 'common/redux/notifier/action';

const mapStateToProps = state => ({
  notifications: state.notifier.notifications
});

const mapDispatchToProps = dispatch => bindActionCreators({ removeSnackbar }, dispatch);

export default withSnackbar(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notifier));
