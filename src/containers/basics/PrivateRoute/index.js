import { connect } from 'react-redux';
import PrivateRoute from 'components/basics/PrivateRoute';
import _ from 'lodash';

const mapStateToProps = state => ({
  isAuth: !_.isEmpty(state.login.token)
});

export default connect(mapStateToProps)(PrivateRoute);
