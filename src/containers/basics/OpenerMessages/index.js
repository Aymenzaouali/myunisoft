import { connect } from 'react-redux';

import _ from 'lodash';

import OpenerMessages from 'components/basics/OpenerMessages';

// Component
const mapStateToProps = state => ({
  tab_id: _.get(state, 'navigation.id')
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(OpenerMessages);
