import { connect } from 'react-redux';

import { openedWindow, updateWindow, closeWindow } from 'redux/windows/actions';

import WindowManager from 'components/basics/WindowManager';

// Component
const mapStateToProps = state => ({
  windows: state.windows
});

const mapDispatchToProps = dispatch => ({
  dispatch,

  openedWindow: (id, ref) => dispatch(openedWindow(id, ref)),
  updateWindow: (id, state) => dispatch(updateWindow(id, state)),
  closeWindow: id => dispatch(closeWindow(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(WindowManager);
