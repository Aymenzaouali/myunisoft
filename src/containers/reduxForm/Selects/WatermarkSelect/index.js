import { connect } from 'react-redux';

import { getWatermarks } from 'redux/watermarks';

import ReduxAsyncSelect from 'components/reduxForm/Selections/ReduxAsyncSelect';

// Component
const mapStateToProps = (state) => {
  const { watermarks, isLoading, isError } = state.watermarks;

  return {
    isLoading,
    isError,
    options: watermarks && watermarks.map(mark => ({ value: mark.id_watermark, label: mark.label }))
  };
};

const mapDispatchToProps = dispatch => ({
  load: () => dispatch(getWatermarks())
});

export default connect(mapStateToProps, mapDispatchToProps)(ReduxAsyncSelect);
