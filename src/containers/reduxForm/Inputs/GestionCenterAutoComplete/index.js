import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getCurrentTabState } from 'helpers/tabs';
import _ from 'lodash';
import {
  getGestionCenter as getGestionCenterThunk
} from 'redux/tabs/companyCreation';

const mapStateToProps = state => ({
  gestionCenterList: _.get(getCurrentTabState(state), 'companyCreation.gestionCenterList', [])
});

const mapDispatchToProps = dispatch => ({
  getGestionCenter: () => dispatch(getGestionCenterThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    gestionCenterList
  } = stateProps;

  const {
    getGestionCenter
  } = dispatchProps;

  const options = gestionCenterList.map(({ center_gestion_id, name }) => ({
    id: center_gestion_id,
    value: name,
    label: name
  }));

  const loadData = () => {
    getGestionCenter();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    options,
    isClearable: true,
    loadData
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
