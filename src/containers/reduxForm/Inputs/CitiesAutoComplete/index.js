import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = state => ({
  cities: _.get(getCurrentTabState(state), 'companyCreation.cities', [])
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { cities } = stateProps;

  const options = cities.map(({ city }) => ({
    value: city,
    label: city
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    isCreatable: true,
    autoFill: true,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
