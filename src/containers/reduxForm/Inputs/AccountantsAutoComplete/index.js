import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getAccountants as getAccountantsThunk
} from 'redux/tabs/companyCreation';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = state => ({
  accountants: _.get(getCurrentTabState(state), 'companyCreation.accountants', [])
});

const mapDispatchToProps = dispatch => ({
  getAccountants: () => dispatch(getAccountantsThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { accountants } = stateProps;
  const { getAccountants } = dispatchProps;

  const options = accountants.map(({ accountant_id, name }) => ({
    id: accountant_id,
    value: name,
    label: name
  }));

  const onCustomFocus = () => {
    if (accountants.length) {
      return;
    }

    getAccountants();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    isCreatable: true,
    onFocus: onCustomFocus,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
