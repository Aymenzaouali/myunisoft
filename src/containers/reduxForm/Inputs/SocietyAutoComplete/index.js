import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import _ from 'lodash';
import { getSociety as getSocietyThunk } from 'redux/navigation';

const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  listSociety: _.get(state, 'navigation.societies', [])
});

const mapDispatchToProps = dispatch => ({
  getSociety: () => dispatch(getSocietyThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    listSociety
  } = stateProps;
  const {
    getSociety
  } = dispatchProps;

  const { onInputChange = () => {} } = ownProps;
  const onCustomFocus = () => {
    getSociety();
  };

  const inputChange = (q) => {
    onInputChange(q);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onCustomFocus,
    onInputChange: inputChange,
    options: listSociety.map(a => ({ ...a, value: a.name })),
    getOptionLabel: options => `${options.name}`
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
