import { connect } from 'react-redux';
import { compose } from 'redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getPaymentType as getPaymentTypeThunk
} from 'redux/accounting';
import _ from 'lodash';
import withFilter from 'hoc/withFilter';

const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  paymentType: state.accountingWeb.paymentType
});

const mapDispatchToProps = dispatch => ({
  getPaymentType: societyId => dispatch(getPaymentTypeThunk(societyId, undefined, null))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId, paymentType } = stateProps;
  const { getPaymentType } = dispatchProps;
  const { onInputChange = () => {}, filter } = ownProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onInputChange,
    isClearable: true,
    cacheOptions: true,
    loadData: getPaymentType.bind(null, societyId),
    options: paymentType.reduce((acc, payment) => {
      if (!filter.length || payment.name.toUpperCase().search(filter.toUpperCase()) !== -1) {
        acc.push({ ...payment, value: payment.payment_type_id, label: payment.name });
      }

      return acc;
    }, [])
  };
};

const enhance = compose(
  withFilter(),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(AutoComplete);
