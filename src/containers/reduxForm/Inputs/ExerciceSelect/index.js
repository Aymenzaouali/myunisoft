import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getExercice } from 'redux/accounting';
import I18n from 'assets/I18n';
import _ from 'lodash';

const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  exercice: state.accountingWeb.exercice,
  consulting: state.consulting
});

const mapDispatchToProps = dispatch => ({
  getExercice: societyId => dispatch(getExercice(societyId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId, exercice } = stateProps;
  const { getExercice } = dispatchProps;
  const { autoFill = true } = ownProps;
  const options = _.isUndefined(exercice[societyId])
    ? []
    : _.get(exercice, `${[societyId]}.exercice`, []).map(value => ({ ...value, value: value.label }));
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    options,
    isLoading: _.isUndefined(exercice[societyId]) || exercice[societyId].loading,
    autoFill,
    closeMenuOnSelect: (ownProps.isMulti === false),
    label: I18n.t('exercice'),
    loadData: () => {
      if (exercice[societyId] === undefined) {
        getExercice(societyId);
      }
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
