import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';
import {
  getSocietyStatus as getSocietyStatusThunk
} from 'redux/tabs/companyCreation';

const mapStateToProps = state => ({
  societyStatus: _.get(getCurrentTabState(state), 'companyCreation.societyStatus', []).map(e => ({
    label: e.name,
    value: e.id,
    id: e.id
  }))
});

const mapDispatchToProps = dispatch => ({
  getSocietyStatus: () => dispatch(getSocietyStatusThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyStatus
  } = stateProps;

  const {
    getSocietyStatus
  } = dispatchProps;


  const loadData = () => {
    getSocietyStatus();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    loadData,
    options: societyStatus
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
