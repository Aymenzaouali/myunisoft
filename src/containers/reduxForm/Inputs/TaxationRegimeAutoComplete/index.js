import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getTaxationRegime as getTaxationRegimeThunk
} from 'redux/tabs/companyCreation';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';


const mapStateToProps = state => ({
  taxes: _.get(getCurrentTabState(state), 'companyCreation.taxationRegime', [])
});

const mapDispatchToProps = dispatch => ({
  getTaxes: () => dispatch(getTaxationRegimeThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { taxes } = stateProps;
  const { getTaxes } = dispatchProps;

  const options = taxes.map(({ id_sheet_group, code, label }) => ({
    id: id_sheet_group,
    value: code,
    label
  }));

  const onCustomFocus = () => {
    if (options.length) {
      return;
    }
    getTaxes();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    onFocus: onCustomFocus,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
