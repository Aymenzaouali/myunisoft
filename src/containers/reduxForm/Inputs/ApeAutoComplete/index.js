import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getApe as getApeThunk
} from 'redux/tabs/companyCreation';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = state => ({
  apeCodes: _.get(getCurrentTabState(state), 'companyCreation.apeCodes', [])
});

const mapDispatchToProps = dispatch => ({
  getApe: payload => dispatch(getApeThunk(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { apeCodes } = stateProps;
  const { getApe } = dispatchProps;

  const options = apeCodes.map(({ id, code, name }) => ({
    id,
    value: code,
    label: `${code} - ${name}`,
    info: name
  }));

  const onInputChange = (value = '') => {
    if (value.length < 3) {
      return;
    }

    getApe(value);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    onInputChange,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
