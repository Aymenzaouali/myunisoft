import { connect } from 'react-redux';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';
import { AutoCompleteCell } from 'components/reduxForm/Inputs';
import { getCompanyList } from 'redux/tabs/companyList';

const mapStateToProps = state => ({
  societyList: _.get(getCurrentTabState(state), 'companyList.companiesData.society_array', [])
});

const mapDispatchToProps = dispatch => ({
  loadData: q => dispatch(getCompanyList({ q, limit: 4 }))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyList } = stateProps;
  const { loadData } = dispatchProps;
  const list = societyList.map(society => ({
    value: `${society.society_id}`,
    society,
    label: society.name
  }));

  const onInputChange = (q) => {
    loadData(q);
  };

  return {
    ...ownProps,
    options: list,
    isClearable: true,
    isCreatable: true,
    onInputChange,
    getValueLabel: opts => opts.label
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoCompleteCell);
