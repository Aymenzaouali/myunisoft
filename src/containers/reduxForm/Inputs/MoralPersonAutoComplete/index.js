import { connect } from 'react-redux';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';
import { AutoComplete } from 'components/reduxForm/Inputs';

const mapStateToProps = state => ({
  societyList: _.get(getCurrentTabState(state), 'companyCreation.listMoralPerson', [])
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyList } = stateProps;
  const list = societyList.map(society => ({
    value: society,
    label: society.name
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    options: list
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
