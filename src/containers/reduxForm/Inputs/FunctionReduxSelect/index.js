import { connect } from 'react-redux';
import { AutoCompleteCell } from 'components/basics/Cells';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';


const mapStateToProps = state => ({
  employmentFunctionTypes: _.get(getCurrentTabState(state), 'physicalPersonCreation.employmentFunctionTypes', []).map(e => ({
    value: e,
    label: e.name
  }))
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { employmentFunctionTypes } = stateProps;

  const { ...otherProps } = ownProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...otherProps,
    isClearable: true,
    options: employmentFunctionTypes
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoCompleteCell);
