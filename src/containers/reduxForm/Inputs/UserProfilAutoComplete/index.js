import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getUserProfil as getUserProfilThunk } from 'redux/tabs/user';
import {
  getFormValues
} from 'redux-form';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const formName = tabFormName('newUser', state.navigation.id);

  return {
    userProfil: _.get(getCurrentTabState(state), 'user.userProfil'),
    formValues: getFormValues(formName)(state)
  };
};

const mapDispatchToProps = dispatch => ({
  getUserProfil: id_type => dispatch(getUserProfilThunk(id_type))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    userProfil
  } = stateProps;

  const {
    getUserProfil
  } = dispatchProps;

  const {
    id_type = ''
  } = ownProps;

  const loadData = () => {
    getUserProfil(id_type);
  };

  const options = userProfil.map(({ id_profil, libelle }) => ({
    id: id_profil,
    value: libelle,
    label: libelle
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onFocus: loadData,
    options
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
