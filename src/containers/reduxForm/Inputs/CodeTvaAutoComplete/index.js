import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getCodeTva as getCodeTvaThunk } from 'redux/tva';
import _ from 'lodash';


const mapStateToProps = state => ({
  codeTva: _.get(state, 'tva.codeTva', [{}])
});

const mapDispatchToProps = dispatch => ({
  getCodeTva: () => dispatch(getCodeTvaThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { codeTva = [] } = stateProps;
  const { getCodeTva } = dispatchProps;

  const options = codeTva.map(tva => ({ ...tva, value: tva.vat_param_id }));

  const loadData = () => {
    getCodeTva();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    loadData,
    options,
    getOptionLabel: options => `${options.code}`
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
