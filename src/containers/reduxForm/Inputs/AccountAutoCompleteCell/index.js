import { connect } from 'react-redux';
import { compose } from 'redux';
import { AutoCompleteCell } from 'components/reduxForm/Inputs';
import {
  getAccount as getAccountThunk
} from 'redux/accounting';
import I18n from 'assets/I18n';
import _ from 'lodash';
import withFilter from 'hoc/withFilter';

const defaultGetOptionLabel = options => (options.account_number ? `${options.account_number} - ${options.label}` : options.label);
export const getFilteredOptions = (accounts, filter) => {
  const options = [];

  for (let i = 0; i < accounts.length && options.length < 10; i += 1) {
    const account = accounts[i];
    if (!filter.length
      || account.label.toUpperCase().includes(filter.toUpperCase())
      || account.account_number.toUpperCase().startsWith(filter.toUpperCase())) {
      options.push({ ...account, value: account.account_number });
    }
  }

  return options;
};
const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  account: _.get(state, 'accountingWeb.account', [])
});

const mapDispatchToProps = dispatch => ({
  getAccount: societyId => dispatch(getAccountThunk(societyId, undefined, null))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    account
  } = stateProps;
  const { getAccount } = dispatchProps;
  const {
    onInputChange = () => {},
    getOptionLabel,
    getOptions,
    filter,
    name
  } = ownProps;
  const filteredOptions = getFilteredOptions(account, filter);

  const inputChange = (q = '') => {
    onInputChange(q);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onInputChange: inputChange,
    options: getOptions ? getOptions(account, filter) : filteredOptions,
    cacheOptions: true,
    name: name || '',
    isClearable: true,
    loadData: (!account || !account.length) ? getAccount.bind(null, societyId) : undefined,
    getOptionLabel: getOptionLabel || defaultGetOptionLabel,
    formatCreateLabel: account_number => I18n.t('account.autoCompleteLabel', { account_number })
  };
};

const enhance = compose(
  withFilter(),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(AutoCompleteCell);
