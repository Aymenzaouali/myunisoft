import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { formValueSelector } from 'redux-form';
import { tabFormName } from 'helpers/tabs';
import I18n from 'assets/I18n';

const mapStateToProps = state => ({
  exercices: formValueSelector(tabFormName('exportForm', state.navigation.id))(state, 'exercises')
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { exercices } = stateProps;
  const { autoFill = true } = ownProps;
  return {
    ...stateProps,
    ...ownProps,
    options: exercices.map(item => ({ ...item, value: item.label })),
    autoFill,
    closeMenuOnSelect: (ownProps.isMulti === false),
    label: I18n.t('exercice')
  };
};

export default connect(mapStateToProps, undefined, mergeProps)(AutoComplete);
