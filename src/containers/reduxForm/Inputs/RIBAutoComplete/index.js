import { connect } from 'react-redux';
import { compose } from 'redux';
import { AutoCompleteCell } from 'components/reduxForm/Inputs';
import {
  getRibsList as getRibsListThunk
} from 'redux/rib';
import I18n from 'assets/I18n';
import _ from 'lodash';
import withFilter from 'hoc/withFilter';

const defaultGetOptionLabel = options => options.label;
const getFilteredOptions = (ribs, filter, limit = 10000) => {
  const options = [];

  for (let i = 0; i < ribs.length && options.length < limit; i += 1) {
    const rib = ribs[i];
    const { label } = rib;
    if (!filter.length || label.toUpperCase().includes(filter.toUpperCase())) {
      options.push({ ...rib });
    }
  }

  return options;
};
const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  ribs: _.get(state, 'rib.ribList', []).map(rib => ({ value: rib.iban, label: rib.iban, bic: rib.bic }))
});

const mapDispatchToProps = dispatch => ({
  getRibsList: societyId => dispatch(getRibsListThunk(societyId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    ribs
  } = stateProps;
  const { getRibsList } = dispatchProps;
  const {
    getOptionLabel,
    getOptions,
    filter,
    name,
    limit
  } = ownProps;
  const filteredOptions = getFilteredOptions(ribs, filter, limit);
  return {
    isCreatable: true,
    border: true,
    placeholder: '',
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    options: getOptions ? getOptions(filteredOptions) : filteredOptions,
    cacheOptions: true,
    name: name || '',
    isClearable: true,
    loadData: getRibsList.bind(null, societyId),
    getValueLabel: defaultGetOptionLabel,
    getOptionLabel: getOptionLabel || defaultGetOptionLabel,
    menuPosition: 'absolute',
    formatCreateLabel: account_number => I18n.t('account.autoCompleteLabel', { account_number })
  };
};

const enhance = compose(
  withFilter(),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(AutoCompleteCell);
