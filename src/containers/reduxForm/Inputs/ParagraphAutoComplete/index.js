import { connect } from 'react-redux';
import { compose } from 'redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import SettingStandardMailService from 'redux/settingStandardMail';
import _ from 'lodash';
import withFilter from 'hoc/withFilter';

const defaultGetOptionLabel = options => options.label;
const getFilteredOptions = (paragraphs = [], filter) => {
  const options = [];

  for (let i = 0; i < paragraphs.length && options.length < 10; i += 1) {
    const paragraph = paragraphs[i];
    const { id_paragraph, label, paragraph_text } = paragraph;
    const filterParagraph = paragraph.label.toUpperCase().startsWith(filter.toUpperCase);
    if (!filter || filterParagraph) {
      options.push({ value: id_paragraph, label, paragraph_text });
    }
  }

  return options;
};
const mapStateToProps = state => ({
  paragraphs: _.get(state, `settingStandardMail.paragraphs.${state.navigation.id}.paragraphs_list`, [])
});

const mapDispatchToProps = dispatch => ({
  loadData: () => dispatch(
    SettingStandardMailService.getParagraphs()
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    paragraphs
  } = stateProps;
  const { loadData } = dispatchProps;
  const {
    onInputChange = () => {},
    getOptionLabel,
    getOptions,
    filter,
    name,
    prefixed
  } = ownProps;

  const filteredOptions = getFilteredOptions(paragraphs, filter, prefixed);
  const inputChange = (q = '') => {
    onInputChange(q);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onInputChange: inputChange,
    options: getOptions ? getOptions(filteredOptions) : filteredOptions,
    cacheOptions: true,
    name: name || '',
    isClearable: true,
    loadData,
    getOptionLabel: getOptionLabel || defaultGetOptionLabel
  };
};

const enhance = compose(
  withFilter(),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(AutoComplete);
