import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';
import {
  getRules as getRulesThunk
} from 'redux/tabs/companyCreation';


const mapStateToProps = state => ({
  rules: _.get(getCurrentTabState(state), 'companyCreation.rules', [])
});

const mapDispatchToProps = dispatch => ({
  getRules: () => dispatch(getRulesThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    rules
  } = stateProps;

  const {
    getRules
  } = dispatchProps;

  const options = rules.map(({ id, value }) => ({
    id,
    value,
    label: value
  }));

  const loadData = () => {
    getRules();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    loadData,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
