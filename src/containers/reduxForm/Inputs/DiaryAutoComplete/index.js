import { connect } from 'react-redux';
import { compose } from 'redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getDiaries as getDiariesThunk } from 'redux/accounting';
import _ from 'lodash';
import withFilter from 'hoc/withFilter';

const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  diaries: state.accountingWeb.diaries
});

const mapDispatchToProps = dispatch => ({
  getDiaries: societyId => dispatch(getDiariesThunk(societyId, undefined, null))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId, diaries } = stateProps;
  const { getDiaries } = dispatchProps;
  const { onInputChange = () => {}, filter } = ownProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData: getDiaries.bind(null, societyId),
    onInputChange,
    cacheOptions: true,
    isClearable: true,
    options: diaries.reduce((acc, diary) => {
      const label = `${diary.code} - ${diary.name}`;
      if (!filter.length || label.toUpperCase().includes(filter.toUpperCase())) {
        acc.push({ ...diary, value: diary.diary_id, label });
      }
      return acc;
    }, [])
  };
};

const enhance = compose(
  withFilter(),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(AutoComplete);
