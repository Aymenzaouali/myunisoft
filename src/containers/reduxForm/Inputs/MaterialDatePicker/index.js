import { connect } from 'react-redux';
import { ReduxMaterialDatePicker } from 'components/reduxForm/Inputs';


const mapStateToProps = () => ({
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { onCustomChange, ...otherProps } = ownProps;
  const onChange = (value) => {
    onCustomChange(value.name);
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...otherProps,
    isClearable: true,
    onChangeCustom: onChange
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(ReduxMaterialDatePicker);
