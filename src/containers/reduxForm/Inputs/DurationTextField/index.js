import { connect } from 'react-redux';
import { change } from 'redux-form';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import moment from 'moment';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  changeValue: (formName, field, value) => dispatch(change(formName, field, value))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    input,
    start_date,
    end_date
  } = ownProps;

  let differenceInMonths = 0;
  if (moment(end_date).isValid() && moment(start_date).isValid()) {
    differenceInMonths = moment.duration(moment(end_date).diff(moment(start_date))).asMonths();
  }

  const value = Math.round(differenceInMonths);

  input.onChange(value);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    /**
     * Duration is a disabled field,
     * So value can be safely manipulated
     */
    input: {
      ...input,
      value
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ReduxTextField);
