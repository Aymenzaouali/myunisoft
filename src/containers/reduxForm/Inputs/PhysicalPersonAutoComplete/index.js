import { connect } from 'react-redux';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getPersonnePhysiqueList } from 'redux/tabs/physicalPersonList';

const mapStateToProps = state => ({
  physicalPersonList: _.get(getCurrentTabState(state), 'physicalPersonList.physicalPersonsData.array_pers_physique', [])
});

const mapDispatchToProps = dispatch => ({
  loadData: q => dispatch(getPersonnePhysiqueList({ q, limit: 4 }))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { physicalPersonList } = stateProps;
  const { loadData } = dispatchProps;

  const list = physicalPersonList.map(person => ({
    value: person,
    label: ` ${person.name} ${person.firstname}`
  }));

  const onInputChange = (q) => {
    loadData(q);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    options: list,
    isClearable: true,
    onInputChange,
    reduxInput: ownProps.input,
    getValueLabel: opts => opts.label
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
