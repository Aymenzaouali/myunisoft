import { connect } from 'react-redux';
import { change } from 'redux-form';
import Periode from 'components/reduxForm/Inputs/Periode';
import { getExercice } from 'redux/accounting';
import _ from 'lodash';

const mapStateToProps = (state, ownProps) => {
  const { formName } = ownProps;
  return ({
    societyId: _.get(state, 'navigation.id'),
    formData: _.get(state.form, formName, {}),
    accountingExercice: state.accountingWeb.exercice,
    exercice: _.get(state.form, `${formName}.values.exercice`, [])
  });
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { formName } = ownProps;

  return ({
    getExercice: societyId => dispatch(getExercice(societyId)),
    changeDateStart: value => dispatch(change(`${formName}`, 'dateStart', value)),
    changeDateEnd: value => dispatch(change(`${formName}`, 'dateEnd', value)),
    updateSelectedExercices: exercices => dispatch(change(`${formName}`, 'exercice', exercices))
  });
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Periode);
