import { connect } from 'react-redux';
import { ReduxTextField } from 'components/reduxForm/Inputs';
import {
  getCities as getCitiesThunk
} from 'redux/tabs/companyCreation';

const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
  getCities: payload => dispatch(getCitiesThunk(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const onCustomChange = (value = '') => {
    const { getCities } = dispatchProps;
    if (value.length !== 5) {
      return;
    }

    getCities(value);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onCustomChange
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(ReduxTextField);
