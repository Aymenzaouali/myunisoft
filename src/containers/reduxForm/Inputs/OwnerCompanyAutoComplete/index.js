import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getMemberCompanies as getMemberCompaniesThunk
} from 'redux/tabs/companyCreation';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = state => ({
  memberCompanies: _.get(getCurrentTabState(state), 'companyCreation.memberCompanies', [])
});

const mapDispatchToProps = dispatch => ({
  getMemberCompanies: () => dispatch(getMemberCompaniesThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { memberCompanies } = stateProps;
  const { getMemberCompanies } = dispatchProps;

  const options = memberCompanies.map(({ member_id, name }) => ({
    id: member_id,
    value: name,
    label: name
  }));

  const onCustomFocus = () => {
    if (memberCompanies.length) {
      return;
    }

    getMemberCompanies();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    onFocus: onCustomFocus,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
