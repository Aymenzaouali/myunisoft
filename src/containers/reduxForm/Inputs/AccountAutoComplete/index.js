import { connect } from 'react-redux';
import { compose } from 'redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getAccount as getAccountThunk
} from 'redux/accounting';
import I18n from 'assets/I18n';
import _ from 'lodash';
import withFilter from 'hoc/withFilter';

const defaultGetOptionLabel = options => (options.account_number ? `${options.account_number} - ${options.label}` : options.label);
const getFilteredOptions = (accounts, filter, prefixed) => {
  const upercaseFilter = (filter || '').toUpperCase();

  return accounts
    .filter(({ account_number, label }) => {
      if (!filter) {
        return true;
      }

      const isFilteredByAccountNumber = account_number.toUpperCase().startsWith(upercaseFilter);
      const isFilteredByAccountLabel = label.toUpperCase().includes(upercaseFilter);

      return prefixed
        ? isFilteredByAccountNumber
        : isFilteredByAccountLabel || isFilteredByAccountNumber;
    })
    .slice(0, 10)
    .map(option => ({
      ...option,
      value: option.account_number
    }));
};

const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  account: _.get(state, 'accountingWeb.account', [])
});

const mapDispatchToProps = dispatch => ({
  getAccount: societyId => dispatch(getAccountThunk(societyId, undefined, null))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    account
  } = stateProps;
  const { getAccount } = dispatchProps;
  const {
    onInputChange = () => {},
    getOptionLabel,
    getOptions,
    filter,
    name,
    prefixed
  } = ownProps;

  const filteredOptions = getFilteredOptions(account, filter, prefixed);

  const inputChange = (q = '') => {
    onInputChange(q);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onInputChange: inputChange,
    options: getOptions ? getOptions(account) : filteredOptions,
    cacheOptions: true,
    name: name || '',
    isClearable: true,
    loadData: getAccount.bind(null, societyId),
    getOptionLabel: getOptionLabel || defaultGetOptionLabel,
    formatCreateLabel: account_number => I18n.t('account.autoCompleteLabel', { account_number })
  };
};

const enhance = compose(
  withFilter(),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(AutoComplete);
