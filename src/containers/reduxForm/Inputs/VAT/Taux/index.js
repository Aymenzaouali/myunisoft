import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getVatTaux as getVatTauxThunk
} from 'redux/tva';
import _ from 'lodash';

const mapStateToProps = state => ({
  taux: _.get(state, `tva.taux.${state.navigation.id}`)
});

const mapDispatchToProps = dispatch => ({
  getVatTaux: () => dispatch(getVatTauxThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { taux } = stateProps;
  const { getVatTaux } = dispatchProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    options: taux,
    cacheOptions: true,
    isClearable: true,
    loadData: getVatTaux,
    getOptionLabel: options => options.rate,
    InputLabelProps: {
      shrink: true
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
