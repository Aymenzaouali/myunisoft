import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getVatType as getVatTypeThunk
} from 'redux/tva';
import _ from 'lodash';

const mapStateToProps = state => ({
  type: _.get(state, `tva.type.${state.navigation.id}`)
});

const mapDispatchToProps = dispatch => ({
  getVatType: () => dispatch(getVatTypeThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { type } = stateProps;
  const { getVatType } = dispatchProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    options: type,
    cacheOptions: true,
    isClearable: true,
    loadData: getVatType,
    getOptionLabel: options => options.label
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
