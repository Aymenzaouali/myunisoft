import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getVatDue as getVatDueThunk
} from 'redux/tva';
import _ from 'lodash';

const mapStateToProps = state => ({
  due: _.get(state, `tva.due.${state.navigation.id}`)
});

const mapDispatchToProps = dispatch => ({
  getVatDue: () => dispatch(getVatDueThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { due } = stateProps;
  const { getVatDue } = dispatchProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    options: due,
    cacheOptions: true,
    isClearable: true,
    loadData: getVatDue,
    getOptionLabel: options => options.label
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
