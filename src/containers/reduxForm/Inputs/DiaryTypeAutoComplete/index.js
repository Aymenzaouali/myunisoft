import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import _ from 'lodash';

const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  diariesType: state.diary.diariesType
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { diariesType } = stateProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    cacheOptions: true,
    isClearable: true,
    autoFill: true,
    options: diariesType.map(diary => ({ id: diary.id, value: diary.code, label: diary.name }))
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
