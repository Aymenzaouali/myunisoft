import { connect } from 'react-redux';
import { compose } from 'redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import SettingStandardMailService from 'redux/settingStandardMail';
// import I18n from 'assets/I18n';
import _ from 'lodash';
import withFilter from 'hoc/withFilter';

const defaultGetOptionLabel = options => options.label;
const getFilteredOptions = (paragraphTypes = [], filter) => {
  const options = [];

  for (let i = 0; i < paragraphTypes.length && options.length < 10; i += 1) {
    const paragraphType = paragraphTypes[i];
    const { id_paragraph_type, name } = paragraphType;
    const filterParagraph = paragraphType.name.toUpperCase().startsWith(filter.toUpperCase);
    if (!filter || filterParagraph) {
      options.push({ value: id_paragraph_type, label: name });
    }
  }

  return options;
};
const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  paragraphTypes: _.get(state, `settingStandardMail.paragraphs_types.${state.navigation.id}.paragraphs_types_list`, [])
});

const mapDispatchToProps = dispatch => ({
  loadData: societyId => dispatch(
    SettingStandardMailService.geParagraphsTypes(societyId, undefined, null)
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    paragraphTypes
  } = stateProps;
  const { loadData } = dispatchProps;
  const {
    onInputChange = () => {},
    getOptionLabel,
    getOptions,
    filter,
    name,
    prefixed
  } = ownProps;

  const filteredOptions = getFilteredOptions(paragraphTypes, filter, prefixed);
  const inputChange = (q = '') => {
    onInputChange(q);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onInputChange: inputChange,
    options: getOptions ? getOptions(filteredOptions) : filteredOptions,
    cacheOptions: true,
    name: name || '',
    isClearable: true,
    loadData: loadData.bind(null, societyId),
    getOptionLabel: getOptionLabel || defaultGetOptionLabel
  };
};

const enhance = compose(
  withFilter(),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(AutoComplete);
