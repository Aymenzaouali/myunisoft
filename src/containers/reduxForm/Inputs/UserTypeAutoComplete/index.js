import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getUserType as getUserTypeThunk } from 'redux/tabs/user';
import { getCurrentTabState } from 'helpers/tabs';
import _ from 'lodash';

const mapStateToProps = state => ({
  userType: _.get(getCurrentTabState(state), 'user.userType')
});

const mapDispatchToProps = dispatch => ({
  getUserType: () => dispatch(getUserTypeThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    userType
  } = stateProps;

  const {
    getUserType
  } = dispatchProps;

  const loadData = () => {
    if (userType.length) {
      return;
    }

    getUserType();
  };

  const options = userType.map(({ id, libelle }) => ({
    id,
    value: libelle,
    label: libelle
  }));

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onFocus: loadData,
    options
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
