import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getAccountClass as getAccountClassThunk } from 'redux/chartAccounts';

const mapStateToProps = state => ({
  accountClass: state.chartAccountsWeb.accountClass
});

const mapDispatchToProps = dispatch => ({
  getAccountClass: () => dispatch(getAccountClassThunk())
});


const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { accountClass } = stateProps;
  const { getAccountClass } = dispatchProps;

  const loadData = () => {
    getAccountClass();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData,
    options: accountClass.map(classAccount => ({ ...classAccount, value: classAccount.value })),
    getOptionLabel: options => `${options.label}`
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
