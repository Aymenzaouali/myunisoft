import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getCurrentTabState } from 'helpers/tabs';
import _ from 'lodash';
import {
  getVatRegime as getVatRegimeThunk
} from 'redux/tabs/companyCreation';

const mapStateToProps = state => ({
  vatRegime: _.get(getCurrentTabState(state), 'companyCreation.vatRegime', []).map(e => ({
    label: e.name,
    id: e.id,
    code: e.code
  }))
});

const mapDispatchToProps = dispatch => ({
  getVatRegime: () => dispatch(getVatRegimeThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    vatRegime
  } = stateProps;

  const {
    getVatRegime
  } = dispatchProps;

  const loadData = () => {
    getVatRegime();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    loadData,
    options: vatRegime
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
