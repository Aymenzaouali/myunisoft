import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getSilaeAccounts as getSilaeAccountsThunk
} from 'redux/accounting';

const mapStateToProps = state => ({
  society_id: state.navigation.id,
  SilaeAccounts: state.accountingWeb.silae.list
});

const mapDispatchToProps = dispatch => ({
  getSilaeAccounts: society_id => dispatch(getSilaeAccountsThunk(society_id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { getSilaeAccounts } = dispatchProps;
  const { SilaeAccounts, society_id } = stateProps;

  const options = SilaeAccounts.map(value => ({
    value: value.account_id_final,
    label: value.account_num_origin
  }));

  const onCustomFocus = () => {
    if (SilaeAccounts.length) {
      return;
    }
    getSilaeAccounts(society_id);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    onFocus: onCustomFocus,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
