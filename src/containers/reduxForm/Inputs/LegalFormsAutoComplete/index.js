import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getLegalForms as getLegalFormsThunk
} from 'redux/tabs/companyCreation';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = state => ({
  legalForms: _.get(getCurrentTabState(state), 'companyCreation.legalForms', [])
});

const mapDispatchToProps = dispatch => ({
  getLegalForms: () => dispatch(getLegalFormsThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { legalForms } = stateProps;
  const { getLegalForms } = dispatchProps;

  const options = legalForms.map(({ id, code }) => ({
    id,
    value: code,
    label: code
  }));

  const onCustomFocus = () => {
    if (legalForms.length) {
      return;
    }

    getLegalForms();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    onFocus: onCustomFocus,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
