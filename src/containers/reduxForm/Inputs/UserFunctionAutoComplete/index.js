import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getGroupFunction as getGroupFunctionThunk
} from 'redux/function';
import _ from 'lodash';

const mapStateToProps = state => ({
  functions: _.get(state, 'groupFunctions.functions', [])
});

const mapDispatchToProps = dispatch => ({
  getFunction: () => dispatch(getGroupFunctionThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { functions } = stateProps;
  const { getFunction } = dispatchProps;

  const options = functions.map(({ id_group, libelle }) => ({
    id: id_group,
    value: libelle,
    label: libelle
  }));

  const loadData = () => {
    if (functions.length) {
      return;
    }

    getFunction();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    options,
    onFocus: loadData,
    isMulti: true
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
