import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getRoadTypes as getRoadTypesThunk
} from 'redux/tabs/companyCreation';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = state => ({
  roadTypes: _.get(getCurrentTabState(state), 'companyCreation.roadTypes', [])
});

const mapDispatchToProps = dispatch => ({
  getRoadTypes: () => dispatch(getRoadTypesThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { roadTypes } = stateProps;
  const { getRoadTypes } = dispatchProps;

  const options = roadTypes.map(({ id, name }) => ({
    id,
    value: name,
    label: name
  }));

  const onCustomFocus = () => {
    if (roadTypes.length) {
      return;
    }

    getRoadTypes();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    onFocus: onCustomFocus,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
