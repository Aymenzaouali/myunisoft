import { connect } from 'react-redux';
import { compose } from 'redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getReviews } from 'redux/tabs/dadp';
import { getCurrentTabState } from 'helpers/tabs';
import moment from 'moment';
import _ from 'lodash';
import withFilter from 'hoc/withFilter';

const defaultGetOptionLabel = options => options.label;
const getFilteredOptions = (reviews = [], filter) => {
  const options = [];

  for (let i = 0; i < reviews.length && options.length < 10; i += 1) {
    const review = reviews[i];
    const {
      start_date, end_date, id_dossier_revision, type
    } = review;
    const formatedStartDate = moment(start_date, 'YYYY-MM-DD').format('DD/MM/YYYY');
    const formatedEndDate = moment(end_date, 'YYYY-MM-DD').format('DD/MM/YYYY');
    const filterParagraph = formatedStartDate.startsWith(filter.toUpperCase)
    || formatedEndDate.startsWith(filter.toUpperCase);
    if (!filter || filterParagraph) {
      options.push({ value: id_dossier_revision, label: `${type} - ${formatedStartDate} - ${formatedEndDate}` });
    }
  }

  return options;
};
const mapStateToProps = state => ({
  societyId: _.get(state, 'navigation.id'),
  reviews: _.get(getCurrentTabState(state), 'dadp.reviews.data', [])
});

const mapDispatchToProps = dispatch => ({
  loadData: () => dispatch(
    getReviews()
  )
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    reviews
  } = stateProps;
  const { loadData } = dispatchProps;
  const {
    onInputChange = () => {},
    getOptionLabel,
    getOptions,
    filter,
    name,
    prefixed
  } = ownProps;

  const filteredOptions = getFilteredOptions(reviews, filter, prefixed);
  const inputChange = (q = '') => {
    onInputChange(q);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onInputChange: inputChange,
    options: getOptions ? getOptions(filteredOptions) : filteredOptions,
    cacheOptions: true,
    name: name || '',
    isClearable: true,
    loadData: loadData.bind(null, societyId),
    getOptionLabel: getOptionLabel || defaultGetOptionLabel
  };
};

const enhance = compose(
  withFilter(),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(AutoComplete);
