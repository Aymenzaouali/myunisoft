import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getTaxes as getTaxesThunk
} from 'redux/tabs/companyCreation';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';


const mapStateToProps = state => ({
  taxes: _.get(getCurrentTabState(state), 'companyCreation.taxes', [])
});

const mapDispatchToProps = dispatch => ({
  getTaxes: () => dispatch(getTaxesThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { taxes } = stateProps;
  const { getTaxes } = dispatchProps;

  const options = taxes.map(({ id, code, name }) => ({
    id,
    value: name,
    label: code
  }));

  const onCustomFocus = () => {
    if (options.length) {
      return;
    }

    getTaxes();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    onFocus: onCustomFocus,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
