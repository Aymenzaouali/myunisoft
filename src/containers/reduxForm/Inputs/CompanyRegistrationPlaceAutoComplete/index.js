import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import {
  getRegistrationPlaces as getRegistrationPlacesThunk
} from 'redux/tabs/companyCreation';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';


const mapStateToProps = state => ({
  registrationPlaces: _.get(getCurrentTabState(state), 'companyCreation.registrationPlaces', [])
});

const mapDispatchToProps = dispatch => ({
  getRegistrationPlaces: payload => dispatch(getRegistrationPlacesThunk(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { registrationPlaces } = stateProps;
  const { getRegistrationPlaces } = dispatchProps;

  const options = registrationPlaces.map(({ id, name }) => ({
    id,
    value: name,
    label: name
  }));

  const onInputChange = (value = '') => {
    if (value.length < 1) {
      return;
    }

    getRegistrationPlaces(value);
  };


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onInputChange,
    isClearable: true,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
