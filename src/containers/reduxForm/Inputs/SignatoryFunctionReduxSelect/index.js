import { connect } from 'react-redux';
import { AutoCompleteCell } from 'components/basics/Cells';
import _ from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const mapStateToProps = state => ({
  signatoryFunctionTypes: _.get(getCurrentTabState(state), 'physicalPersonCreation.signatoryFunctionTypes', []).map(e => ({
    value: e,
    label: e.name
  }))
});

const mapDispatchToProps = () => ({
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { signatoryFunctionTypes } = stateProps;
  const { ...otherProps } = ownProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...otherProps,
    isClearable: true,
    options: signatoryFunctionTypes
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoCompleteCell);
