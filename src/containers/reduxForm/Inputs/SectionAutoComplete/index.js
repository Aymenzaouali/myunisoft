import { connect } from 'react-redux';

import { getCurrentTabState } from 'helpers/tabs';

import { SUMMARY_SECTION } from 'helpers/dadp';
import { getSections } from 'redux/tabs/dadp';

import { AutoComplete } from 'components/reduxForm/Inputs';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);

  return {
    category: dadp.category,
    sections: dadp.sections.data.map(s => ({ ...s, value: s.id_section })),
    isLoading: dadp.sections.loading
  };
};

const mapDispatchToProps = dispatch => ({
  getSections: (q, category) => dispatch(getSections(q, category))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { category, sections } = stateProps;
  const { getSections } = dispatchProps;

  const onCustomFocus = (e, value = {}) => {
    getSections(value.value ? value.label : '', category);
  };

  const onInputChange = (q) => {
    if (q.length > 0) {
      getSections(q, category);
    }
  };

  const options = [
    SUMMARY_SECTION,
    ...sections
  ];

  return {
    ...ownProps,
    menuPosition: 'fixed',
    isClearable: true,
    onCustomFocus,
    onInputChange,
    getOptionLabel: (option) => {
      if (option.id_section === -1) {
        return option.label;
      }

      return `${option.code} - ${option.label}`;
    },
    options
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AutoComplete);
