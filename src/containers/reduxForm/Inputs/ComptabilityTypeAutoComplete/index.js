import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getCurrentTabState } from 'helpers/tabs';
import _ from 'lodash';
import {
  getAccountingType as getAccountingTypeThunk
} from 'redux/tabs/companyCreation';


const mapStateToProps = state => ({
  accountingType: _.get(getCurrentTabState(state), 'companyCreation.accountingType', [])
});

const mapDispatchToProps = dispatch => ({
  getAccountingType: () => dispatch(getAccountingTypeThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    accountingType
  } = stateProps;

  const {
    getAccountingType
  } = dispatchProps;

  const options = accountingType.map(({ id, value }) => ({
    id,
    value,
    label: value
  }));

  const loadData = () => {
    getAccountingType();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    loadData,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
