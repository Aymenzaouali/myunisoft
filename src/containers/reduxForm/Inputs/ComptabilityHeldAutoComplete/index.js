import { connect } from 'react-redux';
import { AutoComplete } from 'components/reduxForm/Inputs';
import { getCurrentTabState } from 'helpers/tabs';
import _ from 'lodash';
import {
  getAccountingOutfit as getAccountingOutfitThunk
} from 'redux/tabs/companyCreation';


const mapStateToProps = state => ({
  accountingOutfit: _.get(getCurrentTabState(state), 'companyCreation.accountingOutfit', [])
});

const mapDispatchToProps = dispatch => ({
  getAccountingOutfit: () => dispatch(getAccountingOutfitThunk())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    accountingOutfit
  } = stateProps;

  const {
    getAccountingOutfit
  } = dispatchProps;

  const options = accountingOutfit.map(({ id, value }) => ({
    id,
    value,
    label: value
  }));

  const loadData = () => {
    getAccountingOutfit();
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    isClearable: true,
    loadData,
    options
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(AutoComplete);
