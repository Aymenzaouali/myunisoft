import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm, formValueSelector } from 'redux-form';

import { getCurrentTabState, tabFormName } from 'helpers/tabs';

import { SUMMARY_SECTION } from 'helpers/dadp';
import { setSubCategory } from 'redux/tabs/dadp/actions';

import DADP from 'components/screens/DADP';

// Component
const mapStateToProps = (state) => {
  const { dadp } = getCurrentTabState(state);
  const societyId = state.navigation.id;

  const sectionForm = tabFormName('dadpSection', societyId);

  return {
    hasCycle: dadp.cycles.selectedId != null,
    summary: formValueSelector(sectionForm)(state, 'section') === SUMMARY_SECTION,
    subCategory: dadp.subCategory,
    form: `dadpPlaquette_${societyId}`
  };
};

const mapDispatchToProps = dispatch => ({
  setSubCategory: subCategory => dispatch(setSubCategory(subCategory))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    enableReinitialize: true
  })
)(DADP);
