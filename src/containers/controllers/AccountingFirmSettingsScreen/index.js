import { connect } from 'react-redux';
import AccountingFirmSettingsScreen from 'components/screens/AccountingFirmSettingsScreen';
import { ACCOUNTING_FIRM_TOP_MAIN_TABLE, getTableName } from 'assets/constants/tableName';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, ACCOUNTING_FIRM_TOP_MAIN_TABLE);
  return {
    lastSelectedRow: _.get(state, `tables.${tableName}.lastSelectedRow`, null),
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`),
    selectedRows: _.get(state, `tables.${tableName}.selectedRows`, {}),
    societyId: state.navigation.id
  };
};

export default connect(
  mapStateToProps,
  null
)(AccountingFirmSettingsScreen);
