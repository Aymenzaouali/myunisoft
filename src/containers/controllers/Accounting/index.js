import { connect } from 'react-redux';
import AccountingScreen from 'components/screens/Accounting';
import { getExercice as getExerciceThunk } from 'redux/accounting';
import { push } from 'connected-react-router';
import { reset } from 'redux-form';
import { routesByKey } from 'helpers/routes';
import {
  getCompanyList as getCompanyListThunk,
  getCompany as getCompanyThunk
} from 'redux/tabs/companyList';
import { getExercises as getExercisesThunk } from 'redux/tabs/companyCreation';
import { setCurrentForm, closeCurrentForm } from 'redux/tabs/form/actions';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import _ from 'lodash';

// Component
const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    exercicesAreLoading: _.get(state, `accountingWeb.exercice.${societyId}.load`, false),
    companyList: getCurrentTabState(state).companyList || {}
  };
};

const mapDispatchToProps = dispatch => ({
  getExercice: societyId => dispatch(getExerciceThunk(societyId)),
  push: path => dispatch(push(path)),
  getExercises: id => dispatch(getExercisesThunk(id)),
  getCompany: id => dispatch(getCompanyThunk(id)),
  getCompanyList: payload => dispatch(getCompanyListThunk(payload)),
  resetFormValues: form => dispatch(reset(form)),
  setCurrentForm: (name, initial) => dispatch(setCurrentForm(name, initial)),
  closeCurrentForm: discardCb => dispatch(closeCurrentForm(discardCb))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId, companyList } = stateProps;
  const {
    getCompany,
    resetFormValues,
    getExercises,
    setCurrentForm,
    closeCurrentForm,
    push
  } = dispatchProps;

  const { selectedCompany } = companyList;

  const formName = tabFormName('companyCreationForm', societyId);

  const resetFormAndGetCompanyAndResetExercises = async (society_id) => {
    await resetFormValues(formName);
    const society = await getCompany(society_id);
    const exercises = await getExercises(society_id);
    setCurrentForm(formName, { ...society, exercises });
  };

  const checkChangesAndGetCompany = async (society_id) => {
    await push(routesByKey.companyList);
    if (selectedCompany) {
      await closeCurrentForm(() => resetFormAndGetCompanyAndResetExercises(society_id));
    } else {
      await resetFormAndGetCompanyAndResetExercises(society_id);
    }
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    checkChangesAndGetCompany
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AccountingScreen);
