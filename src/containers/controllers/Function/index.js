import { connect } from 'react-redux';
import I18n from 'assets/I18n';
import FunctionScreen from 'components/screens/Function';
import {
  getGroupFunction as getGroupFunctionThunk,
  deleteGroupFunction as deleteGroupFunctionThunk
} from 'redux/function';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    tableName,
    currentCompanyName: _.get(currentTab, 'label', I18n.t('function.default')),
    formatted_function_table_data: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {}),
    list_selectedFunction: _.get(state, 'groupFunctions.list_selectedFunction', []),
    allFunctionList: _.get(state, 'groupFunctions.functions', []),
    selectedFunction: _.get(state, 'groupFunctions.selectedFunction')
  };
};

const mapDispatchToProps = dispatch => ({
  initializeFunctionForm: data => dispatch(autoFillFormValues('functionForm', data)),
  getFunction: () => dispatch(getGroupFunctionThunk()),
  deleteFunction: payload => dispatch(deleteGroupFunctionThunk(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedFunction, allFunctionList, tableName, currentCompanyName, formatted_function_table_data
  } = stateProps;

  const selectedFunctionDetail = allFunctionList.find(func => func.id_group === selectedFunction);
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);
  const transformedData = formatTableDataToCSVData(formatted_function_table_data);

  const disabledExport = _.isEmpty(transformedData);

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    csvData: transformedData,
    disabledExport,
    baseFileName,
    selectedFunctionDetail
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(FunctionScreen);
