import { connect } from 'react-redux';
import _ from 'lodash';
import VATScreen from 'components/screens/VAT';
import { deleteVat as deleteVatThunk } from 'redux/tva';
import actions from 'redux/tva/actions';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { initialize, reset } from 'redux-form';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';
import I18n from 'i18next';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = I18n.t('tva.list');
  return {
    societyId: _.get(state, 'navigation.id'),
    selectList: _.get(state.tva, `selectedVat.${state.navigation.id}`, []),
    vatSelectedLine: state.tva.vatSelectedLine,
    vat: _.get(state.tva, `vat.${state.navigation.id}.list`, []),
    currentCompanyName: _.get(currentTab, 'label'),
    vatTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  deleteVat: () => dispatch(deleteVatThunk()),
  resetAllVat: societyId => dispatch(actions.resetAllVat(societyId)),
  changeValue: (a, w, e) => dispatch(initialize(a, w, e)),
  initializeForm: data => dispatch(autoFillFormValues('vatForm', data)),
  resetVatForm: () => dispatch(reset('vatForm'))

});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    currentCompanyName, vatTableData, tableName, societyId
  } = stateProps;
  const {
    deleteVat, resetAllVat
  } = dispatchProps;

  const dataToExport = formatTableDataToCSVData(vatTableData);
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);

  const onDeleteVAT = async () => {
    await deleteVat();
    await resetAllVat(societyId);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName,
    onDeleteVAT
  };
};
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(VATScreen);
