import { connect } from 'react-redux';
import PasswordForgot from 'components/screens/PasswordForgot';
import { reduxForm } from 'redux-form';
import { push } from 'connected-react-router';
import { closeSnackbar, enqueueSnackbar } from 'common/redux/notifier/action';
import { loginValidation } from 'helpers/validation';
import { createTokenPWD as createTokenPWDThunk } from 'redux/account';

const mapStateToProps = () => ({

});

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  enqueueSnackbar: text => dispatch(enqueueSnackbar(text)),
  closeSnackbar: () => dispatch(closeSnackbar),
  createTokenPWD: (mail, redirectUrl) => dispatch(createTokenPWDThunk(mail, redirectUrl))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { createTokenPWD } = dispatchProps;

  const onSubmit = async (data, redirectUrl) => {
    await createTokenPWD({ mail: data.mail, redirectUrl });
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmit
  };
};

const WrappedWithFormPasswordForgot = reduxForm({
  form: 'passwordForgotForm',
  validate: loginValidation
})(PasswordForgot);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(WrappedWithFormPasswordForgot);
