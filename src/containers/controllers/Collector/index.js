import CollectorScreen from 'components/screens/Collector';
import _ from 'lodash';
import { connect } from 'react-redux';
import {
  getbudgetInsightToken as getBudgetInsightRedirectionThunk,
  getAccounting as getAccountingThunk
} from 'redux/collector';
import { getTableName } from 'assets/constants/tableName';
import I18n from 'assets/I18n';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

const pageName = 'collector';
const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = I18n.t('collector.title');
  return {
    currentCompanyName: _.get(currentTab, 'label'),
    tableName,
    societyId: state.navigation.id,
    selectedDocuments: _.get(state, `tables[${getTableName(state.navigation.id, pageName)}].selectedRows`, {}),
    selectedDocumentsCount: _.get(state, `tables[${getTableName(state.navigation.id, pageName)}].selectedRowsCount`, 0),
    formattedCollectorTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  getBudgetInsightRedirection: societyId => dispatch(getBudgetInsightRedirectionThunk(societyId)),
  getAccounting: (societyId, id_document) => dispatch(getAccountingThunk(societyId, id_document))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentCompanyName, formattedCollectorTableData, tableName } = stateProps;
  const baseFileName = createFormattedFileName(currentCompanyName, tableName);

  const dataToExport = formatTableDataToCSVData(formattedCollectorTableData);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    disabledExport: _.isEmpty(dataToExport),
    csvData: dataToExport,
    baseFileName
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CollectorScreen);
