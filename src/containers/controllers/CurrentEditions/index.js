import { connect } from 'react-redux';

import CurrentEditionsScreen from 'components/screens/CurrentEditions';

// Component
const mapStateToProps = state => ({
  societyId: state.navigation.id,
  currentOption: state.balance.currentOption
});

export default connect(mapStateToProps)(CurrentEditionsScreen);
