import { connect } from 'react-redux';
import LettersScreen from 'components/screens/Letters';
import { reduxForm } from 'redux-form';
import { getLetters, postLetters } from 'redux/letters';
import { LETTERS_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import get from 'lodash/get';

const mapStateToProps = (state) => {
  const tableName = getTableName(state.navigation.id, LETTERS_TABLE_NAME);
  return {
    initialValues: {
      type: 'all'
    },
    pagination: get(state, `tables.${tableName}.pagination`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  loadData: payload => dispatch(getLetters(payload)),
  createLetter: type => dispatch(postLetters(type))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

const WrappedWithFormLetters = reduxForm({
  form: 'lettersForm',
  destroyOnUnmount: false,
  enableReinitialize: true
})(LettersScreen);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WrappedWithFormLetters);
