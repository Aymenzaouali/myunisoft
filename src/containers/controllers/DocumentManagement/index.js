import { connect } from 'react-redux';
import { splitWrap } from 'context/SplitContext';
import { compose } from 'redux';
import DocumentManagement from 'components/screens/DocumentManagement';
import { formValueSelector } from 'redux-form';
import actions from '../../../redux/docManagement/actions';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  activeTabId: state.docManagement.activeTabId,
  isShowSendInDiscussionModal: state.docManagement.isShowSendInDiscussionModal,
  account: formValueSelector(`${state.navigation.id}consultingFilter`)(state, 'Account')
});

const mapDispatchToProps = dispatch => ({
  showSendInDiscussionModal: status => dispatch(
    actions.showSendInDiscussionModal(status)
  )
});

const enhance = compose(
  splitWrap,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

export default enhance(DocumentManagement);
