import { connect } from 'react-redux';
import PasswordReset from 'components/screens/PasswordReset';
import { push } from 'connected-react-router';
import { reduxForm } from 'redux-form';
import { closeSnackbar, enqueueSnackbar } from 'common/redux/notifier/action';
import { replacePassword as replacePasswordThunk } from 'redux/account';

const mapStateToProps = () => ({

});

const mapDispatchToProps = dispatch => ({
  push: path => dispatch(push(path)),
  enqueueSnackbar: text => dispatch(enqueueSnackbar(text)),
  closeSnackbar: () => dispatch(closeSnackbar),
  replacePassword: (mail, redirectUrl) => dispatch(replacePasswordThunk(mail, redirectUrl))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { replacePassword } = dispatchProps;

  const onSubmit = async (user_mail, token, password) => {
    await replacePassword({ user_mail, token, password });
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmit
  };
};

const WrappedWithFormPasswordReset = reduxForm({
  form: 'passwordResetForm'
})(PasswordReset);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(WrappedWithFormPasswordReset);
