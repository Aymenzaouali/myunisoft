import { connect } from 'react-redux';
import VATReturn from 'components/screens/VATReturn';
import actions from 'redux/cycle/actions';
import pdfActions from 'redux/pdfForms/actions';
import {
  sendVat as sendVatThunk
} from 'redux/cycle';
import { getFormValues } from 'redux-form';
import _ from 'lodash';

const mapStateToProps = (state) => {
  const { isNothingness } = state.cycle;
  const societyId = state.navigation.id;
  const selectedFormId = _.get(state, `cycle.selectedFormId[${societyId}]`, 'tvaca3');
  return {
    societyId,
    headerValues: getFormValues(`${state.navigation.id}VATHeader`)(state),
    selectedFormId,
    tvaFormValues: getFormValues(`${societyId}${selectedFormId}`)(state),
    isNothingness
  };
};

const mapDispatchToProps = dispatch => ({
  selectFormId: (societyId, form_id) => dispatch(actions.selectFormId(societyId, form_id)),
  sendVat: month => dispatch(sendVatThunk(month)),
  setAllLinesErrors: (formId, errors) => dispatch(pdfActions.setAllLinesErrors(formId, errors))
});
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    headerValues,
    selectedFormId,
    tvaFormValues,
    isNothingness
  } = stateProps;
  const { selectFormId, sendVat } = dispatchProps;
  const period = _.get(headerValues, 'period');
  const selectForm = value => selectFormId(societyId, value);
  const save = () => {
    if (period) {
      sendVat(period);
    }
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    selectForm,
    isNothingness,
    sendVat: save,
    societyId,
    selectedFormId,
    tvaFormValues
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(VATReturn);
