import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';
import {
  reduxForm, formValueSelector, change, reset
} from 'redux-form';
import ImportScreen from 'components/screens/Imports';
import {
  importComptaSa as importComptaSaThunk,
  importCegid as importCegidThunk,
  importEbics as importEbicsThunk,
  postFec as postFecThunk,
  postExcel as postExcelThunk,
  postAssets as postAssetsThunk,
  getExerciceInfos
} from 'redux/tabs/import';
import {
  setSociety,
  addEbics, removeEbics,
  addExcel, removeExcel,
  resetImport
} from 'redux/tabs/import/actions';
import { getExercice as getExerciceThunk } from 'redux/accounting';
import { getMemberCompanies as getMemberCompaniesThunk } from 'redux/tabs/companyCreation';
import { getSociety as getSocietyThunk } from 'redux/navigation';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import { importComptaSaValidation } from 'helpers/validation';
import I18n from 'assets/I18n';
import moment from 'moment';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const selectedSociety = _.get(state, 'tabs.collab.imports.selectedSociety');
  const { imports } = getCurrentTabState(state);
  const fecImport = _.get(imports, 'fec');
  const excelImport = _.get(imports, `excelImport.${selectedSociety}`);
  const assetsImport = _.get(imports, `assets.${selectedSociety}`);
  const formName = tabFormName('importForm', societyId);
  const societies = _.get(state, 'navigation.societies', []);

  return {
    imports,
    societyId,
    form: formName,
    formValues: formValueSelector(formName)(state, 'type', 'source', 'member_code', 'society_code', 'member_id', 'filename', 'importedFile', 'exerciseId', 'format', 'writingType', 'society_id', 'importMode'),
    isLoading: _.get(imports, 'isLoading') || _.get(assetsImport, 'isLoading') || _.get(excelImport, 'isLoading'),
    memberCompanies: _.get(getCurrentTabState(state), 'companyCreation.memberCompanies', []),
    importSuccess: _.get(getCurrentTabState(state), 'imports.importSuccess') || _.get(fecImport, 'importSuccess') || _.get(assetsImport, 'importSuccess') || _.get(excelImport, 'importSuccess'),
    exercices: _.get(state, `accountingWeb.exercice.${selectedSociety}.exercice`, []).map(ex => ({
      label: `${moment(ex.start_date).format('L')} au ${moment(ex.end_date).format('L')}`,
      value: ex.id_exercice
    })),
    societies: societies.map(e => ({
      label: e.name,
      value: e.society_id
    })),
    files: _.get(getCurrentTabState(state), 'imports.ebics'),
    excelFiles: _.get(getCurrentTabState(state), 'imports.excel'),
    importedRows: _.get(fecImport, 'rows_number') || _.get(excelImport, 'rows_number'),
    exerciceInfos: _.get(imports, 'exerciceInfos')
  };
};

const mapDispatchToProps = dispatch => ({
  importComptaSa: payload => dispatch(importComptaSaThunk(payload)),
  importCegid: payload => dispatch(importCegidThunk(payload)),
  importEbics: payload => dispatch(importEbicsThunk(payload)),
  getMemberCompanies: () => dispatch(getMemberCompaniesThunk()),
  getExercice: societyId => dispatch(getExerciceThunk(societyId)),
  getSociety: () => dispatch(getSocietyThunk()),
  postFec: (values, refreshExCompany) => dispatch(postFecThunk(values, refreshExCompany)),
  postExcel: values => dispatch(postExcelThunk(values)),
  postAssets: values => dispatch(postAssetsThunk(values)),
  setSociety: id => dispatch(setSociety(id)),
  addEbics: files => dispatch(addEbics(files)),
  removeEbics: id => dispatch(removeEbics(id)),
  addExcel: files => dispatch(addExcel(files)),
  removeExcel: id => dispatch(removeExcel(id)),
  resetImport: () => dispatch(resetImport()),
  getExerciceInfos: (exerciceId, societyId) => dispatch(getExerciceInfos(exerciceId, societyId)),
  changeExerciceSelectedValue: (formName, value) => dispatch(change(formName, 'exerciseId', value)),
  changeImportMode: (formName, value) => dispatch(change(formName, 'importMode', value)),
  resetFormValues: form => dispatch(reset(form))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    memberCompanies, formValues, files,
    form: formName
  } = stateProps;

  const {
    importComptaSa, importCegid, importEbics, postFec, postAssets, postExcel,
    getExerciceInfos, changeExerciceSelectedValue, getExercice, setSociety,
    resetImport,
    resetFormValues,
    changeImportMode
  } = dispatchProps;

  const { refreshExCompany } = ownProps;

  const societyId = ownProps.societyId || formValues.society_id;

  const { type, source, format } = formValues;

  const typeOptions = [
    { value: 0, label: I18n.t('imports.typeOptions.whole') },
    { value: 1, label: I18n.t('imports.typeOptions.fec') },
    { value: 2, label: I18n.t('imports.typeOptions.bank') },
    // { value: 3, label: I18n.t('imports.typeOptions.other') }
    { value: 4, label: I18n.t('imports.typeOptions.assets') },
    { value: 5, label: I18n.t('imports.typeOptions.excel') }
  ];

  const sourceOptions = [
    { value: 0, label: I18n.t('imports.sourceOptions.comptaSa') },
    { value: 1, label: I18n.t('imports.sourceOptions.cegid') }
  ];

  const writingTypes = [
    { value: 0, label: 'Ecritures par lignes' },
    { value: 1, label: 'Ecritures par ecritures' }
  ];

  const formats = [
    {
      id: 0,
      label: I18n.t('imports.formats.cegid'),
      value: 'CEGID',
      disabled: false
    },
    {
      id: 1,
      label: I18n.t('imports.formats.coala'),
      value: 'COALA',
      disabled: true,
      color: '#ff8b1f'
    },
    {
      id: 2,
      label: I18n.t('imports.formats.eic'),
      value: 'EIC',
      disabled: false
    },
    {
      id: 3,
      label: I18n.t('imports.formats.agiris'),
      value: 'AGIRIS',
      disabled: true,
      color: '#ff8b1f'
    },
    {
      id: 4,
      label: I18n.t('imports.formats.acd'),
      value: 'ACD',
      disabled: true,
      color: '#ff8b1f'
    },
    {
      id: 5,
      label: I18n.t('imports.typeOptions.quadra'),
      value: 'QUADRA'
    }
  ];

  const import_source = [
    {
      id: 0,
      label: I18n.t('imports.import_source.autres'),
      value: 0
    },
    {
      id: 1,
      label: I18n.t('imports.import_source.coala'),
      value: 1
    }
  ];

  const memberOptions = memberCompanies.map(({ member_id, name }) => ({
    value: member_id, label: name
  }));

  const onSubmitImportForm = async (payload) => {
    try {
      switch (type) {
      case 0:
        if (source === '0') await importComptaSa(payload);
        if (source === '1') await importCegid(payload);
        break;
      case 1:
        await postFec(payload, refreshExCompany);
        break;
      case 2:
        files.map(file => importEbics(file));
        break;
      case 4:
        if (format === 'QUADRA') {
          await postAssets({ ...payload, format: 'ASCII' });
        } else {
          await postAssets(payload);
        }
        break;
      case 5:
        await postExcel(payload);
        break;
      default:
        return null;
      }
      return null;
    } catch (error) {
      console.log(error); //eslint-disable-line
      return error;
    }
  };

  const onSocietyChange = async (id) => {
    await changeExerciceSelectedValue(formName);
    await setSociety(id);
    await getExercice(id);
  };

  const onTypeChange = async () => {
    await resetFormValues(formName);
    await resetImport();
  };

  const onExerciceChange = (event) => {
    const { target: { value } } = event;

    changeExerciceSelectedValue(formName, value);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    typeOptions,
    sourceOptions,
    formats,
    import_source,
    memberOptions,
    onSubmitImportForm,
    writingTypes,
    onExerciceChange,
    onSocietyChange,
    onTypeChange,
    changeImportMode: value => changeImportMode(formName, value),
    onExerciseIdChanged: () => getExerciceInfos(formValues.exerciseId, societyId)
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  reduxForm({
    validate: importComptaSaValidation,
    enableReinitialize: true,
    initialValues: {
      type: 0, // Compta complète
      source: '0', // ComptaSa
      writingType: 0,
      fecImportFrom: '0',
      importMode: 0
    }
  })
)(ImportScreen);
