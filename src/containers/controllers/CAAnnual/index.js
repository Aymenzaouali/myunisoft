import { connect } from 'react-redux';
import CAAnnual from 'components/screens/CAAnnual';
import actions from 'redux/cycle/actions';
import { getFormValues } from 'redux-form';
import pdfActions from 'redux/pdfForms/actions';
import {
  sendCycleForms as sendCycleFormsThunk
} from 'redux/cycle';

import { get as _get } from 'lodash';
import { getCurrentTabState } from 'helpers/tabs';

const defaultExercise = 'N';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const { exercises } = getCurrentTabState(state);
  const selectedFormId = _get(state, `cycle.selectedFormId[${societyId}]`, 'T-IDENTIF');
  const deadlines = _get(state, `cycle.deadline.${societyId}.list`, []);
  return {
    societyId,
    selectedFormId,
    headerValues: getFormValues(`${societyId}cycleHeader`)(state),
    exercises: exercises ? exercises.map((
      {
        label, start_date, end_date, id_exercice
      }
    ) => ({
      start_date,
      end_date,
      label,
      value: label,
      exerciseId: id_exercice
    })) : [],
    deadlines: deadlines ? deadlines.map(deadline => ({
      label: deadline,
      value: deadline
    })) : []
  };
};

const mapDispatchToProps = dispatch => ({
  selectFormId: (societyId, form_id) => dispatch(actions.selectFormId(societyId, form_id)),
  sendCycleForms: (
    exerciseId, cycle_code, deadline, code_sheet_group
  ) => dispatch(sendCycleFormsThunk(exerciseId, cycle_code, deadline, code_sheet_group)),
  setAllLinesErrors: (formId, errors) => dispatch(pdfActions.setAllLinesErrors(formId, errors))
});
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    deadlines,
    exercises,
    headerValues,
    selectedFormId
  } = stateProps;
  const {
    selectFormId,
    sendCycleForms
  } = dispatchProps;

  const deadultDeadline = _get(deadlines, '[0].value', '');
  const exercise = _get(headerValues, 'exercise', defaultExercise);
  const deadline = _get(headerValues, 'deadline', deadultDeadline);

  const currentYearExercise = exercises.find(exerciseItem => exerciseItem.label === exercise) || {};
  const currentDeadline = deadlines.find(deadlineItem => deadlineItem.label === deadline) || {};

  const exerciseId = _get(currentYearExercise, 'exerciseId', null);
  const deadlineValue = _get(currentDeadline, 'value', null);

  const selectForm = value => selectFormId(societyId, value);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    selectForm,
    selectedFormId,
    sendCycleForms: () => {
      if (exerciseId && deadlineValue) {
        sendCycleForms(exerciseId, 'CA12-ANNUAL', deadlineValue);
      }
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CAAnnual);
