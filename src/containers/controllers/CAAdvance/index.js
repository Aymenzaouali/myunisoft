import { connect } from 'react-redux';
import CAAdvance from 'components/screens/CAAdvance';
import actions from 'redux/cycle/actions';
import { getFormValues } from 'redux-form';
import { sendCycleForms as sendCycleFormsThunk } from 'redux/cycle';
import { getCurrentTabState } from 'helpers/tabs';
import _ from 'lodash';

const defaultExercise = 'N';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const { exercises } = getCurrentTabState(state);
  const selectedFormId = _.get(state, `cycle.selectedFormId[${societyId}]`, 'T-IDENTIF');
  const deadlines = _.get(state, `cycle.deadline.${societyId}.list`, []);
  return {
    societyId,
    selectedFormId,
    headerValues: getFormValues(`${societyId}cycleHeader`)(state),
    exercises: exercises ? exercises.map((
      {
        label, start_date, end_date, id_exercice
      }
    ) => ({
      start_date,
      end_date,
      label,
      value: label,
      exerciseId: id_exercice
    })) : [],
    deadlines: deadlines ? deadlines.map(deadline => ({
      label: deadline,
      value: deadline
    })) : []
  };
};

const mapDispatchToProps = dispatch => ({
  selectFormId: (societyId, form_id) => dispatch(actions.selectFormId(societyId, form_id)),
  sendCycleForms: (
    exerciseId, cycle_code, deadline, code_sheet_group
  ) => dispatch(sendCycleFormsThunk(exerciseId, cycle_code, deadline, code_sheet_group))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId,
    deadlines,
    exercises,
    headerValues,
    selectedFormId
  } = stateProps;
  const {
    selectFormId,
    sendCycleForms
  } = dispatchProps;

  const deadultDeadline = _.get(deadlines, '[0].value', '');
  const exercise = _.get(headerValues, 'exercise', defaultExercise);
  const deadline = _.get(headerValues, 'deadline', deadultDeadline);

  const currentYearExercise = exercises.find(exerciseItem => exerciseItem.label === exercise) || {};
  const currentDeadline = deadlines.find(deadlineItem => deadlineItem.label === deadline) || {};

  const exerciseId = _.get(currentYearExercise, 'exerciseId', null);
  const deadlineValue = _.get(currentDeadline, 'value', null);

  const selectForm = value => selectFormId(societyId, value);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    selectForm,
    selectedFormId,
    sendCycleForms: () => {
      if (exerciseId && deadlineValue) {
        sendCycleForms(exerciseId, 'CA12-ADVANCE', deadlineValue);
      }
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CAAdvance);
