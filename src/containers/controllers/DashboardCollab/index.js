import { connect } from 'react-redux';
import DashboardCollab from 'components/screens/DashboardCollab';
import {
  getTrackingSocieties as getTrackingSocietiesThunk
} from 'redux/dashboard';
import _ from 'lodash';

const mapStateToProps = state => ({
  trackingSocietiesIsLoading: _.get(state, 'dashboardWeb.trackingSocietiesIsLoading', false),
  trackingSocieties: _.get(state, 'dashboardWeb.trackingSocieties', []),
  isTrackingSocietiesLoading: _.get(state, 'dashboardWeb.isLoading', false),
  currentMode: _.get(state, 'dashboardWeb.currentMode', 1)
});

const mapDispatchToProps = dispatch => ({
  getTrackingSocieties: mode => dispatch(getTrackingSocietiesThunk(mode))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentMode } = stateProps;
  const { getTrackingSocieties } = dispatchProps;

  const loadData = () => {
    getTrackingSocieties(currentMode);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    loadData
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(DashboardCollab);
