import { connect } from 'react-redux';
import Connector from 'components/screens/Connector';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import {
  getConnector as getConnectorThunk,
  deleteConnector as deleteConnectorThunk
} from 'redux/connector';
import { resetAllConnector } from 'redux/connector/actions';
import _ from 'lodash';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    tableName,
    currentCompanyName: _.get(currentTab, 'label'),
    list_selectedConnector: _.get(state, `connector.list_selectedConnector.${state.navigation.id}`, []),
    societyId: state.navigation.id,
    connectorDataToCsv: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {})
  };
};

const mapDispatchToProps = dispatch => ({
  initializeConnectorForm: data => dispatch(autoFillFormValues('connectorForm', data)),
  getConnector: () => dispatch(getConnectorThunk()),
  deleteConnector: connectorList => dispatch(deleteConnectorThunk(connectorList)),
  resetSelectedList: societyId => dispatch(resetAllConnector(societyId))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { connectorDataToCsv, currentCompanyName, tableName } = stateProps;

  const baseFileName = createFormattedFileName(currentCompanyName, tableName);
  const dataToExport = formatTableDataToCSVData(connectorDataToCsv);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    csvData: dataToExport,
    disabledExport: _.isEmpty(dataToExport),
    baseFileName
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Connector);
