import { connect } from 'react-redux';
import RentDeclarationsScreen from 'components/screens/RentDeclarations';

const mapStateToProps = state => ({
  societyId: state.navigation.id
});

const mapDispatchToProps = () => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId } = stateProps;
  const { selectFormId } = dispatchProps;
  const selectForm = value => selectFormId(societyId, value);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    selectForm
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(RentDeclarationsScreen);
