import { connect } from 'react-redux';
import NewAccountingScreen from 'components/screens/NewAccounting';
import _ from 'lodash';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  exercices: _.get(state, `accountingWeb.exercice.${state.navigation.id}.exercice`, [])
});

export default connect(mapStateToProps)(NewAccountingScreen);
