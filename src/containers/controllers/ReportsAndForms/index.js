import { connect } from 'react-redux';

import ReportsAndForms from 'components/screens/ReportsAndForms';

// Component
const mapStateToProps = state => ({
  societyId: state.navigation.id,
  isAddAllFormsAndReportsAddView: state.reportsAndForms.isAddAllFormsAndReportsAddView
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ReportsAndForms);
