import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import ConsultingScreen from 'components/screens/Consulting';

// Component
const mapStateToProps = state => ({
  societyId: state.navigation.id,
  account: formValueSelector(`${state.navigation.id}consultingFilter`)(state, 'Account')
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ConsultingScreen);
