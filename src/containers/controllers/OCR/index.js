import { connect } from 'react-redux';

import {
  addDocuments, deleteDocument, setCollabSociety, deleteUploadedDocuments,
  setLoading
} from 'redux/document/actions';
import { sendDocuments } from 'redux/document';
import {
  getSociety
} from 'redux/navigation';
import { push } from 'connected-react-router';
import OCR from 'components/screens/OCR';
import _ from 'lodash';

const mapStateToProps = (state, {
  match: { params: { id } }
}) => {
  const societies = _.get(state, 'navigation.societies', []);
  const collabSociety = _.get(state, 'documentWeb.collabSociety', {});
  const isLoading = _.get(state, 'documentWeb.loading');
  const docs = _.get(state, 'documentWeb.docs', []);
  const idSelectedSociety = _.get(state, 'navigation.id', []);
  const isCollab = id === 'collab';
  const society = isCollab ? collabSociety : societies.find(
    s => parseInt(s.society_id, 10) === parseInt(idSelectedSociety, 10)
  );
  return ({
    idSelectedSociety,
    isCollab,
    societies: societies.map(({ name: label, society_id: value }) => ({ label, value })),
    society,
    files: isCollab ? docs : docs.filter(
      f => parseInt(f.society.society_id, 10) === parseInt(_.get(society, 'society_id'), 10)
    ),
    isLoading
  });
};

const mapDispatchToProps = ({
  addDocuments,
  deleteDocument,
  setCollabSociety,
  sendDocuments,
  push,
  getSociety,
  onUnmount: deleteUploadedDocuments,
  setLoading
});

export default connect(mapStateToProps, mapDispatchToProps)(OCR);
