import { connect } from 'react-redux';
import _ from 'lodash';
import { compose } from 'redux';
import { reduxForm, formValueSelector } from 'redux-form';
import ExportScreen from 'components/screens/Export';
import { exportFec as exportFecThunk } from 'redux/tabs/export';
import { getDiaries as getDiariesThunk } from 'redux/diary';
import { getSociety as getSocietyThunk } from 'redux/navigation';
import { getExercice as getExerciceThunk } from 'redux/accounting';
import { resetImport } from 'redux/tabs/export/actions';
import tableActions from 'redux/tables/actions';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import I18n from 'assets/I18n';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  const currentSocietyId = state.navigation.id === -2 ? 'collab' : societyId;
  const selectedSociety = _.get(state, `tabs.${currentSocietyId}.exports.selectedSociety`);
  const { exports } = getCurrentTabState(state);
  const fecExport = _.get(exports, `fec.${selectedSociety}`);
  return {
    societyId,
    form: tabFormName('exportForm', societyId),
    isLoading: _.get(fecExport, 'isLoading', false),
    exercices: formValueSelector(tabFormName('exportForm', societyId))(state, 'exercises'),
    fecType: formValueSelector(tabFormName('exportForm', societyId))(state, 'type'),
    tabsSocieties: state.navigation.tabs,
    exercicesLoading: _.get(state, `accountingWeb.exercice.${societyId}.loading`),
    diariesForExport: _.get(state, `tables.diary-${societyId}.selectedRows`)
  };
};

const mapDispatchToProps = dispatch => ({
  getSociety: () => dispatch(getSocietyThunk()),
  resetImportAndSetType: value => dispatch(resetImport(value)),
  exportFec: (payload, diaries) => dispatch(exportFecThunk(payload, diaries)),
  getExercice: (societyId, id) => dispatch(getExerciceThunk(societyId, id)),
  initExportForm: (id, societyInfo, dateStart, dateEnd) => {
    dispatch(autoFillFormValues(tabFormName('exportForm', id), {
      societyInfo, dateStart, dateEnd
    }));
  },
  getDiaries: () => dispatch(getDiariesThunk()),
  resetSelectedDiaries: tableName => dispatch(tableActions.unselectAllRows(tableName))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    societyId, tabsSocieties, diariesForExport, fecType
  } = stateProps;
  const {
    exportFec,
    initExportForm,
    getExercice,
    resetSelectedDiaries
  } = dispatchProps;
  const typeOptions = [
    { value: 1, label: I18n.t('exports.typeOptions.fec') },
    { value: 2, label: I18n.t('exports.typeOptions.partial_fec') }
  ];
  const onSubmitExportForm = (payload) => {
    if (fecType === 2 && diariesForExport) {
      const selectedDiaries = Object.keys(diariesForExport).map(key => +key);
      exportFec(payload, selectedDiaries);
    } else { exportFec(payload); }
    resetSelectedDiaries(`diary-${societyId}`);
  };
  const currentSociety = tabsSocieties && tabsSocieties.map(e => ({
    ...e,
    society_id: e.id,
    name: e.label
  })).filter(s => parseInt(s.society_id, 10) === societyId);

  const initializeForm = async () => {
    const { data: exercises } = await getExercice(societyId, societyId);
    const exercice_N = exercises && exercises.find(ex => ex.label === 'N');
    const start_date = _.get(exercice_N, 'start_date', '');
    const end_date = _.get(exercice_N, 'end_date', '');
    await initExportForm(societyId, currentSociety[0], start_date, end_date, exercises);
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    typeOptions,
    onSubmitExportForm,
    initializeForm,
    initialValues: {
      type: 1
    }
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps, mergeProps),
  reduxForm({
    enableReinitialize: true
  })
)(ExportScreen);
