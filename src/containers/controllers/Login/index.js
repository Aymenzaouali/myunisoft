import { connect } from 'react-redux';
import LoginInScreen from 'components/screens/Login';
import { reduxForm, formValueSelector } from 'redux-form';
// eslint-disable-next-line import/no-named-as-default
import LoginService from 'common/redux/login';
import actions from 'common/redux/group/actions';
import { getGroupMember } from 'common/redux/group';
import { push } from 'connected-react-router';
import navigationActions from 'redux/navigation/actions';
import { routesByKey } from 'helpers/routes';
import { loginValidation } from 'helpers/validation';
import { enqueueSnackbar as enqueueSnackbarAction } from 'common/redux/notifier/action';
import { Notifications } from 'common/helpers/SnackBarNotifications';
import I18n from 'assets/I18n';
import _ from 'lodash';

const NODE_ENV = process.env.NODE_ENV; // eslint-disable-line
const selector = formValueSelector('loginForm');

const mapStateToProps = state => ({
  initialValues: {
    mail: NODE_ENV === 'development' ? 'b.finot@saminfo.fr' : '',
    password: NODE_ENV === 'development' ? 'saminfo' : ''
  },
  cgu: selector(state, 'cgu'),
  allowData: selector(state, 'allowData'),
  isActiveSession: !_.isEmpty(_.get(state, 'login.token'))
});

const mapDispatchToProps = dispatch => ({
  checkGroupMember: payload => dispatch(getGroupMember(payload)),
  signIn: payload => dispatch(LoginService.signIn(payload)),
  enqueueSnackbar: notification => dispatch(
    enqueueSnackbarAction(notification)
  ),
  changeCurrentGroup: group => dispatch(actions.changeCurrentGroup(group)),
  push: path => dispatch(push(path)),
  initTabs: user => dispatch(navigationActions.initTabs(user))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    checkGroupMember,
    signIn,
    push,
    initTabs,
    enqueueSnackbar
  } = dispatchProps;

  const onSubmit = async (payload) => {
    try {
      const groups = await checkGroupMember(payload);
      if (groups.length === 1) {
        const response = await signIn(payload);
        await initTabs(response.user);
        await push(routesByKey.dashboardCollab);
      }

      return groups;
    } catch (error) {
      if (_.get(error, 'code') === 'MismatchError' && error.local) {
        enqueueSnackbar(Notifications(I18n.t('errors.MismatchError.bad_email_password'), 'warning'));
      }
      return error;
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onSubmit
  };
};

const WrappedWithFormSignIn = reduxForm({
  form: 'loginForm',
  enableReinitialize: true,
  validate: loginValidation
})(LoginInScreen);

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WrappedWithFormSignIn);
