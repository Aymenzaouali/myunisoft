import ChartAccounts from 'components/screens/ChartAccounts';
import { connect } from 'react-redux';
import {
  getChartAccount as getChartAccountThunk
} from 'redux/chartAccounts';

const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
  getChartAccount: society_id => dispatch(getChartAccountThunk(society_id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});


export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ChartAccounts);
