import { connect } from 'react-redux';
import BankLinkScreen from 'components/screens/BankLink';
import {
  getExercice as getExerciceThunk
} from 'redux/accounting';
import {
  getBankLink as getBankLinkThunk
} from 'redux/bankLink';

const mapStateToProps = state => ({
  societyId: state.navigation.id
});

const mapDispatchToProps = dispatch => ({
  getExercice: () => dispatch(getExerciceThunk()),
  getBankLink: () => dispatch(getBankLinkThunk())
});

export default connect(mapStateToProps, mapDispatchToProps)(BankLinkScreen);
