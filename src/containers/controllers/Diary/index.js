import DiaryScreen from 'components/screens/Diary';
import { connect } from 'react-redux';
import { reset } from 'redux-form';
import {
  getDiariesType as getDiariesTypeThunk,
  deleteDiary as deleteDiaryThunk,
  getDiaryDetail as getDiaryDetailThunk,
  getDiaries as getDiariesThunk
} from 'redux/diary';
import _ from 'lodash';
import actions from 'redux/diary/actions';
import tables from 'redux/tables/actions';
import { getTableName } from 'assets/constants/tableName';
import { formatTableDataToCSVData, createFormattedFileName } from 'helpers/tableDataToCSV';

const pageName = 'diary';
const mapStateToProps = (state) => {
  const { id: societyId, tabs } = state.navigation;
  const currentTab = _.find(tabs, tab => (Number(tab.id) === societyId));
  const tableName = window.location.href.split('/').slice(-1).pop().toUpperCase();
  return {
    societyId,
    tableName,
    dairyTableData: _.get(state, `tables.transformedTableData[${tableName}][${societyId}].params`, {}),
    currentCompanyName: _.get(currentTab, 'label'),
    diarySelectedLine: _.get(state, 'diary.diarySelectedLine'),
    selectedDiary: _.get(state, `tables[${getTableName(state.navigation.id, pageName)}].selectedRows`, {}),
    selectedDiaryCount: _.get(state, `tables[${getTableName(state.navigation.id, pageName)}].selectedRowsCount`, 0),
    diaryDetail: _.get(state, 'diary.diaryDetail', {})
  };
};

const mapDispatchToProps = dispatch => ({
  resetDiarySelected: () => dispatch(actions.selectDiary(null)),
  getDiariesType: () => dispatch(getDiariesTypeThunk()),
  deleteDiary: (society_Id) => {
    dispatch(deleteDiaryThunk());
    dispatch(tables.unselectAllRows(getTableName(society_Id, pageName)));
  },
  getDiaryDetail: () => dispatch(getDiaryDetailThunk()),
  getDiaries: () => dispatch(getDiariesThunk()),
  resetForm: form => dispatch(reset(form))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    currentCompanyName, dairyTableData, tableName
  } = stateProps;

  const baseFileName = createFormattedFileName(currentCompanyName, tableName);
  const dataToExport = formatTableDataToCSVData(dairyTableData);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    baseFileName,
    dataToExport,
    csvData: dataToExport
  };
};
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(DiaryScreen);
