import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';
import actions from 'redux/tax/actions';
import CIRefundScreen from 'components/screens/CIRefund';
import { getSheetOfDocs as getSheetOfDocsThunk } from 'redux/balance';

const mapStateToProps = state => ({
  societyId: state.navigation.id,
  headerValues: getFormValues(`${state.navigation.id}TaxDeclarationHeader`)(state),
  sheet: state.balance.sheet
});

const mapDispatchToProps = dispatch => ({
  selectFormId: (societyId, form_id) => dispatch(actions.selectFormId(societyId, form_id)),
  getSheetOfDocs: () => dispatch(getSheetOfDocsThunk)
});
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { societyId } = stateProps;
  const { selectFormId, getSheetOfDocs } = dispatchProps;
  const selectForm = value => selectFormId(societyId, value);
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    selectForm,
    getSheetOfDocs
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CIRefundScreen);
