import { compose } from 'redux';
import { connect } from 'react-redux';
import NewLetterScreen from 'components/screens/NewLetter';
import { reduxForm, getFormValues } from 'redux-form';
import { getLetter, postLetters as postLettersThunk } from 'redux/letters';

const mapStateToProps = state => ({
  initialValues: {
    type: 'cabinet'
  },
  lettersForm: getFormValues('newLetterForm')(state)
});

const mapDispatchToProps = dispatch => ({
  loadData: idLetter => dispatch(getLetter(idLetter)),
  postLetters: payload => dispatch(postLettersThunk(payload))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    lettersForm,
    ...otherStateProps
  } = stateProps;

  const {
    postLetters
  } = dispatchProps;

  const {
    change
  } = ownProps;

  const onSelectParagraph = (value, caret) => {
    const currentPageName = `body${caret.page}`;
    const currentPage = lettersForm[currentPageName] || '';
    const newPageText = `${currentPage.substring(0, caret.carretPosition)} ${value.paragraph_text} ${currentPage.substring(caret.carretPosition)}`;
    change(currentPageName, newPageText);
  };

  const onSave = (payload) => {
    console.log(payload);
    postLetters(payload);
  };

  return {
    ...otherStateProps,
    ...dispatchProps,
    ...ownProps,
    onSave,
    onSelectParagraph
  };
};

const enhance = compose(
  reduxForm({
    form: 'newLetterForm',
    destroyOnUnmount: false
  }),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
);

export default enhance(NewLetterScreen);
