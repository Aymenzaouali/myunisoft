import AccountingPlans from 'components/screens/AccountingPlans';
import { connect } from 'react-redux';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const mergeProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AccountingPlans);
