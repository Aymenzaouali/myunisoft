import { connect } from 'react-redux';
import CVAEAdvance from 'components/screens/CVAEAdvance';

const mapStateToProps = (state) => {
  const societyId = state.navigation.id;
  return {
    societyId,
    sheet: state.balance.sheet
  };
};

const mapDispatchToProps = () => ({});
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { getSheetOfDocs } = dispatchProps;
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    getSheetOfDocs
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CVAEAdvance);
