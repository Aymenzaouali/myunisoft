import { connect } from 'react-redux';
import DashboardScreen from 'components/screens/Dashboard';
import {
  getCharts as getChartsThunk
} from 'redux/dashboard';
import {
  getRights
} from 'redux/rights';
import { push } from 'connected-react-router';
import { redirectFromCollab as redirectFromCollabThunk } from 'redux/navigation';

const mapStateToProps = state => ({
  societyId: state.navigation.id
});

const mapDispatchToProps = dispatch => ({
  getCharts: idSociety => dispatch(getChartsThunk(idSociety)),
  redirect: route => dispatch(push(route)),
  redirectFromCollab: (route, type, id) => dispatch(redirectFromCollabThunk(route, type, id)),
  getRights: () => dispatch(getRights())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(DashboardScreen);
