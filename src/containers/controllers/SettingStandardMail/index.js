import { connect } from 'react-redux';
import SettingStandardMail from 'components/screens/SettingStandardMail';
import { PARAGRAPHS_TYPES_TABLE, getTableName } from 'assets/constants/tableName';
import _ from 'lodash';


// Component
const mapStateToProps = (state) => {
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, PARAGRAPHS_TYPES_TABLE);
  return {
    isParagraphVisible: state.settingStandardMail.isParagraphVisible,
    selectedRowsCount: _.get(state, `tables.${tableName}.selectedRowsCount`)
  };
};

export default connect(mapStateToProps, null)(SettingStandardMail);
