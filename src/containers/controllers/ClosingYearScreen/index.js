import { connect } from 'react-redux';

import { getCurrentTabState } from 'helpers/tabs';

import { closingYear } from 'redux/tabs/closing';
import { getExercises } from 'redux/tabs/exercises';

import ClosingYearScreen from 'components/screens/ClosingYearScreen';

// Component
const mapStateToProps = (state) => {
  const { closing, exercises } = getCurrentTabState(state);
  const exercise = exercises && exercises.find(e => e.label === 'N');

  return {
    society_id: state.navigation.id,
    no_exercises: exercises != null && exercises.length === 0,
    exercise,

    loading: closing.loading,
    error: closing.error
  };
};

const mapDispatchToProps = dispatch => ({
  closingYear: (id_exercice, options) => dispatch(closingYear(id_exercice, options)),
  getExercises: society_id => dispatch(getExercises(society_id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ClosingYearScreen);
