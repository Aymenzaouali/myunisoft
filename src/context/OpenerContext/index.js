import React, { useContext } from 'react';
import PropTypes from 'prop-types';

// Default values
const openerDefaults = {
  message: {},
  from: '',
  to: '',

  isPopup: false,

  sendAction: () => {},
  sendMessage: () => {},
  focusOpener: () => {}
};

// Context
const OpenerContext = React.createContext(openerDefaults);

// HOC
const openerWrap = (WrappedComponent) => {
  const wrappedName = WrappedComponent.displayName || WrappedComponent.name || 'Component';
  const wrapper = (props) => {
    const opener = useContext(OpenerContext);

    return (
      <WrappedComponent {...props} opener={opener} />
    );
  };

  wrapper.displayName = `OpenerWrap(${wrappedName})`;

  return wrapper;
};

// Shape
const openerShape = PropTypes.shape({
  from: PropTypes.string.isRequired, /** id of the sender of the last message */
  to: PropTypes.string.isRequired, /** id of the destination of the last message */
  message: PropTypes.shape({}).isRequired, /** last message received from the opener window (defaults to empty object) */ // eslint-disable-line max-len

  isPopup: PropTypes.bool.isRequired, /** true if is in popup */

  sendAction: PropTypes.func.isRequired, /** sends a action (redux) to the opener window */
  sendMessage: PropTypes.func.isRequired, /** sends a message to the opener window */
  focusOpener: PropTypes.func.isRequired /** give the focus to the opener window */
});

export default OpenerContext;
export {
  openerDefaults,
  openerWrap, openerShape
};
