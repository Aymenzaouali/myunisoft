import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Default value
const splitDefaults = {
  master_id: '',
  master: false,
  slave: false,
  active: false,
  popuped: false,
  swapped: false,
  swappable: true,

  args: {},
  slaves: [],

  onActivateSplit: () => {},
  onPopupSplit: () => {},
  onSwapSplit: () => {},
  focusOther: () => {},
  updateArgsSplit: () => {},
  onCloseSplit: () => {},
  closeSlave: () => {}
};

// Context
const SplitContext = React.createContext(splitDefaults);

// Wrapper (HOC)
const splitWrap = (WrappedComponent) => {
  const wrappedName = WrappedComponent.displayName || WrappedComponent.name || 'Component';

  return class extends PureComponent {
    static displayName = `SplitWrap(${wrappedName})`;

    render() {
      const { props } = this;

      return (
        <SplitContext.Consumer>
          {split => (
            <WrappedComponent {...props} split={{ ...split }} />
          )}
        </SplitContext.Consumer>
      );
    }
  };
};

// Shape
const splitShape = PropTypes.shape({
  master_id: PropTypes.string.isRequired, /** id of the master */
  active: PropTypes.bool.isRequired, /** true if the split is active */
  master: PropTypes.bool.isRequired, /** true if you're within the master */
  slave: PropTypes.bool.isRequired, /** true if you're within the slave */
  popuped: PropTypes.bool.isRequired, /** true if slave is in an other navigator tab */
  swapped: PropTypes.bool.isRequired, /** true if master and slave are swapped */
  swappable: PropTypes.bool.isRequired, /** allows master-slave swap */

  args: PropTypes.shape({}).isRequired, /** data coming from the master */

  slaves: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired, /** id of the slave */
      label: PropTypes.string.isRequired /** label of the slave */
    }).isRequired
  ).isRequired, /** Data on the current possible slaves */

  onActivateSplit: PropTypes.func.isRequired, /** activate a slave based on the given id */
  onPopupSplit: PropTypes.func.isRequired, /** send the current part to an other navigator tab */
  onSwapSplit: PropTypes.func.isRequired, /** swaps the split invert positions and roles */
  focusOther: PropTypes.func.isRequired, /** give the focus to the other tab (if popuped) */
  updateArgsSplit: PropTypes.func.isRequired, /** allows the master to change the arguments */
  onCloseSplit: PropTypes.func.isRequired, /** close the current window */
  closeSlave: PropTypes.func.isRequired /** close current active slave */
});

export default SplitContext;
export {
  splitDefaults,
  splitShape, splitWrap
};
