import { groupValidationState } from 'helpers/dadp';

describe('[Helpers][dadp]', () => {
  // Tests groupValidationState
  test('groupValidationState (empty)', () => {
    const data = [];

    expect(groupValidationState('valid_collab', data)).toBeNull();
    expect(groupValidationState('valid_rm', data)).toBeNull();
  });
  test('groupValidationState (full to_do)', () => {
    const data = [
      { valid_collab: 'to_do', valid_rm: 'to_do' },
      { valid_collab: 'to_do', valid_rm: 'to_do' },
      { valid_collab: 'to_do', valid_rm: 'to_do' },
      { valid_collab: 'to_do', valid_rm: 'to_do' },
      { valid_collab: 'to_do', valid_rm: 'to_do' }
    ];

    expect(groupValidationState('valid_collab', data)).toBe('to_do');
    expect(groupValidationState('valid_rm', data)).toBe('to_do');
  });
  test('groupValidationState (full to_validate)', () => {
    const data = [
      { valid_collab: 'to_validate', valid_rm: 'to_validate' },
      { valid_collab: 'to_validate', valid_rm: 'to_validate' },
      { valid_collab: 'to_validate', valid_rm: 'to_validate' },
      { valid_collab: 'to_validate', valid_rm: 'to_validate' },
      { valid_collab: 'to_validate', valid_rm: 'to_validate' }
    ];

    expect(groupValidationState('valid_collab', data)).toBe('to_validate');
    expect(groupValidationState('valid_rm', data)).toBe('to_validate');
  });
  test('groupValidationState (mix to_do & to_validate)', () => {
    const data = [
      { valid_collab: 'to_do', valid_rm: 'to_do' },
      { valid_collab: 'to_validate', valid_rm: 'to_do' },
      { valid_collab: 'to_validate', valid_rm: 'to_validate' },
      { valid_collab: 'to_do', valid_rm: 'to_validate' },
      { valid_collab: 'to_validate', valid_rm: 'to_validate' }
    ];

    expect(groupValidationState('valid_collab', data)).toBe('to_do');
    expect(groupValidationState('valid_rm', data)).toBe('to_do');
  });
  test('groupValidationState (some ko)', () => {
    const data = [
      { valid_collab: 'to_do', valid_rm: 'ok' },
      { valid_collab: 'ko', valid_rm: 'to_do' },
      { valid_collab: 'running', valid_rm: 'to_validate' },
      { valid_collab: 'to_co', valid_rm: 'to_validate' },
      { valid_collab: 'to_validate', valid_rm: 'ko' }
    ];

    expect(groupValidationState('valid_collab', data)).toBe('ko');
    expect(groupValidationState('valid_rm', data)).toBe('ko');
  });
  test('groupValidationState (full ok)', () => {
    const data = [
      { valid_collab: 'ok', valid_rm: 'ok' },
      { valid_collab: 'ok', valid_rm: 'ok' },
      { valid_collab: 'ok', valid_rm: 'ok' },
      { valid_collab: 'ok', valid_rm: 'ok' },
      { valid_collab: 'ok', valid_rm: 'ok' }
    ];

    expect(groupValidationState('valid_collab', data)).toBe('ok');
    expect(groupValidationState('valid_rm', data)).toBe('ok');
  });
  test('groupValidationState (mix of all except ko)', () => {
    const data = [
      { valid_collab: 'to_do', valid_rm: 'running' },
      { valid_collab: 'running', valid_rm: 'to_validate' },
      { valid_collab: 'to_validate', valid_rm: 'ok' },
      { valid_collab: 'ok', valid_rm: 'to_do' },
      { valid_collab: 'running', valid_rm: 'ok' }
    ];

    expect(groupValidationState('valid_collab', data)).toBe('running');
    expect(groupValidationState('valid_rm', data)).toBe('running');
  });
});
