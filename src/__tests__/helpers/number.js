import { roundNumber, formatNumber } from 'helpers/number';
import 'numeral/locales/fr';
import numeral from 'numeral';

numeral.locale('fr');

describe('[Helpers][number]', () => {
  test('roundNumber : no decimal', () => {
    expect(roundNumber(2)).toEqual(2);
  });
  test('roundNumber : one decimal', () => {
    expect(roundNumber('2.1')).toEqual(2.1);
  });
  test('roundNumber : two decimal', () => {
    expect(roundNumber('2.44')).toEqual(2.44);
  });
  test('roundNumber : three decimal, third is lower', () => {
    expect(roundNumber('2.443')).toEqual(2.44);
  });
  test('roundNumber : three decimal, third is higher', () => {
    expect(roundNumber('2.446')).toEqual(2.45);
  });
  test('roundNumber : three decimal, third is middle', () => {
    expect(roundNumber('2.445')).toEqual(2.45);
  });
  test('formatNumber : two numbers no decimal', () => {
    expect(formatNumber(24)).toEqual('24,00');
  });
  test('formatNumber : two numbers two decimals', () => {
    expect(formatNumber(24.24)).toEqual('24,24');
  });
  test('formatNumber : thounsands', () => {
    expect(formatNumber(1000)).toEqual('1 000,00');
  });
  test('formatNumber : thounsands and decimals', () => {
    expect(formatNumber(1000.23)).toEqual('1 000,23');
  });
  test('formatNumber : 0', () => {
    expect(formatNumber(0)).toEqual('0,00');
  });
  test('formatNumber : currency', () => {
    expect(formatNumber(0, '0,0.00 $')).toEqual('0,00 €');
  });
});
