import { percentageFormat } from 'helpers/charts';

describe('[Helpers][charts]', () => {
  test('Test for charts', () => {
    expect(percentageFormat(
      [
        {
          x: 'Month_1',
          y: 100,
          z: '201901'
        },
        {
          x: 'Month_2',
          y: 50,
          z: '201902'
        }
      ],
      [
        {
          x: 'Month_1',
          y: 100,
          z: '201901'
        },
        {
          x: 'Month_2',
          y: 10,
          z: '201902'
        }
      ]
    )).toEqual([
      {
        x: 'Month_1',
        y: 100,
        z: '201901'
      },
      {
        x: 'Month_2',
        y: 20,
        z: '201902'
      }
    ]);
  });
});
