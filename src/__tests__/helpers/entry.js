import { calculTotal, defaultAccountType } from 'helpers/entry';

describe('[Helpers][entry]', () => {
  test('calculTotal : With values', () => {
    expect(calculTotal({ a: 10, b: 20 })).toBe(30);
  });
  test('calculTotal : Without values', () => {
    expect(calculTotal()).toBe(0);
  });
  test('defaultAccountType : With a journal d\'achat and account dont start with 40', () => {
    const currentRowMock = {
      account: { account_number: '300030302' }
    };
    const currentEntriesMock = [{
      account: { account_number: '300030302' }
    }];
    expect(defaultAccountType(1, currentRowMock, currentEntriesMock)).toEqual('debit');
  });
  test('defaultAccountType : With a journal d\'achat, currentRow is 40, and no value for debit and credit', () => {
    const currentRowMock = {
      account: { account_number: '401938927' }
    };
    const currentEntriesMock = [{
      account: { account_number: '401938927' }
    }];
    expect(defaultAccountType(1, currentRowMock, currentEntriesMock)).toEqual('credit');
  });
  test('defaultAccountType : With a journal d\'achat, there is an account 40, and no value for debit and credit', () => {
    const currentRowMock = {
      account: { account_number: '300000' }
    };
    const currentEntriesMock = [{
      account: { account_number: '401938927' }
    }];
    expect(defaultAccountType(1, currentRowMock, currentEntriesMock)).toEqual('debit');
  });
  test('defaultAccountType : With a journal d\'achat, currentRow is 40, and value for debit', () => {
    const currentRowMock = {
      account: { account_number: '401938927' },
      debit: 20
    };
    const currentEntriesMock = [{
      account: { account_number: '401938927' }
    }];
    expect(defaultAccountType(1, currentRowMock, currentEntriesMock)).toEqual('debit');
  });
  test('defaultAccountType : With a journal d\'achat, there is an account 40, and value for debit', () => {
    const currentRowMock = {
      account: { account_number: '3000000' }
    };
    const currentEntriesMock = [{
      account: { account_number: '401938927' },
      debit: 20
    }];
    expect(defaultAccountType(1, currentRowMock, currentEntriesMock)).toEqual('credit');
  });
  test('defaultAccountType : With a journal de vente, there is no account 41', () => {
    const currentRowMock = {
      account: { account_number: '3000000' }
    };
    const currentEntriesMock = [{
      account: { account_number: '401938927' }
    }];
    expect(defaultAccountType(2, currentRowMock, currentEntriesMock)).toEqual('debit');
  });
  test('defaultAccountType : With a journal de vente, current row is 41, with no value for debit and credit', () => {
    const currentRowMock = {
      account: { account_number: '410000000' }
    };
    const currentEntriesMock = [{
      account: { account_number: '401938927' }
    }];
    expect(defaultAccountType(2, currentRowMock, currentEntriesMock)).toEqual('debit');
  });
  test('defaultAccountType : With a journal de vente, there is an account 41, with no value for debit and credit', () => {
    const currentRowMock = {
      account: { account_number: '1003303' }
    };
    const currentEntriesMock = [{
      account: { account_number: '410000000' }
    }];
    expect(defaultAccountType(2, currentRowMock, currentEntriesMock)).toEqual('credit');
  });
  test('defaultAccountType : With a journal de vente, current row is 41, there is value for credit', () => {
    const currentRowMock = {
      account: { account_number: '410000000' },
      credit: 20
    };
    const currentEntriesMock = [{
      account: { account_number: '401938927' }
    }];
    expect(defaultAccountType(2, currentRowMock, currentEntriesMock)).toEqual('credit');
  });
  test('defaultAccountType : With a journal de vente, there is an account 41, there is value for credit', () => {
    const currentRowMock = {
      account: { account_number: '302002' }
    };
    const currentEntriesMock = [{
      account: { account_number: '410000000' },
      credit: 20
    }];
    expect(defaultAccountType(2, currentRowMock, currentEntriesMock)).toEqual('debit');
  });
  test('defaultAccountType : With a journal de banque, current row is 41', () => {
    const currentRowMock = {
      account: { account_number: '410000000' }
    };
    const currentEntriesMock = [{
      account: { account_number: '410000000' }
    }];
    expect(defaultAccountType(3, currentRowMock, currentEntriesMock)).toEqual('credit');
  });
  test('defaultAccountType : With a random journal, current row is 7', () => {
    const currentRowMock = {
      account: { account_number: '7' }
    };
    const currentEntriesMock = [{
      account: { account_number: '410000000' }
    }];
    expect(defaultAccountType(5, currentRowMock, currentEntriesMock)).toEqual('credit');
  });
  test('defaultAccountType : With a random journal, current row is 6', () => {
    const currentRowMock = {
      account: { account_number: '6' }
    };
    const currentEntriesMock = [{
      account: { account_number: '410000000' }
    }];
    expect(defaultAccountType(5, currentRowMock, currentEntriesMock)).toEqual('debit');
  });
});
