import { verifyDebitCredit } from 'helpers/validation';

describe('[Helpers][validation]', () => {
  test('verifyDebitCredit: should be equal', () => {
    const mock = [
      {
        debit: 2,
        credit: 0
      },
      {
        debit: 0,
        credit: 2
      },
      {
        debit: 2.23,
        credit: 0
      },
      {
        debit: 0,
        credit: 2.23
      }
    ];
    expect(verifyDebitCredit(mock)).toBeTruthy();
  });
  test('verifyDebitCredit: should not be equal', () => {
    const mock = [
      {
        debit: 2,
        credit: 0
      },
      {
        debit: 0,
        credit: 3
      },
      {
        debit: 2.23,
        credit: 0
      },
      {
        debit: 0,
        credit: 2.32
      }
    ];
    expect(verifyDebitCredit(mock)).toBeFalsy();
  });
});
