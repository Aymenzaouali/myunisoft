import { parse, sum, sub } from 'helpers/pdfforms';

describe('[Helpers][pdfforms]', () => {
  const data = {
    A: '10',
    B: '15',
    C: 5,
    D: 'blabla',
    E: ''
  };

  // parse
  test('parse (empty)', () => {
    expect(parse('')).toBe(0);
  });
  test('parse (text)', () => {
    expect(parse('78.25')).toBe(78.25);
  });
  test('parse (number)', () => {
    expect(parse(78.25)).toBe(78.25);
  });
  test('parse (invalid)', () => {
    expect(parse('blabla')).toBe(0);
  });

  // sum
  test('sum (1 empty value)', () => {
    expect(sum(data, ['E'])).toBe(0);
  });
  test('sum (1 text value)', () => {
    expect(sum(data, ['A'])).toBe(10);
  });
  test('sum (1 number value)', () => {
    expect(sum(data, ['C'])).toBe(5);
  });
  test('sum (1 invalid value)', () => {
    expect(sum(data, ['D'])).toBe(0);
  });
  test('sum (all values)', () => {
    expect(sum(data, ['A', 'B', 'C', 'D', 'E'])).toBe(30);
  });

  // sub
  test('sub (1 empty value)', () => {
    expect(sub(data, ['E'])).toBe(0);
  });
  test('sub (1 text value)', () => {
    expect(sub(data, ['A'])).toBe(10);
  });
  test('sub (1 number value)', () => {
    expect(sub(data, ['C'])).toBe(5);
  });
  test('sub (1 invalid value)', () => {
    expect(sub(data, ['D'])).toBe(0);
  });
  test('sub (all values)', () => {
    expect(sub(data, ['A', 'B', 'C', 'D', 'E'])).toBe(-10);
  });
});
