import { LOCATION_CHANGE } from 'connected-react-router';

import {
  formatTabId,
  getTabState,
  getCurrentTabState,
  tabFormName,
  getNewTab,
  isChangingTab
} from 'helpers/tabs';

describe('[Helpers][tabs]', () => {
  // formatTabId
  test('formatTabId : number', () => {
    expect(formatTabId(5)).toEqual('5');
  });
  test('formatTabId : string', () => {
    expect(formatTabId('5')).toEqual('5');
  });
  test('formatTabId : collab', () => {
    expect(formatTabId('collab')).toEqual('collab');
  });
  test('formatTabId : -2 => collab', () => {
    expect(formatTabId(-2)).toEqual('collab');
  });

  // getTabState
  test('getTabState : exists', () => {
    const state = {
      tabs: {
        5: {
          test: 'test'
        }
      }
    };

    expect(getTabState(state, 5)).toEqual({ test: 'test' });
  });
  test('getTabState : not found', () => {
    const state = {
      tabs: {
        5: {
          test: 'test'
        }
      }
    };

    expect(getTabState(state, 8)).toEqual({});
  });

  // getCurrentTabState
  test('getCurrentTabState : exists', () => {
    const state = {
      navigation: {
        id: 5
      },
      tabs: {
        5: {
          test: 'test'
        }
      }
    };

    expect(getCurrentTabState(state)).toEqual({ test: 'test' });
  });
  test('getCurrentTabState : not found', () => {
    const state = {
      navigation: {
        id: 8
      },
      tabs: {
        5: {
          test: 'test'
        }
      }
    };

    expect(getCurrentTabState(state)).toEqual({});
  });

  // isChangingTab
  test('isChangingTab [false]', () => {
    const action = {
      type: LOCATION_CHANGE,
      payload: {
        location: {
          pathname: '/tab/5/test'
        }
      }
    };
    const state = {
      navigation: {
        id: 5
      }
    };

    expect(isChangingTab(action, state)).toBeFalsy();
  });
  test('isChangingTab [true]', () => {
    const action = {
      type: LOCATION_CHANGE,
      payload: {
        location: {
          pathname: '/tab/78/test'
        }
      }
    };
    const state = {
      navigation: {
        id: 5
      }
    };

    expect(isChangingTab(action, state)).toBeTruthy();
  });

  // others
  test('tabFormName', () => {
    expect(tabFormName('test', 7)).toEqual('test_7');
  });
  test('getNewTab', () => {
    const action = {
      type: LOCATION_CHANGE,
      payload: {
        location: {
          pathname: '/tab/78/test'
        }
      }
    };

    expect(getNewTab(action)).toEqual('78');
  });
});
