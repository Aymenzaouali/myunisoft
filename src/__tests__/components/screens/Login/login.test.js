import React from 'react';
import Login from 'components/screens/Login';
import renderer from 'react-test-renderer';
import { reduxForm } from 'redux-form';


jest.mock('containers/groups/Dialogs/Groups', () => () => (
  <div id="mockUserCom">
   mockUserCom
  </div>
));


describe('[Component][Basics][Avatar]', () => {
  test('Component should render', () => {
    const props = {
      handleSubmit: jest.fn,
      groups: []
    };
    const wrappedLogin = reduxForm({
      form: 'loginForm',
      destroyOnUnmount: false,
      enableReinitialize: true
    })(Login);
    const tree = renderer.create(<wrappedLogin {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
