import React from 'react';
import { render } from 'enzyme';
import toJson from 'enzyme-to-json';
import FooterCell from 'components/basics/Cells/FooterCell';


jest.mock('components/basics/Cells/StringCell', () => () => (
  <div id="String">
   StringComponent
  </div>
));

describe('[Component][Basics][Cell][FooterCell]', () => {
  test('should be defined', () => {
    expect(FooterCell).toBeDefined();
  });

  test('Component should render with no props', () => {
    const wrapper = render(<FooterCell />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render for custom Cell', () => {
    const wrapper = render(<FooterCell><span>Test custom</span></FooterCell>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render for string type', () => {
    const wrapper = render(
      <FooterCell
        type="string"
        keyCell="header2 cell2"
        props={{ label: 'testString' }}
      />
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
