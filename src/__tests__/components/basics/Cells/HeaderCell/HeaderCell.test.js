import React from 'react';
import { render } from 'enzyme';
import toJson from 'enzyme-to-json';

import HeaderCell from 'components/basics/Cells/HeaderCell';


jest.mock('components/basics/Cells/StringCell', () => () => (
  <div id="String">
   StringComponent
  </div>
));

describe('[Component][Basics][Cell][HeaderCell]', () => {
  test('should be defined', () => {
    expect(HeaderCell).toBeDefined();
  });

  test('Component should render with no props', () => {
    const wrapper = render(<HeaderCell />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render for custom Cell', () => {
    const wrapper = render(<HeaderCell><span>Test custom</span></HeaderCell>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render for string Cell', () => {
    const wrapper = render(
      <HeaderCell
        type="string"
        keyCell="header2 cell2"
        props={{ label: 'testString' }}
      />
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
