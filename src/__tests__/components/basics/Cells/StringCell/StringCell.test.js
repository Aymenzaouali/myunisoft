import React from 'react';
import { render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import StringCell from 'components/basics/Cells/StringCell';

// Props use for test
const props = {
  label: 'test',
  className: 'test',
  value: 0,
  onClick: () => {}
};


describe('[Component][Basics][Cell][String]', () => {
  test('should be defined', () => {
    expect(StringCell).toBeDefined();
  });

  test('Component should render with no props', () => {
    const wrapper = render(<StringCell />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render with all this.props', () => {
    const wrapper = render(<StringCell {...props} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component style with no props', () => {
    const wrapper = mount(<StringCell />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component style with all this.props', () => {
    const wrapper = mount(<StringCell {...props} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component Test onclick', () => {
    const mockAction = jest.fn();

    const wrapper = mount(<StringCell {...props} onClick={mockAction} />);
    wrapper.find('Button').at(0).simulate('click');
    expect(mockAction.mock.calls.length).toEqual(1);
  });
});
