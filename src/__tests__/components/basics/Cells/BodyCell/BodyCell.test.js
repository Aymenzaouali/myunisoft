import React from 'react';
import { render } from 'enzyme';
import toJson from 'enzyme-to-json';
import BodyCell from 'components/basics/Cells/BodyCell';


jest.mock('components/basics/Cells/StringCell', () => () => (
  <div id="String">
   StringComponent
  </div>
));

describe('[Component][Basics][Cell][BodyCell]', () => {
  test('should be defined', () => {
    expect(BodyCell).toBeDefined();
  });

  test('Component should render with no props', () => {
    const wrapper = render(<BodyCell />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render for custom Cell', () => {
    const wrapper = render(<BodyCell><span>Test custom</span></BodyCell>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render for string type', () => {
    const wrapper = render(
      <BodyCell
        type="string"
        keyCell="header2 cell2"
        props={{ label: 'testString' }}
      />
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
