import React from 'react';
import {
  render,
  mount
} from 'enzyme';
import toJson from 'enzyme-to-json';
import SelectCell from 'components/basics/Cells/SelectCell';

// Props use for test
const props = {
  onChange: jest.fn(),
  className: 'testSelectCell',
  value: '',
  renderList: () => {},
  list: [
    {
      value: 'toto'
    },
    {
      value: 'tata'
    },
    {
      value: 'titi'
    },
    {
      value: 'tutu'
    }
  ]
};


describe('[Component][Basics][Cell][SelectCell]', () => {
  test('should be defined', () => {
    expect(SelectCell).toBeDefined();
  });

  test('Component should render', () => {
    const wrapper = render(<SelectCell {...props} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component Test renderList', () => {
    const wrappedComponent = mount(<SelectCell {...props} />);
    const select = wrappedComponent.find('Select');
    select.at(0).props().onChange();
    expect(props.onChange).toHaveBeenCalled();
  });
});
