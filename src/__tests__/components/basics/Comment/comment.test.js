import React from 'react';
import { render } from 'enzyme';
import toJson from 'enzyme-to-json';

import Comment from 'components/basics/Comment';

// Props required
const defaultProps = {
  avatar: {
    initial: 'TT'
  },
  userName: 'Toto Toto'
};

// Props read complet
const fullReadProps = {
  ...defaultProps,
  comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  date: '10/01/2010'
};

// Props edit complet
const fullEditProps = {
  ...fullReadProps,
  edit: true,
  actionEdit: jest.fn()
};

describe('[Component][Basics][Comment]', () => {
  test('should be defined', () => {
    expect(Comment).toBeDefined();
  });

  test('Component should render with props required', () => {
    const wrapper = render(<Comment {...defaultProps} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render with full read props', () => {
    const wrapper = render(<Comment {...fullReadProps} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render with full edit props', () => {
    const wrapper = render(<Comment {...fullEditProps} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
