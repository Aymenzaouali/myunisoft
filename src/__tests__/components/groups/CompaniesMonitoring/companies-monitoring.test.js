import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Provider } from 'react-redux';
import { store } from 'redux/store';
// import { getSpecWrapper } from 'helpers/unitTests';
import CompaniesMonitoring from 'components/groups/CompaniesMonitoring';

describe('[Components][Groups][CompaniesMonitoring]', () => {
  test('Component should render', () => {
    const wrapper = mount(
      <Provider store={store}>
        <CompaniesMonitoring />
      </Provider>
    );

    expect(toJson(wrapper)).toMatchSnapshot();
  });

  // test('Component should have 2 display type buttons', () => {
  //   const wrapper = shallow(
  //     <Provider store={store}>
  //       <CompaniesMonitoring />
  //     </Provider>
  //   );
  //   const displayTypeButtonWrappers = getSpecWrapper(wrapper, 'display-type-button');

  //   expect(displayTypeButtonWrappers).toHaveLength(2);
  // });


  // test.skip('Component should have 4 history checkboxes filters', () => {
  //   const wrapper = mount(
  //     <Provider store={store}>
  //       <CompaniesMonitoring />
  //     </Provider>
  //   );
  //   const displayTypeButtonWrappers = getSpecWrapper(wrapper, 'display-type-button');

  //   displayTypeButtonWrappers.at(1).simulate('click');

  //   const historyCheckboxesFilters = getSpecWrapper(wrapper, 'history-filter-checkbox');
  //   const historyFilterCheckboxLength = Object.keys(
  //     wrapper.state().historyFilters.checkboxes
  //   ).length;

  //   expect(historyCheckboxesFilters).toHaveLength(historyFilterCheckboxLength);
  // });
});
