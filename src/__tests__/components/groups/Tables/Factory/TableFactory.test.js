import React from 'react';
import { render } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Provider } from 'react-redux';
import { store } from 'redux/store';
import TableFactory from 'components/groups/Tables/Factory';

// Props use for test

const mockHeader = {
  props: {},
  row: [{
    keyRow: 'row1',
    props: {},
    value: [{
      type: 'string',
      keyCell: 'header1 cell1',
      props: {
        label: 'header1 cell1'
      }
    }, {
      type: 'string',
      keyCell: 'header1 cell2',
      props: {
        label: 'header1 cell2'
      }
    }, {
      type: 'string',
      keyCell: 'header1 cell3',
      props: {
        label: 'header1 cell3'
      }
    }]
  },
  {
    keyRow: 'row2',
    props: {},
    value: [{
      type: 'string',
      keyCell: 'header2 cell1',
      props: {
        label: 'header2 cell1'
      }
    }, {
      type: 'string',
      keyCell: 'header2 cell2',
      props: {
        label: 'header2 cell2'
      }
    }, {
      type: 'string',
      keyCell: 'header2 cell3',
      props: {
        label: 'header2 cell3'
      }
    }]
  }
  ]
};

const mockBody = {
  props: {},
  row: [{
    keyRow: 'row3',
    props: {},
    value: [{
      type: 'string',
      keyCell: 'body-1 cell-1',
      props: {
        label: 'body-1 cell-1'
      }
    }, {
      type: 'string',
      keyCell: 'body-1 cell-2',
      props: {
        label: 'body-1 cell-2'
      }
    }, {
      type: 'string',
      keyCell: 'body-1 cell-3',
      props: {
        label: 'body-1 cell-3'
      }
    }]
  },
  {
    keyRow: 'row4',
    props: {},
    value: [{
      type: 'string',
      keyCell: 'body-2 cell-1',
      props: {
        label: 'body-2 cell-1'
      }
    }, {
      type: 'string',
      keyCell: 'body-2 cell-2',
      props: {
        label: 'body-2 cell-2'
      }
    }, {
      type: 'string',
      keyCell: 'body-2 cell-3',
      props: {
        label: 'body-2 cell-3'
      }
    }]
  }]
};

const mockFooter = {
  props: {},
  row: [{
    keyRow: 'row5',
    props: {},
    value: [{
      type: 'string',
      keyCell: 'footer-1 cell-1',
      props: {
        label: 'footer-1 cell-1'
      }
    }, {
      type: 'none',
      keyCell: 'none1'
    }, {
      type: 'string',
      keyCell: 'footer-1 cell-3',
      props: {
        label: 'footer-1 cell-3'
      }
    }]
  },
  {
    keyRow: 'row6',
    props: {},
    value: [{
      type: 'string',
      keyCell: 'footer-2 cell-1',
      props: {
        label: 'footer-2 cell-1'
      }
    }, {
      type: 'string',
      keyCell: 'footer-2 cell-2',
      props: {
        label: 'footer-2 cell-2'
      }
    }, {
      type: 'string',
      keyCell: 'footer-2 cell-3',
      props: {
        label: 'footer-2 cell-3'
      }
    }]
  }]
};


describe('[Component][Groups][Table][Factory]', () => {
  // test('should be defined', () => {
  //   expect(TableFactory).toBeDefined();
  // });

  test('Component should render with section header', () => {
    const wrapper = render(
      <Provider store={store}>
        <TableFactory param={{ header: mockHeader }} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render with section body', () => {
    const wrapper = render(
      <Provider store={store}>
        <TableFactory param={{ body: mockBody }} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render with section footer', () => {
    const wrapper = render(
      <Provider store={store}>
        <TableFactory param={{ footer: mockFooter }} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render with section header/body', () => {
    const wrapper = render(
      <Provider store={store}>
        <TableFactory param={{ header: mockHeader, body: mockBody }} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render with section header/footer', () => {
    const wrapper = render(
      <Provider store={store}>
        <TableFactory param={{ header: mockHeader, footer: mockFooter }} />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render with section header/body/footer', () => {
    const wrapper = render(
      <Provider store={store}>
        <TableFactory
          param={{
            header: mockHeader,
            body: mockBody,
            footer: mockFooter
          }}
        />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  test('Component should render with section body/footer', () => {
    const wrapper = render(<TableFactory param={{ body: mockBody, footer: mockFooter }} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
