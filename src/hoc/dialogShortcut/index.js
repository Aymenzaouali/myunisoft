import React, { Fragment } from 'react';
import { Shortcuts } from 'react-shortcuts/lib';

export const withShortcut = () => WrappedComponent => class extends React.Component {
  handleShortcuts = (action, e) => {
    const {
      onValidate = () => {}, // eslint-disable-line
      onClose = () => {} // eslint-disable-line
    } = this.props;

    e.preventDefault();
    e.stopPropagation();
    switch (action) {
    case 'VALIDATE':
      onValidate();
      break;
    case 'CLOSE':
      onClose();
      break;
    default:
      break;
    }
  }

  render() {
    const { isOpen = false } = this.props; // eslint-disable-line

    return isOpen
      ? (
        <Shortcuts
          name="DIALOGS"
          tabIndex={-1}
          handler={this.handleShortcuts}
          alwaysFireHandler
          stopPropagation={false}
          preventDefault={false}
          targetNodeSelector="body"
        >
          <WrappedComponent {...this.props} />
        </Shortcuts>
      )
      : (
        <Fragment>
          <WrappedComponent {...this.props} />
        </Fragment>
      );
  }
};
