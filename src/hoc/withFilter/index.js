import React, { useState, useEffect } from 'react';

const withFilter = () => Component => (props) => {
  const { prefixed = '' } = props; //eslint-disable-line
  const [filter, setFilter] = useState(prefixed);
  const onInputChange = (q = '') => {
    setFilter(`${prefixed}${q}`);
  };

  useEffect(onInputChange, [prefixed]);

  return <Component {...props} filter={filter} onInputChange={onInputChange} />;
};

export default withFilter;
