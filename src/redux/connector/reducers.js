import _ from 'lodash';
import {
  GET_THIRD_API_ATTEMPT,
  GET_THIRD_API_SUCCESS,
  GET_THIRD_API_FAIL,

  GET_CONNECTOR_ATTEMPT,
  GET_CONNECTOR_SUCCESS,
  GET_CONNECTOR_FAIL,

  PUT_CONNECTOR_ATTEMPT,
  PUT_CONNECTOR_SUCCESS,
  PUT_CONNECTOR_FAIL,

  POST_CONNECTOR_ATTEMPT,
  POST_CONNECTOR_SUCCESS,
  POST_CONNECTOR_FAIL,

  ADD_CONNECTOR,
  REMOVE_CONNECTOR,

  RESET_ALL_CONNECTOR,
  ADD_ALL_CONNECTOR
} from './constants';

const initialState = {
  api: [],
  connector: [],
  list_selectedConnector: []
};

const connector = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_THIRD_API_ATTEMPT:
      return {
        ...state
      };
    case GET_THIRD_API_SUCCESS:
      const { api } = action;
      return {
        ...state,
        api
      };
    case GET_THIRD_API_FAIL:
      return {
        ...state
      };

    case GET_CONNECTOR_ATTEMPT:
      return {
        ...state,
        isLoading: true
      };
    case GET_CONNECTOR_SUCCESS:
      const { connector } = action;
      return {
        ...state,
        isLoading: false,
        connector
      };
    case GET_CONNECTOR_FAIL:
      return {
        ...state,
        isLoading: false,
        connector: []
      };

    case PUT_CONNECTOR_ATTEMPT:
      return {
        ...state
      };
    case PUT_CONNECTOR_SUCCESS:
      return {
        ...state
      };
    case PUT_CONNECTOR_FAIL:
      return {
        ...state
      };

    case POST_CONNECTOR_ATTEMPT:
      return {
        ...state
      };
    case POST_CONNECTOR_SUCCESS:
      return {
        ...state
      };
    case POST_CONNECTOR_FAIL:
      return {
        ...state
      };

    case ADD_CONNECTOR: {
      return {
        ...state,
        list_selectedConnector: {
          ...state.list_selectedConnector,
          [action.society_id]: [..._.get(state, `list_selectedConnector.${action.society_id}`, []), action.connector]
        }
      };
    }
    case REMOVE_CONNECTOR: {
      return {
        ...state,
        list_selectedConnector: {
          ...state.list_selectedConnector,
          [action.society_id]: _.get(state, `list_selectedConnector[${action.society_id}]`, []).filter(e => e.id_connector !== action.connector.id_connector)
        }
      };
    }

    case RESET_ALL_CONNECTOR: {
      return {
        ...state,
        list_selectedConnector: {
          ...state.list_selectedConnector,
          [action.society_id]: []
        }
      };
    }

    case ADD_ALL_CONNECTOR: {
      return {
        ...state,
        list_selectedConnector: {
          ...state.list_selectedConnector,
          [action.society_id]: _.get(state, 'connector', [])
        }
      };
    }
    default:
      return state;
    }
  }
  return state;
};

export default connector;
