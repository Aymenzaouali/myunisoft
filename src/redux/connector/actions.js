import {
  GET_THIRD_API_ATTEMPT,
  GET_THIRD_API_SUCCESS,
  GET_THIRD_API_FAIL,

  GET_CONNECTOR_ATTEMPT,
  GET_CONNECTOR_SUCCESS,
  GET_CONNECTOR_FAIL,

  PUT_CONNECTOR_ATTEMPT,
  PUT_CONNECTOR_SUCCESS,
  PUT_CONNECTOR_FAIL,

  POST_CONNECTOR_ATTEMPT,
  POST_CONNECTOR_SUCCESS,
  POST_CONNECTOR_FAIL,

  DELETE_CONNECTOR_ATTEMPT,
  DELETE_CONNECTOR_SUCCESS,
  DELETE_CONNECTOR_FAIL,

  ADD_CONNECTOR,
  REMOVE_CONNECTOR,

  RESET_ALL_CONNECTOR,
  ADD_ALL_CONNECTOR
} from './constants';

export const getThirdApiAttempt = () => ({
  type: GET_THIRD_API_ATTEMPT
});

export const getThirdApiSuccess = api => ({
  type: GET_THIRD_API_SUCCESS,
  api
});

export const getThirdApiFail = () => ({
  type: GET_THIRD_API_FAIL
});

export const getConnectorAttempt = () => ({
  type: GET_CONNECTOR_ATTEMPT
});

export const getConnectorSuccess = connector => ({
  type: GET_CONNECTOR_SUCCESS,
  connector
});

export const getConnectorFail = () => ({
  type: GET_CONNECTOR_FAIL
});

export const putConnectorAttempt = () => ({
  type: PUT_CONNECTOR_ATTEMPT
});

export const putConnectorSuccess = () => ({
  type: PUT_CONNECTOR_SUCCESS
});

export const putConnectorFail = () => ({
  type: PUT_CONNECTOR_FAIL
});

export const postConnectorAttempt = () => ({
  type: POST_CONNECTOR_ATTEMPT
});

export const postConnectorSuccess = () => ({
  type: POST_CONNECTOR_SUCCESS
});

export const postConnectorFail = () => ({
  type: POST_CONNECTOR_FAIL
});

export const deleteConnectorAttempt = () => ({
  type: DELETE_CONNECTOR_ATTEMPT
});

export const deleteConnectorSuccess = () => ({
  type: DELETE_CONNECTOR_SUCCESS
});

export const deleteConnectorFail = () => ({
  type: DELETE_CONNECTOR_FAIL
});

export const addConnector = (connector, society_id) => ({
  type: ADD_CONNECTOR,
  connector,
  society_id
});

export const removeConnector = (connector, society_id) => ({
  type: REMOVE_CONNECTOR,
  connector,
  society_id
});

export const resetAllConnector = society_id => ({
  type: RESET_ALL_CONNECTOR,
  society_id
});

export const addAllConnector = society_id => ({
  type: ADD_ALL_CONNECTOR,
  society_id
});
