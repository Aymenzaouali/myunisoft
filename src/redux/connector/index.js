import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import {
  getThirdApiAttempt,
  getThirdApiSuccess,
  getThirdApiFail,

  getConnectorAttempt,
  getConnectorSuccess,
  getConnectorFail,

  putConnectorAttempt,
  putConnectorSuccess,
  putConnectorFail,

  postConnectorAttempt,
  postConnectorSuccess,
  postConnectorFail,

  deleteConnectorAttempt,
  deleteConnectorSuccess,
  deleteConnectorFail
} from './actions';


export const getThirdApi = () => async (dispatch) => {
  try {
    await dispatch(getThirdApiAttempt());
    const { data } = await windev.makeApiCall('/third_party_api', 'get');
    await dispatch(getThirdApiSuccess(data));
    return data;
  } catch (err) {
    await dispatch(getThirdApiFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getConnector = () => async (dispatch) => {
  try {
    await dispatch(getConnectorAttempt());
    const { data } = await windev.makeApiCall('/connector', 'get');
    await dispatch(getConnectorSuccess(data));
    return data;
  } catch (err) {
    await dispatch(getConnectorFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const putConnector = payload => async (dispatch) => {
  const {
    id_connector,
    id_society,
    code,
    complementary_information,
    id_third_party_api
  } = payload;

  const body = {
    id_third_party_api,
    id_society,
    code,
    complementary_information
  };

  try {
    await dispatch(putConnectorAttempt());
    const { data } = await windev.makeApiCall('/connector', 'put', { id_connector }, body);
    await dispatch(putConnectorSuccess(data));
    return data;
  } catch (err) {
    await dispatch(putConnectorFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const postConnector = payload => async (dispatch) => {
  const {
    id_society,
    id_third_party_api,
    code,
    complementary_information
  } = payload;

  const body = {
    id_society,
    id_third_party_api,
    code,
    complementary_information
  };

  try {
    await dispatch(postConnectorAttempt());
    const { data } = await windev.makeApiCall('/connector', 'post', {}, body);
    await dispatch(postConnectorSuccess(data));
    return data;
  } catch (err) {
    await dispatch(postConnectorFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const deleteConnector = connectorList => async (dispatch) => {
  const connectors = connectorList.map(e => ({
    id_connector: e.id_connector
  }));
  const body = { connectors };
  try {
    await dispatch(deleteConnectorAttempt());
    const { data } = await windev.makeApiCall('/connector', 'delete', {}, body);
    await dispatch(deleteConnectorSuccess(data));
    return data;
  } catch (err) {
    await dispatch(deleteConnectorFail());
    await dispatch(handleError(err));
    return err;
  }
};
