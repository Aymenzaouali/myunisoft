import {
  closeDialogActionCreator as closeDialog,
  openDialogActionCreator as openDialog
} from './actions';

export {
  openDialog,
  closeDialog
};
