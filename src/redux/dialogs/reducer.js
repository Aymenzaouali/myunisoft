import { Stack } from 'helpers/stack';
import {
  OPEN_DIALOG,
  CLOSE_DIALOG
} from './constants';

const initialState = new Stack();

const dialogs = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case OPEN_DIALOG: {
      const updatedStack = new Stack(state.elements);
      updatedStack.push(action.payload);
      return updatedStack;
    }
    case CLOSE_DIALOG: {
      const updatedStack = new Stack(state.elements);
      updatedStack.pop();
      return updatedStack;
    }
    default:
      return state;
    }
  }
  return state;
};

export default dialogs;
