import {
  OPEN_DIALOG,
  CLOSE_DIALOG
} from './constants';

export const openDialogActionCreator = payload => ({
  type: OPEN_DIALOG,
  payload
});

export const closeDialogActionCreator = () => ({
  type: CLOSE_DIALOG
});
