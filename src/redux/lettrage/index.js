import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import { change } from 'redux-form';
import { updateSelect } from 'redux/consulting/actions';
import actions from './actions';
import { getConsultationEntrie } from '../consulting';

export const getSuggestion = accountId => async (dispatch) => {
  try {
    await dispatch(actions.getSuggestionAttemps());
    const response = await windev.makeApiCall('/account/next_lettering', 'get', { account_id: accountId });
    const { data } = response;
    await dispatch(actions.getSuggestionSuccess(data));
    await dispatch(change('Lettrage', 'value', data.next_lettering));
    return response;
  } catch (err) {
    await dispatch(actions.getSuggestionFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const pushLettering = (society_id, lettering, lines, accountID) => async (dispatch) => {
  try {
    await dispatch(actions.postLetterAttempt());
    const param = {
      society_id,
      lettering,
      line_entry_list: lines.map(item => ({ line_entry_id: item.entry_line_id }))
    };
    await windev.makeApiCall('/entry/lettering', 'put', {}, param);
    await dispatch(actions.postLetterSuccess());
    await dispatch(updateSelect(society_id, [], accountID));
    await dispatch(getConsultationEntrie(society_id));
    return true;
  } catch (err) {
    await dispatch(actions.postLetterFail());
    await dispatch(handleError(err));
    return err;
  }
};
