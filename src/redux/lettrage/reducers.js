import {
  GET_SUGGESTION_ATTEMPT,
  GET_SUGGESTION_SUCCESS,
  GET_SUGGESTION_FAIL,
  POST_LETTER_ATTEMPT,
  POST_LETTER_SUCCESS,
  POST_LETTER_FAIL
} from './constants';

const initialState = {
  suggestion: {
    letter: '',
    fetched: false,
    error: false,
    fetching: false
  },
  fetched: false,
  error: false,
  fetching: false
};

const lettrageReducer = (state = initialState, action) => {
  if (action && action.type) {
    const { payload } = action;
    switch (action.type) {
    case GET_SUGGESTION_ATTEMPT:
      return {
        ...state,
        suggestion: {
          ...state.suggestion,
          fetching: true,
          fetched: false,
          error: false
        }
      };
    case GET_SUGGESTION_SUCCESS:
      return {
        ...state,
        suggestion: {
          ...state.suggestion,
          letter: payload.next_lettering,
          fetching: false,
          fetched: true,
          error: false
        }
      };

    case GET_SUGGESTION_FAIL:
      return {
        ...state,
        suggestion: {
          ...state.suggestion,
          fetching: false,
          fetched: true,
          error: true
        }
      };
    case POST_LETTER_ATTEMPT:
      return {
        ...state,
        fetching: true,
        fetched: false,
        error: false
      };
    case POST_LETTER_SUCCESS:
      return {
        ...state,
        fetching: false,
        fetched: true,
        error: false
      };
    case POST_LETTER_FAIL:
      return {
        ...state,
        fetching: false,
        fetched: false,
        error: true
      };
    default: return state;
    }
  }
  return state;
};

export default lettrageReducer;
