import {
  GET_SUGGESTION_ATTEMPT,
  GET_SUGGESTION_SUCCESS,
  GET_SUGGESTION_FAIL,
  POST_LETTER_ATTEMPT,
  POST_LETTER_SUCCESS,
  POST_LETTER_FAIL
} from './constants';

const getSuggestionAttemps = () => ({
  type: GET_SUGGESTION_ATTEMPT
});

const getSuggestionSuccess = payload => ({
  type: GET_SUGGESTION_SUCCESS,
  payload
});

const getSuggestionFail = () => ({
  type: GET_SUGGESTION_FAIL
});

const postLetterAttempt = () => ({
  type: POST_LETTER_ATTEMPT
});
const postLetterSuccess = () => ({
  type: POST_LETTER_SUCCESS
});
const postLetterFail = () => ({
  type: POST_LETTER_FAIL
});

export default {
  getSuggestionAttemps,
  getSuggestionSuccess,
  getSuggestionFail,
  postLetterAttempt,
  postLetterSuccess,
  postLetterFail
};
