import {
  GET_BI_ATTEMPT,
  GET_BI_SUCCESS,
  GET_BI_FAIL,
  ADD_ALL_STATEMENTS,
  RESET_ALL_STATEMENTS,
  SELECT_STATEMENT,
  ADD_STATEMENT,
  REMOVE_STATEMENT,
  GET_AN_ATTEMPT,
  GET_AN_SUCCESS,
  GET_AN_FAIL,
  SEND_BI_ATTEMPT,
  SEND_BI_SUCCESS,
  SEND_BI_FAIL,
  DELETE_BI_ATTEMPT,
  DELETE_BI_SUCCESS,
  DELETE_BI_FAIL,
  GET_INTERBANK_CODES_ATTEMPT,
  GET_INTERBANK_CODES_SUCCESS,
  GET_INTERBANK_CODES_FAIL,
  MODIFY_BI_ATTEMPT,
  MODIFY_BI_SUCCESS,
  MODIFY_BI_FAIL
} from './constants';

const getBIAttempt = society_id => ({
  type: GET_BI_ATTEMPT,
  society_id
});

const getBISuccess = (statements, society_id) => ({
  type: GET_BI_SUCCESS,
  statements,
  society_id
});

const getBIFail = society_id => ({
  type: GET_BI_FAIL,
  society_id
});

const deleteBIAttempt = society_id => ({
  type: DELETE_BI_ATTEMPT,
  society_id
});

const deleteBISuccess = (statements, society_id) => ({
  type: DELETE_BI_SUCCESS,
  statements,
  society_id
});

const deleteBIFail = society_id => ({
  type: DELETE_BI_FAIL,
  society_id
});

const modifyBIAttempt = society_id => ({
  type: MODIFY_BI_ATTEMPT,
  society_id
});

const modifyBISuccess = (statements, society_id) => ({
  type: MODIFY_BI_SUCCESS,
  statements,
  society_id
});

const modifyBIFail = society_id => ({
  type: MODIFY_BI_FAIL,
  society_id
});

const sendBIAttempt = society_id => ({
  type: SEND_BI_ATTEMPT,
  society_id
});

const sendBISuccess = society_id => ({
  type: SEND_BI_SUCCESS,
  society_id
});

const sendBIFail = society_id => ({
  type: SEND_BI_FAIL,
  society_id
});

const getAccountNumbersAttempt = society_id => ({
  type: GET_AN_ATTEMPT,
  society_id
});

const getAccountNumbersSuccess = (numbers, society_id) => ({
  type: GET_AN_SUCCESS,
  numbers,
  society_id
});

const getAccountNumbersFail = society_id => ({
  type: GET_AN_FAIL,
  society_id
});

const getInterbankCodesAttempt = society_id => ({
  type: GET_INTERBANK_CODES_ATTEMPT,
  society_id
});

const getInterbankCodesSuccess = (codes, society_id) => ({
  type: GET_INTERBANK_CODES_SUCCESS,
  codes,
  society_id
});

const getInterbankCodesFail = society_id => ({
  type: GET_INTERBANK_CODES_FAIL,
  society_id
});

const addAllStatements = society_id => ({
  type: ADD_ALL_STATEMENTS,
  society_id
});

const resetAllStatements = society_id => ({
  type: RESET_ALL_STATEMENTS,
  society_id
});

const selectStatement = settingSelectedLine => ({
  type: SELECT_STATEMENT,
  settingSelectedLine
});

const addStatement = (statementId, society_id) => ({
  type: ADD_STATEMENT,
  society_id,
  statementId
});

const removeStatement = (statementId, society_id) => ({
  type: REMOVE_STATEMENT,
  society_id,
  statementId
});

export default {
  getBIAttempt,
  getBISuccess,
  getBIFail,
  addAllStatements,
  resetAllStatements,
  selectStatement,
  addStatement,
  removeStatement,
  getAccountNumbersAttempt,
  getAccountNumbersSuccess,
  getAccountNumbersFail,
  sendBIAttempt,
  sendBISuccess,
  modifyBIAttempt,
  modifyBISuccess,
  modifyBIFail,
  deleteBIAttempt,
  deleteBISuccess,
  deleteBIFail,
  sendBIFail,
  getInterbankCodesAttempt,
  getInterbankCodesSuccess,
  getInterbankCodesFail
};
