import _ from 'lodash';
import {
  GET_BI_ATTEMPT,
  GET_BI_SUCCESS,
  GET_BI_FAIL,
  ADD_ALL_STATEMENTS,
  RESET_ALL_STATEMENTS,
  SELECT_STATEMENT,
  ADD_STATEMENT,
  REMOVE_STATEMENT,
  GET_AN_SUCCESS,
  GET_INTERBANK_CODES_ATTEMPT,
  GET_INTERBANK_CODES_SUCCESS,
  GET_INTERBANK_CODES_FAIL,
  DELETE_BI_SUCCESS
} from './constants';

const initialState = {
  settings_id: null,
  statements: {},
  selectedStatements: {},
  statementSelectedLine: {},
  interbankCodes: {
    interbankCodes_list: []
  }
};

const bankIntegrationSettings = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case DELETE_BI_SUCCESS:
      return {
        ...state,
        selectedStatements: {}
      };
    case GET_BI_ATTEMPT:
      return {
        ...state,
        statements: {
          [action.society_id]: {
            isLoading: true,
            isError: false,
            statements_list: []
          }
        }
      };
    case GET_BI_SUCCESS:
      return {
        ...state,
        statements: {
          [action.society_id]: {
            ...state.statements[action.society_id],
            isLoading: false,
            isError: false,
            statements_list: action.statements
          }
        }
      };
    case GET_BI_FAIL:
      return {
        ...state,
        statements: {
          [action.society_id]: {
            ...state.statements[action.society_id],
            isLoading: false,
            isError: true
          }
        }
      };
    case GET_INTERBANK_CODES_ATTEMPT:
      return {
        ...state,
        interbankCodes: {
          [action.society_id]: {
            isLoading: true,
            isError: false,
            interbankCodes_list: []
          }
        }
      };
    case GET_INTERBANK_CODES_SUCCESS:
      return {
        ...state,
        interbankCodes: {
          [action.society_id]: {
            ...state.interbankCodes[action.society_id],
            isLoading: false,
            isError: false,
            interbankCodes_list: action.codes
          }
        }
      };
    case GET_INTERBANK_CODES_FAIL:
      return {
        ...state,
        interbankCodes: {
          [action.society_id]: {
            ...state.interbankCodes[action.society_id],
            isLoading: false,
            isError: true
          }
        }
      };
    case ADD_ALL_STATEMENTS:
      return {
        ...state,
        selectedStatements: {
          ...state.selectedStatements,
          [action.society_id]: _.get(state, `statements[${action.society_id}].statements_list`, [])
            .map(statement => statement)
        }
      };
    case RESET_ALL_STATEMENTS:
      return {
        ...state,
        selectedStatements: {
          ...state.selectedStatements,
          [action.society_id]: []
        }
      };
    case SELECT_STATEMENT:
      const {
        statementSelectedLine
      } = action;
      return {
        ...state,
        statementSelectedLine
      };
    case ADD_STATEMENT: {
      return {
        ...state,
        selectedStatements: {
          ...state.selectedStatements,
          [action.society_id]: [..._.get(state, `selectedStatements.${action.society_id}`, []),
            action.statementId]
        }
      };
    }
    case REMOVE_STATEMENT:
      return {
        ...state,
        selectedStatements: {
          ...state.selectedStatements,
          [action.society_id]: _.get(state, `selectedStatements[${action.society_id}]`, [])
            .filter(statement => statement.id_cfg_import_releve_bancaire !== action.statementId)
        }
      };
    case GET_AN_SUCCESS:
      return {
        ...state,
        accountNumbers: {
          [action.society_id]: action.numbers
        }
      };
    default: return state;
    }
  }
  return state;
};

export default bankIntegrationSettings;
