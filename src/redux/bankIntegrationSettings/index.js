import _ from 'lodash';
import { windev } from 'helpers/api';
import actions from './actions';

export class BISettingsService {
  static instance;

  static getInstance() {
    if (!BISettingsService.instance) {
      BISettingsService.instance = new BISettingsService();
    }
    return BISettingsService.instance;
  }

  getStatementsList = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;

    try {
      await dispatch(actions.getBIAttempt(society_id));
      const response = await windev.makeApiCall('/releve_bancaire/config_ecriture_rb', 'get');
      const statements = response.data ? response.data : [];
      await dispatch(actions.getBISuccess(statements, society_id));
      return statements;
    } catch (err) {
      await dispatch(actions.getBIFail(society_id));
      return err;
    }
  };

  getAccountNumbers = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    try {
      await dispatch(actions.getAccountNumbersAttempt(society_id));
      const response = await windev.makeApiCall('/account?mode=1&q=1', 'get', { society_id });
      const numbers = response.data ? response.data : [];
      await dispatch(actions.getAccountNumbersSuccess(numbers, society_id));
      return society_id;
    } catch (err) {
      await dispatch(actions.getAccountNumbersFail(society_id));
      return err;
    }
  };

  getInterbankCodes = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    try {
      await dispatch(actions.getInterbankCodesAttempt(society_id));
      const response = await windev.makeApiCall('/releve_bancaire/code_ope_interbancaire', 'get');
      const codes = response.data ? response.data : [];
      await dispatch(actions.getInterbankCodesSuccess(codes, society_id));
      return codes;
    } catch (err) {
      await dispatch(actions.getInterbankCodesFail(society_id));
      return err;
    }
  };

  sendStatement = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const {
      account_id,
      code_interbancaire,
      allCodes,
      debit,
      credit,
      label_to_find,
      forced_label,
      calculateTVA,
      commision,
      tvaCode,
      pourcentage = 0
    } = state.form.BISettingsForm.values;

    const list_of_all_codes = _.get(state, `bankIntegrationSettings.interbankCodes[${state.navigation.id}].interbankCodes_list`, []);
    const list_of_selected_codes = _.map(code_interbancaire, item => item.value);
    const param_vat_id = _.get(calculateTVA, 'value.vat_param_id', null);
    const fees_account_id = _.get(commision, 'value.account_id', null);
    const id_param_fees_rb = _.get(tvaCode, 'value.code', null);

    const params = {
      account_id: account_id.account_id,
      list_id_code_interbancaire:
        allCodes ? list_of_all_codes.map(code_interbancaire => code_interbancaire.id)
          : list_of_selected_codes,
      debit,
      credit,
      label_to_find,
      forced_label,
      param_vat_id,
      fees_account_id,
      id_param_fees_rb: Number(id_param_fees_rb) || null,
      fees_percent: Number(pourcentage)
    };
    try {
      await dispatch(actions.sendBIAttempt(society_id));
      const response = await windev.makeApiCall('/releve_bancaire/config_ecriture_rb', 'post', {}, params);
      const numbers = response.data ? response.data : [];
      await dispatch(actions.sendBISuccess(society_id));
      return numbers;
    } catch (err) {
      await dispatch(actions.sendBIFail(society_id));
      return err;
    }
  };

  modifyStatement = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const selectedStatements = _.get(state, `bankIntegrationSettings.selectedStatements[${society_id}]`, []);
    const {
      account_id,
      allCodes,
      code_interbancaire,
      debit,
      credit,
      label_to_find,
      forced_label
    } = state.form.BISettingsForm.values;

    const list_of_all_codes = _.get(state, `bankIntegrationSettings.interbankCodes[${state.navigation.id}].interbankCodes_list`, []);
    const list_of_selected_codes = _.map(code_interbancaire, item => item.value);

    const params = {
      id_cfg_import_releve_bancaire:
        selectedStatements[selectedStatements.length - 1].id_cfg_import_releve_bancaire,
      account_id: account_id.account_id,
      list_id_code_interbancaire:
        allCodes ? list_of_all_codes.map(code_interbancaire => code_interbancaire.id)
          : list_of_selected_codes,
      debit,
      credit,
      label_to_find,
      forced_label
    };
    try {
      await dispatch(actions.modifyBIAttempt(society_id));
      const response = await windev.makeApiCall('/releve_bancaire/config_ecriture_rb', 'put', {}, params);
      const numbers = response.data ? response.data : [];
      await dispatch(actions.modifyBISuccess(society_id));
      await dispatch(actions.resetAllStatements(society_id));
      return numbers;
    } catch (err) {
      await dispatch(actions.modifyBIFail(society_id));
      return err;
    }
  };

  deleteStatement = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const { selectedStatements } = state.bankIntegrationSettings;
    const selected = selectedStatements[society_id];
    const id = selected[selected.length - 1].id_cfg_import_releve_bancaire;
    const params = {
      id_cfg_import_releve_bancaire: id
    };
    try {
      await dispatch(actions.deleteBIAttempt(society_id));
      const response = await windev.makeApiCall('/releve_bancaire/config_ecriture_rb', 'delete', {}, params);
      const numbers = response.data ? response.data : [];
      await dispatch(actions.deleteBISuccess(society_id));
      await dispatch(this.getStatementsList());
      return numbers;
    } catch (err) {
      await dispatch(actions.deleteBIFail(society_id));
      return err;
    }
  };
}


export default BISettingsService.getInstance();
