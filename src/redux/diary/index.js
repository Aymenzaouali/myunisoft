import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';
import _ from 'lodash';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { getTableName } from 'assets/constants/tableName';
import actions from './actions';

export const getDiaries = (opts = {}, societyId) => async (dispatch, getState) => {
  const state = getState();
  const society_id = societyId || state.navigation.id;
  const {
    sort: sortOpts,
    code,
    type
  } = opts;

  const diaries = _.get(state, `diary.diaries[${society_id}]`);
  const sort = sortOpts !== undefined ? sortOpts : _.get(diaries, 'sort', { column: 'diary_code', direction: 'asc' });

  try {
    const actionOpts = { sort };

    if (!_.isEmpty(sortOpts)) {
      const direction = 'asc';
      const storedColumn = _.get(diaries, 'sort.column');
      const currentColumn = _.get(sortOpts, 'column');
      if (currentColumn === storedColumn) {
        const storedDirection = _.get(diaries, 'sort.direction');
        const direction = storedDirection === 'desc' ? 'asc' : 'desc';
        actionOpts.sort = { column: currentColumn, direction };
      } else {
        actionOpts.sort = { column: currentColumn, direction };
      }
    }

    const params = {
      society_id,
      sort: actionOpts.sort,
      code,
      type
    };

    await dispatch(actions.getDiariesAttempt(society_id));
    const { data } = await windev.makeApiCall('/diary', 'get', params, {});
    await dispatch(actions.getDiariesSuccess(data, society_id, actionOpts));
    return data;
  } catch (err) {
    await dispatch(actions.getDiariesFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const postDiary = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;
  const {
    diary_type,
    diary_accountNumber,
    diary_label,
    diary_code
  } = _.get(state, 'form.diaryForm.values', {});

  const body = {
    society_id,
    diary_type_id: _.get(diary_type, 'id', ''),
    diary_account_id: _.get(diary_accountNumber, 'account_id', ''),
    name: diary_label,
    code: diary_code
  };

  try {
    await dispatch(actions.postDiaryAttempt(society_id));
    const { data } = await windev.makeApiCall('/diary', 'post', {}, body);
    await dispatch(actions.postDiarySuccess(data, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.postDiaryFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getDiariesType = () => async (dispatch) => {
  try {
    await dispatch(actions.getDiariesTypeAttempt());
    const { data } = await windev.makeApiCall('/diary/type', 'get', {}, {});
    await dispatch(actions.getDiariesTypeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getDiariesTypeFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteDiary = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;
  const selectedDiaries = _.get(state, `tables[${getTableName(state.navigation.id, 'diary')}].selectedRows`, {});
  const body = {
    diary: Object.keys(selectedDiaries)
      .reduce((acc, key) => (selectedDiaries[key] ? [...acc, key] : acc), [])
  };

  try {
    await dispatch(actions.deleteDiaryAttempt());
    const { data } = await windev.makeApiCall('/diary', 'delete', {}, body);
    await dispatch(actions.deleteDiarySuccess(data));
    dispatch(actions.resetAllDiary(society_id));
    return data;
  } catch (err) {
    await dispatch(actions.deleteDiaryFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const modifyDiary = ({ diaryId } = {}) => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;
  const {
    diary_type,
    diary_accountNumber,
    diary_label,
    diary_code
  } = _.get(state, 'form.diaryForm.values', {});

  const id = diaryId || _.get(state, 'diary.diarySelectedLine.id', '');

  const body = {
    diary_type_id: _.get(diary_type, 'id', ''),
    diary_account_id: _.get(diary_accountNumber, 'account_id', ''),
    name: diary_label,
    code: diary_code
  };

  try {
    await dispatch(actions.putDiaryAttempt());
    const { data } = await windev.makeApiCall('/diary', 'put', { id }, body);
    await dispatch(actions.putDiarySuccess(data));
    dispatch(actions.removeDiary(id, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.putDiaryFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getDiaryDetail = (diaryCode = '') => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;
  const code = diaryCode !== '' ? diaryCode : _.get(state, 'diary.diarySelectedLine.code', '');

  try {
    await dispatch(actions.getDiaryDetailAttempt(society_id));
    const { data } = await windev.makeApiCall('/diary', 'get', { society_id, code });
    await dispatch(actions.getDiaryDetailSuccess(data[0]));

    const diaryType = {
      id: _.get(data[0], 'diary_type_id', ''),
      label: _.get(data[0], 'diary_type_name', ''),
      value: _.get(data[0], 'diary_type_code', '')
    };

    const accountNumber = data[0].account && {
      id: _.get(data[0], 'account.id', ''),
      label: _.get(data[0], 'account.label', ''),
      value: _.get(data[0], 'account.number', ''),
      account_number: _.get(data[0], 'account.number', '')
    };

    const dataForm = {
      diary_code: _.get(data[0], 'code', ''),
      diary_label: _.get(data[0], 'name', ''),
      diary_type: diaryType,
      diary_accountNumber: accountNumber,
      code: _.get(data[0], 'code', '')
    };

    await dispatch(autoFillFormValues('diaryForm', dataForm));

    return data;
  } catch (err) {
    await dispatch(actions.getDiaryDetailFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const checkDiaryCode = diaryCode => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;

  try {
    await dispatch(actions.checkDiaryCodeAttempt());
    const { data } = await windev.makeApiCall('/diary', 'get', { society_id, code: diaryCode });
    await dispatch(actions.checkDiaryCodeSuccess(_.isEmpty(data)));
    return data;
  } catch (err) {
    await dispatch(actions.checkDiaryCodeFail());
    await dispatch(handleError(err));
    return err;
  }
};
