import {
  GET_DIARIES_LIST_ATTEMPT,
  GET_DIARIES_LIST_SUCCESS,
  GET_DIARIES_LIST_FAIL,
  GET_DIARIES_TYPE_ATTEMPT,
  GET_DIARIES_TYPE_SUCCESS,
  GET_DIARIES_TYPE_FAIL,
  GET_DIARY_DETAIL_ATTEMPT,
  GET_DIARY_DETAIL_SUCCESS,
  GET_DIARY_DETAIL_FAIL,
  POST_DIARY_ATTEMPT,
  POST_DIARY_SUCCESS,
  POST_DIARY_FAIL,
  PUT_DIARY_ATTEMPT,
  PUT_DIARY_SUCCESS,
  PUT_DIARY_FAIL,
  DEL_DIARY_ATTEMPT,
  DEL_DIARY_SUCCESS,
  DEL_DIARY_FAIL,
  SELECT_DIARY,
  CHECK_DIARY_CODE_SUCCESS,
  CHECK_DIARY_CODE_FAIL,
  CHECK_DIARY_CODE_ATTEMPT,
  CLEAR_DIARY_DETAILS
} from './constants';

const getDiariesAttempt = society_id => ({
  type: GET_DIARIES_LIST_ATTEMPT,
  society_id
});

const getDiariesSuccess = (diary_list, society_id, opts) => ({
  type: GET_DIARIES_LIST_SUCCESS,
  diary_list,
  society_id,
  opts
});

const getDiariesFail = society_id => ({
  type: GET_DIARIES_LIST_FAIL,
  society_id
});

const getDiariesTypeAttempt = () => ({
  type: GET_DIARIES_TYPE_ATTEMPT
});

const getDiariesTypeSuccess = diariesType => ({
  type: GET_DIARIES_TYPE_SUCCESS,
  diariesType
});

const getDiariesTypeFail = () => ({
  type: GET_DIARIES_TYPE_FAIL
});

const getDiaryDetailAttempt = () => ({
  type: GET_DIARY_DETAIL_ATTEMPT
});

const getDiaryDetailSuccess = diaryDetail => ({
  type: GET_DIARY_DETAIL_SUCCESS,
  diaryDetail
});

const getDiaryDetailFail = () => ({
  type: GET_DIARY_DETAIL_FAIL
});

const postDiaryAttempt = society_id => ({
  type: POST_DIARY_ATTEMPT,
  society_id
});

const postDiarySuccess = (diary_id, society_id) => ({
  type: POST_DIARY_SUCCESS,
  diary_id,
  society_id
});

const postDiaryFail = society_id => ({
  type: POST_DIARY_FAIL,
  society_id
});

const putDiaryAttempt = () => ({
  type: PUT_DIARY_ATTEMPT
});

const putDiarySuccess = diary_id => ({
  type: PUT_DIARY_SUCCESS,
  diary_id
});

const putDiaryFail = () => ({
  type: PUT_DIARY_FAIL
});

const deleteDiaryAttempt = () => ({
  type: DEL_DIARY_ATTEMPT
});

const deleteDiarySuccess = diary_id => ({
  type: DEL_DIARY_SUCCESS,
  diary_id
});

const deleteDiaryFail = () => ({
  type: DEL_DIARY_FAIL
});

const selectDiary = diarySelectedLine => ({
  type: SELECT_DIARY,
  diarySelectedLine
});

const clearDiaryDetails = () => ({
  type: CLEAR_DIARY_DETAILS
});

const checkDiaryCodeAttempt = () => ({
  type: CHECK_DIARY_CODE_ATTEMPT
});

const checkDiaryCodeSuccess = diaryCodeAvailable => ({
  type: CHECK_DIARY_CODE_SUCCESS,
  diaryCodeAvailable
});

const checkDiaryCodeFail = () => ({
  type: CHECK_DIARY_CODE_FAIL
});

export default {
  getDiariesAttempt,
  getDiariesSuccess,
  getDiariesFail,
  getDiariesTypeAttempt,
  getDiariesTypeSuccess,
  getDiariesTypeFail,
  getDiaryDetailAttempt,
  getDiaryDetailSuccess,
  getDiaryDetailFail,
  postDiaryAttempt,
  postDiarySuccess,
  postDiaryFail,
  putDiaryAttempt,
  putDiarySuccess,
  putDiaryFail,
  deleteDiaryAttempt,
  deleteDiarySuccess,
  deleteDiaryFail,
  selectDiary,
  checkDiaryCodeAttempt,
  checkDiaryCodeSuccess,
  checkDiaryCodeFail,
  clearDiaryDetails
};
