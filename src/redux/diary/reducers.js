import _ from 'lodash';
import {
  GET_DIARIES_LIST_ATTEMPT,
  GET_DIARIES_LIST_SUCCESS,
  GET_DIARIES_LIST_FAIL,
  GET_DIARIES_TYPE_SUCCESS,
  GET_DIARY_DETAIL_SUCCESS,
  SELECT_DIARY,
  CHECK_DIARY_CODE_SUCCESS,
  CLEAR_DIARY_DETAILS
} from './constants';

const initialState = {
  diaries: {},
  diaryDetail: {},
  diariesType: [],
  diariesSelected: {},
  selectedDiary: {},
  diarySelectedLine: undefined,
  diaryCodeAvailable: true
};

const diary = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_DIARIES_LIST_ATTEMPT:
      return {
        ...state,
        diaries: {
          ...state.diaries,
          [action.society_id]: {
            ...state.diaries[action.society_id],
            isLoading: true,
            isError: false,
            diary_list: []
          }
        }
      };
    case GET_DIARIES_LIST_SUCCESS:
      const {
        diary_list,
        society_id,
        opts
      } = action;

      return {
        ...state,
        diaries: {
          ...state.diaries,
          [society_id]: {
            ..._.get(state, `diaries[${society_id}]`, {}),
            ...opts,
            diary_list,
            isLoading: false
          }
        }
      };
    case GET_DIARIES_LIST_FAIL:
      return {
        ...state,
        diaries: {
          ...state.diaries,
          [action.society_id]: {
            isLoading: false,
            isError: true,
            diary_list: []
          }
        }
      };
    case GET_DIARIES_TYPE_SUCCESS:
      const {
        diariesType
      } = action;

      return {
        ...state,
        diariesType
      };
    case GET_DIARY_DETAIL_SUCCESS:
      const {
        diaryDetail
      } = action;

      return {
        ...state,
        diaryDetail
      };
    case SELECT_DIARY:
      const {
        diarySelectedLine
      } = action;

      return {
        ...state,
        diarySelectedLine
      };
    case CLEAR_DIARY_DETAILS:
      return {
        ...state,
        diaryDetail: {}
      };
    case CHECK_DIARY_CODE_SUCCESS:
      const {
        diaryCodeAvailable
      } = action;

      return {
        ...state,
        diaryCodeAvailable
      };
    default: return state;
    }
  }
  return state;
};

export default diary;
