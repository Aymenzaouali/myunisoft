export const GET_DIARIES_LIST_ATTEMPT = 'GET_DIARIES_LIST_ATTEMPT';
export const GET_DIARIES_LIST_SUCCESS = 'GET_DIARIES_LIST_SUCCESS';
export const GET_DIARIES_LIST_FAIL = 'GET_DIARIES_LIST_FAIL';
export const GET_DIARIES_TYPE_ATTEMPT = 'GET_DIARIES_TYPE_ATTEMPT';
export const GET_DIARIES_TYPE_SUCCESS = 'GET_DIARIES_TYPE_SUCCESS';
export const GET_DIARIES_TYPE_FAIL = 'GET_DIARIES_TYPE_FAIL';
export const GET_DIARY_DETAIL_ATTEMPT = 'GET_DIARY_DETAIL_ATTEMPT';
export const GET_DIARY_DETAIL_SUCCESS = 'GET_DIARY_DETAIL_SUCCESS';
export const GET_DIARY_DETAIL_FAIL = 'GET_DIARY_DETAIL_FAIL';
export const POST_DIARY_ATTEMPT = 'POST_DIARY_ATTEMPT';
export const POST_DIARY_SUCCESS = 'POST_DIARY_SUCCESS';
export const POST_DIARY_FAIL = 'POST_DIARY_FAIL';
export const PUT_DIARY_ATTEMPT = 'PUT_DIARY_ATTEMPT';
export const PUT_DIARY_SUCCESS = 'PUT_DIARY_SUCCESS';
export const PUT_DIARY_FAIL = 'PUT_DIARY_FAIL';
export const DEL_DIARY_ATTEMPT = 'DEL_DIARY_ATTEMPT';
export const DEL_DIARY_SUCCESS = 'DEL_DIARY_SUCCESS';
export const DEL_DIARY_FAIL = 'DEL_DIARY_FAIL';
export const SELECT_DIARY = 'SELECT_DIARY';
export const ADD_ALL_DIARY = 'ADD_ALL_DIARY';
export const CHECK_DIARY_CODE_ATTEMPT = 'CHECK_DIARY_CODE_ATTEMPT';
export const CHECK_DIARY_CODE_SUCCESS = 'CHECK_DIARY_CODE_SUCCESS';
export const CHECK_DIARY_CODE_FAIL = 'CHECK_DIARY_CODE_FAIL';
export const CLEAR_DIARY_DETAILS = 'CLEAR_DIARY_DETAILS';
