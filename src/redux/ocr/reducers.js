import _ from 'lodash';
import {
  GET_OCR_TRACKING_ATTEMPT,
  GET_OCR_TRACKING_SUCCESS,
  GET_OCR_TRACKING_FAIL
} from './constants';

const initialeState = {
  ocr_tracking: {},
  maxRowsOcrTracking: 20
};

const ocr = (state = initialeState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_OCR_TRACKING_ATTEMPT:
      return {
        ...state,
        ocr_tracking: {
          ...state.ocr_tracking,
          [action.society_id]: {
            ...state.ocr_tracking[action.society_id],
            isLoading: true,
            isError: false,
            ocr_follow_up_array: []
          }
        }
      };
    case GET_OCR_TRACKING_SUCCESS:
      const {
        ocr_tracking,
        society_id,
        opts
      } = action;

      return {
        ...state,
        ocr_tracking: {
          ...state.ocr_tracking,
          [society_id]: {
            ..._.get(state, `ocr_tracking[${society_id}]`, {}),
            ...ocr_tracking,
            ...opts,
            isLoading: false
          }
        }
      };
    case GET_OCR_TRACKING_FAIL:
      return {
        ...state,
        ocr_tracking: {
          ...state.ocr_tracking,
          [action.society_id]: {
            ...state.ocr_tracking[action.society_id],
            isLoading: false,
            isError: true,
            ocr_follow_up_array: []
          }
        }
      };
    default: return state;
    }
  }
  return state;
};

export default ocr;
