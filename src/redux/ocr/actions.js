import {
  GET_OCR_TRACKING_ATTEMPT,
  GET_OCR_TRACKING_SUCCESS,
  GET_OCR_TRACKING_FAIL
} from './constants';

const getTrackingOcrAttempt = society_id => ({
  type: GET_OCR_TRACKING_ATTEMPT,
  society_id
});

const getTrackingOcrSuccess = (ocr_tracking, society_id, opts) => ({
  type: GET_OCR_TRACKING_SUCCESS,
  ocr_tracking,
  society_id,
  opts
});

const getTrackingOcrFail = society_id => ({
  type: GET_OCR_TRACKING_FAIL,
  society_id
});

export default {
  getTrackingOcrAttempt,
  getTrackingOcrSuccess,
  getTrackingOcrFail
};
