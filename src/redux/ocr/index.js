import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import { formValueSelector } from 'redux-form';
import moment from 'moment';
import _ from 'lodash';
import actions from './actions';

export const getTrackingOcr = (opts = {}, societyId) => async (dispatch, getState) => {
  const state = getState();

  const {
    sort
  } = opts;

  const society_id = societyId !== undefined ? societyId : _.get(state, 'navigation.id', false);
  const formValue = formValueSelector(`${society_id}TrackingOcrFilter`);
  const ocrTracking = _.get(state, `ocrWeb.ocr_tracking[${society_id}]`, '');
  const listSociety = _.get(state, 'navigation.societies', []);
  const sortParam = sort !== undefined ? sort : _.get(ocrTracking, 'sort', '');
  const limit = 1000000;
  const page = 0;
  const offset = 0;

  const startDate = formValue(state, 'start_date');
  const endDate = moment(formValue(state, 'end_date')).format('YYYYMMDD');
  const request_mode = formValue(state, 'sendBy');
  const society_filter = formValue(state, 'societyFilter');

  try {
    let societies_id = [];

    switch (society_id) {
    case -2:
      if (society_filter.length === 0) {
        // ***********************************************************
        // TODO: Si rien spécifié le back renvoyer pour TOUTES les sociétés... lol
        // ***********************************************************
        societies_id = listSociety.map(item => ({ id: item.id_societe }));
      } else {
        societies_id = society_filter.map(item => ({ id: item.society_id }));
      }
      break;
    default:
      societies_id = [{ id: society_id }];

      break;
    }

    const actionOpts = { limit, page };
    const filter = formValue(state, 'search');
    const array_society_id = JSON.stringify(societies_id);
    const start_date = moment(startDate).format('YYYYMMDD');
    const end_date = moment(endDate).format('YYYYMMDD');

    const params = {
      start_date,
      end_date,
      request_mode,
      offset,
      limit,
      array_society_id,
      filter,
      sort: sortParam
    };

    if (!_.isEmpty(sort)) {
      const {
        column
      } = sort;

      const storedSort = _.get(ocrTracking, 'sort.column');

      let direction = 'asc';

      if (storedSort === column) {
        const storedDirection = _.get(ocrTracking, 'sort.direction');
        direction = storedDirection === 'desc' ? 'asc' : 'desc';
      }
      params.sort = {
        column,
        direction
      };
      actionOpts.sort = { column, direction };
    } else {
      actionOpts.sort = '';
    }

    await dispatch(actions.getTrackingOcrAttempt(society_id));
    const response = await windev.makeApiCall('/ocr_follow_up', 'get', params, {});
    const { data } = response;
    await dispatch(actions.getTrackingOcrSuccess(data, society_id, actionOpts));
    return response;
  } catch (err) {
    await dispatch(actions.getTrackingOcrFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};
