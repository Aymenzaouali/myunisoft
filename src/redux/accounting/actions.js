import {
  GET_ENTRIES_ATTEMPT,
  GET_ENTRIES_SUCCESS,
  GET_ENTRIES_FAIL,
  DELETE_ENTRY_ATTEMPT,
  DELETE_ENTRY_SUCCESS,
  DELETE_ENTRY_FAIL,
  DELETE_ENTRY_TEMP_ATTEMPT,
  DELETE_ENTRY_TEMP_SUCCESS,
  DELETE_ENTRY_TEMP_FAIL,
  GET_DIARIES_ATTEMPT,
  GET_DIARIES_SUCCESS,
  GET_DIARIES_FAIL,
  GET_ACCOUNTING_ATTEMPT,
  GET_ACCOUNTING_SUCCESS,
  GET_ACCOUNTING_FAIL,
  GET_PAYMENT_TYPE_ATTEMPT,
  GET_PAYMENT_TYPE_SUCCESS,
  GET_PAYMENT_TYPE_FAIL,
  CREATE_ENTRY_ATTEMPT,
  CREATE_ENTRY_SUCCESS,
  CREATE_ENTRY_FAIL,
  GET_ENTRY_COUNTERPART_ATTEMPT,
  GET_ENTRY_COUNTERPART_SUCCESS,
  MODIFY_ENTRY_ATTEMPT,
  MODIFY_ENTRY_SUCCESS,
  MODIFY_ENTRY_FAIL,
  GET_EXERCICE_ATTEMPT,
  GET_EXERCICE_SUCCESS,
  GET_EXERCICE_FAIL,
  VALIDATE_ENTRIES_ATTEMPT,
  VALIDATE_ENTRIES_SUCCESS,
  VALIDATE_ENTRIES_FAIL,
  ADD_ENTRY,
  REMOVE_ENTRY,
  UPDATE_ENTRY,
  RESET_ALL_ENTRIES,
  ADD_ALL_ENTRIES,
  OPEN_SHOULD_DELETE_ENTRIES,
  CLOSE_SHOULD_DELETE_ENTRIES,

  OPEN_CONFIRMATION_FROM_HEADER,
  OPEN_PERMISSION_FROM_HEADER,
  OPEN_INFORMATION_FROM_HEADER,

  GET_ENTRY_COMMENTS_ATTEMPT,
  GET_ENTRY_COMMENTS_SUCCESS,
  GET_ENTRY_COMMENTS_FAIL,

  POST_COMMENT_ATTEMPT,
  POST_COMMENT_FAIL,
  POST_COMMENT_SUCCESS,

  PUT_COMMENT_ATTEMPT,
  PUT_COMMENT_FAIL,
  PUT_COMMENT_SUCCESS,

  GET_SILAE_ACCOUNT_ATTEMPT,
  GET_SILAE_ACCOUNT_SUCCESS,
  GET_SILAE_ACCOUNT_FAIL,

  POST_ACCOUNT_PARAM_ATTEMPT,
  POST_ACCOUNT_PARAM_SUCCESS,
  POST_ACCOUNT_PARAM_FAIL,

  LOAD_ODPAY_ATTEMPT,
  LOAD_ODPAY_SUCCESS,
  LOAD_ODPAY_FAIL,

  PREPARE_COMMENT_FOR_NEW_ENTRY,
  RESET_COMMENT_FOR_NEW_ENTRY,

  GET_WORKSHEET_TYPE_ATTEMPT,
  GET_WORKSHEET_TYPE_SUCCESS,
  GET_WORKSHEET_TYPE_FAIL,

  GET_WORKSHEET_PARAM_ATTEMPT,
  GET_WORKSHEET_PARAM_SUCCESS,
  GET_WORKSHEET_PARAM_FAIL,

  SET_CURRENT_ENTRY_LINE,
  SET_CURRENT_ENTRY,

  CREATE_ACCOUNT_ATTEMPT,
  CREATE_ACCOUNT_SUCCESS,
  CREATE_ACCOUNT_FAIL,

  UPDATE_ACCOUNT_SUCCESS,
  UPDATE_ACCOUNT_ATTEMPT,
  UPDATE_ACCOUNT_FAIL,

  SET_INFORMATION_DIALOG
} from './constants';

const loadOdpayAttempt = () => ({
  type: LOAD_ODPAY_ATTEMPT
});

const loadOdpaySuccess = () => ({
  type: LOAD_ODPAY_SUCCESS
});

const loadOdpayFail = () => ({
  type: LOAD_ODPAY_FAIL
});

const postAccountParamAttempt = () => ({
  type: POST_ACCOUNT_PARAM_ATTEMPT
});

const postAccountParamSuccess = () => ({
  type: POST_ACCOUNT_PARAM_SUCCESS
});

const postAccountParamFail = () => ({
  type: POST_ACCOUNT_PARAM_FAIL
});

const getSilaeAccountAttempt = () => ({
  type: GET_SILAE_ACCOUNT_ATTEMPT
});

const getSilaeAccountSuccess = payload => ({
  type: GET_SILAE_ACCOUNT_SUCCESS,
  payload
});

const getSilaeAccountFail = () => ({
  type: GET_SILAE_ACCOUNT_FAIL
});

const getExerciceAttempt = societyId => ({
  type: GET_EXERCICE_ATTEMPT,
  societyId
});

const getExerciceSuccess = (societyId, data) => ({
  type: GET_EXERCICE_SUCCESS,
  payload: {
    id: societyId,
    data
  }
});

const getExerciceFail = societyId => ({
  type: GET_EXERCICE_FAIL,
  societyId
});

const getEntriesAttempt = (societyId, opts) => ({
  type: GET_ENTRIES_ATTEMPT,
  societyId,
  opts
});

const getEntriesSuccess = (societyId, entries) => ({
  type: GET_ENTRIES_SUCCESS,
  societyId,
  entries
});

const getEntriesFail = societyId => ({
  type: GET_ENTRIES_FAIL,
  societyId
});

const getDiariesAttempt = () => ({
  type: GET_DIARIES_ATTEMPT
});

const getDiariesSuccess = diaries => ({
  type: GET_DIARIES_SUCCESS,
  diaries
});

const getDiariesFail = () => ({
  type: GET_DIARIES_FAIL
});

const deleteEntryAttempt = () => ({
  type: DELETE_ENTRY_ATTEMPT
});

const deleteEntrySuccess = (societyId, entries) => ({
  type: DELETE_ENTRY_SUCCESS,
  societyId,
  entries
});

const deleteEntryFail = () => ({
  type: DELETE_ENTRY_FAIL
});

const deleteEntryTempAttempt = () => ({
  type: DELETE_ENTRY_TEMP_ATTEMPT
});

const deleteEntryTempSuccess = (entries, societyId) => ({
  type: DELETE_ENTRY_TEMP_SUCCESS,
  entries,
  societyId
});

const deleteEntryTempFail = () => ({
  type: DELETE_ENTRY_TEMP_FAIL
});

const getAccountingAttempt = () => ({
  type: GET_ACCOUNTING_ATTEMPT
});

const getAccountingSuccess = account => ({
  type: GET_ACCOUNTING_SUCCESS,
  account
});

const getAccountingFail = () => ({
  type: GET_ACCOUNTING_FAIL
});

const getPaymentTypeAttempt = () => ({
  type: GET_PAYMENT_TYPE_ATTEMPT
});

const getPaymentTypeSuccess = paymentType => ({
  type: GET_PAYMENT_TYPE_SUCCESS,
  paymentType
});

const getPaymentTypeFail = () => ({
  type: GET_PAYMENT_TYPE_FAIL
});

const getEntryCounterPartAttempt = () => ({
  type: GET_ENTRY_COUNTERPART_ATTEMPT
});

const getEntryCounterPartSuccess = paymentType => ({
  type: GET_ENTRY_COUNTERPART_SUCCESS,
  paymentType
});

const getEntryCounterPartFail = () => ({
  type: GET_ENTRIES_FAIL
});

const createEntryAttempt = societyId => ({
  type: CREATE_ENTRY_ATTEMPT,
  societyId
});

const createEntrySuccess = societyId => ({
  type: CREATE_ENTRY_SUCCESS,
  societyId
});

const createEntryFail = (societyId, errorObject) => ({
  type: CREATE_ENTRY_FAIL,
  societyId,
  errorObject
});

const modifyEntryAttempt = (societyId, entryId) => ({
  type: MODIFY_ENTRY_ATTEMPT,
  societyId,
  entryId
});

const modifyEntrySuccess = (societyId, entry) => ({
  type: MODIFY_ENTRY_SUCCESS,
  societyId,
  entry
});

const modifyEntryFail = societyId => ({
  type: MODIFY_ENTRY_FAIL,
  societyId
});

const updateEntry = (societyId, entry) => ({
  type: UPDATE_ENTRY,
  societyId,
  entry
});

const validateEntriesAttempt = entry => ({
  type: VALIDATE_ENTRIES_ATTEMPT,
  entry
});

const validateEntriesSuccess = entry => ({
  type: VALIDATE_ENTRIES_SUCCESS,
  entry
});

const validateEntriesFail = entry => ({
  type: VALIDATE_ENTRIES_FAIL,
  entry
});

const addEntry = (entry, societyId) => ({
  type: ADD_ENTRY,
  entry,
  societyId
});

const removeEntry = (entry, societyId) => ({
  type: REMOVE_ENTRY,
  entry,
  societyId
});

const resetAllEntries = societyId => ({
  type: RESET_ALL_ENTRIES,
  societyId
});

const addAllEntries = societyId => ({
  type: ADD_ALL_ENTRIES,
  societyId
});

const openShouldDeleteEntries = (entries, societyId, entriesName) => ({
  type: OPEN_SHOULD_DELETE_ENTRIES,
  societyId,
  entries,
  entriesName
});

const closeShouldDeleteEntries = societyId => ({
  type: CLOSE_SHOULD_DELETE_ENTRIES,
  societyId
});

const enableCreateCounterPartConfirmation = openConfirmFromHeader => ({
  type: OPEN_CONFIRMATION_FROM_HEADER,
  openConfirmFromHeader
});

const enableLinkDiaryAccountPermission = openPermissionFromHeader => ({
  type: OPEN_PERMISSION_FROM_HEADER,
  openPermissionFromHeader
});

const enableCounterPartInformation = openInformationFromHeader => ({
  type: OPEN_INFORMATION_FROM_HEADER,
  openInformationFromHeader
});

const getEntryCommentsAttempt = society_id => ({
  type: GET_ENTRY_COMMENTS_ATTEMPT,
  society_id
});
const getEntryCommentsSuccess = payload => ({
  type: GET_ENTRY_COMMENTS_SUCCESS,
  payload
});
const getEntryCommentsFail = society_id => ({
  type: GET_ENTRY_COMMENTS_FAIL,
  society_id
});

const postCommentAttempt = society_id => ({
  type: POST_COMMENT_ATTEMPT,
  society_id
});
const postCommentSuccess = payload => ({
  type: POST_COMMENT_SUCCESS,
  payload
});
const postCommentFail = society_id => ({
  type: POST_COMMENT_FAIL,
  society_id
});

const putCommentAttempt = society_id => ({
  type: PUT_COMMENT_ATTEMPT,
  society_id
});

const putCommentSuccess = payload => ({
  type: PUT_COMMENT_SUCCESS,
  payload
});

const putCommentFail = society_id => ({
  type: PUT_COMMENT_FAIL,
  society_id
});

const prepareCommentForNewEntry = comment => ({
  type: PREPARE_COMMENT_FOR_NEW_ENTRY,
  comment
});

const resetCommentForNewEntry = () => ({
  type: RESET_COMMENT_FOR_NEW_ENTRY
});

const getWorksheetTypeAttempt = () => ({
  type: GET_WORKSHEET_TYPE_ATTEMPT
});

const getWorksheetTypeSuccess = payload => ({
  type: GET_WORKSHEET_TYPE_SUCCESS,
  payload
});

const getWorksheetTypeFail = () => ({
  type: GET_WORKSHEET_TYPE_FAIL
});

const getWorksheetParamAttempt = () => ({
  type: GET_WORKSHEET_PARAM_ATTEMPT
});

const getWorksheetParamSuccess = (data, societyId) => ({
  type: GET_WORKSHEET_PARAM_SUCCESS,
  payload: {
    data,
    societyId
  }
});

const getWorksheetParamFail = societyId => ({
  type: GET_WORKSHEET_PARAM_FAIL,
  payload: {
    societyId
  }
});

const setCurrentEntryLine = i => ({
  type: SET_CURRENT_ENTRY_LINE,
  i
});

const setCurrentEntry = (currentEntry, society_id) => ({
  type: SET_CURRENT_ENTRY,
  currentEntry,
  society_id
});

const createAccountAttempt = () => ({
  type: CREATE_ACCOUNT_ATTEMPT
});

const createAccountSuccess = account => ({
  type: CREATE_ACCOUNT_SUCCESS,
  account
});

const createAccountFail = account => ({
  type: CREATE_ACCOUNT_FAIL,
  account
});

const updateAccountAttempt = () => ({
  type: UPDATE_ACCOUNT_ATTEMPT
});

const updateAccountSuccess = account => ({
  type: UPDATE_ACCOUNT_SUCCESS,
  account
});

const updateAccountFail = account => ({
  type: UPDATE_ACCOUNT_FAIL,
  account
});

const setInformationDialog = isOpen => ({
  type: SET_INFORMATION_DIALOG,
  isOpen
});

export default {
  getEntriesAttempt,
  getEntriesSuccess,
  getEntriesFail,

  getDiariesAttempt,
  getDiariesSuccess,
  getDiariesFail,

  deleteEntryAttempt,
  deleteEntrySuccess,
  deleteEntryFail,

  deleteEntryTempAttempt,
  deleteEntryTempSuccess,
  deleteEntryTempFail,

  getAccountingAttempt,
  getAccountingSuccess,
  getAccountingFail,

  getPaymentTypeAttempt,
  getPaymentTypeSuccess,
  getPaymentTypeFail,

  getEntryCounterPartAttempt,
  getEntryCounterPartSuccess,
  getEntryCounterPartFail,

  createEntryAttempt,
  createEntrySuccess,
  createEntryFail,

  modifyEntryAttempt,
  modifyEntrySuccess,
  modifyEntryFail,

  getExerciceAttempt,
  getExerciceSuccess,
  getExerciceFail,

  validateEntriesAttempt,
  validateEntriesSuccess,
  validateEntriesFail,

  addEntry,
  removeEntry,
  updateEntry,
  resetAllEntries,
  addAllEntries,

  openShouldDeleteEntries,
  closeShouldDeleteEntries,

  enableCreateCounterPartConfirmation,
  enableLinkDiaryAccountPermission,
  enableCounterPartInformation,

  getEntryCommentsAttempt,
  getEntryCommentsSuccess,
  getEntryCommentsFail,

  putCommentAttempt,
  putCommentSuccess,
  putCommentFail,

  postCommentAttempt,
  postCommentSuccess,
  postCommentFail,

  getSilaeAccountAttempt,
  getSilaeAccountSuccess,
  getSilaeAccountFail,

  postAccountParamAttempt,
  postAccountParamSuccess,
  postAccountParamFail,

  loadOdpayAttempt,
  loadOdpaySuccess,
  loadOdpayFail,

  prepareCommentForNewEntry,
  resetCommentForNewEntry,

  getWorksheetTypeAttempt,
  getWorksheetTypeSuccess,
  getWorksheetTypeFail,

  getWorksheetParamAttempt,
  getWorksheetParamSuccess,
  getWorksheetParamFail,

  setCurrentEntryLine,
  setCurrentEntry,

  createAccountAttempt,
  createAccountSuccess,
  createAccountFail,

  updateAccountAttempt,
  updateAccountSuccess,
  updateAccountFail,

  setInformationDialog
};
