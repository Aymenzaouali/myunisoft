import _ from 'lodash';

import { deleteEntry, updateEntry } from 'helpers/accounting';

import {
  GET_ENTRIES_ATTEMPT,
  GET_ENTRIES_SUCCESS,
  GET_ENTRIES_FAIL,

  GET_DIARIES_SUCCESS,

  GET_ACCOUNTING_SUCCESS,

  GET_PAYMENT_TYPE_SUCCESS,

  CREATE_ENTRY_ATTEMPT,
  CREATE_ENTRY_SUCCESS,
  CREATE_ENTRY_FAIL,

  MODIFY_ENTRY_ATTEMPT,
  MODIFY_ENTRY_SUCCESS,
  MODIFY_ENTRY_FAIL,

  DELETE_ENTRY_SUCCESS,

  DELETE_ENTRY_TEMP_SUCCESS,

  GET_EXERCICE_ATTEMPT,
  GET_EXERCICE_SUCCESS,
  GET_EXERCICE_FAIL,

  ADD_ENTRY,

  REMOVE_ENTRY,

  UPDATE_ENTRY,

  RESET_ALL_ENTRIES,

  ADD_ALL_ENTRIES,

  OPEN_SHOULD_DELETE_ENTRIES,
  CLOSE_SHOULD_DELETE_ENTRIES,

  OPEN_CONFIRMATION_FROM_HEADER,
  OPEN_PERMISSION_FROM_HEADER,
  OPEN_INFORMATION_FROM_HEADER,

  GET_ENTRY_COMMENTS_ATTEMPT,
  GET_ENTRY_COMMENTS_FAIL,
  GET_ENTRY_COMMENTS_SUCCESS,

  POST_COMMENT_ATTEMPT,
  POST_COMMENT_FAIL,
  POST_COMMENT_SUCCESS,

  PUT_COMMENT_FAIL,
  PUT_COMMENT_ATTEMPT,
  PUT_COMMENT_SUCCESS,

  GET_SILAE_ACCOUNT_ATTEMPT,
  GET_SILAE_ACCOUNT_SUCCESS,
  GET_SILAE_ACCOUNT_FAIL,

  POST_ACCOUNT_PARAM_ATTEMPT,
  POST_ACCOUNT_PARAM_SUCCESS,
  POST_ACCOUNT_PARAM_FAIL,

  LOAD_ODPAY_ATTEMPT,
  LOAD_ODPAY_SUCCESS,
  LOAD_ODPAY_FAIL,

  PREPARE_COMMENT_FOR_NEW_ENTRY,

  RESET_COMMENT_FOR_NEW_ENTRY,

  GET_WORKSHEET_TYPE_ATTEMPT,
  GET_WORKSHEET_TYPE_SUCCESS,
  GET_WORKSHEET_TYPE_FAIL,

  GET_WORKSHEET_PARAM_ATTEMPT,
  GET_WORKSHEET_PARAM_SUCCESS,
  GET_WORKSHEET_PARAM_FAIL,

  SET_CURRENT_ENTRY_LINE,
  SET_CURRENT_ENTRY,

  CREATE_ACCOUNT_SUCCESS,
  UPDATE_ACCOUNT_SUCCESS,

  SET_INFORMATION_DIALOG
} from './constants';

const initialState = {
  entries: {},
  selectedEntries: {},
  diaries: [],
  account: [],
  exercice: [],
  paymentType: [],
  newEntries: {},
  deleteConfirmation: {},
  isEntriesLoading: false,
  openConfirmFromHeader: false,
  openPermissionFromHeader: false,
  silae: {
    list: [],
    loading: false
  },
  postAccountParam: {
    loading: false
  },
  comment: {},
  worksheetType: [],
  worksheetParam: [],
  entryLinePosition: null
};

const accounting = (state = initialState, action) => {
  const {
    societyId,
    entry,
    entryId
  } = action;

  if (action && action.type) {
    switch (action.type) {
    case LOAD_ODPAY_ATTEMPT:
      return {
        ...state,
        loadOdpay: {
          ...state.loadOdpay,
          loading: true
        }
      };
    case LOAD_ODPAY_SUCCESS:
      return {
        ...state,
        loadOdpay: {
          ...state.loadOdpay,
          loading: false
        }
      };
    case LOAD_ODPAY_FAIL:
      return {
        ...state,
        loadOdpay: {
          ...state.loadOdpay,
          loading: false
        }
      };
    case POST_ACCOUNT_PARAM_ATTEMPT:
      return {
        ...state,
        postAccountParam: {
          ...state.postAccountParam,
          loading: true
        }
      };

    case POST_ACCOUNT_PARAM_SUCCESS:
      return {
        ...state,
        postAccountParam: {
          ...state.postAccountParam,
          loading: false
        }
      };

    case POST_ACCOUNT_PARAM_FAIL:
      return {
        ...state,
        postAccountParam: {
          ...state.postAccountParam,
          loading: false
        }
      };

    case GET_SILAE_ACCOUNT_ATTEMPT:
      return {
        ...state,
        silae: {
          ...state.Silae,
          list: [],
          loading: true
        }
      };
    case GET_SILAE_ACCOUNT_SUCCESS:
      return {
        ...state,
        silae: {
          ...state.Silae,
          list: action.payload,
          loading: false
        }
      };
    case GET_SILAE_ACCOUNT_FAIL:
      return {
        ...state,
        silae: {
          ...state.Silae,
          list: [],
          loading: false
        }
      };
    case GET_EXERCICE_ATTEMPT:
      return {
        ...state,
        exercice: {
          ...state.exercice,
          [societyId]: {
            ...state.exercice[societyId],
            defaultValue: [],
            exercice: [],
            load: true,
            error: false,
            loading: true
          }
        }
      };
    case GET_EXERCICE_SUCCESS:
      const { data, id } = action.payload;

      return {
        ...state,
        exercice: {
          ...state.exercice,
          [id]: {
            ...state.exercice[id],
            exercice: data,
            load: false,
            error: false,
            loading: false
          }
        }
      };
    case GET_EXERCICE_FAIL:
      return {
        ...state,
        ...state,
        exercice: {
          ...state.exercice,
          [societyId]: {
            ...state.exercice[societyId],
            load: false,
            error: true,
            loading: false
          }
        }
      };
    case GET_ENTRIES_ATTEMPT:
      return {
        ...state,
        entries: {
          ...state.entries,
          [societyId]: {
            ...action.opts,
            isLoading: true,
            isError: false,
            entry_array: []
          }
        }
      };
    case GET_ENTRIES_SUCCESS:
      return {
        ...state,

        entries: {
          ...state.entries,
          [societyId]: {
            ...state.entries[societyId],
            ...action.entries,
            isLoading: false,
            isError: false
          }
        }
      };
    case GET_ENTRIES_FAIL:
      return {
        ...state,
        entries: {
          ...state.entries,
          [societyId]: {
            ...state.entries[societyId],
            isLoading: false,
            isError: true,
            entry_array: []
          }
        }
      };
    case GET_DIARIES_SUCCESS:
      return {
        ...state,
        diaries: action.diaries
      };
    case GET_ACCOUNTING_SUCCESS:
      return {
        ...state,
        account: action.account
      };

    case CREATE_ACCOUNT_SUCCESS:
      return {
        ...state,
        account: [...state.account, action.account]
      };

    case UPDATE_ACCOUNT_SUCCESS:
      const updatedAccountIndex = state.account
        .findIndex(({ account_id }) => account_id === action.account.account_id);
      const updatedAccounts = [...state.account];

      updatedAccounts[updatedAccountIndex] = action.account;
      return {
        ...state,
        account: updatedAccounts
      };

    case GET_PAYMENT_TYPE_SUCCESS:
      return {
        ...state,
        paymentType: action.paymentType
      };
    case CREATE_ENTRY_ATTEMPT:
      return {
        ...state,
        [societyId]: {
          isCreateEntryLoading: true
        }
      };
    case CREATE_ENTRY_SUCCESS:
      return {
        ...state,
        [societyId]: {
          isCreateEntryLoading: false
        },
        selectedEntries: {
          ...state.selectedEntries,
          [societyId]: []
        }
      };
    case CREATE_ENTRY_FAIL:
      return {
        ...state,
        [societyId]: {
          isCreateEntryLoading: false,
          errorObject: action.errorObject
        }
      };
    case MODIFY_ENTRY_ATTEMPT:
      return {
        ...state,
        [societyId]: {
          modifyEntryId: entryId
        }
      };
    case MODIFY_ENTRY_SUCCESS:
      return {
        ...state,
        [societyId]: {
          modifyEntryId: undefined
        },
        entries: {
          ...state.entries,
          [societyId]: updateEntry(state.entries[societyId], action.entry)
        }
      };
    case UPDATE_ENTRY:
      return {
        ...state,
        entries: {
          ...state.entries,
          [societyId]: updateEntry(state.entries[societyId], action.entry)
        }
      };
    case MODIFY_ENTRY_FAIL:
      return {
        ...state,
        [societyId]: {
          modifyEntryId: undefined
        }
      };
    case DELETE_ENTRY_SUCCESS:
    case DELETE_ENTRY_TEMP_SUCCESS:
      return {
        ...state,
        entries: {
          ...state.entries,
          [societyId]: deleteEntry(state.entries[societyId], action.entries)
        }
      };
    case ADD_ENTRY:
      return {
        ...state,
        selectedEntries: {
          ...state.selectedEntries,
          [societyId]: [..._.get(state, `selectedEntries[${societyId}]`, []), entry.entry_id]
        }
      };
    case REMOVE_ENTRY:
      return {
        ...state,
        selectedEntries: {
          ...state.selectedEntries,
          [societyId]: _.get(state, `selectedEntries[${societyId}]`, []).filter(e => e !== entry.entry_id)
        }
      };
    case RESET_ALL_ENTRIES:
      return {
        ...state,
        selectedEntries: {
          ...state.selectedEntries,
          [societyId]: []
        }
      };
    case ADD_ALL_ENTRIES:
      return {
        ...state,
        selectedEntries: {
          ...state.selectedEntries,
          [societyId]: _.get(state, `entries[${societyId}].entry_array`, []).map(e => e.entry_id)
        }
      };
    case OPEN_SHOULD_DELETE_ENTRIES:
      try {
        return {
          ...state,
          deleteConfirmation: {
            [societyId]: {
              isOpen: true,
              entries: action.entries,
              entriesName: action.entriesName,
              canValidate: action.entries.every(
                id => (state.entries[societyId].entry_array.find(
                  ({ entry_id }) => entry_id === id
                ) || { entry_list: [] }).entry_list.every(
                  e => !e.flags || ((!e.flags.worksheet || e.flags.worksheet.length === 0)
                  && (!e.flags.immo || e.flags.immo.length === 0))
                )
              )
            }
          }
        };
      } catch (err) {
        throw err;
      }
    case CLOSE_SHOULD_DELETE_ENTRIES:
      return {
        ...state,
        deleteConfirmation: {
          [societyId]: {
            isOpen: false
          }
        }
      };
    case OPEN_CONFIRMATION_FROM_HEADER:
      const {
        openConfirmFromHeader
      } = action;

      return {
        ...state,
        openConfirmFromHeader
      };
    case OPEN_PERMISSION_FROM_HEADER:
      const {
        openPermissionFromHeader
      } = action;

      return {
        ...state,
        openPermissionFromHeader
      };
    case OPEN_INFORMATION_FROM_HEADER:
      const {
        openInformationFromHeader
      } = action;

      return {
        ...state,
        openInformationFromHeader
      };
    case GET_ENTRY_COMMENTS_ATTEMPT: {
      return {
        ...state,
        entries: {
          ...state.entries,
          [action.society_id]: {
            ...state.entries[action.society_id],
            comments: []
          }
        }
      };
    }
    case GET_ENTRY_COMMENTS_FAIL: {
      return {
        ...state,
        entries: {
          ...state.entries,
          [action.society_id]: {
            ...state.entries[action.society_id],
            comments: []
          }
        }
      };
    }
    case GET_ENTRY_COMMENTS_SUCCESS: {
      const {
        comments,
        society_id
      } = action.payload;

      return {
        ...state,
        entries: {
          ...state.entries,
          [society_id]: {
            ...state.entries[society_id],
            comments
          }
        }
      };
    }

    // TODO: Add loading, error states
    case POST_COMMENT_ATTEMPT: {
      return state;
    }
    case POST_COMMENT_FAIL: {
      return state;
    }
    case POST_COMMENT_SUCCESS: {
      const {
        comment,
        society_id
      } = action.payload;

      return {
        ...state,
        entries: {
          ...state.entries,
          [society_id]: {
            ...state.entries[society_id],
            comments: comment
          }
        }
      };
    }

    case PUT_COMMENT_ATTEMPT: {
      return state;
    }
    case PUT_COMMENT_FAIL: {
      return state;
    }
    case PUT_COMMENT_SUCCESS: {
      const {
        comment,
        society_id
      } = action.payload;

      return {
        ...state,
        entries: {
          ...state.entries,
          [society_id]: {
            ...state.entries[society_id],
            comments: comment
          }
        }
      };
    }
    case PREPARE_COMMENT_FOR_NEW_ENTRY:
      return {
        ...state,
        comment: action.comment
      };
    case RESET_COMMENT_FOR_NEW_ENTRY:
      return {
        ...state,
        comment: {}
      };

    case GET_WORKSHEET_TYPE_ATTEMPT:
      return {
        ...state
      };
    case GET_WORKSHEET_TYPE_SUCCESS:
      return {
        ...state,
        worksheetType: action.payload
      };
    case GET_WORKSHEET_TYPE_FAIL:
      return {
        ...state
      };

    case GET_WORKSHEET_PARAM_ATTEMPT:
      return {
        ...state
      };
    case GET_WORKSHEET_PARAM_SUCCESS:
      return {
        ...state,
        worksheetParam: {
          ...state.worksheetParam,
          [action.payload.societyId]: {
            ...state.worksheetParam[action.payload.societyId],
            param: action.payload.data === null ? [] : action.payload.data
          }
        }
      };
    case GET_WORKSHEET_PARAM_FAIL:
      return {
        ...state,
        worksheetParam: {
          ...state.worksheetParam,
          [action.payload.societyId]: {
            ...state.worksheetParam[action.payload.societyId],
            param: []
          }
        }
      };

    case SET_CURRENT_ENTRY_LINE:
      return {
        ...state,
        entryLinePosition: action.i
      };

    case SET_CURRENT_ENTRY:
      return {
        ...state,
        currentEntry: {
          ...state.currentEntry,
          [action.society_id]: action.currentEntry
        }
      };

    case SET_INFORMATION_DIALOG:
      return {
        ...state,
        isInformationDialogOpen: action.isOpen
      };

    default: return state;
    }
  }
  return state;
};

export default accounting;
