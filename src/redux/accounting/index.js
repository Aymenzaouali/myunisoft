import { getFormValues, arrayPush, change } from 'redux-form';
import moment from 'moment';
import _ from 'lodash';

import { windev, mock } from 'helpers/api';
import { handleError } from 'common/redux/error';

import { convertFormEntry, parseFilters } from 'helpers/accounting';
import { formatEntry } from 'helpers/format';
import { formatNumber } from 'helpers/number';
import { getCurrentTabState } from 'helpers/tabs';

import { getDefaultFilter } from 'redux/tabs/accounting';
import { addManualDocs, resetManualDocs } from 'redux/tabs/accounting/actions';
import { getDocumentsByRowNumbers, refreshDocuments } from 'redux/tabs/documents';

import { openAutoReverseDialog } from 'redux/autoReverseDialog/actions';

import actions from './actions';

const getCommentParam = (state) => {
  const society_id = _.get(state, 'navigation.id');
  const entry_type = _.get(state, `accountingWeb.entries[${society_id}].type`, 'E');
  let param;

  switch (entry_type) {
  case 'E':
    param = {
      location: 'entries'
    };
    break;
  case 'O':
    param = {
      location: 'ENTRIES_OCR'
    };
    break;
  case 'IB':
    param = {
      location: 'ENTRIES_RB'
    };
    break;
  default:
    break;
  }

  return param;
};

export const loadOdpay = () => async (dispatch, getState) => {
  try {
    await dispatch(actions.loadOdpayAttempt());
    const state = getState();
    const societyId = _.get(state, 'navigation.id', false);
    const formValues = getFormValues(`${societyId}accountingFilter`)(state);
    const params = {
      start_date: moment(formValues.od_start_date).format('YYYY-MM-DD'),
      end_date: moment(formValues.od_end_date).format('YYYY-MM-DD'),
      society_id: societyId
    };
    const response = await windev.makeApiCall('/silae/od_paie', 'get', params, {});
    const { data } = response;
    await dispatch(actions.loadOdpaySuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.loadOdpayFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const postAccountParam = () => async (dispatch, getState) => {
  try {
    await dispatch(actions.postAccountParamAttempt());
    const state = getState();
    const formValues = getFormValues('accountParamDialog')(state);
    const {
      account_number_original,
      final_account
    } = formValues;

    const param = {
      account_number_final: final_account.account_id,
      account_number_original
    };

    const response = await windev.makeApiCall('/account_third_parties', 'post', {}, param);
    const { data } = response;
    await dispatch(actions.postAccountParamSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.postAccountParamFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getAccountParam = () => async (dispatch) => {
  try {
    await dispatch(actions.getSilaeAccountAttempt());
    const response = await windev.makeApiCall('/account_third_parties', 'get', {}, {});
    const { data } = response;
    await dispatch(actions.getSilaeAccountSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.getSilaeAccountFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getSilaeAccounts = society_id => async (dispatch) => {
  try {
    await dispatch(actions.getSilaeAccountAttempt());
    const response = await mock.makeMockCall('/silae/account', 'get', { society_id }, {});
    const { data } = response;
    await dispatch(actions.getSilaeAccountSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.getSilaeAccountFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getExercice = (ste_id, id) => async (dispatch, getState) => {
  const state = getState();
  const society_id = ste_id || _.get(state, 'navigation.id');
  try {
    await dispatch(actions.getExerciceAttempt(society_id));
    const response = await windev.makeApiCall('/exercices', 'get', { society_id }, {});
    const { data } = response;
    if (id) await dispatch(change(`exportForm_${id}`, 'exercises', data)); // For FEC export feature
    await dispatch(actions.getExerciceSuccess(society_id, data));
    return response;
  } catch (err) {
    await dispatch(actions.getExerciceFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getEntries = (opts = {}) => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;
  const filters = { ...getFormValues(`${society_id}accountingFilter`)(state) || {} };

  if (opts.entry_id) {
    filters.entry_id = opts.entry_id;
  }

  // Prepare params
  const prev = _.get(state, `accountingWeb.entries[${society_id}]`, {});

  const sort = ((typeof opts.sort === 'string') && opts.sort) || ((typeof prev.sort === 'string') && prev.sort) || 'ecr';
  const direction = opts.direction || prev.direction || 'desc';

  const params = {
    society_id,
    type: opts.entry_id ? 'e' : _.get(filters, 'type', 'e')
  };

  const body = {
    sort: { [sort]: direction },
    filters: parseFilters(filters)
  };

  try {
    await dispatch(actions.getEntriesAttempt(society_id, {
      sort,
      direction,
      filters: body.filters
    }));

    const { data } = await windev.makeApiCall('/entries', 'post', params, body);
    await dispatch(actions.getEntriesSuccess(society_id, data));
    return data;
  } catch (err) {
    console.error(err); // eslint-disable-line
    await dispatch(actions.getEntriesFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getDiaries = (society_id, q, limit = 4) => async (dispatch) => {
  try {
    await dispatch(actions.getDiariesAttempt());
    const response = await windev.makeApiCall('/diary', 'get', { society_id, limit, q }, {});
    const { data } = response;
    await dispatch(actions.getDiariesSuccess(data));
    return response;
  } catch (err) {
    console.log(err.response); // eslint-disable-line
    await dispatch(actions.getDiariesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteEntry = (entries, deleteConfirmation = {}) => async (dispatch, getState) => {
  try {
    const society_id = _.get(getState(), 'navigation.id', false);
    await dispatch(actions.deleteEntryAttempt());
    const entry = entries.map(e => ({ entry_id: e }));
    const response = await windev.makeApiCall('/entry', 'delete', deleteConfirmation, { entry });
    await dispatch(actions.deleteEntrySuccess(society_id, entries));
    await dispatch(getDefaultFilter());
    dispatch(actions.resetAllEntries(society_id));
    return response;
  } catch (err) {
    console.log(err); // eslint-disable-line
    await dispatch(actions.deleteEntryFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteEntryTemp = entries => async (dispatch, getState) => {
  const body = {
    entry_temp: entries.map(entry_temp_id => ({ entry_temp_id }))
  };
  try {
    const society_id = _.get(getState(), 'navigation.id', false);
    await dispatch(actions.deleteEntryTempAttempt());
    const response = await windev.makeApiCall('/entry_temp', 'delete', {}, body);
    await dispatch(actions.deleteEntryTempSuccess(entries, society_id));
    return response;
  } catch (err) {
    console.log(err.response); // eslint-disable-line
    await dispatch(actions.deleteEntryTempFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getAccount = (society_id, q, limit = 4) => async (dispatch) => {
  try {
    await dispatch(actions.getAccountingAttempt());
    const response = await windev.makeApiCall('/account', 'get', {
      society_id, limit, q, mode: 1
    }, {});
    const { data } = response;
    await dispatch(actions.getAccountingSuccess(data));
    return response;
  } catch (err) {
    console.log(err.response); // eslint-disable-line
    await dispatch(actions.getAccountingFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getPaymentType = (society_id, q, limit = 4) => async (dispatch) => {
  try {
    await dispatch(actions.getPaymentTypeAttempt());
    const response = await windev.makeApiCall('/payment_type', 'get', { society_id, limit, q }, {});
    const { data } = response;
    await dispatch(actions.getPaymentTypeSuccess(data));
    return response;
  } catch (err) {
    console.log(err.response); // eslint-disable-line
    await dispatch(actions.getPaymentTypeFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getEntryCounterPart = entry => async (dispatch) => {
  try {
    await dispatch(actions.getEntryCounterPartAttempt());
    const response = await windev.makeApiCall('/entry/counterpart_line', 'post', {}, entry);
    const { data } = response;
    await dispatch(actions.getEntryCounterPartSuccess(data));
    return data;
  } catch (err) {
    console.log(err.response); // eslint-disable-line
    await dispatch(actions.getEntryCounterPartFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const modifyEntry = (entry, formName, typeEntry) => async (dispatch, getState) => {
  const state = getState();
  const societyId = state.navigation.id;
  const currentRouteKey = _.get(state, 'navigation.routeKey');
  try {
    await dispatch(actions.modifyEntryAttempt(societyId, entry.entry_id));
    const { data } = await windev.makeApiCall('/entry', 'put', { type: typeEntry || entry.type || 'e' }, entry);
    if (currentRouteKey === 'newAccounting') {
      await dispatch(actions.modifyEntrySuccess(societyId, convertFormEntry(entry)));
      await dispatch(getDefaultFilter());
      await dispatch(getEntries());
    }
    return data;
  } catch (err) {
    if (_.get(err, 'response.data.message') === 'already_reversed') {
      dispatch(openAutoReverseDialog());
    }
    console.log(err.response); // eslint-disable-line
    await dispatch(actions.modifyEntryFail(societyId));
    await dispatch(handleError(err));
    throw err.response;
  }
};

export const validateEntries = entries => async (dispatch) => {
  try {
    await dispatch(actions.validateEntriesAttempt());
    const response = await windev.makeApiCall('/entry/validate', 'post', {}, entries);
    const { data } = response;
    await dispatch(actions.validateEntriesSuccess(data));
    await dispatch(getEntries());
    return data;
  } catch (err) {
    console.log(err.response); // eslint-disable-line
    await dispatch(actions.validateEntriesFail());
    await dispatch(handleError(err));
    throw err.response;
  }
};

export const createEntry = entry => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    const body = _.get(state, 'accountingWeb.comment.body');
    const newEntry = body ? { comment: { content: body }, ...entry } : entry;
    await dispatch(actions.createEntryAttempt(society_id));
    const filtersValues = getFormValues(`${society_id}accountingFilter`)(state) || {};
    const { data } = await windev.makeApiCall('/entry', 'post', { type: entry.type || 'e' }, newEntry);

    if (filtersValues.type === 'm') {
      const { accounting } = getCurrentTabState(state);
      const indexes = _.values(accounting.manual_docs).map(docs => docs.row_number);

      const [, attached] = await Promise.all([
        dispatch(refreshDocuments()), // refresh screen
        dispatch(getDocumentsByRowNumbers(indexes)) // load new attached documents
      ]);

      await dispatch(resetManualDocs());
      await dispatch(addManualDocs(attached));
    }

    await dispatch(actions.resetCommentForNewEntry());
    if (filtersValues === {}) await dispatch(getDefaultFilter());
    await dispatch(getEntries());
    await dispatch(actions.createEntrySuccess(society_id));
    return data;
  } catch (err) {
    console.log(err.response); // eslint-disable-line no-console
    await dispatch(actions.createEntryFail(society_id, err.response.data));
    await dispatch(handleError(err));
    throw err.response;
  }
};

export const counterPartLines = payload => async (dispatch, getState) => {
  try {
    const state = getState();
    const societyId = _.get(state, 'navigation.id', false);
    const formValues = payload || getFormValues(`${societyId}newAccounting`)(state);
    const entry = await formatEntry(formValues, societyId);
    const backEntry = await dispatch(
      getEntryCounterPart({ ...entry, etablissement_id: societyId })
    );
    backEntry.entry_list.forEach((entry, i) => {
      if (i !== 0) {
        dispatch(arrayPush(`${societyId}newAccounting`, 'entry_list', {
          account: entry.account,
          label: entry.label,
          date_piece: backEntry.date_piece,
          deadline: entry.deadline,
          debit: !entry.debit ? null : formatNumber(entry.debit),
          credit: !entry.credit ? null : formatNumber(entry.credit),
          payment_type: entry.payment_type_id,
          piece: entry.piece,
          piece2: entry.piece2,
          date_entry: moment(new Date()).format('YYYY-MM-DD')
        }));
      }
    });
  } catch (err) {
    console.log(err); //eslint-disable-line
  }
};

export const getEntryComments = entry_id => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');
  const entry_type = _.get(state, `accountingWeb.entries[${society_id}].type`, 'E');
  let param;

  switch (entry_type) {
  case 'E':
    param = {
      entry_id
    };
    break;
  case 'O':
    param = {
      ocr_id: entry_id
    };
    break;
  case 'IB':
    param = {
      rb_id: entry_id
    };
    break;
  default:
    break;
  }

  try {
    await dispatch(actions.getEntryCommentsAttempt(society_id));
    const { data } = await windev.makeApiCall(
      '/comments',
      'get',
      param
    );
    await dispatch(actions.getEntryCommentsSuccess({
      society_id,
      comments: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.getEntryCommentsFail(society_id));
    return error;
  }
};

export const postComment = (entry_id, comment) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');
  const param = getCommentParam(state, entry_id);

  try {
    await dispatch(actions.postCommentAttempt(society_id));

    const {
      body,
      attachTo
    } = comment;

    const { data } = await windev.makeApiCall(
      '/comments',
      'post',
      {},
      {
        ...param,
        entry_id,
        society_id,
        comment: body,
        attachTo
      }
    );

    await dispatch(actions.postCommentSuccess({
      society_id,
      comment: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.postCommentFail(society_id));
    return error;
  }
};

export const putComment = (entry_id, comment) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');
  const param = getCommentParam(state);

  const {
    body,
    id
  } = comment;

  try {
    await dispatch(actions.putCommentAttempt(society_id));
    const { data } = await windev.makeApiCall(
      `/comments/${id}`,
      'put',
      {},
      {
        ...param,
        entry_id,
        comment: body
      }
    );
    await dispatch(actions.putCommentSuccess({
      society_id,
      comment: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.putCommentFail(society_id));
    return error;
  }
};

export const getWorksheetType = () => async (dispatch) => {
  try {
    const params = { flags: true };
    await dispatch(actions.getWorksheetTypeAttempt());
    const response = await windev.makeApiCall('/worksheet/type', 'get', params);
    const { data } = response;
    await dispatch(actions.getWorksheetTypeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getWorksheetTypeFail());
    return err;
  }
};

export const getWorksheetParam = code_worksheet => async (dispatch, getState) => {
  const society_id = _.get(getState(), 'navigation.id');
  try {
    await dispatch(actions.getWorksheetParamAttempt());
    const { data } = await windev.makeApiCall('/worksheet/param', 'get', { code_worksheet });
    await dispatch(actions.getWorksheetParamSuccess(data, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.getWorksheetParamFail(society_id));
    return err;
  }
};

export const createAccount = account => async (dispatch) => {
  try {
    await dispatch(actions.createAccountAttempt());
    const response = await windev.makeApiCall('/account', 'post', {}, account);
    const { data } = response;
    await dispatch(actions.createAccountSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.createAccountFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const updateAccount = account => async (dispatch) => {
  try {
    await dispatch(actions.updateAccountAttempt());
    const response = await windev.makeApiCall('/account', 'put', {}, account);
    const { data } = response;
    await dispatch(actions.updateAccountSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.updateAccountFail());
    await dispatch(handleError(err));
    throw err;
  }
};
