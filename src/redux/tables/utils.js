import _get from 'lodash/get';

export const getTableName = (societyId, tableName) => `${tableName}-${societyId}`;

export const getFieldProps = (editRow, item, propName, tableName) => ({
  placeholder: _get(item, propName),
  defaultValue: _get(item, propName),
  onBlur: (e) => {
    const newItem = {
      ...item,
      [propName]: e.target.value
    };
    if (tableName) {
      editRow(tableName, newItem);
    } else {
      editRow(newItem);
    }
  }
});

export const getFieldPropsV2 = (editRow, item, valueName, editName, tableName) => ({
  placeholder: _get(item, valueName),
  defaultValue: _get(item, valueName),
  onBlur: (value) => {
    if (tableName) {
      const newItem = {
        ...item,
        [editName]: value
      };
      editRow(tableName, newItem);
    }
  }
});

export const getAutoCompleteProps = (
  editRow,
  item,
  placeholderName,
  editName,
  tableName
) => ({
  placeholder: _get(item, placeholderName),
  value: _get(item, editName),
  onChange: (value) => {
    if (tableName) {
      const newItem = {
        ...item,
        [editName]: value
      };
      editRow(tableName, newItem);
    }
  }
});
