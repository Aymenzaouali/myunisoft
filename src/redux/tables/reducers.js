import _get from 'lodash/get';
import _isEqual from 'lodash/isEqual';
import {
  START_EDIT,
  END_EDIT,
  SET_DATA,
  UPDATE_DATA,
  CREATE_ROW,
  EDIT_ROW,
  DELETE_ROWS,
  CANCEL,
  SAVE,
  SELECT_ROW,
  SELET_ONLY_ONE_ROW,
  SELECT_ALL_ROWS,
  UNSELECT_ROW,
  UNSELECT_ALL_ROWS,
  SET_ERRORS,
  SET_ALL_ERRORS,
  SET_ALL_LINES_ERRORS,
  SET_PAGINATION,
  SET_TRANSFORMED_TABLE_DATA
} from './constants';

/**
 *  {
 *    [tableName]: {
 *      selectedRows: Object,
 *      selectedRowsCount: Number,
 *      lastSelectedRow: (String|Number)?
 *    }
 *  }
 */

const defaultState = {
  createdRows: {},
  createdRowsCount: 0,
  data: [],
  deletedRows: {},
  editedRows: {},
  initialData: [],
  isEditing: false,
  lastSelectedRow: null,
  selectedRows: {},
  selectedRowsCount: 0,
  errors: {},
  transformedTableData: {},
  pagination: {
    limit: 20,
    offset: 0
  }
};

const tables = (state = {}, action) => {
  const currentTable = _get(state, action.tableName, {});
  const currentSelectionCount = _get(state, `${action.tableName}.selectedRowsCount`, 0);
  const createdRows = _get(currentTable, 'createdRows', {});
  const createdRowsCount = _get(currentTable, 'createdRowsCount', 0);
  const data = _get(currentTable, 'data', []);
  const deletedRows = _get(currentTable, 'deletedRows', {});
  const editedRows = _get(currentTable, 'editedRows', {});
  const initialData = _get(currentTable, 'initialData', []);
  const selectedRows = _get(currentTable, 'selectedRows', {});
  const selectedRowsCount = _get(currentTable, 'selectedRowsCount', 0);
  const errors = _get(currentTable, 'errors', {});
  const pagination = _get(state, action.pagination, {});
  switch (action.type) {
  case START_EDIT:
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        isEditing: true
      }
    };
  case END_EDIT:
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        isEditing: false
      }
    };
  case SET_PAGINATION: {
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        pagination
      }
    };
  }
  case SET_DATA:
    return {
      ...state,
      [action.tableName]: {
        ...defaultState,
        data: action.data,
        initialData: action.data
      }
    };
  case UPDATE_DATA:
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        data: action.data
      }
    };
  case CREATE_ROW:
    const newId = _get(action, 'options.defaultValue.id', `created-${createdRowsCount}`);
    const createdRow = { id: newId, ...action.options.defaultValue, isCreated: true };
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        createdRows: {
          ...createdRows,
          [newId]: createdRow
        },
        createdRowsCount: createdRowsCount + 1,
        data: [createdRow, ...data],
        isEditing: true,
        lastSelectedRow: newId,
        selectedRows: {
          ...selectedRows,
          [newId]: true
        },
        selectedRowsCount: selectedRowsCount + 1
      }
    };
  case EDIT_ROW:
    const initialItem = initialData.find(i => i.id === action.item.id);
    const isEdited = !_isEqual(initialItem, action.item);
    const isCreated = _get(action, 'item.isCreated');
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        createdRows: isCreated
          ? {
            ...createdRows,
            [action.item.id]: action.item
          }
          : createdRows,
        data: _get(currentTable, 'data', []).map(i => (i.id === action.item.id ? action.item : i)),
        editedRows: (isEdited && !isCreated)
          ? {
            ...editedRows,
            [action.item.id]: action.item
          } : editedRows
      }
    };
  case DELETE_ROWS:
    const { options: { createdOnly } } = action;
    const oldSelectedRows = Object.keys(selectedRows).reduce((acc, key) => {
      if (selectedRows[key] && !createdRows[key]) {
        acc[key] = true;
      }

      return acc;
    }, {});
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        createdRows: {},
        data: data.filter(
          i => (createdOnly && !createdRows[i.id]) || !selectedRows[i.id]
        ),
        deletedRows: {
          ...deletedRows,
          ...oldSelectedRows
        },
        lastSelectedRow: null,
        selectedRows: {},
        selectedRowsCount: 0
      }
    };
  case CANCEL:
    return {
      ...state,
      [action.tableName]: {
        ...defaultState,
        data: initialData,
        initialData,
        isEditing: false
      }
    };
  case SAVE:
    const newData = data.map(item => ({ ...item, isCreated: undefined }));
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        createdRows: {},
        deletedRows: {},
        editedRows: {},
        data: newData,
        initialData: newData,
        errors: {},
        isEditing: false
      }
    };
  case SELECT_ROW:
    const isAlreadySelected = selectedRows && selectedRows[action.id];
    return isAlreadySelected ? state : {
      ...state,
      [action.tableName]: {
        ...currentTable,
        selectedRows: {
          ...selectedRows,
          [action.id]: true
        },
        selectedRowsCount: currentSelectionCount + 1,
        lastSelectedRow: action.id
      }
    };
  case UNSELECT_ROW:
    const isAlreadyUnselected = !selectedRows[action.id];
    return isAlreadyUnselected ? state : {
      ...state,
      [action.tableName]: {
        ...currentTable,
        selectedRows: {
          ...selectedRows,
          [action.id]: false
        },
        selectedRowsCount: currentSelectionCount - 1,
        lastSelectedRow: currentSelectionCount - 1 !== 0
          ? state[action.tableName].lastSelectedRow : null
      }
    };
  case SELET_ONLY_ONE_ROW:
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        selectedRows: {
          [action.id]: true
        },
        selectedRowsCount: 1,
        lastSelectedRow: action.id
      }
    };
  case UNSELECT_ALL_ROWS:
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        selectedRows: {},
        selectedRowsCount: 0,
        lastSelectedRow: null
      }
    };
  case SELECT_ALL_ROWS:
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        selectedRows: (action.ids || []).reduce((acc, id) => ({ ...acc, [id]: true }), {}),
        selectedRowsCount: (action.ids || []).length,
        lastSelectedRow: _get(action, 'ids[0]', null)
      }
    };
  case SET_ERRORS:
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        errors: {
          ...errors,
          [action.id]: {
            ...errors[action.id],
            ...action.errors
          }
        }
      }
    };
  case SET_ALL_ERRORS:
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        errors: {
          [action.id]: action.errors || {}
        }
      }
    };
  case SET_ALL_LINES_ERRORS:
    return {
      ...state,
      [action.tableName]: {
        ...currentTable,
        errors: action.errors || {}
      }
    };
  case SET_TRANSFORMED_TABLE_DATA:
    return {
      ...state,
      transformedTableData: {
        ...state.transformedTableData,
        [action.tableName]: {
          [action.id]: {
            params: action.tableData
          }
        }
      }
    };
  default:
    return state;
  }
};

export default tables;
