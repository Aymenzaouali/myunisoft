import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';

export const createTokenPWD = ({ mail, redirectUrl }) => async (dispatch) => {
  const body = { user_email: mail, url_server: redirectUrl };
  try {
    const response = await windev.makeApiCall('/user/create_token_pwd', 'post', {}, body);
    const { data } = response;
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    throw err;
  }
};

export const replacePassword = ({ user_mail, token, password }) => async (dispatch) => {
  const body = {
    user_mail,
    token,
    password
  };

  try {
    const response = await windev.makeApiCall('/user/replace_password', 'post', {}, body);
    const { data } = response;
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    throw err;
  }
};
