import _ from 'lodash';
import {
  ADD_ACCOUNT,
  REMOVE_ACCOUNT,
  RESET_ALL_ACCOUNTS,
  ADD_ALL_ACCOUNTS,
  GET_CHARTACCOUNT_ATTEMPT,
  GET_CHARTACCOUNT_FAIL,
  GET_CHARTACCOUNT_SUCCESS,
  GET_ACCOUNTCLASS_ATTEMPT,
  GET_ACCOUNTCLASS_SUCCESS,
  GET_ACCOUNTCLASS_FAIL,
  GET_ACCOUNT_DETAIL_ATTEMPT,
  GET_ACCOUNT_DETAIL_SUCCESS,
  GET_ACCOUNT_DETAIL_FAIL,
  SELECT_ACCOUNT
} from './constants';

const initialState = {
  accountList: {},
  selectedAccount: {},
  maxRowsChartAccount: 20,
  accountClass: [],
  accountSelected: null,
  accountDetail: {},
  chartAccountIsLoading: false,
  accountClassIsLoading: false,
  accountDetailIsLoading: false
};

const chartAccount = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_CHARTACCOUNT_ATTEMPT:
      return {
        ...state,
        accountList: {
          ...state.accountList,
          [action.societyId]: {
            ...state.accountList[action.societyId],
            isLoading: true,
            isError: false,
            account_array: []
          }
        }
      };
    case GET_CHARTACCOUNT_SUCCESS:
      const {
        accountList,
        societyId,
        opts
      } = action;

      return {
        ...state,
        accountList: {
          ...state.accountList,
          [societyId]: {
            ..._.get(state, `accountList[${societyId}]`, {}),
            ...accountList,
            ...opts,
            isLoading: false
          }
        }
      };
    case GET_CHARTACCOUNT_FAIL:
      return {
        ...state,
        accountList: {
          ...state.accountList,
          [action.societyId]: {
            ...state.accountList[action.societyId],
            isLoading: false,
            isError: true,
            account_array: []
          }
        }
      };
    case GET_ACCOUNTCLASS_ATTEMPT:
      return {
        ...state,
        accountClassIsLoading: true
      };
    case GET_ACCOUNTCLASS_SUCCESS:
      const {
        accountClass
      } = action;
      return {
        ...state,
        accountClass,
        accountClassIsLoading: false
      };
    case GET_ACCOUNTCLASS_FAIL:
      return {
        ...state,
        accountClassIsLoading: true
      };
    case GET_ACCOUNT_DETAIL_ATTEMPT:
      return {
        ...state,
        accountDetailIsLoading: true
      };
    case GET_ACCOUNT_DETAIL_SUCCESS:
      const {
        accountDetail
      } = action;

      return {
        ...state,
        accountDetail,
        accountDetailIsLoading: false
      };
    case GET_ACCOUNT_DETAIL_FAIL:
      return {
        ...state,
        accountDetailIsLoading: false
      };
    case SELECT_ACCOUNT:
      const {
        accountSelected
      } = action;

      return {
        ...state,
        accountSelected
      };
    case ADD_ACCOUNT:
      return {
        ...state,
        selectedAccount: {
          ...state.selectedAccount,
          [action.societyId]: [..._.get(state, `selectedAccount[${action.societyId}]`, []), action.account.account_id]
        }
      };
    case REMOVE_ACCOUNT:
      return {
        ...state,
        selectedAccount: {
          ...state.selectedAccount,
          [action.societyId]: _.get(state, `selectedAccount[${action.societyId}]`, []).filter(e => e !== action.account.account_id)
        }
      };
    case RESET_ALL_ACCOUNTS:
      return {
        ...state,
        selectedAccount: {
          ...state.selectedAccount,
          [action.societyId]: []
        }
      };
    case ADD_ALL_ACCOUNTS:
      return {
        ...state,
        selectedAccount: {
          ...state.selectedAccount,
          [action.societyId]: _.get(state, `accountList[${action.societyId}].account_array`, []).filter(e => !e.blocked).map(accountAvailable => accountAvailable.account_id)
        }
      };
    default: return state;
    }
  }
  return state;
};

export default chartAccount;
