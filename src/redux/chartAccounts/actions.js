import {
  GET_CHARTACCOUNT_ATTEMPT,
  GET_CHARTACCOUNT_SUCCESS,
  GET_CHARTACCOUNT_FAIL,
  CREATE_CHARTACCOUNT_ATTEMPT,
  CREATE_CHARTACCOUNT_SUCCESS,
  CREATE_CHARTACCOUNT_FAIL,
  UPDATE_CHARTACCOUNT_ATTEMPT,
  UPDATE_CHARTACCOUNT_SUCCESS,
  UPDATE_CHARTACCOUNT_FAIL,
  DELETE_CHARTACCOUNT_ATTEMPT,
  DELETE_CHARTACCOUNT_SUCCESS,
  DELETE_CHARTACCOUNT_FAIL,
  GET_ACCOUNTCLASS_ATTEMPT,
  GET_ACCOUNTCLASS_SUCCESS,
  GET_ACCOUNTCLASS_FAIL,
  GET_ACCOUNT_DETAIL_ATTEMPT,
  GET_ACCOUNT_DETAIL_SUCCESS,
  GET_ACCOUNT_DETAIL_FAIL,
  SELECT_ACCOUNT,
  ADD_ACCOUNT,
  REMOVE_ACCOUNT,
  RESET_ALL_ACCOUNTS,
  ADD_ALL_ACCOUNTS
} from './constants';

const getAccountClassAttempt = () => ({
  type: GET_ACCOUNTCLASS_ATTEMPT
});

const getAccountClassSuccess = accountClass => ({
  type: GET_ACCOUNTCLASS_SUCCESS,
  accountClass
});

const getAccountClassFail = () => ({
  type: GET_ACCOUNTCLASS_FAIL
});

const getChartAccountAttempt = societyId => ({
  type: GET_CHARTACCOUNT_ATTEMPT,
  societyId
});

const getChartAccountSuccess = (accountList, societyId, opts) => ({
  type: GET_CHARTACCOUNT_SUCCESS,
  societyId,
  accountList,
  opts
});

const getChartAccountFail = societyId => ({
  type: GET_CHARTACCOUNT_FAIL,
  societyId
});


const createChartAccountAttempt = () => ({
  type: CREATE_CHARTACCOUNT_ATTEMPT
});

const createChartAccountSuccess = account => ({
  type: CREATE_CHARTACCOUNT_SUCCESS,
  account
});

const createChartAccountFail = () => ({
  type: CREATE_CHARTACCOUNT_FAIL
});


const updateChartAccountAttempt = () => ({
  type: UPDATE_CHARTACCOUNT_ATTEMPT
});

const updateChartAccountSuccess = account => ({
  type: UPDATE_CHARTACCOUNT_SUCCESS,
  account
});

const updateChartAccountFail = () => ({
  type: UPDATE_CHARTACCOUNT_FAIL
});


const deleteChartAccountAttempt = () => ({
  type: DELETE_CHARTACCOUNT_ATTEMPT
});

const deleteChartAccountSuccess = account => ({
  type: DELETE_CHARTACCOUNT_SUCCESS,
  account
});

const deleteChartAccountFail = () => ({
  type: DELETE_CHARTACCOUNT_FAIL
});

const getAccountDetailAttempt = () => ({
  type: GET_ACCOUNT_DETAIL_ATTEMPT
});

const getAccountDetailSuccess = accountDetail => ({
  type: GET_ACCOUNT_DETAIL_SUCCESS,
  accountDetail
});
const getAccountDetailFail = () => ({
  type: GET_ACCOUNT_DETAIL_FAIL
});

const selectAccount = accountSelected => ({
  type: SELECT_ACCOUNT,
  accountSelected
});

const addAccount = (account, societyId) => ({
  type: ADD_ACCOUNT,
  account,
  societyId
});

const removeAccount = (account, societyId) => ({
  type: REMOVE_ACCOUNT,
  account,
  societyId
});

const resetAllAccounts = societyId => ({
  type: RESET_ALL_ACCOUNTS,
  societyId
});

const addAllAccounts = societyId => ({
  type: ADD_ALL_ACCOUNTS,
  societyId
});

export default {
  getChartAccountAttempt,
  getChartAccountSuccess,
  getChartAccountFail,
  createChartAccountAttempt,
  createChartAccountSuccess,
  createChartAccountFail,
  updateChartAccountAttempt,
  updateChartAccountSuccess,
  updateChartAccountFail,
  deleteChartAccountAttempt,
  deleteChartAccountSuccess,
  deleteChartAccountFail,
  getAccountClassAttempt,
  getAccountClassSuccess,
  getAccountClassFail,
  getAccountDetailAttempt,
  getAccountDetailSuccess,
  getAccountDetailFail,
  selectAccount,
  addAccount,
  removeAccount,
  resetAllAccounts,
  addAllAccounts
};
