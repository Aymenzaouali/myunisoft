import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import _ from 'lodash';
import actions from './actions';

export const getChartAccount = (opts = {}, societyId) => async (dispatch, getState) => {
  const state = getState();

  const {
    sort: sortOpts
  } = opts;

  const society_id = societyId !== undefined ? societyId : _.get(state, 'navigation.id', false);
  const chartAccountList = _.get(state, `chartAccountsWeb.accountList[${society_id}]`, '');
  const sort = sortOpts !== undefined ? sortOpts : _.get(chartAccountList, 'sort', '');

  const limit = 1000000;
  const page = 0;
  const offset = 0;

  try {
    const actionOpts = { limit, page, sort };

    if (!_.isEmpty(sortOpts)) {
      const direction = 'asc';
      const storedColumn = _.get(chartAccountList, 'sort.column');
      const currentColumn = _.get(sortOpts, 'column');
      if (currentColumn === storedColumn) {
        const storedDirection = _.get(chartAccountList, 'sort.direction');
        const direction = storedDirection === 'desc' ? 'asc' : 'desc';
        actionOpts.sort = { column: currentColumn, direction };
      } else {
        actionOpts.sort = { column: currentColumn, direction };
      }
    }

    const params = {
      society_id,
      begin_by: _.get(state, `form.${society_id}chartAccountFilter.values.classFilterChartAccount.value`),
      mode: 2,
      offset,
      limit,
      sort: actionOpts.sort
    };

    await dispatch(actions.getChartAccountAttempt(society_id));
    const response = await windev.makeApiCall('/account', 'get', params, {});
    const { data } = response;
    await dispatch(actions.getChartAccountSuccess(data, society_id, actionOpts));
    return response;
  } catch (err) {
    await dispatch(actions.getChartAccountFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getAccountClass = () => async (dispatch) => {
  try {
    await dispatch(actions.getAccountClassAttempt());
    const response = await windev.makeApiCall('/account/class', 'get', {}, {});
    const { data } = response;
    await dispatch(actions.getAccountClassSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.getAccountClassFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getAccountDetail = () => async (dispatch, getState) => {
  const state = getState();

  try {
    await dispatch(actions.getAccountDetailAttempt());
    const society_id = _.get(state, 'navigation.id', false);
    const account_id = _.get(state, 'chartAccountsWeb.accountSelected', false);
    const params = { society_id, account_id, mode: 2 };
    const response = await windev.makeApiCall('/account', 'get', params, {});
    const { data } = response;
    await dispatch(actions.getAccountDetailSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.getAccountDetailFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteAccount = accountList => async (dispatch, getState) => {
  try {
    const society_id = _.get(getState(), 'navigation.id', false);
    await dispatch(actions.deleteChartAccountAttempt());
    const account = accountList.map(e => ({ account_id: e }));
    const response = await windev.makeApiCall('/account', 'delete', {}, { account });
    const { data } = response;
    await dispatch(actions.deleteChartAccountSuccess(data));
    dispatch(actions.resetAllAccount(society_id));
    return response;
  } catch (err) {
    console.log(err.response); // eslint-disable-line
    await dispatch(actions.deleteChartAccountFail());
    await dispatch(handleError(err));
    return err;
  }
};
