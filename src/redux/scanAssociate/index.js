import { windev } from 'helpers/api';
import { reset } from 'redux-form';
import actions from './actions';

export class ScanAssociateService {
  static instance;

  static getInstance() {
    if (!ScanAssociateService.instance) {
      ScanAssociateService.instance = new ScanAssociateService();
    }
    return ScanAssociateService.instance;
  }

  sendPinCode = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const { associationCode } = state.form.scanAssociateForm.values;
    try {
      await dispatch(actions.sendPinCodeAttempt(society_id));
      const response = await windev.makeApiCall('/dematbox/subscribe', 'post',
        { code_pin: associationCode }, {});
      const statements = response.data ? response.data : [];
      await dispatch(actions.sendPinCodeSuccess(statements, society_id)).then(
        await dispatch(reset('scanAssociateForm'))
      );
      return statements;
    } catch (err) {
      await dispatch(actions.sendPinCodeFail(society_id));
      return err;
    }
  };
}


export default ScanAssociateService.getInstance();
