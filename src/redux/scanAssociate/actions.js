import {
  SEND_PIN_CODE_ATTEMPT,
  SEND_PIN_CODE_SUCCESS,
  SEND_PIN_CODE_FAIL
} from './constants';

const sendPinCodeAttempt = society_id => ({
  type: SEND_PIN_CODE_ATTEMPT,
  society_id
});

const sendPinCodeSuccess = society_id => ({
  type: SEND_PIN_CODE_SUCCESS,
  society_id
});

const sendPinCodeFail = society_id => ({
  type: SEND_PIN_CODE_FAIL,
  society_id
});

export default {
  sendPinCodeAttempt,
  sendPinCodeSuccess,
  sendPinCodeFail
};
