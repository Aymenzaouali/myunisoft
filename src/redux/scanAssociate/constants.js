export const SEND_PIN_CODE_ATTEMPT = 'scanAssociate/SEND_PIN_CODE_ATTEMPT';
export const SEND_PIN_CODE_SUCCESS = 'scanAssociate/SEND_PIN_CODE_SUCCESS';
export const SEND_PIN_CODE_FAIL = 'scanAssociate/SEND_PIN_CODE_FAIL';
