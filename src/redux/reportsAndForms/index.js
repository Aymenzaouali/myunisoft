import { windev } from 'helpers/api';
import tableActions from 'redux/tables/actions';
import { STATE_OR_FORM_DIALOG_TABLE_NAME, REPORT_AND_FORMS_TABLE, getTableName } from 'assets/constants/tableName';
import _ from 'lodash';
import actions from './actions';
import { mainTable, sateOrFormSelectedTable1, sateOrFormSelectedTable2 } from './mockData';

export class ReportsAndFormsService {
  static instance;

  static getInstance() {
    if (!ReportsAndFormsService.instance) {
      ReportsAndFormsService.instance = new ReportsAndFormsService();
    }
    return ReportsAndFormsService.instance;
  }

  getFormsAndReports = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = _.get(state, 'navigation.id');
    const tableName = getTableName(society_id, REPORT_AND_FORMS_TABLE);
    try {
      await dispatch(actions.getFormsAndReportsAttempt(society_id));
      // const response = await windev.makeApiCall('/', 'get');
      // put endpoint when it will be done
      const resp = mainTable;
      // const statements = response.data ? response.data : [];
      await dispatch(actions.getFormsAndReportsSuccess(resp, society_id));
      await dispatch(tableActions.setData(tableName, resp));
      return resp;
    } catch (err) {
      await dispatch(actions.getFormsAndReportsFail(society_id));
      return err;
    }
  };

  getFormsAndReportsAdditional = id => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;

    try {
      await dispatch(actions.getFormsAndReportsAdditionalAttempt(society_id));
      // const response = await windev.makeApiCall('/', 'get');
      let resp;
      if (id === 0) {
        resp = [
          {
            id: 0,
            orderNumber: '001',
            codeEDI: 'UNH',
            wording: 'Exemple de libellé',
            dataType: 'Label',
            viewing: 'Normal',
            dataSource: 'Compta ana toutes sections',
            references: '60'
          }
        ];
      }

      if (id === 1) {
        resp = [
          {
            id: 0,
            orderNumber: '001',
            codeEDI: 'UNH',
            wording: 'Exemple de libellé',
            dataType: 'Label',
            viewing: 'Normal',
            dataSource: 'Compta ana toutes sections',
            references: '60'
          },
          {
            id: 1,
            orderNumber: '002',
            codeEDI: 'UNH',
            wording: 'Exemple de libellé',
            dataType: 'Label',
            viewing: 'Normal',
            dataSource: 'Compta ana toutes sections',
            references: '80'
          }
        ];
      }
      // const statements = response.data ? response.data : [];
      await dispatch(actions.getFormsAndReportsAdditionalSuccess(resp, society_id));
      await dispatch(actions.setAdditionalReportsAndForms(true));
      return resp;
    } catch (err) {
      await dispatch(actions.getFormsAndReportsAdditionalFail(society_id));
      return err;
    }
  };

  getStateOrFormDialogTable = id => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, STATE_OR_FORM_DIALOG_TABLE_NAME);
    try {
      await dispatch(actions.getStateOrFormDialogTableAttempt(society_id));
      // const response = await windev.makeApiCall('/', 'get', { state: id });
      let resp = [];
      if (id === 1) {
        resp = sateOrFormSelectedTable1;
        await dispatch(tableActions.setData(tableName, sateOrFormSelectedTable1));
      }
      if (id === 2) {
        resp = sateOrFormSelectedTable2;
        await dispatch(tableActions.setData(tableName, sateOrFormSelectedTable2));
      }
      // const statements = response.data ? response.data : [];
      await dispatch(actions.getStateOrFormDialogTableSuccess(resp, society_id));
      return resp;
    } catch (err) {
      await dispatch(actions.getStateOrFormDialogTableFail(society_id));
      return err;
    }
  };

  sendReportAndForm = () => async (dispatch, getState) => { // TODO replace with real back end
    const state = getState();
    const reportsAndFormsForm = state.form.reportsAndFormsForm.values;
    const params = {
      ...reportsAndFormsForm
    };

    try {
      await dispatch(actions.sendReportAndFormAttempt());
      const response = await windev.makeApiCall('/fake_url', 'post', {}, params);
      const reports = response.data ? response.data : [];
      await dispatch(actions.sendReportAndFormSuccess(reports));
      return reports;
    } catch (err) {
      await dispatch(actions.sendReportAndFormFail());
      return err;
    }
  };

  modifyReportAndForm = () => async (dispatch, getState) => { // TODO replace with real back end
    const state = getState();
    const reportsAndFormsForm = state.form.reportsAndFormsForm.values;
    const params = {
      ...reportsAndFormsForm
    };
    try {
      await dispatch(actions.modifyReportAndFormAttempt());
      const response = await windev.makeApiCall('/fake_url', 'put', {}, params);
      const reports = response.data ? response.data : [];
      await dispatch(actions.modifyReportAndFormSuccess(reports));
      await dispatch(actions.resetAllFormsAndReports());
      return reports;
    } catch (err) {
      await dispatch(actions.modifyReportAndFormFail());
      return err;
    }
  };


  deleteState = (
    societyId, bill_id, id_param_account_worksheet, pageName
  ) => async (dispatch, getState) => {
    const state = getState();
    const society_id = societyId || _.get(state, 'navigation.id', false);
    const tableName = getTableName(society_id, pageName);
    const selectedRows = _.get(state, `tables.${tableName}.selectedRows`, {});
    const createdRows = _.get(state, `tables.${tableName}.createdRows`, {});

    const newStates = Object.keys(selectedRows).reduce((acc, key) => {
      if (!createdRows[key] && selectedRows[key]) {
        acc.push({ id_state: parseInt(key, 10) });
      }
      return acc;
    }, []);

    try {
      await dispatch(actions.deleteStateAttempt(society_id, bill_id, pageName));
      // put endpoint when it will be done
      // const statements = response.data ? response.data : [];
      const response = await windev.makeApiCall('/FAKE_DELETE_ENDPOINT', 'delete', {}, { newStates });
      await dispatch(actions.deleteStateSuccess(society_id, bill_id, pageName));
      await dispatch(tableActions.deleteRows(tableName));
      await dispatch(tableActions.save(tableName));
      return response;
    } catch (err) {
      await dispatch(actions.deleteStateFail(society_id, bill_id, pageName));
      return err;
    }
  };

  getStates = (
    societyId,
    id_param_account_worksheet,
    opts = {},
    pageName
  ) => async (dispatch, getState) => {
    const {
      limit: limitOpts,
      page: pageOpts
    } = opts;
    const state = getState();
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, pageName);
    const limit = limitOpts !== undefined
      ? limitOpts
      : _.get(getState(), `reportsAndForms.statesList[${society_id}][${pageName}].limit`, 10);
    const page = pageOpts !== undefined
      ? pageOpts
      : _.get(getState(), `reportsAndForms.statesList[${society_id}][${pageName}].page`, 0);
    const offset = page !== undefined
      ? page * limit
      : _.get(getState(), `reportsAndForms.statesList[${society_id}][${pageName}].entry_array.length`, 0);
    const actionOpts = { limit, page };
    try {
      await dispatch(actions.getStatesAttempt(society_id, actionOpts, pageName));
      // put endpoint when it will be done
      const response = await windev.makeApiCall('/FAKE_GET_API', 'get', { limit, offset });
      const { data } = response;
      // const statements = response.data ? response.data : [];
      await dispatch(actions.getStatesSuccess(society_id, actionOpts, pageName));
      await dispatch(
        tableActions.setData(tableName, data.states.map(s => ({ id: s.state_id, ...s })))
      );
      return response;
    } catch (err) {
      await dispatch(actions.getStatesFail(society_id, actionOpts, pageName));
      return err;
    }
  };

  editOrCreateState = (
    societyId, id_param_account_worksheet, pageName
  ) => async (dispatch, getState) => {
    const state = getState();
    const society_id = societyId || _.get(state, 'navigation.id', false);
    const tableName = getTableName(society_id, pageName);
    const createdRows = _.get(state, `tables.${tableName}.createdRows`, {});
    const editedRows = _.get(state, `tables.${tableName}.editedRows`, {});
    const createdStates = Object.values(createdRows).map(state => ({
      ...state,
      // change data here
      id: undefined
    }));
    const updatedStates = Object.values(editedRows).map(state => ({
      ...state
    }));
    try {
      let response;
      await dispatch(actions.editOrCreateStateAttempt(society_id, pageName));
      if (updatedStates.length > 0) {
        response = await windev.makeApiCall('/FAKE_PUT_API', 'put', {}, { states: updatedStates });
      }
      if (createdStates.length > 0) {
        response = await windev.makeApiCall('/FAKE_PUT_API', 'post', {}, { states: createdStates });
      }
      await dispatch(actions.editOrCreateStateSuccess(society_id, pageName));
      await dispatch(tableActions.save(tableName));
      return response;
    } catch (err) {
      await dispatch(actions.editOrCreateStateFail(society_id, pageName));
      return err;
    }
  };

  deleteReportAndForm = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, REPORT_AND_FORMS_TABLE);
    const reportsAndForms_list = _.get(state, `tables.${tableName}.data`, []);
    const lastSelectedRow = _.get(state, `tables.${tableName}.lastSelectedRow`, null);
    const params = {
      id: reportsAndForms_list[lastSelectedRow].id
    };
    try {
      await dispatch(actions.deleteFARAttempt(society_id));
      const response = await windev.makeApiCall('/FAKE_API', 'delete', {}, params);
      const numbers = response.data ? response.data : [];
      await dispatch(actions.deleteFARSuccess(society_id));
      await dispatch(this.getFormsAndReports());
      return numbers;
    } catch (err) {
      await dispatch(actions.deleteFARFail(society_id));
      return err;
    }
  };
}


export default ReportsAndFormsService.getInstance();
