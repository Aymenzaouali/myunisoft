import {
  GET_FORMS_AND_REPORTS_ATTEMPT,
  GET_FORMS_AND_REPORTS_SUCCESS,
  GET_FORMS_AND_REPORTS_FAIL,
  GET_FORMS_AND_REPORTS_ADDITIONAL_ATTEMPT,
  GET_FORMS_AND_REPORTS_ADDITIONAL_SUCCESS,
  GET_FORMS_AND_REPORTS_ADDITIONAL_FAIL,
  DELETE_FAR_ATTEMPT,
  DELETE_FAR_SUCCESS,
  DELETE_FAR_FAIL,
  MODIFY_REPORT_AND_FORM_ATTEMPT,
  MODIFY_REPORT_AND_FORM_SUCCESS,
  MODIFY_REPORT_AND_FORM_FAIL,
  SEND_REPORT_AND_FORM_ATTEMPT,
  SEND_REPORT_AND_FORM_SUCCESS,
  SEND_REPORT_AND_FORM_FAIL,
  GET_STATE_AND_FORM_DIALOG_ATTEMPT,
  GET_STATE_AND_FORM_DIALOG_SUCCESS,
  GET_STATE_AND_FORM_DIALOG_FAIL,
  SET_SELECTED_STATE_OR_FORM,
  RESET_SELECTED_STATE_OR_FORM,
  EDIT_OR_CREATE_ATTEMPT,
  EDIT_OR_CREATE_FAIL,
  EDIT_OR_CREATE_SUCCESS,
  GET_STATES_DATA_ATTEMPT,
  GET_STATES_DATA_SUCCESS,
  GET_STATES_DATA_FAIL,
  DELETE_STATES_ATTEMPT,
  DELETE_STATES_SUCCESS,
  DELETE_STATES_FAIL,
  SET_ADDITIONAL_REPORT_AND_FORM
} from './constants';

const getFormsAndReportsAdditionalAttempt = society_id => ({
  type: GET_FORMS_AND_REPORTS_ADDITIONAL_ATTEMPT,
  society_id
});

const getFormsAndReportsAdditionalSuccess = (formAndReportsAdditional, society_id) => ({
  type: GET_FORMS_AND_REPORTS_ADDITIONAL_SUCCESS,
  formAndReportsAdditional,
  society_id
});

const getFormsAndReportsAdditionalFail = society_id => ({
  type: GET_FORMS_AND_REPORTS_ADDITIONAL_FAIL,
  society_id
});

const getFormsAndReportsAttempt = society_id => ({
  type: GET_FORMS_AND_REPORTS_ATTEMPT,
  society_id
});

const getFormsAndReportsSuccess = (formAndReports, society_id) => ({
  type: GET_FORMS_AND_REPORTS_SUCCESS,
  formAndReports,
  society_id
});

const getFormsAndReportsFail = society_id => ({
  type: GET_FORMS_AND_REPORTS_FAIL,
  society_id
});

const modifyReportAndFormAttempt = () => ({
  type: MODIFY_REPORT_AND_FORM_ATTEMPT
});

const modifyReportAndFormSuccess = wallets => ({
  type: MODIFY_REPORT_AND_FORM_SUCCESS,
  wallets
});

const modifyReportAndFormFail = () => ({
  type: MODIFY_REPORT_AND_FORM_FAIL
});

const sendReportAndFormSuccess = reports => ({
  type: SEND_REPORT_AND_FORM_SUCCESS,
  reports
});

const sendReportAndFormAttempt = () => ({
  type: SEND_REPORT_AND_FORM_ATTEMPT
});

const sendReportAndFormFail = () => ({
  type: SEND_REPORT_AND_FORM_FAIL
});

const getStateOrFormDialogTableAttempt = society_id => ({
  type: GET_STATE_AND_FORM_DIALOG_ATTEMPT,
  society_id
});

const getStateOrFormDialogTableSuccess = (stateAndFormsForDialog, society_id) => ({
  type: GET_STATE_AND_FORM_DIALOG_SUCCESS,
  stateAndFormsForDialog,
  society_id
});

const getStateOrFormDialogTableFail = society_id => ({
  type: GET_STATE_AND_FORM_DIALOG_FAIL,
  society_id
});

const setSelectedStateOrFormData = (society_id, selected, selectedStateOrFormId) => ({
  type: SET_SELECTED_STATE_OR_FORM,
  society_id,
  selected,
  selectedStateOrFormId
});
const resetSelectedStateOrFormData = society_id => ({
  type: RESET_SELECTED_STATE_OR_FORM,
  society_id
});

const editOrCreateStateAttempt = (societyId, pageName) => ({
  type: EDIT_OR_CREATE_SUCCESS,
  societyId,
  pageName
});

const editOrCreateStateSuccess = (societyId, pageName) => ({
  type: EDIT_OR_CREATE_ATTEMPT,
  societyId,
  pageName
});

const editOrCreateStateFail = (societyId, pageName) => ({
  type: EDIT_OR_CREATE_FAIL,
  societyId,
  pageName
});

const getStatesAttempt = (societyId, opts, pageName) => ({
  type: GET_STATES_DATA_ATTEMPT,
  societyId,
  opts,
  pageName
});

const getStatesSuccess = (data, societyId, opts, pageName) => ({
  type: GET_STATES_DATA_SUCCESS,
  pageName,
  data,
  opts,
  societyId
});

const getStatesFail = (societyId, pageName) => ({
  type: GET_STATES_DATA_FAIL,
  societyId,
  pageName
});

const deleteStateAttempt = (societyId, states, pageName) => ({
  type: DELETE_STATES_ATTEMPT,
  societyId,
  states,
  pageName
});

const deleteStateSuccess = (societyId, states, pageName) => ({
  type: DELETE_STATES_SUCCESS,
  societyId,
  states,
  pageName
});

const deleteStateFail = (societyId, states, pageName) => ({
  type: DELETE_STATES_FAIL,
  societyId,
  states,
  pageName
});

const setAdditionalReportsAndForms = status => ({
  type: SET_ADDITIONAL_REPORT_AND_FORM,
  status
});

const deleteFARAttempt = society_id => ({
  type: DELETE_FAR_ATTEMPT,
  society_id
});

const deleteFARSuccess = (formAndReports, society_id) => ({
  type: DELETE_FAR_SUCCESS,
  formAndReports,
  society_id
});

const deleteFARFail = society_id => ({
  type: DELETE_FAR_FAIL,
  society_id
});

export default {
  getFormsAndReportsAttempt,
  getFormsAndReportsSuccess,
  getFormsAndReportsFail,
  getFormsAndReportsAdditionalAttempt,
  getFormsAndReportsAdditionalSuccess,
  getFormsAndReportsAdditionalFail,
  setAdditionalReportsAndForms,
  deleteFARAttempt,
  deleteFARSuccess,
  deleteFARFail,
  sendReportAndFormAttempt,
  sendReportAndFormSuccess,
  sendReportAndFormFail,
  modifyReportAndFormAttempt,
  modifyReportAndFormSuccess,
  modifyReportAndFormFail,
  getStateOrFormDialogTableAttempt,
  getStateOrFormDialogTableSuccess,
  getStateOrFormDialogTableFail,
  setSelectedStateOrFormData,
  resetSelectedStateOrFormData,
  editOrCreateStateAttempt,
  editOrCreateStateSuccess,
  editOrCreateStateFail,
  getStatesAttempt,
  getStatesSuccess,
  getStatesFail,
  deleteStateAttempt,
  deleteStateSuccess,
  deleteStateFail
};
