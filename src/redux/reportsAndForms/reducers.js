import _ from 'lodash';
import {
  GET_FORMS_AND_REPORTS_ATTEMPT,
  GET_FORMS_AND_REPORTS_SUCCESS,
  GET_FORMS_AND_REPORTS_FAIL,
  GET_FORMS_AND_REPORTS_ADDITIONAL_ATTEMPT,
  GET_FORMS_AND_REPORTS_ADDITIONAL_SUCCESS,
  GET_STATE_AND_FORM_DIALOG_ATTEMPT,
  GET_STATE_AND_FORM_DIALOG_SUCCESS,
  GET_STATE_AND_FORM_DIALOG_FAIL,
  SET_SELECTED_STATE_OR_FORM,
  RESET_SELECTED_STATE_OR_FORM,
  GET_STATES_DATA_ATTEMPT,
  GET_STATES_DATA_SUCCESS,
  GET_STATES_DATA_FAIL,
  DELETE_STATES_ATTEMPT,
  DELETE_STATES_SUCCESS,
  GET_FORMS_AND_REPORTS_ADDITIONAL_FAIL,
  SET_ADDITIONAL_REPORT_AND_FORM,
  DELETE_STATES_FAIL
} from './constants';

const initialState = {
  settings_id: null,
  formAndReports: {},
  selectedformAndReports: {},
  statementSelectedLineAdditional: {},
  stateOrForm: [{ label: 'Formulaire TVA 356C', value: 1 }, { label: 'Formulaire TVA 856AA', value: 2 }],
  formAndReportsSelectedLine: {},
  formAndReportsAdditional: {},
  isAddAllFormsAndReportsAddView: false,
  stateAndFormsForDialog: {},
  selectedStateOrForm: {}
};

const reportsAndForms = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_FORMS_AND_REPORTS_ATTEMPT:
      return {
        ...state,
        formAndReports: {
          [action.society_id]: {
            isLoading: true,
            isError: false,
            formAndReports_list: []
          }
        }
      };
    case GET_FORMS_AND_REPORTS_SUCCESS:
      return {
        ...state,
        formAndReports: {
          [action.society_id]: {
            ...state.formAndReports[action.society_id],
            isLoading: false,
            isError: false,
            formAndReports_list: action.formAndReports
          }
        }
      };
    case GET_FORMS_AND_REPORTS_FAIL:
      return {
        ...state,
        formAndReports: {
          [action.society_id]: {
            ...state.formAndReports[action.society_id],
            isLoading: false,
            isError: true
          }
        }
      };
    case GET_STATE_AND_FORM_DIALOG_ATTEMPT:
      return {
        ...state,
        stateAndFormsForDialog: {
          [action.society_id]: {
            isLoading: true,
            isError: false,
            stateAndFormsForDialog_list: []
          }
        }
      };
    case GET_STATE_AND_FORM_DIALOG_SUCCESS:
      return {
        ...state,
        stateAndFormsForDialog: {
          [action.society_id]: {
            ...state.stateAndFormsForDialog[action.society_id],
            isLoading: false,
            isError: false,
            stateAndFormsForDialog_list: action.stateAndFormsForDialog
          }
        }
      };
    case GET_STATE_AND_FORM_DIALOG_FAIL:
      return {
        ...state,
        stateAndFormsForDialog: {
          [action.society_id]: {
            ...state.stateAndFormsForDialog[action.society_id],
            isLoading: false,
            isError: true
          }
        }
      };
    case GET_FORMS_AND_REPORTS_ADDITIONAL_ATTEMPT:
      return {
        ...state,
        formAndReportsAdditional: {
          [action.society_id]: {
            isLoading: true,
            isError: false,
            formAndReportsAdditional_list: []
          }
        }
      };
    case GET_FORMS_AND_REPORTS_ADDITIONAL_SUCCESS:
      return {
        ...state,
        formAndReportsAdditional: {
          [action.society_id]: {
            ...state.formAndReportsAdditional[action.society_id],
            isLoading: false,
            isError: false,
            formAndReportsAdditional_list: action.formAndReportsAdditional
          }
        }
      };
    case GET_FORMS_AND_REPORTS_ADDITIONAL_FAIL:
      return {
        ...state,
        formAndReportsAdditional: {
          [action.society_id]: {
            ...state.formAndReportsAdditional[action.society_id],
            isLoading: false,
            isError: true
          }
        }
      };
    case SET_SELECTED_STATE_OR_FORM:
      return {
        ...state,
        selectedStateOrForm: {
          [action.society_id]: {
            selectedStateOrForm_list: {
              ..._.get(state, `selectedStateOrForm[${action.society_id}].selectedStateOrForm_list`, {}),
              [action.selectedStateOrFormId]: {
                name: state.stateOrForm
                  .filter(state => state.value === action.selectedStateOrFormId)[0],
                data: action.selected
              }
            }
          }
        }
      };
    case RESET_SELECTED_STATE_OR_FORM:
      return {
        ...state,
        selectedStateOrForm: {
          [action.society_id]: {
            selectedStateOrForm_list: []
          }
        }
      };
    case GET_STATES_DATA_ATTEMPT:
      return {
        ...state,
        statesList: {
          ...state.statesList,
          [action.societyId]: {
            [action.pageName]: {
              ..._.get(state, `statesList[${action.societyId}][${action.pageName}]`),
              isLoading: false,
              isError: true
            }
          }
        }
      };
    case GET_STATES_DATA_SUCCESS:
      return {
        ...state,
        statesList: {
          ...state.statesList,
          [action.societyId]: {
            [action.pageName]: {
              ..._.get(state, `statesList[${action.societyId}][${action.pageName}]`),
              isLoading: true,
              isError: false,
              ...action.opts
            }
          }
        }
      };
    case GET_STATES_DATA_FAIL:
      return {
        ...state,
        statesList: {
          ...state.statesList,
          [action.societyId]: {
            [action.pageName]: {
              ...action.data,
              isLoading: false,
              isError: false,
              ...action.opts
            }
          }
        }
      };
    case DELETE_STATES_ATTEMPT:
      return {
        ...state,
        statesList: {
          ...state.statesList,
          [action.societyId]: {
            [action.pageName]: {
              ..._.get(state, `statesList[${action.societyId}][${action.pageName}]`, {}),
              ...action.statesList,
              isLoading: false,
              isError: false
            }
          }
        },
        selectedState: {}
      };
    case DELETE_STATES_SUCCESS:
      return {
        ...state,
        statesList: {
          ...state.statesList,
          [action.societyId]: {
            [action.pageName]: {
              ..._.get(state, `statesList[${action.societyId}][${action.pageName}]`, {}),
              ...action.statesList,
              isLoading: false,
              isError: true
            }
          }
        }
      };
    case DELETE_STATES_FAIL:
      return {
        ...state,
        statesList: {
          ...state.statesList,
          [action.societyId]: {
            [action.pageName]: {
              ..._.get(state, `statesList[${action.societyId}][${action.pageName}]`, {}),
              isLoading: true,
              isError: false
            }
          }
        }
      };
    case SET_ADDITIONAL_REPORT_AND_FORM:
      return {
        ...state,
        isAddAllFormsAndReportsAddView: action.status
      };
    default: return state;
    }
  }
  return state;
};

export default reportsAndForms;
