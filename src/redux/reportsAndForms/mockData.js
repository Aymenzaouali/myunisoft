const mainTable = [
  {
    id: 0,
    type: 'Formulaire',
    name: 'Exemple de nom',
    decription: 'Ceci est un texte descriptif de document ',
    dateStart: '2014-09-08T08:02:17-05:00',
    dateEnd: '2010-09-08T08:02:17-05:00',
    date: '01/012019 au 31/12/2019',
    enterpriseName: 'Toutes',
    enterprise_list: [
      { name: 'test', id: 4 },
      { name: 'FWANCKY_SOCIETE', id: 11 }
    ]
  },
  {
    id: 1,
    type: 'État Myunisoft',
    name: 'Exemple de nom',
    decription: 'Ceci est un texte descriptif de document ',
    dateStart: '2014-09-08T08:02:17-05:00',
    dateEnd: '2010-09-08T08:02:17-05:00',
    date: '01/012019 au 31/12/2019',
    enterpriseName: 'Taxes',
    enterprise_list: null
  }
];

const sateOrFormSelectedTable1 = [
  {
    id: 0,
    orderNumber: '001',
    codeEDI: 'UNH',
    wording: '1 Exemple de libellé',
    dataType: 'Label',
    viewing: 'Normal',
    dataSource: '1 Compta ana toutes sections',
    references: '60'
  },
  {
    id: 1,
    orderNumber: '002',
    codeEDI: 'UNH',
    wording: '2 Exemple de libellé',
    dataType: 'Label',
    viewing: 'Normal',
    dataSource: '2 Compta ana toutes sections',
    references: '80'
  }
];

const sateOrFormSelectedTable2 = [
  {
    id: 0,
    orderNumber: '003',
    codeEDI: 'UNH',
    wording: '3 Exemple de libellé',
    dataType: 'Label',
    viewing: 'Normal',
    dataSource: '3 Compta ana toutes sections',
    references: '60'
  }
];

export {
  mainTable,
  sateOrFormSelectedTable1,
  sateOrFormSelectedTable2
};
