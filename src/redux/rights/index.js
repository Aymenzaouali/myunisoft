import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';
import actions from './actions';


export const getRights = () => async (dispatch) => {
  try {
    await dispatch(actions.getRightsAttempt());
    const { data } = await windev.makeApiCall('/rights', 'get', {});
    await dispatch(actions.getRightsSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getRightsFail());
    await dispatch(handleError(err));
    return err;
  }
};
