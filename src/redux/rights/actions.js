import {
  GET_RIGHTS_SUCCESS,
  GET_RIGHTS_ATTEMPT,
  GET_RIGHTS_FAIL

} from './constants';

const getRightsFail = () => ({
  type: GET_RIGHTS_FAIL
});
const getRightsAttempt = () => ({
  type: GET_RIGHTS_ATTEMPT
});
const getRightsSuccess = payload => ({
  type: GET_RIGHTS_SUCCESS,
  payload
});


export default {
  getRightsFail,
  getRightsAttempt,
  getRightsSuccess
};
