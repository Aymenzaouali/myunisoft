export const GET_RIGHTS_FAIL = 'rights/GET_RIGHTS_FAIL';
export const GET_RIGHTS_ATTEMPT = 'rights/GET_RIGHTS_ATTEMPT';
export const GET_RIGHTS_SUCCESS = 'rights/GET_RIGHTS_SUCCESS';
