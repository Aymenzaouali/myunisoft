import {
  GET_RIGHTS_SUCCESS,
  GET_RIGHTS_ATTEMPT,
  GET_RIGHTS_FAIL
} from './constants';

const initialState = {
  rights: [],
  isLoading: false,
  isError: false
};

const rights = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_RIGHTS_FAIL: {
      return {
        ...state,
        rights: [],
        isLoading: false,
        isError: true
      };
    }
    case GET_RIGHTS_ATTEMPT: {
      return {
        ...state,
        isLoading: true,
        isError: false
      };
    }
    case GET_RIGHTS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        rights: action.payload
      };
    }
    default:
      return state;
    }
  }
  return state;
};

export default rights;
