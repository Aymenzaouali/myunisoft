import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
// import actions from 'common/redux/error/actions';
import _ from 'lodash';
import { printOrDownload } from 'helpers/file';
import { checkAndFormatDate } from 'helpers/date';
import {
  getBalanceAction,
  getBalanceAttempt,
  getBalanceFail,

  submitPrintOrExportAttempt,
  submitPrintOrExportFail,

  getLedgerAttempt,
  getLedgerFail,
  getLedgerSuccess,

  getSigAttempt,
  getSigFail,
  getSigSuccess,

  submitPrintOrExportSigAttempt,
  submitPrintOrExportSigFail,

  getSheetOfDocsSuccess
} from './actions';

export const getSigData = (start_date, end_date, mode) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');

  try {
    await dispatch(getSigAttempt());
    const { data } = await windev.makeApiCall('/SIG', 'get', {
      society_id,
      mode: 'JSON',
      start_date,
      end_date,
      cumul: mode || 0
    });
    await dispatch(getSigSuccess(data));
    return data;
  } catch (err) {
    await dispatch(getSigFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getBalanceData = (
  filter,
  start,
  end,
  mode,
  id_exercice
) => async (dispatch, getState) => {
  const start_date = checkAndFormatDate(start);
  const end_date = checkAndFormatDate(end);
  try {
    await dispatch(getBalanceAttempt());
    const society_id = _.get(getState(), 'navigation.id', -2);
    const { data } = await windev.makeApiCall('/balance_dynamique', 'get', { // TO DO: Replace this request with /balance
      society_id,
      cumul: mode || 0,
      filter,
      start_date,
      end_date,
      fiscal_year_id: id_exercice
    });
    // await dispatch(actions.emptyError()); // Waiting for Common to be modified
    await dispatch(getBalanceAction(data));
    return data;
  } catch (err) {
    if (_.get(err, 'response.data.code') === 'NoData') {
      const data = {
        data: [],
        month_list: [],
        results: {
          exercice_values: [],
          month_values: []
        }
      };

      await dispatch(getBalanceAction(data));
      return data;
    }
    await dispatch(getBalanceFail());
    await dispatch(handleError(err));
    return err;
  }
};

/**
 *
 * @param {string} type - Type of dialog, currently export | print
 * @param {object} params - params for the chosen type
 * @param {string} params.start_date - Date start for balance export
 * @param {string} params.end_date - Date end for balance export
 * @param {string} params.id_watermark - watermark type
 * @param {bool} params.lettered_only - Active or not lettering
 */

export const submitPrintOrExportBalance = (type, params = {}) => async (dispatch, getState) => {
  try {
    await dispatch(submitPrintOrExportAttempt());

    const society_id = _.get(getState(), 'navigation.id', -2);
    const {
      data,
      headers
    } = await windev.makeApiCall(
      '/balance',
      'get',
      {
        society_id,
        ...params
      },
      {},
      { responseType: 'blob' }
    );
    const filename = _.split(_.get(headers, 'content-disposition'), '"')[1];
    printOrDownload(data, type, `${filename}`);

    return data;
  } catch (err) {
    await dispatch(submitPrintOrExportFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const submitPrintorExportSig = (type, params = {}) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', -2);
  try {
    await dispatch(submitPrintOrExportSigAttempt());
    const { data, headers } = await windev.makeApiCall('/SIG', 'get', { society_id, ...params }, {}, { responseType: 'blob' });
    const filename = _.split(_.get(headers, 'content-disposition'), '"')[1];
    printOrDownload(data, type, `${filename}`);
  } catch (err) {
    await dispatch(submitPrintOrExportSigFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const getLedger = (type, params = {}) => async (dispatch) => {
  try {
    await dispatch(getLedgerAttempt());
    const { data, headers } = await windev.makeApiCall('/grand_livre', 'get', params, {}, { responseType: 'blob' });
    const filename = _.split(_.get(headers, 'content-disposition'), '"')[1];

    printOrDownload(data, type, `Grand_Livre_${filename}`);
    await dispatch(getLedgerSuccess());
    return data;
  } catch (err) {
    await dispatch(getLedgerFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getSheetOfDocs = async (dispatch) => {
  try {
    const { data } = await windev.makeApiCall('/sheet', 'get');
    await dispatch(getSheetOfDocsSuccess(data));
  } catch (err) {
    await dispatch(handleError(err));
    throw err;
  }
};
