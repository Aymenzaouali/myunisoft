import {
  GET_BALANCE_ATTEMPT,
  GET_BALANCE,
  GET_BALANCE_FAIL,
  SELECT_CURRENT_OPTION,

  GET_LEDGER_ATTEMPT,
  GET_LEDGER_FAIL,
  GET_LEDGER_SUCCESS,

  GET_SIG_ATTEMPT,
  GET_SIG_FAIL,
  GET_SIG_SUCCESS,

  GET_SHEET_OF_DOCS_SUCCESS
} from './constants';

const initialState = {
  data: [],
  month_list: [],
  results: {
    exercice_values: [],
    month_values: []
  },
  currentOption: 'balance',
  isLoading: false,
  isError: false,
  sig: {
    data: [],
    month_list: [],
    results: {
      exercice_values: [],
      month_values: []
    },
    currentOption: 'balance',
    isLoading: false,
    isError: false
  },
  sheet: null
};

const balance = (state = initialState, action) => {
  switch (action.type) {
  case GET_SIG_ATTEMPT: {
    return {
      ...state,
      sig: {
        isLoading: true,
        isError: false
      }
    };
  }
  case GET_SIG_FAIL: {
    return {
      ...state,
      sig: {
        data: [],
        month_list: [],
        results: {
          exercice_values: [],
          month_values: []
        },
        isLoading: false,
        isError: true
      }
    };
  }
  case GET_SIG_SUCCESS: {
    return {
      ...state,
      sig: {
        ...action.payload,
        isLoading: false,
        isError: false
      }
    };
  }
  case GET_BALANCE_ATTEMPT: {
    return {
      ...state,
      isLoading: true,
      isError: false
    };
  }
  case GET_BALANCE: {
    return {
      ...state,
      ...action.payload,
      isLoading: false,
      isError: false
    };
  }
  case GET_BALANCE_FAIL: {
    return {
      ...state,
      data: [],
      month_list: [],
      results: {
        exercice_values: [],
        month_values: []
      },
      isLoading: false,
      isError: true
    };
  }
  case SELECT_CURRENT_OPTION: {
    return {
      ...state,
      currentOption: action.option
    };
  }
  case GET_LEDGER_ATTEMPT: {
    return {
      ...state,
      isLoading: true
    };
  }
  case GET_LEDGER_SUCCESS: {
    return {
      ...state,
      isLoading: false
    };
  }
  case GET_LEDGER_FAIL: {
    return {
      ...state,
      isLoading: false
    };
  }
  case GET_SHEET_OF_DOCS_SUCCESS: {
    return {
      ...state,
      sheet: action.data
    };
  }

  default:
    return state;
  }
};

export default balance;
