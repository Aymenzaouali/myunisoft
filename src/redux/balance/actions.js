import {
  GET_BALANCE,
  GET_BALANCE_ATTEMPT,
  GET_BALANCE_FAIL,

  GET_LEDGER_ATTEMPT,
  GET_LEDGER_FAIL,
  GET_LEDGER_SUCCESS,

  SELECT_CURRENT_OPTION,

  SUBMIT_PRINT_EXPORT_ATTEMPT,
  SUBMIT_PRINT_EXPORT_FAIL,

  GET_SIG_ATTEMPT,
  GET_SIG_FAIL,
  GET_SIG_SUCCESS,

  SUBMIT_PRINT_EXPORT_SIG_ATTEMPT,
  SUBMIT_PRINT_EXPORT_SIG_FAIL,

  GET_SHEET_OF_DOCS_SUCCESS
} from './constants';

export const getBalanceAction = payload => ({
  type: GET_BALANCE,
  payload
});
export const getBalanceAttempt = () => ({
  type: GET_BALANCE_ATTEMPT
});
export const getBalanceFail = () => ({
  type: GET_BALANCE_FAIL
});

export const submitPrintOrExportAttempt = () => ({
  type: SUBMIT_PRINT_EXPORT_ATTEMPT
});
export const submitPrintOrExportFail = () => ({
  type: SUBMIT_PRINT_EXPORT_FAIL
});

export const getLedgerAttempt = () => ({
  type: GET_LEDGER_ATTEMPT
});

export const getLedgerFail = () => ({
  type: GET_LEDGER_FAIL
});

export const getLedgerSuccess = () => ({
  type: GET_LEDGER_SUCCESS
});

export const selectCurrentOption = option => ({
  type: SELECT_CURRENT_OPTION,
  option
});

export const getSigAttempt = () => ({
  type: GET_SIG_ATTEMPT
});

export const getSigFail = () => ({
  type: GET_SIG_FAIL
});

export const getSigSuccess = payload => ({
  type: GET_SIG_SUCCESS,
  payload
});

export const submitPrintOrExportSigAttempt = () => ({
  type: SUBMIT_PRINT_EXPORT_SIG_ATTEMPT
});

export const submitPrintOrExportSigFail = () => ({
  type: SUBMIT_PRINT_EXPORT_SIG_FAIL
});

export const getSheetOfDocsSuccess = data => ({
  type: GET_SHEET_OF_DOCS_SUCCESS,
  data
});
