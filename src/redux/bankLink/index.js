import { handleError } from 'common/redux/error';
import _ from 'lodash';
import { windev } from 'helpers/api';
import { getFormValues } from 'redux-form';
import { printOrDownload } from 'helpers/file';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import actions from './actions';

export const setFormValues = formValues => async (dispatch, getState) => {
  const state = getState();

  await dispatch(autoFillFormValues(`${state.navigation.id}banklinkFilter`, formValues));
};

export const getDate = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getDateAttempt(society_id));
    const { data } = await windev.makeApiCall('/bank_reconciliation/date', 'get', {});
    await dispatch(actions.getDateSuccess(data.date, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.getDateFail(society_id));
    return err;
  }
};

export const getPJbankLink = id_diligence => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getBankLinkDocumentsAttempt(society_id));
    const { data } = await windev.makeApiCall('/dadp/work_program/document', 'get', {
      id_diligence
    });
    await dispatch(actions.getBankLinkDocumentsSuccess(data, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.getBankLinkDocumentsFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getBankLink = (
  dateCall,
  id_diaryCall,
  dted,
  sort
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  const tmpDate = _.get(state, `form.${society_id}banklinkFilter.values`, '');
  // const { bank_balance } = tmpDate;
  const date = dateCall || `${tmpDate.year}${tmpDate.month}${tmpDate.day}`;
  const bankFormValues = getFormValues(`${society_id}banklinkFilter`)(state);
  const id_diary = id_diaryCall || _.get(bankFormValues, 'bank.value.diary_id', '');
  const dotted = dted || _.get(bankFormValues, 'dotted', 0);
  const limit = 1000000;
  const offset = 0;

  try {
    const actionOpts = { limit: 1000000, page: 0, sort };
    await dispatch(actions.getBankLinkDataAttempt(actionOpts, society_id));
    const params = {
      date,
      mode: 2,
      dotted,
      id_diary,
      limit,
      offset,
      sort
    };

    const { data } = await windev.makeApiCall('/bank_reconciliation/list_entries', 'get', params, {});

    const { diligence } = data;

    const dottedData = {
      ...data,
      undotted_lines: data.line_writing.reduce(
        (acc, b) => (!b.dotting_aa
          ? [...acc, b.id_line_entries] : acc), []
      ),
      dotted_lines: data.line_writing.reduce(
        (acc, b) => (b.dotting_aa
          ? [...acc, b.id_line_entries] : acc), []
      )
    };
    await dispatch(getPJbankLink(diligence.id));
    await dispatch(actions.getBankLinkDataSuccess(dottedData, actionOpts, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.getBankLinkDataFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const postValidate = (date, balance, id_diary) => async (dispatch, getState) => {
  const state = getState();
  const id_diligence = _.get(state, `bankLink.bankLink[${state.navigation.id}].diligence.id`);
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.postValidateAttempt());
    const response = await windev.makeApiCall('/bank_reconciliation/validate', 'post', {
      date,
      balance,
      id_diary,
      id_diligence
    }, {});
    const { data } = response;
    const diligence = {
      id: id_diligence,
      closed: data.status === 'success'
    };
    await dispatch(actions.postValidateSuccess(diligence, society_id));
    return response;
  } catch (err) {
    await dispatch(actions.postValidateFail());
    return err;
  }
};

export const getDiaries = societyId => async (dispatch, getState) => {
  const state = getState();
  const society_id = societyId || _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getDiariesAttempt());
    const response = await windev.makeApiCall('/diary', 'get', { society_id, limit: 20, type: 'BQ' }, {});
    const { data } = response;
    await dispatch(actions.getDiariesSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getDiariesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const printBankLink = (
  societyId, date, id_diary, bank_balance, dotted
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = societyId || _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getBankLinkBinaryAttempt(society_id));
    const { data } = await windev.makeApiCall('/bank_reconciliation', 'get', {
      id_society: society_id,
      date,
      id_diary,
      mode: 2,
      balance: bank_balance,
      dotted
    }, {}, { responseType: 'blob' });
    await dispatch(actions.getBankLinkBinarySuccess(data, society_id));
    printOrDownload(data, 'print', `${date}.pdf`);
    return data;
  } catch (err) {
    await dispatch(actions.getBankLinkBinaryFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const dotOrUndot = (
  societe_id, line_id, aa, mm, diary, balance_date, balance, month, year
) => async (dispatch, getState) => {
  try {
    if (aa && mm) {
      await dispatch(actions.pointLine(societe_id, line_id, aa, mm));
    } else {
      await dispatch(actions.unpointLine(societe_id, line_id));
    }
    const state = getState();
    const pointed_line_entries = _.get(state, `bankLink.bankLink[${state.navigation.id}].dotted_lines`, []);
    const unpointed_line_entries = _.get(state, `bankLink.bankLink[${state.navigation.id}].undotted_lines`, []);
    const {
      diary_id
    } = diary;

    const params = {
      id_diary: diary_id,
      balance_date,
      balance,
      month,
      year: year.slice(2),
      pointed_line_entries,
      unpointed_line_entries
    };
    await dispatch(actions.putBankLinkAttempt());
    await dispatch(actions.resetAllBills(societe_id));
    const { data } = await windev.makeApiCall('/bank_reconciliation', 'put', {}, params);
    await dispatch(actions.putBankLinkSuccess());
    await dispatch(getBankLink(balance_date, diary_id));
    return data;
  } catch (err) {
    await dispatch(actions.putBankLinkFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const addPjBankLink = (files, date, id_diligence) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');
  const queries = files.map((file) => {
    const params = {
      file_name: file.name.split('.').shift(),
      date,
      extension: `.${file.name.split('.').pop()}`
    };
    return windev.makeApiCall('/bank_reconciliation/document', 'post', params, file,
      { headers: { 'Content-Type': 'application/octet-stream', 'society-id': society_id } });
  });
  try {
    await Promise.all(queries);
    await dispatch(actions.addPjBankLinkSuccess());
    await dispatch(getPJbankLink(id_diligence));
  } catch (err) {
    await dispatch(actions.addPjBankLinkFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const putBankLink = (
  id_diary, balance_date, balance, month, year
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  const pointed_line_entries = _.get(state, `bankLink.bankLink[${state.navigation.id}].dotted_lines`, []);
  const unpointed_line_entries = _.get(state, `bankLink.bankLink[${state.navigation.id}].undotted_lines`, []);
  try {
    const params = {
      id_diary,
      balance_date,
      balance,
      month,
      year,
      pointed_line_entries,
      unpointed_line_entries
    };

    await dispatch(actions.putBankLinkAttempt());
    const { data } = await windev.makeApiCall('/bank_reconciliation', 'put', {}, params);
    await dispatch(actions.putBankLinkSuccess());
    await dispatch(getBankLink(society_id, balance_date, id_diary, 0));
    return data;
  } catch (err) {
    await dispatch(actions.putBankLinkFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const updateBankBalance = (
  id_diary, balance_date, balance, month, year
) => async (dispatch) => {
  try {
    const params = {
      id_diary,
      balance_date,
      balance,
      month,
      year,
      pointed_line_entries: [],
      unpointed_line_entries: []
    };

    await dispatch(actions.updateBankBalanceAttempt());
    const { data } = await windev.makeApiCall('/bank_reconciliation', 'put', {}, params);
    await dispatch(actions.updateBankBalanceSuccess());
    return data;
  } catch (err) {
    await dispatch(actions.updateBankBalanceFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getComments = diligence_id => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');

  try {
    await dispatch(actions.getCommentsAttempt(society_id));
    const { data } = await windev.makeApiCall(
      '/comments',
      'get',
      {
        diligence_id
      }
    );
    await dispatch(actions.getCommentsSuccess({
      society_id,
      comments: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.getCommentsFail(society_id));
    return error;
  }
};

export const postBankLinkComment = (comment, worksheet_id) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');

  try {
    await dispatch(actions.postCommentAttempt(society_id));

    const {
      body,
      attachTo
    } = comment;

    const { data } = await windev.makeApiCall(
      '/comments',
      'post',
      {},
      {
        location: 'WORKSHEET',
        worksheet_id,
        society_id,
        comment: body,
        attachTo
      }
    );

    await dispatch(actions.postCommentSuccess({
      society_id,
      comment: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.postCommentFail(society_id));
    return error;
  }
};
