import _ from 'lodash';
import {
  GET_BANK_LINK_DATA_ATTEMPT,
  GET_BANK_LINK_DATA_SUCCESS,
  GET_BANK_LINK_DATA_FAIL,
  GET_BANK_LINK_DOCUMENTS_ATTEMPT,
  GET_BANK_LINK_DOCUMENTS_SUCCESS,
  GET_BANK_LINK_DOCUMENTS_FAIL,
  GET_DIARIES_ATTEMPT,
  GET_DIARIES_FAIL,
  GET_DIARIES_SUCCESS,
  ADD_ALL_BILLS,
  ADD_BILL,
  REMOVE_BILL,
  RESET_ALL_BILLS,
  UNPOINT_LINE,
  POINT_LINE,
  GET_COMMENTS_ATTEMPT,
  GET_COMMENTS_FAIL,
  GET_COMMENTS_SUCCESS,
  POST_COMMENT_ATTEMPT,
  POST_COMMENT_FAIL,
  POST_COMMENT_SUCCESS,
  PUT_COMMENT_FAIL,
  PUT_COMMENT_ATTEMPT,
  PUT_COMMENT_SUCCESS,
  GET_DATE_ATTEMPT,
  GET_DATE_SUCCESS,
  GET_DATE_FAIL,
  SET_BANK_LINK_TABLE_DATA,
  POST_VALIDATE_SUCCESS
} from './constants';

const initialState = {
  diaries: [],
  date: '',
  selectedBill: {},
  isLoading: false,
  isError: false,
  isDateLoading: false,
  isDateError: false
};

const bankLink = (state = initialState, action) => {
  switch (action.type) {
  case GET_BANK_LINK_DATA_FAIL:
    return {
      ...state,
      bankLink: {
        ...state.bankLink,
        [action.societyId]: {
          line_writing: []
        }
      },
      isLoading: false,
      isError: true
    };
  case GET_BANK_LINK_DATA_ATTEMPT:
    return {
      ...state,
      bankLink: {},
      isLoading: true,
      isError: false
    };
  case GET_BANK_LINK_DATA_SUCCESS:
    return {
      ...state,
      bankLink: {
        ...state.bankLink,
        [action.societyId]: {
          ...action.payload,
          ...action.opts
        }
      },
      isLoading: false,
      isError: false
    };
  case GET_BANK_LINK_DOCUMENTS_FAIL:
    return {
      ...state,
      bankLink: {
        ...state.bankLink
      }
    };
  case SET_BANK_LINK_TABLE_DATA:
    return {
      ...state,
      bankLinkTableData: {
        [action.societyId]: {
          table_data: action.tableData
        }
      }
    };
  case GET_BANK_LINK_DOCUMENTS_ATTEMPT:
    return {
      ...state
    };
  case GET_BANK_LINK_DOCUMENTS_SUCCESS:
    return {
      ...state,
      pj_list: action.payload
    };
  case GET_DATE_ATTEMPT:
    return {
      ...state,
      isDateLoading: true,
      isDateError: false
    };
  case GET_DATE_FAIL:
    return {
      ...state,
      isDateLoading: false,
      isDateError: true
    };
  case GET_DATE_SUCCESS:
    const {
      date
    } = action;

    return {
      ...state,
      date,
      isDateLoading: false,
      isDateError: false
    };
  case GET_DIARIES_ATTEMPT:
    return {
      ...state
    };
  case GET_DIARIES_FAIL:
    return {
      ...state
    };
  case GET_DIARIES_SUCCESS:
    const {
      diaries
    } = action;

    return {
      ...state,
      diaries
    };
  case ADD_BILL:
    const {
      id_line_entries,
      credit, debit,
      dotting_aa,
      dotting_mm
    } = action.bill;
    const isDotted = !!(dotting_aa && dotting_mm);
    return {
      ...state,
      selectedBill: {
        ...state.selectedBill,
        [action.societyId]: [..._.get(state, `selectedBill[${action.societyId}]`, []), {
          id_line_entries,
          credit,
          debit,
          isDotted
        }]
      }
    };
  case REMOVE_BILL:
    return {
      ...state,
      selectedBill: {
        ...state.selectedBill,
        [action.societyId]: _.get(state, `selectedBill[${action.societyId}]`, []).filter(e => e.id_line_entries !== action.bill.id_line_entries)
      }
    };
  case RESET_ALL_BILLS:
    return {
      ...state,
      selectedBill: {
        ...state.selectedBill,
        [action.societyId]: []
      }
    };
  case ADD_ALL_BILLS:
    return {
      ...state,
      selectedBill: {
        ...state.selectedBill,
        [action.societyId]: [...action.bills.map(({
          id_line_entries, credit, debit, dotting_aa, dotting_mm
        }) => ({
          id_line_entries, credit, debit, isDotted: !!(dotting_aa && dotting_mm)
        }))]
      }
    };
  case POINT_LINE:
    const newSelectedBills = _.get(state, `selectedBill[${action.societyId}]`, []).map(b => ({ ...b, isDotted: true }));
    const oldUndotted = _.get(state, `bankLink[${action.societyId}].undotted_lines`, []);
    const oldDotted = _.get(state, `bankLink[${action.societyId}].dotted_lines`, []);
    const addedDotted = newSelectedBills.map(b => b.id_line_entries);
    const dotting = _.get(state, `bankLink[${action.societyId}].line_writing`, []).map(line => (
      line.id_line_entries === action.line ? {
        ...line,
        dotting_aa: action.aa,
        dotting_mm: action.mm
      } : line
    ));
    const newUndotted = oldUndotted.reduce((acc, id) => (!newSelectedBills.some(
      b => b.id_line_entries === id
    ) ? [...acc, id] : acc), []);
    const newDotted = oldDotted.reduce((acc, id) => (newSelectedBills.some(
      b => b.id_line_entries === id
    ) ? acc : [...acc, id]), addedDotted);
    return {
      ...state,
      selectedBill: {
        ...state.selectedBill,
        [action.societyId]: newSelectedBills
      },
      bankLink: {
        ...state.bankLink,
        [action.societyId]: {
          line_writing: [
            ...dotting
          ],
          dotted_lines: newDotted,
          undotted_lines: newUndotted
        }
      }
    };
  case UNPOINT_LINE:
    const newSelectedBills2 = _.get(state, `selectedBill[${action.societyId}]`, []).map(b => ({ ...b, isDotted: false }));
    const oldUndotted2 = _.get(state, `bankLink[${action.societyId}].undotted_lines`, []);
    const oldDotted2 = _.get(state, `bankLink[${action.societyId}].dotted_lines`, []);
    const addedUndotted = newSelectedBills2.map(b => b.id_line_entries);
    const undotting = _.get(state, `bankLink[${action.societyId}].line_writing`, []).map(line => (
      line.id_line_entries === action.line ? {
        ...line,
        dotting_aa: null,
        dotting_mm: null
      } : line
    ));
    const newUndotted2 = oldUndotted2.reduce((acc, id) => (newSelectedBills2.some(
      b => b.id_line_entries === id
    ) ? acc : [...acc, id]), addedUndotted);
    const newDotted2 = oldDotted2.reduce((acc, id) => (!newSelectedBills2.some(
      b => b.id_line_entries === id
    ) ? [...acc, id] : acc), []);
    return {
      ...state,
      selectedBill: {
        ...state.selectedBill,
        [action.societyId]: newSelectedBills2
      },
      bankLink: {
        ...state.bankLink,
        [action.societyId]: {
          line_writing: [
            ...undotting
          ],
          dotted_lines: newDotted2,
          undotted_lines: newUndotted2
        }
      }
    };

  case GET_COMMENTS_ATTEMPT: {
    return {
      ...state,
      bankLink: {
        ...state.bankLink,
        [action.society_id]: {
          ...state.bankLink[action.society_id],
          comments: []
        }
      }
    };
  }
  case GET_COMMENTS_FAIL: {
    return {
      ...state,
      bankLink: {
        ...state.bankLink,
        [action.society_id]: {
          ...state.bankLink[action.society_id],
          comments: []
        }
      }
    };
  }
  case GET_COMMENTS_SUCCESS: {
    const {
      comments,
      society_id
    } = action.payload;

    return {
      ...state,
      bankLink: {
        ...state.entries,
        [society_id]: {
          ...state.bankLink[society_id],
          comments
        }
      }
    };
  }

  case POST_COMMENT_ATTEMPT: {
    return state;
  }
  case POST_COMMENT_FAIL: {
    return state;
  }
  case POST_COMMENT_SUCCESS: {
    const {
      comment,
      society_id
    } = action.payload;

    return {
      ...state,
      entries: {
        ...state.entries,
        [society_id]: {
          ...state.entries[society_id],
          comments: comment
        }
      }
    };
  }
  case PUT_COMMENT_ATTEMPT: {
    return state;
  }
  case PUT_COMMENT_FAIL: {
    return state;
  }
  case PUT_COMMENT_SUCCESS: {
    const {
      comment,
      society_id
    } = action.payload;

    return {
      ...state,
      entries: {
        ...state.entries,
        [society_id]: {
          ...state.entries[society_id],
          comments: comment
        }
      }
    };
  }
  case POST_VALIDATE_SUCCESS: {
    return {
      ...state,
      bankLink: {
        ...state.bankLink,
        [action.societyId]: {
          ...state.bankLink[action.societyId],
          diligence: action.diligence
        }
      }
    };
  }
  default:
    return state;
  }
};

export default bankLink;
