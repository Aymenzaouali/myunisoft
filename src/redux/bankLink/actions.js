import {
  GET_BANK_LINK_DATA_ATTEMPT,
  GET_BANK_LINK_DATA_SUCCESS,
  GET_BANK_LINK_DATA_FAIL,

  GET_BANK_LINK_DOCUMENTS_ATTEMPT,
  GET_BANK_LINK_DOCUMENTS_SUCCESS,
  GET_BANK_LINK_DOCUMENTS_FAIL,

  GET_DIARIES_ATTEMPT,
  GET_DIARIES_FAIL,
  GET_DIARIES_SUCCESS,

  ADD_BILL,
  ADD_ALL_BILLS,
  REMOVE_BILL,
  RESET_ALL_BILLS,

  GET_BANK_LINK_BINARY_ATTEMPT,
  GET_BANK_LINK_BINARY_FAIL,
  GET_BANK_LINK_BINARY_SUCCESS,

  PUT_BANK_LINK_ATTEMPT,
  PUT_BANK_LINK_SUCCESS,
  PUT_BANK_LINK_FAIL,

  UPDATE_BANK_BALANCE_ATTEMPT,
  UPDATE_BANK_BALANCE_SUCCESS,
  UPDATE_BANK_BALANCE_FAIL,

  POINT_LINE,
  UNPOINT_LINE,

  GET_COMMENTS_ATTEMPT,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAIL,

  POST_COMMENT_ATTEMPT,
  POST_COMMENT_FAIL,
  POST_COMMENT_SUCCESS,

  PUT_COMMENT_ATTEMPT,
  PUT_COMMENT_FAIL,
  PUT_COMMENT_SUCCESS,

  ADD_PJ_BANK_LINK_FAIL,
  ADD_PJ_BANK_LINK_SUCCESS,

  GET_DATE_ATTEMPT,
  GET_DATE_SUCCESS,
  GET_DATE_FAIL,

  POST_VALIDATE_ATTEMPT,
  POST_VALIDATE_FAIL,
  POST_VALIDATE_SUCCESS,

  SET_BANK_LINK_TABLE_DATA
} from './constants';

const setBankLinkTableData = (societyId, tableData) => ({
  type: SET_BANK_LINK_TABLE_DATA,
  societyId,
  tableData
});

const getBankLinkDataAttempt = (societyId, opts) => ({
  type: GET_BANK_LINK_DATA_ATTEMPT,
  opts,
  societyId
});

const getBankLinkDataSuccess = (payload, opts, societyId) => ({
  type: GET_BANK_LINK_DATA_SUCCESS,
  payload,
  opts,
  societyId
});

const getBankLinkDataFail = societyId => ({
  type: GET_BANK_LINK_DATA_FAIL,
  societyId
});

const getBankLinkDocumentsAttempt = (societyId, opts) => ({
  type: GET_BANK_LINK_DOCUMENTS_ATTEMPT,
  opts,
  societyId
});

const getBankLinkDocumentsSuccess = (payload, societyId) => ({
  type: GET_BANK_LINK_DOCUMENTS_SUCCESS,
  payload,
  societyId
});

const getBankLinkDocumentsFail = societyId => ({
  type: GET_BANK_LINK_DOCUMENTS_FAIL,
  societyId
});

const getDiariesSuccess = (diaries, societyId) => ({
  type: GET_DIARIES_SUCCESS,
  diaries,
  societyId
});

const getDiariesFail = societyId => ({
  type: GET_DIARIES_FAIL,
  societyId
});

const getDiariesAttempt = societyId => ({
  type: GET_DIARIES_ATTEMPT,
  societyId
});

const addBill = (bill, societyId) => ({
  type: ADD_BILL,
  bill,
  societyId
});

const removeBill = (bill, societyId) => ({
  type: REMOVE_BILL,
  bill,
  societyId
});

const resetAllBills = societyId => ({
  type: RESET_ALL_BILLS,
  societyId
});

const addAllBills = (societyId, bills) => ({
  type: ADD_ALL_BILLS,
  societyId,
  bills
});

const getBankLinkBinaryAttempt = societyId => ({
  type: GET_BANK_LINK_BINARY_ATTEMPT,
  societyId
});

const getBankLinkBinarySuccess = (file, societyId) => ({
  type: GET_BANK_LINK_BINARY_SUCCESS,
  societyId,
  file
});

const getBankLinkBinaryFail = societyId => ({
  type: GET_BANK_LINK_BINARY_FAIL,
  societyId
});

const putBankLinkAttempt = () => ({
  type: PUT_BANK_LINK_ATTEMPT
});

const putBankLinkSuccess = () => ({
  type: PUT_BANK_LINK_SUCCESS
});

const putBankLinkFail = () => ({
  type: PUT_BANK_LINK_FAIL
});

const updateBankBalanceAttempt = () => ({
  type: UPDATE_BANK_BALANCE_ATTEMPT
});

const updateBankBalanceSuccess = () => ({
  type: UPDATE_BANK_BALANCE_SUCCESS
});

const updateBankBalanceFail = () => ({
  type: UPDATE_BANK_BALANCE_FAIL
});

const addPjBankLinkSuccess = () => ({
  type: ADD_PJ_BANK_LINK_SUCCESS
});

const addPjBankLinkFail = () => ({
  type: ADD_PJ_BANK_LINK_FAIL
});

const getDateAttempt = societyId => ({
  type: GET_DATE_ATTEMPT,
  societyId
});

const getDateSuccess = (date, societyId) => ({
  type: GET_DATE_SUCCESS,
  date,
  societyId
});

const getDateFail = societyId => ({
  type: GET_DATE_FAIL,
  societyId
});

const unpointLine = (societyId, line) => ({
  type: UNPOINT_LINE,
  societyId,
  line
});

const pointLine = (societyId, line, aa, mm) => ({
  type: POINT_LINE,
  societyId,
  line,
  aa,
  mm
});

const getCommentsAttempt = society_id => ({
  type: GET_COMMENTS_ATTEMPT,
  society_id
});
const getCommentsSuccess = payload => ({
  type: GET_COMMENTS_SUCCESS,
  payload
});
const getCommentsFail = society_id => ({
  type: GET_COMMENTS_FAIL,
  society_id
});

const postCommentAttempt = society_id => ({
  type: POST_COMMENT_ATTEMPT,
  society_id
});
const postCommentSuccess = payload => ({
  type: POST_COMMENT_SUCCESS,
  payload
});
const postCommentFail = society_id => ({
  type: POST_COMMENT_FAIL,
  society_id
});

const putCommentAttempt = society_id => ({
  type: PUT_COMMENT_ATTEMPT,
  society_id
});

const putCommentSuccess = payload => ({
  type: PUT_COMMENT_SUCCESS,
  payload
});

const putCommentFail = society_id => ({
  type: PUT_COMMENT_FAIL,
  society_id
});

const postValidateAttempt = () => ({
  type: POST_VALIDATE_ATTEMPT
});

const postValidateFail = () => ({
  type: POST_VALIDATE_FAIL
});

const postValidateSuccess = (diligence, societyId) => ({
  type: POST_VALIDATE_SUCCESS,
  diligence,
  societyId
});

export default {
  getBankLinkDataAttempt,
  getBankLinkDataSuccess,
  getBankLinkDataFail,

  getBankLinkDocumentsAttempt,
  getBankLinkDocumentsSuccess,
  getBankLinkDocumentsFail,

  getDateAttempt,
  getDateSuccess,
  getDateFail,

  getDiariesSuccess,
  getDiariesFail,
  getDiariesAttempt,

  addBill,
  removeBill,
  resetAllBills,
  addAllBills,

  getBankLinkBinaryAttempt,
  getBankLinkBinaryFail,
  getBankLinkBinarySuccess,

  putBankLinkAttempt,
  putBankLinkFail,
  putBankLinkSuccess,

  updateBankBalanceAttempt,
  updateBankBalanceSuccess,
  updateBankBalanceFail,

  unpointLine,
  pointLine,

  getCommentsAttempt,
  getCommentsFail,
  getCommentsSuccess,

  putCommentAttempt,
  putCommentSuccess,
  putCommentFail,

  postCommentAttempt,
  postCommentSuccess,
  postCommentFail,

  addPjBankLinkSuccess,
  addPjBankLinkFail,

  postValidateAttempt,
  postValidateFail,
  postValidateSuccess,

  setBankLinkTableData
};
