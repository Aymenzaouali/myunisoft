import {
  GET_VAT_CYCLE_FAIL,
  GET_VAT_CYCLE_SUCCESS,
  GET_VAT_CYCLE_ATTEMPT,
  SELECT_FORM_ID,
  RESET_FORM_ID,
  getCycle,
  GET_DEADLINE_SUCCESS,
  GET_DEADLINE_FAIL,
  CHANGE_IS_NOTHING
} from './constants';

const initialeState = {
  isNothingness: false
};

const cycle = (state = initialeState, action) => {
  const {
    societyId,
    vatCycle,
    form_id,
    cycleActionType = 'ca12Annual',
    forms,
    deadlines,
    isNothingness = false
  } = action;

  switch (action.type) {
  case SELECT_FORM_ID:
    return {
      ...state,
      selectedFormId: {
        [societyId]: form_id
      }
    };
  case RESET_FORM_ID:
    return {
      ...state,
      selectedFormId: {
        [societyId]: undefined
      }
    };
  case GET_VAT_CYCLE_ATTEMPT:
    return {
      ...state,
      vatCycle: {
        [societyId]: {
          forms: [],
          isLoading: true,
          isError: false
        }
      }
    };
  case GET_VAT_CYCLE_FAIL:
    return {
      ...state,
      vatCycle: {
        [societyId]: {
          forms: [],
          isLoading: false,
          isError: true
        }
      }
    };
  case GET_VAT_CYCLE_SUCCESS:
    return {
      ...state,
      vatCycle: {
        [societyId]: {
          forms: vatCycle,
          isLoading: false,
          isError: false
        }
      },
      selectedFormId: {
        [societyId]: 'tvaca3'
      }
    };
  case getCycle[cycleActionType].GET_ATTEMPT:
    return {
      ...state,
      [cycleActionType]: {
        [societyId]: {
          forms: [],
          isLoading: true,
          isError: false
        }
      }
    };
  case getCycle[cycleActionType].GET_SUCCESS:
    return {
      ...state,
      [cycleActionType]: {
        [societyId]: {
          forms,
          isLoading: false,
          isError: false
        }
      }
    };
  case getCycle[cycleActionType].GET_FAIL:
    return {
      ...state,
      [cycleActionType]: {
        [societyId]: {
          forms: [],
          isLoading: false,
          isError: true
        }
      }
    };
  case GET_DEADLINE_SUCCESS:
    return {
      ...state,
      deadline: {
        [societyId]: {
          list: deadlines,
          isLoading: false,
          isError: false
        }
      }
    };
  case GET_DEADLINE_FAIL:
    return {
      ...state,
      deadline: {
        [societyId]: {
          list: [],
          isLoading: false,
          isError: false
        }
      }
    };
  case CHANGE_IS_NOTHING:
    return {
      ...state,
      isNothingness
    };
  default: return state;
  }
};

export default cycle;
