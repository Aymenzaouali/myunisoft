import {
  get as _get,
  isEmpty as _isEmpty,
  split as _split
} from 'lodash';
import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import { printOrDownload } from 'helpers/file';
import { initialize, getFormValues } from 'redux-form';
import actions from './actions';

const listRadioTVA3519 = [
  {
    name: 'NATIONALITY',
    values: ['DD', 'DE', 'DF']
  },
  {
    name: 'DEMAND',
    values: ['DI', 'DJ', 'DK']
  }
];

const radioValueTVA3519 = 'X';

export const getVatCycle = month => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  await dispatch(initialize(`${society_id}tvaca3`));
  await dispatch(initialize(`${society_id}tva3310`));
  await dispatch(initialize(`${society_id}tva3519`));
  await dispatch(initialize(`${society_id}T_IDENTIF`));
  try {
    await dispatch(actions.getVatCycleAttempt(society_id));
    const { data } = await windev.makeApiCall('/cycle', 'get', { society_id, cycle_code: 'TVA', month });
    await dispatch(actions.getVatCycleSuccess(data, society_id));
    const tvaca3 = _get(data.filter(tva => tva.name === '3310CA3'), '[0]');
    if (!_isEmpty(tvaca3)) {
      const tvaca3Form = _get(tvaca3, 'data');
      if (!_isEmpty(tvaca3Form)) {
        const { KF } = tvaca3Form;
        const isNothingness = !!KF;
        dispatch(actions.changeIsNothing(isNothingness));
        await dispatch(initialize(`${society_id}tvaca3`, tvaca3Form));
      }
    }
    const tidentif = _get(data.filter(tva => tva.name === 'T-IDENTIF'), '[0].data');
    if (!_isEmpty(tidentif)) {
      // Handle RIB autocomplete initialization
      if (tidentif.GAE) {
        tidentif.GAE = { value: tidentif.GAE, label: tidentif.GAE };
      }
      if (tidentif.GBE) {
        tidentif.GBE = { value: tidentif.GBE, label: tidentif.GBE };
      }
      if (tidentif.GCE) {
        tidentif.GCE = { value: tidentif.GCE, label: tidentif.GCE };
      }
      await dispatch(initialize(`${society_id}T_IDENTIF`, tidentif));
    }
    const tva3310A = _get(data.filter(tva => tva.name === '3310A'), '[0].data');
    if (!_isEmpty(tva3310A)) {
      await dispatch(initialize(`${society_id}tva3310`, tva3310A));
    }
    const tva3519 = _get(data.filter(tva => tva.name === '3519'), '[0].data');
    if (!_isEmpty(tva3519)) {
      const modifiedTva = { ...tva3519 };
      if (modifiedTva.AAC) {
        modifiedTva.AAC = { value: modifiedTva.AAC, label: modifiedTva.AAC };
      }
      listRadioTVA3519.map((item) => {
        const values = _get(item, 'values', []);
        values.map((value) => {
          const name = _get(item, 'name', null);
          if (value && name) {
            if (tva3519[value] && tva3519[value] === radioValueTVA3519) {
              modifiedTva[name] = value;
            }
          }
          return null;
        });
        return null;
      });

      await dispatch(initialize(`${society_id}tva3519`, modifiedTva));
    }
    return data;
  } catch (err) {
    await dispatch(actions.getVatCycleFail(society_id));
    return err;
  }
};

export const autoFillVatCA3 = month => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  try {
    const tva_forms = _get(state, `cycle.vatCycle[${society_id}].forms`);
    const form_id = _get(tva_forms.filter(tva => tva.name === '3310CA3'), '[0].form_id');
    const params = {
      month,
      form_id
    };
    await dispatch(actions.autoFillVatCA3());
    const { data } = await windev.makeApiCall('/cycle/auto_fill', 'get', params);
    await dispatch(getVatCycle(month));
    await dispatch(actions.selectFormId(society_id, 'tvaca3'));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    throw err;
  }
};

export const sendVat = month => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  try {
    const tva_forms = _get(state, `cycle.vatCycle[${society_id}].forms`);
    const selectedFormId = _get(state, `cycle.selectedFormId[${society_id}]`, 'tvaca3');
    let form_id = '';
    let vat = {};
    if (selectedFormId === 'tidentif') {
      form_id = _get(tva_forms.filter(tva => tva.name === 'T-IDENTIF'), '[0].form_id');
      vat = { ...getFormValues(`${society_id}T_IDENTIF`)(state) };
      // Handle RIB get value from autocomplete object
      if (vat.GAE) {
        vat.GAE = _get(vat, 'GAE.value');
      }
      if (vat.GBE) {
        vat.GBE = _get(vat, 'GBE.value');
      }
      if (vat.GCE) {
        vat.GCE = _get(vat, 'GCE.value');
      }
    } else if (selectedFormId === 'tvaca3') {
      form_id = _get(tva_forms.filter(tva => tva.name === '3310CA3'), '[0].form_id');
      vat = getFormValues(`${society_id}tvaca3`)(state);
    } else if (selectedFormId === 'tva3310') {
      form_id = _get(tva_forms.filter(tva => tva.name === '3310A'), '[0].form_id');
      vat = getFormValues(`${society_id}tva3310`)(state);
    } else {
      form_id = _get(tva_forms.filter(tva => tva.name === '3519'), '[0].form_id');
      const vatForm = getFormValues(`${society_id}tva3519`)(state);

      const { NATIONALITY, DEMAND, ...modifiedVat } = vatForm;
      if (modifiedVat.AAC) {
        modifiedVat.AAC = _get(modifiedVat, 'AAC.value');
      }
      listRadioTVA3519.map((item) => {
        const values = _get(item, 'values', []);
        values.map((value) => {
          const name = _get(item, 'name', null);
          if (value && name) {
            modifiedVat[value] = vatForm[name] === value ? radioValueTVA3519 : '';
          }
          return null;
        });
        return null;
      });

      vat = modifiedVat;
    }
    await dispatch(actions.sendVatCycleAttempt(society_id));
    if (!vat || vat === 'undefined') throw new Error();
    const { data } = await windev.makeApiCall('/cycle/form', 'post', {
      society_id, month, form_id
    }, vat);
    await dispatch(actions.sendVatCycleSuccess(data, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.sendVatCycleFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const sendVatEdi = (month, confirmed) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  const payloadParams = {
    society_id,
    month,
    cycle_code: 'TVA'
  };
  if (confirmed) payloadParams.confirmed = true;
  try {
    await dispatch(sendVat(month));
    const { data } = await windev.makeApiCall('/cycle/send_edi', 'post', payloadParams, {});
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    throw err;
  }
};

const printOrDownloadRequest = (month, type, cycle_code) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getBinaryVatCycleAttempt(society_id));
    const { data, headers } = await windev.makeApiCall('/cycle/download', 'get', { society_id, month, cycle_code }, {}, { responseType: 'blob' });
    await dispatch(actions.getBinaryVatCycleSuccess(data, society_id));
    const filename = _split(_get(headers, 'content-disposition'), '"')[1];
    printOrDownload(data, type, `${filename}`);
    return data;
  } catch (err) {
    await dispatch(actions.getBinaryVatCycleFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const printOrDownloadCycle = (month, type) => async (dispatch) => {
  dispatch(printOrDownloadRequest(month, type, 'TVA'));
};

const availableForms = {
  ca12Annual: ['3517SCA12', '3517DDR', 'T-IDENTIF'],
  ca12Advance: ['3514', 'T-IDENTIF']
};

const getCycleActionType = (code_sheet_group) => {
  switch (code_sheet_group) {
  case 'CA12-ANNUAL':
    return 'ca12Annual';
  case 'CA12-ACOMPTE':
    return 'ca12Advance';
  default:
    return 'ca12Annual';
  }
};

const getDefaultForm = (code_sheet_group) => {
  switch (code_sheet_group) {
  case 'CA12-ANNUAL':
    return '3517SCA12';
  case 'CA12-ACOMPTE':
    return '3514';
  default:
    return '3517SCA12';
  }
};

export const getDeadline = (exercice_id, code_sheet_group) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getDeadlineAttempt(society_id));
    const { data } = await windev.makeApiCall('/cycle/deadline', 'get', {
      cycle: code_sheet_group,
      exercice_id
    });
    await dispatch(actions.getDeadlineSuccess(society_id, data));
  } catch (error) {
    await dispatch(actions.getDeadlineFail(society_id));
    await dispatch(handleError(error));
    throw error;
  }
};

export const getCycleForms = (
  exercice_id, cycle_code, date_declare, code_sheet_group
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  const cycleActionType = getCycleActionType(code_sheet_group);
  try {
    await dispatch(actions.getActions.getAttempt(cycleActionType, society_id));

    const { data } = await windev.makeApiCall('/cycle', 'get', {
      society_id,
      cycle_code,
      month: date_declare,
      exercice_id
    });
    // set Neant checkbox
    const neant = _get(data.filter(form => form.name === '3517SCA12'), '[0].data.SD', false);
    const isNothingness = !!neant;

    dispatch(actions.changeIsNothing(isNothingness));
    // Init pdf forms
    availableForms[cycleActionType].forEach((formItem) => {
      const formName = `${society_id}_${cycleActionType}_${formItem}`;
      const formExistData = _get(state, `form.${formName}.values`);
      dispatch(initialize(formName));
      if (data) {
        const formData = _get(data.filter(form => form.name === formItem), '[0]');
        if (!_isEmpty(formData)) {
          const form = _get(formData, 'data');
          if (!_isEmpty(form)) {
            const formDataToDispatch = { ...formExistData, ...form };
            dispatch(initialize(`${society_id}_${cycleActionType}_${formItem}`, formDataToDispatch));
          }
        }
      }
    });

    await dispatch(actions.getActions.getSuccess(cycleActionType, data, society_id));
    return data;
  } catch (error) {
    await dispatch(actions.getActions.getFail(cycleActionType, society_id));
    await dispatch(handleError(error));
    return error;
  }
};

export const sendCycleForms = (
  exercice_id, cycle_code, date_declare, code_sheet_group
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  const cycleActionType = getCycleActionType(code_sheet_group);
  const cycleForms = _get(state, `cycle.${cycleActionType}.${society_id}.forms`, []);
  const selectedFormId = _get(state, `cycle.selectedFormId[${society_id}]`, getDefaultForm(code_sheet_group));
  const form_id = _get(cycleForms.filter(form => form.name === selectedFormId), '[0].form_id', '');
  let cycle = {};
  cycle = getFormValues(`${society_id}_${cycleActionType}_${selectedFormId}`)(state);

  try {
    await dispatch(actions.sendActions.sendAttempt(cycleActionType, society_id));
    if (!cycle || cycle === 'undefined') throw new Error();
    await windev.makeApiCall('/cycle/form', 'post', {
      society_id, month: date_declare, form_id
    }, cycle);
    await dispatch(actions.sendActions.sendSuccess(cycleActionType, society_id));
    return true;
  } catch (error) {
    await dispatch(actions.sendActions.sendFail(cycleActionType, society_id));
    await dispatch(handleError(error));
    return error;
  }
};

export const sendCycleFormsEdi = (
  exercice_id, cycle_code, date_declare, code_sheet_group
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  const cycleActionType = getCycleActionType(code_sheet_group);

  const cycleForms = _get(state, `cycle.${cycleActionType}.${society_id}.forms`, []);
  const selectedFormId = _get(state, `cycle.selectedFormId[${society_id}]`, getDefaultForm(code_sheet_group));
  const form_id = _get(cycleForms.filter(form => form.name === selectedFormId), '[0].form_id', '');
  let cycle = {};
  cycle = getFormValues(`${society_id}_${cycleActionType}_${selectedFormId}`)(state);

  try {
    await dispatch(actions.sendEdiActions.sendEdiAttempt(cycleActionType, society_id));
    if (!cycle || cycle === 'undefined') throw new Error();
    await windev.makeApiCall('/cycle/send_edi', 'post', {
      society_id, month: date_declare, form_id
    }, cycle);
    await dispatch(actions.sendEdiActions.sendEdiSuccess(cycleActionType, society_id));
    return true;
  } catch (error) {
    await dispatch(actions.sendEdiActions.sendEdiFail(cycleActionType, society_id));
    await dispatch(handleError(error));
    return error;
  }
};

export const printOrDownloadCycleForms = (month, type, cycle_code) => async (dispatch) => {
  dispatch(printOrDownloadRequest(month, type, cycle_code));
};

const resetList = {
  TVA: [
    'tvaca3',
    'tva3310',
    'tva3519',
    'T_IDENTIF'
  ],
  CA12: [
    '_ca12Annual_T-IDENTIF',
    '_ca12Annual_3517DDR'
  ]
};

export const sendIsNothingness = ({ month, cycle_code, neant }) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  dispatch(actions.changeIsNothing(neant));
  const sendingNeant = neant ? 1 : 0;
  try {
    await windev.makeApiCall('/cycle/neant', 'post', { month, cycle_code, neant: sendingNeant }, []);
    if (neant) {
      resetList[cycle_code].forEach((form) => {
        dispatch(initialize(`${society_id}${form}`));
      });
    }
  } catch (err) {
    await dispatch(handleError(err));
  }
};
