import {
  GET_VAT_CYCLE_ATTEMPT,
  GET_VAT_CYCLE_SUCCESS,
  GET_VAT_CYCLE_FAIL,
  SEND_VAT_CYCLE_ATTEMPT,
  SEND_VAT_CYCLE_SUCCESS,
  SEND_VAT_CYCLE_FAIL,
  GET_BINARY_VAT_CYCLE_ATTEMPT,
  GET_BINARY_VAT_CYCLE_SUCCESS,
  GET_BINARY_VAT_CYCLE_FAIL,
  RESET_FORM_ID,
  SELECT_FORM_ID,
  AUTO_FILL_VAT_CA3,
  getCycle,
  sendCycle,
  sendEdiCycle,
  GET_DEADLINE_ATTEMPT,
  GET_DEADLINE_FAIL,
  GET_DEADLINE_SUCCESS,
  CHANGE_IS_NOTHING
} from './constants';

const getVatCycleAttempt = societyId => ({
  type: GET_VAT_CYCLE_ATTEMPT,
  societyId
});

const getVatCycleSuccess = (vatCycle, societyId) => ({
  type: GET_VAT_CYCLE_SUCCESS,
  societyId,
  vatCycle
});

const getVatCycleFail = societyId => ({
  type: GET_VAT_CYCLE_FAIL,
  societyId
});

const sendVatCycleAttempt = societyId => ({
  type: SEND_VAT_CYCLE_ATTEMPT,
  societyId
});

const sendVatCycleSuccess = (vatCycle, societyId) => ({
  type: SEND_VAT_CYCLE_SUCCESS,
  societyId,
  vatCycle
});

const sendVatCycleFail = societyId => ({
  type: SEND_VAT_CYCLE_FAIL,
  societyId
});

const getBinaryVatCycleAttempt = societyId => ({
  type: GET_BINARY_VAT_CYCLE_ATTEMPT,
  societyId
});

const getBinaryVatCycleSuccess = (file, societyId) => ({
  type: GET_BINARY_VAT_CYCLE_SUCCESS,
  societyId,
  file
});

const getBinaryVatCycleFail = societyId => ({
  type: GET_BINARY_VAT_CYCLE_FAIL,
  societyId
});

const selectFormId = (societyId, form_id) => ({
  type: SELECT_FORM_ID,
  societyId,
  form_id
});

const resetFormId = societyId => ({
  type: RESET_FORM_ID,
  societyId
});

const autoFillVatCA3 = () => ({
  type: AUTO_FILL_VAT_CA3
});

const getActions = {
  getAttempt: (cycleActionType, societyId) => ({
    type: getCycle[cycleActionType].GET_ATTEMPT,
    cycleActionType,
    societyId
  }),
  getSuccess: (cycleActionType, forms, societyId) => ({
    type: getCycle[cycleActionType].GET_SUCCESS,
    cycleActionType,
    societyId,
    forms
  }),
  getFail: (cycleActionType, societyId) => ({
    type: getCycle[cycleActionType].GET_FAIL,
    cycleActionType,
    societyId
  })
};

const sendActions = {
  sendAttempt: (cycleActionType, societyId) => ({
    type: sendCycle[cycleActionType].SEND_ATTEMPT,
    cycleActionType,
    societyId
  }),
  sendSuccess: (cycleActionType, forms, societyId) => ({
    type: sendCycle[cycleActionType].SEND_SUCCESS,
    cycleActionType,
    societyId,
    forms
  }),
  sendFail: (cycleActionType, societyId) => ({
    type: sendCycle[cycleActionType].SEND_FAIL,
    cycleActionType,
    societyId
  })
};

const sendEdiActions = {
  sendEdiAttempt: (cycleActionType, societyId) => ({
    type: sendEdiCycle[cycleActionType].SEND_EDI_ATTEMPT,
    cycleActionType,
    societyId
  }),
  sendEdiSuccess: (cycleActionType, forms, societyId) => ({
    type: sendEdiCycle[cycleActionType].SEND_EDI_SUCCESS,
    cycleActionType,
    societyId,
    forms
  }),
  sendEdiFail: (cycleActionType, societyId) => ({
    type: sendEdiCycle[cycleActionType].SEND_EDI_FAIL,
    cycleActionType,
    societyId
  })
};

const getDeadlineAttempt = societyId => ({
  type: GET_DEADLINE_ATTEMPT,
  societyId
});

const getDeadlineSuccess = (societyId, deadlines) => ({
  type: GET_DEADLINE_SUCCESS,
  societyId,
  deadlines
});

const getDeadlineFail = societyId => ({
  type: GET_DEADLINE_FAIL,
  societyId
});


const changeIsNothing = isNothingness => ({
  type: CHANGE_IS_NOTHING,
  isNothingness
});

export default {
  getVatCycleAttempt,
  getVatCycleSuccess,
  getVatCycleFail,
  sendVatCycleAttempt,
  sendVatCycleSuccess,
  sendVatCycleFail,
  getBinaryVatCycleAttempt,
  getBinaryVatCycleSuccess,
  getBinaryVatCycleFail,
  selectFormId,
  resetFormId,
  autoFillVatCA3,
  getActions,
  sendActions,
  sendEdiActions,
  getDeadlineAttempt,
  getDeadlineSuccess,
  getDeadlineFail,
  changeIsNothing
};
