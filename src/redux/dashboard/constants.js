export const GET_TRACKING_SOCIETIES_ATTEMPT = 'GET_TRACKING_SOCIETIES_ATTEMPT';
export const GET_TRACKING_SOCIETIES_SUCCESS = 'GET_TRACKING_SOCIETIES_SUCCESS';
export const GET_TRACKING_SOCIETIES_FAIL = 'GET_TRACKING_SOCIETIES_FAIL';
export const GET_PROGRESSION_SOCIETY_ATTEMPT = 'GET_PROGRESSION_SOCIETY_ATTEMPT';
export const GET_PROGRESSION_SOCIETY_SUCCESS = 'GET_PROGRESSION_SOCIETY_SUCCESS';
export const GET_PROGRESSION_SOCIETY_FAIL = 'GET_PROGRESSION_SOCIETY_FAIL';
export const GET_MEMO_ATTEMPT = 'GET_MEMO_ATTEMPT';
export const GET_MEMO_SUCCESS = 'GET_MEMO_SUCCESS';
export const GET_MEMO_FAIL = 'GET_MEMO_FAIL';
export const GET_CHARTS_ATTEMPT = 'GET_CHARTS_ATTEMPT';
export const GET_CHARTS_SUCCESS = 'GET_CHARTS_SUCCESS';
export const GET_CHARTS_FAIL = 'GET_CHARTS_FAIL';
export const PUT_MEMO_ATTEMPT = 'PUT_MEMO_ATTEMPT';
export const PUT_MEMO_SUCCESS = 'PUT_MEMO_SUCCESS';
export const PUT_MEMO_FAIL = 'PUT_MEMO_FAIL';
export const POST_MEMO_ATTEMPT = 'POST_MEMO_ATTEMPT';
export const POST_MEMO_SUCCESS = 'POST_MEMO_SUCCESS';
export const POST_MEMO_FAIL = 'POST_MEMO_FAIL';
export const GET_EXERCISES_LIST_ATTEMPT = 'GET_EXERCISES_LIST_ATTEMPT';
export const GET_EXERCISES_LIST_SUCCESS = 'GET_EXERCISES_LIST_SUCCESS';
export const GET_EXERCISES_LIST_FAIL = 'GET_EXERCISES_LIST_FAIL';

export const SELECT_TRACKING_MODE = 'SELECT_TRACKING_MODE';
export const CURRENT_PERIOD = 'CURRENT_PERIOD';
export const CHANGE_SELECTED_PERIOD = 'CHANGE_SELECTED_PERIOD';
export const CLEAR_SELECTED_PERIOD = 'CLEAR_SELECTED_PERIOD';
export const CURRENT_EXERCISE = 'CURRENT_EXERCISE';
export const CHANGE_SELECTED_EXERCISE = 'CHANGE_SELECTED_EXERCISE';
export const INIT_DATA_FOR_ACCOUNTING = 'INIT_DATA_FOR_ACCOUNTING';
export const CLEAR_DATA_FOR_ACCOUNTING = 'CLEAR_DATA_FOR_ACCOUNTING';
export const INIT_FILTER_FOR_VAT = 'INIT_FILTER_FOR_VAT';
export const CLEAR_FILTER_FOR_VAT = 'CLEAR_FILTER_FOR_VAT';
export const INIT_FILTER_FOR_IS = 'INIT_FILTER_FOR_IS';
export const CLEAR_FILTER_FOR_IS = 'CLEAR_FILTER_FOR_IS';
export const INIT_FILTER_FOR_CVAE = 'INIT_FILTER_FOR_CVAE';
export const CLEAR_FILTER_FOR_CVAE = 'CLEAR_FILTER_FOR_CVAE';
export const CLEAR_PROGRESSION_TABLE_SORT = 'CLEAR_PROGRESSION_TABLE_SORT';
