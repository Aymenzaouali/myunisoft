import _ from 'lodash';
import {
  GET_TRACKING_SOCIETIES_ATTEMPT,
  GET_TRACKING_SOCIETIES_SUCCESS,
  GET_TRACKING_SOCIETIES_FAIL,
  GET_PROGRESSION_SOCIETY_ATTEMPT,
  GET_PROGRESSION_SOCIETY_SUCCESS,
  GET_PROGRESSION_SOCIETY_FAIL,
  GET_MEMO_ATTEMPT,
  GET_MEMO_SUCCESS,
  GET_MEMO_FAIL,
  GET_CHARTS_ATTEMPT,
  GET_CHARTS_SUCCESS,
  GET_CHARTS_FAIL,
  PUT_MEMO_ATTEMPT,
  PUT_MEMO_SUCCESS,
  PUT_MEMO_FAIL,
  POST_MEMO_ATTEMPT,
  POST_MEMO_SUCCESS,
  POST_MEMO_FAIL,
  GET_EXERCISES_LIST_ATTEMPT,
  GET_EXERCISES_LIST_SUCCESS,
  GET_EXERCISES_LIST_FAIL,
  SELECT_TRACKING_MODE,
  CURRENT_PERIOD,
  CHANGE_SELECTED_PERIOD,
  CLEAR_SELECTED_PERIOD,
  CHANGE_SELECTED_EXERCISE,
  CURRENT_EXERCISE,
  INIT_DATA_FOR_ACCOUNTING,
  CLEAR_DATA_FOR_ACCOUNTING,
  INIT_FILTER_FOR_VAT,
  CLEAR_FILTER_FOR_VAT,
  INIT_FILTER_FOR_IS,
  CLEAR_FILTER_FOR_IS,
  INIT_FILTER_FOR_CVAE,
  CLEAR_FILTER_FOR_CVAE,
  CLEAR_PROGRESSION_TABLE_SORT
} from './constants';

const cardList = ['situation_globale', 'client', 'tresorerie', 'fournisseur'];

const initialState = {
  trackingSocieties: [],
  progression: [],
  memoItems: [],
  exercisesList: [],
  currentMode: 1,
  selectedExercise: {},
  currentExercise: undefined,
  filterAccounting: undefined,
  filterVat: undefined,
  filterIs: undefined,
  filterCvae: undefined
};

const dashboard = (state = initialState, action) => {
  if (action && action.type) {
    const {
      societyId,
      opts
    } = action;

    switch (action.type) {
    case GET_TRACKING_SOCIETIES_ATTEMPT:
      return {
        ...state,
        isLoading: true,
        isError: false,
        trackingSocieties: []
      };
    case GET_TRACKING_SOCIETIES_SUCCESS:
      const {
        trackingSocieties
      } = action;

      return {
        ...state,
        isLoading: false,
        isError: false,
        ...opts,
        trackingSocieties
      };
    case GET_TRACKING_SOCIETIES_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: true,
        trackingSocieties: []
      };
    case GET_PROGRESSION_SOCIETY_ATTEMPT:
      return {
        ...state,
        isLoading: true,
        isError: false,
        progression: []
      };
    case GET_PROGRESSION_SOCIETY_SUCCESS:
      const {
        progression
      } = action;

      return {
        ...state,
        isLoading: false,
        isError: false,
        ...opts,
        progression
      };
    case GET_PROGRESSION_SOCIETY_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: true,
        progression: []
      };
    case GET_MEMO_ATTEMPT:
      return {
        ...state
      };
    case GET_MEMO_SUCCESS:
      const {
        memoListing
      } = action;
      return {
        ...state,
        memoItems: _.filter(memoListing.memo, { done: false })
      };
    case GET_MEMO_FAIL:
      return {
        ...state
      };
    case GET_CHARTS_ATTEMPT:
      return {
        ...state,
        situation_globale: {
          isLoading: true
        },
        client: {
          isLoading: true
        },
        tresorerie: {
          isLoading: true
        },
        fournisseur: {
          isLoading: true
        }
      };
    case GET_CHARTS_SUCCESS:
      const { chartData } = action;
      const allData = chartData.map(e => ({
        record: e,
        isLoading: false
      }));

      let chartsData;

      for (let i = 0; i < chartData.length; i += 1) {
        const {
          type
        } = chartData[i];

        chartsData = {
          ...chartsData,
          [type]: allData[i]
        };
      }

      if (chartData.length < 4) {
        const missingChart = cardList.filter(card => !chartData.find(chart => chart.type === card));

        if (missingChart) {
          let missingChartData;

          for (let i = 0; i < missingChart.length; i += 1) {
            missingChartData = {
              ...missingChartData,
              [missingChart[i]]: { isLoading: false }
            };
          }

          chartsData = {
            ...chartsData,
            ...missingChartData
          };
        }
      }

      return {
        ...state,
        ...chartsData
      };
    case GET_CHARTS_FAIL:
      return {
        ...state,
        situation_globale: {
          isLoading: false
        },
        client: {
          isLoading: false
        },
        tresorerie: {
          isLoading: false
        },
        fournisseur: {
          isLoading: false
        }
      };
    case PUT_MEMO_ATTEMPT:
      return {
        ...state
      };
    case PUT_MEMO_SUCCESS:
      const { data } = action;
      return {
        ...state,
        memoItems: state.memoItems.map(m => (m.memo_id === data.memo_id ? {
          ...m,
          done: data.done
        } : m)),
        processedMemo: data.done ? state.processedMemo + 1 : state.processedMemo - 1
      };
    case PUT_MEMO_FAIL:
      return {
        ...state
      };
    case POST_MEMO_ATTEMPT:
      return {
        ...state
      };
    case POST_MEMO_SUCCESS:
      return {
        ...state,
        memoItems: [...state.memoItems, action.data.inst_memo],
        allMemo: state.allMemo + 1
      };
    case POST_MEMO_FAIL:
      return {
        ...state
      };
    case GET_EXERCISES_LIST_ATTEMPT:
      return {
        ...state,
        exercisesList: {
          ...state.exercisesList,
          [societyId]: []
        }
      };
    case GET_EXERCISES_LIST_SUCCESS:
      const { exercisesList } = action;
      return {
        ...state,
        exercisesList: {
          ...state.exercisesList,
          [societyId]: exercisesList
        }
      };
    case GET_EXERCISES_LIST_FAIL:
      return {
        ...state,
        exercisesList: {
          ...state.exercisesList,
          [societyId]: []
        }
      };
    case SELECT_TRACKING_MODE:
      const { mode } = action;
      return {
        ...state,
        currentMode: mode
      };
    case CHANGE_SELECTED_PERIOD:
      const { period } = action;
      return {
        ...state,
        selectedPeriod: period
      };
    case CLEAR_SELECTED_PERIOD:
      return {
        ...state,
        selectedPeriod: undefined
      };
    case CURRENT_PERIOD:
      const { currentPeriod } = action;
      return {
        ...state,
        currentPeriod
      };
    case CHANGE_SELECTED_EXERCISE:
      const { exercise } = action;
      return {
        ...state,
        selectedExercise: {
          ...state.selectedExercise,
          [societyId]: exercise
        }
      };
    case CURRENT_EXERCISE:
      const { currentExercise } = action;
      return {
        ...state,
        currentExercise: {
          ...state.currentExercise,
          [societyId]: currentExercise
        }
      };
    case INIT_DATA_FOR_ACCOUNTING:
      const { filterAccounting } = action;
      return {
        ...state,
        filterAccounting
      };
    case CLEAR_DATA_FOR_ACCOUNTING:
      return {
        ...state,
        filterAccounting: undefined
      };
    case INIT_FILTER_FOR_VAT:
      const { filterVat } = action;
      return {
        ...state,
        filterVat
      };
    case CLEAR_FILTER_FOR_VAT:
      return {
        ...state,
        filterVat: undefined
      };
    case INIT_FILTER_FOR_IS:
      const { filterIs } = action;
      return {
        ...state,
        filterIs
      };
    case CLEAR_FILTER_FOR_IS:
      return {
        ...state,
        filterIs: undefined
      };
    case INIT_FILTER_FOR_CVAE:
      const { filterCvae } = action;
      return {
        ...state,
        filterCvae
      };
    case CLEAR_FILTER_FOR_CVAE:
      return {
        ...state,
        filterCvae: undefined
      };
    case CLEAR_PROGRESSION_TABLE_SORT:
      return {
        ...state,
        sort: undefined
      };
    default: return state;
    }
  }
  return state;
};

export default dashboard;
