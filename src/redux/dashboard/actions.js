import {
  GET_TRACKING_SOCIETIES_ATTEMPT,
  GET_TRACKING_SOCIETIES_SUCCESS,
  GET_TRACKING_SOCIETIES_FAIL,
  GET_PROGRESSION_SOCIETY_ATTEMPT,
  GET_PROGRESSION_SOCIETY_SUCCESS,
  GET_PROGRESSION_SOCIETY_FAIL,
  GET_MEMO_ATTEMPT,
  GET_MEMO_SUCCESS,
  GET_MEMO_FAIL,
  GET_CHARTS_ATTEMPT,
  GET_CHARTS_SUCCESS,
  GET_CHARTS_FAIL,
  PUT_MEMO_ATTEMPT,
  PUT_MEMO_SUCCESS,
  PUT_MEMO_FAIL,
  POST_MEMO_ATTEMPT,
  POST_MEMO_SUCCESS,
  POST_MEMO_FAIL,
  GET_EXERCISES_LIST_ATTEMPT,
  GET_EXERCISES_LIST_SUCCESS,
  GET_EXERCISES_LIST_FAIL,
  SELECT_TRACKING_MODE,
  CURRENT_PERIOD,
  CHANGE_SELECTED_PERIOD,
  CLEAR_SELECTED_PERIOD,
  CHANGE_SELECTED_EXERCISE,
  CURRENT_EXERCISE,
  INIT_DATA_FOR_ACCOUNTING,
  CLEAR_DATA_FOR_ACCOUNTING,
  INIT_FILTER_FOR_VAT,
  CLEAR_FILTER_FOR_VAT,
  INIT_FILTER_FOR_IS,
  CLEAR_FILTER_FOR_IS,
  INIT_FILTER_FOR_CVAE,
  CLEAR_FILTER_FOR_CVAE,
  CLEAR_PROGRESSION_TABLE_SORT
} from './constants';

const getTrackingSocietiesAttempt = () => ({
  type: GET_TRACKING_SOCIETIES_ATTEMPT
});

const getTrackingSocietiesSuccess = (trackingSocieties, opts) => ({
  type: GET_TRACKING_SOCIETIES_SUCCESS,
  trackingSocieties,
  opts
});

const getTrackingSocietiesFail = () => ({
  type: GET_TRACKING_SOCIETIES_FAIL
});

const getProgressionSocietyAttempt = () => ({
  type: GET_PROGRESSION_SOCIETY_ATTEMPT
});

const getProgressionSocietySuccess = (progression, opts) => ({
  type: GET_PROGRESSION_SOCIETY_SUCCESS,
  progression,
  opts
});

const getProgressionSocietyFail = () => ({
  type: GET_PROGRESSION_SOCIETY_FAIL
});

const getMemoAttempt = () => ({
  type: GET_MEMO_ATTEMPT
});

const getMemoSuccess = memoListing => ({
  type: GET_MEMO_SUCCESS,
  memoListing
});

const getMemoFail = () => ({
  type: GET_MEMO_FAIL
});

const getChartsAttempt = () => ({
  type: GET_CHARTS_ATTEMPT
});

const getChartsSuccess = chartData => ({
  type: GET_CHARTS_SUCCESS,
  chartData
});

const getChartsFail = () => ({
  type: GET_CHARTS_FAIL
});

const putMemoAttempt = () => ({
  type: PUT_MEMO_ATTEMPT
});

const putMemoSuccess = data => ({
  type: PUT_MEMO_SUCCESS,
  data
});

const putMemoFail = () => ({
  type: PUT_MEMO_FAIL
});

const postMemoAttempt = () => ({
  type: POST_MEMO_ATTEMPT
});

const postMemoSuccess = data => ({
  type: POST_MEMO_SUCCESS,
  data
});

const postMemoFail = () => ({
  type: POST_MEMO_FAIL
});

const getExercisesListAttempt = societyId => ({
  type: GET_EXERCISES_LIST_ATTEMPT,
  societyId
});

const getExercisesListSuccess = (exercisesList, societyId) => ({
  type: GET_EXERCISES_LIST_SUCCESS,
  exercisesList,
  societyId
});

const getExercisesListFail = societyId => ({
  type: GET_EXERCISES_LIST_FAIL,
  societyId
});

const changeTrackingMode = mode => ({
  type: SELECT_TRACKING_MODE,
  mode
});

const changeSelectedPeriod = period => ({
  type: CHANGE_SELECTED_PERIOD,
  period
});

const clearSelectedPeriod = () => ({
  type: CLEAR_SELECTED_PERIOD
});

const initCurrentPeriod = currentPeriod => ({
  type: CURRENT_PERIOD,
  currentPeriod
});

const changeSelectedExercise = (exercise, societyId) => ({
  type: CHANGE_SELECTED_EXERCISE,
  exercise,
  societyId
});

const initCurrentExercise = (currentExercise, societyId) => ({
  type: CURRENT_EXERCISE,
  currentExercise,
  societyId
});

const initDataForAccounting = filterAccounting => ({
  type: INIT_DATA_FOR_ACCOUNTING,
  filterAccounting
});

const clearDataForAccounting = () => ({
  type: CLEAR_DATA_FOR_ACCOUNTING
});

const initFilterForVat = filterVat => ({
  type: INIT_FILTER_FOR_VAT,
  filterVat
});

const clearFilterForVat = () => ({
  type: CLEAR_FILTER_FOR_VAT
});

const initFilterForIs = filterIs => ({
  type: INIT_FILTER_FOR_IS,
  filterIs
});

const clearFilterForIs = () => ({
  type: CLEAR_FILTER_FOR_IS
});

const initFilterForCvae = filterCvae => ({
  type: INIT_FILTER_FOR_CVAE,
  filterCvae
});

const clearFilterForCvae = () => ({
  type: CLEAR_FILTER_FOR_CVAE
});

const clearProgressionTableSort = () => ({
  type: CLEAR_PROGRESSION_TABLE_SORT
});

export default {
  getTrackingSocietiesAttempt,
  getTrackingSocietiesSuccess,
  getTrackingSocietiesFail,
  getProgressionSocietyAttempt,
  getProgressionSocietySuccess,
  getProgressionSocietyFail,
  getMemoAttempt,
  getMemoSuccess,
  getMemoFail,
  getChartsAttempt,
  getChartsSuccess,
  getChartsFail,
  putMemoAttempt,
  putMemoSuccess,
  putMemoFail,
  postMemoAttempt,
  postMemoSuccess,
  postMemoFail,
  getExercisesListAttempt,
  getExercisesListSuccess,
  getExercisesListFail,
  changeTrackingMode,
  changeSelectedPeriod,
  clearSelectedPeriod,
  initCurrentPeriod,
  changeSelectedExercise,
  initCurrentExercise,
  initDataForAccounting,
  clearDataForAccounting,
  initFilterForVat,
  clearFilterForVat,
  initFilterForIs,
  clearFilterForIs,
  initFilterForCvae,
  clearFilterForCvae,
  clearProgressionTableSort
};
