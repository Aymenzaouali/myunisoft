import { windev, mock } from 'helpers/api';
import { handleError } from 'common/redux/error';
import moment from 'moment';
import _ from 'lodash';
import actions from './actions';

const currentDate = moment().format('YYYY-MM-DD');

async function getCurrentExercice(society_id, exercisesList, dispatch) {
  let currentExercise;

  if (_.isEmpty(exercisesList)) {
    try {
      await dispatch(actions.getExercisesListAttempt());
      const { data } = await windev.makeApiCall('/exercices', 'get', { society_id });
      data.forEach((e) => {
        const {
          start_date,
          end_date
        } = e;

        const isBetween = moment(currentDate).isBetween(start_date, end_date);

        if (isBetween) {
          currentExercise = e;
        }
      });

      if (!currentExercise && !_.isEmpty(data)) {
        const maxExerciseId = data.length - 1;
        currentExercise = { ...data[maxExerciseId] };
      }

      await dispatch(actions.initCurrentExercise(currentExercise, society_id));
      await dispatch(actions.changeSelectedExercise(currentExercise, society_id));
      await dispatch(actions.getExercisesListSuccess(data, society_id));
    } catch (err) {
      await dispatch(actions.getExercisesListFail());
      await dispatch(handleError(err));
      return err;
    }
  }

  return currentExercise;
}

export const getTrackingSocieties = (opts = {}) => async (dispatch, getState) => {
  const state = getState();


  const {
    sort: sortOpts
  } = opts;

  const trackingSocieties = _.get(state, 'dashboardWeb', []);
  const currentMode = _.get(state, 'dashboardWeb.currentMode', 1);
  const sort = sortOpts !== undefined ? sortOpts : _.get(trackingSocieties, 'sort', '');
  const selectedPeriod = _.get(state, 'dashboardWeb.selectedPeriod');
  const currentDate = moment().format(currentMode === 2 ? 'YYYY-MM' : 'YYYY');

  try {
    const actionOpts = { sort };

    if (!_.isEmpty(sortOpts)) {
      const direction = 'asc';
      const storedColumn = _.get(trackingSocieties, 'sort.column');
      const currentColumn = _.get(sortOpts, 'column');
      if (currentColumn === storedColumn) {
        const storedDirection = _.get(trackingSocieties, 'sort.direction');
        const direction = storedDirection === 'desc' ? 'asc' : 'desc';
        actionOpts.sort = { column: currentColumn, direction };
      } else {
        actionOpts.sort = { column: currentColumn, direction };
      }
    }

    const period = selectedPeriod ? selectedPeriod.period : currentDate;

    const params = {
      mode: currentMode !== undefined ? currentMode : 1,
      sort: actionOpts.sort || { column: 'company_name', direction: 'asc' },
      month: currentMode === 2 ? moment(period).format('YYYYMM') : undefined,
      year: (currentMode === 3 || currentMode === 0) ? moment(period).format('YYYY') : undefined
    };

    await dispatch(actions.getTrackingSocietiesAttempt());
    const response = currentMode !== 3 ? await windev.makeApiCall('/dashboard/collab', 'get', params, {}) : await mock.makeMockCall('/dashboard/collab', 'get', { mode: currentMode }, {});
    const { data } = response;
    if (!selectedPeriod) {
      await dispatch(actions.changeSelectedPeriod({
        period: currentDate,
        label: moment(currentDate).format(currentMode === 2 ? 'MMMM YYYY' : 'YYYY')
      }));
    }
    await dispatch(actions.getTrackingSocietiesSuccess(data, actionOpts));
    return response;
  } catch (err) {
    await dispatch(actions.getTrackingSocietiesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const refreshExerciceDashboard = () => async (dispatch, getState) => {
  const currentDate = moment().format('YYYY-MM-DD');
  let currentExercise;

  try {
    await dispatch(actions.getExercisesListAttempt());
    const society_id = _.get(getState(), 'navigation.id', -2);
    const { data } = await windev.makeApiCall('/exercices', 'get', { society_id });
    data.forEach((e) => {
      const {
        start_date,
        end_date
      } = e;

      const isBetween = moment(currentDate).isBetween(start_date, end_date);

      if (isBetween) {
        currentExercise = e;
      }
    });

    if (!currentExercise && !_.isEmpty(data)) {
      currentExercise = { ...data[0] };
    }

    await dispatch(actions.initCurrentExercise(currentExercise, society_id));
    await dispatch(actions.changeSelectedExercise(currentExercise, society_id));
    await dispatch(actions.getExercisesListSuccess(data, society_id));
    return currentExercise;
  } catch (err) {
    await dispatch(actions.getExercisesListFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getProgressionSociety = (opts = {}) => async (dispatch, getState) => {
  const state = getState();

  const {
    sort: sortOpts
  } = opts;

  const society_id = state.navigation.id;
  const progression = _.get(state, 'dashboardWeb', []);
  const exercisesList = _.get(state, `dashboardWeb.exercisesList.${society_id}`, []);
  const selectedExercise = _.get(state, `dashboardWeb.selectedExercise.${society_id}`);
  const sort = sortOpts !== undefined ? sortOpts : _.get(progression, 'sort', '');
  let currentExercise;

  try {
    const actionOpts = { sort };

    if (!_.isEmpty(sortOpts)) {
      const direction = 'asc';
      const storedColumn = _.get(progression, 'sort.column');
      const currentColumn = _.get(sortOpts, 'column');
      if (currentColumn === storedColumn) {
        const storedDirection = _.get(progression, 'sort.direction');
        const direction = storedDirection === 'desc' ? 'asc' : 'desc';
        actionOpts.sort = { column: currentColumn, direction };
      } else {
        actionOpts.sort = { column: currentColumn, direction };
      }
    }

    currentExercise = await getCurrentExercice(society_id, exercisesList, dispatch);

    const params = {
      exercice_id: !selectedExercise ? _.get(currentExercise, 'id_exercice', '') : _.get(selectedExercise, 'id_exercice', ''),
      sort: actionOpts.sort || { column: 'company_name', direction: 'asc' }
    };

    await dispatch(actions.getProgressionSocietyAttempt());
    const response = await windev.makeApiCall('/dashboard/society/progression', 'get', params, {});
    const { data } = response;
    await dispatch(actions.getProgressionSocietySuccess(data, actionOpts));
    return response;
  } catch (err) {
    await dispatch(actions.getProgressionSocietyFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getMemo = idSociety => async (dispatch) => {
  try {
    await dispatch(actions.getMemoAttempt());
    const response = await windev.makeApiCall('/memo', 'get', { society_id: idSociety }, {});
    const { data } = response;
    await dispatch(actions.getMemoSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.getMemoFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const putMemo = (memo_id, label, done) => async (dispatch) => {
  try {
    await dispatch(actions.putMemoAttempt());
    const response = await windev.makeApiCall('/memo', 'put', { id: memo_id }, { label, done });
    await dispatch(actions.putMemoSuccess({ memo_id, done }));
    return response;
  } catch (err) {
    await dispatch(actions.putMemoFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getCharts = idSociety => async (dispatch, getState) => {
  const state = getState();

  const selectedExercise = _.get(state, `dashboardWeb.selectedExercise.${idSociety}`);
  const exercisesList = _.get(state, `dashboardWeb.exercisesList.${idSociety}`, []);

  const currentExercise = await getCurrentExercice(idSociety, exercisesList, dispatch);

  try {
    const params = {
      society_id: idSociety,
      application_type: 'Web',
      exercice_id: !selectedExercise ? _.get(currentExercise, 'id_exercice', '') : _.get(selectedExercise, 'id_exercice', '')
    };

    await dispatch(actions.getChartsAttempt(idSociety));
    const response = await windev.makeApiCall('/dashboard', 'get', params);
    const data = response.data.cards_list;
    await dispatch(actions.getChartsSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.getChartsFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const postMemo = (payload, idSociety) => async (dispatch) => {
  try {
    const society_id = idSociety;
    await dispatch(actions.postMemoAttempt());
    const response = await windev.makeApiCall('memo', 'post', {}, {
      society_id,
      label: payload,
      done: false
    });
    const { data } = response;
    await dispatch(actions.postMemoSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.postMemoFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const initAccountingFilter = filter => async (dispatch, getState) => {
  const state = getState();

  try {
    const {
      start_date,
      end_date,
      diary_code
    } = filter;

    const society_id = _.get(state, 'navigation.id', false);

    const { data } = await windev.makeApiCall('/diary', 'get', { society_id, code: diary_code });

    const select_diary = {
      ...data[0],
      value: data[0].diary_id,
      label: `${data[0].code} - ${data[0].name}`
    };

    const filter_data = {
      start_date,
      end_date,
      select_diary
    };

    await dispatch(actions.initDataForAccounting(filter_data));
  } catch (err) {
    await dispatch(handleError(err));
  }
};
