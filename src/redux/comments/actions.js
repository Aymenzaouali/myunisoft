import {
  GET_COMMENTS_ATTEMPT,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAIL,

  PUT_ACCOUNT_COMMENTS_ATTEMPT,
  PUT_ACCOUNT_COMMENTS_SUCCESS,
  PUT_ACCOUNT_COMMENTS_FAIL,

  POST_ACCOUNT_COMMENTS_ATTEMPT,
  POST_ACCOUNT_COMMENTS_SUCCESS,
  POST_ACCOUNT_COMMENTS_FAIL
} from './constants';

const getCommentsAttempt = societyId => ({
  type: GET_COMMENTS_ATTEMPT,
  societyId
});

const getCommentsSuccess = (societyId, data) => ({
  type: GET_COMMENTS_SUCCESS,
  societyId,
  data
});

const getCommentsFail = societyId => ({
  type: GET_COMMENTS_FAIL,
  societyId
});

const putAccountCommentAttempt = societyId => ({
  type: PUT_ACCOUNT_COMMENTS_ATTEMPT,
  societyId
});

const putAccountCommentSuccess = (societyId, data) => ({
  type: PUT_ACCOUNT_COMMENTS_SUCCESS,
  societyId,
  data
});

const putAccountCommentFail = societyId => ({
  type: PUT_ACCOUNT_COMMENTS_FAIL,
  societyId
});

const postAccountCommentAttempt = societyId => ({
  type: POST_ACCOUNT_COMMENTS_ATTEMPT,
  societyId
});

const postAccountCommentSuccess = (societyId, data) => ({
  type: POST_ACCOUNT_COMMENTS_SUCCESS,
  societyId,
  data
});

const postAccountCommentFail = societyId => ({
  type: POST_ACCOUNT_COMMENTS_FAIL,
  societyId
});

export default {
  getCommentsAttempt,
  getCommentsSuccess,
  getCommentsFail,

  putAccountCommentAttempt,
  putAccountCommentSuccess,
  putAccountCommentFail,

  postAccountCommentAttempt,
  postAccountCommentSuccess,
  postAccountCommentFail
};
