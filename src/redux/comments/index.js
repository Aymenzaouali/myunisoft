// import { mock } from 'helpers/api'; <--- circ dependency

import * as _ from 'lodash';
import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
// import actions from '../fixedAssets/actions';
import commentsActions from './actions';

export const getTemplateComments = async () => {
  try {
    /**
     * TODO: Import moved here because there's
     * a circular dependency when importing mock
     * on top of the file
     *
     * Need further investigation
     */
    const apiHelper = await import('helpers/api');
    const lang = document.documentElement.getAttribute('lang');
    const { data } = await apiHelper.mock.makeMockCall('/comments/templates', 'get', { lang });
    return data;
  } catch (error) {
    console.error(error);
    return error;
  }
};

export const getComments = (account_id, dossier_revision_id) => async (dispatch, getState) => {
  const state = getState();
  const societyId = _.get(state, 'navigation.id');
  try {
    await dispatch(commentsActions.getCommentsAttempt(societyId));
    const { data } = await windev.makeApiCall('/comments', 'get', { account_id, dossier_revision_id }, {});
    await dispatch(commentsActions.getCommentsSuccess(societyId, data));
    return data;
  } catch (error) {
    await dispatch(commentsActions.getCommentsFail(societyId));
    await dispatch(handleError(error));
    return error;
  }
};

export const postAccountsComment = (comment, params) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');
  try {
    await dispatch(commentsActions.postAccountCommentAttempt(society_id));

    const {
      body,
      attachTo
    } = comment;

    const { data } = await windev.makeApiCall(
      '/comments',
      'post',
      {},
      {
        location: 'ACCOUNT',
        ...params,
        society_id,
        comment: body,
        attachTo
      }
    );
    await dispatch(commentsActions.postAccountCommentSuccess({
      society_id,
      comment: data
    }));
    return data;
  } catch (error) {
    await dispatch(commentsActions.postAccountCommentFail(society_id));
    return error;
  }
};

export const putAccountsComment = (comment, params) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');
  try {
    await dispatch(commentsActions.putAccountCommentAttempt(society_id));

    const {
      body,
      id,
      attachTo
    } = comment;

    const { data } = await windev.makeApiCall(
      `/comments/${id}`,
      'put',
      {},
      {
        location: 'ACCOUNT',
        ...params,
        society_id,
        comment: body,
        attachTo
      }
    );
    await dispatch(commentsActions.putAccountCommentSuccess({
      society_id,
      comment: data
    }));
    return data;
  } catch (error) {
    await dispatch(commentsActions.putAccountCommentFail(society_id));
    return error;
  }
};
