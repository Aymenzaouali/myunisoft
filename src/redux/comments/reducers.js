import {
  GET_COMMENTS_ATTEMPT,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAIL,

  PUT_ACCOUNT_COMMENTS_ATTEMPT,
  PUT_ACCOUNT_COMMENTS_SUCCESS,
  PUT_ACCOUNT_COMMENTS_FAIL,

  POST_ACCOUNT_COMMENTS_ATTEMPT,
  POST_ACCOUNT_COMMENTS_SUCCESS,
  POST_ACCOUNT_COMMENTS_FAIL
} from './constants';

const initialState = {};

const comments = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_COMMENTS_ATTEMPT:
      return {
        ...state,
        [action.societyId]: {
          isLoading: true
        }
      };
    case GET_COMMENTS_SUCCESS:
      return {
        ...state,
        [action.societyId]: {
          comment_info: action.data,
          isLoading: false
        }
      };
    case GET_COMMENTS_FAIL:
      return {
        ...state,
        [action.societyId]: {
          isLoading: false
        }
      };

    case PUT_ACCOUNT_COMMENTS_ATTEMPT:
      return {
        ...state
      };
    case PUT_ACCOUNT_COMMENTS_SUCCESS:
      return {
        ...state
      };
    case PUT_ACCOUNT_COMMENTS_FAIL:
      return {
        ...state
      };

    case POST_ACCOUNT_COMMENTS_ATTEMPT:
      return {
        ...state
      };
    case POST_ACCOUNT_COMMENTS_SUCCESS:
      return {
        ...state
      };
    case POST_ACCOUNT_COMMENTS_FAIL:
      return {
        ...state
      };

    default: return state;
    }
  }
  return state;
};

export default comments;
