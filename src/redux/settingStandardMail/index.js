import { windev } from 'helpers/api';
import _ from 'lodash';
import { PARAGRAPHS_TYPES_TABLE, PARAGRAPH_TABLE, getTableName } from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import actions from './actions';

export class SettingStandardMailService {
  static instance;

  static getInstance() {
    if (!SettingStandardMailService.instance) {
      SettingStandardMailService.instance = new SettingStandardMailService();
    }
    return SettingStandardMailService.instance;
  }

  geParagraphsTypes = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, PARAGRAPHS_TYPES_TABLE);
    try {
      await dispatch(actions.getParagraphsTypesAttempt(society_id));
      const response = await windev.makeApiCall('/letter/paragraph_type', 'get');
      const statements = response.data ? response.data : [];
      await dispatch(actions.getParagraphsTypesSuccess(statements, society_id));
      await dispatch(tableActions.setData(tableName, statements));
      return statements;
    } catch (err) {
      await dispatch(actions.getParagraphsTypesFail(society_id));
      return err;
    }
  };

  sendParagraphsTypes = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const {
      name,
      label_as_object
    } = state.form.settingStandardMailForm.values;
    try {
      await dispatch(actions.sendParagraphsTypesAttempt(society_id));
      const response = await windev.makeApiCall('letter/paragraph_type', 'post', {}, {
        name,
        label_as_object
      });
      const status = response.data ? response.data : [];
      await dispatch(actions.sendParagraphsTypesSuccess(status, society_id));
      return status;
    } catch (err) {
      await dispatch(actions.sendParagraphsTypesFail(society_id));
      return err;
    }
  };

  modifyParagraphsTypes = (id, type) => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const {
      name,
      label_as_object,
      id_base_paragraph_type
    } = state.form.settingStandardMailForm.values;
    try {
      await dispatch(actions.modifyParagraphsTypesAttempt(society_id));
      const response = await windev.makeApiCall('/letter/paragraph_type', 'put', {}, {
        name,
        id_base_paragraph_type,
        label_as_object,
        id_paragraph_type: id,
        type
      });
      const status = response.data ? response.data : [];
      await dispatch(actions.modifyParagraphsTypesSuccess(status, society_id));
      return status;
    } catch (err) {
      await dispatch(actions.modifyParagraphsTypesFail(society_id));
      return err;
    }
  };

  deleteParagraphsTypes = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, PARAGRAPHS_TYPES_TABLE);
    const selectedTypes = _.get(state, `tables.${tableName}.data`, []);
    const selectedRows = _.get(state, `tables.${tableName}.selectedRows`, {});
    const selected = selectedTypes
      .filter((type, index) => selectedRows[index + 1])
      .map(type => ({ id_paragraph_type: type.id_paragraph_type }));
    try {
      await dispatch(actions.deleteParagraphsTypesAttempt(society_id));
      const response = await windev.makeApiCall('/letter/paragraph_type', 'delete', {},
        {
          paragraph_type: selected
        });
      const status = response.data ? response.data : [];
      await dispatch(actions.deleteParagraphsTypesSuccess(status, society_id));
      await dispatch(this.geParagraphsTypes());
      return status;
    } catch (err) {
      await dispatch(actions.deleteParagraphsTypesFail(society_id));
      return err;
    }
  };

  getParagraphs = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, PARAGRAPH_TABLE);
    try {
      await dispatch(actions.getParagraphsAttempt(society_id));
      const response = await windev.makeApiCall('/letter/paragraph', 'get');
      const statements = response.data ? response.data : [];
      await dispatch(actions.getParagraphsSuccess(statements, society_id));
      await dispatch(actions.setParagraphsVisible(true));
      await dispatch(tableActions.setData(tableName, statements));
      return statements;
    } catch (err) {
      await dispatch(actions.getParagraphsFail(society_id));
      return err;
    }
  };

  sendParagraph = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const {
      label,
      paragraph_text
    } = state.form.mailParagraphDialogForm.values;
    try {
      await dispatch(actions.sendParagraphAttempt(society_id));
      const response = await windev.makeApiCall('/letter/paragraph', 'post', {}, {
        label,
        paragraph_text,
        ordre: 1
      });
      const status = response.data ? response.data : [];
      await dispatch(actions.sendParagraphSuccess(status, society_id));
      return status;
    } catch (err) {
      await dispatch(actions.sendParagraphFail(society_id));
      return err;
    }
  };

  modifyParagraph = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const {
      label,
      paragraph_text
    } = state.form.mailParagraphDialogForm.values;

    const tableName = getTableName(society_id, PARAGRAPH_TABLE);
    const selectedTypes = _.get(state, `tables.${tableName}.data`, []);
    const lastSelectedRow = _.get(state, `tables.${tableName}.lastSelectedRow`, null);
    const {
      id_paragraph,
      type,
      id_global_paragraph_type,
      id_local_paragraph_type,
      ordre
    } = _.filter(selectedTypes, type => type.unique_id === lastSelectedRow)[0];

    try {
      await dispatch(actions.modifyParagraphAttempt(society_id));
      const response = await windev.makeApiCall('/letter/paragraph', 'put', {}, {
        label,
        paragraph_text,
        ordre,
        id_paragraph,
        type,
        id_global_paragraph_type,
        id_local_paragraph_type
      });
      const status = response.data ? response.data : [];
      await dispatch(actions.modifyParagraphSuccess(status, society_id));
      return status;
    } catch (err) {
      await dispatch(actions.modifyParagraphFail(society_id));
      return err;
    }
  };

  deleteParagraph = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, PARAGRAPH_TABLE);
    const selectedTypes = _.get(state, `tables.${tableName}.data`, []);
    const selectedRows = _.get(state, `tables.${tableName}.selectedRows`, null);
    const selected = selectedTypes
      .filter((type, index) => selectedRows[index + 1])
      .map(type => ({ id_paragraph: type.id_paragraph }));
    try {
      await dispatch(actions.deleteParagraphAttempt(society_id));
      const response = await windev.makeApiCall('/letter/paragraph', 'delete', {}, {
        paragraph: selected
      });
      const status = response.data ? response.data : [];
      await dispatch(actions.deleteParagraphSuccess(status, society_id));
      await dispatch(this.getParagraphs());
      return status;
    } catch (err) {
      await dispatch(actions.deleteParagraphFail(society_id));
      return err;
    }
  };
}


export default SettingStandardMailService.getInstance();
