import {
  GET_PARAGRAPHS_TYPES_ATTEMPT,
  GET_PARAGRAPHS_TYPES__SUCCESS,
  GET_PARAGRAPHS_TYPES__FAIL,
  GET_PARAGRAPHS_ATTEMPT,
  GET_PARAGRAPHS_SUCCESS,
  GET_PARAGRAPHS_FAIL,
  SET_PARAGRAPH_VISIBILITY
} from './constants';

const initialState = {
  paragraphs_types: {},
  isParagraphVisible: false
};

const settingStandardMail = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_PARAGRAPHS_TYPES_ATTEMPT:
      return {
        ...state,
        paragraphs_types: {
          [action.society_id]: {
            isLoading: true,
            isError: false,
            paragraphs_types_list: []
          }
        }
      };
    case GET_PARAGRAPHS_TYPES__SUCCESS:
      return {
        ...state,
        paragraphs_types: {
          [action.society_id]: {
            ...state.paragraphs_types[action.society_id],
            isLoading: false,
            isError: false,
            paragraphs_types_list: action.paragraphs
          }
        }
      };
    case GET_PARAGRAPHS_TYPES__FAIL:
      return {
        ...state,
        paragraphs_types: {
          [action.society_id]: {
            paragraphs_types_list: [],
            isLoading: false,
            isError: true
          }
        }
      };
    case GET_PARAGRAPHS_ATTEMPT:
      return {
        ...state,
        paragraphs: {
          [action.society_id]: {
            isLoading: true,
            isError: false,
            paragraphs_list: []
          }
        }
      };
    case GET_PARAGRAPHS_SUCCESS:
      return {
        ...state,
        paragraphs: {
          [action.society_id]: {
            ...state.paragraphs[action.society_id],
            isLoading: false,
            isError: false,
            paragraphs_list: action.paragraphs
          }
        }
      };
    case GET_PARAGRAPHS_FAIL:
      return {
        ...state,
        paragraphs: {
          [action.society_id]: {
            ...state.paragraphs[action.society_id],
            isLoading: false,
            isError: true
          }
        }
      };
    case SET_PARAGRAPH_VISIBILITY:
      return {
        ...state,
        isParagraphVisible: action.status
      };
    default: return state;
    }
  }
  return state;
};

export default settingStandardMail;
