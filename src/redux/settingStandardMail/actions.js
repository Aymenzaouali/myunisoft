import {
  GET_PARAGRAPHS_TYPES_ATTEMPT,
  GET_PARAGRAPHS_TYPES__SUCCESS,
  GET_PARAGRAPHS_TYPES__FAIL,
  GET_PARAGRAPHS_ATTEMPT,
  GET_PARAGRAPHS_SUCCESS,
  GET_PARAGRAPHS_FAIL,
  SEND_PARAGRAPHS__TYPES_ATTEMPT,
  SEND_PARAGRAPHS__TYPES_SUCCESS,
  SEND_PARAGRAPHS__TYPES_FAIL,
  MODIFY_PARAGRAPHS__TYPES_ATTEMPT,
  MODIFY_PARAGRAPHS__TYPES_SUCCESS,
  MODIFY_PARAGRAPHS__TYPES_FAIL,
  DELETE_PARAGRAPHS__TYPES_ATTEMPT,
  DELETE_PARAGRAPHS__TYPES_SUCCESS,
  DELETE_PARAGRAPHS__TYPES_FAIL,
  DELETE_PARAGRAPH_ATTEMPT,
  DELETE_PARAGRAPH_SUCCESS,
  DELETE_PARAGRAPH_FAIL,
  SEND_PARAGRAPH_ATTEMPT,
  SEND_PARAGRAPH_SUCCESS,
  SEND_PARAGRAPH_FAIL,
  MODIFY_PARAGRAPH_ATTEMPT,
  MODIFY_PARAGRAPH_SUCCESS,
  MODIFY_PARAGRAPH_FAIL,
  SET_PARAGRAPH_VISIBILITY
} from './constants';

const getParagraphsTypesAttempt = society_id => ({
  type: GET_PARAGRAPHS_TYPES_ATTEMPT,
  society_id
});

const getParagraphsTypesSuccess = (paragraphs, society_id) => ({
  type: GET_PARAGRAPHS_TYPES__SUCCESS,
  paragraphs,
  society_id
});

const getParagraphsTypesFail = society_id => ({
  type: GET_PARAGRAPHS_TYPES__FAIL,
  society_id
});

const getParagraphsAttempt = society_id => ({
  type: GET_PARAGRAPHS_ATTEMPT,
  society_id
});

const getParagraphsSuccess = (paragraphs, society_id) => ({
  type: GET_PARAGRAPHS_SUCCESS,
  paragraphs,
  society_id
});

const getParagraphsFail = society_id => ({
  type: GET_PARAGRAPHS_FAIL,
  society_id
});

const sendParagraphsTypesAttempt = society_id => ({
  type: SEND_PARAGRAPHS__TYPES_ATTEMPT,
  society_id
});

const sendParagraphsTypesSuccess = (status, society_id) => ({
  type: SEND_PARAGRAPHS__TYPES_SUCCESS,
  status,
  society_id
});

const sendParagraphsTypesFail = society_id => ({
  type: SEND_PARAGRAPHS__TYPES_FAIL,
  society_id
});

const sendParagraphAttempt = society_id => ({
  type: SEND_PARAGRAPH_ATTEMPT,
  society_id
});

const sendParagraphSuccess = (status, society_id) => ({
  type: SEND_PARAGRAPH_SUCCESS,
  status,
  society_id
});

const sendParagraphFail = society_id => ({
  type: SEND_PARAGRAPH_FAIL,
  society_id
});

const modifyParagraphsTypesAttempt = society_id => ({
  type: MODIFY_PARAGRAPHS__TYPES_ATTEMPT,
  society_id
});

const modifyParagraphsTypesSuccess = (status, society_id) => ({
  type: MODIFY_PARAGRAPHS__TYPES_SUCCESS,
  status,
  society_id
});

const modifyParagraphsTypesFail = society_id => ({
  type: MODIFY_PARAGRAPHS__TYPES_FAIL,
  society_id
});

const modifyParagraphAttempt = society_id => ({
  type: MODIFY_PARAGRAPH_ATTEMPT,
  society_id
});

const modifyParagraphSuccess = (status, society_id) => ({
  type: MODIFY_PARAGRAPH_SUCCESS,
  status,
  society_id
});

const modifyParagraphFail = society_id => ({
  type: MODIFY_PARAGRAPH_FAIL,
  society_id
});

const deleteParagraphsTypesAttempt = society_id => ({
  type: DELETE_PARAGRAPHS__TYPES_ATTEMPT,
  society_id
});

const deleteParagraphsTypesSuccess = (status, society_id) => ({
  type: DELETE_PARAGRAPHS__TYPES_SUCCESS,
  status,
  society_id
});

const deleteParagraphsTypesFail = society_id => ({
  type: DELETE_PARAGRAPHS__TYPES_FAIL,
  society_id
});

const deleteParagraphAttempt = society_id => ({
  type: DELETE_PARAGRAPH_ATTEMPT,
  society_id
});

const deleteParagraphSuccess = (status, society_id) => ({
  type: DELETE_PARAGRAPH_SUCCESS,
  status,
  society_id
});

const deleteParagraphFail = society_id => ({
  type: DELETE_PARAGRAPH_FAIL,
  society_id
});

const setParagraphsVisible = status => ({
  type: SET_PARAGRAPH_VISIBILITY,
  status
});

export default {
  getParagraphsTypesAttempt,
  getParagraphsTypesSuccess,
  getParagraphsTypesFail,
  getParagraphsAttempt,
  getParagraphsSuccess,
  getParagraphsFail,
  sendParagraphsTypesAttempt,
  sendParagraphsTypesSuccess,
  sendParagraphsTypesFail,
  modifyParagraphsTypesAttempt,
  modifyParagraphsTypesSuccess,
  modifyParagraphsTypesFail,
  deleteParagraphsTypesAttempt,
  deleteParagraphsTypesSuccess,
  deleteParagraphsTypesFail,
  deleteParagraphAttempt,
  deleteParagraphSuccess,
  deleteParagraphFail,
  sendParagraphAttempt,
  sendParagraphSuccess,
  sendParagraphFail,
  modifyParagraphAttempt,
  modifyParagraphSuccess,
  modifyParagraphFail,
  setParagraphsVisible
};
