import {
  GET_WATERMARKS_ATTEMPT, GET_WATERMARKS_SUCCESS, GET_WATERMARKS_FAILURE
} from './constants';

// Actions
const getWatermarksAttempt = () => ({ type: GET_WATERMARKS_ATTEMPT });
const getWatermarksSuccess = watermarks => ({ type: GET_WATERMARKS_SUCCESS, watermarks });
const getWatermarksFailure = () => ({ type: GET_WATERMARKS_FAILURE });

export {
  getWatermarksAttempt,
  getWatermarksSuccess,
  getWatermarksFailure
};
