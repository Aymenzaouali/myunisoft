import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';

import {
  getWatermarksAttempt, getWatermarksSuccess, getWatermarksFailure
} from './actions';

// Thunks
const getWatermarks = () => async (dispatch) => {
  try {
    await dispatch(getWatermarksAttempt());
    const { data: watermarks } = await windev.makeApiCall('/watermarks', 'get');
    await dispatch(getWatermarksSuccess(watermarks));

    return null;
  } catch (err) {
    await dispatch(getWatermarksFailure());
    await dispatch(handleError(err));

    return err;
  }
};

export {
  getWatermarks
};
