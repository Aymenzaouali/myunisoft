import {
  GET_WATERMARKS_ATTEMPT, GET_WATERMARKS_SUCCESS, GET_WATERMARKS_FAILURE
} from './constants';

// Initial state
const initial = {
  isLoading: false,
  isError: false
};

// Reducer
const filigraneReducer = (state = initial, action) => {
  switch (action.type) {
  case GET_WATERMARKS_ATTEMPT:
    return {
      ...state,

      isLoading: true,
      isError: false
    };

  case GET_WATERMARKS_SUCCESS:
    return {
      ...state,

      watermarks: action.watermarks,
      isLoading: false,
      isError: false
    };

  case GET_WATERMARKS_FAILURE:
    return {
      ...state,
      isLoading: false,
      isError: true
    };

  default:
  }

  return state;
};

export default filigraneReducer;
