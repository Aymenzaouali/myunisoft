import {
  OPEN_SUCCESS_DIALOG,
  CLOSE_SUCCESS_DIALOG
} from './constants';

export const openSuccessDialog = () => ({
  type: OPEN_SUCCESS_DIALOG,
  payload: true
});

export const closeSuccessDialog = () => ({
  type: CLOSE_SUCCESS_DIALOG,
  payload: false
});
