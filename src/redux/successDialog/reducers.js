import {
  CLOSING_YEAR_SUCCESS
} from 'redux/tabs/closing/constants';

import {
  GENERATE_SUCCESS
} from 'redux/workingPage/constants';

import {
  OPEN_SUCCESS_DIALOG,
  CLOSE_SUCCESS_DIALOG
} from './constants';

const defaultState = {
  isOpen: false
};

const successDialog = (state = defaultState, action) => {
  switch (action.type) {
  case OPEN_SUCCESS_DIALOG:
  case CLOSING_YEAR_SUCCESS:
  case GENERATE_SUCCESS:
    return {
      ...state,
      isOpen: action.payload || true
    };
  case CLOSE_SUCCESS_DIALOG:
    return {
      ...state,
      isOpen: action.payload || false
    };
  default:
    return state;
  }
};

export default successDialog;
