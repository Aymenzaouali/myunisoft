import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';
import {
  ACCOUNTING_FIRM_TOP_MAIN_TABLE,
  getTableName
} from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';
import _ from 'lodash';
import actions from './actions';

export class AccountingFirmSettingsService {
  static instance;

  static getInstance() {
    if (!AccountingFirmSettingsService.instance) {
      AccountingFirmSettingsService.instance = new AccountingFirmSettingsService();
    }
    return AccountingFirmSettingsService.instance;
  }

  setActiveTab = tabId => async (dispatch) => {
    try {
      await dispatch(actions.setActiveTab(tabId));
    } catch (err) {
      await dispatch(handleError(err));
      throw err;
    }
  };

  setAccountFirmDirection = directions => async (dispatch) => {
    try {
      await dispatch(actions.setDirections(directions));
    } catch (err) {
      await dispatch(handleError(err));
      throw err;
    }
  };

  setCreateNewStatus = isCreate => async (dispatch) => {
    try {
      await dispatch(actions.setCreateNewStatus(isCreate));
      await dispatch(this.setActiveTab(0));
    } catch (err) {
      await dispatch(handleError(err));
      throw err;
    }
  };

  getMembers = (order = {}) => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, ACCOUNTING_FIRM_TOP_MAIN_TABLE);
    const paginations = {
      limit: 1000000,
      offset: 0,
      page: 0,
      mode: 1
    };
    const sorting = {
      ...(order.siret && {
        column: 'siret',
        direction: order.siret
      }),
      ...(order.nameOfFirm && {
        column: 'name',
        direction: order.nameOfFirm
      }),
      ...(order.city && {
        column: 'city',
        direction: order.city
      }),
      ...(order.postalCode && {
        column: 'postal_code',
        direction: order.postalCode
      })
    };
    try {
      await dispatch(actions.getMembersAttempt(society_id));
      const response = await windev.makeApiCall('/member', 'get',
        {
          ...paginations,
          ...(!_.isEmpty(sorting) && { sort: { ...sorting } })
        });
      const member_data = response.data ? response.data : [];
      const members = member_data.array_data || [];
      const members_pages_number = member_data.pages_number || [];
      const members_rows_number = member_data.rows_number || [];
      await dispatch(
        actions.getMembersSuccess(members, members_pages_number, members_rows_number, society_id)
      );
      await dispatch(tableActions.setData(tableName, members));
      return members;
    } catch (err) {
      await dispatch(actions.getMembersFail(society_id));
      await dispatch(handleError(err));
      return err;
    }
  };

  sendMember = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const accountingFirmForm = _.get(state, `form.accountingFirmCreationForm_${society_id}.values`, {});
    const {
      siret,
      cabinetName,
      address_number,
      address_bis,
      road_type,
      street_name,
      addressSupplement,
      postalCode,
      city,
      email,
      phone_number,
      fax,
      site,
      loginSilae,
      loginWsSilae = null,
      passwordSilaeConfirmed = null,
      addressWsSilae,
      passwordSilaeConfirm
    } = accountingFirmForm;

    const params = {
      name: cabinetName || null,
      address: street_name || null,
      postal_code: postalCode || null,
      city: city && city.value,
      website: site || null,
      phone_number: phone_number || null,
      email: email || null,
      fax: fax || null,
      login_silae: loginSilae || null,
      login_ws_silae: loginWsSilae,
      pw_silae: passwordSilaeConfirm || null,
      pw_ws_silae: passwordSilaeConfirmed || null,
      street_number: address_number || null, // №
      repetition_index: address_bis || null,
      address_complement: addressSupplement || null, // Complement adresse
      siret: siret || null,
      id_type_voie: _.get(road_type, 'id', null),
      adresse_ws_silae: addressWsSilae || null
    };
    try {
      await dispatch(actions.setMembersAttempt(society_id));
      const response = await windev.makeApiCall('/member', 'post', {}, params);
      const members = response.data ? response.data : [];
      await dispatch(actions.setMembersSuccess(members, society_id));
      return members;
    } catch (err) {
      await dispatch(actions.setMembersFail(society_id));
      await dispatch(handleError(err));
      return err;
    }
  };

  changeMember = member_id => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const accountingFirmForm = _.get(state, `form.accountingFirmCreationForm_${society_id}.values`, {});

    const {
      siret,
      cabinetName,
      address_number,
      address_bis,
      road_type,
      street_name, // Nom de la voie
      addressSupplement,
      postalCode,
      city,
      email,
      phone_number,
      fax,
      site,
      loginSilae,
      loginWsSilae = null,
      passwordSilaeConfirmed = null,
      addressWsSilae,
      passwordSilaeConfirm
    } = accountingFirmForm;

    const params = {
      name: cabinetName,
      address: street_name,
      postal_code: postalCode,
      city: city && city.value,
      website: site,
      phone_number,
      email: email || null,
      fax: fax || null,
      login_silae: loginSilae || null,
      login_ws_silae: loginWsSilae,
      ...(passwordSilaeConfirm && !passwordSilaeConfirm.includes('********') && { pw_silae: passwordSilaeConfirm }), // TODO can user edit this?
      ...(passwordSilaeConfirmed && !passwordSilaeConfirmed.includes('********') && { pw_ws_silae: passwordSilaeConfirmed }),
      street_number: address_number, // №
      repetition_index: address_bis,
      address_complement: addressSupplement,
      siret,
      id_type_voie: _.get(road_type, 'id', undefined),
      adresse_ws_silae: addressWsSilae
    };

    try {
      await dispatch(actions.changeMembersAttempt(society_id));
      const response = await windev.makeApiCall('/member', 'put', { member_id }, params);
      const statements = response.data ? response.data : [];
      await dispatch(actions.changeMembersSuccess(statements, society_id));
      return statements;
    } catch (err) {
      await dispatch(actions.changeMembersFail(society_id));
      await dispatch(handleError(err));
      return err;
    }
  };

  sendRBAccount = (member_id, memberIndex) => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const {
      mail,
      active,
      password
    } = state.form.accountingFirmSettingsDeclareForm.values;
    const params = {
      mail,
      enabled: active,
      password
    };
    try {
      await dispatch(actions.sendRBAccountAttempt(society_id));
      const response = await windev.makeApiCall('/rb_account', 'post', {}, params);
      const statements = response.data ? response.data : [];
      const rb = _.get(response, 'data[0]', null);
      await dispatch(actions.sendRBAccountSuccess(society_id, rb, memberIndex));
      await dispatch(this.updateDataMid());
      return statements;
    } catch (err) {
      await dispatch(actions.sendRBAccountFail(society_id));
      await dispatch(handleError(err));
      return err;
    }
  };

  updateDataMid = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const tableName = getTableName(society_id, ACCOUNTING_FIRM_TOP_MAIN_TABLE);
    const members = await _.get(state, 'accountingFirmSettings.members.members_list', []);
    await dispatch(tableActions.updateData(tableName, members));
  };

  modifyRBAccount = (account_id, memberIndex, rbIndex) => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const {
      password,
      mail,
      active
    } = state.form.accountingFirmSettingsDeclareForm.values;
    const params = {
      enabled: active,
      mail,
      account_id,
      password
    };
    try {
      await dispatch(actions.modifyRBAccountAttempt(society_id));
      const response = await windev.makeApiCall('/rb_account', 'put', {
        id: account_id
      }, params);
      const rb = _.get(response, 'data', null);
      await dispatch(actions.modifyRBAccountSuccess(society_id, rb, memberIndex, rbIndex));
      await dispatch(this.updateDataMid());
      return rb;
    } catch (err) {
      await dispatch(actions.modifyRBAccountFail(society_id));
      await dispatch(handleError(err));
      return err;
    }
  };

  sendEDIAccount = (member_id, memberIndex) => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const {
      mail,
      active,
      password
    } = state.form.accountingFirmSettingsDeclareForm.values;
    const params = {
      enabled: active,
      member_id,
      mail,
      password
    };
    try {
      await dispatch(actions.sendEDIAccountAttempt(society_id));
      const response = await windev.makeApiCall('/edi_account', 'post', {}, params);
      const edi = _.get(response, 'data[0]', null);
      await dispatch(actions.sendEDIAccountSuccess(society_id, edi, memberIndex));
      await dispatch(this.updateDataMid());
      return edi;
    } catch (err) {
      await dispatch(actions.sendEDIAccountFail(society_id));
      await dispatch(handleError(err));
      return err;
    }
  };

  modifyEDIAccount = (account_id, memberIndex, ediIndex) => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const {
      password,
      mail,
      active
    } = state.form.accountingFirmSettingsDeclareForm.values;
    const params = {
      enabled: active,
      mail,
      password
    };
    try {
      await dispatch(actions.modifyEDIAccountAttempt(society_id));
      const response = await windev.makeApiCall('/edi_account', 'put', {
        id: account_id
      }, params);
      const edi = _.get(response, 'data', null);
      await dispatch(actions.modifyEDIAccountSuccess(society_id, edi, memberIndex, ediIndex));
      await dispatch(this.updateDataMid());
      return edi;
    } catch (err) {
      await dispatch(actions.modifyEDIAccountFail(society_id));
      await dispatch(handleError(err));
      return err;
    }
  };

  setItemsPerPage = items_qty => async (dispatch) => {
    dispatch(actions.setItemsPerPage(items_qty));
  };

  setCurrentPage = page => async (dispatch) => {
    dispatch(actions.setCurrentPage(page));
  }
}


export default AccountingFirmSettingsService.getInstance();
