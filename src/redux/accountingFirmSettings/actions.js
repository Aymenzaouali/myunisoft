import {
  SET_ACTIVE_TAB,
  GET_MEMBERS_ATTEMPT,
  GET_MEMBERS_SUCCESS,
  GET_MEMBERS_FAIL,
  SET_MEMBERS_ATTEMPT,
  SET_MEMBERS_SUCCESS,
  SET_MEMBERS_FAIL,
  CHANGE_MEMBERS_ATTEMPT,
  CHANGE_MEMBERS_SUCCESS,
  CHANGE_MEMBERS_FAIL,
  SET_CREATE_NEW_STATUS,
  SEND_RB_ACCOUNT_ATTEMPT,
  SEND_RB_ACCOUNT_SUCCESS,
  SEND_RB_ACCOUNT_FAIL,
  MODIFY_RB_ACCOUNT_ATTEMPT,
  MODIFY_RB_ACCOUNT_SUCCESS,
  MODIFY_RB_ACCOUNT_FAIL,
  DELETE_RB_ACCOUNT_ATTEMPT,
  DELETE_RB_ACCOUNT_SUCCESS,
  DELETE_RB_ACCOUNT_FAIL,
  SEND_EDI_ACCOUNT_ATTEMPT,
  SEND_EDI_ACCOUNT_SUCCESS,
  SEND_EDI_ACCOUNT_FAIL,
  MODIFY_EDI_ACCOUNT_ATTEMPT,
  MODIFY_EDI_ACCOUNT_SUCCESS,
  MODIFY_EDI_ACCOUNT_FAIL,
  DELETE_EDI_ACCOUNT_ATTEMPT,
  DELETE_EDI_ACCOUNT_SUCCESS,
  DELETE_EDI_ACCOUNT_FAIL,
  SET_ITEMS_PER_PAGE,
  SET_CURRENT_PAGE,
  SET_DIRECTIONS
} from './constants';

const setActiveTab = activeTabId => ({
  type: SET_ACTIVE_TAB,
  activeTabId
});

const getMembersAttempt = society_id => ({
  type: GET_MEMBERS_ATTEMPT,
  society_id
});

const getMembersSuccess = (members, members_pages_number, members_rows_number, society_id) => ({
  type: GET_MEMBERS_SUCCESS,
  society_id,
  members_pages_number,
  members_rows_number,
  members
});

const getMembersFail = society_id => ({
  type: GET_MEMBERS_FAIL,
  society_id
});

const sendRBAccountAttempt = society_id => ({
  type: SEND_RB_ACCOUNT_ATTEMPT,
  society_id
});

const sendRBAccountSuccess = (society_id, rb, memberIndex) => ({
  type: SEND_RB_ACCOUNT_SUCCESS,
  society_id,
  rb,
  memberIndex
});

const sendRBAccountFail = society_id => ({
  type: SEND_RB_ACCOUNT_FAIL,
  society_id
});

const modifyRBAccountAttempt = society_id => ({
  type: MODIFY_RB_ACCOUNT_ATTEMPT,
  society_id
});

const modifyRBAccountSuccess = (society_id, rb, memberIndex, rbIndex) => ({
  type: MODIFY_RB_ACCOUNT_SUCCESS,
  society_id,
  rb,
  memberIndex,
  rbIndex
});

const modifyRBAccountFail = society_id => ({
  type: MODIFY_RB_ACCOUNT_FAIL,
  society_id
});

const deleteRBAccountAttempt = society_id => ({
  type: DELETE_RB_ACCOUNT_ATTEMPT,
  society_id
});

const deleteRBAccountSuccess = society_id => ({
  type: DELETE_RB_ACCOUNT_SUCCESS,
  society_id
});

const deleteRBAccountFail = society_id => ({
  type: DELETE_RB_ACCOUNT_FAIL,
  society_id
});

const sendEDIAccountAttempt = society_id => ({
  type: SEND_EDI_ACCOUNT_ATTEMPT,
  society_id
});

const sendEDIAccountSuccess = (society_id, edi, memberIndex) => ({
  type: SEND_EDI_ACCOUNT_SUCCESS,
  society_id,
  edi,
  memberIndex
});

const sendEDIAccountFail = (society_id, edi, memberIndex, ediIndex) => ({
  type: SEND_EDI_ACCOUNT_FAIL,
  society_id,
  edi,
  memberIndex,
  ediIndex
});

const modifyEDIAccountAttempt = society_id => ({
  type: MODIFY_EDI_ACCOUNT_ATTEMPT,
  society_id
});

const modifyEDIAccountSuccess = (society_id, edi, memberIndex, ediIndex) => ({
  type: MODIFY_EDI_ACCOUNT_SUCCESS,
  society_id,
  edi,
  memberIndex,
  ediIndex
});

const modifyEDIAccountFail = society_id => ({
  type: MODIFY_EDI_ACCOUNT_FAIL,
  society_id
});

const deleteEDIAccountAttempt = society_id => ({
  type: DELETE_EDI_ACCOUNT_ATTEMPT,
  society_id
});

const deleteEDIAccountSuccess = society_id => ({
  type: DELETE_EDI_ACCOUNT_SUCCESS,
  society_id
});

const deleteEDIAccountFail = society_id => ({
  type: DELETE_EDI_ACCOUNT_FAIL,
  society_id
});

const setMembersAttempt = society_id => ({
  type: SET_MEMBERS_ATTEMPT,
  society_id
});

const setMembersSuccess = (members, society_id) => ({
  type: SET_MEMBERS_SUCCESS,
  society_id,
  members
});

const setMembersFail = society_id => ({
  type: SET_MEMBERS_FAIL,
  society_id
});

const changeMembersAttempt = society_id => ({
  type: CHANGE_MEMBERS_ATTEMPT,
  society_id
});

const changeMembersSuccess = (members, society_id) => ({
  type: CHANGE_MEMBERS_SUCCESS,
  society_id,
  members
});

const changeMembersFail = society_id => ({
  type: CHANGE_MEMBERS_FAIL,
  society_id
});

const setCreateNewStatus = isCreateNew => ({
  type: SET_CREATE_NEW_STATUS,
  isCreateNew
});

const setItemsPerPage = items_qty => ({
  type: SET_ITEMS_PER_PAGE,
  items_qty
});

const setCurrentPage = page_number => ({
  type: SET_CURRENT_PAGE,
  page_number
});

const setDirections = directions => ({
  type: SET_DIRECTIONS,
  directions
});

export default {
  setActiveTab,
  getMembersAttempt,
  getMembersSuccess,
  getMembersFail,
  setMembersAttempt,
  setMembersSuccess,
  setMembersFail,
  changeMembersAttempt,
  changeMembersSuccess,
  changeMembersFail,
  setCreateNewStatus,
  sendRBAccountAttempt,
  sendRBAccountSuccess,
  sendRBAccountFail,
  modifyRBAccountAttempt,
  modifyRBAccountSuccess,
  modifyRBAccountFail,
  deleteRBAccountAttempt,
  deleteRBAccountSuccess,
  deleteRBAccountFail,
  sendEDIAccountAttempt,
  sendEDIAccountSuccess,
  sendEDIAccountFail,
  modifyEDIAccountAttempt,
  modifyEDIAccountSuccess,
  modifyEDIAccountFail,
  deleteEDIAccountAttempt,
  deleteEDIAccountSuccess,
  deleteEDIAccountFail,
  setItemsPerPage,
  setCurrentPage,
  setDirections
};
