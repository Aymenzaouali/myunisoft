import {
  GET_MEMBERS_ATTEMPT,
  GET_MEMBERS_SUCCESS,
  GET_MEMBERS_FAIL,
  SET_ACTIVE_TAB,
  SET_CREATE_NEW_STATUS,
  SET_ITEMS_PER_PAGE,
  SET_CURRENT_PAGE,
  SEND_EDI_ACCOUNT_SUCCESS,
  MODIFY_EDI_ACCOUNT_SUCCESS,
  SET_DIRECTIONS,
  MODIFY_RB_ACCOUNT_SUCCESS,
  SEND_RB_ACCOUNT_SUCCESS
} from './constants';

const initialState = {
  activeTabId: 0,
  members: [],
  isTabsVisible: false,
  items_qty: 10,
  page_number: 0,
  tableColumnDirections: {},
  isCreateNew: false
};

const accountingFirmSettings = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case SET_ITEMS_PER_PAGE:
      return {
        ...state,
        items_qty: action.items_qty
      };
    case SET_DIRECTIONS:
      return {
        ...state,
        tableColumnDirections: {
          ...action.directions
        }
      };
    case SET_CURRENT_PAGE:
      return {
        ...state,
        page_number: action.page_number
      };
    case SET_ACTIVE_TAB:
      return {
        ...state,
        activeTabId: action.activeTabId
      };
    case GET_MEMBERS_ATTEMPT:
      return {
        ...state,
        members: {
          isLoading: true,
          isError: false,
          pages: 0,
          rows_number: 0,
          members_list: []
        }
      };
    case GET_MEMBERS_SUCCESS:
      return {
        ...state,
        members: {
          ...state.members,
          isLoading: false,
          isError: false,
          pages: action.members_pages_number,
          rows_number: action.members_rows_number,
          members_list: action.members
        }
      };
    case GET_MEMBERS_FAIL:
      return {
        ...state,
        members: {
          ...state.members,
          isLoading: false,
          pages: 0,
          rows_number: 0,
          isError: true
        }
      };
    case SEND_EDI_ACCOUNT_SUCCESS: {
      const compte_edi = [...state.members.members_list[action.memberIndex].compte_edi];
      compte_edi.push(action.edi);

      const members_list = [...state.members.members_list];
      members_list[action.memberIndex] = {
        ...state.members.members_list[action.memberIndex],
        compte_edi
      };

      return {
        ...state,
        members: {
          ...state.members,
          members_list
        }
      };
    }
    case SEND_RB_ACCOUNT_SUCCESS: {
      const compte_rb = [...state.members.members_list[action.memberIndex].compte_rb];
      compte_rb.push(action.rb);

      const members_list = [...state.members.members_list];
      members_list[action.memberIndex] = {
        ...state.members.members_list[action.memberIndex],
        compte_rb
      };

      return {
        ...state,
        members: {
          ...state.members,
          members_list
        }
      };
    }
    case MODIFY_RB_ACCOUNT_SUCCESS: {
      const compte_rb = [...state.members.members_list[action.memberIndex].compte_rb];
      compte_rb[action.rbIndex] = action.rb;

      const members_list = [...state.members.members_list];
      members_list[action.memberIndex] = {
        ...state.members.members_list[action.memberIndex],
        compte_rb
      };

      return {
        ...state,
        members: {
          ...state.members,
          members_list
        }
      };
    }
    case MODIFY_EDI_ACCOUNT_SUCCESS: {
      const compte_edi = [...state.members.members_list[action.memberIndex].compte_edi];
      compte_edi[action.ediIndex] = action.edi;

      const members_list = [...state.members.members_list];
      members_list[action.memberIndex] = {
        ...state.members.members_list[action.memberIndex],
        compte_edi
      };

      return {
        ...state,
        members: {
          ...state.members,
          members_list
        }
      };
    }
    case SET_CREATE_NEW_STATUS:
      return {
        ...state,
        isCreateNew: action.isCreateNew
      };
    default: return state;
    }
  }
  return state;
};

export default accountingFirmSettings;
