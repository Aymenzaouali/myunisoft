import { applyMiddleware, createStore } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk';
// import logger from 'redux-logger';
import storage from 'redux-persist/lib/storage';
import { persistStore, persistReducer } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension';
import {
  formManagingMiddleware,
  tokenMiddleware,
  menuMiddleware,
  splitMiddleware
} from 'middlewares';
import rootReducer from './reducers';

export const history = createBrowserHistory();

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['login', 'navigation']
};

const reducers = persistReducer(persistConfig, rootReducer);

const store = createStore(
  connectRouter(history)(reducers),
  composeWithDevTools(
    applyMiddleware(
      thunk,
      /**
       * Order is important !!!
       */

      /**
       * Handles location change
       * and can prevent the app from redirecting
       */
      formManagingMiddleware,
      tokenMiddleware,
      menuMiddleware,
      splitMiddleware,
      /**
       * Final middleware for
       * location change handling
       */
      routerMiddleware(history),
      // logger,
    ),
  ),
);

const persistor = persistStore(store);

export { persistor, store };
