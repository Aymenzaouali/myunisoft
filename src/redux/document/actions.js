import {
  ADD_DOCUMENTS,
  DELETE_DOCUMENT,
  DELETE_UPLOADED_DOCUMENTS,
  SEND_DOCUMENTS_ATTEMPT,
  SEND_DOCUMENT_PROGRESS_INFOS,
  SEND_DOCUMENT_SUCCESS,
  SEND_DOCUMENT_FAIL,
  SET_COLLAB_SOCIETY,
  SET_LOADING,
  SELECT_DOCUMENT,
  GROUP_DOCUMENTS,
  UNGROUP_DOCUMENTS,
  EXPAND_DOCUMENT,
  DELETE_DOCUMENTS
} from './constants';

/**
 * Send ADD_DOCUMENTS action to reducer
 * @param {Object} { type, format, files, society }
 */
export const addDocuments = ({
  type, format, files, society
}) => ({
  type: ADD_DOCUMENTS,
  payload: {
    type, format, files, society
  }
});

/**
 * Send DELETE_DOCUMENT action to reducer
 * @param {Number} document_id
 */
export const deleteDocument = document_id => ({
  type: DELETE_DOCUMENT,
  payload: { document_id }
});

/**
 * Send DELETE_DOCUMENTS action to reducer
 * @param {Array} of numbers document_id
 */
export const deleteDocuments = documents => ({
  type: DELETE_DOCUMENTS,
  payload: { documents }
});

/**
 * Delete DELETE_UPLOADED_DOCUMENTS action to reducer
 */
export const deleteUploadedDocuments = () => ({
  type: DELETE_UPLOADED_DOCUMENTS
});

/**
 * Send SET_COLLAB_SOCIETY action to reducer
 * @param {Object} society
 */
export const setCollabSociety = society => ({
  type: SET_COLLAB_SOCIETY,
  payload: { society }
});

/**
 * Send SEND_DOCUMENTS_ATTEMPT action to reducer for update document status to "waiting"
 * @param {Object} ids
 */
export const sendDocumentsAttempt = ids => ({
  type: SEND_DOCUMENTS_ATTEMPT,
  payload: { ids }
});

/**
 * Send SEND_DOCUMENT_PROGRESS_INFOS action to reducer for
 * update document status to $progress (progress number)
 * @param {Object} { document_id, progress }
 */
export const sendDocumentProgressInfos = ({ document_id, progress }) => ({
  type: SEND_DOCUMENT_PROGRESS_INFOS,
  payload: { document_id, progress }
});

/**
 * Send SEND_DOCUMENT_SUCCESS action to reducer for update document status to 'success'
 * @param {Number} document_id
 */
export const sendDocumentSuccess = (document_id, noOCR) => ({
  type: SEND_DOCUMENT_SUCCESS,
  payload: { document_id, noOCR }
});

/**
 * Send SEND_DOCUMENT_FAIL action to reducer for update document status to 'error'
 * @param {Number} document_id
 */
export const sendDocumentFail = document_id => ({
  type: SEND_DOCUMENT_FAIL,
  payload: { document_id }
});

export const groupDocuments = () => ({
  type: GROUP_DOCUMENTS
});

export const ungroupDocuments = () => ({
  type: UNGROUP_DOCUMENTS
});

export const selectDocument = (document_id, value) => ({
  type: SELECT_DOCUMENT,
  payload: { document_id, value }
});

export const expandDocument = (document_id, value) => ({
  type: EXPAND_DOCUMENT,
  payload: { document_id, value }
});

export const setLoading = isLoading => ({
  type: SET_LOADING,
  payload: { isLoading }
});
