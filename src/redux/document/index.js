import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import { convertImagesToPdf } from 'helpers/file';

import {
  sendDocumentsAttempt,
  sendDocumentProgressInfos,
  sendDocumentSuccess,
  sendDocumentFail
} from './actions';

const errorStatus = ['Erreur Format fichier'];


/**
 * Send specified ids documents to OCR, and dispatch attempt, progress, fail and success actions.
 * @param {Number} ids - Redux document entry ids
 */
export const sendDocuments = (ids, noOCR = false) => async (dispatch, getState) => {
  const { documentWeb: { docs } } = getState();
  const ocr_type_id = noOCR ? 2 : 1;

  dispatch(sendDocumentsAttempt(ids));

  const documentsGroup = docs.filter(f => ids.includes(f.document_id));

  try {
    await Promise.all(
      documentsGroup.map(async ({ documents }) => {
        const [f] = documents;
        let { blob } = f;
        let extension = blob.name.split('.').pop();
        const name = blob.name.split('.').shift();

        if (documents.length > 1) {
          blob = await convertImagesToPdf(documents);
          extension = 'pdf';
        }

        const config = {
          params: {
            name,
            extension,
            society_id: f.society.society_id || f.society.value,
            ocr_type_id,
            invoice_type_id: [null, 'purchases', 'expenseReports', 'sales', 'holdings'].indexOf(f.type)
          }
        };

        try {
          const data = await windev.makeApiCall('/invoice', 'post', config.params, blob,
            {
              headers: { 'Content-Type': 'application/octet-stream' },
              onUploadProgress: (ev) => {
                dispatch(sendDocumentProgressInfos({
                  document_id: f.document_id, progress: parseInt((ev.loaded * 100) / ev.total, 10)
                }));
              }
            });
          dispatch(errorStatus.includes(data.statusText)
            ? sendDocumentFail(f.document_id)
            : sendDocumentSuccess(f.document_id, noOCR));
          return data;
        } catch (err) {
          dispatch(sendDocumentFail(f.document_id));
          return err;
        }
      })
    );
  } catch (error) {
    console.log(error); //eslint-disable-line
    await dispatch(handleError(error));
  }
};
