import {
  ADD_DOCUMENTS,
  DELETE_DOCUMENT,
  DELETE_UPLOADED_DOCUMENTS,
  SEND_DOCUMENTS_ATTEMPT,
  SEND_DOCUMENT_PROGRESS_INFOS,
  SEND_DOCUMENT_SUCCESS,
  SEND_DOCUMENT_FAIL,
  SET_COLLAB_SOCIETY,
  SET_LOADING,
  SELECT_DOCUMENT,
  GROUP_DOCUMENTS,
  UNGROUP_DOCUMENTS,
  EXPAND_DOCUMENT,
  DELETE_DOCUMENTS
} from './constants';

const initialState = {
  collabSociety: null,
  loading: false,
  error: false,
  docs: [],
  selected: [],
  expanded: []
};

let nextdocument_id = 0;

const updateFileStatusById = (state, document_id, status, withoutOCR) => ({
  ...state,
  docs: state.docs.map(documentGroup => (document_id === documentGroup.document_id
    ? ({
      ...documentGroup,
      documents: documentGroup.documents.map(document => ({
        ...document,
        withoutOCR,
        status
      }))
    })
    : documentGroup))
});

const document = (state = initialState, action) => {
  const { docs } = state;

  switch (action.type) {
  case SET_COLLAB_SOCIETY:
    return ({ ...state, collabSociety: action.payload.society });
  case ADD_DOCUMENTS:
    const {
      type, format, files, society
    } = action.payload;
    const newDocuments = files
      .map((f) => {
        nextdocument_id += 1;

        const document_id = nextdocument_id;

        return {
          document_id,
          society,
          format,
          documents: [
            {
              type,
              format,
              status: 'waiting',
              blob: f,
              name: f.name,
              document_id,
              society
            }
          ]
        };
      });

    return ({ ...state, docs: [...docs, ...newDocuments] });
  case DELETE_DOCUMENT:
    return {
      ...state,
      docs: docs.filter(({ document_id }) => document_id !== action.payload.document_id)
    };

  case DELETE_DOCUMENTS:
    return {
      ...state,
      docs: docs.filter(({ document_id }) => !action.payload.documents?.includes(document_id))
    };

  case DELETE_UPLOADED_DOCUMENTS:
    return {
      ...state,
      docs: docs.filter(({ status }) => status !== 'success')
    };

  case SEND_DOCUMENTS_ATTEMPT:
    return updateFileStatusById(state, action.payload.document_id, 0);
  case SEND_DOCUMENT_PROGRESS_INFOS:
    return updateFileStatusById(state, action.payload.document_id, action.payload.progress);
  case SEND_DOCUMENT_SUCCESS:
    return updateFileStatusById(state, action.payload.document_id, 'success', action.payload.noOCR);
  case SEND_DOCUMENT_FAIL:
    return updateFileStatusById(state, action.payload.document_id, 'error');
  case SET_LOADING:
    return {
      ...state,
      loading: action.payload.isLoading
    };

  case SELECT_DOCUMENT:
    if (!action.payload.value) {
      return {
        ...state,
        selected: state.selected.filter(document_id => document_id !== action.payload.document_id)
      };
    }

    return {
      ...state,
      selected: [...state.selected, action.payload.document_id]
    };

  case EXPAND_DOCUMENT:
    if (!action.payload.value) {
      return {
        ...state,
        expanded: state.expanded.filter(document_id => document_id !== action.payload.document_id)
      };
    }

    return {
      ...state,
      expanded: [...state.expanded, action.payload.document_id]
    };

  case GROUP_DOCUMENTS: {
    const selectedDocumentsIds = state.selected;
    const documentsBySelectedIds = docs
      .filter(doc => selectedDocumentsIds.includes(doc.document_id));
    const ungroupedDocuments = docs
      .filter(doc => !selectedDocumentsIds.includes(doc.document_id));
    const newGroupedDocuments = [].concat(...documentsBySelectedIds
      .map(documentsBySelectedId => documentsBySelectedId.documents));
    const { society, document_id, format } = documentsBySelectedIds[0];
    const groupedDocuments = {
      society,
      document_id,
      format,
      document_attached_id: document_id,
      documents: newGroupedDocuments
    };

    return {
      ...state,
      docs: [groupedDocuments, ...ungroupedDocuments],
      selected: []
    };
  }

  case UNGROUP_DOCUMENTS: {
    const selectedDocumentsIds = state.selected;
    const documents = docs
      .filter(doc => !selectedDocumentsIds.includes(doc.document_id));
    const groupedDocuments = docs
      .filter(doc => selectedDocumentsIds.includes(doc.document_id));
    const unGroupedDocuments = []
      .concat(...groupedDocuments.map(groupedDocument => groupedDocument.documents))
      .map(document => ({
        society: groupedDocuments[0].society,
        document_id: document.document_id,
        format: document.format,
        documents: [document]
      }));

    return {
      ...state,
      docs: [...unGroupedDocuments, ...documents],
      selected: []
    };
  }

  default: return state;
  }
};

export default document;
