import {
  settingsWorksheetsKeys
} from 'assets/constants/keys';
import _ from 'lodash';
import {
  RESET_ALL_WORKSHEETS,
  ADD_ALL_WORKSHEETS,
  SELECT_WORKSHEET,
  ADD_WORKSHEET,
  REMOVE_WORKSHEET,
  GET_WORKSHEET_LIST_SUCCESS,
  GET_WORKSHEET_ATTEMPT,
  GET_WORKSHEET_LIST_FAIL,
  GET_WORKSHEET_TYPES_SUCCESS,
  GET_WORKSHEET_ACCOUNT_NUMBERS_SUCCESS
} from './constants';
import {
  settingsWorksheetsBody
} from './dummyData';

const initialState = {
  settings_id: null,
  settingsWorksheetsTable: {
    header: settingsWorksheetsKeys,
    body: settingsWorksheetsBody
  },
  selectedWorksheet: {},
  settingSelectedLine: {},
  worksheets: {},
  worksheets_types: [],
  accountNumbersTTC: [],
  accountNumbersTVA: []
};

const settingsWorksheets = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_WORKSHEET_ATTEMPT:
      return {
        ...state,
        worksheets: {
          ...state.worksheets,
          [action.society_id]: {
            isLoading: true,
            isError: false,
            setting_list: []
          }
        }
      };
    case GET_WORKSHEET_LIST_SUCCESS:
      const {
        worksheets,
        society_id,
        opts
      } = action;

      return {
        ...state,
        worksheets: {
          ...state.worksheets,
          [society_id]: {
            ..._.get(state, `worksheets[${society_id}]`, {}),
            ...opts,
            worksheets_list: worksheets,
            isLoading: false
          }
        }
      };
    case GET_WORKSHEET_TYPES_SUCCESS:
      return {
        ...state,
        worksheets_types: [
          ...action.worksheets.worksheet_types
        ]
      };
    case GET_WORKSHEET_ACCOUNT_NUMBERS_SUCCESS:
      return {
        ...state,
        accountNumbersTTC: [
          ...action.accountTTC
        ],
        accountNumbersTVA: [
          ...action.accountTVA
        ]
      };
    case GET_WORKSHEET_LIST_FAIL:
      return {
        ...state,
        worksheets: {
          ...state.worksheets,
          [action.settings_id]: {
            isLoading: false,
            isError: true,
            setting_list: []
          }
        }
      };
    case ADD_ALL_WORKSHEETS:
      return {
        ...state,
        selectedWorksheet: {
          ...state.selectedWorksheet,
          [action.society_id]: _.get(state, `worksheets[${action.society_id}].worksheets_list`, []).map(setting => setting)
        }
      };
    case RESET_ALL_WORKSHEETS:
      return {
        ...state,
        selectedWorksheet: {
          ...state.selectedWorksheet,
          [action.society_id]: []
        }
      };
    case SELECT_WORKSHEET:
      const {
        settingSelectedLine
      } = action;

      return {
        ...state,
        settingSelectedLine
      };
    case ADD_WORKSHEET: {
      return {
        ...state,
        selectedWorksheet: {
          ...state.selectedWorksheet,
          [action.society_id]: [..._.get(state, `selectedWorksheet.${action.society_id}`, []), action.settingId]
        }
      };
    }
    case REMOVE_WORKSHEET:
      return {
        ...state,
        selectedWorksheet: {
          ...state.selectedWorksheet,
          [action.society_id]: _.get(state, `selectedWorksheet[${action.society_id}]`, []).filter(setting => setting.id !== action.settingId)
        }
      };
    default: return state;
    }
  }
  return state;
};

export default settingsWorksheets;
