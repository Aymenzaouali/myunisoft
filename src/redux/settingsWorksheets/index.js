import { windev } from 'helpers/api';
import _ from 'lodash';
import actions from './actions';

export class SettingsWorksheetsService {
  static instance;

  static getInstance() {
    if (!SettingsWorksheetsService.instance) {
      SettingsWorksheetsService.instance = new SettingsWorksheetsService();
    }
    return SettingsWorksheetsService.instance;
  }

  getWorksheets = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    try {
      await dispatch(actions.getWorksheetsAttempt(society_id));
      const response = await windev.makeApiCall('/worksheet/param', 'get');
      const worksheets = response.data ? response.data.map(worksheet => ({
        id: worksheet.id_param_account_worksheet || null,
        account_ttc: {
          id: worksheet.account_ttc.id || null,
          label: worksheet.account_ttc.label || null,
          number: worksheet.account_ttc.number || null
        },
        account_tva: (worksheet.account_tva && {
          id: worksheet.account_tva.id,
          label: worksheet.account_ttc.label || null,
          number: worksheet.account_tva.number
        }) || null,
        type: worksheet.worksheet_type[0].code || null,
        blocked: worksheet.blocked
      })) : [];
      await dispatch(actions.getWorksheetsSuccess(worksheets, society_id));
      return worksheets;
    } catch (err) {
      await dispatch(actions.getWorksheetsFail(society_id));
      return err;
    }
  };

  getWorksheetsTypes = () => async (dispatch) => {
    try {
      await dispatch(actions.getWorksheetsTypesAttempt());
      const response = await windev.makeApiCall('/worksheet/type', 'get');
      const worksheetsTypes = response.data;
      await dispatch(actions.getWorksheetsTypesSuccess(worksheetsTypes));
      return worksheetsTypes;
    } catch (err) {
      await dispatch(actions.getWorksheetsTypesFail());
      return err;
    }
  };

  getWorksheetsAccountNumbers = (accountTTC, accountTVA) => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const params = { society_id, q: accountTTC };
    const params2 = { society_id, q: accountTVA };
    let responseTVA = {};
    let accountNumbersTVA = [];
    try {
      await dispatch(actions.getWorksheetAccountNumbersAttempt(society_id));
      const responseTTC = await windev.makeApiCall('account?mode=1&&limit=5', 'get', params);
      const accountNumbersTTC = responseTTC.data;
      responseTVA = accountTVA ? await windev.makeApiCall('account?mode=1&&limit=5', 'get', params2) : {};
      accountNumbersTVA = accountTVA ? responseTVA.data : [];
      await dispatch(
        actions.getWorksheetAccountNumbersSuccess(accountNumbersTTC, accountNumbersTVA)
      );
      return accountNumbersTTC;
    } catch (err) {
      await dispatch(actions.getWorksheetAccountNumbersTypesFail());
      return err;
    }
  };

  resetWorksheetsAccountNumbers = () => async (dispatch) => {
    await dispatch(
      actions.getWorksheetAccountNumbersSuccess([], [])
    );
  };

  sendWorksheets = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const worksheetsForm = state.form.worksheetsForm.values;
    const { id_account_ttc, id_account_tva, id_worksheet_type } = worksheetsForm;
    const ttc_id = id_account_ttc ? id_account_ttc.account_id : null;
    const tva_id = id_account_tva ? id_account_tva.account_id : null;
    const params = {
      param_list: [
        {
          society_id,
          id_account_ttc: ttc_id,
          id_account_tva: tva_id,
          id_worksheet_type: id_worksheet_type.id
        }
      ]
    };
    try {
      await dispatch(actions.sendWorksheetsAttempt(society_id));
      const response = await windev.makeApiCall('/worksheet/param', 'post', {}, params);
      const { data } = response;
      await dispatch(actions.sendWorksheetsSuccess(society_id));
      return data;
    } catch (err) {
      await dispatch(actions.setWorksheetsFail(society_id));
      return err;
    }
  };

  modifyWorksheets = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const worksheetsForm = state.form.worksheetsForm.values;
    const selectedWorksheets = state.settingsWorksheets.selectedWorksheet[society_id];
    const id_param_account_worksheet = selectedWorksheets[selectedWorksheets.length - 1].id;
    const { id_account_ttc, id_account_tva, id_worksheet_type } = worksheetsForm;
    const ttc_id = _.get(id_account_ttc, 'account_id', null);
    const tva_id = _.get(id_account_tva, 'account_id', null);
    const params = {
      param_list: [
        {
          id_param_account_worksheet,
          id_account_ttc: ttc_id,
          id_account_tva: tva_id,
          id_worksheet_type: id_worksheet_type.id
        }
      ]
    };
    try {
      await dispatch(actions.modifyWorksheetsAttempt(society_id));
      const response = await windev.makeApiCall('/worksheet/param', 'PUT', {}, params);
      const { data } = response;
      await dispatch(actions.modifyWorksheetsSuccess(society_id));
      await dispatch(actions.resetAllWorksheet(society_id));
      return data;
    } catch (err) {
      await dispatch(actions.modifyWorksheetsFail(society_id));
      return err;
    }
  };

  deleteWorksheets = () => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const worksheets = state.settingsWorksheets.selectedWorksheet[society_id];
    const params = {
      param_worksheets: worksheets.map(worksheet => ({ id_param_account_worksheet: worksheet.id }))
    };
    try {
      await dispatch(actions.deleteWorksheetsAttempt(society_id, worksheets));
      const { data } = await windev.makeApiCall('/worksheet/param', 'delete', {}, params);
      await dispatch(actions.deleteWorksheetsSuccess(data));
      await dispatch(actions.resetAllWorksheet(society_id));
      dispatch(this.getWorksheets());
      return data;
    } catch (err) {
      await dispatch(actions.deleteWorksheetsFail(err));
      return err;
    }
  };
}


export default SettingsWorksheetsService.getInstance();
