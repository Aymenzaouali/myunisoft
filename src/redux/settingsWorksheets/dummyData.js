export const settingsWorksheetsBody = [
  {
    code: '01',
    settings_id: 556,
    accountNumberTVA: 445862,
    settings_type_code: 'ACH',
    account_number: 403040
  },
  {
    code: '02',
    settings_id: 557,
    accountNumberTVA: 445861,
    settings_type_code: 'VTE',
    account_number: 2040
  },
  {
    code: '03',
    settings_id: 558,
    accountNumberTVA: 445862,
    settings_type_code: 'ACH',
    account_number: 80300
  },
  {
    code: '04',
    settings_id: 559,
    accountNumberTVA: 445872,
    settings_type_code: 'ACH',
    account_number: 60300
  },
  {
    code: '05',
    settings_id: 560,
    accountNumberTVA: 445872,
    settings_type_code: 'ACH',
    account_number: 11880
  },
  {
    code: '06',
    settings_id: 561,
    accountNumberTVA: 445861,
    settings_type_code: 'BQ',
    account_number: 30300
  },
  {
    code: '07',
    settings_id: 562,
    accountNumberTVA: 445862,
    settings_type_code: 'ACH',
    account_number: 10300
  },
  {
    code: '08',
    settings_id: 563,
    accountNumberTVA: 445862,
    settings_type_code: 'BQ',
    account_number: 21300
  },
  {
    code: '09',
    settings_id: 564,
    accountNumberTVA: 445862,
    settings_type_code: 'CAISSE',
    account_number: 130300
  },
  {
    code: '10',
    settings_id: 565,
    accountNumberTVA: 445862,
    settings_type_code: 'OD',
    account_number: 20300
  }
];
