import {
  ADD_ALL_WORKSHEETS,
  RESET_ALL_WORKSHEETS,
  SELECT_WORKSHEET,
  ADD_WORKSHEET,
  REMOVE_WORKSHEET,
  GET_WORKSHEET_ATTEMPT,
  GET_WORKSHEET_LIST_SUCCESS,
  GET_WORKSHEET_LIST_FAIL,
  SEND_WORKSHEET_ATTEMPT,
  SEND_WORKSHEET_LIST_SUCCESS,
  SEND_WORKSHEET_LIST_FAIL,
  MODIFY_WORKSHEET_ATTEMPT,
  MODIFY_WORKSHEET_SUCCESS,
  MODIFY_WORKSHEET_FAIL,
  DELETE_WORKSHEET_ATTEMPT,
  DELETE_WORKSHEET_LIST_SUCCESS,
  DELETE_WORKSHEET_LIST_FAIL,
  GET_WORKSHEET_TYPES_ATTEMPT,
  GET_WORKSHEET_TYPES_SUCCESS,
  GET_WORKSHEET_TYPES_FAIL,
  GET_WORKSHEET_ACCOUNT_NUMBERS_ATTEMPT,
  GET_WORKSHEET_ACCOUNT_NUMBERS_SUCCESS,
  GET_WORKSHEET_ACCOUNT_NUMBERS_FAIL
} from './constants';

const addAllWorksheet = society_id => ({
  type: ADD_ALL_WORKSHEETS,
  society_id
});

const resetAllWorksheet = society_id => ({
  type: RESET_ALL_WORKSHEETS,
  society_id
});

const selectWorksheet = settingSelectedLine => ({
  type: SELECT_WORKSHEET,
  settingSelectedLine
});

const addWorksheet = (settingId, society_id) => ({
  type: ADD_WORKSHEET,
  society_id,
  settingId
});

const removeWorksheet = (settingId, society_id) => ({
  type: REMOVE_WORKSHEET,
  society_id,
  settingId
});

const getWorksheetsAttempt = society_id => ({
  type: GET_WORKSHEET_ATTEMPT,
  society_id
});

const getWorksheetsSuccess = (worksheets, society_id, opts) => ({
  type: GET_WORKSHEET_LIST_SUCCESS,
  worksheets,
  society_id,
  opts
});

const getWorksheetsFail = society_id => ({
  type: GET_WORKSHEET_LIST_FAIL,
  society_id
});

const sendWorksheetsAttempt = society_id => ({
  type: SEND_WORKSHEET_ATTEMPT,
  society_id
});

const sendWorksheetsSuccess = society_id => ({
  type: SEND_WORKSHEET_LIST_SUCCESS,
  society_id
});

const setWorksheetsFail = society_id => ({
  type: SEND_WORKSHEET_LIST_FAIL,
  society_id
});

const modifyWorksheetsAttempt = society_id => ({
  type: MODIFY_WORKSHEET_ATTEMPT,
  society_id
});

const modifyWorksheetsSuccess = society_id => ({
  type: MODIFY_WORKSHEET_SUCCESS,
  society_id
});

const modifyWorksheetsFail = society_id => ({
  type: MODIFY_WORKSHEET_FAIL,
  society_id
});

const deleteWorksheetsAttempt = (society_id, worksheets) => ({
  type: DELETE_WORKSHEET_ATTEMPT,
  society_id,
  worksheets
});

const deleteWorksheetsSuccess = society_id => ({
  type: DELETE_WORKSHEET_LIST_SUCCESS,
  society_id
});

const deleteWorksheetsFail = society_id => ({
  type: DELETE_WORKSHEET_LIST_FAIL,
  society_id
});

const getWorksheetsTypesAttempt = (society_id, worksheets) => ({
  type: GET_WORKSHEET_TYPES_ATTEMPT,
  society_id,
  worksheets
});

const getWorksheetsTypesSuccess = worksheets => ({
  type: GET_WORKSHEET_TYPES_SUCCESS,
  worksheets
});

const getWorksheetsTypesFail = society_id => ({
  type: GET_WORKSHEET_TYPES_FAIL,
  society_id
});

const getWorksheetAccountNumbersAttempt = society_id => ({
  type: GET_WORKSHEET_ACCOUNT_NUMBERS_ATTEMPT,
  society_id
});

const getWorksheetAccountNumbersSuccess = (accountTTC, accountTVA) => ({
  type: GET_WORKSHEET_ACCOUNT_NUMBERS_SUCCESS,
  accountTTC,
  accountTVA
});

const getWorksheetAccountNumbersTypesFail = society_id => ({
  type: GET_WORKSHEET_ACCOUNT_NUMBERS_FAIL,
  society_id
});

export default {
  addAllWorksheet,
  resetAllWorksheet,
  selectWorksheet,
  addWorksheet,
  removeWorksheet,
  getWorksheetsAttempt,
  getWorksheetsSuccess,
  getWorksheetsFail,
  sendWorksheetsAttempt,
  sendWorksheetsSuccess,
  setWorksheetsFail,
  modifyWorksheetsAttempt,
  modifyWorksheetsSuccess,
  modifyWorksheetsFail,
  deleteWorksheetsAttempt,
  deleteWorksheetsSuccess,
  deleteWorksheetsFail,
  getWorksheetsTypesAttempt,
  getWorksheetsTypesSuccess,
  getWorksheetsTypesFail,
  getWorksheetAccountNumbersAttempt,
  getWorksheetAccountNumbersSuccess,
  getWorksheetAccountNumbersTypesFail
};
