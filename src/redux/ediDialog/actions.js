import {
  OPEN_DIALOG,
  INIT_DIALOG,
  CLOSE_DIALOG
} from './constants';

const openDialog = name => ({
  type: OPEN_DIALOG,
  name
});

const initDialog = (name, message, buttonsLabel) => ({
  type: INIT_DIALOG,
  name,
  message,
  buttonsLabel
});

const closeDialog = name => ({
  type: CLOSE_DIALOG,
  name
});

export default {
  openDialog,
  initDialog,
  closeDialog
};
