import { get } from 'lodash';
import actions from './actions';

export const openDialog = (screen, message, buttonsLabel) => async (dispatch, getState) => {
  const state = getState();
  const society_id = get(state, 'navigation.id', false);
  try {
    await dispatch(actions.initDialog(`dialog_${screen}_${society_id}`, message, buttonsLabel));
    await dispatch(actions.openDialog(`dialog_${screen}_${society_id}`));
    return null;
  } catch (err) {
    await dispatch(actions.closeDialog(`dialog_${screen}_${society_id}`));
    return err;
  }
};

export const closeDialog = screen => async (dispatch, getState) => {
  const state = getState();
  const society_id = get(state, 'navigation.id', false);
  await dispatch(actions.closeDialog(`dialog_${screen}_${society_id}`));
};
