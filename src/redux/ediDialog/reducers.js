import {
  OPEN_DIALOG,
  INIT_DIALOG,
  CLOSE_DIALOG
} from './constants';

const initialState = {};

const dialogs = (state = initialState, action) => {
  const {
    name,
    message,
    buttonsLabel
  } = action;

  if (action && action.type) {
    switch (action.type) {
    case OPEN_DIALOG:
      return {
        ...state,
        [name]: {
          ...state[name],
          isOpen: true
        }
      };
    case INIT_DIALOG:
      return {
        ...state,
        [name]: {
          ...state[name],
          message,
          buttonsLabel
        }
      };
    case CLOSE_DIALOG:
      return {
        ...state,
        [name]: {
          ...state[name],
          isOpen: false
        }
      };
    default:
      return state;
    }
  }
  return state;
};

export default dialogs;
