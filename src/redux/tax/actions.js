import {
  GET_TAX_CYCLE_FAIL,
  GET_TAX_CYCLE_ATTEMPT,
  GET_TAX_CYCLE_SUCCESS,
  SEND_TAX_CYCLE_FAIL,
  SEND_TAX_CYCLE_ATTEMPT,
  SEND_TAX_CYCLE_SUCCESS,
  TAX_SELECT_FORM,
  GET_BINARY_TAX_FORM_ATTEMPT,
  GET_BINARY_TAX_FORM_SUCCESS,
  GET_BINARY_TAX_FORM_FAIL,
  CONTROLS_TAX_ATTEMPT,
  CONTROLS_TAX_SUCCESS,
  CONTROLS_TAX_FAIL,
  REFRESH_TAX_AUTO_ATTEMPT,
  REFRESH_TAX_AUTO_FAIL
} from './constants';

const refreshAutoAttempt = societyId => ({
  type: REFRESH_TAX_AUTO_ATTEMPT,
  societyId
});

const refreshAutoFail = societyId => ({
  type: REFRESH_TAX_AUTO_FAIL,
  societyId
});

const getTaxAttempt = societyId => ({
  type: GET_TAX_CYCLE_ATTEMPT,
  societyId
});

const getTaxSuccess = (taxForm, societyId) => ({
  type: GET_TAX_CYCLE_SUCCESS,
  societyId,
  taxForm
});

const getTaxFail = societyId => ({
  type: GET_TAX_CYCLE_FAIL,
  societyId
});

const sendTaxAttempt = societyId => ({
  type: SEND_TAX_CYCLE_ATTEMPT,
  societyId
});

const sendTaxSuccess = (taxForm, societyId) => ({
  type: SEND_TAX_CYCLE_SUCCESS,
  societyId,
  taxForm
});

const sendTaxFail = societyId => ({
  type: SEND_TAX_CYCLE_FAIL,
  societyId
});

const selectFormId = (societyId, form_id) => ({
  type: TAX_SELECT_FORM,
  societyId,
  form_id
});

const getBinaryTaxFormAttempt = societyId => ({
  type: GET_BINARY_TAX_FORM_ATTEMPT,
  societyId
});

const getBinaryTaxFormSuccess = (file, societyId) => ({
  type: GET_BINARY_TAX_FORM_SUCCESS,
  societyId,
  file
});

const getBinaryTaxFormFail = societyId => ({
  type: GET_BINARY_TAX_FORM_FAIL,
  societyId
});

const controlsTaxAttempt = societyId => ({
  type: CONTROLS_TAX_ATTEMPT,
  societyId
});

const controlsTaxSuccess = (file, societyId) => ({
  type: CONTROLS_TAX_SUCCESS,
  societyId,
  file
});

const controlsTaxFail = societyId => ({
  type: CONTROLS_TAX_FAIL,
  societyId
});

export default {
  getTaxAttempt,
  getTaxSuccess,
  getTaxFail,
  selectFormId,
  sendTaxAttempt,
  sendTaxSuccess,
  sendTaxFail,
  getBinaryTaxFormAttempt,
  getBinaryTaxFormSuccess,
  getBinaryTaxFormFail,
  controlsTaxAttempt,
  controlsTaxSuccess,
  controlsTaxFail,
  refreshAutoAttempt,
  refreshAutoFail
};
