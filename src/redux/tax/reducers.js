import {
  GET_TAX_CYCLE_FAIL,
  GET_TAX_CYCLE_ATTEMPT,
  GET_TAX_CYCLE_SUCCESS,
  TAX_SELECT_FORM
} from './constants';

const initialeState = {
};

const tax = (state = initialeState, action) => {
  const {
    societyId,
    taxForm,
    form_id
  } = action;

  switch (action.type) {
  case GET_TAX_CYCLE_ATTEMPT:
    return {
      ...state,
      taxForm: {
        [societyId]: {
          forms: [],
          isLoading: true,
          isError: false
        }
      }
    };
  case GET_TAX_CYCLE_FAIL:
    return {
      ...state,
      taxForm: {
        [societyId]: {
          forms: [],
          isLoading: false,
          isError: true
        }
      }
    };
  case GET_TAX_CYCLE_SUCCESS:
    return {
      ...state,
      taxForm: {
        [societyId]: {
          forms: taxForm,
          isLoading: false,
          isError: false
        }
      }
    };
  case TAX_SELECT_FORM:
    return {
      ...state,
      selectedFormId: {
        [societyId]: form_id
      }
    };
  default: return state;
  }
};

export default tax;
