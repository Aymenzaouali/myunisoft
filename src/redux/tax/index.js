import _ from 'lodash';
import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import { initialize } from 'redux-form';
import { printOrDownload } from 'helpers/file';
import moment from 'moment';
import actions from './actions';

export const getTaxForms = (exerciseId, code_sheet_group) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  await dispatch(initialize(`${society_id}taxDeclaration2033A`));
  await dispatch(initialize(`${society_id}taxDeclaration2033B`));
  await dispatch(initialize(`${society_id}taxDeclaration2033C`));
  await dispatch(initialize(`${society_id}taxDeclaration2033D`));
  await dispatch(initialize(`${society_id}taxDeclaration2033E`));
  await dispatch(initialize(`${society_id}taxDeclaration2033F`));
  await dispatch(initialize(`${society_id}taxDeclaration2033G`));
  await dispatch(initialize(`${society_id}taxDeclaration2035E`));
  await dispatch(initialize(`${society_id}taxDeclaration2050`));
  await dispatch(initialize(`${society_id}taxDeclaration2051`));
  await dispatch(initialize(`${society_id}taxDeclaration2052`));
  await dispatch(initialize(`${society_id}taxDeclaration2053`));
  await dispatch(initialize(`${society_id}taxDeclaration2054`));
  await dispatch(initialize(`${society_id}taxDeclaration2055`));
  await dispatch(initialize(`${society_id}taxDeclaration2056`));
  await dispatch(initialize(`${society_id}taxDeclaration2057`));
  await dispatch(initialize(`${society_id}taxDeclaration2059B`));
  await dispatch(initialize(`${society_id}taxDeclaration2069A`));
  await dispatch(initialize(`${society_id}taxDeclaration2059C`));
  await dispatch(initialize(`${society_id}taxDeclaration2059D`));
  await dispatch(initialize(`${society_id}taxDeclaration2059E`));
  await dispatch(initialize(`${society_id}taxDeclaration2059F`));
  await dispatch(initialize(`${society_id}taxDeclaration2059G`));
  await dispatch(initialize(`${society_id}taxDeclaration2035`));
  await dispatch(initialize(`${society_id}taxDeclaration2035SUITE`));
  await dispatch(initialize(`${society_id}taxDeclaration2035A`));
  await dispatch(initialize(`${society_id}taxDeclaration2035B`));
  await dispatch(initialize(`${society_id}taxDeclaration2035F`));
  await dispatch(initialize(`${society_id}taxDeclaration2065BIS`));
  await dispatch(initialize(`${society_id}taxDeclaration2035G`));
  await dispatch(initialize(`${society_id}taxDeclaration2058A`));
  await dispatch(initialize(`${society_id}taxDeclaration2058B`));
  await dispatch(initialize(`${society_id}taxDeclaration2058C`));
  await dispatch(initialize(`${society_id}taxDeclaration2059A`));
  await dispatch(initialize(`${society_id}taxDeclaration2065`));
  await dispatch(initialize(`${society_id}taxDeclaration2573`));
  await dispatch(initialize(`${society_id}taxDeclaration2054BIS`));
  try {
    const payload = { exercice_id: exerciseId };
    if (code_sheet_group) payload.code_sheet_group = code_sheet_group;
    await dispatch(actions.getTaxAttempt(society_id));
    const { data } = await windev.makeApiCall('/liasse', 'get', payload);
    await dispatch(actions.getTaxSuccess(data, society_id));

    const data2573 = _.get(data.filter(form => form.name === '2573'), '[0]');
    if (!_.isEmpty(data2573)) {
      const form2573 = _.get(data2573, 'data');
      if (!_.isEmpty(form2573)) {
        await dispatch(initialize(`${society_id}taxDeclaration2573`, form2573));
      }
    }

    const data2033A = _.get(data.filter(form => form.name === '2033A'), '[0]');
    if (!_.isEmpty(data2033A)) {
      const form2033A = _.get(data2033A, 'data');
      if (!_.isEmpty(form2033A)) {
        await dispatch(initialize(`${society_id}taxDeclaration2033A`, form2033A));
      }
    }

    const data2033B = _.get(data.filter(form => form.name === '2033B'), '[0]');
    if (!_.isEmpty(data2033B)) {
      const form2033B = _.get(data2033B, 'data');
      if (!_.isEmpty(form2033B)) {
        await dispatch(initialize(`${society_id}taxDeclaration2033B`, form2033B));
      }
    }

    const data2033C = _.get(data.filter(form => form.name === '2033C'), '[0]');
    if (!_.isEmpty(data2033C)) {
      const form2033C = _.get(data2033C, 'data');
      if (!_.isEmpty(form2033C)) {
        await dispatch(initialize(`${society_id}taxDeclaration2033C`, form2033C));
      }
    }

    const data2033D = _.get(data.filter(form => form.name === '2033D'), '[0]');
    if (!_.isEmpty(data2033D)) {
      const form2033D = _.get(data2033D, 'data');
      if (!_.isEmpty(form2033D)) {
        await dispatch(initialize(`${society_id}taxDeclaration2033D`, form2033D));
      }
    }

    const data2033E = _.get(data.filter(form => form.name === '2033E'), '[0]');
    if (!_.isEmpty(data2033E)) {
      const form2033E = _.get(data2033E, 'data');
      if (!_.isEmpty(form2033E)) {
        await dispatch(initialize(`${society_id}taxDeclaration2033E`, form2033E));
      }
    }

    const data2033F = _.get(data.filter(form => form.name === '2033F'), '[0]');
    if (!_.isEmpty(data2033F)) {
      const form2033F = _.get(data2033F, 'data');
      if (!_.isEmpty(form2033F)) {
        await dispatch(initialize(`${society_id}taxDeclaration2033F`, form2033F));
      }
    }

    const data2033G = _.get(data.filter(form => form.name === '2033G'), '[0]');
    if (!_.isEmpty(data2033G)) {
      const form2033G = _.get(data2033G, 'data');
      if (!_.isEmpty(form2033G)) {
        await dispatch(initialize(`${society_id}taxDeclaration2033G`, form2033G));
      }
    }

    const data2035E = _.get(data.filter(form => form.name === '2035E'), '[0]');
    if (!_.isEmpty(data2035E)) {
      const form2035E = _.get(data2035E, 'data');
      if (!_.isEmpty(form2035E)) {
        await dispatch(initialize(`${society_id}taxDeclaration2035E`, form2035E));
      }
    }

    const data2050 = _.get(data.filter(form => form.name === '2050'), '[0]');
    if (!_.isEmpty(data2050)) {
      const form2050 = _.get(data2050, 'data');
      if (!_.isEmpty(form2050)) {
        await dispatch(initialize(`${society_id}taxDeclaration2050`, form2050));
      }
    }

    const data2051 = _.get(data.filter(form => form.name === '2051'), '[0]');
    if (!_.isEmpty(data2051)) {
      const form2051 = _.get(data2051, 'data');
      if (!_.isEmpty(form2051)) {
        await dispatch(initialize(`${society_id}taxDeclaration2051`, form2051));
      }
    }

    const data2052 = _.get(data.filter(form => form.name === '2052'), '[0]');
    if (!_.isEmpty(data2052)) {
      const form2052 = _.get(data2052, 'data');
      if (!_.isEmpty(form2052)) {
        await dispatch(initialize(`${society_id}taxDeclaration2052`, form2052));
      }
    }

    const data2053 = _.get(data.filter(form => form.name === '2053'), '[0]');
    if (!_.isEmpty(data2053)) {
      const form2053 = _.get(data2053, 'data');
      if (!_.isEmpty(form2053)) {
        await dispatch(initialize(`${society_id}taxDeclaration2053`, form2053));
      }
    }

    const data2054 = _.get(data.filter(form => form.name === '2054'), '[0]');
    if (!_.isEmpty(data2054)) {
      const form2054 = _.get(data2054, 'data');
      if (!_.isEmpty(form2054)) {
        await dispatch(initialize(`${society_id}taxDeclaration2054`, form2054));
      }
    }

    const data2055 = _.get(data.filter(form => form.name === '2055'), '[0]');
    if (!_.isEmpty(data2055)) {
      const form2055 = _.get(data2055, 'data');
      if (!_.isEmpty(form2055)) {
        await dispatch(initialize(`${society_id}taxDeclaration2055`, form2055));
      }
    }

    const data2056 = _.get(data.filter(form => form.name === '2056'), '[0]');
    if (!_.isEmpty(data2056)) {
      const form2056 = _.get(data2056, 'data');
      if (!_.isEmpty(form2056)) {
        await dispatch(initialize(`${society_id}taxDeclaration2056`, form2056));
      }
    }

    const data2057 = _.get(data.filter(form => form.name === '2057'), '[0]');
    if (!_.isEmpty(data2057)) {
      const form2057 = _.get(data2057, 'data');
      if (!_.isEmpty(form2057)) {
        await dispatch(initialize(`${society_id}taxDeclaration2057`, form2057));
      }
    }

    const data2059B = _.get(data.filter(form => form.name === '2059B'), '[0]');
    if (!_.isEmpty(data2059B)) {
      const form2059B = _.get(data2059B, 'data');
      if (!_.isEmpty(form2059B)) {
        await dispatch(initialize(`${society_id}taxDeclaration2059B`, form2059B));
      }
    }

    const data2069A = _.get(data.filter(form => form.name === '2069A'), '[0]');
    if (!_.isEmpty(data2069A)) {
      const form2069A = _.get(data2069A, 'data');
      if (!_.isEmpty(form2069A)) {
        await dispatch(initialize(`${society_id}taxDeclaration2069A`, form2069A));
      }
    }

    const data2059C = _.get(data.filter(form => form.name === '2059C'), '[0]');
    if (!_.isEmpty(data2059C)) {
      const form2059C = _.get(data2059C, 'data');
      if (!_.isEmpty(form2059C)) {
        await dispatch(initialize(`${society_id}taxDeclaration2059C`, form2059C));
      }
    }

    const data2059D = _.get(data.filter(form => form.name === '2059D'), '[0]');
    if (!_.isEmpty(data2059D)) {
      const form2059D = _.get(data2059D, 'data');
      if (!_.isEmpty(form2059D)) {
        await dispatch(initialize(`${society_id}taxDeclaration2059D`, form2059D));
      }
    }

    const data2059E = _.get(data.filter(form => form.name === '2059E'), '[0]');
    if (!_.isEmpty(data2059E)) {
      const form2059E = _.get(data2059E, 'data');
      if (!_.isEmpty(form2059E)) {
        await dispatch(initialize(`${society_id}taxDeclaration2059E`, form2059E));
      }
    }

    const data2059F = _.get(data.filter(form => form.name === '2059F'), '[0]');
    if (!_.isEmpty(data2059F)) {
      const form2059F = _.get(data2059F, 'data');
      if (!_.isEmpty(form2059F)) {
        await dispatch(initialize(`${society_id}taxDeclaration2059F`, form2059F));
      }
    }

    const data2059G = _.get(data.filter(form => form.name === '2059G'), '[0]');
    if (!_.isEmpty(data2059G)) {
      const form2059G = _.get(data2059G, 'data');
      if (!_.isEmpty(form2059G)) {
        await dispatch(initialize(`${society_id}taxDeclaration2059G`, form2059G));
      }
    }

    const data2035 = _.get(data.filter(form => form.name === '2035'), '[0]');
    if (!_.isEmpty(data2035)) {
      const form2035 = _.get(data2035, 'data');
      if (!_.isEmpty(form2035)) {
        await dispatch(initialize(`${society_id}taxDeclaration2035`, form2035));
      }
    }

    const data2035SUITE = _.get(data.filter(form => form.name === '2035SUITE'), '[0]');
    if (!_.isEmpty(data2035SUITE)) {
      const form2035SUITE = _.get(data2035SUITE, 'data');
      if (!_.isEmpty(form2035SUITE)) {
        await dispatch(initialize(`${society_id}taxDeclaration2035v`, form2035SUITE));
      }
    }

    const data2035A = _.get(data.filter(form => form.name === '2035A'), '[0]');
    if (!_.isEmpty(data2035A)) {
      const form2035A = _.get(data2035A, 'data');
      if (!_.isEmpty(form2035A)) {
        await dispatch(initialize(`${society_id}taxDeclaration2035A`, form2035A));
      }
    }

    const data2035B = _.get(data.filter(form => form.name === '2035B'), '[0]');
    if (!_.isEmpty(data2035B)) {
      const form2035B = _.get(data2035B, 'data');
      if (!_.isEmpty(form2035B)) {
        await dispatch(initialize(`${society_id}taxDeclaration2035B`, form2035B));
      }
    }

    const data2035F = _.get(data.filter(form => form.name === '2035F'), '[0]');
    if (!_.isEmpty(data2035F)) {
      const form2035F = _.get(data2035F, 'data');
      if (!_.isEmpty(form2035F)) {
        await dispatch(initialize(`${society_id}taxDeclaration2035F`, form2035F));
      }
    }

    const data2065BIS = _.get(data.filter(form => form.name === '2065BIS'), '[0]');
    if (!_.isEmpty(data2065BIS)) {
      const form2065BIS = _.get(data2065BIS, 'data');
      if (!_.isEmpty(form2065BIS)) {
        await dispatch(initialize(`${society_id}taxDeclaration2065BIS`, form2065BIS));
      }
    }

    const data2035G = _.get(data.filter(form => form.name === '2035G'), '[0]');
    if (!_.isEmpty(data2035G)) {
      const form2035G = _.get(data2035G, 'data');
      if (!_.isEmpty(form2035G)) {
        await dispatch(initialize(`${society_id}taxDeclaration2035G`, form2035G));
      }
    }

    const data2058A = _.get(data.filter(form => form.name === '2058A'), '[0]');
    if (!_.isEmpty(data2058A)) {
      const form2058A = _.get(data2058A, 'data');
      if (!_.isEmpty(form2058A)) {
        await dispatch(initialize(`${society_id}taxDeclaration2058A`, form2058A));
      }
    }

    const data2058B = _.get(data.filter(form => form.name === '2058B'), '[0]');
    if (!_.isEmpty(data2058B)) {
      const form2058B = _.get(data2058B, 'data');
      if (!_.isEmpty(form2058B)) {
        await dispatch(initialize(`${society_id}taxDeclaration2058B`, form2058B));
      }
    }

    const data2058C = _.get(data.filter(form => form.name === '2058C'), '[0]');
    if (!_.isEmpty(data2058C)) {
      const form2058C = _.get(data2058C, 'data');
      if (!_.isEmpty(form2058C)) {
        await dispatch(initialize(`${society_id}taxDeclaration2058C`, form2058C));
      }
    }

    const data2059A = _.get(data.filter(form => form.name === '2059A'), '[0]');
    if (!_.isEmpty(data2059A)) {
      const form2059A = _.get(data2059A, 'data');
      if (!_.isEmpty(form2059A)) {
        await dispatch(initialize(`${society_id}taxDeclaration2059A`, form2059A));
      }
    }

    const data2065 = _.get(data.filter(form => form.name === '2065'), '[0]');
    if (!_.isEmpty(data2065)) {
      const form2065 = _.get(data2065, 'data');
      if (!_.isEmpty(form2065)) {
        await dispatch(initialize(`${society_id}taxDeclaration2065`, form2065));
      }
    }

    const data2054BIS = _.get(data.filter(form => form.name === '2054BIS'), '[0]');
    if (!_.isEmpty(data2054BIS)) {
      const form2054BIS = _.get(data2054BIS, 'data');
      if (!_.isEmpty(form2054BIS)) {
        await dispatch(initialize(`${society_id}taxDeclaration2054BIS`, form2054BIS));
      }
    }

    return data;
  } catch (err) {
    await dispatch(actions.getTaxFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const refreshAuto = (
  exerciseId, date_end, code_sheet_group
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.refreshAutoAttempt(society_id));
    const { data } = await windev.makeApiCall('/liasse/auto', 'post',
      { exercice_id: exerciseId, code_sheet_group, date_end: moment(date_end).format('YYYYMMDD') }, {});
    await dispatch(actions.getTaxSuccess(data, society_id));
    await dispatch(getTaxForms(exerciseId));
    return null;
  } catch (err) {
    await dispatch(actions.refreshAutoFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const sendTaxForms = (exerciseId, code_sheet_group) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  const tax_forms = _.get(state, `tax.taxForm[${society_id}].forms`);
  const existForms = _.get(state, 'form');
  const date_declare = _.get(state, `form.${society_id}TaxDeclarationHeader.values.dateStart`);
  try {
    const requestBody = tax_forms.map(({ form_id: id_sheet, name, data }) => {
      const field_list = existForms[`${society_id}taxDeclaration${name}`];
      if (field_list && field_list.values) {
        return {
          id_sheet,
          field_list: field_list.values
        };
      }
      return {
        id_sheet,
        field_list: data
      };
    });
    const body = { date_declare, sheet_list: requestBody };
    if (code_sheet_group) body.code_sheet_group = code_sheet_group;
    await windev.makeApiCall('/liasse', 'post', { exercice_id: exerciseId }, body);
    await dispatch(actions.sendTaxAttempt(society_id));
    return null;
  } catch (err) {
    await dispatch(actions.getTaxFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const sendTaxFormEdi = () => async () => {
  console.log('sendTaxFormEdi');
};

export const controlsTax = (exerciseId, type) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.controlsTaxAttempt(society_id));
    const { data } = await windev.makeApiCall('/liasse/controle', 'get',
      { exercice_id: exerciseId, pdf_json: 3 }, {}, { responseType: 'blob' });
    await dispatch(actions.controlsTaxSuccess(data, society_id));

    printOrDownload(data, type, `${exerciseId}.pdf`);
    return data;
  } catch (err) {
    await dispatch(actions.controlsTaxFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const printOrDownloadTax = (
  exerciseId, type, code_sheet_group
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    const payload = { exercice_id: exerciseId };
    if (code_sheet_group) payload.code_sheet_group = code_sheet_group;
    await dispatch(actions.getBinaryTaxFormAttempt(society_id));
    const { data } = await windev.makeApiCall('/liasse/download', 'get', payload, {}, { responseType: 'blob' });

    await dispatch(actions.getBinaryTaxFormSuccess(data, society_id));

    printOrDownload(data, type, `${exerciseId}.pdf`);
    return data;
  } catch (err) {
    await dispatch(actions.getBinaryTaxFormFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};
