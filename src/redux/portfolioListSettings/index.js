import { windev } from 'helpers/api';
import _ from 'lodash';
import actions from './actions';

export class PortfolioListSettingsService {
  static instance;

  static getInstance() {
    if (!PortfolioListSettingsService.instance) {
      PortfolioListSettingsService.instance = new PortfolioListSettingsService();
    }
    return PortfolioListSettingsService.instance;
  }

  getWalletList = () => async (dispatch) => {
    try {
      await dispatch(actions.getPortfolioAttempt());
      const response = await windev.makeApiCall('/wallet', 'get');
      const wallets = response.data ? response.data : [];
      await dispatch(actions.getPortfolioSuccess(wallets));
      return wallets;
    } catch (err) {
      await dispatch(actions.getPortfolioFail());
      return err;
    }
  };

  sendWallet = () => async (dispatch, getState) => {
    const state = getState();
    const portfolioForm = state.form.portfolioListSettingsForm.values;
    const { libelle } = portfolioForm;
    const societies = _.get(portfolioForm, 'societies', []);
    const list_society = societies.map(society => ({ id_society: society.value }));
    const params = {
      libelle,
      list_society
    };

    try {
      await dispatch(actions.sendPortfolioAttempt());
      const response = await windev.makeApiCall('/wallet', 'post', {}, params);
      const wallets = response.data ? response.data : [];
      await dispatch(actions.sendPortfolioSuccess(wallets));
      return wallets;
    } catch (err) {
      await dispatch(actions.sendPortfolioFail());
      return err;
    }
  };

  deleteWallets = () => async (dispatch, getState) => {
    const state = getState();
    const { selectedWallets } = state.portfolioListSettings;
    const { id_wallet } = selectedWallets[selectedWallets.length - 1];
    const params = {
      id_wallet
    };

    try {
      await dispatch(actions.deletePortfolioAttempt());
      const response = await windev.makeApiCall('/wallet', 'delete', params);
      const wallets = response.data ? response.data : [];
      await dispatch(actions.deletePortfolioSuccess(wallets));
      await dispatch(actions.resetAllWallets());
      dispatch(this.getWalletList());
      return wallets;
    } catch (err) {
      await dispatch(actions.deletePortfolioFail());
      return err;
    }
  };

  modifyWallet = () => async (dispatch, getState) => {
    const state = getState();
    const id_wallet = _.get(state, 'portfolioListSettings.walletSelectedLine', -1);
    const portfolioForm = state.form.portfolioListSettingsForm.values;
    const { libelle, societies } = portfolioForm;
    const list_society = societies.map(society => ({ id_society: society.value }));
    const params = {
      id_wallet,
      libelle,
      list_society
    };

    try {
      await dispatch(actions.modifyPortfolioAttempt());
      const response = await windev.makeApiCall('/wallet', 'put', {}, params);
      const wallets = response.data ? response.data : [];
      await dispatch(actions.modifyPortfolioSuccess(wallets));
      await dispatch(actions.selectWallet(undefined));
      await dispatch(actions.resetAllWallets());
      return wallets;
    } catch (err) {
      await dispatch(actions.modifyPortfolioFail());
      return err;
    }
  };
}


export default PortfolioListSettingsService.getInstance();
