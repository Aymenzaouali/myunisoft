import _ from 'lodash';
import {
  GET_PORTFOLIO_ATTEMPT,
  GET_PORTFOLIO_SUCCESS,
  GET_PORTFOLIO_FAIL,
  ADD_ALL_WALLETS,
  RESET_ALL_WALLETS,
  SELECT_WALLET,
  ADD_WALLET,
  REMOVE_WALLET
} from './constants';

const initialState = {
  settings_id: null,
  wallets: {},
  selectedWallets: [],
  walletSelectedLine: undefined
};

const portfolioListSettings = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_PORTFOLIO_ATTEMPT:
      return {
        ...state,
        wallets: {
          isLoading: true,
          isError: false,
          wallets_list: []
        }
      };
    case GET_PORTFOLIO_SUCCESS:
      return {
        ...state,
        wallets: {
          ...state.wallets,
          isLoading: false,
          isError: false,
          wallets_list: action.wallets
        }
      };
    case GET_PORTFOLIO_FAIL:
      return {
        ...state,
        wallets: {
          ...state.wallets,
          isLoading: false,
          isError: true
        }
      };
    case ADD_ALL_WALLETS:
      return {
        ...state,
        selectedWallets: _.get(state, 'wallets.wallets_list', []).filter(e => !e.blocked)
      };
    case RESET_ALL_WALLETS:
      return {
        ...state,
        selectedWallets: []
      };
    case SELECT_WALLET:
      const {
        walletSelectedLine
      } = action;

      return {
        ...state,
        walletSelectedLine
      };
    case ADD_WALLET: {
      return {
        ...state,
        selectedWallets: [..._.get(state, 'selectedWallets', []), action.walletId]
      };
    }
    case REMOVE_WALLET:
      return {
        ...state,
        selectedWallets: _.get(state, 'selectedWallets', []).filter(wallet => wallet.id_wallet !== action.walletId)
      };
    default: return state;
    }
  }
  return state;
};

export default portfolioListSettings;
