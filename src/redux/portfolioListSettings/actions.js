import {
  GET_PORTFOLIO_ATTEMPT,
  GET_PORTFOLIO_SUCCESS,
  GET_PORTFOLIO_FAIL,
  ADD_ALL_WALLETS,
  RESET_ALL_WALLETS,
  SELECT_WALLET,
  ADD_WALLET,
  REMOVE_WALLET,
  SEND_PORTFOLIO_ATTEMPT,
  SEND_PORTFOLIO_SUCCESS,
  SEND_PORTFOLIO_FAIL,
  MODIFY_PORTFOLIO_ATTEMPT,
  MODIFY_PORTFOLIO_SUCCESS,
  MODIFY_PORTFOLIO_FAIL,
  DELETE_PORTFOLIO_ATTEMPT,
  DELETE_PORTFOLIO_SUCCESS,
  DELETE_PORTFOLIO_FAIL
} from './constants';

const getPortfolioAttempt = () => ({
  type: GET_PORTFOLIO_ATTEMPT
});

const getPortfolioSuccess = wallets => ({
  type: GET_PORTFOLIO_SUCCESS,
  wallets
});

const getPortfolioFail = () => ({
  type: GET_PORTFOLIO_FAIL
});

const modifyPortfolioAttempt = () => ({
  type: MODIFY_PORTFOLIO_ATTEMPT
});

const modifyPortfolioSuccess = wallets => ({
  type: MODIFY_PORTFOLIO_SUCCESS,
  wallets
});

const modifyPortfolioFail = () => ({
  type: MODIFY_PORTFOLIO_FAIL
});

const sendPortfolioAttempt = () => ({
  type: SEND_PORTFOLIO_ATTEMPT
});

const sendPortfolioSuccess = wallets => ({
  type: SEND_PORTFOLIO_SUCCESS,
  wallets
});

const sendPortfolioFail = () => ({
  type: SEND_PORTFOLIO_FAIL
});

const deletePortfolioAttempt = () => ({
  type: DELETE_PORTFOLIO_ATTEMPT
});

const deletePortfolioSuccess = wallets => ({
  type: DELETE_PORTFOLIO_SUCCESS,
  wallets
});

const deletePortfolioFail = () => ({
  type: DELETE_PORTFOLIO_FAIL
});

const addAllWallets = () => ({
  type: ADD_ALL_WALLETS
});

const resetAllWallets = () => ({
  type: RESET_ALL_WALLETS
});

const selectWallet = walletSelectedLine => ({
  type: SELECT_WALLET,
  walletSelectedLine
});

const addWallet = walletId => ({
  type: ADD_WALLET,
  walletId
});

const removeWallet = walletId => ({
  type: REMOVE_WALLET,
  walletId
});

export default {
  getPortfolioAttempt,
  getPortfolioSuccess,
  getPortfolioFail,
  addAllWallets,
  resetAllWallets,
  selectWallet,
  addWallet,
  removeWallet,
  sendPortfolioAttempt,
  sendPortfolioSuccess,
  sendPortfolioFail,
  modifyPortfolioAttempt,
  modifyPortfolioSuccess,
  modifyPortfolioFail,
  deletePortfolioAttempt,
  deletePortfolioSuccess,
  deletePortfolioFail
};
