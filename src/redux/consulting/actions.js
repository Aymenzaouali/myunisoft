import {
  GET_ENTRIES_ATTEMPT,
  GET_ENTRIES_SUCCESS,
  GET_ENTRIES_FAIL,
  CHANGE_SORT,
  CHANGE_EXERCICE,
  CHANGE_ACCOUNT,
  CHANGE_DIARY,
  UPDATE_SELECT,
  ADD_ALL_CHECKBOX_ENTRIES,
  RESET_ALL_CHECKBOX_ENTRIES,
  SET_CURRENT_INDEX
} from './constants';

const getConsultationEntrieAttempt = societyId => ({
  type: GET_ENTRIES_ATTEMPT,
  societyId
});

const getConsultationEntrieError = societyId => ({
  type: GET_ENTRIES_FAIL,
  societyId
});

const changeExerciceConsultation = (societyId, start, end) => ({
  type: CHANGE_EXERCICE,
  societyId,
  start,
  end
});

const getConsultationEntrieSuccess = (societyId, payload, page, rows) => ({
  type: GET_ENTRIES_SUCCESS,
  payload: {
    societyId,
    data: payload.data,
    page,
    rows
  }
});

const changeSortConsultation = (societyId, key, direction) => ({
  type: CHANGE_SORT,
  societyId,
  key,
  direction
});

const putAccount = payload => ({
  type: CHANGE_ACCOUNT,
  payload
});

const putDiary = payload => ({
  type: CHANGE_DIARY,
  payload
});

const updateSelect = (societyId, newSelect, accountID) => ({
  type: UPDATE_SELECT,
  societyId,
  newSelect,
  accountID
});

/* action Add all select checkbox */
const addAllEntriesSelect = (societyId, accountID) => ({
  type: ADD_ALL_CHECKBOX_ENTRIES,
  societyId,
  accountID
});

/* action Remove all select checkbox */
const resetAllEntriesSelect = (societyId, accountID) => ({
  type: RESET_ALL_CHECKBOX_ENTRIES,
  societyId,
  accountID
});

const setCurrentIndex = index => ({
  type: SET_CURRENT_INDEX,
  index
});

export {
  getConsultationEntrieAttempt,
  getConsultationEntrieError,
  getConsultationEntrieSuccess,
  changeSortConsultation,
  changeExerciceConsultation,
  putAccount,
  putDiary,
  updateSelect,
  addAllEntriesSelect,
  resetAllEntriesSelect,
  setCurrentIndex
};
