import _ from 'lodash';
import moment from 'moment';
import { windev } from 'helpers/api';
import { getSelectedMonth, getSelectedYear } from 'helpers/date';
import {
  formatNumber
} from 'helpers/number';

import { change } from 'redux-form';
import {
  getConsultationEntrieAttempt,
  getConsultationEntrieSuccess,
  getConsultationEntrieError,
  changeSortConsultation,
  changeExerciceConsultation,
  putAccount,
  putDiary
} from './actions';

// Get consutation entries
export const getConsultationEntrie = (societyId, rows = 1000000, page = 0, prevnext) => async (dispatch, getState) => { // eslint-disable-line max-len
  try {
    const {
      form,
      consulting
    } = getState();

    const params = {
      offset: page,
      limit: rows,
      society_id: societyId
    };

    if (prevnext) params.prevnext = prevnext;

    if (!_.isUndefined(form[`${societyId}consultingFilter`])) {
      const {
        values: {
          dateStart, dateEnd, Account, selectLettrage
        }
      } = form[`${societyId}consultingFilter`];


      if (Account) {
        params.account_id = Account.account_id;
      } else {
        return;
      }

      if (selectLettrage) params.lettering = selectLettrage.value;

      const sort = _.get(consulting, `${societyId}.filter.sort`, { key: 'date', direction: 'asc' });
      params.sort = {
        column: sort.key,
        direction: sort.direction
      };

      params.search_date = {
        start_date: dateStart,
        end_date: dateEnd
      };
    }

    await dispatch(getConsultationEntrieAttempt(societyId));
    const response = await windev.makeApiCall('/account/entries', 'get', params, {});
    const { id, label, number } = response.data.thisAccount;
    const newAccount = {
      ...response.data.thisAccount,
      account_id: id,
      account_number: number,
      label,
      value: number
    };

    if (id > 0 || !_.isUndefined(id)) {
      await dispatch(getConsultationEntrieSuccess(societyId, response, page, rows));
      if (prevnext) dispatch(change(`${societyId}consultingFilter`, 'Account', newAccount));
    }
  } catch (err) {
    await dispatch(getConsultationEntrieError(societyId));
  }
};

export const changeExercice = (societyId, value) => async (dispatch) => {
  try {
    const start = value.sort(
      (a, b) => new Date(a.start_date).getTime() - new Date(b.start_date).getTime()
    );

    await dispatch(changeExerciceConsultation(
      societyId,
      moment(start[0].start_date).format('YYYYMMDD'),
      moment(start[value.length - 1].end_date).format('YYYYMMDD')
    ));
    await dispatch(getConsultationEntrie(societyId));
    return true;
  } catch (err) {
    return false;
  }
};

// Change filter for Consutation Table
export const changeSort = (societyId, key, direction) => async (dispatch) => {
  try {
    await dispatch(changeSortConsultation(societyId, key, direction));
    await dispatch(getConsultationEntrie(societyId));
    return true;
  } catch (err) {
    return false;
  }
};

export const changeAccount = (societyId, entry_id, line_entry_list) => async (dispatch) => {
  try {
    const params = {
      society_id: societyId,
      new_account_id: entry_id,
      line_entry_list
    };

    const payload = await windev.makeApiCall('/entries/account', 'put', {}, params);
    await dispatch(putAccount(payload));
    await dispatch(getConsultationEntrie(societyId));
    return true;
  } catch (err) {
    return false;
  }
};

export const changeDiary = (societyId, newDiary, line_entry_list) => async (dispatch) => {
  try {
    const params = {
      society_id: societyId,
      new_diary_id: newDiary,
      line_entry_list
    };

    const payload = await windev.makeApiCall('entry/diary', 'put', {}, params);
    await dispatch(putDiary(payload));
    await dispatch(getConsultationEntrie(societyId));
    return true;
  } catch (err) {
    return false;
  }
};

export const updateEntry = (societyId, line_entry_id) => async (dispatch) => {
  try {
    const params = {
      society_id: societyId,
      line_entry_id
    };

    const { data } = await windev.makeApiCall('/entry/line_entry', 'get', params, {});
    const newEntries = data.entry_list.map((e) => {
      const payment_type = {
        ...e.payment_type,
        value: _.get(e, 'payment_type.payment_type_id'),
        label: _.get(e, 'payment_type.name')
      };

      return ({
        ...e,
        account: e.account,
        label: e.label,
        deadline: e.deadline,
        comment: '',
        debit: e.debit && formatNumber(e.debit),
        credit: e.credit && formatNumber(e.credit),
        payment_type,
        piece: e.piece,
        piece2: e.piece2
      });
    });

    await dispatch(change(`${societyId}newAccounting`, 'entry_list', newEntries));
    await dispatch(change(`${societyId}newAccounting`, 'pj_list', data.pj_list));
    await dispatch(change(`${societyId}newAccounting`, 'comment', data.comment));
    await dispatch(change(`${societyId}newAccounting`, 'diary', data.diary));
    await dispatch(change(`${societyId}newAccounting`, 'day', moment(data.date_piece, 'YYYYMMDD').format('DD')));
    await dispatch(change(`${societyId}newAccounting`, 'month', getSelectedMonth(data.date_piece)));
    await dispatch(change(`${societyId}newAccounting`, 'year', getSelectedYear(data.date_piece)));
    await dispatch(change(`${societyId}newAccounting`, 'entry_id', data.entry_id));
    return true;
  } catch (err) {
    return false;
  }
};
