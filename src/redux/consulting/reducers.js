import moment from 'moment';
import _get from 'lodash/get';

import {
  GET_ENTRIES_ATTEMPT,
  GET_ENTRIES_SUCCESS,
  GET_ENTRIES_FAIL,
  CHANGE_SORT,
  CHANGE_EXERCICE,
  CHANGE_ACCOUNT,
  CHANGE_DIARY,
  UPDATE_SELECT,
  ADD_ALL_CHECKBOX_ENTRIES,
  RESET_ALL_CHECKBOX_ENTRIES,
  SET_CURRENT_INDEX
} from './constants';

const initialState = {
  currentIndex: 0
};

const consulting = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case UPDATE_SELECT: {
      return {
        ...state,
        [action.societyId]: {
          ...state[action.societyId],
          select: {
            ...state[action.societyId].select,
            [action.accountID]: action.newSelect
          }
        }
      };
    }
    case CHANGE_DIARY: {
      return {
        ...state
      };
    }
    case CHANGE_ACCOUNT: {
      return {
        ...state
      };
    }

    case CHANGE_EXERCICE: {
      return {
        ...state,
        [action.societyId]: {
          error: false,
          loading: true,
          load: false,
          filter: {
            ...state[action.societyId].filter,
            exercice: {
              start: action.start,
              end: action.end
            },
            date: {
              start: action.start,
              end: action.end
            }
          }
        }
      };
    }
    case CHANGE_SORT:
      return {
        ...state,
        [action.societyId]: {
          error: false,
          loading: true,
          load: false,
          filter: {
            ...state[action.societyId].filter,
            sort: {
              key: action.key,
              direction: action.direction
            }
          }
        }
      };


    case GET_ENTRIES_ATTEMPT:
      return {
        ...state,
        [action.societyId]: {
          ...state[action.societyId],
          rows_number: 20,
          pages_number: 0,
          loading: true,
          error: false,
          filter: {
            account: null,
            sort: {
              key: 'date',
              direction: 'asc'
            },
            date: {
              start: null,
              end: null
            },
            exercice: {
              start: '20111201',
              end: moment(new Date()).format('YYYYMMDD')
            }
          },
          select: {}
        }
      };
    case GET_ENTRIES_SUCCESS:
      const {
        data: {
          list_entries_line,
          rows_number,
          pages_number,
          total_debit,
          total_credit
        },
        societyId,
        page,
        rows
      } = action.payload;

      return {
        ...state,
        [societyId]: {
          ...state[societyId],
          entries: list_entries_line,
          rows_number,
          pages_number,
          total_debit,
          total_credit,
          page,
          rowsPerPage: rows,
          loading: false,
          load: true,
          error: false
        }
      };
    case GET_ENTRIES_FAIL:
      return {
        ...state,
        [action.societyId]: {
          ...state[action.societyId],
          entries: [],
          loading: false,
          load: true,
          error: true,
          filter: {
            account: null,
            sort: {
              key: 'date',
              direction: 'desc'
            },
            date: {
              start: null,
              end: null
            },
            exercice: {
              start: '20111201',
              end: moment(new Date()).format('YYYYMMDD')
            }
          }
        }
      };
    case ADD_ALL_CHECKBOX_ENTRIES: {
      return {
        ...state,
        [action.societyId]: {
          ...state[action.societyId],
          select: {
            ...state[action.societyId].select,
            [action.accountID]: _get(state, `${action.societyId}.entries`)
          }
        }
      };
    }
    case RESET_ALL_CHECKBOX_ENTRIES: {
      return {
        ...state,
        [action.societyId]: {
          ...state[action.societyId],
          select: {
            [action.accountID]: []
          }
        }
      };
    }
    case SET_CURRENT_INDEX: {
      return {
        ...state,
        currentIndex: action.index
      };
    }

    default: return state;
    }
  }
  return state;
};

export default consulting;
