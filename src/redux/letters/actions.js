import {
  GET_LETTERS_ATTEMPT,
  GET_LETTERS_SUCCESS,
  GET_LETTERS_FAIL,
  GET_DETAIL_LETTER_ATTEMPT,
  GET_DETAIL_LETTER_SUCCESS,
  GET_DETAIL_LETTER_FAIL,
  POST_LETTERS_ATTEMPT,
  POST_LETTERS_SUCCESS,
  POST_LETTERS_FAIL
} from './constants';

const getLettersAttempt = () => ({
  type: GET_LETTERS_ATTEMPT
});

const getLettersSuccess = letters => ({
  type: GET_LETTERS_SUCCESS,
  letters
});

const getLettersFail = () => ({
  type: GET_LETTERS_FAIL
});

const getDetailLetterAttempt = () => ({
  type: GET_DETAIL_LETTER_ATTEMPT
});

const getDetailLetterSuccess = letters => ({
  type: GET_DETAIL_LETTER_SUCCESS,
  letters
});

const getDetailLetterFail = () => ({
  type: GET_DETAIL_LETTER_FAIL
});

const postLettersAttempt = () => ({
  type: POST_LETTERS_ATTEMPT
});

const postLettersSuccess = letters => ({
  type: POST_LETTERS_SUCCESS,
  letters
});

const postLettersFail = () => ({
  type: POST_LETTERS_FAIL
});

export default {
  getDetailLetterAttempt,
  getDetailLetterFail,
  getDetailLetterSuccess,
  getLettersAttempt,
  getLettersFail,
  getLettersSuccess,
  postLettersAttempt,
  postLettersFail,
  postLettersSuccess
};
