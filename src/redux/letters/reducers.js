import {
  GET_LETTERS_ATTEMPT,
  GET_LETTERS_SUCCESS,
  GET_LETTERS_FAIL
} from './constants';

const initialState = {
  letters: []
};

const letters = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_LETTERS_ATTEMPT:
      return {
        ...state
      };
    case GET_LETTERS_SUCCESS:
      const { letters } = action;
      return {
        ...state,
        letters
      };
    case GET_LETTERS_FAIL:
      return {
        ...state
      };
    default:
      return state;
    }
  }
  return state;
};

export default letters;
