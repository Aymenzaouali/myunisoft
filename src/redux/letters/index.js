import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import { LETTERS_TABLE_NAME, getTableName } from 'assets/constants/tableName';
import tablesActions from 'redux/tables/actions';
import _ from 'lodash';
import actions from './actions';

export const getLetters = params => async (dispatch, getState) => {
  const state = getState();
  try {
    const society_id = _.get(state, 'navigation.id', false);
    await dispatch(actions.getLettersAttempt());
    const { data } = await windev.makeApiCall('/letter', 'get', params);
    await dispatch(actions.getLettersSuccess(data.letter_list));
    const tableName = getTableName(society_id, LETTERS_TABLE_NAME);
    await dispatch(
      tablesActions.setData(tableName, data.letter_list.map(l => ({ ...l, id: l.id_letter })))
    );
    return data;
  } catch (err) {
    await dispatch(actions.getLettersFail(err));
    await dispatch(handleError(err));
    return err;
  }
};

export const getLetter = idLetter => async (dispatch) => {
  try {
    const params = {
      id_letter: idLetter
    };
    await dispatch(actions.getDetailLetterAttempt());
    const { data } = await windev.makeApiCall('/letter', 'get', params);
    await dispatch(actions.getDetailLetterSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getDetailLetterFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const postLetters = payload => async (dispatch) => {
  // const state = getState();
  try {
    let body = [];
    let i = 0;
    while (typeof payload[`body${i}`] !== 'undefined') {
      body = [...body, payload[`body${i}`]];
      i += 1;
    }
    const params = {
      ...payload,
      body
    };
    await dispatch(actions.postLettersAttempt());
    const { data } = await windev.makeApiCall('/letter', 'post', {}, params);
    await dispatch(actions.postLettersSuccess(data));
    // const tableName = getTableName(society_id, LETTERS_TABLE_NAME);
    // await dispatch(tablesActions.setData(tableName, data.map(l => ({ ...l, id: l.id_letter }))));
    return data;
  } catch (err) {
    await dispatch(actions.postLettersFail());
    await dispatch(handleError(err));
    return err;
  }
};
