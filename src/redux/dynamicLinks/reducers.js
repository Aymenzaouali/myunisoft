import {
  GET_MEMBER_GROUP_INFO,
  START_LOADING_GROUP_INFO,
  STOP_LOADING_GROUP
} from './constants';

const initialState = {
  member_id: null,
  member_group: null,
  loading: false
};

const memberGroup = (state = initialState, action) => {
  switch (action.type) {
  case STOP_LOADING_GROUP:
    return {
      ...state,
      loading: false
    };
  case START_LOADING_GROUP_INFO:
    return {
      ...state,
      loading: true
    };
  case GET_MEMBER_GROUP_INFO:
    const { member_id, member_group } = action.payload;
    return {
      ...state,
      member_id,
      member_group
    };
  default: return state;
  }
};

export default memberGroup;
