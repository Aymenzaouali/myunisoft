import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';

import {
  setMemberGroup,
  startLoadingGroup,
  stopLoadingGroup
} from './actions';

export const getMember = () => async (dispatch) => {
  try {
    dispatch(startLoadingGroup());
    const { data: { id: member_id, nom: member_group } } = await windev.makeApiCall('/member_group/info', 'get', {});
    dispatch(setMemberGroup({ member_id, member_group }));
    dispatch(stopLoadingGroup());
  } catch (error) {
    dispatch(stopLoadingGroup());
    await dispatch(handleError(error));
  }
};
