import {
  GET_MEMBER_GROUP_INFO,
  START_LOADING_GROUP_INFO,
  STOP_LOADING_GROUP
} from './constants';

export const stopLoadingGroup = () => ({
  type: STOP_LOADING_GROUP
});

export const startLoadingGroup = () => ({
  type: START_LOADING_GROUP_INFO
});

export const setMemberGroup = memberGroup => ({
  type: GET_MEMBER_GROUP_INFO,
  payload: memberGroup
});
