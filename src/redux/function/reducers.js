import _ from 'lodash';
import {
  GET_GROUP_FUNCTION_ATTEMPT,
  GET_GROUP_FUNCTION_SUCCESS,
  GET_GROUP_FUNCTION_FAIL,

  ADD_FUNCTION,
  REMOVE_FUNCTION,

  RESET_ALL_FUNCTION,
  ADD_ALL_FUNCTION,

  SELECT_FUNCTION
} from './constants';

const initialState = {
  functions: [],
  list_selectedFunction: [],
  selectedFunction: undefined
};

const groupFunction = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_GROUP_FUNCTION_ATTEMPT:
      return {
        ...state,
        isLoading: true
      };
    case GET_GROUP_FUNCTION_SUCCESS:
      const { functions } = action;
      return {
        ...state,
        isLoading: false,
        functions
      };
    case GET_GROUP_FUNCTION_FAIL:
      return {
        ...state,
        isLoading: false
      };
    case ADD_FUNCTION: {
      const { func } = action;
      return {
        ...state,
        list_selectedFunction: [...state.list_selectedFunction, func]
      };
    }
    case REMOVE_FUNCTION: {
      const { func } = action;
      return {
        ...state,
        list_selectedFunction: _.get(state, 'list_selectedFunction', []).filter(e => e.id_group !== func.id_group)
      };
    }

    case RESET_ALL_FUNCTION: {
      return {
        ...state,
        list_selectedFunction: []
      };
    }

    case ADD_ALL_FUNCTION: {
      return {
        ...state,
        list_selectedFunction: _.get(state, 'functions', []).filter(e => !e.blocked)
      };
    }

    case SELECT_FUNCTION: {
      const { func } = action;
      return {
        ...state,
        selectedFunction: func
      };
    }

    default:
      return state;
    }
  }
  return state;
};

export default groupFunction;
