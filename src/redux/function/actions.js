import {
  GET_GROUP_FUNCTION_ATTEMPT,
  GET_GROUP_FUNCTION_SUCCESS,
  GET_GROUP_FUNCTION_FAIL,

  POST_GROUP_FUNCTION_ATTEMPT,
  POST_GROUP_FUNCTION_SUCCESS,
  POST_GROUP_FUNCTION_FAIL,

  PUT_GROUP_FUNCTION_ATTEMPT,
  PUT_GROUP_FUNCTION_SUCCESS,
  PUT_GROUP_FUNCTION_FAIL,

  DELETE_GROUP_FUNCTION_ATTEMPT,
  DELETE_GROUP_FUNCTION_SUCCESS,
  DELETE_GROUP_FUNCTION_FAIL,

  ADD_FUNCTION,
  REMOVE_FUNCTION,

  RESET_ALL_FUNCTION,
  ADD_ALL_FUNCTION,

  SELECT_FUNCTION
} from './constants';

export const getGroupFunctionAttempt = () => ({
  type: GET_GROUP_FUNCTION_ATTEMPT
});

export const getGroupFunctionSuccess = functions => ({
  type: GET_GROUP_FUNCTION_SUCCESS,
  functions
});

export const getGroupFunctionFail = () => ({
  type: GET_GROUP_FUNCTION_FAIL
});

export const postGroupFunctionAttempt = () => ({
  type: POST_GROUP_FUNCTION_ATTEMPT
});

export const postGroupFunctionSuccess = () => ({
  type: POST_GROUP_FUNCTION_SUCCESS
});

export const postGroupFunctionFail = () => ({
  type: POST_GROUP_FUNCTION_FAIL
});

export const putGroupFunctionAttempt = () => ({
  type: PUT_GROUP_FUNCTION_ATTEMPT
});

export const putGroupFunctionSuccess = () => ({
  type: PUT_GROUP_FUNCTION_SUCCESS
});

export const putGroupFunctionFail = () => ({
  type: PUT_GROUP_FUNCTION_FAIL
});

export const deleteGroupFunctionAttempt = () => ({
  type: DELETE_GROUP_FUNCTION_ATTEMPT
});

export const deleteGroupFunctionSuccess = () => ({
  type: DELETE_GROUP_FUNCTION_SUCCESS
});

export const deleteGroupFunctionFail = () => ({
  type: DELETE_GROUP_FUNCTION_FAIL
});

export const addFunction = func => ({
  type: ADD_FUNCTION,
  func
});

export const removeFunction = func => ({
  type: REMOVE_FUNCTION,
  func
});

export const resetAllFunction = () => ({
  type: RESET_ALL_FUNCTION

});

export const addAllFunction = () => ({
  type: ADD_ALL_FUNCTION
});

export const selectFunction = func => ({
  type: SELECT_FUNCTION,
  func
});
