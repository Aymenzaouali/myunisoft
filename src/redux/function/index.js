import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import {
  getGroupFunctionAttempt,
  getGroupFunctionSuccess,
  getGroupFunctionFail,

  postGroupFunctionAttempt,
  postGroupFunctionSuccess,
  postGroupFunctionFail,

  putGroupFunctionAttempt,
  putGroupFunctionSuccess,
  putGroupFunctionFail,

  deleteGroupFunctionAttempt,
  deleteGroupFunctionSuccess,
  deleteGroupFunctionFail
} from './actions';

export const getGroupFunction = () => async (dispatch) => {
  try {
    const params = {
      sort: {
        column: 'nb_users',
        direction: 'asc'
      }
    };
    await dispatch(getGroupFunctionAttempt());
    const { data } = await windev.makeApiCall('/fonctions', 'get', params);
    await dispatch(getGroupFunctionSuccess(data));
    return data;
  } catch (err) {
    await dispatch(getGroupFunctionFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const postGroupFunction = payload => async (dispatch) => {
  const { libelle } = payload;
  const body = [
    { libelle }
  ];
  try {
    await dispatch(postGroupFunctionAttempt());
    const { data } = await windev.makeApiCall('/fonctions', 'post', {}, body);
    await dispatch(postGroupFunctionSuccess(data));
    return data;
  } catch (err) {
    await dispatch(postGroupFunctionFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const putGroupFunction = (payload, idgroup) => async (dispatch) => {
  const { libelle } = payload;
  const body = [{
    id_users_groups: idgroup,
    libelle
  }];
  try {
    await dispatch(putGroupFunctionAttempt());
    const { data } = await windev.makeApiCall('/fonctions', 'put', {}, body);
    await dispatch(putGroupFunctionSuccess(data));
    return data;
  } catch (err) {
    await dispatch(putGroupFunctionFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteGroupFunction = payload => async (dispatch) => {
  const body = payload.map(func => ({
    id_user_groups: func.id_group
  }));
  try {
    await dispatch(deleteGroupFunctionAttempt());
    const { data } = await windev.makeApiCall('/fonctions', 'delete', {}, body);
    await dispatch(deleteGroupFunctionSuccess(data));
    return data;
  } catch (err) {
    await dispatch(deleteGroupFunctionFail());
    await dispatch(handleError(err));
    return err;
  }
};
