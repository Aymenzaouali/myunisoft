import {
  GET_COLLECTOR_BILLS_ATTEMPT,
  GET_COLLECTOR_BILLS_SUCCESS,
  GET_COLLECTOR_BILLS_FAIL,
  GET_BUDGET_INSIGHT_TOKEN_ATTEMPT,
  GET_BUDGET_INSIGHT_TOKEN_SUCCESS,
  GET_BUDGET_INSIGHT_TOKEN_FAIL,
  GET_ACCOUNTING_ATTEMPT,
  GET_ACCOUNTING_SUCCESS,
  GET_ACCOUNTING_FAIL
} from './constants';

const getCollectorFacturesAttempt = societyId => ({
  type: GET_COLLECTOR_BILLS_ATTEMPT,
  societyId
});

const getCollectorFacturesSuccess = (documentsList, societyId) => ({
  type: GET_COLLECTOR_BILLS_SUCCESS,
  societyId,
  documentsList
});

const getCollectorFacturesFail = societyId => ({
  type: GET_COLLECTOR_BILLS_FAIL,
  societyId
});

const getBudgetInsightTokenAttempt = () => ({
  type: GET_BUDGET_INSIGHT_TOKEN_ATTEMPT
});

const getBudgetInsightTokenSuccess = () => ({
  type: GET_BUDGET_INSIGHT_TOKEN_SUCCESS
});

const getBudgetInsightTokenFail = () => ({
  type: GET_BUDGET_INSIGHT_TOKEN_FAIL
});

const getAccountingAttempt = () => ({
  type: GET_ACCOUNTING_ATTEMPT
});

const getAccountingSuccess = () => ({
  type: GET_ACCOUNTING_SUCCESS
});

const getAccountingFail = () => ({
  type: GET_ACCOUNTING_FAIL
});

export default {
  getCollectorFacturesAttempt,
  getCollectorFacturesSuccess,
  getCollectorFacturesFail,
  getBudgetInsightTokenAttempt,
  getBudgetInsightTokenSuccess,
  getBudgetInsightTokenFail,
  getAccountingAttempt,
  getAccountingSuccess,
  getAccountingFail
};
