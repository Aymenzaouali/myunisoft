import _ from 'lodash';
import {
  GET_COLLECTOR_BILLS_SUCCESS,
  GET_COLLECTOR_BILLS_ATTEMPT,
  GET_COLLECTOR_BILLS_FAIL
} from './constants';

const initialState = {
  documentsList: {},
  selectedBill: {}
};

const collector = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_COLLECTOR_BILLS_ATTEMPT:
      return {
        ...state,
        documentsList: {
          ...state.documentsList,
          [action.societyId]: {
            ...state.documentsList[action.societyId],
            isLoading: true,
            isError: false,
            collector_array: []
          }
        }
      };
    case GET_COLLECTOR_BILLS_SUCCESS:
      return {
        ...state,
        documentsList: {
          ...state.documentsList,
          [action.societyId]: {
            ..._.get(state, `documentsList[${action.societyId}]`, {}),
            ...action.documentsList,
            isLoading: false,
            isError: false,
            collector_array: []
          }
        }
      };
    case GET_COLLECTOR_BILLS_FAIL:
      return {
        ...state,
        documentsList: {
          ...state.documentsList,
          [action.societyId]: {
            ...state.documentsList[action.societyId],
            isLoading: false,
            isError: true,
            collector_array: []
          }
        }
      };
    default:
      return state;
    }
  }
  return state;
};

export default collector;
