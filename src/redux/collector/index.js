import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import _ from 'lodash';
import actions from './actions';

export const getCollector = societyId => async (dispatch, getState) => {
  const state = getState();
  const society_id = societyId !== undefined ? societyId : _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getCollectorFacturesAttempt(society_id));
    const response = await windev.makeApiCall('/retriever/pastDocuments', 'get', { id_society: society_id });
    const { data } = response;
    await dispatch(actions.getCollectorFacturesSuccess(data, society_id));
    return response;
  } catch (err) {
    await dispatch(actions.getCollectorFacturesFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getbudgetInsightToken = societyId => async (dispatch) => {
  try {
    const response = await windev.makeApiCall('/retriever/token', 'get', { id_society: societyId });
    await dispatch(actions.getBudgetInsightTokenSuccess());
    const { data } = response;
    if (data.token === 'notFound' || data.token === 'forbidden') {
      try {
        await windev.makeApiCall('/retriever/user', 'post', { id_society: societyId }, {});
        await dispatch(actions.getBudgetInsightTokenSuccess());
        const { data: { token } } = await windev.makeApiCall('/retriever/token', 'get', { id_society: societyId });
        return token;
      } catch (error) {
        await dispatch(handleError(error));
        await dispatch(actions.getBudgetInsightTokenFail());
        return error;
      }
    }
    return data.token;
  } catch (error) {
    await dispatch(handleError(error));
    await dispatch(actions.getBudgetInsightTokenFail());
    return error;
  }
};

export const getAccounting = id_documents => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getAccountingAttempt());
    const { data } = await windev.makeApiCall('/retriever/accounting', 'post', {}, { id_documents });
    await dispatch(actions.getAccountingSuccess());
    await dispatch(getCollector(society_id));
    return { data };
  } catch (error) {
    await dispatch(handleError(error));
    await dispatch(actions.getAccountingFail());
    return error;
  }
};
