import { handleError } from 'common/redux/error';
import _ from 'lodash';
import { windev } from 'helpers/api';
import actions from './actions';

export const getCodeTva = () => async (dispatch, getState) => {
  const state = getState();
  try {
    const society_id = _.get(state, 'navigation.id', false);
    const { data } = await windev.makeApiCall('/vat_param', 'get', { society_id });
    await dispatch(actions.getCodeTvaSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getCodeTvaFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getVat = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getVatAttempt(society_id));
    const { data } = await windev.makeApiCall('/vat_param', 'get', { society_id });
    await dispatch(actions.getVatSuccess(data, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.getVatFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteVat = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  const vat_param = {
    vat_param: _.get(state, `tva.selectedVat.${society_id}`, false).map(
      item => ({ id: item })
    )
  };
  try {
    await dispatch(actions.deleteVatAttempt(society_id));
    const { data } = await windev.makeApiCall('/vat_param', 'delete', {}, vat_param);
    await dispatch(actions.deleteVatSuccess(data, society_id));
    await dispatch(getVat());
    return data;
  } catch (err) {
    await dispatch(actions.deleteVatFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getVatTaux = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getVatTauxAttempt(society_id));
    const { data } = await windev.makeApiCall('/vat_param/vat', 'get', { society_id });
    await dispatch(actions.getVatTauxSuccess(data, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.getVatTauxFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getVatType = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getVatTypeAttempt(society_id));
    const { data } = await windev.makeApiCall('/vat_param/vat_type', 'get', { society_id });
    await dispatch(actions.getVatTypeSuccess(data, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.getVatTypeFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getVatDue = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getVatDueAttempt(society_id));
    const { data } = await windev.makeApiCall('/vat_param/vat_exigibility', 'get', { society_id });
    await dispatch(actions.getVatDueSuccess(data, society_id));
    return data;
  } catch (err) {
    await dispatch(actions.getVatDueFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const createVat = (vat = {}, modify) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.createVatAttempt(society_id));
    const { data } = await windev.makeApiCall('/vat_param', modify ? 'put' : 'post', {}, { society_id, ...vat });
    await dispatch(actions.createVatSuccess(data, society_id));
    await dispatch(getVat());
    return data;
  } catch (err) {
    await dispatch(actions.createVatFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};
