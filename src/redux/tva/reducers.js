
import _ from 'lodash';
import {
  GET_CODE_TVA_SUCCESS,
  GET_CODE_TVA_FAIL,
  GET_VAT_ATTEMPT,
  GET_VAT_SUCCESS,
  GET_VAT_FAIL,
  ADD_VAT,
  REMOVE_VAT,
  ADD_ALL_VAT,
  SELECT_VAT,
  RESET_ALL_VAT,
  GET_VAT_DUE_SUCCESS,
  GET_VAT_DUE_FAIL,
  GET_VAT_TAUX_SUCCESS,
  GET_VAT_TAUX_FAIL,
  GET_VAT_TYPE_SUCCESS,
  GET_VAT_TYPE_FAIL
} from './constants';

const initialState = {
  codeTva: [],
  vat: {},
  selectedVat: [],
  vatSelectedLine: null,
  due: {},
  taux: {},
  type: {}
};

const tva = (state = initialState, action) => {
  const {
    societyId,
    vatId,
    vatList = [],
    data = []
  } = action;

  if (action && action.type) {
    switch (action.type) {
    case GET_VAT_TYPE_SUCCESS: {
      return {
        ...state,
        type: {
          ...state.type,
          [societyId]: data
        }
      };
    }
    case GET_VAT_TYPE_FAIL: {
      return {
        ...state,
        type: {
          ...state.type,
          [societyId]: []
        }
      };
    }
    case GET_VAT_TAUX_SUCCESS: {
      return {
        ...state,
        taux: {
          ...state.taux,
          [societyId]: data
        }
      };
    }
    case GET_VAT_TAUX_FAIL: {
      return {
        ...state,
        taux: {
          ...state.taux,
          [societyId]: []
        }
      };
    }
    case GET_VAT_DUE_SUCCESS: {
      return {
        ...state,
        due: {
          ...state.due,
          [societyId]: data
        }
      };
    }
    case GET_VAT_DUE_FAIL: {
      return {
        ...state,
        due: {
          ...state.due,
          [societyId]: []
        }
      };
    }
    case GET_CODE_TVA_SUCCESS: {
      return {
        ...state,
        codeTva: action.payload
      };
    }
    case GET_CODE_TVA_FAIL: {
      return {
        ...state,
        codeTva: []
      };
    }
    case GET_VAT_ATTEMPT: {
      return {
        ...state,
        vat: {
          ...state.vat,
          [societyId]: {
            isLoading: true,
            isError: false,
            list: []
          }
        }
      };
    }
    case GET_VAT_SUCCESS: {
      return {
        ...state,
        vat: {
          ...state.vat,
          [societyId]: {
            isLoading: false,
            isError: false,
            list: vatList
          }
        }
      };
    }
    case GET_VAT_FAIL: {
      return {
        ...state,
        vat: {
          ...state.vat,
          [societyId]: {
            isLoading: false,
            isError: true,
            list: []
          }
        }
      };
    }
    case ADD_VAT: {
      return {
        ...state,
        selectedVat: {
          ...state.selectedVat,
          [societyId]: [..._.get(state, `selectedVat.${societyId}`, []), vatId]
        }
      };
    }
    case REMOVE_VAT:
      return {
        ...state,
        selectedVat: {
          ...state.selectedVat,
          [societyId]: _.get(state, `selectedVat[${societyId}]`, []).filter(v => v !== vatId)
        }
      };
    case SELECT_VAT:
      const {
        vatSelectedLine
      } = action;

      return {
        ...state,
        vatSelectedLine
      };
    case RESET_ALL_VAT:
      return {
        ...state,
        selectedVat: {
          ...state.selectedVat,
          [societyId]: []
        }
      };
    case ADD_ALL_VAT:
      return {
        ...state,
        selectedVat: {
          ...state.selectedVat,
          [societyId]: _.get(state, `vat[${societyId}].list`, []).filter(v => !v.blocked).map(vatAvailable => vatAvailable.vat_param_id)
        }
      };
    default:
      return state;
    }
  }
  return state;
};

export default tva;
