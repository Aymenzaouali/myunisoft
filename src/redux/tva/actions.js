import {
  GET_CODE_TVA_SUCCESS,
  GET_CODE_TVA_FAIL,
  GET_VAT_SUCCESS,
  GET_VAT_ATTEMPT,
  GET_VAT_FAIL,
  DELETE_VAT_SUCCESS,
  DELETE_VAT_ATTEMPT,
  DELETE_VAT_FAIL,
  GET_VAT_TAUX_SUCCESS,
  GET_VAT_TAUX_ATTEMPT,
  GET_VAT_TAUX_FAIL,
  GET_VAT_TYPE_SUCCESS,
  GET_VAT_TYPE_ATTEMPT,
  GET_VAT_TYPE_FAIL,
  GET_VAT_DUE_SUCCESS,
  GET_VAT_DUE_ATTEMPT,
  GET_VAT_DUE_FAIL,
  CREATE_VAT_SUCCESS,
  CREATE_VAT_ATTEMPT,
  CREATE_VAT_FAIL,
  ADD_VAT,
  REMOVE_VAT,
  ADD_ALL_VAT,
  RESET_ALL_VAT,
  SELECT_VAT
} from './constants';

const getCodeTvaSuccess = payload => ({
  type: GET_CODE_TVA_SUCCESS,
  payload
});

const getCodeTvaFail = payload => ({
  type: GET_CODE_TVA_FAIL,
  payload
});

const getVatAttempt = societyId => ({
  type: GET_VAT_ATTEMPT,
  societyId
});

const getVatSuccess = (vatList, societyId) => ({
  type: GET_VAT_SUCCESS,
  vatList,
  societyId
});

const getVatFail = societyId => ({
  type: GET_VAT_FAIL,
  societyId
});

const deleteVatAttempt = societyId => ({
  type: DELETE_VAT_ATTEMPT,
  societyId
});

const deleteVatSuccess = (vatList, societyId) => ({
  type: DELETE_VAT_SUCCESS,
  vatList,
  societyId
});

const deleteVatFail = societyId => ({
  type: DELETE_VAT_FAIL,
  societyId
});

const getVatTauxAttempt = societyId => ({
  type: GET_VAT_TAUX_ATTEMPT,
  societyId
});

const getVatTauxSuccess = (data, societyId) => ({
  type: GET_VAT_TAUX_SUCCESS,
  data,
  societyId
});

const getVatTauxFail = societyId => ({
  type: GET_VAT_TAUX_FAIL,
  societyId
});

const getVatTypeAttempt = societyId => ({
  type: GET_VAT_TYPE_ATTEMPT,
  societyId
});

const getVatTypeSuccess = (data, societyId) => ({
  type: GET_VAT_TYPE_SUCCESS,
  data,
  societyId
});

const getVatTypeFail = societyId => ({
  type: GET_VAT_TYPE_FAIL,
  societyId
});

const getVatDueAttempt = societyId => ({
  type: GET_VAT_DUE_ATTEMPT,
  societyId
});

const getVatDueSuccess = (data, societyId) => ({
  type: GET_VAT_DUE_SUCCESS,
  data,
  societyId
});

const getVatDueFail = societyId => ({
  type: GET_VAT_DUE_FAIL,
  societyId
});

const createVatAttempt = societyId => ({
  type: CREATE_VAT_ATTEMPT,
  societyId
});

const createVatSuccess = (data, societyId) => ({
  type: CREATE_VAT_SUCCESS,
  data,
  societyId
});

const createVatFail = societyId => ({
  type: CREATE_VAT_FAIL,
  societyId
});

const addVat = (vatId, societyId) => ({
  type: ADD_VAT,
  societyId,
  vatId
});

const removeVat = (vatId, societyId) => ({
  type: REMOVE_VAT,
  societyId,
  vatId
});

const selectVat = vatSelectedLine => ({
  type: SELECT_VAT,
  vatSelectedLine
});

const addAllVat = societyId => ({
  type: ADD_ALL_VAT,
  societyId
});

const resetAllVat = societyId => ({
  type: RESET_ALL_VAT,
  societyId
});

export default {
  getCodeTvaSuccess,
  getCodeTvaFail,
  getVatAttempt,
  getVatSuccess,
  getVatFail,
  deleteVatAttempt,
  deleteVatSuccess,
  deleteVatFail,
  getVatTauxAttempt,
  getVatTauxSuccess,
  getVatTauxFail,
  getVatTypeAttempt,
  getVatTypeSuccess,
  getVatTypeFail,
  getVatDueAttempt,
  getVatDueSuccess,
  getVatDueFail,
  createVatAttempt,
  createVatSuccess,
  createVatFail,
  addVat,
  removeVat,
  selectVat,
  addAllVat,
  resetAllVat
};
