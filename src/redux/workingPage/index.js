import { handleError } from 'common/redux/error';
import _ from 'lodash';
import moment from 'moment';
import { roundNumber } from 'helpers/number';
import { windev } from 'helpers/api';
import { getTableName } from 'assets/constants/tableName';
import tables from 'redux/tables/actions';
import actions from './actions';

export const getWorksheetType = pageName => async (dispatch) => {
  /*
  ** 1 = CCA    
  ** 2 = PCA    
  ** 3 = FNP
  ** 4 = AAR
  ** 5 = AAE
  ** 6 = AP_PER
  ** 7 = CAP_CS    
  ** 8 = CAP_IT
  ** 9 = CAP_DIV
  ** 10 = CAP_INT CAP
   */
  try {
    const response = await windev.makeApiCall('/worksheet/type', 'get');
    const { data } = response;
    return data.worksheet_types.reduce((acc, type) => (
      type.code === pageName
        ? acc + type.id
        : acc), 0);
  } catch (err) {
    await dispatch(handleError(err));
    return err;
  }
};

export const getAccountNumber = (societyId, pageName) => async (dispatch, getState) => {
  const state = getState();
  const society_id = societyId || _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getAccountNumberAttempt(society_id, pageName));
    const response = await windev.makeApiCall('/worksheet/param', 'get', {
      code_worksheet: pageName
    });
    const { data } = response;
    await dispatch(actions.getAccountNumberSuccess(society_id, pageName, data));
  } catch (err) {
    await dispatch(handleError(err));
  }
};

export const postGenerate = (societyId, worksheetsIds, pageName) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = societyId || _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.generateAttempt(society_id, pageName));
    const response = await windev.makeApiCall('worksheet/generate', 'post', {
      id_society: society_id
    }, { worksheets: worksheetsIds });
    const { data } = response;
    await dispatch(actions.generateSuccess(society_id, pageName, data));
  } catch (err) {
    await dispatch(handleError(err));
  }
};

export const editOrCreateBill = (societyId, id_param_account_worksheet, pageName) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = societyId || _.get(state, 'navigation.id', false);
  const id_dossier_revision = _.get(state, `tabs.${society_id}.dadp.reviews.selectedId`);
  const tableName = getTableName(society_id, pageName);
  const billsList = _.get(state, `tables.${tableName}.data`, []);
  const createdRows = _.get(state, `tables.${tableName}.createdRows`, {});
  const editedRows = _.get(state, `tables.${tableName}.editedRows`, {});
  const createdBills = Object.values(createdRows).map(bill => ({
    ...bill,
    id: undefined,
    isCreated: undefined,
    total_excl_taxes: (pageName === 'CCA' || pageName === 'PCA' ? undefined : bill.total_excl_taxes),
    total_vat: (pageName === 'CCA' || pageName === 'PCA' ? undefined : bill.total_vat || bill.vat_total),
    total_incl_taxes: roundNumber(bill.total_incl_taxes),
    id_worksheet: bill.id,
    id_society: societyId,
    label: bill.label,
    id_dossier_revision,
    start_period: (pageName === 'CCA' || pageName === 'PCA' ? moment(bill.start_period).format('YYYYMMDD') : undefined),
    end_period: (pageName === 'CCA' || pageName === 'PCA' ? moment(bill.end_period).format('YYYYMMDD') : undefined),
    id_param_account_worksheet,
    id_account: bill.worksheet_account.account_id || bill.worksheet_account.id
  }));
  const updatedBills = Object.values(editedRows).map(bill => ({
    id_worksheet: bill.bill_id,
    id_society: society_id,
    id_account: _.get(bill, 'worksheet_account.id') || _.get(bill, 'worksheet_account.account_id'),
    date: moment(bill.date).format('YYYYMMDD'),
    label: bill.label,
    total_excl_taxes: (pageName === 'CCA' || pageName === 'PCA' ? undefined : bill.total_excl_taxes),
    total_vat: (pageName === 'CCA' || pageName === 'PCA' ? undefined : bill.total_vat || bill.vat_total),
    total_incl_taxes: bill.total_incl_taxes,
    id_param_account_worksheet: bill.id_param_account_worksheet || id_param_account_worksheet,
    start_period: moment(bill.start_period).format('YYYYMMDD'),
    end_period: moment(bill.end_period).format('YYYYMMDD'),
    id_dossier_revision
  }));
  try {
    let response;
    await dispatch(actions.editOrCreateBillAttempt(society_id, pageName));
    if (updatedBills.length > 0) {
      response = await windev.makeApiCall('/worksheet/bills', 'put', {}, { bills: updatedBills });
    }
    if (createdBills.length > 0) {
      response = await windev.makeApiCall('/worksheet/bills', 'post', {}, { bills: createdBills });

      const { data: { bills } } = response;
      bills.forEach((bill) => {
        dispatch(tables.editRow(tableName, {
          ...bill,
          id: bill.bill_id,
          bill_id: bill.id_worksheet
        }));
      });
    }
    await dispatch(actions.editOrCreateBillSuccess(society_id, pageName));
    billsList.forEach((bill) => {
      if (updatedBills.some(updatedBill => bill.bill_id === updatedBill.id_worksheet)) {
        dispatch(tables.editRow(tableName, {
          ...bill,
          generated: false
        }));
      }
    });

    await dispatch(tables.save(tableName));
    return response;
  } catch (err) {
    await dispatch(actions.editOrCreateBillFail(society_id, pageName));
    await dispatch(handleError(err));
    return err;
  }
};

export const getBills = (societyId,
  id_param_account_worksheet,
  pageName) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = societyId || _.get(state, 'navigation.id');
  const tableName = getTableName(society_id, pageName);
  const id_dossier_revision = _.get(state, `tabs.${society_id}.dadp.reviews.selectedId`);
  const limit = 1000000;
  const page = 0;
  const offset = 0;
  try {
    const actionOpts = { limit, page };
    await dispatch(actions.getBillsDataAttempt(society_id, actionOpts, pageName));
    const typeId = await dispatch(getWorksheetType(pageName));
    const response = await windev.makeApiCall('/worksheet/bills', 'get', {
      id_society: societyId,
      id_param_account_worksheet,
      id_worksheet_type: typeId,
      id_dossier_revision,
      limit,
      offset
    });
    const { data } = response;
    await dispatch(actions.getBillsDataSuccess(data, society_id, actionOpts, pageName));
    await dispatch(tables.setData(tableName, data.bills.map(b => ({ id: b.bill_id, ...b }))));
  } catch (err) {
    await dispatch(actions.getBillsDataFail(society_id, pageName));
    await dispatch(handleError(err));
  }
};


export const deleteBills = (societyId, bill_id, pageName) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = societyId || _.get(state, 'navigation.id', false);
  const tableName = getTableName(society_id, pageName);
  const selectedRows = _.get(state, `tables.${tableName}.selectedRows`, {});
  const createdRows = _.get(state, `tables.${tableName}.createdRows`, {});

  const worksheets = Object.keys(selectedRows).reduce((acc, key) => {
    if (!createdRows[key] && selectedRows[key]) {
      acc.push({ id_worksheet: parseInt(key, 10) });
    }

    return acc;
  }, []);
  try {
    await dispatch(actions.deleteBillsAttempt(society_id, bill_id, pageName));
    const response = await windev.makeApiCall('/worksheet/bills', 'delete', {}, { worksheets });
    if (response.status === 200) {
      await dispatch(actions.deleteBillsSuccess(society_id, bill_id, pageName));
      await dispatch(tables.deleteRows(tableName));
      await dispatch(tables.save(tableName));
    } else {
      await dispatch(actions.deleteBillsFail(society_id, bill_id, pageName));
    }
    return response;
  } catch (err) {
    await dispatch(handleError(err));
    return err;
  }
};

export const getComments = worksheet_id => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');

  try {
    await dispatch(actions.getCommentsAttempt(society_id));
    const { data } = await windev.makeApiCall(
      '/comments',
      'get',
      {
        worksheet_id
      }
    );
    await dispatch(actions.getCommentsSuccess({
      society_id,
      comments: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.getCommentsFail(society_id));
    return error;
  }
};

export const getGlobalComments = diligence_id => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');

  try {
    await dispatch(actions.getCommentsAttempt(society_id));
    const { data } = await windev.makeApiCall(
      '/comments',
      'get',
      {
        diligence_id
      }
    );
    await dispatch(actions.getCommentsSuccess({
      society_id,
      comments: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.getCommentsFail(society_id));
    return error;
  }
};

export const postWorksheetComment = (comment, worksheet_id) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');

  try {
    await dispatch(actions.postCommentAttempt(society_id));

    const {
      body,
      attachTo
    } = comment;

    const { data } = await windev.makeApiCall(
      '/comments',
      'post',
      {},
      {
        location: 'WORKSHEET',
        worksheet_id,
        society_id,
        comment: body,
        attachTo
      }
    );

    await dispatch(actions.postCommentSuccess({
      society_id,
      comment: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.postCommentFail(society_id));
    return error;
  }
};

export const postGlobalWorksheetComment = (comment, diligence_id) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');
  try {
    await dispatch(actions.postCommentAttempt(society_id));

    const {
      body,
      attachTo
    } = comment;

    const { data } = await windev.makeApiCall(
      '/comments',
      'post',
      {},
      {
        location: 'DILIGENCE',
        diligence_id,
        society_id,
        comment: body,
        attachTo
      }
    );

    await dispatch(actions.postCommentSuccess({
      society_id,
      comment: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.postCommentFail(society_id));
    return error;
  }
};

export const putWorksheetComment = (payload, worksheet_id) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');

  const body = {
    location: 'WORKSHEET',
    comment: payload.body,
    worksheet_id
  };

  try {
    await dispatch(actions.putCommentAttempt(society_id));
    const { data } = await windev.makeApiCall(`/comments/${payload.id}`, 'put', {}, body);
    await dispatch(actions.putCommentSuccess({ society_id, comment: data }));
    return data;
  } catch (error) {
    console.log({ error });
    await dispatch(actions.putCommentFail(society_id));
    return error;
  }
};

export const addPJ = (files, object_id, pageName) => async (dispatch, getState) => {
  const state = getState();
  const id_param_account_worksheet = _.get(state, 'form.AccountNumberWorksheet.values.accountNumberField', 0);
  const society_id = _.get(state, 'navigation.id');
  try {
    const params = {
      location: 'worksheet',
      filename_extension: files[0].name,
      object_id
    };

    await dispatch(actions.addPjAttempt());
    const { data } = await windev.makeApiCall('/document/add_all_types', 'post', params, files[0],
      { headers: { 'Content-Type': 'application/octet-stream', 'society-id': society_id } });
    await dispatch(actions.addPjSuccess());
    await dispatch(getBills(society_id, id_param_account_worksheet, pageName));
    return data;
  } catch (err) {
    await dispatch(actions.addPjFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const putGlobalWorksheetComment = (diligence_id, comment) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');

  const {
    body,
    id
  } = comment;

  try {
    await dispatch(actions.putCommentAttempt(society_id));
    const { data } = await windev.makeApiCall(
      `/comments/${id}`,
      'put',
      {},
      {
        location: 'DILIGENCE',
        diligence_id,
        comment: body
      }
    );
    await dispatch(actions.putCommentSuccess({
      society_id,
      comment: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.putCommentFail(society_id));
    return error;
  }
};
