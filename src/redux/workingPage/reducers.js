import _ from 'lodash';
import {
  GET_BILLS_DATA_ATTEMPT,
  GET_BILLS_DATA_SUCCESS,
  GET_BILLS_DATA_FAIL,
  DELETE_BILLS_SUCCESS,
  DELETE_BILLS_FAIL,
  DELETE_BILLS_ATTEMPT,
  GET_ACCOUNT_NUMBER_ATTEMPT,
  GET_ACCOUNT_NUMBER_SUCCESS,
  EDIT_OR_CREATE_SUCCESS,
  EDIT_OR_CREATE_ATTEMPT,
  EDIT_OR_CREATE_FAIL,
  GET_COMMENTS_ATTEMPT,
  GET_COMMENTS_FAIL,
  GET_COMMENTS_SUCCESS,
  POST_COMMENT_ATTEMPT,
  POST_COMMENT_FAIL,
  POST_COMMENT_SUCCESS,
  PUT_COMMENT_FAIL,
  PUT_COMMENT_ATTEMPT,
  PUT_COMMENT_SUCCESS
} from './constants';

const initialState = {
  billsList: {},
  selectedBill: {},
  billSelected: '',
  accountNumber: [],
  updatedBills: {},
  entries: {}
};

const workingPage = (state = initialState, action) => {
  switch (action.type) {
  case GET_BILLS_DATA_FAIL:
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.societyId]: {
          [action.pageName]: {
            ..._.get(state, `billsList[${action.societyId}][${action.pageName}]`),
            isLoading: false,
            isError: true
          }
        }
      }
    };
  case GET_BILLS_DATA_ATTEMPT:
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.societyId]: {
          [action.pageName]: {
            ..._.get(state, `billsList[${action.societyId}][${action.pageName}]`),
            isLoading: true,
            isError: false,
            ...action.opts

          }
        }
      }
    };
  case GET_BILLS_DATA_SUCCESS:
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.societyId]: {
          [action.pageName]: {
            ...action.data,
            isLoading: false,
            isError: false,
            ...action.opts
          }
        }
      }
    };
  case DELETE_BILLS_SUCCESS:
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.societyId]: {
          [action.pageName]: {
            ..._.get(state, `billsList[${action.societyId}][${action.pageName}]`, {}),
            ...action.billsList,
            isLoading: false,
            isError: false
          }
        }
      },
      selectedBill: {}
    };
  case DELETE_BILLS_FAIL:
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.societyId]: {
          [action.pageName]: {
            ..._.get(state, `billsList[${action.societyId}][${action.pageName}]`, {}),
            ...action.billsList,
            isLoading: false,
            isError: true
          }
        }
      }
    };
  case DELETE_BILLS_ATTEMPT:
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.societyId]: {
          [action.pageName]: {
            ..._.get(state, `billsList[${action.societyId}][${action.pageName}]`, {}),
            isLoading: true,
            isError: false
          }
        }
      }
    };
  case GET_ACCOUNT_NUMBER_ATTEMPT:
    return {
      ...state
    };
  case GET_ACCOUNT_NUMBER_SUCCESS:
    const {
      accountNumber
    } = action;

    return {
      ...state,
      accountNumber: {
        [action.societyId]: {
          [action.pageName]: [accountNumber]
        }
      }
    };
  case EDIT_OR_CREATE_SUCCESS:
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.societyId]: {
          [action.pageName]: {
            ..._.get(state, `billsList[${action.societyId}][${action.pageName}]`),
            isLoading: false,
            isError: false
          }
        }
      }
    };
  case EDIT_OR_CREATE_ATTEMPT:
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.societyId]: {
          [action.pageName]: {
            ..._.get(state, `billsList[${action.societyId}][${action.pageName}]`),
            isLoading: true,
            isError: false

          }
        }
      }
    };
  case EDIT_OR_CREATE_FAIL:
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.societyId]: {
          [action.pageName]: {
            ..._.get(state, `billsList[${action.societyId}][${action.pageName}]`),
            isLoading: false,
            isError: true
          }
        }
      }
    };

  case GET_COMMENTS_ATTEMPT: {
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.society_id]: {
          ...state.billsList[action.society_id],
          comments: []
        }
      }
    };
  }
  case GET_COMMENTS_FAIL: {
    return {
      ...state,
      billsList: {
        ...state.billsList,
        [action.society_id]: {
          ...state.billsList[action.society_id],
          comments: []
        }
      }
    };
  }
  case GET_COMMENTS_SUCCESS: {
    const {
      comments,
      society_id
    } = action.payload;

    return {
      ...state,
      billsList: {
        ...state.entries,
        [society_id]: {
          ...state.billsList[society_id],
          comments
        }
      }
    };
  }

  case POST_COMMENT_ATTEMPT: {
    return state;
  }
  case POST_COMMENT_FAIL: {
    return state;
  }
  case POST_COMMENT_SUCCESS: {
    const {
      comment,
      society_id
    } = action.payload;

    return {
      ...state,
      entries: {
        ...state.entries,
        [society_id]: {
          ...state.entries[society_id],
          comments: comment
        }
      }
    };
  }
  case PUT_COMMENT_ATTEMPT: {
    return state;
  }
  case PUT_COMMENT_FAIL: {
    return state;
  }
  case PUT_COMMENT_SUCCESS: {
    const {
      comment,
      society_id
    } = action.payload;

    return {
      ...state,
      entries: {
        ...state.entries,
        [society_id]: {
          ...state.entries[society_id],
          comments: comment
        }
      }
    };
  }
  default:
    return state;
  }
};

export default workingPage;
