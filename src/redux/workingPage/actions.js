import {
  GET_BILLS_DATA_ATTEMPT,
  GET_BILLS_DATA_SUCCESS,
  GET_BILLS_DATA_FAIL,
  DELETE_BILLS_ATTEMPT,
  DELETE_BILLS_SUCCESS,
  DELETE_BILLS_FAIL,
  GET_ACCOUNT_NUMBER_SUCCESS,
  GET_ACCOUNT_NUMBER_ATTEMPT,
  GENERATE_SUCCESS,
  GENERATE_ATTEMPT,
  EDIT_OR_CREATE_SUCCESS,
  EDIT_OR_CREATE_ATTEMPT,
  EDIT_OR_CREATE_FAIL,
  GET_COMMENTS_ATTEMPT,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAIL,

  POST_COMMENT_ATTEMPT,
  POST_COMMENT_FAIL,
  POST_COMMENT_SUCCESS,

  PUT_COMMENT_ATTEMPT,
  PUT_COMMENT_FAIL,
  PUT_COMMENT_SUCCESS,

  ADD_PJ_ATTEMPT,
  ADD_PJ_SUCCESS,
  ADD_PJ_FAIL
} from './constants';

const getBillsDataAttempt = (societyId, opts, pageName) => ({
  type: GET_BILLS_DATA_ATTEMPT,
  societyId,
  opts,
  pageName
});

const getBillsDataSuccess = (data, societyId, opts, pageName) => ({
  type: GET_BILLS_DATA_SUCCESS,
  pageName,
  data,
  opts,
  societyId
});

const getBillsDataFail = (societyId, pageName) => ({
  type: GET_BILLS_DATA_FAIL,
  societyId,
  pageName
});

const deleteBillsAttempt = (societyId, bills, pageName) => ({
  type: DELETE_BILLS_ATTEMPT,
  societyId,
  bills,
  pageName
});

const deleteBillsSuccess = (societyId, bills, pageName) => ({
  type: DELETE_BILLS_SUCCESS,
  societyId,
  bills,
  pageName
});

const deleteBillsFail = (societyId, bills, pageName) => ({
  type: DELETE_BILLS_FAIL,
  societyId,
  bills,
  pageName
});

const getAccountNumberSuccess = (societyId, pageName, accountNumber) => ({
  type: GET_ACCOUNT_NUMBER_SUCCESS,
  societyId,
  pageName,
  accountNumber
});

const getAccountNumberAttempt = (societyId, pageName) => ({
  type: GET_ACCOUNT_NUMBER_ATTEMPT,
  societyId,
  pageName
});

const generateSuccess = (societyId, pageName, bills) => ({
  type: GENERATE_SUCCESS,
  societyId,
  pageName,
  bills
});

const generateAttempt = (societyId, pageName, bills) => ({
  type: GENERATE_ATTEMPT,
  societyId,
  pageName,
  bills
});

const editOrCreateBillSuccess = (societyId, pageName) => ({
  type: EDIT_OR_CREATE_SUCCESS,
  societyId,
  pageName
});

const editOrCreateBillAttempt = (societyId, pageName) => ({
  type: EDIT_OR_CREATE_ATTEMPT,
  societyId,
  pageName
});

const editOrCreateBillFail = (societyId, pageName) => ({
  type: EDIT_OR_CREATE_FAIL,
  societyId,
  pageName
});

const getCommentsAttempt = society_id => ({
  type: GET_COMMENTS_ATTEMPT,
  society_id
});
const getCommentsSuccess = payload => ({
  type: GET_COMMENTS_SUCCESS,
  payload
});
const getCommentsFail = society_id => ({
  type: GET_COMMENTS_FAIL,
  society_id
});

const postCommentAttempt = society_id => ({
  type: POST_COMMENT_ATTEMPT,
  society_id
});
const postCommentSuccess = payload => ({
  type: POST_COMMENT_SUCCESS,
  payload
});
const postCommentFail = society_id => ({
  type: POST_COMMENT_FAIL,
  society_id
});

const putCommentAttempt = society_id => ({
  type: PUT_COMMENT_ATTEMPT,
  society_id
});

const putCommentSuccess = payload => ({
  type: PUT_COMMENT_SUCCESS,
  payload
});

const putCommentFail = society_id => ({
  type: PUT_COMMENT_FAIL,
  society_id
});

const addPjAttempt = () => ({
  type: ADD_PJ_ATTEMPT
});

const addPjSuccess = () => ({
  type: ADD_PJ_SUCCESS
});

const addPjFail = () => ({
  type: ADD_PJ_FAIL
});

export default {
  getBillsDataAttempt,
  getBillsDataSuccess,
  getBillsDataFail,
  deleteBillsAttempt,
  deleteBillsSuccess,
  deleteBillsFail,
  getAccountNumberSuccess,
  getAccountNumberAttempt,
  generateAttempt,
  generateSuccess,
  editOrCreateBillSuccess,
  editOrCreateBillAttempt,
  editOrCreateBillFail,
  getCommentsAttempt,
  getCommentsFail,
  getCommentsSuccess,

  putCommentAttempt,
  putCommentSuccess,
  putCommentFail,

  postCommentAttempt,
  postCommentSuccess,
  postCommentFail,

  addPjAttempt,
  addPjFail,
  addPjSuccess
};
