import {
  getBundle,
  sendBundle,
  sendEdiBundle,
  GET_DEADLINE_ATTEMPT,
  GET_DEADLINE_SUCCESS,
  GET_DEADLINE_FAIL,
  SET_DEADLINE_ATTEMPT,
  SET_DEADLINE_SUCCESS,
  SET_DEADLINE_FAIL,
  GET_BINARY_TAX_FORM_ATTEMPT,
  GET_BINARY_TAX_FORM_SUCCESS,
  GET_BINARY_TAX_FORM_FAIL,
  SELECT_FORM,
  SET_DEADLINE
} from './constants';

const getActions = {
  getAttempt: (bundleActionType, societyId) => ({
    type: getBundle[bundleActionType].GET_ATTEMPT,
    bundleActionType,
    societyId
  }),
  getSuccess: (bundleActionType, forms, societyId) => ({
    type: getBundle[bundleActionType].GET_SUCCESS,
    bundleActionType,
    societyId,
    forms
  }),
  getFail: (bundleActionType, societyId) => ({
    type: getBundle[bundleActionType].GET_FAIL,
    bundleActionType,
    societyId
  })
};

const sendActions = {
  sendAttempt: (bundleActionType, societyId) => ({
    type: sendBundle[bundleActionType].SEND_ATTEMPT,
    bundleActionType,
    societyId
  }),
  sendSuccess: (bundleActionType, forms, societyId) => ({
    type: sendBundle[bundleActionType].SEND_SUCCESS,
    bundleActionType,
    societyId,
    forms
  }),
  sendFail: (bundleActionType, societyId) => ({
    type: sendBundle[bundleActionType].SEND_FAIL,
    bundleActionType,
    societyId
  })
};

const sendEdiActions = {
  sendEdiAttempt: (bundleActionType, societyId) => ({
    type: sendEdiBundle[bundleActionType].SEND_EDI_ATTEMPT,
    bundleActionType,
    societyId
  }),
  sendEdiSuccess: (bundleActionType, forms, societyId) => ({
    type: sendEdiBundle[bundleActionType].SEND_EDI_SUCCESS,
    bundleActionType,
    societyId,
    forms
  }),
  sendEdiFail: (bundleActionType, societyId) => ({
    type: sendEdiBundle[bundleActionType].SEND_EDI_FAIL,
    bundleActionType,
    societyId
  })
};

const getDeadlineAttempt = societyId => ({
  type: GET_DEADLINE_ATTEMPT,
  societyId
});

const getDeadlineSuccess = (societyId, deadlines) => ({
  type: GET_DEADLINE_SUCCESS,
  societyId,
  deadlines
});

const getDeadlineFail = societyId => ({
  type: GET_DEADLINE_FAIL,
  societyId
});

const setDeadlineAttempt = societyId => ({
  type: SET_DEADLINE_ATTEMPT,
  societyId
});

const setDeadlineSuccess = (societyId, deadline) => ({
  type: SET_DEADLINE_SUCCESS,
  societyId,
  deadline
});

const setDeadlineFail = societyId => ({
  type: SET_DEADLINE_FAIL,
  societyId
});

const selectFormId = (societyId, form_id) => ({
  type: SELECT_FORM,
  societyId,
  form_id
});

const setDeadline = (deadline, societyId) => ({
  type: SET_DEADLINE,
  societyId,
  deadline
});

const getBinaryTaxFormAttempt = societyId => ({
  type: GET_BINARY_TAX_FORM_ATTEMPT,
  societyId
});

const getBinaryTaxFormSuccess = (file, societyId) => ({
  type: GET_BINARY_TAX_FORM_SUCCESS,
  societyId,
  file
});

const getBinaryTaxFormFail = societyId => ({
  type: GET_BINARY_TAX_FORM_FAIL,
  societyId
});

export default {
  getActions,
  sendActions,
  sendEdiActions,

  getDeadlineAttempt,
  getDeadlineSuccess,
  getDeadlineFail,
  setDeadlineAttempt,
  setDeadlineSuccess,
  setDeadlineFail,

  getBinaryTaxFormAttempt,
  getBinaryTaxFormSuccess,
  getBinaryTaxFormFail,

  selectFormId,
  setDeadline
};
