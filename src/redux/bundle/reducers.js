import {
  getBundle,
  SELECT_FORM,

  GET_DEADLINE_ATTEMPT,
  GET_DEADLINE_SUCCESS,
  GET_DEADLINE_FAIL,
  SET_DEADLINE_ATTEMPT,
  SET_DEADLINE_SUCCESS,
  SET_DEADLINE_FAIL
} from './constants';

const initialeState = {};

const bundle = (state = initialeState, action) => {
  const {
    societyId,
    forms,
    bundleActionType = 'isAdvance',
    form_id,
    deadlines,
    deadline
  } = action;

  switch (action.type) {
  case GET_DEADLINE_ATTEMPT:
    return {
      ...state,
      deadline: {
        [societyId]: {
          list: [],
          isLoading: false,
          isError: false
        }
      }
    };
  case GET_DEADLINE_SUCCESS:
    return {
      ...state,
      deadline: {
        [societyId]: {
          list: deadlines,
          isLoading: false,
          isError: false
        }
      }
    };
  case GET_DEADLINE_FAIL:
    return {
      ...state,
      deadline: {
        [societyId]: {
          list: [],
          isLoading: false,
          isError: false
        }
      }
    };
  case SET_DEADLINE_ATTEMPT:
    return {
      ...state,
      selectedDeadline: {
        [societyId]: {
          deadline: null
        }
      }
    };
  case SET_DEADLINE_SUCCESS:
    return {
      ...state,
      selectedDeadline: {
        [societyId]: {
          deadline
        }
      }
    };
  case SET_DEADLINE_FAIL:
    return {
      ...state,
      selectedDeadline: {
        [societyId]: {
          deadline: null
        }
      }
    };
  case getBundle[bundleActionType].GET_ATTEMPT:
    return {
      ...state,
      [bundleActionType]: {
        [societyId]: {
          forms: [],
          isLoading: true,
          isError: false
        }
      }
    };
  case getBundle[bundleActionType].GET_SUCCESS:
    return {
      ...state,
      [bundleActionType]: {
        [societyId]: {
          forms,
          isLoading: false,
          isError: false
        }
      }
    };
  case getBundle[bundleActionType].GET_FAIL:
    return {
      ...state,
      [bundleActionType]: {
        [societyId]: {
          forms: [],
          isLoading: false,
          isError: true
        }
      }
    };
  case SELECT_FORM:
    return {
      ...state,
      selectedFormId: {
        [societyId]: form_id
      }
    };
  default:
    return state;
  }
};

export default bundle;
