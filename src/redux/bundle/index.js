import { get as _get, isEmpty as _isEmpty } from 'lodash';
import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import { initialize, getFormValues } from 'redux-form';
import { printOrDownload } from 'helpers/file';
import actions from './actions';

const availableForms = {
  isAdvance: ['2571'],
  isSolde: ['2572'],
  cvaeSolde: ['1329DEF'],
  cvaeAdvance: ['1329AC'],
  rentDeclarations: ['DECLOYER']
};

const getBundleActionType = (code_sheet_group) => {
  switch (code_sheet_group) {
  case 'ACOMPTE-IS':
    return 'isAdvance';
  case 'SOLDE-IS':
    return 'isSolde';
  case 'SOLDE-CVAE':
    return 'cvaeSolde';
  case 'ACOMPTE-CVAE':
    return 'cvaeAdvance';
  case 'DECLOYER':
    return 'rentDeclarations';
  default:
    return 'isAdvance';
  }
};

export const getDeadline = args => async (dispatch, getState) => {
  const state = getState();
  const { code_sheet_group, exercice_id } = args;
  const society_id = _get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getDeadlineAttempt(society_id));
    const { data } = await windev.makeApiCall('/liasse/deadline', 'get', {
      code_sheet_group,
      exercice_id
    });
    await dispatch(actions.getDeadlineSuccess(society_id, data));
    await dispatch(actions.setDeadlineSuccess(society_id, _get(data, '[0]', null)));
    return true;
  } catch (error) {
    await dispatch(actions.getDeadlineFail(society_id));
    await dispatch(actions.setDeadlineFail(society_id));
    await dispatch(handleError(error));
    throw error;
  }
};

export const getBundleForms = args => async (dispatch, getState) => {
  const state = getState();
  const { exercice_id, code_sheet_group, date_declare } = args;
  const society_id = _get(state, 'navigation.id', false);
  const bundleActionType = getBundleActionType(code_sheet_group);

  try {
    await dispatch(actions.getActions.getAttempt(bundleActionType, society_id));

    const { data } = await windev.makeApiCall('/liasse', 'get', {
      exercice_id,
      code_sheet_group,
      date_declare
    });

    availableForms[bundleActionType].forEach((formItem) => {
      const formName = `${society_id}_${bundleActionType}_${formItem}`;
      dispatch(initialize(formName));
      if (data) {
        const formData = _get(data.filter(form => form.name === formItem), '[0]');
        if (!_isEmpty(formData)) {
          const form = _get(formData, 'data');
          if (!_isEmpty(form)) {
            dispatch(initialize(`${society_id}_${bundleActionType}_${formItem}`, form));
          }
        }
      }
    });
    await dispatch(actions.getActions.getSuccess(bundleActionType, data, society_id));
    await dispatch(actions.setDeadlineSuccess(society_id, date_declare));
    return data;
  } catch (error) {
    await dispatch(actions.getActions.getFail(bundleActionType, society_id));
    await dispatch(actions.setDeadlineFail(society_id));
    await dispatch(handleError(error));
    return error;
  }
};

export const sendBundleForms = args => async (dispatch, getState) => {
  const state = getState();
  const { exercice_id, code_sheet_group, date_declare } = args;
  const society_id = _get(state, 'navigation.id', false);
  const bundleActionType = getBundleActionType(code_sheet_group);

  const forms = _get(state, `bundle.${bundleActionType}.${society_id}.forms`);
  const sheet_list = [];

  try {
    await dispatch(actions.sendActions.sendAttempt(bundleActionType, society_id));
    availableForms[bundleActionType].forEach((formItem) => {
      sheet_list.push({
        id_sheet: _get(forms.filter(form => form.name === formItem), '[0].form_id'),
        field_list: getFormValues(`${society_id}_${bundleActionType}_${formItem}`)(state)
      });
    });

    await windev.makeApiCall('/liasse', 'post',
      { society_id, exercice_id },
      { code_sheet_group, date_declare, sheet_list });
    return true;
  } catch (error) {
    await dispatch(actions.sendActions.sendFail(bundleActionType, society_id));
    await dispatch(handleError(error));
    return error;
  }
};

export const sendBundleFormsEdi = args => async (dispatch, getState) => {
  const state = getState();
  const { code_sheet_group, date_declare, exercice_id } = args;
  const society_id = _get(state, 'navigation.id', false);
  const bundleActionType = getBundleActionType(code_sheet_group);

  try {
    await dispatch(actions.sendEdiActions.sendEdiAttempt(bundleActionType, society_id));
    await windev.makeApiCall('/liasse/sendEDI', 'post',
      {
        society_id, exercice_id, code_sheet_group, date_declare
      });
    return true;
  } catch (error) {
    await dispatch(actions.sendEdiActions.sendEdiFail(bundleActionType, society_id));
    await dispatch(handleError(error));
    return error;
  }
};

export const printOrDownloadBundle = (args, type) => async (dispatch, getState) => {
  const state = getState();
  const { exercice_id, code_sheet_group, date_declare } = args;
  const society_id = _get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getBinaryTaxFormAttempt(society_id));
    const { data } = await windev.makeApiCall('/liasse/download', 'get',
      { code_sheet_group, date_declare, exercice_id },
      {},
      { responseType: 'blob' });
    await dispatch(actions.getBinaryTaxFormSuccess(data, society_id));
    await printOrDownload(data, type, `${exercice_id}.pdf`);
    return true;
  } catch (error) {
    await dispatch(actions.getBinaryTaxFormFail(society_id));
    await dispatch(handleError(error));
    throw error;
  }
};
