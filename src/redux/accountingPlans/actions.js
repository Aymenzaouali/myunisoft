import {
  GET_ACCOUNTING_PLANS_ATTEMPT,
  GET_ACCOUNTING_PLANS_SUCCESS,
  GET_ACCOUNTING_PLANS_FAIL,

  CREATE_ACCOUNTING_PLAN_ATTEMPT,
  CREATE_ACCOUNTING_PLAN_SUCCESS,
  CREATE_ACCOUNTING_PLAN_FAIL,

  UPDATE_ACCOUNTING_PLAN_ATTEMPT,
  UPDATE_ACCOUNTING_PLAN_SUCCESS,
  UPDATE_ACCOUNTING_PLAN_FAIL,

  DELETE_ACCOUNTING_PLANS_ATTEMPT,
  DELETE_ACCOUNTING_PLANS_SUCCESS,
  DELETE_ACCOUNTING_PLANS_FAIL,

  SELECT_PLAN,
  ADD_PLAN,
  REMOVE_PLAN,
  ADD_ALL_PLANS,
  RESET_ALL_PLANS,
  RESET_SELECTED_PLAN
} from './constants';

const getAccountingPlansAttempt = () => ({
  type: GET_ACCOUNTING_PLANS_ATTEMPT
});

const getAccountingPlansSuccess = (plansList, options) => ({
  type: GET_ACCOUNTING_PLANS_SUCCESS,
  plansList,
  options
});

const getAccountingPlansFail = () => ({
  type: GET_ACCOUNTING_PLANS_FAIL
});

const createAccountingPlanAttempt = () => ({
  type: CREATE_ACCOUNTING_PLAN_ATTEMPT
});

const createAccountingPlanSuccess = plan => ({
  type: CREATE_ACCOUNTING_PLAN_SUCCESS,
  plan
});

const createAccountingPlanFail = () => ({
  type: CREATE_ACCOUNTING_PLAN_FAIL
});

const updateAccountingPlanAttempt = () => ({
  type: UPDATE_ACCOUNTING_PLAN_ATTEMPT
});

const updateAccountingPlanSuccess = plan => ({
  type: UPDATE_ACCOUNTING_PLAN_SUCCESS,
  plan
});

const updateAccountingPlanFail = () => ({
  type: UPDATE_ACCOUNTING_PLAN_FAIL
});

const deleteAccountingPlansAttempt = () => ({
  type: DELETE_ACCOUNTING_PLANS_ATTEMPT
});

const deleteAccountingPlansSuccess = plansList => ({
  type: DELETE_ACCOUNTING_PLANS_SUCCESS,
  plansList
});

const deleteAccountingPlansFail = () => ({
  type: DELETE_ACCOUNTING_PLANS_FAIL
});

const selectPlan = (selectedPlan, selectedLine) => ({
  type: SELECT_PLAN,
  selectedPlan,
  selectedLine
});

const addPlan = planId => ({
  type: ADD_PLAN,
  planId
});

const removePlan = planId => ({
  type: REMOVE_PLAN,
  planId
});

const addAllPlans = () => ({
  type: ADD_ALL_PLANS
});

const resetAllPlans = () => ({
  type: RESET_ALL_PLANS
});

const resetSelectedPlan = () => ({
  type: RESET_SELECTED_PLAN
});

export default {
  getAccountingPlansAttempt,
  getAccountingPlansSuccess,
  getAccountingPlansFail,
  createAccountingPlanAttempt,
  createAccountingPlanSuccess,
  createAccountingPlanFail,
  updateAccountingPlanAttempt,
  updateAccountingPlanSuccess,
  updateAccountingPlanFail,
  deleteAccountingPlansAttempt,
  deleteAccountingPlansSuccess,
  deleteAccountingPlansFail,
  selectPlan,
  addPlan,
  removePlan,
  addAllPlans,
  resetAllPlans,
  resetSelectedPlan
};
