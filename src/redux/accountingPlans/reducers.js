import _ from 'lodash';

import {
  GET_ACCOUNTING_PLANS_ATTEMPT,
  GET_ACCOUNTING_PLANS_SUCCESS,
  GET_ACCOUNTING_PLANS_FAIL,

  SELECT_PLAN,
  ADD_PLAN,
  REMOVE_PLAN,
  ADD_ALL_PLANS,
  RESET_ALL_PLANS,
  RESET_SELECTED_PLAN
} from './constants';

const initialState = {
  plans: {},
  selectedPlans: [],
  selectedPlan: null,
  selectedLine: null
};

const accountingPlans = (state = initialState, action) => {
  const {
    plansList = {},
    planId,
    selectedPlan,
    selectedLine,
    options
  } = action;

  if (action && action.type) {
    switch (action.type) {
    case GET_ACCOUNTING_PLANS_ATTEMPT:
      return {
        ...state,
        plans: {
          ...state.plans,
          isLoading: true,
          isError: false,
          list: []
        }
      };
    case GET_ACCOUNTING_PLANS_SUCCESS:
      return {
        ...state,
        plans: {
          ...state.plans,
          isLoading: false,
          isError: false,
          ...plansList,
          ...options
        }
      };
    case GET_ACCOUNTING_PLANS_FAIL:
      return {
        ...state,
        plans: {
          ...state.plans,
          isLoading: false,
          isError: true,
          list: []
        }
      };
    case SELECT_PLAN:
      return {
        ...state,
        selectedPlan,
        selectedLine
      };
    case ADD_PLAN:
      return {
        ...state,
        selectedPlans: [
          ...state.selectedPlans,
          planId
        ]
      };
    case REMOVE_PLAN:
      return {
        ...state,
        selectedPlans: [
          ..._.get(state, 'selectedPlans', []).filter(v => v !== planId)
        ]
      };
    case ADD_ALL_PLANS:
      return {
        ...state,
        selectedPlans: [
          ..._.get(state, 'plans.list', []).map(plan => plan.id)
        ]
      };
    case RESET_ALL_PLANS:
      return {
        ...state,
        selectedPlans: []
      };
    case RESET_SELECTED_PLAN:
      return {
        ...state,
        selectedPlans: [],
        selectedPlan: null,
        selectedLine: null
      };
    default:
      return state;
    }
  }
  return state;
};

export default accountingPlans;
