import _ from 'lodash';
import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';

import actions from './actions';

export const getAccountingPlans = (options = {}) => async (dispatch, getState) => {
  const state = getState();

  const {
    sort: sortOptions
  } = options;

  const plans = _.get(state, 'accountingPlans.plans', {});
  const sort = sortOptions !== undefined ? sortOptions : _.get(plans, 'sort', '');

  const limit = 1000000;
  const page = 0;
  const offset = 0;

  try {
    const actionOptions = { limit, page, sort };

    if (!_.isEmpty(sortOptions)) {
      const direction = 'asc';
      const storedColumn = _.get(plans, 'sort.column');
      const currentColumn = _.get(sortOptions, 'column');
      if (currentColumn === storedColumn) {
        const storedDirection = _.get(plans, 'sort.direction');
        const direction = storedDirection === 'desc' ? 'asc' : 'desc';
        actionOptions.sort = { column: currentColumn, direction };
      } else {
        actionOptions.sort = { column: currentColumn, direction };
      }
    }

    const params = {
      offset,
      limit,
      sort: actionOptions.sort
    };

    await dispatch(actions.getAccountingPlansAttempt());
    const response = await windev.makeApiCall('/model/account', 'get', params);
    const { data } = response;
    await dispatch(actions.getAccountingPlansSuccess(data, actionOptions));
    return response;
  } catch (err) {
    await dispatch(actions.getAccountingPlansFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const createPlan = newPlan => async (dispatch) => {
  const {
    label,
    society,
    description = ''
  } = newPlan;

  const body = {
    label,
    description,
    from_society_id: _.get(society, 'society_id', '')
  };

  try {
    await dispatch(actions.createAccountingPlanAttempt());
    const { data } = await windev.makeApiCall('/model/account', 'post', {}, body);
    await dispatch(actions.createAccountingPlanSuccess(data));
    await dispatch(actions.resetSelectedPlan());
    await dispatch(getAccountingPlans());
    return data;
  } catch (err) {
    await dispatch(actions.createAccountingPlanFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const modifyPlan = selectedPlan => async (dispatch, getState) => {
  const state = getState();
  const {
    label,
    description = ''
  } = _.get(state, 'form.planForm.values', {});

  const body = {
    label,
    description
  };

  try {
    await dispatch(actions.updateAccountingPlanAttempt());
    const { data } = await windev.makeApiCall(`/model/account/${selectedPlan}`, 'put', {}, body);
    await dispatch(actions.updateAccountingPlanSuccess(data));
    await dispatch(getAccountingPlans());
    return data;
  } catch (err) {
    await dispatch(actions.updateAccountingPlanFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const deleteAccountingPlans = () => async (dispatch, getState) => {
  const state = getState();
  const plansList = _.get(state, 'accountingPlans.selectedPlans', false);

  try {
    await dispatch(actions.deleteAccountingPlansAttempt());
    const { data } = await windev.makeApiCall('/model/account', 'delete', {}, plansList);
    await dispatch(actions.deleteAccountingPlansSuccess(data));
    await dispatch(actions.resetSelectedPlan());
    await dispatch(getAccountingPlans());
    return data;
  } catch (err) {
    await dispatch(actions.deleteAccountingPlansFail());
    await dispatch(handleError(err));
    throw err;
  }
};
