import _ from 'lodash';
import I18n from 'assets/I18n';
import { windev } from 'helpers/api';
import { buildPaymentsList } from 'helpers/payment';
import { handleError } from 'common/redux/error';
import { initialize, getFormValues } from 'redux-form';
import actions from './actions';

import { getRibsList } from '../rib';

const formName = 'Statements';

const initializePaymentStatementsForm = async (dispatch, getState) => {
  const state = getState();
  const formValue = getFormValues(formName)(state);
  const payment = _.get(state, 'statements.form.payment', []);
  const ribList = _.get(state, 'rib.ribList', []);
  formValue.payment = [];

  payment.forEach((item) => {
    const { id_rib, amount } = item;
    if (id_rib && Number(id_rib)) {
      formValue.payment.push({
        amount,
        rib: {
          value: id_rib,
          label: ribList.filter(rib => rib.rib_id === Number(id_rib))[0].iban
        }
      });
    }
  });
  if (!formValue.payment.length) {
    formValue.payment.push({
      amount: 0,
      rib: {
        value: 0,
        label: ''
      }
    });
  }

  dispatch(initialize(formName, formValue));
};

export const removeRib = ribId => async (dispatch) => {
  dispatch(actions.removeBundleStatementsRib(ribId));
};

export const getBundleStatements = (
  sheet_group_code, date_declaration
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    dispatch(actions.getBundleStatementsAttempt());
    await dispatch(getRibsList(society_id));
    const { data } = await windev.makeApiCall('/liasse/payment', 'get', { sheet_group_code, date_declaration });
    if (data) {
      const { full_payment, amount_due } = data;
      const form = {
        type: {
          value: full_payment ? 'full' : 'part',
          label: full_payment ? I18n.t('statement.fullPayment') : I18n.t('statement.partPayment')
        },
        amountToBePaid: amount_due
      };
      dispatch(initialize(formName, form));
      await dispatch(actions.getBundleStatementsSuccess(data));
      await initializePaymentStatementsForm(dispatch, getState);
    }
    return null;
  } catch (err) {
    await dispatch(actions.getBundleStatementsFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const sendBundleStatements = (
  code_sheet_group, date_declaration, isFull
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  try {
    await dispatch(actions.sendBundleStatementsAttempt(society_id));
    const form = getFormValues(formName)(state);

    const params = {
      amount_due: form.amountToBePaid,
      date_declaration,
      id_sheet_group: 8,
      code_sheet_group,
      full_payment: isFull,
      payment: buildPaymentsList(form.payment, 3)
    };

    await windev.makeApiCall('/liasse/payment', 'post', { society_id }, params);
    await dispatch(actions.sendBundleStatementsSuccess(society_id));
    return null;
  } catch (err) {
    await dispatch(actions.sendBundleStatementsFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};
