import {
  GET_STATEMENTS_FAIL,
  GET_STATEMENTS_ATTEMPT,
  GET_STATEMENTS_SUCCESS,
  SEND_STATEMENTS_FAIL,
  SEND_STATEMENTS_ATTEMPT,
  SEND_STATEMENTS_SUCCESS,
  REMOVE_STATEMENTS_RIB
} from './constants';

const getBundleStatementsAttempt = () => ({
  type: GET_STATEMENTS_ATTEMPT
});

const getBundleStatementsSuccess = form => ({
  type: GET_STATEMENTS_SUCCESS,
  form
});

const getBundleStatementsFail = societyId => ({
  type: GET_STATEMENTS_FAIL,
  societyId
});

const sendBundleStatementsAttempt = () => ({
  type: SEND_STATEMENTS_ATTEMPT
});

const sendBundleStatementsSuccess = data => ({
  type: SEND_STATEMENTS_SUCCESS,
  data
});

const sendBundleStatementsFail = data => ({
  type: SEND_STATEMENTS_FAIL,
  data
});

const removeBundleStatementsRib = ribId => ({
  type: REMOVE_STATEMENTS_RIB,
  ribId
});

export default {
  getBundleStatementsAttempt,
  getBundleStatementsSuccess,
  getBundleStatementsFail,
  removeBundleStatementsRib,
  sendBundleStatementsAttempt,
  sendBundleStatementsSuccess,
  sendBundleStatementsFail
};
