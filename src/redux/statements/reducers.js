import _ from 'lodash';

import {
  GET_STATEMENTS_ATTEMPT,
  GET_STATEMENTS_SUCCESS,
  GET_STATEMENTS_FAIL,
  REMOVE_STATEMENTS_RIB,
  SEND_STATEMENTS_ATTEMPT,
  SEND_STATEMENTS_SUCCESS
} from './constants';

const initialeState = {
  forms: {},
  isLoading: false,
  isError: false
};

const statements = (state = initialeState, action) => {
  const {
    form,
    ribId
  } = action;

  switch (action.type) {
  case GET_STATEMENTS_ATTEMPT:
  case SEND_STATEMENTS_ATTEMPT:
    return {
      ...state,
      isLoading: true,
      isError: false
    };
  case SEND_STATEMENTS_SUCCESS:
    return {
      ...state,
      isLoading: false,
      isError: false
    };
  case GET_STATEMENTS_SUCCESS:
    return {
      ...state,
      isLoading: false,
      isError: false,
      form
    };
  case REMOVE_STATEMENTS_RIB:
    const formData = _.get(state, 'form', null);
    const payment = _.get(formData, 'payment', []);
    const newPayment = payment.map((item) => {
      if (item.id_rib === ribId) {
        return { id_rib: 0, amount: 0 };
      } return item;
    });
    return {
      ...state,
      form: {
        ...formData,
        payment: newPayment
      }
    };
  case GET_STATEMENTS_FAIL:
    return {
      ...state,
      isLoading: false,
      isError: true
    };
  default:
    return state;
  }
};

export default statements;
