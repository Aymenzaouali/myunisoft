import {
  // TODO: Implement in scope of MYUN-433
  // GET_ACCOUNT_ATTEMPT,
  // GET_ACCOUNT_SUCCESS,
  // GET_ACCOUNT_FAIL,
  CHANGE_TAB_ACCOUNT_CREATION,
  CHANGE_TAB_RIB_ACCOUNT_CREATION
  // SAVING_ACCOUNT_DATA
} from './constants';

const changeTabAccountCreation = tab => ({
  type: CHANGE_TAB_ACCOUNT_CREATION,
  tab
});

const changeTabRibAccountCreation = tab => ({
  type: CHANGE_TAB_RIB_ACCOUNT_CREATION,
  tab
});

export default {
  changeTabAccountCreation,
  changeTabRibAccountCreation
};
