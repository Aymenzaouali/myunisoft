import { handleError } from 'common/redux/error';
import actions from './actions';

export const switchTabAccountCreation = payload => async (dispatch) => {
  try {
    if (payload === 0 || payload === 1) {
      await dispatch(actions.changeTabAccountCreation(payload));
    }
    return payload;
  } catch (err) {
    console.log(err); //eslint-disable-line
    await dispatch(handleError(err));

    return err;
  }
};

export const switchTabRibAccountCreation = payload => async (dispatch) => {
  try {
    if (payload === 0 || payload === 1) {
      await dispatch(actions.changeTabRibAccountCreation(payload));
    }
    return payload;
  } catch (err) {
    console.log(err); //eslint-disable-line
    await dispatch(handleError(err));

    return err;
  }
};
