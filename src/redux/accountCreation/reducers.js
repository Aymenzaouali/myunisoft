import {
  // TODO: Implement in scope of MYUN-433
  // GET_ACCOUNT_ATTEMPT,
  // GET_ACCOUNT_SUCCESS,
  // GET_ACCOUNT_FAIL,
  CHANGE_TAB_ACCOUNT_CREATION,
  CHANGE_TAB_RIB_ACCOUNT_CREATION
  // SAVING_ACCOUNT_DATA
} from './constants';

const initialState = {
  selectTab: 0,
  selectTabRib: 0
};

const accountCreation = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case CHANGE_TAB_ACCOUNT_CREATION:
      return {
        ...state,
        selectTab: action.tab
      };
    case CHANGE_TAB_RIB_ACCOUNT_CREATION:
      return {
        ...state,
        selectTabRib: action.tab
      };
    default: return state;
    }
  }
  return state;
};

export default accountCreation;
