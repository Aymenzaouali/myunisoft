import { windev } from 'helpers/api';
import { getFormValues } from 'redux-form';
import _ from 'lodash';
import actions from 'redux/accounting/actions';
import { convertFormEntry } from 'helpers/accounting';
import { getDefaultFilter } from 'redux/tabs/accounting';
import {
  confirmAttempt, confirmSuccess, confirmFailure
} from './actions';

export const confirmed = (entry, formName) => async (dispatch, getState) => {
  const state = getState();
  const societyId = state.navigation.id;
  const filtersValues = getFormValues(formName)(state);

  try {
    await dispatch(confirmAttempt());
    await dispatch(actions.modifyEntryAttempt(societyId, entry.entry_id));
    const { data } = await windev.makeApiCall('/entry', 'put', { type: _.get(filtersValues, 'type', 'e'), confirmed: true }, entry);
    await dispatch(confirmSuccess());
    await dispatch(actions.modifyEntrySuccess(societyId, convertFormEntry(entry)));
    await dispatch(getDefaultFilter());
    return data;
  } catch (err) {
    console.log(err.response); // eslint-disable-line
    await dispatch(confirmFailure());
    throw err.response;
  }
};
