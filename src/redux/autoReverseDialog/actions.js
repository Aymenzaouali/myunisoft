import {
  AUTO_REVERSE_OPEN_DIALOG,
  AUTO_REVERSE_CLOSE_DIALOG,

  AUTO_REVERSE_CONFIRMED_ATTEMP,
  AUTO_REVERSE_CONFIRMED_SUCCESS,
  AUTO_REVERSE_CONFIRMED_FAILURE,

  AUTO_REVERSE_CANCEL_ATTEMP,
  AUTO_REVERSE_CANCEL_SUCCESS,
  AUTO_REVERSE_CANCEL_FAILURE
} from './constants';

export const openAutoReverseDialog = () => ({
  type: AUTO_REVERSE_OPEN_DIALOG
});

export const closeAutoReverseDialog = () => ({
  type: AUTO_REVERSE_CLOSE_DIALOG
});

export const confirmAttempt = () => ({
  type: AUTO_REVERSE_CONFIRMED_ATTEMP
});

export const confirmSuccess = () => ({
  type: AUTO_REVERSE_CONFIRMED_SUCCESS
});

export const confirmFailure = () => ({
  type: AUTO_REVERSE_CONFIRMED_FAILURE
});

export const cancelAttempt = () => ({
  type: AUTO_REVERSE_CANCEL_ATTEMP
});

export const cancelSuccess = () => ({
  type: AUTO_REVERSE_CANCEL_SUCCESS
});

export const cancelFailure = () => ({
  type: AUTO_REVERSE_CANCEL_FAILURE
});
