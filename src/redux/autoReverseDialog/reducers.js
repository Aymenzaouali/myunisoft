import {
  AUTO_REVERSE_OPEN_DIALOG,
  AUTO_REVERSE_CLOSE_DIALOG,

  AUTO_REVERSE_CONFIRMED_ATTEMP,
  AUTO_REVERSE_CONFIRMED_SUCCESS,
  AUTO_REVERSE_CONFIRMED_FAILURE,

  AUTO_REVERSE_CANCEL_ATTEMP,
  AUTO_REVERSE_CANCEL_SUCCESS,
  AUTO_REVERSE_CANCEL_FAILURE
} from './constants';

const initialState = {
  isOpen: false,
  isLoadingSubmit: false,
  confirmed: '',
  isLoadingCancel: false,
  cancelled: ''
};

const autoReverseDialog = (state = initialState, action) => {
  switch (action.type) {
  case AUTO_REVERSE_OPEN_DIALOG: {
    return {
      ...state,
      isOpen: true
    };
  }
  case AUTO_REVERSE_CLOSE_DIALOG: {
    return {
      ...state,
      isOpen: false
    };
  }
  case AUTO_REVERSE_CONFIRMED_ATTEMP: {
    return {
      ...state,
      isLoadingSubmit: false
    };
  }
  case AUTO_REVERSE_CONFIRMED_SUCCESS: {
    return {
      ...state,
      isLoadingSubmit: false,
      confirmed: true,
      isOpen: false
    };
  }
  case AUTO_REVERSE_CONFIRMED_FAILURE: {
    return {
      ...state,
      isLoadingSubmit: false,
      confirmed: false,
      isOpen: false
    };
  }
  case AUTO_REVERSE_CANCEL_ATTEMP: {
    return {
      ...state,
      isLoadingCancel: false
    };
  }
  case AUTO_REVERSE_CANCEL_SUCCESS: {
    return {
      ...state,
      isLoadingCancel: false,
      cancelled: true,
      isOpen: false
    };
  }
  case AUTO_REVERSE_CANCEL_FAILURE: {
    return {
      ...state,
      isLoadingCancel: false,
      cancelled: false,
      isOpen: false
    };
  }
  default:
    return state;
  }
};

export default autoReverseDialog;
