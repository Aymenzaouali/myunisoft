import {
  GET_PLAN_DETAILS_ATTEMPT,
  GET_PLAN_DETAILS_SUCCESS,
  GET_PLAN_DETAILS_FAIL,

  CREATE_PLAN_DETAIL_ATTEMPT,
  CREATE_PLAN_DETAIL_SUCCESS,
  CREATE_PLAN_DETAIL_FAIL,

  UPDATE_PLAN_DETAIL_ATTEMPT,
  UPDATE_PLAN_DETAIL_SUCCESS,
  UPDATE_PLAN_DETAIL_FAIL,

  DELETE_PLAN_DETAILS_ATTEMPT,
  DELETE_PLAN_DETAILS_SUCCESS,
  DELETE_PLAN_DETAILS_FAIL,

  SELECT_PLAN_DETAIL,
  ADD_PLAN_DETAIL,
  REMOVE_PLAN_DETAIL,
  ADD_ALL_PLAN_DETAILS,
  RESET_ALL_PLAN_DETAILS
} from './constants';

const getPlanDetailsAttempt = (planId, options) => ({
  type: GET_PLAN_DETAILS_ATTEMPT,
  planId,
  options
});

const getPlanDetailsSuccess = (planDetailsList, planId, options) => ({
  type: GET_PLAN_DETAILS_SUCCESS,
  planDetailsList,
  planId,
  options
});

const getPlanDetailsFail = () => ({
  type: GET_PLAN_DETAILS_FAIL
});

const createPlanDetailAttempt = () => ({
  type: CREATE_PLAN_DETAIL_ATTEMPT
});

const createPlanDetailSuccess = planDetail => ({
  type: CREATE_PLAN_DETAIL_SUCCESS,
  planDetail
});

const createPlanDetailFail = () => ({
  type: CREATE_PLAN_DETAIL_FAIL
});

const updatePlanDetailAttempt = () => ({
  type: UPDATE_PLAN_DETAIL_ATTEMPT
});

const updatePlanDetailSuccess = (itemId, planDetail) => ({
  type: UPDATE_PLAN_DETAIL_SUCCESS,
  itemId,
  planDetail
});

const updatePlanDetailFail = () => ({
  type: UPDATE_PLAN_DETAIL_FAIL
});

const deletePlanDetailsAttempt = itemId => ({
  type: DELETE_PLAN_DETAILS_ATTEMPT,
  itemId
});

const deletePlanDetailsSuccess = planDetail => ({
  type: DELETE_PLAN_DETAILS_SUCCESS,
  planDetail
});

const deletePlanDetailsFail = () => ({
  type: DELETE_PLAN_DETAILS_FAIL
});

const selectPlanDetail = (selectedPlanDetail, selectedLine) => ({
  type: SELECT_PLAN_DETAIL,
  selectedPlanDetail,
  selectedLine
});

const addPlanDetail = (itemId, planId) => ({
  type: ADD_PLAN_DETAIL,
  planId,
  itemId
});

const removePlanDetail = (itemId, planId) => ({
  type: REMOVE_PLAN_DETAIL,
  planId,
  itemId
});

const addAllPlanDetail = planId => ({
  type: ADD_ALL_PLAN_DETAILS,
  planId
});

const resetAllPlanDetail = planId => ({
  type: RESET_ALL_PLAN_DETAILS,
  planId
});

export default {
  getPlanDetailsAttempt,
  getPlanDetailsSuccess,
  getPlanDetailsFail,
  createPlanDetailAttempt,
  createPlanDetailSuccess,
  createPlanDetailFail,
  updatePlanDetailAttempt,
  updatePlanDetailSuccess,
  updatePlanDetailFail,
  deletePlanDetailsAttempt,
  deletePlanDetailsSuccess,
  deletePlanDetailsFail,
  selectPlanDetail,
  addPlanDetail,
  removePlanDetail,
  addAllPlanDetail,
  resetAllPlanDetail
};
