import _ from 'lodash';
import { windev } from 'helpers/api';

import { handleError } from 'common/redux/error';
import actions from './actions';

export const getPlanDetails = (options = {}) => async (dispatch, getState) => {
  const state = getState();
  const planId = _.get(state, 'accountingPlans.selectedPlan');

  const {
    sort: sortOptions
  } = options;

  const planDetails = _.get(state, `planDetails.planDetails[${planId}]`, {});
  const sort = sortOptions !== undefined ? sortOptions : _.get(planDetails, 'sort', '');

  const limit = 1000000;
  const page = 0;
  const offset = 0;

  try {
    if (planId) {
      const actionOptions = { limit, page, sort };

      if (!_.isEmpty(sortOptions)) {
        const direction = 'asc';
        const storedColumn = _.get(planDetails, 'sort.column');
        const currentColumn = _.get(sortOptions, 'column');
        if (currentColumn === storedColumn) {
          const storedDirection = _.get(planDetails, 'sort.direction');
          const direction = storedDirection === 'desc' ? 'asc' : 'desc';
          actionOptions.sort = { column: currentColumn, direction };
        } else {
          actionOptions.sort = { column: currentColumn, direction };
        }
      }

      const params = {
        offset,
        limit,
        sort: actionOptions.sort
      };

      await dispatch(actions.getPlanDetailsAttempt(planId, actionOptions));
      const response = await windev.makeApiCall(`/model/account/${planId}/detail`, 'get', params);
      const { data } = response;
      await dispatch(actions.getPlanDetailsSuccess(data, planId, actionOptions));
      return response;
    } return new Error('No plan Details selected');
  } catch (err) {
    await dispatch(actions.getPlanDetailsFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const createPlanDetail = () => async (dispatch, getState) => {
  const state = getState();
  const planId = _.get(state, 'accountingPlans.selectedPlan');
  const {
    account_number,
    label = ''
  } = _.get(state, 'form.planDetailForm.values', {});

  const body = {
    account_number,
    label
  };

  try {
    await dispatch(actions.createPlanDetailAttempt());
    const { data } = await windev.makeApiCall(`/model/account/${planId}/detail`, 'post', {}, body);
    await dispatch(actions.createPlanDetailSuccess(data));
    await dispatch(getPlanDetails());
    return data;
  } catch (err) {
    await dispatch(actions.createPlanDetailFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const modifyPlanDetail = () => async (dispatch, getState) => {
  const state = getState();
  const {
    id,
    account_number,
    label = ''
  } = _.get(state, 'form.planDetailForm.values', {});

  const body = {
    account_number,
    label
  };

  try {
    await dispatch(actions.updatePlanDetailAttempt());
    const { data } = await windev.makeApiCall(`/model/account/detail/${id}`, 'put', {}, body);
    await dispatch(actions.updatePlanDetailSuccess(data));
    await dispatch(getPlanDetails());

    return data;
  } catch (err) {
    await dispatch(actions.updatePlanDetailFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const deletePlanDetails = () => async (dispatch, getState) => {
  const state = getState();
  const planId = _.get(state, 'accountingPlans.selectedPlan');
  const planDetailsList = _.get(state, `planDetails.selectedPlanDetails[${planId}]`, false);

  try {
    await dispatch(actions.deletePlanDetailsAttempt());
    const { data } = await windev.makeApiCall(`/model/account/${planId}/detail`, 'delete', {}, planDetailsList);
    await dispatch(actions.deletePlanDetailsSuccess(data));
    await dispatch(getPlanDetails());
    return data;
  } catch (err) {
    await dispatch(actions.deletePlanDetailsFail());
    await dispatch(handleError(err));
    throw err;
  }
};
