import _ from 'lodash';

import {
  GET_PLAN_DETAILS_ATTEMPT,
  GET_PLAN_DETAILS_SUCCESS,
  GET_PLAN_DETAILS_FAIL,

  SELECT_PLAN_DETAIL,
  ADD_PLAN_DETAIL,
  REMOVE_PLAN_DETAIL,
  ADD_ALL_PLAN_DETAILS,
  RESET_ALL_PLAN_DETAILS
} from './constants';

const initialState = {
  planDetails: {},
  selectedPlanDetails: [],
  selectedPlanDetail: null,
  selectedLine: null
};

const planDetails = (state = initialState, action) => {
  const {
    planId,
    options,
    itemId
  } = action;
  if (action && action.type) {
    switch (action.type) {
    case GET_PLAN_DETAILS_ATTEMPT:
      return {
        ...state,
        planDetails: {
          ...state.planDetails,
          [planId]: {
            isLoading: true,
            isError: false,
            list: [],
            ...options
          }
        },
        selectedPlanDetails: [],
        selectedPlanDetail: null,
        selectedLine: null
      };
    case GET_PLAN_DETAILS_SUCCESS:
      const {
        planDetailsList = {}
      } = action;
      return {
        ...state,
        planDetails: {
          ...state.planDetails,
          [planId]: {
            isLoading: false,
            isError: false,
            ...planDetailsList,
            ...options
          }
        }
      };
    case GET_PLAN_DETAILS_FAIL:
      return {
        ...state,
        planDetails: {},
        selectedPlanDetails: [],
        selectedPlanDetail: null
      };
    case SELECT_PLAN_DETAIL:
      const {
        selectedPlanDetail,
        selectedLine
      } = action;
      return {
        ...state,
        selectedPlanDetail,
        selectedLine
      };
    case ADD_PLAN_DETAIL:
      return {
        ...state,
        selectedPlanDetails: {
          ...state.selectedPlanDetails,
          [planId]: [..._.get(state, `selectedPlanDetails.${planId}`, []), itemId]
        }
      };
    case REMOVE_PLAN_DETAIL:
      return {
        ...state,
        selectedPlanDetails: {
          ...state.selectedPlanDetails,
          [planId]: _.get(state, `selectedPlanDetails[${planId}]`, []).filter(v => v !== itemId)
        }
      };
    case ADD_ALL_PLAN_DETAILS:
      return {
        ...state,
        selectedPlanDetails: {
          ...state.selectedPlanDetails,
          [planId]: _.get(state, `planDetails[${planId}].list`, []).map(planDetailAvailable => planDetailAvailable.id)
        }
      };
    case RESET_ALL_PLAN_DETAILS:
      return {
        ...state,
        selectedPlanDetails: {
          ...state.selectedPlanDetails,
          [planId]: []
        }
      };
    default:
      return state;
    }
  }
  return state;
};

export default planDetails;
