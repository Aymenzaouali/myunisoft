import { findRouteKeyByPathname, isSlaveRoute } from 'helpers/routes';
import {
  GET_SOCIETY_ATTEMPT,
  GET_SOCIETY_SUCCESS,
  GET_SOCIETY_FAIL,

  INIT_TABS, ADD_TABS, CLOSE_TAB,
  TOGGLE_MENU,
  TOGGLE_DIALOG, CLOSE_DIALOG
} from './constants';

const initialState = {
  tabs: [],
  societies: [],
  routeKey: '',
  isMenuOpen: true,
  id: 'collab',
  isDialogOpen: false,
  societyDialog: {},
  forms: []
};

const navigationReducer = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    // Tabs
    case INIT_TABS:
      return {
        ...state,
        tabs: []
      };

    case ADD_TABS:
      const { tabs } = state;
      const actionTabs = action.tabs;

      return {
        ...state,
        tabs: [...tabs, ...actionTabs]
      };

    case '@@router/LOCATION_CHANGE':
      const path = action.payload.location.pathname;
      if (isSlaveRoute(path)) {
        return state;
      }

      const match = path.match(/\/tab\/(.*)\//);
      if (match !== null) {
        const id = parseInt(match[1], 10) ? match[1] : -2;
        return {
          ...state,
          routeKey: findRouteKeyByPathname(path),
          tabs: state.tabs.map(
            tab => (parseInt(tab.id, 10) === parseInt(id, 10) ? { ...tab, path } : tab)
          ),
          id: parseInt(id, 10)
        };
      }
      return state;

    case CLOSE_TAB:
      // const currentTab = state.tabs.findIndex(tab => parseInt(tab.id) === parseInt(action.id));
      const tabsFiltered = state.tabs.filter(
        tab => parseInt(tab.id, 10) !== parseInt(action.id, 10)
      );
      return {
        ...state,
        tabs: tabsFiltered
      };

    // Menu
    case TOGGLE_MENU:
      const { isMenuOpen } = action;
      return {
        ...state,
        isMenuOpen
      };

    // Dialog
    case TOGGLE_DIALOG:
      const { isDialogOpen, route } = action;
      return {
        ...state,
        societyDialog: {
          isOpen: isDialogOpen,
          path: route
        }
      };
    case CLOSE_DIALOG:
      const { isOpen } = action;
      return {
        ...state,
        societyDialog: {
          path: state.societyDialog.path,
          isOpen
        }
      };
    // Society
    case GET_SOCIETY_FAIL:
      return {
        ...state
      };
    case GET_SOCIETY_ATTEMPT:
      return {
        ...state
      };
    case GET_SOCIETY_SUCCESS:
      const { societies } = action;
      return {
        ...state,
        societies
      };
    default:
      return state;
    }
  }
  return state;
};

export default navigationReducer;
