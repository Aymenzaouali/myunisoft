import {
  GET_SOCIETY_ATTEMPT,
  GET_SOCIETY_SUCCESS,
  GET_SOCIETY_FAIL,

  INIT_TABS, ADD_TABS, CLOSE_TAB,
  TOGGLE_MENU,
  TOGGLE_DIALOG, CLOSE_DIALOG
} from './constants';

const initTabs = user => ({
  type: INIT_TABS,
  user
});

const addTabs = tabs => ({
  type: ADD_TABS,
  tabs
});

const closeTab = id => ({
  type: CLOSE_TAB,
  id
});

const toggleMenu = isMenuOpen => ({
  type: TOGGLE_MENU,
  isMenuOpen
});

const toggleDialog = (isDialogOpen, route) => ({
  type: TOGGLE_DIALOG,
  isDialogOpen,
  route
});

const closeDialog = isOpen => ({
  type: CLOSE_DIALOG,
  isOpen
});

const getSocietyAttempt = () => ({
  type: GET_SOCIETY_ATTEMPT
});

const getSocietySuccess = societies => ({
  type: GET_SOCIETY_SUCCESS,
  societies
});

const getSocietyFail = () => ({
  type: GET_SOCIETY_FAIL
});

export default {
  getSocietyAttempt,
  getSocietySuccess,
  getSocietyFail,

  initTabs,
  addTabs,
  closeTab,
  toggleMenu,
  toggleDialog,
  closeDialog
};
