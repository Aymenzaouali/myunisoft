export const INIT_TABS = 'INIT_TABS';

export const ADD_TABS = 'ADD_TABS';
export const CLOSE_TAB = 'CLOSE_TAB';

export const TOGGLE_MENU = 'TOGGLE_MENU';

export const TOGGLE_DIALOG = 'TOGGLE_DIALOG';

export const CLOSE_DIALOG = 'CLOSE_DIALOG';

export const GET_SOCIETY_ATTEMPT = 'GET_SOCIETY_ATTEMPT';
export const GET_SOCIETY_SUCCESS = 'GET_SOCIETY_SUCCESS';
export const GET_SOCIETY_FAIL = 'GET_SOCIETY_FAIL';
