import { change } from 'redux-form';
import { push } from 'connected-react-router';
import actions from 'redux/navigation/actions';
import { selectCurrentOption } from 'redux/balance/actions';
import _ from 'lodash';

import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';

import { setCategory, setSubCategory } from 'redux/tabs/dadp/actions';
import { getCategory } from 'helpers/dadp';

const dadpMenus = ['da_balanceSheet', 'da_statusPending', 'da_newSituation'];

// This function seems to be used also change redux store's before redirect.
export const redirectFromCollab = (route, type, societyId) => async (dispatch) => {
  const dadpCategory = getCategory(type);

  try {
    if (type === 'e' || type === 'ib' || type === 'o' || type === 'm' || type === 'ext') {
      await dispatch(push(`/tab/${societyId || ':id'}/accounting/new`, { type }));
      await dispatch(change(`${societyId}accountingFilter`, 'type', type));
    } else if (type === 'ledger') {
      await dispatch(selectCurrentOption('ledger'));
      await dispatch(change('currentEditionsHeaderForm', 'current', type));
      await dispatch(push(`/tab/${societyId || ':id'}/current-editions/ledger`));
    } else if (type === 'sig') {
      await dispatch(selectCurrentOption('sig'));
      await dispatch(change('currentEditionsHeaderForm', 'current', type));
      await dispatch(push(`/tab/${societyId || ':id'}/current-editions/sig`));
    } else if (type === 'balance') {
      await dispatch(selectCurrentOption('balance'));
      await dispatch(change('currentEditionsHeaderForm', 'current', type));
      await dispatch(push(`/tab/${societyId || ':id'}/current-editions/balance`));
    } else if (type === 'booklet') {
      await dispatch(push(route.replace(':id', societyId)));
      await dispatch(setSubCategory(7));
    } else if (dadpMenus.indexOf(type) !== -1) {
      await dispatch(setCategory(dadpCategory));
      await dispatch(push(`/tab/${societyId || ':id'}/dadp/${type}`));
    } else if (route === '/tab/:id/import') {
      await dispatch(push(route.replace(':id', 'collab')));
    } else {
      await dispatch(push(route.replace(':id', societyId)));
    }
  } catch (err) {
    console.log(err); // eslint-disable-line
  }
};

export const getSociety = reference_front => async (dispatch) => {
  const params = {
    sort: {
      column: 'name',
      direction: 'asc'
    },
    mode: 2,
    reference_front
  };
  try {
    await dispatch(actions.getSocietyAttempt());
    const response = await windev.makeApiCall('/society', 'get', params);
    const { data } = response;
    await dispatch(actions.getSocietySuccess(_.get(data, 'society_array', [])));
    return response;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.getSocietyFail());
    return err;
  }
};

export const toggleSocietyDialog = (value, route) => async (dispatch) => {
  const {
    key
  } = route;
  try {
    await dispatch(actions.toggleDialog(value, route));
    await dispatch(getSociety(key));
  } catch (err) {
    console.log(err); // eslint-disable-line
  }
};
