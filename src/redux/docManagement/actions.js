import {
  SET_ACTIVE_TAB,
  GET_DOCUMENTS_BY_TREE,
  SET_DOCUMENTS_SEARCH_VALUES,
  GET_ACCOUNTS_TREE_ATTEMPT,
  GET_ACCOUNTS_TREE_SUCCESS,
  GET_ACCOUNTS_TREE_FAIL,
  GET_DOCUMENTS_ATTEMPT,
  GET_DOCUMENTS_SUCCESS,
  GET_DOCUMENTS_FAIL,
  GET_DOCUMENTS_SEARCH_ATTEMPT,
  GET_DOCUMENTS_SEARCH_SUCCESS,
  GET_DOCUMENTS_SEARCH_FAIL,
  SET_ACTIVE_FOLDER,
  GET_HEADER_SUCCESS,
  SET_ITEMS_PER_PAGE,
  SET_DOCUMENTS_MODE,
  SET_LINKED_DOCS_ATTEMPT,
  SET_LINKED_DOCS_SUCCESS,
  SET_LINKED_DOCS_FAIL,
  SHOW_SEND_IN_DISCUSSION_MODAL,
  SET_DOCUMENT_TO_COPY
} from './constants';

const setActiveTab = activeTabId => ({
  type: SET_ACTIVE_TAB,
  activeTabId
});

const getDocumentsByTree = leafId => ({
  type: GET_DOCUMENTS_BY_TREE,
  leafId
});

const setDocumentsSearchValues = (values, society_id, activeTabId) => ({
  type: SET_DOCUMENTS_SEARCH_VALUES,
  values,
  society_id,
  activeTabId
});

const getAccountsTreeAttempt = (society_id, tabId) => ({
  type: GET_ACCOUNTS_TREE_ATTEMPT,
  society_id,
  tabId
});

const getAccountsTreeSuccess = (society_id, tabId, tree) => ({
  type: GET_ACCOUNTS_TREE_SUCCESS,
  tree,
  society_id,
  tabId
});

const getAccountsTreeFail = (society_id, tabId) => ({
  type: GET_ACCOUNTS_TREE_FAIL,
  society_id,
  tabId
});

const getDocumentsAttempt = (society_id, activeTabId) => ({
  type: GET_DOCUMENTS_ATTEMPT,
  society_id,
  tabId: activeTabId
});

const setItemsPerPage = count => ({
  type: SET_ITEMS_PER_PAGE,
  itemsPerPage: count
});

const showSendInDiscussionModal = status => ({
  type: SHOW_SEND_IN_DISCUSSION_MODAL,
  status
});

const setActiveDocFolder = folder => ({
  type: SET_ACTIVE_FOLDER,
  activeFolder: folder
});

const getDocumentsSuccess = (
  documents,
  society_id,
  activeTabId,
  totalDocs,
  currentPage,
  isSearch
) => ({
  type: GET_DOCUMENTS_SUCCESS,
  documents,
  society_id,
  tabId: activeTabId,
  totalDocs,
  currentPage,
  isSearch
});

const getHeaderSuccess = (
  header,
  society_id,
  activeTabId
) => ({
  type: GET_HEADER_SUCCESS,
  header,
  society_id,
  activeTabId
});

const getDocumentsFail = (society_id, activeTabId) => ({
  type: GET_DOCUMENTS_FAIL,
  society_id,
  tabId: activeTabId
});

const getDocumentSearchAttempt = (society_id, activeTabId) => ({
  type: GET_DOCUMENTS_SEARCH_ATTEMPT,
  society_id,
  tabId: activeTabId
});

const getDocumentSearchSuccess = (documents, society_id, activeTabId) => ({
  type: GET_DOCUMENTS_SEARCH_SUCCESS,
  documents,
  society_id,
  tabId: activeTabId
});

const getDocumentSearchFail = (society_id, activeTabId) => ({
  type: GET_DOCUMENTS_SEARCH_FAIL,
  society_id,
  tabId: activeTabId
});

const setLinkedDocsAttempt = id => ({
  type: SET_LINKED_DOCS_ATTEMPT,
  id
});
const setLinkedDocsSuccess = (docs, id) => ({
  type: SET_LINKED_DOCS_SUCCESS,
  docs,
  id
});
const setLinkedDocsFail = (docs, id) => ({
  type: SET_LINKED_DOCS_FAIL,
  docs,
  id
});

const setDocumentsMode = (
  society_id,
  activeTabId,
  isSearch
) => ({
  type: SET_DOCUMENTS_MODE,
  society_id,
  tabId: activeTabId,
  isSearch
});

const setDocumentToCopy = ({
  document,
  location
}) => ({
  type: SET_DOCUMENT_TO_COPY,
  copy: { ...(document && { document }), location }
});


export default {
  setLinkedDocsAttempt,
  setLinkedDocsSuccess,
  setLinkedDocsFail,
  setActiveTab,
  getDocumentsByTree,
  setDocumentsSearchValues,
  getAccountsTreeAttempt,
  getAccountsTreeSuccess,
  getAccountsTreeFail,
  getDocumentsAttempt,
  getDocumentsSuccess,
  getDocumentsFail,
  getDocumentSearchAttempt,
  getDocumentSearchSuccess,
  showSendInDiscussionModal,
  getDocumentSearchFail,
  setActiveDocFolder,
  getHeaderSuccess,
  setItemsPerPage,
  setDocumentsMode,
  setDocumentToCopy
};
