import moment from 'moment';
import { service_ged } from 'helpers/api';
import { handleError } from 'common/redux/error';
import _ from 'lodash';
import actions from './actions';

export class DocManService {
  static instance;

  static getInstance() {
    if (!DocManService.instance) DocManService.instance = new DocManService();
    return DocManService.instance;
  }

  setDocumentsMode = isSearch => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const { activeTabId } = state.docManagement;
    await dispatch(actions.setDocumentsMode(society_id, activeTabId, isSearch));
  };

  getSearchDate = () => async (dispatch, getState) => {
    const state = getState();
    const { values } = state.form.documentManagementHeaderSearch;
    const {
      dateStart,
      dateEnd,
      search_type
    } = values;
    const date = search_type === 'toutes' || !dateEnd ? {} : {
      start_date: moment(dateStart).format('YYYYMMDD'),
      end_date: moment(dateEnd).format('YYYYMMDD')
    };
    return {
      ...date
    };
  };

  getSearchValues = () => async (dispatch, getState) => {
    const state = getState();
    const { values } = state.form.documentManagementHeaderSearch;
    const {
      doc_name,
      debit,
      debit_amount,
      user_name,
      format_document
    } = values;
    const date = await dispatch(this.getSearchDate());

    let r = {
      ...date,
      label_document: doc_name,
      montant: debit,
      plus_less: debit_amount,
      format_document,
      pers_physique: user_name
    };

    // filter empty values
    r = Object.fromEntries(Object.entries(r).filter(([, v]) => v));

    return r;
  };

  getActiveFolderParams = leaf => async (dispatch, getState) => {
    const state = getState();
    const { activeTabId } = state.docManagement;
    const search_date = await dispatch(this.getSearchDate());
    let requestParams = {};
    if (activeTabId === 0) {
      requestParams = {
        ...search_date
      };
    }
    if (activeTabId === 1) {
      requestParams = !_.isEmpty(leaf) ? {
        code_ocr_rtvr: leaf.code,
        search_type: leaf.code
      } : search_date;
    }
    if (activeTabId === 2) {
      requestParams = !_.isEmpty(leaf) ? {
        no_compte:
        leaf.account_number
      } : search_date;
    }
    if (activeTabId === 3) {
      const journal_id = _.get(leaf, 'journal_id', undefined);
      const journal_label = _.get(leaf, 'label', undefined);
      const date_piece_mm_aaaa = _.get(leaf, 'date_piece_mm_aaaa', undefined);
      requestParams = !_.isEmpty(leaf) ? {
        date_piece_mm_aaaa: date_piece_mm_aaaa || journal_label.trim().split(' ')[0],
        id_journaux: journal_id
      } : search_date;
    }
    if (activeTabId === 4) {
      const exercice = _.get(leaf, 'id_exercice', undefined);
      const type_dossier = _.get(leaf, 'type_dossier', undefined);
      const dossier_revision = _.get(leaf, 'dossier_revision_id', undefined);
      const section = _.get(leaf, 'id_section', undefined);
      const cycle_da_dp = _.get(leaf, 'id_cycle', undefined);
      const ref_da_dp = _.get(leaf, 'id_ref_da_dp', undefined);
      requestParams = !_.isEmpty(leaf) ? {
        exercice,
        dossier_revision,
        type_dossier,
        section,
        cycle_da_dp,
        ref_da_dp
      } : search_date;
    }
    if (activeTabId === 5) {
      const folder_id = _.get(leaf, 'folder_id', undefined);
      const room_id = _.get(leaf, 'room_id', undefined);
      requestParams = !_.isEmpty(leaf) ? {
        folder_id,
        room_id
      } : search_date;
    }

    return requestParams;
  };

  getTree = (tabId, isSearch) => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const { prevActiveTabId } = state.docManagement;
    if (prevActiveTabId === tabId) return false;
    try {
      if (prevActiveTabId !== tabId && !isSearch) {
        await dispatch(actions.getAccountsTreeAttempt(society_id, tabId));
      }
      const search = await dispatch(this.getSearchValues());
      if (tabId === 0) {
        dispatch(this.getDocuments());
        return tabId;
      }
      const response = await service_ged.makeApiCall(`/ged/tab/${tabId + 1}`, 'get', { ...search });
      const tree = response.data ? response.data[0] : {};
      await dispatch(actions.getAccountsTreeSuccess(society_id, tabId, tree));
      return response;
    } catch (err) {
      await dispatch(actions.getAccountsTreeFail(society_id, tabId));
      await dispatch(handleError(err));
      return err;
    }
  };

  setActiveTab = tabId => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    try {
      await dispatch(actions.setActiveTab(tabId, society_id));
    } catch (err) {
      await dispatch(handleError(err));
      throw err;
    }
  };

  getDocuments = (
    leaf, page_number = 1, rows_per_page, sort_name, sort_order
  ) => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const { activeTabId } = state.docManagement;
    const tabId = activeTabId + 1;
    try {
      const items_per_page = 1000000;
      const pagination = { items_per_page, page_number };
      await dispatch(actions.getDocumentsAttempt(society_id, activeTabId));
      const requestParams = await dispatch(this.getActiveFolderParams(leaf));
      const search_params = await dispatch(this.getSearchValues());
      const response = await service_ged.makeApiCall(`/ged/tab/${tabId}/docs`, 'get',
        {
          ...requestParams,
          ...pagination,
          ...search_params,
          ...(sort_name && { sort_name }),
          ...(sort_order && { sort_order })
        });
      const documents = response.data ? response.data : {};
      const totalDocs = _.get(documents, 'pagination.total', 10);
      const currentPage = _.get(documents, 'pagination.currentPage', 1);
      let header;
      header = _.get(documents, 'header', []).filter(column => column.keyLabel !== 'id');
      if (activeTabId === 0) {
        header = header.filter(column => column.keyLabel !== 'company_name');
      }
      const index = header.findIndex(x => x.keyLabel === 'linked_to');
      // sort header if linked_to exist
      if (header[index]) {
        const linked = header[index];
        header.splice(index, 1);
        header.push(linked);
      } else {
        header.push({ type: 'empty', keyLabel: '' });
      }
      const heeaderWithEmptyColumn = [...header];
      const body = _.get(documents, 'body', []);
      await dispatch(actions.getHeaderSuccess(
        header,
        society_id,
        activeTabId
      ));
      await dispatch(actions.getDocumentsSuccess(
        { header: _.isEmpty(body) ? header : heeaderWithEmptyColumn, body },
        society_id,
        activeTabId,
        totalDocs,
        currentPage
      ));
      return documents;
    } catch (err) {
      await dispatch(actions.getDocumentsFail(society_id, activeTabId));
      await dispatch(handleError(err));
      return err;
    }
  };

  saveDocumentsSearchValues = val => async (dispatch, getState) => {
    const state = getState();
    const society_id = state.navigation.id;
    const { activeTabId } = state.docManagement;
    const { values } = state.form.documentManagementHeaderSearch;
    try {
      await dispatch(actions.setDocumentsSearchValues(val || values, society_id, activeTabId));
    } catch (err) {
      await dispatch(handleError(err));
      throw err;
    }
  };
}

export default DocManService.getInstance();
