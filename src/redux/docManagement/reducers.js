import _ from 'lodash';
import {
  GET_ACCOUNTS_TREE_ATTEMPT,
  GET_ACCOUNTS_TREE_SUCCESS,
  GET_ACCOUNTS_TREE_FAIL,
  GET_DOCUMENTS_BY_TREE,
  SET_ACTIVE_TAB,
  SET_DOCUMENTS_SEARCH_VALUES,
  GET_DOCUMENTS_ATTEMPT,
  GET_DOCUMENTS_SUCCESS,
  GET_DOCUMENTS_FAIL,
  SET_ACTIVE_FOLDER,
  SET_ITEMS_PER_PAGE,
  GET_HEADER_SUCCESS,
  SET_DOCUMENTS_MODE,
  SET_LINKED_DOCS_ATTEMPT,
  SET_LINKED_DOCS_SUCCESS,
  SET_LINKED_DOCS_FAIL,
  SHOW_SEND_IN_DISCUSSION_MODAL,
  SET_DOCUMENT_TO_COPY
} from './constants';
import {
  inProgresTableData,
  inProgresOCRTableData,
  inProgresManuelleTableData,
  inProgresCollecteursTableData
} from './dummyData';

const initialState = {
  activeTabId: 0,
  prevActiveTabId: undefined,
  itemsPerPage: 10,
  gedTableData: {
    header: [],
    body: []
  },
  isShowSendInDiscussionModal: false,
  selectedDocumentToCopy: {},
  documentMode: {},
  tree: {},
  documents: {},
  headers: {},
  activeFolder: {},
  linked_docs: {},
  searchData: {}
};

const docMan = (state = initialState, action) => {
  const {
    activeTabId,
    leafId
  } = action;

  if (action && action.type) {
    switch (action.type) {
    case SET_LINKED_DOCS_ATTEMPT:
      if (_.isEmpty(state.linked_docs[action.id])) {
        return {
          ...state,
          linked_docs: {
            ...state.linked_docs,
            [action.id]: []
          }
        };
      }
      return state;
    case SET_LINKED_DOCS_SUCCESS:
      if (!_.isEmpty(state.linked_docs[action.id])) {
        return state;
      }
      return {
        ...state,
        linked_docs: {
          ...state.linked_docs,
          [action.id]: action.docs
        }
      };
    case SET_LINKED_DOCS_FAIL:
      return {
        ...state,
        linked_docs: {
          ...state.linked_docs,
          [action.id]: []
        }
      };
    case GET_ACCOUNTS_TREE_ATTEMPT:
      return {
        ...state,
        tree: {
          [action.society_id]: {
            ...state.tree[action.society_id],
            [action.tabId]: {
              isLoading: true,
              isError: false,
              totalQty: undefined,
              tree_list: []
            }
          }
        }
      };
    case GET_ACCOUNTS_TREE_SUCCESS:
      return {
        ...state,
        tree: {
          [action.society_id]: {
            ...state.tree[action.society_id],
            [action.tabId]: {
              isLoading: false,
              isError: false,
              totalQty: action.tree.totalQty,
              /** reverse for discussion tab */
              tree_list: state.activeTabId === 5 ? action.tree.tree.reverse() : action.tree.tree
            }
          }
        }
      };
    case GET_ACCOUNTS_TREE_FAIL:
      return {
        ...state,
        tree: {
          [action.society_id]: {
            ...state.tree[action.society_id],
            [action.tabId]: {
              isLoading: false,
              isError: true,
              totalQty: undefined,
              tree_list: []
            }
          }
        }
      };
    case GET_DOCUMENTS_ATTEMPT:
      return {
        ...state,
        currentPage: 1,
        totalDocs: 0,
        documents: {
          [action.society_id]: {
            ...state.documents[action.society_id],
            [action.tabId]: {
              ...state.documents[action.society_id][action.tabId],
              isLoading: true,
              isError: false,
              documents_header: [],
              documents_list: []
            }
          }
        }
      };
    case GET_DOCUMENTS_SUCCESS:
      return {
        ...state,
        currentPage: action.currentPage,
        totalDocs: action.totalDocs,
        documents: {
          [action.society_id]: {
            ...state.documents[action.society_id],
            [action.tabId]: {
              ...state.documents[action.society_id][action.tabId],
              isLoading: false,
              isError: false,
              documents_header: action.documents.header,
              documents_list: action.documents.body,
              isSearch: action.isSearch
            }
          }
        }
      };
    case SET_DOCUMENTS_MODE:
      return {
        ...state,
        documentMode: {
          [action.society_id]: {
            ...state.documentMode[action.society_id],
            [action.tabId]: {
              isSearch: action.isSearch
            }
          }
        }
      };
    case GET_HEADER_SUCCESS:
      return {
        ...state,
        currentPage: action.currentPage,
        totalDocs: action.totalDocs,
        headers: {
          [action.society_id]: {
            ...state.headers[action.society_id],
            [action.activeTabId]: {
              documents_header: action.header
            }
          }
        }
      };
    case SET_ACTIVE_FOLDER:
      return {
        ...state,
        activeFolder: action.activeFolder
      };
    case SET_ITEMS_PER_PAGE:
      return {
        ...state,
        itemsPerPage: action.itemsPerPage
      };
    case GET_DOCUMENTS_FAIL:
      return {
        ...state,
        documents: {
          [action.society_id]: {
            ...state.documents[action.society_id],
            [action.tabId]: {
              isLoading: false,
              isError: true,
              documents_header: [],
              documents_list: []
            }
          }
        }
      };
    case SET_DOCUMENTS_SEARCH_VALUES:
      return {
        ...state,
        searchData: {
          [action.society_id]: {
            ...state.searchData[action.society_id],
            [action.activeTabId]: {
              searchValues: action.values
            }
          }
        }
      };
    case SET_ACTIVE_TAB: {
      return {
        ...state,
        prevActiveTabId: state.activeTabId,
        activeTabId
      };
    }
    case SHOW_SEND_IN_DISCUSSION_MODAL: {
      return {
        ...state,
        isShowSendInDiscussionModal: action.status
      };
    }
    case SET_DOCUMENT_TO_COPY: {
      return {
        ...state,
        isShowSendInDiscussionModal: action.status,
        selectedDocumentToCopy: {
          ...state.selectedDocumentToCopy,
          ...action.copy
        }
      };
    }
    case GET_DOCUMENTS_BY_TREE: {
      if (leafId === 0) {
        return {
          ...state,
          gedTableData: {
            ...state.gedTableData,
            body: [...inProgresTableData]
          }
        };
      }
      if (leafId === 11) {
        return {
          ...state,
          gedTableData: {
            ...state.gedTableData,
            body: [...inProgresOCRTableData]
          }
        };
      }
      if (leafId === 12) {
        return {
          ...state,
          gedTableData: {
            ...state.gedTableData,
            body: [...inProgresManuelleTableData]
          }
        };
      }
      if (leafId === 13) {
        return {
          ...state,
          gedTableData: {
            ...state.gedTableData,
            body: [...inProgresCollecteursTableData]
          }
        };
      }
      break;
    }
    default: return state;
    }
  }
  return state;
};

export default docMan;
