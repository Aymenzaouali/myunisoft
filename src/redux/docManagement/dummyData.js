// General
export const generalTableData = [
  {
    id: 0,
    name: 'testDocument.mob',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    linkedTo: 'User Name'
  },
  {
    id: 1,
    name: 'testDocument.txt',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    linkedTo: 'User Name'
  }
];

// En cours OCR
export const inProgresOCRTableData = [
  {
    id: 0,
    name: 'refer.docx',
    entitled: 'lorem',
    date: '2018-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  },
  {
    id: 1,
    name: 'ecomerce.zip',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  }
];

// En cours Saisie Manuelle
export const inProgresManuelleTableData = [
  {
    id: 0,
    name: 'schema.pdf',
    entitled: 'lorem',
    date: '2018-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  },
  {
    id: 1,
    name: 'gogo.mp3',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  }
];
// En cours Saisie Collecteurs
export const inProgresCollecteursTableData = [
  {
    id: 0,
    name: 'sea.mkv',
    entitled: 'lorem',
    date: '2018-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  },
  {
    id: 1,
    name: 'plan.txt',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  }
];

// En cours
export const inProgresTableData = [
  {
    id: 0,
    name: 'refer.docx',
    entitled: 'lorem',
    date: '2018-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  },
  {
    id: 1,
    name: 'ecomerce.zip',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  },
  {
    id: 2,
    name: 'schema.pdf',
    entitled: 'lorem',
    date: '2018-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  },
  {
    id: 3,
    name: 'gogo.mp3',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  },
  {
    id: 4,
    name: 'sea.mkv',
    entitled: 'lorem',
    date: '2018-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  },
  {
    id: 5,
    name: 'plan.txt',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    amountTTC: '123456'
  }
];

// Comptes
export const gedAccountsTableData = [
  {
    id: 0,
    name: 'testDocument.pdf',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    linkedTo: 'User Name',
    amountTT: 458,
    amountHT: 55664544,
    amountTVA: 4564564,
    dueDate: '2019-07-24 19:42:36.590019',
    comments: 'SOme long comment',
    letter: 'Oui'
  },
  {
    id: 1,
    name: 'report.xls',
    entitled: 'lorem',
    date: '2018-05-24 09:42:36.590019',
    linkedTo: 'User Name',
    amountTT: 458,
    amountHT: 55664544,
    amountTVA: 4564564,
    dueDate: '2019-07-24 19:42:36.590019',
    comments: 'SOme long comment',
    letter: 'Oui'
  },
  {
    id: 2,
    name: 'users.doc',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    linkedTo: 'User Name',
    amountTT: 458,
    amountHT: 55664544,
    amountTVA: 4564564,
    dueDate: '2019-07-24 19:42:36.590019',
    comments: 'SOme long comment',
    letter: 'Oui'
  }
];


// Écritures
export const scripturesTableData = [
  {
    id: 0,
    name: 'gvid.mp3',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    linkedTo: 'Some name',
    amountTTC: '123456',
    amountHT: '123456',
    amountTVA: '123456',
    dueDate: '2019-05-24 09:42:36.590019',
    comments: 'sersddf',
    letter: 'qwerty'
  },
  {
    id: 1,
    name: 'gvid.png',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    linkedTo: 'Some name',
    amountTTC: '123456',
    amountHT: '123456',
    amountTVA: '123456',
    dueDate: '2019-05-24 09:42:36.590019',
    comments: 'sersddf',
    letter: 'qwerty'
  }
];

// DA/DP - list
export const lDADPDTableData = [
  {
    id: 0,
    name: 'gvid.tiff',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    linkedTo: 'Some name',
    titleFT: 'lorem',
    accountNumber: '123456',
    comments: 'sersddf',
    amountTTC: '123456',
    amountTVA: '123456',
    amountHT: '123456'
  }
];

// Discussions
export const discussionsTableData = [
  {
    id: 0,
    name: 'arch.rar',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019',
    title: 'Some title',
    bodyMessage: '123456',
    tags: 'one'
    // tags: [{ id: 0, message: 'sdfsdf' }, { id: 1, message: '123' }]
  }
];

// Base documentaire
export const baseTableData = [
  {
    id: 0,
    name: 'arch.zip',
    entitled: 'lorem',
    date: '2019-05-24 09:42:36.590019',
    addedBy: '2019-05-24 09:42:36.590019'
  }
];

// TREES

export const sampleTree = [
  {
    title: 'Vie du Cabinet (280)',
    id: 'Vie du cabinet',
    createdBy: 'Robert Niro',
    createdAt: '2018-04-23T18:25:43.511Z',
    isFolder: true,
    children: [
      {
        title: 'Compatabilite (2)',
        id: 'Compatabilite',
        canAdd: true,
        children: [
          {
            title: 'Declaration TVA',
            id: 'Declaration TVA'
          },
          {
            title: 'Lorem ipsum',
            id: 'Lorem ipsum',
            canAdd: true
          },
          {
            title: 'Report bilan erreTOO LONG STRING',
            id: 'Report bilan erreTOO LONG STRING'
          },
          {
            title: 'Other',
            id: 'Other1',
            children: [
              {
                title: 'ccc1 (30)',
                id: 'ccc1',
                children: [
                  {
                    title: 'Report bilan erreTOO LONG STRING',
                    id: 'Report bilan erreTOO LONG STRING'
                  },
                  {
                    title: 'Other',
                    id: 'Other2',
                    children: [
                      {
                        title: 'ccc1',
                        id: 'ccc11',
                        children: [
                          {
                            title: 'Report bilan erreTOO LONG STRING',
                            id: 'Report bilan erreTOO LONG STRING'
                          },
                          {
                            title: 'Other',
                            id: 'Other3',
                            children: [
                              {
                                title: 'ccc1',
                                id: 'ccc12'
                              },
                              {
                                title: 'ccc2',
                                id: 'ccc22',
                                children: [
                                  {
                                    title: 'Report bilan erreTOO LONG STRING',
                                    id: 'Report bilan erreTOO LONG STRING'
                                  },
                                  {
                                    title: 'Other',
                                    id: 'Other4',
                                    children: [
                                      {
                                        title: 'ccc1',
                                        id: 'ccc13'
                                      },
                                      {
                                        title: 'ccc2',
                                        id: 'ccc22'
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      },
                      {
                        title: 'ccc2',
                        id: 'ccc2'
                      }
                    ]
                  }
                ]
              },
              {
                title: 'ccc2',
                id: 'ccc2'
              }
            ]
          }
        ]
      },
      {
        title: 'Fiscalite0',
        id: 'Fiscalite0',
        children: [
          {
            title: 'fff1',
            id: 'fff1'
          },
          {
            title: 'fff2',
            id: 'fff2'
          }
        ]
      },
      {
        title: 'Jusridique',
        id: 'Jusridique',
        children: [
          {
            title: 'fff1',
            id: 'fff1'
          },
          {
            title: 'fff2',
            id: 'fff2'
          }
        ]
      },
      {
        title: 'Autres',
        id: 'Autres',
        canAdd: true,
        children: [
          {
            title: 'fff1',
            id: 'fff1'
          },
          {
            title: 'fff2',
            id: 'fff2'
          }
        ]
      },
      {
        title: 'SomeElse  ',
        id: 'SomeElse  ',
        children: [
          {
            title: 'fff1',
            id: 'fff1'
          },
          {
            title: 'fff2',
            id: 'fff2'
          }
        ]
      }
    ]
  }
];

export const inProgressTree = {
  tree: [
    {
      label: 'Une société',
      account_number: 0,
      isFolder: true,
      children: [
        {
          id: Math.floor(Math.random() * 100000),
          label: 'OCR',
          search_type: 'OCR'
        },
        {
          id: Math.floor(Math.random() * 100000),
          label: 'Saisie Manuelle',
          search_type: 'SaisieManuelle'
        },
        {
          id: Math.floor(Math.random() * 100000),
          label: 'Collecteurs',
          search_type: 'RTVR'
        }
      ]
    }
  ]
};
