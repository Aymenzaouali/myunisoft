import {
  SET_ERROR,
  SET_ALL_LINES_ERRORS
} from './constants';

const setError = (formId, id, error) => ({
  type: SET_ERROR,
  formId,
  id,
  error
});

const setAllLinesErrors = (formId, errors) => ({
  type: SET_ALL_LINES_ERRORS,
  formId,
  errors
});


export default {
  setError,
  setAllLinesErrors
};
