import _get from 'lodash/get';
import {
  SET_ERROR,
  SET_ALL_LINES_ERRORS
} from './constants';

const pdfForms = (state = {}, action) => {
  const currentPdfForm = _get(state, action.formId, {});
  const errors = _get(currentPdfForm, 'errors', {});
  switch (action.type) {
  case SET_ERROR:
    return {
      ...state,
      [action.formId]: {
        ...currentPdfForm,
        errors: {
          ...errors,
          [action.id]: action.error
        }
      }
    };
  case SET_ALL_LINES_ERRORS:
    return {
      ...state,
      [action.formId]: {
        ...currentPdfForm,
        errors: action.errors || {}
      }
    };
  default:
    return state;
  }
};

export default pdfForms;
