import { OPENED_WINDOW, UPDATE_WINDOW, CLOSE_WINDOW } from './constants';

// Actions
const openedWindow = (id, ref) => ({ type: OPENED_WINDOW, id, ref });
const updateWindow = (id, state) => ({ type: UPDATE_WINDOW, id, state });
const closeWindow = id => ({ type: CLOSE_WINDOW, id });

export {
  openedWindow, updateWindow, closeWindow
};
