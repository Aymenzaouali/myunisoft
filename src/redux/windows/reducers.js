import { OPENED_WINDOW, UPDATE_WINDOW, CLOSE_WINDOW } from './constants';

// Reducer
const windowsReducer = (state = {}, action) => {
  switch (action.type) {
  case OPENED_WINDOW:
    return {
      [action.id]: {
        ref: action.ref,

        loading: true,
        focused: false
      },

      ...state
    };

  case UPDATE_WINDOW: {
    const { [action.id]: ws, ...others } = state;

    return {
      ...others,

      [action.id]: {
        ...ws,
        ...action.state
      }
    };
  }

  case CLOSE_WINDOW: {
    const { [action.id]: ws, ...others } = state;

    return { ...others };
  }

  default:
    return state;
  }
};

export default windowsReducer;
