import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';
import _ from 'lodash';
import {
  getExercisesSuccess,
  getExercisesFail,
  getExercisesAttempt
} from './actions';

export const getExercises = () => async (dispatch, getState) => {
  try {
    await dispatch(getExercisesAttempt());
    const society_id = _.get(getState(), 'navigation.id', -2);
    const { data } = await windev.makeApiCall('/exercices', 'get', { society_id });
    await dispatch(getExercisesSuccess(data));
    return data;
  } catch (err) {
    await dispatch(getExercisesFail());
    await dispatch(handleError(err));
    return err;
  }
};
