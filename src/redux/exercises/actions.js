import {
  GET_EXERCISES_SUCCESS,
  GET_EXERCISES_ATTEMPT,
  GET_EXERCISES_FAIL
} from './constants';

export const getExercisesSuccess = payload => ({
  type: GET_EXERCISES_SUCCESS,
  payload
});
export const getExercisesAttempt = () => ({
  type: GET_EXERCISES_ATTEMPT
});
export const getExercisesFail = () => ({
  type: GET_EXERCISES_FAIL
});
