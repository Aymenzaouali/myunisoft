import {
  GET_EXERCISES_SUCCESS
} from './constants';

const initialState = [];

const balance = (state = initialState, action) => {
  switch (action.type) {
  case GET_EXERCISES_SUCCESS: {
    return action.payload;
  }
  default:
    return state;
  }
};

export default balance;
