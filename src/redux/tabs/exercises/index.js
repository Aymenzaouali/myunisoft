import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';

import {
  getExercisesSuccess,
  getExercisesFail,
  getExercisesAttempt
} from './actions';

export const getExercises = (society_id = null) => async (dispatch, getState) => {
  const state = getState();
  const params = {
    society_id: society_id || state.navigation.id
  };

  try {
    await dispatch(getExercisesAttempt());
    const { data } = await windev.makeApiCall('/exercices', 'get', params);
    await dispatch(getExercisesSuccess(data));
    return data;
  } catch (err) {
    await dispatch(getExercisesFail());
    await dispatch(handleError(err));
    return err;
  }
};
