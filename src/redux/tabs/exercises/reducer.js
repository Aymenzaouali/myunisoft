import { GET_EXERCISES_SUCCESS } from './constants';

const initialState = null;

const exercises = (state = initialState, action) => {
  switch (action.type) {
  case GET_EXERCISES_SUCCESS: {
    return action.exercises || [];
  }
  default:
    return state;
  }
};

export default exercises;
