import { GET_EXERCISES_ATTEMPT, GET_EXERCISES_SUCCESS, GET_EXERCISES_FAIL } from './constants';

export const getExercisesAttempt = () => ({ type: GET_EXERCISES_ATTEMPT });
export const getExercisesSuccess = exercises => ({ type: GET_EXERCISES_SUCCESS, exercises });
export const getExercisesFail = () => ({ type: GET_EXERCISES_FAIL });
