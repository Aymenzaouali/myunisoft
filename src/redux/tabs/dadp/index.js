import { formValueSelector } from 'redux-form';
import moment from 'moment';
import _ from 'lodash';
import { windev } from 'helpers/api';
import {
  VALIDATION_TYPES_FB,
  VALIDATION_STATES_FB,
  evolGroupValidation,
  setDossierRevisionId
} from 'helpers/dadp';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import { routesByKey } from 'helpers/routes';
import { downloadFile } from 'helpers/file';
import {
  getReviewsAttempt,
  getReviewsSuccess,
  getReviewsFail,
  selectReview,
  getSectionAttempt,
  getSectionSuccess,
  getSectionFail,
  getCyclesAttempt,
  getCyclesSuccess,
  getCyclesFail,
  selectCycle,
  refreshCycleAttempt,
  refreshCycleSuccess,
  refreshCycleFail,
  getSummaryAttempt,
  getSummarySuccess,
  getSummaryFail,
  getCommentsAttempt,
  getCommentsSuccess,
  getCommentsFail,
  updateCommentAttempt,
  updateCommentSuccess,
  updateCommentFail,
  getDiligencesAttempt,
  getDiligencesSuccess,
  getDiligencesFail,
  updateDiligenceAttempt,
  updateDiligenceSuccess,
  updateDiligenceFail,
  addDiligencePJ,
  bulkUpdateDiligenceSuccess,
  setDiligenceComments,
  setDiligenceCommentsAttempt,
  setDiligenceCommentsFail,
  addDiligenceComment,
  addDiligenceCommentAttempt,
  addDiligenceCommentFail,
  updateDiligenceComment,
  updateDiligenceCommentAttempt,
  updateDiligenceCommentFail,
  getAnalyticReviewAttempt,
  getAnalyticReviewSuccess,
  getAnalyticReviewFail,
  updateAnalyticReviewAttempt,
  updateAnalyticReviewSuccess,
  updateAnalyticReviewFail,
  getDilligenceIdAttempt,
  getDilligenceIdSuccess,
  getDilligenceIdFail,
  getPlaquetteModelsAttempt,
  getPlaquetteModelsSuccess,
  getPlaquetteModelsFail,
  getPlaquetteComplementAttempt,
  getPlaquetteComplementSuccess,
  getPlaquetteComplementFail,
  getPlaquetteTreeAttempt,
  getPlaquetteTreeSuccess,
  getPlaquetteTreeFail,
  postPlaquetteAttempt,
  postPlaquetteSuccess,
  postPlaquetteFail
} from './actions';

// Thunks
export const getReviews = () => async (dispatch, getState) => {
  const state = getState();
  const societyId = _.get(state, 'navigation.id');
  let exercices = _.get(
    state,
    `accountingWeb.exercice.${societyId}.exercice`,
    []
  );
  try {
    if (!exercices.length) {
      const { data } = await windev.makeApiCall(
        '/exercices',
        'get',
        { society_id: societyId },
        {}
      );
      exercices = data;
    }
    await dispatch(getReviewsAttempt());
    const { data } = await windev.makeApiCall(
      '/dadp/dossier_revision_list',
      'get'
    );
    let { dossier_revision_list: reviews } = data;
    reviews = reviews && true ? reviews : [];
    await dispatch(getReviewsSuccess(reviews));
    if (reviews.length > 0) {
      const id = setDossierRevisionId(
        reviews,
        exercices,
        reviews[0].id_dossier_revision
      );
      dispatch(selectReview(id));
    }
  } catch (err) {
    await dispatch(getReviewsFail());
    throw err;
  }
};
export const getSections = (q, category) => async (dispatch) => {
  const params = { q, category };
  try {
    await dispatch(getSectionAttempt());
    const { data } = await windev.makeApiCall('/dadp/section', 'get', params);
    await dispatch(getSectionSuccess(data));
    return data;
  } catch (err) {
    console.log('dadp.getSection', err);
    await dispatch(getSectionFail());
    throw err;
  }
};
export const getCycles = (
  category,
  review_id,
  start_date,
  end_date,
  section_id
) => async (dispatch) => {
  const params = {
    category,
    dossier_revision_id: review_id,
    start_date: moment(start_date).format('YYYY-MM-DD'),
    end_date: moment(end_date).format('YYYY-MM-DD'),
    section_id,
    cycle_da_dp_id: -1
  };
  try {
    await dispatch(getCyclesAttempt());
    let cycles = [];
    if (section_id > 0) {
      const { data } = await windev.makeApiCall('/dadp/cycle', 'get', params);
      cycles = data;
    }
    await dispatch(getCyclesSuccess(cycles));
    if (cycles.length > 0) {
      dispatch(selectCycle(cycles[0].cycle_da_dp_id));
    }
  } catch (err) {
    console.log('dadp.getCycles', err);
    await dispatch(getCyclesFail());
    throw err;
  }
};
export const refreshCycle = (
  cycle_id,
  type = null,
  prev_state = null,
  new_state = null
) => async (dispach, getState) => {
  const state = getState();
  const { dadp } = getCurrentTabState(state);
  // if type is null => refresh all types
  const types = type ? [type] : ['valid_collab', 'valid_rm'];
  const cycle = dadp.cycles.data.find(c => c.cycle_da_dp_id === cycle_id);
  // Can self compute ?
  if (cycle && type && prev_state && new_state) {
    const res = evolGroupValidation(prev_state, new_state, cycle[type]);
    if (res != null) {
      await dispach(refreshCycleSuccess(cycle_id, type, res));
      return;
    }
  }
  // Ask the server
  const sectionSelector = formValueSelector(
    tabFormName('dadpSection', state.navigation.id)
  );
  const reviewSelector = formValueSelector(
    tabFormName('dadpReviewInfo', state.navigation.id)
  );
  const params = {
    category: dadp.category,
    dossier_revision_id: dadp.reviews.selectedId,
    start_date: moment(reviewSelector(state, 'start_date')).format(
      'YYYY-MM-DD'
    ),
    end_date: moment(reviewSelector(state, 'end_date')).format('YYYY-MM-DD'),
    section_id: _.get(sectionSelector(state, 'section'), 'id_section'),
    cycle_da_dp_id: cycle_id
  };
  try {
    await Promise.all(
      types.map(t => dispach(refreshCycleAttempt(cycle_id, t)))
    );
    const { data } = await windev.makeApiCall('/dadp/cycle', 'get', params);
    const new_cycle = data[0];
    await Promise.all(
      types.map(t => dispach(refreshCycleSuccess(cycle_id, t, new_cycle[t])))
    );
  } catch (err) {
    console.log('dadp.refreshCycle', err);
    await dispach(refreshCycleFail());
    throw err;
  }
};
export const getSummary = (category, revision_id) => async (
  dispatch,
  getState
) => {
  const state = getState();
  const params = {
    society_id: state.navigation.id,
    code: category,
    dossier_revision_id: revision_id
  };
  try {
    await dispatch(getSummaryAttempt());
    const { data: summary } = await windev.makeApiCall(
      '/comments',
      'get',
      params
    );
    await dispatch(getSummarySuccess(summary));
  } catch (err) {
    console.log('dadp.getSummary', err);
    await dispatch(getSummaryFail());
    throw err;
  }
};
export const getComments = (revision_id, cycle_id) => async (
  dispatch,
  getState
) => {
  const state = getState();
  const params = {
    society_id: state.navigation.id,
    dossier_revision_id: revision_id,
    cycle_da_dp_id: cycle_id
  };
  try {
    await dispatch(getCommentsAttempt());
    const { data: comments } = await windev.makeApiCall(
      '/comments',
      'get',
      params
    );
    await dispatch(getCommentsSuccess(comments));
  } catch (err) {
    console.log('dadp.getComments', err);
    await dispatch(getCommentsFail());
    throw err;
  }
};
export const addComment = (
  revision_id,
  section_code,
  cycle_id,
  comment
) => async (dispatch, getState) => {
  const state = getState();
  const body = {
    location: 'cycle',
    society_id: state.navigation.id,
    dossier_revision_id: revision_id,
    cycle_da_dp_id: cycle_id,
    comment
  };
  try {
    await dispatch(updateCommentAttempt());
    const { data: comments } = await windev.makeApiCall(
      '/comments',
      'post',
      {},
      body
    );
    await dispatch(updateCommentSuccess(section_code, cycle_id, comments));
  } catch (err) {
    console.log('dadp.addComment', err);
    await dispatch(updateCommentFail());
    throw err;
  }
};
export const updateComment = (
  revision_id,
  section_code,
  cycle_id,
  comment_id,
  comment
) => async (dispatch) => {
  const body = {
    location: 'cycle',
    dossier_revision_id: revision_id,
    cycle_da_dp_id: cycle_id,
    comment
  };
  try {
    await dispatch(updateCommentAttempt());
    const { data: comments } = await windev.makeApiCall(
      `/comments/${comment_id}`,
      'put',
      {},
      body
    );
    await dispatch(updateCommentSuccess(section_code, cycle_id, comments));
  } catch (err) {
    console.log('dadp.updateComment', err);
    await dispatch(updateCommentFail());
    throw err;
  }
};
export const getDiligences = (
  category,
  section_id,
  cycle_id,
  start_date,
  end_date,
  workSheetOnly
) => async (dispatch) => {
  const params = {
    category,
    section_id,
    cycle_id,
    start_date: moment(start_date).format('YYYY-MM-DD'),
    end_date: moment(end_date).format('YYYY-MM-DD'),
    workSheetOnly
  };
  try {
    await dispatch(getDiligencesAttempt());
    const { data } = await windev.makeApiCall(
      '/dadp/work_program',
      'get',
      params
    );
    const diligences = data.map(d => ({
      ...d,
      children: d.children && {
        ...d.children,
        diligence_list: d.children.diligence_list || []
      }
    }));
    await dispatch(getDiligencesSuccess(diligences));
    return diligences;
  } catch (err) {
    console.log('dadp.getDiligences', err);
    await dispatch(getDiligencesFail());
    throw err;
  }
};
export const updateDiligence = (
  diligence_id,
  type,
  state,
  prev_state
) => async (dispatch, getState) => {
  const params = {
    workprogram_id: diligence_id,
    validation_type: VALIDATION_TYPES_FB[type],
    validation_state: VALIDATION_STATES_FB[state]
  };
  try {
    await dispatch(updateDiligenceAttempt());
    await windev.makeApiCall(
      '/dadp/work_program/validation',
      'post',
      params,
      {}
    );
    await dispatch(updateDiligenceSuccess(diligence_id, type, state));
    const { dadp } = getCurrentTabState(getState());
    await dispatch(
      refreshCycle(dadp.cycles.selectedId, type, prev_state, state)
    );
  } catch (err) {
    console.log('dadp.updateDiligence', err);
    await dispatch(updateDiligenceFail());
    throw err;
  }
};
export const bulkUpdateDiligence = updates => async (dispatch, getState) => {
  try {
    await dispatch(updateDiligenceAttempt());
    const promises = updates.map(
      ({ diligence_id, validation_type, validation_state }) => {
        const params = {
          workprogram_id: diligence_id,
          validation_type: VALIDATION_TYPES_FB[validation_type],
          validation_state: VALIDATION_STATES_FB[validation_state]
        };
        return windev.makeApiCall(
          '/dadp/work_program/validation',
          'post',
          params,
          {}
        );
      }
    );
    await Promise.all(promises);
    await dispatch(bulkUpdateDiligenceSuccess(updates));
    const { dadp } = getCurrentTabState(getState());
    await dispatch(refreshCycle(dadp.cycles.selectedId));
  } catch (err) {
    console.log('dadp.bulkUpdateDiligence', err);
    await dispatch(updateDiligenceFail());
    throw err;
  }
};
export const getDiligenceComments = diligence_id => async (dispatch) => {
  try {
    await dispatch(setDiligenceCommentsAttempt());
    const { data } = await windev.makeApiCall('/comments', 'get', {
      diligence_id
    });
    await dispatch(setDiligenceComments(diligence_id, data));
    return data;
  } catch (err) {
    console.log({ err });
    await dispatch(setDiligenceCommentsFail());
    throw err;
  }
};
export const submitDiligenceComment = (diligence_id, payload) => async (
  dispatch,
  getState
) => {
  const state = getState();
  const body = {
    location: 'DILIGENCE',
    society_id: state.navigation.id,
    diligence_id,
    comment: payload.body
  };
  try {
    await dispatch(addDiligenceCommentAttempt());
    const { data } = await windev.makeApiCall('/comments', 'post', {}, body);
    await dispatch(addDiligenceComment(diligence_id, data));
  } catch (err) {
    console.log({ err });
    await dispatch(addDiligenceCommentFail());
    throw err;
  }
};
export const editDiligenceComment = (
  diligence_id,
  payload
) => async (dispatch) => {
  const body = {
    location: 'DILIGENCE',
    entry_id: payload.id,
    comment: payload.body,
    diligence_id
  };
  try {
    await dispatch(updateDiligenceCommentAttempt());
    const { data } = await windev.makeApiCall(
      `/comments/${payload.id}`,
      'put',
      {},
      body
    );
    await dispatch(updateDiligenceComment(diligence_id, payload.id, data));
  } catch (err) {
    console.log({ err });
    await dispatch(updateDiligenceCommentFail());
    throw err;
  }
};
export const sendDiligencePJ = (diligence_id, files) => async (
  dispatch,
  getState
) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');
  const queries = files.map((file) => {
    const params = {
      location: 'diligence',
      filename_extension: file.name,
      object_id: diligence_id
    };
    return windev.makeApiCall('/document/add_all_types', 'post', params, file, {
      headers: {
        'Content-Type': 'application/octet-stream',
        'society-id': society_id
      }
    });
  });

  try {
    await Promise.all(queries);
    await dispatch(addDiligencePJ(diligence_id, files));
  } catch (err) {
    console.log('dadp.addDiligencePJ', err);
    throw err;
  }
};
export const getAnalyticReview = (review_id, cycle_id) => async (dispatch) => {
  const params = { review_id, cycle_id };
  try {
    await dispatch(getAnalyticReviewAttempt());
    const { data: analyticReview } = await windev.makeApiCall(
      '/dadp/analytic_review',
      'get',
      params
    );
    await dispatch(getAnalyticReviewSuccess(analyticReview));
  } catch (err) {
    console.log('dadp.getAnalyticReview', err);
    await dispatch(getAnalyticReviewFail());
    throw err;
  }
};
export const updateAnalyticReview = (
  review_id,
  account_id,
  type,
  state,
  prev_state
) => async (dispatch, getState) => {
  const params = {
    review_id,
    account_id,
    validation_type: VALIDATION_TYPES_FB[type],
    validation_state: VALIDATION_STATES_FB[state]
  };
  try {
    await dispatch(updateAnalyticReviewAttempt());
    await windev.makeApiCall(
      '/dadp/analytic_review/validation',
      'post',
      params,
      {}
    );
    await dispatch(updateAnalyticReviewSuccess(account_id, type, state));
    const { dadp } = getCurrentTabState(getState());
    await dispatch(
      refreshCycle(dadp.cycles.selectedId, type, prev_state, state)
    );
  } catch (err) {
    console.log('dadp.updateDiligence', err);
    await dispatch(updateAnalyticReviewFail());
    throw err;
  }
};

export const getDilligenceIdByDossierRevision = (
  diligence_code,
  dossier_revision_id,
  page
) => async (dispatch) => {
  const params = {
    diligence_code,
    dossier_revision_id
  };
  const route = {
    url: routesByKey[page]
  };
  try {
    await dispatch(getDilligenceIdAttempt());
    const { data: id_diligence } = await windev.makeApiCall(
      '/dadp/diligence',
      'get',
      params,
      {}
    );
    await dispatch(
      getDilligenceIdSuccess(id_diligence, { ...route, ...id_diligence }, page)
    );
    return id_diligence;
  } catch (err) {
    console.log('dadp.updateDiligence', err);
    await dispatch(getDilligenceIdFail());
    throw err;
  }
};

export const getPlaquetteModels = () => async (dispatch) => {
  try {
    await dispatch(getPlaquetteModelsAttempt());
    const { data } = await windev.makeApiCall('/plaquette_models', 'get', {
      mode: 1
    });
    await dispatch(getPlaquetteModelsSuccess(data));
    return data;
  } catch (err) {
    await dispatch(getPlaquetteModelsFail(err));
    return err;
  }
};

export const getPlaquetteComplement = ({
  reviewId,
  modelId
}) => async (dispatch) => {
  const params = {
    dossier_revision_id: reviewId,
    html_model_id: modelId
  };
  try {
    await dispatch(getPlaquetteComplementAttempt());
    const { data } = await windev.makeApiCall(
      '/plaquette_complement',
      'get',
      params
    );
    await dispatch(getPlaquetteComplementSuccess(data));
    return data;
  } catch (err) {
    await dispatch(getPlaquetteComplementFail(err));
    return err;
  }
};

export const getPlaquetteTreeView = () => async (dispatch) => {
  try {
    await dispatch(getPlaquetteTreeAttempt());
    const { data } = await windev.makeApiCall(
      '/plaquetteTreeView',
      'get',
      {},
      {}
    );
    await dispatch(getPlaquetteTreeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(getPlaquetteTreeFail(err));
    return err;
  }
};

export const postPlaquette = ({ payload, reviewId }) => async (dispatch) => {
  const codes = Object.keys(payload).map(key => ({
    code: key,
    checked: payload[key]
  }));
  try {
    await dispatch(postPlaquetteAttempt());
    const { data } = await windev.makeApiCall(
      '/plaquette',
      'post',
      {
        dossier_revision_id: reviewId
      },
      codes,
      { responseType: 'blob' }
    );
    await dispatch(postPlaquetteSuccess(data));
    downloadFile(data, `Plaquette_${moment().format()}`);
    return data;
  } catch (err) {
    await dispatch(postPlaquetteFail(err));
    return err;
  }
};
