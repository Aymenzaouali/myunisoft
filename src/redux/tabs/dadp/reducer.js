import {
  commentsWrap,
  formatDiligence,
  updateSummary,
  updateDiligences,
  bulkUpdateDiligences,
  updateAccount,
  formatReview
} from 'helpers/dadp';

import {
  SET_CATEGORY, SET_SUB_CATEGORY,
  GET_REVIEWS_ATTEMPT, GET_REVIEWS_SUCCESS, GET_REVIEWS_FAIL, SELECT_REVIEW,
  GET_SECTION_ATTEMPT, GET_SECTION_SUCCESS, GET_SECTION_FAIL,
  GET_CYCLES_ATTEMPT, GET_CYCLES_SUCCESS, GET_CYCLES_FAIL, SELECT_CYCLE,
  REFRESH_CYCLE_ATTEMPT, REFRESH_CYCLE_SUCCESS, REFRESH_CYCLE_FAIL,
  GET_SUMMARY_ATTEMPT, GET_SUMMARY_SUCCESS, GET_SUMMARY_FAIL,
  GET_COMMENTS_ATTEMPT, GET_COMMENTS_SUCCESS, GET_COMMENTS_FAIL,
  UPDATE_COMMENT_ATTEMPT, UPDATE_COMMENT_SUCCESS, UPDATE_COMMENT_FAIL,
  GET_DILIGENCES_ATTEMPT, GET_DILIGENCES_SUCCESS, GET_DILIGENCES_FAIL, TOGGLE_DILIGENCE,
  UPDATE_DILIGENCE_ATTEMPT, UPDATE_DILIGENCE_SUCCESS, UPDATE_DILIGENCE_FAIL,
  BULK_UPDATE_DILIGENCE_SUCCESS, ADD_DILIGENCE_PJ,
  SET_DILIGENCE_COMMENTS, SET_DILIGENCE_COMMENTS_ATTEMPT, SET_DILIGENCE_COMMENTS_FAIL,
  ADD_DILIGENCE_COMMENT, ADD_DILIGENCE_COMMENT_ATTEMPT, ADD_DILIGENCE_COMMENT_FAIL,
  UPDATE_DILIGENCE_COMMENT, UPDATE_DILIGENCE_COMMENT_ATTEMPT, UPDATE_DILIGENCE_COMMENT_FAIL,
  GET_ANALYTIC_REVIEW_ATTEMPT, GET_ANALYTIC_REVIEW_SUCCESS, GET_ANALYTIC_REVIEW_FAIL,
  UPDATE_ANALYTIC_REVIEW_ATTEMPT, UPDATE_ANALYTIC_REVIEW_SUCCESS, UPDATE_ANALYTIC_REVIEW_FAIL,
  SET_REDIRECT_LINK, REMOVE_REDIRECT_LINK, GET_DILLIGENCE_ID_SUCCESS,
  GET_PLAQUETTE_MODELS_ATTEMPT, GET_PLAQUETTE_MODELS_SUCCESS, GET_PLAQUETTE_MODELS_FAIL,
  GET_PLAQUETTE_TREE_ATTEMPT, GET_PLAQUETTE_TREE_SUCCESS, GET_PLAQUETTE_TREE_FAIL,
  SET_PLAQUETTE_TREE_DIALOG, ADD_SELECTED_DILIGENCE, REMOVE_SELECTED_DILIGENCE,
  TOOGLE_PERMANENT_DIALOG, TOOGLE_TIMELY_DIALOG
} from './constants';

// Initial state
const initial_reviews = {
  loading: false,
  error: false,

  data: [],
  selectedId: null
};
const initial_sections = {
  loading: false,
  error: false,

  data: []
};
const initial_cycles = {
  loading: false,
  error: false,

  data: [],
  selectedId: null
};
const initial_summary = {
  loading: false,
  updating: false,
  error: false,

  data: {}
};
const initial_comments = {
  loading: false,
  updating: false,
  error: false,

  data: {}
};
const initial_diligences = {
  loading: false,
  updating: false,
  error: false,

  data: [],
  expanded: {}
};
const initial_analyticReview = {
  loading: false,
  updating: false,
  error: false,

  data: {}
};

const initial_state = {
  category: 'DA',
  subCategory: 2,
  pages: {},
  openTimely: false,
  openPermanent: false,
  reviews: initial_reviews,
  sections: initial_sections,
  cycles: initial_cycles,
  summary: initial_summary,
  comments: initial_comments,
  diligences: initial_diligences,
  selectedDiligence: [],
  analyticReview: initial_analyticReview,
  plaquette_models: [],
  isPlaquetteTreeDialogOpen: false
};

// Reducers
const diligenceReducer = (state = initial_diligences, action) => {
  switch (action.type) {
  case GET_DILIGENCES_ATTEMPT:
    return {
      ...initial_diligences,
      loading: true
    };
  case GET_DILIGENCES_SUCCESS:
    return {
      ...state,
      loading: false,
      data: action.diligences.map(formatDiligence)
    };
  case GET_DILIGENCES_FAIL:
    return {
      ...state,
      loading: false,
      error: true
    };

  case TOGGLE_DILIGENCE:
    return {
      ...state,
      expanded: {
        ...state.expanded,
        [action.diligenceId]: !state.expanded[action.diligenceId]
      }
    };

  case UPDATE_DILIGENCE_ATTEMPT:
    return {
      ...state,
      updating: true,
      error: false
    };
  case UPDATE_DILIGENCE_SUCCESS:
    return {
      ...state,
      updating: false,
      data: state.data.map(
        updateDiligences(
          action.diligence_id,
          action.validation_type,
          action.validation_state
        )
      )
    };
  case BULK_UPDATE_DILIGENCE_SUCCESS:
    return {
      ...state,
      updating: false,
      data: state.data.map(bulkUpdateDiligences(action.updates))
    };
  case UPDATE_DILIGENCE_FAIL:
    return {
      ...state,
      updating: false,
      error: true
    };
  case SET_DILIGENCE_COMMENTS_ATTEMPT:
    return { ...state };
  case SET_DILIGENCE_COMMENTS_FAIL:
    return { ...state };

  case SET_DILIGENCE_COMMENTS:
    return {
      ...state,
      data: state.data.map(diligence => ({
        ...diligence,
        children: diligence.children.map((child) => {
          if (child.diligence_id === action.diligence_id) {
            return {
              ...child,
              comments: action.comments
            };
          }

          return child;
        })
      }))
    };

  case ADD_DILIGENCE_COMMENT_ATTEMPT:
    return { ...state };
  case ADD_DILIGENCE_COMMENT_FAIL:
    return { ...state };

  case ADD_DILIGENCE_COMMENT:
    return {
      ...state,
      data: state.data.map(diligence => ({
        ...diligence,
        children: diligence.children.map((child) => {
          if (child.diligence_id === action.diligence_id) {
            return {
              ...child,
              comments: action.comment
            };
          }

          return child;
        })
      }))
    };

  case UPDATE_DILIGENCE_COMMENT:
    return {
      ...state,
      data: state.data.map(diligence => ({
        ...diligence,
        children: diligence.children.map((child) => {
          if (child.diligence_id === action.diligence_id) {
            return {
              ...child,
              comments: action.comment
            };
          }

          return child;
        })
      }))
    };

  case UPDATE_DILIGENCE_COMMENT_ATTEMPT:
    return { ...state };
  case UPDATE_DILIGENCE_COMMENT_FAIL:
    return { ...state };

  case ADD_DILIGENCE_PJ:
    return {
      ...state,
      data: state.data.map(diligence => ({
        ...diligence,
        children: diligence.children.map((child) => {
          if (child.diligence_id === action.diligence_id) {
            return {
              ...child,
              pj_list: [...child.pj_list, ...action.files]
            };
          }

          return child;
        })
      }))
    };

  default:
    return state;
  }
};

const dadpReducer = (state = initial_state, action) => {
  switch (action.type) {
  case SET_CATEGORY:
    return {
      ...state,
      category: action.category
    };
  case SET_SUB_CATEGORY:
    return {
      ...state,
      subCategory: action.subCategory
    };

  case GET_REVIEWS_ATTEMPT:
    return {
      ...state,
      reviews: {
        ...initial_reviews,
        loading: true
      }
    };
  case GET_REVIEWS_SUCCESS:
    return {
      ...state,
      reviews: {
        ...state.reviews,
        loading: false,
        data: action.reviews.map(formatReview)
      }
    };
  case GET_REVIEWS_FAIL:
    return {
      ...state,
      reviews: {
        ...state.reviews,
        loading: false,
        error: true
      }
    };
  case SELECT_REVIEW:
    return {
      ...state,
      reviews: {
        ...state.reviews,
        selectedId: action.reviewId
      }
    };

  case GET_SECTION_ATTEMPT:
    return {
      ...state,
      sections: {
        ...initial_sections,
        loading: true
      }
    };
  case GET_SECTION_SUCCESS:
    return {
      ...state,
      sections: {
        ...state.sections,
        loading: false,
        error: false,
        data: action.sections
      }
    };
  case GET_SECTION_FAIL:
    return {
      ...state,
      sections: {
        ...state.sections,
        loading: false,
        error: true
      }
    };

  case GET_CYCLES_ATTEMPT:
    return {
      ...state,
      cycles: {
        ...initial_cycles,
        loading: true
      }
    };
  case GET_CYCLES_SUCCESS:
    return {
      ...state,
      cycles: {
        ...state.cycles,
        loading: false,
        error: false,
        data: action.cycles
      }
    };
  case GET_CYCLES_FAIL:
    return {
      ...state,
      cycles: {
        ...state.cycles,
        loading: false,
        error: true
      }
    };
  case SELECT_CYCLE:
    return {
      ...state,
      cycles: {
        ...state.cycles,
        selectedId: action.cycleId
      }
    };

  case REFRESH_CYCLE_ATTEMPT:
  case REFRESH_CYCLE_SUCCESS:
    return {
      ...state,
      cycles: {
        ...state.cycles,
        data: state.cycles.data.map((cycle) => {
          if (cycle.cycle_da_dp_id === action.cycle_id) {
            return { ...cycle, [action.vtype]: action.vstate };
          }

          return cycle;
        })
      }
    };
  case REFRESH_CYCLE_FAIL:
    return {
      ...state,
      cycles: {
        ...state.cycles,
        error: true
      }
    };

  case GET_SUMMARY_ATTEMPT:
    return {
      ...state,
      summary: {
        ...initial_summary,
        loading: true
      }
    };
  case GET_SUMMARY_SUCCESS:
    return {
      ...state,
      summary: {
        ...state.summary,
        loading: false,
        error: false,
        data: action.summary
      }
    };
  case GET_SUMMARY_FAIL:
    return {
      ...state,
      summary: {
        ...state.summary,
        loading: false,
        error: true
      }
    };

  case GET_COMMENTS_ATTEMPT:
    return {
      ...state,
      comments: {
        ...initial_comments,
        loading: true
      }
    };
  case GET_COMMENTS_SUCCESS:
    return {
      ...state,
      comments: {
        ...state.comments,
        loading: false,
        error: false,
        data: commentsWrap(action.comments)
      }
    };
  case GET_COMMENTS_FAIL:
    return {
      ...state,
      comments: {
        ...state.comments,
        loading: false,
        error: true
      }
    };

  case UPDATE_COMMENT_ATTEMPT:
    return {
      ...state,
      summary: {
        ...state.summary,
        updating: true
      },
      comments: {
        ...state.comments,
        updating: true
      }
    };
  case UPDATE_COMMENT_SUCCESS:
    return {
      ...state,
      summary: {
        ...state.summary,
        updating: false,
        error: false,
        data: updateSummary(
          action.section_code,
          action.cycle_id,
          action.comments,
          state.summary.data
        )
      },
      comments: {
        ...state.comments,
        updating: false,
        error: false,
        data: commentsWrap(action.comments)
      }
    };
  case UPDATE_COMMENT_FAIL:
    return {
      ...state,
      summary: {
        ...state.summary,
        updating: false,
        error: true
      },
      comments: {
        ...state.comments,
        updating: false,
        error: true
      }
    };

  case GET_DILIGENCES_ATTEMPT:
  case GET_DILIGENCES_SUCCESS:
  case GET_DILIGENCES_FAIL:
  case TOGGLE_DILIGENCE:
  case UPDATE_DILIGENCE_ATTEMPT:
  case UPDATE_DILIGENCE_SUCCESS:
  case BULK_UPDATE_DILIGENCE_SUCCESS:
  case UPDATE_DILIGENCE_FAIL:
  case SET_DILIGENCE_COMMENTS:
  case ADD_DILIGENCE_COMMENT:
  case UPDATE_DILIGENCE_COMMENT:
  case ADD_DILIGENCE_PJ:
    return {
      ...state,
      diligences: diligenceReducer(state.diligences, action)
    };

  case GET_ANALYTIC_REVIEW_ATTEMPT:
    return {
      ...state,
      analyticReview: {
        ...initial_analyticReview,
        loading: true
      }
    };
  case GET_ANALYTIC_REVIEW_SUCCESS:
    return {
      ...state,
      analyticReview: {
        ...state.analyticReview,
        loading: false,
        data: action.analyticReview
      }
    };
  case GET_ANALYTIC_REVIEW_FAIL:
    return {
      ...state,
      analyticReview: {
        ...state.analyticReview,
        loading: false,
        error: true
      }
    };

  case UPDATE_ANALYTIC_REVIEW_ATTEMPT:
    return {
      ...state,
      analyticReview: {
        ...state.analyticReview,
        updating: true,
        error: false
      }
    };
  case UPDATE_ANALYTIC_REVIEW_SUCCESS:
    return {
      ...state,
      analyticReview: {
        ...state.analyticReview,
        updating: false,

        data: {
          balance_sheet: {
            ...state.analyticReview.data.balance_sheet,
            data: state.analyticReview.data.balance_sheet.data.map(
              updateAccount(
                action.account_id,
                action.validation_type,
                action.validation_state
              )
            )
          },
          accounts_results: {
            ...state.analyticReview.data.accounts_results,
            data: state.analyticReview.data.accounts_results.data.map(
              updateAccount(
                action.account_id,
                action.validation_type,
                action.validation_state
              )
            )
          }
        }
      }
    };
  case UPDATE_ANALYTIC_REVIEW_FAIL:
    return {
      ...state,
      analyticReview: {
        ...state.analyticReview,
        updating: false,
        error: true
      }
    };
  case SET_REDIRECT_LINK:
    return {
      ...state,
      redirectedLink: action.data
    };
  case TOOGLE_TIMELY_DIALOG:
    return {
      ...state,
      openTimely: !state.openTimely,
      timelyDialog: action.data
    };
  case TOOGLE_PERMANENT_DIALOG:
    return {
      ...state,
      openPermanent: !state.openPermanent,
      permanentDialog: action.data
    };

  case REMOVE_REDIRECT_LINK:
    return {
      ...state,
      redirectedLink: ''
    };
  case GET_DILLIGENCE_ID_SUCCESS:
    return {
      ...state,
      pages: {
        ...state.pages,
        [action.page]: action.route
      }
    };
  case GET_PLAQUETTE_MODELS_ATTEMPT:
    return {
      ...state
    };
  case GET_PLAQUETTE_MODELS_SUCCESS:
    return {
      ...state,
      plaquette_models: action.data
    };
  case GET_PLAQUETTE_MODELS_FAIL:
    return {
      ...state
    };
  case GET_PLAQUETTE_TREE_ATTEMPT:
    return {
      ...state
    };
  case GET_PLAQUETTE_TREE_SUCCESS:
    return {
      ...state,
      treeView: action.data
    };
  case GET_PLAQUETTE_TREE_FAIL:
    return {
      ...state
    };
  case SET_PLAQUETTE_TREE_DIALOG:
    return {
      ...state,
      isPlaquetteTreeDialogOpen: action.isOpen
    };
  case ADD_SELECTED_DILIGENCE:
    return {
      ...state,
      selectedDiligence: [...state.selectedDiligence, action.data]
    };
  case REMOVE_SELECTED_DILIGENCE:
    return {
      ...state,
      selectedDiligence: state.selectedDiligence.filter(item => item !== action.data)
    };
  default:
    return state;
  }
};

export default dadpReducer;
