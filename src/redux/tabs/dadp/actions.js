import {
  SET_CATEGORY,
  SET_SUB_CATEGORY,
  GET_REVIEWS_ATTEMPT,
  GET_REVIEWS_SUCCESS,
  GET_REVIEWS_FAIL,
  SELECT_REVIEW,
  GET_SECTION_ATTEMPT,
  GET_SECTION_SUCCESS,
  GET_SECTION_FAIL,
  GET_CYCLES_ATTEMPT,
  GET_CYCLES_SUCCESS,
  GET_CYCLES_FAIL,
  SELECT_CYCLE,
  REFRESH_CYCLE_ATTEMPT,
  REFRESH_CYCLE_SUCCESS,
  REFRESH_CYCLE_FAIL,
  GET_SUMMARY_ATTEMPT,
  GET_SUMMARY_SUCCESS,
  GET_SUMMARY_FAIL,
  GET_COMMENTS_ATTEMPT,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAIL,
  UPDATE_COMMENT_ATTEMPT,
  UPDATE_COMMENT_SUCCESS,
  UPDATE_COMMENT_FAIL,
  GET_DILIGENCES_ATTEMPT,
  GET_DILIGENCES_SUCCESS,
  GET_DILIGENCES_FAIL,
  TOGGLE_DILIGENCE,
  UPDATE_DILIGENCE_ATTEMPT,
  UPDATE_DILIGENCE_SUCCESS,
  UPDATE_DILIGENCE_FAIL,
  BULK_UPDATE_DILIGENCE_SUCCESS,
  ADD_DILIGENCE_PJ,
  SET_DILIGENCE_COMMENTS,
  SET_DILIGENCE_COMMENTS_ATTEMPT,
  SET_DILIGENCE_COMMENTS_FAIL,
  ADD_DILIGENCE_COMMENT,
  ADD_DILIGENCE_COMMENT_ATTEMPT,
  ADD_DILIGENCE_COMMENT_FAIL,
  UPDATE_DILIGENCE_COMMENT,
  UPDATE_DILIGENCE_COMMENT_ATTEMPT,
  UPDATE_DILIGENCE_COMMENT_FAIL,
  GET_ANALYTIC_REVIEW_ATTEMPT,
  GET_ANALYTIC_REVIEW_SUCCESS,
  GET_ANALYTIC_REVIEW_FAIL,
  UPDATE_ANALYTIC_REVIEW_ATTEMPT,
  UPDATE_ANALYTIC_REVIEW_SUCCESS,
  UPDATE_ANALYTIC_REVIEW_FAIL,
  SET_REDIRECT_LINK,
  REMOVE_REDIRECT_LINK,
  GET_DILLIGENCE_ID_ATTEMPT,
  GET_DILLIGENCE_ID_SUCCESS,
  GET_DILLIGENCE_ID_FAIL,
  GET_PLAQUETTE_MODELS_ATTEMPT,
  GET_PLAQUETTE_MODELS_SUCCESS,
  GET_PLAQUETTE_MODELS_FAIL,
  GET_PLAQUETTE_COMPLEMENT_ATTEMPT,
  GET_PLAQUETTE_COMPLEMENT_SUCCESS,
  GET_PLAQUETTE_COMPLEMENT_FAIL,
  GET_PLAQUETTE_TREE_ATTEMPT,
  GET_PLAQUETTE_TREE_SUCCESS,
  GET_PLAQUETTE_TREE_FAIL,
  SET_PLAQUETTE_TREE_DIALOG,
  POST_PLAQUETTE_ATTEMPT,
  POST_PLAQUETTE_SUCCESS,
  POST_PLAQUETTE_FAIL,
  TOOGLE_TIMELY_DIALOG,
  TOOGLE_PERMANENT_DIALOG,
  ADD_SELECTED_DILIGENCE,
  REMOVE_SELECTED_DILIGENCE
} from './constants';

// Actions
export const setCategory = category => ({ type: SET_CATEGORY, category });
export const setSubCategory = subCategory => ({
  type: SET_SUB_CATEGORY,
  subCategory
});

export const getReviewsAttempt = () => ({ type: GET_REVIEWS_ATTEMPT });
export const getReviewsSuccess = reviews => ({
  type: GET_REVIEWS_SUCCESS,
  reviews
});
export const getReviewsFail = () => ({ type: GET_REVIEWS_FAIL });
export const selectReview = reviewId => ({ type: SELECT_REVIEW, reviewId });

export const getSectionAttempt = () => ({ type: GET_SECTION_ATTEMPT });
export const getSectionSuccess = sections => ({
  type: GET_SECTION_SUCCESS,
  sections
});
export const getSectionFail = () => ({ type: GET_SECTION_FAIL });

export const getCyclesAttempt = () => ({ type: GET_CYCLES_ATTEMPT });
export const getCyclesSuccess = cycles => ({
  type: GET_CYCLES_SUCCESS,
  cycles
});
export const getCyclesFail = () => ({ type: GET_CYCLES_FAIL });
export const selectCycle = cycleId => ({ type: SELECT_CYCLE, cycleId });

export const refreshCycleAttempt = (cycle_id, vtype) => ({
  type: REFRESH_CYCLE_ATTEMPT,
  cycle_id,
  vtype,
  vstate: 'loading'
});
export const refreshCycleSuccess = (cycle_id, vtype, vstate) => ({
  type: REFRESH_CYCLE_SUCCESS,
  cycle_id,
  vtype,
  vstate
});
export const refreshCycleFail = () => ({ type: REFRESH_CYCLE_FAIL });

export const getSummaryAttempt = () => ({ type: GET_SUMMARY_ATTEMPT });
export const getSummarySuccess = summary => ({
  type: GET_SUMMARY_SUCCESS,
  summary
});
export const getSummaryFail = () => ({ type: GET_SUMMARY_FAIL });

export const getCommentsAttempt = () => ({ type: GET_COMMENTS_ATTEMPT });
export const getCommentsSuccess = comments => ({
  type: GET_COMMENTS_SUCCESS,
  comments
});
export const getCommentsFail = () => ({ type: GET_COMMENTS_FAIL });

export const updateCommentAttempt = () => ({ type: UPDATE_COMMENT_ATTEMPT });
export const updateCommentSuccess = (section_code, cycle_id, comments) => ({
  type: UPDATE_COMMENT_SUCCESS,
  section_code,
  cycle_id,
  comments
});
export const updateCommentFail = () => ({ type: UPDATE_COMMENT_FAIL });

export const getDiligencesAttempt = () => ({ type: GET_DILIGENCES_ATTEMPT });
export const getDiligencesSuccess = diligences => ({
  type: GET_DILIGENCES_SUCCESS,
  diligences
});
export const getDiligencesFail = () => ({ type: GET_DILIGENCES_FAIL });
export const toggleDiligence = diligenceId => ({
  type: TOGGLE_DILIGENCE,
  diligenceId
});

export const updateDiligenceAttempt = () => ({
  type: UPDATE_DILIGENCE_ATTEMPT
});
export const updateDiligenceSuccess = (
  diligence_id,
  validation_type,
  validation_state
) => ({
  type: UPDATE_DILIGENCE_SUCCESS,
  diligence_id,
  validation_type,
  validation_state
});
export const bulkUpdateDiligenceSuccess = updates => ({
  type: BULK_UPDATE_DILIGENCE_SUCCESS,
  updates
});
export const updateDiligenceFail = () => ({ type: UPDATE_DILIGENCE_FAIL });

export const setDiligenceComments = (diligence_id, comments) => ({
  type: SET_DILIGENCE_COMMENTS,
  diligence_id,
  comments
});
export const setDiligenceCommentsAttempt = () => ({
  type: SET_DILIGENCE_COMMENTS_ATTEMPT
});
export const setDiligenceCommentsFail = () => ({
  type: SET_DILIGENCE_COMMENTS_FAIL
});

export const addDiligenceComment = (diligence_id, comment) => ({
  type: ADD_DILIGENCE_COMMENT,
  diligence_id,
  comment
});
export const addDiligenceCommentAttempt = () => ({
  type: ADD_DILIGENCE_COMMENT_ATTEMPT
});
export const addDiligenceCommentFail = () => ({
  type: ADD_DILIGENCE_COMMENT_FAIL
});

export const updateDiligenceComment = (diligence_id, comment_id, comment) => ({
  type: UPDATE_DILIGENCE_COMMENT,
  diligence_id,
  comment_id,
  comment
});
export const updateDiligenceCommentAttempt = () => ({
  type: UPDATE_DILIGENCE_COMMENT_ATTEMPT
});
export const updateDiligenceCommentFail = () => ({
  type: UPDATE_DILIGENCE_COMMENT_FAIL
});

export const addDiligencePJ = (diligence_id, files) => ({
  type: ADD_DILIGENCE_PJ,
  diligence_id,
  files
});

export const getAnalyticReviewAttempt = () => ({
  type: GET_ANALYTIC_REVIEW_ATTEMPT
});
export const getAnalyticReviewSuccess = analyticReview => ({
  type: GET_ANALYTIC_REVIEW_SUCCESS,
  analyticReview
});
export const getAnalyticReviewFail = () => ({ type: GET_ANALYTIC_REVIEW_FAIL });

export const updateAnalyticReviewAttempt = () => ({
  type: UPDATE_ANALYTIC_REVIEW_ATTEMPT
});
export const updateAnalyticReviewSuccess = (
  account_id,
  validation_type,
  validation_state
) => ({
  type: UPDATE_ANALYTIC_REVIEW_SUCCESS,
  account_id,
  validation_type,
  validation_state
});
export const updateAnalyticReviewFail = () => ({
  type: UPDATE_ANALYTIC_REVIEW_FAIL
});

export const setRedirectLink = data => ({ type: SET_REDIRECT_LINK, data });
export const toggleTimelyDialog = data => ({
  type: TOOGLE_TIMELY_DIALOG,
  data
});
export const togglePermanentDialog = data => ({
  type: TOOGLE_PERMANENT_DIALOG,
  data
});

export const removeRedirectLink = () => ({ type: REMOVE_REDIRECT_LINK });

export const getDilligenceIdAttempt = () => ({
  type: GET_DILLIGENCE_ID_ATTEMPT
});
export const getDilligenceIdSuccess = (id_diligence, route, page) => ({
  type: GET_DILLIGENCE_ID_SUCCESS,
  id_diligence,
  route,
  page
});
export const getDilligenceIdFail = error => ({
  type: GET_DILLIGENCE_ID_FAIL,
  error
});

export const getPlaquetteModelsAttempt = () => ({
  type: GET_PLAQUETTE_MODELS_ATTEMPT
});
export const getPlaquetteModelsSuccess = data => ({
  type: GET_PLAQUETTE_MODELS_SUCCESS,
  data
});
export const getPlaquetteModelsFail = () => ({
  type: GET_PLAQUETTE_MODELS_FAIL
});

export const getPlaquetteComplementAttempt = () => ({
  type: GET_PLAQUETTE_COMPLEMENT_ATTEMPT
});
export const getPlaquetteComplementSuccess = data => ({
  type: GET_PLAQUETTE_COMPLEMENT_SUCCESS,
  data
});
export const getPlaquetteComplementFail = () => ({
  type: GET_PLAQUETTE_COMPLEMENT_FAIL
});

export const getPlaquetteTreeAttempt = () => ({
  type: GET_PLAQUETTE_TREE_ATTEMPT
});
export const getPlaquetteTreeSuccess = data => ({
  type: GET_PLAQUETTE_TREE_SUCCESS,
  data
});
export const getPlaquetteTreeFail = () => ({ type: GET_PLAQUETTE_TREE_FAIL });

export const setPlaquetteTreeDialog = isOpen => ({
  type: SET_PLAQUETTE_TREE_DIALOG,
  isOpen
});

export const postPlaquetteAttempt = () => ({ type: POST_PLAQUETTE_ATTEMPT });
export const postPlaquetteSuccess = () => ({ type: POST_PLAQUETTE_SUCCESS });
export const postPlaquetteFail = () => ({ type: POST_PLAQUETTE_FAIL });

export const addSelectedDiligence = data => ({ type: ADD_SELECTED_DILIGENCE, data });
export const removeSelectedDiligence = data => ({ type: REMOVE_SELECTED_DILIGENCE, data });
