import moment from 'moment';
import _ from 'lodash';

// Fake back ;)
// Utils
const sleep = ms => new Promise(r => setTimeout(r, ms));
const randomize = (a, d) => Math.round(a + d * (1 - ((2 * Math.random() - 1) ** 2)));

const fakeAPI = (name, avg, delta) => api => async (...args) => {
  const wait = randomize(avg, delta);
  console.debug(`Call fake api ${name} : ${wait}ms`);
  await sleep(wait);

  return api(...args);
};

// Data
let ID = 0;
const DB = {
  3277: [
    {
      comment_id: ++ID, // eslint-disable-line no-plusplus
      name: 'Super-Testeur !',
      initials: 'ST',
      date: moment().subtract(4, 'day').format(),
      body: '<p>Cool</p>'
    }
  ]
};

export const getFakeComments = fakeAPI('getFakeComments', 500, 200)(
  id => _.cloneDeep(DB[id] || [])
);

export const addFakeComment = fakeAPI('addFakeComment', 300, 200)(
  (id, comment) => {
    const data = {
      ...comment,
      comment_id: ++ID, // eslint-disable-line no-plusplus
      name: 'Super-Testeur !',
      initials: 'ST',
      date: moment().format()
    };

    if (DB[id]) {
      DB[id].push(comment);
    } else {
      DB[id] = [comment];
    }

    return _.cloneDeep(data);
  }
);

export const editFakeComment = fakeAPI('editFakeComment', 300, 200)(
  (id, comment) => {
    const i = DB[id].findIndex(c => c.comment_id === comment.comment_id);
    const prev = DB[id][i];

    const data = {
      ...comment,
      comment_id: ++ID, // eslint-disable-line no-plusplus
      name: 'Super-Testeur !',
      initials: 'ST',
      date: moment().format(),
      history: [...(prev.history || []), _.omit(prev, 'history')]
    };

    DB[id][i] = data;

    return _.cloneDeep(data);
  }
);
