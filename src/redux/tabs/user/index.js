import _ from 'lodash';

import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';

import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';

import { setCurrentForm, closeCurrentForm } from 'redux/tabs/form/actions';
import actions from './actions';

export const createUser = payload => async (dispatch) => {
  const {
    newUserProfil,
    newUserFunction
  } = payload;

  const id_profil = newUserProfil.id;
  const groupslst = newUserFunction
    ? newUserFunction.map(func => ({ id_user_groups: func.id }))
    : [];

  const param = {
    ...payload,
    id_profil,
    groupslst
  };

  try {
    await dispatch(actions.createUserAttempt());
    const response = await windev.makeApiCall('user', 'post', {}, param);
    const { data } = response;
    await dispatch(actions.createUserSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.createUserFail());
    await dispatch(handleError(err));
    throw _.get(err, 'response', {});
  }
};

export const putUser = payload => async (dispatch, getState) => {
  try {
    const state = getState();
    const user = _.get(getCurrentTabState(state), 'user.selectedUser');
    const mail_id = _.get(user, 'mail.id', null);
    const portable_id = _.get(user, 'tel_portable.id', null);
    const fix_id = _.get(user, 'tel_fix.id', null);

    const {
      user_id,
      civility_code,
      civility_id,
      firstname,
      name,
      maiden_name,
      access_list,
      delete_list,
      mail,
      tel_fix,
      tel_portable,
      newUserProfil,
      newUserFunction
    } = payload;

    const id_profil = newUserProfil.id;
    const groupslst = newUserFunction
      ? newUserFunction.map(func => ({ id_user_groups: func.id }))
      : [];

    const info = {
      user_id,
      civility_code,
      civility_id,
      firstname,
      name,
      maiden_name,
      access_list,
      delete_list,
      mail: mail && { id: mail_id, coordonnee: mail },
      tel_fix: tel_fix && { id: fix_id, coordonnee: tel_fix },
      tel_portable: tel_portable && { id: portable_id, coordonnee: tel_portable },
      id_profil,
      groupslst
    };
    await dispatch(actions.putUserAttempt());
    const { data } = await windev.makeApiCall('user', 'put', {}, info);
    await dispatch(actions.putUserSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.putUserFail());
    await dispatch(handleError(err));
    throw _.get(err, 'response', {});
  }
};

export const getUserList = (payload = {}) => async (dispatch, getState) => {
  const state = getState();
  const tabstate = getCurrentTabState(state);

  const {
    sort: sortOpts,
    q: query
  } = payload;

  const sort = sortOpts !== undefined
    ? sortOpts
    : _.get(tabstate, 'user.sort');

  const limit = 1000000;

  const page = 0;

  const offset = 0;

  const mail = _.get(payload, 'mail');

  try {
    await dispatch(actions.getUserListAttempt());

    const params = {
      limit,
      sort,
      offset,
      q: query,
      mail
    };

    const { data: usersData } = await windev.makeApiCall('/users_v2', 'get', params);
    await dispatch(actions.getUserListSuccess({
      usersData,
      limit,
      page,
      sort: sort || {}
    }));
    return usersData;
  } catch (err) {
    await dispatch(actions.getUserListFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getUser = (user_id, type = 'select') => async (dispatch, getState) => {
  const state = getState();
  const formName = tabFormName('newUser', state.navigation.id);

  try {
    await dispatch(actions.getUserAttempt());
    const { data } = await windev.makeApiCall('users_v2', 'get', { user_id, limit: 1 });
    let user = data.user_array[0];
    const access_list = _.get(user, 'access_list', []);
    let society_access_list = [];
    let wallet_access_list = [];

    let userForm = {
      user_id: _.get(user, 'user_id'),
      access_list
    };

    if (type === 'select') {
      userForm = { ...user };
      userForm.civility_code = _.get(user, 'civility', 'Monsieur') === 'Monsieur' ? 'Mr' : 'Mrs';
      userForm.mail = _.get(user, 'mail.coordonnee');
      userForm.tel_fix = _.get(user, 'tel_fix.coordonnee');
      userForm.tel_portable = _.get(user, 'tel_portable.coordonnee');
      const fields = {};

      if (access_list) {
        for (let i = 0; i < access_list.length; i += 1) {
          const access = access_list[i];
          let typeFieldName;
          let profilFieldName;

          if (access_list[i].society_id) {
            society_access_list = [...society_access_list, access_list[i]];
            typeFieldName = 'accessTypeSociety';
            profilFieldName = 'accessProfilSociety';
          } else if (access_list[i].wallet_id) {
            wallet_access_list = [...wallet_access_list, access_list[i]];
            typeFieldName = 'accessTypeWallet';
            profilFieldName = 'accessProfilWallet';
          }

          Object.assign(fields, {
            [`${typeFieldName}-${access.acces_id}`]: {
              id: access.id_type_profil,
              value: access.libelle_type_profil,
              label: access.libelle_type_profil
            },
            [`${profilFieldName}-${access.acces_id}`]: {
              id: access.profil_id,
              value: access.profil_name,
              label: access.profil_name
            }
          });
        }
      }

      user = {
        ...user, society_access_list, wallet_access_list
      };

      userForm = {
        ...userForm, ...fields, society_access_list, wallet_access_list
      };
    }
    await dispatch(closeCurrentForm(async () => {
      await dispatch(autoFillFormValues(formName, userForm));
      await dispatch(actions.selectUser(user));

      dispatch(setCurrentForm(formName, userForm));
    }));

    return user;
  } catch (err) {
    await dispatch(actions.getUserFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getUserByMail = mail => async (dispatch) => {
  try {
    await dispatch(actions.getUserByMailAttempt());
    const { data } = await windev.makeApiCall('users_v2', 'get', { mail, limit: 1 });
    return data;
  } catch (err) {
    await dispatch(actions.getUserByMailFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getUserType = () => async (dispatch) => {
  try {
    await dispatch(actions.getUserTypeAttempt());
    const { data } = await windev.makeApiCall('profils/type', 'get', {});
    await dispatch(actions.getUserTypeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getUserTypeFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getUserProfil = profil_type_id => async (dispatch) => {
  try {
    await dispatch(actions.getUserProfilAttempt());
    const { data } = await windev.makeApiCall('profils', 'get', { profil_type_id });
    await dispatch(actions.getUserProfilSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getUserProfilFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteUser = user_id => async (dispatch) => {
  try {
    await dispatch(actions.deleteUserAttempt());
    const { data } = await windev.makeApiCall('user', 'delete', { user_id });
    await dispatch(actions.deleteUserSuccess());
    await dispatch(getUserList());
    return data;
  } catch (err) {
    await dispatch(actions.deleteUserFail());
    await dispatch(handleError(err));
    return err;
  }
};
