import {
  SELECT_TAB,

  CREATE_USER_ATTEMPT,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAIL,

  CLOSE_DIALOG,

  SELECT_USER,

  PUT_USER_ATTEMPT,
  PUT_USER_SUCCESS,
  PUT_USER_FAIL,

  GET_USER_LIST_ATTEMPT,
  GET_USER_LIST_SUCCESS,
  GET_USER_LIST_FAIL,

  GET_USER_PROFIL_ATTEMPT,
  GET_USER_PROFIL_SUCCESS,
  GET_USER_PROFIL_FAIL,

  GET_USER_TYPE_ATTEMPT,
  GET_USER_TYPE_SUCCESS,
  GET_USER_TYPE_FAIL,

  RESET_SELECTED_USER
} from './constants';


const initialState = {
  isConfirmationDialogOpen: false,
  selectedTab: 0,
  usersData: {},
  selectedUser: undefined,
  physicalPersonsData: {},
  sort: { column: 'name', direction: 'asc' },
  limit: 10,
  page: 0,
  isLoading: false,
  isError: false,
  userProfil: [],
  userType: []
};

const user = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case SELECT_TAB:
      const { tabIndex } = action;
      return {
        ...state,
        selectedTab: tabIndex
      };
    case CREATE_USER_ATTEMPT:
      return {
        ...state
      };
    case CREATE_USER_SUCCESS:
      return {
        ...state,
        isConfirmationDialogOpen: true
      };
    case CREATE_USER_FAIL:
      return {
        ...state
      };
    case CLOSE_DIALOG:
      return {
        ...state,
        isConfirmationDialogOpen: false
      };
    case SELECT_USER:
      return {
        ...state,
        selectedUser: action.user_data
      };
    case PUT_USER_ATTEMPT:
      return {
        ...state
      };
    case PUT_USER_SUCCESS:
      return {
        ...state
      };
    case PUT_USER_FAIL:
      return {
        ...state
      };
    case GET_USER_LIST_ATTEMPT:
      return {
        ...state,
        usersData: {},
        isLoading: true,
        isError: false
      };
    case GET_USER_LIST_SUCCESS:
      const {
        page,
        limit,
        sort,
        usersData
      } = action.payload;
      return {
        ...state,
        page,
        limit,
        sort,
        usersData,
        isLoading: false,
        isError: false
      };
    case GET_USER_LIST_FAIL:
      return {
        ...state,
        usersData: {},
        isLoading: false,
        isError: true
      };
    case GET_USER_PROFIL_ATTEMPT:
      return {
        ...state,
        userProfil: []
      };
    case GET_USER_PROFIL_SUCCESS:
      return {
        ...state,
        userProfil: action.payload
      };
    case GET_USER_PROFIL_FAIL:
      return {
        ...state,
        userProfil: []
      };
    case GET_USER_TYPE_ATTEMPT:
      return {
        ...state,
        userType: []
      };
    case GET_USER_TYPE_SUCCESS:
      return {
        ...state,
        userType: action.payload
      };
    case GET_USER_TYPE_FAIL:
      return {
        ...state,
        userType: []
      };
    case RESET_SELECTED_USER:
      return {
        ...state,
        selectedUser: undefined
      };
    default: return state;
    }
  }
  return state;
};

export default user;
