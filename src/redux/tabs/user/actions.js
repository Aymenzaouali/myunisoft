import {
  CREATE_USER_ATTEMPT,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAIL,

  SELECT_TAB,

  CLOSE_DIALOG,

  SELECT_USER,

  PUT_USER_ATTEMPT,
  PUT_USER_SUCCESS,
  PUT_USER_FAIL,

  GET_USER_ATTEMPT,
  GET_USER_FAIL,

  GET_USER_LIST_ATTEMPT,
  GET_USER_LIST_SUCCESS,
  GET_USER_LIST_FAIL,

  GET_USER_BY_MAIL_ATTEMPT,
  GET_USER_BY_MAIL_FAIL,

  GET_USER_PROFIL_ATTEMPT,
  GET_USER_PROFIL_SUCCESS,
  GET_USER_PROFIL_FAIL,

  GET_USER_TYPE_ATTEMPT,
  GET_USER_TYPE_SUCCESS,
  GET_USER_TYPE_FAIL,

  RESET_SELECTED_USER,

  DELETE_USER_ATTEMPT,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAIL
} from './constants';

const createUserAttempt = () => ({
  type: CREATE_USER_ATTEMPT
});

const createUserSuccess = societies => ({
  type: CREATE_USER_SUCCESS,
  societies
});

const createUserFail = () => ({
  type: CREATE_USER_FAIL
});

const selectTab = tabIndex => ({
  type: SELECT_TAB,
  tabIndex
});

const closeDialog = () => ({
  type: CLOSE_DIALOG
});

const selectUser = user_data => ({
  type: SELECT_USER,
  user_data
});

const putUserAttempt = () => ({
  type: PUT_USER_ATTEMPT
});

const putUserSuccess = () => ({
  type: PUT_USER_SUCCESS
});

const putUserFail = () => ({
  type: PUT_USER_FAIL
});

const getUserListAttempt = () => ({
  type: GET_USER_LIST_ATTEMPT
});

const getUserListSuccess = payload => ({
  type: GET_USER_LIST_SUCCESS,
  payload
});

const getUserListFail = () => ({
  type: GET_USER_LIST_FAIL
});

const getUserAttempt = () => ({
  type: GET_USER_ATTEMPT
});

const getUserFail = () => ({
  type: GET_USER_FAIL
});

const getUserByMailAttempt = () => ({
  type: GET_USER_BY_MAIL_ATTEMPT
});

const getUserByMailFail = () => ({
  type: GET_USER_BY_MAIL_FAIL
});

const getUserProfilAttempt = () => ({
  type: GET_USER_PROFIL_ATTEMPT
});

const getUserProfilSuccess = payload => ({
  type: GET_USER_PROFIL_SUCCESS,
  payload
});

const getUserProfilFail = () => ({
  type: GET_USER_PROFIL_FAIL
});

const getUserTypeAttempt = () => ({
  type: GET_USER_TYPE_ATTEMPT
});

const getUserTypeSuccess = payload => ({
  type: GET_USER_TYPE_SUCCESS,
  payload
});

const getUserTypeFail = () => ({
  type: GET_USER_TYPE_FAIL
});

const resetSelectedUser = () => ({
  type: RESET_SELECTED_USER
});

const deleteUserAttempt = () => ({ type: DELETE_USER_ATTEMPT });
const deleteUserSuccess = () => ({ type: DELETE_USER_SUCCESS });
const deleteUserFail = () => ({ type: DELETE_USER_FAIL });

export default {
  getUserByMailAttempt,
  getUserByMailFail,

  createUserAttempt,
  createUserSuccess,
  createUserFail,

  selectTab,

  closeDialog,

  selectUser,

  putUserAttempt,
  putUserSuccess,
  putUserFail,

  getUserListAttempt,
  getUserListSuccess,
  getUserListFail,

  getUserAttempt,
  getUserFail,

  getUserProfilAttempt,
  getUserProfilSuccess,
  getUserProfilFail,

  getUserTypeAttempt,
  getUserTypeSuccess,
  getUserTypeFail,

  resetSelectedUser,

  deleteUserAttempt,
  deleteUserSuccess,
  deleteUserFail
};
