import {
  ACTIVATE_SPLIT,
  SWAP_SPLIT, UPDATE_ARGS_SPLIT,
  OPEN_POPUP_SPLIT, CLOSE_POPUP_SPLIT,
  CLOSE_SPLIT, CLOSE_ALL_SPLIT
} from './constants';

// Reducer
const splitReducer = (state = {}, action) => {
  switch (action.type) {
  case ACTIVATE_SPLIT:
    return {
      ...state,

      [action.master_id]: {
        active: action.slave,
        swapped: action.swapped,
        popup: false,
        windowState: {},
        args: action.args
      }
    };

  case SWAP_SPLIT: {
    const split = state[action.master_id];

    return {
      ...state,

      [action.master_id]: {
        ...split,
        swapped: !split.swapped,
        args: action.args
      }
    };
  }

  case UPDATE_ARGS_SPLIT: {
    const split = state[action.master_id];
    if (!split) return state;

    return {
      ...state,

      [action.master_id]: {
        ...split,
        args: {
          ...split.args,
          ...action.args
        }
      }
    };
  }

  case OPEN_POPUP_SPLIT: {
    const split = state[action.master_id];

    return {
      ...state,

      [action.master_id]: {
        ...split,
        popup: true
      }
    };
  }

  case CLOSE_POPUP_SPLIT: {
    const split = state[action.master_id];

    return {
      ...state,

      [action.master_id]: {
        ...split,
        popup: false,
        windowState: {}
      }
    };
  }

  case CLOSE_SPLIT:
    const { [action.master_id]: tab, ...others } = state;
    return { ...others };

  case CLOSE_ALL_SPLIT:
    return {};

  default:
  }

  return state;
};

export default splitReducer;
