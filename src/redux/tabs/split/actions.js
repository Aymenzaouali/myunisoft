import {
  ACTIVATE_SPLIT,
  SWAP_SPLIT, UPDATE_ARGS_SPLIT,
  OPEN_POPUP_SPLIT, CLOSE_POPUP_SPLIT,
  CLOSE_SPLIT, CLOSE_ALL_SPLIT
} from './constants';

// Actions
const activateSplit = (master_id, slave, swapped, args = {}) => ({
  type: ACTIVATE_SPLIT,
  master_id,
  slave,
  swapped,
  args
});

const swapSplit = (master_id, args) => ({ type: SWAP_SPLIT, master_id, args });
const updateArgsSplit = (master_id, args) => ({ type: UPDATE_ARGS_SPLIT, master_id, args });

const openPopupSplit = master_id => ({ type: OPEN_POPUP_SPLIT, master_id });
const closePopupSplit = master_id => ({ type: CLOSE_POPUP_SPLIT, master_id });

const closeSplit = master_id => ({ type: CLOSE_SPLIT, master_id });
const closeAllSplit = () => ({ type: CLOSE_ALL_SPLIT });

export {
  activateSplit,
  swapSplit,
  updateArgsSplit,
  openPopupSplit,
  closePopupSplit,
  closeSplit,
  closeAllSplit
};
