// Constantes
export const SPLIT_INACTIVE = '';

export const ACTIVATE_SPLIT = 'split/ACTIVATE_SPLIT';

export const SWAP_SPLIT = 'split/SWAP_SPLIT';
export const UPDATE_ARGS_SPLIT = 'split/UPDATE_ARGS_SPLIT';

export const OPEN_POPUP_SPLIT = 'split/OPEN_POPUP_SPLIT';
export const CLOSE_POPUP_SPLIT = 'split/CLOSE_POPUP_SPLIT';

export const CLOSE_SPLIT = 'split/CLOSE_SPLIT';
export const CLOSE_ALL_SPLIT = 'split/CLOSE_ALL_SPLIT';
