import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';

import { getCurrentTabState } from 'helpers/tabs';

import { removeManualDoc } from 'redux/tabs/accounting/actions';

import {
  getDocumentsAttempt, getDocumentsSuccess, getDocumentsFail,
  attachDocumentsAttempt, attachDocumentsSuccess, attachDocumentsFail,
  detachDocumentsAttempt, detachDocumentsSuccess, detachDocumentsFail,
  deleteDocumentsAttempt, deleteDocumentsSuccess, deleteDocumentsFail
} from './actions';

// Thunks
export const getDocuments = (page, limit) => async (dispatch, getState) => {
  const state = getState();
  const params = {
    society_id: state.navigation.id,
    offset: page * limit,
    limit
  };

  try {
    await dispatch(getDocumentsAttempt());

    const { data } = await windev.makeApiCall('/document/pending', 'get', params);
    const { rows_number: rows, pages_number: pages, list_manual_document } = data;
    const docs = list_manual_document.map(d => ({
      ...d,
      document_id: d.document_attached_id || d.documents[0].document_id
    }));

    await dispatch(getDocumentsSuccess(rows, pages, docs));

    return docs;
  } catch (err) {
    console.log('documents.getDocuments', err); //eslint-disable-line

    await dispatch(getDocumentsFail());
    await dispatch(handleError(err));

    throw err;
  }
};

export const getDocumentsByRowNumbers = row_numbers => async (dispatch, getState) => {
  const state = getState();

  try {
    const docs = await Promise.all(row_numbers.map(async (rn) => {
      const params = {
        society_id: state.navigation.id,
        offset: rn - 1,
        limit: 1
      };

      const { data } = await windev.makeApiCall('/document/pending', 'get', params);

      return data.list_manual_document[0];
    }));

    return docs.map(d => ({
      ...d,
      document_id: d && d.documents[0].document_id
    }));
  } catch (err) {
    console.log('documents.getDocumentsByRowNumber', err); //eslint-disable-line

    await dispatch(getDocumentsFail());
    await dispatch(handleError(err));

    throw err;
  }
};

export const refreshDocuments = () => (dispatch, getState) => {
  const { documents } = getCurrentTabState(getState());

  return dispatch(getDocuments(documents.page, documents.limit));
};

export const attachDocuments = () => async (dispatch, getState) => {
  const state = getState();
  const { documents } = getCurrentTabState(state);

  const body = Object.values(documents.selected).reduce(
    (acc, doc) => {
      if (doc) {
        if (doc.document_attached_id !== 0) {
          acc.groups.push(doc.document_attached_id);
        } else {
          acc.ungrouped.push(doc.document_id);
        }
      }

      return acc;
    },
    { society_id: state.navigation.id, groups: [], ungrouped: [] }
  );

  try {
    await dispatch(attachDocumentsAttempt());
    await windev.makeApiCall('/document/attach', 'post', {}, body);
    await dispatch(attachDocumentsSuccess());

    await dispatch(refreshDocuments());
    body.ungrouped.forEach(d => dispatch(removeManualDoc({ document_id: d })));
  } catch (err) {
    console.log('documents.attachDocuments', err); //eslint-disable-line

    await dispatch(attachDocumentsFail());
    await dispatch(handleError(err));

    throw err;
  }
};

export const detachDocuments = () => async (dispatch, getState) => {
  const state = getState();
  const { documents } = getCurrentTabState(state);

  const groups = [];
  const body = Object.values(documents.selected).reduce(
    (acc, doc) => {
      if (doc && doc.document_attached_id !== 0) {
        groups.push(doc);

        acc.groups.push(doc.document_attached_id);
        acc.grouped.push(...doc.documents.map(d => d.document_id));
      }

      return acc;
    },
    { society_id: state.navigation.id, groups: [], grouped: [] }
  );

  try {
    await dispatch(detachDocumentsAttempt());
    await windev.makeApiCall('/document/detach', 'post', {}, body);
    await dispatch(detachDocumentsSuccess());

    await dispatch(refreshDocuments());

    groups.forEach(d => dispatch(removeManualDoc(d)));
  } catch (err) {
    console.log('documents.detachDocuments', err); //eslint-disable-line

    await dispatch(detachDocumentsFail());
    await dispatch(handleError(err));

    throw err;
  }
};

export const deleteDocuments = () => async (dispatch, getState) => {
  const state = getState();
  const { documents } = getCurrentTabState(state);

  const docs = [];
  const body = Object.values(documents.selected).reduce(
    (acc, doc) => {
      if (doc) {
        docs.push(docs);
        acc.documents.push(...doc.documents.map(d => d.document_id));
      }

      return acc;
    },
    { society_id: state.navigation.id, documents: [] }
  );

  try {
    await dispatch(deleteDocumentsAttempt());
    await windev.makeApiCall('/document/delete', 'post', {}, body);
    await dispatch(deleteDocumentsSuccess(docs));

    await dispatch(refreshDocuments());

    docs.map(d => dispatch(removeManualDoc(d)));
  } catch (err) {
    console.log('documents.deleteDocuments', err); //eslint-disable-line

    await dispatch(deleteDocumentsFail());
    await dispatch(handleError(err));

    throw err;
  }
};
