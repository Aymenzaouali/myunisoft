import {
  GET_DOCUMENTS_ATTEMPT, GET_DOCUMENTS_SUCCESS, GET_DOCUMENTS_FAIL,
  CHG_LIMIT_DOCUMENTS, CHG_PAGE_DOCUMENTS,
  SELECT_DOCUMENT, EXPAND_DOCUMENT, RESET_DOCUMENT_SELECTION,

  ATTACH_DOCUMENTS_ATTEMPT, ATTACH_DOCUMENTS_SUCCESS, ATTACH_DOCUMENTS_FAIL,
  DETACH_DOCUMENTS_ATTEMPT, DETACH_DOCUMENTS_SUCCESS, DETACH_DOCUMENTS_FAIL,
  DELETE_DOCUMENTS_ATTEMPT, DELETE_DOCUMENTS_SUCCESS, DELETE_DOCUMENTS_FAIL
} from './constants';

// Actions
export const getDocumentsAttempt = () => ({ type: GET_DOCUMENTS_ATTEMPT });
export const getDocumentsSuccess = (rows, pages, docs) => ({
  type: GET_DOCUMENTS_SUCCESS, rows, pages, docs
});
export const getDocumentsFail = () => ({ type: GET_DOCUMENTS_FAIL });

export const changeLimitDocuments = limit => ({ type: CHG_LIMIT_DOCUMENTS, limit });
export const changePageDocuments = page => ({ type: CHG_PAGE_DOCUMENTS, page });

export const selectDocument = (document_id, value) => ({
  type: SELECT_DOCUMENT, document_id, value
});
export const expandDocument = (document_id, value) => ({
  type: EXPAND_DOCUMENT, document_id, value
});
export const resetDocumentSelection = () => ({ type: RESET_DOCUMENT_SELECTION });

export const attachDocumentsAttempt = () => ({ type: ATTACH_DOCUMENTS_ATTEMPT });
export const attachDocumentsSuccess = () => ({ type: ATTACH_DOCUMENTS_SUCCESS });
export const attachDocumentsFail = () => ({ type: ATTACH_DOCUMENTS_FAIL });

export const detachDocumentsAttempt = () => ({ type: DETACH_DOCUMENTS_ATTEMPT });
export const detachDocumentsSuccess = () => ({ type: DETACH_DOCUMENTS_SUCCESS });
export const detachDocumentsFail = () => ({ type: DETACH_DOCUMENTS_FAIL });

export const deleteDocumentsAttempt = () => ({ type: DELETE_DOCUMENTS_ATTEMPT });
export const deleteDocumentsSuccess = docs => ({ type: DELETE_DOCUMENTS_SUCCESS, docs });
export const deleteDocumentsFail = () => ({ type: DELETE_DOCUMENTS_FAIL });
