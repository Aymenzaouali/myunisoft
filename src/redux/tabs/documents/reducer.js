import {
  GET_DOCUMENTS_ATTEMPT, GET_DOCUMENTS_SUCCESS, GET_DOCUMENTS_FAIL,
  CHG_LIMIT_DOCUMENTS, CHG_PAGE_DOCUMENTS,
  SELECT_DOCUMENT, EXPAND_DOCUMENT, RESET_DOCUMENT_SELECTION,

  ATTACH_DOCUMENTS_ATTEMPT, ATTACH_DOCUMENTS_SUCCESS, ATTACH_DOCUMENTS_FAIL,
  DETACH_DOCUMENTS_ATTEMPT, DETACH_DOCUMENTS_SUCCESS, DETACH_DOCUMENTS_FAIL,
  DELETE_DOCUMENTS_ATTEMPT, DELETE_DOCUMENTS_SUCCESS, DELETE_DOCUMENTS_FAIL
} from './constants';

// Initial state
const initial_state = {
  loading: false,
  error: false,

  limit: 25,
  page: 0,
  rows: 0,
  pages: 0,
  docs: [],
  selected: {},
  expanded: {}
};

// Reducer
const documentsReducer = (state = initial_state, action) => {
  switch (action.type) {
  case GET_DOCUMENTS_ATTEMPT:
    return {
      ...state,
      loading: true,
      error: false,
      docs: [],
      expanded: {}
    };

  case GET_DOCUMENTS_SUCCESS:
    return {
      ...state,
      loading: false,

      rows: action.rows,
      pages: action.pages,
      docs: action.docs
    };

  case GET_DOCUMENTS_FAIL:
    return {
      ...state,
      loading: false,
      error: true
    };

  case CHG_LIMIT_DOCUMENTS:
    return {
      ...state,
      limit: action.limit
    };

  case CHG_PAGE_DOCUMENTS:
    return {
      ...state,
      page: action.page
    };

  case SELECT_DOCUMENT:
    return {
      ...state,
      selected: {
        ...state.selected,
        [action.document_id]: action.value
      }
    };

  case EXPAND_DOCUMENT:
    return {
      ...state,
      expanded: {
        ...state.expanded,
        [action.document_id]: action.value
      }
    };

  case RESET_DOCUMENT_SELECTION:
    return {
      ...state,
      selected: {}
    };

  case ATTACH_DOCUMENTS_ATTEMPT:
  case DETACH_DOCUMENTS_ATTEMPT:
  case DELETE_DOCUMENTS_ATTEMPT:
    return {
      ...state,
      loading: true,
      error: false
    };

  case ATTACH_DOCUMENTS_SUCCESS:
  case DETACH_DOCUMENTS_SUCCESS:
  case DELETE_DOCUMENTS_SUCCESS:
    return {
      ...state,
      loading: false,
      selected: {},
      expanded: {}
    };

  case ATTACH_DOCUMENTS_FAIL:
  case DETACH_DOCUMENTS_FAIL:
  case DELETE_DOCUMENTS_FAIL:
    return {
      ...state,
      loading: false,
      error: true
    };

  default:
    return state;
  }
};

export default documentsReducer;
