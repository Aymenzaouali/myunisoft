import { SET_DELETION_DIALOG } from './constants';

export const setDeletionDialog = (mode, id) => ({ type: SET_DELETION_DIALOG, mode, id });
