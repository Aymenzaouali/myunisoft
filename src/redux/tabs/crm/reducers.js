import { SET_DELETION_DIALOG } from './constants';

const initialState = {};

const crm = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case SET_DELETION_DIALOG:
      return {
        ...state,
        deletionInformation: {
          mode: action.mode,
          id: action.id
        }
      };
    default: return state;
    }
  }
  return state;
};

export default crm;
