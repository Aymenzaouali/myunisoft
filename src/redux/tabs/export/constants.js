export const SET_SOCIETY = 'export/SET_SOCIETY';

export const GET_FEC_ATTEMPT = 'export/GET_FEC_ATTEMPT';
export const GET_FEC_SUCCESS = 'export/GET_FEC_SUCCESS';
export const GET_FEC_FAIL = 'export/GET_FEC_FAIL';

export const RESET_IMPORT = 'export/RESET_IMPORT';
