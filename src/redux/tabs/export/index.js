import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';
import _ from 'lodash';

import {
  getFecAttempt,
  getFecSuccess,
  getFecFail
} from './actions';

export const exportFec = ({
  societyInfo, exercice, dateEnd, dateStart
}) => async (dispatch) => {
  const {
    society_id
  } = societyInfo;
  const params = _.isEmpty(exercice)
    ? {
      export_type: 1,
      from: dateStart,
      to: dateEnd
    }
    : {
      export_type: 0,
      exercice_id: _.get(exercice, 'id_exercice')
    };

  try {
    await dispatch(getFecAttempt(society_id));
    const { data, headers } = await windev.makeApiCall('/export/FEC', 'post', params, {}, { headers: { 'Content-Type': 'application/json', 'society-id': society_id } });
    const filename = _.split(_.get(headers, 'content-disposition'), '"')[1];
    const url = window.URL.createObjectURL(new Blob([data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${filename}`);
    document.body.appendChild(link);
    link.click();
    await dispatch(getFecSuccess(society_id));
    return data;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    await dispatch(getFecFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};
