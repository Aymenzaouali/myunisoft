import {
  SET_SOCIETY,

  GET_FEC_ATTEMPT,
  GET_FEC_SUCCESS,
  GET_FEC_FAIL,

  RESET_IMPORT
} from './constants';

export const setSociety = societyId => ({
  type: SET_SOCIETY,
  societyId
});

export const getFecAttempt = societyId => ({
  type: GET_FEC_ATTEMPT,
  societyId
});

export const getFecSuccess = societyId => ({
  type: GET_FEC_SUCCESS,
  societyId
});

export const getFecFail = societyId => ({
  type: GET_FEC_FAIL,
  societyId
});

export const resetImport = payload => ({
  type: RESET_IMPORT,
  payload
});
