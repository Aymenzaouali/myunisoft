import {
  GET_FEC_ATTEMPT,
  GET_FEC_SUCCESS,
  GET_FEC_FAIL,

  SET_SOCIETY,

  RESET_IMPORT
} from './constants';

const initialState = {
  fec: {}
};

const exports = (state = initialState, action) => {
  switch (action.type) {
  case SET_SOCIETY: {
    return {
      ...state,
      selectedSociety: action.societyId
    };
  }

  case GET_FEC_ATTEMPT:
    return {
      ...state,
      fec: {
        ...state.fec,
        [action.societyId]: {
          isLoading: true,
          importSuccess: false
        }
      }
    };
  case GET_FEC_SUCCESS:
    return {
      ...state,
      fec: {
        ...state.fec,
        [action.societyId]: {
          isLoading: false,
          importSuccess: true
        }
      }
    };
  case GET_FEC_FAIL:
    return {
      ...state,
      fec: {
        ...state.fec,
        [action.societyId]: {
          isLoading: false,
          importSuccess: false
        }
      }
    };

  case RESET_IMPORT:
    return {
      ...state,
      selectedType: action.payload.target.value
    };

  default:
    return state;
  }
};

export default exports;
