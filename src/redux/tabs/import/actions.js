import {
  POST_IMPORT_COMPTASA_ATTEMPT,
  POST_IMPORT_COMPTASA_SUCCESS,
  POST_IMPORT_COMPTASA_FAIL,

  POST_IMPORT_CEGID_ATTEMPT,
  POST_IMPORT_CEGID_SUCCESS,
  POST_IMPORT_CEGID_FAIL,

  POST_IMPORT_EBICS_ATTEMPT,
  POST_IMPORT_EBICS_SUCCESS,
  POST_IMPORT_EBICS_FAIL,

  POST_FEC_ATTEMPT,
  POST_FEC_SUCCESS,
  POST_FEC_FAIL,

  POST_EXCEL_ATTEMPT,
  POST_EXCEL_SUCCESS,
  POST_EXCEL_FAIL,

  POST_ASSETS_ATTEMPT,
  POST_ASSETS_SUCCESS,
  POST_ASSETS_FAIL,

  RESET_IMPORT,

  ADD_EBICS,
  REMOVE_EBICS,

  ADD_EXCEL,
  REMOVE_EXCEL,

  SET_SOCIETY,

  GET_EXERCICE_INFOS_ATTEMPT,
  GET_EXERCICE_INFOS_SUCCESS,
  GET_EXERCICE_INFOS_FAIL
} from './constants';

export const importComptaSaAttempt = () => ({
  type: POST_IMPORT_COMPTASA_ATTEMPT
});
export const importComptaSaSuccess = payload => ({
  type: POST_IMPORT_COMPTASA_SUCCESS,
  payload
});
export const importComptaSaFail = err => ({
  type: POST_IMPORT_COMPTASA_FAIL,
  err
});

export const importCegidAttempt = () => ({
  type: POST_IMPORT_CEGID_ATTEMPT
});
export const importCegidSuccess = payload => ({
  type: POST_IMPORT_CEGID_SUCCESS,
  payload
});
export const importCegidFail = () => ({
  type: POST_IMPORT_CEGID_FAIL
});

export const importEbicsAttempt = () => ({
  type: POST_IMPORT_EBICS_ATTEMPT
});
export const importEbicsSuccess = payload => ({
  type: POST_IMPORT_EBICS_SUCCESS,
  payload
});
export const importEbicsFail = () => ({
  type: POST_IMPORT_EBICS_FAIL
});

export const addEbics = files => ({
  type: ADD_EBICS,
  files
});
export const removeEbics = id => ({
  type: REMOVE_EBICS,
  id
});

export const addExcel = files => ({
  type: ADD_EXCEL,
  files
});
export const removeExcel = id => ({
  type: REMOVE_EXCEL,
  id
});

export const setSociety = societyId => ({
  type: SET_SOCIETY,
  societyId
});

export const postFecAttempt = societyId => ({
  type: POST_FEC_ATTEMPT,
  societyId
});

export const postFecSuccess = (rows, societyId) => ({
  type: POST_FEC_SUCCESS,
  rows,
  societyId
});

export const postFecFail = societyId => ({
  type: POST_FEC_FAIL,
  societyId
});

export const postExcelAttempt = society_id => ({
  type: POST_EXCEL_ATTEMPT,
  society_id
});

export const postExcelSuccess = payload => ({
  type: POST_EXCEL_SUCCESS,
  payload
});

export const postExcelFail = society_id => ({
  type: POST_EXCEL_FAIL,
  society_id
});

export const postAssetsAttempt = societyId => ({
  type: POST_ASSETS_ATTEMPT,
  societyId
});

export const postAssetsSuccess = (rows, societyId) => ({
  type: POST_ASSETS_SUCCESS,
  rows,
  societyId
});

export const postAssetsFail = societyId => ({
  type: POST_ASSETS_FAIL,
  societyId
});

export const resetImport = societyId => ({
  type: RESET_IMPORT,
  societyId
});

export const getExerciceInfosAttempt = () => ({
  type: GET_EXERCICE_INFOS_ATTEMPT
});

export const getExerciceInfosSuccess = infos => ({
  type: GET_EXERCICE_INFOS_SUCCESS,
  infos
});

export const getExerciceInfosFail = () => ({
  type: GET_EXERCICE_INFOS_FAIL
});
