import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';
import _ from 'lodash';
import { getExercises as getExercicesCompany } from 'redux/tabs/companyCreation';
import {
  importComptaSaAttempt,
  importComptaSaSuccess,
  importComptaSaFail,

  importCegidAttempt,
  importCegidSuccess,
  importCegidFail,

  importEbicsAttempt,
  importEbicsSuccess,
  importEbicsFail,

  postFecAttempt,
  postFecSuccess,
  postFecFail,

  postExcelAttempt,
  postExcelSuccess,
  postExcelFail,

  postAssetsAttempt,
  postAssetsSuccess,
  postAssetsFail,

  getExerciceInfosAttempt,
  getExerciceInfosSuccess,
  getExerciceInfosFail
} from './actions';

export const importComptaSa = (payload = {}) => async (dispatch) => {
  const { member_code, society_code } = payload;
  try {
    await dispatch(importComptaSaAttempt());
    const { data } = await windev.makeApiCall('/Import_comptasa', 'post', { member_code, society_code }, {});
    await dispatch(importComptaSaSuccess(data));
    return data;
  } catch (err) {
    await dispatch(importComptaSaFail(err));
    await dispatch(handleError(err));
    return err;
  }
};

export const importCegid = (payload = {}) => async (dispatch) => {
  const { member_id, filename } = payload;
  try {
    await dispatch(importCegidAttempt());
    const { data } = await windev.makeApiCall('/TRA', 'post', { member_id, filename: filename.name }, filename, { headers: { 'Content-Type': 'application/octet-stream' } });
    await dispatch(importCegidSuccess(data));
    return data;
  } catch (err) {
    await dispatch(importCegidFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const importEbics = (file = {}) => async (dispatch) => {
  try {
    await dispatch(importEbicsAttempt());
    const { data } = await windev.makeApiCall('/releve_bancaire', 'post', { filename: file.blob.name }, file.blob, { headers: { 'Content-Type': 'application/octet-stream' } });
    await dispatch(importEbicsSuccess(data));
    return data;
  } catch (err) {
    await dispatch(importEbicsFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const postFec = (fecData, refreshExCompany = false) => async (dispatch, getState) => {
  const state = getState();
  const params = {
    society_id: fecData.society_id || _.get(state, 'navigation.id'),
    from_coala: Number(fecData.fecImportFrom),
    filename: fecData.importedFile.name,
    exercice_id: fecData.exerciseId,
    type: fecData.importMode || 0
  };
  try {
    await dispatch(postFecAttempt(params.society_id));
    const { data } = await windev.makeApiCall('/FEC', 'post', params, fecData.importedFile, { headers: { 'Content-Type': 'application/octet-stream' } });

    await dispatch(postFecSuccess(data, params.society_id));
    if (refreshExCompany) {
      await dispatch(getExercicesCompany(params.society_id));
    }
    return data;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    await dispatch(postFecFail(params.society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const postExcel = ({ society_id, importedFile }) => async (dispatch) => {
  const params = {
    filename: importedFile.name
  };
  try {
    await dispatch(postExcelAttempt(society_id));
    const { data } = await windev.makeApiCall('/CSV', 'post', params, importedFile, { headers: { 'Content-Type': 'application/octet-stream', 'society-id': society_id } });
    await dispatch(postExcelSuccess({ society_id, data }));
    return data;
  } catch (err) {
    console.log(err);
    await dispatch(postExcelFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const postAssets = ({
  society_id, importedFile, format
}) => async (dispatch, getState) => {
  const state = getState();
  const params = {
    society_id: society_id || _.get(state, 'navigation.id'),
    filename: importedFile.name,
    format
  };
  try {
    await dispatch(postAssetsAttempt(params.society_id));
    const { data } = await windev.makeApiCall('/import_immo', 'post', params, importedFile, { headers: { 'Content-Type': 'application/octet-stream' } });
    await dispatch(postAssetsSuccess(data, params.society_id));
    return data;
  } catch (err) {
    await dispatch(postAssetsFail(params.society_id));
    await dispatch(handleError(err));
    return err;
  }
};

export const getExerciceInfos = (exerciceId, societyIdArg) => async (dispatch, getState) => {
  const state = getState();
  const societyId = societyIdArg || _.get(state, 'navigation.id');

  try {
    await dispatch(getExerciceInfosAttempt(societyId));
    const { data } = await windev.makeApiCall(`/exercices/${exerciceId}/infos`, 'get', undefined, undefined, {
      headers: {
        'society-id': societyId
      }
    });

    dispatch(getExerciceInfosSuccess(data));

    return data;
  } catch (err) {
    await dispatch(getExerciceInfosFail(societyId));
    await dispatch(handleError(err));
    return err;
  }
};
