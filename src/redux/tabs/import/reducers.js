import {
  POST_IMPORT_COMPTASA_ATTEMPT,
  POST_IMPORT_COMPTASA_SUCCESS,
  POST_IMPORT_COMPTASA_FAIL,

  POST_IMPORT_CEGID_ATTEMPT,
  POST_IMPORT_CEGID_SUCCESS,
  POST_IMPORT_CEGID_FAIL,

  POST_IMPORT_EBICS_ATTEMPT,
  POST_IMPORT_EBICS_SUCCESS,
  POST_IMPORT_EBICS_FAIL,

  POST_FEC_ATTEMPT,
  POST_FEC_SUCCESS,
  POST_FEC_FAIL,

  POST_EXCEL_ATTEMPT,
  POST_EXCEL_SUCCESS,
  POST_EXCEL_FAIL,

  POST_ASSETS_ATTEMPT,
  POST_ASSETS_SUCCESS,
  POST_ASSETS_FAIL,

  RESET_IMPORT,

  ADD_EBICS,
  REMOVE_EBICS,

  ADD_EXCEL,
  REMOVE_EXCEL,

  SET_SOCIETY,

  GET_EXERCICE_INFOS_ATTEMPT,
  GET_EXERCICE_INFOS_SUCCESS,
  GET_EXERCICE_INFOS_FAIL
} from './constants';

const initialState = {
  isLoading: false,
  ebics: [],
  excel: [],
  error: {},
  fec: {},
  assets: {},
  exerciceInfos: {}
};

let nextDocumentId = 0;

const imports = (state = initialState, action) => {
  switch (action.type) {
  case POST_IMPORT_COMPTASA_ATTEMPT: {
    return {
      ...state,
      isLoading: true,
      importSuccess: false
    };
  }
  case POST_IMPORT_COMPTASA_SUCCESS: {
    return {
      ...state,
      isLoading: false,
      importSuccess: true,
      error: {}
    };
  }
  case POST_IMPORT_COMPTASA_FAIL: {
    return {
      ...state,
      isLoading: false,
      error: action.err.response.data,
      importSuccess: false
    };
  }

  case POST_IMPORT_CEGID_ATTEMPT: {
    return {
      ...state,
      isLoading: true,
      importSuccess: false
    };
  }
  case POST_IMPORT_CEGID_SUCCESS: {
    return {
      ...state,
      isLoading: false,
      importSuccess: true
    };
  }
  case POST_IMPORT_CEGID_FAIL: {
    return {
      ...state,
      isLoading: false,
      importSuccess: false
    };
  }

  case POST_IMPORT_EBICS_ATTEMPT: {
    return {
      ...state,
      isLoading: true,
      importSuccess: false
    };
  }
  case POST_IMPORT_EBICS_SUCCESS: {
    return {
      ...state,
      isLoading: false,
      importSuccess: true,
      ebics: []
    };
  }
  case POST_IMPORT_EBICS_FAIL: {
    return {
      ...state,
      isLoading: false,
      importSuccess: false
    };
  }

  case ADD_EBICS: {
    const newEbics = action.files.map(f => ({
      blob: f,
      status: 'waiting',
      type: 'dat',
      // eslint-disable-next-line
      id: nextDocumentId++
    }));
    return ({
      ...state,
      ebics: [...state.ebics, ...newEbics]
    });
  }

  case REMOVE_EBICS:
    return ({
      ...state,
      ebics: state.ebics.filter(f => f.id !== action.id)
    });

  case ADD_EXCEL: {
    const newExcel = action.files.map(f => ({
      blob: f,
      status: 'waiting',
      type: 'dat',
      // eslint-disable-next-line
        id: nextDocumentId++
    }));
    return ({
      ...state,
      excel: [...state.excel, ...newExcel]
    });
  }

  case REMOVE_EXCEL:
    return ({
      ...state,
      excel: state.excel.filter(f => f.id !== action.id)
    });

  case SET_SOCIETY: {
    return {
      ...initialState,
      selectedSociety: action.societyId
    };
  }

  case POST_FEC_ATTEMPT:
    return {
      ...state,
      isLoading: true,
      fec: {
        importSuccess: false
      }
    };
  case POST_FEC_SUCCESS:
    return {
      ...state,
      isLoading: false,
      fec: {
        ...action.rows,
        importSuccess: true
      }
    };
  case POST_FEC_FAIL:
    return {
      ...state,
      isLoading: false,
      fec: {
        importSuccess: false
      }
    };

  case POST_EXCEL_ATTEMPT:
    return {
      ...state,
      excelImport: {
        ...state.excelImport,
        [action.society_id]: {
          isLoading: true,
          importSuccess: false
        }
      }
    };
  case POST_EXCEL_SUCCESS:
    return {
      ...state,
      excelImport: {
        ...state.excelImport,
        [action.payload.society_id]: {
          rows_number: action.payload.data.rows_number,
          isLoading: false,
          importSuccess: true
        }
      }
    };
  case POST_EXCEL_FAIL:
    return {
      ...state,
      excelImport: {
        ...state.excelImport,
        [action.society_id]: {
          isLoading: false,
          importSuccess: false
        }
      }
    };

  case POST_ASSETS_ATTEMPT:
    return {
      ...state,
      assets: {
        ...state.assets,
        [action.societyId]: {
          isLoading: true,
          importSuccess: false
        }
      }
    };
  case POST_ASSETS_SUCCESS:
    return {
      ...state,
      assets: {
        ...state.assets,
        [action.societyId]: {
          ...action.rows,
          isLoading: false,
          importSuccess: true
        }
      }
    };
  case POST_ASSETS_FAIL:
    return {
      ...state,
      assets: {
        ...state.assets,
        [action.societyId]: {
          isLoading: false,
          importSuccess: false
        }
      }
    };
  case RESET_IMPORT:
    return {
      ...initialState
    };
  case GET_EXERCICE_INFOS_ATTEMPT:
    return {
      ...state,
      isLoading: true,
      exerciceInfos: {}
    };
  case GET_EXERCICE_INFOS_SUCCESS:
    return {
      ...state,
      isLoading: false,
      exerciceInfos: action.infos
    };
  case GET_EXERCICE_INFOS_FAIL:
    return {
      ...state,
      isLoading: false
    };
  default:
    return state;
  }
};

export default imports;
