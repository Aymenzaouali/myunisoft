import {
  CLOSING_YEAR_ATTEMPT, CLOSING_YEAR_SUCCESS, CLOSING_YEAR_FAIL
} from './constants';

// Initial state
const initial_state = {
  loading: false,
  error: false
};

// Reducer
const closingReducer = (state = initial_state, action) => {
  switch (action.type) {
  case CLOSING_YEAR_ATTEMPT:
    return {
      ...state,
      loading: true,
      error: false
    };

  case CLOSING_YEAR_SUCCESS:
    return {
      ...state,
      loading: false
    };

  case CLOSING_YEAR_FAIL:
    return {
      ...state,
      loading: false,
      error: true
    };

  default:
    return state;
  }
};

export default closingReducer;
