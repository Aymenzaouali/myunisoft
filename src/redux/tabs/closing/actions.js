import {
  CLOSING_YEAR_ATTEMPT, CLOSING_YEAR_SUCCESS, CLOSING_YEAR_FAIL
} from './constants';

// Actions
export const closingYearAttempt = () => ({ type: CLOSING_YEAR_ATTEMPT });
export const closingYearSuccess = () => ({ type: CLOSING_YEAR_SUCCESS });
export const closingYearFail = () => ({ type: CLOSING_YEAR_FAIL });
