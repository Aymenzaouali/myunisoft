// Constants
export const CLOSING_YEAR_ATTEMPT = 'closing/CLOSING_YEAR_ATTEMPT';
export const CLOSING_YEAR_SUCCESS = 'closing/CLOSING_YEAR_SUCCESS';
export const CLOSING_YEAR_FAIL = 'closing/CLOSING_YEAR_FAIL';
