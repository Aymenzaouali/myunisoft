import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';

import {
  refreshExerciceDashboard
} from 'redux/dashboard';
import {
  closingYearAttempt, closingYearSuccess, closingYearFail
} from './actions';


// Thunks
export const closingYear = (id_exercice, options) => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;

  try {
    await dispatch(closingYearAttempt());
    await windev.makeApiCall(`society/${society_id}/closeExercice`, 'put', { id_exercice }, {});
    if (options.do_generate) {
      await windev.makeApiCall(`society/${society_id}/generateCarryForward`, 'post', {}, {});
    }

    await dispatch(refreshExerciceDashboard());
    await dispatch(closingYearSuccess());

    return true;
  } catch (err) {
    await dispatch(closingYearFail());
    await dispatch(handleError(err));
    return false;
  }
};
