import _ from 'lodash';

import {
  ADD_MANUAL_DOCS, REMOVE_MANUAL_DOC, RESET_MANUAL_DOCS,
  SET_DEFAULT_FILTER
} from './constants';

// Initial state
const initial_state = {
  manual_docs: {},
  default_filter: null
};

// Reducer
const accountingReducer = (state = initial_state, action) => {
  switch (action.type) {
  case ADD_MANUAL_DOCS:
    return {
      ...state,
      manual_docs: {
        ...state.manual_docs,
        ..._.fromPairs(action.docs.map(d => [d.document_id, d]))
      }
    };

  case REMOVE_MANUAL_DOC: {
    const document_id = action.doc.document_attached_id
      !== null ? action.doc.document_attached_id : action.doc.document_id;
    const {
      [document_id]: doc,
      ...others
    } = state.manual_docs;
    return {
      ...state,
      manual_docs: others
    };
  }

  case RESET_MANUAL_DOCS:
    return {
      ...state,
      manual_docs: {}
    };

  case SET_DEFAULT_FILTER:
    return {
      ...state,
      default_filter: {
        ...state.default_filter,
        ...action.filter
      }
    };

  default:
    return state;
  }
};

export default accountingReducer;
