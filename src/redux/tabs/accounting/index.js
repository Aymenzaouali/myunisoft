import { windev } from 'helpers/api';

import { setDefaultFilter } from './actions';

// Thunks
export const getDefaultFilter = () => async (dispatch, getState) => {
  const state = getState();
  const params = {
    etablissement_id: state.navigation.id
  };

  try {
    await dispatch(setDefaultFilter(null));
    const { data } = await windev.makeApiCall('/entries/default_filter', 'get', params);
    const filter = {
      start_date: data.start_date,
      end_date: data.end_date,
      diary: {
        diary_id: data.diary_id,
        code: data.diary_code,
        name: data.diary_label
      }
    };

    await dispatch(setDefaultFilter(filter));
  } catch (err) {
    console.log('accounting.getDefaultFilter', err);

    throw err;
  }
};
