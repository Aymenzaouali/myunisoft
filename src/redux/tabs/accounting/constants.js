export const ADD_MANUAL_DOCS = 'accounting/ADD_MANUAL_DOCS';
export const REMOVE_MANUAL_DOC = 'accounting/REMOVE_MANUAL_DOC';
export const RESET_MANUAL_DOCS = 'accounting/RESET_MANUAL_DOCS';

export const SET_DEFAULT_FILTER = 'accounting/SET_DEFAULT_FILTER';
