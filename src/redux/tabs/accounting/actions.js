import {
  ADD_MANUAL_DOCS, REMOVE_MANUAL_DOC, RESET_MANUAL_DOCS,
  SET_DEFAULT_FILTER
} from './constants';

// Actions
export const addManualDocs = docs => ({ type: ADD_MANUAL_DOCS, docs });
export const removeManualDoc = doc => ({ type: REMOVE_MANUAL_DOC, doc });
export const resetManualDocs = () => ({ type: RESET_MANUAL_DOCS });

export const setDefaultFilter = filter => ({ type: SET_DEFAULT_FILTER, filter });
