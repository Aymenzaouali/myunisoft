import _ from 'lodash';

import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';

import { initialize, getFormValues } from 'redux-form';
import { setCurrentForm, closeCurrentForm } from 'redux/tabs/form/actions';

import { getCurrentTabState, tabFormName } from 'helpers/tabs';

import { validationSocietyPPTable, validationEmployeePPTable } from 'helpers/validationTable';

import {
  SOCIETY_PP_TABLE_NAME,
  EMPLOYEE_PP_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import tableActions from 'redux/tables/actions';

import moment from 'moment';
import actions from './actions';

export const getSignatoryFunction = () => async (dispatch) => {
  try {
    await dispatch(actions.getSignatoryFunctionAttempt());
    const { data } = await windev.makeApiCall('/signatory_function', 'get');
    await dispatch(actions.getSignatoryFunctionSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err, getSignatoryFunction));
    return err;
  }
};

export const getEmploymentFunction = () => async (dispatch) => {
  try {
    await dispatch(actions.getEmploymentFunctionAttempt());
    const { data } = await windev.makeApiCall('/function', 'get');
    await dispatch(actions.getEmploymentFunctionSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err, getEmploymentFunction));
    return err;
  }
};

export const getCivility = () => async (dispatch) => {
  try {
    await dispatch(actions.getCivilityAttempt());
    const { data } = await windev.makeApiCall('/civility', 'get');
    await dispatch(actions.getCivilitySuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err, getCivility));
    return err;
  }
};

export const getMaritalSituation = () => async (dispatch) => {
  try {
    await dispatch(actions.getMaritalSituationAttempt());
    const { data } = await windev.makeApiCall('/marital_situation', 'get');
    await dispatch(actions.getMaritalSituationSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err, getMaritalSituation));
    return err;
  }
};

export const getMaritalRegime = () => async (dispatch) => {
  try {
    await dispatch(actions.getMaritalRegimeAttempt());
    const { data } = await windev.makeApiCall('/matrimonial_regime', 'get');
    await dispatch(actions.getMaritalRegimeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err, getMaritalRegime));
    return err;
  }
};

export const getPersPhysiqueType = () => async (dispatch) => {
  try {
    await dispatch(actions.getPersPhysiqueTypeAttempt());
    const { data } = await windev.makeApiCall('type', 'get');
    await dispatch(actions.getPersPhysiqueTypeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err, getPersPhysiqueType));
    return err;
  }
};

export const getCoordType = () => async (dispatch) => {
  try {
    await dispatch(actions.getCoordTypeAttempt());
    const { data } = await windev.makeApiCall('coordonnee_type', 'get');
    await dispatch(actions.getCoordTypeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err, getCoordType));
    return err;
  }
};

export const postPersPhysique = payload => async (dispatch) => {
  const {
    social_security_number
  } = payload;

  const params = {
    ...payload,
    social_security_number: social_security_number && social_security_number.replace(/\s+/g, '')
  };

  try {
    await dispatch(actions.postPersPhysiqueAttempt());
    const { data } = await windev.makeApiCall('/pers_physique', 'post', {}, params);
    await dispatch(actions.postPersPhysiqueSuccess(data));
    await dispatch(actions.selectPhysicalPerson(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.postPersPhysiqueFail());
    return err;
  }
};

export const getPersPhysiqueById = personId => async (dispatch, getState) => {
  const state = getState();
  try {
    await dispatch(actions.getPersPhysiqueByIdAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}`, 'get');
    await dispatch(actions.getPersPhysiqueByIdSuccess(data));

    // Initialise form
    const formData = {
      ...data,
      civility_id: _.get(data, 'civility.id', '').toString(),
      marital_situation_id: _.get(data, 'marital_situation.id'),
      matrimonial_regime_id: _.get(data, 'matrimonial_regime.id'),
      physical_person_type_id: _.get(data, 'physical_person_type.id'),
      road_type_id: _.get(data, 'road_type.id'),
      coordonnee: _.get(data, 'coordonnee', []).map(e => ({
        type_id: _.get(e, 'type.id', 0),
        ...e
      }))
    };

    dispatch(closeCurrentForm(() => {
      dispatch(initialize(tabFormName('physicalPersonCreationForm', state.navigation.id), formData));
      dispatch(setCurrentForm(tabFormName('physicalPersonCreationForm', state.navigation.id), formData));
    }));

    return data;
  } catch (err) {
    await dispatch(handleError(err, getPersPhysiqueById));
    return err;
  }
};

export const putPersPhysique = (personId, payload) => async (dispatch) => {
  const {
    social_security_number,
    coordonnee: coord
  } = payload;

  const coordonnee = coord && coord.map(e => ({
    ...e,
    type_id: e.type_id || e.type.id
  }));

  const body = {
    ...payload,
    coordonnee,
    social_security_number: social_security_number && social_security_number.replace(/\s+/g, '')
  };

  try {
    await dispatch(actions.putPersPhysiqueAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}`, 'put', {}, body);
    await dispatch(actions.putPersPhysiqueSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err, putPersPhysique));
    return err;
  }
};

export const getLinkType = () => async (dispatch) => {
  try {
    await dispatch(actions.getLinkTypeAttempt());
    const { data } = await windev.makeApiCall('/link_type', 'get');
    await dispatch(actions.getLinkTypeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.getLinkTypeFail());
    return err;
  }
};

export const getFamilyLink = personId => async (dispatch) => {
  try {
    await dispatch(actions.getFamilyLinkAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}/family_link`, 'get');
    await dispatch(actions.getFamilyLinkSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.getFamilyLinkFail());
    return err;
  }
};

export const postFamilyLink = (id, family_link_list) => async (dispatch) => {
  const pers_physique_id_A = id;
  const body = { pers_physique_id_A, family_link_list };
  try {
    await dispatch(actions.postFamilyLinkAttempt());
    const { data } = await windev.makeApiCall('/pers_physique/family_link', 'post', {}, body);
    await dispatch(actions.postFamilyLinkSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.postFamilyLinkFail());
    return err;
  }
};

export const putFamilyLink = (personId, family_link_list) => async (dispatch) => {
  const body = { family_link_list };
  try {
    await dispatch(actions.putPersPhysiqueAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}/family_link`, 'put', {}, body);
    await dispatch(actions.putPersPhysiqueSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.putFamilyLinkFail());
    return err;
  }
};

export const delFamilyLink = family_link => async (dispatch) => {
  const body = { family_link };
  try {
    await dispatch(actions.delFamilyLinkAttempt());
    const { data } = await windev.makeApiCall('/pers_physique/family_link', 'delete', {}, body);
    await dispatch(actions.delFamilyLinkFail(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.delFamilyLinkFail());
    return err;
  }
};

export const getDocument = personId => async (dispatch) => {
  try {
    await dispatch(actions.getDocumentAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}/document`, 'get');
    await dispatch(actions.getDocumentSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.getDocumentFail());
    return err;
  }
};

export const postDocument = (personId, payload) => async (dispatch) => {
  const params = { filename_extension: payload.name };
  const body = payload.file;
  try {
    await dispatch(actions.postDocumentAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}/document`, 'post', params, body, { headers: { 'Content-Type': 'application/octet-stream' } });
    await dispatch(actions.postDocumentSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.postDocumentFail());
    return err;
  }
};

export const delDocument = (documentInfo, personId) => async (dispatch, getState) => {
  const state = getState();
  const newPersonId = _.get(getCurrentTabState(state), 'physicalPersonCreation.newlyCreatedPersonId');
  const body = {
    l_document_pp: [
      { id: documentInfo.id }
    ]
  };

  try {
    await dispatch(actions.delDocumentAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId || newPersonId}/document`, 'delete', {}, body);
    await dispatch(actions.delDocumentSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.delDocumentFail());
    return err;
  }
};

export const getSocietyPP = personId => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;
  try {
    await dispatch(actions.getSocietyAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}/associate`, 'get');
    await dispatch(tableActions.setData(getTableName(society_id, SOCIETY_PP_TABLE_NAME), (data || []).map(lp => ({ id: _.get(lp, 'society.id'), ...lp }))));

    await dispatch(actions.getSocietySuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.getSocietyFail());
    return err;
  }
};

export const delSociety = (personId, params, tableName) => async (dispatch) => {
  try {
    await dispatch(actions.delSocietyAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}/associate`, 'delete', {}, params);
    await dispatch(tableActions.deleteRows(tableName));
    await dispatch(actions.delSocietySuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.delSocietyFail());
    return err;
  }
};

export const putSocietyAccess = (user_id, addedCompanies) => async (dispatch) => {
  const access_list = addedCompanies.map(e => ({
    society_id: e.company.society_id,
    profil_id: e.level.value
  }));
  const body = { user_id, access_list };
  try {
    await dispatch(actions.putSocietyAttempt());
    const { data } = await windev.makeApiCall('/user', 'put', {}, body);
    await dispatch(actions.putSocietySuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.putSocietyFail());
    return err;
  }
};

export const delSocietyAccess = (user_id, removedCompany) => async (dispatch) => {
  const delete_list = [{
    society_id: removedCompany.company.society_id
  }];
  const body = { user_id, delete_list };
  try {
    await dispatch(actions.delSocietyAccessAttempt());
    const { data } = await windev.makeApiCall('/user', 'put', {}, body);
    await dispatch(actions.delSocietyAccessSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.delSocietyAccessFail());
    return err;
  }
};

export const getSocietyAccess = user_id => async (dispatch) => {
  const params = {
    limit: 1,
    user_id
  };
  try {
    await dispatch(actions.getSocietyAccessAttempt());
    const { data } = await windev.makeApiCall('/user', 'get', params);
    await dispatch(actions.getSocietyAccessSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.getSocietyAccessFail());
    return err;
  }
};

export const getEmployee = personId => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;

  try {
    await dispatch(actions.getEmployeeAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}/employee`, 'get');
    await dispatch(tableActions.setData(getTableName(society_id, EMPLOYEE_PP_TABLE_NAME), (data || []).map(lp => ({ id: _.get(lp, 'society.id'), ...lp }))));

    await dispatch(actions.getEmployeeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.getEmployeeFail());
    return err;
  }
};

export const postEmployee = personId => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, EMPLOYEE_PP_TABLE_NAME);

  const editedRows = _.get(state, `tables.${tableName}.editedRows`);
  const createdRows = _.get(state, `tables.${tableName}.createdRows`);

  const formName = tabFormName('physicalPersonCreationForm', society_id);
  const physicalPerson = getFormValues(formName)(state);

  const firm_form = _.get(physicalPerson, 'firm', {});

  let effectiveDateForm = _.get(firm_form, 'effective_date', '');

  const society_list = Object.values({ ...createdRows, ...editedRows }).map(employee => ({
    society_id: employee.id,
    function_id: _.get(employee, 'function.id') || _.get(employee, 'function.value.id', null),
    signatory_function_id: _.get(employee, 'signatory_function.id') || _.get(employee, 'signatory_function.value.id', null),
    effective_date: employee.start_date,
    end_date: _.get(employee, 'end_date', '')
  }));
  if (effectiveDateForm instanceof moment) {
    effectiveDateForm = effectiveDateForm.format('YYYY-MM-DD');
  }

  const validation = validationEmployeePPTable(society_list);

  const {
    hasError,
    errors
  } = validation;

  if (hasError) {
    try {
      await dispatch(tableActions.setAllLinesErrors(tableName, errors));
      return validation;
    } catch (err) {
      return err;
    }
  } else {
    try {
      await dispatch(actions.postEmployeeAttempt());
      const { data } = await windev.makeApiCall(`/pers_physique/${personId}/employee`, 'post', {}, society_list);
      await dispatch(actions.postEmployeeSuccess());
      await dispatch(getEmployee(personId));
      return data;
    } catch (err) {
      await dispatch(actions.postEmployeeFail());
      await dispatch(tableActions.setErrors(tableName, 0, { updateError: _.get(err, 'response.data') }));
      await dispatch(handleError(err, actions.postEmployeeFail));
      return err;
    }
  }
};

export const delEmployee = (personId, params, tableName) => async (dispatch) => {
  try {
    await dispatch(actions.delEmployeeAttempt());
    const { data } = await windev.makeApiCall(`/pers_physique/${personId}/employee`, 'delete', {}, params);
    await dispatch(tableActions.deleteRows(tableName));
    await dispatch(actions.delEmployeeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err));
    await dispatch(actions.delEmployeeFail());
    return err;
  }
};

export const postAssociate = personId => async (dispatch, getState) => {
  const state = getState();
  const society_id = state.navigation.id;
  const tableName = getTableName(society_id, SOCIETY_PP_TABLE_NAME);

  const editedRows = _.get(state, `tables.${tableName}.editedRows`);
  const createdRows = _.get(state, `tables.${tableName}.createdRows`);

  const formName = tabFormName('physicalPersonCreationForm', society_id);
  const physicalPerson = getFormValues(formName)(state);

  const firm_form = _.get(physicalPerson, 'firm', {});

  let effectiveDateForm = _.get(firm_form, 'effective_date', '');

  const society_list = Object.values({ ...createdRows, ...editedRows }).map(society => ({
    society_id: society.id,
    function_id: _.get(society, 'function.id') || _.get(society, 'function.value.id', null),
    signatory_function_id: _.get(society, 'signatory_function.id') || _.get(society, 'signatory_function.value.id', null),
    effective_date: society.start_date,
    end_date: _.get(society, 'end_date', ''),
    social_part: society.social_part && {
      NP: _.get(society, 'social_part.NP', 0),
      PP: _.get(society, 'social_part.PP', 0),
      US: _.get(society, 'social_part.US', 0)
    }
  }));
  if (effectiveDateForm instanceof moment) {
    effectiveDateForm = effectiveDateForm.format('YYYY-MM-DD');
  }

  const validation = validationSocietyPPTable(society_list);

  const {
    hasError,
    errors
  } = validation;

  if (hasError) {
    try {
      await dispatch(tableActions.setAllLinesErrors(tableName, errors));
      return validation;
    } catch (err) {
      return err;
    }
  } else {
    try {
      await dispatch(actions.postAssociateAttempt());
      const { data } = await windev.makeApiCall(`/pers_physique/${personId}/associate`, 'post', {}, { society_list });
      await dispatch(actions.postAssociateSuccess());
      await dispatch(getSocietyPP(personId));
      return data;
    } catch (err) {
      await dispatch(actions.postAssociateFail());
      await dispatch(tableActions.setErrors(tableName, 0, { updateError: _.get(err, 'response.data') }));
      await dispatch(handleError(err, actions.postAssociateFail));
      return err;
    }
  }
};
