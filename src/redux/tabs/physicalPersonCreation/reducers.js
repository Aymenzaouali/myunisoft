import {
  SELECT_TAB,

  GET_SIGNATORY_FUNCTION_ATTEMPT,
  GET_SIGNATORY_FUNCTION_SUCCESS,
  GET_SIGNATORY_FUNCTION_FAIL,

  GET_EMPLOYMENT_FUNCTION_ATTEMPT,
  GET_EMPLOYMENT_FUNCTION_SUCCESS,
  GET_EMPLOYMENT_FUNCTION_FAIL,

  GET_CIVILITY_ATTEMPT,
  GET_CIVILITY_FAIL,
  GET_CIVILITY_SUCCESS,

  GET_MARITAL_SITUATION_ATTEMPT,
  GET_MARITAL_SITUATION_FAIL,
  GET_MARITAL_SITUATION_SUCCESS,

  GET_MARITAL_REGIME_ATTEMPT,
  GET_MARITAL_REGIME_FAIL,
  GET_MARITAL_REGIME_SUCCESS,

  GET_PERS_PHYSIQUE_TYPE_ATTEMPT,
  GET_PERS_PHYSIQUE_TYPE_FAIL,
  GET_PERS_PHYSIQUE_TYPE_SUCCESS,

  GET_COORD_TYPE_ATTEMPT,
  GET_COORD_TYPE_FAIL,
  GET_COORD_TYPE_SUCCESS,

  POST_PERS_PHYSIQUE_ATTEMPT,
  POST_PERS_PHYSIQUE_FAIL,
  POST_PERS_PHYSIQUE_SUCCESS,

  GET_PERS_PHYSIQUE_ID_ATTEMPT,
  GET_PERS_PHYSIQUE_ID_FAIL,
  GET_PERS_PHYSIQUE_ID_SUCCESS,

  PUT_PERS_PHYSIQUE_ATTEMPT,
  PUT_PERS_PHYSIQUE_FAIL,
  PUT_PERS_PHYSIQUE_SUCCESS,

  RESET_SELECTED_PERS_PHYSIQUE,
  SELECT_PERS_PHYSIQUE,

  GET_FAMILY_LINK_ATTEMPT,
  GET_FAMILY_LINK_FAIL,
  GET_FAMILY_LINK_SUCCESS,

  POST_FAMILY_LINK_ATTEMPT,
  POST_FAMILY_LINK_FAIL,
  POST_FAMILY_LINK_SUCCESS,

  PUT_FAMILY_LINK_ATTEMPT,
  PUT_FAMILY_LINK_FAIL,
  PUT_FAMILY_LINK_SUCCESS,

  GET_LINK_TYPE_ATTEMPT,
  GET_LINK_TYPE_FAIL,
  GET_LINK_TYPE_SUCCESS,

  DEL_FAMILY_LINK_ATTEMPT,
  DEL_FAMILY_LINK_FAIL,
  DEL_FAMILY_LINK_SUCCESS,

  GET_DOCUMENT_ATTEMPT,
  GET_DOCUMENT_FAIL,
  GET_DOCUMENT_SUCCESS,

  POST_DOCUMENT_ATTEMPT,
  POST_DOCUMENT_FAIL,
  POST_DOCUMENT_SUCCESS,

  GET_PP_SOCIETY_ATTEMPT,
  GET_PP_SOCIETY_FAIL,
  GET_PP_SOCIETY_SUCCESS,

  DEL_PP_SOCIETY_ATTEMPT,
  DEL_PP_SOCIETY_FAIL,
  DEL_PP_SOCIETY_SUCCESS,

  GET_SOCIETY_ACCESS_ATTEMPT,
  GET_SOCIETY_ACCESS_FAIL,
  GET_SOCIETY_ACCESS_SUCCESS,

  PUT_SOCIETY_ACCESS_ATTEMPT,
  PUT_SOCIETY_ACCESS_FAIL,
  PUT_SOCIETY_ACCESS_SUCCESS,

  DEL_SOCIETY_ACCESS_ATTEMPT,
  DEL_SOCIETY_ACCESS_FAIL,
  DEL_SOCIETY_ACCESS_SUCCESS,

  GET_PP_EMPLOYEE_ATTEMPT,
  GET_PP_EMPLOYEE_FAIL,
  GET_PP_EMPLOYEE_SUCCESS
} from './constants';

const initialState = {
  selectedTab: 0,

  signatoryFunctionTypes: [],
  employmentFunctionTypes: [],
  civilityTypes: [],
  maritalSituationTypes: [],
  maritalRegimeTypes: [],
  physicalPersonTypes: [],
  coordTypes: [],
  selected_physical_person: null,
  linkTypes: [],

  familyLinkMembers: [],
  newlyCreatedPersonId: null,
  document: [],
  physicalPerson_society: [],

  user_access: [],
  employeeFirms: []
};

const physicalPersonCreation = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case SELECT_TAB:
      return {
        ...state,
        selectedTab: action.payload
      };
    case GET_SIGNATORY_FUNCTION_ATTEMPT:
      return {
        ...state
      };
    case GET_SIGNATORY_FUNCTION_FAIL:
      return {
        ...state
      };
    case GET_SIGNATORY_FUNCTION_SUCCESS:
      return {
        ...state,
        signatoryFunctionTypes: action.payload
      };
    case GET_EMPLOYMENT_FUNCTION_ATTEMPT:
      return {
        ...state
      };
    case GET_EMPLOYMENT_FUNCTION_FAIL:
      return {
        ...state
      };
    case GET_EMPLOYMENT_FUNCTION_SUCCESS:
      return {
        ...state,
        employmentFunctionTypes: action.payload
      };
    case GET_CIVILITY_ATTEMPT:
      return {
        ...state
      };
    case GET_CIVILITY_FAIL:
      return {
        ...state
      };
    case GET_CIVILITY_SUCCESS:
      return {
        ...state,
        civilityTypes: action.payload
      };
    case GET_MARITAL_SITUATION_ATTEMPT:
      return {
        ...state
      };
    case GET_MARITAL_SITUATION_FAIL:
      return {
        ...state
      };
    case GET_MARITAL_SITUATION_SUCCESS:
      return {
        ...state,
        maritalSituationTypes: action.payload
      };
    case GET_MARITAL_REGIME_ATTEMPT:
      return {
        ...state
      };
    case GET_MARITAL_REGIME_FAIL:
      return {
        ...state
      };
    case GET_MARITAL_REGIME_SUCCESS:
      return {
        ...state,
        maritalRegimeTypes: action.payload
      };
    case GET_PERS_PHYSIQUE_TYPE_ATTEMPT:
      return {
        ...state
      };
    case GET_PERS_PHYSIQUE_TYPE_FAIL:
      return {
        ...state
      };
    case GET_PERS_PHYSIQUE_TYPE_SUCCESS:
      return {
        ...state,
        physicalPersonTypes: action.payload
      };
    case GET_COORD_TYPE_ATTEMPT:
      return {
        ...state
      };
    case GET_COORD_TYPE_FAIL:
      return {
        ...state
      };
    case GET_COORD_TYPE_SUCCESS:
      return {
        ...state,
        coordTypes: action.payload
      };
    case POST_PERS_PHYSIQUE_ATTEMPT:
      return {
        ...state
      };
    case POST_PERS_PHYSIQUE_FAIL:
      return {
        ...state
      };
    case POST_PERS_PHYSIQUE_SUCCESS:
      return {
        ...state,
        newlyCreatedPersonId: action.payload.pers_physique_id
      };
    case GET_PERS_PHYSIQUE_ID_ATTEMPT:
      return {
        ...state
      };
    case GET_PERS_PHYSIQUE_ID_FAIL:
      return {
        ...state
      };
    case GET_PERS_PHYSIQUE_ID_SUCCESS:
      return {
        ...state,
        selected_physical_person: action.payload
      };
    case PUT_PERS_PHYSIQUE_ATTEMPT:
      return {
        ...state
      };
    case PUT_PERS_PHYSIQUE_FAIL:
      return {
        ...state
      };
    case PUT_PERS_PHYSIQUE_SUCCESS:
      return {
        ...state
      };
    case RESET_SELECTED_PERS_PHYSIQUE:
      return {
        ...state,
        selected_physical_person: null
      };
    case SELECT_PERS_PHYSIQUE:
      return {
        ...state,
        selected_physical_person: action.personPhysique
      };
    case GET_FAMILY_LINK_ATTEMPT:
      return {
        ...state
      };
    case GET_FAMILY_LINK_FAIL:
      return {
        ...state
      };
    case GET_FAMILY_LINK_SUCCESS:
      return {
        ...state,
        familyLinkMembers: action.payload
      };
    case POST_FAMILY_LINK_ATTEMPT:
      return {
        ...state
      };
    case POST_FAMILY_LINK_FAIL:
      return {
        ...state
      };
    case POST_FAMILY_LINK_SUCCESS:
      return {
        ...state
      };
    case PUT_FAMILY_LINK_ATTEMPT:
      return {
        ...state
      };
    case PUT_FAMILY_LINK_FAIL:
      return {
        ...state
      };
    case PUT_FAMILY_LINK_SUCCESS:
      return {
        ...state
      };
    case GET_LINK_TYPE_ATTEMPT:
      return {
        ...state
      };
    case GET_LINK_TYPE_FAIL:
      return {
        ...state
      };
    case GET_LINK_TYPE_SUCCESS:
      return {
        ...state,
        linkTypes: action.payload
      };
    case DEL_FAMILY_LINK_ATTEMPT:
      return {
        ...state
      };
    case DEL_FAMILY_LINK_SUCCESS:
      return {
        ...state
      };
    case DEL_FAMILY_LINK_FAIL:
      return {
        ...state
      };
    case GET_DOCUMENT_ATTEMPT:
      return {
        ...state
      };
    case GET_DOCUMENT_FAIL:
      return {
        ...state
      };
    case GET_DOCUMENT_SUCCESS:
      return {
        ...state,
        document: action.payload
      };
    case POST_DOCUMENT_ATTEMPT:
      return {
        ...state
      };
    case POST_DOCUMENT_FAIL:
      return {
        ...state
      };
    case POST_DOCUMENT_SUCCESS:
      return {
        ...state
      };
    case GET_PP_SOCIETY_ATTEMPT:
      return {
        ...state
      };
    case GET_PP_SOCIETY_FAIL:
      return {
        ...state
      };
    case GET_PP_SOCIETY_SUCCESS:
      return {
        ...state,
        physicalPerson_society: action.payload
      };
    case DEL_PP_SOCIETY_ATTEMPT:
      return {
        ...state
      };
    case DEL_PP_SOCIETY_FAIL:
      return {
        ...state
      };
    case DEL_PP_SOCIETY_SUCCESS:
      return {
        ...state
      };
    case PUT_SOCIETY_ACCESS_ATTEMPT:
      return {
        ...state
      };
    case PUT_SOCIETY_ACCESS_FAIL:
      return {
        ...state
      };
    case PUT_SOCIETY_ACCESS_SUCCESS:
      return {
        ...state
      };
    case DEL_SOCIETY_ACCESS_ATTEMPT:
      return {
        ...state
      };
    case DEL_SOCIETY_ACCESS_FAIL:
      return {
        ...state
      };
    case DEL_SOCIETY_ACCESS_SUCCESS:
      return {
        ...state
      };
    case GET_SOCIETY_ACCESS_ATTEMPT:
      return {
        ...state
      };
    case GET_SOCIETY_ACCESS_FAIL:
      return {
        ...state
      };
    case GET_SOCIETY_ACCESS_SUCCESS:
      return {
        ...state,
        user_access: action.payload.access_list
      };
    case GET_PP_EMPLOYEE_ATTEMPT:
      return {
        ...state
      };
    case GET_PP_EMPLOYEE_FAIL:
      return {
        ...state
      };
    case GET_PP_EMPLOYEE_SUCCESS:
      return {
        ...state,
        employeeFirms: action.payload
      };

    default: return state;
    }
  }
  return state;
};

export default physicalPersonCreation;
