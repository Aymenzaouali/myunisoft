import {
  SELECT_TAB,

  GET_SIGNATORY_FUNCTION_ATTEMPT,
  GET_SIGNATORY_FUNCTION_SUCCESS,
  GET_SIGNATORY_FUNCTION_FAIL,

  GET_EMPLOYMENT_FUNCTION_ATTEMPT,
  GET_EMPLOYMENT_FUNCTION_SUCCESS,
  GET_EMPLOYMENT_FUNCTION_FAIL,

  GET_CIVILITY_ATTEMPT,
  GET_CIVILITY_FAIL,
  GET_CIVILITY_SUCCESS,

  GET_MARITAL_SITUATION_ATTEMPT,
  GET_MARITAL_SITUATION_FAIL,
  GET_MARITAL_SITUATION_SUCCESS,

  GET_MARITAL_REGIME_ATTEMPT,
  GET_MARITAL_REGIME_FAIL,
  GET_MARITAL_REGIME_SUCCESS,

  GET_PERS_PHYSIQUE_TYPE_ATTEMPT,
  GET_PERS_PHYSIQUE_TYPE_FAIL,
  GET_PERS_PHYSIQUE_TYPE_SUCCESS,

  GET_COORD_TYPE_ATTEMPT,
  GET_COORD_TYPE_FAIL,
  GET_COORD_TYPE_SUCCESS,

  POST_PERS_PHYSIQUE_ATTEMPT,
  POST_PERS_PHYSIQUE_FAIL,
  POST_PERS_PHYSIQUE_SUCCESS,

  GET_PERS_PHYSIQUE_ID_ATTEMPT,
  GET_PERS_PHYSIQUE_ID_FAIL,
  GET_PERS_PHYSIQUE_ID_SUCCESS,

  PUT_PERS_PHYSIQUE_ATTEMPT,
  PUT_PERS_PHYSIQUE_FAIL,
  PUT_PERS_PHYSIQUE_SUCCESS,

  RESET_SELECTED_PERS_PHYSIQUE,
  SELECT_PERS_PHYSIQUE,

  GET_FAMILY_LINK_ATTEMPT,
  GET_FAMILY_LINK_FAIL,
  GET_FAMILY_LINK_SUCCESS,

  POST_FAMILY_LINK_ATTEMPT,
  POST_FAMILY_LINK_FAIL,
  POST_FAMILY_LINK_SUCCESS,

  PUT_FAMILY_LINK_ATTEMPT,
  PUT_FAMILY_LINK_FAIL,
  PUT_FAMILY_LINK_SUCCESS,

  GET_LINK_TYPE_ATTEMPT,
  GET_LINK_TYPE_FAIL,
  GET_LINK_TYPE_SUCCESS,

  DEL_FAMILY_LINK_ATTEMPT,
  DEL_FAMILY_LINK_FAIL,
  DEL_FAMILY_LINK_SUCCESS,

  GET_DOCUMENT_ATTEMPT,
  GET_DOCUMENT_FAIL,
  GET_DOCUMENT_SUCCESS,

  POST_DOCUMENT_ATTEMPT,
  POST_DOCUMENT_FAIL,
  POST_DOCUMENT_SUCCESS,

  DEL_DOCUMENT_ATTEMPT,
  DEL_DOCUMENT_FAIL,
  DEL_DOCUMENT_SUCCESS,

  GET_PP_SOCIETY_ATTEMPT,
  GET_PP_SOCIETY_FAIL,
  GET_PP_SOCIETY_SUCCESS,

  DEL_PP_SOCIETY_ATTEMPT,
  DEL_PP_SOCIETY_FAIL,
  DEL_PP_SOCIETY_SUCCESS,

  GET_SOCIETY_ACCESS_ATTEMPT,
  GET_SOCIETY_ACCESS_FAIL,
  GET_SOCIETY_ACCESS_SUCCESS,

  PUT_SOCIETY_ACCESS_ATTEMPT,
  PUT_SOCIETY_ACCESS_FAIL,
  PUT_SOCIETY_ACCESS_SUCCESS,

  DEL_SOCIETY_ACCESS_ATTEMPT,
  DEL_SOCIETY_ACCESS_FAIL,
  DEL_SOCIETY_ACCESS_SUCCESS,

  GET_PP_EMPLOYEE_ATTEMPT,
  GET_PP_EMPLOYEE_FAIL,
  GET_PP_EMPLOYEE_SUCCESS,

  POST_PP_EMPLOYEE_ATTEMPT,
  POST_PP_EMPLOYEE_FAIL,
  POST_PP_EMPLOYEE_SUCCESS,

  DEL_PP_EMPLOYEE_ATTEMPT,
  DEL_PP_EMPLOYEE_FAIL,
  DEL_PP_EMPLOYEE_SUCCESS,

  POST_PP_ASSOCIATE_ATTEMPT,
  POST_PP_ASSOCIATE_FAIL,
  POST_PP_ASSOCIATE_SUCCESS
} from './constants';

const selectTab = payload => ({
  type: SELECT_TAB,
  payload
});

const getSignatoryFunctionAttempt = () => ({
  type: GET_SIGNATORY_FUNCTION_ATTEMPT
});

const getSignatoryFunctionFail = () => ({
  type: GET_SIGNATORY_FUNCTION_FAIL
});

const getSignatoryFunctionSuccess = payload => ({
  type: GET_SIGNATORY_FUNCTION_SUCCESS,
  payload
});

const getEmploymentFunctionAttempt = () => ({
  type: GET_EMPLOYMENT_FUNCTION_ATTEMPT
});

const getEmploymentFunctionFail = () => ({
  type: GET_EMPLOYMENT_FUNCTION_FAIL
});

const getEmploymentFunctionSuccess = payload => ({
  type: GET_EMPLOYMENT_FUNCTION_SUCCESS,
  payload
});

const getCivilityAttempt = () => ({
  type: GET_CIVILITY_ATTEMPT
});

const getCivilityFail = () => ({
  type: GET_CIVILITY_FAIL
});

const getCivilitySuccess = payload => ({
  type: GET_CIVILITY_SUCCESS,
  payload
});

const getMaritalSituationAttempt = () => ({
  type: GET_MARITAL_SITUATION_ATTEMPT
});

const getMaritalSituationFail = () => ({
  type: GET_MARITAL_SITUATION_FAIL
});

const getMaritalSituationSuccess = payload => ({
  type: GET_MARITAL_SITUATION_SUCCESS,
  payload
});

const getMaritalRegimeAttempt = () => ({
  type: GET_MARITAL_REGIME_ATTEMPT
});

const getMaritalRegimeFail = () => ({
  type: GET_MARITAL_REGIME_FAIL
});

const getMaritalRegimeSuccess = payload => ({
  type: GET_MARITAL_REGIME_SUCCESS,
  payload
});

const getPersPhysiqueTypeAttempt = () => ({
  type: GET_PERS_PHYSIQUE_TYPE_ATTEMPT
});

const getPersPhysiqueTypeFail = () => ({
  type: GET_PERS_PHYSIQUE_TYPE_FAIL
});

const getPersPhysiqueTypeSuccess = payload => ({
  type: GET_PERS_PHYSIQUE_TYPE_SUCCESS,
  payload
});

const getCoordTypeAttempt = () => ({
  type: GET_COORD_TYPE_ATTEMPT
});

const getCoordTypeFail = () => ({
  type: GET_COORD_TYPE_FAIL
});

const getCoordTypeSuccess = payload => ({
  type: GET_COORD_TYPE_SUCCESS,
  payload
});

const postPersPhysiqueAttempt = () => ({
  type: POST_PERS_PHYSIQUE_ATTEMPT
});

const postPersPhysiqueFail = () => ({
  type: POST_PERS_PHYSIQUE_FAIL
});

const postPersPhysiqueSuccess = payload => ({
  type: POST_PERS_PHYSIQUE_SUCCESS,
  payload
});

const getPersPhysiqueByIdAttempt = () => ({
  type: GET_PERS_PHYSIQUE_ID_ATTEMPT
});

const getPersPhysiqueByIdFail = () => ({
  type: GET_PERS_PHYSIQUE_ID_FAIL
});

const getPersPhysiqueByIdSuccess = payload => ({
  type: GET_PERS_PHYSIQUE_ID_SUCCESS,
  payload
});

const putPersPhysiqueAttempt = () => ({
  type: PUT_PERS_PHYSIQUE_ATTEMPT
});

const putPersPhysiqueFail = () => ({
  type: PUT_PERS_PHYSIQUE_FAIL
});

const putPersPhysiqueSuccess = () => ({
  type: PUT_PERS_PHYSIQUE_SUCCESS
});

const resetSelectedPersPhysique = () => ({
  type: RESET_SELECTED_PERS_PHYSIQUE
});

const selectPhysicalPerson = personPhysique => ({
  type: SELECT_PERS_PHYSIQUE,
  personPhysique
});

const getFamilyLinkAttempt = () => ({
  type: GET_FAMILY_LINK_ATTEMPT
});

const getFamilyLinkFail = () => ({
  type: GET_FAMILY_LINK_FAIL
});

const getFamilyLinkSuccess = payload => ({
  type: GET_FAMILY_LINK_SUCCESS,
  payload
});

const postFamilyLinkAttempt = () => ({
  type: POST_FAMILY_LINK_ATTEMPT
});

const postFamilyLinkFail = () => ({
  type: POST_FAMILY_LINK_FAIL
});

const postFamilyLinkSuccess = () => ({
  type: POST_FAMILY_LINK_SUCCESS
});

const putFamilyLinkAttempt = () => ({
  type: PUT_FAMILY_LINK_ATTEMPT
});

const putFamilyLinkFail = () => ({
  type: PUT_FAMILY_LINK_FAIL
});

const putFamilyLinkSuccess = () => ({
  type: PUT_FAMILY_LINK_SUCCESS
});

const getLinkTypeAttempt = () => ({
  type: GET_LINK_TYPE_ATTEMPT
});

const getLinkTypeFail = () => ({
  type: GET_LINK_TYPE_FAIL
});

const getLinkTypeSuccess = payload => ({
  type: GET_LINK_TYPE_SUCCESS,
  payload
});

const delFamilyLinkAttempt = () => ({
  type: DEL_FAMILY_LINK_ATTEMPT
});

const delFamilyLinkFail = () => ({
  type: DEL_FAMILY_LINK_FAIL
});

const delFamilyLinkSuccess = () => ({
  type: DEL_FAMILY_LINK_SUCCESS
});

const getDocumentAttempt = () => ({
  type: GET_DOCUMENT_ATTEMPT
});

const getDocumentFail = () => ({
  type: GET_DOCUMENT_FAIL
});

const getDocumentSuccess = payload => ({
  type: GET_DOCUMENT_SUCCESS,
  payload
});

const postDocumentAttempt = () => ({
  type: POST_DOCUMENT_ATTEMPT
});

const postDocumentFail = () => ({
  type: POST_DOCUMENT_FAIL
});

const postDocumentSuccess = payload => ({
  type: POST_DOCUMENT_SUCCESS,
  payload
});

const delDocumentAttempt = () => ({
  type: DEL_DOCUMENT_ATTEMPT
});

const delDocumentFail = () => ({
  type: DEL_DOCUMENT_FAIL
});

const delDocumentSuccess = () => ({
  type: DEL_DOCUMENT_SUCCESS
});

const getSocietyAttempt = () => ({
  type: GET_PP_SOCIETY_ATTEMPT
});

const getSocietyFail = () => ({
  type: GET_PP_SOCIETY_FAIL
});

const getSocietySuccess = payload => ({
  type: GET_PP_SOCIETY_SUCCESS,
  payload
});

const delSocietyAttempt = () => ({
  type: DEL_PP_SOCIETY_ATTEMPT
});

const delSocietyFail = () => ({
  type: DEL_PP_SOCIETY_FAIL
});

const delSocietySuccess = () => ({
  type: DEL_PP_SOCIETY_SUCCESS
});

const putSocietyAttempt = () => ({
  type: PUT_SOCIETY_ACCESS_ATTEMPT
});

const putSocietyFail = () => ({
  type: PUT_SOCIETY_ACCESS_FAIL
});

const putSocietySuccess = () => ({
  type: PUT_SOCIETY_ACCESS_SUCCESS
});

const delSocietyAccessAttempt = () => ({
  type: DEL_SOCIETY_ACCESS_ATTEMPT
});

const delSocietyAccessFail = () => ({
  type: DEL_SOCIETY_ACCESS_FAIL
});

const delSocietyAccessSuccess = () => ({
  type: DEL_SOCIETY_ACCESS_SUCCESS
});

const getSocietyAccessAttempt = () => ({
  type: GET_SOCIETY_ACCESS_ATTEMPT
});

const getSocietyAccessFail = () => ({
  type: GET_SOCIETY_ACCESS_FAIL
});

const getSocietyAccessSuccess = payload => ({
  type: GET_SOCIETY_ACCESS_SUCCESS,
  payload
});

const getEmployeeAttempt = () => ({
  type: GET_PP_EMPLOYEE_ATTEMPT
});

const getEmployeeFail = () => ({
  type: GET_PP_EMPLOYEE_FAIL
});

const getEmployeeSuccess = payload => ({
  type: GET_PP_EMPLOYEE_SUCCESS,
  payload
});

const postEmployeeAttempt = () => ({
  type: POST_PP_EMPLOYEE_ATTEMPT
});

const postEmployeeFail = () => ({
  type: POST_PP_EMPLOYEE_FAIL
});

const postEmployeeSuccess = () => ({
  type: POST_PP_EMPLOYEE_SUCCESS
});

const delEmployeeAttempt = () => ({
  type: DEL_PP_EMPLOYEE_ATTEMPT
});

const delEmployeeFail = () => ({
  type: DEL_PP_EMPLOYEE_FAIL
});

const delEmployeeSuccess = () => ({
  type: DEL_PP_EMPLOYEE_SUCCESS
});

const postAssociateAttempt = () => ({
  type: POST_PP_ASSOCIATE_ATTEMPT
});

const postAssociateFail = () => ({
  type: POST_PP_ASSOCIATE_FAIL
});

const postAssociateSuccess = payload => ({
  type: POST_PP_ASSOCIATE_SUCCESS,
  payload
});

export default {
  selectTab,

  getSignatoryFunctionAttempt,
  getSignatoryFunctionFail,
  getSignatoryFunctionSuccess,

  getEmploymentFunctionAttempt,
  getEmploymentFunctionFail,
  getEmploymentFunctionSuccess,

  getCivilityAttempt,
  getCivilityFail,
  getCivilitySuccess,

  getMaritalSituationAttempt,
  getMaritalSituationFail,
  getMaritalSituationSuccess,

  getMaritalRegimeAttempt,
  getMaritalRegimeFail,
  getMaritalRegimeSuccess,

  getPersPhysiqueTypeAttempt,
  getPersPhysiqueTypeFail,
  getPersPhysiqueTypeSuccess,

  getCoordTypeAttempt,
  getCoordTypeFail,
  getCoordTypeSuccess,

  postPersPhysiqueAttempt,
  postPersPhysiqueFail,
  postPersPhysiqueSuccess,

  getPersPhysiqueByIdAttempt,
  getPersPhysiqueByIdFail,
  getPersPhysiqueByIdSuccess,

  putPersPhysiqueAttempt,
  putPersPhysiqueFail,
  putPersPhysiqueSuccess,

  resetSelectedPersPhysique,
  selectPhysicalPerson,

  getFamilyLinkAttempt,
  getFamilyLinkFail,
  getFamilyLinkSuccess,

  postFamilyLinkAttempt,
  postFamilyLinkFail,
  postFamilyLinkSuccess,

  putFamilyLinkAttempt,
  putFamilyLinkFail,
  putFamilyLinkSuccess,

  getLinkTypeAttempt,
  getLinkTypeFail,
  getLinkTypeSuccess,

  delFamilyLinkAttempt,
  delFamilyLinkFail,
  delFamilyLinkSuccess,

  getDocumentAttempt,
  getDocumentFail,
  getDocumentSuccess,

  postDocumentAttempt,
  postDocumentFail,
  postDocumentSuccess,

  delDocumentAttempt,
  delDocumentFail,
  delDocumentSuccess,

  getSocietyAttempt,
  getSocietyFail,
  getSocietySuccess,

  delSocietyAttempt,
  delSocietyFail,
  delSocietySuccess,

  putSocietyAttempt,
  putSocietyFail,
  putSocietySuccess,

  delSocietyAccessAttempt,
  delSocietyAccessFail,
  delSocietyAccessSuccess,

  getSocietyAccessAttempt,
  getSocietyAccessFail,
  getSocietyAccessSuccess,

  getEmployeeAttempt,
  getEmployeeFail,
  getEmployeeSuccess,

  postEmployeeAttempt,
  postEmployeeFail,
  postEmployeeSuccess,

  delEmployeeAttempt,
  delEmployeeFail,
  delEmployeeSuccess,

  postAssociateAttempt,
  postAssociateFail,
  postAssociateSuccess
};
