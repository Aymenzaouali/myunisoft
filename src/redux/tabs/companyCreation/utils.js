export const defaultExercises = [
  {
    result: '',
    ca: '',
    duration: '',
    start_date: '',
    end_date: '',
    closed: false,
    label: 'N+1'
  },
  {
    result: '',
    ca: '',
    duration: '',
    start_date: '',
    end_date: '',
    closed: false,
    label: 'N'
  },
  {
    result: '',
    ca: '',
    duration: '',
    start_date: '',
    end_date: '',
    closed: true,
    label: 'N-1'
  },

  {
    result: '',
    ca: '',
    duration: '',
    start_date: '',
    end_date: '',
    closed: true,
    label: 'N-2'
  },
  {
    result: '',
    ca: '',
    duration: '',
    start_date: '',
    end_date: '',
    closed: true,
    label: 'N-3'
  },
  {
    result: '',
    ca: '',
    duration: '',
    start_date: '',
    end_date: '',
    closed: true,
    label: 'N-4'
  }
];
