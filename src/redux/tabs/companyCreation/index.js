import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';
import _ from 'lodash';
import { getFormValues, reset } from 'redux-form';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import moment from 'moment';
import { formatFiscalFolder } from 'helpers/format';
import { validationSocietyTable, validationPersonTable, validationSubsidaryTable } from 'helpers/validationTable';
import { normalizeData } from 'helpers/normalizeData';
import { getCurrentTabState, tabFormName } from 'helpers/tabs';
import { selectCompanyAction } from 'redux/tabs/companyList/actions';
import { quitCurrentForm, setCurrentForm } from 'redux/tabs/form/actions';
import tableActions from 'redux/tables/actions';
import {
  PHYSICAL_PERSON_TABLE_NAME,
  LEGAL_PERSON_TABLE_NAME,
  SUBSIDIARY_TABLE_NAME,
  getTableName
} from 'assets/constants/tableName';
import { getCompany } from 'redux/tabs/companyList';
import actions from './actions';
import { defaultExercises } from './utils';

export const selectTab = payload => async (dispatch) => {
  try {
    await dispatch(actions.selectTabAction(payload));
    return payload;
  } catch (err) {
    return err;
  }
};

export const getClientUser = payload => async (dispatch) => {
  const {
    societyId: society_id,
    sort: sortOpts
  } = payload;

  const sort = sortOpts;

  const limit = 1000000;

  const page = 0;

  const offset = 0;

  const params = {
    society_id,
    offset,
    limit,
    sort
  };

  try {
    await dispatch(actions.getClientUserAttempt());
    const { data } = await windev.makeApiCall('/society/users', 'get', params);
    await dispatch(actions.getClientUserSuccess({
      data,
      limit,
      page,
      sort: sort || {}
    }));
    return data;
  } catch (err) {
    await dispatch(actions.getClientUserFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getCities = payload => async (dispatch) => {
  try {
    await dispatch(actions.getCitiesAttempt());
    const { data } = await windev.makeApiCall('/cedex', 'get', { postal_code: payload });
    await dispatch(actions.getCitiesSuccess(data || []));
    return payload;
  } catch (err) {
    await dispatch(actions.getCitiesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getUserTypeOptions = type => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id');
  try {
    await dispatch(actions.getUserTypeAttempt());
    const { data } = await windev.makeApiCall('/user/member', 'get', { search_code: type, society_id }, {});
    await dispatch(actions.getUserTypeSuccess(data || [], type));
    return data;
  } catch (err) {
    await dispatch(actions.getUserTypeFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getAccountants = () => async (dispatch) => {
  try {
    await dispatch(actions.getAccountantsAttempt());
    const { data } = await windev.makeApiCall('/accountant', 'get');
    await dispatch(actions.getAccountantsSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getAccountantsFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getRoadTypes = () => async (dispatch) => {
  try {
    await dispatch(actions.getRoadTypesAttempt());
    const { data } = await windev.makeApiCall('/road_type', 'get');
    await dispatch(actions.getRoadTypesSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getRoadTypesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getApe = code => async (dispatch) => {
  try {
    await dispatch(actions.getApeAttempt());

    const { data } = await windev.makeApiCall('/society/ape', 'get', { code });
    await dispatch(actions.getApeSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getApeFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getLegalForms = () => async (dispatch) => {
  try {
    await dispatch(actions.getLegalFormsAttempt());

    const { data } = await windev.makeApiCall('/society/legal_form', 'get');
    await dispatch(actions.getLegalFormsSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getLegalFormsFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getMemberCompanies = () => async (dispatch) => {
  try {
    await dispatch(actions.getMemberCompaniesAttempt());
    const { data } = await windev.makeApiCall('/member', 'get');
    await dispatch(actions.getMemberCompaniesSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getMemberCompaniesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getExercises = society_id => async (dispatch, getState) => {
  const state = getState();
  const formName = tabFormName('companyCreationForm', state.navigation.id);
  try {
    await dispatch(actions.getExercisesAttempt());
    const { data } = await windev.makeApiCall('/society/exercice', 'get', { society_id });
    let exercices = defaultExercises;
    if (!_.isEmpty(data)) {
      exercices = exercices.map((defaultExercice) => {
        const savedExercice = data.find(exercice => exercice.label === defaultExercice.label);
        if (savedExercice) {
          return {
            ...savedExercice,
            start_date: moment(savedExercice.start_date).format('YYYY-MM-DD'),
            end_date: moment(savedExercice.end_date).format('YYYY-MM-DD')
          };
        }
        return defaultExercice;
      });
    }
    await dispatch(actions.getExercisesSuccess(exercices));
    await dispatch(autoFillFormValues(formName, { exercises: exercices }));

    return exercices;
  } catch (err) {
    await dispatch(actions.getExercisesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const updateExercises = () => async (dispatch, getState) => {
  const state = getState();
  const formName = tabFormName('companyCreationForm', state.navigation.id);

  try {
    await dispatch(actions.updateExercicesAttempt());
    const companyCreationForm = getFormValues(formName)(state);
    const selectedCompany = _.get(getCurrentTabState(state), 'companyList.selectedCompany');
    const { society_id } = selectedCompany;
    const toCreate = [];
    const toUpdate = [];
    const exercices = companyCreationForm.exercises;
    if (exercices) {
      exercices.forEach((exercice) => {
        if (exercice.exercice_id) {
          toUpdate.push({
            exercice_id: exercice.exercice_id,
            start_date: moment(exercice.start_date, 'YYYYMMDD').format('YYYY-MM-DD'),
            end_date: moment(exercice.end_date, 'YYYYMMDD').format('YYYY-MM-DD'),
            label: exercice.label
          });
        } else if (!_.isEmpty(exercice.start_date) && !_.isEmpty(exercice.end_date)) {
          toCreate.push({
            start_date: moment(exercice.start_date, 'YYYYMMDD').format('YYYY-MM-DD'),
            end_date: moment(exercice.end_date, 'YYYYMMDD').format('YYYY-MM-DD'),
            label: exercice.label
          });
        }
      });
    }
    let created = {};
    let updated = {};
    if (toCreate.length > 0) {
      created = await windev.makeApiCall(`/society/${society_id}/exercice`, 'post', {}, { exercice: toCreate });
    }
    if (toUpdate.length > 0) {
      updated = await windev.makeApiCall(`/society/${society_id}/exercice`, 'put', {}, { exercice: toUpdate });
    }
    const receivedExercices = [..._.get(created, 'data', []), ..._.get(updated, 'data', [])];
    await dispatch(getExercises(society_id));
    return receivedExercices;
  } catch (err) {
    await dispatch(actions.updateExercicesFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const getBilan = () => async (dispatch) => {
  try {
    await dispatch(actions.getBilanAttempt());
    const { data } = await windev.makeApiCall('/society/bilan', 'get');
    await dispatch(actions.getBilanSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getBilanFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getTaxes = () => async (dispatch) => {
  try {
    await dispatch(actions.getTaxesAttempt());
    const { data } = await windev.makeApiCall('/society/regime_impot', 'get');
    await dispatch(actions.getTaxesSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getTaxesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getTaxationRegime = () => async (dispatch) => {
  try {
    await dispatch(actions.getTaxationRegimeAttempt());
    const { data } = await windev.makeApiCall('/sheet_group', 'get');
    await dispatch(actions.getTaxationRegimeSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getTaxationRegimeFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getRegistrationPlaces = name => async (dispatch) => {
  try {
    await dispatch(actions.getRegistrationPlacesAttempt());
    const { data } = await windev.makeApiCall('/society/register', 'get', { name });
    await dispatch(actions.getRegistrationPlacesSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getRegistrationPlacesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getFiscalFolder = society_id => async (dispatch, getState) => {
  const state = getState();
  try {
    await dispatch(actions.getFiscalFolderAttempt());
    const { data } = await windev.makeApiCall(`/society/${society_id}/fiscal_file`, 'get');
    await dispatch(actions.getFiscalFolderSuccess(data));
    const fiscal_folder = await formatFiscalFolder(data);
    const formName = tabFormName('companyCreationForm', state.navigation.id);
    await dispatch(autoFillFormValues(formName, { fiscal_folder }));
    return data;
  } catch (err) {
    await dispatch(actions.getAccountantsFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getDataBySiren = siren => async (dispatch) => {
  try {
    await dispatch(actions.getDataBySirenAttempt());
    const { data } = await windev.makeApiCall('/society/data_gouv_fields', 'get', { siren });
    await dispatch(actions.getDataBySirenSuccess());
    return data;
  } catch (err) {
    await dispatch(actions.getDataBySirenFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const createCompany = values => async (dispatch, getState) => {
  try {
    await dispatch(actions.createCompanyAttempt());

    const state = getState();
    const formName = tabFormName('companyCreationForm', state.navigation.id);

    const { society_id } = values;
    const {
      owner_company,
      name,
      address_number,
      address_bis,
      road_type,
      street_name,
      complement,
      postal_code,
      city,
      register,
      country,
      legal_form,
      accounting_expert,
      siret,
      ape,
      activity,
      password,
      comment,
      registration_date
    } = _.get(state, ['form', formName, 'values'], {});

    const {
      expert,
      manager,
      collab
    } = _.get(state, ['form', 'clientUserForm', 'values'], {});

    const body = {
      member_id: _.get(owner_company, 'id'),
      id_accountant: expert?.value,
      id_rm: manager?.value,
      id_collab: collab?.value,
      name,
      address_number,
      road_type_id: _.get(road_type, 'id'),
      street_name,
      complement,
      address_bis,
      postal_code,
      city: _.get(city, 'value'),
      register_id: _.get(register, 'id'),
      country,
      legal_form_id: _.get(legal_form, 'id', null),
      accountant_id: _.get(accounting_expert, 'id'),
      siret: siret === undefined ? null : siret,
      ape_id: _.get(ape, 'id', null),
      activity,
      password,
      comment,
      registration_date
    };

    const { data: rawData } = await windev.makeApiCall(`/society${society_id ? `/${society_id}` : ''}`, society_id ? 'put' : 'post', {}, body);
    /**
     * Response normalization should be handled by the BE
     * during after V0
     */
    const data = normalizeData(rawData);
    await dispatch(quitCurrentForm());
    if (!society_id) {
      await dispatch(selectCompanyAction(data));
      await dispatch(getFiscalFolder(data.society_id));
    }
    return data;
  } catch (err) {
    await dispatch(actions.createCompanyFail());
    await dispatch(handleError(err));
    throw err;
  }
};

export const getPersonnePhysiqueAllList = (
  payload = {},
  tableName = ''
) => async (dispatch, getState) => {
  const state = getState();
  const dataTable = _.get(state, `tables.${tableName}.data`, []);
  const {
    limit = 1000
  } = payload;
  try {
    await dispatch(actions.getListPhysicalPersonAttempt());
    const params = {
      limit
    };
    const { data: physicalPersonsData } = await windev.makeApiCall('/pers_physique', 'get', params);
    await dispatch(actions.getListPhysicalPersonSuccess({
      physicalPersonsData,
      limit,
      dataTable
    }));
    return physicalPersonsData;
  } catch (err) {
    await dispatch(actions.getListPhysicalPersonFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getPersonneMoraleAllList = (
  societyId,
  payload = {},
  tableName = ''
) => async (dispatch, getState) => {
  const state = getState();
  const dataTable = _.get(state, `tables.${tableName}.data`, []);
  const {
    limit = 1000
  } = payload;
  try {
    await dispatch(actions.getListMoralPersonAttempt());
    const params = {
      limit
    };
    const { data: companiesData } = await windev.makeApiCall('/society', 'get', params);
    await dispatch(actions.getListMoralPersonSuccess({
      societyId,
      companiesData,
      limit,
      dataTable
    }));
    return companiesData;
  } catch (err) {
    await dispatch(actions.getListMoralPersonFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getAssociatesList = () => async (dispatch, getState) => {
  const state = getState();
  const navigation_society_id = state.navigation.id;
  const society_id = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null);
  try {
    await dispatch(actions.getAssociatesListAttempt());
    const { data } = await windev.makeApiCall(`/society/${society_id}/associate`, 'get', {});
    await dispatch(tableActions.setData(getTableName(navigation_society_id, PHYSICAL_PERSON_TABLE_NAME), (_.get(data, 'associate_list.physical_person_list') || []).map(pp => ({ ...pp.physical_person, ...pp }))));
    await dispatch(tableActions.setData(getTableName(navigation_society_id, LEGAL_PERSON_TABLE_NAME), (_.get(data, 'associate_list.society_list') || []).map(lp => ({ id: _.get(lp, 'society.id'), ...lp }))));
    const associates = {
      capital: _.get(data, 'capital.capital', null),
      social_part: _.get(data, 'capital.social_part', null),
      social_part_value: _.get(data, 'capital.social_part_value', null),
      effective_date: _.get(data, 'capital.date', null),
      filterPerson: 'a',
      filterSociety: 'a'
    };

    const formName = tabFormName('companyCreationForm', state.navigation.id);
    await dispatch(autoFillFormValues(formName, { associates }));
    await dispatch(actions.getAssociatesListSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getAssociatesListFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const putTaxRecord = () => async (dispatch, getState) => {
  const state = getState();
  const formName = tabFormName('companyCreationForm', state.navigation.id);

  const companyCreationForm = getFormValues(formName)(state);
  const society_id = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null);
  const fiscal_folder_form_all = _.get(companyCreationForm, 'fiscal_folder', {});
  const close_entries_VAT = _.get(fiscal_folder_form_all, 'other.close_entries_VAT', false);
  const society_status_form = _.get(fiscal_folder_form_all, 'society_status', {});
  const societyStatus = _.get(getCurrentTabState(state), 'companyCreation.fiscalFolder.society_status', {});
  const other_form = _.get(fiscal_folder_form_all, 'other', {});
  const idSocietyStatusForm = _.get(society_status_form, 'id', null);
  const idSocietyStatus = _.get(societyStatus, 'id', null);
  let effectiveDateForm = _.get(society_status_form, 'effective_date', '');
  const effectiveDate = _.get(societyStatus, 'effective_date', '');

  const info_bnc_form = _.get(fiscal_folder_form_all, 'info_bnc', {});
  const info_bnc_id = _.get(getCurrentTabState(state), 'companyCreation.fiscalFolder.info_bnc.info_bnc_id', {});
  const {
    comptability_type,
    comptability_held,
    result_rule
  } = info_bnc_form;

  /**
   * TODO add accounting_entries to post request and get task 5168 
   */
  const {
    adherent_code,
    due_date_tva,
    rof_cfe,
    rof_tdfc,
    rof_tva,
    tva_intraco,
    sheet_group,
    vat_regime,
    gestion_center,
    edi,
    mono_etab
  } = other_form;
  const regex = 'BNC';
  const BNC = sheet_group && sheet_group.value.indexOf(regex) !== -1;
  const info_bnc = {
    activity_code_pm: BNC ? _.get(info_bnc_form, 'activity_code_pm', null) : null,
    info_bnc_id: info_bnc_id || null,
    comptability_type_id: BNC ? _.get(comptability_type, 'id', null) : null,
    comptability_held_id: BNC ? _.get(comptability_held, 'id', null) : null,
    result_rule_id: BNC ? _.get(result_rule, 'id', null) : null,
    membership_year: BNC ? _.get(info_bnc_form, 'membership_year', null) : null
  };
  const other = {
    adherent_code,
    due_date_tva,
    rof_cfe,
    rof_tdfc,
    rof_tva,
    tva_intraco,
    mono_etab,
    close_entries_VAT,
    account_edi_id: edi && edi.id_compte_edi,
    id_sheet_group: sheet_group && sheet_group.id,
    vat_regime_id: vat_regime && vat_regime.id,
    gestion_center_id: gestion_center && gestion_center.id
  };

  if (effectiveDateForm instanceof moment) {
    effectiveDateForm = effectiveDateForm.format('YYYY-MM-DD');
  }

  let params;

  if (idSocietyStatusForm !== idSocietyStatus || effectiveDateForm !== effectiveDate) {
    const effectiveDate = typeof society_status_form.effective_date === 'string' ? society_status_form.effective_date : society_status_form.effective_date.format('YYYY-MM-DD');

    params = {
      ...other,
      info_bnc,
      society_status: {
        id: society_status_form && idSocietyStatusForm,
        effective_date: society_status_form && society_status_form.effective_date && effectiveDate
      }
    };
  } else if (idSocietyStatusForm === idSocietyStatus && effectiveDateForm === effectiveDate) {
    params = {
      ...other,
      info_bnc
    };
  }

  try {
    await dispatch(actions.updateTaxRecordAttempt());
    const { data } = await windev.makeApiCall(`/society/${society_id}/fiscal_file`, 'post', {}, params);
    await dispatch(actions.updateTaxRecordSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.updateTaxRecordFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getSocietyStatus = () => async (dispatch) => {
  try {
    await dispatch(actions.getSocietyStatusAttempt());
    const { data } = await windev.makeApiCall('/society/status', 'get');
    await dispatch(actions.getSocietyStatusSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getSocietyStatusFail());
    await dispatch(handleError(err, actions.getSocietyStatusFail));
    return err;
  }
};

export const getVatRegime = () => async (dispatch) => {
  try {
    await dispatch(actions.getVatRegimeAttempt());
    const { data } = await windev.makeApiCall('/society/vat_regime', 'get');
    await dispatch(actions.getVatRegimeSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getVatRegimeFail());
    await dispatch(handleError(err, actions.getVatRegimeFail));
    return err;
  }
};

export const getEDI = society_id => async (dispatch) => {
  const params = { society_id };
  try {
    await dispatch(actions.getEDIAttempt());
    const { data } = await windev.makeApiCall('/society/compte_edi', 'get', params);
    await dispatch(actions.getEDISuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getEDIFail());
    await dispatch(handleError(err, actions.getVatRegimeFail));
    return err;
  }
};

export const getRules = () => async (dispatch) => {
  try {
    await dispatch(actions.getRulesAttempt());
    const { data } = await windev.makeApiCall('/society/result_rule', 'get');
    await dispatch(actions.getRulesSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getRulesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getGestionCenter = () => async (dispatch) => {
  try {
    await dispatch(actions.getGestionCenterAttempt());
    const { data } = await windev.makeApiCall('/society/gestion_center', 'get');
    await dispatch(actions.getGestionCenterSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getGestionCenterFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getAccountingOutfit = () => async (dispatch) => {
  try {
    await dispatch(actions.getAccountingOutfitAttempt());
    const { data } = await windev.makeApiCall('/society/comptability_held', 'get');
    await dispatch(actions.getAccountingOutfitSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getAccountingOutfitFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getAccountingType = () => async (dispatch) => {
  try {
    await dispatch(actions.getAccountingTypeAttempt());
    const { data } = await windev.makeApiCall('/society/comptability_type', 'get');
    await dispatch(actions.getAccountingTypeSuccess(data || []));
    return data;
  } catch (err) {
    await dispatch(actions.getAccountingTypeFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getSubsidiariesList = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null);
  const navigation_society_id = state.navigation.id;
  try {
    await dispatch(actions.getSubsidiariesAttempt());
    const { data } = await windev.makeApiCall(`/society/${society_id}/filiale_associate`, 'get', {});
    await dispatch(tableActions.setData(getTableName(navigation_society_id, SUBSIDIARY_TABLE_NAME), (_.get(data, 'filiale_associate_list') || []).map(sub => ({ id: _.get(sub, 'society.id'), ...sub }))));
    await dispatch(actions.getSubsidiariesSuccess(data));
    const subsidiaries = {
      filterSociety: 'a'
    };
    const formName = tabFormName('companyCreationForm', navigation_society_id);
    await dispatch(autoFillFormValues(formName, { subsidiaries }));
    return data;
  } catch (err) {
    await dispatch(actions.getSubsidiariesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const postAssociates = (type, tableName = '') => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null);
  const tabsValueCapital = _.get(getCurrentTabState(state), 'companyCreation.associates.capital');
  const currentCapital = {
    effective_date: _.get(tabsValueCapital, 'date'),
    capital: _.get(tabsValueCapital, 'capital'),
    social_part: _.get(tabsValueCapital, 'social_part')
  };

  const editedRows = _.get(state, `tables.${tableName}.editedRows`);
  const createdRows = _.get(state, `tables.${tableName}.createdRows`);

  const formName = tabFormName('companyCreationForm', state.navigation.id);
  const companyCreationForm = getFormValues(formName)(state);

  const associates_form = _.get(companyCreationForm, 'associates', {});

  let effectiveDateForm = _.get(associates_form, 'effective_date', '');

  const physical_person_list = Object.values({ ...createdRows, ...editedRows }).map(person => ({
    physical_person_id: person.id,
    signatory_function_id: _.get(person, 'signatory_function.id') || _.get(person, 'signatory_function.value.id', null),
    function_id: _.get(person, 'function.id') || _.get(person, 'function.value.id', null),
    effective_date: person.start_date,
    end_date: _.get(person, 'end_date', ''),
    social_part: person.social_part && {
      NP: _.get(person, 'social_part.NP', 0),
      PP: _.get(person, 'social_part.PP', 0),
      US: _.get(person, 'social_part.US', 0)
    }
  }));
  const society_list = Object.values({ ...createdRows, ...editedRows }).map(society => ({
    society_id: society.id,
    signatory_function_id: _.get(society, 'signatory_function.id') || _.get(society, 'signatory_function.value.id', null),
    effective_date: society.start_date,
    end_date: _.get(society, 'end_date', ''),
    social_part: society.social_part && {
      NP: _.get(society, 'social_part.NP', 0),
      PP: _.get(society, 'social_part.PP', 0),
      US: _.get(society, 'social_part.US', 0)
    }
  }));

  if (effectiveDateForm instanceof moment) {
    effectiveDateForm = effectiveDateForm.format('YYYY-MM-DD');
  }

  const capital = {
    effective_date: effectiveDateForm,
    capital: _.get(associates_form, 'capital', null),
    social_part: _.get(associates_form, 'social_part', null)
  };

  const modifCapital = (effectiveDateForm !== null && !_.isEqual(capital, currentCapital));

  let params;
  let validation = {};

  if (type === 'person') {
    validation = validationPersonTable(physical_person_list);
    if (modifCapital) {
      params = {
        capital,
        physical_person_list
      };
    } else {
      params = {
        physical_person_list
      };
    }
  } else if (type === 'society') {
    validation = validationSocietyTable(society_list);
    if (modifCapital) {
      params = {
        capital,
        society_list
      };
    } else {
      params = {
        society_list
      };
    }
  } else if (modifCapital) {
    params = {
      capital
    };
  } else {
    params = {
    };
  }

  const {
    hasError,
    errors
  } = validation;

  if (hasError) {
    try {
      await dispatch(tableActions.setAllLinesErrors(tableName, errors));
      return validation;
    } catch (err) {
      return err;
    }
  } else {
    try {
      await dispatch(actions.updateAssociatesAttempt());
      const { data } = await windev.makeApiCall(`/society/${society_id}/associate`, 'post', {}, params);
      await dispatch(actions.updateAssociatesSuccess());
      await dispatch(getAssociatesList());
      return data;
    } catch (err) {
      await dispatch(actions.updateAssociatesFail());
      await dispatch(tableActions.setErrors(tableName, 0, { updateError: _.get(err, 'response.data') }));
      await dispatch(handleError(err, actions.updateAssociatesFail));
      return err;
    }
  }
};

export const postSubsidiaries = (tableName = '') => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null);

  const editedRows = _.get(state, `tables.${tableName}.editedRows`);
  const createdRows = _.get(state, `tables.${tableName}.createdRows`);
  const subsidiaries_list = Object.values({ ...createdRows, ...editedRows }).map(sub => ({
    society_id: sub.id,
    signatory_function_id: _.get(sub, 'signatory_function.id') || _.get(sub, 'signatory_function.value.id', null),
    effective_date: sub.start_date,
    end_date: _.get(sub, 'end_date', ''),
    social_part: sub.social_part && {
      NP: _.get(sub, 'social_part.NP', 0),
      PP: _.get(sub, 'social_part.PP', 0),
      US: _.get(sub, 'social_part.US', 0)
    },
    capital: _.get(sub, 'capital') || _.get(sub, 'society.capital', 0)
  }));
  const validation = validationSubsidaryTable(subsidiaries_list);

  const params = {
    society_list: subsidiaries_list
  };
  const {
    hasError,
    errors
  } = validation;
  if (hasError) {
    try {
      await dispatch(tableActions.setAllLinesErrors(tableName, errors));
      return validation;
    } catch (err) {
      return err;
    }
  } else {
    try {
      await dispatch(actions.updateSubsidiariesAttempt());
      const { data } = await windev.makeApiCall(`/society/${society_id}/filiale_associate`, 'post', {}, params);
      await dispatch(actions.updateSubsidiariesSuccess());
      await dispatch(getSubsidiariesList());
      return data;
    } catch (err) {
      await dispatch(actions.updateSubsidiariesFail());
      await dispatch(tableActions.setErrors(tableName, 0, { updateError: _.get(err, 'response.data') }));
      await dispatch(handleError(err, actions.updateSubsidiariesFail));
      return err;
    }
  }
};

export const deleteAssociatePersonPhysical = (
  associate,
  tableName
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null);

  try {
    await dispatch(actions.deleteAssociatesPersonPhysicalAttempt());
    const { data } = await windev.makeApiCall(`/society/${society_id}/associate_pers_physique`, 'delete', {}, associate);
    await dispatch(actions.deleteAssociatesPersonPhysicalSuccess());
    await dispatch(tableActions.deleteRows(tableName));
    await dispatch(getPersonnePhysiqueAllList({}, tableName));
    return data;
  } catch (err) {
    await dispatch(actions.deleteAssociatesPersonPhysicalFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteAssociatePersonLegal = (
  associate,
  tableName
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null);

  try {
    await dispatch(actions.deleteAssociatesPersonLegalAttempt());
    const { data } = await windev.makeApiCall(`/society/${society_id}/associate_pers_morale`, 'delete', {}, associate);
    await dispatch(actions.deleteAssociatesPersonLegalSuccess());
    await dispatch(tableActions.deleteRows(tableName));
    await dispatch(getPersonneMoraleAllList(society_id, {}, tableName));
    return data;
  } catch (err) {
    await dispatch(actions.deleteAssociatesPersonLegalFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteSubsidiaries = (
  subsidiary,
  tableName
) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(getCurrentTabState(state), 'companyList.selectedCompany.society_id', null);

  try {
    await dispatch(actions.deleteSubsidiaryAttempt(society_id));
    const { data } = await windev.makeApiCall(`/society/${society_id}/filiale_pers_morale`, 'delete', {}, subsidiary);
    await dispatch(actions.deleteSubsidiarySuccess());
    await dispatch(tableActions.deleteRows(tableName));
    await dispatch(getPersonneMoraleAllList(society_id, {}, tableName));
    return data;
  } catch (err) {
    await dispatch(actions.deleteSubsidiaryFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const postPassword = (society_id, password) => async () => {
  try {
    const { data } = await windev.makeApiCall('/society/check_access', 'post', {}, { society_id, password });
    return data;
  } catch (err) {
    throw err;
  }
};

export const resetFormAndGetCompanyAndResetExercises = (
  formName, society_id
) => async (dispatch) => {
  await dispatch(reset(formName));
  const society = await dispatch(getCompany(society_id));
  const exercises = await dispatch(getExercises(society_id));
  const fiscal_folder = await dispatch(getFiscalFolder(society_id));

  setCurrentForm(formName, {
    ...society,
    exercises,
    fiscal_folder
  });
};
