import {
  SELECT_TAB,

  GET_CITIES_FAIL,
  GET_CITIES_ATTEMPT,
  GET_CITIES,

  GET_ACCOUNTANTS,
  GET_ACCOUNTANTS_FAIL,
  GET_ACCOUNTANTS_ATTEMPT,

  GET_ROAD_TYPES,
  GET_ROAD_TYPES_FAIL,
  GET_ROAD_TYPES_ATTEMPT,

  GET_LEGAL_FORMS,
  GET_LEGAL_FORMS_ATTEMPT,
  GET_LEGAL_FORMS_FAIL,

  GET_APE,
  GET_APE_ATTEMPT,
  GET_APE_FAIL,

  GET_MEMBERS,
  GET_MEMBERS_ATTEMPT,
  GET_MEMBERS_FAIL,

  GET_EXERCISES,
  GET_EXERCISES_ATTEMPT,
  GET_EXERCISES_FAIL,

  GET_BILAN,
  GET_BILAN_ATTEMPT,
  GET_BILAN_FAIL,

  GET_TAXES,
  GET_TAXES_ATTEMPT,
  GET_TAXES_FAIL,

  SELECT_BUILDING,
  CANCEL_BUILDING,

  GET_TAXATION_REGIME_SUCCESS,
  GET_TAXATION_REGIME_ATTEMPT,
  GET_TAXATION_REGIME_FAIL,

  CREATE_COMPANY_ATTEMPT,
  CREATE_COMPANY_FAIL,

  UPDATE_EXERCISES_ATTEMPT,
  UPDATE_EXERCISES_FAIL,

  GET_REGISTRATION_PLACES,
  GET_REGISTRATION_PLACES_ATTEMPT,
  GET_REGISTRATION_PLACES_FAIL,

  GET_PHYSICAL_PERSON_LIST_SUCCESS,
  GET_PHYSICAL_PERSON_LIST_FAIL,
  GET_PHYSICAL_PERSON_LIST_ATTEMPT,

  GET_MORAL_PERSON_LIST_SUCCESS,
  GET_MORAL_PERSON_LIST_FAIL,
  GET_MORAL_PERSON_LIST_ATTEMPT,

  GET_ASSOCIATES_LIST_SUCCESS,
  GET_ASSOCIATES_LIST_FAIL,
  GET_ASSOCIATES_LIST_ATTEMPT,

  UPDATE_TAX_RECORD_FAIL,
  UPDATE_TAX_RECORD_ATTEMPT,
  UPDATE_TAX_RECORD_SUCCESS,

  GET_SUBSIDIARIES_SUCCESS,
  GET_SUBSIDIARIES_ATTEMPT,
  GET_SUBSIDIARIES_FAIL,

  DELETE_SUBSIDIARY_ATTEMPT,
  DELETE_SUBSIDIARY_SUCCESS,
  DELETE_SUBSIDIARY_FAIL,

  UPDATE_ASSOCIATES_SUCCESS,
  UPDATE_ASSOCIATES_ATTEMPT,
  UPDATE_ASSOCIATES_FAIL,

  UPDATE_SUBSIDIARIES_SUCCESS,
  UPDATE_SUBSIDIARIES_ATTEMPT,
  UPDATE_SUBSIDIARIES_FAIL,

  GET_FISCAL_FOLDER_FAIL,
  GET_FISCAL_FOLDER_ATTEMPT,
  GET_FISCAL_FOLDER_SUCCESS,

  GET_SOCIETY_STATUS_FAIL,
  GET_SOCIETY_STATUS_ATTEMPT,
  GET_SOCIETY_STATUS_SUCCESS,

  GET_VAT_REGIME_FAIL,
  GET_VAT_REGIME_ATTEMPT,
  GET_VAT_REGIME_SUCCESS,

  GET_ESTABLISHMENTS,
  SELECTED_ESTABLISHMENT,
  GET_RULES_FAIL,
  GET_RULES_ATTEMPT,
  GET_RULES_SUCCESS,

  GET_ACCOUNTING_OUTFIT_FAIL,
  GET_ACCOUNTING_OUTFIT_ATTEMPT,
  GET_ACCOUNTING_OUTFIT_SUCCESS,

  GET_ACCOUNTING_TYPE_FAIL,
  GET_ACCOUNTING_TYPE_ATTEMPT,
  GET_ACCOUNTING_TYPE_SUCCESS,

  GET_GESTION_CENTER_FAIL,
  GET_GESTION_CENTER_ATTEMPT,
  GET_GESTION_CENTER_SUCCESS,

  GET_DATA_BY_SIREN_ATTEMPT,
  GET_DATA_BY_SIREN_SUCCESS,
  GET_DATA_BY_SIREN_FAIL,

  DELETE_ASSOCIATES_PHYSICAL_PERSON_ATTEMPT,
  DELETE_ASSOCIATES_PHYSICAL_PERSON_FAIL,
  DELETE_ASSOCIATES_PHYSICAL_PERSON_SUCCESS,

  DELETE_ASSOCIATES_LEGAL_PERSON_ATTEMPT,
  DELETE_ASSOCIATES_LEGAL_PERSON_FAIL,
  DELETE_ASSOCIATES_LEGAL_PERSON_SUCCESS,

  SET_EXISTING_COMPANY,

  GET_CLIENT_USER_ATTEMPT,
  GET_CLIENT_USER_FAIL,
  GET_CLIENT_USER_SUCCESS,

  GET_EDI_ATTEMPT,
  GET_EDI_FAIL,
  GET_EDI_SUCCESS,

  GET_USER_TYPES_ATTEMPT,
  GET_USER_TYPES_SUCCESS,
  GET_USER_TYPES_FAIL
} from './constants';

/**
 * Tab index
 * @param {number} payload
 */
const selectTabAction = payload => ({
  type: SELECT_TAB,
  payload
});

const getClientUserAttempt = () => ({
  type: GET_CLIENT_USER_ATTEMPT
});
const getClientUserFail = () => ({
  type: GET_CLIENT_USER_FAIL
});
const getClientUserSuccess = payload => ({
  type: GET_CLIENT_USER_SUCCESS,
  payload
});

const getCitiesSuccess = payload => ({
  type: GET_CITIES,
  payload
});
const getCitiesFail = () => ({
  type: GET_CITIES_FAIL
});
const getCitiesAttempt = () => ({
  type: GET_CITIES_ATTEMPT
});

const getAccountantsSuccess = payload => ({
  type: GET_ACCOUNTANTS,
  payload
});
const getAccountantsFail = () => ({
  type: GET_ACCOUNTANTS_FAIL
});
const getAccountantsAttempt = () => ({
  type: GET_ACCOUNTANTS_ATTEMPT
});

const getRoadTypesSuccess = payload => ({
  type: GET_ROAD_TYPES,
  payload
});
const getRoadTypesFail = () => ({
  type: GET_ROAD_TYPES_FAIL
});
const getRoadTypesAttempt = () => ({
  type: GET_ROAD_TYPES_ATTEMPT
});

const getLegalFormsSuccess = payload => ({
  type: GET_LEGAL_FORMS,
  payload
});
const getLegalFormsFail = () => ({
  type: GET_LEGAL_FORMS_FAIL
});
const getLegalFormsAttempt = () => ({
  type: GET_LEGAL_FORMS_ATTEMPT
});

const getApeSuccess = payload => ({
  type: GET_APE,
  payload
});
const getApeFail = () => ({
  type: GET_APE_FAIL
});
const getApeAttempt = () => ({
  type: GET_APE_ATTEMPT
});

const getMemberCompaniesSuccess = payload => ({
  type: GET_MEMBERS,
  payload
});
const getMemberCompaniesFail = () => ({
  type: GET_MEMBERS_FAIL
});
const getMemberCompaniesAttempt = () => ({
  type: GET_MEMBERS_ATTEMPT
});

const getExercisesSuccess = payload => ({
  type: GET_EXERCISES,
  payload
});
const getExercisesFail = () => ({
  type: GET_EXERCISES_FAIL
});
const getExercisesAttempt = () => ({
  type: GET_EXERCISES_ATTEMPT
});

const selectBuilding = payload => ({
  type: SELECT_BUILDING,
  payload
});

const cancelBuilding = payload => ({
  type: CANCEL_BUILDING,
  payload
});

const getBilanSuccess = payload => ({
  type: GET_BILAN,
  payload
});
const getBilanFail = () => ({
  type: GET_BILAN_FAIL
});
const getBilanAttempt = () => ({
  type: GET_BILAN_ATTEMPT
});

const getRegistrationPlacesSuccess = payload => ({
  type: GET_REGISTRATION_PLACES,
  payload
});
const getRegistrationPlacesFail = () => ({
  type: GET_REGISTRATION_PLACES_FAIL
});
const getRegistrationPlacesAttempt = () => ({
  type: GET_REGISTRATION_PLACES_ATTEMPT
});

const getTaxesSuccess = payload => ({
  type: GET_TAXES,
  payload
});
const getTaxesFail = () => ({
  type: GET_TAXES_FAIL
});
const getTaxesAttempt = () => ({
  type: GET_TAXES_ATTEMPT
});

const getTaxationRegimeSuccess = payload => ({
  type: GET_TAXATION_REGIME_SUCCESS,
  payload
});

const getTaxationRegimeAttempt = payload => ({
  type: GET_TAXATION_REGIME_ATTEMPT,
  payload
});

const getTaxationRegimeFail = payload => ({
  type: GET_TAXATION_REGIME_FAIL,
  payload
});


const createCompanyFail = () => ({
  type: CREATE_COMPANY_FAIL
});
const createCompanyAttempt = () => ({
  type: CREATE_COMPANY_ATTEMPT
});

const updateExercicesFail = () => ({
  type: UPDATE_EXERCISES_FAIL
});
const updateExercicesAttempt = () => ({
  type: UPDATE_EXERCISES_ATTEMPT
});

const getListPhysicalPersonSuccess = payload => ({
  type: GET_PHYSICAL_PERSON_LIST_SUCCESS,
  payload
});
const getListPhysicalPersonFail = () => ({
  type: GET_PHYSICAL_PERSON_LIST_FAIL
});
const getListPhysicalPersonAttempt = () => ({
  type: GET_PHYSICAL_PERSON_LIST_ATTEMPT
});

const getListMoralPersonSuccess = payload => ({
  type: GET_MORAL_PERSON_LIST_SUCCESS,
  payload
});
const getListMoralPersonFail = () => ({
  type: GET_MORAL_PERSON_LIST_FAIL
});
const getListMoralPersonAttempt = () => ({
  type: GET_MORAL_PERSON_LIST_ATTEMPT
});

const getAssociatesListSuccess = payload => ({
  type: GET_ASSOCIATES_LIST_SUCCESS,
  payload
});
const getAssociatesListFail = () => ({
  type: GET_ASSOCIATES_LIST_FAIL
});
const getAssociatesListAttempt = () => ({
  type: GET_ASSOCIATES_LIST_ATTEMPT
});

const updateTaxRecordFail = () => ({
  type: UPDATE_TAX_RECORD_FAIL
});
const updateTaxRecordAttempt = () => ({
  type: UPDATE_TAX_RECORD_ATTEMPT
});
const updateTaxRecordSuccess = payload => ({
  type: UPDATE_TAX_RECORD_SUCCESS,
  payload
});

const getSubsidiariesFail = () => ({
  type: GET_SUBSIDIARIES_FAIL
});
const getSubsidiariesAttempt = () => ({
  type: GET_SUBSIDIARIES_ATTEMPT
});
const getSubsidiariesSuccess = payload => ({
  type: GET_SUBSIDIARIES_SUCCESS,
  payload
});

const updateAssociatesSuccess = associate => ({
  type: UPDATE_ASSOCIATES_SUCCESS,
  associate
});
const updateAssociatesAttempt = () => ({
  type: UPDATE_ASSOCIATES_ATTEMPT
});
const updateAssociatesFail = () => ({
  type: UPDATE_ASSOCIATES_FAIL
});

const updateSubsidiariesSuccess = subsidiaries => ({
  type: UPDATE_SUBSIDIARIES_SUCCESS,
  subsidiaries
});
const updateSubsidiariesAttempt = () => ({
  type: UPDATE_SUBSIDIARIES_ATTEMPT
});
const updateSubsidiariesFail = () => ({
  type: UPDATE_SUBSIDIARIES_FAIL
});

const getFiscalFolderFail = () => ({
  type: GET_FISCAL_FOLDER_FAIL
});
const getFiscalFolderAttempt = () => ({
  type: GET_FISCAL_FOLDER_ATTEMPT
});
const getFiscalFolderSuccess = fiscal_folder => ({
  type: GET_FISCAL_FOLDER_SUCCESS,
  fiscal_folder
});

const getSocietyStatusFail = () => ({
  type: GET_SOCIETY_STATUS_FAIL
});
const getSocietyStatusAttempt = () => ({
  type: GET_SOCIETY_STATUS_ATTEMPT
});
const getSocietyStatusSuccess = status => ({
  type: GET_SOCIETY_STATUS_SUCCESS,
  status
});

const getVatRegimeFail = () => ({
  type: GET_VAT_REGIME_FAIL
});
const getVatRegimeAttempt = () => ({
  type: GET_VAT_REGIME_ATTEMPT
});
const getVatRegimeSuccess = vat_regime => ({
  type: GET_VAT_REGIME_SUCCESS,
  vat_regime
});

const getRulesFail = () => ({
  type: GET_RULES_FAIL
});
const getRulesAttempt = () => ({
  type: GET_RULES_ATTEMPT
});
const getRulesSuccess = payload => ({
  type: GET_RULES_SUCCESS,
  payload
});

const getAccountingOutfitFail = () => ({
  type: GET_ACCOUNTING_OUTFIT_FAIL
});
const getAccountingOutfitAttempt = () => ({
  type: GET_ACCOUNTING_OUTFIT_ATTEMPT
});
const getAccountingOutfitSuccess = payload => ({
  type: GET_ACCOUNTING_OUTFIT_SUCCESS,
  payload
});

const getAccountingTypeFail = () => ({
  type: GET_ACCOUNTING_TYPE_FAIL
});
const getAccountingTypeAttempt = () => ({
  type: GET_ACCOUNTING_TYPE_ATTEMPT
});
const getAccountingTypeSuccess = payload => ({
  type: GET_ACCOUNTING_TYPE_SUCCESS,
  payload
});

const getGestionCenterFail = () => ({
  type: GET_GESTION_CENTER_FAIL
});
const getGestionCenterAttempt = () => ({
  type: GET_GESTION_CENTER_ATTEMPT
});
const getGestionCenterSuccess = gestion_center => ({
  type: GET_GESTION_CENTER_SUCCESS,
  gestion_center
});

const getDataBySirenAttempt = () => ({
  type: GET_DATA_BY_SIREN_ATTEMPT
});
const getDataBySirenSuccess = () => ({
  type: GET_DATA_BY_SIREN_SUCCESS
});
const getDataBySirenFail = () => ({
  type: GET_DATA_BY_SIREN_FAIL
});

const getEstablishments = payload => ({
  type: GET_ESTABLISHMENTS,
  payload
});

const selectedEstablishment = payload => ({
  type: SELECTED_ESTABLISHMENT,
  payload
});
const deleteSubsidiaryAttempt = society_id => ({
  type: DELETE_SUBSIDIARY_ATTEMPT,
  society_id
});

const deleteSubsidiaryFail = society_id => ({
  type: DELETE_SUBSIDIARY_FAIL,
  society_id
});

const deleteSubsidiarySuccess = (subsidiaryList, subsidiary_id) => ({
  type: DELETE_SUBSIDIARY_SUCCESS,
  subsidiary_id,
  subsidiaryList
});

/* Delete associate */
const deleteAssociatesPersonPhysicalSuccess = () => ({
  type: DELETE_ASSOCIATES_PHYSICAL_PERSON_SUCCESS
});
const deleteAssociatesPersonPhysicalAttempt = () => ({
  type: DELETE_ASSOCIATES_PHYSICAL_PERSON_ATTEMPT
});
const deleteAssociatesPersonPhysicalFail = () => ({
  type: DELETE_ASSOCIATES_PHYSICAL_PERSON_FAIL
});
const deleteAssociatesPersonLegalSuccess = () => ({
  type: DELETE_ASSOCIATES_LEGAL_PERSON_SUCCESS
});
const deleteAssociatesPersonLegalAttempt = () => ({
  type: DELETE_ASSOCIATES_LEGAL_PERSON_ATTEMPT
});
const deleteAssociatesPersonLegalFail = () => ({
  type: DELETE_ASSOCIATES_LEGAL_PERSON_FAIL
});

const setExistingCompany = society_id => ({
  type: SET_EXISTING_COMPANY,
  society_id
});

const getEDIAttempt = () => ({
  type: GET_EDI_ATTEMPT
});
const getEDIFail = () => ({
  type: GET_EDI_FAIL
});
const getEDISuccess = edi => ({
  type: GET_EDI_SUCCESS,
  edi
});

const getUserTypeAttempt = (apiUserType, selectedType) => ({
  type: GET_USER_TYPES_ATTEMPT,
  apiUserType,
  selectedType
});
const getUserTypeSuccess = (apiUserType, selectedType) => ({
  type: GET_USER_TYPES_SUCCESS,
  apiUserType,
  selectedType
});
const getUserTypeFail = edi => ({
  type: GET_USER_TYPES_FAIL,
  edi
});

export default {
  getListPhysicalPersonSuccess,
  getListPhysicalPersonFail,
  getListPhysicalPersonAttempt,
  getListMoralPersonSuccess,
  getListMoralPersonFail,
  getListMoralPersonAttempt,
  getAssociatesListSuccess,
  getAssociatesListFail,
  getAssociatesListAttempt,
  updateTaxRecordSuccess,
  updateTaxRecordAttempt,
  updateTaxRecordFail,

  updateExercicesAttempt,
  updateExercicesFail,

  createCompanyAttempt,
  createCompanyFail,

  getTaxesAttempt,
  getTaxesFail,
  getTaxesSuccess,

  getTaxationRegimeSuccess,
  getTaxationRegimeAttempt,
  getTaxationRegimeFail,

  getRegistrationPlacesAttempt,
  getRegistrationPlacesFail,
  getRegistrationPlacesSuccess,

  getBilanAttempt,
  getBilanFail,

  selectBuilding,
  cancelBuilding,

  getExercisesAttempt,
  getExercisesFail,
  getExercisesSuccess,

  getMemberCompaniesAttempt,
  getMemberCompaniesFail,
  getMemberCompaniesSuccess,

  getBilanSuccess,

  getApeAttempt,
  getApeFail,
  getApeSuccess,

  getLegalFormsAttempt,
  getLegalFormsFail,
  getLegalFormsSuccess,

  getRoadTypesAttempt,
  getRoadTypesFail,
  getRoadTypesSuccess,

  getAccountantsAttempt,
  getAccountantsFail,
  getAccountantsSuccess,

  getCitiesAttempt,
  getCitiesFail,
  getCitiesSuccess,

  selectTabAction,

  getSubsidiariesFail,
  getSubsidiariesAttempt,
  getSubsidiariesSuccess,

  deleteSubsidiarySuccess,
  deleteSubsidiaryFail,
  deleteSubsidiaryAttempt,

  updateAssociatesSuccess,
  updateAssociatesAttempt,
  updateAssociatesFail,

  getFiscalFolderFail,
  getFiscalFolderAttempt,
  getFiscalFolderSuccess,

  getSocietyStatusFail,
  getSocietyStatusAttempt,
  getSocietyStatusSuccess,

  getEstablishments,
  selectedEstablishment,

  getVatRegimeFail,
  getVatRegimeAttempt,
  getVatRegimeSuccess,

  getRulesFail,
  getRulesAttempt,
  getRulesSuccess,

  getAccountingOutfitFail,
  getAccountingOutfitAttempt,
  getAccountingOutfitSuccess,

  getAccountingTypeFail,
  getAccountingTypeAttempt,
  getAccountingTypeSuccess,

  getGestionCenterFail,
  getGestionCenterAttempt,
  getGestionCenterSuccess,

  getDataBySirenAttempt,
  getDataBySirenSuccess,
  getDataBySirenFail,

  updateSubsidiariesSuccess,
  updateSubsidiariesAttempt,
  updateSubsidiariesFail,

  deleteAssociatesPersonPhysicalSuccess,
  deleteAssociatesPersonPhysicalAttempt,
  deleteAssociatesPersonPhysicalFail,

  deleteAssociatesPersonLegalSuccess,
  deleteAssociatesPersonLegalFail,
  deleteAssociatesPersonLegalAttempt,

  setExistingCompany,

  getClientUserAttempt,
  getClientUserFail,
  getClientUserSuccess,

  getEDIAttempt,
  getEDIFail,
  getEDISuccess,
  getUserTypeAttempt,
  getUserTypeSuccess,
  getUserTypeFail
};
