import {
  SELECT_TAB,
  GET_CITIES,
  GET_ACCOUNTANTS,
  GET_ROAD_TYPES,
  GET_LEGAL_FORMS,
  GET_APE,
  GET_MEMBERS,
  GET_EXERCISES,
  GET_BILAN,
  GET_TAXES,
  GET_ESTABLISHMENTS,
  SELECTED_ESTABLISHMENT,

  GET_TAXATION_REGIME_SUCCESS,
  GET_TAXATION_REGIME_ATTEMPT,
  GET_TAXATION_REGIME_FAIL,

  GET_REGISTRATION_PLACES,
  GET_PHYSICAL_PERSON_LIST_SUCCESS,
  GET_PHYSICAL_PERSON_LIST_FAIL,
  GET_PHYSICAL_PERSON_LIST_ATTEMPT,
  GET_MORAL_PERSON_LIST_SUCCESS,
  GET_MORAL_PERSON_LIST_FAIL,
  GET_MORAL_PERSON_LIST_ATTEMPT,
  GET_ASSOCIATES_LIST_SUCCESS,
  GET_ASSOCIATES_LIST_FAIL,
  GET_ASSOCIATES_LIST_ATTEMPT,

  UPDATE_TAX_RECORD_FAIL,
  UPDATE_TAX_RECORD_ATTEMPT,
  UPDATE_TAX_RECORD_SUCCESS,
  GET_SUBSIDIARIES_SUCCESS,
  GET_SUBSIDIARIES_ATTEMPT,
  GET_SUBSIDIARIES_FAIL,

  GET_FISCAL_FOLDER_FAIL,
  GET_FISCAL_FOLDER_ATTEMPT,
  GET_FISCAL_FOLDER_SUCCESS,
  GET_RULES_SUCCESS,
  GET_ACCOUNTING_OUTFIT_SUCCESS,
  GET_ACCOUNTING_TYPE_SUCCESS,
  GET_GESTION_CENTER_FAIL,
  GET_GESTION_CENTER_ATTEMPT,
  GET_GESTION_CENTER_SUCCESS,

  GET_VAT_REGIME_ATTEMPT,
  GET_VAT_REGIME_SUCCESS,
  GET_VAT_REGIME_FAIL,

  GET_SOCIETY_STATUS_ATTEMPT,
  GET_SOCIETY_STATUS_SUCCESS,
  GET_SOCIETY_STATUS_FAIL,
  UPDATE_ASSOCIATES_SUCCESS,
  DELETE_ASSOCIATES_PHYSICAL_PERSON_ATTEMPT,
  DELETE_ASSOCIATES_PHYSICAL_PERSON_FAIL,
  DELETE_ASSOCIATES_PHYSICAL_PERSON_SUCCESS,

  DELETE_ASSOCIATES_LEGAL_PERSON_ATTEMPT,
  DELETE_ASSOCIATES_LEGAL_PERSON_FAIL,
  DELETE_ASSOCIATES_LEGAL_PERSON_SUCCESS,

  GET_CLIENT_USER_ATTEMPT,
  GET_CLIENT_USER_FAIL,
  GET_CLIENT_USER_SUCCESS,

  SELECT_BUILDING,
  CANCEL_BUILDING,

  SET_EXISTING_COMPANY,
  GET_EDI_SUCCESS,
  GET_USER_TYPES_SUCCESS
} from './constants';

const initialState = {
  selectedTab: 0,
  cities: [],
  roadTypes: [],
  accountants: [],
  legalForms: [],
  apeCodes: [],
  exercises: [],
  memberCompanies: [],
  selectedBuilding: undefined,
  disabledButton: false,
  selectedEstablishment: '',
  establishments: [],
  registrationPlaces: [],
  listPhysicalPerson: [],
  listMoralPerson: [],
  associates: {},
  isLoading: false,
  isError: false,
  fiscalFolder: [],
  societyStatus: [],
  vatRegime: [],
  rules: [],
  edi: [],
  accountingOutfit: [],
  accountingType: [],
  gestionCenterList: [],
  existingCompany: null,
  clientUserList: [],
  userTypes: {},
  limit: 10,
  page: 0,
  sort: { column: 'users', direction: 'asc' }
};

const companyCreation = (state = initialState, action) => {
  switch (action.type) {
  case DELETE_ASSOCIATES_PHYSICAL_PERSON_ATTEMPT:
  case DELETE_ASSOCIATES_PHYSICAL_PERSON_FAIL:
  case DELETE_ASSOCIATES_PHYSICAL_PERSON_SUCCESS:
  case DELETE_ASSOCIATES_LEGAL_PERSON_ATTEMPT:
  case DELETE_ASSOCIATES_LEGAL_PERSON_FAIL:
  case DELETE_ASSOCIATES_LEGAL_PERSON_SUCCESS:
  case GET_GESTION_CENTER_FAIL:
  case GET_GESTION_CENTER_ATTEMPT:
  case GET_PHYSICAL_PERSON_LIST_ATTEMPT:
  case GET_PHYSICAL_PERSON_LIST_FAIL:
  case GET_MORAL_PERSON_LIST_ATTEMPT:
  case GET_MORAL_PERSON_LIST_FAIL:
  case UPDATE_TAX_RECORD_ATTEMPT:
  case UPDATE_TAX_RECORD_FAIL:
  case GET_FISCAL_FOLDER_FAIL:
  case GET_VAT_REGIME_ATTEMPT:
  case GET_FISCAL_FOLDER_ATTEMPT:
  case GET_VAT_REGIME_FAIL:
  case GET_SOCIETY_STATUS_ATTEMPT:
  case GET_SOCIETY_STATUS_FAIL:
  case GET_TAXATION_REGIME_ATTEMPT:
  case GET_TAXATION_REGIME_FAIL:
    return state;

  case GET_CLIENT_USER_ATTEMPT: {
    return {
      ...state,
      isLoading: true,
      isError: false,
      clientUserList: []
    };
  }
  case GET_CLIENT_USER_FAIL: {
    return {
      ...state,
      isLoading: false,
      isError: true,
      clientUserList: []
    };
  }
  case GET_CLIENT_USER_SUCCESS: {
    return {
      ...state,
      isLoading: false,
      isError: false,
      clientUserList: action.payload.data,
      limit: action.payload.limit,
      page: action.payload.page,
      sort: action.payload.sort
    };
  }

  case GET_GESTION_CENTER_SUCCESS: {
    return {
      ...state,
      gestionCenterList: action.gestion_center
    };
  }
  case SELECT_TAB: {
    return {
      ...state,
      selectedTab: action.payload
    };
  }
  case GET_CITIES: {
    return {
      ...state,
      cities: action.payload
    };
  }
  case GET_ACCOUNTANTS: {
    return {
      ...state,
      accountants: action.payload
    };
  }
  case GET_ROAD_TYPES: {
    return {
      ...state,
      roadTypes: action.payload
    };
  }
  case GET_LEGAL_FORMS: {
    return {
      ...state,
      legalForms: action.payload
    };
  }
  case GET_APE: {
    return {
      ...state,
      apeCodes: action.payload
    };
  }
  case GET_MEMBERS: {
    return {
      ...state,
      memberCompanies: action.payload
    };
  }
  case GET_EXERCISES: {
    return {
      ...state,
      exercises: action.payload
    };
  }
  case GET_BILAN: {
    return {
      ...state,
      bilan: action.payload
    };
  }
  case GET_TAXES: {
    return {
      ...state,
      taxes: action.payload
    };
  }
  case SELECT_BUILDING: {
    return {
      ...state,
      selectedBuilding: {
        ...state.selectedBuilding
      },
      disabledButton: true
    };
  }
  case CANCEL_BUILDING: {
    return {
      ...state,
      selectedBuilding: undefined,
      disabledButton: false
    };
  }
  case GET_ESTABLISHMENTS: {
    return {
      ...state,
      establishments: action.payload
    };
  }
  case SELECTED_ESTABLISHMENT: {
    return {
      ...state,
      selectedEstablishment: action.payload
    };
  }
  case GET_REGISTRATION_PLACES: {
    return {
      ...state,
      registrationPlaces: action.payload
    };
  }
  case GET_PHYSICAL_PERSON_LIST_SUCCESS: {
    return {
      ...state,
      listPhysicalPerson:
        action.payload.physicalPersonsData.array_pers_physique.filter(
          e => action.payload.dataTable.length === 0
            || !action.payload.dataTable.some(
              s => e.pers_physique_id === s.id
            )
        )
    };
  }
  case GET_MORAL_PERSON_LIST_SUCCESS: {
    return {
      ...state,
      listMoralPerson:
        action.payload.companiesData.society_array.filter(
          e => e.society_id !== action.payload.societyId && !action.payload.dataTable
            .some(
              s => e.society_id === s.id
            )
        )
    };
  }
  case GET_SUBSIDIARIES_ATTEMPT:
    return {
      ...state,
      isLoading: true,
      isError: false
    };
  case GET_SUBSIDIARIES_FAIL: {
    return {
      ...state,
      subsidiaryList: []
    };
  }
  case GET_ASSOCIATES_LIST_ATTEMPT: {
    return {
      ...state,
      associates: {
        isLoading: true,
        isError: false
      }
    };
  }
  case GET_ASSOCIATES_LIST_FAIL: {
    return {
      ...state,
      associates: {
        isLoading: false,
        isError: true
      }
    };
  }
  case GET_ASSOCIATES_LIST_SUCCESS: {
    return {
      ...state,
      associates: {
        isLoading: false,
        isError: false,
        ...action.payload
      }
    };
  }
  case UPDATE_TAX_RECORD_SUCCESS: {
    return {
      ...state,
      fiscalFolder: action.payload
    };
  }
  case GET_SUBSIDIARIES_SUCCESS: {
    return {
      ...state,
      subsidiaryList: action.payload
    };
  }
  case GET_FISCAL_FOLDER_SUCCESS: {
    return {
      ...state,
      fiscalFolder: action.fiscal_folder
    };
  }
  case GET_SOCIETY_STATUS_SUCCESS: {
    return {
      ...state,
      societyStatus: action.status
    };
  }
  case GET_VAT_REGIME_SUCCESS: {
    return {
      ...state,
      vatRegime: action.vat_regime
    };
  }
  case GET_EDI_SUCCESS: {
    return {
      ...state,
      edi: action.edi
    };
  }
  case GET_RULES_SUCCESS: {
    return {
      ...state,
      rules: action.payload
    };
  }
  case GET_ACCOUNTING_OUTFIT_SUCCESS: {
    return {
      ...state,
      accountingOutfit: action.payload
    };
  }
  case GET_ACCOUNTING_TYPE_SUCCESS: {
    return {
      ...state,
      accountingType: action.payload
    };
  }
  case UPDATE_ASSOCIATES_SUCCESS: {
    return {
      ...state
    };
  }
  case GET_TAXATION_REGIME_SUCCESS: {
    return {
      ...state,
      taxationRegime: action.payload
    };
  }
  case SET_EXISTING_COMPANY: {
    return {
      ...state,
      existingCompany: action.society_id
    };
  }
  case GET_USER_TYPES_SUCCESS: {
    return {
      ...state,
      userTypes: {
        ...state.userTypes,
        [action.selectedType]: action.apiUserType
      }
    };
  }
  default:
    return state;
  }
};

export default companyCreation;
