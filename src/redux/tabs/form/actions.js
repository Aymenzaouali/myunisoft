import {
  SET_CURRENT_FORM,
  UPDATE_CURRENT_FORM_DATA,
  CLOSE_CURRENT_FORM,
  QUIT_CURRENT_FORM
} from './constants';

/**
 * Adds the form to the state => allow to cancel navigation within the tab
 *
 * @param {string} name - name of the form
 * @param {{}} initial - initial data in the form
 * @param {[string]} ignore - fields to ignore in check
 * @returns {{tab: *, name: *, type: string}}
 */
const setCurrentForm = (name, initial, ignore = []) => ({
  type: SET_CURRENT_FORM,
  name,
  initial,
  ignore
});

/**
 * Update initial form data
 *
 * @param {{}} data - initial data in the form
 * @returns {{tab: *, name: *, type: string}}
 */
const updateCurrentFormData = data => ({ type: UPDATE_CURRENT_FORM_DATA, data });

/**
 * Test has form values changed and if it has show a confirmation dialog
 * Remove the form from the state => free navigation within the tab
 *
 * @param {() => void} continueCb - Callback to continue actions
 * @param {number} tab_id - precise tab to apply on, defaults to current navigation tab
 */
const closeCurrentForm = (continueCb, tab_id = undefined) => ({
  type: CLOSE_CURRENT_FORM,
  tab_id,
  continueCb
});

/**
 * Remove the form from the state => free navigation within the tab
 */
const quitCurrentForm = () => ({ type: QUIT_CURRENT_FORM });

export {
  setCurrentForm,
  updateCurrentFormData,
  closeCurrentForm,
  quitCurrentForm
};
