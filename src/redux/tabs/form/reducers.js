import {
  SET_CURRENT_FORM,
  UPDATE_CURRENT_FORM_DATA,
  CLOSE_CURRENT_FORM,
  QUIT_CURRENT_FORM
} from './constants';

const initial_state = {};

const formReducer = (state = initial_state, action) => {
  switch (action.type) {
  case SET_CURRENT_FORM:
    return {
      name: action.name,
      initial: action.initial,
      ignore: action.ignore
    };

  case UPDATE_CURRENT_FORM_DATA:
    return {
      ...state,
      initial: {
        ...state.initial,
        ...action.data
      }
    };

  case QUIT_CURRENT_FORM:
  case CLOSE_CURRENT_FORM:
    return initial_state;

  default:
  }

  return state;
};

export default formReducer;
