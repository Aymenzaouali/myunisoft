import {
  GET_PERSONNE_PHYSIQUE_ATTEMPT,
  GET_PERSONNE_PHYSIQUE_SUCCESS,
  GET_PERSONNE_PHYSIQUE_FAIL
} from './constants';

const initialState = {
  physicalPersonsData: {},
  sort: { column: 'name', direction: 'asc' },
  limit: 10,
  page: 0,
  isLoading: false,
  isError: false
};

const physicalPerson = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_PERSONNE_PHYSIQUE_FAIL:
      return {
        ...state,
        physicalPersonsData: {},
        isLoading: false,
        isError: true
      };
    case GET_PERSONNE_PHYSIQUE_ATTEMPT:
      return {
        ...state,
        physicalPersonsData: {},
        isLoading: true,
        isError: false
      };
    case GET_PERSONNE_PHYSIQUE_SUCCESS:
      const {
        page, limit, sort, physicalPersonsData
      } = action.payload;

      return {
        ...state,
        page,
        limit,
        sort,
        physicalPersonsData,
        isLoading: false,
        isError: false
      };
    default: return state;
    }
  }
  return state;
};

export default physicalPerson;
