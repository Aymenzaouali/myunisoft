import {
  GET_PERSONNE_PHYSIQUE_ATTEMPT,
  GET_PERSONNE_PHYSIQUE_SUCCESS,
  GET_PERSONNE_PHYSIQUE_FAIL,

  DELETE_PERSONNE_PHYSIQUE_ATTEMPT,
  DELETE_PERSONNE_PHYSIQUE_SUCCESS,
  DELETE_PERSONNE_PHYSIQUE_FAIL
} from './constants';

const getPersonnePhysiqueAttempt = () => ({
  type: GET_PERSONNE_PHYSIQUE_ATTEMPT
});

const getPersonnePhysiqueSuccess = payload => ({
  type: GET_PERSONNE_PHYSIQUE_SUCCESS,
  payload
});

const getPersonnePhysiqueFail = () => ({
  type: GET_PERSONNE_PHYSIQUE_FAIL
});

const deletePersonnePhysiqueAttempt = () => ({
  type: DELETE_PERSONNE_PHYSIQUE_ATTEMPT
});

const deletePersonnePhysiqueSuccess = () => ({
  type: DELETE_PERSONNE_PHYSIQUE_SUCCESS
});

const deletePersonnePhysiqueFail = () => ({
  type: DELETE_PERSONNE_PHYSIQUE_FAIL
});

export default {
  getPersonnePhysiqueAttempt,
  getPersonnePhysiqueSuccess,
  getPersonnePhysiqueFail,

  deletePersonnePhysiqueAttempt,
  deletePersonnePhysiqueSuccess,
  deletePersonnePhysiqueFail
};
