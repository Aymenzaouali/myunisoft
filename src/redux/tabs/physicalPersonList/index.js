import _ from 'lodash';

import { windev } from 'helpers/api';
import { getCurrentTabState } from 'helpers/tabs';
import { handleError } from 'common/redux/error';

import actions from './actions';

export const getPersonnePhysiqueList = (payload = {}) => async (dispatch, getState) => {
  const state = getState();
  const tabstate = getCurrentTabState(state);

  const {
    sort: sortOpts,
    firstname,
    name,
    q
  } = payload;

  const sort = sortOpts !== undefined
    ? sortOpts
    : _.get(tabstate, 'user.sort');

  const limit = 1000000;

  const page = 0;

  const offset = 0;

  try {
    await dispatch(actions.getPersonnePhysiqueAttempt());

    const params = {
      offset,
      limit,
      sort,
      firstname,
      name,
      q
    };

    const { data: physicalPersonsData } = await windev.makeApiCall('/pers_physique', 'get', params);
    await dispatch(actions.getPersonnePhysiqueSuccess({
      physicalPersonsData,
      limit,
      page,
      sort: sort || {}
    }));
    return physicalPersonsData;
  } catch (err) {
    await dispatch(actions.getPersonnePhysiqueFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deletePersonnePhysique = id_pers_physique => async (dispatch) => {
  try {
    await dispatch(actions.deletePersonnePhysiqueAttempt());
    const { data } = await windev.makeApiCall('pers_physique', 'delete', { id_pers_physique });
    await dispatch(actions.deletePersonnePhysiqueSuccess());
    await dispatch(getPersonnePhysiqueList());
    return data;
  } catch (err) {
    await dispatch(actions.deletePersonnePhysiqueFail());
    await dispatch(handleError(err));
    return err;
  }
};
