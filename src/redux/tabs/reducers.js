import { combineReducers } from 'redux';

import { formatTabId, getNewTab } from 'helpers/tabs';
import { CLOSE_TAB } from 'redux/navigation/constants';

import accounting from './accounting/reducer';
import closing from './closing/reducer';
import companyCreation from './companyCreation/reducers';
import companyList from './companyList/reducers';
import physicalPersonCreation from './physicalPersonCreation/reducers';
import physicalPersonList from './physicalPersonList/reducers';
import dadp from './dadp/reducer';
import documents from './documents/reducer';
import exercises from './exercises/reducer';
import form from './form/reducers';
import split from './split/reducers';
import user from './user/reducers';
import imports from './import/reducers';
import exports from './export/reducers';
import crm from './crm/reducers';

// Child reducers
const reducers = combineReducers({
  accounting,
  closing,
  companyCreation,
  companyList,
  physicalPersonCreation,
  physicalPersonList,
  dadp,
  documents,
  exercises,
  form,
  split,
  user,
  imports,
  exports,
  crm
});

// Tabs reducer
const initial_state = {};

const tabsReducer = (state = initial_state, action, navigation_id) => {
  // Delete tab state as we close the tab
  if (action.type === CLOSE_TAB) {
    const { [formatTabId(action.id)]: tab, ...others } = state;

    return { ...others };
  }

  const tab_id = action.tab_id || getNewTab(action) || navigation_id;

  // Transmit action to correct tab state
  if (tab_id) {
    const id = formatTabId(tab_id);
    const { [id]: tab, ...others } = state;

    return {
      ...others,
      [id]: reducers(tab, action)
    };
  }

  return state;
};

export default tabsReducer;
