import {
  GET_COMPANIES_SUCCESS,
  GET_COMPANIES_ATTEMPT,
  GET_COMPANIES_FAIL,
  GET_MORAL_PERSON_LIST_SUCCESS,
  GET_MORAL_PERSON_LIST_FAIL,
  GET_MORAL_PERSON_LIST_ATTEMPT,
  SELECT_COMPANY
} from './constants';

const initialState = {
  selectedCompany: undefined,
  companiesData: {},
  sort: { column: 'name', direction: 'desc' },
  limit: 10,
  page: 0,
  isLoading: false,
  isError: false
};

const companyList = (state = initialState, action) => {
  switch (action.type) {
  case GET_MORAL_PERSON_LIST_ATTEMPT:
  case GET_MORAL_PERSON_LIST_FAIL:
    return state;
  case GET_COMPANIES_SUCCESS: {
    const {
      page,
      limit,
      sort,
      companiesData
    } = action.payload;

    return {
      ...state,
      page,
      limit,
      sort,
      companiesData,
      isLoading: false,
      isError: false
    };
  }
  case GET_COMPANIES_ATTEMPT: {
    return {
      ...state,
      companiesData: {},
      isLoading: true,
      isError: false
    };
  }
  case GET_COMPANIES_FAIL: {
    return {
      ...state,
      companiesData: {},
      isLoading: false,
      isError: true
    };
  }
  case GET_MORAL_PERSON_LIST_SUCCESS: {
    return {
      ...state,
      listMoralPerson:
        action.payload.companiesData.society_array.filter(
          e => e.society_id !== action.payload.societyId && !action.payload.dataTable
            .some(
              s => e.society_id === s.id
            )
        )
    };
  }
  case SELECT_COMPANY: {
    return {
      ...state,
      selectedCompany: action.payload
    };
  }
  default:
    return state;
  }
};

export default companyList;
