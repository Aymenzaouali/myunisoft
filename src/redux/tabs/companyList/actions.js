import {
  GET_COMPANIES_ATTEMPT, GET_COMPANIES_FAIL, GET_COMPANIES_SUCCESS,
  GET_COMPANY_ATTEMPT, GET_COMPANY_FAIL, GET_COMPANY_SUCCESS,
  GET_MORAL_PERSON_LIST_SUCCESS, GET_MORAL_PERSON_LIST_FAIL, GET_MORAL_PERSON_LIST_ATTEMPT,
  SELECT_COMPANY,
  DELETE_COMPANY_FAIL, DELETE_COMPANY_ATTEMPT, DELETE_COMPANY_SUCCESS
} from './constants';

export const getCompaniesSuccessAction = payload => ({
  type: GET_COMPANIES_SUCCESS,
  payload
});
export const getCompaniesAttemptAction = () => ({ type: GET_COMPANIES_ATTEMPT });
export const getCompaniesFailAction = () => ({ type: GET_COMPANIES_FAIL });

export const getCompanySuccessAction = () => ({ type: GET_COMPANY_SUCCESS });
export const getCompanyAttemptAction = () => ({ type: GET_COMPANY_ATTEMPT });
export const getCompanyFailAction = () => ({ type: GET_COMPANY_FAIL });

export const selectCompanyAction = payload => ({
  type: SELECT_COMPANY,
  payload
});

export const getListMoralPersonSuccess = payload => ({
  type: GET_MORAL_PERSON_LIST_SUCCESS,
  payload
});
export const getListMoralPersonFail = () => ({ type: GET_MORAL_PERSON_LIST_FAIL });
export const getListMoralPersonAttempt = () => ({ type: GET_MORAL_PERSON_LIST_ATTEMPT });

export const deleteCompanySuccessAction = () => ({ type: DELETE_COMPANY_SUCCESS });
export const deleteCompanyAttemptAction = () => ({ type: DELETE_COMPANY_ATTEMPT });
export const deleteCompanyFailAction = () => ({ type: DELETE_COMPANY_FAIL });
