import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';
import _ from 'lodash';
import { autoFillFormValues } from 'helpers/autoFillFormValues';
import { normalizeData } from 'helpers/normalizeData';
import { tabFormName } from 'helpers/tabs';

import {
  getCompaniesAttemptAction, getCompaniesFailAction, getCompaniesSuccessAction,
  selectCompanyAction,
  getCompanyAttemptAction, getCompanyFailAction, getCompanySuccessAction,
  getListMoralPersonAttempt, getListMoralPersonFail, getListMoralPersonSuccess,
  deleteCompanySuccessAction, deleteCompanyAttemptAction, deleteCompanyFailAction
} from './actions';

export const getCompanyList = (payload = {}) => async (dispatch) => {
  const {
    sort: sortOpts,
    q: query,
    societyId
  } = payload;

  const sort = sortOpts;

  const limit = 1000000;

  const page = 0;

  const offset = 0;

  try {
    await dispatch(getCompaniesAttemptAction());

    const params = {
      limit,
      sort,
      offset,
      q: query,
      mode: 1,
      societyId
    };

    const { data: companiesData } = await windev.makeApiCall('/society', 'get', params);
    await dispatch(getCompaniesSuccessAction({
      companiesData,
      limit,
      page,
      sort: sort || {}
    }));
    return companiesData;
  } catch (err) {
    await dispatch(getCompaniesFailAction());
    await dispatch(handleError(err));
    return err;
  }
};

export const getPersonneMoraleAllList = (
  societyId,
  payload = {},
  tableName = ''
) => async (dispatch, getState) => {
  const state = getState();
  const dataTable = _.get(state, `tables.${tableName}.data`, []);
  const {
    limit = 1000
  } = payload;
  try {
    await dispatch(getListMoralPersonAttempt());
    const params = {
      limit
    };
    const { data: companiesData } = await windev.makeApiCall('/society', 'get', params);
    await dispatch(getListMoralPersonSuccess({
      societyId,
      companiesData,
      limit,
      dataTable
    }));
    return companiesData;
  } catch (err) {
    await dispatch(getListMoralPersonFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getCompany = society_id => async (dispatch, getState) => {
  const state = getState();
  const formName = tabFormName('companyCreationForm', state.navigation.id);

  try {
    await dispatch(getCompanyAttemptAction());
    const { data: rawData } = await windev.makeApiCall('/society', 'get', { society_id });
    /**
     * Response normalization should be handled by the BE
     * during after V0
     */
    const data = normalizeData(rawData);

    /**
     * TODO: Might need investigation
     *
     * Select company has to be set first
     * Otherwise, we get redux sync errors dispatched
     */
    await dispatch(selectCompanyAction(data));
    await dispatch(autoFillFormValues(formName, data));
    await dispatch(getCompanySuccessAction());

    return data;
  } catch (err) {
    await dispatch(getCompanyFailAction());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteSociety = id => async (dispatch) => {
  try {
    await dispatch(deleteCompanySuccessAction());
    const { data } = await windev.makeApiCall(`society/${id}`, 'delete');
    await dispatch(deleteCompanyAttemptAction());
    await dispatch(getCompanyList());
    return data;
  } catch (err) {
    await dispatch(deleteCompanyFailAction());
    await dispatch(handleError(err));
    return err;
  }
};
