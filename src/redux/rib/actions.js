import {
  GET_RIBS_SUCCESS,
  GET_RIBS_ATTEMPT,
  GET_RIBS_FAIL,

  SELECT_RIB,
  ADD_RIB,
  REMOVE_RIB,
  RESET_ALL_RIB,
  ADD_ALL_RIB,

  DELETE_RIB_ATTEMPT,
  DELETE_RIB_SUCCESS,
  DELETE_RIB_FAIL,

  GET_DIARIES_FAIL,
  GET_DIARIES_ATTEMPT,
  GET_DIARIES_SUCCESS,

  CREATE_RIB_ATTEMPT,
  CREATE_RIB_FAIL,
  CREATE_RIB_SUCCESS,

  UPDATE_RIB_FAIL,
  UPDATE_RIB_ATTEMPT,
  UPDATE_RIB_SUCCESS

} from './constants';

const getRibsFail = () => ({
  type: GET_RIBS_FAIL
});
const getRibsAttempt = () => ({
  type: GET_RIBS_ATTEMPT
});
const getRibsSuccess = payload => ({
  type: GET_RIBS_SUCCESS,
  payload
});

const selectRib = ribSelected => ({
  type: SELECT_RIB,
  ribSelected
});

const addRib = (rib, society_id) => ({
  type: ADD_RIB,
  rib,
  society_id
});
const removeRib = (rib, society_id) => ({
  type: REMOVE_RIB,
  rib,
  society_id
});
const addAllRib = society_id => ({
  type: ADD_ALL_RIB,
  society_id
});
const resetAllRib = society_id => ({
  type: RESET_ALL_RIB,
  society_id
});

const deleteRibAttempt = society_id => ({
  type: DELETE_RIB_ATTEMPT,
  society_id
});

const deleteRibFail = society_id => ({
  type: DELETE_RIB_FAIL,
  society_id
});

const deleteRibSuccess = (ribList, rib_id) => ({
  type: DELETE_RIB_SUCCESS,
  rib_id,
  ribList
});

const getDiariesFail = () => ({
  type: GET_DIARIES_FAIL
});

const getDiariesAttempt = () => ({
  type: GET_DIARIES_ATTEMPT
});

const getDiariesSuccess = payload => ({
  type: GET_DIARIES_SUCCESS,
  payload
});

const createRibFail = () => ({
  type: CREATE_RIB_FAIL
});

const createRibAttempt = () => ({
  type: CREATE_RIB_ATTEMPT
});
const createRibSuccess = payload => ({
  type: CREATE_RIB_SUCCESS,
  payload
});

const updateRibFail = () => ({
  type: UPDATE_RIB_FAIL
});
const updateRibAttempt = () => ({
  type: UPDATE_RIB_ATTEMPT
});
const updateRibSuccess = () => ({
  type: UPDATE_RIB_SUCCESS
});
export default {
  getRibsFail,
  getRibsAttempt,
  getRibsSuccess,
  selectRib,
  addRib,
  removeRib,
  addAllRib,
  resetAllRib,
  deleteRibSuccess,
  deleteRibFail,
  deleteRibAttempt,
  getDiariesFail,
  getDiariesAttempt,
  getDiariesSuccess,
  createRibFail,
  createRibAttempt,
  createRibSuccess,
  updateRibFail,
  updateRibAttempt,
  updateRibSuccess
};
