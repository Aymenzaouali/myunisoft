export const GET_RIBS_FAIL = 'rib/GET_RIBS_FAIL';
export const GET_RIBS_ATTEMPT = 'rib/GET_RIBS_ATTEMPT';
export const GET_RIBS_SUCCESS = 'rib/GET_RIBS_SUCCESS';

export const SELECT_RIB = 'rib/SELECT_RIB';
export const ADD_RIB = 'rib/ADD_RIB';
export const REMOVE_RIB = 'rib/REMOVE_RIB';
export const RESET_ALL_RIB = 'rib/RESET_ALL_RIB';
export const ADD_ALL_RIB = 'rib/ADD_ALL_RIB';

export const DELETE_RIB_ATTEMPT = 'rib/DELETE_RIB_ATTEMPT';
export const DELETE_RIB_SUCCESS = 'rib/DELETE_RIB_SUCCESS';
export const DELETE_RIB_FAIL = 'rib/DELETE_RIB_FAIL';

export const GET_DIARIES_FAIL = 'rib/GET_DIARIES_FAIL';
export const GET_DIARIES_ATTEMPT = 'rib/GET_DIARIES_ATTEMPT';
export const GET_DIARIES_SUCCESS = 'rib/GET_DIARIES_SUCCESS';

export const CREATE_RIB_ATTEMPT = 'rib/CREATE_RIB_ATTEMPT';
export const CREATE_RIB_FAIL = 'rib/CREATE_RIB_FAIL';
export const CREATE_RIB_SUCCESS = 'rib/CREATE_RIB_SUCCESS';

export const UPDATE_RIB_FAIL = 'rib/UPDATE_RIB_FAIL';
export const UPDATE_RIB_ATTEMPT = 'rib/UPDATE_RIB_ATTEMPT';
export const UPDATE_RIB_SUCCESS = 'rib/UPDATE_RIB_SUCCESS';
