import _ from 'lodash';
import {
  GET_RIBS_SUCCESS,
  GET_RIBS_ATTEMPT,
  GET_RIBS_FAIL,

  SELECT_RIB,
  ADD_RIB,
  REMOVE_RIB,
  RESET_ALL_RIB,
  ADD_ALL_RIB,

  GET_DIARIES_FAIL,
  GET_DIARIES_ATTEMPT,
  GET_DIARIES_SUCCESS,

  CREATE_RIB_ATTEMPT,
  CREATE_RIB_FAIL,
  CREATE_RIB_SUCCESS,

  UPDATE_RIB_FAIL,
  UPDATE_RIB_ATTEMPT,
  UPDATE_RIB_SUCCESS
} from './constants';

const initialState = {
  selectedRib: {},
  ribList: [],
  ribSelected: {},
  diariesList: [],
  rib: [],
  isLoading: false,
  isError: false
};


const rib = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_RIBS_FAIL: {
      return {
        ...state,
        ribList: [],
        isLoading: false,
        isError: true
      };
    }
    case GET_RIBS_ATTEMPT: {
      return {
        ...state,
        isLoading: true,
        isError: false
      };
    }
    case GET_RIBS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        ribList: action.payload
      };
    }
    case SELECT_RIB: {
      return {
        ...state,
        ribSelected: action.ribSelected
      };
    }
    case ADD_RIB: {
      return {
        ...state,
        selectedRib: {
          ...state.selectedRib,
          [action.society_id]: [..._.get(state, `selectedRib.${action.society_id}`, []), action.rib]
        }
      };
    }
    case REMOVE_RIB: {
      return {
        ...state,
        selectedRib: {
          ...state.selectedRib,
          [action.society_id]: _.get(state, `selectedRib[${action.society_id}]`, []).filter(e => e !== action.rib)
        }
      };
    }
    case RESET_ALL_RIB: {
      return {
        ...state,
        selectedRib: {
          ...state.selectedRib,
          [action.society_id]: []
        }
      };
    }
    case ADD_ALL_RIB: {
      return {
        ...state,
        selectedRib: {
          ...state.selectedRib,
          [action.society_id]: _.get(state, 'ribList', [])
        }
      };
    }
    case GET_DIARIES_FAIL: {
      return {
        ...state,
        diariesList: []
      };
    }
    case GET_DIARIES_ATTEMPT: {
      return {
        ...state
      };
    }
    case GET_DIARIES_SUCCESS: {
      return {
        ...state,
        diariesList: action.payload
      };
    }
    case CREATE_RIB_ATTEMPT: {
      return {
        ...state
      };
    }
    case CREATE_RIB_FAIL: {
      return {
        ...state
      };
    }
    case CREATE_RIB_SUCCESS: {
      return {
        ...state
      };
    }
    case UPDATE_RIB_FAIL: {
      return {
        ...state
      };
    }
    case UPDATE_RIB_ATTEMPT: {
      return {
        ...state
      };
    }
    case UPDATE_RIB_SUCCESS: {
      return {
        ...state,
        rib: action.payload
      };
    }
    default:
      return state;
    }
  }
  return state;
};

export default rib;
