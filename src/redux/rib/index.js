import { handleError } from 'common/redux/error';
import { windev } from 'helpers/api';
import _ from 'lodash';
import { getFormValues } from 'redux-form';
import actions from './actions';

export const getRibsList = society_id => async (dispatch) => {
  try {
    await dispatch(actions.getRibsAttempt());
    const { data } = await windev.makeApiCall('/rib', 'get', { society_id });
    await dispatch(actions.getRibsSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getRibsFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const deleteRib = (ribList, ribDeleteList) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  const rib_id = ribDeleteList.map(ids => ids.rib_id);
  const body = {
    society_id,
    rib_id
  };
  try {
    await dispatch(actions.deleteRibAttempt(society_id));
    const { data } = await windev.makeApiCall('/rib', 'delete', {}, body);
    await dispatch(actions.deleteRibSuccess(ribList, body));
    await dispatch(getRibsList(society_id));
    return data;
  } catch (err) {
    await dispatch(actions.deleteRibFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const getDiariesList = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  const type = 'BQ';
  try {
    await dispatch(actions.getDiariesAttempt());
    const { data } = await windev.makeApiCall('/diary', 'get', { society_id, type });
    await dispatch(actions.getDiariesSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getDiariesFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const createRib = rib => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  const body = {
    society_id,
    ...rib
  };
  try {
    await dispatch(actions.createRibAttempt());
    const { data } = await windev.makeApiCall('/rib', 'post', {}, body);
    await dispatch(actions.createRibSuccess(data));
    await dispatch(getRibsList(society_id));
    return data;
  } catch (err) {
    await dispatch(actions.createRibFail());
    await dispatch(handleError(err));
    return err;
  }
};

export const putRib = rib_id => async (dispatch, getState) => {
  const state = getState();
  const society_id = _.get(state, 'navigation.id', false);
  const rib_form = getFormValues('ribEdit')(state);
  const body = {
    ...rib_form,
    society_id
  };
  try {
    await dispatch(actions.updateRibAttempt());
    const { data } = await windev.makeApiCall(`/rib/${rib_id}`, 'put', {}, body);
    await dispatch(actions.updateRibSuccess(data));
    await dispatch(getRibsList(society_id));
    return data;
  } catch (err) {
    await dispatch(actions.updateRibFail());
    await dispatch(handleError(err));
    return err;
  }
};
