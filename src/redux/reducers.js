import { combineReducers } from 'redux';
import _ from 'lodash';
import { SIGN_OUT_SUCCESS } from 'common/redux/login/constants';

import commonReducers from 'common/redux';
import navigation from 'redux/navigation/reducers';
import dashboardWeb from 'redux/dashboard/reducers';
import accountingWeb from 'redux/accounting/reducers';
import chartAccountsWeb from 'redux/chartAccounts/reducers';
import ocrWeb from 'redux/ocr/reducers';
import documentWeb from 'redux/document/reducers';
import consulting from 'redux/consulting/reducers';
import accountCreationWeb from 'redux/accountCreation/reducers';
import tva from 'redux/tva/reducers';
import accountingPlans from 'redux/accountingPlans/reducers';
import planDetails from 'redux/planDetails/reducers';
import tax from 'redux/tax/reducers';
import balance from 'redux/balance/reducers';
import discussions from 'common/redux/discussions/reducers';
import lettrage from 'redux/lettrage/reducers';
import cycle from 'redux/cycle/reducers';
import fixedAssets from 'redux/fixedAssets/reducers';
import workingPage from 'redux/workingPage/reducers';
import collector from 'redux/collector/reducers';
import bankLink from 'redux/bankLink/reducers';
import dialogs from 'redux/dialogs/reducer';
import rib from 'redux/rib/reducers';
import rights from 'redux/rights/reducers';
import docManagement from 'redux/docManagement/reducers';
import connector from 'redux/connector/reducers';
import diary from 'redux/diary/reducers';
import watermarks from 'redux/watermarks/reducers';
import windows from 'redux/windows/reducers';
import settingsWorksheets from 'redux/settingsWorksheets/reducers';
import tables from 'redux/tables/reducers';
import groupFunctions from 'redux/function/reducers';
import letters from 'redux/letters/reducers';
import portfolioListSettings from 'redux/portfolioListSettings/reducers';
import bankIntegrationSettings from 'redux/bankIntegrationSettings/reducers';
import reportsAndForms from 'redux/reportsAndForms/reducers';
import successDialog from 'redux/successDialog/reducers';
import settingStandardMail from 'redux/settingStandardMail/reducers';
import accountingFirmSettings from 'redux/accountingFirmSettings/reducers';
import scanAssociate from 'redux/scanAssociate/reducers';
import autoReverseDialog from 'redux/autoReverseDialog/reducers';
import pdfForms from 'redux/pdfForms/reducers';
import bundle from 'redux/bundle/reducers';
import statements from 'redux/statements/reducers';
import ediDialog from 'redux/ediDialog/reducers';
import comments from 'redux/comments/reducers';
import memberGroup from 'redux/dynamicLinks/reducers';
import rentDeclarations from 'redux/rentDeclarations/reducers';

import tabs from './tabs/reducers';

const reducers = combineReducers({
  ...commonReducers,
  fixedAssets,
  navigation,
  dashboardWeb,
  accountingWeb,
  chartAccountsWeb,
  ocrWeb,
  documentWeb,
  consulting,
  accountCreationWeb,
  tva,
  accountingPlans,
  planDetails,
  balance,
  discussions,
  lettrage,
  cycle,
  collector,
  bankLink,
  dialogs,
  rib,
  rights,
  workingPage,
  docManagement,
  connector,
  diary,
  tables,
  watermarks,
  windows,
  groupFunctions,
  portfolioListSettings,
  settingsWorksheets,
  bankIntegrationSettings,
  reportsAndForms,
  // notifier,
  letters,
  tax,
  settingStandardMail,
  successDialog,
  scanAssociate,
  accountingFirmSettings,
  pdfForms,
  autoReverseDialog,
  bundle,
  statements,
  ediDialog,
  comments,
  memberGroup,
  rentDeclarations,

  tabs: () => ({}) // to remove a warning
});

const appReducer = (state = { }, action) => ({
  ...reducers(state, action),
  tabs: tabs(state.tabs, action, _.get(state, 'navigation.id'))
});

/**
 * @description Reset state when logout
 */
const rootReducer = (state, action) => {
  if (action.type === SIGN_OUT_SUCCESS) {
    state = undefined; // eslint-disable-line
  }

  return appReducer(state, action);
};

export default rootReducer;
