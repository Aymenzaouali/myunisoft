import {
  GET_LAND_TENURE_ATTEMPT,
  GET_LAND_TENURE_SUCCESS,
  GET_LAND_TENURE_FAIL,
  GET_RENT_DECLARATIONS_ATTEMPT,
  GET_RENT_DECLARATIONS_SUCCESS,
  GET_RENT_DECLARATIONS_FAIL,
  SEND_RENT_DECLARATIONS_ATTEMPT,
  SEND_RENT_DECLARATIONS_SUCCESS,
  SEND_RENT_DECLARATIONS_FAIL,
  SEND_EDI_RENT_DECLARATIONS_ATTEMPT,
  SEND_EDI_RENT_DECLARATIONS_SUCCESS,
  SEND_EDI_RENT_DECLARATIONS_FAIL,
  GET_BINARY_RENT_DECLARATIONS_ATTEMPT,
  GET_BINARY_RENT_DECLARATIONS_SUCCESS,
  GET_BINARY_RENT_DECLARATIONS_FAIL,
  SET_SELECTED_REND_DECLARATIONS_FORM
} from './constants';

const getLandTenureAttempt = societyId => ({
  type: GET_LAND_TENURE_ATTEMPT,
  societyId
});

const getLandTenureSuccess = (landTenureList, societyId) => ({
  type: GET_LAND_TENURE_SUCCESS,
  societyId,
  landTenureList
});

const getLandTenureFail = societyId => ({
  type: GET_LAND_TENURE_FAIL,
  societyId
});

const getRentDeclarationsAttempt = societyId => ({
  type: GET_RENT_DECLARATIONS_ATTEMPT,
  societyId
});

const getRentDeclarationsSuccess = (rentDeclarationsForms, societyId) => ({
  type: GET_RENT_DECLARATIONS_SUCCESS,
  societyId,
  rentDeclarationsForms
});

const getRentDeclarationsFail = societyId => ({
  type: GET_RENT_DECLARATIONS_FAIL,
  societyId
});

const sendRentDeclarationsAttempt = societyId => ({
  type: SEND_RENT_DECLARATIONS_ATTEMPT,
  societyId
});

const sendRentDeclarationsSuccess = societyId => ({
  type: SEND_RENT_DECLARATIONS_SUCCESS,
  societyId
});

const sendRentDeclarationsFail = societyId => ({
  type: SEND_RENT_DECLARATIONS_FAIL,
  societyId
});

const sendEdiRentDeclarationsAttempt = societyId => ({
  type: SEND_EDI_RENT_DECLARATIONS_ATTEMPT,
  societyId
});

const sendEdiRentDeclarationsSuccess = (rentDeclarationsForms, societyId) => ({
  type: SEND_EDI_RENT_DECLARATIONS_SUCCESS,
  societyId
});

const sendEdiRentDeclarationsFail = societyId => ({
  type: SEND_EDI_RENT_DECLARATIONS_FAIL,
  societyId
});

const getBinaryRentDeclarationsAttempt = societyId => ({
  type: GET_BINARY_RENT_DECLARATIONS_ATTEMPT,
  societyId
});

const getBinaryRentDeclarationsSuccess = (file, societyId) => ({
  type: GET_BINARY_RENT_DECLARATIONS_SUCCESS,
  societyId,
  file
});

const getBinaryRentDeclarationsFail = societyId => ({
  type: GET_BINARY_RENT_DECLARATIONS_FAIL,
  societyId
});

const setSelectedRentDeclarationsForm = (societyId, id_sheet) => ({
  type: SET_SELECTED_REND_DECLARATIONS_FORM,
  societyId,
  id_sheet
});

export default {
  getLandTenureAttempt,
  getLandTenureSuccess,
  getLandTenureFail,
  getRentDeclarationsAttempt,
  getRentDeclarationsSuccess,
  getRentDeclarationsFail,
  sendRentDeclarationsAttempt,
  sendRentDeclarationsSuccess,
  sendRentDeclarationsFail,
  sendEdiRentDeclarationsAttempt,
  sendEdiRentDeclarationsSuccess,
  sendEdiRentDeclarationsFail,
  getBinaryRentDeclarationsAttempt,
  getBinaryRentDeclarationsSuccess,
  getBinaryRentDeclarationsFail,
  setSelectedRentDeclarationsForm
};
