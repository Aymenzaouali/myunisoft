import {
  GET_LAND_TENURE_ATTEMPT,
  GET_LAND_TENURE_SUCCESS,
  GET_LAND_TENURE_FAIL,
  GET_RENT_DECLARATIONS_ATTEMPT,
  GET_RENT_DECLARATIONS_SUCCESS,
  GET_RENT_DECLARATIONS_FAIL,
  SET_SELECTED_REND_DECLARATIONS_FORM
} from './constants';

const initialeState = {};

const rentDeclarations = (state = initialeState, action) => {
  const {
    societyId,
    landTenureList,
    rentDeclarationsForms,
    id_sheet
  } = action;

  switch (action.type) {
  case GET_LAND_TENURE_ATTEMPT:
    return {
      ...state,
      forms: {
        [societyId]: {
          list: []
        }
      }
    };
  case GET_LAND_TENURE_SUCCESS:
    return {
      ...state,
      landTenure: {
        [societyId]: {
          list: landTenureList
        }
      }
    };
  case GET_LAND_TENURE_FAIL:
    return {
      ...state,
      forms: {
        [societyId]: {
          list: []
        }
      }
    };
  case GET_RENT_DECLARATIONS_ATTEMPT:
    return {
      ...state,
      forms: {
        [societyId]: {
          list: [],
          isLoading: true,
          isError: false
        }
      }
    };
  case GET_RENT_DECLARATIONS_SUCCESS:
    return {
      ...state,
      forms: {
        [societyId]: {
          list: rentDeclarationsForms,
          isLoading: false,
          isError: false
        }
      }
    };
  case GET_RENT_DECLARATIONS_FAIL:
    return {
      ...state,
      forms: {
        [societyId]: {
          list: [],
          isLoading: false,
          isError: true
        }
      }
    };
  case SET_SELECTED_REND_DECLARATIONS_FORM:
    return {
      ...state,
      selectedForm: {
        [societyId]: {
          id_sheet
        }
      }
    };
  default:
    return state;
  }
};

export default rentDeclarations;
