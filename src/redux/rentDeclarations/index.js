import { get as _get, isEmpty as _isEmpty } from 'lodash';
import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import { getFormValues, initialize } from 'redux-form';
import { printOrDownload } from 'helpers/file';
import actions from './actions';

export const getLandTenure = () => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);

  try {
    await dispatch(actions.getLandTenureAttempt(society_id));
    const { data } = await windev.makeApiCall('/liasse/table_edi', 'get', {
      code_sheet_group: 'DECLOYER'
    });
    await dispatch(actions.getLandTenureSuccess(data, society_id));
    return true;
  } catch (error) {
    await dispatch(actions.getLandTenureSuccess(society_id));
    await dispatch(handleError(error));
    return error;
  }
};

export const getRentDeclarationsForms = exercice_id => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);

  const formName = `${society_id}_rentDeclarations_DECLOYER`;
  dispatch(initialize(formName));
  try {
    await dispatch(actions.getRentDeclarationsAttempt(society_id));
    const { data } = await windev.makeApiCall('/liasse', 'get', {
      exercice_id,
      code_sheet_group: 'DECLOYER'
    });

    if (data) {
      const formData = _get(data.filter(form => form.name === 'DECLOYER'), '[0]');
      if (!_isEmpty(formData)) {
        await dispatch(actions.setSelectedRentDeclarationsForm(society_id, formData.form_id));
        const landTenure = _get(state, `rentDeclarations.landTenure.${state.navigation.id}.list`, []);
        const ediLIst = _get(landTenure.filter(item => item.id_sheet === formData.form_id), '[0].edi_list', []);
        const CAList = _get(ediLIst.filter((item => item.code === 'CA')), '[0].tab', []);
        const { ...updatedForm } = _get(formData, 'data');
        for (let i = 1; i < 14; i += 1) {
          if (updatedForm[`CA${i}`]) {
            updatedForm[`CA${i}`] = {
              value: updatedForm[`CA${i}`],
              label: _get(CAList.find(value => value.code_edi === updatedForm[`CA${i}`]), 'label_edi')
            };
          }
        }
        if (!_isEmpty(updatedForm)) {
          dispatch(initialize(`${society_id}_rentDeclarations_DECLOYER`, updatedForm));
        }
      }
    }
    await dispatch(actions.getRentDeclarationsSuccess(data, society_id));
    return true;
  } catch (error) {
    await dispatch(actions.getRentDeclarationsFail(society_id));
    await dispatch(handleError(error));
    return error;
  }
};

export const sendRentDeclarationsForms = exercice_id => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  const forms = _get(state, `rentDeclarations.forms.${society_id}.list`);
  const date_declare = _get(state, `form.${society_id}RentDeclarationsHeader.values.dateStart`);
  const sheet_list = [];
  try {
    await dispatch(actions.sendRentDeclarationsAttempt(society_id));
    const form = getFormValues(`${society_id}_rentDeclarations_DECLOYER`)(state);
    const { ...updatedForm } = form;
    for (let i = 1; i < 14; i += 1) {
      if (updatedForm[`CA${i}`]) {
        updatedForm[`CA${i}`] = updatedForm[`CA${i}`].value;
      }
    }
    sheet_list.push({
      id_sheet: _get(forms.filter(form => form.name === 'DECLOYER'), '[0].form_id'),
      field_list: updatedForm
    });
    await windev.makeApiCall('/liasse', 'post', { exercice_id },
      {
        date_declare,
        sheet_list,
        code_sheet_group: 'DECLOYER'
      });
    await dispatch(actions.sendRentDeclarationsSuccess(society_id));
    return true;
  } catch (error) {
    await actions.sendRentDeclarationsFail(society_id);
    await dispatch(handleError(error));
    return error;
  }
};

export const sendEdiRentDeclarationsForms = exercice_id => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  const date_declare = _get(state, `form.${society_id}RentDeclarationsHeader.values.dateStart`);
  try {
    await dispatch(actions.sendEdiRentDeclarationsAttempt(society_id));
    await windev.makeApiCall('/liasse', 'post',
      {
        society_id, exercice_id, code_sheet_group: 'DECLOYER', date_declare
      });
    await dispatch(actions.sendEdiRentDeclarationsSuccess(society_id));
    return true;
  } catch (error) {
    await actions.sendEdiRentDeclarationsFail(society_id);
    await dispatch(handleError(error));
    return error;
  }
};

export const printOrDownloadRentDeclarations = (exerciseId, type) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id', false);
  try {
    await dispatch(actions.getBinaryRentDeclarationsAttempt(society_id));
    const { data } = await windev.makeApiCall('/liasse/download', 'get', {
      exercice_id: exerciseId,
      code_sheet_group: 'DECLOYER'
    }, {}, { responseType: 'blob' });

    await dispatch(actions.getBinaryRentDeclarationsSuccess(data, society_id));

    printOrDownload(data, type, `${exerciseId}.pdf`);
    return data;
  } catch (err) {
    await dispatch(actions.getBinaryRentDeclarationsFail(society_id));
    await dispatch(handleError(err));
    return err;
  }
};
