import { handleError } from 'common/redux/error';
import _get from 'lodash/get';
import moment from 'moment';
import { windev } from 'helpers/api';
import { validateImmo } from 'helpers/validation';
import actions from './actions';

const parseImmo = immo => ({
  ...immo,
  purchase_date: immo.purchase_date && immo.purchase_date !== '0000-00-00' ? moment(immo.purchase_date).format('YYYY-MM-DD') : undefined,
  start_date: immo.start_date && immo.start_date !== '0000-00-00' ? moment(immo.start_date).format('YYYY-MM-DD') : undefined,
  end_date: immo.end_date && immo.end_date !== '0000-00-00' ? moment(immo.end_date).format('YYYY-MM-DD') : undefined
});
const parseAccount = account => ({
  ...account,
  arrayImmo: _get(account, 'arrayImmo', []).map(parseImmo)
});
const getAccounts = (societyId, dossierRevisionId) => async (dispatch) => {
  try {
    await dispatch(actions.getAccountsAttempt(societyId));
    const { data } = await windev.makeApiCall('/immo', 'get', {
      dossier_revision_id: dossierRevisionId
    });
    await dispatch(actions.getAccountsSuccess(data.map(parseAccount), societyId));
  } catch (err) {
    await dispatch(actions.getAccountsFail(societyId));
    await dispatch(handleError(err));
  }
};

const unParseImmo = immo => ({
  id_immo: immo.isCreated ? undefined : immo.id_immo,
  isCreated: undefined,
  linkedTo: undefined,
  id_account: immo.id_account,
  id_type_amort: immo.id_type_amort,
  provider: immo.provider,
  label: immo.label,
  purchase_date: immo.purchase_date,
  purchase_value: immo.purchase_value,
  month: immo.month,
  start_date: immo.start_date?.split('/').join('-') || immo.purchase_date,
  end_date: immo.end_date || null,
  sale_value: immo.sale_value
});


const saveImmos = () => async (dispatch, getState) => {
  const state = getState();
  const societyId = _get(state, 'navigation.id');
  const selectedAccount = _get(state, `fixedAssets.${societyId}.selectedAccount`, {});
  const createdImmos = Object.values(_get(state, `fixedAssets.${societyId}.createdImmos`, {})).filter(v => !!v);
  const editedImmos = Object.values(_get(state, `fixedAssets.${societyId}.editedImmos`, {})).filter(v => !!v);
  const dossierRevisionId = _get(state, `tabs.${societyId}.dadp.reviews.selectedId`);
  const immos = [...createdImmos, ...editedImmos];
  const errors = immos.reduce((acc, immo) => {
    if (immo.id_account === selectedAccount.id_compte) {
      const immoErrors = validateImmo(immo);
      const hasError = Object.values(immoErrors).some(v => v);

      return {
        ...acc,
        [immo.id_immo]: immoErrors,
        hasError: acc.hasError || hasError
      };
    }

    return acc;
  }, {});

  if (errors.hasError) {
    await dispatch(actions.validateImmosFail(societyId, errors));
  } else {
    try {
      await dispatch(actions.saveImmosAttempt(societyId));
      const { data } = await windev.makeApiCall('/immo', 'post', {}, immos.map(unParseImmo));
      const hasPostError = Array.isArray(data) && data.some(e => e.error > 0);

      if (hasPostError) {
        await dispatch(actions.saveImmosFail(societyId, data));
      } else {
        await dispatch(actions.saveImmosSuccess(societyId));
        if (createdImmos.length) {
          await dispatch(getAccounts(societyId, dossierRevisionId));
        }
      }
    } catch (err) {
      await dispatch(actions.saveImmosFail(societyId));
      await dispatch(handleError(err));
    }
  }
};

const deleteImmos = () => async (dispatch, getState) => {
  const state = getState();
  const societyId = _get(state, 'navigation.id');
  const createdImmos = _get(state, `fixedAssets.${societyId}.createdImmos`, {});
  const selectedImmos = _get(state, `fixedAssets.${societyId}.selectedImmos`, {});
  const ids = Object.keys(selectedImmos).reduce((acc, key) => {
    if (key && !Number.isNaN(key) && selectedImmos[key] && !createdImmos[key]) {
      acc.push(parseInt(key, 10));
    }

    return acc;
  }, []);

  try {
    await dispatch(actions.deleteImmosAttempt(societyId));
    let hasError = false;

    if (ids.length) {
      const response = await windev.makeApiCall('/immo', 'delete', {}, ids);
      hasError = Array.isArray(response) && response.some(e => e.error > 0);
    }
    if (hasError) {
      await dispatch(actions.deleteImmosFail(societyId));
    } else {
      await dispatch(actions.deleteImmosSuccess(societyId));
    }
  } catch (err) {
    await dispatch(actions.deleteImmosFail(societyId));
    await dispatch(handleError(err));
  }
};

const sendImmoPJ = (files, id_immo) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id');
  try {
    const params = {
      location: 'IMMO',
      filename_extension: files[0].name,
      object_id: id_immo
    };
    await windev.makeApiCall('/document/add_all_types', 'post', params, {},
      { headers: { 'Content-Type': 'application/octet-stream', 'society-id': society_id } });
    await dispatch(actions.addImmoPJ(id_immo, files, society_id));
  } catch (err) {
    console.log('fixedAssets.addDiligencePJ', err);
    throw err;
  }
};

const getImmoComments = immo_id => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id');

  try {
    await dispatch(actions.getCommentsAttempt(society_id));
    const { data } = await windev.makeApiCall(
      '/comments',
      'get',
      {
        ...immo_id
      }
    );
    await dispatch(actions.getCommentsSuccess({
      ...immo_id,
      society_id,
      comments: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.getCommentsFail(society_id));
    return error;
  }
};

const postFixedAssetsComment = (comment, immo_id) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id');

  try {
    await dispatch(actions.postCommentAttempt(society_id));

    const {
      body,
      attachTo
    } = comment;

    const { data } = await windev.makeApiCall(
      '/comments',
      'post',
      {},
      {
        location: 'IMMO',
        immo_id,
        society_id,
        comment: body,
        attachTo
      }
    );

    await dispatch(actions.postCommentSuccess({
      immo_id,
      society_id,
      comment: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.postCommentFail(society_id));
    return error;
  }
};

const getDiligenceComments = diligence_id => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id');

  try {
    await dispatch(actions.getCommentDiligenceAttempt(society_id));
    const { data } = await windev.makeApiCall(
      '/comments',
      'get',
      {
        diligence_id
      }
    );
    await dispatch(actions.getCommentDiligenceSuccess(
      society_id,
      data,
    ));
    return data;
  } catch (error) {
    await dispatch(actions.getCommentDiligenceFail(society_id));
    return error;
  }
};

const postDiligenceComments = (comment, diligence_id) => async (
  dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id');
  try {
    await dispatch(actions.postCommentDiligenceAtempt(society_id));

    const {
      body,
      attachTo
    } = comment;

    const { data } = await windev.makeApiCall(
      '/comments',
      'post',
      {},
      {
        location: 'DILIGENCE',
        diligence_id,
        society_id,
        comment: body,
        attachTo
      }
    );

    await dispatch(actions.postCommentDiligenceSuccess(society_id, diligence_id, {
      society_id,
      comments: data
    }));
    return data;
  } catch (error) {
    await dispatch(actions.postCommentFail(society_id));
    return error;
  }
};

const putComment = (comment, ownProps) => async (dispatch, getState) => {
  const state = getState();
  const society_id = _get(state, 'navigation.id');
  const { body, id: entry_id } = comment;
  const { diligence_id } = ownProps;
  const location = 'DILIGENCE';
  try {
    await dispatch(actions.postCommentDiligenceAtempt(society_id));

    const { data } = await windev.makeApiCall(
      `/comments/${society_id}`,
      'put',
      {},
      {
        location,
        diligence_id,
        entry_id,
        comment: body
        // attachTo
      }
    );

    await dispatch(
      actions.putCommentDiligenceSuccess(society_id, diligence_id, {
        society_id,
        comments: data
      })
    );
  } catch (error) {
    actions.putCommentDiligenceFail(society_id);
  }
};

const getSale = () => async (dispatch, getState) => {
  const state = getState();
  const societyId = _get(state, 'navigation.id');
  // const selectedAccount = _get(state, `fixedAssets.${societyId}.selectedAccount.value`, null);
  // const lastSelectedImmo = _get(state, `fixedAssets.${societyId}.lastSelectedImmo`, null);
  const fixedAssets = state.fixedAssets[societyId];
  const { lastSelectedImmo: immoId, selectedAccount = {} } = fixedAssets;
  const { id_line_entry_purchase = 0 } = selectedAccount.arrayImmo.find(
    ({ id_immo }) => id_immo === immoId
  );
  try {
    await dispatch(actions.getSaleAttempt(societyId));
    const {
      data
    } = await windev.makeApiCall(
      'entry/line_entry',
      'get',
      {
        society_id: societyId,
        line_entry_id: id_line_entry_purchase
      },
    );
    await dispatch(actions.getSaleSuccess(societyId, data));
  } catch (err) {
    await dispatch(actions.getSaleFail(societyId));
    await dispatch(handleError(err));
  }
};

const postSale = () => async (dispatch, getState) => {
  const state = getState();
  const societyId = _get(state, 'navigation.id');
  const fixedAssets = state.fixedAssets[societyId];
  const { lastSelectedImmo: immoId, sales = [] } = fixedAssets;
  const lineEntryId = sales.selectedSale.line_entry_id;
  await windev.makeApiCall(
    `immo/${immoId}/vente/${lineEntryId}`,
    'post',
    {},
    {},
    {
      header: {
        'Content-Type': 'application/json'
      }
    }
  );
};


export default {
  postFixedAssetsComment,
  sendImmoPJ,
  postDiligenceComments,
  putComment,
  getAccounts,
  deleteImmos,
  saveImmos,
  getImmoComments,
  getDiligenceComments,
  getSale,
  postSale
};
