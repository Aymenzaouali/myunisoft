import _get from 'lodash/get';
import _isEqual from 'lodash/isEqual';
import { commentsWrap } from 'helpers/dadp';
import {
  GET_COMMENTS_ATTEMPT,
  GET_COMMENTS_FAIL,
  GET_ACCOUNTS_ATTEMPT,
  GET_ACCOUNTS_SUCCESS,
  GET_ACCOUNTS_FAIL,
  START_EDIT,
  END_EDIT,
  SELECT_ACCOUNT,
  UNSELECT_ACCOUNT,
  SELECT_IMMO,
  EDIT_IMMO,
  UNSELECT_IMMO,
  ADD_IMMO_LINE,
  ADD_IMMO_LINES_VALIDER,
  SELECT_DOTATION,
  UNSELECT_DOTATION,
  DELETE_IMMOS_ATTEMPT,
  DELETE_IMMOS_SUCCESS,
  DELETE_IMMOS_FAIL,
  CANCEL,
  SAVE_IMMOS_ATTEMPT,
  SAVE_IMMOS_SUCCESS,
  SAVE_IMMOS_FAIL,
  VALIDATE_IMMOS_FAIL,
  SET_ERRORS,
  OPEN_DIALOG,
  CLOSE_DIALOG,
  ADD_IMMO_PJ,
  GET_COMMENTS_SUCCESS,
  POST_DILIGENCE_COMMENT_SUCCESS,
  POST_DILIGENCE_COMMENT_ATTEMPT,
  GET_DILIGENCE_COMMENT_ATTEMPT,
  GET_DILIGENCE_COMMENT_SUCCESS,
  GET_DILIGENCE_COMMENT_FAIL,
  POST_COMMENT_SUCCESS,
  POST_COMMENT_ATTEMPT,
  EDIT_IMMO_VALIDER,
  GET_SALE_ATTEMPT,
  GET_SALE_SUCCESS,
  GET_SALE_FAIL,
  SELECT_SALE,
  UNSELECT_SALE,
  SALE_MODAL_SUBMIT,
  SHOW_EARLY_START_DATE_MODAL,
  PUT_DILIGENCE_COMMENT_ATTEMPT,
  PUT_DILIGENCE_COMMENT_SUCCESS,
  PUT_DILIGENCE_COMMENT_FAIL
} from './constants';

const initial_comments = {
  loading: false,
  updating: false,
  error: false,
  data: {}
};
const defaultState = {
  initialAccounts: [],
  accounts: [],
  selectedAccount: null,
  selectedImmos: {},
  lastSelectedImmo: null,
  isEditing: false,
  editedImmos: {},
  createdImmos: {},
  deletedImmos: {},
  selectedDotations: {},
  createdImmoCount: 0,
  isLoading: false,
  isDeleteLoading: false,
  isEditLoading: false,
  isError: false,
  isSaveLoading: false,
  isSaveError: false,
  errors: {},
  toDelete: {},
  comments: {
    fixedAssets: {},
    diligence: {}
  },
  diligences: {},
  sales: {
    isSaleLoading: false,
    isSaleError: false,
    isValided: false
  },
  dialog: {}
};

const defaultImmo = {
  id_account: undefined,
  id_type_amort: 1,
  provider: undefined,
  label: undefined,
  purchase_date: null,
  purchase_value: 0,
  previousDotation: 0,
  currentAnnualDotation: 0,
  EndingAnnualDotation: 0,
  netBookValue: 0,
  capitalGain: 0,
  month: 0,
  start_date: null,
  end_date: null,
  sale_value: 0
};

const fixedAssets = (state = {}, action) => {
  const currentState = state[action.societyId];
  const initialAccounts = _get(currentState, 'initialAccounts', []);
  const accounts = _get(currentState, 'accounts', []);
  const selectedAccount = _get(currentState, 'selectedAccount', null);
  const selectedImmos = _get(currentState, 'selectedImmos', {});
  const lastSelectedImmo = _get(currentState, 'lastSelectedImmo', null);
  const toDelete = _get(currentState, 'toDelete', {});
  const createdImmos = _get(currentState, 'createdImmos', {});
  const editedImmos = _get(currentState, 'editedImmos', {});
  const deletedImmos = _get(currentState, 'deletedImmos', {});
  const createdImmoCount = _get(currentState, 'createdImmoCount', 0);
  const newImmoId = `createdImmo${createdImmoCount}`;
  const selectedDotations = _get(currentState, 'selectedDotations', {});
  const errors = _get(currentState, 'errors', {});
  const firstSelectedImmo = currentState && currentState.selectedImmos
    && Object.keys(currentState.selectedImmos).find(key => currentState.selectedImmos[key]);

  switch (action.type) {
  case CANCEL:
    return {
      ...state,
      [action.societyId]: {
        ...defaultState,
        accounts: initialAccounts,
        initialAccounts,
        selectedAccount
      }
    };
  case START_EDIT:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        isEditing: true,
        lastSelectedImmo: lastSelectedImmo || firstSelectedImmo
      }
    };
  case END_EDIT:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        isEditing: false
      }
    };
  case SELECT_ACCOUNT:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        selectedAccount: action.account,
        selectedImmos: {}
      }
    };
  case UNSELECT_ACCOUNT:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        selectedAccount: null,
        selectedImmos: {},
        selectedDotation: null,
        isEditing: false
      }
    };
  case SELECT_IMMO:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        selectedImmos: {
          ...selectedImmos,
          [action.immo.id_immo]: true
        },
        lastSelectedImmo: action.immo.id_immo
      }
    };
    // The edition duplicate the line if the purchase_value is changed and less than initial value
  case EDIT_IMMO:
    const currentImmo = selectedAccount
      .arrayImmo.find(immo => immo.id_immo === action.immo.id_immo);
    const isEdited = !_isEqual(currentImmo, action.immo);
    if (!isEdited) return state;

    const newId2 = `createdImmo${createdImmoCount + 1}`;
    const resultImmos = selectedAccount.arrayImmo.reduce((acc, immo) => {
      if (immo.id_immo === action.immo.id_immo) {
        if (action.purchase_value && immo.purchase_value > action.immo.purchase_value) {
          const createdImmo = {
            ...action.immo,
            id_immo: newImmoId,
            isCreated: true,
            linkedTo: action.immo.linkedTo || action.immo.id_immo,
            purchase_value: immo.purchase_value - action.immo.purchase_value
          };

          const createdImmo2 = {
            ...action.immo,
            id_immo: newId2,
            isCreated: true,
            linkedTo: action.immo.linkedTo || action.immo.id_immo
          };

          acc.immos.push(createdImmo);
          acc.immos.push(createdImmo2);
          acc.createdImmos[action.immo.id_immo] = null;
          acc.createdImmos[newImmoId] = createdImmo;
          acc.createdImmos[newId2] = createdImmo2;
          acc.withCreation = true;
        } else {
          acc.immos.push(action.immo);
        }
      } else {
        acc.immos.push(immo);
      }

      return acc;
    }, { immos: [], createdImmos: { ...createdImmos }, withCreation: false });
    const editedAccount = {
      ...selectedAccount,
      arrayImmo: resultImmos.immos
    };

    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        accounts: accounts.map(
          account => (
            account.id_compte === selectedAccount.id_compte
              ? editedAccount
              : account
          )
        ),
        selectedAccount: editedAccount,
        editedImmos: action.immo.isCreated || resultImmos.withCreation ? editedImmos : {
          ...editedImmos,
          [action.immo.id_immo]: action.immo
        },
        createdImmos: !action.immo.isCreated || resultImmos.withCreation
          ? resultImmos.createdImmos
          : {
            ...createdImmos,
            ...resultImmos.createdImmos,
            [action.immo.id_immo]: action.immo
          },
        toDelete: !action.immo.linkedTo && !action.immo.isCreated
          ? { ...toDelete, [action.immo.id_immo]: true }
          : toDelete,
        selectedImmos: resultImmos.withCreation
          ? { ...selectedImmos, [newImmoId]: true, [newId2]: true }
          : selectedImmos,
        lastSelectedImmo: resultImmos.withCreation ? newImmoId : lastSelectedImmo,
        createdImmoCount: resultImmos.withCreation ? createdImmoCount + 2 : createdImmoCount
      }
    };
  case EDIT_IMMO_VALIDER:
    const newArrayImmo = selectedAccount.arrayImmo.map(
      immo => (immo.id_immo === action.immo.id_immo ? ({ ...immo, ...action.immo }) : immo)
    );

    const editedNewAccount = {
      ...selectedAccount,
      arrayImmo: newArrayImmo
    };

    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        accounts: accounts.map(
          account => (
            account.id_compte === selectedAccount.id_compte
              ? editedNewAccount
              : account
          )
        ),
        selectedAccount: editedNewAccount,
        editedImmos: {
          ...editedImmos,
          [action.immo.id_immo]: action.immo
        },
        toDelete: !action.immo.linkedTo && !action.immo.isCreated
          ? { ...toDelete, [action.immo.id_immo]: true }
          : toDelete,
        selectedImmos,
        lastSelectedImmo,
        createdImmoCount
      }
    };
  case UNSELECT_IMMO:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        selectedImmos: {
          ...selectedImmos,
          [action.immo.id_immo]: false
        },
        lastSelectedImmo: lastSelectedImmo !== action.immo.id_immo
          ? lastSelectedImmo
          : firstSelectedImmo,
        selectedDotation: null
      }
    };
  case ADD_IMMO_LINE:
    const newSelectedAccount = {
      ...selectedAccount,
      arrayImmo: [
        {
          ...defaultImmo,
          ...action.immo,
          id_immo: newImmoId,
          isCreated: true,
          id_account: selectedAccount && selectedAccount.id_compte
        },
        ...selectedAccount.arrayImmo
      ]
    };
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        isEditing: true,
        accounts: accounts.map(
          account => (
            account.id_compte === newSelectedAccount.id_compte
              ? newSelectedAccount
              : account
          )
        ),
        createdImmos: {
          ...createdImmos,
          [newImmoId]: true
        },
        selectedAccount: newSelectedAccount,
        selectedImmos: {
          ...selectedImmos,
          [newImmoId]: true
        },
        lastSelectedImmo: newImmoId,
        createdImmoCount: createdImmoCount + 1
      }
    };
  case ADD_IMMO_LINES_VALIDER:
    let newImmosId = 0;
    const newSelectedAccounts = {
      ...selectedAccount,
      arrayImmo: [
        ...action.newImmos.map((newImmo) => {
          newImmosId += 1;
          return {
            ...newImmo,
            immo_id: newImmosId,
            isCreated: true,
            isEditing: true
          };
        }),
        ...selectedAccount.arrayImmo
      ]
    };
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        isEditing: true,
        accounts: accounts.map(
          account => (
            account.id_compte === newSelectedAccounts.id_compte
              ? newSelectedAccounts
              : account
          )
        ),
        createdImmos: {
          ...createdImmos,
          [newImmoId]: true
        },
        selectedAccount: newSelectedAccounts,
        selectedImmos: {
          ...selectedImmos,
          [newImmoId]: true
        },
        lastSelectedImmo: newImmoId,
        createdImmoCount: createdImmoCount + 1
      }
    };
  case SELECT_DOTATION:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        selectedDotations: {
          ...selectedDotations,
          [action.dotation.id_amort]: true
        }
      }
    };
  case UNSELECT_DOTATION:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        selectedDotations: {
          ...selectedDotations,
          [action.dotation.id_amort]: false
        }
      }
    };
  case GET_ACCOUNTS_ATTEMPT:
    return {
      ...state,
      [action.societyId]: {
        ...defaultState,
        isLoading: true,
        isError: false
      }
    };
  case GET_ACCOUNTS_SUCCESS:
    const firstAccount = _get(action, 'data.0', null);
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        initialAccounts: action.data,
        accounts: action.data,
        selectedAccount: selectedAccount || firstAccount,
        selectedImmos: {},
        selectedDotation: null,
        isLoading: false,
        isError: false
      }
    };
  case GET_ACCOUNTS_FAIL:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        ...defaultState,
        isLoading: false,
        isError: true
      }
    };
  case DELETE_IMMOS_ATTEMPT:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        isDeleteLoading: true,
        isDeleteError: false
      }
    };
    // the deletion on duplicates add their puchase_value to the first linked immo
  case DELETE_IMMOS_SUCCESS:
    const result = selectedAccount.arrayImmo.reduce((acc, immo) => {
      if (!selectedImmos[immo.id_immo]) {
        if (immo.linkedTo) {
          const newPurchaseValue = selectedAccount.arrayImmo.reduce((v, i) => (
            i.linkedTo === immo.linkedTo && selectedImmos[i.id_immo]
              ? v + (i.purchase_value || 0)
              : v
          ), 0);
          acc.immos.push({
            ...immo,
            purchase_value: immo.purchase_value + newPurchaseValue
          });
        } else {
          acc.immos.push(immo);
        }
      } else {
        acc.editedImmos[immo.id_immo] = null;
        acc.createdImmos[immo.id_immo] = null;

        if (!createdImmos[immo.id_immo]) {
          acc.deletedImmos[immo.id_immo] = true;
        }
      }

      return acc;
    }, {
      immos: [],
      editedImmos: { ...editedImmos },
      createdImmos: { ...createdImmos },
      deletedImmos: {}
    });

    const accountWithDeletedImmo = {
      ...selectedAccount,
      arrayImmo: result.immos
    };
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        accounts: accounts.map(
          a => (a.id_compte === accountWithDeletedImmo.id_compte ? accountWithDeletedImmo : a)
        ),
        selectedAccount: accountWithDeletedImmo,
        deletedImmos: {
          ...deletedImmos,
          ...result.deletedImmos
        },
        editedImmos: result.editedImmos,
        selectedImmos: {},
        createdImmos: result.createdImmos,
        isDeleteLoading: false,
        isDeleteError: false
      }
    };
  case DELETE_IMMOS_FAIL:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        isDeleteLoading: false,
        isDeleteError: true
      }
    };
  case SET_ERRORS:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        errors: {
          ...currentState.errors,
          [action.id]: {
            ...currentState.errors[action.id],
            ...action.errors
          }
        },
        isSaveLoading: false,
        isSaveError: true
      }
    };
  case VALIDATE_IMMOS_FAIL:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        errors: action.errors,
        isSaveLoading: false,
        isSaveError: true
      }
    };
  case SAVE_IMMOS_ATTEMPT:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        errors: {},
        isSaveLoading: false,
        isSaveError: false
      }
    };
  case SAVE_IMMOS_SUCCESS:
    return {
      ...state,
      [action.societyId]: {
        ...defaultState,
        accounts,
        initialAccounts: accounts,
        selectedAccount,
        createdImmoCount
      }
    };
  case SAVE_IMMOS_FAIL:
    let index = 0;
    const saveActionErrors = action.errors;
    const createdImmosArray = Object.values(createdImmos);
    const editedImmosArray = Object.values(editedImmos);
    const sendedImmos = [...createdImmosArray, ...editedImmosArray];
    const saveErrors = saveActionErrors ? sendedImmos.reduce((acc, immo) => {
      if (saveActionErrors[index].error > 0) {
        const error = saveActionErrors[index];
        acc[immo.id_immo] = { ...errors[immo.id_immo], [_get(error, 'messages.path')]: _get(error, 'messages.message') };
        index += 1;
      }

      return acc;
    }, {}) : undefined;

    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        errors: { ...errors, ...saveErrors },
        isSaveLoading: false,
        isSaveError: true
      }
    };
  case ADD_IMMO_PJ:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        selectedAccount: {
          ...currentState.selectedAccount,
          arrayImmo: currentState.selectedAccount.arrayImmo.map(
            immo => (immo.id_immo === action.id_immo
              ? ({ ...immo, pj_list: [...action.files] })
              : immo)
          )
        }
      }
    };
  case OPEN_DIALOG:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        dialog: {
          ...action.dialog,
          isOpen: true
        }
      }
    };
  case CLOSE_DIALOG:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        dialog: {
          isOpen: false
        }
      }
    };
  case GET_COMMENTS_ATTEMPT:
    return {
      ...state,
      comments: {
        ...initial_comments,
        loading: true
      }
    };
  case GET_COMMENTS_SUCCESS:
    return {
      ...state,
      [action.comments.society_id]: {
        ...state[action.comments.society_id],
        comments: {
          ...state[action.comments.society_id].comments,
          loading: false,
          error: false,
          fixedAssets: {
            ...state[action.comments.society_id].comments.fixedAssets,
            data: commentsWrap(action.comments.comments)
          }
        }
      }
    };
  case GET_COMMENTS_FAIL:
    return {
      ...state,
      comments: {
        ...state.comments,
        loading: false,
        error: true
      }
    };
  case POST_COMMENT_ATTEMPT:
    return {
      ...state
    };
  case POST_COMMENT_SUCCESS:
    const { society_id } = action.payload;
    return {
      ...state,
      [society_id]: {
        ...state[society_id],
        comments: {
          ...state[society_id].comments,
          fixedAssets: {
            ...state[society_id].comments.fixedAssets,
            data: commentsWrap(action.payload.comment)
          },
          loading: false,
          error: false
        }
      }
    };
  case POST_DILIGENCE_COMMENT_SUCCESS:
  case PUT_DILIGENCE_COMMENT_SUCCESS:
    const { societyId, comments } = action;
    return {
      ...state,
      [societyId]: {
        ...state[societyId],
        comments: {
          ...state[societyId].comments,
          diligence: {
            ...state[societyId].comments.diligence,
            loading: false,
            error: false,
            data: commentsWrap(comments.comments)
          }
        }
      }
    };
  case POST_DILIGENCE_COMMENT_ATTEMPT:
    return {
      ...state,
      [action.societyId]: {
        ...state[action.societyId],
        comments: {
          ...state[action.societyId].comments,
          diligence: {
            ...state[action.societyId].comments.diligence,
            loading: true,
            error: true
          }
        }
      }

    };
  case PUT_DILIGENCE_COMMENT_ATTEMPT:
    return {
      ...state,
      [action.societyId]: {
        ...state[action.societyId],
        comments: {
          ...state[action.societyId].comments,
          diligence: {
            ...state[action.societyId].comments.diligence,
            loading: true,
            error: true
          }
        }
      }

    };
  case GET_DILIGENCE_COMMENT_ATTEMPT:
    return {
      ...state,
      [action.society_id]: {
        ...state[action.society_id],
        comments: {
          ...initial_comments,
          loading: true
        }
      }
    };
  case GET_DILIGENCE_COMMENT_SUCCESS:
    return {
      ...state,
      [action.society_id]: {
        ...state[action.society_id],
        comments: {
          ...state[action.society_id].comments,
          diligence: {
            ...state[action.society_id].comments.diligence,
            loading: false,
            error: false,
            data: commentsWrap(action.comments)
          }
        }
      }
    };
  case PUT_DILIGENCE_COMMENT_FAIL:
  case GET_DILIGENCE_COMMENT_FAIL:
    return {
      ...state,
      comments: {
        ...state.comments,
        loading: false,
        error: true
      }
    };
  case GET_SALE_ATTEMPT:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        isSaleLoading: true,
        isSaleError: false
      }
    };
  case GET_SALE_SUCCESS:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        sales: {
          ...state[action.societyId].sales,
          data: action.data,
          isSaleLoading: false,
          isSaleError: false
        }
      }
    };
  case GET_SALE_FAIL:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        sales: {
          isSaleLoading: false,
          isSaleError: true
        }
      }
    };
  case SELECT_SALE:
    return {
      ...state,
      [action.societyId]: {
        ...state[action.societyId],
        sales: {
          ...state[action.societyId].sales,
          selectedSale: action.sale
        }
      }
    };
  case UNSELECT_SALE:
    return {
      ...state,
      [action.societyId]: {
        ...defaultState,
        ...currentState,
        sales: {
          ...state[action.societyId].sales,
          selectedSale: {}
        }
      }
    };
  case SALE_MODAL_SUBMIT:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        sales: {
          ...state[action.societyId].sales,
          isValided: true
        }
      }
    };
  case SHOW_EARLY_START_DATE_MODAL:
    return {
      ...state,
      [action.societyId]: {
        ...currentState,
        showEarlyStartDateModal: action.status,
        modalEarlyStartImmoIndex: action.index
      }
    };
  default:
    return state;
  }
};

export default fixedAssets;
