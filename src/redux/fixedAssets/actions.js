import {
  GET_COMMENTS_ATTEMPT,
  GET_COMMENTS_FAIL,
  GET_COMMENTS_SUCCESS,
  GET_ACCOUNTS_ATTEMPT,
  GET_ACCOUNTS_SUCCESS,
  GET_ACCOUNTS_FAIL,
  START_EDIT,
  ADD_IMMO_LINES_VALIDER,
  END_EDIT,
  SELECT_ACCOUNT,
  UNSELECT_ACCOUNT,
  SELECT_IMMO,
  EDIT_IMMO,
  UNSELECT_IMMO,
  ADD_IMMO_LINE,
  SELECT_DOTATION,
  UNSELECT_DOTATION,
  DELETE_IMMOS_ATTEMPT,
  DELETE_IMMOS_SUCCESS,
  DELETE_IMMOS_FAIL,
  POST_COMMENT_ATTEMPT,
  POST_COMMENT_FAIL,
  POST_COMMENT_SUCCESS,
  SAVE_IMMOS_ATTEMPT,
  SAVE_IMMOS_SUCCESS,
  SAVE_IMMOS_FAIL,
  VALIDATE_IMMOS_FAIL,
  SET_ERRORS,
  CANCEL,
  OPEN_DIALOG,
  CLOSE_DIALOG,
  ADD_IMMO_PJ,
  POST_DILIGENCE_COMMENT_SUCCESS,
  POST_DILIGENCE_COMMENT_ATTEMPT,
  POST_DILIGENCE_COMMENT_FAIL,
  GET_DILIGENCE_COMMENT_ATTEMPT,
  GET_DILIGENCE_COMMENT_SUCCESS,
  GET_DILIGENCE_COMMENT_FAIL,
  EDIT_IMMO_VALIDER,
  GET_SALE_ATTEMPT,
  GET_SALE_SUCCESS,
  GET_SALE_FAIL,
  SELECT_SALE,
  UNSELECT_SALE,
  SALE_MODAL_SUBMIT,
  SHOW_EARLY_START_DATE_MODAL,
  PUT_DILIGENCE_COMMENT_ATTEMPT,
  PUT_DILIGENCE_COMMENT_SUCCESS,
  PUT_DILIGENCE_COMMENT_FAIL
} from './constants';

const validateImmosFail = (societyId, errors) => ({
  type: VALIDATE_IMMOS_FAIL,
  societyId,
  errors
});
const startEdit = societyId => ({
  type: START_EDIT,
  societyId
});
const endEdit = societyId => ({
  type: END_EDIT,
  societyId
});
const addImmoLine = (societyId, immo) => ({
  type: ADD_IMMO_LINE,
  societyId,
  immo
});
const addImmoLines = (societyId, newImmos) => ({
  type: ADD_IMMO_LINES_VALIDER,
  societyId,
  newImmos
});
const editImmo = (societyId, immo) => ({
  type: EDIT_IMMO,
  societyId,
  immo
});
const editImmoValider = (societyId, immo) => ({
  type: EDIT_IMMO_VALIDER,
  societyId,
  immo
});
const deleteImmosAttempt = societyId => ({
  type: DELETE_IMMOS_ATTEMPT,
  societyId
});
const deleteImmosSuccess = societyId => ({
  type: DELETE_IMMOS_SUCCESS,
  societyId
});
const deleteImmosFail = societyId => ({
  type: DELETE_IMMOS_FAIL,
  societyId
});
const saveImmosAttempt = societyId => ({
  type: SAVE_IMMOS_ATTEMPT,
  societyId
});
const saveImmosSuccess = societyId => ({
  type: SAVE_IMMOS_SUCCESS,
  societyId
});
const saveImmosFail = (societyId, errors) => ({
  type: SAVE_IMMOS_FAIL,
  societyId,
  errors
});
const selectAccount = (societyId, account) => ({
  type: SELECT_ACCOUNT,
  societyId,
  account
});
const unselectAccount = societyId => ({
  type: UNSELECT_ACCOUNT,
  societyId
});
const selectImmo = (societyId, immo) => ({
  type: SELECT_IMMO,
  societyId,
  immo
});
const unselectImmo = (societyId, immo) => ({
  type: UNSELECT_IMMO,
  societyId,
  immo
});
const selectDotation = (societyId, dotation) => ({
  type: SELECT_DOTATION,
  societyId,
  dotation
});
const unselectDotation = (societyId, dotation) => ({
  type: UNSELECT_DOTATION,
  societyId,
  dotation
});
const getAccountsAttempt = societyId => ({
  type: GET_ACCOUNTS_ATTEMPT,
  societyId
});
const getAccountsSuccess = (data, societyId) => ({
  type: GET_ACCOUNTS_SUCCESS,
  societyId,
  data
});
const getAccountsFail = societyId => ({
  type: GET_ACCOUNTS_FAIL,
  societyId
});
const setErrors = (societyId, id, errors) => ({
  type: SET_ERRORS,
  societyId,
  id,
  errors
});
const cancel = societyId => ({
  type: CANCEL,
  societyId
});
const openDialog = (societyId, dialog) => ({
  type: OPEN_DIALOG,
  societyId,
  dialog
});
const closeDialog = (societyId, dialog) => ({
  type: CLOSE_DIALOG,
  societyId,
  dialog
});
const getCommentsAttempt = () => ({
  type: GET_COMMENTS_ATTEMPT
});
const getCommentsSuccess = comments => ({
  type: GET_COMMENTS_SUCCESS,
  comments
});

const getCommentsFail = () => ({ type: GET_COMMENTS_FAIL });
const postCommentAttempt = society_id => ({
  type: POST_COMMENT_ATTEMPT,
  society_id
});
const postCommentSuccess = payload => ({
  type: POST_COMMENT_SUCCESS,
  payload
});
const postCommentFail = society_id => ({
  type: POST_COMMENT_FAIL,
  society_id
});

const addImmoPJ = (id_immo, files, societyId) => ({
  type: ADD_IMMO_PJ, id_immo, files, societyId
});

const postCommentDiligenceAtempt = societyId => ({
  type: POST_DILIGENCE_COMMENT_ATTEMPT,
  societyId
});

const putCommentDiligenceAtempt = societyId => ({
  type: PUT_DILIGENCE_COMMENT_ATTEMPT,
  societyId
});

const postCommentDiligenceSuccess = (societyId, diligence_id, comments) => ({
  type: POST_DILIGENCE_COMMENT_SUCCESS,
  societyId,
  diligence_id,
  comments
});

const putCommentDiligenceSuccess = (societyId, diligence_id, comments) => ({
  type: PUT_DILIGENCE_COMMENT_SUCCESS,
  societyId,
  diligence_id,
  comments
});

const postCommentDiligenceFailure = society_id => ({
  type: POST_DILIGENCE_COMMENT_FAIL,
  society_id
});

const getCommentDiligenceAttempt = society_id => ({
  type: GET_DILIGENCE_COMMENT_ATTEMPT,
  society_id
});

const getCommentDiligenceSuccess = (society_id, comments) => ({
  type: GET_DILIGENCE_COMMENT_SUCCESS,
  society_id,
  comments
});

const getCommentDiligenceFail = society_id => ({
  type: GET_DILIGENCE_COMMENT_FAIL,
  society_id
});

const putCommentDiligenceFail = society_id => ({
  type: PUT_DILIGENCE_COMMENT_FAIL,
  society_id
});

const getSaleAttempt = societyId => ({
  type: GET_SALE_ATTEMPT,
  societyId
});
const getSaleSuccess = (societyId, data) => ({
  type: GET_SALE_SUCCESS,
  societyId,
  data
});
const getSaleFail = societyId => ({
  type: GET_SALE_FAIL,
  societyId
});
const selectSale = (societyId, sale) => ({
  type: SELECT_SALE,
  sale,
  societyId
});
const unselectSale = societyId => ({
  type: UNSELECT_SALE,
  societyId
});
const saleModalSubmit = societyId => ({
  type: SALE_MODAL_SUBMIT,
  societyId
});

const showEarlyStartDateModal = (status, societyId, index) => ({
  type: SHOW_EARLY_START_DATE_MODAL,
  societyId,
  status,
  index
});

export default {
  addImmoPJ,
  postCommentAttempt,
  postCommentSuccess,
  postCommentFail,
  setErrors,
  startEdit,
  endEdit,
  selectAccount,
  unselectAccount,
  selectImmo,
  editImmo,
  deleteImmosAttempt,
  deleteImmosSuccess,
  editImmoValider,
  deleteImmosFail,
  unselectImmo,
  addImmoLine,
  selectDotation,
  unselectDotation,
  getAccountsAttempt,
  getAccountsSuccess,
  getAccountsFail,
  validateImmosFail,
  addImmoLines,
  saveImmosAttempt,
  saveImmosSuccess,
  saveImmosFail,
  getCommentsAttempt,
  getCommentsSuccess,
  getCommentsFail,
  postCommentDiligenceAtempt,
  postCommentDiligenceSuccess,
  postCommentDiligenceFailure,
  getCommentDiligenceAttempt,
  getCommentDiligenceSuccess,
  getCommentDiligenceFail,
  getSaleAttempt,
  getSaleSuccess,
  getSaleFail,
  cancel,
  openDialog,
  closeDialog,
  selectSale,
  unselectSale,
  saleModalSubmit,
  showEarlyStartDateModal,
  putCommentDiligenceSuccess,
  putCommentDiligenceAtempt,
  putCommentDiligenceFail
};
