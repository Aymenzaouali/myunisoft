import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router';
import Login from 'containers/controllers/Login';
import NavBarContainer from 'containers/groups/NavBar';
import PrivateRoute from 'containers/basics/PrivateRoute';
import AbstractDialogContainer from 'containers/groups/Dialogs/AbstractDialogContainer';
import PasswordRecovery from 'containers/controllers/PasswordForgot';
import PasswordReset from 'containers/controllers/PasswordReset';
import Overview from 'components/screens/Overview';
import { isChromeBrowser } from 'helpers/browser';
import BrowserCheckerRoute from 'components/basics/BrowserCheckerRoute';

const Routes = () => (
  <Fragment>
    {isChromeBrowser()
      ? (
        <>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/forgot_password" component={PasswordRecovery} />
            <Route exact path="/reset" component={PasswordReset} />
            <Route exact path="/overview" component={Overview} />
            <PrivateRoute path="/tab/:id" component={NavBarContainer} />
            <Route render={() => (<div>Page Not found</div>)} />
          </Switch>
          <AbstractDialogContainer />
        </>
      ) : <BrowserCheckerRoute />}
  </Fragment>
);

export default Routes;
