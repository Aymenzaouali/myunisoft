import _ from 'lodash';
import socketIOClient from 'socket.io-client';
import sailsIOClient from 'sails.io.js';
import { store } from 'redux/store';
import { service_discussion_conf } from 'common/config/server';
import { messagePatchSuccess } from 'common/redux/discussions/actions';
import { createMessageInternal } from 'common/redux/discussions';

class SocketService {
  static instance;

  static getInstance() {
    if (!SocketService.instance) { SocketService.instance = new SocketService(); }
    return SocketService.instance;
  }

  constructor() {
    this.subscribers = [];
    this.io = sailsIOClient(socketIOClient);
  }

  init() {
    if (process.env.NODE_ENV !== 'test') {
      if (this.isConnected()) this.io.socket.disconnect();
      this.io.sails.useCORSRouteToGetCookie = false;
      this.io.sails.transports = ['websocket'];
      this.io.sails.autoConnect = true;
      this.setURL(`${service_discussion_conf.protocol}://${service_discussion_conf.baseUrl}:${service_discussion_conf.port}`);
      this.setHeaders();
      this.connect();
    }
  }

  setHeaders() {
    const state = store && store.getState();
    const token = _.get(state, 'login.token.access_token', false);
    this.io.sails.headers = {
      Authorization: `Bearer ${token}`
    };
    return this;
  }

  post(url, message) {
    this.io.socket.post(url, { message });
    return this;
  }

  getSubscribers() {
    return this.subscribers;
  }

  setURL(URL) {
    this.io.sails.url = URL;
    return this;
  }

  isConnected() {
    return this.io && this.io.socket && this.io.socket.isConnected();
  }

  subscribe() {
    if ((!this.io || !this.io.socket.isConnected())) {
      return;
    }
    if (this.io && this.io.sails) {
      this.io.socket.get(`${service_discussion_conf.protocol}://${service_discussion_conf.baseUrl}:${service_discussion_conf.port}/subscribe`, { }, () => {
        this.io.socket.on('message', (msg) => {
          console.debug('s m', msg);
          if (store) {
            const message = msg;
            const state = store.getState();
            const current_user_id = _.get(state, 'login.user.user_id');
            if (current_user_id !== message.user_id) {
              store.dispatch(createMessageInternal(message));
            }
          }
        });
        this.io.socket.on('edit_message', (msg) => {
          console.debug('s em', msg);
          if (store) {
            const message = msg.message;
            store.dispatch(
              messagePatchSuccess(
                message.room_id || message.room.room_id,
                message.message_id,
                message
              )
            );
          }
        });
        // to keep socket alive
        this.io.socket.on('ping', (msg) => {
          console.debug('pi');
        });
        this.io.socket.on('pong', (msg) => {
          console.debug('po');
        });
      });
    } else {
      console.warn('Socket instance in not found');
    }

    // ? return this;
  }

  unsubscribe() {
    if (!this.io || !this.io.socket.isConnected()) {
      console.warn('Socket is not connected');
    }
    // Do unsubscribe logic
    // ? return this;
  }

  disconnect() {
    console.log('[index.js] [disconnect] ');
    if (this.io && this.io.socket.isConnected()) { this.io.socket.disconnect(); }
    return this;
  }

  connect() {
    if (!this.io.socket.isConnected()) this.io.sails.connect();
    this.io.socket.on('connect', () => {
      this.subscribe();
    });
    return this;
  }
}
export default SocketService.getInstance();
