export const validatePassword = (values) => {
  const errors = {};
  if (values.newPassword && values.confirmedPassword
      && values.newPassword !== values.confirmedPassword) {
    errors.confirmedPassword = 'Veuillez saisir des mots de passe identiques';
  }
  return errors;
};
