export const localStringSearch = (array, string) => array.filter((item) => {
  const lowerString = string.toLowerCase();
  const label = item.label.toLowerCase();
  if (label.includes(lowerString)) {
    return item;
  }
  return false;
});
