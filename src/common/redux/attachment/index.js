import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import actions from './actions';

export const getAttachment = tokens => async (dispatch) => {
  try {
    await dispatch(actions.getAttachmentAttempt());
    const response = await windev.makeApiCall('/attachment', 'post', {}, tokens);
    const { data } = response;
    await dispatch(actions.getAttachmentSuccess(data));
    return response;
  } catch (err) {
    await dispatch(handleError(err, getAttachment.bind(null, tokens)));
    return err;
  }
};
