import {
  GET_ATTACHMENT_ATTEMPT,
  GET_ATTACHMENT_SUCCESS,
  GET_ATTACHMENT_FAIL
} from './constants';

const initialState = {
  attachment: []
};

const attachment = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_ATTACHMENT_ATTEMPT:
      return {
        ...state
      };
    case GET_ATTACHMENT_SUCCESS:
      return {
        ...state,
        attachment: [...state.attachment, ...action.attachment]
      };
    case GET_ATTACHMENT_FAIL:
      return {
        ...state
      };
    default: return state;
    }
  }
  return state;
};

export default attachment;
