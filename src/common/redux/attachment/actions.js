import {
  GET_ATTACHMENT_ATTEMPT,
  GET_ATTACHMENT_SUCCESS,
  GET_ATTACHMENT_FAIL
} from './constants';

const getAttachmentAttempt = () => ({
  type: GET_ATTACHMENT_ATTEMPT
});

const getAttachmentSuccess = attachment => ({
  type: GET_ATTACHMENT_SUCCESS,
  attachment
});

const getAttachmentFail = () => ({
  type: GET_ATTACHMENT_FAIL
});

export default {
  getAttachmentAttempt,
  getAttachmentSuccess,
  getAttachmentFail
};
