import {
  ADD_ERROR,
  DELETE_ERROR
} from './constants';

const addError = error => ({
  type: ADD_ERROR,
  error
});

const deleteError = error => ({
  type: DELETE_ERROR,
  error
});

export default {
  addError,
  deleteError
};
