import _ from 'lodash';
import {
  ADD_ERROR,
  DELETE_ERROR
} from './constants';

const initialState = {
  errors: []
};

const error = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case ADD_ERROR:
      return {
        ...state,
        errors: [...state.errors, action.error]
      };
    case DELETE_ERROR:
      return {
        ...state,
        errors: state.errors.filter(error => !_.isEqual(error, action.error))
      };
    default: return state;
    }
  }
  return state;
};

export default error;
