import LoginService from 'common/redux/login';
import _ from 'lodash';
// ?? import { Actions } from 'react-native-router-flux';
// ?? import { AsyncStorage } from 'react-native';
import actions from './actions';

export const handleError = (payload, callBack) => async (dispatch) => {
  try {
    if (_.get(payload, 'response.data.message', '') === 'expired_token') {
      try {
        await dispatch(LoginService.refreshToken());
        if (callBack) await dispatch(callBack());
      } catch (err) {
        // TODO: CREATE MIDDLEWARE FOR RELAUNCHING REQUEST MAYBE ?
        // BIG THINK FOR THAT AFTER V0
        // ?? await AsyncStorage.multiRemove(['myunisoft:access_token', 'myunisoft:refresh_token',
        //  'myunisoft:old_push_token', 'myunisoft:push_token']);
        // ? Actions.login();
      }
    } else if (_.get(payload, 'response.data.code', false) && _.get(payload, 'response.data.message', false)) {
      await dispatch(actions.addError(_.get(payload, 'response.data', false)));
    } else if (_.get(payload, 'code', false) && _.get(payload, 'message', false)) {
      await dispatch(actions.addError({ code: _.get(payload, 'code', false), message: _.get(payload, 'message', false) }));
    }
  } catch (err) {
    await dispatch(actions.addError({ code: 'InternalError', message: 'Erreur interne' }));
    throw err;
  }
};
