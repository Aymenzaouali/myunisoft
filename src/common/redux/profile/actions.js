import {
  PUT_PASSWORD_CHANGE_ATTEMPT,
  PUT_PASSWORD_CHANGE_SUCCESS,
  PUT_PASSWORD_CHANGE_FAIL,
  PUT_MAIL_CHANGE_ATTEMPT,
  PUT_MAIL_CHANGE_SUCCESS,
  PUT_MAIL_CHANGE_FAIL
} from './constants';

const passwordChangeAttempt = () => ({
  type: PUT_PASSWORD_CHANGE_ATTEMPT
});

const passwordChangeSuccess = () => ({
  type: PUT_PASSWORD_CHANGE_SUCCESS
});

const passwordChangeFail = () => ({
  type: PUT_PASSWORD_CHANGE_FAIL
});

const mailChangeAttempt = () => ({
  type: PUT_MAIL_CHANGE_ATTEMPT
});

const mailChangeSuccess = () => ({
  type: PUT_MAIL_CHANGE_SUCCESS
});

const mailChangeFail = () => ({
  type: PUT_MAIL_CHANGE_FAIL
});

export default {
  passwordChangeAttempt,
  passwordChangeSuccess,
  passwordChangeFail,
  mailChangeAttempt,
  mailChangeSuccess,
  mailChangeFail
};
