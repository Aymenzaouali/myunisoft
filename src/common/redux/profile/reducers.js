import {
  PUT_PASSWORD_CHANGE_ATTEMPT,
  PUT_PASSWORD_CHANGE_SUCCESS,
  PUT_PASSWORD_CHANGE_FAIL,
  PUT_MAIL_CHANGE_ATTEMPT,
  PUT_MAIL_CHANGE_SUCCESS,
  PUT_MAIL_CHANGE_FAIL
} from './constants';

const inistialState = {};

const profileChangeReducer = (state = inistialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case PUT_PASSWORD_CHANGE_ATTEMPT:
      return {
        ...state
      };
    case PUT_PASSWORD_CHANGE_SUCCESS:
      return {
        ...state
      };
    case PUT_PASSWORD_CHANGE_FAIL:
      return {
        ...state
      };
    case PUT_MAIL_CHANGE_ATTEMPT:
      return {
        ...state
      };
    case PUT_MAIL_CHANGE_SUCCESS:
      return {
        ...state
      };
    case PUT_MAIL_CHANGE_FAIL:
      return {
        ...state
      };

    default: return state;
    }
  }
  return state;
};

export default profileChangeReducer;
