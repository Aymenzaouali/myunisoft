import { service_auth } from 'helpers/api';
import { handleError } from 'common/redux/error';
import actions from './actions';

export const changePassword = payload => async (dispatch) => {
  try {
    const { old_password, new_password } = payload;
    await dispatch(actions.passwordChangeAttempt());
    const response = await service_auth.makeApiCall('/user/password', 'put', {}, { old_password, new_password });
    const { data } = response;
    await dispatch(actions.passwordChangeSuccess(data));
    return response;
  } catch (err) {
    await dispatch(handleError(err, changePassword.bind(null, payload)));
    throw err;
  }
};

export const changeMail = payload => async (dispatch) => {
  try {
    const { old_mail, new_mail, password } = payload;
    await dispatch(actions.mailChangeAttempt());
    const response = await service_auth.makeApiCall('/user/mail', 'put', {}, { old_mail, new_mail, password });
    const { data } = response;
    await dispatch(actions.mailChangeSuccess(data));
    return response;
  } catch (err) {
    await dispatch(handleError(err, changeMail.bind(null, payload)));
    throw err;
  }
};
