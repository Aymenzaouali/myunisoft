export const PUT_PASSWORD_CHANGE_ATTEMPT = 'PUT_PASSWORD_CHANGE_ATTEMPT';
export const PUT_PASSWORD_CHANGE_SUCCESS = 'PUT_PASSWORD_CHANGE_SUCCESS';
export const PUT_PASSWORD_CHANGE_FAIL = 'PUT_PASSWORD_CHANGE_FAIL';

export const PUT_MAIL_CHANGE_ATTEMPT = 'PUT_MAIL_CHANGE_ATTEMPT';
export const PUT_MAIL_CHANGE_SUCCESS = 'PUT_MAIL_CHANGE_SUCCESS';
export const PUT_MAIL_CHANGE_FAIL = 'PUT_MAIL_CHANGE_FAIL';
