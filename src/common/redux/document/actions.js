import {
  GET_DOCUMENT_ATTEMPT,
  GET_DOCUMENT_SUCCESS,
  GET_DOCUMENT_FAIL,
  GET_DOCUMENT_TYPES_ATTEMPT,
  GET_DOCUMENT_TYPES_SUCCESS,
  GET_DOCUMENT_TYPES_FAIL,
  SEND_DOCUMENT_ATTEMPT,
  SEND_DOCUMENT_SUCCESS,
  SEND_DOCUMENT_FAIL,
  SENDING_DOCUMENT_PROGRESS,
  RESET_EXCHANGE_WITH
} from './constants';

const getDocumentAttempt = () => ({
  type: GET_DOCUMENT_ATTEMPT
});

const getDocumentSuccess = (typeDoc, documents) => ({
  type: GET_DOCUMENT_SUCCESS,
  typeDoc,
  documents
});

const resetDocumentWith = (typeDoc, documents) => ({
  type: RESET_EXCHANGE_WITH,
  typeDoc,
  documents
});

const getDocumentFail = () => ({
  type: GET_DOCUMENT_FAIL
});

const getDocumentTypesAttempt = () => ({
  type: GET_DOCUMENT_TYPES_ATTEMPT
});

const getDocumentTypesSuccess = types => ({
  type: GET_DOCUMENT_TYPES_SUCCESS,
  types
});

const getDocumentTypesFail = () => ({
  type: GET_DOCUMENT_TYPES_FAIL
});

const sendDocumentAttempt = doc => ({
  type: SEND_DOCUMENT_ATTEMPT,
  doc
});

const sendDocumentSuccess = (doc, data) => ({
  type: SEND_DOCUMENT_SUCCESS,
  doc,
  data
});

const sendDocumentFail = doc => ({
  type: SEND_DOCUMENT_FAIL,
  doc
});

const sendingDocumentProgress = (doc, progress) => ({
  type: SENDING_DOCUMENT_PROGRESS,
  doc,
  progress
});

export default {
  getDocumentAttempt,
  getDocumentSuccess,
  getDocumentFail,
  getDocumentTypesAttempt,
  getDocumentTypesSuccess,
  getDocumentTypesFail,
  sendDocumentAttempt,
  sendDocumentSuccess,
  sendDocumentFail,
  sendingDocumentProgress,
  resetDocumentWith
};
