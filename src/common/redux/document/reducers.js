import {
  GET_DOCUMENT_ATTEMPT,
  GET_DOCUMENT_SUCCESS,
  GET_DOCUMENT_FAIL,
  GET_DOCUMENT_TYPES_ATTEMPT,
  GET_DOCUMENT_TYPES_SUCCESS,
  GET_DOCUMENT_TYPES_FAIL,
  SENDING_DOCUMENT_PROGRESS,
  SEND_DOCUMENT_FAIL,
  SEND_DOCUMENT_ATTEMPT,
  RESET_EXCHANGE_WITH,
  SEND_DOCUMENT_SUCCESS
} from './constants';

const initialState = {
  list: [],
  isDrawerOpen: false,
  documents: {
    bills: [],
    documents: [],
    search: []
  },
  onSendDocs: []
};

const document = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_DOCUMENT_ATTEMPT:
      return {
        ...state,
        loading: {
          [action.typeDoc]: true
        }
      };
    case GET_DOCUMENT_SUCCESS:
      return {
        ...state,
        documents: {
          ...state.documents,
          [action.typeDoc]: [...state.documents[action.typeDoc], ...action.documents]
        },
        loading: {
          [action.typeDoc]: false
        }
      };
    case RESET_EXCHANGE_WITH:
      return {
        ...state,
        documents: {
          ...state.documents,
          [action.typeDoc]: action.documents
        },
        loading: {
          [action.typeDoc]: false
        }
      };
    case GET_DOCUMENT_FAIL:
      return {
        ...state,
        loading: {
          [action.typeDoc]: false
        }
      };
    case GET_DOCUMENT_TYPES_ATTEMPT:
      return {
        ...state
      };
    case GET_DOCUMENT_TYPES_SUCCESS:
      return {
        ...state,
        types: action.types
      };
    case GET_DOCUMENT_TYPES_FAIL:
      return {
        ...state
      };
    case SEND_DOCUMENT_SUCCESS: {
      return {
        ...state,
        onSendDocs: state.onSendDocs.filter(doc => doc.id !== action.doc.id)
      };
    }
    case SEND_DOCUMENT_FAIL:
      return {
        ...state,
        onSendDocs: state.onSendDocs.map(
          doc => (doc.id === action.doc.id ? { ...doc, failed: true } : doc)
        )
      };
    case SEND_DOCUMENT_ATTEMPT:
      const doesExist = state.onSendDocs.some(d => d.id === action.doc.id);
      return {
        ...state,
        onSendDocs: doesExist ? state.onSendDocs : [...state.onSendDocs, action.doc]
      };
    case SENDING_DOCUMENT_PROGRESS:
      return {
        ...state,
        onSendDocs: state.onSendDocs.map(
          doc => (doc.id === action.doc.id ? { ...doc, progress: action.progress } : doc)
        )
      };
    case 'Navigation/OPEN_DRAWER':
      return {
        ...state,
        isDrawerOpen: true
      };
    case 'Navigation/MARK_DRAWER_SETTLING':
      return state;
    case 'Navigation/DRAWER_OPENED':
      return {
        ...state,
        isDrawerOpen: true
      };
    case 'Navigation/DRAWER_CLOSED': {
      return {
        ...state,
        isDrawerOpen: false
      };
    }
    default: return state;
    }
  }
  return state;
};

export default document;
