import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import _ from 'lodash';
import actions from './actions';
// import RNFetchBlob from 'rn-fetch-blob';

export const getDocument = (type, customOffset, query) => async (dispatch, getState) => {
  try {
    const offset = customOffset === 0 ? 0 : _.get(getState(), `document.documents[${type}].length`, 0);
    await dispatch(actions.getDocumentAttempt());
    const params = {
      offset,
      type,
      query,
      society_id: parseInt(_.get(getState(), 'login.currentSociety.id_societe', 0), 10)
    };
    const response = await windev.makeApiCall('/document', 'get', params);
    const { data } = response;
    // console.log('resPONSE :  ', response);
    if (customOffset === 0) {
      await dispatch(actions.resetDocumentWith(type, data));
    } else {
      await dispatch(actions.getDocumentSuccess(type, data));
    }
    return response;
  } catch (err) {
    await dispatch(handleError(err, getDocument.bind(null, type, customOffset, query)));
    throw err;
  }
};

export const getDocumentTypes = () => async (dispatch) => {
  try {
    await dispatch(actions.getDocumentTypesAttempt());
    const response = await windev.makeApiCall('/invoice/type', 'get');
    const { data } = response;
    await dispatch(actions.getDocumentTypesSuccess(data));
    return response;
  } catch (err) {
    await dispatch(handleError(err, getDocumentTypes.bind(null)));
    throw err;
  }
};

// export const sendDocument = (base64, selectedDocType, ocr_type_id, path) => {
export const sendDocument = (path, selectedDocType, ocr_type_id) => (
  async (dispatch, getState) => {
    const society_id = parseInt(_.get(getState(), 'login.currentSociety.id_societe', 0), 10);
    const doc = {
      id: new Date().getTime(),
      path,
      society_id,
      ...selectedDocType,
      ocr_type_id,
      extension: 'jpg'
    };
    return doc;
  }
);
