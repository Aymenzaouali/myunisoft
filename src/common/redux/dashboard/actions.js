import {
  GET_DASHBOARD_ATTEMPT,
  GET_DASHBOARD_SUCCESS,
  GET_DASHBOARD_FAIL,

  GET_ACCOUNTS_ATTEMPT,
  GET_ACCOUNTS_SUCCESS,
  GET_ACCOUNTS_FAIL,

  GET_DETAILS_ATTEMPT,
  GET_DETAILS_SUCCESS,
  GET_DETAILS_FAIL
} from './constants';

const getDashboardAttempt = () => ({
  type: GET_DASHBOARD_ATTEMPT
});

const getDashboardSuccess = dashboards => ({
  type: GET_DASHBOARD_SUCCESS,
  dashboards
});

const getDashboardFail = () => ({
  type: GET_DASHBOARD_FAIL
});

const getAccountsAttempt = () => ({
  type: GET_ACCOUNTS_ATTEMPT
});

const getAccountsSuccess = (accountList, idCard, dataType) => ({
  type: GET_ACCOUNTS_SUCCESS,
  accountList,
  idCard,
  dataType
});

const getAccountsFail = () => ({
  type: GET_ACCOUNTS_FAIL
});

const getDetailsAttempt = () => ({
  type: GET_DETAILS_ATTEMPT
});

const getDetailsSuccess = (entryList, idCard, idAccount, dataType) => ({
  type: GET_DETAILS_SUCCESS,
  entryList,
  idCard,
  idAccount,
  dataType
});

const getDetailsFail = () => ({
  type: GET_DETAILS_FAIL
});

export default {
  getDashboardAttempt,
  getDashboardSuccess,
  getDashboardFail,

  getAccountsAttempt,
  getAccountsSuccess,
  getAccountsFail,

  getDetailsAttempt,
  getDetailsSuccess,
  getDetailsFail
};
