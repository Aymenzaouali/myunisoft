import {
  GET_DASHBOARD_ATTEMPT,
  GET_DASHBOARD_SUCCESS,
  GET_DASHBOARD_FAIL,

  GET_ACCOUNTS_ATTEMPT,
  GET_ACCOUNTS_SUCCESS,
  GET_ACCOUNTS_FAIL,

  GET_DETAILS_ATTEMPT,
  GET_DETAILS_SUCCESS,
  GET_DETAILS_FAIL
} from './constants';

const initialState = {
  list: [],
  dashboards: [],
  previousList: [],
  currentList: [],
  detailList: [],
  detailEntryList: {},
  dashboardIsLoading: false,
  accountIsLoading: false,
  detailIsLoading: false
};

const dashboard = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_DASHBOARD_ATTEMPT:
      return {
        ...state,
        dashboardIsLoading: true
      };
    case GET_DASHBOARD_SUCCESS:
      const {
        dashboards
      } = action;
      return {
        ...state,
        dashboardIsLoading: false,
        dashboards
      };
    case GET_DASHBOARD_FAIL:
      return {
        ...state,
        dashboardIsLoading: false,
        currentList: [],
        previousList: []
      };
    case GET_ACCOUNTS_ATTEMPT:
      return {
        ...state,
        accountIsLoading: true
      };
    case GET_ACCOUNTS_SUCCESS:
      return {
        ...state,
        accountIsLoading: false,
        detailList: {
          [action.idCard]: {
            ...state.detailList[action.idCard],
            [action.dataType]: action.accountList.list_accounts
          },
          total: action.accountList.total
        }
      };
    case GET_ACCOUNTS_FAIL:
      return {
        ...state,
        accountIsLoading: false,
        detailList: {
          [action.idCard]: {
            [action.dataType]: []
          },
          total: []
        }
      };
    case GET_DETAILS_ATTEMPT:
      return {
        ...state,
        detailIsLoading: true
      };
    case GET_DETAILS_SUCCESS:
      return {
        ...state,
        detailIsLoading: false,
        detailEntryList: {
          [action.idCard]: {
            [action.idAccount]: {
              [action.dataType]: action.entryList.list_entries_line
            }
          }
        },
        latePaymentSumDetail: {
          [action.idCard]: action.entryList.total[0].amount
        },
        onRunningPaymentSumDetail: {
          [action.idCard]: action.entryList.total[1].amount
        },
        settlementPaymentSumDetail: {
          [action.idCard]: action.entryList.total[2].amount
        },
        totalPaymentSumDetail: {
          [action.idCard]: action.entryList.total_amount_account
        }
      };
    case GET_DETAILS_FAIL:
      return {
        ...state,
        detailIsLoading: false,
        detailEntryList: {
          [action.idCard]: {
            [action.idAccount]: {
              [action.dataType]: []
            }
          }
        },
        latePaymentSumDetail: {
          [action.idCard]: 0
        },
        onRunningPaymentSumDetail: {
          [action.idCard]: 0
        },
        settlementPaymentSumDetail: {
          [action.idCard]: 0
        },
        totalPaymentSumDetail: {
          [action.idCard]: 0
        }
      };
    default: return state;
    }
  }
  return state;
};

export default dashboard;
