import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import _ from 'lodash';
import actions from './actions';

export const getDashboard = () => async (dispatch, getState) => {
  try {
    await dispatch(actions.getDashboardAttempt());
    const society_id = _.get(getState(), 'login.currentSociety.id_societe', 0);
    const response = await windev.makeApiCall('/dashboard', 'get', { society_id, application_type: 'Mobile' });
    const data = response.data.dashboards_list;
    await dispatch(actions.getDashboardSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.getDashboardFail());
    await dispatch(handleError(err, getDashboard));
    return err;
  }
};

export const getCardAccounts = (card_id, data_type) => async (dispatch, getState) => {
  try {
    await dispatch(actions.getAccountsAttempt());
    const society_id = _.get(getState(), 'login.currentSociety.id_societe', 0);
    const response = await windev.makeApiCall('/dashboard/account', 'get', {
      society_id, application_type: 'Mobile', card_id, data_type
    });
    const { data } = response;
    await dispatch(actions.getAccountsSuccess(data, card_id, data_type));
    return response;
  } catch (err) {
    await dispatch(actions.getAccountsFail());
    await dispatch(handleError(err, getCardAccounts.bind(null, card_id, data_type)));
    return err;
  }
};

export const getDetails = (card_id, account_id, data_type) => async (dispatch, getState) => {
  try {
    await dispatch(actions.getDetailsAttempt());
    const society_id = _.get(getState(), 'login.currentSociety.id_societe', 0);
    const response = await windev.makeApiCall('/dashboard/entries', 'get', {
      society_id, application_type: 'Mobile', data_type, card_id, account_id
    });
    const { data } = response;
    await dispatch(actions.getDetailsSuccess(data, card_id, account_id, data_type));
    console.log('GET DETAILED DASHBOARD', data);
    return response;
  } catch (err) {
    await dispatch(actions.getDetailsFail());
    await dispatch(handleError(err, getDetails.bind(null, card_id, account_id, data_type)));
    return err;
  }
};
