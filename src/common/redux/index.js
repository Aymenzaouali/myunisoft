import { reducer as form } from 'redux-form';
import login from './login/reducers';
import attachment from './attachment/reducers';
import exchange from './exchange/reducers';
import document from './document/reducers';
import group from './group/reducers';
import dashboard from './dashboard/reducers';
import error from './error/reducers';
import toast from './toast/reducers';
import support from './support/reducers';
import discussions from './discussions/reducers';
import notifier from './notifier/reducer';

const reducers = {
  login,
  exchange,
  attachment,
  form,
  document,
  group,
  dashboard,
  error,
  toast,
  support,
  discussions,
  notifier
};

export default reducers;
