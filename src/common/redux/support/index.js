import { windev } from 'helpers/api';
import { handleError } from 'common/redux/error';
import _ from 'lodash';
import actions from './actions';

export const sendTicket = payload => async (dispatch, getState) => { // eslint-disable-line
  try {
    const { issue_type, issue_description } = payload;
    const issue_user_mail = _.get(getState(), 'login.user.mail[0].mail', '');
    await dispatch(actions.ticketAttempt());
    const response = await windev.makeApiCall('/support', 'post', {}, { issue_type, issue_description, issue_user_mail });
    const { data } = response;
    await dispatch(actions.ticketSuccess(data));
    return response;
  } catch (err) {
    await dispatch(actions.ticketFail());
    await dispatch(handleError(err, sendTicket.bind(null, payload)));
  }
};
