import {
  POST_TICKET_ATTEMPT,
  POST_TICKET_SUCCESS,
  POST_TICKET_FAIL
} from './constants';

const initialState = {};

const support = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case POST_TICKET_ATTEMPT:
      return {
        ...state
      };
    case POST_TICKET_SUCCESS:
      return {
        ...state
      };
    case POST_TICKET_FAIL:
      return {
        ...state
      };
    default: return state;
    }
  }
  return state;
};

export default support;
