import {
  POST_TICKET_ATTEMPT,
  POST_TICKET_SUCCESS,
  POST_TICKET_FAIL
} from './constants';

const ticketAttempt = () => ({
  type: POST_TICKET_ATTEMPT
});

const ticketSuccess = () => ({
  type: POST_TICKET_SUCCESS
});

const ticketFail = () => ({
  type: POST_TICKET_FAIL
});

export default {
  ticketAttempt,
  ticketSuccess,
  ticketFail
};
