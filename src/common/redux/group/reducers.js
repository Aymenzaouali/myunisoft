import {
  GET_GROUPMEMBER_ATTEMPT,
  GET_GROUPMEMBER_SUCCESS,
  CHANGE_CURRENTGROUP,
  GET_GROUPMEMBER_FAIL
} from './constants';

const initialState = {
  currentGroup: {},
  list: []
};

const group = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_GROUPMEMBER_ATTEMPT:
      return {
        ...state
      };
    case GET_GROUPMEMBER_SUCCESS:
      const {
        listGroupMember
      } = action;
      return {
        ...state,
        list: listGroupMember,
        currentGroup: listGroupMember[0]
      };
    case CHANGE_CURRENTGROUP:
      const { NewGroupMember } = action;
      return {
        ...state,
        currentGroup: NewGroupMember
      };
    case GET_GROUPMEMBER_FAIL:
      return {
        ...state
      };
    default: return state;
    }
  }
  return state;
};

export default group;
