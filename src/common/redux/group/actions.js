import {
  GET_GROUPMEMBER_ATTEMPT,
  GET_GROUPMEMBER_SUCCESS,
  CHANGE_CURRENTGROUP,
  GET_GROUPMEMBER_FAIL
} from './constants';

const getGroupMemberAttempt = () => ({
  type: GET_GROUPMEMBER_ATTEMPT
});

const getGroupMemberSuccess = listGroupMember => ({
  type: GET_GROUPMEMBER_SUCCESS,
  listGroupMember
});

const changeCurrentGroup = NewGroupMember => ({
  type: CHANGE_CURRENTGROUP,
  NewGroupMember
});

const getGroupMemberFail = () => ({
  type: GET_GROUPMEMBER_FAIL
});

export default {
  getGroupMemberAttempt,
  getGroupMemberSuccess,
  getGroupMemberFail,
  changeCurrentGroup
};
