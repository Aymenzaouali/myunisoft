import { service_auth } from 'helpers/api';
import { handleError } from 'common/redux/error';
import actions from './actions';
// import errorActions from '../../redux/error/actions';

export const getGroupMember = payload => async (dispatch) => { // eslint-disable-line
  try {
    const { mail } = payload;
    await dispatch(actions.getGroupMemberAttempt());
    const response = await service_auth.makeApiCall('/group', 'get', { mail });
    const { data } = response;
    let groups;
    if (data.length > 0) {
      groups = data.map(group => ({ id: group.member_group_id, name: group.label }));
      await dispatch(actions.getGroupMemberSuccess(groups));
    } else {
      throw { code: 'MismatchError', message: 'bad_credentials', local: true }; // eslint-disable-line
    }
    return groups;
  } catch (err) {
    await dispatch(handleError(err, getGroupMember.bind(null, payload)));
    throw err;
  }
};
