import { getDocumentTypes } from 'common/redux/document';
import { service_auth, service_notification } from 'helpers/api';
import { handleError } from 'common/redux/error';
import _ from 'lodash';
import actions from './actions';

export class LoginService {
  static instance;

  static getInstance() {
    if (!LoginService.instance) LoginService.instance = new LoginService();
    return LoginService.instance;
  }

  signIn(payload, token_name) {
    return async (dispatch, getState) => {
      try {
        // const token_name = `System: ${DeviceInfo.getSystemName()},
        // Marque: ${DeviceInfo.getBrand()}, Version: ${DeviceInfo.getSystemVersion()}`
        await dispatch(actions.getTokenAttempt());
        const { id } = _.get(getState(), 'group.currentGroup');
        const token = await service_auth.makeApiCall('/auth/token', 'post', {}, {
          mail: payload.mail, password: payload.password, grant_type: 'password', member_group_id: parseInt(id, 10), token_name
        });
        const { data } = token;
        await dispatch(actions.getTokenSuccess(data));
        await dispatch(actions.signInAttempt());
        const user = await service_auth.makeApiCall('/user', 'get');
        await dispatch(actions.signInSuccess(user.data));
        await dispatch(getDocumentTypes());
        await dispatch(this.registerPushToServer());
        return user.data;
      } catch (err) {
        console.log(err);
        console.log(err.response);
        if (_.get(err, 'response.data.code') === 'MismatchError') {
          await dispatch(handleError(err, this.signIn.bind(this, payload)));
          throw { code: 'MismatchError', message: 'bad_email_password', local: true }; // eslint-disable-line
        } else {
          await dispatch(handleError(err, this.signIn.bind(this, payload)));
          throw err;
        }
      }
    };
  }

  signOut() {
    return async (dispatch) => {
      try {
        await dispatch(actions.signOutAttempt());
        const disco = await service_auth.makeApiCall('/auth/disconnect', 'get', {}, {});
        await dispatch(actions.signOutSuccess(disco));
        await dispatch(this.deletePushToServer());
      } catch (err) {
        await dispatch(handleError(err));
      }
    };
  }

  refreshToken() { // eslint-disable-line
    return async (dispatch, getState) => {
      try {
        await dispatch(actions.getTokenAttempt());
        const { refresh_token } = _.get(getState(), 'login.token');
        const token = await service_auth.makeApiCall('/auth/token', 'put', {}, { refresh_token, grant_type: 'refresh_token' });
        const { data } = token;
        await dispatch(actions.getTokenSuccess(data));
        return token;
      } catch (err) {
        console.log(err);
        console.log(err.response);
        throw err;
      }
    };
  }

  registerPushToServer() { // eslint-disable-line
    return async (dispatch, getState) => {
      try {
        await dispatch(actions.registerPushToServerAttempt());
        const { token } = _.get(getState(), 'login.deviceToken', {});
        const register = await service_notification.makeApiCall('/push/register', 'post', {}, { token });
        return await dispatch(actions.registerPushToServerSuccess(register));
      } catch (err) {
        return dispatch(handleError(err));
      }
    };
  }

  updatePushToServer(payload) { // eslint-disable-line
    return async (dispatch) => {
      try {
        await dispatch(actions.updatePushToServerAttempt());
        const { old_push_token, new_push_token } = payload;
        const update = await service_notification.makeApiCall('/push/register', 'put', {}, { old_push_token, new_push_token });
        await dispatch(actions.updatePushToServerSuccess(update));
      } catch (err) {
        await dispatch(handleError(err));
      }
    };
  }

  deletePushToServer() { // eslint-disable-line
    return async (dispatch, getState) => {
      try {
        await dispatch(actions.deletePushToServerAttempt());
        const { token } = _.get(getState(), 'login.deviceToken', {});
        const del = await service_notification.makeApiCall('/push/register', 'delete', {}, { token });
        await dispatch(actions.deletePushToServerSuccess(del));
      } catch (err) {
        await dispatch(handleError(err));
      }
    };
  }
}

export default LoginService.getInstance();
