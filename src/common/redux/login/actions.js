import {
  SIGN_IN_ATTEMPT,
  SIGN_IN_FAIL,
  SIGN_IN_SUCCESS,
  SIGN_OUT_ATTEMPT,
  SIGN_OUT_FAIL,
  SIGN_OUT_SUCCESS,
  GET_TOKEN_ATTEMPT,
  GET_TOKEN_FAIL,
  GET_TOKEN_SUCCESS,
  CHANGE_CURRENT_SOCIETY,
  REGISTER_PUSH,
  UNREGISTER_PUSH,
  REGISTER_PUSH_TO_SERVER_ATTEMPT,
  REGISTER_PUSH_TO_SERVER_SUCCESS,
  UPDATE_PUSH_TO_SERVER_ATTEMPT,
  UPDATE_PUSH_TO_SERVER_SUCCESS,
  DELETE_PUSH_TO_SERVER_ATTEMPT,
  DELETE_PUSH_TO_SERVER_SUCCESS,
  UPDATE_SOCIETIES_LIST
} from './constants';

const signInAttempt = () => ({
  type: SIGN_IN_ATTEMPT
});

const signInSuccess = data => ({
  type: SIGN_IN_SUCCESS,
  data
});

const signInFail = payload => ({
  type: SIGN_IN_FAIL,
  payload
});

const signOutAttempt = () => ({
  type: SIGN_OUT_ATTEMPT
});

const signOutSuccess = data => ({
  type: SIGN_OUT_SUCCESS,
  data
});

const signOutFail = payload => ({
  type: SIGN_OUT_FAIL,
  payload
});

const getTokenAttempt = () => ({
  type: GET_TOKEN_ATTEMPT
});

const getTokenSuccess = data => ({
  type: GET_TOKEN_SUCCESS,
  data
});

const getTokenFail = payload => ({
  type: GET_TOKEN_FAIL,
  payload
});

const changeCurrentSociety = society => ({
  type: CHANGE_CURRENT_SOCIETY,
  society
});

const registerPush = deviceToken => ({
  type: REGISTER_PUSH,
  deviceToken
});

const unregisterPush = () => ({
  type: UNREGISTER_PUSH
});

const registerPushToServerAttempt = () => ({
  type: REGISTER_PUSH_TO_SERVER_ATTEMPT
});

const registerPushToServerSuccess = register => ({
  type: REGISTER_PUSH_TO_SERVER_SUCCESS,
  register
});

const updatePushToServerAttempt = () => ({
  type: UPDATE_PUSH_TO_SERVER_ATTEMPT
});

const updatePushToServerSuccess = token => ({
  type: UPDATE_PUSH_TO_SERVER_SUCCESS,
  token
});

const deletePushToServerAttempt = () => ({
  type: DELETE_PUSH_TO_SERVER_ATTEMPT
});

const deletePushToServerSuccess = token => ({
  type: DELETE_PUSH_TO_SERVER_SUCCESS,
  token
});

const updateSocietiesList = newSociety => ({
  type: UPDATE_SOCIETIES_LIST,
  newSociety
});

export default {
  signInAttempt,
  signInSuccess,
  signInFail,
  getTokenAttempt,
  getTokenSuccess,
  getTokenFail,
  changeCurrentSociety,
  registerPush,
  unregisterPush,
  registerPushToServerAttempt,
  registerPushToServerSuccess,
  updatePushToServerAttempt,
  updatePushToServerSuccess,
  signOutAttempt,
  signOutSuccess,
  signOutFail,
  deletePushToServerAttempt,
  deletePushToServerSuccess,
  updateSocietiesList
};
