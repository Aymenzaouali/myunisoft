import _ from 'lodash';
import {
  SIGN_IN_ATTEMPT,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAIL,
  SIGN_OUT_SUCCESS,
  GET_TOKEN_ATTEMPT,
  GET_TOKEN_SUCCESS,
  GET_TOKEN_FAIL,
  CHANGE_CURRENT_SOCIETY,
  REGISTER_PUSH,
  UNREGISTER_PUSH,
  UPDATE_SOCIETIES_LIST
} from './constants';

const initialState = {
  token: {},
  societies: [],
  user: {},
  currentSociety: {}
};

const login = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case GET_TOKEN_ATTEMPT:
      return state;
    case GET_TOKEN_SUCCESS:
      return {
        ...state,
        token: action.data
      };
    case GET_TOKEN_FAIL:
      return state;
    case SIGN_IN_ATTEMPT:
      return state;
    case SIGN_IN_SUCCESS:
      const {
        user,
        societies
      } = action.data;
      return {
        ...state,
        societies,
        user,
        currentSociety: _.get(action.data, 'societies[0]', {})
      };
    case SIGN_OUT_SUCCESS: {
      return {
        ...state,
        token: {},
        societies: [],
        user: {},
        currentSociety: {}
      };
    }
    case SIGN_IN_FAIL:
      return state;
    case CHANGE_CURRENT_SOCIETY:
      return {
        ...state,
        currentSociety: action.society
      };
    case REGISTER_PUSH:
      return {
        ...state,
        deviceToken: action.deviceToken
      };
    case UNREGISTER_PUSH:
      return {
        ...state,
        deviceToken: undefined
      };
    case UPDATE_SOCIETIES_LIST:
      const {
        newSociety
      } = action;

      return {
        ...state,
        societies: [
          ...state.societies,
          newSociety
        ]
      };
    default: return state;
    }
  }
  return state;
};

export default login;
