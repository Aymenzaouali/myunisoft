import {
  GET_EXCHANGES_ATTEMPT,
  GET_EXCHANGES_SUCCESS,
  GET_EXCHANGES_FAIL,
  RESET_EXCHANGES_WITH,
  GET_EXCHANGE_ATTEMPT,
  GET_EXCHANGE_SUCCESS,
  GET_EXCHANGE_FAIL,
  RESET_EXCHANGE_WITH,
  GET_PARTICIPANTALLOWED_ATTEMPT,
  GET_PARTICIPANTALLOWED_SUCCESS,
  GET_PARTICIPANTALLOWED_FAIL,

  POST_MESSAGE_ATTEMPT,
  POST_MESSAGE_SUCCESS,
  POST_MESSAGE_FAIL,
  POST_POSTNEWEXCHANGE_ATTEMPT,
  POST_POSTNEWEXCHANGE_SUCCESS,
  POST_POSTNEWEXCHANGE_FAIL,

  PUT_MESSAGE_READ_ATTEMPT,
  PUT_MESSAGE_READ_SUCCESS,
  PUT_MESSAGE_READ_FAIL

} from './constants';

// -----------Exchanges'list-----------\\
const getExchangesAttempt = () => ({
  type: GET_EXCHANGES_ATTEMPT
});

const getExchangesSuccess = exchanges => ({
  type: GET_EXCHANGES_SUCCESS,
  exchanges
});

const getExchangesFail = () => ({
  type: GET_EXCHANGES_FAIL
});

const resetExchangesWith = exchanges => ({
  type: RESET_EXCHANGES_WITH,
  exchanges
});

// --------------One exchange-----------\\
const getExchangeAttempt = () => ({
  type: GET_EXCHANGE_ATTEMPT
});

const getExchangeSuccess = (messages, exchange_id) => ({
  type: GET_EXCHANGE_SUCCESS,
  messages,
  exchange_id
});

const resetExchangeWith = (messages, exchange_id) => ({
  type: RESET_EXCHANGE_WITH,
  messages,
  exchange_id
});

const getExchangeFail = () => ({
  type: GET_EXCHANGE_FAIL
});

// --------------Participant Allowed-----------\\
const getParticipantAllowedAttempt = () => ({
  type: GET_PARTICIPANTALLOWED_ATTEMPT
});

const getParticipantAllowedSuccess = participants => ({
  type: GET_PARTICIPANTALLOWED_SUCCESS,
  participants
});

const getParticipantAllowedFail = () => ({
  type: GET_PARTICIPANTALLOWED_FAIL
});


// -----------post message-----------\\

const postMessageAttempt = (exchange_id, message, name) => ({
  type: POST_MESSAGE_ATTEMPT,
  exchange_id,
  message,
  name
});

const postMessageSuccess = (exchange_id, message) => ({
  type: POST_MESSAGE_SUCCESS,
  exchange_id,
  message
});

const postMessageFail = exchange_id => ({
  type: POST_MESSAGE_FAIL,
  exchange_id
});

// -----------post new exchange-----------\\

const postNewExchangeAttempt = (titleExchange, participants, message) => ({
  type: POST_POSTNEWEXCHANGE_ATTEMPT,
  titleExchange,
  participants,
  message
});

const postNewExchangeSuccess = exchange => ({
  type: POST_POSTNEWEXCHANGE_SUCCESS,
  exchange
});

const postNewExchangeFail = exchange_id => ({
  type: POST_POSTNEWEXCHANGE_FAIL,
  exchange_id
});


// -----------put message unread-----------\\

const putMessageReadAttempt = message_id => ({
  type: PUT_MESSAGE_READ_ATTEMPT,
  message_id
});

const putMessageReadSuccess = message_id => ({
  type: PUT_MESSAGE_READ_SUCCESS,
  message_id
});

const putMessageReadFail = () => ({
  type: PUT_MESSAGE_READ_FAIL
});

export default {
  getExchangesAttempt,
  getExchangesSuccess,
  getExchangesFail,
  resetExchangesWith,
  getExchangeAttempt,
  getExchangeSuccess,
  getExchangeFail,
  resetExchangeWith,
  getParticipantAllowedAttempt,
  getParticipantAllowedSuccess,
  getParticipantAllowedFail,

  postMessageAttempt,
  postMessageSuccess,
  postMessageFail,
  postNewExchangeAttempt,
  postNewExchangeSuccess,
  postNewExchangeFail,

  putMessageReadAttempt,
  putMessageReadSuccess,
  putMessageReadFail

};
