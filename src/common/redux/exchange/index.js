import { windev } from 'helpers/api';
import { change } from 'redux-form';
import { handleError } from 'common/redux/error';
import _ from 'lodash';
import actions from './actions';

export const getExchanges = customOffset => async (dispatch, getState) => {
  try {
    const offset = customOffset === 0 ? 0 : _.get(getState(), 'exchange.list.length', 0);
    await dispatch(actions.getExchangesAttempt());
    const response = await windev.makeApiCall('exchange', 'get', { offset });
    const { data } = response;
    data.sort((a, b) => (a.last_date_message > b.last_date_message ? -1 : 1));
    if (customOffset === 0) {
      await dispatch(actions.resetExchangesWith(data));
    } else {
      await dispatch(actions.getExchangesSuccess(data));
    }
    return response;
  } catch (err) {
    await dispatch(handleError(err, getExchanges.bind(null, customOffset)));
    return err;
  }
};


export const getExchange = (exchange_id, customOffset) => async (dispatch, getState) => {
  try {
    const offset = customOffset === 0 ? 0 : _.get(getState(), `exchange.detailExchange[${exchange_id}].length`, 0);
    await dispatch(actions.getExchangeAttempt());
    const response = await windev.makeApiCall('/exchange', 'get', { exchange_id, offset });
    const { data } = response;
    if (customOffset === 0) {
      await dispatch(actions.resetExchangeWith(data, exchange_id));
    } else {
      await dispatch(actions.getExchangeSuccess(data, exchange_id));
    }
    return response;
  } catch (err) {
    await dispatch(handleError(err, getExchange.bind(null, exchange_id, customOffset)));
    return err;
  }
};

export const getParticipantAllowed = () => async (dispatch) => {
  try {
    await dispatch(actions.getParticipantAllowedAttempt());
    const response = await windev.makeApiCall('/exchange/participant_allowed', 'get', {});
    const { data } = response;
    await dispatch(actions.getParticipantAllowedSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(err, getParticipantAllowed.bind()));
    return err;
  }
};


export const postMessage = (exchange_id, message, arrayPJ = []) => async (dispatch, getState) => {
  try {
    const firstName = _.get(getState(), 'login.user.prenom', '');
    const lastName = _.get(getState(), 'login.user.nom', '');
    await dispatch(actions.postMessageAttempt(exchange_id, message, `${firstName} ${lastName}`));
    await dispatch(change('sendMessageForm', 'message', ''));
    const response = await windev.makeApiCall('exchange/message', 'post', {}, { exchange_id, content: message });
    const { data } = response;
    await dispatch(actions.postMessageSuccess(exchange_id, data));
    return response;
  } catch (err) {
    await dispatch(actions.postMessageFail(exchange_id));
    await dispatch(handleError(err, postMessage.bind(null, exchange_id, message, arrayPJ)));
    return err;
  }
};

export const postNewExchange = (
  titleExchange,
  participants,
  message
) => async (dispatch, getState) => {
  try {
    const idSociety = _.get(getState(), 'login.currentSociety.id_societe', 0);
    await dispatch(actions.postNewExchangeAttempt(titleExchange, participants, message));
    const participantsToAdd = participants.map(item => ({ physical_person_id: item }));
    const response = await windev.makeApiCall('/exchange', 'post', {}, {
      society_id: idSociety, title: titleExchange, content: message, participant: participantsToAdd
    });
    const { data } = response;
    await dispatch(actions.postNewExchangeSuccess(data));
    return data;
  } catch (err) {
    await dispatch(handleError(
      err, postNewExchange.bind(null, titleExchange, participants, message)
    ));
    throw new Error(err);
  }
};


export const putMessageRead = idMessage => async (dispatch) => {
  try {
    await dispatch(actions.putMessageReadAttempt());
    const response = await windev.makeApiCall('/Message', 'put', { idMessage, isRead: true });
    const { data } = response;
    await dispatch(actions.putMessageReadSuccess(data, idMessage));
    return response;
  } catch (err) {
    await dispatch(actions.putMessageReadFail());
    await dispatch(handleError(err, putMessageRead.bind(null, idMessage)));
    return err;
  }
};
