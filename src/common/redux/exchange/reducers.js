import moment from 'moment';
import _ from 'lodash';
import {
  GET_EXCHANGES_ATTEMPT,
  GET_EXCHANGES_SUCCESS,
  GET_EXCHANGES_FAIL,
  RESET_EXCHANGES_WITH,
  GET_EXCHANGE_ATTEMPT,
  GET_EXCHANGE_SUCCESS,
  GET_EXCHANGE_FAIL,
  RESET_EXCHANGE_WITH,
  GET_PARTICIPANTALLOWED_ATTEMPT,
  GET_PARTICIPANTALLOWED_SUCCESS,
  GET_PARTICIPANTALLOWED_FAIL,

  POST_MESSAGE_ATTEMPT,
  POST_MESSAGE_SUCCESS,
  POST_MESSAGE_FAIL,
  POST_POSTNEWEXCHANGE_ATTEMPT,
  POST_POSTNEWEXCHANGE_SUCCESS,
  POST_POSTNEWEXCHANGE_FAIL,

  PUT_MESSAGE_READ_ATTEMPT,
  PUT_MESSAGE_READ_SUCCESS,
  PUT_MESSAGE_READ_FAIL

} from './constants';

const initialState = {
  getExchangesIsLoading: false,
  getExchangeIsLoading: false,
  getParticipantsIsLoading: false,
  list: [],
  participants: [],
  offset: 0,
  limit: 20,
  detailExchange: {},
  loadingMessages: {}
};

const exchange = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    // -----------Exchanges'list-----------\\
    case GET_EXCHANGES_ATTEMPT:
      return {
        ...state,
        getExchangesIsLoading: true
      };
    case GET_EXCHANGES_SUCCESS:
      return {
        ...state,
        list: [...state.list, ...action.exchanges],
        getExchangesIsLoading: false
      };
    case RESET_EXCHANGES_WITH:
      return {
        ...state,
        list: action.exchanges,
        getExchangesIsLoading: false
      };
    case GET_EXCHANGES_FAIL:
      return {
        ...state,
        getExchangesIsLoading: false
      };

      // --------------One exchange-----------\\
    case GET_EXCHANGE_ATTEMPT:
      return {
        ...state,
        getExchangeIsLoading: true
      };
    case GET_EXCHANGE_SUCCESS:
      return {
        ...state,
        detailExchange: {
          [action.exchange_id]: [
            ..._.get(state, `detailExchange[${action.exchange_id}]`, []),
            ...action.messages
          ]
        },
        getExchangeIsLoading: false
      };
    case GET_EXCHANGE_FAIL:
      return {
        ...state,
        getExchangeIsLoading: false
      };
    case RESET_EXCHANGE_WITH:
      return {
        ...state,
        detailExchange: {
          [action.exchange_id]: action.messages
        },
        getExchangeIsLoading: false
      };

      // -----------get Participants allowed-----------\\
    case GET_PARTICIPANTALLOWED_ATTEMPT:
      return {
        ...state,
        getParticipantsIsLoading: true
      };
    case GET_PARTICIPANTALLOWED_SUCCESS:
      return {
        ...state,
        participants: action.participants,
        getParticipantsIsLoading: false
      };
    case GET_PARTICIPANTALLOWED_FAIL:
      return {
        ...state,
        getParticipantsIsLoading: false
      };


      // -----------POST message-----------\\
    case POST_MESSAGE_ATTEMPT:
      return {
        ...state,
        detailExchange: {
          [action.exchange_id]: [{
            exchange_id: action.exchange_id,
            content: action.message,
            last_date_message: moment(),
            physical_person_name: action.name
          }, ..._.get(state, `detailExchange[${action.exchange_id}]`, [])]
        },
        loadingMessages: {
          [action.exchange_id]: true
        }
      };
    case POST_MESSAGE_SUCCESS:
      return {
        ...state,
        detailExchange: {
          [action.exchange_id]: _.get(state, `detailExchange[${action.exchange_id}]`, []).map((message) => {
            if (message.exchange_id === action.exchange_id) {
              return { ...action.message };
            }
            return message;
          })
        },
        loadingMessages: {
          [action.exchange_id]: false
        }
      };
    case POST_MESSAGE_FAIL:
      return {
        ...state,
        detailExchange: {
          [action.exchange_id]: _.get(state, `detailExchange[${action.exchange_id}]`, []).map((message) => {
            if (message.exchange_id === action.exchange_id) {
              return { ...message, fail: true };
            }
            return message;
          })
        },
        loadingMessages: {
          [action.exchange_id]: false
        }
      };

      // -----------POST New Exchange-----------\\
    case POST_POSTNEWEXCHANGE_ATTEMPT:
      return {
        ...state,
        titleExchange: action.titleExchange,
        participants: action.participants,
        message: action.message
      };
    case POST_POSTNEWEXCHANGE_SUCCESS:
      return {
        ...state,
        list: [{
          exchange_id: action.exchange.exchange_id,
          ...action.exchange.exchange_message
        },
        ...state.list]
        // exchange_id: action.exchange_id,
        // exchange_message: action.exchange_message
      };
    case POST_POSTNEWEXCHANGE_FAIL:
      return {
        ...state
      };


      // -----------put message unread-----------\\
    case PUT_MESSAGE_READ_ATTEMPT:
      return {
        ...state
      };
    case PUT_MESSAGE_READ_SUCCESS:
      return {
        ...state
      };
    case PUT_MESSAGE_READ_FAIL:
      return {
        ...state
      };
    default: return state;
    }
  }
  return state;
};

export default exchange;
