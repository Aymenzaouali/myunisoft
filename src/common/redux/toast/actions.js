import {
  ADD_TOAST,
  DELETE_TOAST
} from './constants';

const addToast = (idToast, textToast) => ({
  type: ADD_TOAST,
  idToast,
  textToast
});

const deleteToast = idToast => ({
  type: DELETE_TOAST,
  idToast
});

export default {
  addToast,
  deleteToast
};
