import actions from './actions';

export const addToast = textToast => async (dispatch) => { // eslint-disable-line
  const idToast = Math.floor(Math.random() * 100000);
  await dispatch(actions.addToast(idToast, textToast));
  setTimeout(() => {
    dispatch(actions.deleteToast(idToast));
  }, 1000);
};
