import _ from 'lodash';
import {
  ADD_TOAST,
  DELETE_TOAST
} from './constants';

const initialState = {
  toasts: []
};

const toast = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case ADD_TOAST:
      return {
        ...state,
        toasts: [...state.toasts, { idToast: action.idToast, textToast: action.textToast }]
      };
    case DELETE_TOAST:
      return {
        ...state,
        toasts: state.toasts.filter(toast => !_.isEqual(toast.idToast, action.idToast))
      };
    default: return state;
    }
  }
  return state;
};

export default toast;
