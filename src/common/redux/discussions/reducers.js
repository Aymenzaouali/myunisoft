import _ from 'lodash';

import {
  GET_FOLDERS_SUCCESS,
  // GET_FOLDERS_FAIL,
  GET_ROOM_SUCCESS,
  GET_ROOM_MESSAGES_ATTEMPT,
  GET_ROOM_MESSAGES_SUCCESS,
  GET_ROOM_MESSAGES_FAIL,
  CREATE_MESSAGE_SUCCESS,
  GET_ROOM_AVAILABLE_USERS_SUCCESS,
  GET_ROOM_AVAILABLE_IMAGES_SUCCESS,
  GET_ROOM_GEDITEMS_SUCCESS,
  CREATE_ROOM_SUCCESS,
  PATCH_ROOM_SUCCESS,
  MESSAGE_PATCH_SUCCESS,
  MESSAGE_REACT_CHANGED_SUCCESS,
  SET_CURRENT_ROOM_AND_FOLDER,
  SET_ACTIVE_ROOM_ID,
  SET_ROOM_UNREAD_MESSAGES
} from './constants';

import { getFlatFolders, Delta } from './common';

const initialState = {
  folders: null, // list of ALL folders (filled by necessity via ajax "/folders" call)
  rooms: [], // list of ALL rooms (filled by necessity via ajax "/rooms/:id" calls)
  roomMessages: {}, // all rooms messages: {<roomId>: [</rooms/<id>/messages>]}
  lastCreatedMessage: null, // used to scroll to it
  roomAvailableUsers: {}, // {<roomIdTyped>: [<users>]}
  roomAvailableImages: {}, // {<roomIdTyped>: [<availableImages>]}
  roomGEDItems: {}, // all room GEDitems: {<roomId>: [{id, filename, preview, file}, ...]}
  lastCreatedRoom: null,
  activeRoomId: null
};

const balance = (state = initialState, action) => {
  switch (action.type) {
  case GET_FOLDERS_SUCCESS: {
    const { folders } = action;

    return {
      ...state,
      folders
    };
  }
  case GET_ROOM_SUCCESS: {
    const { room } = action;

    const rooms = _.cloneDeep(state.rooms);
    const index = state.rooms.findIndex(item => item.room_id === room.room_id);

    if (index !== -1) {
      rooms.splice(index, 1, room);
    } else {
      rooms.push(room);
    }

    return {
      ...state,
      rooms
    };
  }
  case GET_ROOM_MESSAGES_ATTEMPT: {
    return {
      ...state,
      isLoading: true
    };
  }
  case GET_ROOM_MESSAGES_FAIL: {
    return {
      ...state,
      isLoading: false
    };
  }
  case GET_ROOM_MESSAGES_SUCCESS: {
    const { roomId, messages, hardReset } = action;

    // STUB: correctize backend data until fixed: make user as subobject
    messages.forEach((item) => {
      if (!item.user) {
        item.user = { // eslint-disable-line
          avatar: item.avatar,
          id: item.user_id,
          nom: item.nom,
          prenom: item.prenom
        };
      }
    });

    const roomMessages = _.cloneDeep(state.roomMessages);
    if (hardReset) {
      roomMessages[roomId] = messages;
    } else {
      if (!roomMessages[roomId]) roomMessages[roomId] = [];
      roomMessages[roomId] = [
        ...roomMessages[roomId],
        ...messages
      ];
    }

    return {
      ...state,
      isLoading: false,
      roomMessages
    };
  }
  case CREATE_MESSAGE_SUCCESS: {
    const { message } = action;
    const lastCreatedMessage = message;

    // STUB: correctize backend data until fixed: make user as subobject
    if (!message.user.id) {
      message.user.id = message.user_id;
    }

    const roomMessages = _.cloneDeep(state.roomMessages);

    if (!roomMessages[message.room_id]) roomMessages[message.room_id] = [];
    // clone
    roomMessages[message.room_id] = [message, ...roomMessages[message.room_id]];

    // roomMessages[message.room_id].unshift(message); // assumption: ordered by "newer first"

    // increment unread_messages (need think better way than in folders)
    const folders = _.cloneDeep(state.folders);

    return {
      ...state,
      folders,
      roomMessages,
      lastCreatedMessage
    };
  }
  case SET_ROOM_UNREAD_MESSAGES: {
    const { roomId, value } = action;

    const folders = _.cloneDeep(state.folders);
    const flatFolders = getFlatFolders(folders);

    let foundRoom = null;
    flatFolders.find(
      (item) => {
        if (!item.rooms) return false;

        foundRoom = item.rooms.find(
          room => room.room_id === roomId
        );

        return foundRoom;
      }
    );

    switch (value) {
    case Delta.increment:
      foundRoom.unread_messages += 1;
      break;
    case Delta.decrement:
      foundRoom.unread_messages -= 1;
      break;
    default:
      foundRoom.unread_messages = value;
    }

    return {
      ...state,
      folders
    };
  }
  case GET_ROOM_AVAILABLE_USERS_SUCCESS: {
    const { roomIdTyped, users } = action;

    const roomAvailableUsers = _.cloneDeep(state.roomAvailableUsers);
    roomAvailableUsers[roomIdTyped] = users;

    return {
      ...state,
      roomAvailableUsers
    };
  }
  case GET_ROOM_AVAILABLE_IMAGES_SUCCESS: {
    const { roomId, images } = action;

    const roomAvailableImages = _.cloneDeep(state.roomAvailableImages);
    roomAvailableImages[roomId] = images;

    return {
      ...state,
      roomAvailableImages
    };
  }
  case GET_ROOM_GEDITEMS_SUCCESS: {
    const { roomId, GEDItems } = action;

    const roomGEDItems = _.cloneDeep(state.roomGEDItems);
    roomGEDItems[roomId] = GEDItems;

    return {
      ...state,
      roomGEDItems
    };
  }
  case CREATE_ROOM_SUCCESS: {
    const { data } = action;
    const { folder_id } = data;

    // patch rooms
    const rooms = _.cloneDeep(state.rooms);
    rooms.push(data);

    // patch also folder -> rooms
    const folders = _.cloneDeep(state.folders);
    const flatFolders = getFlatFolders(folders);

    const folder = flatFolders.find(item => item.folder_id === folder_id);
    folder.rooms.push(data);

    return {
      ...state,
      rooms,
      folders,
      lastCreatedRoom: data
    };
  }
  case PATCH_ROOM_SUCCESS: {
    const { roomId, data } = action;

    const rooms = _.cloneDeep(state.rooms);
    const room = rooms.find(
      item => item.room_id === roomId
    );

    Object.assign(room, data); // in-place modify

    return {
      ...state,
      rooms
    };
  }
  case MESSAGE_PATCH_SUCCESS: {
    const { roomId, messageId, data } = action;
    if (!state.roomMessages[roomId]) return state;
    const roomMessages = {
      ...state.roomMessages,
      [roomId]: state.roomMessages[roomId].map((msg) => {
        if (msg.message_id === messageId) {
          return {
            ...msg,
            ...data
          };
        }
        return msg;
      })
    };
    return {
      ...state,
      roomMessages
    };
  }
  case MESSAGE_REACT_CHANGED_SUCCESS: {
    const { roomId, messageId, reactions } = action;

    const roomMessages = _.cloneDeep(state.roomMessages);
    const messages = roomMessages[roomId];
    const message = messages.find(
      item => item.message_id === messageId
    );
    message.reactions = reactions;

    return {
      ...state,
      roomMessages
    };
  }
  case SET_CURRENT_ROOM_AND_FOLDER: {
    const { roomId, folderId } = action;
    return {
      ...state,
      currentRoomId: roomId,
      currentFolderId: folderId
    };
  }
  case SET_ACTIVE_ROOM_ID: {
    const { roomId } = action;
    return {
      ...state,
      activeRoomId: roomId
    };
  }
  default:
    return state;
  }
};

export default balance;
