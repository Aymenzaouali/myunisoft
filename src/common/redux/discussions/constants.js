export const GET_FOLDERS_SUCCESS = 'discussions/GET_FOLDERS_SUCCESS';
export const GET_FOLDERS_FAIL = 'discussions/GET_FOLDERS_FAIL';

export const GET_ROOM_SUCCESS = 'discussions/GET_ROOM_SUCCESS';

export const GET_ROOM_MESSAGES_ATTEMPT = 'discussions/GET_ROOM_MESSAGES_ATTEMPT';
export const GET_ROOM_MESSAGES_SUCCESS = 'discussions/GET_ROOM_MESSAGES_SUCCESS';
export const GET_ROOM_MESSAGES_FAIL = 'discussions/GET_ROOM_MESSAGES_FAIL';

export const CREATE_MESSAGE_SUCCESS = 'discussions/CREATE_MESSAGE';
export const MODIFY_MESSAGE_SUCCESS = 'discussions/MODIFY_MESSAGE';
export const DELETE_MESSAGE_SUCCESS = 'discussions/DELETE_MESSAGE';

export const GET_ROOM_DOCS_SUCCESS = 'discussions/GET_ROOM_DOCS_SUCCESS';

export const GET_ROOM_AVAILABLE_USERS_SUCCESS = 'discussions/GET_ROOM_AVAILABLE_USERS_SUCCESS';

export const GET_ROOM_AVAILABLE_IMAGES_SUCCESS = 'discussions/GET_ROOM_AVAILABLE_IMAGES_SUCCESS';

export const GET_ROOM_GEDITEMS_SUCCESS = 'discussions/GET_ROOM_GEDITEMS_SUCCESS';

export const CREATE_ROOM_SUCCESS = 'discussions/CREATE_ROOM_SUCCESS';
export const PATCH_ROOM_SUCCESS = 'discussions/PATCH_ROOM_SUCCESS';
export const MESSAGE_PATCH_SUCCESS = 'discussions/MESSAGE_PATCH_SUCCESS';

export const MESSAGE_REACT_CHANGED_SUCCESS = 'discussions/MESSAGE_REACT_CHANGED_SUCCESS';

export const SET_CURRENT_ROOM_AND_FOLDER = 'discussions/SET_CURRENT_ROOM_AND_FOLDER';

export const SET_ACTIVE_ROOM_ID = 'SET_ACTIVE_ROOM_ID';

export const SET_ROOM_UNREAD_MESSAGES = 'SET_ROOM_UNREAD_MESSAGES';
