import { service_discussion } from 'helpers/api';
import { handleError } from '../error';
import { Delta } from './common';
import {
  getFoldersSuccess,
  // getFoldersFail
  getRoomSuccess,
  getRoomMessagesSuccess,
  getRoomMessagesAttempt,
  // getRoomMessagesFail,
  createMessageSuccess,
  // updateMessageSuccess,
  // deleteMessageSuccess
  getRoomAvailableUsersSuccess,
  getRoomAvailableImagesSuccess,
  getGEDItemsSuccess,
  createRoomSuccess,
  patchRoomSuccess,
  messagePatchSuccess,
  messageReactChangedSuccess,
  setActiveRoomIdSuccess,
  setRoomUnreadMessages
} from './actions';

import { testGEDItems } from './testData';

export const ERROR_CODE = 500;

export const getFolders = () => async (dispatch/* , getState */) => {
  try {
    const r = (
      (await service_discussion.makeApiCall('/folders', 'get', {})).data
    );

    await dispatch(getFoldersSuccess(r));
  } catch (err) {
    // await dispatch(getFoldersFail(err));
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const getRoom = id => async (dispatch, getState) => {
  const { rooms } = getState().discussions;

  try {
    const r = (
      rooms.find(item => item.room_id === id)
        || (await service_discussion.makeApiCall(`/rooms/${id}`, 'get', {})).data
    );

    await dispatch(getRoomSuccess(r));
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const getRoomMessages = (
  roomId, params = { limit: 20, offset: 0 }, hardReset
) => async (dispatch, getState) => {
  const { roomMessages } = getState().discussions;

  try {
    await dispatch(getRoomMessagesAttempt());
    if (!hardReset && !roomMessages[roomId]) {
      const { data } = await service_discussion.makeApiCall(`/rooms/${roomId}/messages`, 'get', params);
      await dispatch(getRoomMessagesSuccess(roomId, data, hardReset));
    }
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};


export const createMessageInternal = message => async (dispatch, getState) => {
  const { activeRoomId } = getState().discussions;

  await dispatch(createMessageSuccess(message));

  if (activeRoomId === message.room_id) {
    await dispatch(resetRoomUnreadMessages(message.room_id));
  } else {
    await dispatch(setRoomUnreadMessages(message.room_id, Delta.increment));
  }
};

export const createMessage = (room_id, data) => async (dispatch, getState) => {
  try {
    const r = (await service_discussion.makeApiCall(`/rooms/${room_id}/messages`, 'post', {}, data)).data;

    // supply result with required fields until fixed https://myunisoft.atlassian.net/browse/MYUN-3254
    r.reactions = r.reactions || [];
    r.documents = r.documents || [];

    dispatch(createMessageInternal(r));
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const reactMessage = (
  roomId, messageId, reactionChar
) => async (dispatch) => {
  try {
    const r = (await service_discussion.makeApiCall(`/rooms/${roomId}/messages/${messageId}/reaction`, 'post', {}, { reaction: reactionChar })).data;

    await dispatch(messageReactChangedSuccess(roomId, messageId, r.reactions));
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const unreactMessage = (roomId, messageId, reactionId) => async (dispatch) => {
  try {
    const r = (await service_discussion.makeApiCall(`/rooms/${roomId}/messages/${messageId}/reaction/${reactionId}`, 'delete', {}, {})).data;

    await dispatch(messageReactChangedSuccess(roomId, messageId, r.reactions));
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const getRoomAvailableUsers = (
  room_id, room_type_id, society_id, force
) => async (dispatch, getState) => {
  const { roomAvailableUsers } = getState().discussions;

  const roomIdTyped = `${room_id}-${room_type_id}-${society_id}`;

  function patchAPIResponse(resp) {
    // patch user id type (until https://myunisoft.atlassian.net/browse/MYUN-3469 fixed)
    resp.forEach((item) => {
      // eslint-disable-next-line no-param-reassign
      item.user_id = Number(item.user_id);
    });
    return resp;
  }

  const params = new URLSearchParams({
    room_id,
    type_id: room_type_id,
    society_id
  });

  try {
    if (force || !roomAvailableUsers[roomIdTyped]) {
      const users = patchAPIResponse((await service_discussion.makeApiCall(`/rooms/${room_id}/availableUsers?${params.toString()}`, 'get', {})).data.users);
      await dispatch(getRoomAvailableUsersSuccess(roomIdTyped, users));
    }
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const getRoomAvailableImages = roomId => async (dispatch, getState) => {
  const { roomAvailableImages } = getState().discussions;

  try {
    if (!roomAvailableImages[roomId]) {
      const { data } = await service_discussion.makeApiCall(`/search/${roomId}/images`, 'get', {});
      await dispatch(getRoomAvailableImagesSuccess(roomId, data));
    }
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};


export const roomAddUsers = data => async (dispatch/* , getState */) => {
  const { room_id, users } = data;

  try {
    const r = await service_discussion.makeApiCall(
      `/rooms/${room_id}/users`, 'post', {}, { users }
    );

    await dispatch(patchRoomSuccess(room_id, r.data)); // update whole room by response
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const roomDeleteUsers = data => async (dispatch /* , getState */) => {
  const { room_id, ids } = data;

  try {
    const r = await service_discussion.makeApiCall(
      `/rooms/${room_id}/users`, 'delete', {}, { ids }
    );

    await dispatch(patchRoomSuccess(room_id, r.data)); // sic
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};


export const getRoomGEDItems = room_id => async (dispatch, getState) => {
  const { roomGEDItems } = getState().discussions;

  try {
    if (!roomGEDItems[room_id]) {
      const r = testGEDItems[room_id]; // dummy until backend fixed
      // (await service_discussion.makeApiCall(`/rooms/${room_id}/ged`, 'get', {})).data
      await dispatch(getGEDItemsSuccess(room_id, r));
    }
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};


export const createRoom = data => async (dispatch) => {
  try {
    const r = (await service_discussion.makeApiCall('/rooms', 'post', {}, data)).data;

    // patching obsolete named fields data
    r.members.forEach(
      item => item.user_id = Number(item.id_pers_physique)
    );

    await dispatch(createRoomSuccess(r));
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const patchRoom = (roomId, data) => async (dispatch) => {
  try {
    const r = await service_discussion.makeApiCall(
      `/rooms/${roomId}`, 'patch', {}, data
    );
    await dispatch(patchRoomSuccess(roomId, r.data));
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const messagePatch = (roomId, messageId, data) => async (dispatch) => {
  try {
    // fix backend bug (should be "body" only) https://myunisoft.atlassian.net/browse/MYUN-3308
    data.message = data.body; // eslint-disable-line no-param-reassign

    const r = await service_discussion.makeApiCall(
      `/rooms/${roomId}/messages/${messageId}`, 'patch', {}, data
    );
    await dispatch(messagePatchSuccess(roomId, messageId, r.data));
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const setActiveRoomId = roomId => async (dispatch) => {
  await dispatch(setActiveRoomIdSuccess(roomId));

  // mark all room messages as read (similar to server behavior)
  await dispatch(resetRoomUnreadMessages(roomId));
};

export const resetRoomUnreadMessages = roomId => async (dispatch) => {
  if (!roomId) return;

  await dispatch(setRoomUnreadMessages(roomId, 0));

  // reset on SERVER (it's the only way to clear it now)
  try {
    await service_discussion.makeApiCall(`/rooms/${roomId}`, 'get', {});
  } catch (err) {
    await dispatch(handleError({
      response: {
        data: {
          message: err.message || String(err),
          code: ERROR_CODE
        }
      }
    }));
  }
};

export const initUnreadMessagesStat = () => async (dispatch, getState) => {
  const { folders } = getState().discussions;
  if (!folders) {
    await dispatch(getFolders());
  }
};
