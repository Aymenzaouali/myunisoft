import {
  GET_FOLDERS_SUCCESS,
  // GET_FOLDERS_FAIL,
  GET_ROOM_SUCCESS,
  GET_ROOM_MESSAGES_ATTEMPT,
  GET_ROOM_MESSAGES_SUCCESS,
  GET_ROOM_MESSAGES_FAIL,
  // GET_ROOM_DOCS_SUCCESS,
  CREATE_MESSAGE_SUCCESS,
  MODIFY_MESSAGE_SUCCESS,
  DELETE_MESSAGE_SUCCESS,
  GET_ROOM_AVAILABLE_USERS_SUCCESS,
  GET_ROOM_AVAILABLE_IMAGES_SUCCESS,
  // ROOM_ADD_USERS_SUCCESS,
  // ROOM_DELETE_USERS_SUCCESS
  GET_ROOM_GEDITEMS_SUCCESS,
  CREATE_ROOM_SUCCESS,
  PATCH_ROOM_SUCCESS,
  MESSAGE_PATCH_SUCCESS,
  MESSAGE_REACT_CHANGED_SUCCESS,
  SET_CURRENT_ROOM_AND_FOLDER,
  SET_ACTIVE_ROOM_ID,
  SET_ROOM_UNREAD_MESSAGES
} from './constants';

export const getFoldersSuccess = folders => ({
  type: GET_FOLDERS_SUCCESS,
  folders
});

// export const getFoldersFail = error => ({
//   type: GET_FOLDERS_FAIL,
//   error
// });

export const getRoomSuccess = room => ({
  type: GET_ROOM_SUCCESS,
  room
});

export const getRoomMessagesAttempt = () => ({
  type: GET_ROOM_MESSAGES_ATTEMPT
});

export const getRoomMessagesFail = () => ({
  type: GET_ROOM_MESSAGES_FAIL
});

export const getRoomMessagesSuccess = (roomId, messages, hardReset) => ({
  type: GET_ROOM_MESSAGES_SUCCESS,
  roomId,
  messages,
  hardReset
});

export const createMessageSuccess = message => ({
  type: CREATE_MESSAGE_SUCCESS,
  message
});

export const modifyMessageSuccess = message => ({
  type: MODIFY_MESSAGE_SUCCESS,
  message
});

export const deleteMessageSuccess = id => ({
  type: DELETE_MESSAGE_SUCCESS,
  id
});

export const getRoomAvailableUsersSuccess = (roomIdTyped, users) => ({
  type: GET_ROOM_AVAILABLE_USERS_SUCCESS,
  roomIdTyped,
  users
});

export const getRoomAvailableImagesSuccess = (roomId, images) => ({
  type: GET_ROOM_AVAILABLE_IMAGES_SUCCESS,
  roomId,
  images
});

export const getGEDItemsSuccess = (roomId, GEDItems) => ({
  type: GET_ROOM_GEDITEMS_SUCCESS,
  roomId,
  GEDItems
});

export const createRoomSuccess = data => ({
  type: CREATE_ROOM_SUCCESS,
  data
});

export const patchRoomSuccess = (roomId, data) => ({
  type: PATCH_ROOM_SUCCESS,
  roomId,
  data
});

export const messagePatchSuccess = (roomId, messageId, data) => ({
  type: MESSAGE_PATCH_SUCCESS,
  roomId,
  messageId,
  data
});

export const messageReactChangedSuccess = (roomId, messageId, reactions) => ({
  type: MESSAGE_REACT_CHANGED_SUCCESS,
  roomId,
  messageId,
  reactions
});

export const setCurrentRoomAndFolder = (roomId, folderId) => ({
  type: SET_CURRENT_ROOM_AND_FOLDER,
  roomId,
  folderId
});

export const setActiveRoomIdSuccess = roomId => ({
  type: SET_ACTIVE_ROOM_ID,
  roomId
});

export const setRoomUnreadMessages = (roomId, value) => ({
  type: SET_ROOM_UNREAD_MESSAGES,
  roomId,
  value
});
