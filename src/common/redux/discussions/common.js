import _ from 'lodash';
import memoize from 'common/helpers/memoize';

// GET /folders ->folder_type_id field
export const FOLDER_TYPE_GENERAL = '1';
export const FOLDER_TYPE_DOSSIERS = '2';
export const FOLDER_TYPE_DIRECT = '3';

export function cloneFolders(folders, getAdditionalAttributes, filterFunc, otherFunc, parentFolder = null) {
  const r = [];

  let i = folders.length;
  while (i--) { // eslint-disable-line
    const item = folders[i];
    // any additional func for item
    if (otherFunc) otherFunc(item, parentFolder);

    if (filterFunc && !filterFunc(item, parentFolder)) continue; // eslint-disable-line

    const clone = _.cloneDeep(item);

    clone.folders = item.folders
      ? cloneFolders(item.folders, getAdditionalAttributes, filterFunc, otherFunc, item)
      : [];

    clone.rooms = item.rooms
      ? cloneFolders(item.rooms, getAdditionalAttributes, filterFunc, otherFunc, item)
      : [];

    if (getAdditionalAttributes) {
      Object.assign(clone, getAdditionalAttributes(clone));
    }

    r.unshift(clone);
  }

  return r;
}

// in-place sort folders
export function sortFolders(folders, compareFunc = null) {
  let f = folders;

  if (compareFunc) {
    f.sort(compareFunc);

    f = f.map((item) => {
      if (item.folders) {
        // eslint-disable-next-line no-param-reassign
        item.folders = sortFolders(item.folders, compareFunc);
      }

      if (item.rooms) {
        item.rooms = sortFolders(item.rooms, compareFunc); // eslint-disable-line no-param-reassign
      }

      return item;
    });
  }

  return f;
}


export function cloneAndSortFolders(folders, compareFunc, getAdditionalAttributes, filterFunc) {
  const newFolders = cloneFolders(folders, getAdditionalAttributes, filterFunc);
  sortFolders(newFolders, compareFunc);
  return newFolders;
}

// avoid copy-paste with commmon.js
export const getFlatFolders = (folder, parentFolders = []) => (
  folder.reduce(
    (r, v) => {
      // add additional fields
      v.id = v.room_id || v.folder_id; // eslint-disable-line no-param-reassign
      v.parents = parentFolders; // eslint-disable-line no-param-reassign

      r.push(v);

      if (v.folders) {
        r.push(
          ...getFlatFolders(v.folders, parentFolders.concat(v.id))
        );
      }

      if (v.rooms) {
        r.push(
          ...getFlatFolders(v.rooms, parentFolders.concat(v.id))
        );
      }

      return r;
    },
    []
  )
);

// return visible folders for active society and statistics basing on redux state
function getUnreadMessagesStat(state, unfreezeMemoizeParam) {
  const activeSocietyId = String(state.navigation.id);
  const isPersonalTab = activeSocietyId === '-2'; // seems that criterion
  const foldersAll = state.discussions.folders;

  if (!foldersAll) {
    return {
      unreadMessagesOfActiveSociety: null,
      unreadMessagesTotal: null
    };
  }

  const directMessagesFolder = foldersAll.find(
    item => item.type_folder_id === FOLDER_TYPE_DIRECT
  );

  let unreadMessagesTotal = 0;
  let unreadMessagesOfActiveSociety = 0;

  // don't need result, calling only for recursion
  cloneFolders(
    foldersAll,
    null,
    (item) => {
      // skip General for non-Personal tab
      if (!isPersonalTab && item.root && (item.type_folder_id === FOLDER_TYPE_GENERAL)) {
        return null;
      }

      const isFolder = 'type_folder_id' in item;

      // skip not active society folders (except personal)
      if (!isPersonalTab
          && !item.root
          && item.parent_id !== directMessagesFolder.folder_id
          && activeSocietyId
          && isFolder
          && item.society_id !== activeSocietyId
      ) {
        return null;
      }

      return item;
    },
    (item, parentFolder) => {
      // summarize unread messages (total and by society)

      if (!item.unread_messages) return;

      const unreadMessages = item.unread_messages || 0;

      // const socifetyId = getDossiersTopFolderSocietyId(item);
      if (activeSocietyId && parentFolder.society_id === activeSocietyId) {
        unreadMessagesOfActiveSociety += unreadMessages;
      }

      unreadMessagesTotal += unreadMessages;
    }
  );

  // no need this value for personal tab
  if (isPersonalTab) unreadMessagesOfActiveSociety = null;

  return {
    unreadMessagesOfActiveSociety, unreadMessagesTotal
  };
}

export const getUnreadMessagesStatMemo = memoize(getUnreadMessagesStat);

export const Delta = {
  increment: 'INCREMENT',
  decrement: 'DECREMENT'
};
