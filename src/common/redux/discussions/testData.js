

// https://yoda.myunisoft.fr:1345/folders?a=11 + some nestings + canAdd + locked

// copypaste of real db test data
export const testFolders = [
  {
    folder_id: 1,
    title: 'Général',
    user_id: 6,
    root: true,
    parent_id: null,
    rooms: [],
    folders: [
      {
        folder_id: 11,
        title: 'Social',
        user_id: 6,
        root: false,
        parent_id: 1,
        can_add: true,
        rooms: [
          {
            folder_id: 111,
            room_title: 'Paris tours',
            room_id: 111,
            room_type_id: 1,
            room_type_label: 'Public',
            unread_message_count: 0
          },
          {
            folder_id: 111,
            room_title: 'Rome tours',
            room_id: 112,
            room_type_id: 1,
            room_type_label: 'Public',
            unread_message_count: 0
          },
          {
            folder_id: 111,
            room_title: 'Amsterdam tours',
            room_id: 113,
            room_type_id: 1,
            room_type_label: 'Public',
            unread_message_count: 0
          },
          {
            folder_id: 111,
            room_title: 'Istanbul tours',
            room_id: 114,
            room_type_id: 1,
            room_type_label: 'Public',
            unread_message_count: 0
          }
        ]
      },
      {
        folder_id: 12,
        title: 'ABCD folder',
        user_id: 6,
        root: false,
        can_add: true,
        parent_id: 1
      }
    ]
  },
  {
    folder_id: 2,
    title: 'Dossiers',
    user_id: 6,
    root: true,
    parent_id: null,
    rooms: [
      {
        folder_id: 22,
        room_title: 'Some archived room long caption',
        room_id: 22,
        room_type_id: 1,
        room_type_label: 'Public',
        is_archived: true,
        unread_message_count: 0
      },
      {
        folder_id: 6,
        room_title: 'Choose me',
        room_id: 6, // number used for test room
        room_type_id: 1,
        room_type_label: 'Public',
        unread_message_count: 0
      },
      {
        folder_id: 24,
        room_title: 'Some private',
        room_id: 24, // number used for test room
        room_type_id: 2,
        room_type_label: 'Private',
        unread_message_count: 0
      }
    ],
    folders: [
      {
        folder_id: 5,
        title: 'Juridique',
        user_id: 6,
        root: false,
        parent_id: 2,
        folders: [
          {
            folder_id: 51,
            title: 'Myjur',
            user_id: 6,
            root: false,
            parent_id: 1,
            rooms: [
              {
                folder_id: 5111,
                room_title: 'jur room',
                room_id: 5111,
                room_type_id: 1,
                room_type_label: 'Public',
                unread_message_count: 0
              },
              {
                folder_id: 5112,
                room_title: 'abc room',
                room_id: 5112,
                room_type_id: 1,
                room_type_label: 'Public',
                unread_message_count: 0
              },
              {
                folder_id: 5113,
                room_title: 'def room',
                room_id: 5113,
                room_type_id: 1,
                room_type_label: 'Public',
                unread_message_count: 0
              }
            ]
          }
        ],
        rooms: [
          {
            folder_id: 651,
            room_title: 'Holidays',
            room_id: 651,
            room_type_id: 2,
            room_type_label: 'Private',
            unread_message_count: 0
          },
          {
            folder_id: 652,
            room_title: 'Super cool room',
            room_id: 652,
            room_type_id: 1,
            room_type_label: 'Public',
            unread_message_count: 0
          }
        ]
      }
    ]
  },
  {
    folder_id: 3,
    title: 'Messages directs',
    user_id: 6,
    root: true,
    parent_id: null,
    rooms: [
      {
        folder_id: 31,
        room_title: 'Room One',
        room_id: 31,
        room_type_id: 3,
        room_type_label: 'Direct',
        unread_message_count: 0
      }
    ],
    folders: []
  }
];

//
export const testRooms = {
  6: {
    member_count: '3',
    room_id: 6,
    title: 'Choose ME',
    user_id: 22,
    nom: 'John',
    prenom: 'Createrson',
    created_by: {
      user_id: 122,
      prenom: 'Benjamingo',
      nom: 'Pinoko'
    },
    avatar: null,
    folder_id: 2,
    room_type_id: 2,
    is_archived: false,
    created_at: '2019-05-29T12:57:05.391Z',
    updated_at: '2019-05-29T12:57:05.391Z',
    label: 'Public',
    users: [
      {
        nom: 'Edel',
        prenom: 'Apoula',
        id_pers_physique: '9',
        avatar: '/android-icon-36x36.png'
      },
      {
        nom: 'Molongo',
        prenom: 'Francky',
        id_pers_physique: '8'
      },
      {
        nom: 'Finot',
        prenom: 'Benjamin',
        id_pers_physique: 6,
        avatar: '/android-icon-36x36.png'
      },
      {
        nom: 'Smith',
        prenom: 'Thomas',
        id_pers_physique: '5',
        avatar: '/android-icon-36x36.png'
      },
      {
        nom: 'Dylan',
        prenom: 'Bob',
        id_pers_physique: '4',
        avatar: '/android-icon-36x36.png'
      },
      {
        nom: 'Frampton',
        prenom: 'Peter',
        id_pers_physique: '3',
        avatar: '/android-icon-36x36.png'
      },
      {
        nom: 'Gates',
        prenom: 'David',
        id_pers_physique: '2',
        avatar: '/android-icon-36x36.png'
      }
    ],
    tags: ['office', 'bitcoin', 'hype', 'longer', 'fact', 'hell'],
    documents: [
      {
        id_document: '6_1',
        libelle: 'Facture 809 BEA1.pdf',
        user_id: 10,
        created_at: '2019-04-24T09:46:56.060Z',
        nom: 'Kouoi',
        prenom: 'Victor',
        src: '/android-icon-36x36.png',
        url: 'https://raw.githubusercontent.com/JsCommunity/human-format/master/LICENSE'
      },
      {
        id_document: '6_11',
        libelle: 'Document with a very very long name.jpg',
        user_id: 46,
        created_at: '2019-05-17T05:11:03.244Z',
        nom: 'x',
        prenom: 'Victor',
        src: '/android-icon-36x36.png',
        url: 'https://raw.githubusercontent.com/JsCommunity/human-format/master/LICENSE'
      },
      {
        id_document: '6_12',
        libelle: 'Test document.doc',
        user_id: 46,
        created_at: '2019-05-17T05:11:03.244Z',
        nom: 'Smith',
        prenom: 'John',
        src: '/android-icon-36x36.png',
        url: 'https://raw.githubusercontent.com/JsCommunity/human-format/master/LICENSE'
      },
      {
        id_document: '6_13',
        libelle: 'Other document.doc',
        user_id: 46,
        created_at: '2019-05-17T05:11:03.244Z',
        nom: 'Smith',
        prenom: 'John',
        src: '/android-icon-36x36.png',
        url: 'https://raw.githubusercontent.com/JsCommunity/human-format/master/LICENSE'
      }
    ]
  },
  24: {
    member_count: '3',
    room_id: 24,
    title: 'Benjamin Finot private',
    user_id: 6,
    nom: 'Finot',
    prenom: 'Benjamin',
    avatar: null,
    created_by: {
      user_id: 6,
      prenom: 'Benjamin',
      nom: 'Finot'
    },
    folder_id: 2,
    room_type_id: 1,
    is_archived: false,
    created_at: '2018-05-29T12:57:05.391Z',
    updated_at: '2018-05-29T12:57:05.391Z',
    label: 'Public',
    users: [
      {
        nom: 'Thmmolongo',
        prenom: 'Francky',
        id_pers_physique: '8',
        avatar: '/android-icon-36x36.png'
      },
      {
        nom: 'Thminot',
        prenom: 'Benjamin',
        id_pers_physique: '6',
        avatar: '/android-icon-36x36.png'
      }
    ],
    tags: ['microsoft', 'apple', 'oracle'],
    documents: [
      {
        id_document: '24_12',
        libelle: 'Twenty four twelve.doc',
        user_id: 46,
        created_at: '2019-05-17T05:11:03.244Z',
        nom: 'Smith',
        prenom: 'John',
        src: '/android-icon-36x36.png',
        url: 'https://raw.githubusercontent.com/JsCommunity/human-format/master/LICENSE'
      },
      {
        id_document: '24_13',
        libelle: 'Twenty four thurteen.doc',
        user_id: 46,
        created_at: '2019-05-17T05:11:03.244Z',
        nom: 'Smith',
        prenom: 'John',
        src: '/android-icon-36x36.png',
        url: 'https://raw.githubusercontent.com/JsCommunity/human-format/master/LICENSE'
      }
    ]
  },
  22: {
    member_count: '3',
    room_id: 22,
    title: 'Some archived room long caption',
    is_archived: true,
    user_id: 88,
    nom: 'John',
    prenom: 'Createrson',
    avatar: null,
    created_by: {
      user_id: 88,
      prenom: 'Nineghty',
      nom: 'Nine'
    },
    folder_id: 2,
    room_type_id: 1,
    created_at: '2018-05-29T12:57:05.391Z',
    updated_at: '2018-05-29T12:57:05.391Z',
    label: 'Public',
    users: [
      {
        nom: 'Shmedel',
        prenom: 'Shmapoula',
        id_pers_physique: '9',
        avatar: '/android-icon-36x36.png'
      },
      {
        nom: 'Shmmolongo',
        prenom: 'Francky',
        id_pers_physique: '8'
      },
      {
        nom: 'Shminot',
        prenom: 'Benjamin',
        id_pers_physique: '6',
        avatar: '/android-icon-36x36.png'
      }
    ],
    tags: ['yahoo', 'alta', 'vista'],
    documents: [
      {
        id_document: '22_12',
        libelle: 'Test document.doc',
        user_id: 46,
        created_at: '2019-05-17T05:11:03.244Z',
        nom: 'Smith',
        prenom: 'John',
        src: '/android-icon-36x36.png',
        url: 'https://raw.githubusercontent.com/JsCommunity/human-format/master/LICENSE'
      },
      {
        id_document: '22_13',
        libelle: 'Other document.doc',
        user_id: 46,
        created_at: '2019-05-17T05:11:03.244Z',
        nom: 'Smith',
        prenom: 'John',
        src: '/android-icon-36x36.png',
        url: 'https://raw.githubusercontent.com/JsCommunity/human-format/master/LICENSE'
      }
    ]
  }
};

export const testRoomMessages = {
  6: [
    {
      message_id: 10,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T15:59:57.074Z',
      updated_at: '2019-06-27T15:59:57.074Z',
      body: 'message 7',
      nom: 'Hellix',
      prenom: 'Albertio',
      avatar: null,
      documents: [],
      reactions: []
    },
    {
      message_id: 9,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T15:51:08.531Z',
      updated_at: '2019-06-27T15:51:08.531Z',
      body: '<span>message <i>62</i></span><br>text',
      nom: 'Finok',
      prenom: 'George',
      avatar: '/android-icon-36x36.png',
      documents: [],
      reactions: [
        {
          l_reaction_message_id: 9,
          message_id: 9,
          reaction: ':smile',
          user: {
            id: 46,
            prenom: 'Victor',
            nom: 'Johnson'
          },
          avatar: null
        },
        {
          l_reaction_message_id: 8,
          message_id: 9,
          reaction: ':smile',
          user: {
            id: 6,
            prenom: 'Finoz',
            nom: 'Benjamin'
          },
          avatar: null
        }
      ]
    },
    {
      message_id: 8,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T15:29:58.545Z',
      updated_at: '2019-06-27T15:29:58.545Z',
      body: 'message 5',
      nom: 'Finot',
      prenom: 'Benjamin',
      avatar: null,
      documents: [],
      reactions: []
    },
    {
      message_id: 4,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T08:00:09.955Z',
      updated_at: '2019-06-27T08:00:09.955Z',
      body: '#dima. #hola',
      nom: 'Finot',
      prenom: 'Benjamin',
      avatar: null,
      documents: [],
      reactions: []
    },
    {
      message_id: 3,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T07:59:03.419Z',
      updated_at: '2019-06-27T07:59:03.419Z',
      body: 'third message',
      nom: 'Finoa',
      prenom: 'Benjamin',
      avatar: null,
      documents: [
        {
          id_document: '20',
          nom_original: 'SomeDocument.odt',
          size: 3490344,
          url: 'https://example.com/SomeDocument.odt'
        },
        {
          id_document: '30',
          nom_original: 'My file 2.jpg',
          size: 3490344,
          url: 'https://example.com/MyFile2.jpg'
        },
        {
          id_document: '40',
          nom_original: 'My license long name.txt',
          size: 434434,
          url: 'https://raw.githubusercontent.com/JsCommunity/human-format/master/LICENSE'
        }
      ],
      reactions: []
    },
    {
      message_id: 2,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T07:57:47.592Z',
      updated_at: '2019-06-27T07:57:47.592Z',
      body: 'second message',
      nom: 'Sinog',
      prenom: 'Klide',
      avatar: null,
      documents: [],
      reactions: []
    },
    {
      message_id: 601,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T07:57:47.592Z',
      updated_at: '2019-06-27T07:57:47.592Z',
      body: 'sdfaasdf',
      nom: 'Finoka',
      prenom: 'Shmenj',
      avatar: null,
      documents: [],
      reactions: []
    },
    {
      message_id: 602,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T07:57:47.592Z',
      updated_at: '2019-06-27T07:57:47.592Z',
      body: '602 sdfaasdf',
      nom: 'Kinot',
      prenom: 'Ken',
      avatar: null,
      documents: [],
      reactions: []
    },
    {
      message_id: 603,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T07:57:47.592Z',
      updated_at: '2019-06-27T07:57:47.592Z',
      body: '602 sdfaasdf',
      nom: 'Tomas',
      prenom: 'Albert',
      avatar: null,
      documents: [],
      reactions: []
    },
    {
      message_id: 604,
      room_id: 6,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T07:57:47.592Z',
      updated_at: '2019-06-27T07:57:47.592Z',
      body: '603 sdfaasdf',
      nom: 'Finot',
      prenom: 'Benjamin',
      avatar: null,
      documents: [],
      reactions: []
    }
  ],
  24: [
    {
      message_id: 243,
      room_id: 24,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T07:59:03.419Z',
      updated_at: '2019-06-27T07:59:03.419Z',
      body: 'Private message 1',
      nom: 'Finoks',
      prenom: 'Klide',
      avatar: null,
      documents: [],
      reactions: []
    },
    {
      message_id: 242,
      room_id: 24,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T07:57:47.592Z',
      updated_at: '2019-06-27T07:57:47.592Z',
      body: 'Other private message',
      nom: 'Finor',
      prenom: 'Ralph',
      avatar: null,
      documents: [],
      reactions: []
    }
  ],
  22: [
    {
      message_id: 243,
      room_id: 22,
      user_id: 6,
      is_updated: false,
      created_at: '2019-06-27T07:59:03.419Z',
      updated_at: '2019-06-27T07:59:03.419Z',
      body: 'My message 1',
      nom: 'Fino',
      prenom: 'Olie',
      avatar: null,
      documents: [],
      reactions: []
    }
  ]
};


export const testRoomUsers = [
  {
    id_pers_physique: 'selma-zaim',
    nom: 'Zaim',
    prenom: 'Selma',
    avatar: '/android-icon-36x36.png'
  },
  {
    id_pers_physique: 'victor-kouoi',
    prenom: 'Victor',
    nom: 'Knom',
    avatar: '/android-icon-36x36.png'
  },
  {
    id_pers_physique: 'emma-bovary',
    prenom: 'Emma',
    nom: 'Smith',
    avatar: '/android-icon-36x36.png'
  },
  {
    id_pers_physique: 'cyril-mandrilly',
    prenom: 'Cyril',
    nom: 'Mandrilly',
    avatar: '/android-icon-36x36.png'
  },
  {
    id_pers_physique: 'bill-gates',
    prenom: 'Bill',
    nom: 'Gates',
    avatar: '/android-icon-36x36.png'
  },
  {
    id_pers_physique: 'bill-clinton',
    prenom: 'Bill',
    nom: 'Clinton',
    avatar: '/android-icon-36x36.png'
  },
  {
    id_pers_physique: 'bill-layall',
    prenom: 'Bill',
    nom: 'Layel',
    avatar: '/android-icon-36x36.png'
  },
  {
    id_pers_physique: 'bill-smith',
    prenom: 'Bill',
    nom: 'Jackson',
    avatar: '/android-icon-36x36.png'
  }
];

export const testRoomAvailableUsers = [
  {
    id_pers_physique: 'john-smith',
    prenom: 'Johna',
    nom: 'Smitha',
    avatar: '/android-icon-36x36.png'
  },
  {
    id_pers_physique: 'jack-brown',
    prenom: 'Jacka',
    nom: 'Browna',
    avatar: '/android-icon-36x36.png'
  }
];

// roomId: roomGEDItems
export const testGEDItems = {
  6: [
    {
      id: 'Facturexy(1).pdf',
      filename: 'Facturexy(1).pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1245.pdf',
      filename: 'Factuire 1245.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1246.pdf',
      filename: 'Factuire 1246.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1247.pdf',
      filename: 'Factuire 1247.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1248.pdf',
      filename: 'Factuire 1248.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1249.pdf',
      filename: 'Factuire 1249.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1250.pdf',
      filename: 'Factuire 1250.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1251.pdf',
      filename: 'Factuire 1251.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1252.pdf',
      filename: 'Factuire 1252.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1253.pdf',
      filename: 'Factuire 1253.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1254.pdf',
      filename: 'Factuire 1254.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1255.pdf',
      filename: 'Factuire 1255.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    },
    {
      id: 'Factuire 1256.pdf',
      filename: 'Factuire 1256.pdf',
      preview: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Hereford-Karte.jpg'
    }
  ]
};
