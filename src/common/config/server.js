export const windev_conf = {
  protocol: 'https',
  baseUrl: 'ws-dev.myunisoft.fr',
  name: 'Windev',
  apiVersion: 1
};

export const service_auth_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1337',
  name: 'Auth',
  apiVersion: 1
};

export const service_notification_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1342'
};

export const service_discussion_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1345',
  name: 'Discussions',
  apiVersion: 1
};

export const service_ged_conf = {
  protocol: 'https',
  baseUrl: 'yoda.myunisoft.fr',
  port: '1350',
  name: 'Ged',
  apiVersion: 1
};

export const mock_conf = {
  protocol: 'https',
  baseUrl: '1f9d0697-227d-4bbb-ada9-e92772f9c5ad.mock.pstmn.io/'
};

export const redirect_url_budget = {
  protocol: 'https',
  baseUrl: 'palpatine.myunisoft.fr',
  port: '5000'
};

export const download_dynamic = {
  protocol: 'https',
  baseUrl: 'www.myunisoft.fr',
  path: 'outils/liens-dynamiques/Liens-Dynamiques.xlsm'
};

export const mode = 'dev';
