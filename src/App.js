import React, { Component } from 'react';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import { store, history, persistor } from 'redux/store';
import { ConnectedRouter } from 'connected-react-router';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
import { PersistGate } from 'redux-persist/integration/react';
import Routes from 'routes';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import { IconButton } from '@material-ui/core';
import theme from 'assets/theme';
import shortcutManager from 'assets/config/shortcuts';
import { FontIcon } from 'components/basics/Icon';
import 'assets/config/numeral';
import Notifier from 'containers/basics/Notifier';
import OpenerMessages from 'containers/basics/OpenerMessages';
import WindowManager from 'containers/basics/WindowManager';
import { SnackbarProvider } from 'notistack';
import 'moment/locale/fr';

require('events').EventEmitter.defaultMaxListeners = 0;

const classes = ({ palette }) => ({
  warning: {
    backgroundColor: '#ffcfd8',
    borderRadius: 2,
    color: palette.error.main,
    fontSize: 16,
    boxShadow: 'none'
  },
  root: {
    zIndex: 3001
  }
});
class App extends Component {
  constructor(props) {
    super(props);
    this.notistackRef = React.createRef();
  }

  getChildContext() {
    return { shortcuts: shortcutManager };
  }

  onClickDismiss(key) {
    this.notistackRef.current.handleDismissSnack(key);
  }

  render() {
    const {
      classes
    } = this.props;

    return (
      <Provider store={store}>
        <OpenerMessages>
          <PersistGate persistor={persistor} loading={<div />}>
            <WindowManager>
              <ConnectedRouter history={history}>
                <MuiThemeProvider theme={theme}>
                  <MuiPickersUtilsProvider utils={MomentUtils}>
                    <SnackbarProvider
                      maxSnack={4}
                      ref={this.notistackRef}
                      action={key => (
                        <IconButton onClick={() => this.onClickDismiss(key)}>
                          <FontIcon name="icon-close" />
                        </IconButton>
                      )}
                      anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                      }}
                      classes={{
                        root: classes.root,
                        variantWarning: classes.warning
                      }}
                      persist
                      iconVariant={{
                        warning: <FontIcon
                          marginRight={8}
                          name="icon-alert"
                          size={25}
                          color="error"
                        />
                      }}
                    >
                      <Notifier />
                      <Routes />
                    </SnackbarProvider>
                  </MuiPickersUtilsProvider>
                </MuiThemeProvider>
              </ConnectedRouter>
            </WindowManager>
          </PersistGate>
        </OpenerMessages>
      </Provider>
    );
  }
}

App.childContextTypes = {
  shortcuts: PropTypes.shape({}).isRequired
};

export default withStyles(classes)(App);
